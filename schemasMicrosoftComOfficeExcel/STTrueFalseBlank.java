// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComOfficeExcel;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STTrueFalseBlank extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTrueFalseBlank.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttruefalseblanka061type");
    public static final Enum TRUE = Enum.forString("True");
    public static final Enum T = Enum.forString("t");
    public static final Enum FALSE = Enum.forString("False");
    public static final Enum F = Enum.forString("f");
    public static final Enum X = Enum.forString("");
    public static final int INT_TRUE = 1;
    public static final int INT_T = 2;
    public static final int INT_FALSE = 3;
    public static final int INT_F = 4;
    public static final int INT_X = 5;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STTrueFalseBlank newValue(final Object o) {
            return (STTrueFalseBlank)STTrueFalseBlank.type.newValue(o);
        }
        
        public static STTrueFalseBlank newInstance() {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().newInstance(STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank newInstance(final XmlOptions xmlOptions) {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().newInstance(STTrueFalseBlank.type, xmlOptions);
        }
        
        public static STTrueFalseBlank parse(final String s) throws XmlException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(s, STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(s, STTrueFalseBlank.type, xmlOptions);
        }
        
        public static STTrueFalseBlank parse(final File file) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(file, STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(file, STTrueFalseBlank.type, xmlOptions);
        }
        
        public static STTrueFalseBlank parse(final URL url) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(url, STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(url, STTrueFalseBlank.type, xmlOptions);
        }
        
        public static STTrueFalseBlank parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(inputStream, STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(inputStream, STTrueFalseBlank.type, xmlOptions);
        }
        
        public static STTrueFalseBlank parse(final Reader reader) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(reader, STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(reader, STTrueFalseBlank.type, xmlOptions);
        }
        
        public static STTrueFalseBlank parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTrueFalseBlank.type, xmlOptions);
        }
        
        public static STTrueFalseBlank parse(final Node node) throws XmlException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(node, STTrueFalseBlank.type, null);
        }
        
        public static STTrueFalseBlank parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(node, STTrueFalseBlank.type, xmlOptions);
        }
        
        @Deprecated
        public static STTrueFalseBlank parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTrueFalseBlank.type, null);
        }
        
        @Deprecated
        public static STTrueFalseBlank parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTrueFalseBlank)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTrueFalseBlank.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTrueFalseBlank.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTrueFalseBlank.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_TRUE = 1;
        static final int INT_T = 2;
        static final int INT_FALSE = 3;
        static final int INT_F = 4;
        static final int INT_X = 5;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("True", 1), new Enum("t", 2), new Enum("False", 3), new Enum("f", 4), new Enum("", 5) });
        }
    }
}
