// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComOfficeExcel.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComOfficeExcel.STTrueFalseBlank;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTrueFalseBlankImpl extends JavaStringEnumerationHolderEx implements STTrueFalseBlank
{
    public STTrueFalseBlankImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTrueFalseBlankImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
