// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComOfficeExcel.impl;

import schemasMicrosoftComOfficeExcel.STObjectType;
import org.apache.xmlbeans.XmlNonNegativeInteger;
import schemasMicrosoftComOfficeExcel.STCF;
import org.apache.xmlbeans.XmlInteger;
import java.math.BigInteger;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SimpleValue;
import java.util.ArrayList;
import schemasMicrosoftComOfficeExcel.STTrueFalseBlank;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import schemasMicrosoftComOfficeExcel.CTClientData;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTClientDataImpl extends XmlComplexContentImpl implements CTClientData
{
    private static final QName MOVEWITHCELLS$0;
    private static final QName SIZEWITHCELLS$2;
    private static final QName ANCHOR$4;
    private static final QName LOCKED$6;
    private static final QName DEFAULTSIZE$8;
    private static final QName PRINTOBJECT$10;
    private static final QName DISABLED$12;
    private static final QName AUTOFILL$14;
    private static final QName AUTOLINE$16;
    private static final QName AUTOPICT$18;
    private static final QName FMLAMACRO$20;
    private static final QName TEXTHALIGN$22;
    private static final QName TEXTVALIGN$24;
    private static final QName LOCKTEXT$26;
    private static final QName JUSTLASTX$28;
    private static final QName SECRETEDIT$30;
    private static final QName DEFAULT$32;
    private static final QName HELP$34;
    private static final QName CANCEL$36;
    private static final QName DISMISS$38;
    private static final QName ACCEL$40;
    private static final QName ACCEL2$42;
    private static final QName ROW$44;
    private static final QName COLUMN$46;
    private static final QName VISIBLE$48;
    private static final QName ROWHIDDEN$50;
    private static final QName COLHIDDEN$52;
    private static final QName VTEDIT$54;
    private static final QName MULTILINE$56;
    private static final QName VSCROLL$58;
    private static final QName VALIDIDS$60;
    private static final QName FMLARANGE$62;
    private static final QName WIDTHMIN$64;
    private static final QName SEL$66;
    private static final QName NOTHREED2$68;
    private static final QName SELTYPE$70;
    private static final QName MULTISEL$72;
    private static final QName LCT$74;
    private static final QName LISTITEM$76;
    private static final QName DROPSTYLE$78;
    private static final QName COLORED$80;
    private static final QName DROPLINES$82;
    private static final QName CHECKED$84;
    private static final QName FMLALINK$86;
    private static final QName FMLAPICT$88;
    private static final QName NOTHREED$90;
    private static final QName FIRSTBUTTON$92;
    private static final QName FMLAGROUP$94;
    private static final QName VAL$96;
    private static final QName MIN$98;
    private static final QName MAX$100;
    private static final QName INC$102;
    private static final QName PAGE$104;
    private static final QName HORIZ$106;
    private static final QName DX$108;
    private static final QName MAPOCX$110;
    private static final QName CF$112;
    private static final QName CAMERA$114;
    private static final QName RECALCALWAYS$116;
    private static final QName AUTOSCALE$118;
    private static final QName DDE$120;
    private static final QName UIOBJ$122;
    private static final QName SCRIPTTEXT$124;
    private static final QName SCRIPTEXTENDED$126;
    private static final QName SCRIPTLANGUAGE$128;
    private static final QName SCRIPTLOCATION$130;
    private static final QName FMLATXBX$132;
    private static final QName OBJECTTYPE$134;
    
    public CTClientDataImpl(final SchemaType type) {
        super(type);
    }
    
    public List<STTrueFalseBlank.Enum> getMoveWithCellsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.MoveWithCellsList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getMoveWithCellsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.MOVEWITHCELLS$0, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getMoveWithCellsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MOVEWITHCELLS$0, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetMoveWithCellsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.MoveWithCellsList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetMoveWithCellsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.MOVEWITHCELLS$0, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetMoveWithCellsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.MOVEWITHCELLS$0, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfMoveWithCellsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.MOVEWITHCELLS$0);
        }
    }
    
    public void setMoveWithCellsArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MOVEWITHCELLS$0);
        }
    }
    
    public void setMoveWithCellsArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MOVEWITHCELLS$0, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetMoveWithCellsArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MOVEWITHCELLS$0);
        }
    }
    
    public void xsetMoveWithCellsArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.MOVEWITHCELLS$0, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertMoveWithCells(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.MOVEWITHCELLS$0, n)).setEnumValue(enumValue);
        }
    }
    
    public void addMoveWithCells(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.MOVEWITHCELLS$0)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewMoveWithCells(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.MOVEWITHCELLS$0, n);
        }
    }
    
    public STTrueFalseBlank addNewMoveWithCells() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.MOVEWITHCELLS$0);
        }
    }
    
    public void removeMoveWithCells(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.MOVEWITHCELLS$0, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getSizeWithCellsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.SizeWithCellsList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getSizeWithCellsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SIZEWITHCELLS$2, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getSizeWithCellsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SIZEWITHCELLS$2, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetSizeWithCellsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.SizeWithCellsList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetSizeWithCellsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SIZEWITHCELLS$2, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetSizeWithCellsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.SIZEWITHCELLS$2, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfSizeWithCellsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SIZEWITHCELLS$2);
        }
    }
    
    public void setSizeWithCellsArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SIZEWITHCELLS$2);
        }
    }
    
    public void setSizeWithCellsArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SIZEWITHCELLS$2, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetSizeWithCellsArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SIZEWITHCELLS$2);
        }
    }
    
    public void xsetSizeWithCellsArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.SIZEWITHCELLS$2, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertSizeWithCells(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SIZEWITHCELLS$2, n)).setEnumValue(enumValue);
        }
    }
    
    public void addSizeWithCells(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SIZEWITHCELLS$2)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewSizeWithCells(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.SIZEWITHCELLS$2, n);
        }
    }
    
    public STTrueFalseBlank addNewSizeWithCells() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.SIZEWITHCELLS$2);
        }
    }
    
    public void removeSizeWithCells(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SIZEWITHCELLS$2, n);
        }
    }
    
    public List<String> getAnchorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.AnchorList(this);
        }
    }
    
    public String[] getAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.ANCHOR$4, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getAnchorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ANCHOR$4, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetAnchorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.AnchorList(this);
        }
    }
    
    public XmlString[] xgetAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.ANCHOR$4, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetAnchorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.ANCHOR$4, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.ANCHOR$4);
        }
    }
    
    public void setAnchorArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ANCHOR$4);
        }
    }
    
    public void setAnchorArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ANCHOR$4, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetAnchorArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ANCHOR$4);
        }
    }
    
    public void xsetAnchorArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.ANCHOR$4, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertAnchor(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.ANCHOR$4, n)).setStringValue(stringValue);
        }
    }
    
    public void addAnchor(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.ANCHOR$4)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.ANCHOR$4, n);
        }
    }
    
    public XmlString addNewAnchor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.ANCHOR$4);
        }
    }
    
    public void removeAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.ANCHOR$4, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getLockedList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.LockedList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getLockedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.LOCKED$6, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getLockedArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LOCKED$6, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetLockedList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.LockedList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetLockedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.LOCKED$6, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetLockedArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.LOCKED$6, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfLockedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.LOCKED$6);
        }
    }
    
    public void setLockedArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LOCKED$6);
        }
    }
    
    public void setLockedArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LOCKED$6, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetLockedArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LOCKED$6);
        }
    }
    
    public void xsetLockedArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.LOCKED$6, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertLocked(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.LOCKED$6, n)).setEnumValue(enumValue);
        }
    }
    
    public void addLocked(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.LOCKED$6)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewLocked(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.LOCKED$6, n);
        }
    }
    
    public STTrueFalseBlank addNewLocked() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.LOCKED$6);
        }
    }
    
    public void removeLocked(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.LOCKED$6, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getDefaultSizeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.DefaultSizeList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getDefaultSizeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DEFAULTSIZE$8, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getDefaultSizeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DEFAULTSIZE$8, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetDefaultSizeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.DefaultSizeList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetDefaultSizeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DEFAULTSIZE$8, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetDefaultSizeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DEFAULTSIZE$8, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfDefaultSizeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DEFAULTSIZE$8);
        }
    }
    
    public void setDefaultSizeArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DEFAULTSIZE$8);
        }
    }
    
    public void setDefaultSizeArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DEFAULTSIZE$8, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetDefaultSizeArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DEFAULTSIZE$8);
        }
    }
    
    public void xsetDefaultSizeArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DEFAULTSIZE$8, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertDefaultSize(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DEFAULTSIZE$8, n)).setEnumValue(enumValue);
        }
    }
    
    public void addDefaultSize(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DEFAULTSIZE$8)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewDefaultSize(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.DEFAULTSIZE$8, n);
        }
    }
    
    public STTrueFalseBlank addNewDefaultSize() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.DEFAULTSIZE$8);
        }
    }
    
    public void removeDefaultSize(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DEFAULTSIZE$8, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getPrintObjectList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.PrintObjectList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getPrintObjectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.PRINTOBJECT$10, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getPrintObjectArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.PRINTOBJECT$10, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetPrintObjectList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.PrintObjectList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetPrintObjectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.PRINTOBJECT$10, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetPrintObjectArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.PRINTOBJECT$10, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfPrintObjectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.PRINTOBJECT$10);
        }
    }
    
    public void setPrintObjectArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.PRINTOBJECT$10);
        }
    }
    
    public void setPrintObjectArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.PRINTOBJECT$10, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetPrintObjectArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.PRINTOBJECT$10);
        }
    }
    
    public void xsetPrintObjectArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.PRINTOBJECT$10, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertPrintObject(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.PRINTOBJECT$10, n)).setEnumValue(enumValue);
        }
    }
    
    public void addPrintObject(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.PRINTOBJECT$10)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewPrintObject(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.PRINTOBJECT$10, n);
        }
    }
    
    public STTrueFalseBlank addNewPrintObject() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.PRINTOBJECT$10);
        }
    }
    
    public void removePrintObject(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.PRINTOBJECT$10, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getDisabledList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.DisabledList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getDisabledArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DISABLED$12, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getDisabledArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DISABLED$12, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetDisabledList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.DisabledList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetDisabledArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DISABLED$12, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetDisabledArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DISABLED$12, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfDisabledArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DISABLED$12);
        }
    }
    
    public void setDisabledArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DISABLED$12);
        }
    }
    
    public void setDisabledArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DISABLED$12, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetDisabledArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DISABLED$12);
        }
    }
    
    public void xsetDisabledArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DISABLED$12, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertDisabled(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DISABLED$12, n)).setEnumValue(enumValue);
        }
    }
    
    public void addDisabled(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DISABLED$12)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewDisabled(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.DISABLED$12, n);
        }
    }
    
    public STTrueFalseBlank addNewDisabled() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.DISABLED$12);
        }
    }
    
    public void removeDisabled(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DISABLED$12, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getAutoFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.AutoFillList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getAutoFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOFILL$14, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getAutoFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOFILL$14, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetAutoFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.AutoFillList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetAutoFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOFILL$14, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetAutoFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOFILL$14, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfAutoFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.AUTOFILL$14);
        }
    }
    
    public void setAutoFillArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOFILL$14);
        }
    }
    
    public void setAutoFillArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOFILL$14, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetAutoFillArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOFILL$14);
        }
    }
    
    public void xsetAutoFillArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOFILL$14, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertAutoFill(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.AUTOFILL$14, n)).setEnumValue(enumValue);
        }
    }
    
    public void addAutoFill(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.AUTOFILL$14)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewAutoFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.AUTOFILL$14, n);
        }
    }
    
    public STTrueFalseBlank addNewAutoFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.AUTOFILL$14);
        }
    }
    
    public void removeAutoFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.AUTOFILL$14, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getAutoLineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.AutoLineList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getAutoLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOLINE$16, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getAutoLineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOLINE$16, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetAutoLineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.AutoLineList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetAutoLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOLINE$16, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetAutoLineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOLINE$16, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfAutoLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.AUTOLINE$16);
        }
    }
    
    public void setAutoLineArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOLINE$16);
        }
    }
    
    public void setAutoLineArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOLINE$16, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetAutoLineArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOLINE$16);
        }
    }
    
    public void xsetAutoLineArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOLINE$16, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertAutoLine(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.AUTOLINE$16, n)).setEnumValue(enumValue);
        }
    }
    
    public void addAutoLine(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.AUTOLINE$16)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewAutoLine(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.AUTOLINE$16, n);
        }
    }
    
    public STTrueFalseBlank addNewAutoLine() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.AUTOLINE$16);
        }
    }
    
    public void removeAutoLine(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.AUTOLINE$16, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getAutoPictList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.AutoPictList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getAutoPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOPICT$18, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getAutoPictArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOPICT$18, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetAutoPictList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.AutoPictList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetAutoPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOPICT$18, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetAutoPictArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOPICT$18, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfAutoPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.AUTOPICT$18);
        }
    }
    
    public void setAutoPictArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOPICT$18);
        }
    }
    
    public void setAutoPictArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOPICT$18, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetAutoPictArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOPICT$18);
        }
    }
    
    public void xsetAutoPictArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOPICT$18, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertAutoPict(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.AUTOPICT$18, n)).setEnumValue(enumValue);
        }
    }
    
    public void addAutoPict(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.AUTOPICT$18)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewAutoPict(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.AUTOPICT$18, n);
        }
    }
    
    public STTrueFalseBlank addNewAutoPict() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.AUTOPICT$18);
        }
    }
    
    public void removeAutoPict(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.AUTOPICT$18, n);
        }
    }
    
    public List<String> getFmlaMacroList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.FmlaMacroList(this);
        }
    }
    
    public String[] getFmlaMacroArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLAMACRO$20, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getFmlaMacroArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLAMACRO$20, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetFmlaMacroList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.FmlaMacroList(this);
        }
    }
    
    public XmlString[] xgetFmlaMacroArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLAMACRO$20, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetFmlaMacroArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLAMACRO$20, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfFmlaMacroArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.FMLAMACRO$20);
        }
    }
    
    public void setFmlaMacroArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLAMACRO$20);
        }
    }
    
    public void setFmlaMacroArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLAMACRO$20, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetFmlaMacroArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLAMACRO$20);
        }
    }
    
    public void xsetFmlaMacroArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLAMACRO$20, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertFmlaMacro(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.FMLAMACRO$20, n)).setStringValue(stringValue);
        }
    }
    
    public void addFmlaMacro(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.FMLAMACRO$20)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewFmlaMacro(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.FMLAMACRO$20, n);
        }
    }
    
    public XmlString addNewFmlaMacro() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.FMLAMACRO$20);
        }
    }
    
    public void removeFmlaMacro(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.FMLAMACRO$20, n);
        }
    }
    
    public List<String> getTextHAlignList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.TextHAlignList(this);
        }
    }
    
    public String[] getTextHAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.TEXTHALIGN$22, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getTextHAlignArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.TEXTHALIGN$22, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetTextHAlignList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.TextHAlignList(this);
        }
    }
    
    public XmlString[] xgetTextHAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.TEXTHALIGN$22, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetTextHAlignArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.TEXTHALIGN$22, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfTextHAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.TEXTHALIGN$22);
        }
    }
    
    public void setTextHAlignArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.TEXTHALIGN$22);
        }
    }
    
    public void setTextHAlignArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.TEXTHALIGN$22, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetTextHAlignArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.TEXTHALIGN$22);
        }
    }
    
    public void xsetTextHAlignArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.TEXTHALIGN$22, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertTextHAlign(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.TEXTHALIGN$22, n)).setStringValue(stringValue);
        }
    }
    
    public void addTextHAlign(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.TEXTHALIGN$22)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewTextHAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.TEXTHALIGN$22, n);
        }
    }
    
    public XmlString addNewTextHAlign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.TEXTHALIGN$22);
        }
    }
    
    public void removeTextHAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.TEXTHALIGN$22, n);
        }
    }
    
    public List<String> getTextVAlignList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.TextVAlignList(this);
        }
    }
    
    public String[] getTextVAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.TEXTVALIGN$24, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getTextVAlignArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.TEXTVALIGN$24, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetTextVAlignList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.TextVAlignList(this);
        }
    }
    
    public XmlString[] xgetTextVAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.TEXTVALIGN$24, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetTextVAlignArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.TEXTVALIGN$24, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfTextVAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.TEXTVALIGN$24);
        }
    }
    
    public void setTextVAlignArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.TEXTVALIGN$24);
        }
    }
    
    public void setTextVAlignArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.TEXTVALIGN$24, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetTextVAlignArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.TEXTVALIGN$24);
        }
    }
    
    public void xsetTextVAlignArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.TEXTVALIGN$24, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertTextVAlign(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.TEXTVALIGN$24, n)).setStringValue(stringValue);
        }
    }
    
    public void addTextVAlign(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.TEXTVALIGN$24)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewTextVAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.TEXTVALIGN$24, n);
        }
    }
    
    public XmlString addNewTextVAlign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.TEXTVALIGN$24);
        }
    }
    
    public void removeTextVAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.TEXTVALIGN$24, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getLockTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.LockTextList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getLockTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.LOCKTEXT$26, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getLockTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LOCKTEXT$26, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetLockTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.LockTextList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetLockTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.LOCKTEXT$26, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetLockTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.LOCKTEXT$26, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfLockTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.LOCKTEXT$26);
        }
    }
    
    public void setLockTextArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LOCKTEXT$26);
        }
    }
    
    public void setLockTextArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LOCKTEXT$26, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetLockTextArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LOCKTEXT$26);
        }
    }
    
    public void xsetLockTextArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.LOCKTEXT$26, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertLockText(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.LOCKTEXT$26, n)).setEnumValue(enumValue);
        }
    }
    
    public void addLockText(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.LOCKTEXT$26)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewLockText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.LOCKTEXT$26, n);
        }
    }
    
    public STTrueFalseBlank addNewLockText() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.LOCKTEXT$26);
        }
    }
    
    public void removeLockText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.LOCKTEXT$26, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getJustLastXList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.JustLastXList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getJustLastXArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.JUSTLASTX$28, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getJustLastXArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.JUSTLASTX$28, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetJustLastXList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.JustLastXList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetJustLastXArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.JUSTLASTX$28, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetJustLastXArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.JUSTLASTX$28, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfJustLastXArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.JUSTLASTX$28);
        }
    }
    
    public void setJustLastXArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.JUSTLASTX$28);
        }
    }
    
    public void setJustLastXArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.JUSTLASTX$28, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetJustLastXArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.JUSTLASTX$28);
        }
    }
    
    public void xsetJustLastXArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.JUSTLASTX$28, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertJustLastX(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.JUSTLASTX$28, n)).setEnumValue(enumValue);
        }
    }
    
    public void addJustLastX(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.JUSTLASTX$28)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewJustLastX(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.JUSTLASTX$28, n);
        }
    }
    
    public STTrueFalseBlank addNewJustLastX() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.JUSTLASTX$28);
        }
    }
    
    public void removeJustLastX(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.JUSTLASTX$28, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getSecretEditList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.SecretEditList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getSecretEditArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SECRETEDIT$30, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getSecretEditArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SECRETEDIT$30, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetSecretEditList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.SecretEditList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetSecretEditArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SECRETEDIT$30, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetSecretEditArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.SECRETEDIT$30, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfSecretEditArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SECRETEDIT$30);
        }
    }
    
    public void setSecretEditArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SECRETEDIT$30);
        }
    }
    
    public void setSecretEditArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SECRETEDIT$30, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetSecretEditArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SECRETEDIT$30);
        }
    }
    
    public void xsetSecretEditArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.SECRETEDIT$30, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertSecretEdit(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SECRETEDIT$30, n)).setEnumValue(enumValue);
        }
    }
    
    public void addSecretEdit(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SECRETEDIT$30)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewSecretEdit(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.SECRETEDIT$30, n);
        }
    }
    
    public STTrueFalseBlank addNewSecretEdit() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.SECRETEDIT$30);
        }
    }
    
    public void removeSecretEdit(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SECRETEDIT$30, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getDefaultList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.DefaultList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getDefaultArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DEFAULT$32, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getDefaultArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DEFAULT$32, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetDefaultList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.DefaultList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetDefaultArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DEFAULT$32, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetDefaultArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DEFAULT$32, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfDefaultArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DEFAULT$32);
        }
    }
    
    public void setDefaultArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DEFAULT$32);
        }
    }
    
    public void setDefaultArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DEFAULT$32, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetDefaultArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DEFAULT$32);
        }
    }
    
    public void xsetDefaultArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DEFAULT$32, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertDefault(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DEFAULT$32, n)).setEnumValue(enumValue);
        }
    }
    
    public void addDefault(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DEFAULT$32)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewDefault(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.DEFAULT$32, n);
        }
    }
    
    public STTrueFalseBlank addNewDefault() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.DEFAULT$32);
        }
    }
    
    public void removeDefault(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DEFAULT$32, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getHelpList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.HelpList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getHelpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.HELP$34, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getHelpArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.HELP$34, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetHelpList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.HelpList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetHelpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.HELP$34, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetHelpArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.HELP$34, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfHelpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.HELP$34);
        }
    }
    
    public void setHelpArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.HELP$34);
        }
    }
    
    public void setHelpArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.HELP$34, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetHelpArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.HELP$34);
        }
    }
    
    public void xsetHelpArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.HELP$34, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertHelp(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.HELP$34, n)).setEnumValue(enumValue);
        }
    }
    
    public void addHelp(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.HELP$34)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewHelp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.HELP$34, n);
        }
    }
    
    public STTrueFalseBlank addNewHelp() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.HELP$34);
        }
    }
    
    public void removeHelp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.HELP$34, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getCancelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.CancelList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getCancelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.CANCEL$36, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getCancelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CANCEL$36, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetCancelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.CancelList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetCancelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.CANCEL$36, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetCancelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.CANCEL$36, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfCancelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.CANCEL$36);
        }
    }
    
    public void setCancelArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.CANCEL$36);
        }
    }
    
    public void setCancelArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CANCEL$36, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetCancelArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.CANCEL$36);
        }
    }
    
    public void xsetCancelArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.CANCEL$36, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertCancel(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.CANCEL$36, n)).setEnumValue(enumValue);
        }
    }
    
    public void addCancel(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.CANCEL$36)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewCancel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.CANCEL$36, n);
        }
    }
    
    public STTrueFalseBlank addNewCancel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.CANCEL$36);
        }
    }
    
    public void removeCancel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.CANCEL$36, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getDismissList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.DismissList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getDismissArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DISMISS$38, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getDismissArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DISMISS$38, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetDismissList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.DismissList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetDismissArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DISMISS$38, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetDismissArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DISMISS$38, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfDismissArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DISMISS$38);
        }
    }
    
    public void setDismissArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DISMISS$38);
        }
    }
    
    public void setDismissArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DISMISS$38, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetDismissArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DISMISS$38);
        }
    }
    
    public void xsetDismissArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DISMISS$38, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertDismiss(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DISMISS$38, n)).setEnumValue(enumValue);
        }
    }
    
    public void addDismiss(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DISMISS$38)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewDismiss(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.DISMISS$38, n);
        }
    }
    
    public STTrueFalseBlank addNewDismiss() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.DISMISS$38);
        }
    }
    
    public void removeDismiss(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DISMISS$38, n);
        }
    }
    
    public List<BigInteger> getAccelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.AccelList(this);
        }
    }
    
    public BigInteger[] getAccelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.ACCEL$40, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getAccelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ACCEL$40, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetAccelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.AccelList(this);
        }
    }
    
    public XmlInteger[] xgetAccelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.ACCEL$40, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetAccelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.ACCEL$40, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfAccelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.ACCEL$40);
        }
    }
    
    public void setAccelArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ACCEL$40);
        }
    }
    
    public void setAccelArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ACCEL$40, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetAccelArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ACCEL$40);
        }
    }
    
    public void xsetAccelArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.ACCEL$40, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertAccel(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.ACCEL$40, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addAccel(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.ACCEL$40)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewAccel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.ACCEL$40, n);
        }
    }
    
    public XmlInteger addNewAccel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.ACCEL$40);
        }
    }
    
    public void removeAccel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.ACCEL$40, n);
        }
    }
    
    public List<BigInteger> getAccel2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.Accel2List(this);
        }
    }
    
    public BigInteger[] getAccel2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.ACCEL2$42, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getAccel2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ACCEL2$42, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetAccel2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.Accel2List(this);
        }
    }
    
    public XmlInteger[] xgetAccel2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.ACCEL2$42, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetAccel2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.ACCEL2$42, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfAccel2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.ACCEL2$42);
        }
    }
    
    public void setAccel2Array(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ACCEL2$42);
        }
    }
    
    public void setAccel2Array(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ACCEL2$42, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetAccel2Array(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ACCEL2$42);
        }
    }
    
    public void xsetAccel2Array(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.ACCEL2$42, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertAccel2(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.ACCEL2$42, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addAccel2(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.ACCEL2$42)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewAccel2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.ACCEL2$42, n);
        }
    }
    
    public XmlInteger addNewAccel2() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.ACCEL2$42);
        }
    }
    
    public void removeAccel2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.ACCEL2$42, n);
        }
    }
    
    public List<BigInteger> getRowList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.RowList(this);
        }
    }
    
    public BigInteger[] getRowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.ROW$44, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getRowArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ROW$44, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetRowList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.RowList(this);
        }
    }
    
    public XmlInteger[] xgetRowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.ROW$44, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetRowArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.ROW$44, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfRowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.ROW$44);
        }
    }
    
    public void setRowArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ROW$44);
        }
    }
    
    public void setRowArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ROW$44, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetRowArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ROW$44);
        }
    }
    
    public void xsetRowArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.ROW$44, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertRow(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.ROW$44, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addRow(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.ROW$44)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewRow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.ROW$44, n);
        }
    }
    
    public XmlInteger addNewRow() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.ROW$44);
        }
    }
    
    public void removeRow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.ROW$44, n);
        }
    }
    
    public List<BigInteger> getColumnList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.ColumnList(this);
        }
    }
    
    public BigInteger[] getColumnArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.COLUMN$46, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getColumnArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.COLUMN$46, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetColumnList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.ColumnList(this);
        }
    }
    
    public XmlInteger[] xgetColumnArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.COLUMN$46, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetColumnArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.COLUMN$46, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfColumnArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.COLUMN$46);
        }
    }
    
    public void setColumnArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.COLUMN$46);
        }
    }
    
    public void setColumnArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.COLUMN$46, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetColumnArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.COLUMN$46);
        }
    }
    
    public void xsetColumnArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.COLUMN$46, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertColumn(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.COLUMN$46, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addColumn(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.COLUMN$46)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewColumn(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.COLUMN$46, n);
        }
    }
    
    public XmlInteger addNewColumn() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.COLUMN$46);
        }
    }
    
    public void removeColumn(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.COLUMN$46, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getVisibleList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.VisibleList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getVisibleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.VISIBLE$48, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getVisibleArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VISIBLE$48, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetVisibleList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.VisibleList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetVisibleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.VISIBLE$48, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetVisibleArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.VISIBLE$48, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfVisibleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.VISIBLE$48);
        }
    }
    
    public void setVisibleArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VISIBLE$48);
        }
    }
    
    public void setVisibleArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VISIBLE$48, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetVisibleArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VISIBLE$48);
        }
    }
    
    public void xsetVisibleArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.VISIBLE$48, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertVisible(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.VISIBLE$48, n)).setEnumValue(enumValue);
        }
    }
    
    public void addVisible(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.VISIBLE$48)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewVisible(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.VISIBLE$48, n);
        }
    }
    
    public STTrueFalseBlank addNewVisible() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.VISIBLE$48);
        }
    }
    
    public void removeVisible(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.VISIBLE$48, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getRowHiddenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.RowHiddenList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getRowHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.ROWHIDDEN$50, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getRowHiddenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ROWHIDDEN$50, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetRowHiddenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.RowHiddenList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetRowHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.ROWHIDDEN$50, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetRowHiddenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.ROWHIDDEN$50, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfRowHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.ROWHIDDEN$50);
        }
    }
    
    public void setRowHiddenArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ROWHIDDEN$50);
        }
    }
    
    public void setRowHiddenArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.ROWHIDDEN$50, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetRowHiddenArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.ROWHIDDEN$50);
        }
    }
    
    public void xsetRowHiddenArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.ROWHIDDEN$50, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertRowHidden(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.ROWHIDDEN$50, n)).setEnumValue(enumValue);
        }
    }
    
    public void addRowHidden(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.ROWHIDDEN$50)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewRowHidden(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.ROWHIDDEN$50, n);
        }
    }
    
    public STTrueFalseBlank addNewRowHidden() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.ROWHIDDEN$50);
        }
    }
    
    public void removeRowHidden(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.ROWHIDDEN$50, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getColHiddenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.ColHiddenList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getColHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.COLHIDDEN$52, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getColHiddenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.COLHIDDEN$52, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetColHiddenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.ColHiddenList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetColHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.COLHIDDEN$52, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetColHiddenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.COLHIDDEN$52, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfColHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.COLHIDDEN$52);
        }
    }
    
    public void setColHiddenArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.COLHIDDEN$52);
        }
    }
    
    public void setColHiddenArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.COLHIDDEN$52, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetColHiddenArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.COLHIDDEN$52);
        }
    }
    
    public void xsetColHiddenArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.COLHIDDEN$52, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertColHidden(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.COLHIDDEN$52, n)).setEnumValue(enumValue);
        }
    }
    
    public void addColHidden(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.COLHIDDEN$52)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewColHidden(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.COLHIDDEN$52, n);
        }
    }
    
    public STTrueFalseBlank addNewColHidden() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.COLHIDDEN$52);
        }
    }
    
    public void removeColHidden(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.COLHIDDEN$52, n);
        }
    }
    
    public List<BigInteger> getVTEditList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.VTEditList(this);
        }
    }
    
    public BigInteger[] getVTEditArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.VTEDIT$54, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getVTEditArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VTEDIT$54, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetVTEditList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.VTEditList(this);
        }
    }
    
    public XmlInteger[] xgetVTEditArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.VTEDIT$54, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetVTEditArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.VTEDIT$54, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfVTEditArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.VTEDIT$54);
        }
    }
    
    public void setVTEditArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VTEDIT$54);
        }
    }
    
    public void setVTEditArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VTEDIT$54, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetVTEditArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VTEDIT$54);
        }
    }
    
    public void xsetVTEditArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.VTEDIT$54, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertVTEdit(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.VTEDIT$54, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addVTEdit(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.VTEDIT$54)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewVTEdit(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.VTEDIT$54, n);
        }
    }
    
    public XmlInteger addNewVTEdit() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.VTEDIT$54);
        }
    }
    
    public void removeVTEdit(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.VTEDIT$54, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getMultiLineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.MultiLineList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getMultiLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.MULTILINE$56, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getMultiLineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MULTILINE$56, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetMultiLineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.MultiLineList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetMultiLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.MULTILINE$56, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetMultiLineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.MULTILINE$56, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfMultiLineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.MULTILINE$56);
        }
    }
    
    public void setMultiLineArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MULTILINE$56);
        }
    }
    
    public void setMultiLineArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MULTILINE$56, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetMultiLineArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MULTILINE$56);
        }
    }
    
    public void xsetMultiLineArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.MULTILINE$56, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertMultiLine(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.MULTILINE$56, n)).setEnumValue(enumValue);
        }
    }
    
    public void addMultiLine(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.MULTILINE$56)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewMultiLine(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.MULTILINE$56, n);
        }
    }
    
    public STTrueFalseBlank addNewMultiLine() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.MULTILINE$56);
        }
    }
    
    public void removeMultiLine(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.MULTILINE$56, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getVScrollList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.VScrollList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getVScrollArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.VSCROLL$58, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getVScrollArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VSCROLL$58, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetVScrollList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.VScrollList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetVScrollArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.VSCROLL$58, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetVScrollArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.VSCROLL$58, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfVScrollArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.VSCROLL$58);
        }
    }
    
    public void setVScrollArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VSCROLL$58);
        }
    }
    
    public void setVScrollArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VSCROLL$58, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetVScrollArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VSCROLL$58);
        }
    }
    
    public void xsetVScrollArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.VSCROLL$58, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertVScroll(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.VSCROLL$58, n)).setEnumValue(enumValue);
        }
    }
    
    public void addVScroll(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.VSCROLL$58)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewVScroll(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.VSCROLL$58, n);
        }
    }
    
    public STTrueFalseBlank addNewVScroll() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.VSCROLL$58);
        }
    }
    
    public void removeVScroll(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.VSCROLL$58, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getValidIdsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.ValidIdsList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getValidIdsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.VALIDIDS$60, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getValidIdsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VALIDIDS$60, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetValidIdsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.ValidIdsList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetValidIdsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.VALIDIDS$60, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetValidIdsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.VALIDIDS$60, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfValidIdsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.VALIDIDS$60);
        }
    }
    
    public void setValidIdsArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VALIDIDS$60);
        }
    }
    
    public void setValidIdsArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VALIDIDS$60, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetValidIdsArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VALIDIDS$60);
        }
    }
    
    public void xsetValidIdsArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.VALIDIDS$60, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertValidIds(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.VALIDIDS$60, n)).setEnumValue(enumValue);
        }
    }
    
    public void addValidIds(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.VALIDIDS$60)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewValidIds(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.VALIDIDS$60, n);
        }
    }
    
    public STTrueFalseBlank addNewValidIds() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.VALIDIDS$60);
        }
    }
    
    public void removeValidIds(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.VALIDIDS$60, n);
        }
    }
    
    public List<String> getFmlaRangeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.FmlaRangeList(this);
        }
    }
    
    public String[] getFmlaRangeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLARANGE$62, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getFmlaRangeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLARANGE$62, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetFmlaRangeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.FmlaRangeList(this);
        }
    }
    
    public XmlString[] xgetFmlaRangeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLARANGE$62, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetFmlaRangeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLARANGE$62, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfFmlaRangeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.FMLARANGE$62);
        }
    }
    
    public void setFmlaRangeArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLARANGE$62);
        }
    }
    
    public void setFmlaRangeArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLARANGE$62, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetFmlaRangeArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLARANGE$62);
        }
    }
    
    public void xsetFmlaRangeArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLARANGE$62, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertFmlaRange(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.FMLARANGE$62, n)).setStringValue(stringValue);
        }
    }
    
    public void addFmlaRange(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.FMLARANGE$62)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewFmlaRange(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.FMLARANGE$62, n);
        }
    }
    
    public XmlString addNewFmlaRange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.FMLARANGE$62);
        }
    }
    
    public void removeFmlaRange(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.FMLARANGE$62, n);
        }
    }
    
    public List<BigInteger> getWidthMinList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.WidthMinList(this);
        }
    }
    
    public BigInteger[] getWidthMinArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.WIDTHMIN$64, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getWidthMinArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.WIDTHMIN$64, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetWidthMinList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.WidthMinList(this);
        }
    }
    
    public XmlInteger[] xgetWidthMinArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.WIDTHMIN$64, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetWidthMinArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.WIDTHMIN$64, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfWidthMinArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.WIDTHMIN$64);
        }
    }
    
    public void setWidthMinArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.WIDTHMIN$64);
        }
    }
    
    public void setWidthMinArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.WIDTHMIN$64, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetWidthMinArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.WIDTHMIN$64);
        }
    }
    
    public void xsetWidthMinArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.WIDTHMIN$64, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertWidthMin(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.WIDTHMIN$64, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addWidthMin(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.WIDTHMIN$64)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewWidthMin(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.WIDTHMIN$64, n);
        }
    }
    
    public XmlInteger addNewWidthMin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.WIDTHMIN$64);
        }
    }
    
    public void removeWidthMin(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.WIDTHMIN$64, n);
        }
    }
    
    public List<BigInteger> getSelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.SelList(this);
        }
    }
    
    public BigInteger[] getSelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SEL$66, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getSelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SEL$66, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetSelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.SelList(this);
        }
    }
    
    public XmlInteger[] xgetSelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SEL$66, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetSelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.SEL$66, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfSelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SEL$66);
        }
    }
    
    public void setSelArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SEL$66);
        }
    }
    
    public void setSelArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SEL$66, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetSelArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SEL$66);
        }
    }
    
    public void xsetSelArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.SEL$66, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertSel(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SEL$66, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addSel(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SEL$66)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewSel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.SEL$66, n);
        }
    }
    
    public XmlInteger addNewSel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.SEL$66);
        }
    }
    
    public void removeSel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SEL$66, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getNoThreeD2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.NoThreeD2List(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getNoThreeD2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.NOTHREED2$68, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getNoThreeD2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.NOTHREED2$68, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetNoThreeD2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.NoThreeD2List(this);
        }
    }
    
    public STTrueFalseBlank[] xgetNoThreeD2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.NOTHREED2$68, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetNoThreeD2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.NOTHREED2$68, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfNoThreeD2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.NOTHREED2$68);
        }
    }
    
    public void setNoThreeD2Array(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.NOTHREED2$68);
        }
    }
    
    public void setNoThreeD2Array(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.NOTHREED2$68, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetNoThreeD2Array(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.NOTHREED2$68);
        }
    }
    
    public void xsetNoThreeD2Array(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.NOTHREED2$68, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertNoThreeD2(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.NOTHREED2$68, n)).setEnumValue(enumValue);
        }
    }
    
    public void addNoThreeD2(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.NOTHREED2$68)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewNoThreeD2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.NOTHREED2$68, n);
        }
    }
    
    public STTrueFalseBlank addNewNoThreeD2() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.NOTHREED2$68);
        }
    }
    
    public void removeNoThreeD2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.NOTHREED2$68, n);
        }
    }
    
    public List<String> getSelTypeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.SelTypeList(this);
        }
    }
    
    public String[] getSelTypeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SELTYPE$70, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getSelTypeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SELTYPE$70, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetSelTypeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.SelTypeList(this);
        }
    }
    
    public XmlString[] xgetSelTypeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SELTYPE$70, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetSelTypeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.SELTYPE$70, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfSelTypeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SELTYPE$70);
        }
    }
    
    public void setSelTypeArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SELTYPE$70);
        }
    }
    
    public void setSelTypeArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SELTYPE$70, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetSelTypeArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SELTYPE$70);
        }
    }
    
    public void xsetSelTypeArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.SELTYPE$70, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertSelType(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SELTYPE$70, n)).setStringValue(stringValue);
        }
    }
    
    public void addSelType(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SELTYPE$70)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewSelType(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.SELTYPE$70, n);
        }
    }
    
    public XmlString addNewSelType() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.SELTYPE$70);
        }
    }
    
    public void removeSelType(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SELTYPE$70, n);
        }
    }
    
    public List<String> getMultiSelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.MultiSelList(this);
        }
    }
    
    public String[] getMultiSelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.MULTISEL$72, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getMultiSelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MULTISEL$72, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetMultiSelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.MultiSelList(this);
        }
    }
    
    public XmlString[] xgetMultiSelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.MULTISEL$72, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetMultiSelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.MULTISEL$72, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfMultiSelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.MULTISEL$72);
        }
    }
    
    public void setMultiSelArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MULTISEL$72);
        }
    }
    
    public void setMultiSelArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MULTISEL$72, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetMultiSelArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MULTISEL$72);
        }
    }
    
    public void xsetMultiSelArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.MULTISEL$72, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertMultiSel(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.MULTISEL$72, n)).setStringValue(stringValue);
        }
    }
    
    public void addMultiSel(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.MULTISEL$72)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewMultiSel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.MULTISEL$72, n);
        }
    }
    
    public XmlString addNewMultiSel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.MULTISEL$72);
        }
    }
    
    public void removeMultiSel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.MULTISEL$72, n);
        }
    }
    
    public List<String> getLCTList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.LCTList(this);
        }
    }
    
    public String[] getLCTArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.LCT$74, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getLCTArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LCT$74, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetLCTList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.LCTList(this);
        }
    }
    
    public XmlString[] xgetLCTArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.LCT$74, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetLCTArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.LCT$74, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfLCTArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.LCT$74);
        }
    }
    
    public void setLCTArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LCT$74);
        }
    }
    
    public void setLCTArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LCT$74, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetLCTArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LCT$74);
        }
    }
    
    public void xsetLCTArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.LCT$74, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertLCT(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.LCT$74, n)).setStringValue(stringValue);
        }
    }
    
    public void addLCT(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.LCT$74)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewLCT(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.LCT$74, n);
        }
    }
    
    public XmlString addNewLCT() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.LCT$74);
        }
    }
    
    public void removeLCT(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.LCT$74, n);
        }
    }
    
    public List<String> getListItemList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.ListItemList(this);
        }
    }
    
    public String[] getListItemArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.LISTITEM$76, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getListItemArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LISTITEM$76, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetListItemList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.ListItemList(this);
        }
    }
    
    public XmlString[] xgetListItemArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.LISTITEM$76, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetListItemArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.LISTITEM$76, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfListItemArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.LISTITEM$76);
        }
    }
    
    public void setListItemArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LISTITEM$76);
        }
    }
    
    public void setListItemArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.LISTITEM$76, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetListItemArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.LISTITEM$76);
        }
    }
    
    public void xsetListItemArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.LISTITEM$76, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertListItem(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.LISTITEM$76, n)).setStringValue(stringValue);
        }
    }
    
    public void addListItem(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.LISTITEM$76)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewListItem(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.LISTITEM$76, n);
        }
    }
    
    public XmlString addNewListItem() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.LISTITEM$76);
        }
    }
    
    public void removeListItem(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.LISTITEM$76, n);
        }
    }
    
    public List<String> getDropStyleList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.DropStyleList(this);
        }
    }
    
    public String[] getDropStyleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DROPSTYLE$78, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getDropStyleArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DROPSTYLE$78, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetDropStyleList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.DropStyleList(this);
        }
    }
    
    public XmlString[] xgetDropStyleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DROPSTYLE$78, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetDropStyleArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.DROPSTYLE$78, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfDropStyleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DROPSTYLE$78);
        }
    }
    
    public void setDropStyleArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DROPSTYLE$78);
        }
    }
    
    public void setDropStyleArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DROPSTYLE$78, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetDropStyleArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DROPSTYLE$78);
        }
    }
    
    public void xsetDropStyleArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.DROPSTYLE$78, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertDropStyle(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DROPSTYLE$78, n)).setStringValue(stringValue);
        }
    }
    
    public void addDropStyle(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DROPSTYLE$78)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewDropStyle(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.DROPSTYLE$78, n);
        }
    }
    
    public XmlString addNewDropStyle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.DROPSTYLE$78);
        }
    }
    
    public void removeDropStyle(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DROPSTYLE$78, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getColoredList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.ColoredList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getColoredArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.COLORED$80, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getColoredArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.COLORED$80, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetColoredList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.ColoredList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetColoredArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.COLORED$80, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetColoredArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.COLORED$80, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfColoredArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.COLORED$80);
        }
    }
    
    public void setColoredArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.COLORED$80);
        }
    }
    
    public void setColoredArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.COLORED$80, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetColoredArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.COLORED$80);
        }
    }
    
    public void xsetColoredArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.COLORED$80, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertColored(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.COLORED$80, n)).setEnumValue(enumValue);
        }
    }
    
    public void addColored(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.COLORED$80)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewColored(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.COLORED$80, n);
        }
    }
    
    public STTrueFalseBlank addNewColored() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.COLORED$80);
        }
    }
    
    public void removeColored(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.COLORED$80, n);
        }
    }
    
    public List<BigInteger> getDropLinesList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.DropLinesList(this);
        }
    }
    
    public BigInteger[] getDropLinesArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DROPLINES$82, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getDropLinesArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DROPLINES$82, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetDropLinesList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.DropLinesList(this);
        }
    }
    
    public XmlInteger[] xgetDropLinesArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DROPLINES$82, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetDropLinesArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.DROPLINES$82, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfDropLinesArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DROPLINES$82);
        }
    }
    
    public void setDropLinesArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DROPLINES$82);
        }
    }
    
    public void setDropLinesArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DROPLINES$82, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetDropLinesArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DROPLINES$82);
        }
    }
    
    public void xsetDropLinesArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.DROPLINES$82, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertDropLines(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DROPLINES$82, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addDropLines(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DROPLINES$82)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewDropLines(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.DROPLINES$82, n);
        }
    }
    
    public XmlInteger addNewDropLines() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.DROPLINES$82);
        }
    }
    
    public void removeDropLines(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DROPLINES$82, n);
        }
    }
    
    public List<BigInteger> getCheckedList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.CheckedList(this);
        }
    }
    
    public BigInteger[] getCheckedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.CHECKED$84, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getCheckedArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CHECKED$84, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetCheckedList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.CheckedList(this);
        }
    }
    
    public XmlInteger[] xgetCheckedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.CHECKED$84, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetCheckedArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.CHECKED$84, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfCheckedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.CHECKED$84);
        }
    }
    
    public void setCheckedArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.CHECKED$84);
        }
    }
    
    public void setCheckedArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CHECKED$84, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetCheckedArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.CHECKED$84);
        }
    }
    
    public void xsetCheckedArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.CHECKED$84, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertChecked(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.CHECKED$84, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addChecked(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.CHECKED$84)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewChecked(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.CHECKED$84, n);
        }
    }
    
    public XmlInteger addNewChecked() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.CHECKED$84);
        }
    }
    
    public void removeChecked(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.CHECKED$84, n);
        }
    }
    
    public List<String> getFmlaLinkList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.FmlaLinkList(this);
        }
    }
    
    public String[] getFmlaLinkArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLALINK$86, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getFmlaLinkArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLALINK$86, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetFmlaLinkList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.FmlaLinkList(this);
        }
    }
    
    public XmlString[] xgetFmlaLinkArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLALINK$86, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetFmlaLinkArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLALINK$86, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfFmlaLinkArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.FMLALINK$86);
        }
    }
    
    public void setFmlaLinkArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLALINK$86);
        }
    }
    
    public void setFmlaLinkArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLALINK$86, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetFmlaLinkArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLALINK$86);
        }
    }
    
    public void xsetFmlaLinkArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLALINK$86, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertFmlaLink(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.FMLALINK$86, n)).setStringValue(stringValue);
        }
    }
    
    public void addFmlaLink(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.FMLALINK$86)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewFmlaLink(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.FMLALINK$86, n);
        }
    }
    
    public XmlString addNewFmlaLink() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.FMLALINK$86);
        }
    }
    
    public void removeFmlaLink(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.FMLALINK$86, n);
        }
    }
    
    public List<String> getFmlaPictList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.FmlaPictList(this);
        }
    }
    
    public String[] getFmlaPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLAPICT$88, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getFmlaPictArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLAPICT$88, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetFmlaPictList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.FmlaPictList(this);
        }
    }
    
    public XmlString[] xgetFmlaPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLAPICT$88, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetFmlaPictArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLAPICT$88, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfFmlaPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.FMLAPICT$88);
        }
    }
    
    public void setFmlaPictArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLAPICT$88);
        }
    }
    
    public void setFmlaPictArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLAPICT$88, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetFmlaPictArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLAPICT$88);
        }
    }
    
    public void xsetFmlaPictArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLAPICT$88, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertFmlaPict(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.FMLAPICT$88, n)).setStringValue(stringValue);
        }
    }
    
    public void addFmlaPict(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.FMLAPICT$88)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewFmlaPict(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.FMLAPICT$88, n);
        }
    }
    
    public XmlString addNewFmlaPict() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.FMLAPICT$88);
        }
    }
    
    public void removeFmlaPict(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.FMLAPICT$88, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getNoThreeDList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.NoThreeDList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getNoThreeDArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.NOTHREED$90, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getNoThreeDArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.NOTHREED$90, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetNoThreeDList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.NoThreeDList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetNoThreeDArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.NOTHREED$90, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetNoThreeDArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.NOTHREED$90, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfNoThreeDArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.NOTHREED$90);
        }
    }
    
    public void setNoThreeDArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.NOTHREED$90);
        }
    }
    
    public void setNoThreeDArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.NOTHREED$90, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetNoThreeDArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.NOTHREED$90);
        }
    }
    
    public void xsetNoThreeDArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.NOTHREED$90, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertNoThreeD(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.NOTHREED$90, n)).setEnumValue(enumValue);
        }
    }
    
    public void addNoThreeD(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.NOTHREED$90)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewNoThreeD(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.NOTHREED$90, n);
        }
    }
    
    public STTrueFalseBlank addNewNoThreeD() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.NOTHREED$90);
        }
    }
    
    public void removeNoThreeD(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.NOTHREED$90, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getFirstButtonList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.FirstButtonList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getFirstButtonArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.FIRSTBUTTON$92, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getFirstButtonArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FIRSTBUTTON$92, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetFirstButtonList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.FirstButtonList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetFirstButtonArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.FIRSTBUTTON$92, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetFirstButtonArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.FIRSTBUTTON$92, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfFirstButtonArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.FIRSTBUTTON$92);
        }
    }
    
    public void setFirstButtonArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FIRSTBUTTON$92);
        }
    }
    
    public void setFirstButtonArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FIRSTBUTTON$92, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetFirstButtonArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FIRSTBUTTON$92);
        }
    }
    
    public void xsetFirstButtonArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.FIRSTBUTTON$92, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertFirstButton(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.FIRSTBUTTON$92, n)).setEnumValue(enumValue);
        }
    }
    
    public void addFirstButton(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.FIRSTBUTTON$92)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewFirstButton(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.FIRSTBUTTON$92, n);
        }
    }
    
    public STTrueFalseBlank addNewFirstButton() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.FIRSTBUTTON$92);
        }
    }
    
    public void removeFirstButton(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.FIRSTBUTTON$92, n);
        }
    }
    
    public List<String> getFmlaGroupList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.FmlaGroupList(this);
        }
    }
    
    public String[] getFmlaGroupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLAGROUP$94, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getFmlaGroupArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLAGROUP$94, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetFmlaGroupList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.FmlaGroupList(this);
        }
    }
    
    public XmlString[] xgetFmlaGroupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLAGROUP$94, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetFmlaGroupArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLAGROUP$94, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfFmlaGroupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.FMLAGROUP$94);
        }
    }
    
    public void setFmlaGroupArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLAGROUP$94);
        }
    }
    
    public void setFmlaGroupArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLAGROUP$94, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetFmlaGroupArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLAGROUP$94);
        }
    }
    
    public void xsetFmlaGroupArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLAGROUP$94, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertFmlaGroup(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.FMLAGROUP$94, n)).setStringValue(stringValue);
        }
    }
    
    public void addFmlaGroup(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.FMLAGROUP$94)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewFmlaGroup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.FMLAGROUP$94, n);
        }
    }
    
    public XmlString addNewFmlaGroup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.FMLAGROUP$94);
        }
    }
    
    public void removeFmlaGroup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.FMLAGROUP$94, n);
        }
    }
    
    public List<BigInteger> getValList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.ValList(this);
        }
    }
    
    public BigInteger[] getValArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.VAL$96, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getValArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VAL$96, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetValList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.ValList(this);
        }
    }
    
    public XmlInteger[] xgetValArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.VAL$96, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetValArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.VAL$96, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfValArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.VAL$96);
        }
    }
    
    public void setValArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VAL$96);
        }
    }
    
    public void setValArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.VAL$96, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetValArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.VAL$96);
        }
    }
    
    public void xsetValArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.VAL$96, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertVal(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.VAL$96, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addVal(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.VAL$96)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewVal(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.VAL$96, n);
        }
    }
    
    public XmlInteger addNewVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.VAL$96);
        }
    }
    
    public void removeVal(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.VAL$96, n);
        }
    }
    
    public List<BigInteger> getMinList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.MinList(this);
        }
    }
    
    public BigInteger[] getMinArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.MIN$98, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getMinArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MIN$98, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetMinList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.MinList(this);
        }
    }
    
    public XmlInteger[] xgetMinArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.MIN$98, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetMinArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.MIN$98, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfMinArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.MIN$98);
        }
    }
    
    public void setMinArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MIN$98);
        }
    }
    
    public void setMinArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MIN$98, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetMinArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MIN$98);
        }
    }
    
    public void xsetMinArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.MIN$98, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertMin(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.MIN$98, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addMin(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.MIN$98)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewMin(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.MIN$98, n);
        }
    }
    
    public XmlInteger addNewMin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.MIN$98);
        }
    }
    
    public void removeMin(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.MIN$98, n);
        }
    }
    
    public List<BigInteger> getMaxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.MaxList(this);
        }
    }
    
    public BigInteger[] getMaxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.MAX$100, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getMaxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MAX$100, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetMaxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.MaxList(this);
        }
    }
    
    public XmlInteger[] xgetMaxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.MAX$100, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetMaxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.MAX$100, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfMaxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.MAX$100);
        }
    }
    
    public void setMaxArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MAX$100);
        }
    }
    
    public void setMaxArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MAX$100, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetMaxArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MAX$100);
        }
    }
    
    public void xsetMaxArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.MAX$100, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertMax(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.MAX$100, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addMax(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.MAX$100)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewMax(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.MAX$100, n);
        }
    }
    
    public XmlInteger addNewMax() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.MAX$100);
        }
    }
    
    public void removeMax(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.MAX$100, n);
        }
    }
    
    public List<BigInteger> getIncList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.IncList(this);
        }
    }
    
    public BigInteger[] getIncArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.INC$102, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getIncArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.INC$102, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetIncList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.IncList(this);
        }
    }
    
    public XmlInteger[] xgetIncArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.INC$102, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetIncArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.INC$102, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfIncArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.INC$102);
        }
    }
    
    public void setIncArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.INC$102);
        }
    }
    
    public void setIncArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.INC$102, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetIncArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.INC$102);
        }
    }
    
    public void xsetIncArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.INC$102, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertInc(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.INC$102, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addInc(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.INC$102)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewInc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.INC$102, n);
        }
    }
    
    public XmlInteger addNewInc() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.INC$102);
        }
    }
    
    public void removeInc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.INC$102, n);
        }
    }
    
    public List<BigInteger> getPageList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.PageList(this);
        }
    }
    
    public BigInteger[] getPageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.PAGE$104, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getPageArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.PAGE$104, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetPageList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.PageList(this);
        }
    }
    
    public XmlInteger[] xgetPageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.PAGE$104, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetPageArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.PAGE$104, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfPageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.PAGE$104);
        }
    }
    
    public void setPageArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.PAGE$104);
        }
    }
    
    public void setPageArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.PAGE$104, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetPageArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.PAGE$104);
        }
    }
    
    public void xsetPageArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.PAGE$104, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertPage(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.PAGE$104, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addPage(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.PAGE$104)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewPage(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.PAGE$104, n);
        }
    }
    
    public XmlInteger addNewPage() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.PAGE$104);
        }
    }
    
    public void removePage(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.PAGE$104, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getHorizList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.HorizList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getHorizArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.HORIZ$106, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getHorizArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.HORIZ$106, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetHorizList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.HorizList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetHorizArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.HORIZ$106, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetHorizArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.HORIZ$106, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfHorizArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.HORIZ$106);
        }
    }
    
    public void setHorizArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.HORIZ$106);
        }
    }
    
    public void setHorizArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.HORIZ$106, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetHorizArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.HORIZ$106);
        }
    }
    
    public void xsetHorizArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.HORIZ$106, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertHoriz(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.HORIZ$106, n)).setEnumValue(enumValue);
        }
    }
    
    public void addHoriz(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.HORIZ$106)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewHoriz(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.HORIZ$106, n);
        }
    }
    
    public STTrueFalseBlank addNewHoriz() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.HORIZ$106);
        }
    }
    
    public void removeHoriz(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.HORIZ$106, n);
        }
    }
    
    public List<BigInteger> getDxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.DxList(this);
        }
    }
    
    public BigInteger[] getDxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DX$108, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getDxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DX$108, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlInteger> xgetDxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInteger>)new CTClientDataImpl.DxList(this);
        }
    }
    
    public XmlInteger[] xgetDxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DX$108, list);
            final XmlInteger[] array = new XmlInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInteger xgetDxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.DX$108, n);
            if (xmlInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInteger;
        }
    }
    
    public int sizeOfDxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DX$108);
        }
    }
    
    public void setDxArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DX$108);
        }
    }
    
    public void setDxArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DX$108, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetDxArray(final XmlInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DX$108);
        }
    }
    
    public void xsetDxArray(final int n, final XmlInteger xmlInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInteger xmlInteger2 = (XmlInteger)this.get_store().find_element_user(CTClientDataImpl.DX$108, n);
            if (xmlInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInteger2.set(xmlInteger);
        }
    }
    
    public void insertDx(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DX$108, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addDx(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DX$108)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlInteger insertNewDx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().insert_element_user(CTClientDataImpl.DX$108, n);
        }
    }
    
    public XmlInteger addNewDx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInteger)this.get_store().add_element_user(CTClientDataImpl.DX$108);
        }
    }
    
    public void removeDx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DX$108, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getMapOCXList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.MapOCXList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getMapOCXArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.MAPOCX$110, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getMapOCXArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MAPOCX$110, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetMapOCXList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.MapOCXList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetMapOCXArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.MAPOCX$110, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetMapOCXArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.MAPOCX$110, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfMapOCXArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.MAPOCX$110);
        }
    }
    
    public void setMapOCXArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MAPOCX$110);
        }
    }
    
    public void setMapOCXArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.MAPOCX$110, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetMapOCXArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.MAPOCX$110);
        }
    }
    
    public void xsetMapOCXArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.MAPOCX$110, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertMapOCX(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.MAPOCX$110, n)).setEnumValue(enumValue);
        }
    }
    
    public void addMapOCX(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.MAPOCX$110)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewMapOCX(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.MAPOCX$110, n);
        }
    }
    
    public STTrueFalseBlank addNewMapOCX() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.MAPOCX$110);
        }
    }
    
    public void removeMapOCX(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.MAPOCX$110, n);
        }
    }
    
    public List<STCF.Enum> getCFList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STCF.Enum>)new CTClientDataImpl.CFList(this);
        }
    }
    
    public STCF.Enum[] getCFArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.CF$112, list);
            final STCF.Enum[] array = new STCF.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STCF.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STCF.Enum getCFArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CF$112, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STCF.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STCF> xgetCFList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STCF>)new CTClientDataImpl.CFList(this);
        }
    }
    
    public STCF[] xgetCFArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.CF$112, list);
            final STCF[] array = new STCF[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STCF xgetCFArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STCF stcf = (STCF)this.get_store().find_element_user(CTClientDataImpl.CF$112, n);
            if (stcf == null) {
                throw new IndexOutOfBoundsException();
            }
            return stcf;
        }
    }
    
    public int sizeOfCFArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.CF$112);
        }
    }
    
    public void setCFArray(final STCF.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((StringEnumAbstractBase[])sources, CTClientDataImpl.CF$112);
        }
    }
    
    public void setCFArray(final int n, final STCF.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CF$112, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetCFArray(final STCF[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTClientDataImpl.CF$112);
        }
    }
    
    public void xsetCFArray(final int n, final STCF stcf) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STCF stcf2 = (STCF)this.get_store().find_element_user(CTClientDataImpl.CF$112, n);
            if (stcf2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stcf2.set((XmlObject)stcf);
        }
    }
    
    public void insertCF(final int n, final STCF.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.CF$112, n)).setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void addCF(final STCF.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.CF$112)).setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public STCF insertNewCF(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STCF)this.get_store().insert_element_user(CTClientDataImpl.CF$112, n);
        }
    }
    
    public STCF addNewCF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STCF)this.get_store().add_element_user(CTClientDataImpl.CF$112);
        }
    }
    
    public void removeCF(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.CF$112, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getCameraList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.CameraList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getCameraArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.CAMERA$114, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getCameraArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CAMERA$114, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetCameraList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.CameraList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetCameraArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.CAMERA$114, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetCameraArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.CAMERA$114, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfCameraArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.CAMERA$114);
        }
    }
    
    public void setCameraArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.CAMERA$114);
        }
    }
    
    public void setCameraArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.CAMERA$114, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetCameraArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.CAMERA$114);
        }
    }
    
    public void xsetCameraArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.CAMERA$114, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertCamera(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.CAMERA$114, n)).setEnumValue(enumValue);
        }
    }
    
    public void addCamera(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.CAMERA$114)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewCamera(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.CAMERA$114, n);
        }
    }
    
    public STTrueFalseBlank addNewCamera() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.CAMERA$114);
        }
    }
    
    public void removeCamera(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.CAMERA$114, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getRecalcAlwaysList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.RecalcAlwaysList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getRecalcAlwaysArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.RECALCALWAYS$116, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getRecalcAlwaysArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.RECALCALWAYS$116, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetRecalcAlwaysList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.RecalcAlwaysList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetRecalcAlwaysArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.RECALCALWAYS$116, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetRecalcAlwaysArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.RECALCALWAYS$116, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfRecalcAlwaysArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.RECALCALWAYS$116);
        }
    }
    
    public void setRecalcAlwaysArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.RECALCALWAYS$116);
        }
    }
    
    public void setRecalcAlwaysArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.RECALCALWAYS$116, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetRecalcAlwaysArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.RECALCALWAYS$116);
        }
    }
    
    public void xsetRecalcAlwaysArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.RECALCALWAYS$116, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertRecalcAlways(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.RECALCALWAYS$116, n)).setEnumValue(enumValue);
        }
    }
    
    public void addRecalcAlways(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.RECALCALWAYS$116)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewRecalcAlways(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.RECALCALWAYS$116, n);
        }
    }
    
    public STTrueFalseBlank addNewRecalcAlways() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.RECALCALWAYS$116);
        }
    }
    
    public void removeRecalcAlways(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.RECALCALWAYS$116, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getAutoScaleList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.AutoScaleList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getAutoScaleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOSCALE$118, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getAutoScaleArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOSCALE$118, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetAutoScaleList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.AutoScaleList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetAutoScaleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.AUTOSCALE$118, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetAutoScaleArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOSCALE$118, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfAutoScaleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.AUTOSCALE$118);
        }
    }
    
    public void setAutoScaleArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOSCALE$118);
        }
    }
    
    public void setAutoScaleArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.AUTOSCALE$118, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetAutoScaleArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.AUTOSCALE$118);
        }
    }
    
    public void xsetAutoScaleArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.AUTOSCALE$118, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertAutoScale(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.AUTOSCALE$118, n)).setEnumValue(enumValue);
        }
    }
    
    public void addAutoScale(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.AUTOSCALE$118)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewAutoScale(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.AUTOSCALE$118, n);
        }
    }
    
    public STTrueFalseBlank addNewAutoScale() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.AUTOSCALE$118);
        }
    }
    
    public void removeAutoScale(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.AUTOSCALE$118, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getDDEList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.DDEList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getDDEArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.DDE$120, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getDDEArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DDE$120, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetDDEList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.DDEList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetDDEArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.DDE$120, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetDDEArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DDE$120, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfDDEArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.DDE$120);
        }
    }
    
    public void setDDEArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DDE$120);
        }
    }
    
    public void setDDEArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.DDE$120, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetDDEArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.DDE$120);
        }
    }
    
    public void xsetDDEArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.DDE$120, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertDDE(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.DDE$120, n)).setEnumValue(enumValue);
        }
    }
    
    public void addDDE(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.DDE$120)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewDDE(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.DDE$120, n);
        }
    }
    
    public STTrueFalseBlank addNewDDE() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.DDE$120);
        }
    }
    
    public void removeDDE(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.DDE$120, n);
        }
    }
    
    public List<STTrueFalseBlank.Enum> getUIObjList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank.Enum>)new CTClientDataImpl.UIObjList(this);
        }
    }
    
    public STTrueFalseBlank.Enum[] getUIObjArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.UIOBJ$122, list);
            final STTrueFalseBlank.Enum[] array = new STTrueFalseBlank.Enum[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = (STTrueFalseBlank.Enum)((SimpleValue)list.get(i)).getEnumValue();
            }
            return array;
        }
    }
    
    public STTrueFalseBlank.Enum getUIObjArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.UIOBJ$122, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return (STTrueFalseBlank.Enum)simpleValue.getEnumValue();
        }
    }
    
    public List<STTrueFalseBlank> xgetUIObjList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STTrueFalseBlank>)new CTClientDataImpl.UIObjList(this);
        }
    }
    
    public STTrueFalseBlank[] xgetUIObjArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.UIOBJ$122, list);
            final STTrueFalseBlank[] array = new STTrueFalseBlank[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STTrueFalseBlank xgetUIObjArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.UIOBJ$122, n);
            if (stTrueFalseBlank == null) {
                throw new IndexOutOfBoundsException();
            }
            return stTrueFalseBlank;
        }
    }
    
    public int sizeOfUIObjArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.UIOBJ$122);
        }
    }
    
    public void setUIObjArray(final STTrueFalseBlank.Enum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.UIOBJ$122);
        }
    }
    
    public void setUIObjArray(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.UIOBJ$122, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetUIObjArray(final STTrueFalseBlank[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.UIOBJ$122);
        }
    }
    
    public void xsetUIObjArray(final int n, final STTrueFalseBlank stTrueFalseBlank) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STTrueFalseBlank stTrueFalseBlank2 = (STTrueFalseBlank)this.get_store().find_element_user(CTClientDataImpl.UIOBJ$122, n);
            if (stTrueFalseBlank2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stTrueFalseBlank2.set(stTrueFalseBlank);
        }
    }
    
    public void insertUIObj(final int n, final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.UIOBJ$122, n)).setEnumValue(enumValue);
        }
    }
    
    public void addUIObj(final STTrueFalseBlank.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.UIOBJ$122)).setEnumValue(enumValue);
        }
    }
    
    public STTrueFalseBlank insertNewUIObj(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().insert_element_user(CTClientDataImpl.UIOBJ$122, n);
        }
    }
    
    public STTrueFalseBlank addNewUIObj() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STTrueFalseBlank)this.get_store().add_element_user(CTClientDataImpl.UIOBJ$122);
        }
    }
    
    public void removeUIObj(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.UIOBJ$122, n);
        }
    }
    
    public List<String> getScriptTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.ScriptTextList(this);
        }
    }
    
    public String[] getScriptTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTTEXT$124, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getScriptTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTTEXT$124, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetScriptTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.ScriptTextList(this);
        }
    }
    
    public XmlString[] xgetScriptTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTTEXT$124, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetScriptTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.SCRIPTTEXT$124, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfScriptTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SCRIPTTEXT$124);
        }
    }
    
    public void setScriptTextArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTTEXT$124);
        }
    }
    
    public void setScriptTextArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTTEXT$124, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetScriptTextArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTTEXT$124);
        }
    }
    
    public void xsetScriptTextArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.SCRIPTTEXT$124, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertScriptText(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTTEXT$124, n)).setStringValue(stringValue);
        }
    }
    
    public void addScriptText(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SCRIPTTEXT$124)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewScriptText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTTEXT$124, n);
        }
    }
    
    public XmlString addNewScriptText() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.SCRIPTTEXT$124);
        }
    }
    
    public void removeScriptText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SCRIPTTEXT$124, n);
        }
    }
    
    public List<String> getScriptExtendedList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.ScriptExtendedList(this);
        }
    }
    
    public String[] getScriptExtendedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTEXTENDED$126, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getScriptExtendedArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTEXTENDED$126, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetScriptExtendedList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.ScriptExtendedList(this);
        }
    }
    
    public XmlString[] xgetScriptExtendedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTEXTENDED$126, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetScriptExtendedArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.SCRIPTEXTENDED$126, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfScriptExtendedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SCRIPTEXTENDED$126);
        }
    }
    
    public void setScriptExtendedArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTEXTENDED$126);
        }
    }
    
    public void setScriptExtendedArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTEXTENDED$126, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetScriptExtendedArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTEXTENDED$126);
        }
    }
    
    public void xsetScriptExtendedArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.SCRIPTEXTENDED$126, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertScriptExtended(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTEXTENDED$126, n)).setStringValue(stringValue);
        }
    }
    
    public void addScriptExtended(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SCRIPTEXTENDED$126)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewScriptExtended(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTEXTENDED$126, n);
        }
    }
    
    public XmlString addNewScriptExtended() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.SCRIPTEXTENDED$126);
        }
    }
    
    public void removeScriptExtended(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SCRIPTEXTENDED$126, n);
        }
    }
    
    public List<BigInteger> getScriptLanguageList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.ScriptLanguageList(this);
        }
    }
    
    public BigInteger[] getScriptLanguageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTLANGUAGE$128, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getScriptLanguageArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlNonNegativeInteger> xgetScriptLanguageList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlNonNegativeInteger>)new CTClientDataImpl.ScriptLanguageList(this);
        }
    }
    
    public XmlNonNegativeInteger[] xgetScriptLanguageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTLANGUAGE$128, list);
            final XmlNonNegativeInteger[] array = new XmlNonNegativeInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlNonNegativeInteger xgetScriptLanguageArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlNonNegativeInteger xmlNonNegativeInteger = (XmlNonNegativeInteger)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128, n);
            if (xmlNonNegativeInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlNonNegativeInteger;
        }
    }
    
    public int sizeOfScriptLanguageArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SCRIPTLANGUAGE$128);
        }
    }
    
    public void setScriptLanguageArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTLANGUAGE$128);
        }
    }
    
    public void setScriptLanguageArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetScriptLanguageArray(final XmlNonNegativeInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTLANGUAGE$128);
        }
    }
    
    public void xsetScriptLanguageArray(final int n, final XmlNonNegativeInteger xmlNonNegativeInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlNonNegativeInteger xmlNonNegativeInteger2 = (XmlNonNegativeInteger)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128, n);
            if (xmlNonNegativeInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlNonNegativeInteger2.set(xmlNonNegativeInteger);
        }
    }
    
    public void insertScriptLanguage(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addScriptLanguage(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlNonNegativeInteger insertNewScriptLanguage(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlNonNegativeInteger)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128, n);
        }
    }
    
    public XmlNonNegativeInteger addNewScriptLanguage() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlNonNegativeInteger)this.get_store().add_element_user(CTClientDataImpl.SCRIPTLANGUAGE$128);
        }
    }
    
    public void removeScriptLanguage(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SCRIPTLANGUAGE$128, n);
        }
    }
    
    public List<BigInteger> getScriptLocationList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTClientDataImpl.ScriptLocationList(this);
        }
    }
    
    public BigInteger[] getScriptLocationArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTLOCATION$130, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getScriptLocationArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLOCATION$130, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlNonNegativeInteger> xgetScriptLocationList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlNonNegativeInteger>)new CTClientDataImpl.ScriptLocationList(this);
        }
    }
    
    public XmlNonNegativeInteger[] xgetScriptLocationArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.SCRIPTLOCATION$130, list);
            final XmlNonNegativeInteger[] array = new XmlNonNegativeInteger[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlNonNegativeInteger xgetScriptLocationArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlNonNegativeInteger xmlNonNegativeInteger = (XmlNonNegativeInteger)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLOCATION$130, n);
            if (xmlNonNegativeInteger == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlNonNegativeInteger;
        }
    }
    
    public int sizeOfScriptLocationArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.SCRIPTLOCATION$130);
        }
    }
    
    public void setScriptLocationArray(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTLOCATION$130);
        }
    }
    
    public void setScriptLocationArray(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLOCATION$130, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetScriptLocationArray(final XmlNonNegativeInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.SCRIPTLOCATION$130);
        }
    }
    
    public void xsetScriptLocationArray(final int n, final XmlNonNegativeInteger xmlNonNegativeInteger) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlNonNegativeInteger xmlNonNegativeInteger2 = (XmlNonNegativeInteger)this.get_store().find_element_user(CTClientDataImpl.SCRIPTLOCATION$130, n);
            if (xmlNonNegativeInteger2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlNonNegativeInteger2.set(xmlNonNegativeInteger);
        }
    }
    
    public void insertScriptLocation(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTLOCATION$130, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addScriptLocation(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.SCRIPTLOCATION$130)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlNonNegativeInteger insertNewScriptLocation(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlNonNegativeInteger)this.get_store().insert_element_user(CTClientDataImpl.SCRIPTLOCATION$130, n);
        }
    }
    
    public XmlNonNegativeInteger addNewScriptLocation() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlNonNegativeInteger)this.get_store().add_element_user(CTClientDataImpl.SCRIPTLOCATION$130);
        }
    }
    
    public void removeScriptLocation(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.SCRIPTLOCATION$130, n);
        }
    }
    
    public List<String> getFmlaTxbxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTClientDataImpl.FmlaTxbxList(this);
        }
    }
    
    public String[] getFmlaTxbxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLATXBX$132, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getFmlaTxbxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLATXBX$132, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetFmlaTxbxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTClientDataImpl.FmlaTxbxList(this);
        }
    }
    
    public XmlString[] xgetFmlaTxbxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTClientDataImpl.FMLATXBX$132, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetFmlaTxbxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLATXBX$132, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfFmlaTxbxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTClientDataImpl.FMLATXBX$132);
        }
    }
    
    public void setFmlaTxbxArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLATXBX$132);
        }
    }
    
    public void setFmlaTxbxArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTClientDataImpl.FMLATXBX$132, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetFmlaTxbxArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTClientDataImpl.FMLATXBX$132);
        }
    }
    
    public void xsetFmlaTxbxArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTClientDataImpl.FMLATXBX$132, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertFmlaTxbx(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTClientDataImpl.FMLATXBX$132, n)).setStringValue(stringValue);
        }
    }
    
    public void addFmlaTxbx(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTClientDataImpl.FMLATXBX$132)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewFmlaTxbx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTClientDataImpl.FMLATXBX$132, n);
        }
    }
    
    public XmlString addNewFmlaTxbx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTClientDataImpl.FMLATXBX$132);
        }
    }
    
    public void removeFmlaTxbx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTClientDataImpl.FMLATXBX$132, n);
        }
    }
    
    public STObjectType.Enum getObjectType() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTClientDataImpl.OBJECTTYPE$134);
            if (simpleValue == null) {
                return null;
            }
            return (STObjectType.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STObjectType xgetObjectType() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STObjectType)this.get_store().find_attribute_user(CTClientDataImpl.OBJECTTYPE$134);
        }
    }
    
    public void setObjectType(final STObjectType.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTClientDataImpl.OBJECTTYPE$134);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTClientDataImpl.OBJECTTYPE$134);
            }
            simpleValue.setEnumValue(enumValue);
        }
    }
    
    public void xsetObjectType(final STObjectType stObjectType) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STObjectType stObjectType2 = (STObjectType)this.get_store().find_attribute_user(CTClientDataImpl.OBJECTTYPE$134);
            if (stObjectType2 == null) {
                stObjectType2 = (STObjectType)this.get_store().add_attribute_user(CTClientDataImpl.OBJECTTYPE$134);
            }
            stObjectType2.set(stObjectType);
        }
    }
    
    static {
        MOVEWITHCELLS$0 = new QName("urn:schemas-microsoft-com:office:excel", "MoveWithCells");
        SIZEWITHCELLS$2 = new QName("urn:schemas-microsoft-com:office:excel", "SizeWithCells");
        ANCHOR$4 = new QName("urn:schemas-microsoft-com:office:excel", "Anchor");
        LOCKED$6 = new QName("urn:schemas-microsoft-com:office:excel", "Locked");
        DEFAULTSIZE$8 = new QName("urn:schemas-microsoft-com:office:excel", "DefaultSize");
        PRINTOBJECT$10 = new QName("urn:schemas-microsoft-com:office:excel", "PrintObject");
        DISABLED$12 = new QName("urn:schemas-microsoft-com:office:excel", "Disabled");
        AUTOFILL$14 = new QName("urn:schemas-microsoft-com:office:excel", "AutoFill");
        AUTOLINE$16 = new QName("urn:schemas-microsoft-com:office:excel", "AutoLine");
        AUTOPICT$18 = new QName("urn:schemas-microsoft-com:office:excel", "AutoPict");
        FMLAMACRO$20 = new QName("urn:schemas-microsoft-com:office:excel", "FmlaMacro");
        TEXTHALIGN$22 = new QName("urn:schemas-microsoft-com:office:excel", "TextHAlign");
        TEXTVALIGN$24 = new QName("urn:schemas-microsoft-com:office:excel", "TextVAlign");
        LOCKTEXT$26 = new QName("urn:schemas-microsoft-com:office:excel", "LockText");
        JUSTLASTX$28 = new QName("urn:schemas-microsoft-com:office:excel", "JustLastX");
        SECRETEDIT$30 = new QName("urn:schemas-microsoft-com:office:excel", "SecretEdit");
        DEFAULT$32 = new QName("urn:schemas-microsoft-com:office:excel", "Default");
        HELP$34 = new QName("urn:schemas-microsoft-com:office:excel", "Help");
        CANCEL$36 = new QName("urn:schemas-microsoft-com:office:excel", "Cancel");
        DISMISS$38 = new QName("urn:schemas-microsoft-com:office:excel", "Dismiss");
        ACCEL$40 = new QName("urn:schemas-microsoft-com:office:excel", "Accel");
        ACCEL2$42 = new QName("urn:schemas-microsoft-com:office:excel", "Accel2");
        ROW$44 = new QName("urn:schemas-microsoft-com:office:excel", "Row");
        COLUMN$46 = new QName("urn:schemas-microsoft-com:office:excel", "Column");
        VISIBLE$48 = new QName("urn:schemas-microsoft-com:office:excel", "Visible");
        ROWHIDDEN$50 = new QName("urn:schemas-microsoft-com:office:excel", "RowHidden");
        COLHIDDEN$52 = new QName("urn:schemas-microsoft-com:office:excel", "ColHidden");
        VTEDIT$54 = new QName("urn:schemas-microsoft-com:office:excel", "VTEdit");
        MULTILINE$56 = new QName("urn:schemas-microsoft-com:office:excel", "MultiLine");
        VSCROLL$58 = new QName("urn:schemas-microsoft-com:office:excel", "VScroll");
        VALIDIDS$60 = new QName("urn:schemas-microsoft-com:office:excel", "ValidIds");
        FMLARANGE$62 = new QName("urn:schemas-microsoft-com:office:excel", "FmlaRange");
        WIDTHMIN$64 = new QName("urn:schemas-microsoft-com:office:excel", "WidthMin");
        SEL$66 = new QName("urn:schemas-microsoft-com:office:excel", "Sel");
        NOTHREED2$68 = new QName("urn:schemas-microsoft-com:office:excel", "NoThreeD2");
        SELTYPE$70 = new QName("urn:schemas-microsoft-com:office:excel", "SelType");
        MULTISEL$72 = new QName("urn:schemas-microsoft-com:office:excel", "MultiSel");
        LCT$74 = new QName("urn:schemas-microsoft-com:office:excel", "LCT");
        LISTITEM$76 = new QName("urn:schemas-microsoft-com:office:excel", "ListItem");
        DROPSTYLE$78 = new QName("urn:schemas-microsoft-com:office:excel", "DropStyle");
        COLORED$80 = new QName("urn:schemas-microsoft-com:office:excel", "Colored");
        DROPLINES$82 = new QName("urn:schemas-microsoft-com:office:excel", "DropLines");
        CHECKED$84 = new QName("urn:schemas-microsoft-com:office:excel", "Checked");
        FMLALINK$86 = new QName("urn:schemas-microsoft-com:office:excel", "FmlaLink");
        FMLAPICT$88 = new QName("urn:schemas-microsoft-com:office:excel", "FmlaPict");
        NOTHREED$90 = new QName("urn:schemas-microsoft-com:office:excel", "NoThreeD");
        FIRSTBUTTON$92 = new QName("urn:schemas-microsoft-com:office:excel", "FirstButton");
        FMLAGROUP$94 = new QName("urn:schemas-microsoft-com:office:excel", "FmlaGroup");
        VAL$96 = new QName("urn:schemas-microsoft-com:office:excel", "Val");
        MIN$98 = new QName("urn:schemas-microsoft-com:office:excel", "Min");
        MAX$100 = new QName("urn:schemas-microsoft-com:office:excel", "Max");
        INC$102 = new QName("urn:schemas-microsoft-com:office:excel", "Inc");
        PAGE$104 = new QName("urn:schemas-microsoft-com:office:excel", "Page");
        HORIZ$106 = new QName("urn:schemas-microsoft-com:office:excel", "Horiz");
        DX$108 = new QName("urn:schemas-microsoft-com:office:excel", "Dx");
        MAPOCX$110 = new QName("urn:schemas-microsoft-com:office:excel", "MapOCX");
        CF$112 = new QName("urn:schemas-microsoft-com:office:excel", "CF");
        CAMERA$114 = new QName("urn:schemas-microsoft-com:office:excel", "Camera");
        RECALCALWAYS$116 = new QName("urn:schemas-microsoft-com:office:excel", "RecalcAlways");
        AUTOSCALE$118 = new QName("urn:schemas-microsoft-com:office:excel", "AutoScale");
        DDE$120 = new QName("urn:schemas-microsoft-com:office:excel", "DDE");
        UIOBJ$122 = new QName("urn:schemas-microsoft-com:office:excel", "UIObj");
        SCRIPTTEXT$124 = new QName("urn:schemas-microsoft-com:office:excel", "ScriptText");
        SCRIPTEXTENDED$126 = new QName("urn:schemas-microsoft-com:office:excel", "ScriptExtended");
        SCRIPTLANGUAGE$128 = new QName("urn:schemas-microsoft-com:office:excel", "ScriptLanguage");
        SCRIPTLOCATION$130 = new QName("urn:schemas-microsoft-com:office:excel", "ScriptLocation");
        FMLATXBX$132 = new QName("urn:schemas-microsoft-com:office:excel", "FmlaTxbx");
        OBJECTTYPE$134 = new QName("", "ObjectType");
    }
}
