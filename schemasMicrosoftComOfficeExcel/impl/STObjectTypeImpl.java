// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComOfficeExcel.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComOfficeExcel.STObjectType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STObjectTypeImpl extends JavaStringEnumerationHolderEx implements STObjectType
{
    public STObjectTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STObjectTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
