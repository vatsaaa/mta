// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComOfficeOffice;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import schemasMicrosoftComVml.STExt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTShapeLayout extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTShapeLayout.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctshapelayoutbda4type");
    
    CTIdMap getIdmap();
    
    boolean isSetIdmap();
    
    void setIdmap(final CTIdMap p0);
    
    CTIdMap addNewIdmap();
    
    void unsetIdmap();
    
    CTRegroupTable getRegrouptable();
    
    boolean isSetRegrouptable();
    
    void setRegrouptable(final CTRegroupTable p0);
    
    CTRegroupTable addNewRegrouptable();
    
    void unsetRegrouptable();
    
    CTRules getRules();
    
    boolean isSetRules();
    
    void setRules(final CTRules p0);
    
    CTRules addNewRules();
    
    void unsetRules();
    
    STExt.Enum getExt();
    
    STExt xgetExt();
    
    boolean isSetExt();
    
    void setExt(final STExt.Enum p0);
    
    void xsetExt(final STExt p0);
    
    void unsetExt();
    
    public static final class Factory
    {
        public static CTShapeLayout newInstance() {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().newInstance(CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout newInstance(final XmlOptions xmlOptions) {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().newInstance(CTShapeLayout.type, xmlOptions);
        }
        
        public static CTShapeLayout parse(final String s) throws XmlException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(s, CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(s, CTShapeLayout.type, xmlOptions);
        }
        
        public static CTShapeLayout parse(final File file) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(file, CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(file, CTShapeLayout.type, xmlOptions);
        }
        
        public static CTShapeLayout parse(final URL url) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(url, CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(url, CTShapeLayout.type, xmlOptions);
        }
        
        public static CTShapeLayout parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(inputStream, CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(inputStream, CTShapeLayout.type, xmlOptions);
        }
        
        public static CTShapeLayout parse(final Reader reader) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(reader, CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(reader, CTShapeLayout.type, xmlOptions);
        }
        
        public static CTShapeLayout parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShapeLayout.type, xmlOptions);
        }
        
        public static CTShapeLayout parse(final Node node) throws XmlException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(node, CTShapeLayout.type, null);
        }
        
        public static CTShapeLayout parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(node, CTShapeLayout.type, xmlOptions);
        }
        
        @Deprecated
        public static CTShapeLayout parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShapeLayout.type, null);
        }
        
        @Deprecated
        public static CTShapeLayout parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTShapeLayout)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShapeLayout.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShapeLayout.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShapeLayout.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
