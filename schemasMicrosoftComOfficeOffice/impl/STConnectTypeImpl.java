// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComOfficeOffice.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComOfficeOffice.STConnectType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STConnectTypeImpl extends JavaStringEnumerationHolderEx implements STConnectType
{
    public STConnectTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STConnectTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
