// 
// Decompiled by Procyon v0.5.36
// 

package schemasMicrosoftComOfficeOffice.impl;

import org.apache.xmlbeans.SchemaType;
import schemasMicrosoftComOfficeOffice.STInsetMode;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STInsetModeImpl extends JavaStringEnumerationHolderEx implements STInsetMode
{
    public STInsetModeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STInsetModeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
