// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;
import com.google.gson.internal.$Gson$Types;

final class ObjectNavigator
{
    private final ExclusionStrategy exclusionStrategy;
    private final ReflectingFieldNavigator reflectingFieldNavigator;
    
    ObjectNavigator(final ExclusionStrategy strategy) {
        this.exclusionStrategy = ((strategy == null) ? new NullExclusionStrategy() : strategy);
        this.reflectingFieldNavigator = new ReflectingFieldNavigator(this.exclusionStrategy);
    }
    
    public void accept(final ObjectTypePair objTypePair, final Visitor visitor) {
        if (this.exclusionStrategy.shouldSkipClass($Gson$Types.getRawType(objTypePair.type))) {
            return;
        }
        final boolean visitedWithCustomHandler = visitor.visitUsingCustomHandler(objTypePair);
        if (!visitedWithCustomHandler) {
            final Object obj = objTypePair.getObject();
            final Object objectToVisit = (obj == null) ? visitor.getTarget() : obj;
            if (objectToVisit == null) {
                return;
            }
            objTypePair.setObject(objectToVisit);
            visitor.start(objTypePair);
            try {
                if ($Gson$Types.isArray(objTypePair.type)) {
                    visitor.visitArray(objectToVisit, objTypePair.type);
                }
                else if (objTypePair.type == Object.class && isPrimitiveOrString(objectToVisit)) {
                    visitor.visitPrimitive(objectToVisit);
                    visitor.getTarget();
                }
                else {
                    visitor.startVisitingObject(objectToVisit);
                    this.reflectingFieldNavigator.visitFieldsReflectively(objTypePair, visitor);
                }
            }
            finally {
                visitor.end(objTypePair);
            }
        }
    }
    
    private static boolean isPrimitiveOrString(final Object objectToVisit) {
        final Class<?> realClazz = objectToVisit.getClass();
        return realClazz == Object.class || realClazz == String.class || Primitives.unwrap(realClazz).isPrimitive();
    }
    
    public interface Visitor
    {
        void start(final ObjectTypePair p0);
        
        void end(final ObjectTypePair p0);
        
        void startVisitingObject(final Object p0);
        
        void visitArray(final Object p0, final Type p1);
        
        void visitObjectField(final FieldAttributes p0, final Type p1, final Object p2);
        
        void visitArrayField(final FieldAttributes p0, final Type p1, final Object p2);
        
        boolean visitUsingCustomHandler(final ObjectTypePair p0);
        
        boolean visitFieldUsingCustomHandler(final FieldAttributes p0, final Type p1, final Object p2);
        
        void visitPrimitive(final Object p0);
        
        Object getTarget();
    }
}
