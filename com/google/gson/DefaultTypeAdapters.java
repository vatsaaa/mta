// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Iterator;
import com.google.gson.internal.$Gson$Types;
import java.lang.reflect.ParameterizedType;
import java.util.StringTokenizer;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Set;
import java.util.HashSet;
import java.util.Queue;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Collection;
import java.net.InetAddress;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.net.URI;
import java.lang.reflect.Type;
import java.net.URL;

final class DefaultTypeAdapters
{
    private static final DefaultDateTypeAdapter DATE_TYPE_ADAPTER;
    private static final DefaultJavaSqlDateTypeAdapter JAVA_SQL_DATE_TYPE_ADAPTER;
    private static final DefaultTimeTypeAdapter TIME_TYPE_ADAPTER;
    private static final DefaultTimestampDeserializer TIMESTAMP_DESERIALIZER;
    private static final EnumTypeAdapter ENUM_TYPE_ADAPTER;
    private static final UrlTypeAdapter URL_TYPE_ADAPTER;
    private static final UriTypeAdapter URI_TYPE_ADAPTER;
    private static final UuidTypeAdapter UUUID_TYPE_ADAPTER;
    private static final LocaleTypeAdapter LOCALE_TYPE_ADAPTER;
    private static final DefaultInetAddressAdapter INET_ADDRESS_ADAPTER;
    private static final CollectionTypeAdapter COLLECTION_TYPE_ADAPTER;
    private static final MapTypeAdapter MAP_TYPE_ADAPTER;
    private static final BigDecimalTypeAdapter BIG_DECIMAL_TYPE_ADAPTER;
    private static final BigIntegerTypeAdapter BIG_INTEGER_TYPE_ADAPTER;
    private static final BooleanTypeAdapter BOOLEAN_TYPE_ADAPTER;
    private static final ByteTypeAdapter BYTE_TYPE_ADAPTER;
    private static final CharacterTypeAdapter CHARACTER_TYPE_ADAPTER;
    private static final DoubleDeserializer DOUBLE_TYPE_ADAPTER;
    private static final FloatDeserializer FLOAT_TYPE_ADAPTER;
    private static final IntegerTypeAdapter INTEGER_TYPE_ADAPTER;
    private static final LongDeserializer LONG_DESERIALIZER;
    private static final NumberTypeAdapter NUMBER_TYPE_ADAPTER;
    private static final ShortTypeAdapter SHORT_TYPE_ADAPTER;
    private static final StringTypeAdapter STRING_TYPE_ADAPTER;
    private static final StringBuilderTypeAdapter STRING_BUILDER_TYPE_ADAPTER;
    private static final StringBufferTypeAdapter STRING_BUFFER_TYPE_ADAPTER;
    private static final GregorianCalendarTypeAdapter GREGORIAN_CALENDAR_TYPE_ADAPTER;
    private static final ParameterizedTypeHandlerMap<JsonSerializer<?>> DEFAULT_SERIALIZERS;
    static final ParameterizedTypeHandlerMap<JsonSerializer<?>> DEFAULT_HIERARCHY_SERIALIZERS;
    private static final ParameterizedTypeHandlerMap<JsonDeserializer<?>> DEFAULT_DESERIALIZERS;
    static final ParameterizedTypeHandlerMap<JsonDeserializer<?>> DEFAULT_HIERARCHY_DESERIALIZERS;
    private static final ParameterizedTypeHandlerMap<InstanceCreator<?>> DEFAULT_INSTANCE_CREATORS;
    
    private static ParameterizedTypeHandlerMap<JsonSerializer<?>> createDefaultSerializers() {
        final ParameterizedTypeHandlerMap<JsonSerializer<?>> map = new ParameterizedTypeHandlerMap<JsonSerializer<?>>();
        map.register(URL.class, DefaultTypeAdapters.URL_TYPE_ADAPTER);
        map.register(URI.class, DefaultTypeAdapters.URI_TYPE_ADAPTER);
        map.register(UUID.class, DefaultTypeAdapters.UUUID_TYPE_ADAPTER);
        map.register(Locale.class, DefaultTypeAdapters.LOCALE_TYPE_ADAPTER);
        map.register(Date.class, DefaultTypeAdapters.DATE_TYPE_ADAPTER);
        map.register(java.sql.Date.class, DefaultTypeAdapters.JAVA_SQL_DATE_TYPE_ADAPTER);
        map.register(Timestamp.class, DefaultTypeAdapters.DATE_TYPE_ADAPTER);
        map.register(Time.class, DefaultTypeAdapters.TIME_TYPE_ADAPTER);
        map.register(Calendar.class, DefaultTypeAdapters.GREGORIAN_CALENDAR_TYPE_ADAPTER);
        map.register(GregorianCalendar.class, DefaultTypeAdapters.GREGORIAN_CALENDAR_TYPE_ADAPTER);
        map.register(BigDecimal.class, DefaultTypeAdapters.BIG_DECIMAL_TYPE_ADAPTER);
        map.register(BigInteger.class, DefaultTypeAdapters.BIG_INTEGER_TYPE_ADAPTER);
        map.register(Boolean.class, DefaultTypeAdapters.BOOLEAN_TYPE_ADAPTER);
        map.register(Boolean.TYPE, DefaultTypeAdapters.BOOLEAN_TYPE_ADAPTER);
        map.register(Byte.class, DefaultTypeAdapters.BYTE_TYPE_ADAPTER);
        map.register(Byte.TYPE, DefaultTypeAdapters.BYTE_TYPE_ADAPTER);
        map.register(Character.class, DefaultTypeAdapters.CHARACTER_TYPE_ADAPTER);
        map.register(Character.TYPE, DefaultTypeAdapters.CHARACTER_TYPE_ADAPTER);
        map.register(Integer.class, DefaultTypeAdapters.INTEGER_TYPE_ADAPTER);
        map.register(Integer.TYPE, DefaultTypeAdapters.INTEGER_TYPE_ADAPTER);
        map.register(Number.class, DefaultTypeAdapters.NUMBER_TYPE_ADAPTER);
        map.register(Short.class, DefaultTypeAdapters.SHORT_TYPE_ADAPTER);
        map.register(Short.TYPE, DefaultTypeAdapters.SHORT_TYPE_ADAPTER);
        map.register(String.class, DefaultTypeAdapters.STRING_TYPE_ADAPTER);
        map.register(StringBuilder.class, DefaultTypeAdapters.STRING_BUILDER_TYPE_ADAPTER);
        map.register(StringBuffer.class, DefaultTypeAdapters.STRING_BUFFER_TYPE_ADAPTER);
        map.makeUnmodifiable();
        return map;
    }
    
    private static ParameterizedTypeHandlerMap<JsonSerializer<?>> createDefaultHierarchySerializers() {
        final ParameterizedTypeHandlerMap<JsonSerializer<?>> map = new ParameterizedTypeHandlerMap<JsonSerializer<?>>();
        map.registerForTypeHierarchy(Enum.class, DefaultTypeAdapters.ENUM_TYPE_ADAPTER);
        map.registerForTypeHierarchy(InetAddress.class, DefaultTypeAdapters.INET_ADDRESS_ADAPTER);
        map.registerForTypeHierarchy(Collection.class, DefaultTypeAdapters.COLLECTION_TYPE_ADAPTER);
        map.registerForTypeHierarchy(Map.class, DefaultTypeAdapters.MAP_TYPE_ADAPTER);
        map.makeUnmodifiable();
        return map;
    }
    
    private static ParameterizedTypeHandlerMap<JsonDeserializer<?>> createDefaultDeserializers() {
        final ParameterizedTypeHandlerMap<JsonDeserializer<?>> map = new ParameterizedTypeHandlerMap<JsonDeserializer<?>>();
        map.register(URL.class, wrapDeserializer(DefaultTypeAdapters.URL_TYPE_ADAPTER));
        map.register(URI.class, wrapDeserializer(DefaultTypeAdapters.URI_TYPE_ADAPTER));
        map.register(UUID.class, wrapDeserializer(DefaultTypeAdapters.UUUID_TYPE_ADAPTER));
        map.register(Locale.class, wrapDeserializer(DefaultTypeAdapters.LOCALE_TYPE_ADAPTER));
        map.register(Date.class, wrapDeserializer(DefaultTypeAdapters.DATE_TYPE_ADAPTER));
        map.register(java.sql.Date.class, wrapDeserializer(DefaultTypeAdapters.JAVA_SQL_DATE_TYPE_ADAPTER));
        map.register(Timestamp.class, wrapDeserializer(DefaultTypeAdapters.TIMESTAMP_DESERIALIZER));
        map.register(Time.class, wrapDeserializer(DefaultTypeAdapters.TIME_TYPE_ADAPTER));
        map.register(Calendar.class, DefaultTypeAdapters.GREGORIAN_CALENDAR_TYPE_ADAPTER);
        map.register(GregorianCalendar.class, DefaultTypeAdapters.GREGORIAN_CALENDAR_TYPE_ADAPTER);
        map.register(BigDecimal.class, DefaultTypeAdapters.BIG_DECIMAL_TYPE_ADAPTER);
        map.register(BigInteger.class, DefaultTypeAdapters.BIG_INTEGER_TYPE_ADAPTER);
        map.register(Boolean.class, DefaultTypeAdapters.BOOLEAN_TYPE_ADAPTER);
        map.register(Boolean.TYPE, DefaultTypeAdapters.BOOLEAN_TYPE_ADAPTER);
        map.register(Byte.class, DefaultTypeAdapters.BYTE_TYPE_ADAPTER);
        map.register(Byte.TYPE, DefaultTypeAdapters.BYTE_TYPE_ADAPTER);
        map.register(Character.class, wrapDeserializer(DefaultTypeAdapters.CHARACTER_TYPE_ADAPTER));
        map.register(Character.TYPE, wrapDeserializer(DefaultTypeAdapters.CHARACTER_TYPE_ADAPTER));
        map.register(Double.class, DefaultTypeAdapters.DOUBLE_TYPE_ADAPTER);
        map.register(Double.TYPE, DefaultTypeAdapters.DOUBLE_TYPE_ADAPTER);
        map.register(Float.class, DefaultTypeAdapters.FLOAT_TYPE_ADAPTER);
        map.register(Float.TYPE, DefaultTypeAdapters.FLOAT_TYPE_ADAPTER);
        map.register(Integer.class, DefaultTypeAdapters.INTEGER_TYPE_ADAPTER);
        map.register(Integer.TYPE, DefaultTypeAdapters.INTEGER_TYPE_ADAPTER);
        map.register(Long.class, DefaultTypeAdapters.LONG_DESERIALIZER);
        map.register(Long.TYPE, DefaultTypeAdapters.LONG_DESERIALIZER);
        map.register(Number.class, DefaultTypeAdapters.NUMBER_TYPE_ADAPTER);
        map.register(Short.class, DefaultTypeAdapters.SHORT_TYPE_ADAPTER);
        map.register(Short.TYPE, DefaultTypeAdapters.SHORT_TYPE_ADAPTER);
        map.register(String.class, wrapDeserializer(DefaultTypeAdapters.STRING_TYPE_ADAPTER));
        map.register(StringBuilder.class, wrapDeserializer(DefaultTypeAdapters.STRING_BUILDER_TYPE_ADAPTER));
        map.register(StringBuffer.class, wrapDeserializer(DefaultTypeAdapters.STRING_BUFFER_TYPE_ADAPTER));
        map.makeUnmodifiable();
        return map;
    }
    
    private static ParameterizedTypeHandlerMap<JsonDeserializer<?>> createDefaultHierarchyDeserializers() {
        final ParameterizedTypeHandlerMap<JsonDeserializer<?>> map = new ParameterizedTypeHandlerMap<JsonDeserializer<?>>();
        map.registerForTypeHierarchy(Enum.class, wrapDeserializer(DefaultTypeAdapters.ENUM_TYPE_ADAPTER));
        map.registerForTypeHierarchy(InetAddress.class, wrapDeserializer(DefaultTypeAdapters.INET_ADDRESS_ADAPTER));
        map.registerForTypeHierarchy(Collection.class, wrapDeserializer(DefaultTypeAdapters.COLLECTION_TYPE_ADAPTER));
        map.registerForTypeHierarchy(Map.class, wrapDeserializer(DefaultTypeAdapters.MAP_TYPE_ADAPTER));
        map.makeUnmodifiable();
        return map;
    }
    
    private static ParameterizedTypeHandlerMap<InstanceCreator<?>> createDefaultInstanceCreators() {
        final ParameterizedTypeHandlerMap<InstanceCreator<?>> map = new ParameterizedTypeHandlerMap<InstanceCreator<?>>();
        final DefaultConstructorAllocator allocator = new DefaultConstructorAllocator(50);
        map.registerForTypeHierarchy(Map.class, new DefaultConstructorCreator<Object>(LinkedHashMap.class, allocator));
        final DefaultConstructorCreator<List> listCreator = new DefaultConstructorCreator<List>(ArrayList.class, allocator);
        final DefaultConstructorCreator<Queue> queueCreator = new DefaultConstructorCreator<Queue>(LinkedList.class, allocator);
        final DefaultConstructorCreator<Set> setCreator = new DefaultConstructorCreator<Set>(HashSet.class, allocator);
        final DefaultConstructorCreator<SortedSet> sortedSetCreator = new DefaultConstructorCreator<SortedSet>(TreeSet.class, allocator);
        map.registerForTypeHierarchy(Collection.class, listCreator);
        map.registerForTypeHierarchy(Queue.class, queueCreator);
        map.registerForTypeHierarchy(Set.class, setCreator);
        map.registerForTypeHierarchy(SortedSet.class, sortedSetCreator);
        map.makeUnmodifiable();
        return map;
    }
    
    private static JsonDeserializer<?> wrapDeserializer(final JsonDeserializer<?> deserializer) {
        return new JsonDeserializerExceptionWrapper<Object>(deserializer);
    }
    
    static ParameterizedTypeHandlerMap<JsonSerializer<?>> getDefaultSerializers() {
        return getDefaultSerializers(false, LongSerializationPolicy.DEFAULT);
    }
    
    static ParameterizedTypeHandlerMap<JsonSerializer<?>> getAllDefaultSerializers() {
        final ParameterizedTypeHandlerMap<JsonSerializer<?>> defaultSerializers = getDefaultSerializers(false, LongSerializationPolicy.DEFAULT);
        defaultSerializers.register(DefaultTypeAdapters.DEFAULT_HIERARCHY_SERIALIZERS);
        return defaultSerializers;
    }
    
    static ParameterizedTypeHandlerMap<JsonDeserializer<?>> getAllDefaultDeserializers() {
        final ParameterizedTypeHandlerMap<JsonDeserializer<?>> defaultDeserializers = getDefaultDeserializers().copyOf();
        defaultDeserializers.register(DefaultTypeAdapters.DEFAULT_HIERARCHY_DESERIALIZERS);
        return defaultDeserializers;
    }
    
    static ParameterizedTypeHandlerMap<JsonSerializer<?>> getDefaultSerializers(final boolean serializeSpecialFloatingPointValues, final LongSerializationPolicy longSerializationPolicy) {
        final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers = new ParameterizedTypeHandlerMap<JsonSerializer<?>>();
        final DoubleSerializer doubleSerializer = new DoubleSerializer(serializeSpecialFloatingPointValues);
        serializers.registerIfAbsent(Double.class, doubleSerializer);
        serializers.registerIfAbsent(Double.TYPE, doubleSerializer);
        final FloatSerializer floatSerializer = new FloatSerializer(serializeSpecialFloatingPointValues);
        serializers.registerIfAbsent(Float.class, floatSerializer);
        serializers.registerIfAbsent(Float.TYPE, floatSerializer);
        final LongSerializer longSerializer = new LongSerializer(longSerializationPolicy);
        serializers.registerIfAbsent(Long.class, longSerializer);
        serializers.registerIfAbsent(Long.TYPE, longSerializer);
        serializers.registerIfAbsent(DefaultTypeAdapters.DEFAULT_SERIALIZERS);
        return serializers;
    }
    
    static ParameterizedTypeHandlerMap<JsonDeserializer<?>> getDefaultDeserializers() {
        return DefaultTypeAdapters.DEFAULT_DESERIALIZERS;
    }
    
    static ParameterizedTypeHandlerMap<InstanceCreator<?>> getDefaultInstanceCreators() {
        return DefaultTypeAdapters.DEFAULT_INSTANCE_CREATORS;
    }
    
    static {
        DATE_TYPE_ADAPTER = new DefaultDateTypeAdapter();
        JAVA_SQL_DATE_TYPE_ADAPTER = new DefaultJavaSqlDateTypeAdapter();
        TIME_TYPE_ADAPTER = new DefaultTimeTypeAdapter();
        TIMESTAMP_DESERIALIZER = new DefaultTimestampDeserializer();
        ENUM_TYPE_ADAPTER = new EnumTypeAdapter();
        URL_TYPE_ADAPTER = new UrlTypeAdapter();
        URI_TYPE_ADAPTER = new UriTypeAdapter();
        UUUID_TYPE_ADAPTER = new UuidTypeAdapter();
        LOCALE_TYPE_ADAPTER = new LocaleTypeAdapter();
        INET_ADDRESS_ADAPTER = new DefaultInetAddressAdapter();
        COLLECTION_TYPE_ADAPTER = new CollectionTypeAdapter();
        MAP_TYPE_ADAPTER = new MapTypeAdapter();
        BIG_DECIMAL_TYPE_ADAPTER = new BigDecimalTypeAdapter();
        BIG_INTEGER_TYPE_ADAPTER = new BigIntegerTypeAdapter();
        BOOLEAN_TYPE_ADAPTER = new BooleanTypeAdapter();
        BYTE_TYPE_ADAPTER = new ByteTypeAdapter();
        CHARACTER_TYPE_ADAPTER = new CharacterTypeAdapter();
        DOUBLE_TYPE_ADAPTER = new DoubleDeserializer();
        FLOAT_TYPE_ADAPTER = new FloatDeserializer();
        INTEGER_TYPE_ADAPTER = new IntegerTypeAdapter();
        LONG_DESERIALIZER = new LongDeserializer();
        NUMBER_TYPE_ADAPTER = new NumberTypeAdapter();
        SHORT_TYPE_ADAPTER = new ShortTypeAdapter();
        STRING_TYPE_ADAPTER = new StringTypeAdapter();
        STRING_BUILDER_TYPE_ADAPTER = new StringBuilderTypeAdapter();
        STRING_BUFFER_TYPE_ADAPTER = new StringBufferTypeAdapter();
        GREGORIAN_CALENDAR_TYPE_ADAPTER = new GregorianCalendarTypeAdapter();
        DEFAULT_SERIALIZERS = createDefaultSerializers();
        DEFAULT_HIERARCHY_SERIALIZERS = createDefaultHierarchySerializers();
        DEFAULT_DESERIALIZERS = createDefaultDeserializers();
        DEFAULT_HIERARCHY_DESERIALIZERS = createDefaultHierarchyDeserializers();
        DEFAULT_INSTANCE_CREATORS = createDefaultInstanceCreators();
    }
    
    static final class DefaultDateTypeAdapter implements JsonSerializer<Date>, JsonDeserializer<Date>
    {
        private final DateFormat enUsFormat;
        private final DateFormat localFormat;
        private final DateFormat iso8601Format;
        
        DefaultDateTypeAdapter() {
            this(DateFormat.getDateTimeInstance(2, 2, Locale.US), DateFormat.getDateTimeInstance(2, 2));
        }
        
        DefaultDateTypeAdapter(final String datePattern) {
            this(new SimpleDateFormat(datePattern, Locale.US), new SimpleDateFormat(datePattern));
        }
        
        DefaultDateTypeAdapter(final int style) {
            this(DateFormat.getDateInstance(style, Locale.US), DateFormat.getDateInstance(style));
        }
        
        public DefaultDateTypeAdapter(final int dateStyle, final int timeStyle) {
            this(DateFormat.getDateTimeInstance(dateStyle, timeStyle, Locale.US), DateFormat.getDateTimeInstance(dateStyle, timeStyle));
        }
        
        DefaultDateTypeAdapter(final DateFormat enUsFormat, final DateFormat localFormat) {
            this.enUsFormat = enUsFormat;
            this.localFormat = localFormat;
            (this.iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)).setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        
        public JsonElement serialize(final Date src, final Type typeOfSrc, final JsonSerializationContext context) {
            synchronized (this.localFormat) {
                final String dateFormatAsString = this.enUsFormat.format(src);
                return new JsonPrimitive(dateFormatAsString);
            }
        }
        
        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            if (!(json instanceof JsonPrimitive)) {
                throw new JsonParseException("The date should be a string value");
            }
            final Date date = this.deserializeToDate(json);
            if (typeOfT == Date.class) {
                return date;
            }
            if (typeOfT == Timestamp.class) {
                return new Timestamp(date.getTime());
            }
            if (typeOfT == java.sql.Date.class) {
                return new java.sql.Date(date.getTime());
            }
            throw new IllegalArgumentException(this.getClass() + " cannot deserialize to " + typeOfT);
        }
        
        private Date deserializeToDate(final JsonElement json) {
            synchronized (this.localFormat) {
                try {
                    return this.localFormat.parse(json.getAsString());
                }
                catch (ParseException ignored) {
                    try {
                        return this.enUsFormat.parse(json.getAsString());
                    }
                    catch (ParseException ignored) {
                        try {
                            // monitorexit(this.localFormat)
                            return this.iso8601Format.parse(json.getAsString());
                        }
                        catch (ParseException e) {
                            throw new JsonSyntaxException(json.getAsString(), e);
                        }
                    }
                }
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(DefaultDateTypeAdapter.class.getSimpleName());
            sb.append('(').append(this.localFormat.getClass().getSimpleName()).append(')');
            return sb.toString();
        }
    }
    
    static final class DefaultJavaSqlDateTypeAdapter implements JsonSerializer<java.sql.Date>, JsonDeserializer<java.sql.Date>
    {
        private final DateFormat format;
        
        DefaultJavaSqlDateTypeAdapter() {
            this.format = new SimpleDateFormat("MMM d, yyyy");
        }
        
        public JsonElement serialize(final java.sql.Date src, final Type typeOfSrc, final JsonSerializationContext context) {
            synchronized (this.format) {
                final String dateFormatAsString = this.format.format(src);
                return new JsonPrimitive(dateFormatAsString);
            }
        }
        
        public java.sql.Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            if (!(json instanceof JsonPrimitive)) {
                throw new JsonParseException("The date should be a string value");
            }
            try {
                synchronized (this.format) {
                    final Date date = this.format.parse(json.getAsString());
                    return new java.sql.Date(date.getTime());
                }
            }
            catch (ParseException e) {
                throw new JsonSyntaxException(e);
            }
        }
    }
    
    static final class DefaultTimestampDeserializer implements JsonDeserializer<Timestamp>
    {
        public Timestamp deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            final Date date = context.deserialize(json, Date.class);
            return new Timestamp(date.getTime());
        }
    }
    
    static final class DefaultTimeTypeAdapter implements JsonSerializer<Time>, JsonDeserializer<Time>
    {
        private final DateFormat format;
        
        DefaultTimeTypeAdapter() {
            this.format = new SimpleDateFormat("hh:mm:ss a");
        }
        
        public JsonElement serialize(final Time src, final Type typeOfSrc, final JsonSerializationContext context) {
            synchronized (this.format) {
                final String dateFormatAsString = this.format.format(src);
                return new JsonPrimitive(dateFormatAsString);
            }
        }
        
        public Time deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            if (!(json instanceof JsonPrimitive)) {
                throw new JsonParseException("The date should be a string value");
            }
            try {
                synchronized (this.format) {
                    final Date date = this.format.parse(json.getAsString());
                    return new Time(date.getTime());
                }
            }
            catch (ParseException e) {
                throw new JsonSyntaxException(e);
            }
        }
    }
    
    private static final class GregorianCalendarTypeAdapter implements JsonSerializer<GregorianCalendar>, JsonDeserializer<GregorianCalendar>
    {
        private static final String YEAR = "year";
        private static final String MONTH = "month";
        private static final String DAY_OF_MONTH = "dayOfMonth";
        private static final String HOUR_OF_DAY = "hourOfDay";
        private static final String MINUTE = "minute";
        private static final String SECOND = "second";
        
        public JsonElement serialize(final GregorianCalendar src, final Type typeOfSrc, final JsonSerializationContext context) {
            final JsonObject obj = new JsonObject();
            obj.addProperty("year", src.get(1));
            obj.addProperty("month", src.get(2));
            obj.addProperty("dayOfMonth", src.get(5));
            obj.addProperty("hourOfDay", src.get(11));
            obj.addProperty("minute", src.get(12));
            obj.addProperty("second", src.get(13));
            return obj;
        }
        
        public GregorianCalendar deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            final JsonObject obj = json.getAsJsonObject();
            final int year = obj.get("year").getAsInt();
            final int month = obj.get("month").getAsInt();
            final int dayOfMonth = obj.get("dayOfMonth").getAsInt();
            final int hourOfDay = obj.get("hourOfDay").getAsInt();
            final int minute = obj.get("minute").getAsInt();
            final int second = obj.get("second").getAsInt();
            return new GregorianCalendar(year, month, dayOfMonth, hourOfDay, minute, second);
        }
        
        @Override
        public String toString() {
            return GregorianCalendarTypeAdapter.class.getSimpleName();
        }
    }
    
    static final class DefaultInetAddressAdapter implements JsonDeserializer<InetAddress>, JsonSerializer<InetAddress>
    {
        public InetAddress deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return InetAddress.getByName(json.getAsString());
            }
            catch (UnknownHostException e) {
                throw new JsonParseException(e);
            }
        }
        
        public JsonElement serialize(final InetAddress src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.getHostAddress());
        }
    }
    
    private static final class EnumTypeAdapter<T extends Enum<T>> implements JsonSerializer<T>, JsonDeserializer<T>
    {
        public JsonElement serialize(final T src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.name());
        }
        
        public T deserialize(final JsonElement json, final Type classOfT, final JsonDeserializationContext context) throws JsonParseException {
            return Enum.valueOf((Class<T>)classOfT, json.getAsString());
        }
        
        @Override
        public String toString() {
            return EnumTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class UrlTypeAdapter implements JsonSerializer<URL>, JsonDeserializer<URL>
    {
        public JsonElement serialize(final URL src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.toExternalForm());
        }
        
        public URL deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return new URL(json.getAsString());
            }
            catch (MalformedURLException e) {
                throw new JsonSyntaxException(e);
            }
        }
        
        @Override
        public String toString() {
            return UrlTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class UriTypeAdapter implements JsonSerializer<URI>, JsonDeserializer<URI>
    {
        public JsonElement serialize(final URI src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.toASCIIString());
        }
        
        public URI deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return new URI(json.getAsString());
            }
            catch (URISyntaxException e) {
                throw new JsonSyntaxException(e);
            }
        }
        
        @Override
        public String toString() {
            return UriTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class UuidTypeAdapter implements JsonSerializer<UUID>, JsonDeserializer<UUID>
    {
        public JsonElement serialize(final UUID src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
        
        public UUID deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            return UUID.fromString(json.getAsString());
        }
        
        @Override
        public String toString() {
            return UuidTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class LocaleTypeAdapter implements JsonSerializer<Locale>, JsonDeserializer<Locale>
    {
        public JsonElement serialize(final Locale src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
        
        public Locale deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            final String locale = json.getAsString();
            final StringTokenizer tokenizer = new StringTokenizer(locale, "_");
            String language = null;
            String country = null;
            String variant = null;
            if (tokenizer.hasMoreElements()) {
                language = tokenizer.nextToken();
            }
            if (tokenizer.hasMoreElements()) {
                country = tokenizer.nextToken();
            }
            if (tokenizer.hasMoreElements()) {
                variant = tokenizer.nextToken();
            }
            if (country == null && variant == null) {
                return new Locale(language);
            }
            if (variant == null) {
                return new Locale(language, country);
            }
            return new Locale(language, country, variant);
        }
        
        @Override
        public String toString() {
            return LocaleTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class CollectionTypeAdapter implements JsonSerializer<Collection>, JsonDeserializer<Collection>
    {
        public JsonElement serialize(final Collection src, final Type typeOfSrc, final JsonSerializationContext context) {
            if (src == null) {
                return JsonNull.createJsonNull();
            }
            final JsonArray array = new JsonArray();
            Type childGenericType = null;
            if (typeOfSrc instanceof ParameterizedType) {
                final Class<?> rawTypeOfSrc = $Gson$Types.getRawType(typeOfSrc);
                childGenericType = $Gson$Types.getCollectionElementType(typeOfSrc, rawTypeOfSrc);
            }
            for (final Object child : src) {
                if (child == null) {
                    array.add(JsonNull.createJsonNull());
                }
                else {
                    final Type childType = (childGenericType == null || childGenericType == Object.class) ? child.getClass() : childGenericType;
                    final JsonElement element = context.serialize(child, childType);
                    array.add(element);
                }
            }
            return array;
        }
        
        public Collection deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            if (json.isJsonNull()) {
                return null;
            }
            final Collection collection = this.constructCollectionType(typeOfT, context);
            final Type childType = $Gson$Types.getCollectionElementType(typeOfT, $Gson$Types.getRawType(typeOfT));
            for (final JsonElement childElement : json.getAsJsonArray()) {
                if (childElement == null || childElement.isJsonNull()) {
                    collection.add(null);
                }
                else {
                    final Object value = context.deserialize(childElement, childType);
                    collection.add(value);
                }
            }
            return collection;
        }
        
        private Collection constructCollectionType(final Type collectionType, final JsonDeserializationContext context) {
            final JsonDeserializationContextDefault contextImpl = (JsonDeserializationContextDefault)context;
            final ObjectConstructor objectConstructor = contextImpl.getObjectConstructor();
            return objectConstructor.construct(collectionType);
        }
    }
    
    private static final class BigDecimalTypeAdapter implements JsonSerializer<BigDecimal>, JsonDeserializer<BigDecimal>
    {
        public JsonElement serialize(final BigDecimal src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public BigDecimal deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsBigDecimal();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return BigDecimalTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class BigIntegerTypeAdapter implements JsonSerializer<BigInteger>, JsonDeserializer<BigInteger>
    {
        public JsonElement serialize(final BigInteger src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public BigInteger deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsBigInteger();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return BigIntegerTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class NumberTypeAdapter implements JsonSerializer<Number>, JsonDeserializer<Number>
    {
        public JsonElement serialize(final Number src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public Number deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsNumber();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return NumberTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class LongSerializer implements JsonSerializer<Long>
    {
        private final LongSerializationPolicy longSerializationPolicy;
        
        private LongSerializer(final LongSerializationPolicy longSerializationPolicy) {
            this.longSerializationPolicy = longSerializationPolicy;
        }
        
        public JsonElement serialize(final Long src, final Type typeOfSrc, final JsonSerializationContext context) {
            return this.longSerializationPolicy.serialize(src);
        }
        
        @Override
        public String toString() {
            return LongSerializer.class.getSimpleName();
        }
    }
    
    private static final class LongDeserializer implements JsonDeserializer<Long>
    {
        public Long deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsLong();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return LongDeserializer.class.getSimpleName();
        }
    }
    
    private static final class IntegerTypeAdapter implements JsonSerializer<Integer>, JsonDeserializer<Integer>
    {
        public JsonElement serialize(final Integer src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public Integer deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsInt();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return IntegerTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class ShortTypeAdapter implements JsonSerializer<Short>, JsonDeserializer<Short>
    {
        public JsonElement serialize(final Short src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public Short deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsShort();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return ShortTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class ByteTypeAdapter implements JsonSerializer<Byte>, JsonDeserializer<Byte>
    {
        public JsonElement serialize(final Byte src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public Byte deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsByte();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return ByteTypeAdapter.class.getSimpleName();
        }
    }
    
    static final class FloatSerializer implements JsonSerializer<Float>
    {
        private final boolean serializeSpecialFloatingPointValues;
        
        FloatSerializer(final boolean serializeSpecialDoubleValues) {
            this.serializeSpecialFloatingPointValues = serializeSpecialDoubleValues;
        }
        
        public JsonElement serialize(final Float src, final Type typeOfSrc, final JsonSerializationContext context) {
            if (!this.serializeSpecialFloatingPointValues && (Float.isNaN(src) || Float.isInfinite(src))) {
                throw new IllegalArgumentException(src + " is not a valid float value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
            }
            return new JsonPrimitive(src);
        }
    }
    
    private static final class FloatDeserializer implements JsonDeserializer<Float>
    {
        public Float deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsFloat();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return FloatDeserializer.class.getSimpleName();
        }
    }
    
    static final class DoubleSerializer implements JsonSerializer<Double>
    {
        private final boolean serializeSpecialFloatingPointValues;
        
        DoubleSerializer(final boolean serializeSpecialDoubleValues) {
            this.serializeSpecialFloatingPointValues = serializeSpecialDoubleValues;
        }
        
        public JsonElement serialize(final Double src, final Type typeOfSrc, final JsonSerializationContext context) {
            if (!this.serializeSpecialFloatingPointValues && (Double.isNaN(src) || Double.isInfinite(src))) {
                throw new IllegalArgumentException(src + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialDoubleValues() method.");
            }
            return new JsonPrimitive(src);
        }
    }
    
    private static final class DoubleDeserializer implements JsonDeserializer<Double>
    {
        public Double deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsDouble();
            }
            catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
            catch (UnsupportedOperationException e2) {
                throw new JsonSyntaxException(e2);
            }
            catch (IllegalStateException e3) {
                throw new JsonSyntaxException(e3);
            }
        }
        
        @Override
        public String toString() {
            return DoubleDeserializer.class.getSimpleName();
        }
    }
    
    private static final class CharacterTypeAdapter implements JsonSerializer<Character>, JsonDeserializer<Character>
    {
        public JsonElement serialize(final Character src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public Character deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            return json.getAsCharacter();
        }
        
        @Override
        public String toString() {
            return CharacterTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class StringTypeAdapter implements JsonSerializer<String>, JsonDeserializer<String>
    {
        public JsonElement serialize(final String src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public String deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            return json.getAsString();
        }
        
        @Override
        public String toString() {
            return StringTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class StringBuilderTypeAdapter implements JsonSerializer<StringBuilder>, JsonDeserializer<StringBuilder>
    {
        public JsonElement serialize(final StringBuilder src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
        
        public StringBuilder deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            return new StringBuilder(json.getAsString());
        }
        
        @Override
        public String toString() {
            return StringBuilderTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class StringBufferTypeAdapter implements JsonSerializer<StringBuffer>, JsonDeserializer<StringBuffer>
    {
        public JsonElement serialize(final StringBuffer src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
        
        public StringBuffer deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            return new StringBuffer(json.getAsString());
        }
        
        @Override
        public String toString() {
            return StringBufferTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class BooleanTypeAdapter implements JsonSerializer<Boolean>, JsonDeserializer<Boolean>
    {
        public JsonElement serialize(final Boolean src, final Type typeOfSrc, final JsonSerializationContext context) {
            return new JsonPrimitive(src);
        }
        
        public Boolean deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
            try {
                return json.getAsBoolean();
            }
            catch (UnsupportedOperationException e) {
                throw new JsonSyntaxException(e);
            }
            catch (IllegalStateException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
        
        @Override
        public String toString() {
            return BooleanTypeAdapter.class.getSimpleName();
        }
    }
    
    private static final class DefaultConstructorCreator<T> implements InstanceCreator<T>
    {
        private final Class<? extends T> defaultInstance;
        private final DefaultConstructorAllocator allocator;
        
        public DefaultConstructorCreator(final Class<? extends T> defaultInstance, final DefaultConstructorAllocator allocator) {
            this.defaultInstance = defaultInstance;
            this.allocator = allocator;
        }
        
        public T createInstance(final Type type) {
            final Class<?> rawType = $Gson$Types.getRawType(type);
            try {
                final T specificInstance = this.allocator.newInstance(rawType);
                return (specificInstance == null) ? this.allocator.newInstance((Class<T>)this.defaultInstance) : specificInstance;
            }
            catch (Exception e) {
                throw new JsonIOException(e);
            }
        }
        
        @Override
        public String toString() {
            return DefaultConstructorCreator.class.getSimpleName();
        }
    }
}
