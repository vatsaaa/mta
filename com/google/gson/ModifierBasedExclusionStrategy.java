// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Iterator;
import java.util.HashSet;
import java.util.Collection;

final class ModifierBasedExclusionStrategy implements ExclusionStrategy
{
    private final Collection<Integer> modifiers;
    
    public ModifierBasedExclusionStrategy(final int... modifiers) {
        this.modifiers = new HashSet<Integer>();
        if (modifiers != null) {
            for (final int modifier : modifiers) {
                this.modifiers.add(modifier);
            }
        }
    }
    
    public boolean shouldSkipField(final FieldAttributes f) {
        for (final int modifier : this.modifiers) {
            if (f.hasModifier(modifier)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean shouldSkipClass(final Class<?> clazz) {
        return false;
    }
}
