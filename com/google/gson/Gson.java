// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.stream.MalformedJsonException;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.IOException;
import com.google.gson.stream.JsonWriter;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Collection;
import java.util.LinkedList;

public final class Gson
{
    static final boolean DEFAULT_JSON_NON_EXECUTABLE = false;
    static final AnonymousAndLocalClassExclusionStrategy DEFAULT_ANON_LOCAL_CLASS_EXCLUSION_STRATEGY;
    static final SyntheticFieldExclusionStrategy DEFAULT_SYNTHETIC_FIELD_EXCLUSION_STRATEGY;
    static final ModifierBasedExclusionStrategy DEFAULT_MODIFIER_BASED_EXCLUSION_STRATEGY;
    static final FieldNamingStrategy2 DEFAULT_NAMING_POLICY;
    private static final ExclusionStrategy DEFAULT_EXCLUSION_STRATEGY;
    private static final String JSON_NON_EXECUTABLE_PREFIX = ")]}'\n";
    private final ExclusionStrategy deserializationExclusionStrategy;
    private final ExclusionStrategy serializationExclusionStrategy;
    private final FieldNamingStrategy2 fieldNamingPolicy;
    private final MappedObjectConstructor objectConstructor;
    private final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers;
    private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers;
    private final boolean serializeNulls;
    private final boolean htmlSafe;
    private final boolean generateNonExecutableJson;
    private final boolean prettyPrinting;
    
    public Gson() {
        this(Gson.DEFAULT_EXCLUSION_STRATEGY, Gson.DEFAULT_EXCLUSION_STRATEGY, Gson.DEFAULT_NAMING_POLICY, new MappedObjectConstructor(DefaultTypeAdapters.getDefaultInstanceCreators()), false, DefaultTypeAdapters.getAllDefaultSerializers(), DefaultTypeAdapters.getAllDefaultDeserializers(), false, true, false);
    }
    
    Gson(final ExclusionStrategy deserializationExclusionStrategy, final ExclusionStrategy serializationExclusionStrategy, final FieldNamingStrategy2 fieldNamingPolicy, final MappedObjectConstructor objectConstructor, final boolean serializeNulls, final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers, final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers, final boolean generateNonExecutableGson, final boolean htmlSafe, final boolean prettyPrinting) {
        this.deserializationExclusionStrategy = deserializationExclusionStrategy;
        this.serializationExclusionStrategy = serializationExclusionStrategy;
        this.fieldNamingPolicy = fieldNamingPolicy;
        this.objectConstructor = objectConstructor;
        this.serializeNulls = serializeNulls;
        this.serializers = serializers;
        this.deserializers = deserializers;
        this.generateNonExecutableJson = generateNonExecutableGson;
        this.htmlSafe = htmlSafe;
        this.prettyPrinting = prettyPrinting;
    }
    
    private static ExclusionStrategy createExclusionStrategy() {
        final List<ExclusionStrategy> strategies = new LinkedList<ExclusionStrategy>();
        strategies.add(Gson.DEFAULT_ANON_LOCAL_CLASS_EXCLUSION_STRATEGY);
        strategies.add(Gson.DEFAULT_SYNTHETIC_FIELD_EXCLUSION_STRATEGY);
        strategies.add(Gson.DEFAULT_MODIFIER_BASED_EXCLUSION_STRATEGY);
        return new DisjunctionExclusionStrategy(strategies);
    }
    
    public JsonElement toJsonTree(final Object src) {
        if (src == null) {
            return JsonNull.createJsonNull();
        }
        return this.toJsonTree(src, src.getClass());
    }
    
    public JsonElement toJsonTree(final Object src, final Type typeOfSrc) {
        final JsonSerializationContextDefault context = new JsonSerializationContextDefault(new ObjectNavigator(this.serializationExclusionStrategy), this.fieldNamingPolicy, this.serializeNulls, this.serializers);
        return context.serialize(src, typeOfSrc);
    }
    
    public String toJson(final Object src) {
        if (src == null) {
            return this.toJson(JsonNull.createJsonNull());
        }
        return this.toJson(src, src.getClass());
    }
    
    public String toJson(final Object src, final Type typeOfSrc) {
        final StringWriter writer = new StringWriter();
        this.toJson(this.toJsonTree(src, typeOfSrc), writer);
        return writer.toString();
    }
    
    public void toJson(final Object src, final Appendable writer) throws JsonIOException {
        if (src != null) {
            this.toJson(src, src.getClass(), writer);
        }
        else {
            this.toJson(JsonNull.createJsonNull(), writer);
        }
    }
    
    public void toJson(final Object src, final Type typeOfSrc, final Appendable writer) throws JsonIOException {
        final JsonElement jsonElement = this.toJsonTree(src, typeOfSrc);
        this.toJson(jsonElement, writer);
    }
    
    public void toJson(final Object src, final Type typeOfSrc, final JsonWriter writer) throws JsonIOException {
        this.toJson(this.toJsonTree(src, typeOfSrc), writer);
    }
    
    public String toJson(final JsonElement jsonElement) {
        final StringWriter writer = new StringWriter();
        this.toJson(jsonElement, writer);
        return writer.toString();
    }
    
    public void toJson(final JsonElement jsonElement, final Appendable writer) throws JsonIOException {
        try {
            if (this.generateNonExecutableJson) {
                writer.append(")]}'\n");
            }
            final JsonWriter jsonWriter = new JsonWriter(Streams.writerForAppendable(writer));
            if (this.prettyPrinting) {
                jsonWriter.setIndent("  ");
            }
            this.toJson(jsonElement, jsonWriter);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void toJson(final JsonElement jsonElement, final JsonWriter writer) throws JsonIOException {
        final boolean oldLenient = writer.isLenient();
        writer.setLenient(true);
        final boolean oldHtmlSafe = writer.isHtmlSafe();
        writer.setHtmlSafe(this.htmlSafe);
        try {
            Streams.write(jsonElement, this.serializeNulls, writer);
        }
        catch (IOException e) {
            throw new JsonIOException(e);
        }
        finally {
            writer.setLenient(oldLenient);
            writer.setHtmlSafe(oldHtmlSafe);
        }
    }
    
    public <T> T fromJson(final String json, final Class<T> classOfT) throws JsonSyntaxException {
        final Object object = this.fromJson(json, (Type)classOfT);
        return Primitives.wrap(classOfT).cast(object);
    }
    
    public <T> T fromJson(final String json, final Type typeOfT) throws JsonSyntaxException {
        if (json == null) {
            return null;
        }
        final StringReader reader = new StringReader(json);
        final T target = this.fromJson(reader, typeOfT);
        return target;
    }
    
    public <T> T fromJson(final Reader json, final Class<T> classOfT) throws JsonSyntaxException, JsonIOException {
        final JsonReader jsonReader = new JsonReader(json);
        final Object object = this.fromJson(jsonReader, classOfT);
        assertFullConsumption(object, jsonReader);
        return Primitives.wrap(classOfT).cast(object);
    }
    
    public <T> T fromJson(final Reader json, final Type typeOfT) throws JsonIOException, JsonSyntaxException {
        final JsonReader jsonReader = new JsonReader(json);
        final T object = this.fromJson(jsonReader, typeOfT);
        assertFullConsumption(object, jsonReader);
        return object;
    }
    
    private static void assertFullConsumption(final Object obj, final JsonReader reader) {
        try {
            if (obj != null && reader.peek() != JsonToken.END_DOCUMENT) {
                throw new JsonIOException("JSON document was not fully consumed.");
            }
        }
        catch (MalformedJsonException e) {
            throw new JsonSyntaxException(e);
        }
        catch (IOException e2) {
            throw new JsonIOException(e2);
        }
    }
    
    public <T> T fromJson(final JsonReader reader, final Type typeOfT) throws JsonIOException, JsonSyntaxException {
        final boolean oldLenient = reader.isLenient();
        reader.setLenient(true);
        try {
            final JsonElement root = Streams.parse(reader);
            return this.fromJson(root, typeOfT);
        }
        finally {
            reader.setLenient(oldLenient);
        }
    }
    
    public <T> T fromJson(final JsonElement json, final Class<T> classOfT) throws JsonSyntaxException {
        final Object object = this.fromJson(json, (Type)classOfT);
        return Primitives.wrap(classOfT).cast(object);
    }
    
    public <T> T fromJson(final JsonElement json, final Type typeOfT) throws JsonSyntaxException {
        if (json == null) {
            return null;
        }
        final JsonDeserializationContext context = new JsonDeserializationContextDefault(new ObjectNavigator(this.deserializationExclusionStrategy), this.fieldNamingPolicy, this.deserializers, this.objectConstructor);
        final T target = context.deserialize(json, typeOfT);
        return target;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{").append("serializeNulls:").append(this.serializeNulls).append(",serializers:").append(this.serializers).append(",deserializers:").append(this.deserializers).append(",instanceCreators:").append(this.objectConstructor).append("}");
        return sb.toString();
    }
    
    static {
        DEFAULT_ANON_LOCAL_CLASS_EXCLUSION_STRATEGY = new AnonymousAndLocalClassExclusionStrategy();
        DEFAULT_SYNTHETIC_FIELD_EXCLUSION_STRATEGY = new SyntheticFieldExclusionStrategy(true);
        DEFAULT_MODIFIER_BASED_EXCLUSION_STRATEGY = new ModifierBasedExclusionStrategy(new int[] { 128, 8 });
        DEFAULT_NAMING_POLICY = new SerializedNameAnnotationInterceptingNamingPolicy(new JavaFieldNamingPolicy());
        DEFAULT_EXCLUSION_STRATEGY = createExclusionStrategy();
    }
}
