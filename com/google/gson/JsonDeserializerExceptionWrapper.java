// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;
import com.google.gson.internal.$Gson$Preconditions;

final class JsonDeserializerExceptionWrapper<T> implements JsonDeserializer<T>
{
    private final JsonDeserializer<T> delegate;
    
    JsonDeserializerExceptionWrapper(final JsonDeserializer<T> delegate) {
        this.delegate = $Gson$Preconditions.checkNotNull(delegate);
    }
    
    public T deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        try {
            return this.delegate.deserialize(json, typeOfT, context);
        }
        catch (JsonParseException e) {
            throw e;
        }
        catch (Exception e2) {
            final StringBuilder errorMsg = new StringBuilder().append("The JsonDeserializer ").append(this.delegate).append(" failed to deserialize json object ").append(json).append(" given the type ").append(typeOfT);
            throw new JsonParseException(errorMsg.toString(), e2);
        }
    }
    
    @Override
    public String toString() {
        return this.delegate.toString();
    }
}
