// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.lang.reflect.Type;

abstract class RecursiveFieldNamingPolicy implements FieldNamingStrategy2
{
    public final String translateName(final FieldAttributes f) {
        return this.translateName(f.getName(), f.getDeclaredType(), f.getAnnotations());
    }
    
    protected abstract String translateName(final String p0, final Type p1, final Collection<Annotation> p2);
}
