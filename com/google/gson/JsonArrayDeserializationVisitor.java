// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Array;
import com.google.gson.internal.$Gson$Types;
import java.lang.reflect.Type;

final class JsonArrayDeserializationVisitor<T> extends JsonDeserializationVisitor<T>
{
    JsonArrayDeserializationVisitor(final JsonArray jsonArray, final Type arrayType, final ObjectNavigator objectNavigator, final FieldNamingStrategy2 fieldNamingPolicy, final ObjectConstructor objectConstructor, final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers, final JsonDeserializationContext context) {
        super(jsonArray, arrayType, objectNavigator, fieldNamingPolicy, objectConstructor, deserializers, context);
    }
    
    @Override
    protected T constructTarget() {
        if (!this.json.isJsonArray()) {
            throw new JsonParseException("Expecting array found: " + this.json);
        }
        final JsonArray jsonArray = this.json.getAsJsonArray();
        if ($Gson$Types.isArray(this.targetType)) {
            return (T)this.objectConstructor.constructArray($Gson$Types.getArrayComponentType(this.targetType), jsonArray.size());
        }
        return this.objectConstructor.construct($Gson$Types.getRawType(this.targetType));
    }
    
    public void visitArray(final Object array, final Type arrayType) {
        if (!this.json.isJsonArray()) {
            throw new JsonParseException("Expecting array found: " + this.json);
        }
        final JsonArray jsonArray = this.json.getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); ++i) {
            final JsonElement jsonChild = jsonArray.get(i);
            Object child;
            if (jsonChild == null || jsonChild.isJsonNull()) {
                child = null;
            }
            else if (jsonChild instanceof JsonObject) {
                child = this.visitChildAsObject($Gson$Types.getArrayComponentType(arrayType), jsonChild);
            }
            else if (jsonChild instanceof JsonArray) {
                child = this.visitChildAsArray($Gson$Types.getArrayComponentType(arrayType), jsonChild.getAsJsonArray());
            }
            else {
                if (!(jsonChild instanceof JsonPrimitive)) {
                    throw new IllegalStateException();
                }
                child = this.visitChildAsObject($Gson$Types.getArrayComponentType(arrayType), jsonChild.getAsJsonPrimitive());
            }
            Array.set(array, i, child);
        }
    }
    
    public void startVisitingObject(final Object node) {
        throw new JsonParseException("Expecting array but found object: " + node);
    }
    
    public void visitArrayField(final FieldAttributes f, final Type typeOfF, final Object obj) {
        throw new JsonParseException("Expecting array but found array field " + f.getName() + ": " + obj);
    }
    
    public void visitObjectField(final FieldAttributes f, final Type typeOfF, final Object obj) {
        throw new JsonParseException("Expecting array but found object field " + f.getName() + ": " + obj);
    }
    
    public boolean visitFieldUsingCustomHandler(final FieldAttributes f, final Type actualTypeOfField, final Object parent) {
        throw new JsonParseException("Expecting array but found field " + f.getName() + ": " + parent);
    }
    
    public void visitPrimitive(final Object primitive) {
        throw new JsonParseException("Type information is unavailable, and the target is not a primitive: " + this.json);
    }
}
