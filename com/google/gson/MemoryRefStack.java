// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Iterator;
import com.google.gson.internal.$Gson$Preconditions;
import java.util.Stack;

final class MemoryRefStack
{
    private final Stack<ObjectTypePair> stack;
    
    MemoryRefStack() {
        this.stack = new Stack<ObjectTypePair>();
    }
    
    public ObjectTypePair push(final ObjectTypePair obj) {
        $Gson$Preconditions.checkNotNull(obj);
        return this.stack.push(obj);
    }
    
    public ObjectTypePair pop() {
        return this.stack.pop();
    }
    
    public boolean isEmpty() {
        return this.stack.isEmpty();
    }
    
    public ObjectTypePair peek() {
        return this.stack.peek();
    }
    
    public boolean contains(final ObjectTypePair obj) {
        if (obj == null) {
            return false;
        }
        for (final ObjectTypePair stackObject : this.stack) {
            if (stackObject.getObject() == obj.getObject() && stackObject.type.equals(obj.type)) {
                return true;
            }
        }
        return false;
    }
}
