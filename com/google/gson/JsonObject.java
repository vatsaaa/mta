// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import com.google.gson.internal.$Gson$Preconditions;
import java.util.LinkedHashMap;
import java.util.Map;

public final class JsonObject extends JsonElement
{
    private final Map<String, JsonElement> members;
    
    public JsonObject() {
        this.members = new LinkedHashMap<String, JsonElement>();
    }
    
    public void add(final String property, JsonElement value) {
        if (value == null) {
            value = JsonNull.createJsonNull();
        }
        this.members.put($Gson$Preconditions.checkNotNull(property), value);
    }
    
    public JsonElement remove(final String property) {
        return this.members.remove(property);
    }
    
    public void addProperty(final String property, final String value) {
        this.add(property, this.createJsonElement(value));
    }
    
    public void addProperty(final String property, final Number value) {
        this.add(property, this.createJsonElement(value));
    }
    
    public void addProperty(final String property, final Boolean value) {
        this.add(property, this.createJsonElement(value));
    }
    
    public void addProperty(final String property, final Character value) {
        this.add(property, this.createJsonElement(value));
    }
    
    private JsonElement createJsonElement(final Object value) {
        return (value == null) ? JsonNull.createJsonNull() : new JsonPrimitive(value);
    }
    
    public Set<Map.Entry<String, JsonElement>> entrySet() {
        return this.members.entrySet();
    }
    
    public boolean has(final String memberName) {
        return this.members.containsKey(memberName);
    }
    
    public JsonElement get(final String memberName) {
        if (this.members.containsKey(memberName)) {
            final JsonElement member = this.members.get(memberName);
            return (member == null) ? JsonNull.createJsonNull() : member;
        }
        return null;
    }
    
    public JsonPrimitive getAsJsonPrimitive(final String memberName) {
        return this.members.get(memberName);
    }
    
    public JsonArray getAsJsonArray(final String memberName) {
        return this.members.get(memberName);
    }
    
    public JsonObject getAsJsonObject(final String memberName) {
        return this.members.get(memberName);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof JsonObject && ((JsonObject)o).members.equals(this.members));
    }
    
    @Override
    public int hashCode() {
        return this.members.hashCode();
    }
    
    @Override
    protected void toString(final Appendable sb, final Escaper escaper) throws IOException {
        sb.append('{');
        boolean first = true;
        for (final Map.Entry<String, JsonElement> entry : this.members.entrySet()) {
            if (first) {
                first = false;
            }
            else {
                sb.append(',');
            }
            sb.append('\"');
            sb.append(escaper.escapeJsonString(entry.getKey()));
            sb.append("\":");
            entry.getValue().toString(sb, escaper);
        }
        sb.append('}');
    }
}
