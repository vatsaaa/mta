// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;

final class ObjectTypePair
{
    private Object obj;
    final Type type;
    private final boolean preserveType;
    
    ObjectTypePair(final Object obj, final Type type, final boolean preserveType) {
        this.obj = obj;
        this.type = type;
        this.preserveType = preserveType;
    }
    
    Object getObject() {
        return this.obj;
    }
    
    void setObject(final Object obj) {
        this.obj = obj;
    }
    
    Type getType() {
        return this.type;
    }
    
    @Override
    public String toString() {
        return String.format("preserveType: %b, type: %s, obj: %s", this.preserveType, this.type, this.obj);
    }
    
     <HANDLER> Pair<HANDLER, ObjectTypePair> getMatchingHandler(final ParameterizedTypeHandlerMap<HANDLER> handlers) {
        HANDLER handler = null;
        if (!this.preserveType && this.obj != null) {
            final ObjectTypePair moreSpecificType = this.toMoreSpecificType();
            handler = handlers.getHandlerFor(moreSpecificType.type);
            if (handler != null) {
                return new Pair<HANDLER, ObjectTypePair>(handler, moreSpecificType);
            }
        }
        handler = handlers.getHandlerFor(this.type);
        return (handler == null) ? null : new Pair<HANDLER, ObjectTypePair>(handler, this);
    }
    
    ObjectTypePair toMoreSpecificType() {
        if (this.preserveType || this.obj == null) {
            return this;
        }
        final Type actualType = getActualTypeIfMoreSpecific(this.type, this.obj.getClass());
        if (actualType == this.type) {
            return this;
        }
        return new ObjectTypePair(this.obj, actualType, this.preserveType);
    }
    
    Type getMoreSpecificType() {
        if (this.preserveType || this.obj == null) {
            return this.type;
        }
        return getActualTypeIfMoreSpecific(this.type, this.obj.getClass());
    }
    
    static Type getActualTypeIfMoreSpecific(Type type, final Class<?> actualClass) {
        if (type instanceof Class) {
            final Class<?> typeAsClass = (Class<?>)type;
            if (typeAsClass.isAssignableFrom(actualClass)) {
                type = actualClass;
            }
            if (type == Object.class) {
                type = actualClass;
            }
        }
        return type;
    }
    
    @Override
    public int hashCode() {
        return (this.obj == null) ? 31 : this.obj.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ObjectTypePair other = (ObjectTypePair)obj;
        if (this.obj == null) {
            if (other.obj != null) {
                return false;
            }
        }
        else if (this.obj != other.obj) {
            return false;
        }
        if (this.type == null) {
            if (other.type != null) {
                return false;
            }
        }
        else if (!this.type.equals(other.type)) {
            return false;
        }
        return this.preserveType == other.preserveType;
    }
    
    public boolean isPreserveType() {
        return this.preserveType;
    }
}
