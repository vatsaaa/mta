// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.annotations.Expose;

final class ExposeAnnotationSerializationExclusionStrategy implements ExclusionStrategy
{
    public boolean shouldSkipClass(final Class<?> clazz) {
        return false;
    }
    
    public boolean shouldSkipField(final FieldAttributes f) {
        final Expose annotation = f.getAnnotation(Expose.class);
        return annotation == null || !annotation.serialize();
    }
}
