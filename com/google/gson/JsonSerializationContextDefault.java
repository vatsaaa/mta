// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;

final class JsonSerializationContextDefault implements JsonSerializationContext
{
    private final ObjectNavigator objectNavigator;
    private final FieldNamingStrategy2 fieldNamingPolicy;
    private final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers;
    private final boolean serializeNulls;
    private final MemoryRefStack ancestors;
    
    JsonSerializationContextDefault(final ObjectNavigator objectNavigator, final FieldNamingStrategy2 fieldNamingPolicy, final boolean serializeNulls, final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers) {
        this.objectNavigator = objectNavigator;
        this.fieldNamingPolicy = fieldNamingPolicy;
        this.serializeNulls = serializeNulls;
        this.serializers = serializers;
        this.ancestors = new MemoryRefStack();
    }
    
    public JsonElement serialize(final Object src) {
        if (src == null) {
            return JsonNull.createJsonNull();
        }
        return this.serialize(src, src.getClass(), false);
    }
    
    public JsonElement serialize(final Object src, final Type typeOfSrc) {
        return this.serialize(src, typeOfSrc, true);
    }
    
    JsonElement serialize(final Object src, final Type typeOfSrc, final boolean preserveType) {
        if (src == null) {
            return JsonNull.createJsonNull();
        }
        final JsonSerializationVisitor visitor = new JsonSerializationVisitor(this.objectNavigator, this.fieldNamingPolicy, this.serializeNulls, this.serializers, this, this.ancestors);
        this.objectNavigator.accept(new ObjectTypePair(src, typeOfSrc, preserveType), visitor);
        return visitor.getJsonElement();
    }
}
