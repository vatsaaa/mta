// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

public enum FieldNamingPolicy
{
    UPPER_CAMEL_CASE((FieldNamingStrategy2)new ModifyFirstLetterNamingPolicy(ModifyFirstLetterNamingPolicy.LetterModifier.UPPER)), 
    UPPER_CAMEL_CASE_WITH_SPACES((FieldNamingStrategy2)new UpperCamelCaseSeparatorNamingPolicy(" ")), 
    LOWER_CASE_WITH_UNDERSCORES((FieldNamingStrategy2)new LowerCamelCaseSeparatorNamingPolicy("_")), 
    LOWER_CASE_WITH_DASHES((FieldNamingStrategy2)new LowerCamelCaseSeparatorNamingPolicy("-"));
    
    private final FieldNamingStrategy2 namingPolicy;
    
    private FieldNamingPolicy(final FieldNamingStrategy2 namingPolicy) {
        this.namingPolicy = namingPolicy;
    }
    
    FieldNamingStrategy2 getFieldNamingPolicy() {
        return this.namingPolicy;
    }
}
