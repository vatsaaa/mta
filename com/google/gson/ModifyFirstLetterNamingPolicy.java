// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.lang.reflect.Type;
import com.google.gson.internal.$Gson$Preconditions;

final class ModifyFirstLetterNamingPolicy extends RecursiveFieldNamingPolicy
{
    private final LetterModifier letterModifier;
    
    ModifyFirstLetterNamingPolicy(final LetterModifier modifier) {
        this.letterModifier = $Gson$Preconditions.checkNotNull(modifier);
    }
    
    @Override
    protected String translateName(final String target, final Type fieldType, final Collection<Annotation> annotations) {
        final StringBuilder fieldNameBuilder = new StringBuilder();
        int index;
        char firstCharacter;
        for (index = 0, firstCharacter = target.charAt(index); index < target.length() - 1 && !Character.isLetter(firstCharacter); firstCharacter = target.charAt(++index)) {
            fieldNameBuilder.append(firstCharacter);
        }
        if (index == target.length()) {
            return fieldNameBuilder.toString();
        }
        final boolean capitalizeFirstLetter = this.letterModifier == LetterModifier.UPPER;
        if (capitalizeFirstLetter && !Character.isUpperCase(firstCharacter)) {
            final String modifiedTarget = this.modifyString(Character.toUpperCase(firstCharacter), target, ++index);
            return fieldNameBuilder.append(modifiedTarget).toString();
        }
        if (!capitalizeFirstLetter && Character.isUpperCase(firstCharacter)) {
            final String modifiedTarget = this.modifyString(Character.toLowerCase(firstCharacter), target, ++index);
            return fieldNameBuilder.append(modifiedTarget).toString();
        }
        return target;
    }
    
    private String modifyString(final char firstCharacter, final String srcString, final int indexOfSubstring) {
        return (indexOfSubstring < srcString.length()) ? (firstCharacter + srcString.substring(indexOfSubstring)) : String.valueOf(firstCharacter);
    }
    
    public enum LetterModifier
    {
        UPPER, 
        LOWER;
    }
}
