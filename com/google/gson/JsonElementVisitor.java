// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.io.IOException;

interface JsonElementVisitor
{
    void visitPrimitive(final JsonPrimitive p0) throws IOException;
    
    void visitNull() throws IOException;
    
    void startArray(final JsonArray p0) throws IOException;
    
    void visitArrayMember(final JsonArray p0, final JsonPrimitive p1, final boolean p2) throws IOException;
    
    void visitArrayMember(final JsonArray p0, final JsonArray p1, final boolean p2) throws IOException;
    
    void visitArrayMember(final JsonArray p0, final JsonObject p1, final boolean p2) throws IOException;
    
    void visitNullArrayMember(final JsonArray p0, final boolean p1) throws IOException;
    
    void endArray(final JsonArray p0) throws IOException;
    
    void startObject(final JsonObject p0) throws IOException;
    
    void visitObjectMember(final JsonObject p0, final String p1, final JsonPrimitive p2, final boolean p3) throws IOException;
    
    void visitObjectMember(final JsonObject p0, final String p1, final JsonArray p2, final boolean p3) throws IOException;
    
    void visitObjectMember(final JsonObject p0, final String p1, final JsonObject p2, final boolean p3) throws IOException;
    
    void visitNullObjectMember(final JsonObject p0, final String p1, final boolean p2) throws IOException;
    
    void endObject(final JsonObject p0) throws IOException;
}
