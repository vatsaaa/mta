// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Collections;
import java.util.HashSet;
import java.io.IOException;
import java.util.Set;

final class Escaper
{
    private static final char[] HEX_CHARS;
    private static final Set<Character> JS_ESCAPE_CHARS;
    private static final Set<Character> HTML_ESCAPE_CHARS;
    private final boolean escapeHtmlCharacters;
    
    Escaper(final boolean escapeHtmlCharacters) {
        this.escapeHtmlCharacters = escapeHtmlCharacters;
    }
    
    public String escapeJsonString(final CharSequence plainText) {
        final StringBuilder escapedString = new StringBuilder(plainText.length() + 20);
        try {
            this.escapeJsonString(plainText, escapedString);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return escapedString.toString();
    }
    
    private void escapeJsonString(final CharSequence plainText, final StringBuilder out) throws IOException {
        int pos = 0;
        final int len = plainText.length();
        int charCount;
        for (int i = 0; i < len; i += charCount) {
            final int codePoint = Character.codePointAt(plainText, i);
            charCount = Character.charCount(codePoint);
            if (isControlCharacter(codePoint) || this.mustEscapeCharInJsString(codePoint)) {
                out.append(plainText, pos, i);
                pos = i + charCount;
                switch (codePoint) {
                    case 8: {
                        out.append("\\b");
                        break;
                    }
                    case 9: {
                        out.append("\\t");
                        break;
                    }
                    case 10: {
                        out.append("\\n");
                        break;
                    }
                    case 12: {
                        out.append("\\f");
                        break;
                    }
                    case 13: {
                        out.append("\\r");
                        break;
                    }
                    case 92: {
                        out.append("\\\\");
                        break;
                    }
                    case 47: {
                        out.append("\\/");
                        break;
                    }
                    case 34: {
                        out.append("\\\"");
                        break;
                    }
                    default: {
                        appendHexJavaScriptRepresentation(codePoint, out);
                        break;
                    }
                }
            }
        }
        out.append(plainText, pos, len);
    }
    
    private boolean mustEscapeCharInJsString(final int codepoint) {
        if (!Character.isSupplementaryCodePoint(codepoint)) {
            final char c = (char)codepoint;
            return Escaper.JS_ESCAPE_CHARS.contains(c) || (this.escapeHtmlCharacters && Escaper.HTML_ESCAPE_CHARS.contains(c));
        }
        return false;
    }
    
    private static boolean isControlCharacter(final int codePoint) {
        return codePoint < 32 || codePoint == 8232 || codePoint == 8233 || (codePoint >= 127 && codePoint <= 159);
    }
    
    private static void appendHexJavaScriptRepresentation(final int codePoint, final Appendable out) throws IOException {
        if (Character.isSupplementaryCodePoint(codePoint)) {
            final char[] surrogates = Character.toChars(codePoint);
            appendHexJavaScriptRepresentation(surrogates[0], out);
            appendHexJavaScriptRepresentation(surrogates[1], out);
            return;
        }
        out.append("\\u").append(Escaper.HEX_CHARS[codePoint >>> 12 & 0xF]).append(Escaper.HEX_CHARS[codePoint >>> 8 & 0xF]).append(Escaper.HEX_CHARS[codePoint >>> 4 & 0xF]).append(Escaper.HEX_CHARS[codePoint & 0xF]);
    }
    
    static {
        HEX_CHARS = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        final Set<Character> mandatoryEscapeSet = new HashSet<Character>();
        mandatoryEscapeSet.add('\"');
        mandatoryEscapeSet.add('\\');
        JS_ESCAPE_CHARS = Collections.unmodifiableSet((Set<? extends Character>)mandatoryEscapeSet);
        final Set<Character> htmlEscapeSet = new HashSet<Character>();
        htmlEscapeSet.add('<');
        htmlEscapeSet.add('>');
        htmlEscapeSet.add('&');
        htmlEscapeSet.add('=');
        htmlEscapeSet.add('\'');
        HTML_ESCAPE_CHARS = Collections.unmodifiableSet((Set<? extends Character>)htmlEscapeSet);
    }
}
