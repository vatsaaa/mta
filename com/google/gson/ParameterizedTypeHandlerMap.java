// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Collection;
import com.google.gson.internal.$Gson$Types;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Logger;

final class ParameterizedTypeHandlerMap<T>
{
    private static final Logger logger;
    private final Map<Type, T> map;
    private final List<Pair<Class<?>, T>> typeHierarchyList;
    private boolean modifiable;
    
    ParameterizedTypeHandlerMap() {
        this.map = new HashMap<Type, T>();
        this.typeHierarchyList = new ArrayList<Pair<Class<?>, T>>();
        this.modifiable = true;
    }
    
    public synchronized void registerForTypeHierarchy(final Class<?> typeOfT, final T value) {
        final Pair<Class<?>, T> pair = new Pair<Class<?>, T>(typeOfT, value);
        this.registerForTypeHierarchy(pair);
    }
    
    public synchronized void registerForTypeHierarchy(final Pair<Class<?>, T> pair) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        int index = this.getIndexOfSpecificHandlerForTypeHierarchy(pair.first);
        if (index >= 0) {
            ParameterizedTypeHandlerMap.logger.log(Level.WARNING, "Overriding the existing type handler for {0}", pair.first);
            this.typeHierarchyList.remove(index);
        }
        index = this.getIndexOfAnOverriddenHandler(pair.first);
        if (index >= 0) {
            throw new IllegalArgumentException("The specified type handler for type " + pair.first + " hides the previously registered type hierarchy handler for " + this.typeHierarchyList.get(index).first + ". Gson does not allow this.");
        }
        this.typeHierarchyList.add(0, pair);
    }
    
    private int getIndexOfAnOverriddenHandler(final Class<?> type) {
        for (int i = this.typeHierarchyList.size() - 1; i >= 0; --i) {
            final Pair<Class<?>, T> entry = this.typeHierarchyList.get(i);
            if (type.isAssignableFrom(entry.first)) {
                return i;
            }
        }
        return -1;
    }
    
    public synchronized void register(final Type typeOfT, final T value) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        if (this.hasSpecificHandlerFor(typeOfT)) {
            ParameterizedTypeHandlerMap.logger.log(Level.WARNING, "Overriding the existing type handler for {0}", typeOfT);
        }
        this.map.put(typeOfT, value);
    }
    
    public synchronized void registerIfAbsent(final ParameterizedTypeHandlerMap<T> other) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        for (final Map.Entry<Type, T> entry : other.map.entrySet()) {
            if (!this.map.containsKey(entry.getKey())) {
                this.register(entry.getKey(), entry.getValue());
            }
        }
        for (int i = other.typeHierarchyList.size() - 1; i >= 0; --i) {
            final Pair<Class<?>, T> entry2 = other.typeHierarchyList.get(i);
            final int index = this.getIndexOfSpecificHandlerForTypeHierarchy(entry2.first);
            if (index < 0) {
                this.registerForTypeHierarchy(entry2);
            }
        }
    }
    
    public synchronized void register(final ParameterizedTypeHandlerMap<T> other) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        for (final Map.Entry<Type, T> entry : other.map.entrySet()) {
            this.register(entry.getKey(), entry.getValue());
        }
        for (int i = other.typeHierarchyList.size() - 1; i >= 0; --i) {
            final Pair<Class<?>, T> entry2 = other.typeHierarchyList.get(i);
            this.registerForTypeHierarchy(entry2);
        }
    }
    
    public synchronized void registerIfAbsent(final Type typeOfT, final T value) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        if (!this.map.containsKey(typeOfT)) {
            this.register(typeOfT, value);
        }
    }
    
    public synchronized void makeUnmodifiable() {
        this.modifiable = false;
    }
    
    public synchronized T getHandlerFor(final Type type) {
        T handler = this.map.get(type);
        if (handler == null) {
            final Class<?> rawClass = $Gson$Types.getRawType(type);
            if (rawClass != type) {
                handler = this.getHandlerFor(rawClass);
            }
            if (handler == null) {
                handler = this.getHandlerForTypeHierarchy(rawClass);
            }
        }
        return handler;
    }
    
    private T getHandlerForTypeHierarchy(final Class<?> type) {
        for (final Pair<Class<?>, T> entry : this.typeHierarchyList) {
            if (entry.first.isAssignableFrom(type)) {
                return entry.second;
            }
        }
        return null;
    }
    
    public synchronized boolean hasSpecificHandlerFor(final Type type) {
        return this.map.containsKey(type);
    }
    
    private synchronized int getIndexOfSpecificHandlerForTypeHierarchy(final Class<?> type) {
        for (int i = this.typeHierarchyList.size() - 1; i >= 0; --i) {
            if (type.equals(this.typeHierarchyList.get(i).first)) {
                return i;
            }
        }
        return -1;
    }
    
    public synchronized ParameterizedTypeHandlerMap<T> copyOf() {
        final ParameterizedTypeHandlerMap<T> copy = new ParameterizedTypeHandlerMap<T>();
        copy.map.putAll((Map<? extends Type, ? extends T>)this.map);
        copy.typeHierarchyList.addAll(this.typeHierarchyList);
        return copy;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{mapForTypeHierarchy:{");
        boolean first = true;
        for (final Pair<Class<?>, T> entry : this.typeHierarchyList) {
            if (first) {
                first = false;
            }
            else {
                sb.append(',');
            }
            sb.append(this.typeToString(entry.first)).append(':');
            sb.append(entry.second);
        }
        sb.append("},map:{");
        first = true;
        for (final Map.Entry<Type, T> entry2 : this.map.entrySet()) {
            if (first) {
                first = false;
            }
            else {
                sb.append(',');
            }
            sb.append(this.typeToString(entry2.getKey())).append(':');
            sb.append(entry2.getValue());
        }
        sb.append("}");
        return sb.toString();
    }
    
    private String typeToString(final Type type) {
        return $Gson$Types.getRawType(type).getSimpleName();
    }
    
    static {
        logger = Logger.getLogger(ParameterizedTypeHandlerMap.class.getName());
    }
}
