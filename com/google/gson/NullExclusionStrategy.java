// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

final class NullExclusionStrategy implements ExclusionStrategy
{
    public boolean shouldSkipField(final FieldAttributes f) {
        return false;
    }
    
    public boolean shouldSkipClass(final Class<?> clazz) {
        return false;
    }
}
