// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

final class UpperCamelCaseSeparatorNamingPolicy extends CompositionFieldNamingPolicy
{
    public UpperCamelCaseSeparatorNamingPolicy(final String separatorString) {
        super(new RecursiveFieldNamingPolicy[] { new CamelCaseSeparatorNamingPolicy(separatorString), new ModifyFirstLetterNamingPolicy(ModifyFirstLetterNamingPolicy.LetterModifier.UPPER) });
    }
}
