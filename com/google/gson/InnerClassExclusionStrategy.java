// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

final class InnerClassExclusionStrategy implements ExclusionStrategy
{
    public boolean shouldSkipField(final FieldAttributes f) {
        return this.isInnerClass(f.getDeclaredClass());
    }
    
    public boolean shouldSkipClass(final Class<?> clazz) {
        return this.isInnerClass(clazz);
    }
    
    private boolean isInnerClass(final Class<?> clazz) {
        return clazz.isMemberClass() && !this.isStatic(clazz);
    }
    
    private boolean isStatic(final Class<?> clazz) {
        return (clazz.getModifiers() & 0x8) != 0x0;
    }
}
