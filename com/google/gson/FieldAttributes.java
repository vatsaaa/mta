// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.internal.$Gson$Types;
import java.util.Iterator;
import java.util.Collections;
import java.util.Arrays;
import com.google.gson.internal.$Gson$Preconditions;
import java.lang.reflect.Type;
import java.lang.reflect.Field;
import java.lang.annotation.Annotation;
import java.util.Collection;

public final class FieldAttributes
{
    private static final String MAX_CACHE_PROPERTY_NAME = "com.google.gson.annotation_cache_size_hint";
    private static final Cache<Pair<Class<?>, String>, Collection<Annotation>> ANNOTATION_CACHE;
    private final Class<?> declaringClazz;
    private final Field field;
    private final Class<?> declaredType;
    private final boolean isSynthetic;
    private final int modifiers;
    private final String name;
    private final Type resolvedType;
    private Type genericType;
    private Collection<Annotation> annotations;
    
    FieldAttributes(final Class<?> declaringClazz, final Field f, final Type declaringType) {
        this.declaringClazz = $Gson$Preconditions.checkNotNull(declaringClazz);
        this.name = f.getName();
        this.declaredType = f.getType();
        this.isSynthetic = f.isSynthetic();
        this.modifiers = f.getModifiers();
        this.field = f;
        this.resolvedType = getTypeInfoForField(f, declaringType);
    }
    
    private static int getMaxCacheSize() {
        final int defaultMaxCacheSize = 2000;
        try {
            final String propertyValue = System.getProperty("com.google.gson.annotation_cache_size_hint", String.valueOf(2000));
            return Integer.parseInt(propertyValue);
        }
        catch (NumberFormatException e) {
            return 2000;
        }
    }
    
    public Class<?> getDeclaringClass() {
        return this.declaringClazz;
    }
    
    public String getName() {
        return this.name;
    }
    
    public Type getDeclaredType() {
        if (this.genericType == null) {
            this.genericType = this.field.getGenericType();
        }
        return this.genericType;
    }
    
    public Class<?> getDeclaredClass() {
        return this.declaredType;
    }
    
    public <T extends Annotation> T getAnnotation(final Class<T> annotation) {
        return getAnnotationFromArray(this.getAnnotations(), annotation);
    }
    
    public Collection<Annotation> getAnnotations() {
        if (this.annotations == null) {
            final Pair<Class<?>, String> key = new Pair<Class<?>, String>(this.declaringClazz, this.name);
            this.annotations = FieldAttributes.ANNOTATION_CACHE.getElement(key);
            if (this.annotations == null) {
                this.annotations = Collections.unmodifiableCollection((Collection<? extends Annotation>)Arrays.asList(this.field.getAnnotations()));
                FieldAttributes.ANNOTATION_CACHE.addElement(key, this.annotations);
            }
        }
        return this.annotations;
    }
    
    public boolean hasModifier(final int modifier) {
        return (this.modifiers & modifier) != 0x0;
    }
    
    void set(final Object instance, final Object value) throws IllegalAccessException {
        this.field.set(instance, value);
    }
    
    Object get(final Object instance) throws IllegalAccessException {
        return this.field.get(instance);
    }
    
    boolean isSynthetic() {
        return this.isSynthetic;
    }
    
    @Deprecated
    Field getFieldObject() {
        return this.field;
    }
    
    Type getResolvedType() {
        return this.resolvedType;
    }
    
    private static <T extends Annotation> T getAnnotationFromArray(final Collection<Annotation> annotations, final Class<T> annotation) {
        for (final Annotation a : annotations) {
            if (a.annotationType() == annotation) {
                return (T)a;
            }
        }
        return null;
    }
    
    static Type getTypeInfoForField(final Field f, final Type typeDefiningF) {
        final Class<?> rawType = $Gson$Types.getRawType(typeDefiningF);
        if (!f.getDeclaringClass().isAssignableFrom(rawType)) {
            return f.getGenericType();
        }
        return $Gson$Types.resolve(typeDefiningF, rawType, f.getGenericType());
    }
    
    static {
        ANNOTATION_CACHE = new LruCache<Pair<Class<?>, String>, Collection<Annotation>>(getMaxCacheSize());
    }
}
