// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;
import java.util.Map;

abstract class BaseMapTypeAdapter implements JsonSerializer<Map<?, ?>>, JsonDeserializer<Map<?, ?>>
{
    protected static final JsonElement serialize(final JsonSerializationContext context, final Object src, final Type srcType) {
        final JsonSerializationContextDefault contextImpl = (JsonSerializationContextDefault)context;
        return contextImpl.serialize(src, srcType, false);
    }
    
    protected static final Map<Object, Object> constructMapType(final Type mapType, final JsonDeserializationContext context) {
        final JsonDeserializationContextDefault contextImpl = (JsonDeserializationContextDefault)context;
        final ObjectConstructor objectConstructor = contextImpl.getObjectConstructor();
        return objectConstructor.construct(mapType);
    }
}
