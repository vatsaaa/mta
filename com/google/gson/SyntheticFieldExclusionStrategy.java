// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

final class SyntheticFieldExclusionStrategy implements ExclusionStrategy
{
    private final boolean skipSyntheticFields;
    
    SyntheticFieldExclusionStrategy(final boolean skipSyntheticFields) {
        this.skipSyntheticFields = skipSyntheticFields;
    }
    
    public boolean shouldSkipClass(final Class<?> clazz) {
        return false;
    }
    
    public boolean shouldSkipField(final FieldAttributes f) {
        return this.skipSyntheticFields && f.isSynthetic();
    }
}
