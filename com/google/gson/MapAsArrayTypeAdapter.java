// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.lang.reflect.Type;
import java.util.Map;

final class MapAsArrayTypeAdapter extends BaseMapTypeAdapter implements JsonSerializer<Map<?, ?>>, JsonDeserializer<Map<?, ?>>
{
    public Map<?, ?> deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        final Map<Object, Object> result = BaseMapTypeAdapter.constructMapType(typeOfT, context);
        final Type[] keyAndValueType = this.typeToTypeArguments(typeOfT);
        if (json.isJsonArray()) {
            final JsonArray array = json.getAsJsonArray();
            for (int i = 0; i < array.size(); ++i) {
                final JsonArray entryArray = array.get(i).getAsJsonArray();
                final Object k = context.deserialize(entryArray.get(0), keyAndValueType[0]);
                final Object v = context.deserialize(entryArray.get(1), keyAndValueType[1]);
                result.put(k, v);
            }
            this.checkSize(array, array.size(), result, result.size());
        }
        else {
            final JsonObject object = json.getAsJsonObject();
            for (final Map.Entry<String, JsonElement> entry : object.entrySet()) {
                final Object k = context.deserialize(new JsonPrimitive(entry.getKey()), keyAndValueType[0]);
                final Object v = context.deserialize(entry.getValue(), keyAndValueType[1]);
                result.put(k, v);
            }
            this.checkSize(object, object.entrySet().size(), result, result.size());
        }
        return result;
    }
    
    public JsonElement serialize(final Map<?, ?> src, final Type typeOfSrc, final JsonSerializationContext context) {
        final Type[] keyAndValueType = this.typeToTypeArguments(typeOfSrc);
        boolean serializeAsArray = false;
        final List<JsonElement> keysAndValues = new ArrayList<JsonElement>();
        for (final Map.Entry<?, ?> entry : src.entrySet()) {
            final JsonElement key = BaseMapTypeAdapter.serialize(context, entry.getKey(), keyAndValueType[0]);
            serializeAsArray |= (key.isJsonObject() || key.isJsonArray());
            keysAndValues.add(key);
            keysAndValues.add(BaseMapTypeAdapter.serialize(context, entry.getValue(), keyAndValueType[1]));
        }
        if (serializeAsArray) {
            final JsonArray result = new JsonArray();
            for (int i = 0; i < keysAndValues.size(); i += 2) {
                final JsonArray entryArray = new JsonArray();
                entryArray.add(keysAndValues.get(i));
                entryArray.add(keysAndValues.get(i + 1));
                result.add(entryArray);
            }
            return result;
        }
        final JsonObject result2 = new JsonObject();
        for (int i = 0; i < keysAndValues.size(); i += 2) {
            result2.add(keysAndValues.get(i).getAsString(), keysAndValues.get(i + 1));
        }
        this.checkSize(src, src.size(), result2, result2.entrySet().size());
        return result2;
    }
    
    private Type[] typeToTypeArguments(final Type typeOfT) {
        if (!(typeOfT instanceof ParameterizedType)) {
            return new Type[] { Object.class, Object.class };
        }
        final Type[] typeArguments = ((ParameterizedType)typeOfT).getActualTypeArguments();
        if (typeArguments.length != 2) {
            throw new IllegalArgumentException("MapAsArrayTypeAdapter cannot handle " + typeOfT);
        }
        return typeArguments;
    }
    
    private void checkSize(final Object input, final int inputSize, final Object output, final int outputSize) {
        if (inputSize != outputSize) {
            throw new JsonSyntaxException("Input size " + inputSize + " != output size " + outputSize + " for input " + input + " and output " + output);
        }
    }
}
