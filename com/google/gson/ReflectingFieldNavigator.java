// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Field;
import java.lang.reflect.AccessibleObject;
import java.util.ArrayList;
import java.util.Iterator;
import com.google.gson.internal.$Gson$Types;
import com.google.gson.internal.$Gson$Preconditions;
import java.util.List;
import java.lang.reflect.Type;

final class ReflectingFieldNavigator
{
    private static final Cache<Type, List<FieldAttributes>> fieldsCache;
    private final ExclusionStrategy exclusionStrategy;
    
    ReflectingFieldNavigator(final ExclusionStrategy exclusionStrategy) {
        this.exclusionStrategy = $Gson$Preconditions.checkNotNull(exclusionStrategy);
    }
    
    void visitFieldsReflectively(final ObjectTypePair objTypePair, final ObjectNavigator.Visitor visitor) {
        final Type moreSpecificType = objTypePair.getMoreSpecificType();
        final Object obj = objTypePair.getObject();
        for (final FieldAttributes fieldAttributes : this.getAllFields(moreSpecificType, objTypePair.getType())) {
            if (!this.exclusionStrategy.shouldSkipField(fieldAttributes)) {
                if (this.exclusionStrategy.shouldSkipClass(fieldAttributes.getDeclaredClass())) {
                    continue;
                }
                final Type resolvedTypeOfField = fieldAttributes.getResolvedType();
                final boolean visitedWithCustomHandler = visitor.visitFieldUsingCustomHandler(fieldAttributes, resolvedTypeOfField, obj);
                if (visitedWithCustomHandler) {
                    continue;
                }
                if ($Gson$Types.isArray(resolvedTypeOfField)) {
                    visitor.visitArrayField(fieldAttributes, resolvedTypeOfField, obj);
                }
                else {
                    visitor.visitObjectField(fieldAttributes, resolvedTypeOfField, obj);
                }
            }
        }
    }
    
    private List<FieldAttributes> getAllFields(final Type type, final Type declaredType) {
        List<FieldAttributes> fields = ReflectingFieldNavigator.fieldsCache.getElement(type);
        if (fields == null) {
            fields = new ArrayList<FieldAttributes>();
            for (final Class<?> curr : this.getInheritanceHierarchy(type)) {
                final Field[] currentClazzFields = curr.getDeclaredFields();
                AccessibleObject.setAccessible(currentClazzFields, true);
                final Field[] arr$;
                final Field[] classFields = arr$ = currentClazzFields;
                for (final Field f : arr$) {
                    fields.add(new FieldAttributes(curr, f, declaredType));
                }
            }
            ReflectingFieldNavigator.fieldsCache.addElement(type, fields);
        }
        return fields;
    }
    
    private List<Class<?>> getInheritanceHierarchy(final Type type) {
        final List<Class<?>> classes = new ArrayList<Class<?>>();
        Class<?> curr;
        for (Class<?> topLevelClass = curr = $Gson$Types.getRawType(type); curr != null && !curr.equals(Object.class); curr = curr.getSuperclass()) {
            if (!curr.isSynthetic()) {
                classes.add(curr);
            }
        }
        return classes;
    }
    
    static {
        fieldsCache = new LruCache<Type, List<FieldAttributes>>(500);
    }
}
