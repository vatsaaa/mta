// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.annotations.Until;
import com.google.gson.annotations.Since;
import com.google.gson.internal.$Gson$Preconditions;

final class VersionExclusionStrategy implements ExclusionStrategy
{
    private final double version;
    
    VersionExclusionStrategy(final double version) {
        $Gson$Preconditions.checkArgument(version >= 0.0);
        this.version = version;
    }
    
    public boolean shouldSkipField(final FieldAttributes f) {
        return !this.isValidVersion(f.getAnnotation(Since.class), f.getAnnotation(Until.class));
    }
    
    public boolean shouldSkipClass(final Class<?> clazz) {
        return !this.isValidVersion(clazz.getAnnotation(Since.class), clazz.getAnnotation(Until.class));
    }
    
    private boolean isValidVersion(final Since since, final Until until) {
        return this.isValidSince(since) && this.isValidUntil(until);
    }
    
    private boolean isValidSince(final Since annotation) {
        if (annotation != null) {
            final double annotationVersion = annotation.value();
            if (annotationVersion > this.version) {
                return false;
            }
        }
        return true;
    }
    
    private boolean isValidUntil(final Until annotation) {
        if (annotation != null) {
            final double annotationVersion = annotation.value();
            if (annotationVersion <= this.version) {
                return false;
            }
        }
        return true;
    }
}
