// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import com.google.gson.internal.$Gson$Preconditions;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Collection;
import java.util.Arrays;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;

public final class GsonBuilder
{
    private static final MapAsArrayTypeAdapter COMPLEX_KEY_MAP_TYPE_ADAPTER;
    private static final InnerClassExclusionStrategy innerClassExclusionStrategy;
    private static final ExposeAnnotationDeserializationExclusionStrategy exposeAnnotationDeserializationExclusionStrategy;
    private static final ExposeAnnotationSerializationExclusionStrategy exposeAnnotationSerializationExclusionStrategy;
    private final Set<ExclusionStrategy> serializeExclusionStrategies;
    private final Set<ExclusionStrategy> deserializeExclusionStrategies;
    private double ignoreVersionsAfter;
    private ModifierBasedExclusionStrategy modifierBasedExclusionStrategy;
    private boolean serializeInnerClasses;
    private boolean excludeFieldsWithoutExposeAnnotation;
    private LongSerializationPolicy longSerializationPolicy;
    private FieldNamingStrategy2 fieldNamingPolicy;
    private final ParameterizedTypeHandlerMap<InstanceCreator<?>> instanceCreators;
    private final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers;
    private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers;
    private boolean serializeNulls;
    private String datePattern;
    private int dateStyle;
    private int timeStyle;
    private boolean serializeSpecialFloatingPointValues;
    private boolean escapeHtmlChars;
    private boolean prettyPrinting;
    private boolean generateNonExecutableJson;
    
    public GsonBuilder() {
        this.serializeExclusionStrategies = new HashSet<ExclusionStrategy>();
        (this.deserializeExclusionStrategies = new HashSet<ExclusionStrategy>()).add(Gson.DEFAULT_ANON_LOCAL_CLASS_EXCLUSION_STRATEGY);
        this.deserializeExclusionStrategies.add(Gson.DEFAULT_SYNTHETIC_FIELD_EXCLUSION_STRATEGY);
        this.serializeExclusionStrategies.add(Gson.DEFAULT_ANON_LOCAL_CLASS_EXCLUSION_STRATEGY);
        this.serializeExclusionStrategies.add(Gson.DEFAULT_SYNTHETIC_FIELD_EXCLUSION_STRATEGY);
        this.ignoreVersionsAfter = -1.0;
        this.serializeInnerClasses = true;
        this.prettyPrinting = false;
        this.escapeHtmlChars = true;
        this.modifierBasedExclusionStrategy = Gson.DEFAULT_MODIFIER_BASED_EXCLUSION_STRATEGY;
        this.excludeFieldsWithoutExposeAnnotation = false;
        this.longSerializationPolicy = LongSerializationPolicy.DEFAULT;
        this.fieldNamingPolicy = Gson.DEFAULT_NAMING_POLICY;
        this.instanceCreators = new ParameterizedTypeHandlerMap<InstanceCreator<?>>();
        this.serializers = new ParameterizedTypeHandlerMap<JsonSerializer<?>>();
        this.deserializers = new ParameterizedTypeHandlerMap<JsonDeserializer<?>>();
        this.serializeNulls = false;
        this.dateStyle = 2;
        this.timeStyle = 2;
        this.serializeSpecialFloatingPointValues = false;
        this.generateNonExecutableJson = false;
    }
    
    public GsonBuilder setVersion(final double ignoreVersionsAfter) {
        this.ignoreVersionsAfter = ignoreVersionsAfter;
        return this;
    }
    
    public GsonBuilder excludeFieldsWithModifiers(final int... modifiers) {
        this.modifierBasedExclusionStrategy = new ModifierBasedExclusionStrategy(modifiers);
        return this;
    }
    
    public GsonBuilder generateNonExecutableJson() {
        this.generateNonExecutableJson = true;
        return this;
    }
    
    public GsonBuilder excludeFieldsWithoutExposeAnnotation() {
        this.excludeFieldsWithoutExposeAnnotation = true;
        return this;
    }
    
    public GsonBuilder serializeNulls() {
        this.serializeNulls = true;
        return this;
    }
    
    public GsonBuilder enableComplexMapKeySerialization() {
        this.registerTypeHierarchyAdapter(Map.class, GsonBuilder.COMPLEX_KEY_MAP_TYPE_ADAPTER);
        return this;
    }
    
    public GsonBuilder disableInnerClassSerialization() {
        this.serializeInnerClasses = false;
        return this;
    }
    
    public GsonBuilder setLongSerializationPolicy(final LongSerializationPolicy serializationPolicy) {
        this.longSerializationPolicy = serializationPolicy;
        return this;
    }
    
    public GsonBuilder setFieldNamingPolicy(final FieldNamingPolicy namingConvention) {
        return this.setFieldNamingStrategy(namingConvention.getFieldNamingPolicy());
    }
    
    public GsonBuilder setFieldNamingStrategy(final FieldNamingStrategy fieldNamingStrategy) {
        return this.setFieldNamingStrategy(new FieldNamingStrategy2Adapter(fieldNamingStrategy));
    }
    
    GsonBuilder setFieldNamingStrategy(final FieldNamingStrategy2 fieldNamingStrategy) {
        this.fieldNamingPolicy = new SerializedNameAnnotationInterceptingNamingPolicy(fieldNamingStrategy);
        return this;
    }
    
    public GsonBuilder setExclusionStrategies(final ExclusionStrategy... strategies) {
        final List<ExclusionStrategy> strategyList = Arrays.asList(strategies);
        this.serializeExclusionStrategies.addAll(strategyList);
        this.deserializeExclusionStrategies.addAll(strategyList);
        return this;
    }
    
    public GsonBuilder addSerializationExclusionStrategy(final ExclusionStrategy strategy) {
        this.serializeExclusionStrategies.add(strategy);
        return this;
    }
    
    public GsonBuilder addDeserializationExclusionStrategy(final ExclusionStrategy strategy) {
        this.deserializeExclusionStrategies.add(strategy);
        return this;
    }
    
    public GsonBuilder setPrettyPrinting() {
        this.prettyPrinting = true;
        return this;
    }
    
    public GsonBuilder disableHtmlEscaping() {
        this.escapeHtmlChars = false;
        return this;
    }
    
    public GsonBuilder setDateFormat(final String pattern) {
        this.datePattern = pattern;
        return this;
    }
    
    public GsonBuilder setDateFormat(final int style) {
        this.dateStyle = style;
        this.datePattern = null;
        return this;
    }
    
    public GsonBuilder setDateFormat(final int dateStyle, final int timeStyle) {
        this.dateStyle = dateStyle;
        this.timeStyle = timeStyle;
        this.datePattern = null;
        return this;
    }
    
    public GsonBuilder registerTypeAdapter(final Type type, final Object typeAdapter) {
        $Gson$Preconditions.checkArgument(typeAdapter instanceof JsonSerializer || typeAdapter instanceof JsonDeserializer || typeAdapter instanceof InstanceCreator);
        if (typeAdapter instanceof InstanceCreator) {
            this.registerInstanceCreator(type, (InstanceCreator<?>)typeAdapter);
        }
        if (typeAdapter instanceof JsonSerializer) {
            this.registerSerializer(type, (JsonSerializer<Object>)typeAdapter);
        }
        if (typeAdapter instanceof JsonDeserializer) {
            this.registerDeserializer(type, (JsonDeserializer<Object>)typeAdapter);
        }
        return this;
    }
    
    private <T> GsonBuilder registerInstanceCreator(final Type typeOfT, final InstanceCreator<? extends T> instanceCreator) {
        this.instanceCreators.register(typeOfT, instanceCreator);
        return this;
    }
    
    private <T> GsonBuilder registerSerializer(final Type typeOfT, final JsonSerializer<T> serializer) {
        this.serializers.register(typeOfT, serializer);
        return this;
    }
    
    private <T> GsonBuilder registerDeserializer(final Type typeOfT, final JsonDeserializer<T> deserializer) {
        this.deserializers.register(typeOfT, new JsonDeserializerExceptionWrapper<Object>((JsonDeserializer<Object>)deserializer));
        return this;
    }
    
    public GsonBuilder registerTypeHierarchyAdapter(final Class<?> baseType, final Object typeAdapter) {
        $Gson$Preconditions.checkArgument(typeAdapter instanceof JsonSerializer || typeAdapter instanceof JsonDeserializer || typeAdapter instanceof InstanceCreator);
        if (typeAdapter instanceof InstanceCreator) {
            this.registerInstanceCreatorForTypeHierarchy(baseType, (InstanceCreator<?>)typeAdapter);
        }
        if (typeAdapter instanceof JsonSerializer) {
            this.registerSerializerForTypeHierarchy(baseType, (JsonSerializer<Object>)typeAdapter);
        }
        if (typeAdapter instanceof JsonDeserializer) {
            this.registerDeserializerForTypeHierarchy(baseType, (JsonDeserializer<Object>)typeAdapter);
        }
        return this;
    }
    
    private <T> GsonBuilder registerInstanceCreatorForTypeHierarchy(final Class<?> classOfT, final InstanceCreator<? extends T> instanceCreator) {
        this.instanceCreators.registerForTypeHierarchy(classOfT, instanceCreator);
        return this;
    }
    
    private <T> GsonBuilder registerSerializerForTypeHierarchy(final Class<?> classOfT, final JsonSerializer<T> serializer) {
        this.serializers.registerForTypeHierarchy(classOfT, serializer);
        return this;
    }
    
    private <T> GsonBuilder registerDeserializerForTypeHierarchy(final Class<?> classOfT, final JsonDeserializer<T> deserializer) {
        this.deserializers.registerForTypeHierarchy(classOfT, new JsonDeserializerExceptionWrapper<Object>((JsonDeserializer<Object>)deserializer));
        return this;
    }
    
    public GsonBuilder serializeSpecialFloatingPointValues() {
        this.serializeSpecialFloatingPointValues = true;
        return this;
    }
    
    public Gson create() {
        final List<ExclusionStrategy> deserializationStrategies = new LinkedList<ExclusionStrategy>(this.deserializeExclusionStrategies);
        final List<ExclusionStrategy> serializationStrategies = new LinkedList<ExclusionStrategy>(this.serializeExclusionStrategies);
        deserializationStrategies.add(this.modifierBasedExclusionStrategy);
        serializationStrategies.add(this.modifierBasedExclusionStrategy);
        if (!this.serializeInnerClasses) {
            deserializationStrategies.add(GsonBuilder.innerClassExclusionStrategy);
            serializationStrategies.add(GsonBuilder.innerClassExclusionStrategy);
        }
        if (this.ignoreVersionsAfter != -1.0) {
            final VersionExclusionStrategy versionExclusionStrategy = new VersionExclusionStrategy(this.ignoreVersionsAfter);
            deserializationStrategies.add(versionExclusionStrategy);
            serializationStrategies.add(versionExclusionStrategy);
        }
        if (this.excludeFieldsWithoutExposeAnnotation) {
            deserializationStrategies.add(GsonBuilder.exposeAnnotationDeserializationExclusionStrategy);
            serializationStrategies.add(GsonBuilder.exposeAnnotationSerializationExclusionStrategy);
        }
        final ParameterizedTypeHandlerMap<JsonSerializer<?>> customSerializers = DefaultTypeAdapters.DEFAULT_HIERARCHY_SERIALIZERS.copyOf();
        customSerializers.register(this.serializers.copyOf());
        final ParameterizedTypeHandlerMap<JsonDeserializer<?>> customDeserializers = DefaultTypeAdapters.DEFAULT_HIERARCHY_DESERIALIZERS.copyOf();
        customDeserializers.register(this.deserializers.copyOf());
        addTypeAdaptersForDate(this.datePattern, this.dateStyle, this.timeStyle, customSerializers, customDeserializers);
        customSerializers.registerIfAbsent(DefaultTypeAdapters.getDefaultSerializers(this.serializeSpecialFloatingPointValues, this.longSerializationPolicy));
        customDeserializers.registerIfAbsent(DefaultTypeAdapters.getDefaultDeserializers());
        final ParameterizedTypeHandlerMap<InstanceCreator<?>> customInstanceCreators = this.instanceCreators.copyOf();
        customInstanceCreators.registerIfAbsent(DefaultTypeAdapters.getDefaultInstanceCreators());
        customSerializers.makeUnmodifiable();
        customDeserializers.makeUnmodifiable();
        this.instanceCreators.makeUnmodifiable();
        final MappedObjectConstructor objConstructor = new MappedObjectConstructor(customInstanceCreators);
        final Gson gson = new Gson(new DisjunctionExclusionStrategy(deserializationStrategies), new DisjunctionExclusionStrategy(serializationStrategies), this.fieldNamingPolicy, objConstructor, this.serializeNulls, customSerializers, customDeserializers, this.generateNonExecutableJson, this.escapeHtmlChars, this.prettyPrinting);
        return gson;
    }
    
    private static void addTypeAdaptersForDate(final String datePattern, final int dateStyle, final int timeStyle, final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers, final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers) {
        DefaultTypeAdapters.DefaultDateTypeAdapter dateTypeAdapter = null;
        if (datePattern != null && !"".equals(datePattern.trim())) {
            dateTypeAdapter = new DefaultTypeAdapters.DefaultDateTypeAdapter(datePattern);
        }
        else if (dateStyle != 2 && timeStyle != 2) {
            dateTypeAdapter = new DefaultTypeAdapters.DefaultDateTypeAdapter(dateStyle, timeStyle);
        }
        if (dateTypeAdapter != null) {
            registerIfAbsent(Date.class, serializers, dateTypeAdapter);
            registerIfAbsent(Date.class, deserializers, dateTypeAdapter);
            registerIfAbsent(Timestamp.class, serializers, dateTypeAdapter);
            registerIfAbsent(Timestamp.class, deserializers, dateTypeAdapter);
            registerIfAbsent(java.sql.Date.class, serializers, dateTypeAdapter);
            registerIfAbsent(java.sql.Date.class, deserializers, dateTypeAdapter);
        }
    }
    
    private static <T> void registerIfAbsent(final Class<?> type, final ParameterizedTypeHandlerMap<T> adapters, final T adapter) {
        if (!adapters.hasSpecificHandlerFor(type)) {
            adapters.register(type, adapter);
        }
    }
    
    static {
        COMPLEX_KEY_MAP_TYPE_ADAPTER = new MapAsArrayTypeAdapter();
        innerClassExclusionStrategy = new InnerClassExclusionStrategy();
        exposeAnnotationDeserializationExclusionStrategy = new ExposeAnnotationDeserializationExclusionStrategy();
        exposeAnnotationSerializationExclusionStrategy = new ExposeAnnotationSerializationExclusionStrategy();
    }
}
