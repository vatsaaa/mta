// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;
import java.io.EOFException;
import com.google.gson.stream.JsonReader;

final class Streams
{
    static JsonElement parse(final JsonReader reader) throws JsonParseException {
        boolean isEmpty = true;
        try {
            reader.peek();
            isEmpty = false;
            return parseRecursive(reader);
        }
        catch (EOFException e) {
            if (isEmpty) {
                return JsonNull.createJsonNull();
            }
            throw new JsonIOException(e);
        }
        catch (MalformedJsonException e2) {
            throw new JsonSyntaxException(e2);
        }
        catch (IOException e3) {
            throw new JsonIOException(e3);
        }
        catch (NumberFormatException e4) {
            throw new JsonSyntaxException(e4);
        }
    }
    
    private static JsonElement parseRecursive(final JsonReader reader) throws IOException {
        switch (reader.peek()) {
            case STRING: {
                return new JsonPrimitive(reader.nextString());
            }
            case NUMBER: {
                final String number = reader.nextString();
                return new JsonPrimitive(JsonPrimitive.stringToNumber(number));
            }
            case BOOLEAN: {
                return new JsonPrimitive(reader.nextBoolean());
            }
            case NULL: {
                reader.nextNull();
                return JsonNull.createJsonNull();
            }
            case BEGIN_ARRAY: {
                final JsonArray array = new JsonArray();
                reader.beginArray();
                while (reader.hasNext()) {
                    array.add(parseRecursive(reader));
                }
                reader.endArray();
                return array;
            }
            case BEGIN_OBJECT: {
                final JsonObject object = new JsonObject();
                reader.beginObject();
                while (reader.hasNext()) {
                    object.add(reader.nextName(), parseRecursive(reader));
                }
                reader.endObject();
                return object;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    static void write(final JsonElement element, final boolean serializeNulls, final JsonWriter writer) throws IOException {
        if (element == null || element.isJsonNull()) {
            if (serializeNulls) {
                writer.nullValue();
            }
        }
        else if (element.isJsonPrimitive()) {
            final JsonPrimitive primitive = element.getAsJsonPrimitive();
            if (primitive.isNumber()) {
                writer.value(primitive.getAsNumber());
            }
            else if (primitive.isBoolean()) {
                writer.value(primitive.getAsBoolean());
            }
            else {
                writer.value(primitive.getAsString());
            }
        }
        else if (element.isJsonArray()) {
            writer.beginArray();
            for (final JsonElement e : element.getAsJsonArray()) {
                if (e.isJsonNull()) {
                    writer.nullValue();
                }
                else {
                    write(e, serializeNulls, writer);
                }
            }
            writer.endArray();
        }
        else {
            if (!element.isJsonObject()) {
                throw new IllegalArgumentException("Couldn't write " + element.getClass());
            }
            writer.beginObject();
            for (final Map.Entry<String, JsonElement> e2 : element.getAsJsonObject().entrySet()) {
                final JsonElement value = e2.getValue();
                if (!serializeNulls && value.isJsonNull()) {
                    continue;
                }
                writer.name(e2.getKey());
                write(value, serializeNulls, writer);
            }
            writer.endObject();
        }
    }
    
    static Writer writerForAppendable(final Appendable appendable) {
        return (appendable instanceof Writer) ? ((Writer)appendable) : new AppendableWriter(appendable);
    }
    
    private static class AppendableWriter extends Writer
    {
        private final Appendable appendable;
        private final CurrentWrite currentWrite;
        
        private AppendableWriter(final Appendable appendable) {
            this.currentWrite = new CurrentWrite();
            this.appendable = appendable;
        }
        
        @Override
        public void write(final char[] chars, final int offset, final int length) throws IOException {
            this.currentWrite.chars = chars;
            this.appendable.append(this.currentWrite, offset, offset + length);
        }
        
        @Override
        public void write(final int i) throws IOException {
            this.appendable.append((char)i);
        }
        
        @Override
        public void flush() {
        }
        
        @Override
        public void close() {
        }
        
        static class CurrentWrite implements CharSequence
        {
            char[] chars;
            
            public int length() {
                return this.chars.length;
            }
            
            public char charAt(final int i) {
                return this.chars[i];
            }
            
            public CharSequence subSequence(final int start, final int end) {
                return new String(this.chars, start, end - start);
            }
        }
    }
}
