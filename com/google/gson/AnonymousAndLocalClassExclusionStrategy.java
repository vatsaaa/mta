// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

final class AnonymousAndLocalClassExclusionStrategy implements ExclusionStrategy
{
    public boolean shouldSkipField(final FieldAttributes f) {
        return this.isAnonymousOrLocal(f.getDeclaredClass());
    }
    
    public boolean shouldSkipClass(final Class<?> clazz) {
        return this.isAnonymousOrLocal(clazz);
    }
    
    private boolean isAnonymousOrLocal(final Class<?> clazz) {
        return !Enum.class.isAssignableFrom(clazz) && (clazz.isAnonymousClass() || clazz.isLocalClass());
    }
}
