// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.internal.$Gson$Preconditions;
import com.google.gson.internal.$Gson$Types;
import java.lang.reflect.Array;
import java.lang.reflect.Type;

final class JsonSerializationVisitor implements ObjectNavigator.Visitor
{
    private final ObjectNavigator objectNavigator;
    private final FieldNamingStrategy2 fieldNamingPolicy;
    private final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers;
    private final boolean serializeNulls;
    private final JsonSerializationContext context;
    private final MemoryRefStack ancestors;
    private JsonElement root;
    
    JsonSerializationVisitor(final ObjectNavigator objectNavigator, final FieldNamingStrategy2 fieldNamingPolicy, final boolean serializeNulls, final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers, final JsonSerializationContext context, final MemoryRefStack ancestors) {
        this.objectNavigator = objectNavigator;
        this.fieldNamingPolicy = fieldNamingPolicy;
        this.serializeNulls = serializeNulls;
        this.serializers = serializers;
        this.context = context;
        this.ancestors = ancestors;
    }
    
    public Object getTarget() {
        return null;
    }
    
    public void start(final ObjectTypePair node) {
        if (node == null) {
            return;
        }
        if (this.ancestors.contains(node)) {
            throw new CircularReferenceException(node);
        }
        this.ancestors.push(node);
    }
    
    public void end(final ObjectTypePair node) {
        if (node != null) {
            this.ancestors.pop();
        }
    }
    
    public void startVisitingObject(final Object node) {
        this.assignToRoot(new JsonObject());
    }
    
    public void visitArray(final Object array, final Type arrayType) {
        this.assignToRoot(new JsonArray());
        final int length = Array.getLength(array);
        final Type componentType = $Gson$Types.getArrayComponentType(arrayType);
        for (int i = 0; i < length; ++i) {
            final Object child = Array.get(array, i);
            this.addAsArrayElement(new ObjectTypePair(child, componentType, false));
        }
    }
    
    public void visitArrayField(final FieldAttributes f, final Type typeOfF, final Object obj) {
        try {
            if (this.isFieldNull(f, obj)) {
                if (this.serializeNulls) {
                    this.addChildAsElement(f, JsonNull.createJsonNull());
                }
            }
            else {
                final Object array = this.getFieldValue(f, obj);
                this.addAsChildOfObject(f, new ObjectTypePair(array, typeOfF, false));
            }
        }
        catch (CircularReferenceException e) {
            throw e.createDetailedException(f);
        }
    }
    
    public void visitObjectField(final FieldAttributes f, final Type typeOfF, final Object obj) {
        try {
            if (this.isFieldNull(f, obj)) {
                if (this.serializeNulls) {
                    this.addChildAsElement(f, JsonNull.createJsonNull());
                }
            }
            else {
                final Object fieldValue = this.getFieldValue(f, obj);
                this.addAsChildOfObject(f, new ObjectTypePair(fieldValue, typeOfF, false));
            }
        }
        catch (CircularReferenceException e) {
            throw e.createDetailedException(f);
        }
    }
    
    public void visitPrimitive(final Object obj) {
        final JsonElement json = (obj == null) ? JsonNull.createJsonNull() : new JsonPrimitive(obj);
        this.assignToRoot(json);
    }
    
    private void addAsChildOfObject(final FieldAttributes f, final ObjectTypePair fieldValuePair) {
        final JsonElement childElement = this.getJsonElementForChild(fieldValuePair);
        this.addChildAsElement(f, childElement);
    }
    
    private void addChildAsElement(final FieldAttributes f, final JsonElement childElement) {
        this.root.getAsJsonObject().add(this.fieldNamingPolicy.translateName(f), childElement);
    }
    
    private void addAsArrayElement(final ObjectTypePair elementTypePair) {
        if (elementTypePair.getObject() == null) {
            this.root.getAsJsonArray().add(JsonNull.createJsonNull());
        }
        else {
            final JsonElement childElement = this.getJsonElementForChild(elementTypePair);
            this.root.getAsJsonArray().add(childElement);
        }
    }
    
    private JsonElement getJsonElementForChild(final ObjectTypePair fieldValueTypePair) {
        final JsonSerializationVisitor childVisitor = new JsonSerializationVisitor(this.objectNavigator, this.fieldNamingPolicy, this.serializeNulls, this.serializers, this.context, this.ancestors);
        this.objectNavigator.accept(fieldValueTypePair, childVisitor);
        return childVisitor.getJsonElement();
    }
    
    public boolean visitUsingCustomHandler(final ObjectTypePair objTypePair) {
        try {
            final Object obj = objTypePair.getObject();
            if (obj == null) {
                if (this.serializeNulls) {
                    this.assignToRoot(JsonNull.createJsonNull());
                }
                return true;
            }
            final JsonElement element = this.findAndInvokeCustomSerializer(objTypePair);
            if (element != null) {
                this.assignToRoot(element);
                return true;
            }
            return false;
        }
        catch (CircularReferenceException e) {
            throw e.createDetailedException(null);
        }
    }
    
    private JsonElement findAndInvokeCustomSerializer(ObjectTypePair objTypePair) {
        final Pair<JsonSerializer<?>, ObjectTypePair> pair = objTypePair.getMatchingHandler(this.serializers);
        if (pair == null) {
            return null;
        }
        final JsonSerializer serializer = pair.first;
        objTypePair = pair.second;
        this.start(objTypePair);
        try {
            final JsonElement element = serializer.serialize(objTypePair.getObject(), objTypePair.getType(), this.context);
            return (element == null) ? JsonNull.createJsonNull() : element;
        }
        finally {
            this.end(objTypePair);
        }
    }
    
    public boolean visitFieldUsingCustomHandler(final FieldAttributes f, final Type declaredTypeOfField, final Object parent) {
        try {
            $Gson$Preconditions.checkState(this.root.isJsonObject());
            final Object obj = f.get(parent);
            if (obj == null) {
                if (this.serializeNulls) {
                    this.addChildAsElement(f, JsonNull.createJsonNull());
                }
                return true;
            }
            final ObjectTypePair objTypePair = new ObjectTypePair(obj, declaredTypeOfField, false);
            final JsonElement child = this.findAndInvokeCustomSerializer(objTypePair);
            if (child != null) {
                this.addChildAsElement(f, child);
                return true;
            }
            return false;
        }
        catch (IllegalAccessException e2) {
            throw new RuntimeException();
        }
        catch (CircularReferenceException e) {
            throw e.createDetailedException(f);
        }
    }
    
    private void assignToRoot(final JsonElement newRoot) {
        this.root = $Gson$Preconditions.checkNotNull(newRoot);
    }
    
    private boolean isFieldNull(final FieldAttributes f, final Object obj) {
        return this.getFieldValue(f, obj) == null;
    }
    
    private Object getFieldValue(final FieldAttributes f, final Object obj) {
        try {
            return f.get(obj);
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    
    public JsonElement getJsonElement() {
        return this.root;
    }
}
