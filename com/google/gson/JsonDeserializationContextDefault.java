// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;

final class JsonDeserializationContextDefault implements JsonDeserializationContext
{
    private final ObjectNavigator objectNavigator;
    private final FieldNamingStrategy2 fieldNamingPolicy;
    private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers;
    private final MappedObjectConstructor objectConstructor;
    
    JsonDeserializationContextDefault(final ObjectNavigator objectNavigator, final FieldNamingStrategy2 fieldNamingPolicy, final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers, final MappedObjectConstructor objectConstructor) {
        this.objectNavigator = objectNavigator;
        this.fieldNamingPolicy = fieldNamingPolicy;
        this.deserializers = deserializers;
        this.objectConstructor = objectConstructor;
    }
    
    ObjectConstructor getObjectConstructor() {
        return this.objectConstructor;
    }
    
    public <T> T deserialize(final JsonElement json, final Type typeOfT) throws JsonParseException {
        if (json == null || json.isJsonNull()) {
            return null;
        }
        if (json.isJsonArray()) {
            return this.fromJsonArray(typeOfT, json.getAsJsonArray(), this);
        }
        if (json.isJsonObject()) {
            return this.fromJsonObject(typeOfT, json.getAsJsonObject(), this);
        }
        if (json.isJsonPrimitive()) {
            return this.fromJsonPrimitive(typeOfT, json.getAsJsonPrimitive(), this);
        }
        throw new JsonParseException("Failed parsing JSON source: " + json + " to Json");
    }
    
    private <T> T fromJsonArray(final Type arrayType, final JsonArray jsonArray, final JsonDeserializationContext context) throws JsonParseException {
        final JsonArrayDeserializationVisitor<T> visitor = new JsonArrayDeserializationVisitor<T>(jsonArray, arrayType, this.objectNavigator, this.fieldNamingPolicy, this.objectConstructor, this.deserializers, context);
        this.objectNavigator.accept(new ObjectTypePair(null, arrayType, true), visitor);
        return visitor.getTarget();
    }
    
    private <T> T fromJsonObject(final Type typeOfT, final JsonObject jsonObject, final JsonDeserializationContext context) throws JsonParseException {
        final JsonObjectDeserializationVisitor<T> visitor = new JsonObjectDeserializationVisitor<T>(jsonObject, typeOfT, this.objectNavigator, this.fieldNamingPolicy, this.objectConstructor, this.deserializers, context);
        this.objectNavigator.accept(new ObjectTypePair(null, typeOfT, true), visitor);
        return visitor.getTarget();
    }
    
    private <T> T fromJsonPrimitive(final Type typeOfT, final JsonPrimitive json, final JsonDeserializationContext context) throws JsonParseException {
        final JsonObjectDeserializationVisitor<T> visitor = new JsonObjectDeserializationVisitor<T>(json, typeOfT, this.objectNavigator, this.fieldNamingPolicy, this.objectConstructor, this.deserializers, context);
        this.objectNavigator.accept(new ObjectTypePair(json.getAsObject(), typeOfT, true), visitor);
        final Object target = visitor.getTarget();
        return (T)target;
    }
}
