// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Array;
import com.google.gson.internal.$Gson$Types;
import java.lang.reflect.Type;

final class MappedObjectConstructor implements ObjectConstructor
{
    private static final UnsafeAllocator unsafeAllocator;
    private static final DefaultConstructorAllocator defaultConstructorAllocator;
    private final ParameterizedTypeHandlerMap<InstanceCreator<?>> instanceCreatorMap;
    
    public MappedObjectConstructor(final ParameterizedTypeHandlerMap<InstanceCreator<?>> instanceCreators) {
        this.instanceCreatorMap = instanceCreators;
    }
    
    public <T> T construct(final Type typeOfT) {
        final InstanceCreator<T> creator = (InstanceCreator<T>)this.instanceCreatorMap.getHandlerFor(typeOfT);
        if (creator != null) {
            return creator.createInstance(typeOfT);
        }
        return (T)this.constructWithAllocators(typeOfT);
    }
    
    public Object constructArray(final Type type, final int length) {
        return Array.newInstance($Gson$Types.getRawType(type), length);
    }
    
    private <T> T constructWithAllocators(final Type typeOfT) {
        try {
            final Class<T> clazz = (Class<T>)$Gson$Types.getRawType(typeOfT);
            final T obj = MappedObjectConstructor.defaultConstructorAllocator.newInstance(clazz);
            return (obj == null) ? MappedObjectConstructor.unsafeAllocator.newInstance(clazz) : obj;
        }
        catch (Exception e) {
            throw new RuntimeException("Unable to invoke no-args constructor for " + typeOfT + ". " + "Register an InstanceCreator with Gson for this type may fix this problem.", e);
        }
    }
    
    @Override
    public String toString() {
        return this.instanceCreatorMap.toString();
    }
    
    static {
        unsafeAllocator = UnsafeAllocator.create();
        defaultConstructorAllocator = new DefaultConstructorAllocator(500);
    }
}
