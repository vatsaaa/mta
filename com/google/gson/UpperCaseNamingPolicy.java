// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.lang.reflect.Type;

final class UpperCaseNamingPolicy extends RecursiveFieldNamingPolicy
{
    @Override
    protected String translateName(final String target, final Type fieldType, final Collection<Annotation> annotations) {
        return target.toUpperCase();
    }
}
