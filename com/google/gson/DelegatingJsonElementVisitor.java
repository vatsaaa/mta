// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.io.IOException;
import com.google.gson.internal.$Gson$Preconditions;

final class DelegatingJsonElementVisitor implements JsonElementVisitor
{
    private final JsonElementVisitor delegate;
    
    protected DelegatingJsonElementVisitor(final JsonElementVisitor delegate) {
        this.delegate = $Gson$Preconditions.checkNotNull(delegate);
    }
    
    public void endArray(final JsonArray array) throws IOException {
        this.delegate.endArray(array);
    }
    
    public void endObject(final JsonObject object) throws IOException {
        this.delegate.endObject(object);
    }
    
    public void startArray(final JsonArray array) throws IOException {
        this.delegate.startArray(array);
    }
    
    public void startObject(final JsonObject object) throws IOException {
        this.delegate.startObject(object);
    }
    
    public void visitArrayMember(final JsonArray parent, final JsonPrimitive member, final boolean isFirst) throws IOException {
        this.delegate.visitArrayMember(parent, member, isFirst);
    }
    
    public void visitArrayMember(final JsonArray parent, final JsonArray member, final boolean isFirst) throws IOException {
        this.delegate.visitArrayMember(parent, member, isFirst);
    }
    
    public void visitArrayMember(final JsonArray parent, final JsonObject member, final boolean isFirst) throws IOException {
        this.delegate.visitArrayMember(parent, member, isFirst);
    }
    
    public void visitObjectMember(final JsonObject parent, final String memberName, final JsonPrimitive member, final boolean isFirst) throws IOException {
        this.delegate.visitObjectMember(parent, memberName, member, isFirst);
    }
    
    public void visitObjectMember(final JsonObject parent, final String memberName, final JsonArray member, final boolean isFirst) throws IOException {
        this.delegate.visitObjectMember(parent, memberName, member, isFirst);
    }
    
    public void visitObjectMember(final JsonObject parent, final String memberName, final JsonObject member, final boolean isFirst) throws IOException {
        this.delegate.visitObjectMember(parent, memberName, member, isFirst);
    }
    
    public void visitNullObjectMember(final JsonObject parent, final String memberName, final boolean isFirst) throws IOException {
        this.delegate.visitNullObjectMember(parent, memberName, isFirst);
    }
    
    public void visitPrimitive(final JsonPrimitive primitive) throws IOException {
        this.delegate.visitPrimitive(primitive);
    }
    
    public void visitNull() throws IOException {
        this.delegate.visitNull();
    }
    
    public void visitNullArrayMember(final JsonArray parent, final boolean isFirst) throws IOException {
        this.delegate.visitNullArrayMember(parent, isFirst);
    }
}
