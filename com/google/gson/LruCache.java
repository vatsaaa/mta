// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Map;
import java.util.LinkedHashMap;

final class LruCache<K, V> extends LinkedHashMap<K, V> implements Cache<K, V>
{
    private static final long serialVersionUID = 1L;
    private final int maxCapacity;
    
    public LruCache(final int maxCapacity) {
        super(maxCapacity, 0.7f, true);
        this.maxCapacity = maxCapacity;
    }
    
    public synchronized void addElement(final K key, final V value) {
        this.put(key, value);
    }
    
    public synchronized V getElement(final K key) {
        return this.get(key);
    }
    
    public synchronized V removeElement(final K key) {
        return this.remove(key);
    }
    
    @Override
    protected boolean removeEldestEntry(final Map.Entry<K, V> entry) {
        return this.size() > this.maxCapacity;
    }
}
