// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;

interface ObjectConstructor
{
     <T> T construct(final Type p0);
    
    Object constructArray(final Type p0, final int p1);
}
