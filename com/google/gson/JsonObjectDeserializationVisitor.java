// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Type;

final class JsonObjectDeserializationVisitor<T> extends JsonDeserializationVisitor<T>
{
    JsonObjectDeserializationVisitor(final JsonElement json, final Type type, final ObjectNavigator objectNavigator, final FieldNamingStrategy2 fieldNamingPolicy, final ObjectConstructor objectConstructor, final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers, final JsonDeserializationContext context) {
        super(json, type, objectNavigator, fieldNamingPolicy, objectConstructor, deserializers, context);
    }
    
    @Override
    protected T constructTarget() {
        return this.objectConstructor.construct(this.targetType);
    }
    
    public void startVisitingObject(final Object node) {
    }
    
    public void visitArray(final Object array, final Type componentType) {
        throw new JsonParseException("Expecting object but found array: " + array);
    }
    
    public void visitObjectField(final FieldAttributes f, final Type typeOfF, final Object obj) {
        try {
            if (!this.json.isJsonObject()) {
                throw new JsonParseException("Expecting object found: " + this.json);
            }
            final JsonObject jsonObject = this.json.getAsJsonObject();
            final String fName = this.getFieldName(f);
            final JsonElement jsonChild = jsonObject.get(fName);
            if (jsonChild != null) {
                final Object child = this.visitChildAsObject(typeOfF, jsonChild);
                f.set(obj, child);
            }
            else {
                f.set(obj, null);
            }
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void visitArrayField(final FieldAttributes f, final Type typeOfF, final Object obj) {
        try {
            if (!this.json.isJsonObject()) {
                throw new JsonParseException("Expecting object found: " + this.json);
            }
            final JsonObject jsonObject = this.json.getAsJsonObject();
            final String fName = this.getFieldName(f);
            final JsonArray jsonChild = (JsonArray)jsonObject.get(fName);
            if (jsonChild != null) {
                final Object array = this.visitChildAsArray(typeOfF, jsonChild);
                f.set(obj, array);
            }
            else {
                f.set(obj, null);
            }
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    
    private String getFieldName(final FieldAttributes f) {
        return this.fieldNamingPolicy.translateName(f);
    }
    
    public boolean visitFieldUsingCustomHandler(final FieldAttributes f, final Type declaredTypeOfField, final Object parent) {
        try {
            final String fName = this.getFieldName(f);
            if (!this.json.isJsonObject()) {
                throw new JsonParseException("Expecting object found: " + this.json);
            }
            final JsonElement child = this.json.getAsJsonObject().get(fName);
            final boolean isPrimitive = Primitives.isPrimitive(declaredTypeOfField);
            if (child == null) {
                return true;
            }
            if (child.isJsonNull()) {
                if (!isPrimitive) {
                    f.set(parent, null);
                }
                return true;
            }
            final ObjectTypePair objTypePair = new ObjectTypePair(null, declaredTypeOfField, false);
            final Pair<JsonDeserializer<?>, ObjectTypePair> pair = objTypePair.getMatchingHandler(this.deserializers);
            if (pair == null) {
                return false;
            }
            final Object value = this.invokeCustomDeserializer(child, pair);
            if (value != null || !isPrimitive) {
                f.set(parent, value);
            }
            return true;
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException();
        }
    }
    
    public void visitPrimitive(final Object primitive) {
        if (!this.json.isJsonPrimitive()) {
            throw new JsonParseException("Type information is unavailable, and the target object is not a primitive: " + this.json);
        }
        final JsonPrimitive prim = this.json.getAsJsonPrimitive();
        this.target = (T)prim.getAsObject();
    }
}
