// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

public enum LongSerializationPolicy
{
    DEFAULT((Strategy)new DefaultStrategy()), 
    STRING((Strategy)new StringStrategy());
    
    private final Strategy strategy;
    
    private LongSerializationPolicy(final Strategy strategy) {
        this.strategy = strategy;
    }
    
    public JsonElement serialize(final Long value) {
        return this.strategy.serialize(value);
    }
    
    private static class DefaultStrategy implements Strategy
    {
        public JsonElement serialize(final Long value) {
            return new JsonPrimitive(value);
        }
    }
    
    private static class StringStrategy implements Strategy
    {
        public JsonElement serialize(final Long value) {
            return new JsonPrimitive(String.valueOf(value));
        }
    }
    
    private interface Strategy
    {
        JsonElement serialize(final Long p0);
    }
}
