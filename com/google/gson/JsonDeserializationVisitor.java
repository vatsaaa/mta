// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.internal.$Gson$Preconditions;
import java.lang.reflect.Type;

abstract class JsonDeserializationVisitor<T> implements ObjectNavigator.Visitor
{
    protected final ObjectNavigator objectNavigator;
    protected final FieldNamingStrategy2 fieldNamingPolicy;
    protected final ObjectConstructor objectConstructor;
    protected final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers;
    protected T target;
    protected final JsonElement json;
    protected final Type targetType;
    protected final JsonDeserializationContext context;
    protected boolean constructed;
    
    JsonDeserializationVisitor(final JsonElement json, final Type targetType, final ObjectNavigator objectNavigator, final FieldNamingStrategy2 fieldNamingPolicy, final ObjectConstructor objectConstructor, final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers, final JsonDeserializationContext context) {
        this.targetType = targetType;
        this.objectNavigator = objectNavigator;
        this.fieldNamingPolicy = fieldNamingPolicy;
        this.objectConstructor = objectConstructor;
        this.deserializers = deserializers;
        this.json = $Gson$Preconditions.checkNotNull(json);
        this.context = context;
        this.constructed = false;
    }
    
    public T getTarget() {
        if (!this.constructed) {
            this.target = this.constructTarget();
            this.constructed = true;
        }
        return this.target;
    }
    
    protected abstract T constructTarget();
    
    public void start(final ObjectTypePair node) {
    }
    
    public void end(final ObjectTypePair node) {
    }
    
    public final boolean visitUsingCustomHandler(final ObjectTypePair objTypePair) {
        final Pair<JsonDeserializer<?>, ObjectTypePair> pair = objTypePair.getMatchingHandler(this.deserializers);
        if (pair == null) {
            return false;
        }
        final Object value = this.invokeCustomDeserializer(this.json, pair);
        this.target = (T)value;
        return this.constructed = true;
    }
    
    protected Object invokeCustomDeserializer(final JsonElement element, final Pair<JsonDeserializer<?>, ObjectTypePair> pair) {
        if (element == null || element.isJsonNull()) {
            return null;
        }
        final Type objType = pair.second.type;
        return pair.first.deserialize(element, objType, this.context);
    }
    
    final Object visitChildAsObject(final Type childType, final JsonElement jsonChild) {
        final JsonDeserializationVisitor<?> childVisitor = new JsonObjectDeserializationVisitor<Object>(jsonChild, childType, this.objectNavigator, this.fieldNamingPolicy, this.objectConstructor, this.deserializers, this.context);
        return this.visitChild(childType, childVisitor);
    }
    
    final Object visitChildAsArray(final Type childType, final JsonArray jsonChild) {
        final JsonDeserializationVisitor<?> childVisitor = new JsonArrayDeserializationVisitor<Object>(jsonChild.getAsJsonArray(), childType, this.objectNavigator, this.fieldNamingPolicy, this.objectConstructor, this.deserializers, this.context);
        return this.visitChild(childType, childVisitor);
    }
    
    private Object visitChild(final Type type, final JsonDeserializationVisitor<?> childVisitor) {
        this.objectNavigator.accept(new ObjectTypePair(null, type, false), childVisitor);
        return childVisitor.getTarget();
    }
}
