// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.reflect.Constructor;

final class DefaultConstructorAllocator
{
    private static final Constructor<Null> NULL_CONSTRUCTOR;
    private final Cache<Class<?>, Constructor<?>> constructorCache;
    
    public DefaultConstructorAllocator() {
        this(200);
    }
    
    public DefaultConstructorAllocator(final int cacheSize) {
        this.constructorCache = new LruCache<Class<?>, Constructor<?>>(cacheSize);
    }
    
    final boolean isInCache(final Class<?> cacheKey) {
        return this.constructorCache.getElement(cacheKey) != null;
    }
    
    private static final Constructor<Null> createNullConstructor() {
        try {
            return getNoArgsConstructor(Null.class);
        }
        catch (Exception e) {
            return null;
        }
    }
    
    public <T> T newInstance(final Class<T> c) throws Exception {
        final Constructor<T> constructor = this.findConstructor(c);
        return (constructor != null) ? constructor.newInstance(new Object[0]) : null;
    }
    
    private <T> Constructor<T> findConstructor(final Class<T> c) {
        final Constructor<T> cachedElement = (Constructor<T>)this.constructorCache.getElement(c);
        if (cachedElement == null) {
            final Constructor<T> noArgsConstructor = (Constructor<T>)getNoArgsConstructor((Class<Object>)c);
            if (noArgsConstructor != null) {
                this.constructorCache.addElement(c, noArgsConstructor);
            }
            else {
                this.constructorCache.addElement(c, DefaultConstructorAllocator.NULL_CONSTRUCTOR);
            }
            return noArgsConstructor;
        }
        if (cachedElement == DefaultConstructorAllocator.NULL_CONSTRUCTOR) {
            return null;
        }
        return cachedElement;
    }
    
    private static <T> Constructor<T> getNoArgsConstructor(final Class<T> c) {
        try {
            final Constructor<T> declaredConstructor = c.getDeclaredConstructor((Class<?>[])new Class[0]);
            declaredConstructor.setAccessible(true);
            return declaredConstructor;
        }
        catch (Exception e) {
            return null;
        }
    }
    
    static {
        NULL_CONSTRUCTOR = createNullConstructor();
    }
    
    private static final class Null
    {
    }
}
