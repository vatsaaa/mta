// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.annotations.SerializedName;

final class SerializedNameAnnotationInterceptingNamingPolicy implements FieldNamingStrategy2
{
    private final FieldNamingStrategy2 delegate;
    
    SerializedNameAnnotationInterceptingNamingPolicy(final FieldNamingStrategy2 delegate) {
        this.delegate = delegate;
    }
    
    public String translateName(final FieldAttributes f) {
        final SerializedName serializedName = f.getAnnotation(SerializedName.class);
        return (serializedName == null) ? this.delegate.translateName(f) : serializedName.value();
    }
}
