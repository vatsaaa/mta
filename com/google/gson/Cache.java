// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

interface Cache<K, V>
{
    void addElement(final K p0, final V p1);
    
    V getElement(final K p0);
    
    V removeElement(final K p0);
}
