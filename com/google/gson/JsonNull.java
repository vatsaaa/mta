// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.io.IOException;

public final class JsonNull extends JsonElement
{
    private static final JsonNull INSTANCE;
    
    @Override
    protected void toString(final Appendable sb, final Escaper escaper) throws IOException {
        sb.append("null");
    }
    
    @Override
    public int hashCode() {
        return JsonNull.class.hashCode();
    }
    
    @Override
    public boolean equals(final Object other) {
        return this == other || other instanceof JsonNull;
    }
    
    static JsonNull createJsonNull() {
        return JsonNull.INSTANCE;
    }
    
    static {
        INSTANCE = new JsonNull();
    }
}
