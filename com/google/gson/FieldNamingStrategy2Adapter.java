// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import com.google.gson.internal.$Gson$Preconditions;

final class FieldNamingStrategy2Adapter implements FieldNamingStrategy2
{
    private final FieldNamingStrategy adaptee;
    
    FieldNamingStrategy2Adapter(final FieldNamingStrategy adaptee) {
        this.adaptee = $Gson$Preconditions.checkNotNull(adaptee);
    }
    
    public String translateName(final FieldAttributes f) {
        return this.adaptee.translateName(f.getFieldObject());
    }
}
