// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Iterator;
import com.google.gson.internal.$Gson$Preconditions;
import java.util.Collection;

final class DisjunctionExclusionStrategy implements ExclusionStrategy
{
    private final Collection<ExclusionStrategy> strategies;
    
    DisjunctionExclusionStrategy(final Collection<ExclusionStrategy> strategies) {
        this.strategies = $Gson$Preconditions.checkNotNull(strategies);
    }
    
    public boolean shouldSkipField(final FieldAttributes f) {
        for (final ExclusionStrategy strategy : this.strategies) {
            if (strategy.shouldSkipField(f)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean shouldSkipClass(final Class<?> clazz) {
        for (final ExclusionStrategy strategy : this.strategies) {
            if (strategy.shouldSkipClass(clazz)) {
                return true;
            }
        }
        return false;
    }
}
