// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

final class JsonTreeNavigator
{
    private final JsonElementVisitor visitor;
    private final boolean visitNulls;
    
    JsonTreeNavigator(final JsonElementVisitor visitor, final boolean visitNulls) {
        this.visitor = visitor;
        this.visitNulls = visitNulls;
    }
    
    public void navigate(final JsonElement element) throws IOException {
        if (element.isJsonNull()) {
            this.visitor.visitNull();
        }
        else if (element.isJsonArray()) {
            final JsonArray array = element.getAsJsonArray();
            this.visitor.startArray(array);
            boolean isFirst = true;
            for (final JsonElement child : array) {
                this.visitChild(array, child, isFirst);
                if (isFirst) {
                    isFirst = false;
                }
            }
            this.visitor.endArray(array);
        }
        else if (element.isJsonObject()) {
            final JsonObject object = element.getAsJsonObject();
            this.visitor.startObject(object);
            boolean isFirst = true;
            for (final Map.Entry<String, JsonElement> member : object.entrySet()) {
                final boolean visited = this.visitChild(object, member.getKey(), member.getValue(), isFirst);
                if (visited && isFirst) {
                    isFirst = false;
                }
            }
            this.visitor.endObject(object);
        }
        else {
            this.visitor.visitPrimitive(element.getAsJsonPrimitive());
        }
    }
    
    private boolean visitChild(final JsonObject parent, final String childName, final JsonElement child, final boolean isFirst) throws IOException {
        if (child.isJsonNull()) {
            if (!this.visitNulls) {
                return false;
            }
            this.visitor.visitNullObjectMember(parent, childName, isFirst);
            this.navigate(child.getAsJsonNull());
        }
        else if (child.isJsonArray()) {
            final JsonArray childAsArray = child.getAsJsonArray();
            this.visitor.visitObjectMember(parent, childName, childAsArray, isFirst);
            this.navigate(childAsArray);
        }
        else if (child.isJsonObject()) {
            final JsonObject childAsObject = child.getAsJsonObject();
            this.visitor.visitObjectMember(parent, childName, childAsObject, isFirst);
            this.navigate(childAsObject);
        }
        else {
            this.visitor.visitObjectMember(parent, childName, child.getAsJsonPrimitive(), isFirst);
        }
        return true;
    }
    
    private void visitChild(final JsonArray parent, final JsonElement child, final boolean isFirst) throws IOException {
        if (child.isJsonNull()) {
            this.visitor.visitNullArrayMember(parent, isFirst);
            this.navigate(child);
        }
        else if (child.isJsonArray()) {
            final JsonArray childAsArray = child.getAsJsonArray();
            this.visitor.visitArrayMember(parent, childAsArray, isFirst);
            this.navigate(childAsArray);
        }
        else if (child.isJsonObject()) {
            final JsonObject childAsObject = child.getAsJsonObject();
            this.visitor.visitArrayMember(parent, childAsObject, isFirst);
            this.navigate(childAsObject);
        }
        else {
            this.visitor.visitArrayMember(parent, child.getAsJsonPrimitive(), isFirst);
        }
    }
}
