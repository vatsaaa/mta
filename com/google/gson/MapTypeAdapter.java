// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.util.Iterator;
import com.google.gson.internal.$Gson$Types;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

final class MapTypeAdapter extends BaseMapTypeAdapter
{
    public JsonElement serialize(final Map src, final Type typeOfSrc, final JsonSerializationContext context) {
        final JsonObject map = new JsonObject();
        Type childGenericType = null;
        if (typeOfSrc instanceof ParameterizedType) {
            final Class<?> rawTypeOfSrc = $Gson$Types.getRawType(typeOfSrc);
            childGenericType = $Gson$Types.getMapKeyAndValueTypes(typeOfSrc, rawTypeOfSrc)[1];
        }
        for (final Map.Entry entry : src.entrySet()) {
            final Object value = entry.getValue();
            JsonElement valueElement;
            if (value == null) {
                valueElement = JsonNull.createJsonNull();
            }
            else {
                final Type childType = (childGenericType == null) ? value.getClass() : childGenericType;
                valueElement = BaseMapTypeAdapter.serialize(context, value, childType);
            }
            map.add(String.valueOf(entry.getKey()), valueElement);
        }
        return map;
    }
    
    public Map deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
        final Map<Object, Object> map = BaseMapTypeAdapter.constructMapType(typeOfT, context);
        final Type[] keyAndValueTypes = $Gson$Types.getMapKeyAndValueTypes(typeOfT, $Gson$Types.getRawType(typeOfT));
        for (final Map.Entry<String, JsonElement> entry : json.getAsJsonObject().entrySet()) {
            final Object key = context.deserialize(new JsonPrimitive(entry.getKey()), keyAndValueTypes[0]);
            final Object value = context.deserialize(entry.getValue(), keyAndValueTypes[1]);
            map.put(key, value);
        }
        return map;
    }
    
    @Override
    public String toString() {
        return MapTypeAdapter.class.getSimpleName();
    }
}
