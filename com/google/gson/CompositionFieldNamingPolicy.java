// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.lang.reflect.Type;

abstract class CompositionFieldNamingPolicy extends RecursiveFieldNamingPolicy
{
    private final RecursiveFieldNamingPolicy[] fieldPolicies;
    
    public CompositionFieldNamingPolicy(final RecursiveFieldNamingPolicy... fieldNamingPolicies) {
        if (fieldNamingPolicies == null) {
            throw new NullPointerException("naming policies can not be null.");
        }
        this.fieldPolicies = fieldNamingPolicies;
    }
    
    @Override
    protected String translateName(String target, final Type fieldType, final Collection<Annotation> annotations) {
        for (final RecursiveFieldNamingPolicy policy : this.fieldPolicies) {
            target = policy.translateName(target, fieldType, annotations);
        }
        return target;
    }
}
