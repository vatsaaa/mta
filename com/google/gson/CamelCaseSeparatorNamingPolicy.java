// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.lang.reflect.Type;
import com.google.gson.internal.$Gson$Preconditions;

final class CamelCaseSeparatorNamingPolicy extends RecursiveFieldNamingPolicy
{
    private final String separatorString;
    
    public CamelCaseSeparatorNamingPolicy(final String separatorString) {
        $Gson$Preconditions.checkNotNull(separatorString);
        $Gson$Preconditions.checkArgument(!"".equals(separatorString));
        this.separatorString = separatorString;
    }
    
    @Override
    protected String translateName(final String target, final Type fieldType, final Collection<Annotation> annnotations) {
        final StringBuilder translation = new StringBuilder();
        for (int i = 0; i < target.length(); ++i) {
            final char character = target.charAt(i);
            if (Character.isUpperCase(character) && translation.length() != 0) {
                translation.append(this.separatorString);
            }
            translation.append(character);
        }
        return translation.toString();
    }
}
