// 
// Decompiled by Procyon v0.5.36
// 

package com.google.gson;

final class LowerCamelCaseSeparatorNamingPolicy extends CompositionFieldNamingPolicy
{
    public LowerCamelCaseSeparatorNamingPolicy(final String separatorString) {
        super(new RecursiveFieldNamingPolicy[] { new CamelCaseSeparatorNamingPolicy(separatorString), new LowerCaseNamingPolicy() });
    }
}
