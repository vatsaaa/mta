// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import org.jdom.JDOMException;
import org.jdom.output.DOMOutputter;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;
import java.io.File;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import com.sun.syndication.feed.WireFeed;
import java.util.List;
import com.sun.syndication.io.impl.FeedGenerators;

public class WireFeedOutput
{
    private static final FeedGenerators GENERATORS;
    
    public static List getSupportedFeedTypes() {
        return WireFeedOutput.GENERATORS.getSupportedFeedTypes();
    }
    
    public String outputString(final WireFeed feed) throws IllegalArgumentException, FeedException {
        final Document doc = this.outputJDom(feed);
        final String encoding = feed.getEncoding();
        final Format format = Format.getPrettyFormat();
        if (encoding != null) {
            format.setEncoding(encoding);
        }
        final XMLOutputter outputter = new XMLOutputter(format);
        return outputter.outputString(doc);
    }
    
    public void output(final WireFeed feed, final File file) throws IllegalArgumentException, IOException, FeedException {
        final Writer writer = new FileWriter(file);
        this.output(feed, writer);
        writer.close();
    }
    
    public void output(final WireFeed feed, final Writer writer) throws IllegalArgumentException, IOException, FeedException {
        final Document doc = this.outputJDom(feed);
        final String encoding = feed.getEncoding();
        final Format format = Format.getPrettyFormat();
        if (encoding != null) {
            format.setEncoding(encoding);
        }
        final XMLOutputter outputter = new XMLOutputter(format);
        outputter.output(doc, writer);
    }
    
    public org.w3c.dom.Document outputW3CDom(final WireFeed feed) throws IllegalArgumentException, FeedException {
        final Document doc = this.outputJDom(feed);
        final DOMOutputter outputter = new DOMOutputter();
        try {
            return outputter.output(doc);
        }
        catch (JDOMException jdomEx) {
            throw new FeedException("Could not create DOM", jdomEx);
        }
    }
    
    public Document outputJDom(final WireFeed feed) throws IllegalArgumentException, FeedException {
        final String type = feed.getFeedType();
        final WireFeedGenerator generator = WireFeedOutput.GENERATORS.getGenerator(type);
        if (generator == null) {
            throw new IllegalArgumentException("Invalid feed type [" + type + "]");
        }
        if (!generator.getType().equals(type)) {
            throw new IllegalArgumentException("WireFeedOutput type[" + type + "] and WireFeed type [" + type + "] don't match");
        }
        return generator.generate(feed);
    }
    
    static {
        GENERATORS = new FeedGenerators();
    }
}
