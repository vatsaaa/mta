// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import com.sun.syndication.feed.WireFeed;
import org.jdom.Document;

public interface WireFeedParser
{
    String getType();
    
    boolean isMyType(final Document p0);
    
    WireFeed parse(final Document p0, final boolean p1) throws IllegalArgumentException, FeedException;
}
