// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import org.w3c.dom.Document;
import java.io.Writer;
import java.io.IOException;
import java.io.File;
import com.sun.syndication.feed.synd.SyndFeed;

public class SyndFeedOutput
{
    private WireFeedOutput _feedOutput;
    
    public SyndFeedOutput() {
        this._feedOutput = new WireFeedOutput();
    }
    
    public String outputString(final SyndFeed feed) throws FeedException {
        return this._feedOutput.outputString(feed.createWireFeed());
    }
    
    public void output(final SyndFeed feed, final File file) throws IOException, FeedException {
        this._feedOutput.output(feed.createWireFeed(), file);
    }
    
    public void output(final SyndFeed feed, final Writer writer) throws IOException, FeedException {
        this._feedOutput.output(feed.createWireFeed(), writer);
    }
    
    public Document outputW3CDom(final SyndFeed feed) throws FeedException {
        return this._feedOutput.outputW3CDom(feed.createWireFeed());
    }
    
    public org.jdom.Document outputJDom(final SyndFeed feed) throws FeedException {
        return this._feedOutput.outputJDom(feed.createWireFeed());
    }
}
