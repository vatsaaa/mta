// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import org.jdom.input.JDOMParseException;

public class ParsingFeedException extends FeedException
{
    public ParsingFeedException(final String msg) {
        super(msg);
    }
    
    public ParsingFeedException(final String msg, final Throwable rootCause) {
        super(msg, rootCause);
    }
    
    public int getLineNumber() {
        return (this.getCause() instanceof JDOMParseException) ? ((JDOMParseException)this.getCause()).getLineNumber() : -1;
    }
    
    public int getColumnNumber() {
        return (this.getCause() instanceof JDOMParseException) ? ((JDOMParseException)this.getCause()).getColumnNumber() : -1;
    }
}
