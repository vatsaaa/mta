// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.util.regex.Matcher;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URL;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.text.MessageFormat;
import java.util.regex.Pattern;
import java.io.Reader;

public class XmlReader extends Reader
{
    private static final int PUSHBACK_MAX_SIZE = 4096;
    private static final String UTF_8 = "UTF-8";
    private static final String US_ASCII = "US-ASCII";
    private static final String UTF_16BE = "UTF-16BE";
    private static final String UTF_16LE = "UTF-16LE";
    private static final String UTF_16 = "UTF-16";
    private Reader _reader;
    private String _encoding;
    private static final Pattern CHARSET_PATTERN;
    private static final Pattern ENCODING_PATTERN;
    private static final MessageFormat RAW_EX_1;
    private static final MessageFormat RAW_EX_2;
    private static final MessageFormat HTTP_EX_1;
    private static final MessageFormat HTTP_EX_2;
    private static final MessageFormat HTTP_EX_3;
    
    public XmlReader(final File file) throws IOException {
        this(new FileInputStream(file));
    }
    
    public XmlReader(final InputStream is) throws IOException {
        this(is, true);
    }
    
    public XmlReader(final InputStream is, final boolean lenient) throws IOException, XmlReaderException {
        try {
            this.doRawStream(is, lenient);
        }
        catch (XmlReaderException ex) {
            if (!lenient) {
                throw ex;
            }
            this.doLenientDetection(null, ex);
        }
    }
    
    public XmlReader(final URL url) throws IOException {
        this(url.openConnection());
    }
    
    public XmlReader(final URLConnection conn) throws IOException {
        final boolean lenient = true;
        if (conn instanceof HttpURLConnection) {
            try {
                this.doHttpStream(conn.getInputStream(), conn.getContentType(), lenient);
            }
            catch (XmlReaderException ex) {
                this.doLenientDetection(conn.getContentType(), ex);
            }
        }
        else if (conn.getContentType() != null) {
            try {
                this.doHttpStream(conn.getInputStream(), conn.getContentType(), lenient);
            }
            catch (XmlReaderException ex) {
                this.doLenientDetection(conn.getContentType(), ex);
            }
        }
        else {
            try {
                this.doRawStream(conn.getInputStream(), lenient);
            }
            catch (XmlReaderException ex) {
                this.doLenientDetection(null, ex);
            }
        }
    }
    
    public XmlReader(final InputStream is, final String httpContentType) throws IOException {
        this(is, httpContentType, true);
    }
    
    public XmlReader(final InputStream is, final String httpContentType, final boolean lenient) throws IOException, XmlReaderException {
        try {
            this.doHttpStream(is, httpContentType, lenient);
        }
        catch (XmlReaderException ex) {
            if (!lenient) {
                throw ex;
            }
            this.doLenientDetection(httpContentType, ex);
        }
    }
    
    private void doLenientDetection(String httpContentType, XmlReaderException ex) throws IOException {
        if (httpContentType != null && httpContentType.startsWith("text/html")) {
            httpContentType = httpContentType.substring("text/html".length());
            httpContentType = "text/xml" + httpContentType;
            try {
                this.doHttpStream(ex.getInputStream(), httpContentType, true);
                ex = null;
            }
            catch (XmlReaderException ex2) {
                ex = ex2;
            }
        }
        if (ex != null) {
            String encoding = ex.getXmlEncoding();
            if (encoding == null) {
                encoding = ex.getContentTypeEncoding();
            }
            if (encoding == null) {
                encoding = "UTF-8";
            }
            this.prepareReader(ex.getInputStream(), encoding);
        }
    }
    
    public String getEncoding() {
        return this._encoding;
    }
    
    public int read(final char[] buf, final int offset, final int len) throws IOException {
        return this._reader.read(buf, offset, len);
    }
    
    public void close() throws IOException {
        this._reader.close();
    }
    
    private void doRawStream(final InputStream is, final boolean lenient) throws IOException {
        final PushbackInputStream pis = new PushbackInputStream(is, 4096);
        final String bomEnc = getBOMEncoding(pis);
        final String xmlGuessEnc = getXMLGuessEncoding(pis);
        final String xmlEnc = getXmlProlog(pis, xmlGuessEnc);
        final String encoding = calculateRawEncoding(bomEnc, xmlGuessEnc, xmlEnc, pis);
        this.prepareReader(pis, encoding);
    }
    
    private void doHttpStream(final InputStream is, final String httpContentType, final boolean lenient) throws IOException {
        final PushbackInputStream pis = new PushbackInputStream(is, 4096);
        final String cTMime = getContentTypeMime(httpContentType);
        final String cTEnc = getContentTypeEncoding(httpContentType);
        final String bomEnc = getBOMEncoding(pis);
        final String xmlGuessEnc = getXMLGuessEncoding(pis);
        final String xmlEnc = getXmlProlog(pis, xmlGuessEnc);
        final String encoding = calculateHttpEncoding(cTMime, cTEnc, bomEnc, xmlGuessEnc, xmlEnc, pis, lenient);
        this.prepareReader(pis, encoding);
    }
    
    private void prepareReader(final InputStream is, final String encoding) throws IOException {
        this._reader = new InputStreamReader(is, encoding);
        this._encoding = encoding;
    }
    
    private static String calculateRawEncoding(final String bomEnc, final String xmlGuessEnc, final String xmlEnc, final InputStream is) throws IOException {
        if (bomEnc == null) {
            if (xmlGuessEnc == null || xmlEnc == null) {
                final String encoding = "UTF-8";
            }
            else if (xmlEnc.equals("UTF-16") && (xmlGuessEnc.equals("UTF-16BE") || xmlGuessEnc.equals("UTF-16LE"))) {
                final String encoding = xmlGuessEnc;
            }
            else {
                final String encoding = xmlEnc;
            }
        }
        else if (bomEnc.equals("UTF-8")) {
            if (xmlGuessEnc != null && !xmlGuessEnc.equals("UTF-8")) {
                throw new XmlReaderException(XmlReader.RAW_EX_1.format(new Object[] { bomEnc, xmlGuessEnc, xmlEnc }), bomEnc, xmlGuessEnc, xmlEnc, is);
            }
            if (xmlEnc != null && !xmlEnc.equals("UTF-8")) {
                throw new XmlReaderException(XmlReader.RAW_EX_1.format(new Object[] { bomEnc, xmlGuessEnc, xmlEnc }), bomEnc, xmlGuessEnc, xmlEnc, is);
            }
            final String encoding = "UTF-8";
        }
        else {
            if (!bomEnc.equals("UTF-16BE") && !bomEnc.equals("UTF-16LE")) {
                throw new XmlReaderException(XmlReader.RAW_EX_2.format(new Object[] { bomEnc, xmlGuessEnc, xmlEnc }), bomEnc, xmlGuessEnc, xmlEnc, is);
            }
            if (xmlGuessEnc != null && !xmlGuessEnc.equals(bomEnc)) {
                throw new IOException(XmlReader.RAW_EX_1.format(new Object[] { bomEnc, xmlGuessEnc, xmlEnc }));
            }
            if (xmlEnc != null && !xmlEnc.equals("UTF-16") && !xmlEnc.equals(bomEnc)) {
                throw new XmlReaderException(XmlReader.RAW_EX_1.format(new Object[] { bomEnc, xmlGuessEnc, xmlEnc }), bomEnc, xmlGuessEnc, xmlEnc, is);
            }
            final String encoding = bomEnc;
        }
        return;
    }
    
    private static String calculateHttpEncoding(final String cTMime, final String cTEnc, final String bomEnc, final String xmlGuessEnc, final String xmlEnc, final InputStream is, final boolean lenient) throws IOException {
        if (lenient & xmlEnc != null) {
            final String encoding = xmlEnc;
        }
        else {
            final boolean appXml = isAppXml(cTMime);
            final boolean textXml = isTextXml(cTMime);
            if (!appXml && !textXml) {
                throw new XmlReaderException(XmlReader.HTTP_EX_3.format(new Object[] { cTMime, cTEnc, bomEnc, xmlGuessEnc, xmlEnc }), cTMime, cTEnc, bomEnc, xmlGuessEnc, xmlEnc, is);
            }
            if (cTEnc == null) {
                if (appXml) {
                    final String encoding = calculateRawEncoding(bomEnc, xmlGuessEnc, xmlEnc, is);
                }
                else {
                    final String encoding = "US-ASCII";
                }
            }
            else {
                if (bomEnc != null && (cTEnc.equals("UTF-16BE") || cTEnc.equals("UTF-16LE"))) {
                    throw new XmlReaderException(XmlReader.HTTP_EX_1.format(new Object[] { cTMime, cTEnc, bomEnc, xmlGuessEnc, xmlEnc }), cTMime, cTEnc, bomEnc, xmlGuessEnc, xmlEnc, is);
                }
                if (cTEnc.equals("UTF-16")) {
                    if (bomEnc == null || !bomEnc.startsWith("UTF-16")) {
                        throw new XmlReaderException(XmlReader.HTTP_EX_2.format(new Object[] { cTMime, cTEnc, bomEnc, xmlGuessEnc, xmlEnc }), cTMime, cTEnc, bomEnc, xmlGuessEnc, xmlEnc, is);
                    }
                    final String encoding = bomEnc;
                }
                else {
                    final String encoding = cTEnc;
                }
            }
        }
        return;
    }
    
    private static String getContentTypeMime(final String httpContentType) {
        String mime = null;
        if (httpContentType != null) {
            final int i = httpContentType.indexOf(";");
            mime = ((i == -1) ? httpContentType : httpContentType.substring(0, i)).trim();
        }
        return mime;
    }
    
    private static String getContentTypeEncoding(final String httpContentType) {
        String encoding = null;
        if (httpContentType != null) {
            final int i = httpContentType.indexOf(";");
            if (i > -1) {
                final String postMime = httpContentType.substring(i + 1);
                final Matcher m = XmlReader.CHARSET_PATTERN.matcher(postMime);
                encoding = (m.find() ? m.group(1) : null);
                encoding = ((encoding != null) ? encoding.toUpperCase() : null);
            }
        }
        return encoding;
    }
    
    private static String getBOMEncoding(final PushbackInputStream is) throws IOException {
        String encoding = null;
        final int[] bytes = { is.read(), is.read(), is.read() };
        if (bytes[0] == 254 && bytes[1] == 255) {
            encoding = "UTF-16BE";
            is.unread(bytes[2]);
        }
        else if (bytes[0] == 255 && bytes[1] == 254) {
            encoding = "UTF-16LE";
            is.unread(bytes[2]);
        }
        else if (bytes[0] == 239 && bytes[1] == 187 && bytes[2] == 191) {
            encoding = "UTF-8";
        }
        else {
            for (int i = bytes.length - 1; i >= 0; --i) {
                is.unread(bytes[i]);
            }
        }
        return encoding;
    }
    
    private static String getXMLGuessEncoding(final PushbackInputStream is) throws IOException {
        String encoding = null;
        final int[] bytes = { is.read(), is.read(), is.read(), is.read() };
        for (int i = bytes.length - 1; i >= 0; --i) {
            is.unread(bytes[i]);
        }
        if (bytes[0] == 0 && bytes[1] == 60 && bytes[2] == 0 && bytes[3] == 63) {
            encoding = "UTF-16BE";
        }
        else if (bytes[0] == 60 && bytes[1] == 0 && bytes[2] == 63 && bytes[3] == 0) {
            encoding = "UTF-16LE";
        }
        else if (bytes[0] == 60 && bytes[1] == 63 && bytes[2] == 120 && bytes[3] == 109) {
            encoding = "UTF-8";
        }
        return encoding;
    }
    
    private static String getXmlProlog(final PushbackInputStream is, final String guessedEnc) throws IOException {
        String encoding = null;
        if (guessedEnc != null) {
            final byte[] bytes = new byte[4096];
            int offset = 0;
            for (int max = 4096, c = is.read(bytes, offset, max); c != -1 && offset < 4096; offset += c, max -= c, c = is.read(bytes, offset, max)) {}
            final int bytesRead = offset;
            if (bytesRead > 0) {
                is.unread(bytes, 0, bytesRead);
                final Reader reader = new InputStreamReader(new ByteArrayInputStream(bytes, 0, bytesRead), guessedEnc);
                final BufferedReader br = new BufferedReader(reader);
                final StringBuffer prolog = new StringBuffer(4096);
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    prolog.append(line).append("\n");
                }
                final Matcher m = XmlReader.ENCODING_PATTERN.matcher(prolog);
                if (m.find()) {
                    encoding = m.group(1).toUpperCase();
                    encoding = encoding.substring(1, encoding.length() - 1);
                }
            }
        }
        return encoding;
    }
    
    private static boolean isAppXml(final String mime) {
        return mime != null && (mime.equals("application/xml") || mime.equals("application/xml-dtd") || mime.equals("application/xml-external-parsed-entity") || (mime.startsWith("application/") && mime.endsWith("+xml")));
    }
    
    private static boolean isTextXml(final String mime) {
        return mime != null && (mime.equals("text/xml") || mime.equals("text/xml-external-parsed-entity") || (mime.startsWith("text/") && mime.endsWith("+xml")));
    }
    
    static {
        CHARSET_PATTERN = Pattern.compile("charset=([.[^; ]]*)");
        ENCODING_PATTERN = Pattern.compile("<\\?xml.*encoding[\\s]*=[\\s]*((?:\".[^\"]*\")|(?:'.[^']*')).*\\?>", 8);
        RAW_EX_1 = new MessageFormat("Invalid encoding, BOM [{0}] XML guess [{1}] XML prolog [{2}] encoding mismatch");
        RAW_EX_2 = new MessageFormat("Invalid encoding, BOM [{0}] XML guess [{1}] XML prolog [{2}] unknown BOM");
        HTTP_EX_1 = new MessageFormat("Invalid encoding, CT-MIME [{0}] CT-Enc [{1}] BOM [{2}] XML guess [{3}] XML prolog [{4}], BOM must be NULL");
        HTTP_EX_2 = new MessageFormat("Invalid encoding, CT-MIME [{0}] CT-Enc [{1}] BOM [{2}] XML guess [{3}] XML prolog [{4}], encoding mismatch");
        HTTP_EX_3 = new MessageFormat("Invalid encoding, CT-MIME [{0}] CT-Enc [{1}] BOM [{2}] XML guess [{3}] XML prolog [{4}], Invalid MIME");
    }
}
