// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

public class FeedException extends Exception
{
    public FeedException(final String msg) {
        super(msg);
    }
    
    public FeedException(final String msg, final Throwable rootCause) {
        super(msg, rootCause);
    }
}
