// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.xml.sax.XMLReader;
import org.jdom.JDOMException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXNotRecognizedException;
import org.jdom.input.DOMBuilder;
import org.jdom.Document;
import org.jdom.input.JDOMParseException;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.Reader;
import com.sun.syndication.io.impl.XmlFixerReader;
import java.io.FileReader;
import com.sun.syndication.feed.WireFeed;
import java.io.File;
import java.util.List;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import com.sun.syndication.io.impl.FeedParsers;

public class WireFeedInput
{
    private static FeedParsers FEED_PARSERS;
    private static final InputSource EMPTY_INPUTSOURCE;
    private static final EntityResolver RESOLVER;
    private boolean _validate;
    private boolean _xmlHealerOn;
    
    public static List getSupportedFeedTypes() {
        return WireFeedInput.FEED_PARSERS.getSupportedFeedTypes();
    }
    
    public WireFeedInput() {
        this(false);
    }
    
    public WireFeedInput(final boolean validate) {
        this._validate = false;
        this._xmlHealerOn = true;
    }
    
    public void setXmlHealerOn(final boolean heals) {
        this._xmlHealerOn = heals;
    }
    
    public boolean getXmlHealerOn() {
        return this._xmlHealerOn;
    }
    
    public WireFeed build(final File file) throws FileNotFoundException, IOException, IllegalArgumentException, FeedException {
        Reader reader = new FileReader(file);
        if (this._xmlHealerOn) {
            reader = new XmlFixerReader(reader);
        }
        final WireFeed feed = this.build(reader);
        reader.close();
        return feed;
    }
    
    public WireFeed build(Reader reader) throws IllegalArgumentException, FeedException {
        final SAXBuilder saxBuilder = this.createSAXBuilder();
        try {
            if (this._xmlHealerOn) {
                reader = new XmlFixerReader(reader);
            }
            final Document document = saxBuilder.build(reader);
            return this.build(document);
        }
        catch (JDOMParseException ex) {
            throw new ParsingFeedException("Invalid XML: " + ex.getMessage(), ex);
        }
        catch (Exception ex2) {
            throw new ParsingFeedException("Invalid XML", ex2);
        }
    }
    
    public WireFeed build(final InputSource is) throws IllegalArgumentException, FeedException {
        final SAXBuilder saxBuilder = this.createSAXBuilder();
        try {
            final Document document = saxBuilder.build(is);
            return this.build(document);
        }
        catch (JDOMParseException ex) {
            throw new ParsingFeedException("Invalid XML: " + ex.getMessage(), ex);
        }
        catch (Exception ex2) {
            throw new ParsingFeedException("Invalid XML", ex2);
        }
    }
    
    public WireFeed build(final org.w3c.dom.Document document) throws IllegalArgumentException, FeedException {
        final DOMBuilder domBuilder = new DOMBuilder();
        try {
            final Document jdomDoc = domBuilder.build(document);
            return this.build(jdomDoc);
        }
        catch (Exception ex) {
            throw new ParsingFeedException("Invalid XML", ex);
        }
    }
    
    public WireFeed build(final Document document) throws IllegalArgumentException, FeedException {
        final WireFeedParser parser = WireFeedInput.FEED_PARSERS.getParserFor(document);
        if (parser == null) {
            throw new IllegalArgumentException("Invalid document");
        }
        return parser.parse(document, this._validate);
    }
    
    protected SAXBuilder createSAXBuilder() {
        final SAXBuilder saxBuilder = new SAXBuilder(this._validate);
        saxBuilder.setEntityResolver(WireFeedInput.RESOLVER);
        try {
            final XMLReader parser = saxBuilder.createParser();
            try {
                parser.setFeature("http://xml.org/sax/features/external-general-entities", false);
                saxBuilder.setFeature("http://xml.org/sax/features/external-general-entities", false);
            }
            catch (SAXNotRecognizedException e) {}
            catch (SAXNotSupportedException ex) {}
            try {
                parser.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                saxBuilder.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            }
            catch (SAXNotRecognizedException e) {}
            catch (SAXNotSupportedException ex2) {}
            try {
                parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                saxBuilder.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            }
            catch (SAXNotRecognizedException e) {}
            catch (SAXNotSupportedException ex3) {}
        }
        catch (JDOMException e2) {
            throw new IllegalStateException("JDOM could not create a SAX parser");
        }
        saxBuilder.setExpandEntities(false);
        return saxBuilder;
    }
    
    static {
        WireFeedInput.FEED_PARSERS = new FeedParsers();
        EMPTY_INPUTSOURCE = new InputSource(new ByteArrayInputStream(new byte[0]));
        RESOLVER = new EmptyEntityResolver();
    }
    
    private static class EmptyEntityResolver implements EntityResolver
    {
        public InputSource resolveEntity(final String publicId, final String systemId) {
            if (systemId != null && systemId.endsWith(".dtd")) {
                return WireFeedInput.EMPTY_INPUTSOURCE;
            }
            return null;
        }
    }
}
