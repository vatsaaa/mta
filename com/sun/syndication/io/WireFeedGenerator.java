// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import org.jdom.Document;
import com.sun.syndication.feed.WireFeed;

public interface WireFeedGenerator
{
    String getType();
    
    Document generate(final WireFeed p0) throws IllegalArgumentException, FeedException;
}
