// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import java.io.Reader;
import java.io.IOException;
import java.io.FileNotFoundException;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import java.io.File;

public class SyndFeedInput
{
    private WireFeedInput _feedInput;
    
    public SyndFeedInput() {
        this(false);
    }
    
    public SyndFeedInput(final boolean validate) {
        this._feedInput = new WireFeedInput(validate);
    }
    
    public void setXmlHealerOn(final boolean heals) {
        this._feedInput.setXmlHealerOn(heals);
    }
    
    public boolean getXmlHealerOn() {
        return this._feedInput.getXmlHealerOn();
    }
    
    public SyndFeed build(final File file) throws FileNotFoundException, IOException, IllegalArgumentException, FeedException {
        return new SyndFeedImpl(this._feedInput.build(file));
    }
    
    public SyndFeed build(final Reader reader) throws IllegalArgumentException, FeedException {
        return new SyndFeedImpl(this._feedInput.build(reader));
    }
    
    public SyndFeed build(final InputSource is) throws IllegalArgumentException, FeedException {
        return new SyndFeedImpl(this._feedInput.build(is));
    }
    
    public SyndFeed build(final Document document) throws IllegalArgumentException, FeedException {
        return new SyndFeedImpl(this._feedInput.build(document));
    }
    
    public SyndFeed build(final org.jdom.Document document) throws IllegalArgumentException, FeedException {
        return new SyndFeedImpl(this._feedInput.build(document));
    }
}
