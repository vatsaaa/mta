// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io;

import org.jdom.Element;
import com.sun.syndication.feed.module.Module;
import java.util.Set;

public interface ModuleGenerator
{
    String getNamespaceUri();
    
    Set getNamespaces();
    
    void generate(final Module p0, final Element p1);
}
