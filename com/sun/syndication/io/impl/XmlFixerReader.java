// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Pattern;
import java.util.Map;
import java.io.Reader;

public class XmlFixerReader extends Reader
{
    protected Reader in;
    private boolean trimmed;
    private StringBuffer _buffer;
    private int _bufferPos;
    private int _state;
    private static Map CODED_ENTITIES;
    private static Pattern ENTITIES_PATTERN;
    
    public static void main(final String[] args) throws Exception {
        Reader r = new InputStreamReader(new URL(args[0]).openStream());
        r = new XmlFixerReader(r);
        final BufferedReader br = new BufferedReader(r);
        for (String l = br.readLine(); l != null; l = br.readLine()) {
            System.out.println(l);
        }
    }
    
    public XmlFixerReader(final Reader in) {
        super(in);
        this._state = 0;
        this.in = in;
        this._buffer = new StringBuffer();
        this._state = 0;
    }
    
    private boolean trimStream() throws IOException {
        boolean hasContent = true;
        int state = 0;
        final Object o;
        do {
            switch (state) {
                case 0: {
                    final int c = this.in.read();
                    if (c == -1) {
                        final boolean loop = false;
                        hasContent = false;
                        continue;
                    }
                    if (c == 32 || c == 10) {
                        final boolean loop = true;
                        continue;
                    }
                    if (c == 60) {
                        state = 1;
                        this._buffer.setLength(0);
                        this._bufferPos = 0;
                        this._buffer.append((char)c);
                        final boolean loop = true;
                        continue;
                    }
                    this._buffer.setLength(0);
                    this._bufferPos = 0;
                    this._buffer.append((char)c);
                    final boolean loop = false;
                    hasContent = true;
                    this._state = 3;
                    continue;
                }
                case 1: {
                    final int c = this.in.read();
                    if (c == -1) {
                        final boolean loop = false;
                        hasContent = true;
                        this._state = 3;
                        continue;
                    }
                    if (c != 33) {
                        this._buffer.append((char)c);
                        this._state = 3;
                        final boolean loop = false;
                        hasContent = true;
                        this._state = 3;
                        continue;
                    }
                    this._buffer.append((char)c);
                    state = 2;
                    final boolean loop = true;
                    continue;
                }
                case 2: {
                    final int c = this.in.read();
                    if (c == -1) {
                        final boolean loop = false;
                        hasContent = true;
                        this._state = 3;
                        continue;
                    }
                    if (c == 45) {
                        this._buffer.append((char)c);
                        state = 3;
                        final boolean loop = true;
                        continue;
                    }
                    this._buffer.append((char)c);
                    final boolean loop = false;
                    hasContent = true;
                    this._state = 3;
                    continue;
                }
                case 3: {
                    final int c = this.in.read();
                    if (c == -1) {
                        final boolean loop = false;
                        hasContent = true;
                        this._state = 3;
                        continue;
                    }
                    if (c == 45) {
                        this._buffer.append((char)c);
                        state = 4;
                        final boolean loop = true;
                        continue;
                    }
                    this._buffer.append((char)c);
                    final boolean loop = false;
                    hasContent = true;
                    this._state = 3;
                    continue;
                }
                case 4: {
                    final int c = this.in.read();
                    if (c == -1) {
                        final boolean loop = false;
                        hasContent = true;
                        this._state = 3;
                        continue;
                    }
                    if (c != 45) {
                        this._buffer.append((char)c);
                        final boolean loop = true;
                        continue;
                    }
                    this._buffer.append((char)c);
                    state = 5;
                    final boolean loop = true;
                    continue;
                }
                case 5: {
                    final int c = this.in.read();
                    if (c == -1) {
                        final boolean loop = false;
                        hasContent = true;
                        this._state = 3;
                        continue;
                    }
                    if (c != 45) {
                        this._buffer.append((char)c);
                        final boolean loop = true;
                        state = 4;
                        continue;
                    }
                    this._buffer.append((char)c);
                    state = 6;
                    final boolean loop = true;
                    continue;
                }
                case 6: {
                    final int c = this.in.read();
                    if (c == -1) {
                        final boolean loop = false;
                        hasContent = true;
                        this._state = 3;
                        continue;
                    }
                    if (c != 62) {
                        this._buffer.append((char)c);
                        final boolean loop = true;
                        state = 4;
                        continue;
                    }
                    this._buffer.setLength(0);
                    state = 0;
                    final boolean loop = true;
                    continue;
                }
                default: {
                    throw new IOException("It shouldn't happen");
                }
            }
        } while (o != 0);
        return hasContent;
    }
    
    public int read() throws IOException {
        if (!this.trimmed) {
            this.trimmed = true;
            if (!this.trimStream()) {
                return -1;
            }
        }
        final Object o;
        do {
            switch (this._state) {
                case 0: {
                    final int c = this.in.read();
                    if (c <= -1) {
                        final boolean loop = false;
                        continue;
                    }
                    if (c == 38) {
                        this._state = 1;
                        this._buffer.setLength(0);
                        this._bufferPos = 0;
                        this._buffer.append((char)c);
                        this._state = 1;
                        final boolean loop = true;
                        continue;
                    }
                    final boolean loop = false;
                    continue;
                }
                case 1: {
                    final int c = this.in.read();
                    if (c <= -1) {
                        this._state = 3;
                        final boolean loop = true;
                        continue;
                    }
                    if (c == 59) {
                        this._buffer.append((char)c);
                        this._state = 2;
                        final boolean loop = true;
                        continue;
                    }
                    if ((c >= 97 && c <= 122) || (c >= 65 && c <= 90) || c == 35 || (c >= 48 && c <= 57)) {
                        this._buffer.append((char)c);
                        final boolean loop = true;
                        continue;
                    }
                    this._buffer.append((char)c);
                    this._state = 3;
                    final boolean loop = true;
                    continue;
                }
                case 2: {
                    final int c = 0;
                    final String literalEntity = this._buffer.toString();
                    final String codedEntity = XmlFixerReader.CODED_ENTITIES.get(literalEntity);
                    if (codedEntity != null) {
                        this._buffer.setLength(0);
                        this._buffer.append(codedEntity);
                    }
                    this._state = 3;
                    final boolean loop = true;
                    continue;
                }
                case 3: {
                    if (this._bufferPos < this._buffer.length()) {
                        final int c = this._buffer.charAt(this._bufferPos++);
                        final boolean loop = false;
                        continue;
                    }
                    final int c = 0;
                    this._state = 0;
                    final boolean loop = true;
                    continue;
                }
                default: {
                    throw new IOException("It shouldn't happen");
                }
            }
        } while (o != 0);
        return;
    }
    
    public int read(final char[] buffer, final int offset, final int len) throws IOException {
        int charsRead = 0;
        int c = this.read();
        if (c == -1) {
            return -1;
        }
        buffer[offset + charsRead++] = (char)c;
        while (charsRead < len && (c = this.read()) > -1) {
            buffer[offset + charsRead++] = (char)c;
        }
        return charsRead;
    }
    
    public long skip(final long n) throws IOException {
        if (n == 0L) {
            return 0L;
        }
        if (n < 0L) {
            throw new IllegalArgumentException("'n' cannot be negative");
        }
        int c;
        long counter;
        for (c = this.read(), counter = 1L; c > -1 && counter < n; c = this.read(), ++counter) {}
        return counter;
    }
    
    public boolean ready() throws IOException {
        return this._state != 0 || this.in.ready();
    }
    
    public boolean markSupported() {
        return false;
    }
    
    public void mark(final int readAheadLimit) throws IOException {
        throw new IOException("Stream does not support mark");
    }
    
    public void reset() throws IOException {
        throw new IOException("Stream does not support mark");
    }
    
    public void close() throws IOException {
        this.in.close();
    }
    
    public String processHtmlEntities(final String s) {
        if (s.indexOf(38) == -1) {
            return s;
        }
        final StringBuffer sb = new StringBuffer(s.length());
        int pos = 0;
        while (pos < s.length()) {
            String chunck = s.substring(pos);
            final Matcher m = XmlFixerReader.ENTITIES_PATTERN.matcher(chunck);
            if (m.find()) {
                final int b = pos + m.start();
                final int e = pos + m.end();
                if (b > pos) {
                    sb.append(s.substring(pos, b));
                    pos = b;
                }
                chunck = s.substring(pos, e);
                String codedEntity = XmlFixerReader.CODED_ENTITIES.get(chunck);
                if (codedEntity == null) {
                    codedEntity = chunck;
                }
                sb.append(codedEntity);
                pos = e;
            }
            else {
                sb.append(chunck);
                pos += chunck.length();
            }
        }
        return sb.toString();
    }
    
    static {
        (XmlFixerReader.CODED_ENTITIES = new HashMap()).put("&nbsp;", "&#160;");
        XmlFixerReader.CODED_ENTITIES.put("&iexcl;", "&#161;");
        XmlFixerReader.CODED_ENTITIES.put("&cent;", "&#162;");
        XmlFixerReader.CODED_ENTITIES.put("&pound;", "&#163;");
        XmlFixerReader.CODED_ENTITIES.put("&curren;", "&#164;");
        XmlFixerReader.CODED_ENTITIES.put("&yen;", "&#165;");
        XmlFixerReader.CODED_ENTITIES.put("&brvbar;", "&#166;");
        XmlFixerReader.CODED_ENTITIES.put("&sect;", "&#167;");
        XmlFixerReader.CODED_ENTITIES.put("&uml;", "&#168;");
        XmlFixerReader.CODED_ENTITIES.put("&copy;", "&#169;");
        XmlFixerReader.CODED_ENTITIES.put("&ordf;", "&#170;");
        XmlFixerReader.CODED_ENTITIES.put("&laquo;", "&#171;");
        XmlFixerReader.CODED_ENTITIES.put("&not;", "&#172;");
        XmlFixerReader.CODED_ENTITIES.put("&shy;", "&#173;");
        XmlFixerReader.CODED_ENTITIES.put("&reg;", "&#174;");
        XmlFixerReader.CODED_ENTITIES.put("&macr;", "&#175;");
        XmlFixerReader.CODED_ENTITIES.put("&deg;", "&#176;");
        XmlFixerReader.CODED_ENTITIES.put("&plusmn;", "&#177;");
        XmlFixerReader.CODED_ENTITIES.put("&sup2;", "&#178;");
        XmlFixerReader.CODED_ENTITIES.put("&sup3;", "&#179;");
        XmlFixerReader.CODED_ENTITIES.put("&acute;", "&#180;");
        XmlFixerReader.CODED_ENTITIES.put("&micro;", "&#181;");
        XmlFixerReader.CODED_ENTITIES.put("&para;", "&#182;");
        XmlFixerReader.CODED_ENTITIES.put("&middot;", "&#183;");
        XmlFixerReader.CODED_ENTITIES.put("&cedil;", "&#184;");
        XmlFixerReader.CODED_ENTITIES.put("&sup1;", "&#185;");
        XmlFixerReader.CODED_ENTITIES.put("&ordm;", "&#186;");
        XmlFixerReader.CODED_ENTITIES.put("&raquo;", "&#187;");
        XmlFixerReader.CODED_ENTITIES.put("&frac14;", "&#188;");
        XmlFixerReader.CODED_ENTITIES.put("&frac12;", "&#189;");
        XmlFixerReader.CODED_ENTITIES.put("&frac34;", "&#190;");
        XmlFixerReader.CODED_ENTITIES.put("&iquest;", "&#191;");
        XmlFixerReader.CODED_ENTITIES.put("&Agrave;", "&#192;");
        XmlFixerReader.CODED_ENTITIES.put("&Aacute;", "&#193;");
        XmlFixerReader.CODED_ENTITIES.put("&Acirc;", "&#194;");
        XmlFixerReader.CODED_ENTITIES.put("&Atilde;", "&#195;");
        XmlFixerReader.CODED_ENTITIES.put("&Auml;", "&#196;");
        XmlFixerReader.CODED_ENTITIES.put("&Aring;", "&#197;");
        XmlFixerReader.CODED_ENTITIES.put("&AElig;", "&#198;");
        XmlFixerReader.CODED_ENTITIES.put("&Ccedil;", "&#199;");
        XmlFixerReader.CODED_ENTITIES.put("&Egrave;", "&#200;");
        XmlFixerReader.CODED_ENTITIES.put("&Eacute;", "&#201;");
        XmlFixerReader.CODED_ENTITIES.put("&Ecirc;", "&#202;");
        XmlFixerReader.CODED_ENTITIES.put("&Euml;", "&#203;");
        XmlFixerReader.CODED_ENTITIES.put("&Igrave;", "&#204;");
        XmlFixerReader.CODED_ENTITIES.put("&Iacute;", "&#205;");
        XmlFixerReader.CODED_ENTITIES.put("&Icirc;", "&#206;");
        XmlFixerReader.CODED_ENTITIES.put("&Iuml;", "&#207;");
        XmlFixerReader.CODED_ENTITIES.put("&ETH;", "&#208;");
        XmlFixerReader.CODED_ENTITIES.put("&Ntilde;", "&#209;");
        XmlFixerReader.CODED_ENTITIES.put("&Ograve;", "&#210;");
        XmlFixerReader.CODED_ENTITIES.put("&Oacute;", "&#211;");
        XmlFixerReader.CODED_ENTITIES.put("&Ocirc;", "&#212;");
        XmlFixerReader.CODED_ENTITIES.put("&Otilde;", "&#213;");
        XmlFixerReader.CODED_ENTITIES.put("&Ouml;", "&#214;");
        XmlFixerReader.CODED_ENTITIES.put("&times;", "&#215;");
        XmlFixerReader.CODED_ENTITIES.put("&Oslash;", "&#216;");
        XmlFixerReader.CODED_ENTITIES.put("&Ugrave;", "&#217;");
        XmlFixerReader.CODED_ENTITIES.put("&Uacute;", "&#218;");
        XmlFixerReader.CODED_ENTITIES.put("&Ucirc;", "&#219;");
        XmlFixerReader.CODED_ENTITIES.put("&Uuml;", "&#220;");
        XmlFixerReader.CODED_ENTITIES.put("&Yacute;", "&#221;");
        XmlFixerReader.CODED_ENTITIES.put("&THORN;", "&#222;");
        XmlFixerReader.CODED_ENTITIES.put("&szlig;", "&#223;");
        XmlFixerReader.CODED_ENTITIES.put("&agrave;", "&#224;");
        XmlFixerReader.CODED_ENTITIES.put("&aacute;", "&#225;");
        XmlFixerReader.CODED_ENTITIES.put("&acirc;", "&#226;");
        XmlFixerReader.CODED_ENTITIES.put("&atilde;", "&#227;");
        XmlFixerReader.CODED_ENTITIES.put("&auml;", "&#228;");
        XmlFixerReader.CODED_ENTITIES.put("&aring;", "&#229;");
        XmlFixerReader.CODED_ENTITIES.put("&aelig;", "&#230;");
        XmlFixerReader.CODED_ENTITIES.put("&ccedil;", "&#231;");
        XmlFixerReader.CODED_ENTITIES.put("&egrave;", "&#232;");
        XmlFixerReader.CODED_ENTITIES.put("&eacute;", "&#233;");
        XmlFixerReader.CODED_ENTITIES.put("&ecirc;", "&#234;");
        XmlFixerReader.CODED_ENTITIES.put("&euml;", "&#235;");
        XmlFixerReader.CODED_ENTITIES.put("&igrave;", "&#236;");
        XmlFixerReader.CODED_ENTITIES.put("&iacute;", "&#237;");
        XmlFixerReader.CODED_ENTITIES.put("&icirc;", "&#238;");
        XmlFixerReader.CODED_ENTITIES.put("&iuml;", "&#239;");
        XmlFixerReader.CODED_ENTITIES.put("&eth;", "&#240;");
        XmlFixerReader.CODED_ENTITIES.put("&ntilde;", "&#241;");
        XmlFixerReader.CODED_ENTITIES.put("&ograve;", "&#242;");
        XmlFixerReader.CODED_ENTITIES.put("&oacute;", "&#243;");
        XmlFixerReader.CODED_ENTITIES.put("&ocirc;", "&#244;");
        XmlFixerReader.CODED_ENTITIES.put("&otilde;", "&#245;");
        XmlFixerReader.CODED_ENTITIES.put("&ouml;", "&#246;");
        XmlFixerReader.CODED_ENTITIES.put("&divide;", "&#247;");
        XmlFixerReader.CODED_ENTITIES.put("&oslash;", "&#248;");
        XmlFixerReader.CODED_ENTITIES.put("&ugrave;", "&#249;");
        XmlFixerReader.CODED_ENTITIES.put("&uacute;", "&#250;");
        XmlFixerReader.CODED_ENTITIES.put("&ucirc;", "&#251;");
        XmlFixerReader.CODED_ENTITIES.put("&uuml;", "&#252;");
        XmlFixerReader.CODED_ENTITIES.put("&yacute;", "&#253;");
        XmlFixerReader.CODED_ENTITIES.put("&thorn;", "&#254;");
        XmlFixerReader.CODED_ENTITIES.put("&yuml;", "&#255;");
        XmlFixerReader.CODED_ENTITIES.put("&fnof;", "&#402;");
        XmlFixerReader.CODED_ENTITIES.put("&Alpha;", "&#913;");
        XmlFixerReader.CODED_ENTITIES.put("&Beta;", "&#914;");
        XmlFixerReader.CODED_ENTITIES.put("&Gamma;", "&#915;");
        XmlFixerReader.CODED_ENTITIES.put("&Delta;", "&#916;");
        XmlFixerReader.CODED_ENTITIES.put("&Epsilon;", "&#917;");
        XmlFixerReader.CODED_ENTITIES.put("&Zeta;", "&#918;");
        XmlFixerReader.CODED_ENTITIES.put("&Eta;", "&#919;");
        XmlFixerReader.CODED_ENTITIES.put("&Theta;", "&#920;");
        XmlFixerReader.CODED_ENTITIES.put("&Iota;", "&#921;");
        XmlFixerReader.CODED_ENTITIES.put("&Kappa;", "&#922;");
        XmlFixerReader.CODED_ENTITIES.put("&Lambda;", "&#923;");
        XmlFixerReader.CODED_ENTITIES.put("&Mu;", "&#924;");
        XmlFixerReader.CODED_ENTITIES.put("&Nu;", "&#925;");
        XmlFixerReader.CODED_ENTITIES.put("&Xi;", "&#926;");
        XmlFixerReader.CODED_ENTITIES.put("&Omicron;", "&#927;");
        XmlFixerReader.CODED_ENTITIES.put("&Pi;", "&#928;");
        XmlFixerReader.CODED_ENTITIES.put("&Rho;", "&#929;");
        XmlFixerReader.CODED_ENTITIES.put("&Sigma;", "&#931;");
        XmlFixerReader.CODED_ENTITIES.put("&Tau;", "&#932;");
        XmlFixerReader.CODED_ENTITIES.put("&Upsilon;", "&#933;");
        XmlFixerReader.CODED_ENTITIES.put("&Phi;", "&#934;");
        XmlFixerReader.CODED_ENTITIES.put("&Chi;", "&#935;");
        XmlFixerReader.CODED_ENTITIES.put("&Psi;", "&#936;");
        XmlFixerReader.CODED_ENTITIES.put("&Omega;", "&#937;");
        XmlFixerReader.CODED_ENTITIES.put("&alpha;", "&#945;");
        XmlFixerReader.CODED_ENTITIES.put("&beta;", "&#946;");
        XmlFixerReader.CODED_ENTITIES.put("&gamma;", "&#947;");
        XmlFixerReader.CODED_ENTITIES.put("&delta;", "&#948;");
        XmlFixerReader.CODED_ENTITIES.put("&epsilon;", "&#949;");
        XmlFixerReader.CODED_ENTITIES.put("&zeta;", "&#950;");
        XmlFixerReader.CODED_ENTITIES.put("&eta;", "&#951;");
        XmlFixerReader.CODED_ENTITIES.put("&theta;", "&#952;");
        XmlFixerReader.CODED_ENTITIES.put("&iota;", "&#953;");
        XmlFixerReader.CODED_ENTITIES.put("&kappa;", "&#954;");
        XmlFixerReader.CODED_ENTITIES.put("&lambda;", "&#955;");
        XmlFixerReader.CODED_ENTITIES.put("&mu;", "&#956;");
        XmlFixerReader.CODED_ENTITIES.put("&nu;", "&#957;");
        XmlFixerReader.CODED_ENTITIES.put("&xi;", "&#958;");
        XmlFixerReader.CODED_ENTITIES.put("&omicron;", "&#959;");
        XmlFixerReader.CODED_ENTITIES.put("&pi;", "&#960;");
        XmlFixerReader.CODED_ENTITIES.put("&rho;", "&#961;");
        XmlFixerReader.CODED_ENTITIES.put("&sigmaf;", "&#962;");
        XmlFixerReader.CODED_ENTITIES.put("&sigma;", "&#963;");
        XmlFixerReader.CODED_ENTITIES.put("&tau;", "&#964;");
        XmlFixerReader.CODED_ENTITIES.put("&upsilon;", "&#965;");
        XmlFixerReader.CODED_ENTITIES.put("&phi;", "&#966;");
        XmlFixerReader.CODED_ENTITIES.put("&chi;", "&#967;");
        XmlFixerReader.CODED_ENTITIES.put("&psi;", "&#968;");
        XmlFixerReader.CODED_ENTITIES.put("&omega;", "&#969;");
        XmlFixerReader.CODED_ENTITIES.put("&thetasym;", "&#977;");
        XmlFixerReader.CODED_ENTITIES.put("&upsih;", "&#978;");
        XmlFixerReader.CODED_ENTITIES.put("&piv;", "&#982;");
        XmlFixerReader.CODED_ENTITIES.put("&bull;", "&#8226;");
        XmlFixerReader.CODED_ENTITIES.put("&hellip;", "&#8230;");
        XmlFixerReader.CODED_ENTITIES.put("&prime;", "&#8242;");
        XmlFixerReader.CODED_ENTITIES.put("&Prime;", "&#8243;");
        XmlFixerReader.CODED_ENTITIES.put("&oline;", "&#8254;");
        XmlFixerReader.CODED_ENTITIES.put("&frasl;", "&#8260;");
        XmlFixerReader.CODED_ENTITIES.put("&weierp;", "&#8472;");
        XmlFixerReader.CODED_ENTITIES.put("&image;", "&#8465;");
        XmlFixerReader.CODED_ENTITIES.put("&real;", "&#8476;");
        XmlFixerReader.CODED_ENTITIES.put("&trade;", "&#8482;");
        XmlFixerReader.CODED_ENTITIES.put("&alefsym;", "&#8501;");
        XmlFixerReader.CODED_ENTITIES.put("&larr;", "&#8592;");
        XmlFixerReader.CODED_ENTITIES.put("&uarr;", "&#8593;");
        XmlFixerReader.CODED_ENTITIES.put("&rarr;", "&#8594;");
        XmlFixerReader.CODED_ENTITIES.put("&darr;", "&#8595;");
        XmlFixerReader.CODED_ENTITIES.put("&harr;", "&#8596;");
        XmlFixerReader.CODED_ENTITIES.put("&crarr;", "&#8629;");
        XmlFixerReader.CODED_ENTITIES.put("&lArr;", "&#8656;");
        XmlFixerReader.CODED_ENTITIES.put("&uArr;", "&#8657;");
        XmlFixerReader.CODED_ENTITIES.put("&rArr;", "&#8658;");
        XmlFixerReader.CODED_ENTITIES.put("&dArr;", "&#8659;");
        XmlFixerReader.CODED_ENTITIES.put("&hArr;", "&#8660;");
        XmlFixerReader.CODED_ENTITIES.put("&forall;", "&#8704;");
        XmlFixerReader.CODED_ENTITIES.put("&part;", "&#8706;");
        XmlFixerReader.CODED_ENTITIES.put("&exist;", "&#8707;");
        XmlFixerReader.CODED_ENTITIES.put("&empty;", "&#8709;");
        XmlFixerReader.CODED_ENTITIES.put("&nabla;", "&#8711;");
        XmlFixerReader.CODED_ENTITIES.put("&isin;", "&#8712;");
        XmlFixerReader.CODED_ENTITIES.put("&notin;", "&#8713;");
        XmlFixerReader.CODED_ENTITIES.put("&ni;", "&#8715;");
        XmlFixerReader.CODED_ENTITIES.put("&prod;", "&#8719;");
        XmlFixerReader.CODED_ENTITIES.put("&sum;", "&#8721;");
        XmlFixerReader.CODED_ENTITIES.put("&minus;", "&#8722;");
        XmlFixerReader.CODED_ENTITIES.put("&lowast;", "&#8727;");
        XmlFixerReader.CODED_ENTITIES.put("&radic;", "&#8730;");
        XmlFixerReader.CODED_ENTITIES.put("&prop;", "&#8733;");
        XmlFixerReader.CODED_ENTITIES.put("&infin;", "&#8734;");
        XmlFixerReader.CODED_ENTITIES.put("&ang;", "&#8736;");
        XmlFixerReader.CODED_ENTITIES.put("&and;", "&#8743;");
        XmlFixerReader.CODED_ENTITIES.put("&or;", "&#8744;");
        XmlFixerReader.CODED_ENTITIES.put("&cap;", "&#8745;");
        XmlFixerReader.CODED_ENTITIES.put("&cup;", "&#8746;");
        XmlFixerReader.CODED_ENTITIES.put("&int;", "&#8747;");
        XmlFixerReader.CODED_ENTITIES.put("&there4;", "&#8756;");
        XmlFixerReader.CODED_ENTITIES.put("&sim;", "&#8764;");
        XmlFixerReader.CODED_ENTITIES.put("&cong;", "&#8773;");
        XmlFixerReader.CODED_ENTITIES.put("&asymp;", "&#8776;");
        XmlFixerReader.CODED_ENTITIES.put("&ne;", "&#8800;");
        XmlFixerReader.CODED_ENTITIES.put("&equiv;", "&#8801;");
        XmlFixerReader.CODED_ENTITIES.put("&le;", "&#8804;");
        XmlFixerReader.CODED_ENTITIES.put("&ge;", "&#8805;");
        XmlFixerReader.CODED_ENTITIES.put("&sub;", "&#8834;");
        XmlFixerReader.CODED_ENTITIES.put("&sup;", "&#8835;");
        XmlFixerReader.CODED_ENTITIES.put("&nsub;", "&#8836;");
        XmlFixerReader.CODED_ENTITIES.put("&sube;", "&#8838;");
        XmlFixerReader.CODED_ENTITIES.put("&supe;", "&#8839;");
        XmlFixerReader.CODED_ENTITIES.put("&oplus;", "&#8853;");
        XmlFixerReader.CODED_ENTITIES.put("&otimes;", "&#8855;");
        XmlFixerReader.CODED_ENTITIES.put("&perp;", "&#8869;");
        XmlFixerReader.CODED_ENTITIES.put("&sdot;", "&#8901;");
        XmlFixerReader.CODED_ENTITIES.put("&lceil;", "&#8968;");
        XmlFixerReader.CODED_ENTITIES.put("&rceil;", "&#8969;");
        XmlFixerReader.CODED_ENTITIES.put("&lfloor;", "&#8970;");
        XmlFixerReader.CODED_ENTITIES.put("&rfloor;", "&#8971;");
        XmlFixerReader.CODED_ENTITIES.put("&lang;", "&#9001;");
        XmlFixerReader.CODED_ENTITIES.put("&rang;", "&#9002;");
        XmlFixerReader.CODED_ENTITIES.put("&loz;", "&#9674;");
        XmlFixerReader.CODED_ENTITIES.put("&spades;", "&#9824;");
        XmlFixerReader.CODED_ENTITIES.put("&clubs;", "&#9827;");
        XmlFixerReader.CODED_ENTITIES.put("&hearts;", "&#9829;");
        XmlFixerReader.CODED_ENTITIES.put("&diams;", "&#9830;");
        XmlFixerReader.CODED_ENTITIES.put("&quot;", "&#34;");
        XmlFixerReader.CODED_ENTITIES.put("&amp;", "&#38;");
        XmlFixerReader.CODED_ENTITIES.put("&lt;", "&#60;");
        XmlFixerReader.CODED_ENTITIES.put("&gt;", "&#62;");
        XmlFixerReader.CODED_ENTITIES.put("&OElig;", "&#338;");
        XmlFixerReader.CODED_ENTITIES.put("&oelig;", "&#339;");
        XmlFixerReader.CODED_ENTITIES.put("&Scaron;", "&#352;");
        XmlFixerReader.CODED_ENTITIES.put("&scaron;", "&#353;");
        XmlFixerReader.CODED_ENTITIES.put("&Yuml;", "&#376;");
        XmlFixerReader.CODED_ENTITIES.put("&circ;", "&#710;");
        XmlFixerReader.CODED_ENTITIES.put("&tilde;", "&#732;");
        XmlFixerReader.CODED_ENTITIES.put("&ensp;", "&#8194;");
        XmlFixerReader.CODED_ENTITIES.put("&emsp;", "&#8195;");
        XmlFixerReader.CODED_ENTITIES.put("&thinsp;", "&#8201;");
        XmlFixerReader.CODED_ENTITIES.put("&zwnj;", "&#8204;");
        XmlFixerReader.CODED_ENTITIES.put("&zwj;", "&#8205;");
        XmlFixerReader.CODED_ENTITIES.put("&lrm;", "&#8206;");
        XmlFixerReader.CODED_ENTITIES.put("&rlm;", "&#8207;");
        XmlFixerReader.CODED_ENTITIES.put("&ndash;", "&#8211;");
        XmlFixerReader.CODED_ENTITIES.put("&mdash;", "&#8212;");
        XmlFixerReader.CODED_ENTITIES.put("&lsquo;", "&#8216;");
        XmlFixerReader.CODED_ENTITIES.put("&rsquo;", "&#8217;");
        XmlFixerReader.CODED_ENTITIES.put("&sbquo;", "&#8218;");
        XmlFixerReader.CODED_ENTITIES.put("&ldquo;", "&#8220;");
        XmlFixerReader.CODED_ENTITIES.put("&rdquo;", "&#8221;");
        XmlFixerReader.CODED_ENTITIES.put("&bdquo;", "&#8222;");
        XmlFixerReader.CODED_ENTITIES.put("&dagger;", "&#8224;");
        XmlFixerReader.CODED_ENTITIES.put("&Dagger;", "&#8225;");
        XmlFixerReader.CODED_ENTITIES.put("&permil;", "&#8240;");
        XmlFixerReader.CODED_ENTITIES.put("&lsaquo;", "&#8249;");
        XmlFixerReader.CODED_ENTITIES.put("&rsaquo;", "&#8250;");
        XmlFixerReader.CODED_ENTITIES.put("&euro;", "&#8364;");
        XmlFixerReader.ENTITIES_PATTERN = Pattern.compile("&[A-Za-z^#]+;");
    }
}
