// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.io.WireFeedParser;
import org.jdom.Document;
import java.util.List;

public class FeedParsers extends PluginManager
{
    public static final String FEED_PARSERS_KEY = "WireFeedParser.classes";
    
    public FeedParsers() {
        super("WireFeedParser.classes");
    }
    
    public List getSupportedFeedTypes() {
        return this.getKeys();
    }
    
    public WireFeedParser getParserFor(final Document document) {
        final List parsers = this.getPlugins();
        WireFeedParser parser = null;
        for (int i = 0; parser == null && i < parsers.size(); ++i) {
            parser = parsers.get(i);
            if (!parser.isMyType(document)) {
                parser = null;
            }
        }
        return parser;
    }
    
    protected String getKey(final Object obj) {
        return ((WireFeedParser)obj).getType();
    }
}
