// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.WireFeed;
import org.jdom.Namespace;
import org.jdom.Element;
import org.jdom.Document;

public class RSS20wNSParser extends RSS20Parser
{
    private static String RSS20_URI;
    
    public RSS20wNSParser() {
        this("rss_2.0wNS");
    }
    
    protected RSS20wNSParser(final String type) {
        super(type);
    }
    
    public boolean isMyType(final Document document) {
        final Element rssRoot = document.getRootElement();
        final Namespace defaultNS = rssRoot.getNamespace();
        boolean ok = defaultNS != null && defaultNS.equals(this.getRSSNamespace());
        if (ok) {
            ok = super.isMyType(document);
        }
        return ok;
    }
    
    protected Namespace getRSSNamespace() {
        return Namespace.getNamespace(RSS20wNSParser.RSS20_URI);
    }
    
    protected WireFeed parseChannel(final Element rssRoot) {
        final WireFeed wFeed = super.parseChannel(rssRoot);
        wFeed.setFeedType("rss_2.0");
        return wFeed;
    }
    
    static {
        RSS20wNSParser.RSS20_URI = "http://backend.userland.com/rss2";
    }
}
