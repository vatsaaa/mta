// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.List;
import java.util.Date;
import org.jdom.Content;
import org.jdom.Element;
import com.sun.syndication.feed.rss.Item;

public class RSS093Generator extends RSS092Generator
{
    public RSS093Generator() {
        this("rss_0.93", "0.93");
    }
    
    protected RSS093Generator(final String feedType, final String version) {
        super(feedType, version);
    }
    
    protected void populateItem(final Item item, final Element eItem, final int index) {
        super.populateItem(item, eItem, index);
        final Date pubDate = item.getPubDate();
        if (pubDate != null) {
            eItem.addContent(this.generateSimpleElement("pubDate", DateParser.formatRFC822(pubDate)));
        }
        final Date expirationDate = item.getExpirationDate();
        if (expirationDate != null) {
            eItem.addContent(this.generateSimpleElement("expirationDate", DateParser.formatRFC822(expirationDate)));
        }
    }
    
    protected int getNumberOfEnclosures(final List enclosures) {
        return enclosures.size();
    }
}
