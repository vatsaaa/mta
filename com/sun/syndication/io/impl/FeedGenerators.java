// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.List;
import com.sun.syndication.io.WireFeedGenerator;

public class FeedGenerators extends PluginManager
{
    public static final String FEED_GENERATORS_KEY = "WireFeedGenerator.classes";
    
    public FeedGenerators() {
        super("WireFeedGenerator.classes");
    }
    
    public WireFeedGenerator getGenerator(final String feedType) {
        return (WireFeedGenerator)this.getPlugin(feedType);
    }
    
    protected String getKey(final Object obj) {
        return ((WireFeedGenerator)obj).getType();
    }
    
    public List getSupportedFeedTypes() {
        return this.getKeys();
    }
}
