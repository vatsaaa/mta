// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.io.FeedException;
import com.sun.syndication.feed.rss.Description;
import java.util.List;
import org.jdom.Content;
import com.sun.syndication.feed.rss.Item;
import org.jdom.Element;
import com.sun.syndication.feed.rss.Channel;
import org.jdom.Namespace;

public class RSS10Generator extends RSS090Generator
{
    private static final String RSS_URI = "http://purl.org/rss/1.0/";
    private static final Namespace RSS_NS;
    
    public RSS10Generator() {
        super("rss_1.0");
    }
    
    protected RSS10Generator(final String feedType) {
        super(feedType);
    }
    
    protected Namespace getFeedNamespace() {
        return RSS10Generator.RSS_NS;
    }
    
    protected void populateChannel(final Channel channel, final Element eChannel) {
        super.populateChannel(channel, eChannel);
        if (channel.getUri() != null) {
            eChannel.setAttribute("about", channel.getUri(), this.getRDFNamespace());
        }
        final List items = channel.getItems();
        if (items.size() > 0) {
            final Element eItems = new Element("items", this.getFeedNamespace());
            final Element eSeq = new Element("Seq", this.getRDFNamespace());
            for (int i = 0; i < items.size(); ++i) {
                final Item item = items.get(i);
                final Element eLi = new Element("li", this.getRDFNamespace());
                final String link = item.getLink();
                if (link != null) {
                    eLi.setAttribute("resource", link);
                }
                eSeq.addContent(eLi);
            }
            eItems.addContent(eSeq);
            eChannel.addContent(eItems);
        }
    }
    
    protected void populateItem(final Item item, final Element eItem, final int index) {
        super.populateItem(item, eItem, index);
        final String link = item.getLink();
        final String uri = item.getUri();
        if (uri != null) {
            eItem.setAttribute("about", uri, this.getRDFNamespace());
        }
        else if (link != null) {
            eItem.setAttribute("about", link, this.getRDFNamespace());
        }
        final Description description = item.getDescription();
        if (description != null) {
            eItem.addContent(this.generateSimpleElement("description", description.getValue()));
        }
        if (item.getModule(this.getContentNamespace().getURI()) == null && item.getContent() != null) {
            final Element elem = new Element("encoded", this.getContentNamespace());
            elem.addContent(item.getContent().getValue());
            eItem.addContent(elem);
        }
    }
    
    protected void checkChannelConstraints(final Element eChannel) throws FeedException {
        this.checkNotNullAndLength(eChannel, "title", 0, -1);
        this.checkNotNullAndLength(eChannel, "description", 0, -1);
        this.checkNotNullAndLength(eChannel, "link", 0, -1);
    }
    
    protected void checkImageConstraints(final Element eImage) throws FeedException {
        this.checkNotNullAndLength(eImage, "title", 0, -1);
        this.checkNotNullAndLength(eImage, "url", 0, -1);
        this.checkNotNullAndLength(eImage, "link", 0, -1);
    }
    
    protected void checkTextInputConstraints(final Element eTextInput) throws FeedException {
        this.checkNotNullAndLength(eTextInput, "title", 0, -1);
        this.checkNotNullAndLength(eTextInput, "description", 0, -1);
        this.checkNotNullAndLength(eTextInput, "name", 0, -1);
        this.checkNotNullAndLength(eTextInput, "link", 0, -1);
    }
    
    protected void checkItemsConstraints(final Element parent) throws FeedException {
    }
    
    protected void checkItemConstraints(final Element eItem) throws FeedException {
        this.checkNotNullAndLength(eItem, "title", 0, -1);
        this.checkNotNullAndLength(eItem, "link", 0, -1);
    }
    
    static {
        RSS_NS = Namespace.getNamespace("http://purl.org/rss/1.0/");
    }
}
