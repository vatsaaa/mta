// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.Guid;
import com.sun.syndication.feed.rss.Item;
import java.util.List;
import com.sun.syndication.feed.rss.Category;
import org.jdom.Content;
import org.jdom.Element;
import com.sun.syndication.feed.rss.Channel;

public class RSS20Generator extends RSS094Generator
{
    public RSS20Generator() {
        this("rss_2.0", "2.0");
    }
    
    protected RSS20Generator(final String feedType, final String version) {
        super(feedType, version);
    }
    
    protected void populateChannel(final Channel channel, final Element eChannel) {
        super.populateChannel(channel, eChannel);
        final String generator = channel.getGenerator();
        if (generator != null) {
            eChannel.addContent(this.generateSimpleElement("generator", generator));
        }
        final int ttl = channel.getTtl();
        if (ttl > -1) {
            eChannel.addContent(this.generateSimpleElement("ttl", String.valueOf(ttl)));
        }
        final List categories = channel.getCategories();
        for (int i = 0; i < categories.size(); ++i) {
            eChannel.addContent(this.generateCategoryElement(categories.get(i)));
        }
    }
    
    public void populateItem(final Item item, final Element eItem, final int index) {
        super.populateItem(item, eItem, index);
        final Element eDescription = eItem.getChild("description", this.getFeedNamespace());
        if (eDescription != null) {
            eDescription.removeAttribute("type");
        }
        final String author = item.getAuthor();
        if (author != null) {
            eItem.addContent(this.generateSimpleElement("author", author));
        }
        final String comments = item.getComments();
        if (comments != null) {
            eItem.addContent(this.generateSimpleElement("comments", comments));
        }
        final Guid guid = item.getGuid();
        if (guid != null) {
            final Element eGuid = this.generateSimpleElement("guid", guid.getValue());
            if (!guid.isPermaLink()) {
                eGuid.setAttribute("isPermaLink", "false");
            }
            eItem.addContent(eGuid);
        }
    }
}
