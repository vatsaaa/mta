// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.atom.Generator;
import java.util.Collection;
import java.io.Reader;
import org.jdom.input.SAXBuilder;
import java.io.StringReader;
import org.jdom.Attribute;
import java.util.Iterator;
import com.sun.syndication.feed.atom.Person;
import com.sun.syndication.feed.atom.Category;
import com.sun.syndication.feed.atom.Link;
import org.jdom.Content;
import java.util.List;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.io.FeedException;
import org.jdom.Element;
import com.sun.syndication.feed.atom.Feed;
import org.jdom.Document;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Namespace;

public class Atom10Generator extends BaseWireFeedGenerator
{
    private static final String ATOM_10_URI = "http://www.w3.org/2005/Atom";
    private static final Namespace ATOM_NS;
    private String _version;
    
    public Atom10Generator() {
        this("atom_1.0", "1.0");
    }
    
    protected Atom10Generator(final String type, final String version) {
        super(type);
        this._version = version;
    }
    
    protected String getVersion() {
        return this._version;
    }
    
    protected Namespace getFeedNamespace() {
        return Atom10Generator.ATOM_NS;
    }
    
    public Document generate(final WireFeed wFeed) throws FeedException {
        final Feed feed = (Feed)wFeed;
        final Element root = this.createRootElement(feed);
        this.populateFeed(feed, root);
        return this.createDocument(root);
    }
    
    protected Document createDocument(final Element root) {
        return new Document(root);
    }
    
    protected Element createRootElement(final Feed feed) {
        final Element root = new Element("feed", this.getFeedNamespace());
        root.addNamespaceDeclaration(this.getFeedNamespace());
        if (feed.getXmlBase() != null) {
            root.setAttribute("base", feed.getXmlBase(), Namespace.XML_NAMESPACE);
        }
        this.generateModuleNamespaceDefs(root);
        return root;
    }
    
    protected void populateFeed(final Feed feed, final Element parent) throws FeedException {
        this.addFeed(feed, parent);
        this.addEntries(feed, parent);
    }
    
    protected void addFeed(final Feed feed, final Element parent) throws FeedException {
        final Element eFeed = parent;
        this.populateFeedHeader(feed, eFeed);
        this.checkFeedHeaderConstraints(eFeed);
        this.generateFeedModules(feed.getModules(), eFeed);
    }
    
    protected void addEntries(final Feed feed, final Element parent) throws FeedException {
        final List items = feed.getEntries();
        for (int i = 0; i < items.size(); ++i) {
            this.addEntry(items.get(i), parent);
        }
        this.checkEntriesConstraints(parent);
    }
    
    protected void addEntry(final Entry entry, final Element parent) throws FeedException {
        final Element eEntry = new Element("entry", this.getFeedNamespace());
        if (entry.getXmlBase() != null) {
            eEntry.setAttribute("base", entry.getXmlBase(), Namespace.XML_NAMESPACE);
        }
        this.populateEntry(entry, eEntry);
        this.checkEntryConstraints(eEntry);
        this.generateItemModules(entry.getModules(), eEntry);
        parent.addContent(eEntry);
    }
    
    protected void populateFeedHeader(final Feed feed, final Element eFeed) throws FeedException {
        if (feed.getTitle() != null) {
            eFeed.addContent(this.generateSimpleElement("title", feed.getTitle()));
        }
        List links = feed.getAlternateLinks();
        if (links != null) {
            for (int i = 0; i < links.size(); ++i) {
                eFeed.addContent(this.generateLinkElement(links.get(i)));
            }
        }
        links = feed.getOtherLinks();
        if (links != null) {
            for (int j = 0; j < links.size(); ++j) {
                eFeed.addContent(this.generateLinkElement(links.get(j)));
            }
        }
        final List cats = feed.getCategories();
        if (cats != null) {
            final Iterator iter = cats.iterator();
            while (iter.hasNext()) {
                eFeed.addContent(this.generateCategoryElement(iter.next()));
            }
        }
        final List authors = feed.getAuthors();
        if (authors != null && authors.size() > 0) {
            for (int k = 0; k < authors.size(); ++k) {
                final Element authorElement = new Element("author", this.getFeedNamespace());
                this.fillPersonElement(authorElement, feed.getAuthors().get(k));
                eFeed.addContent(authorElement);
            }
        }
        final List contributors = feed.getContributors();
        if (contributors != null && contributors.size() > 0) {
            for (int l = 0; l < contributors.size(); ++l) {
                final Element contributorElement = new Element("contributor", this.getFeedNamespace());
                this.fillPersonElement(contributorElement, contributors.get(l));
                eFeed.addContent(contributorElement);
            }
        }
        if (feed.getSubtitle() != null) {
            eFeed.addContent(this.generateSimpleElement("subtitle", feed.getSubtitle().getValue()));
        }
        if (feed.getId() != null) {
            eFeed.addContent(this.generateSimpleElement("id", feed.getId()));
        }
        if (feed.getGenerator() != null) {
            eFeed.addContent(this.generateGeneratorElement(feed.getGenerator()));
        }
        if (feed.getRights() != null) {
            eFeed.addContent(this.generateSimpleElement("rights", feed.getRights()));
        }
        if (feed.getUpdated() != null) {
            final Element updatedElement = new Element("updated", this.getFeedNamespace());
            updatedElement.addContent(DateParser.formatW3CDateTime(feed.getUpdated()));
            eFeed.addContent(updatedElement);
        }
        this.generateForeignMarkup(eFeed, (List)feed.getForeignMarkup());
    }
    
    protected void populateEntry(final Entry entry, final Element eEntry) throws FeedException {
        if (entry.getTitle() != null) {
            eEntry.addContent(this.generateSimpleElement("title", entry.getTitle()));
        }
        List links = entry.getAlternateLinks();
        if (links != null) {
            for (int i = 0; i < links.size(); ++i) {
                eEntry.addContent(this.generateLinkElement(links.get(i)));
            }
        }
        links = entry.getOtherLinks();
        if (links != null) {
            for (int i = 0; i < links.size(); ++i) {
                eEntry.addContent(this.generateLinkElement(links.get(i)));
            }
        }
        final List cats = entry.getCategories();
        if (cats != null) {
            for (int j = 0; j < cats.size(); ++j) {
                eEntry.addContent(this.generateCategoryElement(cats.get(j)));
            }
        }
        final List authors = entry.getAuthors();
        if (authors != null && authors.size() > 0) {
            for (int k = 0; k < authors.size(); ++k) {
                final Element authorElement = new Element("author", this.getFeedNamespace());
                this.fillPersonElement(authorElement, entry.getAuthors().get(k));
                eEntry.addContent(authorElement);
            }
        }
        final List contributors = entry.getContributors();
        if (contributors != null && contributors.size() > 0) {
            for (int l = 0; l < contributors.size(); ++l) {
                final Element contributorElement = new Element("contributor", this.getFeedNamespace());
                this.fillPersonElement(contributorElement, contributors.get(l));
                eEntry.addContent(contributorElement);
            }
        }
        if (entry.getId() != null) {
            eEntry.addContent(this.generateSimpleElement("id", entry.getId()));
        }
        if (entry.getUpdated() != null) {
            final Element updatedElement = new Element("updated", this.getFeedNamespace());
            updatedElement.addContent(DateParser.formatW3CDateTime(entry.getUpdated()));
            eEntry.addContent(updatedElement);
        }
        if (entry.getPublished() != null) {
            final Element publishedElement = new Element("published", this.getFeedNamespace());
            publishedElement.addContent(DateParser.formatW3CDateTime(entry.getPublished()));
            eEntry.addContent(publishedElement);
        }
        if (entry.getContents() != null && entry.getContents().size() > 0) {
            final Element contentElement = new Element("content", this.getFeedNamespace());
            final com.sun.syndication.feed.atom.Content content = entry.getContents().get(0);
            this.fillContentElement(contentElement, content);
            eEntry.addContent(contentElement);
        }
        if (entry.getSummary() != null) {
            final Element summaryElement = new Element("summary", this.getFeedNamespace());
            this.fillContentElement(summaryElement, entry.getSummary());
            eEntry.addContent(summaryElement);
        }
        this.generateForeignMarkup(eEntry, (List)entry.getForeignMarkup());
    }
    
    protected void checkFeedHeaderConstraints(final Element eFeed) throws FeedException {
    }
    
    protected void checkEntriesConstraints(final Element parent) throws FeedException {
    }
    
    protected void checkEntryConstraints(final Element eEntry) throws FeedException {
    }
    
    protected Element generateCategoryElement(final Category cat) {
        final Element catElement = new Element("category", this.getFeedNamespace());
        if (cat.getTerm() != null) {
            final Attribute termAttribute = new Attribute("term", cat.getTerm());
            catElement.setAttribute(termAttribute);
        }
        if (cat.getLabel() != null) {
            final Attribute labelAttribute = new Attribute("label", cat.getLabel());
            catElement.setAttribute(labelAttribute);
        }
        if (cat.getScheme() != null) {
            final Attribute schemeAttribute = new Attribute("scheme", cat.getScheme());
            catElement.setAttribute(schemeAttribute);
        }
        return catElement;
    }
    
    protected Element generateLinkElement(final Link link) {
        final Element linkElement = new Element("link", this.getFeedNamespace());
        if (link.getRel() != null) {
            final Attribute relAttribute = new Attribute("rel", link.getRel().toString());
            linkElement.setAttribute(relAttribute);
        }
        if (link.getType() != null) {
            final Attribute typeAttribute = new Attribute("type", link.getType());
            linkElement.setAttribute(typeAttribute);
        }
        if (link.getHref() != null) {
            final Attribute hrefAttribute = new Attribute("href", link.getHref());
            linkElement.setAttribute(hrefAttribute);
        }
        if (link.getHreflang() != null) {
            final Attribute hreflangAttribute = new Attribute("hreflang", link.getHreflang());
            linkElement.setAttribute(hreflangAttribute);
        }
        return linkElement;
    }
    
    protected void fillPersonElement(final Element element, final Person person) {
        if (person.getName() != null) {
            element.addContent(this.generateSimpleElement("name", person.getName()));
        }
        if (person.getUri() != null) {
            element.addContent(this.generateSimpleElement("uri", person.getUri()));
        }
        if (person.getEmail() != null) {
            element.addContent(this.generateSimpleElement("email", person.getEmail()));
        }
    }
    
    protected Element generateTagLineElement(final com.sun.syndication.feed.atom.Content tagline) {
        final Element taglineElement = new Element("subtitle", this.getFeedNamespace());
        if (tagline.getType() != null) {
            final Attribute typeAttribute = new Attribute("type", tagline.getType());
            taglineElement.setAttribute(typeAttribute);
        }
        if (tagline.getValue() != null) {
            taglineElement.addContent(tagline.getValue());
        }
        return taglineElement;
    }
    
    protected void fillContentElement(final Element contentElement, final com.sun.syndication.feed.atom.Content content) throws FeedException {
        final String type = content.getType();
        if (type != null) {
            String atomType = type;
            if ("text/plain".equals(type)) {
                atomType = "TEXT";
            }
            else if ("text/html".equals(type)) {
                atomType = "HTML";
            }
            else if ("application/xhtml+xml".equals(type)) {
                atomType = "XHTML";
            }
            final Attribute typeAttribute = new Attribute("type", atomType);
            contentElement.setAttribute(typeAttribute);
        }
        final String href = content.getSrc();
        if (href != null) {
            final Attribute srcAttribute = new Attribute("src", href);
            contentElement.setAttribute(srcAttribute);
        }
        if (content.getValue() != null) {
            if (type != null && (type.equals("xhtml") || type.indexOf("/xml") != -1)) {
                final StringBuffer tmpDocString = new StringBuffer("<tmpdoc>");
                tmpDocString.append(content.getValue());
                tmpDocString.append("</tmpdoc>");
                final StringReader tmpDocReader = new StringReader(tmpDocString.toString());
                Document tmpDoc;
                try {
                    final SAXBuilder saxBuilder = new SAXBuilder();
                    tmpDoc = saxBuilder.build(tmpDocReader);
                }
                catch (Exception ex) {
                    throw new FeedException("Invalid XML", ex);
                }
                final List children = tmpDoc.getRootElement().removeContent();
                contentElement.addContent(children);
            }
            else {
                contentElement.addContent(content.getValue());
            }
        }
    }
    
    protected Element generateGeneratorElement(final Generator generator) {
        final Element generatorElement = new Element("generator", this.getFeedNamespace());
        if (generator.getUrl() != null) {
            final Attribute urlAttribute = new Attribute("uri", generator.getUrl());
            generatorElement.setAttribute(urlAttribute);
        }
        if (generator.getVersion() != null) {
            final Attribute versionAttribute = new Attribute("version", generator.getVersion());
            generatorElement.setAttribute(versionAttribute);
        }
        if (generator.getValue() != null) {
            generatorElement.addContent(generator.getValue());
        }
        return generatorElement;
    }
    
    protected Element generateSimpleElement(final String name, final String value) {
        final Element element = new Element(name, this.getFeedNamespace());
        element.addContent(value);
        return element;
    }
    
    static {
        ATOM_NS = Namespace.getNamespace("http://www.w3.org/2005/Atom");
    }
}
