// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.ArrayList;
import org.jdom.Attribute;
import java.util.Iterator;
import java.util.List;
import java.util.Date;
import org.jdom.Content;
import com.sun.syndication.feed.module.DCSubject;
import java.util.Collection;
import com.sun.syndication.feed.module.DCModule;
import org.jdom.Element;
import com.sun.syndication.feed.module.Module;
import java.util.Set;
import org.jdom.Namespace;
import com.sun.syndication.io.ModuleGenerator;

public class DCModuleGenerator implements ModuleGenerator
{
    private static final String DC_URI = "http://purl.org/dc/elements/1.1/";
    private static final String TAXO_URI = "http://purl.org/rss/1.0/modules/taxonomy/";
    private static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    private static final Namespace DC_NS;
    private static final Namespace TAXO_NS;
    private static final Namespace RDF_NS;
    private static final Set NAMESPACES;
    
    public final String getNamespaceUri() {
        return "http://purl.org/dc/elements/1.1/";
    }
    
    private final Namespace getDCNamespace() {
        return DCModuleGenerator.DC_NS;
    }
    
    private final Namespace getRDFNamespace() {
        return DCModuleGenerator.RDF_NS;
    }
    
    private final Namespace getTaxonomyNamespace() {
        return DCModuleGenerator.TAXO_NS;
    }
    
    public final Set getNamespaces() {
        return DCModuleGenerator.NAMESPACES;
    }
    
    public final void generate(final Module module, final Element element) {
        final DCModule dcModule = (DCModule)module;
        if (dcModule.getTitle() != null) {
            element.addContent(this.generateSimpleElementList("title", dcModule.getTitles()));
        }
        if (dcModule.getCreator() != null) {
            element.addContent(this.generateSimpleElementList("creator", dcModule.getCreators()));
        }
        final List subjects = dcModule.getSubjects();
        for (int i = 0; i < subjects.size(); ++i) {
            element.addContent(this.generateSubjectElement(subjects.get(i)));
        }
        if (dcModule.getDescription() != null) {
            element.addContent(this.generateSimpleElementList("description", dcModule.getDescriptions()));
        }
        if (dcModule.getPublisher() != null) {
            element.addContent(this.generateSimpleElementList("publisher", dcModule.getPublishers()));
        }
        if (dcModule.getContributors() != null) {
            element.addContent(this.generateSimpleElementList("contributor", dcModule.getContributors()));
        }
        if (dcModule.getDate() != null) {
            final Iterator j = dcModule.getDates().iterator();
            while (j.hasNext()) {
                element.addContent(this.generateSimpleElement("date", DateParser.formatW3CDateTime(j.next())));
            }
        }
        if (dcModule.getType() != null) {
            element.addContent(this.generateSimpleElementList("type", dcModule.getTypes()));
        }
        if (dcModule.getFormat() != null) {
            element.addContent(this.generateSimpleElementList("format", dcModule.getFormats()));
        }
        if (dcModule.getIdentifier() != null) {
            element.addContent(this.generateSimpleElementList("identifier", dcModule.getIdentifiers()));
        }
        if (dcModule.getSource() != null) {
            element.addContent(this.generateSimpleElementList("source", dcModule.getSources()));
        }
        if (dcModule.getLanguage() != null) {
            element.addContent(this.generateSimpleElementList("language", dcModule.getLanguages()));
        }
        if (dcModule.getRelation() != null) {
            element.addContent(this.generateSimpleElementList("relation", dcModule.getRelations()));
        }
        if (dcModule.getCoverage() != null) {
            element.addContent(this.generateSimpleElementList("coverage", dcModule.getCoverages()));
        }
        if (dcModule.getRights() != null) {
            element.addContent(this.generateSimpleElementList("rights", dcModule.getRightsList()));
        }
    }
    
    protected final Element generateSubjectElement(final DCSubject subject) {
        final Element subjectElement = new Element("subject", this.getDCNamespace());
        if (subject.getTaxonomyUri() != null) {
            final Element descriptionElement = new Element("Description", this.getRDFNamespace());
            final Element topicElement = new Element("topic", this.getTaxonomyNamespace());
            final Attribute resourceAttribute = new Attribute("resource", subject.getTaxonomyUri(), this.getRDFNamespace());
            topicElement.setAttribute(resourceAttribute);
            descriptionElement.addContent(topicElement);
            if (subject.getValue() != null) {
                final Element valueElement = new Element("value", this.getRDFNamespace());
                valueElement.addContent(subject.getValue());
                descriptionElement.addContent(valueElement);
            }
            subjectElement.addContent(descriptionElement);
        }
        else {
            subjectElement.addContent(subject.getValue());
        }
        return subjectElement;
    }
    
    protected final Element generateSimpleElement(final String name, final String value) {
        final Element element = new Element(name, this.getDCNamespace());
        element.addContent(value);
        return element;
    }
    
    protected final List generateSimpleElementList(final String name, final List value) {
        final List elements = new ArrayList();
        final Iterator i = value.iterator();
        while (i.hasNext()) {
            elements.add(this.generateSimpleElement(name, i.next()));
        }
        return elements;
    }
    
    static {
        DC_NS = Namespace.getNamespace("dc", "http://purl.org/dc/elements/1.1/");
        TAXO_NS = Namespace.getNamespace("taxo", "http://purl.org/rss/1.0/modules/taxonomy/");
        RDF_NS = Namespace.getNamespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        final Set nss = new HashSet();
        nss.add(DCModuleGenerator.DC_NS);
        nss.add(DCModuleGenerator.TAXO_NS);
        nss.add(DCModuleGenerator.RDF_NS);
        NAMESPACES = Collections.unmodifiableSet((Set<?>)nss);
    }
}
