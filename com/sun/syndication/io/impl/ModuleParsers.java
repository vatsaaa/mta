// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.module.Module;
import java.util.ArrayList;
import org.jdom.Namespace;
import org.jdom.Element;
import java.util.List;
import com.sun.syndication.io.ModuleParser;
import com.sun.syndication.io.WireFeedGenerator;
import com.sun.syndication.io.WireFeedParser;

public class ModuleParsers extends PluginManager
{
    public ModuleParsers(final String propertyKey, final WireFeedParser parentParser) {
        super(propertyKey, parentParser, null);
    }
    
    public String getKey(final Object obj) {
        return ((ModuleParser)obj).getNamespaceUri();
    }
    
    public List getModuleNamespaces() {
        return this.getKeys();
    }
    
    public List parseModules(final Element root) {
        final List parsers = this.getPlugins();
        List modules = null;
        for (int i = 0; i < parsers.size(); ++i) {
            final ModuleParser parser = parsers.get(i);
            final String namespaceUri = parser.getNamespaceUri();
            final Namespace namespace = Namespace.getNamespace(namespaceUri);
            if (this.hasElementsFrom(root, namespace)) {
                final Module module = parser.parse(root);
                if (module != null) {
                    if (modules == null) {
                        modules = new ArrayList();
                    }
                    modules.add(module);
                }
            }
        }
        return modules;
    }
    
    private boolean hasElementsFrom(final Element root, final Namespace namespace) {
        boolean hasElements = false;
        if (!hasElements) {
            final List children = root.getChildren();
            Element child;
            for (int i = 0; !hasElements && i < children.size(); hasElements = namespace.equals(child.getNamespace()), ++i) {
                child = children.get(i);
            }
        }
        return hasElements;
    }
}
