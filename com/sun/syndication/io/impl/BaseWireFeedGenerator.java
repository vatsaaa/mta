// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import org.jdom.Content;
import java.util.List;
import org.jdom.Element;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;
import org.jdom.Namespace;
import com.sun.syndication.io.WireFeedGenerator;

public abstract class BaseWireFeedGenerator implements WireFeedGenerator
{
    private static final String FEED_MODULE_GENERATORS_POSFIX_KEY = ".feed.ModuleGenerator.classes";
    private static final String ITEM_MODULE_GENERATORS_POSFIX_KEY = ".item.ModuleGenerator.classes";
    private String _type;
    private ModuleGenerators _feedModuleGenerators;
    private ModuleGenerators _itemModuleGenerators;
    private Namespace[] _allModuleNamespaces;
    
    protected BaseWireFeedGenerator(final String type) {
        this._type = type;
        this._feedModuleGenerators = new ModuleGenerators(type + ".feed.ModuleGenerator.classes", this);
        this._itemModuleGenerators = new ModuleGenerators(type + ".item.ModuleGenerator.classes", this);
        final Set allModuleNamespaces = new HashSet();
        Iterator i = this._feedModuleGenerators.getAllNamespaces().iterator();
        while (i.hasNext()) {
            allModuleNamespaces.add(i.next());
        }
        i = this._itemModuleGenerators.getAllNamespaces().iterator();
        while (i.hasNext()) {
            allModuleNamespaces.add(i.next());
        }
        allModuleNamespaces.toArray(this._allModuleNamespaces = new Namespace[allModuleNamespaces.size()]);
    }
    
    public String getType() {
        return this._type;
    }
    
    protected void generateModuleNamespaceDefs(final Element root) {
        for (int i = 0; i < this._allModuleNamespaces.length; ++i) {
            root.addNamespaceDeclaration(this._allModuleNamespaces[i]);
        }
    }
    
    protected void generateFeedModules(final List modules, final Element feed) {
        this._feedModuleGenerators.generateModules(modules, feed);
    }
    
    public void generateItemModules(final List modules, final Element item) {
        this._itemModuleGenerators.generateModules(modules, item);
    }
    
    protected void generateForeignMarkup(final Element e, final List foreignMarkup) {
        if (foreignMarkup != null) {
            for (final Element elem : foreignMarkup) {
                e.addContent(elem);
            }
        }
    }
}
