// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.Item;
import com.sun.syndication.feed.rss.TextInput;
import com.sun.syndication.feed.rss.Image;
import org.jdom.Content;
import java.util.List;
import com.sun.syndication.io.FeedException;
import org.jdom.Element;
import com.sun.syndication.feed.rss.Channel;
import org.jdom.Document;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Namespace;

public class RSS090Generator extends BaseWireFeedGenerator
{
    private static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    private static final String RSS_URI = "http://my.netscape.com/rdf/simple/0.9/";
    private static final String CONTENT_URI = "http://purl.org/rss/1.0/modules/content/";
    private static final Namespace RDF_NS;
    private static final Namespace RSS_NS;
    private static final Namespace CONTENT_NS;
    
    public RSS090Generator() {
        this("rss_0.9");
    }
    
    protected RSS090Generator(final String type) {
        super(type);
    }
    
    public Document generate(final WireFeed feed) throws FeedException {
        final Channel channel = (Channel)feed;
        final Element root = this.createRootElement(channel);
        this.populateFeed(channel, root);
        return this.createDocument(root);
    }
    
    protected Namespace getFeedNamespace() {
        return RSS090Generator.RSS_NS;
    }
    
    protected Namespace getRDFNamespace() {
        return RSS090Generator.RDF_NS;
    }
    
    protected Namespace getContentNamespace() {
        return RSS090Generator.CONTENT_NS;
    }
    
    protected Document createDocument(final Element root) {
        return new Document(root);
    }
    
    protected Element createRootElement(final Channel channel) {
        final Element root = new Element("RDF", this.getRDFNamespace());
        root.addNamespaceDeclaration(this.getFeedNamespace());
        root.addNamespaceDeclaration(this.getRDFNamespace());
        root.addNamespaceDeclaration(this.getContentNamespace());
        this.generateModuleNamespaceDefs(root);
        return root;
    }
    
    protected void populateFeed(final Channel channel, final Element parent) throws FeedException {
        this.addChannel(channel, parent);
        this.addImage(channel, parent);
        this.addTextInput(channel, parent);
        this.addItems(channel, parent);
        this.generateForeignMarkup(parent, (List)channel.getForeignMarkup());
    }
    
    protected void addChannel(final Channel channel, final Element parent) throws FeedException {
        final Element eChannel = new Element("channel", this.getFeedNamespace());
        this.populateChannel(channel, eChannel);
        this.checkChannelConstraints(eChannel);
        parent.addContent(eChannel);
        this.generateFeedModules(channel.getModules(), eChannel);
    }
    
    protected void populateChannel(final Channel channel, final Element eChannel) {
        final String title = channel.getTitle();
        if (title != null) {
            eChannel.addContent(this.generateSimpleElement("title", title));
        }
        final String link = channel.getLink();
        if (link != null) {
            eChannel.addContent(this.generateSimpleElement("link", link));
        }
        final String description = channel.getDescription();
        if (description != null) {
            eChannel.addContent(this.generateSimpleElement("description", description));
        }
    }
    
    protected void checkNotNullAndLength(final Element parent, final String childName, final int minLen, final int maxLen) throws FeedException {
        final Element child = parent.getChild(childName, this.getFeedNamespace());
        if (child == null) {
            throw new FeedException("Invalid " + this.getType() + " feed, missing " + parent.getName() + " " + childName);
        }
        this.checkLength(parent, childName, minLen, maxLen);
    }
    
    protected void checkLength(final Element parent, final String childName, final int minLen, final int maxLen) throws FeedException {
        final Element child = parent.getChild(childName, this.getFeedNamespace());
        if (child != null) {
            if (minLen > 0 && child.getText().length() < minLen) {
                throw new FeedException("Invalid " + this.getType() + " feed, " + parent.getName() + " " + childName + "short of " + minLen + " length");
            }
            if (maxLen > -1 && child.getText().length() > maxLen) {
                throw new FeedException("Invalid " + this.getType() + " feed, " + parent.getName() + " " + childName + "exceeds " + maxLen + " length");
            }
        }
    }
    
    protected void addImage(final Channel channel, final Element parent) throws FeedException {
        final Image image = channel.getImage();
        if (image != null) {
            final Element eImage = new Element("image", this.getFeedNamespace());
            this.populateImage(image, eImage);
            this.checkImageConstraints(eImage);
            parent.addContent(eImage);
        }
    }
    
    protected void populateImage(final Image image, final Element eImage) {
        final String title = image.getTitle();
        if (title != null) {
            eImage.addContent(this.generateSimpleElement("title", title));
        }
        final String url = image.getUrl();
        if (url != null) {
            eImage.addContent(this.generateSimpleElement("url", url));
        }
        final String link = image.getLink();
        if (link != null) {
            eImage.addContent(this.generateSimpleElement("link", link));
        }
    }
    
    protected String getTextInputLabel() {
        return "textInput";
    }
    
    protected void addTextInput(final Channel channel, final Element parent) throws FeedException {
        final TextInput textInput = channel.getTextInput();
        if (textInput != null) {
            final Element eTextInput = new Element(this.getTextInputLabel(), this.getFeedNamespace());
            this.populateTextInput(textInput, eTextInput);
            this.checkTextInputConstraints(eTextInput);
            parent.addContent(eTextInput);
        }
    }
    
    protected void populateTextInput(final TextInput textInput, final Element eTextInput) {
        final String title = textInput.getTitle();
        if (title != null) {
            eTextInput.addContent(this.generateSimpleElement("title", title));
        }
        final String description = textInput.getDescription();
        if (description != null) {
            eTextInput.addContent(this.generateSimpleElement("description", description));
        }
        final String name = textInput.getName();
        if (name != null) {
            eTextInput.addContent(this.generateSimpleElement("name", name));
        }
        final String link = textInput.getLink();
        if (link != null) {
            eTextInput.addContent(this.generateSimpleElement("link", link));
        }
    }
    
    protected void addItems(final Channel channel, final Element parent) throws FeedException {
        final List items = channel.getItems();
        for (int i = 0; i < items.size(); ++i) {
            this.addItem(items.get(i), parent, i);
        }
        this.checkItemsConstraints(parent);
    }
    
    protected void addItem(final Item item, final Element parent, final int index) throws FeedException {
        final Element eItem = new Element("item", this.getFeedNamespace());
        this.populateItem(item, eItem, index);
        this.checkItemConstraints(eItem);
        this.generateItemModules(item.getModules(), eItem);
        parent.addContent(eItem);
    }
    
    protected void populateItem(final Item item, final Element eItem, final int index) {
        final String title = item.getTitle();
        if (title != null) {
            eItem.addContent(this.generateSimpleElement("title", title));
        }
        final String link = item.getLink();
        if (link != null) {
            eItem.addContent(this.generateSimpleElement("link", link));
        }
        this.generateForeignMarkup(eItem, (List)item.getForeignMarkup());
    }
    
    protected Element generateSimpleElement(final String name, final String value) {
        final Element element = new Element(name, this.getFeedNamespace());
        element.addContent(value);
        return element;
    }
    
    protected void checkChannelConstraints(final Element eChannel) throws FeedException {
        this.checkNotNullAndLength(eChannel, "title", 0, 40);
        this.checkNotNullAndLength(eChannel, "description", 0, 500);
        this.checkNotNullAndLength(eChannel, "link", 0, 500);
    }
    
    protected void checkImageConstraints(final Element eImage) throws FeedException {
        this.checkNotNullAndLength(eImage, "title", 0, 40);
        this.checkNotNullAndLength(eImage, "url", 0, 500);
        this.checkNotNullAndLength(eImage, "link", 0, 500);
    }
    
    protected void checkTextInputConstraints(final Element eTextInput) throws FeedException {
        this.checkNotNullAndLength(eTextInput, "title", 0, 40);
        this.checkNotNullAndLength(eTextInput, "description", 0, 100);
        this.checkNotNullAndLength(eTextInput, "name", 0, 500);
        this.checkNotNullAndLength(eTextInput, "link", 0, 500);
    }
    
    protected void checkItemsConstraints(final Element parent) throws FeedException {
        final int count = parent.getChildren("item", this.getFeedNamespace()).size();
        if (count < 1 || count > 15) {
            throw new FeedException("Invalid " + this.getType() + " feed, item count is " + count + " it must be between 1 an 15");
        }
    }
    
    protected void checkItemConstraints(final Element eItem) throws FeedException {
        this.checkNotNullAndLength(eItem, "title", 0, 100);
        this.checkNotNullAndLength(eItem, "link", 0, 500);
    }
    
    static {
        RDF_NS = Namespace.getNamespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        RSS_NS = Namespace.getNamespace("http://my.netscape.com/rdf/simple/0.9/");
        CONTENT_NS = Namespace.getNamespace("content", "http://purl.org/rss/1.0/modules/content/");
    }
}
