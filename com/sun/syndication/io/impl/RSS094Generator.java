// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.Description;
import org.jdom.Attribute;
import org.jdom.Element;
import com.sun.syndication.feed.rss.Item;

public class RSS094Generator extends RSS093Generator
{
    public RSS094Generator() {
        this("rss_0.94", "0.94");
    }
    
    protected RSS094Generator(final String feedType, final String version) {
        super(feedType, version);
    }
    
    protected void populateItem(final Item item, final Element eItem, final int index) {
        super.populateItem(item, eItem, index);
        final Description description = item.getDescription();
        if (description != null && description.getType() != null) {
            final Element eDescription = eItem.getChild("description", this.getFeedNamespace());
            eDescription.setAttribute(new Attribute("type", description.getType()));
        }
        eItem.removeChild("expirationDate", this.getFeedNamespace());
    }
}
