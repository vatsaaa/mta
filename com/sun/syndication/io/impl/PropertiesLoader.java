// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.StringTokenizer;
import java.util.Enumeration;
import java.io.InputStream;
import java.util.List;
import java.net.URL;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class PropertiesLoader
{
    private static final String MASTER_PLUGIN_FILE = "com/sun/syndication/rome.properties";
    private static final String EXTRA_PLUGIN_FILE = "rome.properties";
    private static PropertiesLoader PROPERTIES_LOADER;
    private Properties[] _properties;
    
    public static PropertiesLoader getPropertiesLoader() {
        return PropertiesLoader.PROPERTIES_LOADER;
    }
    
    private PropertiesLoader(final String masterFileLocation, final String extraFileLocation) throws IOException {
        final List propertiesList = new ArrayList();
        final ClassLoader classLoader = PluginManager.class.getClassLoader();
        try {
            final InputStream is = classLoader.getResourceAsStream(masterFileLocation);
            final Properties p = new Properties();
            p.load(is);
            is.close();
            propertiesList.add(p);
        }
        catch (IOException ioex) {
            final IOException ex = new IOException("could not load ROME master plugins file [" + masterFileLocation + "], " + ioex.getMessage());
            ex.setStackTrace(ioex.getStackTrace());
            throw ex;
        }
        final Enumeration urls = classLoader.getResources(extraFileLocation);
        while (urls.hasMoreElements()) {
            final URL url = urls.nextElement();
            final Properties p2 = new Properties();
            try {
                final InputStream is2 = url.openStream();
                p2.load(is2);
                is2.close();
            }
            catch (IOException ioex2) {
                final IOException ex2 = new IOException("could not load ROME extensions plugins file [" + url.toString() + "], " + ioex2.getMessage());
                ex2.setStackTrace(ioex2.getStackTrace());
                throw ex2;
            }
            propertiesList.add(p2);
        }
        propertiesList.toArray(this._properties = new Properties[propertiesList.size()]);
    }
    
    public String[] getTokenizedProperty(final String key, final String separator) {
        final List entriesList = new ArrayList();
        for (int i = 0; i < this._properties.length; ++i) {
            final String values = this._properties[i].getProperty(key);
            if (values != null) {
                final StringTokenizer st = new StringTokenizer(values, separator);
                while (st.hasMoreTokens()) {
                    final String token = st.nextToken();
                    entriesList.add(token);
                }
            }
        }
        final String[] entries = new String[entriesList.size()];
        entriesList.toArray(entries);
        return entries;
    }
    
    public String[] getProperty(final String key) {
        final List entriesList = new ArrayList();
        for (int i = 0; i < this._properties.length; ++i) {
            final String values = this._properties[i].getProperty(key);
            if (values != null) {
                entriesList.add(values);
            }
        }
        final String[] entries = new String[entriesList.size()];
        entriesList.toArray(entries);
        return entries;
    }
    
    static {
        try {
            PropertiesLoader.PROPERTIES_LOADER = new PropertiesLoader("com/sun/syndication/rome.properties", "rome.properties");
        }
        catch (IOException ioex) {
            throw new RuntimeException(ioex.getMessage(), ioex);
        }
    }
}
