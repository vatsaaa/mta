// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.module.DCSubject;
import java.util.Iterator;
import com.sun.syndication.feed.module.DCSubjectImpl;
import java.util.ArrayList;
import org.jdom.Attribute;
import java.util.List;
import com.sun.syndication.feed.module.DCModule;
import com.sun.syndication.feed.module.DCModuleImpl;
import com.sun.syndication.feed.module.Module;
import org.jdom.Element;
import org.jdom.Namespace;
import com.sun.syndication.io.ModuleParser;

public class DCModuleParser implements ModuleParser
{
    private static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    private static final String TAXO_URI = "http://purl.org/rss/1.0/modules/taxonomy/";
    private static final Namespace DC_NS;
    private static final Namespace RDF_NS;
    private static final Namespace TAXO_NS;
    
    public final String getNamespaceUri() {
        return "http://purl.org/dc/elements/1.1/";
    }
    
    private final Namespace getDCNamespace() {
        return DCModuleParser.DC_NS;
    }
    
    private final Namespace getRDFNamespace() {
        return DCModuleParser.RDF_NS;
    }
    
    private final Namespace getTaxonomyNamespace() {
        return DCModuleParser.TAXO_NS;
    }
    
    public Module parse(final Element dcRoot) {
        boolean foundSomething = false;
        final DCModule dcm = new DCModuleImpl();
        List eList = dcRoot.getChildren("title", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setTitles(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("creator", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setCreators(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("subject", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setSubjects(this.parseSubjects(eList));
        }
        eList = dcRoot.getChildren("description", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setDescriptions(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("publisher", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setPublishers(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("contributor", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setContributors(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("date", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setDates(this.parseElementListDate(eList));
        }
        eList = dcRoot.getChildren("type", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setTypes(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("format", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setFormats(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("identifier", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setIdentifiers(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("source", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setSources(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("language", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setLanguages(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("relation", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setRelations(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("coverage", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setCoverages(this.parseElementList(eList));
        }
        eList = dcRoot.getChildren("rights", this.getDCNamespace());
        if (eList.size() > 0) {
            foundSomething = true;
            dcm.setRightsList(this.parseElementList(eList));
        }
        return foundSomething ? dcm : null;
    }
    
    protected final String getTaxonomy(final Element desc) {
        String d = null;
        final Element taxo = desc.getChild("topic", this.getTaxonomyNamespace());
        if (taxo != null) {
            final Attribute a = taxo.getAttribute("resource", this.getRDFNamespace());
            if (a != null) {
                d = a.getValue();
            }
        }
        return d;
    }
    
    protected final List parseSubjects(final List eList) {
        final List subjects = new ArrayList();
        for (final Element eSubject : eList) {
            final Element eDesc = eSubject.getChild("Description", this.getRDFNamespace());
            if (eDesc != null) {
                final String taxonomy = this.getTaxonomy(eDesc);
                final List eValues = eDesc.getChildren("value", this.getRDFNamespace());
                for (final Element eValue : eValues) {
                    final DCSubject subject = new DCSubjectImpl();
                    subject.setTaxonomyUri(taxonomy);
                    subject.setValue(eValue.getText());
                    subjects.add(subject);
                }
            }
            else {
                final DCSubject subject2 = new DCSubjectImpl();
                subject2.setValue(eSubject.getText());
                subjects.add(subject2);
            }
        }
        return subjects;
    }
    
    protected final List parseElementList(final List eList) {
        final List values = new ArrayList();
        for (final Element e : eList) {
            values.add(e.getText());
        }
        return values;
    }
    
    protected final List parseElementListDate(final List eList) {
        final List values = new ArrayList();
        for (final Element e : eList) {
            values.add(DateParser.parseDate(e.getText()));
        }
        return values;
    }
    
    static {
        DC_NS = Namespace.getNamespace("http://purl.org/dc/elements/1.1/");
        RDF_NS = Namespace.getNamespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        TAXO_NS = Namespace.getNamespace("http://purl.org/rss/1.0/modules/taxonomy/");
    }
}
