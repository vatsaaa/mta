// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.Description;
import com.sun.syndication.feed.rss.Category;
import java.util.List;
import com.sun.syndication.feed.rss.Enclosure;
import java.util.ArrayList;
import com.sun.syndication.feed.rss.Source;
import com.sun.syndication.feed.rss.Item;
import com.sun.syndication.feed.rss.Cloud;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Element;

public class RSS092Parser extends RSS091UserlandParser
{
    public RSS092Parser() {
        this("rss_0.92");
    }
    
    protected RSS092Parser(final String type) {
        super(type);
    }
    
    protected String getRSSVersion() {
        return "0.92";
    }
    
    protected WireFeed parseChannel(final Element rssRoot) {
        final Channel channel = (Channel)super.parseChannel(rssRoot);
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        final Element eCloud = eChannel.getChild("cloud", this.getRSSNamespace());
        if (eCloud != null) {
            final Cloud cloud = new Cloud();
            String att = eCloud.getAttributeValue("domain");
            if (att != null) {
                cloud.setDomain(att);
            }
            att = eCloud.getAttributeValue("port");
            if (att != null) {
                cloud.setPort(Integer.parseInt(att.trim()));
            }
            att = eCloud.getAttributeValue("path");
            if (att != null) {
                cloud.setPath(att);
            }
            att = eCloud.getAttributeValue("registerProcedure");
            if (att != null) {
                cloud.setRegisterProcedure(att);
            }
            att = eCloud.getAttributeValue("protocol");
            if (att != null) {
                cloud.setProtocol(att);
            }
            channel.setCloud(cloud);
        }
        return channel;
    }
    
    protected Item parseItem(final Element rssRoot, final Element eItem) {
        final Item item = super.parseItem(rssRoot, eItem);
        Element e = eItem.getChild("source", this.getRSSNamespace());
        if (e != null) {
            final Source source = new Source();
            final String url = e.getAttributeValue("url");
            source.setUrl(url);
            source.setValue(e.getText());
            item.setSource(source);
        }
        final List eEnclosures = eItem.getChildren("enclosure");
        if (eEnclosures.size() > 0) {
            final List enclosures = new ArrayList();
            for (int i = 0; i < eEnclosures.size(); ++i) {
                e = eEnclosures.get(i);
                final Enclosure enclosure = new Enclosure();
                String att = e.getAttributeValue("url");
                if (att != null) {
                    enclosure.setUrl(att);
                }
                att = e.getAttributeValue("length");
                if (att != null && att.trim().length() > 0) {
                    enclosure.setLength(Long.parseLong(att.trim()));
                }
                att = e.getAttributeValue("type");
                if (att != null) {
                    enclosure.setType(att);
                }
                enclosures.add(enclosure);
            }
            item.setEnclosures(enclosures);
        }
        final List eCats = eItem.getChildren("category");
        item.setCategories(this.parseCategories(eCats));
        return item;
    }
    
    protected List parseCategories(final List eCats) {
        List cats = null;
        if (eCats.size() > 0) {
            cats = new ArrayList();
            for (int i = 0; i < eCats.size(); ++i) {
                final Category cat = new Category();
                final Element e = eCats.get(i);
                final String att = e.getAttributeValue("domain");
                if (att != null) {
                    cat.setDomain(att);
                }
                cat.setValue(e.getText());
                cats.add(cat);
            }
        }
        return cats;
    }
    
    protected Description parseItemDescription(final Element rssRoot, final Element eDesc) {
        final Description desc = super.parseItemDescription(rssRoot, eDesc);
        desc.setType("text/html");
        return desc;
    }
}
