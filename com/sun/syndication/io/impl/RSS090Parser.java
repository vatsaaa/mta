// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.TextInput;
import com.sun.syndication.feed.rss.Item;
import java.util.Iterator;
import com.sun.syndication.feed.rss.Image;
import com.sun.syndication.feed.module.Extendable;
import java.util.Collection;
import java.util.ArrayList;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.feed.WireFeed;
import java.util.List;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.Namespace;

public class RSS090Parser extends BaseWireFeedParser
{
    private static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    private static final String RSS_URI = "http://my.netscape.com/rdf/simple/0.9/";
    private static final String CONTENT_URI = "http://purl.org/rss/1.0/modules/content/";
    private static final Namespace RDF_NS;
    private static final Namespace RSS_NS;
    private static final Namespace CONTENT_NS;
    
    public RSS090Parser() {
        this("rss_0.9");
    }
    
    protected RSS090Parser(final String type) {
        super(type);
    }
    
    public boolean isMyType(final Document document) {
        boolean ok = false;
        final Element rssRoot = document.getRootElement();
        final Namespace defaultNS = rssRoot.getNamespace();
        final List additionalNSs = rssRoot.getAdditionalNamespaces();
        ok = (defaultNS != null && defaultNS.equals(this.getRDFNamespace()));
        if (ok) {
            if (additionalNSs == null) {
                ok = false;
            }
            else {
                ok = false;
                for (int i = 0; !ok && i < additionalNSs.size(); ok = this.getRSSNamespace().equals(additionalNSs.get(i)), ++i) {}
            }
        }
        return ok;
    }
    
    public WireFeed parse(final Document document, final boolean validate) throws IllegalArgumentException, FeedException {
        if (validate) {
            this.validateFeed(document);
        }
        final Element rssRoot = document.getRootElement();
        return this.parseChannel(rssRoot);
    }
    
    protected void validateFeed(final Document document) throws FeedException {
    }
    
    protected Namespace getRSSNamespace() {
        return RSS090Parser.RSS_NS;
    }
    
    protected Namespace getRDFNamespace() {
        return RSS090Parser.RDF_NS;
    }
    
    protected Namespace getContentNamespace() {
        return RSS090Parser.CONTENT_NS;
    }
    
    protected WireFeed parseChannel(final Element rssRoot) {
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        final Channel channel = new Channel(this.getType());
        Element e = eChannel.getChild("title", this.getRSSNamespace());
        if (e != null) {
            channel.setTitle(e.getText());
        }
        e = eChannel.getChild("link", this.getRSSNamespace());
        if (e != null) {
            channel.setLink(e.getText());
        }
        e = eChannel.getChild("description", this.getRSSNamespace());
        if (e != null) {
            channel.setDescription(e.getText());
        }
        channel.setImage(this.parseImage(rssRoot));
        channel.setTextInput(this.parseTextInput(rssRoot));
        final List allFeedModules = new ArrayList();
        final List rootModules = this.parseFeedModules(rssRoot);
        final List channelModules = this.parseFeedModules(eChannel);
        if (rootModules != null) {
            allFeedModules.addAll(rootModules);
        }
        if (channelModules != null) {
            allFeedModules.addAll(channelModules);
        }
        channel.setModules(allFeedModules);
        channel.setItems(this.parseItems(rssRoot));
        final List foreignMarkup = this.extractForeignMarkup(eChannel, channel, this.getRSSNamespace());
        if (foreignMarkup.size() > 0) {
            channel.setForeignMarkup(foreignMarkup);
        }
        return channel;
    }
    
    protected List getItems(final Element rssRoot) {
        return rssRoot.getChildren("item", this.getRSSNamespace());
    }
    
    protected Element getImage(final Element rssRoot) {
        return rssRoot.getChild("image", this.getRSSNamespace());
    }
    
    protected Element getTextInput(final Element rssRoot) {
        return rssRoot.getChild("textinput", this.getRSSNamespace());
    }
    
    protected Image parseImage(final Element rssRoot) {
        Image image = null;
        final Element eImage = this.getImage(rssRoot);
        if (eImage != null) {
            image = new Image();
            Element e = eImage.getChild("title", this.getRSSNamespace());
            if (e != null) {
                image.setTitle(e.getText());
            }
            e = eImage.getChild("url", this.getRSSNamespace());
            if (e != null) {
                image.setUrl(e.getText());
            }
            e = eImage.getChild("link", this.getRSSNamespace());
            if (e != null) {
                image.setLink(e.getText());
            }
        }
        return image;
    }
    
    protected List parseItems(final Element rssRoot) {
        final Collection eItems = this.getItems(rssRoot);
        final List items = new ArrayList();
        for (final Element eItem : eItems) {
            items.add(this.parseItem(rssRoot, eItem));
        }
        return items;
    }
    
    protected Item parseItem(final Element rssRoot, final Element eItem) {
        final Item item = new Item();
        Element e = eItem.getChild("title", this.getRSSNamespace());
        if (e != null) {
            item.setTitle(e.getText());
        }
        e = eItem.getChild("link", this.getRSSNamespace());
        if (e != null) {
            item.setLink(e.getText());
        }
        item.setModules(this.parseItemModules(eItem));
        final List foreignMarkup = this.extractForeignMarkup(eItem, item, this.getRSSNamespace());
        if (foreignMarkup.size() > 0) {
            item.setForeignMarkup(foreignMarkup);
        }
        return item;
    }
    
    protected TextInput parseTextInput(final Element rssRoot) {
        TextInput textInput = null;
        final Element eTextInput = this.getTextInput(rssRoot);
        if (eTextInput != null) {
            textInput = new TextInput();
            Element e = eTextInput.getChild("title", this.getRSSNamespace());
            if (e != null) {
                textInput.setTitle(e.getText());
            }
            e = eTextInput.getChild("description", this.getRSSNamespace());
            if (e != null) {
                textInput.setDescription(e.getText());
            }
            e = eTextInput.getChild("name", this.getRSSNamespace());
            if (e != null) {
                textInput.setName(e.getText());
            }
            e = eTextInput.getChild("link", this.getRSSNamespace());
            if (e != null) {
                textInput.setLink(e.getText());
            }
        }
        return textInput;
    }
    
    static {
        RDF_NS = Namespace.getNamespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        RSS_NS = Namespace.getNamespace("http://my.netscape.com/rdf/simple/0.9/");
        CONTENT_NS = Namespace.getNamespace("http://purl.org/rss/1.0/modules/content/");
    }
}
