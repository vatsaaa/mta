// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.Description;
import com.sun.syndication.feed.rss.Guid;
import java.util.Date;
import com.sun.syndication.feed.rss.Item;
import java.util.List;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Element;

public class RSS094Parser extends RSS093Parser
{
    public RSS094Parser() {
        this("rss_0.94");
    }
    
    protected RSS094Parser(final String type) {
        super(type);
    }
    
    protected String getRSSVersion() {
        return "0.94";
    }
    
    protected WireFeed parseChannel(final Element rssRoot) {
        final Channel channel = (Channel)super.parseChannel(rssRoot);
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        final List eCats = eChannel.getChildren("category", this.getRSSNamespace());
        channel.setCategories(this.parseCategories(eCats));
        final Element eTtl = eChannel.getChild("ttl", this.getRSSNamespace());
        if (eTtl != null && eTtl.getText() != null) {
            Integer ttlValue = null;
            try {
                ttlValue = new Integer(eTtl.getText());
            }
            catch (NumberFormatException ex) {}
            if (ttlValue != null) {
                channel.setTtl(ttlValue);
            }
        }
        return channel;
    }
    
    public Item parseItem(final Element rssRoot, final Element eItem) {
        final Item item = super.parseItem(rssRoot, eItem);
        item.setExpirationDate(null);
        Element e = eItem.getChild("author", this.getRSSNamespace());
        if (e != null) {
            item.setAuthor(e.getText());
        }
        e = eItem.getChild("guid", this.getRSSNamespace());
        if (e != null) {
            final Guid guid = new Guid();
            final String att = e.getAttributeValue("isPermaLink");
            if (att != null) {
                guid.setPermaLink(att.equalsIgnoreCase("true"));
            }
            guid.setValue(e.getText());
            item.setGuid(guid);
        }
        e = eItem.getChild("comments", this.getRSSNamespace());
        if (e != null) {
            item.setComments(e.getText());
        }
        return item;
    }
    
    protected Description parseItemDescription(final Element rssRoot, final Element eDesc) {
        final Description desc = super.parseItemDescription(rssRoot, eDesc);
        String att = eDesc.getAttributeValue("type");
        if (att == null) {
            att = "text/html";
        }
        desc.setType(att);
        return desc;
    }
}
