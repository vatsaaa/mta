// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import org.jdom.Attribute;
import java.net.MalformedURLException;
import com.sun.syndication.feed.atom.Category;
import java.util.Iterator;
import org.jdom.output.XMLOutputter;
import com.sun.syndication.feed.atom.Person;
import java.util.ArrayList;
import org.jdom.Parent;
import com.sun.syndication.feed.atom.Link;
import java.util.List;
import com.sun.syndication.feed.module.Extendable;
import com.sun.syndication.feed.atom.Generator;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.feed.atom.Content;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Element;
import org.jdom.Document;
import java.util.regex.Pattern;
import org.jdom.Namespace;

public class Atom10Parser extends BaseWireFeedParser
{
    private static final String ATOM_10_URI = "http://www.w3.org/2005/Atom";
    Namespace ns;
    static Pattern absoluteURIPattern;
    
    public Atom10Parser() {
        this("atom_1.0");
    }
    
    protected Atom10Parser(final String type) {
        super(type);
        this.ns = Namespace.getNamespace("http://www.w3.org/2005/Atom");
    }
    
    protected Namespace getAtomNamespace() {
        return this.ns;
    }
    
    public boolean isMyType(final Document document) {
        final Element rssRoot = document.getRootElement();
        final Namespace defaultNS = rssRoot.getNamespace();
        return defaultNS != null && defaultNS.equals(this.getAtomNamespace());
    }
    
    public WireFeed parse(final Document document, final boolean validate) throws IllegalArgumentException, FeedException {
        if (validate) {
            this.validateFeed(document);
        }
        final Element rssRoot = document.getRootElement();
        return this.parseFeed(rssRoot);
    }
    
    protected void validateFeed(final Document document) throws FeedException {
    }
    
    protected WireFeed parseFeed(final Element eFeed) throws FeedException {
        final Feed feed = new Feed(this.getType());
        String baseURI = null;
        try {
            baseURI = this.findBaseURI(eFeed);
        }
        catch (Exception e) {
            throw new FeedException("ERROR while finding base URI of feed", e);
        }
        final String xmlBase = eFeed.getAttributeValue("base", Namespace.XML_NAMESPACE);
        if (xmlBase != null) {
            feed.setXmlBase(xmlBase);
        }
        Element e2 = eFeed.getChild("title", this.getAtomNamespace());
        if (e2 != null) {
            final Content c = new Content();
            c.setValue(this.parseTextConstructToString(e2));
            c.setType(e2.getAttributeValue("type"));
            feed.setTitleEx(c);
        }
        List eList = eFeed.getChildren("link", this.getAtomNamespace());
        feed.setAlternateLinks(this.parseAlternateLinks(feed, null, baseURI, eList));
        feed.setOtherLinks(this.parseOtherLinks(feed, null, baseURI, eList));
        final List cList = eFeed.getChildren("category", this.getAtomNamespace());
        feed.setCategories(this.parseCategories(baseURI, cList));
        eList = eFeed.getChildren("author", this.getAtomNamespace());
        if (eList.size() > 0) {
            feed.setAuthors(this.parsePersons(baseURI, eList));
        }
        eList = eFeed.getChildren("contributor", this.getAtomNamespace());
        if (eList.size() > 0) {
            feed.setContributors(this.parsePersons(baseURI, eList));
        }
        e2 = eFeed.getChild("subtitle", this.getAtomNamespace());
        if (e2 != null) {
            final Content subtitle = new Content();
            subtitle.setValue(this.parseTextConstructToString(e2));
            subtitle.setType(e2.getAttributeValue("type"));
            feed.setSubtitle(subtitle);
        }
        e2 = eFeed.getChild("id", this.getAtomNamespace());
        if (e2 != null) {
            feed.setId(e2.getText());
        }
        e2 = eFeed.getChild("generator", this.getAtomNamespace());
        if (e2 != null) {
            final Generator gen = new Generator();
            gen.setValue(e2.getText());
            String att = e2.getAttributeValue("uri");
            if (att != null) {
                gen.setUrl(att);
            }
            att = e2.getAttributeValue("version");
            if (att != null) {
                gen.setVersion(att);
            }
            feed.setGenerator(gen);
        }
        e2 = eFeed.getChild("rights", this.getAtomNamespace());
        if (e2 != null) {
            feed.setRights(this.parseTextConstructToString(e2));
        }
        e2 = eFeed.getChild("icon", this.getAtomNamespace());
        if (e2 != null) {
            feed.setIcon(e2.getText());
        }
        e2 = eFeed.getChild("logo", this.getAtomNamespace());
        if (e2 != null) {
            feed.setLogo(e2.getText());
        }
        e2 = eFeed.getChild("updated", this.getAtomNamespace());
        if (e2 != null) {
            feed.setUpdated(DateParser.parseDate(e2.getText()));
        }
        feed.setModules(this.parseFeedModules(eFeed));
        eList = eFeed.getChildren("entry", this.getAtomNamespace());
        if (eList.size() > 0) {
            feed.setEntries(this.parseEntries(feed, baseURI, eList));
        }
        final List foreignMarkup = this.extractForeignMarkup(eFeed, feed, this.getAtomNamespace());
        if (foreignMarkup.size() > 0) {
            feed.setForeignMarkup(foreignMarkup);
        }
        return feed;
    }
    
    private Link parseLink(final Feed feed, final Entry entry, final String baseURI, final Element eLink) {
        final Link link = new Link();
        String att = eLink.getAttributeValue("rel");
        if (att != null) {
            link.setRel(att);
        }
        att = eLink.getAttributeValue("type");
        if (att != null) {
            link.setType(att);
        }
        att = eLink.getAttributeValue("href");
        if (att != null) {
            if (this.isRelativeURI(att)) {
                link.setHref(this.resolveURI(baseURI, eLink, att));
            }
            else {
                link.setHref(att);
            }
        }
        att = eLink.getAttributeValue("title");
        if (att != null) {
            link.setTitle(att);
        }
        att = eLink.getAttributeValue("hreflang");
        if (att != null) {
            link.setHreflang(att);
        }
        att = eLink.getAttributeValue("length");
        if (att != null) {
            link.setLength(Long.parseLong(att));
        }
        return link;
    }
    
    private List parseAlternateLinks(final Feed feed, final Entry entry, final String baseURI, final List eLinks) {
        final List links = new ArrayList();
        for (int i = 0; i < eLinks.size(); ++i) {
            final Element eLink = eLinks.get(i);
            final Link link = this.parseLink(feed, entry, baseURI, eLink);
            if (link.getRel() == null || "".equals(link.getRel().trim()) || "alternate".equals(link.getRel())) {
                links.add(link);
            }
        }
        return (links.size() > 0) ? links : null;
    }
    
    private List parseOtherLinks(final Feed feed, final Entry entry, final String baseURI, final List eLinks) {
        final List links = new ArrayList();
        for (int i = 0; i < eLinks.size(); ++i) {
            final Element eLink = eLinks.get(i);
            final Link link = this.parseLink(feed, entry, baseURI, eLink);
            if (!"alternate".equals(link.getRel())) {
                links.add(link);
            }
        }
        return (links.size() > 0) ? links : null;
    }
    
    private Person parsePerson(final String baseURI, final Element ePerson) {
        final Person person = new Person();
        Element e = ePerson.getChild("name", this.getAtomNamespace());
        if (e != null) {
            person.setName(e.getText());
        }
        e = ePerson.getChild("uri", this.getAtomNamespace());
        if (e != null) {
            person.setUri(this.resolveURI(baseURI, ePerson, e.getText()));
        }
        e = ePerson.getChild("email", this.getAtomNamespace());
        if (e != null) {
            person.setEmail(e.getText());
        }
        return person;
    }
    
    private List parsePersons(final String baseURI, final List ePersons) {
        final List persons = new ArrayList();
        for (int i = 0; i < ePersons.size(); ++i) {
            persons.add(this.parsePerson(baseURI, ePersons.get(i)));
        }
        return (persons.size() > 0) ? persons : null;
    }
    
    private Content parseContent(final Element e) {
        final String value = this.parseTextConstructToString(e);
        final String src = e.getAttributeValue("src");
        final String type = e.getAttributeValue("type");
        final Content content = new Content();
        content.setSrc(src);
        content.setType(type);
        content.setValue(value);
        return content;
    }
    
    private String parseTextConstructToString(final Element e) {
        String value = null;
        String type = e.getAttributeValue("type");
        type = ((type != null) ? type : "text");
        if (type.equals("xhtml")) {
            final XMLOutputter outputter = new XMLOutputter();
            final List eContent = e.getContent();
            for (final org.jdom.Content c : eContent) {
                if (c instanceof Element) {
                    final Element eC = (Element)c;
                    if (!eC.getNamespace().equals(this.getAtomNamespace())) {
                        continue;
                    }
                    ((Element)c).setNamespace(Namespace.NO_NAMESPACE);
                }
            }
            value = outputter.outputString(eContent);
        }
        else {
            value = e.getText();
        }
        return value;
    }
    
    protected List parseEntries(final Feed feed, final String baseURI, final List eEntries) {
        final List entries = new ArrayList();
        for (int i = 0; i < eEntries.size(); ++i) {
            entries.add(this.parseEntry(feed, eEntries.get(i), baseURI));
        }
        return (entries.size() > 0) ? entries : null;
    }
    
    protected Entry parseEntry(final Feed feed, final Element eEntry, final String baseURI) {
        final Entry entry = new Entry();
        final String xmlBase = eEntry.getAttributeValue("base", Namespace.XML_NAMESPACE);
        if (xmlBase != null) {
            entry.setXmlBase(xmlBase);
        }
        Element e = eEntry.getChild("title", this.getAtomNamespace());
        if (e != null) {
            final Content c = new Content();
            c.setValue(this.parseTextConstructToString(e));
            c.setType(e.getAttributeValue("type"));
            entry.setTitleEx(c);
        }
        List eList = eEntry.getChildren("link", this.getAtomNamespace());
        entry.setAlternateLinks(this.parseAlternateLinks(feed, entry, baseURI, eList));
        entry.setOtherLinks(this.parseOtherLinks(feed, entry, baseURI, eList));
        eList = eEntry.getChildren("author", this.getAtomNamespace());
        if (eList.size() > 0) {
            entry.setAuthors(this.parsePersons(baseURI, eList));
        }
        eList = eEntry.getChildren("contributor", this.getAtomNamespace());
        if (eList.size() > 0) {
            entry.setContributors(this.parsePersons(baseURI, eList));
        }
        e = eEntry.getChild("id", this.getAtomNamespace());
        if (e != null) {
            entry.setId(e.getText());
        }
        e = eEntry.getChild("updated", this.getAtomNamespace());
        if (e != null) {
            entry.setUpdated(DateParser.parseW3CDateTime(e.getText()));
        }
        e = eEntry.getChild("published", this.getAtomNamespace());
        if (e != null) {
            entry.setPublished(DateParser.parseW3CDateTime(e.getText()));
        }
        e = eEntry.getChild("summary", this.getAtomNamespace());
        if (e != null) {
            entry.setSummary(this.parseContent(e));
        }
        e = eEntry.getChild("content", this.getAtomNamespace());
        if (e != null) {
            final List contents = new ArrayList();
            contents.add(this.parseContent(e));
            entry.setContents(contents);
        }
        e = eEntry.getChild("rights", this.getAtomNamespace());
        if (e != null) {
            entry.setRights(e.getText());
        }
        final List cList = eEntry.getChildren("category", this.getAtomNamespace());
        entry.setCategories(this.parseCategories(baseURI, cList));
        entry.setModules(this.parseItemModules(eEntry));
        final List foreignMarkup = this.extractForeignMarkup(eEntry, entry, this.getAtomNamespace());
        if (foreignMarkup.size() > 0) {
            entry.setForeignMarkup(foreignMarkup);
        }
        return entry;
    }
    
    private List parseCategories(final String baseURI, final List eCategories) {
        final List cats = new ArrayList();
        for (int i = 0; i < eCategories.size(); ++i) {
            final Element eCategory = eCategories.get(i);
            cats.add(this.parseCategory(baseURI, eCategory));
        }
        return (cats.size() > 0) ? cats : null;
    }
    
    private Category parseCategory(final String baseURI, final Element eCategory) {
        final Category category = new Category();
        String att = eCategory.getAttributeValue("term");
        if (att != null) {
            category.setTerm(att);
        }
        att = eCategory.getAttributeValue("scheme");
        if (att != null) {
            category.setScheme(this.resolveURI(baseURI, eCategory, att));
        }
        att = eCategory.getAttributeValue("label");
        if (att != null) {
            category.setLabel(att);
        }
        return category;
    }
    
    private boolean isAbsoluteURI(final String uri) {
        return Atom10Parser.absoluteURIPattern.matcher(uri).find();
    }
    
    private boolean isRelativeURI(final String uri) {
        return !this.isAbsoluteURI(uri);
    }
    
    private String resolveURI(final String baseURI, final Parent parent, String url) {
        if (this.isRelativeURI(url)) {
            url = ((!".".equals(url) && !"./".equals(url)) ? url : "");
            if (parent != null && parent instanceof Element) {
                String xmlbase = ((Element)parent).getAttributeValue("base", Namespace.XML_NAMESPACE);
                if (xmlbase == null || xmlbase.trim().length() <= 0) {
                    return this.resolveURI(baseURI, parent.getParent(), url);
                }
                if (!this.isAbsoluteURI(xmlbase)) {
                    return this.resolveURI(baseURI, parent.getParent(), stripTrailingSlash(xmlbase) + "/" + stripStartingSlash(url));
                }
                if (url.startsWith("/")) {
                    final int slashslash = xmlbase.indexOf("//");
                    final int nextslash = xmlbase.indexOf("/", slashslash + 2);
                    if (nextslash != -1) {
                        xmlbase = xmlbase.substring(0, nextslash);
                    }
                    return formURI(xmlbase, url);
                }
                if (!xmlbase.endsWith("/")) {
                    xmlbase = xmlbase.substring(0, xmlbase.lastIndexOf("/"));
                }
                return formURI(xmlbase, url);
            }
            else if (parent == null || parent instanceof Document) {
                return formURI(baseURI, url);
            }
        }
        return url;
    }
    
    private String findBaseURI(final Element root) throws MalformedURLException {
        String ret = this.findAtomLink(root, "alternate");
        if (ret != null && this.isRelativeURI(ret)) {
            String self = this.findAtomLink(root, "self");
            if (self != null) {
                self = this.resolveURI(null, root, self);
                self = self.substring(0, self.lastIndexOf("/"));
                ret = this.resolveURI(self, root, ret);
            }
        }
        return ret;
    }
    
    private String findAtomLink(final Element parent, final String rel) {
        String ret = null;
        final List linksList = parent.getChildren("link", this.ns);
        if (linksList != null) {
            for (final Element link : linksList) {
                final Attribute relAtt = link.getAttribute("rel");
                final Attribute hrefAtt = link.getAttribute("href");
                if ((relAtt == null && "alternate".equals(rel)) || (relAtt != null && relAtt.getValue().equals(rel))) {
                    ret = hrefAtt.getValue();
                    break;
                }
            }
        }
        return ret;
    }
    
    private static String formURI(String base, String append) {
        base = stripTrailingSlash(base);
        append = stripStartingSlash(append);
        if (append.startsWith("..")) {
            final String ret = null;
            final String[] parts = append.split("/");
            for (int i = 0; i < parts.length; ++i) {
                if ("..".equals(parts[i])) {
                    final int last = base.lastIndexOf("/");
                    if (last == -1) {
                        break;
                    }
                    base = base.substring(0, last);
                    append = append.substring(3, append.length());
                }
            }
        }
        return base + "/" + append;
    }
    
    private static String stripStartingSlash(String s) {
        if (s != null && s.startsWith("/")) {
            s = s.substring(1, s.length());
        }
        return s;
    }
    
    private static String stripTrailingSlash(String s) {
        if (s != null && s.endsWith("/")) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }
    
    static {
        Atom10Parser.absoluteURIPattern = Pattern.compile("^[a-z0-9]*:.*$");
    }
}
