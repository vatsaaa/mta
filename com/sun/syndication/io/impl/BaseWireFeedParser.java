// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.Iterator;
import java.util.ArrayList;
import org.jdom.Namespace;
import com.sun.syndication.feed.module.Extendable;
import java.util.List;
import org.jdom.Element;
import com.sun.syndication.io.WireFeedParser;

public abstract class BaseWireFeedParser implements WireFeedParser
{
    private static final String FEED_MODULE_PARSERS_POSFIX_KEY = ".feed.ModuleParser.classes";
    private static final String ITEM_MODULE_PARSERS_POSFIX_KEY = ".item.ModuleParser.classes";
    private String _type;
    private ModuleParsers _feedModuleParsers;
    private ModuleParsers _itemModuleParsers;
    
    protected BaseWireFeedParser(final String type) {
        this._type = type;
        this._feedModuleParsers = new ModuleParsers(type + ".feed.ModuleParser.classes", this);
        this._itemModuleParsers = new ModuleParsers(type + ".item.ModuleParser.classes", this);
    }
    
    public String getType() {
        return this._type;
    }
    
    protected List parseFeedModules(final Element feedElement) {
        return this._feedModuleParsers.parseModules(feedElement);
    }
    
    protected List parseItemModules(final Element itemElement) {
        return this._itemModuleParsers.parseModules(itemElement);
    }
    
    protected List extractForeignMarkup(final Element e, final Extendable ext, final Namespace basens) {
        final ArrayList foreignMarkup = new ArrayList();
        for (final Element elem : e.getChildren()) {
            if (!basens.equals(elem.getNamespace()) && null == ext.getModule(elem.getNamespaceURI())) {
                foreignMarkup.add(elem.clone());
            }
        }
        for (final Element elem2 : foreignMarkup) {
            elem2.detach();
        }
        return foreignMarkup;
    }
}
