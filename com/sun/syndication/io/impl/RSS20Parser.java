// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import org.jdom.Attribute;
import org.jdom.Document;
import com.sun.syndication.feed.rss.Description;
import org.jdom.Element;

public class RSS20Parser extends RSS094Parser
{
    public RSS20Parser() {
        this("rss_2.0");
    }
    
    protected RSS20Parser(final String type) {
        super(type);
    }
    
    protected String getRSSVersion() {
        return "2.0";
    }
    
    protected boolean isHourFormat24(final Element rssRoot) {
        return false;
    }
    
    protected Description parseItemDescription(final Element rssRoot, final Element eDesc) {
        final Description desc = super.parseItemDescription(rssRoot, eDesc);
        desc.setType("text/html");
        return desc;
    }
    
    public boolean isMyType(final Document document) {
        final Element rssRoot = document.getRootElement();
        boolean ok = rssRoot.getName().equals("rss");
        if (ok) {
            ok = false;
            final Attribute version = rssRoot.getAttribute("version");
            if (version != null) {
                ok = version.getValue().startsWith(this.getRSSVersion());
            }
        }
        return ok;
    }
}
