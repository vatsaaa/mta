// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.atom.Generator;
import java.util.Collection;
import java.io.Reader;
import org.jdom.input.SAXBuilder;
import java.io.StringReader;
import com.sun.syndication.feed.atom.Person;
import com.sun.syndication.feed.atom.Link;
import org.jdom.Content;
import com.sun.syndication.feed.atom.Entry;
import java.util.List;
import org.jdom.Attribute;
import com.sun.syndication.io.FeedException;
import org.jdom.Element;
import com.sun.syndication.feed.atom.Feed;
import org.jdom.Document;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Namespace;

public class Atom03Generator extends BaseWireFeedGenerator
{
    private static final String ATOM_03_URI = "http://purl.org/atom/ns#";
    private static final Namespace ATOM_NS;
    private String _version;
    
    public Atom03Generator() {
        this("atom_0.3", "0.3");
    }
    
    protected Atom03Generator(final String type, final String version) {
        super(type);
        this._version = version;
    }
    
    protected String getVersion() {
        return this._version;
    }
    
    protected Namespace getFeedNamespace() {
        return Atom03Generator.ATOM_NS;
    }
    
    public Document generate(final WireFeed wFeed) throws FeedException {
        final Feed feed = (Feed)wFeed;
        final Element root = this.createRootElement(feed);
        this.populateFeed(feed, root);
        return this.createDocument(root);
    }
    
    protected Document createDocument(final Element root) {
        return new Document(root);
    }
    
    protected Element createRootElement(final Feed feed) {
        final Element root = new Element("feed", this.getFeedNamespace());
        root.addNamespaceDeclaration(this.getFeedNamespace());
        final Attribute version = new Attribute("version", this.getVersion());
        root.setAttribute(version);
        this.generateModuleNamespaceDefs(root);
        return root;
    }
    
    protected void populateFeed(final Feed feed, final Element parent) throws FeedException {
        this.addFeed(feed, parent);
        this.addEntries(feed, parent);
    }
    
    protected void addFeed(final Feed feed, final Element parent) throws FeedException {
        final Element eFeed = parent;
        this.populateFeedHeader(feed, eFeed);
        this.checkFeedHeaderConstraints(eFeed);
        this.generateFeedModules(feed.getModules(), eFeed);
        this.generateForeignMarkup(eFeed, (List)feed.getForeignMarkup());
    }
    
    protected void addEntries(final Feed feed, final Element parent) throws FeedException {
        final List items = feed.getEntries();
        for (int i = 0; i < items.size(); ++i) {
            this.addEntry(items.get(i), parent);
        }
        this.checkEntriesConstraints(parent);
    }
    
    protected void addEntry(final Entry entry, final Element parent) throws FeedException {
        final Element eEntry = new Element("entry", this.getFeedNamespace());
        this.populateEntry(entry, eEntry);
        this.checkEntryConstraints(eEntry);
        this.generateItemModules(entry.getModules(), eEntry);
        parent.addContent(eEntry);
    }
    
    protected void populateFeedHeader(final Feed feed, final Element eFeed) throws FeedException {
        if (feed.getTitle() != null) {
            eFeed.addContent(this.generateSimpleElement("title", feed.getTitle()));
        }
        List links = feed.getAlternateLinks();
        for (int i = 0; i < links.size(); ++i) {
            eFeed.addContent(this.generateLinkElement(links.get(i)));
        }
        links = feed.getOtherLinks();
        for (int i = 0; i < links.size(); ++i) {
            eFeed.addContent(this.generateLinkElement(links.get(i)));
        }
        if (feed.getAuthors() != null && feed.getAuthors().size() > 0) {
            final Element authorElement = new Element("author", this.getFeedNamespace());
            this.fillPersonElement(authorElement, feed.getAuthors().get(0));
            eFeed.addContent(authorElement);
        }
        final List contributors = feed.getContributors();
        for (int j = 0; j < contributors.size(); ++j) {
            final Element contributorElement = new Element("contributor", this.getFeedNamespace());
            this.fillPersonElement(contributorElement, contributors.get(j));
            eFeed.addContent(contributorElement);
        }
        if (feed.getTagline() != null) {
            final Element taglineElement = new Element("tagline", this.getFeedNamespace());
            this.fillContentElement(taglineElement, feed.getTagline());
            eFeed.addContent(taglineElement);
        }
        if (feed.getId() != null) {
            eFeed.addContent(this.generateSimpleElement("id", feed.getId()));
        }
        if (feed.getGenerator() != null) {
            eFeed.addContent(this.generateGeneratorElement(feed.getGenerator()));
        }
        if (feed.getCopyright() != null) {
            eFeed.addContent(this.generateSimpleElement("copyright", feed.getCopyright()));
        }
        if (feed.getInfo() != null) {
            final Element infoElement = new Element("info", this.getFeedNamespace());
            this.fillContentElement(infoElement, feed.getInfo());
            eFeed.addContent(infoElement);
        }
        if (feed.getModified() != null) {
            final Element modifiedElement = new Element("modified", this.getFeedNamespace());
            modifiedElement.addContent(DateParser.formatW3CDateTime(feed.getModified()));
            eFeed.addContent(modifiedElement);
        }
    }
    
    protected void populateEntry(final Entry entry, final Element eEntry) throws FeedException {
        if (entry.getTitle() != null) {
            eEntry.addContent(this.generateSimpleElement("title", entry.getTitle()));
        }
        List links = entry.getAlternateLinks();
        for (int i = 0; i < links.size(); ++i) {
            eEntry.addContent(this.generateLinkElement(links.get(i)));
        }
        links = entry.getOtherLinks();
        for (int i = 0; i < links.size(); ++i) {
            eEntry.addContent(this.generateLinkElement(links.get(i)));
        }
        if (entry.getAuthors() != null && entry.getAuthors().size() > 0) {
            final Element authorElement = new Element("author", this.getFeedNamespace());
            this.fillPersonElement(authorElement, entry.getAuthors().get(0));
            eEntry.addContent(authorElement);
        }
        final List contributors = entry.getContributors();
        for (int j = 0; j < contributors.size(); ++j) {
            final Element contributorElement = new Element("contributor", this.getFeedNamespace());
            this.fillPersonElement(contributorElement, contributors.get(j));
            eEntry.addContent(contributorElement);
        }
        if (entry.getId() != null) {
            eEntry.addContent(this.generateSimpleElement("id", entry.getId()));
        }
        if (entry.getModified() != null) {
            final Element modifiedElement = new Element("modified", this.getFeedNamespace());
            modifiedElement.addContent(DateParser.formatW3CDateTime(entry.getModified()));
            eEntry.addContent(modifiedElement);
        }
        if (entry.getIssued() != null) {
            final Element issuedElement = new Element("issued", this.getFeedNamespace());
            issuedElement.addContent(DateParser.formatW3CDateTime(entry.getIssued()));
            eEntry.addContent(issuedElement);
        }
        if (entry.getCreated() != null) {
            final Element createdElement = new Element("created", this.getFeedNamespace());
            createdElement.addContent(DateParser.formatW3CDateTime(entry.getCreated()));
            eEntry.addContent(createdElement);
        }
        if (entry.getSummary() != null) {
            final Element summaryElement = new Element("summary", this.getFeedNamespace());
            this.fillContentElement(summaryElement, entry.getSummary());
            eEntry.addContent(summaryElement);
        }
        final List contents = entry.getContents();
        for (int k = 0; k < contents.size(); ++k) {
            final Element contentElement = new Element("content", this.getFeedNamespace());
            this.fillContentElement(contentElement, contents.get(k));
            eEntry.addContent(contentElement);
        }
        this.generateForeignMarkup(eEntry, (List)entry.getForeignMarkup());
    }
    
    protected void checkFeedHeaderConstraints(final Element eFeed) throws FeedException {
    }
    
    protected void checkEntriesConstraints(final Element parent) throws FeedException {
    }
    
    protected void checkEntryConstraints(final Element eEntry) throws FeedException {
    }
    
    protected Element generateLinkElement(final Link link) {
        final Element linkElement = new Element("link", this.getFeedNamespace());
        if (link.getRel() != null) {
            final Attribute relAttribute = new Attribute("rel", link.getRel().toString());
            linkElement.setAttribute(relAttribute);
        }
        if (link.getType() != null) {
            final Attribute typeAttribute = new Attribute("type", link.getType());
            linkElement.setAttribute(typeAttribute);
        }
        if (link.getHref() != null) {
            final Attribute hrefAttribute = new Attribute("href", link.getHref());
            linkElement.setAttribute(hrefAttribute);
        }
        return linkElement;
    }
    
    protected void fillPersonElement(final Element element, final Person person) {
        if (person.getName() != null) {
            element.addContent(this.generateSimpleElement("name", person.getName()));
        }
        if (person.getUrl() != null) {
            element.addContent(this.generateSimpleElement("url", person.getUrl()));
        }
        if (person.getEmail() != null) {
            element.addContent(this.generateSimpleElement("email", person.getEmail()));
        }
    }
    
    protected Element generateTagLineElement(final com.sun.syndication.feed.atom.Content tagline) {
        final Element taglineElement = new Element("tagline", this.getFeedNamespace());
        if (tagline.getType() != null) {
            final Attribute typeAttribute = new Attribute("type", tagline.getType());
            taglineElement.setAttribute(typeAttribute);
        }
        if (tagline.getValue() != null) {
            taglineElement.addContent(tagline.getValue());
        }
        return taglineElement;
    }
    
    protected void fillContentElement(final Element contentElement, final com.sun.syndication.feed.atom.Content content) throws FeedException {
        if (content.getType() != null) {
            final Attribute typeAttribute = new Attribute("type", content.getType());
            contentElement.setAttribute(typeAttribute);
        }
        final String mode = content.getMode();
        if (mode != null) {
            final Attribute modeAttribute = new Attribute("mode", content.getMode().toString());
            contentElement.setAttribute(modeAttribute);
        }
        if (content.getValue() != null) {
            if (mode == null || mode.equals("escaped")) {
                contentElement.addContent(content.getValue());
            }
            else if (mode.equals("base64")) {
                contentElement.addContent(Base64.encode(content.getValue()));
            }
            else if (mode.equals("xml")) {
                final StringBuffer tmpDocString = new StringBuffer("<tmpdoc>");
                tmpDocString.append(content.getValue());
                tmpDocString.append("</tmpdoc>");
                final StringReader tmpDocReader = new StringReader(tmpDocString.toString());
                Document tmpDoc;
                try {
                    final SAXBuilder saxBuilder = new SAXBuilder();
                    tmpDoc = saxBuilder.build(tmpDocReader);
                }
                catch (Exception ex) {
                    throw new FeedException("Invalid XML", ex);
                }
                final List children = tmpDoc.getRootElement().removeContent();
                contentElement.addContent(children);
            }
        }
    }
    
    protected Element generateGeneratorElement(final Generator generator) {
        final Element generatorElement = new Element("generator", this.getFeedNamespace());
        if (generator.getUrl() != null) {
            final Attribute urlAttribute = new Attribute("url", generator.getUrl());
            generatorElement.setAttribute(urlAttribute);
        }
        if (generator.getVersion() != null) {
            final Attribute versionAttribute = new Attribute("version", generator.getVersion());
            generatorElement.setAttribute(versionAttribute);
        }
        if (generator.getValue() != null) {
            generatorElement.addContent(generator.getValue());
        }
        return generatorElement;
    }
    
    protected Element generateSimpleElement(final String name, final String value) {
        final Element element = new Element(name, this.getFeedNamespace());
        element.addContent(value);
        return element;
    }
    
    static {
        ATOM_NS = Namespace.getNamespace("http://purl.org/atom/ns#");
    }
}
