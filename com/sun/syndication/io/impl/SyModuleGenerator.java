// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.Collections;
import java.util.HashSet;
import org.jdom.Content;
import com.sun.syndication.feed.module.SyModule;
import org.jdom.Element;
import com.sun.syndication.feed.module.Module;
import java.util.Set;
import org.jdom.Namespace;
import com.sun.syndication.io.ModuleGenerator;

public class SyModuleGenerator implements ModuleGenerator
{
    private static final String SY_URI = "http://purl.org/rss/1.0/modules/syndication/";
    private static final Namespace SY_NS;
    private static final Set NAMESPACES;
    
    public String getNamespaceUri() {
        return "http://purl.org/rss/1.0/modules/syndication/";
    }
    
    public Set getNamespaces() {
        return SyModuleGenerator.NAMESPACES;
    }
    
    public void generate(final Module module, final Element element) {
        final SyModule syModule = (SyModule)module;
        final Element updatePeriodElement = new Element("updatePeriod", SyModuleGenerator.SY_NS);
        updatePeriodElement.addContent(syModule.getUpdatePeriod().toString());
        element.addContent(updatePeriodElement);
        final Element updateFrequencyElement = new Element("updateFrequency", SyModuleGenerator.SY_NS);
        updateFrequencyElement.addContent(String.valueOf(syModule.getUpdateFrequency()));
        element.addContent(updateFrequencyElement);
        final Element updateBaseElement = new Element("updateBase", SyModuleGenerator.SY_NS);
        updateBaseElement.addContent(DateParser.formatW3CDateTime(syModule.getUpdateBase()));
        element.addContent(updateBaseElement);
    }
    
    static {
        SY_NS = Namespace.getNamespace("sy", "http://purl.org/rss/1.0/modules/syndication/");
        final Set nss = new HashSet();
        nss.add(SyModuleGenerator.SY_NS);
        NAMESPACES = Collections.unmodifiableSet((Set<?>)nss);
    }
}
