// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.Description;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.WireFeed;
import com.sun.syndication.feed.rss.Content;
import com.sun.syndication.feed.rss.Item;
import java.util.List;
import org.jdom.Namespace;
import org.jdom.Element;
import org.jdom.Document;

public class RSS10Parser extends RSS090Parser
{
    private static final String RSS_URI = "http://purl.org/rss/1.0/";
    
    public RSS10Parser() {
        this("rss_1.0");
    }
    
    protected RSS10Parser(final String type) {
        super(type);
    }
    
    public boolean isMyType(final Document document) {
        boolean ok = false;
        final Element rssRoot = document.getRootElement();
        final Namespace defaultNS = rssRoot.getNamespace();
        final List additionalNSs = rssRoot.getAdditionalNamespaces();
        ok = (defaultNS != null && defaultNS.equals(this.getRDFNamespace()));
        if (ok) {
            if (additionalNSs == null) {
                ok = false;
            }
            else {
                ok = false;
                for (int i = 0; !ok && i < additionalNSs.size(); ok = this.getRSSNamespace().equals(additionalNSs.get(i)), ++i) {}
            }
        }
        return ok;
    }
    
    protected Namespace getRSSNamespace() {
        return Namespace.getNamespace("http://purl.org/rss/1.0/");
    }
    
    protected Item parseItem(final Element rssRoot, final Element eItem) {
        final Item item = super.parseItem(rssRoot, eItem);
        final Element e = eItem.getChild("description", this.getRSSNamespace());
        if (e != null) {
            item.setDescription(this.parseItemDescription(rssRoot, e));
        }
        final Element ce = eItem.getChild("encoded", this.getContentNamespace());
        if (ce != null) {
            final Content content = new Content();
            content.setType("html");
            content.setValue(ce.getText());
            item.setContent(content);
        }
        final String uri = eItem.getAttributeValue("about", this.getRDFNamespace());
        if (uri != null) {
            item.setUri(uri);
        }
        return item;
    }
    
    protected WireFeed parseChannel(final Element rssRoot) {
        final Channel channel = (Channel)super.parseChannel(rssRoot);
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        final String uri = eChannel.getAttributeValue("about", this.getRDFNamespace());
        if (uri != null) {
            channel.setUri(uri);
        }
        return channel;
    }
    
    protected Description parseItemDescription(final Element rssRoot, final Element eDesc) {
        final Description desc = new Description();
        desc.setType("text/plain");
        desc.setValue(eDesc.getText());
        return desc;
    }
}
