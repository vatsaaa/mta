// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.io.FeedException;
import com.sun.syndication.feed.rss.Source;
import com.sun.syndication.feed.rss.Category;
import com.sun.syndication.feed.rss.Enclosure;
import com.sun.syndication.feed.rss.Item;
import java.util.List;
import org.jdom.Attribute;
import com.sun.syndication.feed.rss.Cloud;
import org.jdom.Content;
import org.jdom.Element;
import com.sun.syndication.feed.rss.Channel;

public class RSS092Generator extends RSS091UserlandGenerator
{
    public RSS092Generator() {
        this("rss_0.92", "0.92");
    }
    
    protected RSS092Generator(final String type, final String version) {
        super(type, version);
    }
    
    protected void populateChannel(final Channel channel, final Element eChannel) {
        super.populateChannel(channel, eChannel);
        final Cloud cloud = channel.getCloud();
        if (cloud != null) {
            eChannel.addContent(this.generateCloud(cloud));
        }
    }
    
    protected Element generateCloud(final Cloud cloud) {
        final Element eCloud = new Element("cloud", this.getFeedNamespace());
        if (cloud.getDomain() != null) {
            eCloud.setAttribute(new Attribute("domain", cloud.getDomain()));
        }
        if (cloud.getPort() != 0) {
            eCloud.setAttribute(new Attribute("port", String.valueOf(cloud.getPort())));
        }
        if (cloud.getPath() != null) {
            eCloud.setAttribute(new Attribute("path", cloud.getPath()));
        }
        if (cloud.getRegisterProcedure() != null) {
            eCloud.setAttribute(new Attribute("registerProcedure", cloud.getRegisterProcedure()));
        }
        if (cloud.getProtocol() != null) {
            eCloud.setAttribute(new Attribute("protocol", cloud.getProtocol()));
        }
        return eCloud;
    }
    
    protected int getNumberOfEnclosures(final List enclosures) {
        return (enclosures.size() > 0) ? 1 : 0;
    }
    
    protected void populateItem(final Item item, final Element eItem, final int index) {
        super.populateItem(item, eItem, index);
        final Source source = item.getSource();
        if (source != null) {
            eItem.addContent(this.generateSourceElement(source));
        }
        final List enclosures = item.getEnclosures();
        for (int i = 0; i < this.getNumberOfEnclosures(enclosures); ++i) {
            eItem.addContent(this.generateEnclosure(enclosures.get(i)));
        }
        final List categories = item.getCategories();
        for (int j = 0; j < categories.size(); ++j) {
            eItem.addContent(this.generateCategoryElement(categories.get(j)));
        }
    }
    
    protected Element generateSourceElement(final Source source) {
        final Element sourceElement = new Element("source", this.getFeedNamespace());
        if (source.getUrl() != null) {
            sourceElement.setAttribute(new Attribute("url", source.getUrl()));
        }
        sourceElement.addContent(source.getValue());
        return sourceElement;
    }
    
    protected Element generateEnclosure(final Enclosure enclosure) {
        final Element enclosureElement = new Element("enclosure", this.getFeedNamespace());
        if (enclosure.getUrl() != null) {
            enclosureElement.setAttribute("url", enclosure.getUrl());
        }
        if (enclosure.getLength() != 0L) {
            enclosureElement.setAttribute("length", String.valueOf(enclosure.getLength()));
        }
        if (enclosure.getType() != null) {
            enclosureElement.setAttribute("type", enclosure.getType());
        }
        return enclosureElement;
    }
    
    protected Element generateCategoryElement(final Category category) {
        final Element categoryElement = new Element("category", this.getFeedNamespace());
        if (category.getDomain() != null) {
            categoryElement.setAttribute("domain", category.getDomain());
        }
        categoryElement.addContent(category.getValue());
        return categoryElement;
    }
    
    protected void checkChannelConstraints(final Element eChannel) throws FeedException {
        this.checkNotNullAndLength(eChannel, "title", 0, -1);
        this.checkNotNullAndLength(eChannel, "description", 0, -1);
        this.checkNotNullAndLength(eChannel, "link", 0, -1);
    }
    
    protected void checkImageConstraints(final Element eImage) throws FeedException {
        this.checkNotNullAndLength(eImage, "title", 0, -1);
        this.checkNotNullAndLength(eImage, "url", 0, -1);
    }
    
    protected void checkTextInputConstraints(final Element eTextInput) throws FeedException {
        this.checkNotNullAndLength(eTextInput, "title", 0, -1);
        this.checkNotNullAndLength(eTextInput, "description", 0, -1);
        this.checkNotNullAndLength(eTextInput, "name", 0, -1);
        this.checkNotNullAndLength(eTextInput, "link", 0, -1);
    }
    
    protected void checkItemsConstraints(final Element parent) throws FeedException {
    }
    
    protected void checkItemConstraints(final Element eItem) throws FeedException {
    }
}
