// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

public class Base64
{
    private static final byte[] ALPHASET;
    private static final int I6O2 = 252;
    private static final int O6I2 = 3;
    private static final int I4O4 = 240;
    private static final int O4I4 = 15;
    private static final int I2O6 = 192;
    private static final int O2I6 = 63;
    private static final int[] CODES;
    
    public static String encode(String s) {
        byte[] sBytes = s.getBytes();
        sBytes = encode(sBytes);
        s = new String(sBytes);
        return s;
    }
    
    public static String decode(String s) throws IllegalArgumentException {
        byte[] sBytes = s.getBytes();
        sBytes = decode(sBytes);
        s = new String(sBytes);
        return s;
    }
    
    public static byte[] encode(final byte[] dData) {
        if (dData == null) {
            throw new IllegalArgumentException("Cannot encode null");
        }
        final byte[] eData = new byte[(dData.length + 2) / 3 * 4];
        int eIndex = 0;
        for (int i = 0; i < dData.length; i += 3) {
            int d2 = 0;
            int d3 = 0;
            int pad = 0;
            final int d4 = dData[i];
            if (i + 1 < dData.length) {
                d2 = dData[i + 1];
                if (i + 2 < dData.length) {
                    d3 = dData[i + 2];
                }
                else {
                    pad = 1;
                }
            }
            else {
                pad = 2;
            }
            final int e1 = Base64.ALPHASET[(d4 & 0xFC) >> 2];
            final int e2 = Base64.ALPHASET[(d4 & 0x3) << 4 | (d2 & 0xF0) >> 4];
            final int e3 = Base64.ALPHASET[(d2 & 0xF) << 2 | (d3 & 0xC0) >> 6];
            final int e4 = Base64.ALPHASET[d3 & 0x3F];
            eData[eIndex++] = (byte)e1;
            eData[eIndex++] = (byte)e2;
            eData[eIndex++] = (byte)((pad < 2) ? ((byte)e3) : 61);
            eData[eIndex++] = (byte)((pad < 1) ? ((byte)e4) : 61);
        }
        return eData;
    }
    
    public static byte[] decode(final byte[] eData) {
        if (eData == null) {
            throw new IllegalArgumentException("Cannot decode null");
        }
        final byte[] cleanEData = eData.clone();
        int cleanELength = 0;
        for (int i = 0; i < eData.length; ++i) {
            if (eData[i] < 256 && Base64.CODES[eData[i]] < 64) {
                cleanEData[cleanELength++] = eData[i];
            }
        }
        int dLength = cleanELength / 4 * 3;
        switch (cleanELength % 4) {
            case 3: {
                dLength += 2;
                break;
            }
            case 2: {
                ++dLength;
                break;
            }
        }
        final byte[] dData = new byte[dLength];
        int dIndex = 0;
        for (int j = 0; j < cleanELength; j += 4) {
            if (j + 3 > cleanELength) {
                throw new IllegalArgumentException("byte array is not a valid com.sun.syndication.io.impl.Base64 encoding");
            }
            final int e1 = Base64.CODES[cleanEData[j]];
            final int e2 = Base64.CODES[cleanEData[j + 1]];
            final int e3 = Base64.CODES[cleanEData[j + 2]];
            final int e4 = Base64.CODES[cleanEData[j + 3]];
            dData[dIndex++] = (byte)(e1 << 2 | e2 >> 4);
            if (dIndex < dData.length) {
                dData[dIndex++] = (byte)(e2 << 4 | e3 >> 2);
            }
            if (dIndex < dData.length) {
                dData[dIndex++] = (byte)(e3 << 6 | e4);
            }
        }
        return dData;
    }
    
    public static void main(final String[] args) throws Exception {
        final String s = "\nPGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCI+V2UncmUgcHJvcG9zaW5nIDxhIGhy\nZWY9Imh0dHA6Ly93d3cuZ29vZ2xlLmNvbS9jb3Jwb3JhdGUvc29mdHdhcmVfcHJpbmNpcGxlcy5odG1sIj5z\nb21lIGd1aWRlbGluZXMgPC9hPnRvIGhlbHAgY3VyYiB0aGUgcHJvYmxlbSBvZiBJbnRlcm5ldCBzb2Z0d2Fy\nZSB0aGF0IGluc3RhbGxzIGl0c2VsZiB3aXRob3V0IHRlbGxpbmcgeW91LCBvciBiZWhhdmVzIGJhZGx5IG9u\nY2UgaXQgZ2V0cyBvbiB5b3VyIGNvbXB1dGVyLiBXZSd2ZSBiZWVuIGhlYXJpbmcgYSBsb3Qgb2YgY29tcGxh\naW50cyBhYm91dCB0aGlzIGxhdGVseSBhbmQgaXQgc2VlbXMgdG8gYmUgZ2V0dGluZyB3b3JzZS4gV2UgdGhp\nbmsgaXQncyBpbXBvcnRhbnQgdGhhdCB5b3UgcmV0YWluIGNvbnRyb2wgb2YgeW91ciBjb21wdXRlciBhbmQg\ndGhhdCB0aGVyZSBiZSBzb21lIGNsZWFyIHN0YW5kYXJkcyBpbiBvdXIgaW5kdXN0cnkuIExldCB1cyBrbm93\nIGlmIHlvdSB0aGluayB0aGVzZSBndWlkZWxpbmVzIGFyZSB1c2VmdWwgb3IgaWYgeW91IGhhdmUgc3VnZ2Vz\ndGlvbnMgdG8gaW1wcm92ZSB0aGVtLgo8YnIgLz4KPGJyIC8+Sm9uYXRoYW4gUm9zZW5iZXJnCjxiciAvPgo8\nL2Rpdj4K\n";
        System.out.println(decode(s));
    }
    
    static {
        ALPHASET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".getBytes();
        CODES = new int[256];
        for (int i = 0; i < Base64.CODES.length; ++i) {
            Base64.CODES[i] = 64;
        }
        for (int i = 0; i < Base64.ALPHASET.length; ++i) {
            Base64.CODES[Base64.ALPHASET[i]] = i;
        }
    }
}
