// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.Iterator;
import com.sun.syndication.io.DelegatingModuleGenerator;
import com.sun.syndication.io.DelegatingModuleParser;
import java.util.HashMap;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import com.sun.syndication.io.WireFeedGenerator;
import com.sun.syndication.io.WireFeedParser;
import java.util.List;
import java.util.Map;

public abstract class PluginManager
{
    private String[] _propertyValues;
    private Map _pluginsMap;
    private List _pluginsList;
    private List _keys;
    private WireFeedParser _parentParser;
    private WireFeedGenerator _parentGenerator;
    
    protected PluginManager(final String propertyKey) {
        this(propertyKey, null, null);
    }
    
    protected PluginManager(final String propertyKey, final WireFeedParser parentParser, final WireFeedGenerator parentGenerator) {
        this._parentParser = parentParser;
        this._parentGenerator = parentGenerator;
        this._propertyValues = PropertiesLoader.getPropertiesLoader().getTokenizedProperty(propertyKey, ", ");
        this.loadPlugins();
        this._pluginsMap = Collections.unmodifiableMap((Map<?, ?>)this._pluginsMap);
        this._pluginsList = Collections.unmodifiableList((List<?>)this._pluginsList);
        this._keys = Collections.unmodifiableList((List<?>)new ArrayList<Object>(this._pluginsMap.keySet()));
    }
    
    protected abstract String getKey(final Object p0);
    
    protected List getKeys() {
        return this._keys;
    }
    
    protected List getPlugins() {
        return this._pluginsList;
    }
    
    protected Map getPluginMap() {
        return this._pluginsMap;
    }
    
    protected Object getPlugin(final String key) {
        return this._pluginsMap.get(key);
    }
    
    private void loadPlugins() {
        final List finalPluginsList = new ArrayList();
        this._pluginsList = new ArrayList();
        this._pluginsMap = new HashMap();
        String className = null;
        try {
            final Class[] classes = this.getClasses();
            for (int i = 0; i < classes.length; ++i) {
                className = classes[i].getName();
                final Object plugin = classes[i].newInstance();
                if (plugin instanceof DelegatingModuleParser) {
                    ((DelegatingModuleParser)plugin).setFeedParser(this._parentParser);
                }
                if (plugin instanceof DelegatingModuleGenerator) {
                    ((DelegatingModuleGenerator)plugin).setFeedGenerator(this._parentGenerator);
                }
                this._pluginsMap.put(this.getKey(plugin), plugin);
                this._pluginsList.add(plugin);
            }
            Iterator j = this._pluginsMap.values().iterator();
            while (j.hasNext()) {
                finalPluginsList.add(j.next());
            }
            j = this._pluginsList.iterator();
            while (j.hasNext()) {
                final Object plugin = j.next();
                if (!finalPluginsList.contains(plugin)) {
                    j.remove();
                }
            }
        }
        catch (Exception ex) {
            throw new RuntimeException("could not instantiate plugin " + className, ex);
        }
        catch (ExceptionInInitializerError er) {
            throw new RuntimeException("could not instantiate plugin " + className, er);
        }
    }
    
    private Class[] getClasses() throws ClassNotFoundException {
        final ClassLoader classLoader = PluginManager.class.getClassLoader();
        final List classes = new ArrayList();
        for (int i = 0; i < this._propertyValues.length; ++i) {
            final Class mClass = Class.forName(this._propertyValues[i], true, classLoader);
            classes.add(mClass);
        }
        final Class[] array = new Class[classes.size()];
        classes.toArray(array);
        return array;
    }
}
