// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.atom.Entry;
import java.util.Iterator;
import org.jdom.output.XMLOutputter;
import com.sun.syndication.feed.atom.Content;
import com.sun.syndication.feed.atom.Person;
import com.sun.syndication.feed.atom.Link;
import java.util.List;
import com.sun.syndication.feed.module.Extendable;
import com.sun.syndication.feed.atom.Generator;
import java.util.ArrayList;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.Namespace;

public class Atom03Parser extends BaseWireFeedParser
{
    private static final String ATOM_03_URI = "http://purl.org/atom/ns#";
    
    public Atom03Parser() {
        this("atom_0.3");
    }
    
    protected Atom03Parser(final String type) {
        super(type);
    }
    
    protected Namespace getAtomNamespace() {
        return Namespace.getNamespace("http://purl.org/atom/ns#");
    }
    
    public boolean isMyType(final Document document) {
        final Element rssRoot = document.getRootElement();
        final Namespace defaultNS = rssRoot.getNamespace();
        return defaultNS != null && defaultNS.equals(this.getAtomNamespace());
    }
    
    public WireFeed parse(final Document document, final boolean validate) throws IllegalArgumentException, FeedException {
        if (validate) {
            this.validateFeed(document);
        }
        final Element rssRoot = document.getRootElement();
        return this.parseFeed(rssRoot);
    }
    
    protected void validateFeed(final Document document) throws FeedException {
    }
    
    protected WireFeed parseFeed(final Element eFeed) {
        final Feed feed = new Feed(this.getType());
        Element e = eFeed.getChild("title", this.getAtomNamespace());
        if (e != null) {
            feed.setTitle(e.getText());
        }
        List eList = eFeed.getChildren("link", this.getAtomNamespace());
        feed.setAlternateLinks(this.parseAlternateLinks(eList));
        feed.setOtherLinks(this.parseOtherLinks(eList));
        e = eFeed.getChild("author", this.getAtomNamespace());
        if (e != null) {
            final List authors = new ArrayList();
            authors.add(this.parsePerson(e));
            feed.setAuthors(authors);
        }
        eList = eFeed.getChildren("contributor", this.getAtomNamespace());
        if (eList.size() > 0) {
            feed.setContributors(this.parsePersons(eList));
        }
        e = eFeed.getChild("tagline", this.getAtomNamespace());
        if (e != null) {
            feed.setTagline(this.parseContent(e));
        }
        e = eFeed.getChild("id", this.getAtomNamespace());
        if (e != null) {
            feed.setId(e.getText());
        }
        e = eFeed.getChild("generator", this.getAtomNamespace());
        if (e != null) {
            final Generator gen = new Generator();
            gen.setValue(e.getText());
            String att = e.getAttributeValue("url");
            if (att != null) {
                gen.setUrl(att);
            }
            att = e.getAttributeValue("version");
            if (att != null) {
                gen.setVersion(att);
            }
            feed.setGenerator(gen);
        }
        e = eFeed.getChild("copyright", this.getAtomNamespace());
        if (e != null) {
            feed.setCopyright(e.getText());
        }
        e = eFeed.getChild("info", this.getAtomNamespace());
        if (e != null) {
            feed.setInfo(this.parseContent(e));
        }
        e = eFeed.getChild("modified", this.getAtomNamespace());
        if (e != null) {
            feed.setModified(DateParser.parseDate(e.getText()));
        }
        feed.setModules(this.parseFeedModules(eFeed));
        eList = eFeed.getChildren("entry", this.getAtomNamespace());
        if (eList.size() > 0) {
            feed.setEntries(this.parseEntries(eList));
        }
        final List foreignMarkup = this.extractForeignMarkup(eFeed, feed, this.getAtomNamespace());
        if (foreignMarkup.size() > 0) {
            feed.setForeignMarkup(foreignMarkup);
        }
        return feed;
    }
    
    private Link parseLink(final Element eLink) {
        final Link link = new Link();
        String att = eLink.getAttributeValue("rel");
        if (att != null) {
            link.setRel(att);
        }
        att = eLink.getAttributeValue("type");
        if (att != null) {
            link.setType(att);
        }
        att = eLink.getAttributeValue("href");
        if (att != null) {
            link.setHref(att);
        }
        return link;
    }
    
    private List parseLinks(final List eLinks, final boolean alternate) {
        final List links = new ArrayList();
        for (int i = 0; i < eLinks.size(); ++i) {
            final Element eLink = eLinks.get(i);
            final String rel = eLink.getAttributeValue("rel");
            if (alternate) {
                if ("alternate".equals(rel)) {
                    links.add(this.parseLink(eLink));
                }
            }
            else if (!"alternate".equals(rel)) {
                links.add(this.parseLink(eLink));
            }
        }
        return (links.size() > 0) ? links : null;
    }
    
    private List parseAlternateLinks(final List eLinks) {
        return this.parseLinks(eLinks, true);
    }
    
    private List parseOtherLinks(final List eLinks) {
        return this.parseLinks(eLinks, false);
    }
    
    private Person parsePerson(final Element ePerson) {
        final Person person = new Person();
        Element e = ePerson.getChild("name", this.getAtomNamespace());
        if (e != null) {
            person.setName(e.getText());
        }
        e = ePerson.getChild("url", this.getAtomNamespace());
        if (e != null) {
            person.setUrl(e.getText());
        }
        e = ePerson.getChild("email", this.getAtomNamespace());
        if (e != null) {
            person.setEmail(e.getText());
        }
        return person;
    }
    
    private List parsePersons(final List ePersons) {
        final List persons = new ArrayList();
        for (int i = 0; i < ePersons.size(); ++i) {
            persons.add(this.parsePerson(ePersons.get(i)));
        }
        return (persons.size() > 0) ? persons : null;
    }
    
    private Content parseContent(final Element e) {
        String value = null;
        String type = e.getAttributeValue("type");
        type = ((type != null) ? type : "text/plain");
        String mode = e.getAttributeValue("mode");
        if (mode == null) {
            mode = "xml";
        }
        if (mode.equals("escaped")) {
            value = e.getText();
        }
        else if (mode.equals("base64")) {
            value = Base64.decode(e.getText());
        }
        else if (mode.equals("xml")) {
            final XMLOutputter outputter = new XMLOutputter();
            final List eContent = e.getContent();
            for (final org.jdom.Content c : eContent) {
                if (c instanceof Element) {
                    final Element eC = (Element)c;
                    if (!eC.getNamespace().equals(this.getAtomNamespace())) {
                        continue;
                    }
                    ((Element)c).setNamespace(Namespace.NO_NAMESPACE);
                }
            }
            value = outputter.outputString(eContent);
        }
        final Content content = new Content();
        content.setType(type);
        content.setMode(mode);
        content.setValue(value);
        return content;
    }
    
    private List parseEntries(final List eEntries) {
        final List entries = new ArrayList();
        for (int i = 0; i < eEntries.size(); ++i) {
            entries.add(this.parseEntry(eEntries.get(i)));
        }
        return (entries.size() > 0) ? entries : null;
    }
    
    private Entry parseEntry(final Element eEntry) {
        final Entry entry = new Entry();
        Element e = eEntry.getChild("title", this.getAtomNamespace());
        if (e != null) {
            entry.setTitle(e.getText());
        }
        List eList = eEntry.getChildren("link", this.getAtomNamespace());
        entry.setAlternateLinks(this.parseAlternateLinks(eList));
        entry.setOtherLinks(this.parseOtherLinks(eList));
        e = eEntry.getChild("author", this.getAtomNamespace());
        if (e != null) {
            final List authors = new ArrayList();
            authors.add(this.parsePerson(e));
            entry.setAuthors(authors);
        }
        eList = eEntry.getChildren("contributor", this.getAtomNamespace());
        if (eList.size() > 0) {
            entry.setContributors(this.parsePersons(eList));
        }
        e = eEntry.getChild("id", this.getAtomNamespace());
        if (e != null) {
            entry.setId(e.getText());
        }
        e = eEntry.getChild("modified", this.getAtomNamespace());
        if (e != null) {
            entry.setModified(DateParser.parseDate(e.getText()));
        }
        e = eEntry.getChild("issued", this.getAtomNamespace());
        if (e != null) {
            entry.setIssued(DateParser.parseDate(e.getText()));
        }
        e = eEntry.getChild("created", this.getAtomNamespace());
        if (e != null) {
            entry.setCreated(DateParser.parseDate(e.getText()));
        }
        e = eEntry.getChild("summary", this.getAtomNamespace());
        if (e != null) {
            entry.setSummary(this.parseContent(e));
        }
        eList = eEntry.getChildren("content", this.getAtomNamespace());
        if (eList.size() > 0) {
            final List content = new ArrayList();
            for (int i = 0; i < eList.size(); ++i) {
                content.add(this.parseContent(eList.get(i)));
            }
            entry.setContents(content);
        }
        entry.setModules(this.parseItemModules(eEntry));
        final List foreignMarkup = this.extractForeignMarkup(eEntry, entry, this.getAtomNamespace());
        if (foreignMarkup.size() > 0) {
            entry.setForeignMarkup(foreignMarkup);
        }
        return entry;
    }
}
