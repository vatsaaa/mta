// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import com.sun.syndication.feed.rss.Description;
import com.sun.syndication.feed.rss.Content;
import com.sun.syndication.feed.rss.Item;
import java.util.Collections;
import com.sun.syndication.feed.rss.Image;
import java.util.List;
import java.util.ArrayList;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.WireFeed;
import org.jdom.Namespace;
import org.jdom.Attribute;
import org.jdom.Element;
import org.jdom.Document;

public class RSS091UserlandParser extends RSS090Parser
{
    public RSS091UserlandParser() {
        this("rss_0.91U");
    }
    
    protected RSS091UserlandParser(final String type) {
        super(type);
    }
    
    public boolean isMyType(final Document document) {
        final Element rssRoot = document.getRootElement();
        boolean ok = rssRoot.getName().equals("rss");
        if (ok) {
            ok = false;
            final Attribute version = rssRoot.getAttribute("version");
            if (version != null) {
                ok = version.getValue().equals(this.getRSSVersion());
            }
        }
        return ok;
    }
    
    protected String getRSSVersion() {
        return "0.91";
    }
    
    protected Namespace getRSSNamespace() {
        return Namespace.getNamespace("");
    }
    
    protected boolean isHourFormat24(final Element rssRoot) {
        return true;
    }
    
    protected WireFeed parseChannel(final Element rssRoot) {
        final Channel channel = (Channel)super.parseChannel(rssRoot);
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        Element e = eChannel.getChild("language", this.getRSSNamespace());
        if (e != null) {
            channel.setLanguage(e.getText());
        }
        e = eChannel.getChild("rating", this.getRSSNamespace());
        if (e != null) {
            channel.setRating(e.getText());
        }
        e = eChannel.getChild("copyright", this.getRSSNamespace());
        if (e != null) {
            channel.setCopyright(e.getText());
        }
        e = eChannel.getChild("pubDate", this.getRSSNamespace());
        if (e != null) {
            channel.setPubDate(DateParser.parseDate(e.getText()));
        }
        e = eChannel.getChild("lastBuildDate", this.getRSSNamespace());
        if (e != null) {
            channel.setLastBuildDate(DateParser.parseDate(e.getText()));
        }
        e = eChannel.getChild("docs", this.getRSSNamespace());
        if (e != null) {
            channel.setDocs(e.getText());
        }
        e = eChannel.getChild("docs", this.getRSSNamespace());
        if (e != null) {
            channel.setDocs(e.getText());
        }
        e = eChannel.getChild("managingEditor", this.getRSSNamespace());
        if (e != null) {
            channel.setManagingEditor(e.getText());
        }
        e = eChannel.getChild("webMaster", this.getRSSNamespace());
        if (e != null) {
            channel.setWebMaster(e.getText());
        }
        e = eChannel.getChild("skipHours");
        if (e != null) {
            final List skipHours = new ArrayList();
            final List eHours = e.getChildren("hour", this.getRSSNamespace());
            for (int i = 0; i < eHours.size(); ++i) {
                final Element eHour = eHours.get(i);
                skipHours.add(new Integer(eHour.getText().trim()));
            }
            channel.setSkipHours(skipHours);
        }
        e = eChannel.getChild("skipDays");
        if (e != null) {
            final List skipDays = new ArrayList();
            final List eDays = e.getChildren("day", this.getRSSNamespace());
            for (int i = 0; i < eDays.size(); ++i) {
                final Element eDay = eDays.get(i);
                skipDays.add(eDay.getText().trim());
            }
            channel.setSkipDays(skipDays);
        }
        return channel;
    }
    
    protected Image parseImage(final Element rssRoot) {
        final Image image = super.parseImage(rssRoot);
        if (image != null) {
            final Element eImage = this.getImage(rssRoot);
            Element e = eImage.getChild("width", this.getRSSNamespace());
            if (e != null) {
                image.setWidth(Integer.parseInt(e.getText().trim()));
            }
            e = eImage.getChild("height", this.getRSSNamespace());
            if (e != null) {
                image.setHeight(Integer.parseInt(e.getText().trim()));
            }
            e = eImage.getChild("description", this.getRSSNamespace());
            if (e != null) {
                image.setDescription(e.getText());
            }
        }
        return image;
    }
    
    protected List getItems(final Element rssRoot) {
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        return (eChannel != null) ? eChannel.getChildren("item", this.getRSSNamespace()) : Collections.EMPTY_LIST;
    }
    
    protected Element getImage(final Element rssRoot) {
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        return (eChannel != null) ? eChannel.getChild("image", this.getRSSNamespace()) : null;
    }
    
    protected String getTextInputLabel() {
        return "textInput";
    }
    
    protected Element getTextInput(final Element rssRoot) {
        final String elementName = this.getTextInputLabel();
        final Element eChannel = rssRoot.getChild("channel", this.getRSSNamespace());
        return (eChannel != null) ? eChannel.getChild(elementName, this.getRSSNamespace()) : null;
    }
    
    protected Item parseItem(final Element rssRoot, final Element eItem) {
        final Item item = super.parseItem(rssRoot, eItem);
        final Element e = eItem.getChild("description", this.getRSSNamespace());
        if (e != null) {
            item.setDescription(this.parseItemDescription(rssRoot, e));
        }
        final Element ce = eItem.getChild("encoded", this.getContentNamespace());
        if (ce != null) {
            final Content content = new Content();
            content.setType("html");
            content.setValue(ce.getText());
            item.setContent(content);
        }
        return item;
    }
    
    protected Description parseItemDescription(final Element rssRoot, final Element eDesc) {
        final Description desc = new Description();
        desc.setType("text/plain");
        desc.setValue(eDesc.getText());
        return desc;
    }
}
