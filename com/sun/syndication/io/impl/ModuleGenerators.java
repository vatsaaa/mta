// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.io.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import com.sun.syndication.feed.module.Module;
import org.jdom.Element;
import java.util.List;
import com.sun.syndication.io.ModuleGenerator;
import com.sun.syndication.io.WireFeedGenerator;
import com.sun.syndication.io.WireFeedParser;
import java.util.Set;

public class ModuleGenerators extends PluginManager
{
    private Set _allNamespaces;
    
    public ModuleGenerators(final String propertyKey, final BaseWireFeedGenerator parentGenerator) {
        super(propertyKey, null, parentGenerator);
    }
    
    public ModuleGenerator getGenerator(final String uri) {
        return (ModuleGenerator)this.getPlugin(uri);
    }
    
    protected String getKey(final Object obj) {
        return ((ModuleGenerator)obj).getNamespaceUri();
    }
    
    public List getModuleNamespaces() {
        return this.getKeys();
    }
    
    public void generateModules(final List modules, final Element element) {
        final Map generators = this.getPluginMap();
        for (int i = 0; i < modules.size(); ++i) {
            final Module module = modules.get(i);
            final String namespaceUri = module.getUri();
            final ModuleGenerator generator = generators.get(namespaceUri);
            if (generator != null) {
                generator.generate(module, element);
            }
        }
    }
    
    public Set getAllNamespaces() {
        if (this._allNamespaces == null) {
            this._allNamespaces = new HashSet();
            final List mUris = this.getModuleNamespaces();
            for (int i = 0; i < mUris.size(); ++i) {
                final ModuleGenerator mGen = this.getGenerator(mUris.get(i));
                this._allNamespaces.addAll(mGen.getNamespaces());
            }
        }
        return this._allNamespaces;
    }
}
