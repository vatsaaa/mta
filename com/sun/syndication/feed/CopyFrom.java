// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed;

public interface CopyFrom
{
    Class getInterface();
    
    void copyFrom(final Object p0);
}
