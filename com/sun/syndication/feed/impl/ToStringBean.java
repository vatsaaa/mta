// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.impl;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.lang.reflect.Method;
import java.beans.PropertyDescriptor;
import java.util.Stack;
import java.io.Serializable;

public class ToStringBean implements Serializable
{
    private static final ThreadLocal PREFIX_TL;
    private static final Object[] NO_PARAMS;
    private Class _beanClass;
    private Object _obj;
    
    protected ToStringBean(final Class beanClass) {
        this._beanClass = beanClass;
        this._obj = this;
    }
    
    public ToStringBean(final Class beanClass, final Object obj) {
        this._beanClass = beanClass;
        this._obj = obj;
    }
    
    public String toString() {
        final Stack stack = ToStringBean.PREFIX_TL.get();
        final String[] tsInfo = (String[])(stack.isEmpty() ? null : stack.peek());
        String prefix;
        if (tsInfo == null) {
            final String className = this._obj.getClass().getName();
            prefix = className.substring(className.lastIndexOf(".") + 1);
        }
        else {
            prefix = tsInfo[0];
            tsInfo[1] = prefix;
        }
        return this.toString(prefix);
    }
    
    private String toString(final String prefix) {
        final StringBuffer sb = new StringBuffer(128);
        try {
            final PropertyDescriptor[] pds = BeanIntrospector.getPropertyDescriptors(this._beanClass);
            if (pds != null) {
                for (int i = 0; i < pds.length; ++i) {
                    final String pName = pds[i].getName();
                    final Method pReadMethod = pds[i].getReadMethod();
                    if (pReadMethod != null && pReadMethod.getDeclaringClass() != Object.class && pReadMethod.getParameterTypes().length == 0) {
                        final Object value = pReadMethod.invoke(this._obj, ToStringBean.NO_PARAMS);
                        this.printProperty(sb, prefix + "." + pName, value);
                    }
                }
            }
        }
        catch (Exception ex) {
            sb.append("\n\nEXCEPTION: Could not complete " + this._obj.getClass() + ".toString(): " + ex.getMessage() + "\n");
        }
        return sb.toString();
    }
    
    private void printProperty(final StringBuffer sb, final String prefix, final Object value) {
        if (value == null) {
            sb.append(prefix).append("=null\n");
        }
        else if (value.getClass().isArray()) {
            this.printArrayProperty(sb, prefix, value);
        }
        else if (value instanceof Map) {
            final Map map = (Map)value;
            final Iterator i = map.entrySet().iterator();
            if (i.hasNext()) {
                while (i.hasNext()) {
                    final Map.Entry me = i.next();
                    final String ePrefix = prefix + "[" + me.getKey() + "]";
                    final Object eValue = me.getValue();
                    final String[] tsInfo = { ePrefix, null };
                    final Stack stack = ToStringBean.PREFIX_TL.get();
                    stack.push(tsInfo);
                    final String s = (eValue != null) ? eValue.toString() : "null";
                    stack.pop();
                    if (tsInfo[1] == null) {
                        sb.append(ePrefix).append("=").append(s).append("\n");
                    }
                    else {
                        sb.append(s);
                    }
                }
            }
            else {
                sb.append(prefix).append("=[]\n");
            }
        }
        else if (value instanceof Collection) {
            final Collection collection = (Collection)value;
            final Iterator i = collection.iterator();
            if (i.hasNext()) {
                int c = 0;
                while (i.hasNext()) {
                    final String cPrefix = prefix + "[" + c++ + "]";
                    final Object cValue = i.next();
                    final String[] tsInfo = { cPrefix, null };
                    final Stack stack = ToStringBean.PREFIX_TL.get();
                    stack.push(tsInfo);
                    final String s = (cValue != null) ? cValue.toString() : "null";
                    stack.pop();
                    if (tsInfo[1] == null) {
                        sb.append(cPrefix).append("=").append(s).append("\n");
                    }
                    else {
                        sb.append(s);
                    }
                }
            }
            else {
                sb.append(prefix).append("=[]\n");
            }
        }
        else {
            final String[] tsInfo2 = { prefix, null };
            final Stack stack2 = ToStringBean.PREFIX_TL.get();
            stack2.push(tsInfo2);
            final String s2 = value.toString();
            stack2.pop();
            if (tsInfo2[1] == null) {
                sb.append(prefix).append("=").append(s2).append("\n");
            }
            else {
                sb.append(s2);
            }
        }
    }
    
    private void printArrayProperty(final StringBuffer sb, final String prefix, final Object array) {
        for (int length = Array.getLength(array), i = 0; i < length; ++i) {
            final Object obj = Array.get(array, i);
            this.printProperty(sb, prefix + "[" + i + "]", obj);
        }
    }
    
    static {
        PREFIX_TL = new ThreadLocal() {
            public Object get() {
                Object o = super.get();
                if (o == null) {
                    o = new Stack();
                    this.set(o);
                }
                return o;
            }
        };
        NO_PARAMS = new Object[0];
    }
}
