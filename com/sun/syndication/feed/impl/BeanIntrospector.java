// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.impl;

import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import java.util.HashSet;
import java.util.ArrayList;
import java.beans.Introspector;
import java.util.HashMap;
import java.util.List;
import java.lang.reflect.Method;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.util.Map;

public class BeanIntrospector
{
    private static final Map _introspected;
    private static final String SETTER = "set";
    private static final String GETTER = "get";
    private static final String BOOLEAN_GETTER = "is";
    
    public static synchronized PropertyDescriptor[] getPropertyDescriptors(final Class klass) throws IntrospectionException {
        PropertyDescriptor[] descriptors = BeanIntrospector._introspected.get(klass);
        if (descriptors == null) {
            descriptors = getPDs(klass);
            BeanIntrospector._introspected.put(klass, descriptors);
        }
        return descriptors;
    }
    
    private static PropertyDescriptor[] getPDs(final Class klass) throws IntrospectionException {
        final Method[] methods = klass.getMethods();
        final Map getters = getPDs(methods, false);
        final Map setters = getPDs(methods, true);
        final List pds = merge(getters, setters);
        final PropertyDescriptor[] array = new PropertyDescriptor[pds.size()];
        pds.toArray(array);
        return array;
    }
    
    private static Map getPDs(final Method[] methods, final boolean setters) throws IntrospectionException {
        final Map pds = new HashMap();
        for (int i = 0; i < methods.length; ++i) {
            String pName = null;
            PropertyDescriptor pDescriptor = null;
            if ((methods[i].getModifiers() & 0x1) != 0x0) {
                if (setters) {
                    if (methods[i].getName().startsWith("set") && methods[i].getReturnType() == Void.TYPE && methods[i].getParameterTypes().length == 1) {
                        pName = Introspector.decapitalize(methods[i].getName().substring(3));
                        pDescriptor = new PropertyDescriptor(pName, null, methods[i]);
                    }
                }
                else if (methods[i].getName().startsWith("get") && methods[i].getReturnType() != Void.TYPE && methods[i].getParameterTypes().length == 0) {
                    pName = Introspector.decapitalize(methods[i].getName().substring(3));
                    pDescriptor = new PropertyDescriptor(pName, methods[i], null);
                }
                else if (methods[i].getName().startsWith("is") && methods[i].getReturnType() == Boolean.TYPE && methods[i].getParameterTypes().length == 0) {
                    pName = Introspector.decapitalize(methods[i].getName().substring(2));
                    pDescriptor = new PropertyDescriptor(pName, methods[i], null);
                }
            }
            if (pName != null) {
                pds.put(pName, pDescriptor);
            }
        }
        return pds;
    }
    
    private static List merge(final Map getters, final Map setters) throws IntrospectionException {
        final List props = new ArrayList();
        final Set processedProps = new HashSet();
        for (final String name : getters.keySet()) {
            final PropertyDescriptor getter = getters.get(name);
            final PropertyDescriptor setter = setters.get(name);
            if (setter != null) {
                processedProps.add(name);
                final PropertyDescriptor prop = new PropertyDescriptor(name, getter.getReadMethod(), setter.getWriteMethod());
                props.add(prop);
            }
            else {
                props.add(getter);
            }
        }
        final Set writeOnlyProps = new HashSet(setters.keySet());
        writeOnlyProps.removeAll(processedProps);
        for (final String name2 : writeOnlyProps) {
            final PropertyDescriptor setter2 = setters.get(name2);
            props.add(setter2);
        }
        return props;
    }
    
    static {
        _introspected = new HashMap();
    }
}
