// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.lang.reflect.Array;
import java.util.Date;
import java.util.Collection;
import com.sun.syndication.feed.CopyFrom;
import java.lang.reflect.Method;
import java.beans.PropertyDescriptor;
import java.util.Set;
import java.util.Map;

public class CopyFromHelper
{
    private static final Object[] NO_PARAMS;
    private Class _beanInterfaceClass;
    private Map _baseInterfaceMap;
    private Map _baseImplMap;
    private static final Set BASIC_TYPES;
    
    public CopyFromHelper(final Class beanInterfaceClass, final Map basePropInterfaceMap, final Map basePropClassImplMap) {
        this._beanInterfaceClass = beanInterfaceClass;
        this._baseInterfaceMap = basePropInterfaceMap;
        this._baseImplMap = basePropClassImplMap;
    }
    
    public void copy(final Object target, final Object source) {
        try {
            final PropertyDescriptor[] pds = BeanIntrospector.getPropertyDescriptors(this._beanInterfaceClass);
            if (pds != null) {
                for (int i = 0; i < pds.length; ++i) {
                    final String propertyName = pds[i].getName();
                    final Method pReadMethod = pds[i].getReadMethod();
                    final Method pWriteMethod = pds[i].getWriteMethod();
                    if (pReadMethod != null && pWriteMethod != null && pReadMethod.getDeclaringClass() != Object.class && pReadMethod.getParameterTypes().length == 0 && this._baseInterfaceMap.containsKey(propertyName)) {
                        Object value = pReadMethod.invoke(source, CopyFromHelper.NO_PARAMS);
                        if (value != null) {
                            final Class baseInterface = this._baseInterfaceMap.get(propertyName);
                            value = this.doCopy(value, baseInterface);
                            pWriteMethod.invoke(target, value);
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            throw new RuntimeException("Could not do a copyFrom " + ex, ex);
        }
    }
    
    private CopyFrom createInstance(final Class interfaceClass) throws Exception {
        if (this._baseImplMap.get(interfaceClass) == null) {
            return null;
        }
        return this._baseImplMap.get(interfaceClass).newInstance();
    }
    
    private Object doCopy(Object value, final Class baseInterface) throws Exception {
        if (value != null) {
            final Class vClass = value.getClass();
            if (vClass.isArray()) {
                value = this.doCopyArray(value, baseInterface);
            }
            else if (value instanceof Collection) {
                value = this.doCopyCollection((Collection)value, baseInterface);
            }
            else if (value instanceof Map) {
                value = this.doCopyMap((Map)value, baseInterface);
            }
            else if (this.isBasicType(vClass)) {
                if (value instanceof Date) {
                    value = ((Date)value).clone();
                }
            }
            else {
                if (!(value instanceof CopyFrom)) {
                    throw new Exception("unsupported class for 'copyFrom' " + value.getClass());
                }
                final CopyFrom source = (CopyFrom)value;
                CopyFrom target = this.createInstance(source.getInterface());
                target = (CopyFrom)((target == null) ? value.getClass().newInstance() : target);
                target.copyFrom(source);
                value = target;
            }
        }
        return value;
    }
    
    private Object doCopyArray(final Object array, final Class baseInterface) throws Exception {
        final Class elementClass = array.getClass().getComponentType();
        final int length = Array.getLength(array);
        final Object newArray = Array.newInstance(elementClass, length);
        for (int i = 0; i < length; ++i) {
            final Object element = this.doCopy(Array.get(array, i), baseInterface);
            Array.set(newArray, i, element);
        }
        return newArray;
    }
    
    private Object doCopyCollection(final Collection collection, final Class baseInterface) throws Exception {
        final Collection newColl = (collection instanceof Set) ? new HashSet<Object>() : new ArrayList<Object>();
        final Iterator i = collection.iterator();
        while (i.hasNext()) {
            final Object element = this.doCopy(i.next(), baseInterface);
            newColl.add(element);
        }
        return newColl;
    }
    
    private Object doCopyMap(final Map map, final Class baseInterface) throws Exception {
        final Map newMap = new HashMap();
        for (final Map.Entry entry : map.entrySet()) {
            final Object key = entry.getKey();
            final Object element = this.doCopy(entry.getValue(), baseInterface);
            newMap.put(key, element);
        }
        return newMap;
    }
    
    private boolean isBasicType(final Class vClass) {
        return CopyFromHelper.BASIC_TYPES.contains(vClass);
    }
    
    static {
        NO_PARAMS = new Object[0];
        (BASIC_TYPES = new HashSet()).add(Boolean.class);
        CopyFromHelper.BASIC_TYPES.add(Byte.class);
        CopyFromHelper.BASIC_TYPES.add(Character.class);
        CopyFromHelper.BASIC_TYPES.add(Double.class);
        CopyFromHelper.BASIC_TYPES.add(Float.class);
        CopyFromHelper.BASIC_TYPES.add(Integer.class);
        CopyFromHelper.BASIC_TYPES.add(Long.class);
        CopyFromHelper.BASIC_TYPES.add(Short.class);
        CopyFromHelper.BASIC_TYPES.add(String.class);
        CopyFromHelper.BASIC_TYPES.add(Date.class);
    }
}
