// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.impl;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.beans.PropertyDescriptor;
import java.io.Serializable;

public class EqualsBean implements Serializable
{
    private static final Object[] NO_PARAMS;
    private Class _beanClass;
    private Object _obj;
    
    protected EqualsBean(final Class beanClass) {
        this._beanClass = beanClass;
        this._obj = this;
    }
    
    public EqualsBean(final Class beanClass, final Object obj) {
        if (!beanClass.isInstance(obj)) {
            throw new IllegalArgumentException(obj.getClass() + " is not instance of " + beanClass);
        }
        this._beanClass = beanClass;
        this._obj = obj;
    }
    
    public boolean equals(final Object obj) {
        return this.beanEquals(obj);
    }
    
    public boolean beanEquals(final Object obj) {
        final Object bean1 = this._obj;
        final Object bean2 = obj;
        boolean eq;
        if (bean1 == null && bean2 == null) {
            eq = true;
        }
        else if (bean1 == null || bean2 == null) {
            eq = false;
        }
        else if (!this._beanClass.isInstance(bean2)) {
            eq = false;
        }
        else {
            eq = true;
            try {
                final PropertyDescriptor[] pds = BeanIntrospector.getPropertyDescriptors(this._beanClass);
                if (pds != null) {
                    for (int i = 0; eq && i < pds.length; ++i) {
                        final Method pReadMethod = pds[i].getReadMethod();
                        if (pReadMethod != null && pReadMethod.getDeclaringClass() != Object.class && pReadMethod.getParameterTypes().length == 0) {
                            final Object value1 = pReadMethod.invoke(bean1, EqualsBean.NO_PARAMS);
                            final Object value2 = pReadMethod.invoke(bean2, EqualsBean.NO_PARAMS);
                            eq = this.doEquals(value1, value2);
                        }
                    }
                }
            }
            catch (Exception ex) {
                throw new RuntimeException("Could not execute equals()", ex);
            }
        }
        return eq;
    }
    
    public int hashCode() {
        return this.beanHashCode();
    }
    
    public int beanHashCode() {
        return this._obj.toString().hashCode();
    }
    
    private boolean doEquals(final Object obj1, final Object obj2) {
        boolean eq = obj1 == obj2;
        if (!eq && obj1 != null && obj2 != null) {
            final Class classObj1 = obj1.getClass();
            final Class classObj2 = obj2.getClass();
            if (classObj1.isArray() && classObj2.isArray()) {
                eq = this.equalsArray(obj1, obj2);
            }
            else {
                eq = obj1.equals(obj2);
            }
        }
        return eq;
    }
    
    private boolean equalsArray(final Object array1, final Object array2) {
        final int length1 = Array.getLength(array1);
        final int length2 = Array.getLength(array2);
        boolean eq;
        if (length1 == length2) {
            eq = true;
            Object e1;
            Object e2;
            for (int i = 0; eq && i < length1; eq = this.doEquals(e1, e2), ++i) {
                e1 = Array.get(array1, i);
                e2 = Array.get(array2, i);
            }
        }
        else {
            eq = false;
        }
        return eq;
    }
    
    static {
        NO_PARAMS = new Object[0];
    }
}
