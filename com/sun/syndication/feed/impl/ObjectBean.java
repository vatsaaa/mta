// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.impl;

import java.util.Set;
import java.io.Serializable;

public class ObjectBean implements Serializable, Cloneable
{
    private EqualsBean _equalsBean;
    private ToStringBean _toStringBean;
    private CloneableBean _cloneableBean;
    
    public ObjectBean(final Class beanClass, final Object obj) {
        this(beanClass, obj, null);
    }
    
    public ObjectBean(final Class beanClass, final Object obj, final Set ignoreProperties) {
        this._equalsBean = new EqualsBean(beanClass, obj);
        this._toStringBean = new ToStringBean(beanClass, obj);
        this._cloneableBean = new CloneableBean(obj, ignoreProperties);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._cloneableBean.beanClone();
    }
    
    public boolean equals(final Object other) {
        return this._equalsBean.beanEquals(other);
    }
    
    public int hashCode() {
        return this._equalsBean.beanHashCode();
    }
    
    public String toString() {
        return this._toStringBean.toString();
    }
}
