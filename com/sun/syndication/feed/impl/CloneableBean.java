// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.lang.reflect.Array;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.lang.reflect.Method;
import java.beans.PropertyDescriptor;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.io.Serializable;

public class CloneableBean implements Serializable, Cloneable
{
    private static final Class[] NO_PARAMS_DEF;
    private static final Object[] NO_PARAMS;
    private Object _obj;
    private Set _ignoreProperties;
    private static final Set BASIC_TYPES;
    private static final Map CONSTRUCTOR_BASIC_TYPES;
    
    protected CloneableBean() {
        this._obj = this;
    }
    
    public CloneableBean(final Object obj) {
        this(obj, null);
    }
    
    public CloneableBean(final Object obj, final Set ignoreProperties) {
        this._obj = obj;
        this._ignoreProperties = ((ignoreProperties != null) ? ignoreProperties : Collections.EMPTY_SET);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this.beanClone();
    }
    
    public Object beanClone() throws CloneNotSupportedException {
        Object clonedBean;
        try {
            clonedBean = this._obj.getClass().newInstance();
            final PropertyDescriptor[] pds = BeanIntrospector.getPropertyDescriptors(this._obj.getClass());
            if (pds != null) {
                for (int i = 0; i < pds.length; ++i) {
                    final Method pReadMethod = pds[i].getReadMethod();
                    final Method pWriteMethod = pds[i].getWriteMethod();
                    if (pReadMethod != null && pWriteMethod != null && !this._ignoreProperties.contains(pds[i].getName()) && pReadMethod.getDeclaringClass() != Object.class && pReadMethod.getParameterTypes().length == 0) {
                        Object value = pReadMethod.invoke(this._obj, CloneableBean.NO_PARAMS);
                        if (value != null) {
                            value = this.doClone(value);
                            pWriteMethod.invoke(clonedBean, value);
                        }
                    }
                }
            }
        }
        catch (CloneNotSupportedException cnsEx) {
            throw cnsEx;
        }
        catch (Exception ex) {
            System.out.println(ex);
            ex.printStackTrace(System.out);
            throw new CloneNotSupportedException("Cannot clone a " + this._obj.getClass() + " object");
        }
        return clonedBean;
    }
    
    private Object doClone(Object value) throws Exception {
        if (value != null) {
            final Class vClass = value.getClass();
            if (vClass.isArray()) {
                value = this.cloneArray(value);
            }
            else if (value instanceof Collection) {
                value = this.cloneCollection((Collection)value);
            }
            else if (value instanceof Map) {
                value = this.cloneMap((Map)value);
            }
            else if (!this.isBasicType(vClass)) {
                if (!(value instanceof Cloneable)) {
                    throw new CloneNotSupportedException("Cannot clone a " + vClass.getName() + " object");
                }
                final Method cloneMethod = vClass.getMethod("clone", (Class[])CloneableBean.NO_PARAMS_DEF);
                if (!Modifier.isPublic(cloneMethod.getModifiers())) {
                    throw new CloneNotSupportedException("Cannot clone a " + value.getClass() + " object, clone() is not public");
                }
                value = cloneMethod.invoke(value, CloneableBean.NO_PARAMS);
            }
        }
        return value;
    }
    
    private Object cloneArray(final Object array) throws Exception {
        final Class elementClass = array.getClass().getComponentType();
        final int length = Array.getLength(array);
        final Object newArray = Array.newInstance(elementClass, length);
        for (int i = 0; i < length; ++i) {
            final Object element = this.doClone(Array.get(array, i));
            Array.set(newArray, i, element);
        }
        return newArray;
    }
    
    private Object cloneCollection(final Collection collection) throws Exception {
        final Class mClass = collection.getClass();
        final Collection newColl = mClass.newInstance();
        final Iterator i = collection.iterator();
        while (i.hasNext()) {
            final Object element = this.doClone(i.next());
            newColl.add(element);
        }
        return newColl;
    }
    
    private Object cloneMap(final Map map) throws Exception {
        final Class mClass = map.getClass();
        final Map newMap = mClass.newInstance();
        for (final Map.Entry entry : map.entrySet()) {
            final Object key = this.doClone(entry.getKey());
            final Object value = this.doClone(entry.getValue());
            newMap.put(key, value);
        }
        return newMap;
    }
    
    private boolean isBasicType(final Class vClass) {
        return CloneableBean.BASIC_TYPES.contains(vClass);
    }
    
    static {
        NO_PARAMS_DEF = new Class[0];
        NO_PARAMS = new Object[0];
        (BASIC_TYPES = new HashSet()).add(Boolean.class);
        CloneableBean.BASIC_TYPES.add(Byte.class);
        CloneableBean.BASIC_TYPES.add(Character.class);
        CloneableBean.BASIC_TYPES.add(Double.class);
        CloneableBean.BASIC_TYPES.add(Float.class);
        CloneableBean.BASIC_TYPES.add(Integer.class);
        CloneableBean.BASIC_TYPES.add(Long.class);
        CloneableBean.BASIC_TYPES.add(Short.class);
        CloneableBean.BASIC_TYPES.add(String.class);
        (CONSTRUCTOR_BASIC_TYPES = new HashMap()).put(Boolean.class, new Class[] { Boolean.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(Byte.class, new Class[] { Byte.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(Character.class, new Class[] { Character.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(Double.class, new Class[] { Double.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(Float.class, new Class[] { Float.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(Integer.class, new Class[] { Integer.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(Long.class, new Class[] { Long.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(Short.class, new Class[] { Short.TYPE });
        CloneableBean.CONSTRUCTOR_BASIC_TYPES.put(String.class, new Class[] { String.class });
    }
}
