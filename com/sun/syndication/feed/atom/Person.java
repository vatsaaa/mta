// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.atom;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Person implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _name;
    private String _uri;
    private String _email;
    
    public Person() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getName() {
        return this._name;
    }
    
    public void setName(final String name) {
        this._name = name;
    }
    
    public String getUrl() {
        return this._uri;
    }
    
    public void setUrl(final String url) {
        this._uri = url;
    }
    
    public String getEmail() {
        return this._email;
    }
    
    public void setEmail(final String email) {
        this._email = email;
    }
    
    public String getUri() {
        return this._uri;
    }
    
    public void setUri(final String uri) {
        this._uri = uri;
    }
}
