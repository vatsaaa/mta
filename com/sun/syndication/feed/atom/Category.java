// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.atom;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Category implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _term;
    private String _scheme;
    private String _label;
    
    public Category() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getLabel() {
        return this._label;
    }
    
    public void setLabel(final String label) {
        this._label = label;
    }
    
    public String getScheme() {
        return this._scheme;
    }
    
    public void setScheme(final String scheme) {
        this._scheme = scheme;
    }
    
    public String getTerm() {
        return this._term;
    }
    
    public void setTerm(final String term) {
        this._term = term;
    }
}
