// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.atom;

import java.util.HashSet;
import java.util.Set;
import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Content implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _type;
    private String _value;
    private String _src;
    public static final String TEXT = "text";
    public static final String HTML = "html";
    public static final String XHTML = "xhtml";
    public static final String XML = "xml";
    public static final String BASE64 = "base64";
    public static final String ESCAPED = "escaped";
    private String _mode;
    private static final Set MODES;
    
    public Content() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getType() {
        return this._type;
    }
    
    public void setType(final String type) {
        this._type = type;
    }
    
    public String getMode() {
        return this._mode;
    }
    
    public void setMode(String mode) {
        mode = ((mode != null) ? mode.toLowerCase() : null);
        if (mode == null || !Content.MODES.contains(mode)) {
            throw new IllegalArgumentException("Invalid mode [" + mode + "]");
        }
        this._mode = mode;
    }
    
    public String getValue() {
        return this._value;
    }
    
    public void setValue(final String value) {
        this._value = value;
    }
    
    public String getSrc() {
        return this._src;
    }
    
    public void setSrc(final String src) {
        this._src = src;
    }
    
    static {
        (MODES = new HashSet()).add("xml");
        Content.MODES.add("base64");
        Content.MODES.add("escaped");
    }
}
