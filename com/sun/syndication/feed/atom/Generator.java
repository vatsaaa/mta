// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.atom;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Generator implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _url;
    private String _version;
    private String _value;
    
    public Generator() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getUrl() {
        return this._url;
    }
    
    public void setUrl(final String url) {
        this._url = url;
    }
    
    public String getVersion() {
        return this._version;
    }
    
    public void setVersion(final String version) {
        this._version = version;
    }
    
    public String getValue() {
        return this._value;
    }
    
    public void setValue(final String value) {
        this._value = value;
    }
}
