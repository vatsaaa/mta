// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.atom;

import com.sun.syndication.feed.module.impl.ModuleUtils;
import com.sun.syndication.feed.module.Module;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.sun.syndication.feed.impl.ObjectBean;
import com.sun.syndication.feed.module.Extendable;
import java.io.Serializable;

public class Entry implements Cloneable, Serializable, Extendable
{
    private ObjectBean _objBean;
    private String _xmlBase;
    private List _authors;
    private List _contributors;
    private List _categories;
    private List _contents;
    private String _id;
    private Date _published;
    private String _rights;
    private Feed _source;
    private Content _summary;
    private Content _title;
    private Date _updated;
    private List _alternateLinks;
    private List _otherLinks;
    private List _foreignMarkup;
    private List _modules;
    private Date _created;
    
    public Entry() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        final Object fm = this.getForeignMarkup();
        this.setForeignMarkup(((Entry)other).getForeignMarkup());
        final boolean ret = this._objBean.equals(other);
        this.setForeignMarkup(fm);
        return ret;
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getTitle() {
        if (this._title != null) {
            return this._title.getValue();
        }
        return null;
    }
    
    public void setTitle(final String title) {
        if (this._title == null) {
            this._title = new Content();
        }
        this._title.setValue(title);
    }
    
    public Content getTitleEx() {
        return this._title;
    }
    
    public void setTitleEx(final Content title) {
        this._title = title;
    }
    
    public List getAlternateLinks() {
        return (this._alternateLinks == null) ? (this._alternateLinks = new ArrayList()) : this._alternateLinks;
    }
    
    public void setAlternateLinks(final List alternateLinks) {
        this._alternateLinks = alternateLinks;
    }
    
    public List getOtherLinks() {
        return (this._otherLinks == null) ? (this._otherLinks = new ArrayList()) : this._otherLinks;
    }
    
    public void setOtherLinks(final List otherLinks) {
        this._otherLinks = otherLinks;
    }
    
    public List getAuthors() {
        return this._authors;
    }
    
    public void setAuthors(final List authors) {
        this._authors = authors;
    }
    
    public List getContributors() {
        return (this._contributors == null) ? (this._contributors = new ArrayList()) : this._contributors;
    }
    
    public void setContributors(final List contributors) {
        this._contributors = contributors;
    }
    
    public String getId() {
        return this._id;
    }
    
    public void setId(final String id) {
        this._id = id;
    }
    
    public Date getModified() {
        return this._updated;
    }
    
    public void setModified(final Date modified) {
        this._updated = modified;
    }
    
    public Date getIssued() {
        return this._published;
    }
    
    public void setIssued(final Date issued) {
        this._published = issued;
    }
    
    public Date getCreated() {
        return this._created;
    }
    
    public void setCreated(final Date created) {
        this._created = created;
    }
    
    public Content getSummary() {
        return this._summary;
    }
    
    public void setSummary(final Content summary) {
        this._summary = summary;
    }
    
    public List getContents() {
        return (this._contents == null) ? (this._contents = new ArrayList()) : this._contents;
    }
    
    public void setContents(final List contents) {
        this._contents = contents;
    }
    
    public List getModules() {
        return (this._modules == null) ? (this._modules = new ArrayList()) : this._modules;
    }
    
    public void setModules(final List modules) {
        this._modules = modules;
    }
    
    public Module getModule(final String uri) {
        return ModuleUtils.getModule(this._modules, uri);
    }
    
    public Date getPublished() {
        return this._published;
    }
    
    public void setPublished(final Date published) {
        this._published = published;
    }
    
    public String getRights() {
        return this._rights;
    }
    
    public void setRights(final String rights) {
        this._rights = rights;
    }
    
    public Feed getSource() {
        return this._source;
    }
    
    public void setSource(final Feed source) {
        this._source = source;
    }
    
    public Date getUpdated() {
        return this._updated;
    }
    
    public void setUpdated(final Date updated) {
        this._updated = updated;
    }
    
    public List getCategories() {
        return this._categories;
    }
    
    public void setCategories(final List categories) {
        this._categories = categories;
    }
    
    public String getXmlBase() {
        return this._xmlBase;
    }
    
    public void setXmlBase(final String xmlBase) {
        this._xmlBase = xmlBase;
    }
    
    public Object getForeignMarkup() {
        return (this._foreignMarkup == null) ? (this._foreignMarkup = new ArrayList()) : this._foreignMarkup;
    }
    
    public void setForeignMarkup(final Object foreignMarkup) {
        this._foreignMarkup = (List)foreignMarkup;
    }
}
