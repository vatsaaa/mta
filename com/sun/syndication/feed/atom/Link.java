// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.atom;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Link implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _href;
    private String _rel;
    private String _type;
    private String _hreflang;
    private String _title;
    private long _length;
    
    public Link() {
        this._rel = "alternate";
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getRel() {
        return this._rel;
    }
    
    public void setRel(final String rel) {
        this._rel = rel;
    }
    
    public String getType() {
        return this._type;
    }
    
    public void setType(final String type) {
        this._type = type;
    }
    
    public String getHref() {
        return this._href;
    }
    
    public void setHref(final String href) {
        this._href = href;
    }
    
    public String getTitle() {
        return this._title;
    }
    
    public void setTitle(final String title) {
        this._title = title;
    }
    
    public String getHreflang() {
        return this._hreflang;
    }
    
    public void setHreflang(final String hreflang) {
        this._hreflang = hreflang;
    }
    
    public long getLength() {
        return this._length;
    }
    
    public void setLength(final long length) {
        this._length = length;
    }
}
