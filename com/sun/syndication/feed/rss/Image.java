// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.rss;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Image implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _title;
    private String _url;
    private String _link;
    private int _width;
    private int _height;
    private String _description;
    
    public Image() {
        this._width = -1;
        this._height = -1;
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getTitle() {
        return this._title;
    }
    
    public void setTitle(final String title) {
        this._title = title;
    }
    
    public String getUrl() {
        return this._url;
    }
    
    public void setUrl(final String url) {
        this._url = url;
    }
    
    public String getLink() {
        return this._link;
    }
    
    public void setLink(final String link) {
        this._link = link;
    }
    
    public int getWidth() {
        return this._width;
    }
    
    public void setWidth(final int width) {
        this._width = width;
    }
    
    public int getHeight() {
        return this._height;
    }
    
    public void setHeight(final int height) {
        this._height = height;
    }
    
    public String getDescription() {
        return this._description;
    }
    
    public void setDescription(final String description) {
        this._description = description;
    }
}
