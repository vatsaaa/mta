// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.rss;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Content implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _type;
    private String _value;
    public static final String HTML = "html";
    public static final String TEXT = "text";
    
    public Content() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getType() {
        return this._type;
    }
    
    public void setType(final String type) {
        this._type = type;
    }
    
    public String getValue() {
        return this._value;
    }
    
    public void setValue(final String value) {
        this._value = value;
    }
}
