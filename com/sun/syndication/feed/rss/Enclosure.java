// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.rss;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Enclosure implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _url;
    private long _length;
    private String _type;
    
    public Enclosure() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getUrl() {
        return this._url;
    }
    
    public void setUrl(final String url) {
        this._url = url;
    }
    
    public long getLength() {
        return this._length;
    }
    
    public void setLength(final long length) {
        this._length = length;
    }
    
    public String getType() {
        return this._type;
    }
    
    public void setType(final String type) {
        this._type = type;
    }
}
