// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.rss;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class TextInput implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private String _title;
    private String _description;
    private String _name;
    private String _link;
    
    public TextInput() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getTitle() {
        return this._title;
    }
    
    public void setTitle(final String title) {
        this._title = title;
    }
    
    public String getDescription() {
        return this._description;
    }
    
    public void setDescription(final String description) {
        this._description = description;
    }
    
    public String getName() {
        return this._name;
    }
    
    public void setName(final String name) {
        this._name = name;
    }
    
    public String getLink() {
        return this._link;
    }
    
    public void setLink(final String link) {
        this._link = link;
    }
}
