// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.rss;

import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class Guid implements Cloneable, Serializable
{
    private ObjectBean _objBean;
    private boolean _permaLink;
    private String _value;
    
    public Guid() {
        this._objBean = new ObjectBean(this.getClass(), this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public boolean isPermaLink() {
        return this._permaLink;
    }
    
    public void setPermaLink(final boolean permaLink) {
        this._permaLink = permaLink;
    }
    
    public String getValue() {
        return this._value;
    }
    
    public void setValue(final String value) {
        this._value = value;
    }
}
