// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

public interface SyndCategory extends Cloneable
{
    String getName();
    
    void setName(final String p0);
    
    String getTaxonomyUri();
    
    void setTaxonomyUri(final String p0);
    
    Object clone() throws CloneNotSupportedException;
}
