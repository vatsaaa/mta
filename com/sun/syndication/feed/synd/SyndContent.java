// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import com.sun.syndication.feed.CopyFrom;

public interface SyndContent extends Cloneable, CopyFrom
{
    String getType();
    
    void setType(final String p0);
    
    String getMode();
    
    void setMode(final String p0);
    
    String getValue();
    
    void setValue(final String p0);
    
    Object clone() throws CloneNotSupportedException;
}
