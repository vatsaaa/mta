// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import com.sun.syndication.feed.CopyFrom;

public interface SyndImage extends Cloneable, CopyFrom
{
    String getTitle();
    
    void setTitle(final String p0);
    
    String getUrl();
    
    void setUrl(final String p0);
    
    String getLink();
    
    void setLink(final String p0);
    
    String getDescription();
    
    void setDescription(final String p0);
    
    Object clone() throws CloneNotSupportedException;
}
