// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import java.util.Map;
import java.util.Collections;
import java.util.HashMap;
import com.sun.syndication.feed.impl.CopyFromHelper;
import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class SyndContentImpl implements Serializable, SyndContent
{
    private ObjectBean _objBean;
    private String _type;
    private String _value;
    private String _mode;
    private static final CopyFromHelper COPY_FROM_HELPER;
    
    public SyndContentImpl() {
        this._objBean = new ObjectBean(SyndContent.class, this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getType() {
        return this._type;
    }
    
    public void setType(final String type) {
        this._type = type;
    }
    
    public String getMode() {
        return this._mode;
    }
    
    public void setMode(final String mode) {
        this._mode = mode;
    }
    
    public String getValue() {
        return this._value;
    }
    
    public void setValue(final String value) {
        this._value = value;
    }
    
    public Class getInterface() {
        return SyndContent.class;
    }
    
    public void copyFrom(final Object obj) {
        SyndContentImpl.COPY_FROM_HELPER.copy(this, obj);
    }
    
    static {
        final Map basePropInterfaceMap = new HashMap();
        basePropInterfaceMap.put("type", String.class);
        basePropInterfaceMap.put("value", String.class);
        final Map basePropClassImplMap = Collections.EMPTY_MAP;
        COPY_FROM_HELPER = new CopyFromHelper(SyndContent.class, basePropInterfaceMap, basePropClassImplMap);
    }
}
