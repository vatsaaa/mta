// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import com.sun.syndication.feed.module.DCSubject;
import java.util.ArrayList;
import java.util.List;
import java.util.AbstractList;

class SyndCategoryListFacade extends AbstractList
{
    private List _subjects;
    
    public SyndCategoryListFacade() {
        this(new ArrayList());
    }
    
    public SyndCategoryListFacade(final List subjects) {
        this._subjects = subjects;
    }
    
    public Object get(final int index) {
        return new SyndCategoryImpl(this._subjects.get(index));
    }
    
    public int size() {
        return this._subjects.size();
    }
    
    public Object set(final int index, final Object obj) {
        final SyndCategoryImpl sCat = (SyndCategoryImpl)obj;
        DCSubject subject = (sCat != null) ? sCat.getSubject() : null;
        subject = this._subjects.set(index, subject);
        return (subject != null) ? new SyndCategoryImpl(subject) : null;
    }
    
    public void add(final int index, final Object obj) {
        final SyndCategoryImpl sCat = (SyndCategoryImpl)obj;
        final DCSubject subject = (sCat != null) ? sCat.getSubject() : null;
        this._subjects.add(index, subject);
    }
    
    public Object remove(final int index) {
        final DCSubject subject = this._subjects.remove(index);
        return (subject != null) ? new SyndCategoryImpl(subject) : null;
    }
    
    public static List convertElementsSyndCategoryToSubject(final List cList) {
        List sList = null;
        if (cList != null) {
            sList = new ArrayList();
            for (int i = 0; i < cList.size(); ++i) {
                final SyndCategoryImpl sCat = cList.get(i);
                DCSubject subject = null;
                if (sCat != null) {
                    subject = sCat.getSubject();
                }
                sList.add(subject);
            }
        }
        return sList;
    }
}
