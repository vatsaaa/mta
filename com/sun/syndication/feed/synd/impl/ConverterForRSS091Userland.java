// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.feed.rss.Content;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.rss.Description;
import java.util.ArrayList;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.rss.Item;
import com.sun.syndication.feed.synd.SyndImage;
import com.sun.syndication.feed.rss.Image;
import java.util.Set;
import java.util.List;
import java.util.Date;
import java.util.Collection;
import java.util.HashSet;
import com.sun.syndication.feed.module.DCModule;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.WireFeed;

public class ConverterForRSS091Userland extends ConverterForRSS090
{
    public ConverterForRSS091Userland() {
        this("rss_0.91U");
    }
    
    protected ConverterForRSS091Userland(final String type) {
        super(type);
    }
    
    public void copyInto(final WireFeed feed, final SyndFeed syndFeed) {
        final Channel channel = (Channel)feed;
        super.copyInto(channel, syndFeed);
        syndFeed.setLanguage(channel.getLanguage());
        syndFeed.setCopyright(channel.getCopyright());
        final Date pubDate = channel.getPubDate();
        if (pubDate != null) {
            syndFeed.setPublishedDate(pubDate);
        }
        else if (channel.getLastBuildDate() != null) {
            syndFeed.setPublishedDate(channel.getLastBuildDate());
        }
        final String author = channel.getManagingEditor();
        if (author != null) {
            final List creators = ((DCModule)syndFeed.getModule("http://purl.org/dc/elements/1.1/")).getCreators();
            if (!creators.contains(author)) {
                final Set s = new HashSet();
                s.addAll(creators);
                s.add(author);
                creators.clear();
                creators.addAll(s);
            }
        }
    }
    
    protected SyndImage createSyndImage(final Image rssImage) {
        final SyndImage syndImage = super.createSyndImage(rssImage);
        syndImage.setDescription(rssImage.getDescription());
        return syndImage;
    }
    
    protected SyndEntry createSyndEntry(final Item item) {
        final SyndEntry syndEntry = super.createSyndEntry(item);
        final Description desc = item.getDescription();
        if (desc != null) {
            final SyndContent descContent = new SyndContentImpl();
            descContent.setType(desc.getType());
            descContent.setValue(desc.getValue());
            syndEntry.setDescription(descContent);
        }
        final Content cont = item.getContent();
        if (cont != null) {
            final SyndContent content = new SyndContentImpl();
            content.setType(cont.getType());
            content.setValue(cont.getValue());
            final List syndContents = new ArrayList();
            syndContents.add(content);
            syndEntry.setContents(syndContents);
        }
        return syndEntry;
    }
    
    protected WireFeed createRealFeed(final String type, final SyndFeed syndFeed) {
        final Channel channel = (Channel)super.createRealFeed(type, syndFeed);
        channel.setLanguage(syndFeed.getLanguage());
        channel.setCopyright(syndFeed.getCopyright());
        channel.setPubDate(syndFeed.getPublishedDate());
        if (syndFeed.getAuthors() != null && syndFeed.getAuthors().size() > 0) {
            final SyndPerson author = syndFeed.getAuthors().get(0);
            channel.setManagingEditor(author.getName());
        }
        return channel;
    }
    
    protected Image createRSSImage(final SyndImage sImage) {
        final Image image = super.createRSSImage(sImage);
        image.setDescription(sImage.getDescription());
        return image;
    }
    
    protected Item createRSSItem(final SyndEntry sEntry) {
        final Item item = super.createRSSItem(sEntry);
        final SyndContent sContent = sEntry.getDescription();
        if (sContent != null) {
            item.setDescription(this.createItemDescription(sContent));
        }
        final List contents = sEntry.getContents();
        if (contents != null && contents.size() > 0) {
            final SyndContent syndContent = contents.get(0);
            final Content cont = new Content();
            cont.setValue(syndContent.getValue());
            cont.setType(syndContent.getType());
            item.setContent(cont);
        }
        return item;
    }
    
    protected Description createItemDescription(final SyndContent sContent) {
        final Description desc = new Description();
        desc.setValue(sContent.getValue());
        desc.setType(sContent.getType());
        return desc;
    }
}
