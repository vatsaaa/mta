// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.rss.Enclosure;
import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.rss.Category;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.rss.Item;

public class ConverterForRSS092 extends ConverterForRSS091Userland
{
    public ConverterForRSS092() {
        this("rss_0.92");
    }
    
    protected ConverterForRSS092(final String type) {
        super(type);
    }
    
    protected SyndEntry createSyndEntry(final Item item) {
        final SyndEntry syndEntry = super.createSyndEntry(item);
        final List cats = item.getCategories();
        if (cats.size() > 0) {
            final Set s = new HashSet();
            s.addAll(this.createSyndCategories(cats));
            s.addAll(syndEntry.getCategories());
            syndEntry.setCategories(new ArrayList(s));
        }
        final List enclosures = item.getEnclosures();
        if (enclosures.size() > 0) {
            syndEntry.setEnclosures(this.createSyndEnclosures(enclosures));
        }
        return syndEntry;
    }
    
    protected List createSyndCategories(final List rssCats) {
        final List syndCats = new ArrayList();
        for (int i = 0; i < rssCats.size(); ++i) {
            final Category rssCat = rssCats.get(i);
            final SyndCategory sCat = new SyndCategoryImpl();
            sCat.setTaxonomyUri(rssCat.getDomain());
            sCat.setName(rssCat.getValue());
            syndCats.add(sCat);
        }
        return syndCats;
    }
    
    protected List createSyndEnclosures(final List enclosures) {
        final List sEnclosures = new ArrayList();
        for (int i = 0; i < enclosures.size(); ++i) {
            final Enclosure enc = enclosures.get(i);
            final SyndEnclosure sEnc = new SyndEnclosureImpl();
            sEnc.setUrl(enc.getUrl());
            sEnc.setType(enc.getType());
            sEnc.setLength(enc.getLength());
            sEnclosures.add(sEnc);
        }
        return sEnclosures;
    }
    
    protected Item createRSSItem(final SyndEntry sEntry) {
        final Item item = super.createRSSItem(sEntry);
        final List sCats = sEntry.getCategories();
        if (sCats.size() > 0) {
            item.setCategories(this.createRSSCategories(sCats));
        }
        final List sEnclosures = sEntry.getEnclosures();
        if (sEnclosures.size() > 0) {
            item.setEnclosures(this.createEnclosures(sEnclosures));
        }
        return item;
    }
    
    protected List createRSSCategories(final List sCats) {
        final List cats = new ArrayList();
        for (int i = 0; i < sCats.size(); ++i) {
            final SyndCategory sCat = sCats.get(i);
            final Category cat = new Category();
            cat.setDomain(sCat.getTaxonomyUri());
            cat.setValue(sCat.getName());
            cats.add(cat);
        }
        return cats;
    }
    
    protected List createEnclosures(final List sEnclosures) {
        final List enclosures = new ArrayList();
        for (int i = 0; i < sEnclosures.size(); ++i) {
            final SyndEnclosure sEnc = sEnclosures.get(i);
            final Enclosure enc = new Enclosure();
            enc.setUrl(sEnc.getUrl());
            enc.setType(sEnc.getType());
            enc.setLength(sEnc.getLength());
            enclosures.add(enc);
        }
        return enclosures;
    }
}
