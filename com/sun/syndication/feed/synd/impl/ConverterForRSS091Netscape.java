// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

public class ConverterForRSS091Netscape extends ConverterForRSS091Userland
{
    public ConverterForRSS091Netscape() {
        this("rss_0.91N");
    }
    
    protected ConverterForRSS091Netscape(final String type) {
        super(type);
    }
}
