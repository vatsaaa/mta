// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import java.util.List;
import com.sun.syndication.feed.synd.Converter;
import com.sun.syndication.io.impl.PluginManager;

public class Converters extends PluginManager
{
    public static final String CONVERTERS_KEY = "Converter.classes";
    
    public Converters() {
        super("Converter.classes");
    }
    
    public Converter getConverter(final String feedType) {
        return (Converter)this.getPlugin(feedType);
    }
    
    protected String getKey(final Object obj) {
        return ((Converter)obj).getType();
    }
    
    public List getSupportedFeedTypes() {
        return this.getKeys();
    }
}
