// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

public class ConverterForRSS20 extends ConverterForRSS094
{
    public ConverterForRSS20() {
        this("rss_2.0");
    }
    
    protected ConverterForRSS20(final String type) {
        super(type);
    }
}
