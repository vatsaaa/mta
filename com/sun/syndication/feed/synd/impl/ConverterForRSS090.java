// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.rss.Item;
import java.util.ArrayList;
import com.sun.syndication.feed.synd.SyndImageImpl;
import com.sun.syndication.feed.synd.SyndImage;
import com.sun.syndication.feed.rss.Image;
import com.sun.syndication.feed.rss.Channel;
import java.util.List;
import com.sun.syndication.feed.module.impl.ModuleUtils;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.WireFeed;
import com.sun.syndication.feed.synd.Converter;

public class ConverterForRSS090 implements Converter
{
    private String _type;
    
    public ConverterForRSS090() {
        this("rss_0.9");
    }
    
    protected ConverterForRSS090(final String type) {
        this._type = type;
    }
    
    public String getType() {
        return this._type;
    }
    
    public void copyInto(final WireFeed feed, final SyndFeed syndFeed) {
        syndFeed.setModules(ModuleUtils.cloneModules(feed.getModules()));
        if (((List)feed.getForeignMarkup()).size() > 0) {
            syndFeed.setForeignMarkup(feed.getForeignMarkup());
        }
        syndFeed.setEncoding(feed.getEncoding());
        final Channel channel = (Channel)feed;
        syndFeed.setTitle(channel.getTitle());
        syndFeed.setLink(channel.getLink());
        syndFeed.setDescription(channel.getDescription());
        final Image image = channel.getImage();
        if (image != null) {
            syndFeed.setImage(this.createSyndImage(image));
        }
        final List items = channel.getItems();
        if (items != null) {
            syndFeed.setEntries(this.createSyndEntries(items));
        }
    }
    
    protected SyndImage createSyndImage(final Image rssImage) {
        final SyndImage syndImage = new SyndImageImpl();
        syndImage.setTitle(rssImage.getTitle());
        syndImage.setUrl(rssImage.getUrl());
        syndImage.setLink(rssImage.getLink());
        return syndImage;
    }
    
    protected List createSyndEntries(final List rssItems) {
        final List syndEntries = new ArrayList();
        for (int i = 0; i < rssItems.size(); ++i) {
            syndEntries.add(this.createSyndEntry(rssItems.get(i)));
        }
        return syndEntries;
    }
    
    protected SyndEntry createSyndEntry(final Item item) {
        final SyndEntry syndEntry = new SyndEntryImpl();
        syndEntry.setModules(ModuleUtils.cloneModules(item.getModules()));
        if (((List)item.getForeignMarkup()).size() > 0) {
            syndEntry.setForeignMarkup(item.getForeignMarkup());
        }
        syndEntry.setUri(item.getUri());
        syndEntry.setLink(item.getLink());
        syndEntry.setTitle(item.getTitle());
        syndEntry.setLink(item.getLink());
        return syndEntry;
    }
    
    public WireFeed createRealFeed(final SyndFeed syndFeed) {
        return this.createRealFeed(this.getType(), syndFeed);
    }
    
    protected WireFeed createRealFeed(final String type, final SyndFeed syndFeed) {
        final Channel channel = new Channel(type);
        channel.setModules(ModuleUtils.cloneModules(syndFeed.getModules()));
        channel.setEncoding(syndFeed.getEncoding());
        channel.setTitle(syndFeed.getTitle());
        channel.setLink(syndFeed.getLink());
        channel.setDescription(syndFeed.getDescription());
        final SyndImage sImage = syndFeed.getImage();
        if (sImage != null) {
            channel.setImage(this.createRSSImage(sImage));
        }
        final List sEntries = syndFeed.getEntries();
        if (sEntries != null) {
            channel.setItems(this.createRSSItems(sEntries));
        }
        return channel;
    }
    
    protected Image createRSSImage(final SyndImage sImage) {
        final Image image = new Image();
        image.setTitle(sImage.getTitle());
        image.setUrl(sImage.getUrl());
        image.setLink(sImage.getLink());
        return image;
    }
    
    protected List createRSSItems(final List sEntries) {
        final List list = new ArrayList();
        for (int i = 0; i < sEntries.size(); ++i) {
            list.add(this.createRSSItem(sEntries.get(i)));
        }
        return list;
    }
    
    protected Item createRSSItem(final SyndEntry sEntry) {
        final Item item = new Item();
        item.setModules(ModuleUtils.cloneModules(sEntry.getModules()));
        item.setTitle(sEntry.getTitle());
        item.setLink(sEntry.getLink());
        return item;
    }
}
