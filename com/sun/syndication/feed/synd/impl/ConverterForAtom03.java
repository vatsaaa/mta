// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import com.sun.syndication.feed.synd.SyndPersonImpl;
import java.util.Iterator;
import com.sun.syndication.feed.atom.Person;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.atom.Entry;
import java.util.ArrayList;
import java.util.Date;
import com.sun.syndication.feed.atom.Content;
import com.sun.syndication.feed.atom.Link;
import java.util.List;
import com.sun.syndication.feed.module.impl.ModuleUtils;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.WireFeed;
import com.sun.syndication.feed.synd.Converter;

public class ConverterForAtom03 implements Converter
{
    private String _type;
    
    public ConverterForAtom03() {
        this("atom_0.3");
    }
    
    protected ConverterForAtom03(final String type) {
        this._type = type;
    }
    
    public String getType() {
        return this._type;
    }
    
    public void copyInto(final WireFeed feed, final SyndFeed syndFeed) {
        final Feed aFeed = (Feed)feed;
        syndFeed.setModules(ModuleUtils.cloneModules(aFeed.getModules()));
        if (((List)feed.getForeignMarkup()).size() > 0) {
            syndFeed.setForeignMarkup(feed.getForeignMarkup());
        }
        syndFeed.setEncoding(aFeed.getEncoding());
        syndFeed.setUri(aFeed.getId());
        syndFeed.setTitle(aFeed.getTitle());
        String linkHref = null;
        if (aFeed.getAlternateLinks().size() > 0) {
            linkHref = aFeed.getAlternateLinks().get(0).getHref();
        }
        syndFeed.setLink(linkHref);
        final Content tagline = aFeed.getTagline();
        if (tagline != null) {
            syndFeed.setDescription(tagline.getValue());
        }
        final List aEntries = aFeed.getEntries();
        if (aEntries != null) {
            syndFeed.setEntries(this.createSyndEntries(aEntries));
        }
        final String language = aFeed.getLanguage();
        if (language != null) {
            syndFeed.setLanguage(language);
        }
        final List authors = aFeed.getAuthors();
        if (authors != null && authors.size() > 0) {
            syndFeed.setAuthors(createSyndPersons(authors));
        }
        final String copyright = aFeed.getCopyright();
        if (copyright != null) {
            syndFeed.setCopyright(copyright);
        }
        final Date date = aFeed.getModified();
        if (date != null) {
            syndFeed.setPublishedDate(date);
        }
    }
    
    protected List createSyndEntries(final List atomEntries) {
        final List syndEntries = new ArrayList();
        for (int i = 0; i < atomEntries.size(); ++i) {
            syndEntries.add(this.createSyndEntry(atomEntries.get(i)));
        }
        return syndEntries;
    }
    
    protected SyndEntry createSyndEntry(final Entry entry) {
        final SyndEntry syndEntry = new SyndEntryImpl();
        syndEntry.setModules(ModuleUtils.cloneModules(entry.getModules()));
        if (((List)entry.getForeignMarkup()).size() > 0) {
            syndEntry.setForeignMarkup(entry.getForeignMarkup());
        }
        syndEntry.setTitle(entry.getTitle());
        String linkHref = null;
        if (entry.getAlternateLinks().size() > 0) {
            linkHref = entry.getAlternateLinks().get(0).getHref();
        }
        syndEntry.setLink(linkHref);
        final String id = entry.getId();
        if (id != null) {
            syndEntry.setUri(entry.getId());
        }
        else {
            syndEntry.setUri(syndEntry.getLink());
        }
        Content content = entry.getSummary();
        if (content == null) {
            final List contents = entry.getContents();
            if (contents != null && contents.size() > 0) {
                content = contents.get(0);
            }
        }
        if (content != null) {
            final SyndContent sContent = new SyndContentImpl();
            sContent.setType(content.getType());
            sContent.setValue(content.getValue());
            syndEntry.setDescription(sContent);
        }
        final List contents = entry.getContents();
        if (contents.size() > 0) {
            final List sContents = new ArrayList();
            for (int i = 0; i < contents.size(); ++i) {
                content = contents.get(i);
                final SyndContent sContent2 = new SyndContentImpl();
                sContent2.setType(content.getType());
                sContent2.setValue(content.getValue());
                sContent2.setMode(content.getMode());
                sContents.add(sContent2);
            }
            syndEntry.setContents(sContents);
        }
        final List authors = entry.getAuthors();
        if (authors != null && authors.size() > 0) {
            syndEntry.setAuthors(createSyndPersons(authors));
            final SyndPerson person0 = syndEntry.getAuthors().get(0);
            syndEntry.setAuthor(person0.getName());
        }
        Date date = entry.getModified();
        if (date == null) {
            date = entry.getIssued();
            if (date == null) {
                date = entry.getCreated();
            }
        }
        if (date != null) {
            syndEntry.setPublishedDate(date);
        }
        return syndEntry;
    }
    
    public WireFeed createRealFeed(final SyndFeed syndFeed) {
        final Feed aFeed = new Feed(this.getType());
        aFeed.setModules(ModuleUtils.cloneModules(syndFeed.getModules()));
        aFeed.setEncoding(syndFeed.getEncoding());
        aFeed.setId(syndFeed.getUri());
        aFeed.setTitle(syndFeed.getTitle());
        final String sLink = syndFeed.getLink();
        if (sLink != null) {
            final Link link = new Link();
            link.setRel("alternate");
            link.setHref(sLink);
            final List list = new ArrayList();
            list.add(link);
            aFeed.setAlternateLinks(list);
        }
        final String sDesc = syndFeed.getDescription();
        if (sDesc != null) {
            final Content tagline = new Content();
            tagline.setValue(sDesc);
            aFeed.setTagline(tagline);
        }
        aFeed.setLanguage(syndFeed.getLanguage());
        final List authors = syndFeed.getAuthors();
        if (authors != null && authors.size() > 0) {
            aFeed.setAuthors(createAtomPersons(authors));
        }
        aFeed.setCopyright(syndFeed.getCopyright());
        aFeed.setModified(syndFeed.getPublishedDate());
        final List sEntries = syndFeed.getEntries();
        if (sEntries != null) {
            aFeed.setEntries(this.createAtomEntries(sEntries));
        }
        return aFeed;
    }
    
    protected static List createAtomPersons(final List sPersons) {
        final List persons = new ArrayList();
        for (final SyndPerson sPerson : sPersons) {
            final Person person = new Person();
            person.setName(sPerson.getName());
            person.setUri(sPerson.getUri());
            person.setEmail(sPerson.getEmail());
            persons.add(person);
        }
        return persons;
    }
    
    protected static List createSyndPersons(final List aPersons) {
        final List persons = new ArrayList();
        for (final Person aPerson : aPersons) {
            final SyndPerson person = new SyndPersonImpl();
            person.setName(aPerson.getName());
            person.setUri(aPerson.getUri());
            person.setEmail(aPerson.getEmail());
            persons.add(person);
        }
        return persons;
    }
    
    protected List createAtomEntries(final List syndEntries) {
        final List atomEntries = new ArrayList();
        for (int i = 0; i < syndEntries.size(); ++i) {
            atomEntries.add(this.createAtomEntry(syndEntries.get(i)));
        }
        return atomEntries;
    }
    
    protected Entry createAtomEntry(final SyndEntry sEntry) {
        final Entry aEntry = new Entry();
        aEntry.setModules(ModuleUtils.cloneModules(sEntry.getModules()));
        aEntry.setId(sEntry.getUri());
        aEntry.setTitle(sEntry.getTitle());
        final String sLink = sEntry.getLink();
        if (sLink != null) {
            final Link link = new Link();
            link.setRel("alternate");
            link.setHref(sLink);
            final List list = new ArrayList();
            list.add(link);
            aEntry.setAlternateLinks(list);
        }
        SyndContent sContent = sEntry.getDescription();
        if (sContent != null) {
            final Content content = new Content();
            content.setType(sContent.getType());
            content.setValue(sContent.getValue());
            content.setMode("escaped");
            aEntry.setSummary(content);
        }
        final List contents = sEntry.getContents();
        if (contents.size() > 0) {
            final List aContents = new ArrayList();
            for (int i = 0; i < contents.size(); ++i) {
                sContent = contents.get(i);
                final Content content2 = new Content();
                content2.setType(sContent.getType());
                content2.setValue(sContent.getValue());
                content2.setMode(sContent.getMode());
                aContents.add(content2);
            }
            aEntry.setContents(aContents);
        }
        final List sAuthors = sEntry.getAuthors();
        if (sAuthors != null && sAuthors.size() > 0) {
            aEntry.setAuthors(createAtomPersons(sAuthors));
        }
        else if (sEntry.getAuthor() != null) {
            final Person person = new Person();
            person.setName(sEntry.getAuthor());
            final List authors = new ArrayList();
            authors.add(person);
            aEntry.setAuthors(authors);
        }
        aEntry.setModified(sEntry.getPublishedDate());
        aEntry.setIssued(sEntry.getPublishedDate());
        return aEntry;
    }
}
