// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import com.sun.syndication.feed.atom.Person;
import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.atom.Category;
import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.feed.synd.SyndLink;
import java.util.Iterator;
import com.sun.syndication.feed.synd.SyndLinkImpl;
import java.util.Date;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.atom.Content;
import java.util.Collection;
import java.util.ArrayList;
import com.sun.syndication.feed.atom.Link;
import com.sun.syndication.feed.synd.SyndContentImpl;
import java.util.List;
import com.sun.syndication.feed.module.impl.ModuleUtils;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.WireFeed;
import com.sun.syndication.feed.synd.Converter;

public class ConverterForAtom10 implements Converter
{
    private String _type;
    
    public ConverterForAtom10() {
        this("atom_1.0");
    }
    
    protected ConverterForAtom10(final String type) {
        this._type = type;
    }
    
    public String getType() {
        return this._type;
    }
    
    public void copyInto(final WireFeed feed, final SyndFeed syndFeed) {
        final Feed aFeed = (Feed)feed;
        syndFeed.setModules(ModuleUtils.cloneModules(aFeed.getModules()));
        if (((List)feed.getForeignMarkup()).size() > 0) {
            syndFeed.setForeignMarkup(feed.getForeignMarkup());
        }
        syndFeed.setEncoding(aFeed.getEncoding());
        syndFeed.setUri(aFeed.getId());
        final Content aTitle = aFeed.getTitleEx();
        if (aTitle != null) {
            final SyndContent c = new SyndContentImpl();
            c.setType(aTitle.getType());
            c.setValue(aTitle.getValue());
            syndFeed.setTitleEx(c);
        }
        final Content aSubtitle = aFeed.getSubtitle();
        if (aSubtitle != null) {
            final SyndContent c2 = new SyndContentImpl();
            c2.setType(aSubtitle.getType());
            c2.setValue(aSubtitle.getValue());
            syndFeed.setDescriptionEx(c2);
        }
        if (aFeed.getAlternateLinks() != null && aFeed.getAlternateLinks().size() == 1) {
            final Link theLink = aFeed.getAlternateLinks().get(0);
            syndFeed.setLink(theLink.getHref());
        }
        final List syndLinks = new ArrayList();
        if (aFeed.getAlternateLinks() != null && aFeed.getAlternateLinks().size() > 0) {
            syndLinks.addAll(this.createSyndLinks(aFeed.getAlternateLinks()));
        }
        if (aFeed.getOtherLinks() != null && aFeed.getOtherLinks().size() > 0) {
            syndLinks.addAll(this.createSyndLinks(aFeed.getOtherLinks()));
        }
        final List aEntries = aFeed.getEntries();
        if (aEntries != null) {
            syndFeed.setEntries(this.createSyndEntries(aFeed, aEntries));
        }
        final List authors = aFeed.getAuthors();
        if (authors != null && authors.size() > 0) {
            syndFeed.setAuthors(ConverterForAtom03.createSyndPersons(authors));
        }
        final String rights = aFeed.getRights();
        if (rights != null) {
            syndFeed.setCopyright(rights);
        }
        final Date date = aFeed.getUpdated();
        if (date != null) {
            syndFeed.setPublishedDate(date);
        }
    }
    
    protected List createSyndLinks(final List aLinks) {
        final ArrayList sLinks = new ArrayList();
        for (final Link link : aLinks) {
            final SyndLink sLink = new SyndLinkImpl();
            sLink.setRel(link.getRel());
            sLink.setHref(link.getHref());
            sLink.setType(link.getType());
            sLink.setTitle(link.getTitle());
            sLink.setLength(link.getLength());
            sLink.setHreflang(link.getHref());
            sLinks.add(sLink);
        }
        return sLinks;
    }
    
    protected List createSyndEntries(final Feed feed, final List atomEntries) {
        final List syndEntries = new ArrayList();
        for (int i = 0; i < atomEntries.size(); ++i) {
            syndEntries.add(this.createSyndEntry(feed, atomEntries.get(i)));
        }
        return syndEntries;
    }
    
    protected SyndEntry createSyndEntry(final Feed feed, final Entry entry) {
        final SyndEntry syndEntry = new SyndEntryImpl();
        syndEntry.setModules(ModuleUtils.cloneModules(entry.getModules()));
        if (((List)entry.getForeignMarkup()).size() > 0) {
            syndEntry.setForeignMarkup(entry.getForeignMarkup());
        }
        final Content eTitle = entry.getTitleEx();
        if (eTitle != null) {
            syndEntry.setTitleEx(this.createSyndContent(eTitle));
        }
        final Content summary = entry.getSummary();
        if (summary != null) {
            syndEntry.setDescription(this.createSyndContent(summary));
        }
        final String id = entry.getId();
        if (id != null) {
            syndEntry.setUri(entry.getId());
        }
        else {
            syndEntry.setUri(syndEntry.getLink());
        }
        final List contents = entry.getContents();
        if (contents != null && contents.size() > 0) {
            final List sContents = new ArrayList();
            for (final Content content : contents) {
                sContents.add(this.createSyndContent(content));
            }
            syndEntry.setContents(sContents);
        }
        final List authors = entry.getAuthors();
        if (authors != null && authors.size() > 0) {
            syndEntry.setAuthors(ConverterForAtom03.createSyndPersons(authors));
            final SyndPerson person0 = syndEntry.getAuthors().get(0);
            syndEntry.setAuthor(person0.getName());
        }
        Date date = entry.getPublished();
        if (date != null) {
            syndEntry.setPublishedDate(date);
        }
        date = entry.getUpdated();
        if (date != null) {
            syndEntry.setUpdatedDate(date);
        }
        final List categories = entry.getCategories();
        if (categories != null) {
            final List syndCategories = new ArrayList();
            for (final Category c : categories) {
                final SyndCategory syndCategory = new SyndCategoryImpl();
                syndCategory.setName(c.getTerm());
                syndCategory.setTaxonomyUri(c.getScheme());
                syndCategories.add(syndCategory);
            }
            syndEntry.setCategories(syndCategories);
        }
        if (entry.getAlternateLinks() != null && entry.getAlternateLinks().size() == 1) {
            final Link theLink = entry.getAlternateLinks().get(0);
            syndEntry.setLink(theLink.getHref());
        }
        final List syndEnclosures = new ArrayList();
        if (entry.getOtherLinks() != null && entry.getOtherLinks().size() > 0) {
            final List oLinks = entry.getOtherLinks();
            for (final Link thisLink : oLinks) {
                if ("enclosure".equals(thisLink.getRel())) {
                    syndEnclosures.add(this.createSyndEnclosure(feed, entry, thisLink));
                }
            }
        }
        syndEntry.setEnclosures(syndEnclosures);
        final List syndLinks = new ArrayList();
        if (entry.getAlternateLinks() != null && entry.getAlternateLinks().size() > 0) {
            syndLinks.addAll(this.createSyndLinks(entry.getAlternateLinks()));
        }
        if (entry.getOtherLinks() != null && entry.getOtherLinks().size() > 0) {
            syndLinks.addAll(this.createSyndLinks(entry.getOtherLinks()));
        }
        syndEntry.setLinks(syndLinks);
        return syndEntry;
    }
    
    public SyndEnclosure createSyndEnclosure(final Feed feed, final Entry entry, final Link link) {
        final SyndEnclosure syndEncl = new SyndEnclosureImpl();
        syndEncl.setUrl(link.getHref());
        syndEncl.setType(link.getType());
        syndEncl.setLength(link.getLength());
        return syndEncl;
    }
    
    public SyndLink createSyndLink(final Feed feed, final Entry entry, final Link link) {
        final SyndLink syndLink = new SyndLinkImpl();
        syndLink.setRel(link.getRel());
        syndLink.setType(link.getType());
        syndLink.setHref(link.getHref());
        syndLink.setHreflang(link.getHreflang());
        syndLink.setLength(link.getLength());
        return syndLink;
    }
    
    public WireFeed createRealFeed(final SyndFeed syndFeed) {
        final Feed aFeed = new Feed(this.getType());
        aFeed.setModules(ModuleUtils.cloneModules(syndFeed.getModules()));
        aFeed.setEncoding(syndFeed.getEncoding());
        aFeed.setId(syndFeed.getUri());
        final SyndContent sTitle = syndFeed.getTitleEx();
        if (sTitle != null) {
            final Content title = new Content();
            title.setType(sTitle.getType());
            title.setValue(sTitle.getValue());
            aFeed.setTitleEx(title);
        }
        final SyndContent sDesc = syndFeed.getDescriptionEx();
        if (sDesc != null) {
            final Content subtitle = new Content();
            subtitle.setType(sDesc.getType());
            subtitle.setValue(sDesc.getValue());
            aFeed.setSubtitle(subtitle);
        }
        final List alternateLinks = new ArrayList();
        final List otherLinks = new ArrayList();
        final String sLink = syndFeed.getLink();
        final List slinks = syndFeed.getLinks();
        if (slinks != null) {
            for (final SyndLink syndLink : slinks) {
                final Link link = new Link();
                link.setRel(syndLink.getRel());
                link.setHref(syndLink.getHref());
                link.setHreflang(syndLink.getHreflang());
                link.setLength(syndLink.getLength());
                if (link.getRel() == null || "".equals(link.getRel().trim()) || "alternate".equals(syndLink.getRel())) {
                    alternateLinks.add(link);
                }
                else {
                    otherLinks.add(link);
                }
            }
        }
        if (alternateLinks.size() == 0 && syndFeed.getLink() != null) {
            final Link link2 = new Link();
            link2.setRel("alternate");
            link2.setHref(syndFeed.getLink());
            alternateLinks.add(link2);
        }
        if (alternateLinks.size() > 0) {
            aFeed.setAlternateLinks(alternateLinks);
        }
        if (otherLinks.size() > 0) {
            aFeed.setOtherLinks(otherLinks);
        }
        final List sCats = syndFeed.getCategories();
        final List aCats = new ArrayList();
        if (sCats != null) {
            for (final SyndCategory sCat : sCats) {
                final Category aCat = new Category();
                aCat.setTerm(sCat.getName());
                aCat.setScheme(sCat.getTaxonomyUri());
                aCats.add(aCat);
            }
        }
        if (aCats.size() > 0) {
            aFeed.setCategories(aCats);
        }
        final List authors = syndFeed.getAuthors();
        if (authors != null && authors.size() > 0) {
            aFeed.setAuthors(ConverterForAtom03.createAtomPersons(authors));
        }
        aFeed.setRights(syndFeed.getCopyright());
        aFeed.setUpdated(syndFeed.getPublishedDate());
        final List sEntries = syndFeed.getEntries();
        if (sEntries != null) {
            aFeed.setEntries(this.createAtomEntries(sEntries));
        }
        if (((List)syndFeed.getForeignMarkup()).size() > 0) {
            aFeed.setForeignMarkup(syndFeed.getForeignMarkup());
        }
        return aFeed;
    }
    
    protected SyndContent createSyndContent(final Content content) {
        final SyndContent sContent = new SyndContentImpl();
        sContent.setType(content.getType());
        sContent.setValue(content.getValue());
        return sContent;
    }
    
    protected List createAtomEntries(final List syndEntries) {
        final List atomEntries = new ArrayList();
        for (int i = 0; i < syndEntries.size(); ++i) {
            atomEntries.add(this.createAtomEntry(syndEntries.get(i)));
        }
        return atomEntries;
    }
    
    protected Content createAtomContent(final SyndContent sContent) {
        final Content content = new Content();
        content.setType(sContent.getType());
        content.setValue(sContent.getValue());
        return content;
    }
    
    protected List createAtomContents(final List syndContents) {
        final List atomContents = new ArrayList();
        for (int i = 0; i < syndContents.size(); ++i) {
            atomContents.add(this.createAtomContent(syndContents.get(i)));
        }
        return atomContents;
    }
    
    protected Entry createAtomEntry(final SyndEntry sEntry) {
        final Entry aEntry = new Entry();
        aEntry.setModules(ModuleUtils.cloneModules(sEntry.getModules()));
        aEntry.setId(sEntry.getUri());
        final SyndContent sTitle = sEntry.getTitleEx();
        if (sTitle != null) {
            final Content title = new Content();
            title.setType(sTitle.getType());
            title.setValue(sTitle.getValue());
            aEntry.setTitleEx(title);
        }
        final SyndContent sDescription = sEntry.getDescription();
        if (sDescription != null) {
            final Content summary = new Content();
            summary.setType(sDescription.getType());
            summary.setValue(sDescription.getValue());
            aEntry.setSummary(summary);
        }
        final List alternateLinks = new ArrayList();
        final List otherLinks = new ArrayList();
        final List slinks = sEntry.getLinks();
        if (slinks != null) {
            for (final SyndLink syndLink : slinks) {
                final Link link = new Link();
                link.setRel(syndLink.getRel());
                link.setHref(syndLink.getHref());
                link.setHreflang(syndLink.getHreflang());
                link.setLength(syndLink.getLength());
                link.setType(syndLink.getType());
                if (link.getRel() == null || "".equals(link.getRel().trim()) || "alternate".equals(syndLink.getRel())) {
                    alternateLinks.add(link);
                }
                else {
                    otherLinks.add(link);
                }
            }
        }
        if (alternateLinks.size() == 0 && sEntry.getLink() != null) {
            final Link link2 = new Link();
            link2.setRel("alternate");
            link2.setHref(sEntry.getLink());
            alternateLinks.add(link2);
        }
        if (alternateLinks.size() > 0) {
            aEntry.setAlternateLinks(alternateLinks);
        }
        if (otherLinks.size() > 0) {
            aEntry.setOtherLinks(otherLinks);
        }
        final List sCats = sEntry.getCategories();
        final List aCats = new ArrayList();
        if (sCats != null) {
            for (final SyndCategory sCat : sCats) {
                final Category aCat = new Category();
                aCat.setTerm(sCat.getName());
                aCat.setScheme(sCat.getTaxonomyUri());
                aCats.add(aCat);
            }
        }
        if (aCats.size() > 0) {
            aEntry.setCategories(aCats);
        }
        final List syndContents = sEntry.getContents();
        aEntry.setContents(this.createAtomContents(syndContents));
        List authors = sEntry.getAuthors();
        if (authors != null && authors.size() > 0) {
            aEntry.setAuthors(ConverterForAtom03.createAtomPersons(authors));
        }
        else if (sEntry.getAuthor() != null) {
            final Person person = new Person();
            person.setName(sEntry.getAuthor());
            authors = new ArrayList();
            authors.add(person);
            aEntry.setAuthors(authors);
        }
        aEntry.setPublished(sEntry.getPublishedDate());
        if (sEntry.getUpdatedDate() != null) {
            aEntry.setUpdated(sEntry.getUpdatedDate());
        }
        else {
            aEntry.setUpdated(sEntry.getPublishedDate());
        }
        if (((List)sEntry.getForeignMarkup()).size() > 0) {
            aEntry.setForeignMarkup(sEntry.getForeignMarkup());
        }
        return aEntry;
    }
}
