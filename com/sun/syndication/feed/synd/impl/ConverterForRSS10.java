// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import java.util.List;
import com.sun.syndication.feed.rss.Content;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.rss.Description;
import java.util.ArrayList;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.rss.Item;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.WireFeed;

public class ConverterForRSS10 extends ConverterForRSS090
{
    public ConverterForRSS10() {
        this("rss_1.0");
    }
    
    protected ConverterForRSS10(final String type) {
        super(type);
    }
    
    public void copyInto(final WireFeed feed, final SyndFeed syndFeed) {
        final Channel channel = (Channel)feed;
        super.copyInto(channel, syndFeed);
        if (channel.getUri() != null) {
            syndFeed.setUri(channel.getUri());
        }
        else {
            syndFeed.setUri(channel.getLink());
        }
    }
    
    protected SyndEntry createSyndEntry(final Item item) {
        final SyndEntry syndEntry = super.createSyndEntry(item);
        final Description desc = item.getDescription();
        if (desc != null) {
            final SyndContent descContent = new SyndContentImpl();
            descContent.setType(desc.getType());
            descContent.setValue(desc.getValue());
            syndEntry.setDescription(descContent);
        }
        final Content cont = item.getContent();
        if (cont != null) {
            final SyndContent contContent = new SyndContentImpl();
            contContent.setType(cont.getType());
            contContent.setValue(cont.getValue());
            final List contents = new ArrayList();
            contents.add(contContent);
            syndEntry.setContents(contents);
        }
        return syndEntry;
    }
    
    protected WireFeed createRealFeed(final String type, final SyndFeed syndFeed) {
        final Channel channel = (Channel)super.createRealFeed(type, syndFeed);
        if (syndFeed.getUri() != null) {
            channel.setUri(syndFeed.getUri());
        }
        else {
            channel.setUri(syndFeed.getLink());
        }
        return channel;
    }
    
    protected Item createRSSItem(final SyndEntry sEntry) {
        final Item item = super.createRSSItem(sEntry);
        final SyndContent desc = sEntry.getDescription();
        if (desc != null) {
            item.setDescription(this.createItemDescription(desc));
        }
        final List contents = sEntry.getContents();
        if (contents != null && contents.size() > 0) {
            item.setContent(this.createItemContent(contents.get(0)));
        }
        final String uri = sEntry.getUri();
        if (uri != null) {
            item.setUri(uri);
        }
        return item;
    }
    
    protected Description createItemDescription(final SyndContent sContent) {
        final Description desc = new Description();
        desc.setValue(sContent.getValue());
        desc.setType(sContent.getType());
        return desc;
    }
    
    protected Content createItemContent(final SyndContent sContent) {
        final Content cont = new Content();
        cont.setValue(sContent.getValue());
        cont.setType(sContent.getType());
        return cont;
    }
}
