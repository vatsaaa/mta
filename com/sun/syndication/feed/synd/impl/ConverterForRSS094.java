// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import com.sun.syndication.feed.synd.SyndPerson;
import com.sun.syndication.feed.rss.Guid;
import com.sun.syndication.feed.module.DCModule;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.rss.Item;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.WireFeed;

public class ConverterForRSS094 extends ConverterForRSS093
{
    public ConverterForRSS094() {
        this("rss_0.94");
    }
    
    protected ConverterForRSS094(final String type) {
        super(type);
    }
    
    public void copyInto(final WireFeed feed, final SyndFeed syndFeed) {
        final Channel channel = (Channel)feed;
        super.copyInto(channel, syndFeed);
        final List cats = channel.getCategories();
        if (cats.size() > 0) {
            final Set s = new HashSet();
            s.addAll(this.createSyndCategories(cats));
            s.addAll(syndFeed.getCategories());
            syndFeed.setCategories(new ArrayList(s));
        }
    }
    
    protected SyndEntry createSyndEntry(final Item item) {
        final SyndEntry syndEntry = super.createSyndEntry(item);
        final String author = item.getAuthor();
        if (author != null) {
            final List creators = ((DCModule)syndEntry.getModule("http://purl.org/dc/elements/1.1/")).getCreators();
            if (!creators.contains(author)) {
                final Set s = new HashSet();
                s.addAll(creators);
                s.add(author);
                creators.clear();
                creators.addAll(s);
            }
        }
        final Guid guid = item.getGuid();
        if (guid != null) {
            syndEntry.setUri(guid.getValue());
            if (item.getLink() == null && guid.isPermaLink()) {
                syndEntry.setLink(guid.getValue());
            }
        }
        else {
            syndEntry.setUri(item.getLink());
        }
        return syndEntry;
    }
    
    protected WireFeed createRealFeed(final String type, final SyndFeed syndFeed) {
        final Channel channel = (Channel)super.createRealFeed(type, syndFeed);
        final List cats = syndFeed.getCategories();
        if (cats.size() > 0) {
            channel.setCategories(this.createRSSCategories(cats));
        }
        return channel;
    }
    
    protected Item createRSSItem(final SyndEntry sEntry) {
        final Item item = super.createRSSItem(sEntry);
        if (sEntry.getAuthors() != null && sEntry.getAuthors().size() > 0) {
            final SyndPerson author = sEntry.getAuthors().get(0);
            item.setAuthor(author.getEmail());
        }
        Guid guid = null;
        final String uri = sEntry.getUri();
        if (uri != null) {
            guid = new Guid();
            guid.setPermaLink(false);
            guid.setValue(uri);
        }
        else {
            final String link = sEntry.getLink();
            if (link != null) {
                guid = new Guid();
                guid.setPermaLink(true);
                guid.setValue(link);
            }
        }
        item.setGuid(guid);
        return item;
    }
}
