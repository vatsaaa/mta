// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd.impl;

import java.util.Date;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.rss.Item;

public class ConverterForRSS093 extends ConverterForRSS092
{
    public ConverterForRSS093() {
        this("rss_0.93");
    }
    
    protected ConverterForRSS093(final String type) {
        super(type);
    }
    
    protected SyndEntry createSyndEntry(final Item item) {
        final SyndEntry syndEntry = super.createSyndEntry(item);
        final Date pubDate = item.getPubDate();
        if (pubDate != null) {
            syndEntry.setPublishedDate(pubDate);
        }
        return syndEntry;
    }
    
    protected Item createRSSItem(final SyndEntry sEntry) {
        final Item item = super.createRSSItem(sEntry);
        item.setPubDate(sEntry.getPublishedDate());
        return item;
    }
}
