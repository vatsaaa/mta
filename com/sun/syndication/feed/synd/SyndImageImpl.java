// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import java.util.Map;
import java.util.Collections;
import java.util.HashMap;
import com.sun.syndication.feed.impl.CopyFromHelper;
import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class SyndImageImpl implements Serializable, SyndImage
{
    private ObjectBean _objBean;
    private String _title;
    private String _url;
    private String _link;
    private String _description;
    private static final CopyFromHelper COPY_FROM_HELPER;
    
    public SyndImageImpl() {
        this._objBean = new ObjectBean(SyndImage.class, this);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getTitle() {
        return this._title;
    }
    
    public void setTitle(final String title) {
        this._title = title;
    }
    
    public String getUrl() {
        return this._url;
    }
    
    public void setUrl(final String url) {
        this._url = url;
    }
    
    public String getLink() {
        return this._link;
    }
    
    public void setLink(final String link) {
        this._link = link;
    }
    
    public String getDescription() {
        return this._description;
    }
    
    public void setDescription(final String description) {
        this._description = description;
    }
    
    public Class getInterface() {
        return SyndImage.class;
    }
    
    public void copyFrom(final Object syndImage) {
        SyndImageImpl.COPY_FROM_HELPER.copy(this, syndImage);
    }
    
    static {
        final Map basePropInterfaceMap = new HashMap();
        basePropInterfaceMap.put("title", String.class);
        basePropInterfaceMap.put("url", String.class);
        basePropInterfaceMap.put("link", String.class);
        basePropInterfaceMap.put("description", String.class);
        final Map basePropClassImplMap = Collections.EMPTY_MAP;
        COPY_FROM_HELPER = new CopyFromHelper(SyndImage.class, basePropInterfaceMap, basePropClassImplMap);
    }
}
