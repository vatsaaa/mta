// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

public interface SyndPerson extends Cloneable
{
    String getName();
    
    void setName(final String p0);
    
    String getUri();
    
    void setUri(final String p0);
    
    String getEmail();
    
    void setEmail(final String p0);
    
    Object clone() throws CloneNotSupportedException;
}
