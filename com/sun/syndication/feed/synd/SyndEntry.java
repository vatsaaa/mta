// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import com.sun.syndication.feed.module.Module;
import java.util.Date;
import java.util.List;
import com.sun.syndication.feed.module.Extendable;
import com.sun.syndication.feed.CopyFrom;

public interface SyndEntry extends Cloneable, CopyFrom, Extendable
{
    String getUri();
    
    void setUri(final String p0);
    
    String getTitle();
    
    void setTitle(final String p0);
    
    SyndContent getTitleEx();
    
    void setTitleEx(final SyndContent p0);
    
    String getLink();
    
    void setLink(final String p0);
    
    List getLinks();
    
    void setLinks(final List p0);
    
    SyndContent getDescription();
    
    void setDescription(final SyndContent p0);
    
    List getContents();
    
    void setContents(final List p0);
    
    List getEnclosures();
    
    void setEnclosures(final List p0);
    
    Date getPublishedDate();
    
    void setPublishedDate(final Date p0);
    
    Date getUpdatedDate();
    
    void setUpdatedDate(final Date p0);
    
    List getAuthors();
    
    void setAuthors(final List p0);
    
    String getAuthor();
    
    void setAuthor(final String p0);
    
    List getContributors();
    
    void setContributors(final List p0);
    
    List getCategories();
    
    void setCategories(final List p0);
    
    Module getModule(final String p0);
    
    List getModules();
    
    void setModules(final List p0);
    
    Object getForeignMarkup();
    
    void setForeignMarkup(final Object p0);
    
    Object clone() throws CloneNotSupportedException;
}
