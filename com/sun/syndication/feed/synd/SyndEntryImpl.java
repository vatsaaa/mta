// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import com.sun.syndication.feed.module.SyModuleImpl;
import com.sun.syndication.feed.module.SyModule;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.HashSet;
import com.sun.syndication.feed.module.DCModule;
import com.sun.syndication.feed.module.Module;
import com.sun.syndication.feed.module.DCModuleImpl;
import com.sun.syndication.feed.module.impl.ModuleUtils;
import com.sun.syndication.feed.synd.impl.URINormalizer;
import java.util.ArrayList;
import com.sun.syndication.feed.impl.CopyFromHelper;
import java.util.Set;
import java.util.List;
import java.util.Date;
import com.sun.syndication.feed.impl.ObjectBean;
import java.io.Serializable;

public class SyndEntryImpl implements Serializable, SyndEntry
{
    private ObjectBean _objBean;
    private String _uri;
    private String _link;
    private Date _updatedDate;
    private SyndContent _title;
    private SyndContent _description;
    private List _links;
    private List _contents;
    private List _modules;
    private List _enclosures;
    private List _authors;
    private List _contributors;
    private List _foreignMarkup;
    private List _categories;
    private static final Set IGNORE_PROPERTIES;
    public static final Set CONVENIENCE_PROPERTIES;
    private static final CopyFromHelper COPY_FROM_HELPER;
    
    protected SyndEntryImpl(final Class beanClass, final Set convenienceProperties) {
        this._categories = new ArrayList();
        this._objBean = new ObjectBean(beanClass, this, convenienceProperties);
    }
    
    public SyndEntryImpl() {
        this(SyndEntry.class, SyndEntryImpl.IGNORE_PROPERTIES);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public boolean equals(final Object other) {
        final Object fm = this.getForeignMarkup();
        this.setForeignMarkup(((SyndEntryImpl)other).getForeignMarkup());
        final boolean ret = this._objBean.equals(other);
        this.setForeignMarkup(fm);
        return ret;
    }
    
    public int hashCode() {
        return this._objBean.hashCode();
    }
    
    public String toString() {
        return this._objBean.toString();
    }
    
    public String getUri() {
        return this._uri;
    }
    
    public void setUri(final String uri) {
        this._uri = URINormalizer.normalize(uri);
    }
    
    public String getTitle() {
        if (this._title != null) {
            return this._title.getValue();
        }
        return null;
    }
    
    public void setTitle(final String title) {
        if (this._title == null) {
            this._title = new SyndContentImpl();
        }
        this._title.setValue(title);
    }
    
    public SyndContent getTitleEx() {
        return this._title;
    }
    
    public void setTitleEx(final SyndContent title) {
        this._title = title;
    }
    
    public String getLink() {
        return this._link;
    }
    
    public void setLink(final String link) {
        this._link = link;
    }
    
    public SyndContent getDescription() {
        return this._description;
    }
    
    public void setDescription(final SyndContent description) {
        this._description = description;
    }
    
    public List getContents() {
        return (this._contents == null) ? (this._contents = new ArrayList()) : this._contents;
    }
    
    public void setContents(final List contents) {
        this._contents = contents;
    }
    
    public List getEnclosures() {
        return (this._enclosures == null) ? (this._enclosures = new ArrayList()) : this._enclosures;
    }
    
    public void setEnclosures(final List enclosures) {
        this._enclosures = enclosures;
    }
    
    public Date getPublishedDate() {
        return this.getDCModule().getDate();
    }
    
    public void setPublishedDate(final Date publishedDate) {
        this.getDCModule().setDate(publishedDate);
    }
    
    public List getCategories() {
        return this._categories;
    }
    
    public void setCategories(final List categories) {
        this._categories = categories;
    }
    
    public List getModules() {
        if (this._modules == null) {
            this._modules = new ArrayList();
        }
        if (ModuleUtils.getModule(this._modules, "http://purl.org/dc/elements/1.1/") == null) {
            this._modules.add(new DCModuleImpl());
        }
        return this._modules;
    }
    
    public void setModules(final List modules) {
        this._modules = modules;
    }
    
    public Module getModule(final String uri) {
        return ModuleUtils.getModule(this.getModules(), uri);
    }
    
    private DCModule getDCModule() {
        return (DCModule)this.getModule("http://purl.org/dc/elements/1.1/");
    }
    
    public Class getInterface() {
        return SyndEntry.class;
    }
    
    public void copyFrom(final Object obj) {
        SyndEntryImpl.COPY_FROM_HELPER.copy(this, obj);
    }
    
    public List getLinks() {
        return (this._links == null) ? (this._links = new ArrayList()) : this._links;
    }
    
    public void setLinks(final List links) {
        this._links = links;
    }
    
    public Date getUpdatedDate() {
        return this._updatedDate;
    }
    
    public void setUpdatedDate(final Date updatedDate) {
        this._updatedDate = updatedDate;
    }
    
    public List getAuthors() {
        return this._authors;
    }
    
    public void setAuthors(final List authors) {
        this._authors = authors;
    }
    
    public String getAuthor() {
        String author;
        if (this._authors != null && this._authors.size() > 0) {
            author = this._authors.get(0).getName();
        }
        else {
            author = this.getDCModule().getCreator();
        }
        if (author == null) {
            author = "";
        }
        return author;
    }
    
    public void setAuthor(final String author) {
        final DCModule dcModule = this.getDCModule();
        final String currentValue = dcModule.getCreator();
        if (currentValue == null || currentValue.length() == 0) {
            this.getDCModule().setCreator(author);
        }
    }
    
    public List getContributors() {
        return this._contributors;
    }
    
    public void setContributors(final List contributors) {
        this._contributors = contributors;
    }
    
    public Object getForeignMarkup() {
        return (this._foreignMarkup == null) ? (this._foreignMarkup = new ArrayList()) : this._foreignMarkup;
    }
    
    public void setForeignMarkup(final Object foreignMarkup) {
        this._foreignMarkup = (List)foreignMarkup;
    }
    
    static {
        IGNORE_PROPERTIES = new HashSet();
        CONVENIENCE_PROPERTIES = Collections.unmodifiableSet((Set<?>)SyndEntryImpl.IGNORE_PROPERTIES);
        SyndEntryImpl.IGNORE_PROPERTIES.add("publishedDate");
        SyndEntryImpl.IGNORE_PROPERTIES.add("author");
        final Map basePropInterfaceMap = new HashMap();
        basePropInterfaceMap.put("uri", String.class);
        basePropInterfaceMap.put("title", String.class);
        basePropInterfaceMap.put("link", String.class);
        basePropInterfaceMap.put("uri", String.class);
        basePropInterfaceMap.put("description", SyndContent.class);
        basePropInterfaceMap.put("contents", SyndContent.class);
        basePropInterfaceMap.put("enclosures", SyndEnclosure.class);
        basePropInterfaceMap.put("modules", Module.class);
        final Map basePropClassImplMap = new HashMap();
        basePropClassImplMap.put(SyndContent.class, SyndContentImpl.class);
        basePropClassImplMap.put(SyndEnclosure.class, SyndEnclosureImpl.class);
        basePropClassImplMap.put(DCModule.class, DCModuleImpl.class);
        basePropClassImplMap.put(SyModule.class, SyModuleImpl.class);
        COPY_FROM_HELPER = new CopyFromHelper(SyndEntry.class, basePropInterfaceMap, basePropClassImplMap);
    }
}
