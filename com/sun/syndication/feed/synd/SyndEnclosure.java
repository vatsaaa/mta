// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import com.sun.syndication.feed.CopyFrom;

public interface SyndEnclosure extends Cloneable, CopyFrom
{
    String getUrl();
    
    void setUrl(final String p0);
    
    long getLength();
    
    void setLength(final long p0);
    
    String getType();
    
    void setType(final String p0);
}
