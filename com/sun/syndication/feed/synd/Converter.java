// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

import com.sun.syndication.feed.WireFeed;

public interface Converter
{
    String getType();
    
    void copyInto(final WireFeed p0, final SyndFeed p1);
    
    WireFeed createRealFeed(final SyndFeed p0);
}
