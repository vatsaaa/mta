// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.synd;

public interface SyndLink
{
    Object clone() throws CloneNotSupportedException;
    
    boolean equals(final Object p0);
    
    int hashCode();
    
    String toString();
    
    String getRel();
    
    void setRel(final String p0);
    
    String getType();
    
    void setType(final String p0);
    
    String getHref();
    
    void setHref(final String p0);
    
    String getTitle();
    
    void setTitle(final String p0);
    
    String getHreflang();
    
    void setHreflang(final String p0);
    
    long getLength();
    
    void setLength(final long p0);
}
