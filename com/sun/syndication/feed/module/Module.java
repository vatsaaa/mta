// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module;

import java.io.Serializable;
import com.sun.syndication.feed.CopyFrom;

public interface Module extends Cloneable, CopyFrom, Serializable
{
    String getUri();
    
    Object clone() throws CloneNotSupportedException;
}
