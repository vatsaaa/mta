// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module;

import java.util.Map;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import com.sun.syndication.feed.impl.CopyFromHelper;
import java.util.Date;
import java.util.Set;

public class SyModuleImpl extends ModuleImpl implements SyModule
{
    private static final Set PERIODS;
    private String _updatePeriod;
    private int _updateFrequency;
    private Date _updateBase;
    private static final CopyFromHelper COPY_FROM_HELPER;
    
    public SyModuleImpl() {
        super(SyModule.class, "http://purl.org/rss/1.0/modules/syndication/");
    }
    
    public String getUpdatePeriod() {
        return this._updatePeriod;
    }
    
    public void setUpdatePeriod(final String updatePeriod) {
        if (!SyModuleImpl.PERIODS.contains(updatePeriod)) {
            throw new IllegalArgumentException("Invalid period [" + updatePeriod + "]");
        }
        this._updatePeriod = updatePeriod;
    }
    
    public int getUpdateFrequency() {
        return this._updateFrequency;
    }
    
    public void setUpdateFrequency(final int updateFrequency) {
        this._updateFrequency = updateFrequency;
    }
    
    public Date getUpdateBase() {
        return this._updateBase;
    }
    
    public void setUpdateBase(final Date updateBase) {
        this._updateBase = updateBase;
    }
    
    public Class getInterface() {
        return SyModule.class;
    }
    
    public void copyFrom(final Object obj) {
        SyModuleImpl.COPY_FROM_HELPER.copy(this, obj);
    }
    
    static {
        (PERIODS = new HashSet()).add(SyModuleImpl.HOURLY);
        SyModuleImpl.PERIODS.add(SyModuleImpl.DAILY);
        SyModuleImpl.PERIODS.add(SyModuleImpl.WEEKLY);
        SyModuleImpl.PERIODS.add(SyModuleImpl.MONTHLY);
        SyModuleImpl.PERIODS.add(SyModuleImpl.YEARLY);
        final Map basePropInterfaceMap = new HashMap();
        basePropInterfaceMap.put("updatePeriod", String.class);
        basePropInterfaceMap.put("updateFrequency", Integer.TYPE);
        basePropInterfaceMap.put("updateBase", Date.class);
        final Map basePropClassImplMap = Collections.EMPTY_MAP;
        COPY_FROM_HELPER = new CopyFromHelper(SyModule.class, basePropInterfaceMap, basePropClassImplMap);
    }
}
