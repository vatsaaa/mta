// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module;

import java.util.Date;

public interface SyModule extends Module
{
    public static final String URI = "http://purl.org/rss/1.0/modules/syndication/";
    public static final String HOURLY = new String("hourly");
    public static final String DAILY = new String("daily");
    public static final String WEEKLY = new String("weekly");
    public static final String MONTHLY = new String("monthly");
    public static final String YEARLY = new String("yearly");
    
    String getUpdatePeriod();
    
    void setUpdatePeriod(final String p0);
    
    int getUpdateFrequency();
    
    void setUpdateFrequency(final int p0);
    
    Date getUpdateBase();
    
    void setUpdateBase(final Date p0);
}
