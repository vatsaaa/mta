// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.HashSet;
import java.util.Date;
import java.util.ArrayList;
import com.sun.syndication.feed.impl.CopyFromHelper;
import java.util.Set;
import java.util.List;
import com.sun.syndication.feed.impl.ObjectBean;

public class DCModuleImpl extends ModuleImpl implements DCModule
{
    private ObjectBean _objBean;
    private List _title;
    private List _creator;
    private List _subject;
    private List _description;
    private List _publisher;
    private List _contributors;
    private List _date;
    private List _type;
    private List _format;
    private List _identifier;
    private List _source;
    private List _language;
    private List _relation;
    private List _coverage;
    private List _rights;
    private static final Set IGNORE_PROPERTIES;
    public static final Set CONVENIENCE_PROPERTIES;
    private static final CopyFromHelper COPY_FROM_HELPER;
    
    public DCModuleImpl() {
        super(DCModule.class, "http://purl.org/dc/elements/1.1/");
        this._objBean = new ObjectBean(DCModule.class, this, DCModuleImpl.CONVENIENCE_PROPERTIES);
    }
    
    public List getTitles() {
        return (this._title == null) ? (this._title = new ArrayList()) : this._title;
    }
    
    public void setTitles(final List titles) {
        this._title = titles;
    }
    
    public String getTitle() {
        return (this._title != null && this._title.size() > 0) ? this._title.get(0) : null;
    }
    
    public void setTitle(final String title) {
        (this._title = new ArrayList()).add(title);
    }
    
    public List getCreators() {
        return (this._creator == null) ? (this._creator = new ArrayList()) : this._creator;
    }
    
    public void setCreators(final List creators) {
        this._creator = creators;
    }
    
    public String getCreator() {
        return (this._creator != null && this._creator.size() > 0) ? this._creator.get(0) : null;
    }
    
    public void setCreator(final String creator) {
        (this._creator = new ArrayList()).add(creator);
    }
    
    public List getSubjects() {
        return (this._subject == null) ? (this._subject = new ArrayList()) : this._subject;
    }
    
    public void setSubjects(final List subjects) {
        this._subject = subjects;
    }
    
    public DCSubject getSubject() {
        return (this._subject != null && this._subject.size() > 0) ? this._subject.get(0) : null;
    }
    
    public void setSubject(final DCSubject subject) {
        (this._subject = new ArrayList()).add(subject);
    }
    
    public List getDescriptions() {
        return (this._description == null) ? (this._description = new ArrayList()) : this._description;
    }
    
    public void setDescriptions(final List descriptions) {
        this._description = descriptions;
    }
    
    public String getDescription() {
        return (this._description != null && this._description.size() > 0) ? this._description.get(0) : null;
    }
    
    public void setDescription(final String description) {
        (this._description = new ArrayList()).add(description);
    }
    
    public List getPublishers() {
        return (this._publisher == null) ? (this._publisher = new ArrayList()) : this._publisher;
    }
    
    public void setPublishers(final List publishers) {
        this._publisher = publishers;
    }
    
    public String getPublisher() {
        return (this._publisher != null && this._publisher.size() > 0) ? this._publisher.get(0) : null;
    }
    
    public void setPublisher(final String publisher) {
        (this._publisher = new ArrayList()).add(publisher);
    }
    
    public List getContributors() {
        return (this._contributors == null) ? (this._contributors = new ArrayList()) : this._contributors;
    }
    
    public void setContributors(final List contributors) {
        this._contributors = contributors;
    }
    
    public String getContributor() {
        return (this._contributors != null && this._contributors.size() > 0) ? this._contributors.get(0) : null;
    }
    
    public void setContributor(final String contributor) {
        (this._contributors = new ArrayList()).add(contributor);
    }
    
    public List getDates() {
        return (this._date == null) ? (this._date = new ArrayList()) : this._date;
    }
    
    public void setDates(final List dates) {
        this._date = dates;
    }
    
    public Date getDate() {
        return (this._date != null && this._date.size() > 0) ? this._date.get(0) : null;
    }
    
    public void setDate(final Date date) {
        (this._date = new ArrayList()).add(date);
    }
    
    public List getTypes() {
        return (this._type == null) ? (this._type = new ArrayList()) : this._type;
    }
    
    public void setTypes(final List types) {
        this._type = types;
    }
    
    public String getType() {
        return (this._type != null && this._type.size() > 0) ? this._type.get(0) : null;
    }
    
    public void setType(final String type) {
        (this._type = new ArrayList()).add(type);
    }
    
    public List getFormats() {
        return (this._format == null) ? (this._format = new ArrayList()) : this._format;
    }
    
    public void setFormats(final List formats) {
        this._format = formats;
    }
    
    public String getFormat() {
        return (this._format != null && this._format.size() > 0) ? this._format.get(0) : null;
    }
    
    public void setFormat(final String format) {
        (this._format = new ArrayList()).add(format);
    }
    
    public List getIdentifiers() {
        return (this._identifier == null) ? (this._identifier = new ArrayList()) : this._identifier;
    }
    
    public void setIdentifiers(final List identifiers) {
        this._identifier = identifiers;
    }
    
    public String getIdentifier() {
        return (this._identifier != null && this._identifier.size() > 0) ? this._identifier.get(0) : null;
    }
    
    public void setIdentifier(final String identifier) {
        (this._identifier = new ArrayList()).add(identifier);
    }
    
    public List getSources() {
        return (this._source == null) ? (this._source = new ArrayList()) : this._source;
    }
    
    public void setSources(final List sources) {
        this._source = sources;
    }
    
    public String getSource() {
        return (this._source != null && this._source.size() > 0) ? this._source.get(0) : null;
    }
    
    public void setSource(final String source) {
        (this._source = new ArrayList()).add(source);
    }
    
    public List getLanguages() {
        return (this._language == null) ? (this._language = new ArrayList()) : this._language;
    }
    
    public void setLanguages(final List languages) {
        this._language = languages;
    }
    
    public String getLanguage() {
        return (this._language != null && this._language.size() > 0) ? this._language.get(0) : null;
    }
    
    public void setLanguage(final String language) {
        (this._language = new ArrayList()).add(language);
    }
    
    public List getRelations() {
        return (this._relation == null) ? (this._relation = new ArrayList()) : this._relation;
    }
    
    public void setRelations(final List relations) {
        this._relation = relations;
    }
    
    public String getRelation() {
        return (this._relation != null && this._relation.size() > 0) ? this._relation.get(0) : null;
    }
    
    public void setRelation(final String relation) {
        (this._relation = new ArrayList()).add(relation);
    }
    
    public List getCoverages() {
        return (this._coverage == null) ? (this._coverage = new ArrayList()) : this._coverage;
    }
    
    public void setCoverages(final List coverages) {
        this._coverage = coverages;
    }
    
    public String getCoverage() {
        return (this._coverage != null && this._coverage.size() > 0) ? this._coverage.get(0) : null;
    }
    
    public void setCoverage(final String coverage) {
        (this._coverage = new ArrayList()).add(coverage);
    }
    
    public List getRightsList() {
        return (this._rights == null) ? (this._rights = new ArrayList()) : this._rights;
    }
    
    public void setRightsList(final List rights) {
        this._rights = rights;
    }
    
    public String getRights() {
        return (this._rights != null && this._rights.size() > 0) ? this._rights.get(0) : null;
    }
    
    public void setRights(final String rights) {
        (this._rights = new ArrayList()).add(rights);
    }
    
    public final Object clone() throws CloneNotSupportedException {
        return this._objBean.clone();
    }
    
    public final boolean equals(final Object other) {
        return this._objBean.equals(other);
    }
    
    public final int hashCode() {
        return this._objBean.hashCode();
    }
    
    public final String toString() {
        return this._objBean.toString();
    }
    
    public final Class getInterface() {
        return DCModule.class;
    }
    
    public final void copyFrom(final Object obj) {
        DCModuleImpl.COPY_FROM_HELPER.copy(this, obj);
    }
    
    static {
        IGNORE_PROPERTIES = new HashSet();
        CONVENIENCE_PROPERTIES = Collections.unmodifiableSet((Set<?>)DCModuleImpl.IGNORE_PROPERTIES);
        DCModuleImpl.IGNORE_PROPERTIES.add("title");
        DCModuleImpl.IGNORE_PROPERTIES.add("creator");
        DCModuleImpl.IGNORE_PROPERTIES.add("subject");
        DCModuleImpl.IGNORE_PROPERTIES.add("description");
        DCModuleImpl.IGNORE_PROPERTIES.add("publisher");
        DCModuleImpl.IGNORE_PROPERTIES.add("contributor");
        DCModuleImpl.IGNORE_PROPERTIES.add("date");
        DCModuleImpl.IGNORE_PROPERTIES.add("type");
        DCModuleImpl.IGNORE_PROPERTIES.add("format");
        DCModuleImpl.IGNORE_PROPERTIES.add("identifier");
        DCModuleImpl.IGNORE_PROPERTIES.add("source");
        DCModuleImpl.IGNORE_PROPERTIES.add("language");
        DCModuleImpl.IGNORE_PROPERTIES.add("relation");
        DCModuleImpl.IGNORE_PROPERTIES.add("coverage");
        DCModuleImpl.IGNORE_PROPERTIES.add("rights");
        final Map basePropInterfaceMap = new HashMap();
        basePropInterfaceMap.put("titles", String.class);
        basePropInterfaceMap.put("creators", String.class);
        basePropInterfaceMap.put("subjects", DCSubject.class);
        basePropInterfaceMap.put("descriptions", String.class);
        basePropInterfaceMap.put("publishers", String.class);
        basePropInterfaceMap.put("contributors", String.class);
        basePropInterfaceMap.put("dates", Date.class);
        basePropInterfaceMap.put("types", String.class);
        basePropInterfaceMap.put("formats", String.class);
        basePropInterfaceMap.put("identifiers", String.class);
        basePropInterfaceMap.put("sources", String.class);
        basePropInterfaceMap.put("languages", String.class);
        basePropInterfaceMap.put("relations", String.class);
        basePropInterfaceMap.put("coverages", String.class);
        basePropInterfaceMap.put("rightsList", String.class);
        final Map basePropClassImplMap = new HashMap();
        basePropClassImplMap.put(DCSubject.class, DCSubjectImpl.class);
        COPY_FROM_HELPER = new CopyFromHelper(DCModule.class, basePropInterfaceMap, basePropClassImplMap);
    }
}
