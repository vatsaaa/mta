// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module;

import java.util.Date;
import java.util.List;
import com.sun.syndication.feed.CopyFrom;

public interface DCModule extends Module, CopyFrom
{
    public static final String URI = "http://purl.org/dc/elements/1.1/";
    
    List getTitles();
    
    void setTitles(final List p0);
    
    String getTitle();
    
    void setTitle(final String p0);
    
    List getCreators();
    
    void setCreators(final List p0);
    
    String getCreator();
    
    void setCreator(final String p0);
    
    List getSubjects();
    
    void setSubjects(final List p0);
    
    DCSubject getSubject();
    
    void setSubject(final DCSubject p0);
    
    List getDescriptions();
    
    void setDescriptions(final List p0);
    
    String getDescription();
    
    void setDescription(final String p0);
    
    List getPublishers();
    
    void setPublishers(final List p0);
    
    String getPublisher();
    
    void setPublisher(final String p0);
    
    List getContributors();
    
    void setContributors(final List p0);
    
    String getContributor();
    
    void setContributor(final String p0);
    
    List getDates();
    
    void setDates(final List p0);
    
    Date getDate();
    
    void setDate(final Date p0);
    
    List getTypes();
    
    void setTypes(final List p0);
    
    String getType();
    
    void setType(final String p0);
    
    List getFormats();
    
    void setFormats(final List p0);
    
    String getFormat();
    
    void setFormat(final String p0);
    
    List getIdentifiers();
    
    void setIdentifiers(final List p0);
    
    String getIdentifier();
    
    void setIdentifier(final String p0);
    
    List getSources();
    
    void setSources(final List p0);
    
    String getSource();
    
    void setSource(final String p0);
    
    List getLanguages();
    
    void setLanguages(final List p0);
    
    String getLanguage();
    
    void setLanguage(final String p0);
    
    List getRelations();
    
    void setRelations(final List p0);
    
    String getRelation();
    
    void setRelation(final String p0);
    
    List getCoverages();
    
    void setCoverages(final List p0);
    
    String getCoverage();
    
    void setCoverage(final String p0);
    
    List getRightsList();
    
    void setRightsList(final List p0);
    
    String getRights();
    
    void setRights(final String p0);
}
