// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module;

import java.util.List;

public interface Extendable
{
    Module getModule(final String p0);
    
    List getModules();
    
    void setModules(final List p0);
}
