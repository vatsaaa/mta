// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module.impl;

import com.sun.syndication.feed.module.Module;
import java.util.ArrayList;
import java.util.List;

public class ModuleUtils
{
    public static List cloneModules(final List modules) {
        List cModules = null;
        if (modules != null) {
            cModules = new ArrayList();
            for (int i = 0; i < modules.size(); ++i) {
                final Module module = modules.get(i);
                try {
                    final Object c = module.clone();
                    cModules.add(c);
                }
                catch (Exception ex) {
                    throw new RuntimeException("Cloning modules", ex);
                }
            }
        }
        return cModules;
    }
    
    public static Module getModule(final List modules, final String uri) {
        Module module = null;
        for (int i = 0; module == null && modules != null && i < modules.size(); ++i) {
            module = modules.get(i);
            if (!module.getUri().equals(uri)) {
                module = null;
            }
        }
        return module;
    }
}
