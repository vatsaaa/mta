// 
// Decompiled by Procyon v0.5.36
// 

package com.sun.syndication.feed.module;

import com.sun.syndication.feed.CopyFrom;

public interface DCSubject extends Cloneable, CopyFrom
{
    String getTaxonomyUri();
    
    void setTaxonomyUri(final String p0);
    
    String getValue();
    
    void setValue(final String p0);
    
    Object clone() throws CloneNotSupportedException;
}
