// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

public class XMPException extends Exception
{
    private int errorCode;
    
    public XMPException(final String message, final int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
    
    public XMPException(final String message, final int errorCode, final Throwable t) {
        super(message, t);
        this.errorCode = errorCode;
    }
    
    public int getErrorCode() {
        return this.errorCode;
    }
}
