// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

public interface XMPVersionInfo
{
    int getMajor();
    
    int getMinor();
    
    int getMicro();
    
    int getBuild();
    
    boolean isDebug();
    
    String getMessage();
}
