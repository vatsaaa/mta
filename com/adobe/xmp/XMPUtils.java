// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import com.adobe.xmp.impl.Base64;
import com.adobe.xmp.impl.ISO8601Converter;
import com.adobe.xmp.options.PropertyOptions;
import com.adobe.xmp.impl.XMPUtilsImpl;

public class XMPUtils
{
    private XMPUtils() {
    }
    
    public static String catenateArrayItems(final XMPMeta xmp, final String schemaNS, final String arrayName, final String separator, final String quotes, final boolean allowCommas) throws XMPException {
        return XMPUtilsImpl.catenateArrayItems(xmp, schemaNS, arrayName, separator, quotes, allowCommas);
    }
    
    public static void separateArrayItems(final XMPMeta xmp, final String schemaNS, final String arrayName, final String catedStr, final PropertyOptions arrayOptions, final boolean preserveCommas) throws XMPException {
        XMPUtilsImpl.separateArrayItems(xmp, schemaNS, arrayName, catedStr, arrayOptions, preserveCommas);
    }
    
    public static void removeProperties(final XMPMeta xmp, final String schemaNS, final String propName, final boolean doAllProperties, final boolean includeAliases) throws XMPException {
        XMPUtilsImpl.removeProperties(xmp, schemaNS, propName, doAllProperties, includeAliases);
    }
    
    public static void appendProperties(final XMPMeta source, final XMPMeta dest, final boolean doAllProperties, final boolean replaceOldValues) throws XMPException {
        appendProperties(source, dest, doAllProperties, replaceOldValues, false);
    }
    
    public static void appendProperties(final XMPMeta source, final XMPMeta dest, final boolean doAllProperties, final boolean replaceOldValues, final boolean deleteEmptyValues) throws XMPException {
        XMPUtilsImpl.appendProperties(source, dest, doAllProperties, replaceOldValues, deleteEmptyValues);
    }
    
    public static boolean convertToBoolean(String value) throws XMPException {
        if (value == null || value.length() == 0) {
            throw new XMPException("Empty convert-string", 5);
        }
        value = value.toLowerCase();
        try {
            return Integer.parseInt(value) != 0;
        }
        catch (NumberFormatException e) {
            return "true".equals(value) || "t".equals(value) || "on".equals(value) || "yes".equals(value);
        }
    }
    
    public static String convertFromBoolean(final boolean value) {
        return value ? "True" : "False";
    }
    
    public static int convertToInteger(final String rawValue) throws XMPException {
        try {
            if (rawValue == null || rawValue.length() == 0) {
                throw new XMPException("Empty convert-string", 5);
            }
            if (rawValue.startsWith("0x")) {
                return Integer.parseInt(rawValue.substring(2), 16);
            }
            return Integer.parseInt(rawValue);
        }
        catch (NumberFormatException e) {
            throw new XMPException("Invalid integer string", 5);
        }
    }
    
    public static String convertFromInteger(final int value) {
        return String.valueOf(value);
    }
    
    public static long convertToLong(final String rawValue) throws XMPException {
        try {
            if (rawValue == null || rawValue.length() == 0) {
                throw new XMPException("Empty convert-string", 5);
            }
            if (rawValue.startsWith("0x")) {
                return Long.parseLong(rawValue.substring(2), 16);
            }
            return Long.parseLong(rawValue);
        }
        catch (NumberFormatException e) {
            throw new XMPException("Invalid long string", 5);
        }
    }
    
    public static String convertFromLong(final long value) {
        return String.valueOf(value);
    }
    
    public static double convertToDouble(final String rawValue) throws XMPException {
        try {
            if (rawValue == null || rawValue.length() == 0) {
                throw new XMPException("Empty convert-string", 5);
            }
            return Double.parseDouble(rawValue);
        }
        catch (NumberFormatException e) {
            throw new XMPException("Invalid double string", 5);
        }
    }
    
    public static String convertFromDouble(final double value) {
        return String.valueOf(value);
    }
    
    public static XMPDateTime convertToDate(final String rawValue) throws XMPException {
        if (rawValue == null || rawValue.length() == 0) {
            throw new XMPException("Empty convert-string", 5);
        }
        return ISO8601Converter.parse(rawValue);
    }
    
    public static String convertFromDate(final XMPDateTime value) {
        return ISO8601Converter.render(value);
    }
    
    public static String encodeBase64(final byte[] buffer) {
        return new String(Base64.encode(buffer));
    }
    
    public static byte[] decodeBase64(final String base64String) throws XMPException {
        try {
            return Base64.decode(base64String.getBytes());
        }
        catch (Throwable e) {
            throw new XMPException("Invalid base64 string", 5, e);
        }
    }
}
