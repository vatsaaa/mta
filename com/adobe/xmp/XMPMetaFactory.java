// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import com.adobe.xmp.impl.XMPSchemaRegistryImpl;
import com.adobe.xmp.impl.XMPSerializerHelper;
import com.adobe.xmp.options.SerializeOptions;
import java.io.OutputStream;
import com.adobe.xmp.impl.XMPMetaParser;
import com.adobe.xmp.options.ParseOptions;
import java.io.InputStream;
import com.adobe.xmp.impl.XMPMetaImpl;

public final class XMPMetaFactory
{
    private static XMPSchemaRegistry schema;
    private static XMPVersionInfo versionInfo;
    
    private XMPMetaFactory() {
    }
    
    public static XMPSchemaRegistry getSchemaRegistry() {
        return XMPMetaFactory.schema;
    }
    
    public static XMPMeta create() {
        return new XMPMetaImpl();
    }
    
    public static XMPMeta parse(final InputStream in) throws XMPException {
        return parse(in, null);
    }
    
    public static XMPMeta parse(final InputStream in, final ParseOptions options) throws XMPException {
        return XMPMetaParser.parse(in, options);
    }
    
    public static XMPMeta parseFromString(final String packet) throws XMPException {
        return parseFromString(packet, null);
    }
    
    public static XMPMeta parseFromString(final String packet, final ParseOptions options) throws XMPException {
        return XMPMetaParser.parse(packet, options);
    }
    
    public static XMPMeta parseFromBuffer(final byte[] buffer) throws XMPException {
        return parseFromBuffer(buffer, null);
    }
    
    public static XMPMeta parseFromBuffer(final byte[] buffer, final ParseOptions options) throws XMPException {
        return XMPMetaParser.parse(buffer, options);
    }
    
    public static void serialize(final XMPMeta xmp, final OutputStream out) throws XMPException {
        serialize(xmp, out, null);
    }
    
    public static void serialize(final XMPMeta xmp, final OutputStream out, final SerializeOptions options) throws XMPException {
        assertImplementation(xmp);
        XMPSerializerHelper.serialize((XMPMetaImpl)xmp, out, options);
    }
    
    public static byte[] serializeToBuffer(final XMPMeta xmp, final SerializeOptions options) throws XMPException {
        assertImplementation(xmp);
        return XMPSerializerHelper.serializeToBuffer((XMPMetaImpl)xmp, options);
    }
    
    public static String serializeToString(final XMPMeta xmp, final SerializeOptions options) throws XMPException {
        assertImplementation(xmp);
        return XMPSerializerHelper.serializeToString((XMPMetaImpl)xmp, options);
    }
    
    private static void assertImplementation(final XMPMeta xmp) {
        if (!(xmp instanceof XMPMetaImpl)) {
            throw new UnsupportedOperationException("The serializing service works onlywith the XMPMeta implementation of this library");
        }
    }
    
    public static void reset() {
        XMPMetaFactory.schema = new XMPSchemaRegistryImpl();
    }
    
    public static synchronized XMPVersionInfo getVersionInfo() {
        if (XMPMetaFactory.versionInfo == null) {
            try {
                final int major = 5;
                final int minor = 1;
                final int micro = 0;
                final int engBuild = 3;
                final boolean debug = false;
                final String message = "Adobe XMP Core 5.1.0-jc003";
                XMPMetaFactory.versionInfo = new XMPVersionInfo() {
                    public int getMajor() {
                        return 5;
                    }
                    
                    public int getMinor() {
                        return 1;
                    }
                    
                    public int getMicro() {
                        return 0;
                    }
                    
                    public boolean isDebug() {
                        return false;
                    }
                    
                    public int getBuild() {
                        return 3;
                    }
                    
                    public String getMessage() {
                        return "Adobe XMP Core 5.1.0-jc003";
                    }
                    
                    @Override
                    public String toString() {
                        return "Adobe XMP Core 5.1.0-jc003";
                    }
                };
            }
            catch (Throwable e) {
                System.out.println(e);
            }
        }
        return XMPMetaFactory.versionInfo;
    }
    
    static {
        XMPMetaFactory.schema = new XMPSchemaRegistryImpl();
        XMPMetaFactory.versionInfo = null;
    }
}
