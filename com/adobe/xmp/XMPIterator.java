// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import java.util.Iterator;

public interface XMPIterator extends Iterator
{
    void skipSubtree();
    
    void skipSiblings();
}
