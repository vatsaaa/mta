// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import java.util.Calendar;
import java.util.TimeZone;

public interface XMPDateTime extends Comparable
{
    int getYear();
    
    void setYear(final int p0);
    
    int getMonth();
    
    void setMonth(final int p0);
    
    int getDay();
    
    void setDay(final int p0);
    
    int getHour();
    
    void setHour(final int p0);
    
    int getMinute();
    
    void setMinute(final int p0);
    
    int getSecond();
    
    void setSecond(final int p0);
    
    int getNanoSecond();
    
    void setNanoSecond(final int p0);
    
    TimeZone getTimeZone();
    
    void setTimeZone(final TimeZone p0);
    
    boolean hasDate();
    
    boolean hasTime();
    
    boolean hasTimeZone();
    
    Calendar getCalendar();
    
    String getISO8601String();
}
