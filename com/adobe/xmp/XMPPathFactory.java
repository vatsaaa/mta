// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import com.adobe.xmp.impl.Utils;
import com.adobe.xmp.impl.xpath.XMPPath;
import com.adobe.xmp.impl.xpath.XMPPathParser;

public final class XMPPathFactory
{
    private XMPPathFactory() {
    }
    
    public static String composeArrayItemPath(final String arrayName, final int itemIndex) throws XMPException {
        if (itemIndex > 0) {
            return arrayName + '[' + itemIndex + ']';
        }
        if (itemIndex == -1) {
            return arrayName + "[last()]";
        }
        throw new XMPException("Array index must be larger than zero", 104);
    }
    
    public static String composeStructFieldPath(final String fieldNS, final String fieldName) throws XMPException {
        assertFieldNS(fieldNS);
        assertFieldName(fieldName);
        final XMPPath fieldPath = XMPPathParser.expandXPath(fieldNS, fieldName);
        if (fieldPath.size() != 2) {
            throw new XMPException("The field name must be simple", 102);
        }
        return '/' + fieldPath.getSegment(1).getName();
    }
    
    public static String composeQualifierPath(final String qualNS, final String qualName) throws XMPException {
        assertQualNS(qualNS);
        assertQualName(qualName);
        final XMPPath qualPath = XMPPathParser.expandXPath(qualNS, qualName);
        if (qualPath.size() != 2) {
            throw new XMPException("The qualifier name must be simple", 102);
        }
        return "/?" + qualPath.getSegment(1).getName();
    }
    
    public static String composeLangSelector(final String arrayName, final String langName) {
        return arrayName + "[?xml:lang=\"" + Utils.normalizeLangValue(langName) + "\"]";
    }
    
    public static String composeFieldSelector(final String arrayName, final String fieldNS, final String fieldName, final String fieldValue) throws XMPException {
        final XMPPath fieldPath = XMPPathParser.expandXPath(fieldNS, fieldName);
        if (fieldPath.size() != 2) {
            throw new XMPException("The fieldName name must be simple", 102);
        }
        return arrayName + '[' + fieldPath.getSegment(1).getName() + "=\"" + fieldValue + "\"]";
    }
    
    private static void assertQualNS(final String qualNS) throws XMPException {
        if (qualNS == null || qualNS.length() == 0) {
            throw new XMPException("Empty qualifier namespace URI", 101);
        }
    }
    
    private static void assertQualName(final String qualName) throws XMPException {
        if (qualName == null || qualName.length() == 0) {
            throw new XMPException("Empty qualifier name", 102);
        }
    }
    
    private static void assertFieldNS(final String fieldNS) throws XMPException {
        if (fieldNS == null || fieldNS.length() == 0) {
            throw new XMPException("Empty field namespace URI", 101);
        }
    }
    
    private static void assertFieldName(final String fieldName) throws XMPException {
        if (fieldName == null || fieldName.length() == 0) {
            throw new XMPException("Empty f name", 102);
        }
    }
}
