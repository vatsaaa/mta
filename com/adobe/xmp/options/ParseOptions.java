// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.options;

public final class ParseOptions extends Options
{
    public static final int REQUIRE_XMP_META = 1;
    public static final int STRICT_ALIASING = 4;
    public static final int FIX_CONTROL_CHARS = 8;
    public static final int ACCEPT_LATIN_1 = 16;
    public static final int OMIT_NORMALIZATION = 32;
    
    public ParseOptions() {
        this.setOption(24, true);
    }
    
    public boolean getRequireXMPMeta() {
        return this.getOption(1);
    }
    
    public ParseOptions setRequireXMPMeta(final boolean value) {
        this.setOption(1, value);
        return this;
    }
    
    public boolean getStrictAliasing() {
        return this.getOption(4);
    }
    
    public ParseOptions setStrictAliasing(final boolean value) {
        this.setOption(4, value);
        return this;
    }
    
    public boolean getFixControlChars() {
        return this.getOption(8);
    }
    
    public ParseOptions setFixControlChars(final boolean value) {
        this.setOption(8, value);
        return this;
    }
    
    public boolean getAcceptLatin1() {
        return this.getOption(16);
    }
    
    public ParseOptions setOmitNormalization(final boolean value) {
        this.setOption(32, value);
        return this;
    }
    
    public boolean getOmitNormalization() {
        return this.getOption(32);
    }
    
    public ParseOptions setAcceptLatin1(final boolean value) {
        this.setOption(16, value);
        return this;
    }
    
    @Override
    protected String defineOptionName(final int option) {
        switch (option) {
            case 1: {
                return "REQUIRE_XMP_META";
            }
            case 4: {
                return "STRICT_ALIASING";
            }
            case 8: {
                return "FIX_CONTROL_CHARS";
            }
            case 16: {
                return "ACCEPT_LATIN_1";
            }
            case 32: {
                return "OMIT_NORMALIZATION";
            }
            default: {
                return null;
            }
        }
    }
    
    @Override
    protected int getValidOptions() {
        return 61;
    }
}
