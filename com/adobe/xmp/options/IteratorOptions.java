// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.options;

public final class IteratorOptions extends Options
{
    public static final int JUST_CHILDREN = 256;
    public static final int JUST_LEAFNODES = 512;
    public static final int JUST_LEAFNAME = 1024;
    public static final int OMIT_QUALIFIERS = 4096;
    
    public boolean isJustChildren() {
        return this.getOption(256);
    }
    
    public boolean isJustLeafname() {
        return this.getOption(1024);
    }
    
    public boolean isJustLeafnodes() {
        return this.getOption(512);
    }
    
    public boolean isOmitQualifiers() {
        return this.getOption(4096);
    }
    
    public IteratorOptions setJustChildren(final boolean value) {
        this.setOption(256, value);
        return this;
    }
    
    public IteratorOptions setJustLeafname(final boolean value) {
        this.setOption(1024, value);
        return this;
    }
    
    public IteratorOptions setJustLeafnodes(final boolean value) {
        this.setOption(512, value);
        return this;
    }
    
    public IteratorOptions setOmitQualifiers(final boolean value) {
        this.setOption(4096, value);
        return this;
    }
    
    @Override
    protected String defineOptionName(final int option) {
        switch (option) {
            case 256: {
                return "JUST_CHILDREN";
            }
            case 512: {
                return "JUST_LEAFNODES";
            }
            case 1024: {
                return "JUST_LEAFNAME";
            }
            case 4096: {
                return "OMIT_QUALIFIERS";
            }
            default: {
                return null;
            }
        }
    }
    
    @Override
    protected int getValidOptions() {
        return 5888;
    }
}
