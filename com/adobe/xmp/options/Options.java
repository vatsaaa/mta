// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.options;

import java.util.HashMap;
import com.adobe.xmp.XMPException;
import java.util.Map;

public abstract class Options
{
    private int options;
    private Map optionNames;
    
    public Options() {
        this.options = 0;
        this.optionNames = null;
    }
    
    public Options(final int options) throws XMPException {
        this.options = 0;
        this.optionNames = null;
        this.assertOptionsValid(options);
        this.setOptions(options);
    }
    
    public void clear() {
        this.options = 0;
    }
    
    public boolean isExactly(final int optionBits) {
        return this.getOptions() == optionBits;
    }
    
    public boolean containsAllOptions(final int optionBits) {
        return (this.getOptions() & optionBits) == optionBits;
    }
    
    public boolean containsOneOf(final int optionBits) {
        return (this.getOptions() & optionBits) != 0x0;
    }
    
    protected boolean getOption(final int optionBit) {
        return (this.options & optionBit) != 0x0;
    }
    
    public void setOption(final int optionBits, final boolean value) {
        this.options = (value ? (this.options | optionBits) : (this.options & ~optionBits));
    }
    
    public int getOptions() {
        return this.options;
    }
    
    public void setOptions(final int options) throws XMPException {
        this.assertOptionsValid(options);
        this.options = options;
    }
    
    @Override
    public boolean equals(final Object obj) {
        return this.getOptions() == ((Options)obj).getOptions();
    }
    
    @Override
    public int hashCode() {
        return this.getOptions();
    }
    
    public String getOptionsString() {
        if (this.options != 0) {
            final StringBuffer sb = new StringBuffer();
            int oneLessBit;
            for (int theBits = this.options; theBits != 0; theBits = oneLessBit) {
                oneLessBit = (theBits & theBits - 1);
                final int singleBit = theBits ^ oneLessBit;
                final String bitName = this.getOptionName(singleBit);
                sb.append(bitName);
                if (oneLessBit != 0) {
                    sb.append(" | ");
                }
            }
            return sb.toString();
        }
        return "<none>";
    }
    
    @Override
    public String toString() {
        return "0x" + Integer.toHexString(this.options);
    }
    
    protected abstract int getValidOptions();
    
    protected abstract String defineOptionName(final int p0);
    
    protected void assertConsistency(final int options) throws XMPException {
    }
    
    private void assertOptionsValid(final int options) throws XMPException {
        final int invalidOptions = options & ~this.getValidOptions();
        if (invalidOptions == 0) {
            this.assertConsistency(options);
            return;
        }
        throw new XMPException("The option bit(s) 0x" + Integer.toHexString(invalidOptions) + " are invalid!", 103);
    }
    
    private String getOptionName(final int option) {
        final Map optionsNames = this.procureOptionNames();
        final Integer key = new Integer(option);
        String result = optionsNames.get(key);
        if (result == null) {
            result = this.defineOptionName(option);
            if (result != null) {
                optionsNames.put(key, result);
            }
            else {
                result = "<option name not defined>";
            }
        }
        return result;
    }
    
    private Map procureOptionNames() {
        if (this.optionNames == null) {
            this.optionNames = new HashMap();
        }
        return this.optionNames;
    }
}
