// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.options;

import com.adobe.xmp.XMPException;

public final class AliasOptions extends Options
{
    public static final int PROP_DIRECT = 0;
    public static final int PROP_ARRAY = 512;
    public static final int PROP_ARRAY_ORDERED = 1024;
    public static final int PROP_ARRAY_ALTERNATE = 2048;
    public static final int PROP_ARRAY_ALT_TEXT = 4096;
    
    public AliasOptions() {
    }
    
    public AliasOptions(final int options) throws XMPException {
        super(options);
    }
    
    public boolean isSimple() {
        return this.getOptions() == 0;
    }
    
    public boolean isArray() {
        return this.getOption(512);
    }
    
    public AliasOptions setArray(final boolean value) {
        this.setOption(512, value);
        return this;
    }
    
    public boolean isArrayOrdered() {
        return this.getOption(1024);
    }
    
    public AliasOptions setArrayOrdered(final boolean value) {
        this.setOption(1536, value);
        return this;
    }
    
    public boolean isArrayAlternate() {
        return this.getOption(2048);
    }
    
    public AliasOptions setArrayAlternate(final boolean value) {
        this.setOption(3584, value);
        return this;
    }
    
    public boolean isArrayAltText() {
        return this.getOption(4096);
    }
    
    public AliasOptions setArrayAltText(final boolean value) {
        this.setOption(7680, value);
        return this;
    }
    
    public PropertyOptions toPropertyOptions() throws XMPException {
        return new PropertyOptions(this.getOptions());
    }
    
    @Override
    protected String defineOptionName(final int option) {
        switch (option) {
            case 0: {
                return "PROP_DIRECT";
            }
            case 512: {
                return "ARRAY";
            }
            case 1024: {
                return "ARRAY_ORDERED";
            }
            case 2048: {
                return "ARRAY_ALTERNATE";
            }
            case 4096: {
                return "ARRAY_ALT_TEXT";
            }
            default: {
                return null;
            }
        }
    }
    
    @Override
    protected int getValidOptions() {
        return 7680;
    }
}
