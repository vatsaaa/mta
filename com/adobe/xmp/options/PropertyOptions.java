// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.options;

import com.adobe.xmp.XMPException;

public final class PropertyOptions extends Options
{
    public static final int NO_OPTIONS = 0;
    public static final int URI = 2;
    public static final int HAS_QUALIFIERS = 16;
    public static final int QUALIFIER = 32;
    public static final int HAS_LANGUAGE = 64;
    public static final int HAS_TYPE = 128;
    public static final int STRUCT = 256;
    public static final int ARRAY = 512;
    public static final int ARRAY_ORDERED = 1024;
    public static final int ARRAY_ALTERNATE = 2048;
    public static final int ARRAY_ALT_TEXT = 4096;
    public static final int SCHEMA_NODE = Integer.MIN_VALUE;
    public static final int DELETE_EXISTING = 536870912;
    
    public PropertyOptions() {
    }
    
    public PropertyOptions(final int options) throws XMPException {
        super(options);
    }
    
    public boolean isURI() {
        return this.getOption(2);
    }
    
    public PropertyOptions setURI(final boolean value) {
        this.setOption(2, value);
        return this;
    }
    
    public boolean getHasQualifiers() {
        return this.getOption(16);
    }
    
    public PropertyOptions setHasQualifiers(final boolean value) {
        this.setOption(16, value);
        return this;
    }
    
    public boolean isQualifier() {
        return this.getOption(32);
    }
    
    public PropertyOptions setQualifier(final boolean value) {
        this.setOption(32, value);
        return this;
    }
    
    public boolean getHasLanguage() {
        return this.getOption(64);
    }
    
    public PropertyOptions setHasLanguage(final boolean value) {
        this.setOption(64, value);
        return this;
    }
    
    public boolean getHasType() {
        return this.getOption(128);
    }
    
    public PropertyOptions setHasType(final boolean value) {
        this.setOption(128, value);
        return this;
    }
    
    public boolean isStruct() {
        return this.getOption(256);
    }
    
    public PropertyOptions setStruct(final boolean value) {
        this.setOption(256, value);
        return this;
    }
    
    public boolean isArray() {
        return this.getOption(512);
    }
    
    public PropertyOptions setArray(final boolean value) {
        this.setOption(512, value);
        return this;
    }
    
    public boolean isArrayOrdered() {
        return this.getOption(1024);
    }
    
    public PropertyOptions setArrayOrdered(final boolean value) {
        this.setOption(1024, value);
        return this;
    }
    
    public boolean isArrayAlternate() {
        return this.getOption(2048);
    }
    
    public PropertyOptions setArrayAlternate(final boolean value) {
        this.setOption(2048, value);
        return this;
    }
    
    public boolean isArrayAltText() {
        return this.getOption(4096);
    }
    
    public PropertyOptions setArrayAltText(final boolean value) {
        this.setOption(4096, value);
        return this;
    }
    
    public boolean isSchemaNode() {
        return this.getOption(Integer.MIN_VALUE);
    }
    
    public PropertyOptions setSchemaNode(final boolean value) {
        this.setOption(Integer.MIN_VALUE, value);
        return this;
    }
    
    public boolean isCompositeProperty() {
        return (this.getOptions() & 0x300) > 0;
    }
    
    public boolean isSimple() {
        return (this.getOptions() & 0x300) == 0x0;
    }
    
    public boolean equalArrayTypes(final PropertyOptions options) {
        return this.isArray() == options.isArray() && this.isArrayOrdered() == options.isArrayOrdered() && this.isArrayAlternate() == options.isArrayAlternate() && this.isArrayAltText() == options.isArrayAltText();
    }
    
    public void mergeWith(final PropertyOptions options) throws XMPException {
        if (options != null) {
            this.setOptions(this.getOptions() | options.getOptions());
        }
    }
    
    public boolean isOnlyArrayOptions() {
        return (this.getOptions() & 0xFFFFE1FF) == 0x0;
    }
    
    @Override
    protected int getValidOptions() {
        return -2147475470;
    }
    
    @Override
    protected String defineOptionName(final int option) {
        switch (option) {
            case 2: {
                return "URI";
            }
            case 16: {
                return "HAS_QUALIFIER";
            }
            case 32: {
                return "QUALIFIER";
            }
            case 64: {
                return "HAS_LANGUAGE";
            }
            case 128: {
                return "HAS_TYPE";
            }
            case 256: {
                return "STRUCT";
            }
            case 512: {
                return "ARRAY";
            }
            case 1024: {
                return "ARRAY_ORDERED";
            }
            case 2048: {
                return "ARRAY_ALTERNATE";
            }
            case 4096: {
                return "ARRAY_ALT_TEXT";
            }
            case Integer.MIN_VALUE: {
                return "SCHEMA_NODE";
            }
            default: {
                return null;
            }
        }
    }
    
    public void assertConsistency(final int options) throws XMPException {
        if ((options & 0x100) > 0 && (options & 0x200) > 0) {
            throw new XMPException("IsStruct and IsArray options are mutually exclusive", 103);
        }
        if ((options & 0x2) > 0 && (options & 0x300) > 0) {
            throw new XMPException("Structs and arrays can't have \"value\" options", 103);
        }
    }
}
