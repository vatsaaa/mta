// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import com.adobe.xmp.options.ParseOptions;
import com.adobe.xmp.options.IteratorOptions;
import java.util.Calendar;
import com.adobe.xmp.options.PropertyOptions;
import com.adobe.xmp.properties.XMPProperty;

public interface XMPMeta extends Cloneable
{
    XMPProperty getProperty(final String p0, final String p1) throws XMPException;
    
    XMPProperty getArrayItem(final String p0, final String p1, final int p2) throws XMPException;
    
    int countArrayItems(final String p0, final String p1) throws XMPException;
    
    XMPProperty getStructField(final String p0, final String p1, final String p2, final String p3) throws XMPException;
    
    XMPProperty getQualifier(final String p0, final String p1, final String p2, final String p3) throws XMPException;
    
    void setProperty(final String p0, final String p1, final Object p2, final PropertyOptions p3) throws XMPException;
    
    void setProperty(final String p0, final String p1, final Object p2) throws XMPException;
    
    void setArrayItem(final String p0, final String p1, final int p2, final String p3, final PropertyOptions p4) throws XMPException;
    
    void setArrayItem(final String p0, final String p1, final int p2, final String p3) throws XMPException;
    
    void insertArrayItem(final String p0, final String p1, final int p2, final String p3, final PropertyOptions p4) throws XMPException;
    
    void insertArrayItem(final String p0, final String p1, final int p2, final String p3) throws XMPException;
    
    void appendArrayItem(final String p0, final String p1, final PropertyOptions p2, final String p3, final PropertyOptions p4) throws XMPException;
    
    void appendArrayItem(final String p0, final String p1, final String p2) throws XMPException;
    
    void setStructField(final String p0, final String p1, final String p2, final String p3, final String p4, final PropertyOptions p5) throws XMPException;
    
    void setStructField(final String p0, final String p1, final String p2, final String p3, final String p4) throws XMPException;
    
    void setQualifier(final String p0, final String p1, final String p2, final String p3, final String p4, final PropertyOptions p5) throws XMPException;
    
    void setQualifier(final String p0, final String p1, final String p2, final String p3, final String p4) throws XMPException;
    
    void deleteProperty(final String p0, final String p1);
    
    void deleteArrayItem(final String p0, final String p1, final int p2);
    
    void deleteStructField(final String p0, final String p1, final String p2, final String p3);
    
    void deleteQualifier(final String p0, final String p1, final String p2, final String p3);
    
    boolean doesPropertyExist(final String p0, final String p1);
    
    boolean doesArrayItemExist(final String p0, final String p1, final int p2);
    
    boolean doesStructFieldExist(final String p0, final String p1, final String p2, final String p3);
    
    boolean doesQualifierExist(final String p0, final String p1, final String p2, final String p3);
    
    XMPProperty getLocalizedText(final String p0, final String p1, final String p2, final String p3) throws XMPException;
    
    void setLocalizedText(final String p0, final String p1, final String p2, final String p3, final String p4, final PropertyOptions p5) throws XMPException;
    
    void setLocalizedText(final String p0, final String p1, final String p2, final String p3, final String p4) throws XMPException;
    
    Boolean getPropertyBoolean(final String p0, final String p1) throws XMPException;
    
    Integer getPropertyInteger(final String p0, final String p1) throws XMPException;
    
    Long getPropertyLong(final String p0, final String p1) throws XMPException;
    
    Double getPropertyDouble(final String p0, final String p1) throws XMPException;
    
    XMPDateTime getPropertyDate(final String p0, final String p1) throws XMPException;
    
    Calendar getPropertyCalendar(final String p0, final String p1) throws XMPException;
    
    byte[] getPropertyBase64(final String p0, final String p1) throws XMPException;
    
    String getPropertyString(final String p0, final String p1) throws XMPException;
    
    void setPropertyBoolean(final String p0, final String p1, final boolean p2, final PropertyOptions p3) throws XMPException;
    
    void setPropertyBoolean(final String p0, final String p1, final boolean p2) throws XMPException;
    
    void setPropertyInteger(final String p0, final String p1, final int p2, final PropertyOptions p3) throws XMPException;
    
    void setPropertyInteger(final String p0, final String p1, final int p2) throws XMPException;
    
    void setPropertyLong(final String p0, final String p1, final long p2, final PropertyOptions p3) throws XMPException;
    
    void setPropertyLong(final String p0, final String p1, final long p2) throws XMPException;
    
    void setPropertyDouble(final String p0, final String p1, final double p2, final PropertyOptions p3) throws XMPException;
    
    void setPropertyDouble(final String p0, final String p1, final double p2) throws XMPException;
    
    void setPropertyDate(final String p0, final String p1, final XMPDateTime p2, final PropertyOptions p3) throws XMPException;
    
    void setPropertyDate(final String p0, final String p1, final XMPDateTime p2) throws XMPException;
    
    void setPropertyCalendar(final String p0, final String p1, final Calendar p2, final PropertyOptions p3) throws XMPException;
    
    void setPropertyCalendar(final String p0, final String p1, final Calendar p2) throws XMPException;
    
    void setPropertyBase64(final String p0, final String p1, final byte[] p2, final PropertyOptions p3) throws XMPException;
    
    void setPropertyBase64(final String p0, final String p1, final byte[] p2) throws XMPException;
    
    XMPIterator iterator() throws XMPException;
    
    XMPIterator iterator(final IteratorOptions p0) throws XMPException;
    
    XMPIterator iterator(final String p0, final String p1, final IteratorOptions p2) throws XMPException;
    
    String getObjectName();
    
    void setObjectName(final String p0);
    
    String getPacketHeader();
    
    Object clone();
    
    void sort();
    
    void normalize(final ParseOptions p0) throws XMPException;
    
    String dumpObject();
}
