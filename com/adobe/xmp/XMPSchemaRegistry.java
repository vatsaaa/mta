// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import com.adobe.xmp.properties.XMPAliasInfo;
import java.util.Map;

public interface XMPSchemaRegistry
{
    String registerNamespace(final String p0, final String p1) throws XMPException;
    
    String getNamespacePrefix(final String p0);
    
    String getNamespaceURI(final String p0);
    
    Map getNamespaces();
    
    Map getPrefixes();
    
    void deleteNamespace(final String p0);
    
    XMPAliasInfo resolveAlias(final String p0, final String p1);
    
    XMPAliasInfo[] findAliases(final String p0);
    
    XMPAliasInfo findAlias(final String p0);
    
    Map getAliases();
}
