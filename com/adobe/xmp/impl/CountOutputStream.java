// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.io.IOException;
import java.io.OutputStream;

public final class CountOutputStream extends OutputStream
{
    private final OutputStream out;
    private int bytesWritten;
    
    CountOutputStream(final OutputStream out) {
        this.bytesWritten = 0;
        this.out = out;
    }
    
    @Override
    public void write(final byte[] buf, final int off, final int len) throws IOException {
        this.out.write(buf, off, len);
        this.bytesWritten += len;
    }
    
    @Override
    public void write(final byte[] buf) throws IOException {
        this.out.write(buf);
        this.bytesWritten += buf.length;
    }
    
    @Override
    public void write(final int b) throws IOException {
        this.out.write(b);
        ++this.bytesWritten;
    }
    
    public int getBytesWritten() {
        return this.bytesWritten;
    }
}
