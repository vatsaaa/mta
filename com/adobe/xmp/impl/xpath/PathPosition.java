// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl.xpath;

class PathPosition
{
    public String path;
    int nameStart;
    int nameEnd;
    int stepBegin;
    int stepEnd;
    
    PathPosition() {
        this.path = null;
        this.nameStart = 0;
        this.nameEnd = 0;
        this.stepBegin = 0;
        this.stepEnd = 0;
    }
}
