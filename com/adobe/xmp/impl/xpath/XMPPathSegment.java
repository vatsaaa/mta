// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl.xpath;

public class XMPPathSegment
{
    private String name;
    private int kind;
    private boolean alias;
    private int aliasForm;
    
    public XMPPathSegment(final String name) {
        this.name = name;
    }
    
    public XMPPathSegment(final String name, final int kind) {
        this.name = name;
        this.kind = kind;
    }
    
    public int getKind() {
        return this.kind;
    }
    
    public void setKind(final int kind) {
        this.kind = kind;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setAlias(final boolean alias) {
        this.alias = alias;
    }
    
    public boolean isAlias() {
        return this.alias;
    }
    
    public int getAliasForm() {
        return this.aliasForm;
    }
    
    public void setAliasForm(final int aliasForm) {
        this.aliasForm = aliasForm;
    }
    
    @Override
    public String toString() {
        switch (this.kind) {
            case 1:
            case 2:
            case 3:
            case 4: {
                return this.name;
            }
            case 5:
            case 6: {
                return this.name;
            }
            default: {
                return this.name;
            }
        }
    }
}
