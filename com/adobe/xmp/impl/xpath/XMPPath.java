// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl.xpath;

import java.util.ArrayList;
import java.util.List;

public class XMPPath
{
    public static final int STRUCT_FIELD_STEP = 1;
    public static final int QUALIFIER_STEP = 2;
    public static final int ARRAY_INDEX_STEP = 3;
    public static final int ARRAY_LAST_STEP = 4;
    public static final int QUAL_SELECTOR_STEP = 5;
    public static final int FIELD_SELECTOR_STEP = 6;
    public static final int SCHEMA_NODE = Integer.MIN_VALUE;
    public static final int STEP_SCHEMA = 0;
    public static final int STEP_ROOT_PROP = 1;
    private List segments;
    
    public XMPPath() {
        this.segments = new ArrayList(5);
    }
    
    public void add(final XMPPathSegment segment) {
        this.segments.add(segment);
    }
    
    public XMPPathSegment getSegment(final int index) {
        return this.segments.get(index);
    }
    
    public int size() {
        return this.segments.size();
    }
    
    @Override
    public String toString() {
        final StringBuffer result = new StringBuffer();
        for (int index = 1; index < this.size(); ++index) {
            result.append(this.getSegment(index));
            if (index < this.size() - 1) {
                final int kind = this.getSegment(index + 1).getKind();
                if (kind == 1 || kind == 2) {
                    result.append('/');
                }
            }
        }
        return result.toString();
    }
}
