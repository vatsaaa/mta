// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import org.xml.sax.ErrorHandler;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.IOException;
import org.xml.sax.InputSource;
import java.io.InputStream;
import com.adobe.xmp.XMPException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.options.ParseOptions;
import javax.xml.parsers.DocumentBuilderFactory;

public class XMPMetaParser
{
    private static final Object XMP_RDF;
    private static DocumentBuilderFactory factory;
    
    private XMPMetaParser() {
    }
    
    public static XMPMeta parse(final Object input, ParseOptions options) throws XMPException {
        ParameterAsserts.assertNotNull(input);
        options = ((options != null) ? options : new ParseOptions());
        final Document document = parseXml(input, options);
        final boolean xmpmetaRequired = options.getRequireXMPMeta();
        Object[] result = new Object[3];
        result = findRootNode(document, xmpmetaRequired, result);
        if (result == null || result[1] != XMPMetaParser.XMP_RDF) {
            return new XMPMetaImpl();
        }
        final XMPMetaImpl xmp = ParseRDF.parse((Node)result[0]);
        xmp.setPacketHeader((String)result[2]);
        if (!options.getOmitNormalization()) {
            return XMPNormalizer.process(xmp, options);
        }
        return xmp;
    }
    
    private static Document parseXml(final Object input, final ParseOptions options) throws XMPException {
        if (input instanceof InputStream) {
            return parseXmlFromInputStream((InputStream)input, options);
        }
        if (input instanceof byte[]) {
            return parseXmlFromBytebuffer(new ByteBuffer((byte[])input), options);
        }
        return parseXmlFromString((String)input, options);
    }
    
    private static Document parseXmlFromInputStream(final InputStream stream, final ParseOptions options) throws XMPException {
        if (!options.getAcceptLatin1() && !options.getFixControlChars()) {
            return parseInputSource(new InputSource(stream));
        }
        try {
            final ByteBuffer buffer = new ByteBuffer(stream);
            return parseXmlFromBytebuffer(buffer, options);
        }
        catch (IOException e) {
            throw new XMPException("Error reading the XML-file", 204, e);
        }
    }
    
    private static Document parseXmlFromBytebuffer(ByteBuffer buffer, final ParseOptions options) throws XMPException {
        InputSource source = new InputSource(buffer.getByteStream());
        try {
            return parseInputSource(source);
        }
        catch (XMPException e) {
            if (e.getErrorCode() == 201 || e.getErrorCode() == 204) {
                if (options.getAcceptLatin1()) {
                    buffer = Latin1Converter.convert(buffer);
                }
                if (options.getFixControlChars()) {
                    try {
                        final String encoding = buffer.getEncoding();
                        final Reader fixReader = new FixASCIIControlsReader(new InputStreamReader(buffer.getByteStream(), encoding));
                        return parseInputSource(new InputSource(fixReader));
                    }
                    catch (UnsupportedEncodingException e2) {
                        throw new XMPException("Unsupported Encoding", 9, e);
                    }
                }
                source = new InputSource(buffer.getByteStream());
                return parseInputSource(source);
            }
            throw e;
        }
    }
    
    private static Document parseXmlFromString(final String input, final ParseOptions options) throws XMPException {
        InputSource source = new InputSource(new StringReader(input));
        try {
            return parseInputSource(source);
        }
        catch (XMPException e) {
            if (e.getErrorCode() == 201 && options.getFixControlChars()) {
                source = new InputSource(new FixASCIIControlsReader(new StringReader(input)));
                return parseInputSource(source);
            }
            throw e;
        }
    }
    
    private static Document parseInputSource(final InputSource source) throws XMPException {
        try {
            final DocumentBuilder builder = XMPMetaParser.factory.newDocumentBuilder();
            builder.setErrorHandler(null);
            return builder.parse(source);
        }
        catch (SAXException e) {
            throw new XMPException("XML parsing failure", 201, e);
        }
        catch (ParserConfigurationException e2) {
            throw new XMPException("XML Parser not correctly configured", 0, e2);
        }
        catch (IOException e3) {
            throw new XMPException("Error reading the XML-file", 204, e3);
        }
    }
    
    private static Object[] findRootNode(Node root, final boolean xmpmetaRequired, final Object[] result) {
        final NodeList children = root.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i) {
            root = children.item(i);
            if (7 == root.getNodeType() && "xpacket".equals(((ProcessingInstruction)root).getTarget())) {
                if (result != null) {
                    result[2] = ((ProcessingInstruction)root).getData();
                }
            }
            else if (3 != root.getNodeType() && 7 != root.getNodeType()) {
                final String rootNS = root.getNamespaceURI();
                final String rootLocal = root.getLocalName();
                if (("xmpmeta".equals(rootLocal) || "xapmeta".equals(rootLocal)) && "adobe:ns:meta/".equals(rootNS)) {
                    return findRootNode(root, false, result);
                }
                if (!xmpmetaRequired && "RDF".equals(rootLocal) && "http://www.w3.org/1999/02/22-rdf-syntax-ns#".equals(rootNS)) {
                    if (result != null) {
                        result[0] = root;
                        result[1] = XMPMetaParser.XMP_RDF;
                    }
                    return result;
                }
                final Object[] newResult = findRootNode(root, xmpmetaRequired, result);
                if (newResult != null) {
                    return newResult;
                }
            }
        }
        return null;
    }
    
    private static DocumentBuilderFactory createDocumentBuilderFactory() {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setIgnoringComments(true);
        try {
            factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
        }
        catch (Exception ex) {}
        return factory;
    }
    
    static {
        XMP_RDF = new Object();
        XMPMetaParser.factory = createDocumentBuilderFactory();
    }
}
