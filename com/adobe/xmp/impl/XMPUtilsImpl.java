// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import com.adobe.xmp.properties.XMPAliasInfo;
import com.adobe.xmp.XMPMetaFactory;
import java.util.Iterator;
import com.adobe.xmp.impl.xpath.XMPPath;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.options.PropertyOptions;
import com.adobe.xmp.impl.xpath.XMPPathParser;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.XMPConst;

public class XMPUtilsImpl implements XMPConst
{
    private static final int UCK_NORMAL = 0;
    private static final int UCK_SPACE = 1;
    private static final int UCK_COMMA = 2;
    private static final int UCK_SEMICOLON = 3;
    private static final int UCK_QUOTE = 4;
    private static final int UCK_CONTROL = 5;
    private static final String SPACES = " \u3000\u303f";
    private static final String COMMAS = ",\uff0c\uff64\ufe50\ufe51\u3001\u060c\u055d";
    private static final String SEMICOLA = ";\uff1b\ufe54\u061b\u037e";
    private static final String QUOTES = "\"«»\u301d\u301e\u301f\u2015\u2039\u203a";
    private static final String CONTROLS = "\u2028\u2029";
    
    private XMPUtilsImpl() {
    }
    
    public static String catenateArrayItems(final XMPMeta xmp, final String schemaNS, final String arrayName, String separator, String quotes, final boolean allowCommas) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(arrayName);
        ParameterAsserts.assertImplementation(xmp);
        if (separator == null || separator.length() == 0) {
            separator = "; ";
        }
        if (quotes == null || quotes.length() == 0) {
            quotes = "\"";
        }
        final XMPMetaImpl xmpImpl = (XMPMetaImpl)xmp;
        XMPNode arrayNode = null;
        XMPNode currItem = null;
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, arrayName);
        arrayNode = XMPNodeUtils.findNode(xmpImpl.getRoot(), arrayPath, false, null);
        if (arrayNode == null) {
            return "";
        }
        if (!arrayNode.getOptions().isArray() || arrayNode.getOptions().isArrayAlternate()) {
            throw new XMPException("Named property must be non-alternate array", 4);
        }
        checkSeparator(separator);
        final char openQuote = quotes.charAt(0);
        final char closeQuote = checkQuotes(quotes, openQuote);
        final StringBuffer catinatedString = new StringBuffer();
        final Iterator it = arrayNode.iterateChildren();
        while (it.hasNext()) {
            currItem = it.next();
            if (currItem.getOptions().isCompositeProperty()) {
                throw new XMPException("Array items must be simple", 4);
            }
            final String str = applyQuotes(currItem.getValue(), openQuote, closeQuote, allowCommas);
            catinatedString.append(str);
            if (!it.hasNext()) {
                continue;
            }
            catinatedString.append(separator);
        }
        return catinatedString.toString();
    }
    
    public static void separateArrayItems(final XMPMeta xmp, final String schemaNS, final String arrayName, final String catedStr, final PropertyOptions arrayOptions, final boolean preserveCommas) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(arrayName);
        if (catedStr == null) {
            throw new XMPException("Parameter must not be null", 4);
        }
        ParameterAsserts.assertImplementation(xmp);
        final XMPMetaImpl xmpImpl = (XMPMetaImpl)xmp;
        final XMPNode arrayNode = separateFindCreateArray(schemaNS, arrayName, arrayOptions, xmpImpl);
        int nextKind = 0;
        int charKind = 0;
        char ch = '\0';
        char nextChar = '\0';
        int itemEnd = 0;
        final int endPos = catedStr.length();
        while (itemEnd < endPos) {
            int itemStart;
            for (itemStart = itemEnd; itemStart < endPos; ++itemStart) {
                ch = catedStr.charAt(itemStart);
                charKind = classifyCharacter(ch);
                if (charKind == 0) {
                    break;
                }
                if (charKind == 4) {
                    break;
                }
            }
            if (itemStart >= endPos) {
                break;
            }
            String itemValue;
            if (charKind != 4) {
                for (itemEnd = itemStart; itemEnd < endPos; ++itemEnd) {
                    ch = catedStr.charAt(itemEnd);
                    charKind = classifyCharacter(ch);
                    if (charKind != 0 && charKind != 4) {
                        if (charKind != 2 || !preserveCommas) {
                            if (charKind != 1) {
                                break;
                            }
                            if (itemEnd + 1 >= endPos) {
                                break;
                            }
                            ch = catedStr.charAt(itemEnd + 1);
                            nextKind = classifyCharacter(ch);
                            if (nextKind != 0 && nextKind != 4 && (nextKind != 2 || !preserveCommas)) {
                                break;
                            }
                        }
                    }
                }
                itemValue = catedStr.substring(itemStart, itemEnd);
            }
            else {
                final char openQuote = ch;
                final char closeQuote = getClosingQuote(openQuote);
                ++itemStart;
                itemValue = "";
                for (itemEnd = itemStart; itemEnd < endPos; ++itemEnd) {
                    ch = catedStr.charAt(itemEnd);
                    charKind = classifyCharacter(ch);
                    if (charKind != 4 || !isSurroundingQuote(ch, openQuote, closeQuote)) {
                        itemValue += ch;
                    }
                    else {
                        if (itemEnd + 1 < endPos) {
                            nextChar = catedStr.charAt(itemEnd + 1);
                            nextKind = classifyCharacter(nextChar);
                        }
                        else {
                            nextKind = 3;
                            nextChar = ';';
                        }
                        if (ch == nextChar) {
                            itemValue += ch;
                            ++itemEnd;
                        }
                        else {
                            if (isClosingingQuote(ch, openQuote, closeQuote)) {
                                ++itemEnd;
                                break;
                            }
                            itemValue += ch;
                        }
                    }
                }
            }
            int foundIndex = -1;
            for (int oldChild = 1; oldChild <= arrayNode.getChildrenLength(); ++oldChild) {
                if (itemValue.equals(arrayNode.getChild(oldChild).getValue())) {
                    foundIndex = oldChild;
                    break;
                }
            }
            XMPNode newItem = null;
            if (foundIndex >= 0) {
                continue;
            }
            newItem = new XMPNode("[]", itemValue, null);
            arrayNode.addChild(newItem);
        }
    }
    
    private static XMPNode separateFindCreateArray(final String schemaNS, final String arrayName, PropertyOptions arrayOptions, final XMPMetaImpl xmp) throws XMPException {
        arrayOptions = XMPNodeUtils.verifySetOptions(arrayOptions, null);
        if (!arrayOptions.isOnlyArrayOptions()) {
            throw new XMPException("Options can only provide array form", 103);
        }
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, arrayName);
        XMPNode arrayNode = XMPNodeUtils.findNode(xmp.getRoot(), arrayPath, false, null);
        if (arrayNode != null) {
            final PropertyOptions arrayForm = arrayNode.getOptions();
            if (!arrayForm.isArray() || arrayForm.isArrayAlternate()) {
                throw new XMPException("Named property must be non-alternate array", 102);
            }
            if (arrayOptions.equalArrayTypes(arrayForm)) {
                throw new XMPException("Mismatch of specified and existing array form", 102);
            }
        }
        else {
            arrayNode = XMPNodeUtils.findNode(xmp.getRoot(), arrayPath, true, arrayOptions.setArray(true));
            if (arrayNode == null) {
                throw new XMPException("Failed to create named array", 102);
            }
        }
        return arrayNode;
    }
    
    public static void removeProperties(final XMPMeta xmp, final String schemaNS, final String propName, final boolean doAllProperties, final boolean includeAliases) throws XMPException {
        ParameterAsserts.assertImplementation(xmp);
        final XMPMetaImpl xmpImpl = (XMPMetaImpl)xmp;
        if (propName != null && propName.length() > 0) {
            if (schemaNS == null || schemaNS.length() == 0) {
                throw new XMPException("Property name requires schema namespace", 4);
            }
            final XMPPath expPath = XMPPathParser.expandXPath(schemaNS, propName);
            final XMPNode propNode = XMPNodeUtils.findNode(xmpImpl.getRoot(), expPath, false, null);
            if (propNode != null && (doAllProperties || !Utils.isInternalProperty(expPath.getSegment(0).getName(), expPath.getSegment(1).getName()))) {
                final XMPNode parent = propNode.getParent();
                parent.removeChild(propNode);
                if (parent.getOptions().isSchemaNode() && !parent.hasChildren()) {
                    parent.getParent().removeChild(parent);
                }
            }
        }
        else if (schemaNS != null && schemaNS.length() > 0) {
            final XMPNode schemaNode = XMPNodeUtils.findSchemaNode(xmpImpl.getRoot(), schemaNS, false);
            if (schemaNode != null && removeSchemaChildren(schemaNode, doAllProperties)) {
                xmpImpl.getRoot().removeChild(schemaNode);
            }
            if (includeAliases) {
                final XMPAliasInfo[] aliases = XMPMetaFactory.getSchemaRegistry().findAliases(schemaNS);
                for (int i = 0; i < aliases.length; ++i) {
                    final XMPAliasInfo info = aliases[i];
                    final XMPPath path = XMPPathParser.expandXPath(info.getNamespace(), info.getPropName());
                    final XMPNode actualProp = XMPNodeUtils.findNode(xmpImpl.getRoot(), path, false, null);
                    if (actualProp != null) {
                        final XMPNode parent2 = actualProp.getParent();
                        parent2.removeChild(actualProp);
                    }
                }
            }
        }
        else {
            final Iterator it = xmpImpl.getRoot().iterateChildren();
            while (it.hasNext()) {
                final XMPNode schema = it.next();
                if (removeSchemaChildren(schema, doAllProperties)) {
                    it.remove();
                }
            }
        }
    }
    
    public static void appendProperties(final XMPMeta source, final XMPMeta destination, final boolean doAllProperties, final boolean replaceOldValues, final boolean deleteEmptyValues) throws XMPException {
        ParameterAsserts.assertImplementation(source);
        ParameterAsserts.assertImplementation(destination);
        final XMPMetaImpl src = (XMPMetaImpl)source;
        final XMPMetaImpl dest = (XMPMetaImpl)destination;
        final Iterator it = src.getRoot().iterateChildren();
        while (it.hasNext()) {
            final XMPNode sourceSchema = it.next();
            XMPNode destSchema = XMPNodeUtils.findSchemaNode(dest.getRoot(), sourceSchema.getName(), false);
            boolean createdSchema = false;
            if (destSchema == null) {
                destSchema = new XMPNode(sourceSchema.getName(), sourceSchema.getValue(), new PropertyOptions().setSchemaNode(true));
                dest.getRoot().addChild(destSchema);
                createdSchema = true;
            }
            final Iterator ic = sourceSchema.iterateChildren();
            while (ic.hasNext()) {
                final XMPNode sourceProp = ic.next();
                if (doAllProperties || !Utils.isInternalProperty(sourceSchema.getName(), sourceProp.getName())) {
                    appendSubtree(dest, sourceProp, destSchema, replaceOldValues, deleteEmptyValues);
                }
            }
            if (!destSchema.hasChildren() && (createdSchema || deleteEmptyValues)) {
                dest.getRoot().removeChild(destSchema);
            }
        }
    }
    
    private static boolean removeSchemaChildren(final XMPNode schemaNode, final boolean doAllProperties) {
        final Iterator it = schemaNode.iterateChildren();
        while (it.hasNext()) {
            final XMPNode currProp = it.next();
            if (doAllProperties || !Utils.isInternalProperty(schemaNode.getName(), currProp.getName())) {
                it.remove();
            }
        }
        return !schemaNode.hasChildren();
    }
    
    private static void appendSubtree(final XMPMetaImpl destXMP, final XMPNode sourceNode, final XMPNode destParent, final boolean replaceOldValues, final boolean deleteEmptyValues) throws XMPException {
        XMPNode destNode = XMPNodeUtils.findChildNode(destParent, sourceNode.getName(), false);
        boolean valueIsEmpty = false;
        if (deleteEmptyValues) {
            valueIsEmpty = (sourceNode.getOptions().isSimple() ? (sourceNode.getValue() == null || sourceNode.getValue().length() == 0) : (!sourceNode.hasChildren()));
        }
        if (deleteEmptyValues && valueIsEmpty) {
            if (destNode != null) {
                destParent.removeChild(destNode);
            }
        }
        else if (destNode == null) {
            destParent.addChild((XMPNode)sourceNode.clone());
        }
        else if (replaceOldValues) {
            destXMP.setNode(destNode, sourceNode.getValue(), sourceNode.getOptions(), true);
            destParent.removeChild(destNode);
            destNode = (XMPNode)sourceNode.clone();
            destParent.addChild(destNode);
        }
        else {
            final PropertyOptions sourceForm = sourceNode.getOptions();
            final PropertyOptions destForm = destNode.getOptions();
            if (sourceForm != destForm) {
                return;
            }
            if (sourceForm.isStruct()) {
                final Iterator it = sourceNode.iterateChildren();
                while (it.hasNext()) {
                    final XMPNode sourceField = it.next();
                    appendSubtree(destXMP, sourceField, destNode, replaceOldValues, deleteEmptyValues);
                    if (deleteEmptyValues && !destNode.hasChildren()) {
                        destParent.removeChild(destNode);
                    }
                }
            }
            else if (sourceForm.isArrayAltText()) {
                final Iterator it = sourceNode.iterateChildren();
                while (it.hasNext()) {
                    final XMPNode sourceItem = it.next();
                    if (sourceItem.hasQualifier()) {
                        if (!"xml:lang".equals(sourceItem.getQualifier(1).getName())) {
                            continue;
                        }
                        final int destIndex = XMPNodeUtils.lookupLanguageItem(destNode, sourceItem.getQualifier(1).getValue());
                        if (deleteEmptyValues && (sourceItem.getValue() == null || sourceItem.getValue().length() == 0)) {
                            if (destIndex == -1) {
                                continue;
                            }
                            destNode.removeChild(destIndex);
                            if (destNode.hasChildren()) {
                                continue;
                            }
                            destParent.removeChild(destNode);
                        }
                        else {
                            if (destIndex != -1) {
                                continue;
                            }
                            if (!"x-default".equals(sourceItem.getQualifier(1).getValue()) || !destNode.hasChildren()) {
                                sourceItem.cloneSubtree(destNode);
                            }
                            else {
                                final XMPNode destItem = new XMPNode(sourceItem.getName(), sourceItem.getValue(), sourceItem.getOptions());
                                sourceItem.cloneSubtree(destItem);
                                destNode.addChild(1, destItem);
                            }
                        }
                    }
                }
            }
            else if (sourceForm.isArray()) {
                final Iterator is = sourceNode.iterateChildren();
                while (is.hasNext()) {
                    final XMPNode sourceItem = is.next();
                    boolean match = false;
                    final Iterator id = destNode.iterateChildren();
                    while (id.hasNext()) {
                        final XMPNode destItem2 = id.next();
                        if (itemValuesMatch(sourceItem, destItem2)) {
                            match = true;
                        }
                    }
                    if (!match) {
                        destNode = (XMPNode)sourceItem.clone();
                        destParent.addChild(destNode);
                    }
                }
            }
        }
    }
    
    private static boolean itemValuesMatch(final XMPNode leftNode, final XMPNode rightNode) throws XMPException {
        final PropertyOptions leftForm = leftNode.getOptions();
        final PropertyOptions rightForm = rightNode.getOptions();
        if (leftForm.equals(rightForm)) {
            return false;
        }
        if (leftForm.getOptions() == 0) {
            if (!leftNode.getValue().equals(rightNode.getValue())) {
                return false;
            }
            if (leftNode.getOptions().getHasLanguage() != rightNode.getOptions().getHasLanguage()) {
                return false;
            }
            if (leftNode.getOptions().getHasLanguage() && !leftNode.getQualifier(1).getValue().equals(rightNode.getQualifier(1).getValue())) {
                return false;
            }
        }
        else if (leftForm.isStruct()) {
            if (leftNode.getChildrenLength() != rightNode.getChildrenLength()) {
                return false;
            }
            final Iterator it = leftNode.iterateChildren();
            while (it.hasNext()) {
                final XMPNode leftField = it.next();
                final XMPNode rightField = XMPNodeUtils.findChildNode(rightNode, leftField.getName(), false);
                if (rightField == null || !itemValuesMatch(leftField, rightField)) {
                    return false;
                }
            }
        }
        else {
            assert leftForm.isArray();
            final Iterator il = leftNode.iterateChildren();
            while (il.hasNext()) {
                final XMPNode leftItem = il.next();
                boolean match = false;
                final Iterator ir = rightNode.iterateChildren();
                while (ir.hasNext()) {
                    final XMPNode rightItem = ir.next();
                    if (itemValuesMatch(leftItem, rightItem)) {
                        match = true;
                        break;
                    }
                }
                if (!match) {
                    return false;
                }
            }
        }
        return true;
    }
    
    private static void checkSeparator(final String separator) throws XMPException {
        boolean haveSemicolon = false;
        for (int i = 0; i < separator.length(); ++i) {
            final int charKind = classifyCharacter(separator.charAt(i));
            if (charKind == 3) {
                if (haveSemicolon) {
                    throw new XMPException("Separator can have only one semicolon", 4);
                }
                haveSemicolon = true;
            }
            else if (charKind != 1) {
                throw new XMPException("Separator can have only spaces and one semicolon", 4);
            }
        }
        if (!haveSemicolon) {
            throw new XMPException("Separator must have one semicolon", 4);
        }
    }
    
    private static char checkQuotes(final String quotes, final char openQuote) throws XMPException {
        int charKind = classifyCharacter(openQuote);
        if (charKind != 4) {
            throw new XMPException("Invalid quoting character", 4);
        }
        char closeQuote;
        if (quotes.length() == 1) {
            closeQuote = openQuote;
        }
        else {
            closeQuote = quotes.charAt(1);
            charKind = classifyCharacter(closeQuote);
            if (charKind != 4) {
                throw new XMPException("Invalid quoting character", 4);
            }
        }
        if (closeQuote != getClosingQuote(openQuote)) {
            throw new XMPException("Mismatched quote pair", 4);
        }
        return closeQuote;
    }
    
    private static int classifyCharacter(final char ch) {
        if (" \u3000\u303f".indexOf(ch) >= 0 || ('\u2000' <= ch && ch <= '\u200b')) {
            return 1;
        }
        if (",\uff0c\uff64\ufe50\ufe51\u3001\u060c\u055d".indexOf(ch) >= 0) {
            return 2;
        }
        if (";\uff1b\ufe54\u061b\u037e".indexOf(ch) >= 0) {
            return 3;
        }
        if ("\"«»\u301d\u301e\u301f\u2015\u2039\u203a".indexOf(ch) >= 0 || ('\u3008' <= ch && ch <= '\u300f') || ('\u2018' <= ch && ch <= '\u201f')) {
            return 4;
        }
        if (ch < ' ' || "\u2028\u2029".indexOf(ch) >= 0) {
            return 5;
        }
        return 0;
    }
    
    private static char getClosingQuote(final char openQuote) {
        switch (openQuote) {
            case '\"': {
                return '\"';
            }
            case '«': {
                return '»';
            }
            case '»': {
                return '«';
            }
            case '\u2015': {
                return '\u2015';
            }
            case '\u2018': {
                return '\u2019';
            }
            case '\u201a': {
                return '\u201b';
            }
            case '\u201c': {
                return '\u201d';
            }
            case '\u201e': {
                return '\u201f';
            }
            case '\u2039': {
                return '\u203a';
            }
            case '\u203a': {
                return '\u2039';
            }
            case '\u3008': {
                return '\u3009';
            }
            case '\u300a': {
                return '\u300b';
            }
            case '\u300c': {
                return '\u300d';
            }
            case '\u300e': {
                return '\u300f';
            }
            case '\u301d': {
                return '\u301f';
            }
            default: {
                return '\0';
            }
        }
    }
    
    private static String applyQuotes(String item, final char openQuote, final char closeQuote, final boolean allowCommas) {
        if (item == null) {
            item = "";
        }
        boolean prevSpace = false;
        int i;
        for (i = 0; i < item.length(); ++i) {
            final char ch = item.charAt(i);
            final int charKind = classifyCharacter(ch);
            if (i == 0 && charKind == 4) {
                break;
            }
            if (charKind == 1) {
                if (prevSpace) {
                    break;
                }
                prevSpace = true;
            }
            else {
                prevSpace = false;
                if (charKind == 3 || charKind == 5) {
                    break;
                }
                if (charKind == 2 && !allowCommas) {
                    break;
                }
            }
        }
        if (i < item.length()) {
            final StringBuffer newItem = new StringBuffer(item.length() + 2);
            int splitPoint;
            for (splitPoint = 0; splitPoint <= i && classifyCharacter(item.charAt(i)) != 4; ++splitPoint) {}
            newItem.append(openQuote).append(item.substring(0, splitPoint));
            for (int charOffset = splitPoint; charOffset < item.length(); ++charOffset) {
                newItem.append(item.charAt(charOffset));
                if (classifyCharacter(item.charAt(charOffset)) == 4 && isSurroundingQuote(item.charAt(charOffset), openQuote, closeQuote)) {
                    newItem.append(item.charAt(charOffset));
                }
            }
            newItem.append(closeQuote);
            item = newItem.toString();
        }
        return item;
    }
    
    private static boolean isSurroundingQuote(final char ch, final char openQuote, final char closeQuote) {
        return ch == openQuote || isClosingingQuote(ch, openQuote, closeQuote);
    }
    
    private static boolean isClosingingQuote(final char ch, final char openQuote, final char closeQuote) {
        return ch == closeQuote || (openQuote == '\u301d' && ch == '\u301e') || ch == '\u301f';
    }
}
