// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.util.Iterator;
import com.adobe.xmp.impl.xpath.XMPPathSegment;
import java.util.Calendar;
import com.adobe.xmp.XMPDateTimeFactory;
import java.util.GregorianCalendar;
import com.adobe.xmp.XMPDateTime;
import com.adobe.xmp.XMPUtils;
import com.adobe.xmp.impl.xpath.XMPPath;
import com.adobe.xmp.XMPMetaFactory;
import com.adobe.xmp.options.PropertyOptions;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPConst;

public class XMPNodeUtils implements XMPConst
{
    static final int CLT_NO_VALUES = 0;
    static final int CLT_SPECIFIC_MATCH = 1;
    static final int CLT_SINGLE_GENERIC = 2;
    static final int CLT_MULTIPLE_GENERIC = 3;
    static final int CLT_XDEFAULT = 4;
    static final int CLT_FIRST_ITEM = 5;
    
    private XMPNodeUtils() {
    }
    
    static XMPNode findSchemaNode(final XMPNode tree, final String namespaceURI, final boolean createNodes) throws XMPException {
        return findSchemaNode(tree, namespaceURI, null, createNodes);
    }
    
    static XMPNode findSchemaNode(final XMPNode tree, final String namespaceURI, final String suggestedPrefix, final boolean createNodes) throws XMPException {
        assert tree.getParent() == null;
        XMPNode schemaNode = tree.findChildByName(namespaceURI);
        if (schemaNode == null && createNodes) {
            schemaNode = new XMPNode(namespaceURI, new PropertyOptions().setSchemaNode(true));
            schemaNode.setImplicit(true);
            String prefix = XMPMetaFactory.getSchemaRegistry().getNamespacePrefix(namespaceURI);
            if (prefix == null) {
                if (suggestedPrefix == null || suggestedPrefix.length() == 0) {
                    throw new XMPException("Unregistered schema namespace URI", 101);
                }
                prefix = XMPMetaFactory.getSchemaRegistry().registerNamespace(namespaceURI, suggestedPrefix);
            }
            schemaNode.setValue(prefix);
            tree.addChild(schemaNode);
        }
        return schemaNode;
    }
    
    static XMPNode findChildNode(final XMPNode parent, final String childName, final boolean createNodes) throws XMPException {
        if (!parent.getOptions().isSchemaNode() && !parent.getOptions().isStruct()) {
            if (!parent.isImplicit()) {
                throw new XMPException("Named children only allowed for schemas and structs", 102);
            }
            if (parent.getOptions().isArray()) {
                throw new XMPException("Named children not allowed for arrays", 102);
            }
            if (createNodes) {
                parent.getOptions().setStruct(true);
            }
        }
        XMPNode childNode = parent.findChildByName(childName);
        if (childNode == null && createNodes) {
            final PropertyOptions options = new PropertyOptions();
            childNode = new XMPNode(childName, options);
            childNode.setImplicit(true);
            parent.addChild(childNode);
        }
        assert !createNodes;
        return childNode;
    }
    
    static XMPNode findNode(final XMPNode xmpTree, final XMPPath xpath, final boolean createNodes, final PropertyOptions leafOptions) throws XMPException {
        if (xpath == null || xpath.size() == 0) {
            throw new XMPException("Empty XMPPath", 102);
        }
        XMPNode rootImplicitNode = null;
        XMPNode currNode = null;
        currNode = findSchemaNode(xmpTree, xpath.getSegment(0).getName(), createNodes);
        if (currNode == null) {
            return null;
        }
        if (currNode.isImplicit()) {
            currNode.setImplicit(false);
            rootImplicitNode = currNode;
        }
        try {
            for (int i = 1; i < xpath.size(); ++i) {
                currNode = followXPathStep(currNode, xpath.getSegment(i), createNodes);
                if (currNode == null) {
                    if (createNodes) {
                        deleteNode(rootImplicitNode);
                    }
                    return null;
                }
                if (currNode.isImplicit()) {
                    currNode.setImplicit(false);
                    if (i == 1 && xpath.getSegment(i).isAlias() && xpath.getSegment(i).getAliasForm() != 0) {
                        currNode.getOptions().setOption(xpath.getSegment(i).getAliasForm(), true);
                    }
                    else if (i < xpath.size() - 1 && xpath.getSegment(i).getKind() == 1 && !currNode.getOptions().isCompositeProperty()) {
                        currNode.getOptions().setStruct(true);
                    }
                    if (rootImplicitNode == null) {
                        rootImplicitNode = currNode;
                    }
                }
            }
        }
        catch (XMPException e) {
            if (rootImplicitNode != null) {
                deleteNode(rootImplicitNode);
            }
            throw e;
        }
        if (rootImplicitNode != null) {
            currNode.getOptions().mergeWith(leafOptions);
            currNode.setOptions(currNode.getOptions());
        }
        return currNode;
    }
    
    static void deleteNode(final XMPNode node) {
        final XMPNode parent = node.getParent();
        if (node.getOptions().isQualifier()) {
            parent.removeQualifier(node);
        }
        else {
            parent.removeChild(node);
        }
        if (!parent.hasChildren() && parent.getOptions().isSchemaNode()) {
            parent.getParent().removeChild(parent);
        }
    }
    
    static void setNodeValue(final XMPNode node, final Object value) {
        final String strValue = serializeNodeValue(value);
        if (!node.getOptions().isQualifier() || !"xml:lang".equals(node.getName())) {
            node.setValue(strValue);
        }
        else {
            node.setValue(Utils.normalizeLangValue(strValue));
        }
    }
    
    static PropertyOptions verifySetOptions(PropertyOptions options, final Object itemValue) throws XMPException {
        if (options == null) {
            options = new PropertyOptions();
        }
        if (options.isArrayAltText()) {
            options.setArrayAlternate(true);
        }
        if (options.isArrayAlternate()) {
            options.setArrayOrdered(true);
        }
        if (options.isArrayOrdered()) {
            options.setArray(true);
        }
        if (options.isCompositeProperty() && itemValue != null && itemValue.toString().length() > 0) {
            throw new XMPException("Structs and arrays can't have values", 103);
        }
        options.assertConsistency(options.getOptions());
        return options;
    }
    
    static String serializeNodeValue(final Object value) {
        String strValue;
        if (value == null) {
            strValue = null;
        }
        else if (value instanceof Boolean) {
            strValue = XMPUtils.convertFromBoolean((boolean)value);
        }
        else if (value instanceof Integer) {
            strValue = XMPUtils.convertFromInteger((int)value);
        }
        else if (value instanceof Long) {
            strValue = XMPUtils.convertFromLong((long)value);
        }
        else if (value instanceof Double) {
            strValue = XMPUtils.convertFromDouble((double)value);
        }
        else if (value instanceof XMPDateTime) {
            strValue = XMPUtils.convertFromDate((XMPDateTime)value);
        }
        else if (value instanceof GregorianCalendar) {
            final XMPDateTime dt = XMPDateTimeFactory.createFromCalendar((Calendar)value);
            strValue = XMPUtils.convertFromDate(dt);
        }
        else if (value instanceof byte[]) {
            strValue = XMPUtils.encodeBase64((byte[])value);
        }
        else {
            strValue = value.toString();
        }
        return (strValue != null) ? Utils.removeControlChars(strValue) : null;
    }
    
    private static XMPNode followXPathStep(final XMPNode parentNode, final XMPPathSegment nextStep, final boolean createNodes) throws XMPException {
        XMPNode nextNode = null;
        int index = 0;
        final int stepKind = nextStep.getKind();
        if (stepKind == 1) {
            nextNode = findChildNode(parentNode, nextStep.getName(), createNodes);
        }
        else if (stepKind == 2) {
            nextNode = findQualifierNode(parentNode, nextStep.getName().substring(1), createNodes);
        }
        else {
            if (!parentNode.getOptions().isArray()) {
                throw new XMPException("Indexing applied to non-array", 102);
            }
            if (stepKind == 3) {
                index = findIndexedItem(parentNode, nextStep.getName(), createNodes);
            }
            else if (stepKind == 4) {
                index = parentNode.getChildrenLength();
            }
            else if (stepKind == 6) {
                final String[] result = Utils.splitNameAndValue(nextStep.getName());
                final String fieldName = result[0];
                final String fieldValue = result[1];
                index = lookupFieldSelector(parentNode, fieldName, fieldValue);
            }
            else {
                if (stepKind != 5) {
                    throw new XMPException("Unknown array indexing step in FollowXPathStep", 9);
                }
                final String[] result = Utils.splitNameAndValue(nextStep.getName());
                final String qualName = result[0];
                final String qualValue = result[1];
                index = lookupQualSelector(parentNode, qualName, qualValue, nextStep.getAliasForm());
            }
            if (1 <= index && index <= parentNode.getChildrenLength()) {
                nextNode = parentNode.getChild(index);
            }
        }
        return nextNode;
    }
    
    private static XMPNode findQualifierNode(final XMPNode parent, final String qualName, final boolean createNodes) throws XMPException {
        assert !qualName.startsWith("?");
        XMPNode qualNode = parent.findQualifierByName(qualName);
        if (qualNode == null && createNodes) {
            qualNode = new XMPNode(qualName, null);
            qualNode.setImplicit(true);
            parent.addQualifier(qualNode);
        }
        return qualNode;
    }
    
    private static int findIndexedItem(final XMPNode arrayNode, String segment, final boolean createNodes) throws XMPException {
        int index = 0;
        try {
            segment = segment.substring(1, segment.length() - 1);
            index = Integer.parseInt(segment);
            if (index < 1) {
                throw new XMPException("Array index must be larger than zero", 102);
            }
        }
        catch (NumberFormatException e) {
            throw new XMPException("Array index not digits.", 102);
        }
        if (createNodes && index == arrayNode.getChildrenLength() + 1) {
            final XMPNode newItem = new XMPNode("[]", null);
            newItem.setImplicit(true);
            arrayNode.addChild(newItem);
        }
        return index;
    }
    
    private static int lookupFieldSelector(final XMPNode arrayNode, final String fieldName, final String fieldValue) throws XMPException {
        int result = -1;
        for (int index = 1; index <= arrayNode.getChildrenLength() && result < 0; ++index) {
            final XMPNode currItem = arrayNode.getChild(index);
            if (!currItem.getOptions().isStruct()) {
                throw new XMPException("Field selector must be used on array of struct", 102);
            }
            for (int f = 1; f <= currItem.getChildrenLength(); ++f) {
                final XMPNode currField = currItem.getChild(f);
                if (fieldName.equals(currField.getName())) {
                    if (fieldValue.equals(currField.getValue())) {
                        result = index;
                        break;
                    }
                }
            }
        }
        return result;
    }
    
    private static int lookupQualSelector(final XMPNode arrayNode, final String qualName, String qualValue, final int aliasForm) throws XMPException {
        if (!"xml:lang".equals(qualName)) {
            for (int index = 1; index < arrayNode.getChildrenLength(); ++index) {
                final XMPNode currItem = arrayNode.getChild(index);
                final Iterator it = currItem.iterateQualifier();
                while (it.hasNext()) {
                    final XMPNode qualifier = it.next();
                    if (qualName.equals(qualifier.getName()) && qualValue.equals(qualifier.getValue())) {
                        return index;
                    }
                }
            }
            return -1;
        }
        qualValue = Utils.normalizeLangValue(qualValue);
        int index = lookupLanguageItem(arrayNode, qualValue);
        if (index < 0 && (aliasForm & 0x1000) > 0) {
            final XMPNode langNode = new XMPNode("[]", null);
            final XMPNode xdefault = new XMPNode("xml:lang", "x-default", null);
            langNode.addQualifier(xdefault);
            arrayNode.addChild(1, langNode);
            return 1;
        }
        return index;
    }
    
    static void normalizeLangArray(final XMPNode arrayNode) {
        if (!arrayNode.getOptions().isArrayAltText()) {
            return;
        }
        int i = 2;
        while (i <= arrayNode.getChildrenLength()) {
            final XMPNode child = arrayNode.getChild(i);
            if (child.hasQualifier() && "x-default".equals(child.getQualifier(1).getValue())) {
                try {
                    arrayNode.removeChild(i);
                    arrayNode.addChild(1, child);
                }
                catch (XMPException e) {
                    assert false;
                }
                if (i == 2) {
                    arrayNode.getChild(2).setValue(child.getValue());
                    break;
                }
                break;
            }
            else {
                ++i;
            }
        }
    }
    
    static void detectAltText(final XMPNode arrayNode) {
        if (arrayNode.getOptions().isArrayAlternate() && arrayNode.hasChildren()) {
            boolean isAltText = false;
            final Iterator it = arrayNode.iterateChildren();
            while (it.hasNext()) {
                final XMPNode child = it.next();
                if (child.getOptions().getHasLanguage()) {
                    isAltText = true;
                    break;
                }
            }
            if (isAltText) {
                arrayNode.getOptions().setArrayAltText(true);
                normalizeLangArray(arrayNode);
            }
        }
    }
    
    static void appendLangItem(final XMPNode arrayNode, final String itemLang, final String itemValue) throws XMPException {
        final XMPNode newItem = new XMPNode("[]", itemValue, null);
        final XMPNode langQual = new XMPNode("xml:lang", itemLang, null);
        newItem.addQualifier(langQual);
        if (!"x-default".equals(langQual.getValue())) {
            arrayNode.addChild(newItem);
        }
        else {
            arrayNode.addChild(1, newItem);
        }
    }
    
    static Object[] chooseLocalizedText(final XMPNode arrayNode, final String genericLang, final String specificLang) throws XMPException {
        if (!arrayNode.getOptions().isArrayAltText()) {
            throw new XMPException("Localized text array is not alt-text", 102);
        }
        if (!arrayNode.hasChildren()) {
            return new Object[] { new Integer(0), null };
        }
        int foundGenericMatches = 0;
        XMPNode resultNode = null;
        XMPNode xDefault = null;
        final Iterator it = arrayNode.iterateChildren();
        while (it.hasNext()) {
            final XMPNode currItem = it.next();
            if (currItem.getOptions().isCompositeProperty()) {
                throw new XMPException("Alt-text array item is not simple", 102);
            }
            if (!currItem.hasQualifier() || !"xml:lang".equals(currItem.getQualifier(1).getName())) {
                throw new XMPException("Alt-text array item has no language qualifier", 102);
            }
            final String currLang = currItem.getQualifier(1).getValue();
            if (specificLang.equals(currLang)) {
                return new Object[] { new Integer(1), currItem };
            }
            if (genericLang != null && currLang.startsWith(genericLang)) {
                if (resultNode == null) {
                    resultNode = currItem;
                }
                ++foundGenericMatches;
            }
            else {
                if (!"x-default".equals(currLang)) {
                    continue;
                }
                xDefault = currItem;
            }
        }
        if (foundGenericMatches == 1) {
            return new Object[] { new Integer(2), resultNode };
        }
        if (foundGenericMatches > 1) {
            return new Object[] { new Integer(3), resultNode };
        }
        if (xDefault != null) {
            return new Object[] { new Integer(4), xDefault };
        }
        return new Object[] { new Integer(5), arrayNode.getChild(1) };
    }
    
    static int lookupLanguageItem(final XMPNode arrayNode, final String language) throws XMPException {
        if (!arrayNode.getOptions().isArray()) {
            throw new XMPException("Language item must be used on array", 102);
        }
        for (int index = 1; index <= arrayNode.getChildrenLength(); ++index) {
            final XMPNode child = arrayNode.getChild(index);
            if (child.hasQualifier()) {
                if ("xml:lang".equals(child.getQualifier(1).getName())) {
                    if (language.equals(child.getQualifier(1).getValue())) {
                        return index;
                    }
                }
            }
        }
        return -1;
    }
}
