// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import com.adobe.xmp.XMPException;

class ParseState
{
    private String str;
    private int pos;
    
    public ParseState(final String str) {
        this.pos = 0;
        this.str = str;
    }
    
    public int length() {
        return this.str.length();
    }
    
    public boolean hasNext() {
        return this.pos < this.str.length();
    }
    
    public char ch(final int index) {
        return (index < this.str.length()) ? this.str.charAt(index) : '\0';
    }
    
    public char ch() {
        return (this.pos < this.str.length()) ? this.str.charAt(this.pos) : '\0';
    }
    
    public void skip() {
        ++this.pos;
    }
    
    public int pos() {
        return this.pos;
    }
    
    public int gatherInt(final String errorMsg, final int maxValue) throws XMPException {
        int value = 0;
        boolean success = false;
        for (char ch = this.ch(this.pos); '0' <= ch && ch <= '9'; ch = this.ch(this.pos)) {
            value = value * 10 + (ch - '0');
            success = true;
            ++this.pos;
        }
        if (!success) {
            throw new XMPException(errorMsg, 5);
        }
        if (value > maxValue) {
            return maxValue;
        }
        if (value < 0) {
            return 0;
        }
        return value;
    }
}
