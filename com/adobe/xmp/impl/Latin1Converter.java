// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.io.UnsupportedEncodingException;

public class Latin1Converter
{
    private static final int STATE_START = 0;
    private static final int STATE_UTF8CHAR = 11;
    
    private Latin1Converter() {
    }
    
    public static ByteBuffer convert(final ByteBuffer buffer) {
        if ("UTF-8".equals(buffer.getEncoding())) {
            final byte[] readAheadBuffer = new byte[8];
            int readAhead = 0;
            int expectedBytes = 0;
            final ByteBuffer out = new ByteBuffer(buffer.length() * 4 / 3);
            int state = 0;
            for (int i = 0; i < buffer.length(); ++i) {
                final int b = buffer.charAt(i);
                switch (state) {
                    default: {
                        if (b < 127) {
                            out.append((byte)b);
                            break;
                        }
                        if (b >= 192) {
                            expectedBytes = -1;
                            for (int test = b; expectedBytes < 8 && (test & 0x80) == 0x80; ++expectedBytes, test <<= 1) {}
                            readAheadBuffer[readAhead++] = (byte)b;
                            state = 11;
                            break;
                        }
                        final byte[] utf8 = convertToUTF8((byte)b);
                        out.append(utf8);
                        break;
                    }
                    case 11: {
                        if (expectedBytes <= 0 || (b & 0xC0) != 0x80) {
                            final byte[] utf8 = convertToUTF8(readAheadBuffer[0]);
                            out.append(utf8);
                            i -= readAhead;
                            readAhead = 0;
                            state = 0;
                            break;
                        }
                        readAheadBuffer[readAhead++] = (byte)b;
                        if (--expectedBytes == 0) {
                            out.append(readAheadBuffer, 0, readAhead);
                            readAhead = 0;
                            state = 0;
                            break;
                        }
                        break;
                    }
                }
            }
            if (state == 11) {
                for (final byte b2 : readAheadBuffer) {
                    final byte[] utf8 = convertToUTF8(b2);
                    out.append(utf8);
                }
            }
            return out;
        }
        return buffer;
    }
    
    private static byte[] convertToUTF8(final byte ch) {
        final int c = ch & 0xFF;
        try {
            if (c >= 128) {
                if (c == 129 || c == 141 || c == 143 || c == 144 || c == 157) {
                    return new byte[] { 32 };
                }
                return new String(new byte[] { ch }, "cp1252").getBytes("UTF-8");
            }
        }
        catch (UnsupportedEncodingException ex) {}
        return new byte[] { ch };
    }
}
