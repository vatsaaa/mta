// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.util.Collection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import com.adobe.xmp.XMPMetaFactory;
import java.io.IOException;
import com.adobe.xmp.XMPException;
import java.io.OutputStream;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.options.SerializeOptions;
import java.io.OutputStreamWriter;
import java.util.Set;

public class XMPSerializerRDF
{
    private static final int DEFAULT_PAD = 2048;
    private static final String PACKET_HEADER = "<?xpacket begin=\"\ufeff\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?>";
    private static final String PACKET_TRAILER = "<?xpacket end=\"";
    private static final String PACKET_TRAILER2 = "\"?>";
    private static final String RDF_XMPMETA_START = "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"";
    private static final String RDF_XMPMETA_END = "</x:xmpmeta>";
    private static final String RDF_RDF_START = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">";
    private static final String RDF_RDF_END = "</rdf:RDF>";
    private static final String RDF_SCHEMA_START = "<rdf:Description rdf:about=";
    private static final String RDF_SCHEMA_END = "</rdf:Description>";
    private static final String RDF_STRUCT_START = "<rdf:Description";
    private static final String RDF_STRUCT_END = "</rdf:Description>";
    private static final String RDF_EMPTY_STRUCT = "<rdf:Description/>";
    static final Set RDF_ATTR_QUALIFIER;
    private XMPMetaImpl xmp;
    private CountOutputStream outputStream;
    private OutputStreamWriter writer;
    private SerializeOptions options;
    private int unicodeSize;
    private int padding;
    
    public XMPSerializerRDF() {
        this.unicodeSize = 1;
    }
    
    public void serialize(final XMPMeta xmp, final OutputStream out, final SerializeOptions options) throws XMPException {
        try {
            this.outputStream = new CountOutputStream(out);
            this.writer = new OutputStreamWriter(this.outputStream, options.getEncoding());
            this.xmp = (XMPMetaImpl)xmp;
            this.options = options;
            this.padding = options.getPadding();
            this.writer = new OutputStreamWriter(this.outputStream, options.getEncoding());
            this.checkOptionsConsistence();
            final String tailStr = this.serializeAsRDF();
            this.writer.flush();
            this.addPadding(tailStr.length());
            this.write(tailStr);
            this.writer.flush();
            this.outputStream.close();
        }
        catch (IOException e) {
            throw new XMPException("Error writing to the OutputStream", 0);
        }
    }
    
    private void addPadding(final int tailLength) throws XMPException, IOException {
        if (this.options.getExactPacketLength()) {
            final int minSize = this.outputStream.getBytesWritten() + tailLength * this.unicodeSize;
            if (minSize > this.padding) {
                throw new XMPException("Can't fit into specified packet size", 107);
            }
            this.padding -= minSize;
        }
        this.padding /= this.unicodeSize;
        final int newlineLen = this.options.getNewline().length();
        if (this.padding >= newlineLen) {
            this.padding -= newlineLen;
            while (this.padding >= 100 + newlineLen) {
                this.writeChars(100, ' ');
                this.writeNewline();
                this.padding -= 100 + newlineLen;
            }
            this.writeChars(this.padding, ' ');
            this.writeNewline();
        }
        else {
            this.writeChars(this.padding, ' ');
        }
    }
    
    protected void checkOptionsConsistence() throws XMPException {
        if (this.options.getEncodeUTF16BE() | this.options.getEncodeUTF16LE()) {
            this.unicodeSize = 2;
        }
        if (this.options.getExactPacketLength()) {
            if (this.options.getOmitPacketWrapper() | this.options.getIncludeThumbnailPad()) {
                throw new XMPException("Inconsistent options for exact size serialize", 103);
            }
            if ((this.options.getPadding() & this.unicodeSize - 1) != 0x0) {
                throw new XMPException("Exact size must be a multiple of the Unicode element", 103);
            }
        }
        else if (this.options.getReadOnlyPacket()) {
            if (this.options.getOmitPacketWrapper() | this.options.getIncludeThumbnailPad()) {
                throw new XMPException("Inconsistent options for read-only packet", 103);
            }
            this.padding = 0;
        }
        else if (this.options.getOmitPacketWrapper()) {
            if (this.options.getIncludeThumbnailPad()) {
                throw new XMPException("Inconsistent options for non-packet serialize", 103);
            }
            this.padding = 0;
        }
        else {
            if (this.padding == 0) {
                this.padding = 2048 * this.unicodeSize;
            }
            if (this.options.getIncludeThumbnailPad() && !this.xmp.doesPropertyExist("http://ns.adobe.com/xap/1.0/", "Thumbnails")) {
                this.padding += 10000 * this.unicodeSize;
            }
        }
    }
    
    private String serializeAsRDF() throws IOException, XMPException {
        int level = 0;
        if (!this.options.getOmitPacketWrapper()) {
            this.writeIndent(level);
            this.write("<?xpacket begin=\"\ufeff\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?>");
            this.writeNewline();
        }
        if (!this.options.getOmitXmpMetaElement()) {
            this.writeIndent(level);
            this.write("<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"");
            if (!this.options.getOmitVersionAttribute()) {
                this.write(XMPMetaFactory.getVersionInfo().getMessage());
            }
            this.write("\">");
            this.writeNewline();
            ++level;
        }
        this.writeIndent(level);
        this.write("<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">");
        this.writeNewline();
        if (this.options.getUseCanonicalFormat()) {
            this.serializeCanonicalRDFSchemas(level);
        }
        else {
            this.serializeCompactRDFSchemas(level);
        }
        this.writeIndent(level);
        this.write("</rdf:RDF>");
        this.writeNewline();
        if (!this.options.getOmitXmpMetaElement()) {
            --level;
            this.writeIndent(level);
            this.write("</x:xmpmeta>");
            this.writeNewline();
        }
        String tailStr = "";
        if (!this.options.getOmitPacketWrapper()) {
            for (level = this.options.getBaseIndent(); level > 0; --level) {
                tailStr += this.options.getIndent();
            }
            tailStr += "<?xpacket end=\"";
            tailStr += (this.options.getReadOnlyPacket() ? 'r' : 'w');
            tailStr += "\"?>";
        }
        return tailStr;
    }
    
    private void serializeCanonicalRDFSchemas(final int level) throws IOException, XMPException {
        if (this.xmp.getRoot().getChildrenLength() > 0) {
            this.startOuterRDFDescription(this.xmp.getRoot(), level);
            final Iterator it = this.xmp.getRoot().iterateChildren();
            while (it.hasNext()) {
                final XMPNode currSchema = it.next();
                this.serializeCanonicalRDFSchema(currSchema, level);
            }
            this.endOuterRDFDescription(level);
        }
        else {
            this.writeIndent(level + 1);
            this.write("<rdf:Description rdf:about=");
            this.writeTreeName();
            this.write("/>");
            this.writeNewline();
        }
    }
    
    private void writeTreeName() throws IOException {
        this.write(34);
        final String name = this.xmp.getRoot().getName();
        if (name != null) {
            this.appendNodeValue(name, true);
        }
        this.write(34);
    }
    
    private void serializeCompactRDFSchemas(final int level) throws IOException, XMPException {
        this.writeIndent(level + 1);
        this.write("<rdf:Description rdf:about=");
        this.writeTreeName();
        final Set usedPrefixes = new HashSet();
        usedPrefixes.add("xml");
        usedPrefixes.add("rdf");
        final Iterator it = this.xmp.getRoot().iterateChildren();
        while (it.hasNext()) {
            final XMPNode schema = it.next();
            this.declareUsedNamespaces(schema, usedPrefixes, level + 3);
        }
        boolean allAreAttrs = true;
        Iterator it2 = this.xmp.getRoot().iterateChildren();
        while (it2.hasNext()) {
            final XMPNode schema2 = it2.next();
            allAreAttrs &= this.serializeCompactRDFAttrProps(schema2, level + 2);
        }
        if (!allAreAttrs) {
            this.write(62);
            this.writeNewline();
            it2 = this.xmp.getRoot().iterateChildren();
            while (it2.hasNext()) {
                final XMPNode schema2 = it2.next();
                this.serializeCompactRDFElementProps(schema2, level + 2);
            }
            this.writeIndent(level + 1);
            this.write("</rdf:Description>");
            this.writeNewline();
            return;
        }
        this.write("/>");
        this.writeNewline();
    }
    
    private boolean serializeCompactRDFAttrProps(final XMPNode parentNode, final int indent) throws IOException {
        boolean allAreAttrs = true;
        final Iterator it = parentNode.iterateChildren();
        while (it.hasNext()) {
            final XMPNode prop = it.next();
            if (this.canBeRDFAttrProp(prop)) {
                this.writeNewline();
                this.writeIndent(indent);
                this.write(prop.getName());
                this.write("=\"");
                this.appendNodeValue(prop.getValue(), true);
                this.write(34);
            }
            else {
                allAreAttrs = false;
            }
        }
        return allAreAttrs;
    }
    
    private void serializeCompactRDFElementProps(final XMPNode parentNode, final int indent) throws IOException, XMPException {
        final Iterator it = parentNode.iterateChildren();
        while (it.hasNext()) {
            final XMPNode node = it.next();
            if (this.canBeRDFAttrProp(node)) {
                continue;
            }
            boolean emitEndTag = true;
            boolean indentEndTag = true;
            String elemName = node.getName();
            if ("[]".equals(elemName)) {
                elemName = "rdf:li";
            }
            this.writeIndent(indent);
            this.write(60);
            this.write(elemName);
            boolean hasGeneralQualifiers = false;
            boolean hasRDFResourceQual = false;
            final Iterator iq = node.iterateQualifier();
            while (iq.hasNext()) {
                final XMPNode qualifier = iq.next();
                if (!XMPSerializerRDF.RDF_ATTR_QUALIFIER.contains(qualifier.getName())) {
                    hasGeneralQualifiers = true;
                }
                else {
                    hasRDFResourceQual = "rdf:resource".equals(qualifier.getName());
                    this.write(32);
                    this.write(qualifier.getName());
                    this.write("=\"");
                    this.appendNodeValue(qualifier.getValue(), true);
                    this.write(34);
                }
            }
            if (hasGeneralQualifiers) {
                this.serializeCompactRDFGeneralQualifier(indent, node);
            }
            else if (!node.getOptions().isCompositeProperty()) {
                final Object[] result = this.serializeCompactRDFSimpleProp(node);
                emitEndTag = (boolean)result[0];
                indentEndTag = (boolean)result[1];
            }
            else if (node.getOptions().isArray()) {
                this.serializeCompactRDFArrayProp(node, indent);
            }
            else {
                emitEndTag = this.serializeCompactRDFStructProp(node, indent, hasRDFResourceQual);
            }
            if (!emitEndTag) {
                continue;
            }
            if (indentEndTag) {
                this.writeIndent(indent);
            }
            this.write("</");
            this.write(elemName);
            this.write(62);
            this.writeNewline();
        }
    }
    
    private Object[] serializeCompactRDFSimpleProp(final XMPNode node) throws IOException {
        Boolean emitEndTag = Boolean.TRUE;
        Boolean indentEndTag = Boolean.TRUE;
        if (node.getOptions().isURI()) {
            this.write(" rdf:resource=\"");
            this.appendNodeValue(node.getValue(), true);
            this.write("\"/>");
            this.writeNewline();
            emitEndTag = Boolean.FALSE;
        }
        else if (node.getValue() == null || node.getValue().length() == 0) {
            this.write("/>");
            this.writeNewline();
            emitEndTag = Boolean.FALSE;
        }
        else {
            this.write(62);
            this.appendNodeValue(node.getValue(), false);
            indentEndTag = Boolean.FALSE;
        }
        return new Object[] { emitEndTag, indentEndTag };
    }
    
    private void serializeCompactRDFArrayProp(final XMPNode node, final int indent) throws IOException, XMPException {
        this.write(62);
        this.writeNewline();
        this.emitRDFArrayTag(node, true, indent + 1);
        if (node.getOptions().isArrayAltText()) {
            XMPNodeUtils.normalizeLangArray(node);
        }
        this.serializeCompactRDFElementProps(node, indent + 2);
        this.emitRDFArrayTag(node, false, indent + 1);
    }
    
    private boolean serializeCompactRDFStructProp(final XMPNode node, final int indent, final boolean hasRDFResourceQual) throws XMPException, IOException {
        boolean hasAttrFields = false;
        boolean hasElemFields = false;
        boolean emitEndTag = true;
        final Iterator ic = node.iterateChildren();
        while (ic.hasNext()) {
            final XMPNode field = ic.next();
            if (this.canBeRDFAttrProp(field)) {
                hasAttrFields = true;
            }
            else {
                hasElemFields = true;
            }
            if (hasAttrFields && hasElemFields) {
                break;
            }
        }
        if (hasRDFResourceQual && hasElemFields) {
            throw new XMPException("Can't mix rdf:resource qualifier and element fields", 202);
        }
        if (!node.hasChildren()) {
            this.write(" rdf:parseType=\"Resource\"/>");
            this.writeNewline();
            emitEndTag = false;
        }
        else if (!hasElemFields) {
            this.serializeCompactRDFAttrProps(node, indent + 1);
            this.write("/>");
            this.writeNewline();
            emitEndTag = false;
        }
        else if (!hasAttrFields) {
            this.write(" rdf:parseType=\"Resource\">");
            this.writeNewline();
            this.serializeCompactRDFElementProps(node, indent + 1);
        }
        else {
            this.write(62);
            this.writeNewline();
            this.writeIndent(indent + 1);
            this.write("<rdf:Description");
            this.serializeCompactRDFAttrProps(node, indent + 2);
            this.write(">");
            this.writeNewline();
            this.serializeCompactRDFElementProps(node, indent + 1);
            this.writeIndent(indent + 1);
            this.write("</rdf:Description>");
            this.writeNewline();
        }
        return emitEndTag;
    }
    
    private void serializeCompactRDFGeneralQualifier(final int indent, final XMPNode node) throws IOException, XMPException {
        this.write(" rdf:parseType=\"Resource\">");
        this.writeNewline();
        this.serializeCanonicalRDFProperty(node, false, true, indent + 1);
        final Iterator iq = node.iterateQualifier();
        while (iq.hasNext()) {
            final XMPNode qualifier = iq.next();
            this.serializeCanonicalRDFProperty(qualifier, false, false, indent + 1);
        }
    }
    
    private void serializeCanonicalRDFSchema(final XMPNode schemaNode, final int level) throws IOException, XMPException {
        final Iterator it = schemaNode.iterateChildren();
        while (it.hasNext()) {
            final XMPNode propNode = it.next();
            this.serializeCanonicalRDFProperty(propNode, this.options.getUseCanonicalFormat(), false, level + 2);
        }
    }
    
    private void declareUsedNamespaces(final XMPNode node, final Set usedPrefixes, final int indent) throws IOException {
        if (node.getOptions().isSchemaNode()) {
            final String prefix = node.getValue().substring(0, node.getValue().length() - 1);
            this.declareNamespace(prefix, node.getName(), usedPrefixes, indent);
        }
        else if (node.getOptions().isStruct()) {
            final Iterator it = node.iterateChildren();
            while (it.hasNext()) {
                final XMPNode field = it.next();
                this.declareNamespace(field.getName(), null, usedPrefixes, indent);
            }
        }
        Iterator it = node.iterateChildren();
        while (it.hasNext()) {
            final XMPNode child = it.next();
            this.declareUsedNamespaces(child, usedPrefixes, indent);
        }
        it = node.iterateQualifier();
        while (it.hasNext()) {
            final XMPNode qualifier = it.next();
            this.declareNamespace(qualifier.getName(), null, usedPrefixes, indent);
            this.declareUsedNamespaces(qualifier, usedPrefixes, indent);
        }
    }
    
    private void declareNamespace(String prefix, String namespace, final Set usedPrefixes, final int indent) throws IOException {
        if (namespace == null) {
            final QName qname = new QName(prefix);
            if (!qname.hasPrefix()) {
                return;
            }
            prefix = qname.getPrefix();
            namespace = XMPMetaFactory.getSchemaRegistry().getNamespaceURI(prefix + ":");
            this.declareNamespace(prefix, namespace, usedPrefixes, indent);
        }
        if (!usedPrefixes.contains(prefix)) {
            this.writeNewline();
            this.writeIndent(indent);
            this.write("xmlns:");
            this.write(prefix);
            this.write("=\"");
            this.write(namespace);
            this.write(34);
            usedPrefixes.add(prefix);
        }
    }
    
    private void startOuterRDFDescription(final XMPNode schemaNode, final int level) throws IOException {
        this.writeIndent(level + 1);
        this.write("<rdf:Description rdf:about=");
        this.writeTreeName();
        final Set usedPrefixes = new HashSet();
        usedPrefixes.add("xml");
        usedPrefixes.add("rdf");
        this.declareUsedNamespaces(schemaNode, usedPrefixes, level + 3);
        this.write(62);
        this.writeNewline();
    }
    
    private void endOuterRDFDescription(final int level) throws IOException {
        this.writeIndent(level + 1);
        this.write("</rdf:Description>");
        this.writeNewline();
    }
    
    private void serializeCanonicalRDFProperty(final XMPNode node, final boolean useCanonicalRDF, final boolean emitAsRDFValue, int indent) throws IOException, XMPException {
        boolean emitEndTag = true;
        boolean indentEndTag = true;
        String elemName = node.getName();
        if (emitAsRDFValue) {
            elemName = "rdf:value";
        }
        else if ("[]".equals(elemName)) {
            elemName = "rdf:li";
        }
        this.writeIndent(indent);
        this.write(60);
        this.write(elemName);
        boolean hasGeneralQualifiers = false;
        boolean hasRDFResourceQual = false;
        Iterator it = node.iterateQualifier();
        while (it.hasNext()) {
            final XMPNode qualifier = it.next();
            if (!XMPSerializerRDF.RDF_ATTR_QUALIFIER.contains(qualifier.getName())) {
                hasGeneralQualifiers = true;
            }
            else {
                hasRDFResourceQual = "rdf:resource".equals(qualifier.getName());
                if (emitAsRDFValue) {
                    continue;
                }
                this.write(32);
                this.write(qualifier.getName());
                this.write("=\"");
                this.appendNodeValue(qualifier.getValue(), true);
                this.write(34);
            }
        }
        if (hasGeneralQualifiers && !emitAsRDFValue) {
            if (hasRDFResourceQual) {
                throw new XMPException("Can't mix rdf:resource and general qualifiers", 202);
            }
            if (useCanonicalRDF) {
                this.write(">");
                this.writeNewline();
                ++indent;
                this.writeIndent(indent);
                this.write("<rdf:Description");
                this.write(">");
            }
            else {
                this.write(" rdf:parseType=\"Resource\">");
            }
            this.writeNewline();
            this.serializeCanonicalRDFProperty(node, useCanonicalRDF, true, indent + 1);
            it = node.iterateQualifier();
            while (it.hasNext()) {
                final XMPNode qualifier = it.next();
                if (!XMPSerializerRDF.RDF_ATTR_QUALIFIER.contains(qualifier.getName())) {
                    this.serializeCanonicalRDFProperty(qualifier, useCanonicalRDF, false, indent + 1);
                }
            }
            if (useCanonicalRDF) {
                this.writeIndent(indent);
                this.write("</rdf:Description>");
                this.writeNewline();
                --indent;
            }
        }
        else if (!node.getOptions().isCompositeProperty()) {
            if (node.getOptions().isURI()) {
                this.write(" rdf:resource=\"");
                this.appendNodeValue(node.getValue(), true);
                this.write("\"/>");
                this.writeNewline();
                emitEndTag = false;
            }
            else if (node.getValue() == null || "".equals(node.getValue())) {
                this.write("/>");
                this.writeNewline();
                emitEndTag = false;
            }
            else {
                this.write(62);
                this.appendNodeValue(node.getValue(), false);
                indentEndTag = false;
            }
        }
        else if (node.getOptions().isArray()) {
            this.write(62);
            this.writeNewline();
            this.emitRDFArrayTag(node, true, indent + 1);
            if (node.getOptions().isArrayAltText()) {
                XMPNodeUtils.normalizeLangArray(node);
            }
            it = node.iterateChildren();
            while (it.hasNext()) {
                final XMPNode child = it.next();
                this.serializeCanonicalRDFProperty(child, useCanonicalRDF, false, indent + 2);
            }
            this.emitRDFArrayTag(node, false, indent + 1);
        }
        else if (!hasRDFResourceQual) {
            if (!node.hasChildren()) {
                if (useCanonicalRDF) {
                    this.write(">");
                    this.writeNewline();
                    this.writeIndent(indent + 1);
                    this.write("<rdf:Description/>");
                }
                else {
                    this.write(" rdf:parseType=\"Resource\"/>");
                    emitEndTag = false;
                }
                this.writeNewline();
            }
            else {
                if (useCanonicalRDF) {
                    this.write(">");
                    this.writeNewline();
                    ++indent;
                    this.writeIndent(indent);
                    this.write("<rdf:Description");
                    this.write(">");
                }
                else {
                    this.write(" rdf:parseType=\"Resource\">");
                }
                this.writeNewline();
                it = node.iterateChildren();
                while (it.hasNext()) {
                    final XMPNode child = it.next();
                    this.serializeCanonicalRDFProperty(child, useCanonicalRDF, false, indent + 1);
                }
                if (useCanonicalRDF) {
                    this.writeIndent(indent);
                    this.write("</rdf:Description>");
                    this.writeNewline();
                    --indent;
                }
            }
        }
        else {
            it = node.iterateChildren();
            while (it.hasNext()) {
                final XMPNode child = it.next();
                if (!this.canBeRDFAttrProp(child)) {
                    throw new XMPException("Can't mix rdf:resource and complex fields", 202);
                }
                this.writeNewline();
                this.writeIndent(indent + 1);
                this.write(32);
                this.write(child.getName());
                this.write("=\"");
                this.appendNodeValue(child.getValue(), true);
                this.write(34);
            }
            this.write("/>");
            this.writeNewline();
            emitEndTag = false;
        }
        if (emitEndTag) {
            if (indentEndTag) {
                this.writeIndent(indent);
            }
            this.write("</");
            this.write(elemName);
            this.write(62);
            this.writeNewline();
        }
    }
    
    private void emitRDFArrayTag(final XMPNode arrayNode, final boolean isStartTag, final int indent) throws IOException {
        if (isStartTag || arrayNode.hasChildren()) {
            this.writeIndent(indent);
            this.write(isStartTag ? "<rdf:" : "</rdf:");
            if (arrayNode.getOptions().isArrayAlternate()) {
                this.write("Alt");
            }
            else if (arrayNode.getOptions().isArrayOrdered()) {
                this.write("Seq");
            }
            else {
                this.write("Bag");
            }
            if (isStartTag && !arrayNode.hasChildren()) {
                this.write("/>");
            }
            else {
                this.write(">");
            }
            this.writeNewline();
        }
    }
    
    private void appendNodeValue(String value, final boolean forAttribute) throws IOException {
        if (value == null) {
            value = "";
        }
        this.write(Utils.escapeXML(value, forAttribute, true));
    }
    
    private boolean canBeRDFAttrProp(final XMPNode node) {
        return !node.hasQualifier() && !node.getOptions().isURI() && !node.getOptions().isCompositeProperty() && !"[]".equals(node.getName());
    }
    
    private void writeIndent(final int times) throws IOException {
        for (int i = this.options.getBaseIndent() + times; i > 0; --i) {
            this.writer.write(this.options.getIndent());
        }
    }
    
    private void write(final int c) throws IOException {
        this.writer.write(c);
    }
    
    private void write(final String str) throws IOException {
        this.writer.write(str);
    }
    
    private void writeChars(int number, final char c) throws IOException {
        while (number > 0) {
            this.writer.write(c);
            --number;
        }
    }
    
    private void writeNewline() throws IOException {
        this.writer.write(this.options.getNewline());
    }
    
    static {
        RDF_ATTR_QUALIFIER = new HashSet(Arrays.asList("xml:lang", "rdf:resource", "rdf:ID", "rdf:bagID", "rdf:nodeID"));
    }
}
