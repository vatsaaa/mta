// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import com.adobe.xmp.options.AliasOptions;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import com.adobe.xmp.properties.XMPAliasInfo;
import java.util.Collections;
import java.util.TreeMap;
import com.adobe.xmp.XMPException;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.Map;
import com.adobe.xmp.XMPConst;
import com.adobe.xmp.XMPSchemaRegistry;

public final class XMPSchemaRegistryImpl implements XMPSchemaRegistry, XMPConst
{
    private Map namespaceToPrefixMap;
    private Map prefixToNamespaceMap;
    private Map aliasMap;
    private Pattern p;
    
    public XMPSchemaRegistryImpl() {
        this.namespaceToPrefixMap = new HashMap();
        this.prefixToNamespaceMap = new HashMap();
        this.aliasMap = new HashMap();
        this.p = Pattern.compile("[/*?\\[\\]]");
        try {
            this.registerStandardNamespaces();
            this.registerStandardAliases();
        }
        catch (XMPException e) {
            throw new RuntimeException("The XMPSchemaRegistry cannot be initialized!");
        }
    }
    
    public synchronized String registerNamespace(final String namespaceURI, String suggestedPrefix) throws XMPException {
        ParameterAsserts.assertSchemaNS(namespaceURI);
        ParameterAsserts.assertPrefix(suggestedPrefix);
        if (suggestedPrefix.charAt(suggestedPrefix.length() - 1) != ':') {
            suggestedPrefix += ':';
        }
        if (!Utils.isXMLNameNS(suggestedPrefix.substring(0, suggestedPrefix.length() - 1))) {
            throw new XMPException("The prefix is a bad XML name", 201);
        }
        final String registeredPrefix = this.namespaceToPrefixMap.get(namespaceURI);
        final String registeredNS = this.prefixToNamespaceMap.get(suggestedPrefix);
        if (registeredPrefix != null) {
            return registeredPrefix;
        }
        if (registeredNS != null) {
            String generatedPrefix = suggestedPrefix;
            for (int i = 1; this.prefixToNamespaceMap.containsKey(generatedPrefix); generatedPrefix = suggestedPrefix.substring(0, suggestedPrefix.length() - 1) + "_" + i + "_:", ++i) {}
            suggestedPrefix = generatedPrefix;
        }
        this.prefixToNamespaceMap.put(suggestedPrefix, namespaceURI);
        this.namespaceToPrefixMap.put(namespaceURI, suggestedPrefix);
        return suggestedPrefix;
    }
    
    public synchronized void deleteNamespace(final String namespaceURI) {
        final String prefixToDelete = this.getNamespacePrefix(namespaceURI);
        if (prefixToDelete != null) {
            this.namespaceToPrefixMap.remove(namespaceURI);
            this.prefixToNamespaceMap.remove(prefixToDelete);
        }
    }
    
    public synchronized String getNamespacePrefix(final String namespaceURI) {
        return this.namespaceToPrefixMap.get(namespaceURI);
    }
    
    public synchronized String getNamespaceURI(String namespacePrefix) {
        if (namespacePrefix != null && !namespacePrefix.endsWith(":")) {
            namespacePrefix += ":";
        }
        return this.prefixToNamespaceMap.get(namespacePrefix);
    }
    
    public synchronized Map getNamespaces() {
        return Collections.unmodifiableMap((Map<?, ?>)new TreeMap<Object, Object>(this.namespaceToPrefixMap));
    }
    
    public synchronized Map getPrefixes() {
        return Collections.unmodifiableMap((Map<?, ?>)new TreeMap<Object, Object>(this.prefixToNamespaceMap));
    }
    
    private void registerStandardNamespaces() throws XMPException {
        this.registerNamespace("http://www.w3.org/XML/1998/namespace", "xml");
        this.registerNamespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf");
        this.registerNamespace("http://purl.org/dc/elements/1.1/", "dc");
        this.registerNamespace("http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/", "Iptc4xmpCore");
        this.registerNamespace("http://iptc.org/std/Iptc4xmpExt/2008-02-29/", "Iptc4xmpExt");
        this.registerNamespace("http://ns.adobe.com/DICOM/", "DICOM");
        this.registerNamespace("http://ns.useplus.org/ldf/xmp/1.0/", "plus");
        this.registerNamespace("adobe:ns:meta/", "x");
        this.registerNamespace("http://ns.adobe.com/iX/1.0/", "iX");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/", "xmp");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/rights/", "xmpRights");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/mm/", "xmpMM");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/bj/", "xmpBJ");
        this.registerNamespace("http://ns.adobe.com/xmp/note/", "xmpNote");
        this.registerNamespace("http://ns.adobe.com/pdf/1.3/", "pdf");
        this.registerNamespace("http://ns.adobe.com/pdfx/1.3/", "pdfx");
        this.registerNamespace("http://www.npes.org/pdfx/ns/id/", "pdfxid");
        this.registerNamespace("http://www.aiim.org/pdfa/ns/schema#", "pdfaSchema");
        this.registerNamespace("http://www.aiim.org/pdfa/ns/property#", "pdfaProperty");
        this.registerNamespace("http://www.aiim.org/pdfa/ns/type#", "pdfaType");
        this.registerNamespace("http://www.aiim.org/pdfa/ns/field#", "pdfaField");
        this.registerNamespace("http://www.aiim.org/pdfa/ns/id/", "pdfaid");
        this.registerNamespace("http://www.aiim.org/pdfa/ns/extension/", "pdfaExtension");
        this.registerNamespace("http://ns.adobe.com/photoshop/1.0/", "photoshop");
        this.registerNamespace("http://ns.adobe.com/album/1.0/", "album");
        this.registerNamespace("http://ns.adobe.com/exif/1.0/", "exif");
        this.registerNamespace("http://cipa.jp/exif/1.0/", "exifEX");
        this.registerNamespace("http://ns.adobe.com/exif/1.0/aux/", "aux");
        this.registerNamespace("http://ns.adobe.com/tiff/1.0/", "tiff");
        this.registerNamespace("http://ns.adobe.com/png/1.0/", "png");
        this.registerNamespace("http://ns.adobe.com/jpeg/1.0/", "jpeg");
        this.registerNamespace("http://ns.adobe.com/jp2k/1.0/", "jp2k");
        this.registerNamespace("http://ns.adobe.com/camera-raw-settings/1.0/", "crs");
        this.registerNamespace("http://ns.adobe.com/StockPhoto/1.0/", "bmsp");
        this.registerNamespace("http://ns.adobe.com/creatorAtom/1.0/", "creatorAtom");
        this.registerNamespace("http://ns.adobe.com/asf/1.0/", "asf");
        this.registerNamespace("http://ns.adobe.com/xmp/wav/1.0/", "wav");
        this.registerNamespace("http://ns.adobe.com/bwf/bext/1.0/", "bext");
        this.registerNamespace("http://ns.adobe.com/riff/info/", "riffinfo");
        this.registerNamespace("http://ns.adobe.com/xmp/1.0/Script/", "xmpScript");
        this.registerNamespace("http://ns.adobe.com/TransformXMP/", "txmp");
        this.registerNamespace("http://ns.adobe.com/swf/1.0/", "swf");
        this.registerNamespace("http://ns.adobe.com/xmp/1.0/DynamicMedia/", "xmpDM");
        this.registerNamespace("http://ns.adobe.com/xmp/transient/1.0/", "xmpx");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/t/", "xmpT");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/t/pg/", "xmpTPg");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/g/", "xmpG");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/g/img/", "xmpGImg");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/sType/Font#", "stFnt");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/sType/Dimensions#", "stDim");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/sType/ResourceEvent#", "stEvt");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/sType/ResourceRef#", "stRef");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/sType/Version#", "stVer");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/sType/Job#", "stJob");
        this.registerNamespace("http://ns.adobe.com/xap/1.0/sType/ManifestItem#", "stMfs");
        this.registerNamespace("http://ns.adobe.com/xmp/Identifier/qual/1.0/", "xmpidq");
    }
    
    public synchronized XMPAliasInfo resolveAlias(final String aliasNS, final String aliasProp) {
        final String aliasPrefix = this.getNamespacePrefix(aliasNS);
        if (aliasPrefix == null) {
            return null;
        }
        return this.aliasMap.get(aliasPrefix + aliasProp);
    }
    
    public synchronized XMPAliasInfo findAlias(final String qname) {
        return this.aliasMap.get(qname);
    }
    
    public synchronized XMPAliasInfo[] findAliases(final String aliasNS) {
        final String prefix = this.getNamespacePrefix(aliasNS);
        final List result = new ArrayList();
        if (prefix != null) {
            for (final String qname : this.aliasMap.keySet()) {
                if (qname.startsWith(prefix)) {
                    result.add(this.findAlias(qname));
                }
            }
        }
        return result.toArray(new XMPAliasInfo[result.size()]);
    }
    
    synchronized void registerAlias(final String aliasNS, final String aliasProp, final String actualNS, final String actualProp, final AliasOptions aliasForm) throws XMPException {
        ParameterAsserts.assertSchemaNS(aliasNS);
        ParameterAsserts.assertPropName(aliasProp);
        ParameterAsserts.assertSchemaNS(actualNS);
        ParameterAsserts.assertPropName(actualProp);
        final AliasOptions aliasOpts = (aliasForm != null) ? new AliasOptions(XMPNodeUtils.verifySetOptions(aliasForm.toPropertyOptions(), null).getOptions()) : new AliasOptions();
        if (this.p.matcher(aliasProp).find() || this.p.matcher(actualProp).find()) {
            throw new XMPException("Alias and actual property names must be simple", 102);
        }
        final String aliasPrefix = this.getNamespacePrefix(aliasNS);
        final String actualPrefix = this.getNamespacePrefix(actualNS);
        if (aliasPrefix == null) {
            throw new XMPException("Alias namespace is not registered", 101);
        }
        if (actualPrefix == null) {
            throw new XMPException("Actual namespace is not registered", 101);
        }
        final String key = aliasPrefix + aliasProp;
        if (this.aliasMap.containsKey(key)) {
            throw new XMPException("Alias is already existing", 4);
        }
        if (this.aliasMap.containsKey(actualPrefix + actualProp)) {
            throw new XMPException("Actual property is already an alias, use the base property", 4);
        }
        final XMPAliasInfo aliasInfo = new XMPAliasInfo() {
            public String getNamespace() {
                return actualNS;
            }
            
            public String getPrefix() {
                return actualPrefix;
            }
            
            public String getPropName() {
                return actualProp;
            }
            
            public AliasOptions getAliasForm() {
                return aliasOpts;
            }
            
            @Override
            public String toString() {
                return actualPrefix + actualProp + " NS(" + actualNS + "), FORM (" + this.getAliasForm() + ")";
            }
        };
        this.aliasMap.put(key, aliasInfo);
    }
    
    public synchronized Map getAliases() {
        return Collections.unmodifiableMap((Map<?, ?>)new TreeMap<Object, Object>(this.aliasMap));
    }
    
    private void registerStandardAliases() throws XMPException {
        final AliasOptions aliasToArrayOrdered = new AliasOptions().setArrayOrdered(true);
        final AliasOptions aliasToArrayAltText = new AliasOptions().setArrayAltText(true);
        this.registerAlias("http://ns.adobe.com/xap/1.0/", "Author", "http://purl.org/dc/elements/1.1/", "creator", aliasToArrayOrdered);
        this.registerAlias("http://ns.adobe.com/xap/1.0/", "Authors", "http://purl.org/dc/elements/1.1/", "creator", null);
        this.registerAlias("http://ns.adobe.com/xap/1.0/", "Description", "http://purl.org/dc/elements/1.1/", "description", null);
        this.registerAlias("http://ns.adobe.com/xap/1.0/", "Format", "http://purl.org/dc/elements/1.1/", "format", null);
        this.registerAlias("http://ns.adobe.com/xap/1.0/", "Keywords", "http://purl.org/dc/elements/1.1/", "subject", null);
        this.registerAlias("http://ns.adobe.com/xap/1.0/", "Locale", "http://purl.org/dc/elements/1.1/", "language", null);
        this.registerAlias("http://ns.adobe.com/xap/1.0/", "Title", "http://purl.org/dc/elements/1.1/", "title", null);
        this.registerAlias("http://ns.adobe.com/xap/1.0/rights/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", null);
        this.registerAlias("http://ns.adobe.com/pdf/1.3/", "Author", "http://purl.org/dc/elements/1.1/", "creator", aliasToArrayOrdered);
        this.registerAlias("http://ns.adobe.com/pdf/1.3/", "BaseURL", "http://ns.adobe.com/xap/1.0/", "BaseURL", null);
        this.registerAlias("http://ns.adobe.com/pdf/1.3/", "CreationDate", "http://ns.adobe.com/xap/1.0/", "CreateDate", null);
        this.registerAlias("http://ns.adobe.com/pdf/1.3/", "Creator", "http://ns.adobe.com/xap/1.0/", "CreatorTool", null);
        this.registerAlias("http://ns.adobe.com/pdf/1.3/", "ModDate", "http://ns.adobe.com/xap/1.0/", "ModifyDate", null);
        this.registerAlias("http://ns.adobe.com/pdf/1.3/", "Subject", "http://purl.org/dc/elements/1.1/", "description", aliasToArrayAltText);
        this.registerAlias("http://ns.adobe.com/pdf/1.3/", "Title", "http://purl.org/dc/elements/1.1/", "title", aliasToArrayAltText);
        this.registerAlias("http://ns.adobe.com/photoshop/1.0/", "Author", "http://purl.org/dc/elements/1.1/", "creator", aliasToArrayOrdered);
        this.registerAlias("http://ns.adobe.com/photoshop/1.0/", "Caption", "http://purl.org/dc/elements/1.1/", "description", aliasToArrayAltText);
        this.registerAlias("http://ns.adobe.com/photoshop/1.0/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", aliasToArrayAltText);
        this.registerAlias("http://ns.adobe.com/photoshop/1.0/", "Keywords", "http://purl.org/dc/elements/1.1/", "subject", null);
        this.registerAlias("http://ns.adobe.com/photoshop/1.0/", "Marked", "http://ns.adobe.com/xap/1.0/rights/", "Marked", null);
        this.registerAlias("http://ns.adobe.com/photoshop/1.0/", "Title", "http://purl.org/dc/elements/1.1/", "title", aliasToArrayAltText);
        this.registerAlias("http://ns.adobe.com/photoshop/1.0/", "WebStatement", "http://ns.adobe.com/xap/1.0/rights/", "WebStatement", null);
        this.registerAlias("http://ns.adobe.com/tiff/1.0/", "Artist", "http://purl.org/dc/elements/1.1/", "creator", aliasToArrayOrdered);
        this.registerAlias("http://ns.adobe.com/tiff/1.0/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", null);
        this.registerAlias("http://ns.adobe.com/tiff/1.0/", "DateTime", "http://ns.adobe.com/xap/1.0/", "ModifyDate", null);
        this.registerAlias("http://ns.adobe.com/tiff/1.0/", "ImageDescription", "http://purl.org/dc/elements/1.1/", "description", null);
        this.registerAlias("http://ns.adobe.com/tiff/1.0/", "Software", "http://ns.adobe.com/xap/1.0/", "CreatorTool", null);
        this.registerAlias("http://ns.adobe.com/png/1.0/", "Author", "http://purl.org/dc/elements/1.1/", "creator", aliasToArrayOrdered);
        this.registerAlias("http://ns.adobe.com/png/1.0/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", aliasToArrayAltText);
        this.registerAlias("http://ns.adobe.com/png/1.0/", "CreationTime", "http://ns.adobe.com/xap/1.0/", "CreateDate", null);
        this.registerAlias("http://ns.adobe.com/png/1.0/", "Description", "http://purl.org/dc/elements/1.1/", "description", aliasToArrayAltText);
        this.registerAlias("http://ns.adobe.com/png/1.0/", "ModificationTime", "http://ns.adobe.com/xap/1.0/", "ModifyDate", null);
        this.registerAlias("http://ns.adobe.com/png/1.0/", "Software", "http://ns.adobe.com/xap/1.0/", "CreatorTool", null);
        this.registerAlias("http://ns.adobe.com/png/1.0/", "Title", "http://purl.org/dc/elements/1.1/", "title", aliasToArrayAltText);
    }
}
