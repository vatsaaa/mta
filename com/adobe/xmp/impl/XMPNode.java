// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.util.Collection;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.options.PropertyOptions;
import java.util.List;

class XMPNode implements Comparable
{
    private String name;
    private String value;
    private XMPNode parent;
    private List children;
    private List qualifier;
    private PropertyOptions options;
    private boolean implicit;
    private boolean hasAliases;
    private boolean alias;
    private boolean hasValueChild;
    
    public XMPNode(final String name, final String value, final PropertyOptions options) {
        this.children = null;
        this.qualifier = null;
        this.options = null;
        this.name = name;
        this.value = value;
        this.options = options;
    }
    
    public XMPNode(final String name, final PropertyOptions options) {
        this(name, null, options);
    }
    
    public void clear() {
        this.options = null;
        this.name = null;
        this.value = null;
        this.children = null;
        this.qualifier = null;
    }
    
    public XMPNode getParent() {
        return this.parent;
    }
    
    public XMPNode getChild(final int index) {
        return this.getChildren().get(index - 1);
    }
    
    public void addChild(final XMPNode node) throws XMPException {
        this.assertChildNotExisting(node.getName());
        node.setParent(this);
        this.getChildren().add(node);
    }
    
    public void addChild(final int index, final XMPNode node) throws XMPException {
        this.assertChildNotExisting(node.getName());
        node.setParent(this);
        this.getChildren().add(index - 1, node);
    }
    
    public void replaceChild(final int index, final XMPNode node) {
        node.setParent(this);
        this.getChildren().set(index - 1, node);
    }
    
    public void removeChild(final int itemIndex) {
        this.getChildren().remove(itemIndex - 1);
        this.cleanupChildren();
    }
    
    public void removeChild(final XMPNode node) {
        this.getChildren().remove(node);
        this.cleanupChildren();
    }
    
    protected void cleanupChildren() {
        if (this.children.isEmpty()) {
            this.children = null;
        }
    }
    
    public void removeChildren() {
        this.children = null;
    }
    
    public int getChildrenLength() {
        return (this.children != null) ? this.children.size() : 0;
    }
    
    public XMPNode findChildByName(final String expr) {
        return this.find(this.getChildren(), expr);
    }
    
    public XMPNode getQualifier(final int index) {
        return this.getQualifier().get(index - 1);
    }
    
    public int getQualifierLength() {
        return (this.qualifier != null) ? this.qualifier.size() : 0;
    }
    
    public void addQualifier(final XMPNode qualNode) throws XMPException {
        this.assertQualifierNotExisting(qualNode.getName());
        qualNode.setParent(this);
        qualNode.getOptions().setQualifier(true);
        this.getOptions().setHasQualifiers(true);
        if (qualNode.isLanguageNode()) {
            this.options.setHasLanguage(true);
            this.getQualifier().add(0, qualNode);
        }
        else if (qualNode.isTypeNode()) {
            this.options.setHasType(true);
            this.getQualifier().add(this.options.getHasLanguage() ? 1 : 0, qualNode);
        }
        else {
            this.getQualifier().add(qualNode);
        }
    }
    
    public void removeQualifier(final XMPNode qualNode) {
        final PropertyOptions opts = this.getOptions();
        if (qualNode.isLanguageNode()) {
            opts.setHasLanguage(false);
        }
        else if (qualNode.isTypeNode()) {
            opts.setHasType(false);
        }
        this.getQualifier().remove(qualNode);
        if (this.qualifier.isEmpty()) {
            opts.setHasQualifiers(false);
            this.qualifier = null;
        }
    }
    
    public void removeQualifiers() {
        final PropertyOptions opts = this.getOptions();
        opts.setHasQualifiers(false);
        opts.setHasLanguage(false);
        opts.setHasType(false);
        this.qualifier = null;
    }
    
    public XMPNode findQualifierByName(final String expr) {
        return this.find(this.qualifier, expr);
    }
    
    public boolean hasChildren() {
        return this.children != null && this.children.size() > 0;
    }
    
    public Iterator iterateChildren() {
        if (this.children != null) {
            return this.getChildren().iterator();
        }
        return Collections.EMPTY_LIST.listIterator();
    }
    
    public boolean hasQualifier() {
        return this.qualifier != null && this.qualifier.size() > 0;
    }
    
    public Iterator iterateQualifier() {
        if (this.qualifier != null) {
            final Iterator it = this.getQualifier().iterator();
            return new Iterator() {
                public boolean hasNext() {
                    return it.hasNext();
                }
                
                public Object next() {
                    return it.next();
                }
                
                public void remove() {
                    throw new UnsupportedOperationException("remove() is not allowed due to the internal contraints");
                }
            };
        }
        return Collections.EMPTY_LIST.iterator();
    }
    
    public Object clone() {
        PropertyOptions newOptions;
        try {
            newOptions = new PropertyOptions(this.getOptions().getOptions());
        }
        catch (XMPException e) {
            newOptions = new PropertyOptions();
        }
        final XMPNode newNode = new XMPNode(this.name, this.value, newOptions);
        this.cloneSubtree(newNode);
        return newNode;
    }
    
    public void cloneSubtree(final XMPNode destination) {
        try {
            Iterator it = this.iterateChildren();
            while (it.hasNext()) {
                final XMPNode child = it.next();
                destination.addChild((XMPNode)child.clone());
            }
            it = this.iterateQualifier();
            while (it.hasNext()) {
                final XMPNode qualifier = it.next();
                destination.addQualifier((XMPNode)qualifier.clone());
            }
        }
        catch (XMPException e) {
            assert false;
        }
    }
    
    public String dumpNode(final boolean recursive) {
        final StringBuffer result = new StringBuffer(512);
        this.dumpNode(result, recursive, 0, 0);
        return result.toString();
    }
    
    public int compareTo(final Object xmpNode) {
        if (this.getOptions().isSchemaNode()) {
            return this.value.compareTo(((XMPNode)xmpNode).getValue());
        }
        return this.name.compareTo(((XMPNode)xmpNode).getName());
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setValue(final String value) {
        this.value = value;
    }
    
    public PropertyOptions getOptions() {
        if (this.options == null) {
            this.options = new PropertyOptions();
        }
        return this.options;
    }
    
    public void setOptions(final PropertyOptions options) {
        this.options = options;
    }
    
    public boolean isImplicit() {
        return this.implicit;
    }
    
    public void setImplicit(final boolean implicit) {
        this.implicit = implicit;
    }
    
    public boolean getHasAliases() {
        return this.hasAliases;
    }
    
    public void setHasAliases(final boolean hasAliases) {
        this.hasAliases = hasAliases;
    }
    
    public boolean isAlias() {
        return this.alias;
    }
    
    public void setAlias(final boolean alias) {
        this.alias = alias;
    }
    
    public boolean getHasValueChild() {
        return this.hasValueChild;
    }
    
    public void setHasValueChild(final boolean hasValueChild) {
        this.hasValueChild = hasValueChild;
    }
    
    public void sort() {
        if (this.hasQualifier()) {
            XMPNode[] quals;
            int sortFrom;
            for (quals = this.getQualifier().toArray(new XMPNode[this.getQualifierLength()]), sortFrom = 0; quals.length > sortFrom && ("xml:lang".equals(quals[sortFrom].getName()) || "rdf:type".equals(quals[sortFrom].getName())); ++sortFrom) {
                quals[sortFrom].sort();
            }
            Arrays.sort(quals, sortFrom, quals.length);
            final ListIterator it = this.qualifier.listIterator();
            for (int j = 0; j < quals.length; ++j) {
                it.next();
                it.set(quals[j]);
                quals[j].sort();
            }
        }
        if (this.hasChildren()) {
            if (!this.getOptions().isArray()) {
                Collections.sort((List<Comparable>)this.children);
            }
            final Iterator it2 = this.iterateChildren();
            while (it2.hasNext()) {
                it2.next().sort();
            }
        }
    }
    
    private void dumpNode(final StringBuffer result, final boolean recursive, final int indent, final int index) {
        for (int i = 0; i < indent; ++i) {
            result.append('\t');
        }
        if (this.parent != null) {
            if (this.getOptions().isQualifier()) {
                result.append('?');
                result.append(this.name);
            }
            else if (this.getParent().getOptions().isArray()) {
                result.append('[');
                result.append(index);
                result.append(']');
            }
            else {
                result.append(this.name);
            }
        }
        else {
            result.append("ROOT NODE");
            if (this.name != null && this.name.length() > 0) {
                result.append(" (");
                result.append(this.name);
                result.append(')');
            }
        }
        if (this.value != null && this.value.length() > 0) {
            result.append(" = \"");
            result.append(this.value);
            result.append('\"');
        }
        if (this.getOptions().containsOneOf(-1)) {
            result.append("\t(");
            result.append(this.getOptions().toString());
            result.append(" : ");
            result.append(this.getOptions().getOptionsString());
            result.append(')');
        }
        result.append('\n');
        if (recursive && this.hasQualifier()) {
            XMPNode[] quals;
            int j;
            for (quals = this.getQualifier().toArray(new XMPNode[this.getQualifierLength()]), j = 0; quals.length > j && ("xml:lang".equals(quals[j].getName()) || "rdf:type".equals(quals[j].getName())); ++j) {}
            Arrays.sort(quals, j, quals.length);
            for (j = 0; j < quals.length; ++j) {
                final XMPNode qualifier = quals[j];
                qualifier.dumpNode(result, recursive, indent + 2, j + 1);
            }
        }
        if (recursive && this.hasChildren()) {
            final XMPNode[] children = this.getChildren().toArray(new XMPNode[this.getChildrenLength()]);
            if (!this.getOptions().isArray()) {
                Arrays.sort(children);
            }
            for (int j = 0; j < children.length; ++j) {
                final XMPNode child = children[j];
                child.dumpNode(result, recursive, indent + 1, j + 1);
            }
        }
    }
    
    private boolean isLanguageNode() {
        return "xml:lang".equals(this.name);
    }
    
    private boolean isTypeNode() {
        return "rdf:type".equals(this.name);
    }
    
    private List getChildren() {
        if (this.children == null) {
            this.children = new ArrayList(0);
        }
        return this.children;
    }
    
    public List getUnmodifiableChildren() {
        return Collections.unmodifiableList((List<?>)new ArrayList<Object>(this.getChildren()));
    }
    
    private List getQualifier() {
        if (this.qualifier == null) {
            this.qualifier = new ArrayList(0);
        }
        return this.qualifier;
    }
    
    protected void setParent(final XMPNode parent) {
        this.parent = parent;
    }
    
    private XMPNode find(final List list, final String expr) {
        if (list != null) {
            for (final XMPNode child : list) {
                if (child.getName().equals(expr)) {
                    return child;
                }
            }
        }
        return null;
    }
    
    private void assertChildNotExisting(final String childName) throws XMPException {
        if (!"[]".equals(childName) && this.findChildByName(childName) != null) {
            throw new XMPException("Duplicate property or field node '" + childName + "'", 203);
        }
    }
    
    private void assertQualifierNotExisting(final String qualifierName) throws XMPException {
        if (!"[]".equals(qualifierName) && this.findQualifierByName(qualifierName) != null) {
            throw new XMPException("Duplicate '" + qualifierName + "' qualifier", 203);
        }
    }
}
