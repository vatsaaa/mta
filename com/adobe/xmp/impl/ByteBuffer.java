// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteBuffer
{
    private byte[] buffer;
    private int length;
    private String encoding;
    
    public ByteBuffer(final int initialCapacity) {
        this.encoding = null;
        this.buffer = new byte[initialCapacity];
        this.length = 0;
    }
    
    public ByteBuffer(final byte[] buffer) {
        this.encoding = null;
        this.buffer = buffer;
        this.length = buffer.length;
    }
    
    public ByteBuffer(final byte[] buffer, final int length) {
        this.encoding = null;
        if (length > buffer.length) {
            throw new ArrayIndexOutOfBoundsException("Valid length exceeds the buffer length.");
        }
        this.buffer = buffer;
        this.length = length;
    }
    
    public ByteBuffer(final InputStream in) throws IOException {
        this.encoding = null;
        final int chunk = 16384;
        this.length = 0;
        this.buffer = new byte[chunk];
        int read;
        while ((read = in.read(this.buffer, this.length, chunk)) > 0) {
            this.length += read;
            if (read != chunk) {
                break;
            }
            this.ensureCapacity(this.length + chunk);
        }
    }
    
    public ByteBuffer(final byte[] buffer, final int offset, final int length) {
        this.encoding = null;
        if (length > buffer.length - offset) {
            throw new ArrayIndexOutOfBoundsException("Valid length exceeds the buffer length.");
        }
        System.arraycopy(buffer, offset, this.buffer = new byte[length], 0, length);
        this.length = length;
    }
    
    public InputStream getByteStream() {
        return new ByteArrayInputStream(this.buffer, 0, this.length);
    }
    
    public int length() {
        return this.length;
    }
    
    public byte byteAt(final int index) {
        if (index < this.length) {
            return this.buffer[index];
        }
        throw new IndexOutOfBoundsException("The index exceeds the valid buffer area");
    }
    
    public int charAt(final int index) {
        if (index < this.length) {
            return this.buffer[index] & 0xFF;
        }
        throw new IndexOutOfBoundsException("The index exceeds the valid buffer area");
    }
    
    public void append(final byte b) {
        this.ensureCapacity(this.length + 1);
        this.buffer[this.length++] = b;
    }
    
    public void append(final byte[] bytes, final int offset, final int len) {
        this.ensureCapacity(this.length + len);
        System.arraycopy(bytes, offset, this.buffer, this.length, len);
        this.length += len;
    }
    
    public void append(final byte[] bytes) {
        this.append(bytes, 0, bytes.length);
    }
    
    public void append(final ByteBuffer anotherBuffer) {
        this.append(anotherBuffer.buffer, 0, anotherBuffer.length);
    }
    
    public String getEncoding() {
        if (this.encoding == null) {
            if (this.length < 2) {
                this.encoding = "UTF-8";
            }
            else if (this.buffer[0] == 0) {
                if (this.length < 4 || this.buffer[1] != 0) {
                    this.encoding = "UTF-16BE";
                }
                else if ((this.buffer[2] & 0xFF) == 0xFE && (this.buffer[3] & 0xFF) == 0xFF) {
                    this.encoding = "UTF-32BE";
                }
                else {
                    this.encoding = "UTF-32";
                }
            }
            else if ((this.buffer[0] & 0xFF) < 128) {
                if (this.buffer[1] != 0) {
                    this.encoding = "UTF-8";
                }
                else if (this.length < 4 || this.buffer[2] != 0) {
                    this.encoding = "UTF-16LE";
                }
                else {
                    this.encoding = "UTF-32LE";
                }
            }
            else if ((this.buffer[0] & 0xFF) == 0xEF) {
                this.encoding = "UTF-8";
            }
            else if ((this.buffer[0] & 0xFF) == 0xFE) {
                this.encoding = "UTF-16";
            }
            else if (this.length < 4 || this.buffer[2] != 0) {
                this.encoding = "UTF-16";
            }
            else {
                this.encoding = "UTF-32";
            }
        }
        return this.encoding;
    }
    
    private void ensureCapacity(final int requestedLength) {
        if (requestedLength > this.buffer.length) {
            final byte[] oldBuf = this.buffer;
            System.arraycopy(oldBuf, 0, this.buffer = new byte[oldBuf.length * 2], 0, oldBuf.length);
        }
    }
}
