// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import com.adobe.xmp.XMPMetaFactory;
import java.util.NoSuchElementException;
import com.adobe.xmp.properties.XMPPropertyInfo;
import java.util.Collections;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.options.PropertyOptions;
import com.adobe.xmp.impl.xpath.XMPPath;
import com.adobe.xmp.impl.xpath.XMPPathParser;
import java.util.Iterator;
import com.adobe.xmp.options.IteratorOptions;
import com.adobe.xmp.XMPIterator;

public class XMPIteratorImpl implements XMPIterator
{
    private IteratorOptions options;
    private String baseNS;
    protected boolean skipSiblings;
    protected boolean skipSubtree;
    private Iterator nodeIterator;
    
    public XMPIteratorImpl(final XMPMetaImpl xmp, final String schemaNS, final String propPath, final IteratorOptions options) throws XMPException {
        this.baseNS = null;
        this.skipSiblings = false;
        this.skipSubtree = false;
        this.nodeIterator = null;
        this.options = ((options != null) ? options : new IteratorOptions());
        XMPNode startNode = null;
        String initialPath = null;
        final boolean baseSchema = schemaNS != null && schemaNS.length() > 0;
        final boolean baseProperty = propPath != null && propPath.length() > 0;
        if (!baseSchema && !baseProperty) {
            startNode = xmp.getRoot();
        }
        else if (baseSchema && baseProperty) {
            final XMPPath path = XMPPathParser.expandXPath(schemaNS, propPath);
            final XMPPath basePath = new XMPPath();
            for (int i = 0; i < path.size() - 1; ++i) {
                basePath.add(path.getSegment(i));
            }
            startNode = XMPNodeUtils.findNode(xmp.getRoot(), path, false, null);
            this.baseNS = schemaNS;
            initialPath = basePath.toString();
        }
        else {
            if (!baseSchema || baseProperty) {
                throw new XMPException("Schema namespace URI is required", 101);
            }
            startNode = XMPNodeUtils.findSchemaNode(xmp.getRoot(), schemaNS, false);
        }
        if (startNode != null) {
            if (!this.options.isJustChildren()) {
                this.nodeIterator = new NodeIterator(startNode, initialPath, 1);
            }
            else {
                this.nodeIterator = new NodeIteratorChildren(startNode, initialPath);
            }
        }
        else {
            this.nodeIterator = Collections.EMPTY_LIST.iterator();
        }
    }
    
    public void skipSubtree() {
        this.skipSubtree = true;
    }
    
    public void skipSiblings() {
        this.skipSubtree();
        this.skipSiblings = true;
    }
    
    public boolean hasNext() {
        return this.nodeIterator.hasNext();
    }
    
    public Object next() {
        return this.nodeIterator.next();
    }
    
    public void remove() {
        throw new UnsupportedOperationException("The XMPIterator does not support remove().");
    }
    
    protected IteratorOptions getOptions() {
        return this.options;
    }
    
    protected String getBaseNS() {
        return this.baseNS;
    }
    
    protected void setBaseNS(final String baseNS) {
        this.baseNS = baseNS;
    }
    
    private class NodeIterator implements Iterator
    {
        protected static final int ITERATE_NODE = 0;
        protected static final int ITERATE_CHILDREN = 1;
        protected static final int ITERATE_QUALIFIER = 2;
        private int state;
        private XMPNode visitedNode;
        private String path;
        private Iterator childrenIterator;
        private int index;
        private Iterator subIterator;
        private XMPPropertyInfo returnProperty;
        
        public NodeIterator() {
            this.state = 0;
            this.childrenIterator = null;
            this.index = 0;
            this.subIterator = Collections.EMPTY_LIST.iterator();
            this.returnProperty = null;
        }
        
        public NodeIterator(final XMPNode visitedNode, final String parentPath, final int index) {
            this.state = 0;
            this.childrenIterator = null;
            this.index = 0;
            this.subIterator = Collections.EMPTY_LIST.iterator();
            this.returnProperty = null;
            this.visitedNode = visitedNode;
            this.state = 0;
            if (visitedNode.getOptions().isSchemaNode()) {
                XMPIteratorImpl.this.setBaseNS(visitedNode.getName());
            }
            this.path = this.accumulatePath(visitedNode, parentPath, index);
        }
        
        public boolean hasNext() {
            if (this.returnProperty != null) {
                return true;
            }
            if (this.state == 0) {
                return this.reportNode();
            }
            if (this.state == 1) {
                if (this.childrenIterator == null) {
                    this.childrenIterator = this.visitedNode.iterateChildren();
                }
                boolean hasNext = this.iterateChildren(this.childrenIterator);
                if (!hasNext && this.visitedNode.hasQualifier() && !XMPIteratorImpl.this.getOptions().isOmitQualifiers()) {
                    this.state = 2;
                    this.childrenIterator = null;
                    hasNext = this.hasNext();
                }
                return hasNext;
            }
            if (this.childrenIterator == null) {
                this.childrenIterator = this.visitedNode.iterateQualifier();
            }
            return this.iterateChildren(this.childrenIterator);
        }
        
        protected boolean reportNode() {
            this.state = 1;
            if (this.visitedNode.getParent() != null && (!XMPIteratorImpl.this.getOptions().isJustLeafnodes() || !this.visitedNode.hasChildren())) {
                this.returnProperty = this.createPropertyInfo(this.visitedNode, XMPIteratorImpl.this.getBaseNS(), this.path);
                return true;
            }
            return this.hasNext();
        }
        
        private boolean iterateChildren(final Iterator iterator) {
            if (XMPIteratorImpl.this.skipSiblings) {
                XMPIteratorImpl.this.skipSiblings = false;
                this.subIterator = Collections.EMPTY_LIST.iterator();
            }
            if (!this.subIterator.hasNext() && iterator.hasNext()) {
                final XMPNode child = iterator.next();
                ++this.index;
                this.subIterator = new NodeIterator(child, this.path, this.index);
            }
            if (this.subIterator.hasNext()) {
                this.returnProperty = this.subIterator.next();
                return true;
            }
            return false;
        }
        
        public Object next() {
            if (this.hasNext()) {
                final XMPPropertyInfo result = this.returnProperty;
                this.returnProperty = null;
                return result;
            }
            throw new NoSuchElementException("There are no more nodes to return");
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
        
        protected String accumulatePath(final XMPNode currNode, final String parentPath, final int currentIndex) {
            if (currNode.getParent() == null || currNode.getOptions().isSchemaNode()) {
                return null;
            }
            String separator;
            String segmentName;
            if (currNode.getParent().getOptions().isArray()) {
                separator = "";
                segmentName = "[" + String.valueOf(currentIndex) + "]";
            }
            else {
                separator = "/";
                segmentName = currNode.getName();
            }
            if (parentPath == null || parentPath.length() == 0) {
                return segmentName;
            }
            if (XMPIteratorImpl.this.getOptions().isJustLeafname()) {
                return segmentName.startsWith("?") ? segmentName.substring(1) : segmentName;
            }
            return parentPath + separator + segmentName;
        }
        
        protected XMPPropertyInfo createPropertyInfo(final XMPNode node, final String baseNS, final String path) {
            final String value = node.getOptions().isSchemaNode() ? null : node.getValue();
            return new XMPPropertyInfo() {
                public String getNamespace() {
                    if (!node.getOptions().isSchemaNode()) {
                        final QName qname = new QName(node.getName());
                        return XMPMetaFactory.getSchemaRegistry().getNamespaceURI(qname.getPrefix());
                    }
                    return baseNS;
                }
                
                public String getPath() {
                    return path;
                }
                
                public String getValue() {
                    return value;
                }
                
                public PropertyOptions getOptions() {
                    return node.getOptions();
                }
                
                public String getLanguage() {
                    return null;
                }
            };
        }
        
        protected Iterator getChildrenIterator() {
            return this.childrenIterator;
        }
        
        protected void setChildrenIterator(final Iterator childrenIterator) {
            this.childrenIterator = childrenIterator;
        }
        
        protected XMPPropertyInfo getReturnProperty() {
            return this.returnProperty;
        }
        
        protected void setReturnProperty(final XMPPropertyInfo returnProperty) {
            this.returnProperty = returnProperty;
        }
    }
    
    private class NodeIteratorChildren extends NodeIterator
    {
        private String parentPath;
        private Iterator childrenIterator;
        private int index;
        
        public NodeIteratorChildren(final XMPNode parentNode, final String parentPath) {
            this.index = 0;
            if (parentNode.getOptions().isSchemaNode()) {
                XMPIteratorImpl.this.setBaseNS(parentNode.getName());
            }
            this.parentPath = this.accumulatePath(parentNode, parentPath, 1);
            this.childrenIterator = parentNode.iterateChildren();
        }
        
        @Override
        public boolean hasNext() {
            if (this.getReturnProperty() != null) {
                return true;
            }
            if (XMPIteratorImpl.this.skipSiblings) {
                return false;
            }
            if (!this.childrenIterator.hasNext()) {
                return false;
            }
            final XMPNode child = this.childrenIterator.next();
            ++this.index;
            String path = null;
            if (child.getOptions().isSchemaNode()) {
                XMPIteratorImpl.this.setBaseNS(child.getName());
            }
            else if (child.getParent() != null) {
                path = this.accumulatePath(child, this.parentPath, this.index);
            }
            if (!XMPIteratorImpl.this.getOptions().isJustLeafnodes() || !child.hasChildren()) {
                this.setReturnProperty(this.createPropertyInfo(child, XMPIteratorImpl.this.getBaseNS(), path));
                return true;
            }
            return this.hasNext();
        }
    }
}
