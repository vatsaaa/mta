// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import com.adobe.xmp.XMPUtils;
import com.adobe.xmp.options.ParseOptions;
import com.adobe.xmp.options.IteratorOptions;
import com.adobe.xmp.XMPIterator;
import java.util.Calendar;
import com.adobe.xmp.XMPDateTime;
import java.util.Iterator;
import com.adobe.xmp.properties.XMPProperty;
import com.adobe.xmp.XMPPathFactory;
import com.adobe.xmp.impl.xpath.XMPPath;
import com.adobe.xmp.impl.xpath.XMPPathParser;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.options.PropertyOptions;
import com.adobe.xmp.XMPConst;
import com.adobe.xmp.XMPMeta;

public class XMPMetaImpl implements XMPMeta, XMPConst
{
    private static final int VALUE_STRING = 0;
    private static final int VALUE_BOOLEAN = 1;
    private static final int VALUE_INTEGER = 2;
    private static final int VALUE_LONG = 3;
    private static final int VALUE_DOUBLE = 4;
    private static final int VALUE_DATE = 5;
    private static final int VALUE_CALENDAR = 6;
    private static final int VALUE_BASE64 = 7;
    private XMPNode tree;
    private String packetHeader;
    
    public XMPMetaImpl() {
        this.packetHeader = null;
        this.tree = new XMPNode(null, null, null);
    }
    
    public XMPMetaImpl(final XMPNode tree) {
        this.packetHeader = null;
        this.tree = tree;
    }
    
    public void appendArrayItem(final String schemaNS, final String arrayName, PropertyOptions arrayOptions, final String itemValue, final PropertyOptions itemOptions) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(arrayName);
        if (arrayOptions == null) {
            arrayOptions = new PropertyOptions();
        }
        if (!arrayOptions.isOnlyArrayOptions()) {
            throw new XMPException("Only array form flags allowed for arrayOptions", 103);
        }
        arrayOptions = XMPNodeUtils.verifySetOptions(arrayOptions, null);
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, arrayName);
        XMPNode arrayNode = XMPNodeUtils.findNode(this.tree, arrayPath, false, null);
        if (arrayNode != null) {
            if (!arrayNode.getOptions().isArray()) {
                throw new XMPException("The named property is not an array", 102);
            }
        }
        else {
            if (!arrayOptions.isArray()) {
                throw new XMPException("Explicit arrayOptions required to create new array", 103);
            }
            arrayNode = XMPNodeUtils.findNode(this.tree, arrayPath, true, arrayOptions);
            if (arrayNode == null) {
                throw new XMPException("Failure creating array node", 102);
            }
        }
        this.doSetArrayItem(arrayNode, -1, itemValue, itemOptions, true);
    }
    
    public void appendArrayItem(final String schemaNS, final String arrayName, final String itemValue) throws XMPException {
        this.appendArrayItem(schemaNS, arrayName, null, itemValue, null);
    }
    
    public int countArrayItems(final String schemaNS, final String arrayName) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(arrayName);
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, arrayName);
        final XMPNode arrayNode = XMPNodeUtils.findNode(this.tree, arrayPath, false, null);
        if (arrayNode == null) {
            return 0;
        }
        if (arrayNode.getOptions().isArray()) {
            return arrayNode.getChildrenLength();
        }
        throw new XMPException("The named property is not an array", 102);
    }
    
    public void deleteArrayItem(final String schemaNS, final String arrayName, final int itemIndex) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertArrayName(arrayName);
            final String itemPath = XMPPathFactory.composeArrayItemPath(arrayName, itemIndex);
            this.deleteProperty(schemaNS, itemPath);
        }
        catch (XMPException ex) {}
    }
    
    public void deleteProperty(final String schemaNS, final String propName) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertPropName(propName);
            final XMPPath expPath = XMPPathParser.expandXPath(schemaNS, propName);
            final XMPNode propNode = XMPNodeUtils.findNode(this.tree, expPath, false, null);
            if (propNode != null) {
                XMPNodeUtils.deleteNode(propNode);
            }
        }
        catch (XMPException ex) {}
    }
    
    public void deleteQualifier(final String schemaNS, final String propName, final String qualNS, final String qualName) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertPropName(propName);
            final String qualPath = propName + XMPPathFactory.composeQualifierPath(qualNS, qualName);
            this.deleteProperty(schemaNS, qualPath);
        }
        catch (XMPException ex) {}
    }
    
    public void deleteStructField(final String schemaNS, final String structName, final String fieldNS, final String fieldName) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertStructName(structName);
            final String fieldPath = structName + XMPPathFactory.composeStructFieldPath(fieldNS, fieldName);
            this.deleteProperty(schemaNS, fieldPath);
        }
        catch (XMPException ex) {}
    }
    
    public boolean doesPropertyExist(final String schemaNS, final String propName) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertPropName(propName);
            final XMPPath expPath = XMPPathParser.expandXPath(schemaNS, propName);
            final XMPNode propNode = XMPNodeUtils.findNode(this.tree, expPath, false, null);
            return propNode != null;
        }
        catch (XMPException e) {
            return false;
        }
    }
    
    public boolean doesArrayItemExist(final String schemaNS, final String arrayName, final int itemIndex) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertArrayName(arrayName);
            final String path = XMPPathFactory.composeArrayItemPath(arrayName, itemIndex);
            return this.doesPropertyExist(schemaNS, path);
        }
        catch (XMPException e) {
            return false;
        }
    }
    
    public boolean doesStructFieldExist(final String schemaNS, final String structName, final String fieldNS, final String fieldName) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertStructName(structName);
            final String path = XMPPathFactory.composeStructFieldPath(fieldNS, fieldName);
            return this.doesPropertyExist(schemaNS, structName + path);
        }
        catch (XMPException e) {
            return false;
        }
    }
    
    public boolean doesQualifierExist(final String schemaNS, final String propName, final String qualNS, final String qualName) {
        try {
            ParameterAsserts.assertSchemaNS(schemaNS);
            ParameterAsserts.assertPropName(propName);
            final String path = XMPPathFactory.composeQualifierPath(qualNS, qualName);
            return this.doesPropertyExist(schemaNS, propName + path);
        }
        catch (XMPException e) {
            return false;
        }
    }
    
    public XMPProperty getArrayItem(final String schemaNS, final String arrayName, final int itemIndex) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(arrayName);
        final String itemPath = XMPPathFactory.composeArrayItemPath(arrayName, itemIndex);
        return this.getProperty(schemaNS, itemPath);
    }
    
    public XMPProperty getLocalizedText(final String schemaNS, final String altTextName, String genericLang, String specificLang) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(altTextName);
        ParameterAsserts.assertSpecificLang(specificLang);
        genericLang = ((genericLang != null) ? Utils.normalizeLangValue(genericLang) : null);
        specificLang = Utils.normalizeLangValue(specificLang);
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, altTextName);
        final XMPNode arrayNode = XMPNodeUtils.findNode(this.tree, arrayPath, false, null);
        if (arrayNode == null) {
            return null;
        }
        final Object[] result = XMPNodeUtils.chooseLocalizedText(arrayNode, genericLang, specificLang);
        final int match = (int)result[0];
        final XMPNode itemNode = (XMPNode)result[1];
        if (match != 0) {
            return new XMPProperty() {
                public String getValue() {
                    return itemNode.getValue();
                }
                
                public PropertyOptions getOptions() {
                    return itemNode.getOptions();
                }
                
                public String getLanguage() {
                    return itemNode.getQualifier(1).getValue();
                }
                
                @Override
                public String toString() {
                    return itemNode.getValue().toString();
                }
            };
        }
        return null;
    }
    
    public void setLocalizedText(final String schemaNS, final String altTextName, String genericLang, String specificLang, final String itemValue, final PropertyOptions options) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(altTextName);
        ParameterAsserts.assertSpecificLang(specificLang);
        genericLang = ((genericLang != null) ? Utils.normalizeLangValue(genericLang) : null);
        specificLang = Utils.normalizeLangValue(specificLang);
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, altTextName);
        final XMPNode arrayNode = XMPNodeUtils.findNode(this.tree, arrayPath, true, new PropertyOptions(7680));
        if (arrayNode == null) {
            throw new XMPException("Failed to find or create array node", 102);
        }
        if (!arrayNode.getOptions().isArrayAltText()) {
            if (arrayNode.hasChildren() || !arrayNode.getOptions().isArrayAlternate()) {
                throw new XMPException("Specified property is no alt-text array", 102);
            }
            arrayNode.getOptions().setArrayAltText(true);
        }
        boolean haveXDefault = false;
        XMPNode xdItem = null;
        final Iterator it = arrayNode.iterateChildren();
        while (it.hasNext()) {
            final XMPNode currItem = it.next();
            if (!currItem.hasQualifier() || !"xml:lang".equals(currItem.getQualifier(1).getName())) {
                throw new XMPException("Language qualifier must be first", 102);
            }
            if ("x-default".equals(currItem.getQualifier(1).getValue())) {
                xdItem = currItem;
                haveXDefault = true;
                break;
            }
        }
        if (xdItem != null && arrayNode.getChildrenLength() > 1) {
            arrayNode.removeChild(xdItem);
            arrayNode.addChild(1, xdItem);
        }
        final Object[] result = XMPNodeUtils.chooseLocalizedText(arrayNode, genericLang, specificLang);
        final int match = (int)result[0];
        final XMPNode itemNode = (XMPNode)result[1];
        final boolean specificXDefault = "x-default".equals(specificLang);
        switch (match) {
            case 0: {
                XMPNodeUtils.appendLangItem(arrayNode, "x-default", itemValue);
                haveXDefault = true;
                if (!specificXDefault) {
                    XMPNodeUtils.appendLangItem(arrayNode, specificLang, itemValue);
                    break;
                }
                break;
            }
            case 1: {
                if (!specificXDefault) {
                    if (haveXDefault && xdItem != itemNode && xdItem != null && xdItem.getValue().equals(itemNode.getValue())) {
                        xdItem.setValue(itemValue);
                    }
                    itemNode.setValue(itemValue);
                    break;
                }
                assert haveXDefault && xdItem == itemNode;
                final Iterator it2 = arrayNode.iterateChildren();
                while (it2.hasNext()) {
                    final XMPNode currItem2 = it2.next();
                    if (currItem2 != xdItem) {
                        if (!currItem2.getValue().equals((xdItem != null) ? xdItem.getValue() : null)) {
                            continue;
                        }
                        currItem2.setValue(itemValue);
                    }
                }
                if (xdItem != null) {
                    xdItem.setValue(itemValue);
                    break;
                }
                break;
            }
            case 2: {
                if (haveXDefault && xdItem != itemNode && xdItem != null && xdItem.getValue().equals(itemNode.getValue())) {
                    xdItem.setValue(itemValue);
                }
                itemNode.setValue(itemValue);
                break;
            }
            case 3: {
                XMPNodeUtils.appendLangItem(arrayNode, specificLang, itemValue);
                if (specificXDefault) {
                    haveXDefault = true;
                    break;
                }
                break;
            }
            case 4: {
                if (xdItem != null && arrayNode.getChildrenLength() == 1) {
                    xdItem.setValue(itemValue);
                }
                XMPNodeUtils.appendLangItem(arrayNode, specificLang, itemValue);
                break;
            }
            case 5: {
                XMPNodeUtils.appendLangItem(arrayNode, specificLang, itemValue);
                if (specificXDefault) {
                    haveXDefault = true;
                    break;
                }
                break;
            }
            default: {
                throw new XMPException("Unexpected result from ChooseLocalizedText", 9);
            }
        }
        if (!haveXDefault && arrayNode.getChildrenLength() == 1) {
            XMPNodeUtils.appendLangItem(arrayNode, "x-default", itemValue);
        }
    }
    
    public void setLocalizedText(final String schemaNS, final String altTextName, final String genericLang, final String specificLang, final String itemValue) throws XMPException {
        this.setLocalizedText(schemaNS, altTextName, genericLang, specificLang, itemValue, null);
    }
    
    public XMPProperty getProperty(final String schemaNS, final String propName) throws XMPException {
        return this.getProperty(schemaNS, propName, 0);
    }
    
    protected XMPProperty getProperty(final String schemaNS, final String propName, final int valueType) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertPropName(propName);
        final XMPPath expPath = XMPPathParser.expandXPath(schemaNS, propName);
        final XMPNode propNode = XMPNodeUtils.findNode(this.tree, expPath, false, null);
        if (propNode == null) {
            return null;
        }
        if (valueType != 0 && propNode.getOptions().isCompositeProperty()) {
            throw new XMPException("Property must be simple when a value type is requested", 102);
        }
        final Object value = this.evaluateNodeValue(valueType, propNode);
        return new XMPProperty() {
            public String getValue() {
                return (value != null) ? value.toString() : null;
            }
            
            public PropertyOptions getOptions() {
                return propNode.getOptions();
            }
            
            public String getLanguage() {
                return null;
            }
            
            @Override
            public String toString() {
                return value.toString();
            }
        };
    }
    
    protected Object getPropertyObject(final String schemaNS, final String propName, final int valueType) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertPropName(propName);
        final XMPPath expPath = XMPPathParser.expandXPath(schemaNS, propName);
        final XMPNode propNode = XMPNodeUtils.findNode(this.tree, expPath, false, null);
        if (propNode == null) {
            return null;
        }
        if (valueType != 0 && propNode.getOptions().isCompositeProperty()) {
            throw new XMPException("Property must be simple when a value type is requested", 102);
        }
        return this.evaluateNodeValue(valueType, propNode);
    }
    
    public Boolean getPropertyBoolean(final String schemaNS, final String propName) throws XMPException {
        return (Boolean)this.getPropertyObject(schemaNS, propName, 1);
    }
    
    public void setPropertyBoolean(final String schemaNS, final String propName, final boolean propValue, final PropertyOptions options) throws XMPException {
        this.setProperty(schemaNS, propName, propValue ? "True" : "False", options);
    }
    
    public void setPropertyBoolean(final String schemaNS, final String propName, final boolean propValue) throws XMPException {
        this.setProperty(schemaNS, propName, propValue ? "True" : "False", null);
    }
    
    public Integer getPropertyInteger(final String schemaNS, final String propName) throws XMPException {
        return (Integer)this.getPropertyObject(schemaNS, propName, 2);
    }
    
    public void setPropertyInteger(final String schemaNS, final String propName, final int propValue, final PropertyOptions options) throws XMPException {
        this.setProperty(schemaNS, propName, new Integer(propValue), options);
    }
    
    public void setPropertyInteger(final String schemaNS, final String propName, final int propValue) throws XMPException {
        this.setProperty(schemaNS, propName, new Integer(propValue), null);
    }
    
    public Long getPropertyLong(final String schemaNS, final String propName) throws XMPException {
        return (Long)this.getPropertyObject(schemaNS, propName, 3);
    }
    
    public void setPropertyLong(final String schemaNS, final String propName, final long propValue, final PropertyOptions options) throws XMPException {
        this.setProperty(schemaNS, propName, new Long(propValue), options);
    }
    
    public void setPropertyLong(final String schemaNS, final String propName, final long propValue) throws XMPException {
        this.setProperty(schemaNS, propName, new Long(propValue), null);
    }
    
    public Double getPropertyDouble(final String schemaNS, final String propName) throws XMPException {
        return (Double)this.getPropertyObject(schemaNS, propName, 4);
    }
    
    public void setPropertyDouble(final String schemaNS, final String propName, final double propValue, final PropertyOptions options) throws XMPException {
        this.setProperty(schemaNS, propName, new Double(propValue), options);
    }
    
    public void setPropertyDouble(final String schemaNS, final String propName, final double propValue) throws XMPException {
        this.setProperty(schemaNS, propName, new Double(propValue), null);
    }
    
    public XMPDateTime getPropertyDate(final String schemaNS, final String propName) throws XMPException {
        return (XMPDateTime)this.getPropertyObject(schemaNS, propName, 5);
    }
    
    public void setPropertyDate(final String schemaNS, final String propName, final XMPDateTime propValue, final PropertyOptions options) throws XMPException {
        this.setProperty(schemaNS, propName, propValue, options);
    }
    
    public void setPropertyDate(final String schemaNS, final String propName, final XMPDateTime propValue) throws XMPException {
        this.setProperty(schemaNS, propName, propValue, null);
    }
    
    public Calendar getPropertyCalendar(final String schemaNS, final String propName) throws XMPException {
        return (Calendar)this.getPropertyObject(schemaNS, propName, 6);
    }
    
    public void setPropertyCalendar(final String schemaNS, final String propName, final Calendar propValue, final PropertyOptions options) throws XMPException {
        this.setProperty(schemaNS, propName, propValue, options);
    }
    
    public void setPropertyCalendar(final String schemaNS, final String propName, final Calendar propValue) throws XMPException {
        this.setProperty(schemaNS, propName, propValue, null);
    }
    
    public byte[] getPropertyBase64(final String schemaNS, final String propName) throws XMPException {
        return (byte[])this.getPropertyObject(schemaNS, propName, 7);
    }
    
    public String getPropertyString(final String schemaNS, final String propName) throws XMPException {
        return (String)this.getPropertyObject(schemaNS, propName, 0);
    }
    
    public void setPropertyBase64(final String schemaNS, final String propName, final byte[] propValue, final PropertyOptions options) throws XMPException {
        this.setProperty(schemaNS, propName, propValue, options);
    }
    
    public void setPropertyBase64(final String schemaNS, final String propName, final byte[] propValue) throws XMPException {
        this.setProperty(schemaNS, propName, propValue, null);
    }
    
    public XMPProperty getQualifier(final String schemaNS, final String propName, final String qualNS, final String qualName) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertPropName(propName);
        final String qualPath = propName + XMPPathFactory.composeQualifierPath(qualNS, qualName);
        return this.getProperty(schemaNS, qualPath);
    }
    
    public XMPProperty getStructField(final String schemaNS, final String structName, final String fieldNS, final String fieldName) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertStructName(structName);
        final String fieldPath = structName + XMPPathFactory.composeStructFieldPath(fieldNS, fieldName);
        return this.getProperty(schemaNS, fieldPath);
    }
    
    public XMPIterator iterator() throws XMPException {
        return this.iterator(null, null, null);
    }
    
    public XMPIterator iterator(final IteratorOptions options) throws XMPException {
        return this.iterator(null, null, options);
    }
    
    public XMPIterator iterator(final String schemaNS, final String propName, final IteratorOptions options) throws XMPException {
        return new XMPIteratorImpl(this, schemaNS, propName, options);
    }
    
    public void setArrayItem(final String schemaNS, final String arrayName, final int itemIndex, final String itemValue, final PropertyOptions options) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(arrayName);
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, arrayName);
        final XMPNode arrayNode = XMPNodeUtils.findNode(this.tree, arrayPath, false, null);
        if (arrayNode != null) {
            this.doSetArrayItem(arrayNode, itemIndex, itemValue, options, false);
            return;
        }
        throw new XMPException("Specified array does not exist", 102);
    }
    
    public void setArrayItem(final String schemaNS, final String arrayName, final int itemIndex, final String itemValue) throws XMPException {
        this.setArrayItem(schemaNS, arrayName, itemIndex, itemValue, null);
    }
    
    public void insertArrayItem(final String schemaNS, final String arrayName, final int itemIndex, final String itemValue, final PropertyOptions options) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertArrayName(arrayName);
        final XMPPath arrayPath = XMPPathParser.expandXPath(schemaNS, arrayName);
        final XMPNode arrayNode = XMPNodeUtils.findNode(this.tree, arrayPath, false, null);
        if (arrayNode != null) {
            this.doSetArrayItem(arrayNode, itemIndex, itemValue, options, true);
            return;
        }
        throw new XMPException("Specified array does not exist", 102);
    }
    
    public void insertArrayItem(final String schemaNS, final String arrayName, final int itemIndex, final String itemValue) throws XMPException {
        this.insertArrayItem(schemaNS, arrayName, itemIndex, itemValue, null);
    }
    
    public void setProperty(final String schemaNS, final String propName, final Object propValue, PropertyOptions options) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertPropName(propName);
        options = XMPNodeUtils.verifySetOptions(options, propValue);
        final XMPPath expPath = XMPPathParser.expandXPath(schemaNS, propName);
        final XMPNode propNode = XMPNodeUtils.findNode(this.tree, expPath, true, options);
        if (propNode != null) {
            this.setNode(propNode, propValue, options, false);
            return;
        }
        throw new XMPException("Specified property does not exist", 102);
    }
    
    public void setProperty(final String schemaNS, final String propName, final Object propValue) throws XMPException {
        this.setProperty(schemaNS, propName, propValue, null);
    }
    
    public void setQualifier(final String schemaNS, final String propName, final String qualNS, final String qualName, final String qualValue, final PropertyOptions options) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertPropName(propName);
        if (!this.doesPropertyExist(schemaNS, propName)) {
            throw new XMPException("Specified property does not exist!", 102);
        }
        final String qualPath = propName + XMPPathFactory.composeQualifierPath(qualNS, qualName);
        this.setProperty(schemaNS, qualPath, qualValue, options);
    }
    
    public void setQualifier(final String schemaNS, final String propName, final String qualNS, final String qualName, final String qualValue) throws XMPException {
        this.setQualifier(schemaNS, propName, qualNS, qualName, qualValue, null);
    }
    
    public void setStructField(final String schemaNS, final String structName, final String fieldNS, final String fieldName, final String fieldValue, final PropertyOptions options) throws XMPException {
        ParameterAsserts.assertSchemaNS(schemaNS);
        ParameterAsserts.assertStructName(structName);
        final String fieldPath = structName + XMPPathFactory.composeStructFieldPath(fieldNS, fieldName);
        this.setProperty(schemaNS, fieldPath, fieldValue, options);
    }
    
    public void setStructField(final String schemaNS, final String structName, final String fieldNS, final String fieldName, final String fieldValue) throws XMPException {
        this.setStructField(schemaNS, structName, fieldNS, fieldName, fieldValue, null);
    }
    
    public String getObjectName() {
        return (this.tree.getName() != null) ? this.tree.getName() : "";
    }
    
    public void setObjectName(final String name) {
        this.tree.setName(name);
    }
    
    public String getPacketHeader() {
        return this.packetHeader;
    }
    
    public void setPacketHeader(final String packetHeader) {
        this.packetHeader = packetHeader;
    }
    
    public Object clone() {
        final XMPNode clonedTree = (XMPNode)this.tree.clone();
        return new XMPMetaImpl(clonedTree);
    }
    
    public String dumpObject() {
        return this.getRoot().dumpNode(true);
    }
    
    public void sort() {
        this.tree.sort();
    }
    
    public void normalize(ParseOptions options) throws XMPException {
        if (options == null) {
            options = new ParseOptions();
        }
        XMPNormalizer.process(this, options);
    }
    
    public XMPNode getRoot() {
        return this.tree;
    }
    
    private void doSetArrayItem(final XMPNode arrayNode, int itemIndex, final String itemValue, PropertyOptions itemOptions, final boolean insert) throws XMPException {
        final XMPNode itemNode = new XMPNode("[]", null);
        itemOptions = XMPNodeUtils.verifySetOptions(itemOptions, itemValue);
        final int maxIndex = insert ? (arrayNode.getChildrenLength() + 1) : arrayNode.getChildrenLength();
        if (itemIndex == -1) {
            itemIndex = maxIndex;
        }
        if (1 <= itemIndex && itemIndex <= maxIndex) {
            if (!insert) {
                arrayNode.removeChild(itemIndex);
            }
            arrayNode.addChild(itemIndex, itemNode);
            this.setNode(itemNode, itemValue, itemOptions, false);
            return;
        }
        throw new XMPException("Array index out of bounds", 104);
    }
    
    void setNode(final XMPNode node, final Object value, final PropertyOptions newOptions, final boolean deleteExisting) throws XMPException {
        if (deleteExisting) {
            node.clear();
        }
        node.getOptions().mergeWith(newOptions);
        if (!node.getOptions().isCompositeProperty()) {
            XMPNodeUtils.setNodeValue(node, value);
        }
        else {
            if (value != null && value.toString().length() > 0) {
                throw new XMPException("Composite nodes can't have values", 102);
            }
            node.removeChildren();
        }
    }
    
    private Object evaluateNodeValue(final int valueType, final XMPNode propNode) throws XMPException {
        final String rawValue = propNode.getValue();
        Object value = null;
        switch (valueType) {
            case 1: {
                value = new Boolean(XMPUtils.convertToBoolean(rawValue));
                break;
            }
            case 2: {
                value = new Integer(XMPUtils.convertToInteger(rawValue));
                break;
            }
            case 3: {
                value = new Long(XMPUtils.convertToLong(rawValue));
                break;
            }
            case 4: {
                value = new Double(XMPUtils.convertToDouble(rawValue));
                break;
            }
            case 5: {
                value = XMPUtils.convertToDate(rawValue);
                break;
            }
            case 6: {
                final XMPDateTime dt = XMPUtils.convertToDate(rawValue);
                value = dt.getCalendar();
                break;
            }
            case 7: {
                value = XMPUtils.decodeBase64(rawValue);
                break;
            }
            default: {
                value = ((rawValue != null || propNode.getOptions().isCompositeProperty()) ? rawValue : "");
                break;
            }
        }
        return value;
    }
}
