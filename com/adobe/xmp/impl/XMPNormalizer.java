// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.util.HashMap;
import java.util.Calendar;
import com.adobe.xmp.XMPDateTime;
import com.adobe.xmp.XMPUtils;
import com.adobe.xmp.properties.XMPAliasInfo;
import com.adobe.xmp.XMPMetaFactory;
import java.util.Iterator;
import com.adobe.xmp.impl.xpath.XMPPath;
import com.adobe.xmp.options.PropertyOptions;
import com.adobe.xmp.impl.xpath.XMPPathParser;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.options.ParseOptions;
import java.util.Map;

public class XMPNormalizer
{
    private static Map dcArrayForms;
    
    private XMPNormalizer() {
    }
    
    static XMPMeta process(final XMPMetaImpl xmp, final ParseOptions options) throws XMPException {
        final XMPNode tree = xmp.getRoot();
        touchUpDataModel(xmp);
        moveExplicitAliases(tree, options);
        tweakOldXMP(tree);
        deleteEmptySchemas(tree);
        return xmp;
    }
    
    private static void tweakOldXMP(final XMPNode tree) throws XMPException {
        if (tree.getName() != null && tree.getName().length() >= 36) {
            String nameStr = tree.getName().toLowerCase();
            if (nameStr.startsWith("uuid:")) {
                nameStr = nameStr.substring(5);
            }
            if (Utils.checkUUIDFormat(nameStr)) {
                final XMPPath path = XMPPathParser.expandXPath("http://ns.adobe.com/xap/1.0/mm/", "InstanceID");
                final XMPNode idNode = XMPNodeUtils.findNode(tree, path, true, null);
                if (idNode == null) {
                    throw new XMPException("Failure creating xmpMM:InstanceID", 9);
                }
                idNode.setOptions(null);
                idNode.setValue("uuid:" + nameStr);
                idNode.removeChildren();
                idNode.removeQualifiers();
                tree.setName(null);
            }
        }
    }
    
    private static void touchUpDataModel(final XMPMetaImpl xmp) throws XMPException {
        XMPNodeUtils.findSchemaNode(xmp.getRoot(), "http://purl.org/dc/elements/1.1/", true);
        final Iterator it = xmp.getRoot().iterateChildren();
        while (it.hasNext()) {
            final XMPNode currSchema = it.next();
            if ("http://purl.org/dc/elements/1.1/".equals(currSchema.getName())) {
                normalizeDCArrays(currSchema);
            }
            else if ("http://ns.adobe.com/exif/1.0/".equals(currSchema.getName())) {
                fixGPSTimeStamp(currSchema);
                final XMPNode arrayNode = XMPNodeUtils.findChildNode(currSchema, "exif:UserComment", false);
                if (arrayNode == null) {
                    continue;
                }
                repairAltText(arrayNode);
            }
            else if ("http://ns.adobe.com/xmp/1.0/DynamicMedia/".equals(currSchema.getName())) {
                final XMPNode dmCopyright = XMPNodeUtils.findChildNode(currSchema, "xmpDM:copyright", false);
                if (dmCopyright == null) {
                    continue;
                }
                migrateAudioCopyright(xmp, dmCopyright);
            }
            else {
                if (!"http://ns.adobe.com/xap/1.0/rights/".equals(currSchema.getName())) {
                    continue;
                }
                final XMPNode arrayNode = XMPNodeUtils.findChildNode(currSchema, "xmpRights:UsageTerms", false);
                if (arrayNode == null) {
                    continue;
                }
                repairAltText(arrayNode);
            }
        }
    }
    
    private static void normalizeDCArrays(final XMPNode dcSchema) throws XMPException {
        for (int i = 1; i <= dcSchema.getChildrenLength(); ++i) {
            final XMPNode currProp = dcSchema.getChild(i);
            final PropertyOptions arrayForm = XMPNormalizer.dcArrayForms.get(currProp.getName());
            if (arrayForm != null) {
                if (currProp.getOptions().isSimple()) {
                    final XMPNode newArray = new XMPNode(currProp.getName(), arrayForm);
                    currProp.setName("[]");
                    newArray.addChild(currProp);
                    dcSchema.replaceChild(i, newArray);
                    if (arrayForm.isArrayAltText() && !currProp.getOptions().getHasLanguage()) {
                        final XMPNode newLang = new XMPNode("xml:lang", "x-default", null);
                        currProp.addQualifier(newLang);
                    }
                }
                else {
                    currProp.getOptions().setOption(7680, false);
                    currProp.getOptions().mergeWith(arrayForm);
                    if (arrayForm.isArrayAltText()) {
                        repairAltText(currProp);
                    }
                }
            }
        }
    }
    
    private static void repairAltText(final XMPNode arrayNode) throws XMPException {
        if (arrayNode == null || !arrayNode.getOptions().isArray()) {
            return;
        }
        arrayNode.getOptions().setArrayOrdered(true).setArrayAlternate(true).setArrayAltText(true);
        final Iterator it = arrayNode.iterateChildren();
        while (it.hasNext()) {
            final XMPNode currChild = it.next();
            if (currChild.getOptions().isCompositeProperty()) {
                it.remove();
            }
            else {
                if (currChild.getOptions().getHasLanguage()) {
                    continue;
                }
                final String childValue = currChild.getValue();
                if (childValue == null || childValue.length() == 0) {
                    it.remove();
                }
                else {
                    final XMPNode repairLang = new XMPNode("xml:lang", "x-repair", null);
                    currChild.addQualifier(repairLang);
                }
            }
        }
    }
    
    private static void moveExplicitAliases(final XMPNode tree, final ParseOptions options) throws XMPException {
        if (!tree.getHasAliases()) {
            return;
        }
        tree.setHasAliases(false);
        final boolean strictAliasing = options.getStrictAliasing();
        for (final XMPNode currSchema : tree.getUnmodifiableChildren()) {
            if (!currSchema.getHasAliases()) {
                continue;
            }
            final Iterator propertyIt = currSchema.iterateChildren();
            while (propertyIt.hasNext()) {
                final XMPNode currProp = propertyIt.next();
                if (!currProp.isAlias()) {
                    continue;
                }
                currProp.setAlias(false);
                final XMPAliasInfo info = XMPMetaFactory.getSchemaRegistry().findAlias(currProp.getName());
                if (info == null) {
                    continue;
                }
                final XMPNode baseSchema = XMPNodeUtils.findSchemaNode(tree, info.getNamespace(), null, true);
                baseSchema.setImplicit(false);
                XMPNode baseNode = XMPNodeUtils.findChildNode(baseSchema, info.getPrefix() + info.getPropName(), false);
                if (baseNode == null) {
                    if (info.getAliasForm().isSimple()) {
                        final String qname = info.getPrefix() + info.getPropName();
                        currProp.setName(qname);
                        baseSchema.addChild(currProp);
                        propertyIt.remove();
                    }
                    else {
                        baseNode = new XMPNode(info.getPrefix() + info.getPropName(), info.getAliasForm().toPropertyOptions());
                        baseSchema.addChild(baseNode);
                        transplantArrayItemAlias(propertyIt, currProp, baseNode);
                    }
                }
                else if (info.getAliasForm().isSimple()) {
                    if (strictAliasing) {
                        compareAliasedSubtrees(currProp, baseNode, true);
                    }
                    propertyIt.remove();
                }
                else {
                    XMPNode itemNode = null;
                    if (info.getAliasForm().isArrayAltText()) {
                        final int xdIndex = XMPNodeUtils.lookupLanguageItem(baseNode, "x-default");
                        if (xdIndex != -1) {
                            itemNode = baseNode.getChild(xdIndex);
                        }
                    }
                    else if (baseNode.hasChildren()) {
                        itemNode = baseNode.getChild(1);
                    }
                    if (itemNode == null) {
                        transplantArrayItemAlias(propertyIt, currProp, baseNode);
                    }
                    else {
                        if (strictAliasing) {
                            compareAliasedSubtrees(currProp, itemNode, true);
                        }
                        propertyIt.remove();
                    }
                }
            }
            currSchema.setHasAliases(false);
        }
    }
    
    private static void transplantArrayItemAlias(final Iterator propertyIt, final XMPNode childNode, final XMPNode baseArray) throws XMPException {
        if (baseArray.getOptions().isArrayAltText()) {
            if (childNode.getOptions().getHasLanguage()) {
                throw new XMPException("Alias to x-default already has a language qualifier", 203);
            }
            final XMPNode langQual = new XMPNode("xml:lang", "x-default", null);
            childNode.addQualifier(langQual);
        }
        propertyIt.remove();
        childNode.setName("[]");
        baseArray.addChild(childNode);
    }
    
    private static void fixGPSTimeStamp(final XMPNode exifSchema) throws XMPException {
        final XMPNode gpsDateTime = XMPNodeUtils.findChildNode(exifSchema, "exif:GPSTimeStamp", false);
        if (gpsDateTime == null) {
            return;
        }
        try {
            XMPDateTime binGPSStamp = XMPUtils.convertToDate(gpsDateTime.getValue());
            if (binGPSStamp.getYear() != 0 || binGPSStamp.getMonth() != 0 || binGPSStamp.getDay() != 0) {
                return;
            }
            XMPNode otherDate = XMPNodeUtils.findChildNode(exifSchema, "exif:DateTimeOriginal", false);
            if (otherDate == null) {
                otherDate = XMPNodeUtils.findChildNode(exifSchema, "exif:DateTimeDigitized", false);
            }
            final XMPDateTime binOtherDate = XMPUtils.convertToDate(otherDate.getValue());
            final Calendar cal = binGPSStamp.getCalendar();
            cal.set(1, binOtherDate.getYear());
            cal.set(2, binOtherDate.getMonth());
            cal.set(5, binOtherDate.getDay());
            binGPSStamp = new XMPDateTimeImpl(cal);
            gpsDateTime.setValue(XMPUtils.convertFromDate(binGPSStamp));
        }
        catch (XMPException e) {}
    }
    
    private static void deleteEmptySchemas(final XMPNode tree) {
        final Iterator it = tree.iterateChildren();
        while (it.hasNext()) {
            final XMPNode schema = it.next();
            if (!schema.hasChildren()) {
                it.remove();
            }
        }
    }
    
    private static void compareAliasedSubtrees(final XMPNode aliasNode, final XMPNode baseNode, final boolean outerCall) throws XMPException {
        if (!aliasNode.getValue().equals(baseNode.getValue()) || aliasNode.getChildrenLength() != baseNode.getChildrenLength()) {
            throw new XMPException("Mismatch between alias and base nodes", 203);
        }
        if (!outerCall && (!aliasNode.getName().equals(baseNode.getName()) || !aliasNode.getOptions().equals(baseNode.getOptions()) || aliasNode.getQualifierLength() != baseNode.getQualifierLength())) {
            throw new XMPException("Mismatch between alias and base nodes", 203);
        }
        Iterator an = aliasNode.iterateChildren();
        Iterator bn = baseNode.iterateChildren();
        while (an.hasNext() && bn.hasNext()) {
            final XMPNode aliasChild = an.next();
            final XMPNode baseChild = bn.next();
            compareAliasedSubtrees(aliasChild, baseChild, false);
        }
        an = aliasNode.iterateQualifier();
        bn = baseNode.iterateQualifier();
        while (an.hasNext() && bn.hasNext()) {
            final XMPNode aliasQual = an.next();
            final XMPNode baseQual = bn.next();
            compareAliasedSubtrees(aliasQual, baseQual, false);
        }
    }
    
    private static void migrateAudioCopyright(final XMPMeta xmp, final XMPNode dmCopyright) {
        try {
            final XMPNode dcSchema = XMPNodeUtils.findSchemaNode(((XMPMetaImpl)xmp).getRoot(), "http://purl.org/dc/elements/1.1/", true);
            String dmValue = dmCopyright.getValue();
            final String doubleLF = "\n\n";
            final XMPNode dcRightsArray = XMPNodeUtils.findChildNode(dcSchema, "dc:rights", false);
            if (dcRightsArray == null || !dcRightsArray.hasChildren()) {
                dmValue = doubleLF + dmValue;
                xmp.setLocalizedText("http://purl.org/dc/elements/1.1/", "rights", "", "x-default", dmValue, null);
            }
            else {
                int xdIndex = XMPNodeUtils.lookupLanguageItem(dcRightsArray, "x-default");
                if (xdIndex < 0) {
                    final String firstValue = dcRightsArray.getChild(1).getValue();
                    xmp.setLocalizedText("http://purl.org/dc/elements/1.1/", "rights", "", "x-default", firstValue, null);
                    xdIndex = XMPNodeUtils.lookupLanguageItem(dcRightsArray, "x-default");
                }
                final XMPNode defaultNode = dcRightsArray.getChild(xdIndex);
                final String defaultValue = defaultNode.getValue();
                final int lfPos = defaultValue.indexOf(doubleLF);
                if (lfPos < 0) {
                    if (!dmValue.equals(defaultValue)) {
                        defaultNode.setValue(defaultValue + doubleLF + dmValue);
                    }
                }
                else if (!defaultValue.substring(lfPos + 2).equals(dmValue)) {
                    defaultNode.setValue(defaultValue.substring(0, lfPos + 2) + dmValue);
                }
            }
            dmCopyright.getParent().removeChild(dmCopyright);
        }
        catch (XMPException ex) {}
    }
    
    private static void initDCArrays() {
        XMPNormalizer.dcArrayForms = new HashMap();
        final PropertyOptions bagForm = new PropertyOptions();
        bagForm.setArray(true);
        XMPNormalizer.dcArrayForms.put("dc:contributor", bagForm);
        XMPNormalizer.dcArrayForms.put("dc:language", bagForm);
        XMPNormalizer.dcArrayForms.put("dc:publisher", bagForm);
        XMPNormalizer.dcArrayForms.put("dc:relation", bagForm);
        XMPNormalizer.dcArrayForms.put("dc:subject", bagForm);
        XMPNormalizer.dcArrayForms.put("dc:type", bagForm);
        final PropertyOptions seqForm = new PropertyOptions();
        seqForm.setArray(true);
        seqForm.setArrayOrdered(true);
        XMPNormalizer.dcArrayForms.put("dc:creator", seqForm);
        XMPNormalizer.dcArrayForms.put("dc:date", seqForm);
        final PropertyOptions altTextForm = new PropertyOptions();
        altTextForm.setArray(true);
        altTextForm.setArrayOrdered(true);
        altTextForm.setArrayAlternate(true);
        altTextForm.setArrayAltText(true);
        XMPNormalizer.dcArrayForms.put("dc:description", altTextForm);
        XMPNormalizer.dcArrayForms.put("dc:rights", altTextForm);
        XMPNormalizer.dcArrayForms.put("dc:title", altTextForm);
    }
    
    static {
        initDCArrays();
    }
}
