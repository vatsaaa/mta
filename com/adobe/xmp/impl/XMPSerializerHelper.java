// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import java.io.UnsupportedEncodingException;
import java.io.ByteArrayOutputStream;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPMeta;
import com.adobe.xmp.options.SerializeOptions;
import java.io.OutputStream;

public class XMPSerializerHelper
{
    public static void serialize(final XMPMetaImpl xmp, final OutputStream out, SerializeOptions options) throws XMPException {
        options = ((options != null) ? options : new SerializeOptions());
        if (options.getSort()) {
            xmp.sort();
        }
        new XMPSerializerRDF().serialize(xmp, out, options);
    }
    
    public static String serializeToString(final XMPMetaImpl xmp, SerializeOptions options) throws XMPException {
        options = ((options != null) ? options : new SerializeOptions());
        options.setEncodeUTF16BE(true);
        final ByteArrayOutputStream out = new ByteArrayOutputStream(2048);
        serialize(xmp, out, options);
        try {
            return out.toString(options.getEncoding());
        }
        catch (UnsupportedEncodingException e) {
            return out.toString();
        }
    }
    
    public static byte[] serializeToBuffer(final XMPMetaImpl xmp, final SerializeOptions options) throws XMPException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream(2048);
        serialize(xmp, out, options);
        return out.toByteArray();
    }
}
