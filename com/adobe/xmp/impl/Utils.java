// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

import com.adobe.xmp.XMPConst;

public class Utils implements XMPConst
{
    public static final int UUID_SEGMENT_COUNT = 4;
    public static final int UUID_LENGTH = 36;
    private static boolean[] xmlNameStartChars;
    private static boolean[] xmlNameChars;
    
    private Utils() {
    }
    
    public static String normalizeLangValue(final String value) {
        if ("x-default".equals(value)) {
            return value;
        }
        int subTag = 1;
        final StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < value.length(); ++i) {
            switch (value.charAt(i)) {
                case '-':
                case '_': {
                    buffer.append('-');
                    ++subTag;
                    break;
                }
                case ' ': {
                    break;
                }
                default: {
                    if (subTag != 2) {
                        buffer.append(Character.toLowerCase(value.charAt(i)));
                        break;
                    }
                    buffer.append(Character.toUpperCase(value.charAt(i)));
                    break;
                }
            }
        }
        return buffer.toString();
    }
    
    static String[] splitNameAndValue(final String selector) {
        final int eq = selector.indexOf(61);
        int pos = 1;
        if (selector.charAt(pos) == '?') {
            ++pos;
        }
        final String name = selector.substring(pos, eq);
        pos = eq + 1;
        final char quote = selector.charAt(pos);
        ++pos;
        final int end = selector.length() - 2;
        final StringBuffer value = new StringBuffer(end - eq);
        while (pos < end) {
            value.append(selector.charAt(pos));
            ++pos;
            if (selector.charAt(pos) == quote) {
                ++pos;
            }
        }
        return new String[] { name, value.toString() };
    }
    
    static boolean isInternalProperty(final String schema, final String prop) {
        boolean isInternal = false;
        if ("http://purl.org/dc/elements/1.1/".equals(schema)) {
            if ("dc:format".equals(prop) || "dc:language".equals(prop)) {
                isInternal = true;
            }
        }
        else if ("http://ns.adobe.com/xap/1.0/".equals(schema)) {
            if ("xmp:BaseURL".equals(prop) || "xmp:CreatorTool".equals(prop) || "xmp:Format".equals(prop) || "xmp:Locale".equals(prop) || "xmp:MetadataDate".equals(prop) || "xmp:ModifyDate".equals(prop)) {
                isInternal = true;
            }
        }
        else if ("http://ns.adobe.com/pdf/1.3/".equals(schema)) {
            if ("pdf:BaseURL".equals(prop) || "pdf:Creator".equals(prop) || "pdf:ModDate".equals(prop) || "pdf:PDFVersion".equals(prop) || "pdf:Producer".equals(prop)) {
                isInternal = true;
            }
        }
        else if ("http://ns.adobe.com/tiff/1.0/".equals(schema)) {
            isInternal = true;
            if ("tiff:ImageDescription".equals(prop) || "tiff:Artist".equals(prop) || "tiff:Copyright".equals(prop)) {
                isInternal = false;
            }
        }
        else if ("http://ns.adobe.com/exif/1.0/".equals(schema)) {
            isInternal = true;
            if ("exif:UserComment".equals(prop)) {
                isInternal = false;
            }
        }
        else if ("http://ns.adobe.com/exif/1.0/aux/".equals(schema)) {
            isInternal = true;
        }
        else if ("http://ns.adobe.com/photoshop/1.0/".equals(schema)) {
            if ("photoshop:ICCProfile".equals(prop)) {
                isInternal = true;
            }
        }
        else if ("http://ns.adobe.com/camera-raw-settings/1.0/".equals(schema)) {
            if ("crs:Version".equals(prop) || "crs:RawFileName".equals(prop) || "crs:ToneCurveName".equals(prop)) {
                isInternal = true;
            }
        }
        else if ("http://ns.adobe.com/StockPhoto/1.0/".equals(schema)) {
            isInternal = true;
        }
        else if ("http://ns.adobe.com/xap/1.0/mm/".equals(schema)) {
            isInternal = true;
        }
        else if ("http://ns.adobe.com/xap/1.0/t/".equals(schema)) {
            isInternal = true;
        }
        else if ("http://ns.adobe.com/xap/1.0/t/pg/".equals(schema)) {
            isInternal = true;
        }
        else if ("http://ns.adobe.com/xap/1.0/g/".equals(schema)) {
            isInternal = true;
        }
        else if ("http://ns.adobe.com/xap/1.0/g/img/".equals(schema)) {
            isInternal = true;
        }
        else if ("http://ns.adobe.com/xap/1.0/sType/Font#".equals(schema)) {
            isInternal = true;
        }
        return isInternal;
    }
    
    static boolean checkUUIDFormat(final String uuid) {
        boolean result = true;
        int delimCnt = 0;
        int delimPos = 0;
        if (uuid == null) {
            return false;
        }
        for (delimPos = 0; delimPos < uuid.length(); ++delimPos) {
            if (uuid.charAt(delimPos) == '-') {
                ++delimCnt;
                result = (result && (delimPos == 8 || delimPos == 13 || delimPos == 18 || delimPos == 23));
            }
        }
        return result && 4 == delimCnt && 36 == delimPos;
    }
    
    public static boolean isXMLName(final String name) {
        if (name.length() > 0 && !isNameStartChar(name.charAt(0))) {
            return false;
        }
        for (int i = 1; i < name.length(); ++i) {
            if (!isNameChar(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isXMLNameNS(final String name) {
        if (name.length() > 0 && (!isNameStartChar(name.charAt(0)) || name.charAt(0) == ':')) {
            return false;
        }
        for (int i = 1; i < name.length(); ++i) {
            if (!isNameChar(name.charAt(i)) || name.charAt(i) == ':') {
                return false;
            }
        }
        return true;
    }
    
    static boolean isControlChar(final char c) {
        return (c <= '\u001f' || c == '\u007f') && c != '\t' && c != '\n' && c != '\r';
    }
    
    public static String escapeXML(final String value, final boolean forAttribute, final boolean escapeWhitespaces) {
        boolean needsEscaping = false;
        for (int i = 0; i < value.length(); ++i) {
            final char c = value.charAt(i);
            if (c == '<' || c == '>' || c == '&' || (escapeWhitespaces && (c == '\t' || c == '\n' || c == '\r')) || (forAttribute && c == '\"')) {
                needsEscaping = true;
                break;
            }
        }
        if (!needsEscaping) {
            return value;
        }
        final StringBuffer buffer = new StringBuffer(value.length() * 4 / 3);
        for (int j = 0; j < value.length(); ++j) {
            final char c2 = value.charAt(j);
            if (!escapeWhitespaces || (c2 != '\t' && c2 != '\n' && c2 != '\r')) {
                switch (c2) {
                    case '<': {
                        buffer.append("&lt;");
                        break;
                    }
                    case '>': {
                        buffer.append("&gt;");
                        break;
                    }
                    case '&': {
                        buffer.append("&amp;");
                        break;
                    }
                    case '\"': {
                        buffer.append(forAttribute ? "&quot;" : "\"");
                        break;
                    }
                    default: {
                        buffer.append(c2);
                        break;
                    }
                }
            }
            else {
                buffer.append("&#x");
                buffer.append(Integer.toHexString(c2).toUpperCase());
                buffer.append(';');
            }
        }
        return buffer.toString();
    }
    
    static String removeControlChars(final String value) {
        final StringBuffer buffer = new StringBuffer(value);
        for (int i = 0; i < buffer.length(); ++i) {
            if (isControlChar(buffer.charAt(i))) {
                buffer.setCharAt(i, ' ');
            }
        }
        return buffer.toString();
    }
    
    private static boolean isNameStartChar(final char ch) {
        return (ch <= '\u00ff' && Utils.xmlNameStartChars[ch]) || (ch >= '\u0100' && ch <= '\u02ff') || (ch >= '\u0370' && ch <= '\u037d') || (ch >= '\u037f' && ch <= '\u1fff') || (ch >= '\u200c' && ch <= '\u200d') || (ch >= '\u2070' && ch <= '\u218f') || (ch >= '\u2c00' && ch <= '\u2fef') || (ch >= '\u3001' && ch <= '\ud7ff') || (ch >= '\uf900' && ch <= '\ufdcf') || (ch >= '\ufdf0' && ch <= '\ufffd') || (ch >= 65536 && ch <= 983039);
    }
    
    private static boolean isNameChar(final char ch) {
        return (ch <= '\u00ff' && Utils.xmlNameChars[ch]) || isNameStartChar(ch) || (ch >= '\u0300' && ch <= '\u036f') || (ch >= '\u203f' && ch <= '\u2040');
    }
    
    private static void initCharTables() {
        Utils.xmlNameChars = new boolean[256];
        Utils.xmlNameStartChars = new boolean[256];
        for (char ch = '\0'; ch < Utils.xmlNameChars.length; ++ch) {
            Utils.xmlNameStartChars[ch] = (ch == ':' || ('A' <= ch && ch <= 'Z') || ch == '_' || ('a' <= ch && ch <= 'z') || ('\u00c0' <= ch && ch <= '\u00d6') || ('\u00d8' <= ch && ch <= '\u00f6') || ('\u00f8' <= ch && ch <= '\u00ff'));
            Utils.xmlNameChars[ch] = (Utils.xmlNameStartChars[ch] || ch == '-' || ch == '.' || ('0' <= ch && ch <= '9') || ch == '·');
        }
    }
    
    static {
        initCharTables();
    }
}
