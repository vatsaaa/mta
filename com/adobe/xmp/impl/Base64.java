// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.impl;

public class Base64
{
    private static final byte INVALID = -1;
    private static final byte WHITESPACE = -2;
    private static final byte EQUAL = -3;
    private static byte[] base64;
    private static byte[] ascii;
    
    public static final byte[] encode(final byte[] src) {
        return encode(src, 0);
    }
    
    public static final byte[] encode(final byte[] src, int lineFeed) {
        lineFeed = lineFeed / 4 * 4;
        if (lineFeed < 0) {
            lineFeed = 0;
        }
        int codeLength = (src.length + 2) / 3 * 4;
        if (lineFeed > 0) {
            codeLength += (codeLength - 1) / lineFeed;
        }
        final byte[] dst = new byte[codeLength];
        int didx = 0;
        int sidx = 0;
        int lf = 0;
        while (sidx + 3 <= src.length) {
            int bits24 = (src[sidx++] & 0xFF) << 16;
            bits24 |= (src[sidx++] & 0xFF) << 8;
            bits24 |= (src[sidx++] & 0xFF) << 0;
            int bits25 = (bits24 & 0xFC0000) >> 18;
            dst[didx++] = Base64.base64[bits25];
            bits25 = (bits24 & 0x3F000) >> 12;
            dst[didx++] = Base64.base64[bits25];
            bits25 = (bits24 & 0xFC0) >> 6;
            dst[didx++] = Base64.base64[bits25];
            bits25 = (bits24 & 0x3F);
            dst[didx++] = Base64.base64[bits25];
            lf += 4;
            if (didx < codeLength && lineFeed > 0 && lf % lineFeed == 0) {
                dst[didx++] = 10;
            }
        }
        if (src.length - sidx == 2) {
            int bits24 = (src[sidx] & 0xFF) << 16;
            bits24 |= (src[sidx + 1] & 0xFF) << 8;
            int bits25 = (bits24 & 0xFC0000) >> 18;
            dst[didx++] = Base64.base64[bits25];
            bits25 = (bits24 & 0x3F000) >> 12;
            dst[didx++] = Base64.base64[bits25];
            bits25 = (bits24 & 0xFC0) >> 6;
            dst[didx++] = Base64.base64[bits25];
            dst[didx++] = 61;
        }
        else if (src.length - sidx == 1) {
            final int bits24 = (src[sidx] & 0xFF) << 16;
            int bits25 = (bits24 & 0xFC0000) >> 18;
            dst[didx++] = Base64.base64[bits25];
            bits25 = (bits24 & 0x3F000) >> 12;
            dst[didx++] = Base64.base64[bits25];
            dst[didx++] = 61;
            dst[didx++] = 61;
        }
        return dst;
    }
    
    public static final String encode(final String src) {
        return new String(encode(src.getBytes()));
    }
    
    public static final byte[] decode(final byte[] src) throws IllegalArgumentException {
        int srcLen = 0;
        for (int sidx = 0; sidx < src.length; ++sidx) {
            final byte val = Base64.ascii[src[sidx]];
            if (val >= 0) {
                src[srcLen++] = val;
            }
            else if (val == -1) {
                throw new IllegalArgumentException("Invalid base 64 string");
            }
        }
        while (srcLen > 0 && src[srcLen - 1] == -3) {
            --srcLen;
        }
        final byte[] dst = new byte[srcLen * 3 / 4];
        int sidx = 0;
        int didx;
        for (didx = 0; didx < dst.length - 2; didx += 3) {
            dst[didx] = (byte)((src[sidx] << 2 & 0xFF) | (src[sidx + 1] >>> 4 & 0x3));
            dst[didx + 1] = (byte)((src[sidx + 1] << 4 & 0xFF) | (src[sidx + 2] >>> 2 & 0xF));
            dst[didx + 2] = (byte)((src[sidx + 2] << 6 & 0xFF) | (src[sidx + 3] & 0x3F));
            sidx += 4;
        }
        if (didx < dst.length) {
            dst[didx] = (byte)((src[sidx] << 2 & 0xFF) | (src[sidx + 1] >>> 4 & 0x3));
        }
        if (++didx < dst.length) {
            dst[didx] = (byte)((src[sidx + 1] << 4 & 0xFF) | (src[sidx + 2] >>> 2 & 0xF));
        }
        return dst;
    }
    
    public static final String decode(final String src) {
        return new String(decode(src.getBytes()));
    }
    
    static {
        Base64.base64 = new byte[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47 };
        Base64.ascii = new byte[255];
        for (int idx = 0; idx < 255; ++idx) {
            Base64.ascii[idx] = -1;
        }
        for (int idx = 0; idx < Base64.base64.length; ++idx) {
            Base64.ascii[Base64.base64[idx]] = (byte)idx;
        }
        Base64.ascii[9] = -2;
        Base64.ascii[10] = -2;
        Base64.ascii[13] = -2;
        Base64.ascii[32] = -2;
        Base64.ascii[61] = -3;
    }
}
