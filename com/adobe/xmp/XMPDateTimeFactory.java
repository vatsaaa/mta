// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp;

import java.util.Date;
import java.util.GregorianCalendar;
import com.adobe.xmp.impl.XMPDateTimeImpl;
import java.util.Calendar;
import java.util.TimeZone;

public final class XMPDateTimeFactory
{
    private static final TimeZone UTC;
    
    private XMPDateTimeFactory() {
    }
    
    public static XMPDateTime createFromCalendar(final Calendar calendar) {
        return new XMPDateTimeImpl(calendar);
    }
    
    public static XMPDateTime create() {
        return new XMPDateTimeImpl();
    }
    
    public static XMPDateTime create(final int year, final int month, final int day) {
        final XMPDateTime dt = new XMPDateTimeImpl();
        dt.setYear(year);
        dt.setMonth(month);
        dt.setDay(day);
        return dt;
    }
    
    public static XMPDateTime create(final int year, final int month, final int day, final int hour, final int minute, final int second, final int nanoSecond) {
        final XMPDateTime dt = new XMPDateTimeImpl();
        dt.setYear(year);
        dt.setMonth(month);
        dt.setDay(day);
        dt.setHour(hour);
        dt.setMinute(minute);
        dt.setSecond(second);
        dt.setNanoSecond(nanoSecond);
        return dt;
    }
    
    public static XMPDateTime createFromISO8601(final String strValue) throws XMPException {
        return new XMPDateTimeImpl(strValue);
    }
    
    public static XMPDateTime getCurrentDateTime() {
        return new XMPDateTimeImpl(new GregorianCalendar());
    }
    
    public static XMPDateTime setLocalTimeZone(final XMPDateTime dateTime) {
        final Calendar cal = dateTime.getCalendar();
        cal.setTimeZone(TimeZone.getDefault());
        return new XMPDateTimeImpl(cal);
    }
    
    public static XMPDateTime convertToUTCTime(final XMPDateTime dateTime) {
        final long timeInMillis = dateTime.getCalendar().getTimeInMillis();
        final GregorianCalendar cal = new GregorianCalendar(XMPDateTimeFactory.UTC);
        cal.setGregorianChange(new Date(Long.MIN_VALUE));
        cal.setTimeInMillis(timeInMillis);
        return new XMPDateTimeImpl(cal);
    }
    
    public static XMPDateTime convertToLocalTime(final XMPDateTime dateTime) {
        final long timeInMillis = dateTime.getCalendar().getTimeInMillis();
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTimeInMillis(timeInMillis);
        return new XMPDateTimeImpl(cal);
    }
    
    static {
        UTC = TimeZone.getTimeZone("UTC");
    }
}
