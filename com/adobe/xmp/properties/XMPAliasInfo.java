// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.properties;

import com.adobe.xmp.options.AliasOptions;

public interface XMPAliasInfo
{
    String getNamespace();
    
    String getPrefix();
    
    String getPropName();
    
    AliasOptions getAliasForm();
}
