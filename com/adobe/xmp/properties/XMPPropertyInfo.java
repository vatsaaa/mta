// 
// Decompiled by Procyon v0.5.36
// 

package com.adobe.xmp.properties;

import com.adobe.xmp.options.PropertyOptions;

public interface XMPPropertyInfo extends XMPProperty
{
    String getNamespace();
    
    String getPath();
    
    String getValue();
    
    PropertyOptions getOptions();
}
