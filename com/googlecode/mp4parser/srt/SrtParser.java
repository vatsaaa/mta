// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.srt;

import java.io.IOException;
import java.io.Reader;
import java.io.LineNumberReader;
import java.io.InputStreamReader;
import com.googlecode.mp4parser.authoring.tracks.TextTrackImpl;
import java.io.InputStream;

public class SrtParser
{
    public static TextTrackImpl parse(final InputStream is) throws IOException {
        final LineNumberReader r = new LineNumberReader(new InputStreamReader(is, "UTF-8"));
        final TextTrackImpl track = new TextTrackImpl();
        while (r.readLine() != null) {
            final String timeString = r.readLine();
            String lineString = "";
            String s;
            while ((s = r.readLine()) != null && !s.trim().equals("")) {
                lineString = String.valueOf(lineString) + s + "\n";
            }
            final long startTime = parse(timeString.split("-->")[0]);
            final long endTime = parse(timeString.split("-->")[1]);
            track.getSubs().add(new TextTrackImpl.Line(startTime, endTime, lineString));
        }
        return track;
    }
    
    private static long parse(final String in) {
        final long hours = Long.parseLong(in.split(":")[0].trim());
        final long minutes = Long.parseLong(in.split(":")[1].trim());
        final long seconds = Long.parseLong(in.split(":")[2].split(",")[0].trim());
        final long millies = Long.parseLong(in.split(":")[2].split(",")[1].trim());
        return hours * 60L * 60L * 1000L + minutes * 60L * 1000L + seconds * 1000L + millies;
    }
}
