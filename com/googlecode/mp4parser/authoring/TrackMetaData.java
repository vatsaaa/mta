// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring;

import java.util.Date;

public class TrackMetaData implements Cloneable
{
    private String language;
    private long timescale;
    private Date modificationTime;
    private Date creationTime;
    private double width;
    private double height;
    private float volume;
    private long trackId;
    private int group;
    private double startTime;
    int layer;
    
    public TrackMetaData() {
        this.trackId = 1L;
        this.group = 0;
        this.startTime = 0.0;
    }
    
    public String getLanguage() {
        return this.language;
    }
    
    public void setLanguage(final String language) {
        this.language = language;
    }
    
    public long getTimescale() {
        return this.timescale;
    }
    
    public void setTimescale(final long timescale) {
        this.timescale = timescale;
    }
    
    public Date getModificationTime() {
        return this.modificationTime;
    }
    
    public void setModificationTime(final Date modificationTime) {
        this.modificationTime = modificationTime;
    }
    
    public Date getCreationTime() {
        return this.creationTime;
    }
    
    public void setCreationTime(final Date creationTime) {
        this.creationTime = creationTime;
    }
    
    public double getWidth() {
        return this.width;
    }
    
    public void setWidth(final double width) {
        this.width = width;
    }
    
    public double getHeight() {
        return this.height;
    }
    
    public void setHeight(final double height) {
        this.height = height;
    }
    
    public long getTrackId() {
        return this.trackId;
    }
    
    public void setTrackId(final long trackId) {
        this.trackId = trackId;
    }
    
    public int getLayer() {
        return this.layer;
    }
    
    public void setLayer(final int layer) {
        this.layer = layer;
    }
    
    public float getVolume() {
        return this.volume;
    }
    
    public void setVolume(final float volume) {
        this.volume = volume;
    }
    
    public int getGroup() {
        return this.group;
    }
    
    public void setGroup(final int group) {
        this.group = group;
    }
    
    public double getStartTime() {
        return this.startTime;
    }
    
    public void setStartTime(final double startTime) {
        this.startTime = startTime;
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException ex) {
            return null;
        }
    }
}
