// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Movie
{
    List<Track> tracks;
    MovieMetaData movieMetaData;
    
    public Movie() {
        this.tracks = new LinkedList<Track>();
        this.movieMetaData = new MovieMetaData();
    }
    
    public List<Track> getTracks() {
        return this.tracks;
    }
    
    public void setTracks(final List<Track> tracks) {
        this.tracks = tracks;
    }
    
    public void addTrack(final Track nuTrack) {
        if (this.getTrackByTrackId(nuTrack.getTrackMetaData().getTrackId()) != null) {
            nuTrack.getTrackMetaData().setTrackId(this.getNextTrackId());
        }
        this.tracks.add(nuTrack);
    }
    
    public MovieMetaData getMovieMetaData() {
        return this.movieMetaData;
    }
    
    public void setMovieMetaData(final MovieMetaData movieMetaData) {
        this.movieMetaData = movieMetaData;
    }
    
    @Override
    public String toString() {
        String s = "Movie{ ";
        for (final Track track : this.tracks) {
            s = String.valueOf(s) + "track_" + track.getTrackMetaData().getTrackId() + " (" + track.getHandler() + "), ";
        }
        s = String.valueOf(s) + ", movieMetaData=" + this.movieMetaData + '}';
        return s;
    }
    
    public long getNextTrackId() {
        long nextTrackId = 0L;
        for (final Track track : this.tracks) {
            nextTrackId = ((nextTrackId < track.getTrackMetaData().getTrackId()) ? track.getTrackMetaData().getTrackId() : nextTrackId);
        }
        return ++nextTrackId;
    }
    
    public Track getTrackByTrackId(final long trackId) {
        for (final Track track : this.tracks) {
            if (track.getTrackMetaData().getTrackId() == trackId) {
                return track;
            }
        }
        return null;
    }
    
    public long getTimescale() {
        long timescale = this.getTracks().iterator().next().getTrackMetaData().getTimescale();
        for (final Track track : this.getTracks()) {
            timescale = gcd(track.getTrackMetaData().getTimescale(), timescale);
        }
        return timescale;
    }
    
    public static long gcd(final long a, final long b) {
        if (b == 0L) {
            return a;
        }
        return gcd(b, a % b);
    }
}
