// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring;

public class MovieMetaData
{
    private String title;
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    @Override
    public String toString() {
        return "MovieMetaData{title='" + this.title + '\'' + '}';
    }
}
