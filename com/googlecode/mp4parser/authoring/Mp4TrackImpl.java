// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring;

import com.coremedia.iso.boxes.TrackHeaderBox;
import com.coremedia.iso.boxes.MediaHeaderBox;
import java.util.Iterator;
import com.coremedia.iso.boxes.SampleTableBox;
import com.coremedia.iso.boxes.CastUtils;
import com.coremedia.iso.boxes.fragment.TrackRunBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentBox;
import java.util.LinkedList;
import com.coremedia.iso.boxes.fragment.MovieExtendsBox;
import com.coremedia.iso.boxes.mdat.SampleList;
import com.coremedia.iso.boxes.TrackBox;
import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import java.nio.ByteBuffer;
import java.util.List;

public class Mp4TrackImpl extends AbstractTrack
{
    private List<ByteBuffer> samples;
    private SampleDescriptionBox sampleDescriptionBox;
    private List<TimeToSampleBox.Entry> decodingTimeEntries;
    private List<CompositionTimeToSample.Entry> compositionTimeEntries;
    private long[] syncSamples;
    private List<SampleDependencyTypeBox.Entry> sampleDependencies;
    private TrackMetaData trackMetaData;
    private String handler;
    private AbstractMediaHeaderBox mihd;
    
    public Mp4TrackImpl(final TrackBox trackBox) {
        this.trackMetaData = new TrackMetaData();
        this.samples = new SampleList(trackBox);
        final SampleTableBox stbl = trackBox.getMediaBox().getMediaInformationBox().getSampleTableBox();
        this.handler = trackBox.getMediaBox().getHandlerBox().getHandlerType();
        this.mihd = trackBox.getMediaBox().getMediaInformationBox().getMediaHeaderBox();
        this.sampleDescriptionBox = stbl.getSampleDescriptionBox();
        if (trackBox.getParent().getBoxes(MovieExtendsBox.class).size() > 0) {
            this.decodingTimeEntries = new LinkedList<TimeToSampleBox.Entry>();
            this.compositionTimeEntries = new LinkedList<CompositionTimeToSample.Entry>();
            this.sampleDependencies = new LinkedList<SampleDependencyTypeBox.Entry>();
            for (final MovieFragmentBox movieFragmentBox : trackBox.getIsoFile().getBoxes(MovieFragmentBox.class)) {
                final List<TrackFragmentBox> trafs = movieFragmentBox.getBoxes(TrackFragmentBox.class);
                for (final TrackFragmentBox traf : trafs) {
                    if (traf.getTrackFragmentHeaderBox().getTrackId() == trackBox.getTrackHeaderBox().getTrackId()) {
                        final List<TrackRunBox> truns = traf.getBoxes(TrackRunBox.class);
                        for (final TrackRunBox trun : truns) {
                            for (final TrackRunBox.Entry entry : trun.getEntries()) {
                                if (trun.isSampleDurationPresent()) {
                                    if (this.decodingTimeEntries.size() == 0 || this.decodingTimeEntries.get(this.decodingTimeEntries.size() - 1).getDelta() != entry.getSampleDuration()) {
                                        this.decodingTimeEntries.add(new TimeToSampleBox.Entry(1L, entry.getSampleDuration()));
                                    }
                                    else {
                                        final TimeToSampleBox.Entry e = this.decodingTimeEntries.get(this.decodingTimeEntries.size() - 1);
                                        e.setCount(e.getCount() + 1L);
                                    }
                                }
                                if (trun.isSampleCompositionTimeOffsetPresent()) {
                                    if (this.compositionTimeEntries.size() == 0 || this.compositionTimeEntries.get(this.compositionTimeEntries.size() - 1).getOffset() != entry.getSampleCompositionTimeOffset()) {
                                        this.compositionTimeEntries.add(new CompositionTimeToSample.Entry(1, CastUtils.l2i(entry.getSampleCompositionTimeOffset())));
                                    }
                                    else {
                                        final CompositionTimeToSample.Entry e2 = this.compositionTimeEntries.get(this.compositionTimeEntries.size() - 1);
                                        e2.setCount(e2.getCount() + 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            this.decodingTimeEntries = stbl.getTimeToSampleBox().getEntries();
            if (stbl.getCompositionTimeToSample() != null) {
                this.compositionTimeEntries = stbl.getCompositionTimeToSample().getEntries();
            }
            if (stbl.getSyncSampleBox() != null) {
                this.syncSamples = stbl.getSyncSampleBox().getSampleNumber();
            }
            if (stbl.getSampleDependencyTypeBox() != null) {
                this.sampleDependencies = stbl.getSampleDependencyTypeBox().getEntries();
            }
        }
        final MediaHeaderBox mdhd = trackBox.getMediaBox().getMediaHeaderBox();
        final TrackHeaderBox tkhd = trackBox.getTrackHeaderBox();
        this.setEnabled(tkhd.isEnabled());
        this.setInMovie(tkhd.isInMovie());
        this.setInPoster(tkhd.isInPoster());
        this.setInPreview(tkhd.isInPreview());
        this.trackMetaData.setTrackId(tkhd.getTrackId());
        this.trackMetaData.setCreationTime(DateHelper.convert(mdhd.getCreationTime()));
        this.trackMetaData.setLanguage(mdhd.getLanguage());
        this.trackMetaData.setModificationTime(DateHelper.convert(mdhd.getModificationTime()));
        this.trackMetaData.setTimescale(mdhd.getTimescale());
        this.trackMetaData.setHeight(tkhd.getHeight());
        this.trackMetaData.setWidth(tkhd.getWidth());
        this.trackMetaData.setLayer(tkhd.getLayer());
    }
    
    public List<ByteBuffer> getSamples() {
        return this.samples;
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.sampleDescriptionBox;
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        return this.decodingTimeEntries;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        return this.compositionTimeEntries;
    }
    
    public long[] getSyncSamples() {
        return this.syncSamples;
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        return this.sampleDependencies;
    }
    
    public TrackMetaData getTrackMetaData() {
        return this.trackMetaData;
    }
    
    public String getHandler() {
        return this.handler;
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return this.mihd;
    }
}
