// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring;

import java.util.Date;

public class DateHelper
{
    public static Date convert(final long secondsSince) {
        return new Date((secondsSince - 2082844800L) * 1000L);
    }
    
    public static long convert(final Date date) {
        return date.getTime() / 1000L + 2082844800L;
    }
}
