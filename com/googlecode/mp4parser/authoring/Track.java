// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring;

import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import java.nio.ByteBuffer;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.TimeToSampleBox;
import java.util.List;
import com.coremedia.iso.boxes.SampleDescriptionBox;

public interface Track
{
    SampleDescriptionBox getSampleDescriptionBox();
    
    List<TimeToSampleBox.Entry> getDecodingTimeEntries();
    
    List<CompositionTimeToSample.Entry> getCompositionTimeEntries();
    
    long[] getSyncSamples();
    
    List<SampleDependencyTypeBox.Entry> getSampleDependencies();
    
    TrackMetaData getTrackMetaData();
    
    String getHandler();
    
    boolean isEnabled();
    
    boolean isInMovie();
    
    boolean isInPreview();
    
    boolean isInPoster();
    
    List<ByteBuffer> getSamples();
    
    AbstractMediaHeaderBox getMediaHeaderBox();
}
