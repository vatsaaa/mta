// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.container.mp4;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.Mp4TrackImpl;
import com.coremedia.iso.boxes.TrackBox;
import com.coremedia.iso.IsoFile;
import com.googlecode.mp4parser.authoring.Movie;
import java.nio.channels.ReadableByteChannel;

public class MovieCreator
{
    public static Movie build(final ReadableByteChannel channel) throws IOException {
        final IsoFile isoFile = new IsoFile(channel);
        final Movie m = new Movie();
        final List<TrackBox> trackBoxes = isoFile.getMovieBox().getBoxes(TrackBox.class);
        for (final TrackBox trackBox : trackBoxes) {
            m.addTrack(new Mp4TrackImpl(trackBox));
        }
        return m;
    }
}
