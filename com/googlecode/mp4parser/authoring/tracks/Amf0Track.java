// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.tracks;

import com.coremedia.iso.boxes.NullMediaHeaderBox;
import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import java.util.Collections;
import java.util.Collection;
import java.util.Set;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.Box;
import com.googlecode.mp4parser.boxes.adobe.ActionMessageFormat0SampleEntryBox;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import java.util.Iterator;
import java.util.LinkedList;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Date;
import java.util.TreeMap;
import java.util.Map;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import java.util.SortedMap;
import com.googlecode.mp4parser.authoring.AbstractTrack;

public class Amf0Track extends AbstractTrack
{
    SortedMap<Long, byte[]> rawSamples;
    private TrackMetaData trackMetaData;
    
    public Amf0Track(final Map<Long, byte[]> rawSamples) {
        this.rawSamples = new TreeMap<Long, byte[]>() {};
        this.trackMetaData = new TrackMetaData();
        this.rawSamples = new TreeMap<Long, byte[]>(rawSamples);
        this.trackMetaData.setCreationTime(new Date());
        this.trackMetaData.setModificationTime(new Date());
        this.trackMetaData.setTimescale(1000L);
        this.trackMetaData.setLanguage("eng");
    }
    
    public List<ByteBuffer> getSamples() {
        final LinkedList<ByteBuffer> samples = new LinkedList<ByteBuffer>();
        for (final byte[] bytes : this.rawSamples.values()) {
            samples.add(ByteBuffer.wrap(bytes));
        }
        return samples;
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        final SampleDescriptionBox stsd = new SampleDescriptionBox();
        final ActionMessageFormat0SampleEntryBox amf0 = new ActionMessageFormat0SampleEntryBox();
        amf0.setDataReferenceIndex(1);
        stsd.addBox(amf0);
        return stsd;
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        final LinkedList<TimeToSampleBox.Entry> timesToSample = new LinkedList<TimeToSampleBox.Entry>();
        final LinkedList<Long> keys = new LinkedList<Long>(this.rawSamples.keySet());
        Collections.sort(keys);
        long lastTimeStamp = 0L;
        for (final Long key : keys) {
            final long delta = key - lastTimeStamp;
            if (timesToSample.size() > 0 && timesToSample.peek().getDelta() == delta) {
                timesToSample.peek().setCount(timesToSample.peek().getCount() + 1L);
            }
            else {
                timesToSample.add(new TimeToSampleBox.Entry(1L, delta));
            }
            lastTimeStamp = key;
        }
        return timesToSample;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        return null;
    }
    
    public long[] getSyncSamples() {
        return null;
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        return null;
    }
    
    public TrackMetaData getTrackMetaData() {
        return this.trackMetaData;
    }
    
    public String getHandler() {
        return "data";
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return new NullMediaHeaderBox();
    }
}
