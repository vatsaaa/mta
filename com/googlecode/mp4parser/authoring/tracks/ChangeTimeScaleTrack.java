// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.tracks;

import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import java.util.Iterator;
import java.util.ArrayList;
import java.nio.ByteBuffer;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import java.util.List;
import com.googlecode.mp4parser.authoring.Track;

public class ChangeTimeScaleTrack implements Track
{
    Track source;
    List<CompositionTimeToSample.Entry> ctts;
    List<TimeToSampleBox.Entry> tts;
    long timeScale;
    
    public ChangeTimeScaleTrack(final Track source, final long timeScale) {
        this.source = source;
        this.timeScale = timeScale;
        final double timeScaleFactor = timeScale / (double)source.getTrackMetaData().getTimescale();
        this.ctts = adjustCtts(source.getCompositionTimeEntries(), timeScaleFactor);
        this.tts = adjustTts(source.getDecodingTimeEntries(), timeScaleFactor);
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.source.getSampleDescriptionBox();
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        return this.tts;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        return this.ctts;
    }
    
    public long[] getSyncSamples() {
        return this.source.getSyncSamples();
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        return this.source.getSampleDependencies();
    }
    
    public TrackMetaData getTrackMetaData() {
        final TrackMetaData trackMetaData = (TrackMetaData)this.source.getTrackMetaData().clone();
        trackMetaData.setTimescale(this.timeScale);
        return trackMetaData;
    }
    
    public String getHandler() {
        return this.source.getHandler();
    }
    
    public boolean isEnabled() {
        return this.source.isEnabled();
    }
    
    public boolean isInMovie() {
        return this.source.isInMovie();
    }
    
    public boolean isInPreview() {
        return this.source.isInPreview();
    }
    
    public boolean isInPoster() {
        return this.source.isInPoster();
    }
    
    public List<ByteBuffer> getSamples() {
        return this.source.getSamples();
    }
    
    static List<CompositionTimeToSample.Entry> adjustCtts(final List<CompositionTimeToSample.Entry> source, final double timeScaleFactor) {
        if (source != null) {
            final List<CompositionTimeToSample.Entry> entries2 = new ArrayList<CompositionTimeToSample.Entry>(source.size());
            double deviation = 0.0;
            for (final CompositionTimeToSample.Entry entry : source) {
                final double d = timeScaleFactor * entry.getOffset() + deviation;
                final int x = (int)Math.round(d);
                deviation = d - x;
                entries2.add(new CompositionTimeToSample.Entry(entry.getCount(), x));
            }
            return entries2;
        }
        return null;
    }
    
    static List<TimeToSampleBox.Entry> adjustTts(final List<TimeToSampleBox.Entry> source, final double timeScaleFactor) {
        double deviation = 0.0;
        final List<TimeToSampleBox.Entry> entries2 = new ArrayList<TimeToSampleBox.Entry>(source.size());
        for (final TimeToSampleBox.Entry entry : source) {
            final double d = timeScaleFactor * entry.getDelta() + deviation;
            final long x = Math.round(d);
            deviation = d - x;
            entries2.add(new TimeToSampleBox.Entry(entry.getCount(), x));
        }
        return entries2;
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return this.source.getMediaHeaderBox();
    }
}
