// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.tracks;

import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitReaderBuffer;
import com.coremedia.iso.boxes.SoundMediaHeaderBox;
import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import java.util.Iterator;
import java.util.Date;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.AbstractBox;
import com.googlecode.mp4parser.boxes.EC3SpecificBox;
import com.coremedia.iso.boxes.sampleentry.AudioSampleEntry;
import java.io.IOException;
import java.util.LinkedList;
import com.coremedia.iso.boxes.TimeToSampleBox;
import java.nio.ByteBuffer;
import java.io.InputStream;
import java.util.List;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import com.googlecode.mp4parser.authoring.AbstractTrack;

public class EC3TrackImpl extends AbstractTrack
{
    TrackMetaData trackMetaData;
    SampleDescriptionBox sampleDescriptionBox;
    int samplerate;
    int bitrate;
    int frameSize;
    List<BitStreamInfo> entries;
    private InputStream inputStream;
    private List<ByteBuffer> samples;
    List<TimeToSampleBox.Entry> stts;
    
    public EC3TrackImpl(final InputStream fin) throws IOException {
        this.trackMetaData = new TrackMetaData();
        this.entries = new LinkedList<BitStreamInfo>();
        this.stts = new LinkedList<TimeToSampleBox.Entry>();
        this.inputStream = fin;
        boolean done = false;
        while (!done) {
            final BitStreamInfo bsi = this.readVariables();
            if (bsi == null) {
                throw new IOException();
            }
            for (int i = 0; i < this.entries.size(); ++i) {
                if (bsi.strmtyp != 1 && this.entries.get(i).substreamid == bsi.substreamid) {
                    done = true;
                }
            }
            if (done) {
                continue;
            }
            this.entries.add(bsi);
            this.inputStream.skip(bsi.frameSize);
        }
        final Iterator<BitStreamInfo> iterator = this.entries.iterator();
        while (iterator.hasNext()) {
            final BitStreamInfo bsi = iterator.next();
            this.inputStream.skip(-1 * bsi.frameSize);
        }
        if (this.entries.size() == 0) {
            throw new IOException();
        }
        this.samplerate = this.entries.get(0).samplerate;
        this.sampleDescriptionBox = new SampleDescriptionBox();
        final AudioSampleEntry audioSampleEntry = new AudioSampleEntry("ec-3");
        audioSampleEntry.setChannelCount(2);
        audioSampleEntry.setSampleRate(this.samplerate);
        audioSampleEntry.setDataReferenceIndex(1);
        audioSampleEntry.setSampleSize(16);
        final EC3SpecificBox ec3 = new EC3SpecificBox();
        final int[] deps = new int[this.entries.size()];
        final int[] chan_locs = new int[this.entries.size()];
        for (final BitStreamInfo bsi2 : this.entries) {
            if (bsi2.strmtyp == 1) {
                final int[] array = deps;
                final int substreamid = bsi2.substreamid;
                ++array[substreamid];
                chan_locs[bsi2.substreamid] = ((bsi2.chanmap >> 6 & 0x100) | (bsi2.chanmap >> 5 & 0xFF));
            }
        }
        for (final BitStreamInfo bsi2 : this.entries) {
            if (bsi2.strmtyp != 1) {
                final EC3SpecificBox.Entry e = new EC3SpecificBox.Entry();
                e.fscod = bsi2.fscod;
                e.bsid = bsi2.bsid;
                e.bsmod = bsi2.bsmod;
                e.acmod = bsi2.acmod;
                e.lfeon = bsi2.lfeon;
                e.reserved = 0;
                e.num_dep_sub = deps[bsi2.substreamid];
                e.chan_loc = chan_locs[bsi2.substreamid];
                e.reserved2 = 0;
                ec3.addEntry(e);
            }
            this.bitrate += bsi2.bitrate;
            this.frameSize += bsi2.frameSize;
        }
        ec3.setDataRate(this.bitrate / 1000);
        audioSampleEntry.addBox(ec3);
        this.sampleDescriptionBox.addBox(audioSampleEntry);
        this.trackMetaData.setCreationTime(new Date());
        this.trackMetaData.setModificationTime(new Date());
        this.trackMetaData.setLanguage("eng");
        this.trackMetaData.setTimescale(this.samplerate);
        this.samples = new LinkedList<ByteBuffer>();
        if (!this.readSamples()) {
            throw new IOException();
        }
    }
    
    public List<ByteBuffer> getSamples() {
        return this.samples;
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.sampleDescriptionBox;
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        return this.stts;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        return null;
    }
    
    public long[] getSyncSamples() {
        return null;
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        return null;
    }
    
    public TrackMetaData getTrackMetaData() {
        return this.trackMetaData;
    }
    
    public String getHandler() {
        return "soun";
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return new SoundMediaHeaderBox();
    }
    
    private BitStreamInfo readVariables() throws IOException {
        final byte[] data = new byte[200];
        if (200 != this.inputStream.read(data, 0, 200)) {
            return null;
        }
        this.inputStream.skip(-200L);
        final ByteBuffer bb = ByteBuffer.wrap(data);
        final BitReaderBuffer brb = new BitReaderBuffer(bb);
        final int syncword = brb.readBits(16);
        if (syncword != 2935) {
            return null;
        }
        final BitStreamInfo entry = new BitStreamInfo();
        entry.strmtyp = brb.readBits(2);
        entry.substreamid = brb.readBits(3);
        final int frmsiz = brb.readBits(11);
        entry.frameSize = 2 * (frmsiz + 1);
        entry.fscod = brb.readBits(2);
        int fscod2 = -1;
        int numblkscod;
        if (entry.fscod == 3) {
            fscod2 = brb.readBits(2);
            numblkscod = 3;
        }
        else {
            numblkscod = brb.readBits(2);
        }
        int numberOfBlocksPerSyncFrame = 0;
        switch (numblkscod) {
            case 0: {
                numberOfBlocksPerSyncFrame = 1;
                break;
            }
            case 1: {
                numberOfBlocksPerSyncFrame = 2;
                break;
            }
            case 2: {
                numberOfBlocksPerSyncFrame = 3;
                break;
            }
            case 3: {
                numberOfBlocksPerSyncFrame = 6;
                break;
            }
        }
        entry.acmod = brb.readBits(3);
        entry.lfeon = brb.readBits(1);
        entry.bsid = brb.readBits(5);
        brb.readBits(5);
        if (1 == brb.readBits(1)) {
            brb.readBits(8);
        }
        if (entry.acmod == 0) {
            brb.readBits(5);
            if (1 == brb.readBits(1)) {
                brb.readBits(8);
            }
        }
        if (1 == entry.strmtyp && 1 == brb.readBits(1)) {
            entry.chanmap = brb.readBits(16);
        }
        if (1 == brb.readBits(1)) {
            if (entry.acmod > 2) {
                brb.readBits(2);
            }
            if (0x1 == (entry.acmod & 0x1) && entry.acmod > 2) {
                brb.readBits(3);
                brb.readBits(3);
            }
            if ((entry.acmod & 0x4) > 0) {
                brb.readBits(3);
                brb.readBits(3);
            }
            if (1 == entry.lfeon && 1 == brb.readBits(1)) {
                brb.readBits(5);
            }
            if (entry.strmtyp == 0) {
                if (1 == brb.readBits(1)) {
                    brb.readBits(6);
                }
                if (entry.acmod == 0 && 1 == brb.readBits(1)) {
                    brb.readBits(6);
                }
                if (1 == brb.readBits(1)) {
                    brb.readBits(6);
                }
                final int mixdef = brb.readBits(2);
                if (1 == mixdef) {
                    brb.readBits(5);
                }
                else if (2 == mixdef) {
                    brb.readBits(12);
                }
                else if (3 == mixdef) {
                    final int mixdeflen = brb.readBits(5);
                    if (1 == brb.readBits(1)) {
                        brb.readBits(5);
                        if (1 == brb.readBits(1)) {
                            brb.readBits(4);
                        }
                        if (1 == brb.readBits(1)) {
                            brb.readBits(4);
                        }
                        if (1 == brb.readBits(1)) {
                            brb.readBits(4);
                        }
                        if (1 == brb.readBits(1)) {
                            brb.readBits(4);
                        }
                        if (1 == brb.readBits(1)) {
                            brb.readBits(4);
                        }
                        if (1 == brb.readBits(1)) {
                            brb.readBits(4);
                        }
                        if (1 == brb.readBits(1)) {
                            brb.readBits(4);
                        }
                        if (1 == brb.readBits(1)) {
                            if (1 == brb.readBits(1)) {
                                brb.readBits(4);
                            }
                            if (1 == brb.readBits(1)) {
                                brb.readBits(4);
                            }
                        }
                    }
                    if (1 == brb.readBits(1)) {
                        brb.readBits(5);
                        if (1 == brb.readBits(1)) {
                            brb.readBits(7);
                            if (1 == brb.readBits(1)) {
                                brb.readBits(8);
                            }
                        }
                    }
                    for (int i = 0; i < mixdeflen + 2; ++i) {
                        brb.readBits(8);
                    }
                    brb.byteSync();
                }
                if (entry.acmod < 2) {
                    if (1 == brb.readBits(1)) {
                        brb.readBits(14);
                    }
                    if (entry.acmod == 0 && 1 == brb.readBits(1)) {
                        brb.readBits(14);
                    }
                    if (1 == brb.readBits(1)) {
                        if (numblkscod == 0) {
                            brb.readBits(5);
                        }
                        else {
                            for (int j = 0; j < numberOfBlocksPerSyncFrame; ++j) {
                                if (1 == brb.readBits(1)) {
                                    brb.readBits(5);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (1 == brb.readBits(1)) {
            entry.bsmod = brb.readBits(3);
        }
        Label_1043: {
            switch (entry.fscod) {
                case 0: {
                    entry.samplerate = 48000;
                    break;
                }
                case 1: {
                    entry.samplerate = 44100;
                    break;
                }
                case 2: {
                    entry.samplerate = 32000;
                    break;
                }
                case 3: {
                    switch (fscod2) {
                        case 0: {
                            entry.samplerate = 24000;
                            break Label_1043;
                        }
                        case 1: {
                            entry.samplerate = 22050;
                            break Label_1043;
                        }
                        case 2: {
                            entry.samplerate = 16000;
                            break Label_1043;
                        }
                        case 3: {
                            entry.samplerate = 0;
                            break Label_1043;
                        }
                    }
                    break;
                }
            }
        }
        if (entry.samplerate == 0) {
            return null;
        }
        entry.bitrate = (int)(entry.samplerate / 1536.0 * entry.frameSize * 8.0);
        return entry;
    }
    
    private boolean readSamples() throws IOException {
        int read = this.frameSize;
        boolean ret = false;
        while (this.frameSize == read) {
            ret = true;
            final byte[] data = new byte[this.frameSize];
            read = this.inputStream.read(data);
            if (read == this.frameSize) {
                this.samples.add(ByteBuffer.wrap(data));
                this.stts.add(new TimeToSampleBox.Entry(1L, 1536L));
            }
        }
        return ret;
    }
    
    public static class BitStreamInfo extends EC3SpecificBox.Entry
    {
        public int frameSize;
        public int substreamid;
        public int bitrate;
        public int samplerate;
        public int strmtyp;
        public int chanmap;
    }
}
