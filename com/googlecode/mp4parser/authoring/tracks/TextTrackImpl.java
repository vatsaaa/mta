// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.tracks;

import com.coremedia.iso.boxes.NullMediaHeaderBox;
import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.TimeToSampleBox;
import java.util.Iterator;
import java.io.IOException;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Date;
import com.coremedia.iso.boxes.AbstractBox;
import java.util.Collections;
import com.googlecode.mp4parser.boxes.threegpp26245.FontTableBox;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.sampleentry.TextSampleEntry;
import java.util.LinkedList;
import java.util.List;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import com.googlecode.mp4parser.authoring.AbstractTrack;

public class TextTrackImpl extends AbstractTrack
{
    TrackMetaData trackMetaData;
    SampleDescriptionBox sampleDescriptionBox;
    List<Line> subs;
    
    public List<Line> getSubs() {
        return this.subs;
    }
    
    public TextTrackImpl() {
        this.trackMetaData = new TrackMetaData();
        this.subs = new LinkedList<Line>();
        this.sampleDescriptionBox = new SampleDescriptionBox();
        final TextSampleEntry tx3g = new TextSampleEntry("tx3g");
        tx3g.setStyleRecord(new TextSampleEntry.StyleRecord());
        tx3g.setBoxRecord(new TextSampleEntry.BoxRecord());
        this.sampleDescriptionBox.addBox(tx3g);
        final FontTableBox ftab = new FontTableBox();
        ftab.setEntries(Collections.singletonList(new FontTableBox.FontRecord(1, "Serif")));
        tx3g.addBox(ftab);
        this.trackMetaData.setCreationTime(new Date());
        this.trackMetaData.setModificationTime(new Date());
        this.trackMetaData.setTimescale(1000L);
    }
    
    public List<ByteBuffer> getSamples() {
        final List<ByteBuffer> samples = new LinkedList<ByteBuffer>();
        long lastEnd = 0L;
        for (final Line sub : this.subs) {
            final long silentTime = sub.from - lastEnd;
            if (silentTime > 0L) {
                samples.add(ByteBuffer.wrap(new byte[2]));
            }
            else if (silentTime < 0L) {
                throw new Error("Subtitle display times may not intersect");
            }
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final DataOutputStream dos = new DataOutputStream(baos);
            try {
                dos.writeShort(sub.text.getBytes("UTF-8").length);
                dos.write(sub.text.getBytes("UTF-8"));
                dos.close();
            }
            catch (IOException ex) {
                throw new Error("VM is broken. Does not support UTF-8");
            }
            samples.add(ByteBuffer.wrap(baos.toByteArray()));
            lastEnd = sub.to;
        }
        return samples;
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.sampleDescriptionBox;
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        final List<TimeToSampleBox.Entry> stts = new LinkedList<TimeToSampleBox.Entry>();
        long lastEnd = 0L;
        for (final Line sub : this.subs) {
            final long silentTime = sub.from - lastEnd;
            if (silentTime > 0L) {
                stts.add(new TimeToSampleBox.Entry(1L, silentTime));
            }
            else if (silentTime < 0L) {
                throw new Error("Subtitle display times may not intersect");
            }
            stts.add(new TimeToSampleBox.Entry(1L, sub.to - sub.from));
            lastEnd = sub.to;
        }
        return stts;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        return null;
    }
    
    public long[] getSyncSamples() {
        return null;
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        return null;
    }
    
    public TrackMetaData getTrackMetaData() {
        return this.trackMetaData;
    }
    
    public String getHandler() {
        return "text";
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return new NullMediaHeaderBox();
    }
    
    public static class Line
    {
        long from;
        long to;
        String text;
        
        public Line(final long from, final long to, final String text) {
            this.from = from;
            this.to = to;
            this.text = text;
        }
        
        public long getFrom() {
            return this.from;
        }
        
        public String getText() {
            return this.text;
        }
        
        public long getTo() {
            return this.to;
        }
    }
}
