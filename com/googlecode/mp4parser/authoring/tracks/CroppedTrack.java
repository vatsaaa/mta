// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.tracks;

import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import java.util.LinkedList;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import java.nio.ByteBuffer;
import java.util.List;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.AbstractTrack;

public class CroppedTrack extends AbstractTrack
{
    Track origTrack;
    private int fromSample;
    private int toSample;
    private long[] syncSampleArray;
    
    public CroppedTrack(final Track origTrack, final long fromSample, final long toSample) {
        this.origTrack = origTrack;
        assert fromSample <= 2147483647L;
        assert toSample <= 2147483647L;
        this.fromSample = (int)fromSample;
        this.toSample = (int)toSample;
    }
    
    public List<ByteBuffer> getSamples() {
        return this.origTrack.getSamples().subList(this.fromSample, this.toSample);
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.origTrack.getSampleDescriptionBox();
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        if (this.origTrack.getDecodingTimeEntries() != null && !this.origTrack.getDecodingTimeEntries().isEmpty()) {
            final long[] decodingTimes = TimeToSampleBox.blowupTimeToSamples(this.origTrack.getDecodingTimeEntries());
            final long[] nuDecodingTimes = new long[this.toSample - this.fromSample];
            System.arraycopy(decodingTimes, this.fromSample, nuDecodingTimes, 0, this.toSample - this.fromSample);
            final LinkedList<TimeToSampleBox.Entry> returnDecodingEntries = new LinkedList<TimeToSampleBox.Entry>();
            long[] array;
            for (int length = (array = nuDecodingTimes).length, i = 0; i < length; ++i) {
                final long nuDecodingTime = array[i];
                if (returnDecodingEntries.isEmpty() || returnDecodingEntries.getLast().getDelta() != nuDecodingTime) {
                    final TimeToSampleBox.Entry e = new TimeToSampleBox.Entry(1L, nuDecodingTime);
                    returnDecodingEntries.add(e);
                }
                else {
                    final TimeToSampleBox.Entry e = returnDecodingEntries.getLast();
                    e.setCount(e.getCount() + 1L);
                }
            }
            return returnDecodingEntries;
        }
        return null;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        if (this.origTrack.getCompositionTimeEntries() != null && !this.origTrack.getCompositionTimeEntries().isEmpty()) {
            final int[] compositionTime = CompositionTimeToSample.blowupCompositionTimes(this.origTrack.getCompositionTimeEntries());
            final int[] nuCompositionTimes = new int[this.toSample - this.fromSample];
            System.arraycopy(compositionTime, this.fromSample, nuCompositionTimes, 0, this.toSample - this.fromSample);
            final LinkedList<CompositionTimeToSample.Entry> returnDecodingEntries = new LinkedList<CompositionTimeToSample.Entry>();
            int[] array;
            for (int length = (array = nuCompositionTimes).length, i = 0; i < length; ++i) {
                final int nuDecodingTime = array[i];
                if (returnDecodingEntries.isEmpty() || returnDecodingEntries.getLast().getOffset() != nuDecodingTime) {
                    final CompositionTimeToSample.Entry e = new CompositionTimeToSample.Entry(1, nuDecodingTime);
                    returnDecodingEntries.add(e);
                }
                else {
                    final CompositionTimeToSample.Entry e = returnDecodingEntries.getLast();
                    e.setCount(e.getCount() + 1);
                }
            }
            return returnDecodingEntries;
        }
        return null;
    }
    
    public synchronized long[] getSyncSamples() {
        if (this.syncSampleArray != null) {
            return this.syncSampleArray;
        }
        if (this.origTrack.getSyncSamples() != null && this.origTrack.getSyncSamples().length > 0) {
            final List<Long> syncSamples = new LinkedList<Long>();
            long[] syncSamples2;
            for (int length = (syncSamples2 = this.origTrack.getSyncSamples()).length, j = 0; j < length; ++j) {
                final long l = syncSamples2[j];
                if (l >= this.fromSample && l < this.toSample) {
                    syncSamples.add(l - this.fromSample);
                }
            }
            this.syncSampleArray = new long[syncSamples.size()];
            for (int i = 0; i < this.syncSampleArray.length; ++i) {
                this.syncSampleArray[i] = syncSamples.get(i);
            }
            return this.syncSampleArray;
        }
        return null;
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        if (this.origTrack.getSampleDependencies() != null && !this.origTrack.getSampleDependencies().isEmpty()) {
            return this.origTrack.getSampleDependencies().subList(this.fromSample, this.toSample);
        }
        return null;
    }
    
    public TrackMetaData getTrackMetaData() {
        return this.origTrack.getTrackMetaData();
    }
    
    public String getHandler() {
        return this.origTrack.getHandler();
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return this.origTrack.getMediaHeaderBox();
    }
}
