// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.tracks;

import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitReaderBuffer;
import com.coremedia.iso.boxes.SoundMediaHeaderBox;
import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import java.util.Date;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.AbstractBox;
import com.googlecode.mp4parser.boxes.AC3SpecificBox;
import com.coremedia.iso.boxes.sampleentry.AudioSampleEntry;
import java.io.IOException;
import java.util.LinkedList;
import com.coremedia.iso.boxes.TimeToSampleBox;
import java.nio.ByteBuffer;
import java.util.List;
import java.io.InputStream;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import com.googlecode.mp4parser.authoring.AbstractTrack;

public class AC3TrackImpl extends AbstractTrack
{
    TrackMetaData trackMetaData;
    SampleDescriptionBox sampleDescriptionBox;
    int samplerate;
    int bitrate;
    int fscod;
    int bsid;
    int bsmod;
    int acmod;
    int lfeon;
    int frmsizecod;
    int frameSize;
    int[][][][] bitRateAndFrameSizeTable;
    private InputStream inputStream;
    private List<ByteBuffer> samples;
    List<TimeToSampleBox.Entry> stts;
    
    public AC3TrackImpl(final InputStream fin) throws IOException {
        this.trackMetaData = new TrackMetaData();
        this.inputStream = fin;
        this.bitRateAndFrameSizeTable = new int[19][2][3][2];
        this.stts = new LinkedList<TimeToSampleBox.Entry>();
        this.initBitRateAndFrameSizeTable();
        if (!this.readVariables()) {
            throw new IOException();
        }
        this.sampleDescriptionBox = new SampleDescriptionBox();
        final AudioSampleEntry audioSampleEntry = new AudioSampleEntry("ac-3");
        audioSampleEntry.setChannelCount(2);
        audioSampleEntry.setSampleRate(this.samplerate);
        audioSampleEntry.setDataReferenceIndex(1);
        audioSampleEntry.setSampleSize(16);
        final AC3SpecificBox ac3 = new AC3SpecificBox();
        ac3.setAcmod(this.acmod);
        ac3.setBitRateCode(this.frmsizecod >> 1);
        ac3.setBsid(this.bsid);
        ac3.setBsmod(this.bsmod);
        ac3.setFscod(this.fscod);
        ac3.setLfeon(this.lfeon);
        ac3.setReserved(0);
        audioSampleEntry.addBox(ac3);
        this.sampleDescriptionBox.addBox(audioSampleEntry);
        this.trackMetaData.setCreationTime(new Date());
        this.trackMetaData.setModificationTime(new Date());
        this.trackMetaData.setLanguage("eng");
        this.trackMetaData.setTimescale(this.samplerate);
        this.samples = new LinkedList<ByteBuffer>();
        if (!this.readSamples()) {
            throw new IOException();
        }
    }
    
    public List<ByteBuffer> getSamples() {
        return this.samples;
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.sampleDescriptionBox;
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        return this.stts;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        return null;
    }
    
    public long[] getSyncSamples() {
        return null;
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        return null;
    }
    
    public TrackMetaData getTrackMetaData() {
        return this.trackMetaData;
    }
    
    public String getHandler() {
        return "soun";
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return new SoundMediaHeaderBox();
    }
    
    private boolean readVariables() throws IOException {
        final byte[] data = new byte[100];
        if (100 != this.inputStream.read(data, 0, 100)) {
            return false;
        }
        this.inputStream.skip(-100L);
        final ByteBuffer bb = ByteBuffer.wrap(data);
        final BitReaderBuffer brb = new BitReaderBuffer(bb);
        final int syncword = brb.readBits(16);
        if (syncword != 2935) {
            return false;
        }
        brb.readBits(16);
        switch (this.fscod = brb.readBits(2)) {
            case 0: {
                this.samplerate = 48000;
                break;
            }
            case 1: {
                this.samplerate = 44100;
                break;
            }
            case 2: {
                this.samplerate = 32000;
                break;
            }
            case 3: {
                this.samplerate = 0;
                break;
            }
        }
        if (this.samplerate == 0) {
            return false;
        }
        this.frmsizecod = brb.readBits(6);
        if (!this.calcBitrateAndFrameSize(this.frmsizecod)) {
            return false;
        }
        if (this.frameSize == 0) {
            return false;
        }
        this.bsid = brb.readBits(5);
        this.bsmod = brb.readBits(3);
        this.acmod = brb.readBits(3);
        if (this.acmod != 1 && (this.acmod & 0x1) == 0x1) {
            brb.readBits(2);
        }
        if ((this.acmod & 0x4) != 0x0) {
            brb.readBits(2);
        }
        if (this.acmod == 2) {
            brb.readBits(2);
        }
        this.lfeon = brb.readBits(1);
        return true;
    }
    
    private boolean calcBitrateAndFrameSize(final int code) {
        final int frmsizecode = code >>> 1;
        final int flag = code & 0x1;
        if (frmsizecode > 18 || flag > 1 || this.fscod > 2) {
            return false;
        }
        this.bitrate = this.bitRateAndFrameSizeTable[frmsizecode][flag][this.fscod][0];
        this.frameSize = 2 * this.bitRateAndFrameSizeTable[frmsizecode][flag][this.fscod][1];
        return true;
    }
    
    private boolean readSamples() throws IOException {
        int read = this.frameSize;
        boolean ret = false;
        while (this.frameSize == read) {
            ret = true;
            final byte[] data = new byte[this.frameSize];
            read = this.inputStream.read(data);
            if (read == this.frameSize) {
                this.samples.add(ByteBuffer.wrap(data));
                this.stts.add(new TimeToSampleBox.Entry(1L, 1536L));
            }
        }
        return ret;
    }
    
    private void initBitRateAndFrameSizeTable() {
        this.bitRateAndFrameSizeTable[0][0][0][0] = 32;
        this.bitRateAndFrameSizeTable[0][1][0][0] = 32;
        this.bitRateAndFrameSizeTable[0][0][0][1] = 64;
        this.bitRateAndFrameSizeTable[0][1][0][1] = 64;
        this.bitRateAndFrameSizeTable[1][0][0][0] = 40;
        this.bitRateAndFrameSizeTable[1][1][0][0] = 40;
        this.bitRateAndFrameSizeTable[1][0][0][1] = 80;
        this.bitRateAndFrameSizeTable[1][1][0][1] = 80;
        this.bitRateAndFrameSizeTable[2][0][0][0] = 48;
        this.bitRateAndFrameSizeTable[2][1][0][0] = 48;
        this.bitRateAndFrameSizeTable[2][0][0][1] = 96;
        this.bitRateAndFrameSizeTable[2][1][0][1] = 96;
        this.bitRateAndFrameSizeTable[3][0][0][0] = 56;
        this.bitRateAndFrameSizeTable[3][1][0][0] = 56;
        this.bitRateAndFrameSizeTable[3][0][0][1] = 112;
        this.bitRateAndFrameSizeTable[3][1][0][1] = 112;
        this.bitRateAndFrameSizeTable[4][0][0][0] = 64;
        this.bitRateAndFrameSizeTable[4][1][0][0] = 64;
        this.bitRateAndFrameSizeTable[4][0][0][1] = 128;
        this.bitRateAndFrameSizeTable[4][1][0][1] = 128;
        this.bitRateAndFrameSizeTable[5][0][0][0] = 80;
        this.bitRateAndFrameSizeTable[5][1][0][0] = 80;
        this.bitRateAndFrameSizeTable[5][0][0][1] = 160;
        this.bitRateAndFrameSizeTable[5][1][0][1] = 160;
        this.bitRateAndFrameSizeTable[6][0][0][0] = 96;
        this.bitRateAndFrameSizeTable[6][1][0][0] = 96;
        this.bitRateAndFrameSizeTable[6][0][0][1] = 192;
        this.bitRateAndFrameSizeTable[6][1][0][1] = 192;
        this.bitRateAndFrameSizeTable[7][0][0][0] = 112;
        this.bitRateAndFrameSizeTable[7][1][0][0] = 112;
        this.bitRateAndFrameSizeTable[7][0][0][1] = 224;
        this.bitRateAndFrameSizeTable[7][1][0][1] = 224;
        this.bitRateAndFrameSizeTable[8][0][0][0] = 128;
        this.bitRateAndFrameSizeTable[8][1][0][0] = 128;
        this.bitRateAndFrameSizeTable[8][0][0][1] = 256;
        this.bitRateAndFrameSizeTable[8][1][0][1] = 256;
        this.bitRateAndFrameSizeTable[9][0][0][0] = 160;
        this.bitRateAndFrameSizeTable[9][1][0][0] = 160;
        this.bitRateAndFrameSizeTable[9][0][0][1] = 320;
        this.bitRateAndFrameSizeTable[9][1][0][1] = 320;
        this.bitRateAndFrameSizeTable[10][0][0][0] = 192;
        this.bitRateAndFrameSizeTable[10][1][0][0] = 192;
        this.bitRateAndFrameSizeTable[10][0][0][1] = 384;
        this.bitRateAndFrameSizeTable[10][1][0][1] = 384;
        this.bitRateAndFrameSizeTable[11][0][0][0] = 224;
        this.bitRateAndFrameSizeTable[11][1][0][0] = 224;
        this.bitRateAndFrameSizeTable[11][0][0][1] = 448;
        this.bitRateAndFrameSizeTable[11][1][0][1] = 448;
        this.bitRateAndFrameSizeTable[12][0][0][0] = 256;
        this.bitRateAndFrameSizeTable[12][1][0][0] = 256;
        this.bitRateAndFrameSizeTable[12][0][0][1] = 512;
        this.bitRateAndFrameSizeTable[12][1][0][1] = 512;
        this.bitRateAndFrameSizeTable[13][0][0][0] = 320;
        this.bitRateAndFrameSizeTable[13][1][0][0] = 320;
        this.bitRateAndFrameSizeTable[13][0][0][1] = 640;
        this.bitRateAndFrameSizeTable[13][1][0][1] = 640;
        this.bitRateAndFrameSizeTable[14][0][0][0] = 384;
        this.bitRateAndFrameSizeTable[14][1][0][0] = 384;
        this.bitRateAndFrameSizeTable[14][0][0][1] = 768;
        this.bitRateAndFrameSizeTable[14][1][0][1] = 768;
        this.bitRateAndFrameSizeTable[15][0][0][0] = 448;
        this.bitRateAndFrameSizeTable[15][1][0][0] = 448;
        this.bitRateAndFrameSizeTable[15][0][0][1] = 896;
        this.bitRateAndFrameSizeTable[15][1][0][1] = 896;
        this.bitRateAndFrameSizeTable[16][0][0][0] = 512;
        this.bitRateAndFrameSizeTable[16][1][0][0] = 512;
        this.bitRateAndFrameSizeTable[16][0][0][1] = 1024;
        this.bitRateAndFrameSizeTable[16][1][0][1] = 1024;
        this.bitRateAndFrameSizeTable[17][0][0][0] = 576;
        this.bitRateAndFrameSizeTable[17][1][0][0] = 576;
        this.bitRateAndFrameSizeTable[17][0][0][1] = 1152;
        this.bitRateAndFrameSizeTable[17][1][0][1] = 1152;
        this.bitRateAndFrameSizeTable[18][0][0][0] = 640;
        this.bitRateAndFrameSizeTable[18][1][0][0] = 640;
        this.bitRateAndFrameSizeTable[18][0][0][1] = 1280;
        this.bitRateAndFrameSizeTable[18][1][0][1] = 1280;
        this.bitRateAndFrameSizeTable[0][0][1][0] = 32;
        this.bitRateAndFrameSizeTable[0][1][1][0] = 32;
        this.bitRateAndFrameSizeTable[0][0][1][1] = 69;
        this.bitRateAndFrameSizeTable[0][1][1][1] = 70;
        this.bitRateAndFrameSizeTable[1][0][1][0] = 40;
        this.bitRateAndFrameSizeTable[1][1][1][0] = 40;
        this.bitRateAndFrameSizeTable[1][0][1][1] = 87;
        this.bitRateAndFrameSizeTable[1][1][1][1] = 88;
        this.bitRateAndFrameSizeTable[2][0][1][0] = 48;
        this.bitRateAndFrameSizeTable[2][1][1][0] = 48;
        this.bitRateAndFrameSizeTable[2][0][1][1] = 104;
        this.bitRateAndFrameSizeTable[2][1][1][1] = 105;
        this.bitRateAndFrameSizeTable[3][0][1][0] = 56;
        this.bitRateAndFrameSizeTable[3][1][1][0] = 56;
        this.bitRateAndFrameSizeTable[3][0][1][1] = 121;
        this.bitRateAndFrameSizeTable[3][1][1][1] = 122;
        this.bitRateAndFrameSizeTable[4][0][1][0] = 64;
        this.bitRateAndFrameSizeTable[4][1][1][0] = 64;
        this.bitRateAndFrameSizeTable[4][0][1][1] = 139;
        this.bitRateAndFrameSizeTable[4][1][1][1] = 140;
        this.bitRateAndFrameSizeTable[5][0][1][0] = 80;
        this.bitRateAndFrameSizeTable[5][1][1][0] = 80;
        this.bitRateAndFrameSizeTable[5][0][1][1] = 174;
        this.bitRateAndFrameSizeTable[5][1][1][1] = 175;
        this.bitRateAndFrameSizeTable[6][0][1][0] = 96;
        this.bitRateAndFrameSizeTable[6][1][1][0] = 96;
        this.bitRateAndFrameSizeTable[6][0][1][1] = 208;
        this.bitRateAndFrameSizeTable[6][1][1][1] = 209;
        this.bitRateAndFrameSizeTable[7][0][1][0] = 112;
        this.bitRateAndFrameSizeTable[7][1][1][0] = 112;
        this.bitRateAndFrameSizeTable[7][0][1][1] = 243;
        this.bitRateAndFrameSizeTable[7][1][1][1] = 244;
        this.bitRateAndFrameSizeTable[8][0][1][0] = 128;
        this.bitRateAndFrameSizeTable[8][1][1][0] = 128;
        this.bitRateAndFrameSizeTable[8][0][1][1] = 278;
        this.bitRateAndFrameSizeTable[8][1][1][1] = 279;
        this.bitRateAndFrameSizeTable[9][0][1][0] = 160;
        this.bitRateAndFrameSizeTable[9][1][1][0] = 160;
        this.bitRateAndFrameSizeTable[9][0][1][1] = 348;
        this.bitRateAndFrameSizeTable[9][1][1][1] = 349;
        this.bitRateAndFrameSizeTable[10][0][1][0] = 192;
        this.bitRateAndFrameSizeTable[10][1][1][0] = 192;
        this.bitRateAndFrameSizeTable[10][0][1][1] = 417;
        this.bitRateAndFrameSizeTable[10][1][1][1] = 418;
        this.bitRateAndFrameSizeTable[11][0][1][0] = 224;
        this.bitRateAndFrameSizeTable[11][1][1][0] = 224;
        this.bitRateAndFrameSizeTable[11][0][1][1] = 487;
        this.bitRateAndFrameSizeTable[11][1][1][1] = 488;
        this.bitRateAndFrameSizeTable[12][0][1][0] = 256;
        this.bitRateAndFrameSizeTable[12][1][1][0] = 256;
        this.bitRateAndFrameSizeTable[12][0][1][1] = 557;
        this.bitRateAndFrameSizeTable[12][1][1][1] = 558;
        this.bitRateAndFrameSizeTable[13][0][1][0] = 320;
        this.bitRateAndFrameSizeTable[13][1][1][0] = 320;
        this.bitRateAndFrameSizeTable[13][0][1][1] = 696;
        this.bitRateAndFrameSizeTable[13][1][1][1] = 697;
        this.bitRateAndFrameSizeTable[14][0][1][0] = 384;
        this.bitRateAndFrameSizeTable[14][1][1][0] = 384;
        this.bitRateAndFrameSizeTable[14][0][1][1] = 835;
        this.bitRateAndFrameSizeTable[14][1][1][1] = 836;
        this.bitRateAndFrameSizeTable[15][0][1][0] = 448;
        this.bitRateAndFrameSizeTable[15][1][1][0] = 448;
        this.bitRateAndFrameSizeTable[15][0][1][1] = 975;
        this.bitRateAndFrameSizeTable[15][1][1][1] = 975;
        this.bitRateAndFrameSizeTable[16][0][1][0] = 512;
        this.bitRateAndFrameSizeTable[16][1][1][0] = 512;
        this.bitRateAndFrameSizeTable[16][0][1][1] = 1114;
        this.bitRateAndFrameSizeTable[16][1][1][1] = 1115;
        this.bitRateAndFrameSizeTable[17][0][1][0] = 576;
        this.bitRateAndFrameSizeTable[17][1][1][0] = 576;
        this.bitRateAndFrameSizeTable[17][0][1][1] = 1253;
        this.bitRateAndFrameSizeTable[17][1][1][1] = 1254;
        this.bitRateAndFrameSizeTable[18][0][1][0] = 640;
        this.bitRateAndFrameSizeTable[18][1][1][0] = 640;
        this.bitRateAndFrameSizeTable[18][0][1][1] = 1393;
        this.bitRateAndFrameSizeTable[18][1][1][1] = 1394;
        this.bitRateAndFrameSizeTable[0][0][2][0] = 32;
        this.bitRateAndFrameSizeTable[0][1][2][0] = 32;
        this.bitRateAndFrameSizeTable[0][0][2][1] = 96;
        this.bitRateAndFrameSizeTable[0][1][2][1] = 96;
        this.bitRateAndFrameSizeTable[1][0][2][0] = 40;
        this.bitRateAndFrameSizeTable[1][1][2][0] = 40;
        this.bitRateAndFrameSizeTable[1][0][2][1] = 120;
        this.bitRateAndFrameSizeTable[1][1][2][1] = 120;
        this.bitRateAndFrameSizeTable[2][0][2][0] = 48;
        this.bitRateAndFrameSizeTable[2][1][2][0] = 48;
        this.bitRateAndFrameSizeTable[2][0][2][1] = 144;
        this.bitRateAndFrameSizeTable[2][1][2][1] = 144;
        this.bitRateAndFrameSizeTable[3][0][2][0] = 56;
        this.bitRateAndFrameSizeTable[3][1][2][0] = 56;
        this.bitRateAndFrameSizeTable[3][0][2][1] = 168;
        this.bitRateAndFrameSizeTable[3][1][2][1] = 168;
        this.bitRateAndFrameSizeTable[4][0][2][0] = 64;
        this.bitRateAndFrameSizeTable[4][1][2][0] = 64;
        this.bitRateAndFrameSizeTable[4][0][2][1] = 192;
        this.bitRateAndFrameSizeTable[4][1][2][1] = 192;
        this.bitRateAndFrameSizeTable[5][0][2][0] = 80;
        this.bitRateAndFrameSizeTable[5][1][2][0] = 80;
        this.bitRateAndFrameSizeTable[5][0][2][1] = 240;
        this.bitRateAndFrameSizeTable[5][1][2][1] = 240;
        this.bitRateAndFrameSizeTable[6][0][2][0] = 96;
        this.bitRateAndFrameSizeTable[6][1][2][0] = 96;
        this.bitRateAndFrameSizeTable[6][0][2][1] = 288;
        this.bitRateAndFrameSizeTable[6][1][2][1] = 288;
        this.bitRateAndFrameSizeTable[7][0][2][0] = 112;
        this.bitRateAndFrameSizeTable[7][1][2][0] = 112;
        this.bitRateAndFrameSizeTable[7][0][2][1] = 336;
        this.bitRateAndFrameSizeTable[7][1][2][1] = 336;
        this.bitRateAndFrameSizeTable[8][0][2][0] = 128;
        this.bitRateAndFrameSizeTable[8][1][2][0] = 128;
        this.bitRateAndFrameSizeTable[8][0][2][1] = 384;
        this.bitRateAndFrameSizeTable[8][1][2][1] = 384;
        this.bitRateAndFrameSizeTable[9][0][2][0] = 160;
        this.bitRateAndFrameSizeTable[9][1][2][0] = 160;
        this.bitRateAndFrameSizeTable[9][0][2][1] = 480;
        this.bitRateAndFrameSizeTable[9][1][2][1] = 480;
        this.bitRateAndFrameSizeTable[10][0][2][0] = 192;
        this.bitRateAndFrameSizeTable[10][1][2][0] = 192;
        this.bitRateAndFrameSizeTable[10][0][2][1] = 576;
        this.bitRateAndFrameSizeTable[10][1][2][1] = 576;
        this.bitRateAndFrameSizeTable[11][0][2][0] = 224;
        this.bitRateAndFrameSizeTable[11][1][2][0] = 224;
        this.bitRateAndFrameSizeTable[11][0][2][1] = 672;
        this.bitRateAndFrameSizeTable[11][1][2][1] = 672;
        this.bitRateAndFrameSizeTable[12][0][2][0] = 256;
        this.bitRateAndFrameSizeTable[12][1][2][0] = 256;
        this.bitRateAndFrameSizeTable[12][0][2][1] = 768;
        this.bitRateAndFrameSizeTable[12][1][2][1] = 768;
        this.bitRateAndFrameSizeTable[13][0][2][0] = 320;
        this.bitRateAndFrameSizeTable[13][1][2][0] = 320;
        this.bitRateAndFrameSizeTable[13][0][2][1] = 960;
        this.bitRateAndFrameSizeTable[13][1][2][1] = 960;
        this.bitRateAndFrameSizeTable[14][0][2][0] = 384;
        this.bitRateAndFrameSizeTable[14][1][2][0] = 384;
        this.bitRateAndFrameSizeTable[14][0][2][1] = 1152;
        this.bitRateAndFrameSizeTable[14][1][2][1] = 1152;
        this.bitRateAndFrameSizeTable[15][0][2][0] = 448;
        this.bitRateAndFrameSizeTable[15][1][2][0] = 448;
        this.bitRateAndFrameSizeTable[15][0][2][1] = 1344;
        this.bitRateAndFrameSizeTable[15][1][2][1] = 1344;
        this.bitRateAndFrameSizeTable[16][0][2][0] = 512;
        this.bitRateAndFrameSizeTable[16][1][2][0] = 512;
        this.bitRateAndFrameSizeTable[16][0][2][1] = 1536;
        this.bitRateAndFrameSizeTable[16][1][2][1] = 1536;
        this.bitRateAndFrameSizeTable[17][0][2][0] = 576;
        this.bitRateAndFrameSizeTable[17][1][2][0] = 576;
        this.bitRateAndFrameSizeTable[17][0][2][1] = 1728;
        this.bitRateAndFrameSizeTable[17][1][2][1] = 1728;
        this.bitRateAndFrameSizeTable[18][0][2][0] = 640;
        this.bitRateAndFrameSizeTable[18][1][2][0] = 640;
        this.bitRateAndFrameSizeTable[18][0][2][1] = 1920;
        this.bitRateAndFrameSizeTable[18][1][2][1] = 1920;
    }
}
