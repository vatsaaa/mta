// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.tracks;

import com.coremedia.iso.boxes.AbstractMediaHeaderBox;
import com.googlecode.mp4parser.authoring.TrackMetaData;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import java.util.Iterator;
import java.util.LinkedList;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import java.util.Collection;
import java.util.ArrayList;
import java.nio.ByteBuffer;
import java.util.List;
import java.io.IOException;
import java.util.Arrays;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.io.ByteArrayOutputStream;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.AbstractTrack;

public class AppendTrack extends AbstractTrack
{
    Track[] tracks;
    
    public AppendTrack(final Track... tracks) throws IOException {
        this.tracks = tracks;
        byte[] referenceSampleDescriptionBox = null;
        for (final Track track : tracks) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            track.getSampleDescriptionBox().getBox(Channels.newChannel(baos));
            if (referenceSampleDescriptionBox == null) {
                referenceSampleDescriptionBox = baos.toByteArray();
            }
            else if (!Arrays.equals(referenceSampleDescriptionBox, baos.toByteArray())) {
                throw new IOException("Cannot append " + track + " to " + tracks[0] + " since their Sample Description Boxes differ");
            }
        }
    }
    
    public List<ByteBuffer> getSamples() {
        final ArrayList<ByteBuffer> lists = new ArrayList<ByteBuffer>();
        Track[] tracks;
        for (int length = (tracks = this.tracks).length, i = 0; i < length; ++i) {
            final Track track = tracks[i];
            lists.addAll(track.getSamples());
        }
        return lists;
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.tracks[0].getSampleDescriptionBox();
    }
    
    public List<TimeToSampleBox.Entry> getDecodingTimeEntries() {
        if (this.tracks[0].getDecodingTimeEntries() != null && !this.tracks[0].getDecodingTimeEntries().isEmpty()) {
            final List<long[]> lists = new LinkedList<long[]>();
            Track[] tracks;
            for (int length = (tracks = this.tracks).length, i = 0; i < length; ++i) {
                final Track track = tracks[i];
                lists.add(TimeToSampleBox.blowupTimeToSamples(track.getDecodingTimeEntries()));
            }
            final LinkedList<TimeToSampleBox.Entry> returnDecodingEntries = new LinkedList<TimeToSampleBox.Entry>();
            for (final long[] list : lists) {
                long[] array;
                for (int length2 = (array = list).length, j = 0; j < length2; ++j) {
                    final long nuDecodingTime = array[j];
                    if (returnDecodingEntries.isEmpty() || returnDecodingEntries.getLast().getDelta() != nuDecodingTime) {
                        final TimeToSampleBox.Entry e = new TimeToSampleBox.Entry(1L, nuDecodingTime);
                        returnDecodingEntries.add(e);
                    }
                    else {
                        final TimeToSampleBox.Entry e = returnDecodingEntries.getLast();
                        e.setCount(e.getCount() + 1L);
                    }
                }
            }
            return returnDecodingEntries;
        }
        return null;
    }
    
    public List<CompositionTimeToSample.Entry> getCompositionTimeEntries() {
        if (this.tracks[0].getCompositionTimeEntries() != null && !this.tracks[0].getCompositionTimeEntries().isEmpty()) {
            final List<int[]> lists = new LinkedList<int[]>();
            Track[] tracks;
            for (int length = (tracks = this.tracks).length, i = 0; i < length; ++i) {
                final Track track = tracks[i];
                lists.add(CompositionTimeToSample.blowupCompositionTimes(track.getCompositionTimeEntries()));
            }
            final LinkedList<CompositionTimeToSample.Entry> compositionTimeEntries = new LinkedList<CompositionTimeToSample.Entry>();
            for (final int[] list : lists) {
                int[] array;
                for (int length2 = (array = list).length, j = 0; j < length2; ++j) {
                    final int compositionTime = array[j];
                    if (compositionTimeEntries.isEmpty() || compositionTimeEntries.getLast().getOffset() != compositionTime) {
                        final CompositionTimeToSample.Entry e = new CompositionTimeToSample.Entry(1, compositionTime);
                        compositionTimeEntries.add(e);
                    }
                    else {
                        final CompositionTimeToSample.Entry e = compositionTimeEntries.getLast();
                        e.setCount(e.getCount() + 1);
                    }
                }
            }
            return compositionTimeEntries;
        }
        return null;
    }
    
    public long[] getSyncSamples() {
        if (this.tracks[0].getSyncSamples() != null && this.tracks[0].getSyncSamples().length > 0) {
            int numSyncSamples = 0;
            Track[] tracks;
            for (int length = (tracks = this.tracks).length, i = 0; i < length; ++i) {
                final Track track = tracks[i];
                numSyncSamples += track.getSyncSamples().length;
            }
            final long[] returnSyncSamples = new long[numSyncSamples];
            int pos = 0;
            long samplesBefore = 0L;
            Track[] tracks2;
            for (int length2 = (tracks2 = this.tracks).length, j = 0; j < length2; ++j) {
                final Track track2 = tracks2[j];
                long[] syncSamples;
                for (int length3 = (syncSamples = track2.getSyncSamples()).length, k = 0; k < length3; ++k) {
                    final long l = syncSamples[k];
                    returnSyncSamples[pos++] = samplesBefore + l;
                }
                samplesBefore += track2.getSamples().size();
            }
            return returnSyncSamples;
        }
        return null;
    }
    
    public List<SampleDependencyTypeBox.Entry> getSampleDependencies() {
        if (this.tracks[0].getSampleDependencies() != null && !this.tracks[0].getSampleDependencies().isEmpty()) {
            final List<SampleDependencyTypeBox.Entry> list = new LinkedList<SampleDependencyTypeBox.Entry>();
            Track[] tracks;
            for (int length = (tracks = this.tracks).length, i = 0; i < length; ++i) {
                final Track track = tracks[i];
                list.addAll(track.getSampleDependencies());
            }
            return list;
        }
        return null;
    }
    
    public TrackMetaData getTrackMetaData() {
        return this.tracks[0].getTrackMetaData();
    }
    
    public String getHandler() {
        return this.tracks[0].getHandler();
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        return this.tracks[0].getMediaHeaderBox();
    }
}
