// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder;

import java.io.IOException;
import com.coremedia.iso.IsoFile;
import com.googlecode.mp4parser.authoring.Movie;

public interface Mp4Builder
{
    IsoFile build(final Movie p0) throws IOException;
}
