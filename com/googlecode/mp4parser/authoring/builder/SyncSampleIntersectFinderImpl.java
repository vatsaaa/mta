// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder;

import java.util.Iterator;
import java.util.Arrays;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;

public class SyncSampleIntersectFinderImpl implements FragmentIntersectionFinder
{
    public int[] sampleNumbers(final Track track, final Movie movie) {
        Track syncSampleContainingTrack = null;
        int syncSampleContainingTrackSampleCount = 0;
        long[] syncSamples = null;
        for (final Track currentTrack : movie.getTracks()) {
            final long[] currentTrackSyncSamples = currentTrack.getSyncSamples();
            if (currentTrackSyncSamples != null && currentTrackSyncSamples.length > 0) {
                if (syncSampleContainingTrack != null && !Arrays.equals(syncSamples, currentTrackSyncSamples)) {
                    throw new RuntimeException("There is more than one track containing a Sync Sample Box but the algorithm cannot deal with it. What is the most important track?");
                }
                syncSampleContainingTrack = currentTrack;
                syncSampleContainingTrackSampleCount = currentTrack.getSamples().size();
                syncSamples = currentTrackSyncSamples;
            }
        }
        if (syncSampleContainingTrack == null) {
            throw new RuntimeException("There was no track containing a Sync Sample Box but the Sync Sample Box is required to determine the fragment size.");
        }
        final int[] chunkSizes = new int[syncSamples.length];
        final long sc = track.getSamples().size();
        final double stretch = sc / (double)syncSampleContainingTrackSampleCount;
        for (int i = 0; i < chunkSizes.length; ++i) {
            final int start = (int)Math.round(stretch * (syncSamples[i] - 1L));
            chunkSizes[i] = start;
        }
        return chunkSizes;
    }
}
