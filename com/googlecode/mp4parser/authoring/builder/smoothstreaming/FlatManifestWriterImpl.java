// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder.smoothstreaming;

import com.coremedia.iso.boxes.CastUtils;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.ContainerBox;
import com.coremedia.iso.IsoFileConvenienceHelper;
import com.coremedia.iso.boxes.OriginalFormatBox;
import java.util.List;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import com.coremedia.iso.boxes.h264.AvcConfigurationBox;
import com.coremedia.iso.Hex;
import java.nio.ByteBuffer;
import com.googlecode.mp4parser.boxes.mp4.ESDescriptorBox;
import com.coremedia.iso.boxes.sampleentry.SampleEntry;
import java.io.IOException;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import java.util.Iterator;
import nu.xom.Document;
import nu.xom.Node;
import nu.xom.Comment;
import nu.xom.Attribute;
import nu.xom.Element;
import com.coremedia.iso.boxes.sampleentry.AudioSampleEntry;
import com.coremedia.iso.boxes.SoundMediaHeaderBox;
import com.coremedia.iso.boxes.sampleentry.VisualSampleEntry;
import com.coremedia.iso.boxes.VideoMediaHeaderBox;
import com.googlecode.mp4parser.authoring.Track;
import java.util.LinkedList;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.builder.SyncSampleIntersectFinderImpl;
import com.googlecode.mp4parser.authoring.builder.FragmentIntersectionFinder;

public class FlatManifestWriterImpl implements ManifestWriter
{
    private FragmentIntersectionFinder intersectionFinder;
    private long[] audioFragmentsDurations;
    private long[] videoFragmentsDurations;
    
    public FlatManifestWriterImpl() {
        this.intersectionFinder = new SyncSampleIntersectFinderImpl();
    }
    
    public void setIntersectionFinder(final FragmentIntersectionFinder intersectionFinder) {
        this.intersectionFinder = intersectionFinder;
    }
    
    public String getManifest(final Movie movie) throws IOException {
        long duration = 0L;
        final LinkedList<VideoQuality> videoQualities = new LinkedList<VideoQuality>();
        final LinkedList<AudioQuality> audioQualities = new LinkedList<AudioQuality>();
        for (final Track track : movie.getTracks()) {
            final long tracksDuration = getDuration(track) * movie.getTimescale() / track.getTrackMetaData().getTimescale();
            if (tracksDuration > duration) {
                duration = tracksDuration;
            }
        }
        for (final Track track : movie.getTracks()) {
            if (track.getMediaHeaderBox() instanceof VideoMediaHeaderBox) {
                this.videoFragmentsDurations = this.checkFragmentsAlign(this.videoFragmentsDurations, this.calculateFragmentDurations(track, movie));
                final SampleDescriptionBox stsd = track.getSampleDescriptionBox();
                videoQualities.add(this.getVideoQuality(track, (VisualSampleEntry)stsd.getSampleEntry()));
            }
            if (track.getMediaHeaderBox() instanceof SoundMediaHeaderBox) {
                this.audioFragmentsDurations = this.checkFragmentsAlign(this.audioFragmentsDurations, this.calculateFragmentDurations(track, movie));
                final SampleDescriptionBox stsd = track.getSampleDescriptionBox();
                audioQualities.add(this.getAudioQuality(track, (AudioSampleEntry)stsd.getSampleEntry()));
            }
        }
        final Element smoothStreamingMedia = new Element("SmoothStreamingMedia");
        smoothStreamingMedia.addAttribute(new Attribute("MajorVersion", "2"));
        smoothStreamingMedia.addAttribute(new Attribute("MinorVersion", "1"));
        smoothStreamingMedia.addAttribute(new Attribute("Duration", Long.toString(duration)));
        smoothStreamingMedia.appendChild((Node)new Comment("Smooth Streaming develop by castLabs software"));
        final Element videoStreamIndex = new Element("StreamIndex");
        videoStreamIndex.addAttribute(new Attribute("Type", "video"));
        videoStreamIndex.addAttribute(new Attribute("Chunks", Integer.toString(this.videoFragmentsDurations.length)));
        videoStreamIndex.addAttribute(new Attribute("Url", "video/{bitrate}/{start time}"));
        videoStreamIndex.addAttribute(new Attribute("QualityLevels", Integer.toString(videoQualities.size())));
        smoothStreamingMedia.appendChild((Node)videoStreamIndex);
        for (int i = 0; i < videoQualities.size(); ++i) {
            final VideoQuality vq = videoQualities.get(i);
            final Element qualityLevel = new Element("QualityLevel");
            qualityLevel.addAttribute(new Attribute("Index", Integer.toString(i)));
            qualityLevel.addAttribute(new Attribute("Bitrate", Long.toString(vq.bitrate)));
            qualityLevel.addAttribute(new Attribute("FourCC", vq.fourCC));
            qualityLevel.addAttribute(new Attribute("MaxWidth", Long.toString(vq.width)));
            qualityLevel.addAttribute(new Attribute("MaxHeight", Long.toString(vq.height)));
            qualityLevel.addAttribute(new Attribute("CodecPrivateData", vq.codecPrivateData));
            qualityLevel.addAttribute(new Attribute("NALUnitLengthField", Integer.toString(vq.nalLength)));
            videoStreamIndex.appendChild((Node)qualityLevel);
        }
        for (int i = 0; i < this.videoFragmentsDurations.length; ++i) {
            final Element c = new Element("c");
            c.addAttribute(new Attribute("n", Integer.toString(i)));
            c.addAttribute(new Attribute("d", Long.toString(this.videoFragmentsDurations[i])));
            videoStreamIndex.appendChild((Node)c);
        }
        final Element audioStreamIndex = new Element("StreamIndex");
        audioStreamIndex.addAttribute(new Attribute("Type", "audio"));
        audioStreamIndex.addAttribute(new Attribute("Chunks", Integer.toString(this.audioFragmentsDurations.length)));
        audioStreamIndex.addAttribute(new Attribute("Url", "audio/{bitrate}/{start time}"));
        audioStreamIndex.addAttribute(new Attribute("QualityLevels", Integer.toString(audioQualities.size())));
        for (int j = 0; j < audioQualities.size(); ++j) {
            final AudioQuality aq = audioQualities.get(j);
            final Element qualityLevel2 = new Element("QualityLevel");
            qualityLevel2.addAttribute(new Attribute("Index", Integer.toString(j)));
            qualityLevel2.addAttribute(new Attribute("Bitrate", Long.toString(aq.bitrate)));
            qualityLevel2.addAttribute(new Attribute("AudioTag", Integer.toString(aq.audioTag)));
            qualityLevel2.addAttribute(new Attribute("SamplingRate", Long.toString(aq.samplingRate)));
            qualityLevel2.addAttribute(new Attribute("Channels", Integer.toString(aq.channels)));
            qualityLevel2.addAttribute(new Attribute("BitsPerSample", Integer.toString(aq.bitPerSample)));
            qualityLevel2.addAttribute(new Attribute("PacketSize", Integer.toString(aq.packetSize)));
            qualityLevel2.addAttribute(new Attribute("CodecPrivateData", aq.codecPrivateData));
            audioStreamIndex.appendChild((Node)qualityLevel2);
        }
        for (int j = 0; j < this.audioFragmentsDurations.length; ++j) {
            final Element c2 = new Element("c");
            c2.addAttribute(new Attribute("n", Integer.toString(j)));
            c2.addAttribute(new Attribute("d", Long.toString(this.audioFragmentsDurations[j])));
            audioStreamIndex.appendChild((Node)c2);
        }
        return new Document(smoothStreamingMedia).toXML();
    }
    
    private AudioQuality getAudioQuality(final Track track, final AudioSampleEntry ase) {
        if (this.getFormat(ase).equals("mp4a")) {
            final AudioQuality l = new AudioQuality();
            l.bitrate = this.getBitrate(track);
            l.audioTag = 255;
            l.samplingRate = ase.getSampleRate();
            l.channels = ase.getChannelCount();
            l.bitPerSample = ase.getSampleSize();
            l.packetSize = 4;
            l.codecPrivateData = this.getAudioCodecPrivateData(ase.getBoxes(ESDescriptorBox.class).get(0));
            return l;
        }
        throw new InternalError("I don't know what to do with audio of type " + this.getFormat(ase));
    }
    
    public long getBitrate(final Track track) {
        long bitrate = 0L;
        for (final ByteBuffer sample : track.getSamples()) {
            bitrate += sample.limit();
        }
        bitrate *= 8L;
        bitrate /= getDuration(track) / track.getTrackMetaData().getTimescale();
        return bitrate;
    }
    
    private String getAudioCodecPrivateData(final ESDescriptorBox esDescriptorBox) {
        final ByteBuffer configBytes = esDescriptorBox.getEsDescriptor().getDecoderConfigDescriptor().getAudioSpecificInfo().getConfigBytes();
        final byte[] configByteArray = new byte[configBytes.limit()];
        configBytes.rewind();
        configBytes.get(configByteArray);
        return Hex.encodeHex(configByteArray);
    }
    
    private VideoQuality getVideoQuality(final Track track, final VisualSampleEntry vse) {
        if ("avc1".equals(this.getFormat(vse))) {
            final AvcConfigurationBox avcConfigurationBox = vse.getBoxes(AvcConfigurationBox.class).get(0);
            final VideoQuality l = new VideoQuality();
            l.bitrate = this.getBitrate(track);
            l.codecPrivateData = Hex.encodeHex(this.getAvcCodecPrivateData(avcConfigurationBox));
            l.fourCC = "AVC1";
            l.width = vse.getWidth();
            l.height = vse.getHeight();
            l.nalLength = avcConfigurationBox.getLengthSizeMinusOne() + 1;
            return l;
        }
        throw new InternalError("I don't know how to handle video of type " + this.getFormat(vse));
    }
    
    private long[] checkFragmentsAlign(final long[] referenceTimes, final long[] checkTime) throws IOException {
        if (referenceTimes == null || referenceTimes.length == 0) {
            return checkTime;
        }
        if (!Arrays.equals(checkTime, referenceTimes)) {
            System.err.print("Reference     :  [");
            for (final long l : checkTime) {
                System.err.print(String.valueOf(l) + ",");
            }
            System.err.println("]");
            System.err.print("Current       :  [");
            for (final long l : referenceTimes) {
                System.err.print(String.valueOf(l) + ",");
            }
            System.err.println("]");
            throw new IOException("Track does not have the same video fragment borders as its predecessor.");
        }
        return checkTime;
    }
    
    private byte[] getAvcCodecPrivateData(final AvcConfigurationBox avcConfigurationBox) {
        final List<byte[]> sps = avcConfigurationBox.getSequenceParameterSets();
        final List<byte[]> pps = avcConfigurationBox.getPictureParameterSets();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(new byte[] { 0, 0, 0, 1 });
            for (final byte[] sp : sps) {
                baos.write(sp);
            }
            baos.write(new byte[] { 0, 0, 0, 1 });
            for (final byte[] pp : pps) {
                baos.write(pp);
            }
        }
        catch (IOException ex) {
            throw new InternalError("ByteArrayOutputStream do not throw IOException ?!?!?");
        }
        return baos.toByteArray();
    }
    
    private String getFormat(final SampleEntry se) {
        String type = se.getType();
        if (type.equals("encv") || type.equals("enca") || type.equals("encv")) {
            final OriginalFormatBox frma = (OriginalFormatBox)IsoFileConvenienceHelper.get(se, "sinf/frma");
            type = frma.getDataFormat();
        }
        return type;
    }
    
    public long[] calculateFragmentDurations(final Track track, final Movie movie) {
        final int[] startSamples = this.intersectionFinder.sampleNumbers(track, movie);
        final long[] durations = new long[startSamples.length];
        int currentFragment = -1;
        int currentSample = 0;
        for (final TimeToSampleBox.Entry entry : track.getDecodingTimeEntries()) {
            for (int max = currentSample + CastUtils.l2i(entry.getCount()); currentSample < max; ++currentSample) {
                if (currentFragment != startSamples.length - 1 && currentSample == startSamples[currentFragment + 1]) {
                    ++currentFragment;
                }
                final long[] array = durations;
                final int n = currentFragment;
                array[n] += entry.getDelta();
            }
        }
        return durations;
    }
    
    protected static long getDuration(final Track track) {
        long duration = 0L;
        for (final TimeToSampleBox.Entry entry : track.getDecodingTimeEntries()) {
            duration += entry.getCount() * entry.getDelta();
        }
        return duration;
    }
}
