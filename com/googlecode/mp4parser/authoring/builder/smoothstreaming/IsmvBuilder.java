// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder.smoothstreaming;

import java.io.IOException;
import java.util.Iterator;
import com.googlecode.mp4parser.authoring.tracks.ChangeTimeScaleTrack;
import com.googlecode.mp4parser.authoring.Track;
import com.coremedia.iso.IsoFile;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.builder.FragmentedMp4Builder;

public class IsmvBuilder extends FragmentedMp4Builder
{
    long timeScale;
    
    public IsmvBuilder() {
        this.timeScale = 10000000L;
    }
    
    @Override
    public IsoFile build(final Movie movie) throws IOException {
        final Movie nuMovie = new Movie();
        movie.setMovieMetaData(movie.getMovieMetaData());
        for (final Track track : movie.getTracks()) {
            nuMovie.addTrack(new ChangeTimeScaleTrack(track, this.timeScale));
        }
        return super.build(nuMovie);
    }
}
