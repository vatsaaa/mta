// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder.smoothstreaming;

import com.googlecode.mp4parser.authoring.Track;
import java.io.IOException;
import com.googlecode.mp4parser.authoring.Movie;

public interface ManifestWriter
{
    String getManifest(final Movie p0) throws IOException;
    
    long getBitrate(final Track p0);
    
    long[] calculateFragmentDurations(final Track p0, final Movie p1);
}
