// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder.smoothstreaming;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import com.coremedia.iso.IsoFile;
import java.io.FileWriter;
import java.nio.channels.WritableByteChannel;
import java.io.FileOutputStream;
import com.coremedia.iso.boxes.fragment.MovieFragmentBox;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.VideoMediaHeaderBox;
import com.coremedia.iso.boxes.SoundMediaHeaderBox;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.builder.SyncSampleIntersectFinderImpl;
import com.googlecode.mp4parser.authoring.builder.FragmentIntersectionFinder;
import java.io.File;

public class FlatPackageWriterImpl implements PackageWriter
{
    private File outputDirectory;
    FragmentIntersectionFinder intersectionFinder;
    
    public FlatPackageWriterImpl() {
        this.intersectionFinder = new SyncSampleIntersectFinderImpl();
    }
    
    public void setOutputDirectory(final File outputDirectory) {
        assert outputDirectory.isDirectory();
        this.outputDirectory = outputDirectory;
    }
    
    public void write(final Movie qualities) throws IOException {
        final IsmvBuilder ismvBuilder = new IsmvBuilder();
        final ManifestWriter manifestWriter = new FlatManifestWriterImpl();
        ismvBuilder.setIntersectionFinder(this.intersectionFinder);
        final IsoFile isoFile = ismvBuilder.build(qualities);
        for (final Track track : qualities.getTracks()) {
            final String bitrate = Long.toString(manifestWriter.getBitrate(track));
            final long trackId = track.getTrackMetaData().getTrackId();
            final Iterator<Box> boxIt = isoFile.getBoxes().iterator();
            File mediaOutDir;
            if (track.getMediaHeaderBox() instanceof SoundMediaHeaderBox) {
                mediaOutDir = new File(this.outputDirectory, "audio");
            }
            else {
                if (!(track.getMediaHeaderBox() instanceof VideoMediaHeaderBox)) {
                    System.err.println("Skipping Track with handler " + track.getHandler() + " and " + track.getMediaHeaderBox().getClass().getSimpleName());
                    continue;
                }
                mediaOutDir = new File(this.outputDirectory, "video");
            }
            final File bitrateOutputDir = new File(mediaOutDir, bitrate);
            bitrateOutputDir.mkdirs();
            final long[] fragmentTimes = manifestWriter.calculateFragmentDurations(track, qualities);
            long startTime = 0L;
            int currentFragment = 0;
            while (boxIt.hasNext()) {
                final Box b = boxIt.next();
                if (b instanceof MovieFragmentBox) {
                    assert ((MovieFragmentBox)b).getTrackCount() == 1;
                    if (((MovieFragmentBox)b).getTrackNumbers()[0] != trackId) {
                        continue;
                    }
                    final FileOutputStream fos = new FileOutputStream(new File(bitrateOutputDir, Long.toString(startTime)));
                    startTime += fragmentTimes[currentFragment++];
                    final FileChannel fc = fos.getChannel();
                    final Box mdat = boxIt.next();
                    assert mdat.getType().equals("mdat");
                    b.getBox(fc);
                    mdat.getBox(fc);
                    fc.truncate(fc.position());
                    fc.close();
                }
            }
        }
        final FileWriter fw = new FileWriter(new File(this.outputDirectory, "Manifest"));
        fw.write(manifestWriter.getManifest(qualities));
        fw.close();
    }
}
