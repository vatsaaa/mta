// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder.smoothstreaming;

class VideoQuality
{
    long bitrate;
    String fourCC;
    int width;
    int height;
    String codecPrivateData;
    int nalLength;
}
