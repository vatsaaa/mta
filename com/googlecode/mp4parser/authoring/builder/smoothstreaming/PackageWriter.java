// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder.smoothstreaming;

import java.io.IOException;
import com.googlecode.mp4parser.authoring.Movie;

public interface PackageWriter
{
    void write(final Movie p0) throws IOException;
}
