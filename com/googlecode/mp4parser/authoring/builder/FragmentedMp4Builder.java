// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder;

import com.coremedia.iso.boxes.DataEntryUrlBox;
import com.coremedia.iso.boxes.DataReferenceBox;
import com.coremedia.iso.boxes.DataInformationBox;
import com.coremedia.iso.boxes.TrackBox;
import com.coremedia.iso.boxes.MediaBox;
import com.coremedia.iso.boxes.HandlerBox;
import com.coremedia.iso.boxes.MediaInformationBox;
import com.coremedia.iso.boxes.StaticChunkOffsetBox;
import com.coremedia.iso.boxes.SampleTableBox;
import com.coremedia.iso.boxes.MediaHeaderBox;
import com.coremedia.iso.boxes.TrackHeaderBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentRandomAccessOffsetBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentRandomAccessBox;
import com.coremedia.iso.boxes.fragment.TrackExtendsBox;
import com.coremedia.iso.boxes.fragment.MovieExtendsBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox;
import com.coremedia.iso.boxes.MovieBox;
import com.googlecode.mp4parser.authoring.DateHelper;
import java.util.Date;
import com.coremedia.iso.boxes.MovieHeaderBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentBox;
import java.util.Queue;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.TimeToSampleBox;
import java.util.ArrayList;
import com.coremedia.iso.boxes.fragment.TrackRunBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentHeaderBox;
import com.coremedia.iso.boxes.fragment.SampleFlags;
import com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox;
import com.coremedia.iso.Hex;
import com.coremedia.iso.BoxParser;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.GatheringByteChannel;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.boxes.CastUtils;
import java.nio.channels.WritableByteChannel;
import com.coremedia.iso.boxes.ContainerBox;
import java.io.IOException;
import com.coremedia.iso.IsoFile;
import java.util.Collections;
import java.util.Iterator;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.Collection;
import com.googlecode.mp4parser.authoring.Track;
import com.coremedia.iso.boxes.FileTypeBox;
import java.util.LinkedList;
import com.coremedia.iso.boxes.Box;
import com.googlecode.mp4parser.authoring.Movie;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class FragmentedMp4Builder implements Mp4Builder
{
    FragmentIntersectionFinder intersectionFinder;
    private static final Logger LOG;
    
    static {
        LOG = Logger.getLogger(FragmentedMp4Builder.class.getName());
    }
    
    public FragmentedMp4Builder() {
        this.intersectionFinder = new SyncSampleIntersectFinderImpl();
    }
    
    public List<String> getAllowedHandlers() {
        return Arrays.asList("soun", "vide");
    }
    
    public Box createFtyp(final Movie movie) {
        final List<String> minorBrands = new LinkedList<String>();
        minorBrands.add("isom");
        minorBrands.add("iso2");
        minorBrands.add("avc1");
        return new FileTypeBox("isom", 0L, minorBrands);
    }
    
    protected List<Box> createMoofMdat(final Movie movie) {
        final List<Box> boxes = new LinkedList<Box>();
        int maxNumberOfFragments = 0;
        for (final Track track : movie.getTracks()) {
            final int currentLength = this.intersectionFinder.sampleNumbers(track, movie).length;
            maxNumberOfFragments = ((currentLength > maxNumberOfFragments) ? currentLength : maxNumberOfFragments);
        }
        int sequence = 1;
        for (int i = 0; i < maxNumberOfFragments; ++i) {
            final List<Track> sizeSortedTracks = new LinkedList<Track>(movie.getTracks());
            final int j = i;
            Collections.sort(sizeSortedTracks, new Comparator<Track>() {
                public int compare(final Track o1, final Track o2) {
                    final int[] startSamples1 = FragmentedMp4Builder.this.intersectionFinder.sampleNumbers(o1, movie);
                    final int startSample1 = startSamples1[j];
                    final int endSample1 = (j + 1 < startSamples1.length) ? startSamples1[j + 1] : o1.getSamples().size();
                    final int[] startSamples2 = FragmentedMp4Builder.this.intersectionFinder.sampleNumbers(o2, movie);
                    final int startSample2 = startSamples2[j];
                    final int endSample2 = (j + 1 < startSamples2.length) ? startSamples2[j + 1] : o2.getSamples().size();
                    final List<ByteBuffer> samples1 = o1.getSamples().subList(startSample1, endSample1);
                    final List<ByteBuffer> samples2 = o2.getSamples().subList(startSample2, endSample2);
                    int size1 = 0;
                    for (final ByteBuffer byteBuffer : samples1) {
                        size1 += byteBuffer.limit();
                    }
                    int size2 = 0;
                    for (final ByteBuffer byteBuffer2 : samples2) {
                        size2 += byteBuffer2.limit();
                    }
                    return size1 - size2;
                }
            });
            for (final Track track2 : sizeSortedTracks) {
                if (this.getAllowedHandlers().isEmpty() || this.getAllowedHandlers().contains(track2.getHandler())) {
                    final int[] startSamples = this.intersectionFinder.sampleNumbers(track2, movie);
                    if (i >= startSamples.length) {
                        continue;
                    }
                    final int startSample = startSamples[i];
                    final int endSample = (i + 1 < startSamples.length) ? startSamples[i + 1] : track2.getSamples().size();
                    if (startSample == endSample) {
                        continue;
                    }
                    boxes.add(this.createMoof(startSample, endSample, track2, sequence));
                    boxes.add(this.createMdat(startSample, endSample, track2, sequence++));
                }
            }
        }
        return boxes;
    }
    
    public IsoFile build(final Movie movie) throws IOException {
        FragmentedMp4Builder.LOG.info("Creating movie " + movie);
        final IsoFile isoFile = new IsoFile();
        isoFile.addBox(this.createFtyp(movie));
        isoFile.addBox(this.createMoov(movie));
        for (final Box box : this.createMoofMdat(movie)) {
            isoFile.addBox(box);
        }
        isoFile.addBox(this.createMfra(movie, isoFile));
        return isoFile;
    }
    
    protected Box createMdat(final int startSample, final int endSample, final Track track, final int i) {
        final List<ByteBuffer> samples = ByteBufferHelper.mergeAdjacentBuffers(this.getSamples(startSample, endSample, track, i));
        return new Box() {
            ContainerBox parent;
            
            public ContainerBox getParent() {
                return this.parent;
            }
            
            public void setParent(final ContainerBox parent) {
                this.parent = parent;
            }
            
            public long getSize() {
                long size = 8L;
                for (final ByteBuffer sample : samples) {
                    size += sample.limit();
                }
                return size;
            }
            
            public String getType() {
                return "mdat";
            }
            
            public void getBox(final WritableByteChannel writableByteChannel) throws IOException {
                final ByteBuffer header = ByteBuffer.allocate(8);
                IsoTypeWriter.writeUInt32(header, CastUtils.l2i(this.getSize()));
                header.put(IsoFile.fourCCtoBytes(this.getType()));
                header.rewind();
                writableByteChannel.write(header);
                if (writableByteChannel instanceof GatheringByteChannel) {
                    for (int STEPSIZE = 1024, i = 0; i < Math.ceil(samples.size() / (double)STEPSIZE); ++i) {
                        final List<ByteBuffer> sublist = samples.subList(i * STEPSIZE, ((i + 1) * STEPSIZE < samples.size()) ? ((i + 1) * STEPSIZE) : samples.size());
                        final ByteBuffer[] sampleArray = sublist.toArray(new ByteBuffer[sublist.size()]);
                        do {
                            ((GatheringByteChannel)writableByteChannel).write(sampleArray);
                        } while (sampleArray[sampleArray.length - 1].remaining() > 0);
                    }
                }
                else {
                    for (final ByteBuffer sample : samples) {
                        sample.rewind();
                        writableByteChannel.write(sample);
                    }
                }
            }
            
            public void parse(final ReadableByteChannel inFC, final ByteBuffer header, final long contentSize, final BoxParser boxParser) throws IOException {
            }
        };
    }
    
    public static void dumpHex(final ByteBuffer bb) {
        final byte[] b = new byte[bb.limit()];
        bb.get(b);
        System.err.println(Hex.encodeHex(b));
        bb.rewind();
    }
    
    protected Box createTfhd(final int startSample, final int endSample, final Track track, final int sequenceNumber) {
        final TrackFragmentHeaderBox tfhd = new TrackFragmentHeaderBox();
        final SampleFlags sf = new SampleFlags();
        tfhd.setDefaultSampleFlags(sf);
        tfhd.setBaseDataOffset(-1L);
        tfhd.setTrackId(track.getTrackMetaData().getTrackId());
        return tfhd;
    }
    
    protected Box createMfhd(final int startSample, final int endSample, final Track track, final int sequenceNumber) {
        final MovieFragmentHeaderBox mfhd = new MovieFragmentHeaderBox();
        mfhd.setSequenceNumber(sequenceNumber);
        return mfhd;
    }
    
    protected Box createTraf(final int startSample, final int endSample, final Track track, final int sequenceNumber) {
        final TrackFragmentBox traf = new TrackFragmentBox();
        traf.addBox(this.createTfhd(startSample, endSample, track, sequenceNumber));
        for (final Box trun : this.createTruns(startSample, endSample, track, sequenceNumber)) {
            traf.addBox(trun);
        }
        return traf;
    }
    
    protected List<ByteBuffer> getSamples(final int startSample, final int endSample, final Track track, final int sequenceNumber) {
        return track.getSamples().subList(startSample, endSample);
    }
    
    protected List<? extends Box> createTruns(final int startSample, final int endSample, final Track track, final int sequenceNumber) {
        final List<ByteBuffer> samples = this.getSamples(startSample, endSample, track, sequenceNumber);
        final long[] sampleSizes = new long[samples.size()];
        for (int i = 0; i < sampleSizes.length; ++i) {
            sampleSizes[i] = samples.get(i).limit();
        }
        final TrackRunBox trun = new TrackRunBox();
        trun.setSampleDurationPresent(true);
        trun.setSampleSizePresent(true);
        final List<TrackRunBox.Entry> entries = new ArrayList<TrackRunBox.Entry>(endSample - startSample);
        final Queue<TimeToSampleBox.Entry> timeQueue = new LinkedList<TimeToSampleBox.Entry>((Collection<? extends TimeToSampleBox.Entry>)track.getDecodingTimeEntries());
        long durationEntriesLeft = timeQueue.peek().getCount();
        final Queue<CompositionTimeToSample.Entry> compositionTimeQueue = (track.getCompositionTimeEntries() != null && track.getCompositionTimeEntries().size() > 0) ? new LinkedList<CompositionTimeToSample.Entry>((Collection<? extends CompositionTimeToSample.Entry>)track.getCompositionTimeEntries()) : null;
        long compositionTimeEntriesLeft = (compositionTimeQueue != null) ? compositionTimeQueue.peek().getCount() : -1;
        trun.setSampleCompositionTimeOffsetPresent(compositionTimeEntriesLeft > 0L);
        final boolean sampleFlagsRequired = (track.getSampleDependencies() != null && !track.getSampleDependencies().isEmpty()) || (track.getSyncSamples() != null && track.getSyncSamples().length != 0);
        trun.setSampleFlagsPresent(sampleFlagsRequired);
        for (int j = 0; j < sampleSizes.length; ++j) {
            final TrackRunBox.Entry entry = new TrackRunBox.Entry();
            entry.setSampleSize(sampleSizes[j]);
            if (sampleFlagsRequired) {
                final SampleFlags sflags = new SampleFlags();
                if (track.getSampleDependencies() != null && !track.getSampleDependencies().isEmpty()) {
                    final SampleDependencyTypeBox.Entry e = track.getSampleDependencies().get(j);
                    sflags.setSampleDependsOn(e.getSampleDependsOn());
                    sflags.setSampleIsDependedOn(e.getSampleIsDependentOn());
                    sflags.setSampleHasRedundancy(e.getSampleHasRedundancy());
                }
                if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {
                    if (Arrays.binarySearch(track.getSyncSamples(), startSample + j + 1) >= 0) {
                        sflags.setSampleIsDifferenceSample(false);
                        sflags.setSampleDependsOn(2);
                    }
                    else {
                        sflags.setSampleIsDifferenceSample(true);
                        sflags.setSampleDependsOn(1);
                    }
                }
                entry.setSampleFlags(sflags);
            }
            entry.setSampleDuration(timeQueue.peek().getDelta());
            if (--durationEntriesLeft == 0L && timeQueue.size() > 1) {
                timeQueue.remove();
                durationEntriesLeft = timeQueue.peek().getCount();
            }
            if (compositionTimeQueue != null) {
                trun.setSampleCompositionTimeOffsetPresent(true);
                entry.setSampleCompositionTimeOffset(compositionTimeQueue.peek().getOffset());
                if (--compositionTimeEntriesLeft == 0L && compositionTimeQueue.size() > 1) {
                    compositionTimeQueue.remove();
                    compositionTimeEntriesLeft = compositionTimeQueue.element().getCount();
                }
            }
            entries.add(entry);
        }
        trun.setEntries(entries);
        return Collections.singletonList((Box)trun);
    }
    
    protected Box createMoof(final int startSample, final int endSample, final Track track, final int sequenceNumber) {
        final MovieFragmentBox moof = new MovieFragmentBox();
        moof.addBox(this.createMfhd(startSample, endSample, track, sequenceNumber));
        moof.addBox(this.createTraf(startSample, endSample, track, sequenceNumber));
        final TrackRunBox firstTrun = moof.getTrackRunBoxes().get(0);
        firstTrun.setDataOffset(1);
        firstTrun.setDataOffset((int)(8L + moof.getSize()));
        return moof;
    }
    
    protected Box createMvhd(final Movie movie) {
        final MovieHeaderBox mvhd = new MovieHeaderBox();
        mvhd.setCreationTime(DateHelper.convert(new Date()));
        mvhd.setModificationTime(DateHelper.convert(new Date()));
        final long movieTimeScale = movie.getTimescale();
        long duration = 0L;
        for (final Track track : movie.getTracks()) {
            final long tracksDuration = this.getDuration(track) * movieTimeScale / track.getTrackMetaData().getTimescale();
            if (tracksDuration > duration) {
                duration = tracksDuration;
            }
        }
        mvhd.setDuration(duration);
        mvhd.setTimescale(movieTimeScale);
        long nextTrackId = 0L;
        for (final Track track2 : movie.getTracks()) {
            nextTrackId = ((nextTrackId < track2.getTrackMetaData().getTrackId()) ? track2.getTrackMetaData().getTrackId() : nextTrackId);
        }
        mvhd.setNextTrackId(++nextTrackId);
        return mvhd;
    }
    
    protected Box createMoov(final Movie movie) {
        final MovieBox movieBox = new MovieBox();
        movieBox.addBox(this.createMvhd(movie));
        movieBox.addBox(this.createMvex(movie));
        for (final Track track : movie.getTracks()) {
            movieBox.addBox(this.createTrak(track, movie));
        }
        return movieBox;
    }
    
    protected Box createTfra(final Track track, final IsoFile isoFile) {
        final TrackFragmentRandomAccessBox tfra = new TrackFragmentRandomAccessBox();
        tfra.setVersion(1);
        final List<TrackFragmentRandomAccessBox.Entry> offset2timeEntries = new LinkedList<TrackFragmentRandomAccessBox.Entry>();
        final List<Box> boxes = isoFile.getBoxes();
        long offset = 0L;
        long duration = 0L;
        for (final Box box : boxes) {
            if (box instanceof MovieFragmentBox) {
                final List<TrackFragmentBox> trafs = ((MovieFragmentBox)box).getBoxes(TrackFragmentBox.class);
                for (int i = 0; i < trafs.size(); ++i) {
                    final TrackFragmentBox traf = trafs.get(i);
                    if (traf.getTrackFragmentHeaderBox().getTrackId() == track.getTrackMetaData().getTrackId()) {
                        final List<TrackRunBox> truns = traf.getBoxes(TrackRunBox.class);
                        for (int j = 0; j < truns.size(); ++j) {
                            final List<TrackFragmentRandomAccessBox.Entry> offset2timeEntriesThisTrun = new LinkedList<TrackFragmentRandomAccessBox.Entry>();
                            final TrackRunBox trun = truns.get(j);
                            for (int k = 0; k < trun.getEntries().size(); ++k) {
                                final TrackRunBox.Entry trunEntry = trun.getEntries().get(k);
                                SampleFlags sf = null;
                                if (k == 0 && trun.isFirstSampleFlagsPresent()) {
                                    sf = trun.getFirstSampleFlags();
                                }
                                else if (trun.isSampleFlagsPresent()) {
                                    sf = trunEntry.getSampleFlags();
                                }
                                else {
                                    final List<MovieExtendsBox> mvexs = isoFile.getMovieBox().getBoxes(MovieExtendsBox.class);
                                    for (final MovieExtendsBox mvex : mvexs) {
                                        final List<TrackExtendsBox> trexs = mvex.getBoxes(TrackExtendsBox.class);
                                        for (final TrackExtendsBox trex : trexs) {
                                            if (trex.getTrackId() == track.getTrackMetaData().getTrackId()) {
                                                sf = trex.getDefaultSampleFlags();
                                            }
                                        }
                                    }
                                }
                                if (sf == null) {
                                    throw new RuntimeException("Could not find any SampleFlags to indicate random access or not");
                                }
                                if (sf.getSampleDependsOn() == 2) {
                                    offset2timeEntriesThisTrun.add(new TrackFragmentRandomAccessBox.Entry(duration, offset, i + 1, j + 1, k + 1));
                                }
                                duration += trunEntry.getSampleDuration();
                            }
                            if (offset2timeEntriesThisTrun.size() == trun.getEntries().size() && trun.getEntries().size() > 0) {
                                offset2timeEntries.add(offset2timeEntriesThisTrun.get(0));
                            }
                            else {
                                offset2timeEntries.addAll(offset2timeEntriesThisTrun);
                            }
                        }
                    }
                }
            }
            offset += box.getSize();
        }
        tfra.setEntries(offset2timeEntries);
        tfra.setTrackId(track.getTrackMetaData().getTrackId());
        return tfra;
    }
    
    protected Box createMfra(final Movie movie, final IsoFile isoFile) {
        final MovieFragmentRandomAccessBox mfra = new MovieFragmentRandomAccessBox();
        for (final Track track : movie.getTracks()) {
            mfra.addBox(this.createTfra(track, isoFile));
        }
        final MovieFragmentRandomAccessOffsetBox mfro = new MovieFragmentRandomAccessOffsetBox();
        mfra.addBox(mfro);
        mfro.setMfraSize(mfra.getSize());
        return mfra;
    }
    
    protected Box createTrex(final Movie movie, final Track track) {
        final TrackExtendsBox trex = new TrackExtendsBox();
        trex.setTrackId(track.getTrackMetaData().getTrackId());
        trex.setDefaultSampleDescriptionIndex(1L);
        trex.setDefaultSampleDuration(0L);
        trex.setDefaultSampleSize(0L);
        final SampleFlags sf = new SampleFlags();
        if ("soun".equals(track.getHandler())) {
            sf.setSampleDependsOn(2);
            sf.setSampleIsDependedOn(2);
        }
        trex.setDefaultSampleFlags(sf);
        return trex;
    }
    
    protected Box createMvex(final Movie movie) {
        final MovieExtendsBox mvex = new MovieExtendsBox();
        for (final Track track : movie.getTracks()) {
            mvex.addBox(this.createTrex(movie, track));
        }
        return mvex;
    }
    
    protected Box createTkhd(final Movie movie, final Track track) {
        final TrackHeaderBox tkhd = new TrackHeaderBox();
        int flags = 0;
        if (track.isEnabled()) {
            ++flags;
        }
        if (track.isInMovie()) {
            flags += 2;
        }
        if (track.isInPreview()) {
            flags += 4;
        }
        if (track.isInPoster()) {
            flags += 8;
        }
        tkhd.setFlags(flags);
        tkhd.setAlternateGroup(track.getTrackMetaData().getGroup());
        tkhd.setCreationTime(DateHelper.convert(track.getTrackMetaData().getCreationTime()));
        tkhd.setDuration(this.getDuration(track) * movie.getTimescale() / track.getTrackMetaData().getTimescale());
        tkhd.setHeight(track.getTrackMetaData().getHeight());
        tkhd.setWidth(track.getTrackMetaData().getWidth());
        tkhd.setLayer(track.getTrackMetaData().getLayer());
        tkhd.setModificationTime(DateHelper.convert(new Date()));
        tkhd.setTrackId(track.getTrackMetaData().getTrackId());
        tkhd.setVolume(track.getTrackMetaData().getVolume());
        return tkhd;
    }
    
    protected Box createMdhd(final Movie movie, final Track track) {
        final MediaHeaderBox mdhd = new MediaHeaderBox();
        mdhd.setCreationTime(DateHelper.convert(track.getTrackMetaData().getCreationTime()));
        mdhd.setDuration(this.getDuration(track));
        mdhd.setTimescale(track.getTrackMetaData().getTimescale());
        mdhd.setLanguage(track.getTrackMetaData().getLanguage());
        return mdhd;
    }
    
    protected Box createStbl(final Movie movie, final Track track) {
        final SampleTableBox stbl = new SampleTableBox();
        stbl.addBox(track.getSampleDescriptionBox());
        stbl.addBox(new TimeToSampleBox());
        stbl.addBox(new StaticChunkOffsetBox());
        return stbl;
    }
    
    protected Box createMinf(final Track track, final Movie movie) {
        final MediaInformationBox minf = new MediaInformationBox();
        minf.addBox(track.getMediaHeaderBox());
        minf.addBox(this.createDinf(movie, track));
        minf.addBox(this.createStbl(movie, track));
        return minf;
    }
    
    protected Box createMdiaHdlr(final Track track, final Movie movie) {
        final HandlerBox hdlr = new HandlerBox();
        hdlr.setHandlerType(track.getHandler());
        return hdlr;
    }
    
    protected Box createMdia(final Track track, final Movie movie) {
        final MediaBox mdia = new MediaBox();
        mdia.addBox(this.createMdhd(movie, track));
        mdia.addBox(this.createMdiaHdlr(track, movie));
        mdia.addBox(this.createMinf(track, movie));
        return mdia;
    }
    
    protected Box createTrak(final Track track, final Movie movie) {
        FragmentedMp4Builder.LOG.info("Creating Track " + track);
        final TrackBox trackBox = new TrackBox();
        trackBox.addBox(this.createTkhd(movie, track));
        trackBox.addBox(this.createMdia(track, movie));
        return trackBox;
    }
    
    protected DataInformationBox createDinf(final Movie movie, final Track track) {
        final DataInformationBox dinf = new DataInformationBox();
        final DataReferenceBox dref = new DataReferenceBox();
        dinf.addBox(dref);
        final DataEntryUrlBox url = new DataEntryUrlBox();
        url.setFlags(1);
        dref.addBox(url);
        return dinf;
    }
    
    public void setIntersectionFinder(final FragmentIntersectionFinder intersectionFinder) {
        this.intersectionFinder = intersectionFinder;
    }
    
    protected long getDuration(final Track track) {
        long duration = 0L;
        for (final TimeToSampleBox.Entry entry : track.getDecodingTimeEntries()) {
            duration += entry.getCount() * entry.getDelta();
        }
        return duration;
    }
}
