// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder;

import java.nio.channels.GatheringByteChannel;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.channels.WritableByteChannel;
import java.util.Map;
import com.coremedia.iso.BoxParser;
import java.nio.channels.ReadableByteChannel;
import com.coremedia.iso.boxes.ContainerBox;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import com.coremedia.iso.boxes.CastUtils;
import com.coremedia.iso.boxes.SampleSizeBox;
import com.coremedia.iso.boxes.SampleToChunkBox;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.SyncSampleBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.SampleTableBox;
import com.coremedia.iso.boxes.DataEntryUrlBox;
import com.coremedia.iso.boxes.DataReferenceBox;
import com.coremedia.iso.boxes.DataInformationBox;
import com.coremedia.iso.boxes.MediaInformationBox;
import com.coremedia.iso.boxes.HandlerBox;
import com.coremedia.iso.boxes.MediaHeaderBox;
import com.coremedia.iso.boxes.MediaBox;
import java.util.Collections;
import com.coremedia.iso.boxes.EditListBox;
import com.coremedia.iso.boxes.EditBox;
import com.coremedia.iso.boxes.TrackHeaderBox;
import com.coremedia.iso.boxes.TrackBox;
import com.googlecode.mp4parser.authoring.DateHelper;
import java.util.Date;
import com.coremedia.iso.boxes.MovieHeaderBox;
import com.coremedia.iso.boxes.MovieBox;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.FileTypeBox;
import com.coremedia.iso.IsoFile;
import com.googlecode.mp4parser.authoring.Movie;
import java.util.LinkedList;
import java.util.HashSet;
import java.nio.ByteBuffer;
import java.util.List;
import com.googlecode.mp4parser.authoring.Track;
import java.util.HashMap;
import java.util.logging.Logger;
import com.coremedia.iso.boxes.StaticChunkOffsetBox;
import java.util.Set;

public class DefaultMp4Builder implements Mp4Builder
{
    Set<StaticChunkOffsetBox> chunkOffsetBoxes;
    private static Logger LOG;
    HashMap<Track, List<ByteBuffer>> track2Sample;
    HashMap<Track, long[]> track2SampleSizes;
    private FragmentIntersectionFinder intersectionFinder;
    List<String> hdlrs;
    
    static {
        DefaultMp4Builder.LOG = Logger.getLogger(DefaultMp4Builder.class.getName());
    }
    
    public DefaultMp4Builder() {
        this.chunkOffsetBoxes = new HashSet<StaticChunkOffsetBox>();
        this.track2Sample = new HashMap<Track, List<ByteBuffer>>();
        this.track2SampleSizes = new HashMap<Track, long[]>();
        this.intersectionFinder = new TwoSecondIntersectionFinder();
        this.hdlrs = new LinkedList<String>();
    }
    
    public void setAllowedHandlers(final List<String> hdlrs) {
        this.hdlrs = hdlrs;
    }
    
    public void setIntersectionFinder(final FragmentIntersectionFinder intersectionFinder) {
        this.intersectionFinder = intersectionFinder;
    }
    
    public IsoFile build(final Movie movie) throws IOException {
        DefaultMp4Builder.LOG.info("Creating movie " + movie);
        for (final Track track : movie.getTracks()) {
            final List<ByteBuffer> samples = track.getSamples();
            this.track2Sample.put(track, samples);
            final long[] sizes = new long[samples.size()];
            for (int i = 0; i < sizes.length; ++i) {
                sizes[i] = samples.get(i).limit();
            }
            this.track2SampleSizes.put(track, sizes);
        }
        final IsoFile isoFile = new IsoFile();
        final List<String> minorBrands = new LinkedList<String>();
        minorBrands.add("isom");
        minorBrands.add("iso2");
        minorBrands.add("avc1");
        isoFile.addBox(new FileTypeBox("isom", 0L, minorBrands));
        isoFile.addBox(this.createMovieBox(movie));
        final InterleaveChunkMdat mdat = new InterleaveChunkMdat(movie, (InterleaveChunkMdat)null);
        isoFile.addBox(mdat);
        final long dataOffset = mdat.getDataOffset();
        for (final StaticChunkOffsetBox chunkOffsetBox : this.chunkOffsetBoxes) {
            final long[] offsets = chunkOffsetBox.getChunkOffsets();
            for (int j = 0; j < offsets.length; ++j) {
                final long[] array = offsets;
                final int n = j;
                array[n] += dataOffset;
            }
        }
        return isoFile;
    }
    
    private MovieBox createMovieBox(final Movie movie) {
        final MovieBox movieBox = new MovieBox();
        final List<Box> movieBoxChildren = new LinkedList<Box>();
        final MovieHeaderBox mvhd = new MovieHeaderBox();
        mvhd.setCreationTime(DateHelper.convert(new Date()));
        mvhd.setModificationTime(DateHelper.convert(new Date()));
        final long movieTimeScale = this.getTimescale(movie);
        long duration = 0L;
        for (final Track track : movie.getTracks()) {
            final long tracksDuration = getDuration(track) * movieTimeScale / track.getTrackMetaData().getTimescale();
            if (tracksDuration > duration) {
                duration = tracksDuration;
            }
        }
        mvhd.setDuration(duration);
        mvhd.setTimescale(movieTimeScale);
        long nextTrackId = 0L;
        for (final Track track2 : movie.getTracks()) {
            nextTrackId = ((nextTrackId < track2.getTrackMetaData().getTrackId()) ? track2.getTrackMetaData().getTrackId() : nextTrackId);
        }
        mvhd.setNextTrackId(++nextTrackId);
        movieBoxChildren.add(mvhd);
        for (final Track track2 : movie.getTracks()) {
            movieBoxChildren.add(this.createTrackBox(track2, movie));
        }
        movieBox.setBoxes(movieBoxChildren);
        return movieBox;
    }
    
    private TrackBox createTrackBox(final Track track, final Movie movie) {
        DefaultMp4Builder.LOG.info("Creating Mp4TrackImpl " + track);
        final TrackBox trackBox = new TrackBox();
        final TrackHeaderBox tkhd = new TrackHeaderBox();
        int flags = 0;
        if (track.isEnabled()) {
            ++flags;
        }
        if (track.isInMovie()) {
            flags += 2;
        }
        if (track.isInPreview()) {
            flags += 4;
        }
        if (track.isInPoster()) {
            flags += 8;
        }
        tkhd.setFlags(flags);
        tkhd.setAlternateGroup(track.getTrackMetaData().getGroup());
        tkhd.setCreationTime(DateHelper.convert(track.getTrackMetaData().getCreationTime()));
        tkhd.setDuration(getDuration(track) * this.getTimescale(movie) / track.getTrackMetaData().getTimescale());
        tkhd.setHeight(track.getTrackMetaData().getHeight());
        tkhd.setWidth(track.getTrackMetaData().getWidth());
        tkhd.setLayer(track.getTrackMetaData().getLayer());
        tkhd.setModificationTime(DateHelper.convert(new Date()));
        tkhd.setTrackId(track.getTrackMetaData().getTrackId());
        tkhd.setVolume(track.getTrackMetaData().getVolume());
        trackBox.addBox(tkhd);
        final EditBox edit = new EditBox();
        final EditListBox editListBox = new EditListBox();
        editListBox.setEntries(Collections.singletonList(new EditListBox.Entry(editListBox, (long)(track.getTrackMetaData().getStartTime() * this.getTimescale(movie)), -1L, 1.0)));
        edit.addBox(editListBox);
        trackBox.addBox(edit);
        final MediaBox mdia = new MediaBox();
        trackBox.addBox(mdia);
        final MediaHeaderBox mdhd = new MediaHeaderBox();
        mdhd.setCreationTime(DateHelper.convert(track.getTrackMetaData().getCreationTime()));
        mdhd.setDuration(getDuration(track));
        mdhd.setTimescale(track.getTrackMetaData().getTimescale());
        mdhd.setLanguage(track.getTrackMetaData().getLanguage());
        mdia.addBox(mdhd);
        final HandlerBox hdlr = new HandlerBox();
        mdia.addBox(hdlr);
        hdlr.setHandlerType(track.getHandler());
        final MediaInformationBox minf = new MediaInformationBox();
        minf.addBox(track.getMediaHeaderBox());
        final DataInformationBox dinf = new DataInformationBox();
        final DataReferenceBox dref = new DataReferenceBox();
        dinf.addBox(dref);
        final DataEntryUrlBox url = new DataEntryUrlBox();
        url.setFlags(1);
        dref.addBox(url);
        minf.addBox(dinf);
        final SampleTableBox stbl = new SampleTableBox();
        stbl.addBox(track.getSampleDescriptionBox());
        final List<TimeToSampleBox.Entry> decodingTimeToSampleEntries = track.getDecodingTimeEntries();
        if (decodingTimeToSampleEntries != null && !track.getDecodingTimeEntries().isEmpty()) {
            final TimeToSampleBox stts = new TimeToSampleBox();
            stts.setEntries(track.getDecodingTimeEntries());
            stbl.addBox(stts);
        }
        final List<CompositionTimeToSample.Entry> compositionTimeToSampleEntries = track.getCompositionTimeEntries();
        if (compositionTimeToSampleEntries != null && !compositionTimeToSampleEntries.isEmpty()) {
            final CompositionTimeToSample ctts = new CompositionTimeToSample();
            ctts.setEntries(compositionTimeToSampleEntries);
            stbl.addBox(ctts);
        }
        final long[] syncSamples = track.getSyncSamples();
        if (syncSamples != null && syncSamples.length > 0) {
            final SyncSampleBox stss = new SyncSampleBox();
            stss.setSampleNumber(syncSamples);
            stbl.addBox(stss);
        }
        if (track.getSampleDependencies() != null && !track.getSampleDependencies().isEmpty()) {
            final SampleDependencyTypeBox sdtp = new SampleDependencyTypeBox();
            sdtp.setEntries(track.getSampleDependencies());
            stbl.addBox(sdtp);
        }
        final int[] chunkSize = this.getChunkSizes(track, movie);
        final SampleToChunkBox stsc = new SampleToChunkBox();
        stsc.setEntries(new LinkedList<SampleToChunkBox.Entry>());
        long lastChunkSize = -2147483648L;
        for (int i = 0; i < chunkSize.length; ++i) {
            if (lastChunkSize != chunkSize[i]) {
                stsc.getEntries().add(new SampleToChunkBox.Entry(i + 1, chunkSize[i], 1L));
                lastChunkSize = chunkSize[i];
            }
        }
        stbl.addBox(stsc);
        final SampleSizeBox stsz = new SampleSizeBox();
        stsz.setSampleSizes(this.track2SampleSizes.get(track));
        stbl.addBox(stsz);
        final StaticChunkOffsetBox stco = new StaticChunkOffsetBox();
        this.chunkOffsetBoxes.add(stco);
        long offset = 0L;
        final long[] chunkOffset = new long[chunkSize.length];
        DefaultMp4Builder.LOG.fine("Calculating chunk offsets for track_" + track.getTrackMetaData().getTrackId());
        for (int j = 0; j < chunkSize.length; ++j) {
            DefaultMp4Builder.LOG.finer("Calculating chunk offsets for track_" + track.getTrackMetaData().getTrackId() + " chunk " + j);
            for (final Track current : movie.getTracks()) {
                DefaultMp4Builder.LOG.finest("Adding offsets of track_" + current.getTrackMetaData().getTrackId());
                final int[] chunkSizes = this.getChunkSizes(current, movie);
                long firstSampleOfChunk = 0L;
                for (int k = 0; k < j; ++k) {
                    firstSampleOfChunk += chunkSizes[k];
                }
                if (current == track) {
                    chunkOffset[j] = offset;
                }
                for (int k = CastUtils.l2i(firstSampleOfChunk); k < firstSampleOfChunk + chunkSizes[j]; ++k) {
                    offset += this.track2SampleSizes.get(current)[k];
                }
            }
        }
        stco.setChunkOffsets(chunkOffset);
        stbl.addBox(stco);
        minf.addBox(stbl);
        mdia.addBox(minf);
        return trackBox;
    }
    
    int[] getChunkSizes(final Track track, final Movie movie) {
        final int[] referenceChunkStarts = this.intersectionFinder.sampleNumbers(track, movie);
        final int[] chunkSizes = new int[referenceChunkStarts.length];
        for (int i = 0; i < referenceChunkStarts.length; ++i) {
            final int start = referenceChunkStarts[i] - 1;
            int end;
            if (referenceChunkStarts.length == i + 1) {
                end = track.getSamples().size() - 1;
            }
            else {
                end = referenceChunkStarts[i + 1] - 1;
            }
            chunkSizes[i] = end - start;
        }
        assert this.track2Sample.get(track).size() == sum(chunkSizes) : "The number of samples and the sum of all chunk lengths must be equal";
        return chunkSizes;
    }
    
    private static long sum(final int[] ls) {
        long rc = 0L;
        for (final long l : ls) {
            rc += l;
        }
        return rc;
    }
    
    protected static long getDuration(final Track track) {
        long duration = 0L;
        for (final TimeToSampleBox.Entry entry : track.getDecodingTimeEntries()) {
            duration += entry.getCount() * entry.getDelta();
        }
        return duration;
    }
    
    public long getTimescale(final Movie movie) {
        long timescale = movie.getTracks().iterator().next().getTrackMetaData().getTimescale();
        for (final Track track : movie.getTracks()) {
            timescale = gcd(track.getTrackMetaData().getTimescale(), timescale);
        }
        return timescale;
    }
    
    public static long gcd(final long a, final long b) {
        if (b == 0L) {
            return a;
        }
        return gcd(b, a % b);
    }
    
    public List<ByteBuffer> unifyAdjacentBuffers(final List<ByteBuffer> samples) {
        final ArrayList<ByteBuffer> nuSamples = new ArrayList<ByteBuffer>(samples.size());
        for (final ByteBuffer buffer : samples) {
            final int lastIndex = nuSamples.size() - 1;
            if (lastIndex >= 0 && buffer.hasArray() && nuSamples.get(lastIndex).hasArray() && buffer.array() == nuSamples.get(lastIndex).array() && nuSamples.get(lastIndex).arrayOffset() + nuSamples.get(lastIndex).limit() == buffer.arrayOffset()) {
                final ByteBuffer oldBuffer = nuSamples.remove(lastIndex);
                final ByteBuffer nu = ByteBuffer.wrap(buffer.array(), oldBuffer.arrayOffset(), oldBuffer.limit() + buffer.limit()).slice();
                nuSamples.add(nu);
            }
            else if (lastIndex >= 0 && buffer instanceof MappedByteBuffer && nuSamples.get(lastIndex) instanceof MappedByteBuffer && nuSamples.get(lastIndex).limit() == nuSamples.get(lastIndex).capacity() - buffer.capacity()) {
                final ByteBuffer oldBuffer = nuSamples.get(lastIndex);
                oldBuffer.limit(buffer.limit() + oldBuffer.limit());
            }
            else {
                nuSamples.add(buffer);
            }
        }
        return nuSamples;
    }
    
    private class InterleaveChunkMdat implements Box
    {
        List<Track> tracks;
        List<ByteBuffer> samples;
        ContainerBox parent;
        long contentSize;
        
        public ContainerBox getParent() {
            return this.parent;
        }
        
        public void setParent(final ContainerBox parent) {
            this.parent = parent;
        }
        
        public void parse(final ReadableByteChannel inFC, final ByteBuffer header, final long contentSize, final BoxParser boxParser) throws IOException {
        }
        
        private InterleaveChunkMdat(final Movie movie) {
            this.samples = new LinkedList<ByteBuffer>();
            this.contentSize = 0L;
            this.tracks = movie.getTracks();
            final Map<Track, int[]> chunks = new HashMap<Track, int[]>();
            for (final Track track : movie.getTracks()) {
                chunks.put(track, DefaultMp4Builder.this.getChunkSizes(track, movie));
            }
            for (int i = 0; i < chunks.values().iterator().next().length; ++i) {
                for (final Track track2 : this.tracks) {
                    final int[] chunkSizes = chunks.get(track2);
                    long firstSampleOfChunk = 0L;
                    for (int j = 0; j < i; ++j) {
                        firstSampleOfChunk += chunkSizes[j];
                    }
                    for (int j = CastUtils.l2i(firstSampleOfChunk); j < firstSampleOfChunk + chunkSizes[i]; ++j) {
                        final ByteBuffer s = DefaultMp4Builder.this.track2Sample.get(track2).get(j);
                        this.contentSize += s.limit();
                        this.samples.add((ByteBuffer)s.rewind());
                    }
                }
            }
        }
        
        public long getDataOffset() {
            Box b = this;
            long offset = 16L;
            while (b.getParent() != null) {
                for (final Box box : b.getParent().getBoxes()) {
                    if (b == box) {
                        break;
                    }
                    offset += box.getSize();
                }
                b = b.getParent();
            }
            return offset;
        }
        
        public String getType() {
            return "mdat";
        }
        
        public long getSize() {
            return 16L + this.contentSize;
        }
        
        private boolean isSmallBox(final long contentSize) {
            return contentSize + 8L < 4294967296L;
        }
        
        public void getBox(final WritableByteChannel writableByteChannel) throws IOException {
            final ByteBuffer bb = ByteBuffer.allocate(16);
            final long size = this.getSize();
            if (this.isSmallBox(size)) {
                IsoTypeWriter.writeUInt32(bb, size);
            }
            else {
                IsoTypeWriter.writeUInt32(bb, 1L);
            }
            bb.put(IsoFile.fourCCtoBytes("mdat"));
            if (this.isSmallBox(size)) {
                bb.put(new byte[8]);
            }
            else {
                IsoTypeWriter.writeUInt64(bb, size);
            }
            bb.rewind();
            writableByteChannel.write(bb);
            if (writableByteChannel instanceof GatheringByteChannel) {
                final List<ByteBuffer> nuSamples = DefaultMp4Builder.this.unifyAdjacentBuffers(this.samples);
                for (int STEPSIZE = 1024, i = 0; i < Math.ceil(nuSamples.size() / (double)STEPSIZE); ++i) {
                    final List<ByteBuffer> sublist = nuSamples.subList(i * STEPSIZE, ((i + 1) * STEPSIZE < nuSamples.size()) ? ((i + 1) * STEPSIZE) : nuSamples.size());
                    final ByteBuffer[] sampleArray = sublist.toArray(new ByteBuffer[sublist.size()]);
                    do {
                        ((GatheringByteChannel)writableByteChannel).write(sampleArray);
                    } while (sampleArray[sampleArray.length - 1].remaining() > 0);
                }
            }
            else {
                for (final ByteBuffer sample : this.samples) {
                    sample.rewind();
                    writableByteChannel.write(sample);
                }
            }
        }
    }
}
