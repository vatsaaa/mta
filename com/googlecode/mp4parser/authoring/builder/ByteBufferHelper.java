// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder;

import java.util.Iterator;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.nio.ByteBuffer;
import java.util.List;

public class ByteBufferHelper
{
    public static List<ByteBuffer> mergeAdjacentBuffers(final List<ByteBuffer> samples) {
        final ArrayList<ByteBuffer> nuSamples = new ArrayList<ByteBuffer>(samples.size());
        for (final ByteBuffer buffer : samples) {
            final int lastIndex = nuSamples.size() - 1;
            if (lastIndex >= 0 && buffer.hasArray() && nuSamples.get(lastIndex).hasArray() && buffer.array() == nuSamples.get(lastIndex).array() && nuSamples.get(lastIndex).arrayOffset() + nuSamples.get(lastIndex).limit() == buffer.arrayOffset()) {
                final ByteBuffer oldBuffer = nuSamples.remove(lastIndex);
                final ByteBuffer nu = ByteBuffer.wrap(buffer.array(), oldBuffer.arrayOffset(), oldBuffer.limit() + buffer.limit()).slice();
                nuSamples.add(nu);
            }
            else if (lastIndex >= 0 && buffer instanceof MappedByteBuffer && nuSamples.get(lastIndex) instanceof MappedByteBuffer && nuSamples.get(lastIndex).limit() == nuSamples.get(lastIndex).capacity() - buffer.capacity()) {
                final ByteBuffer oldBuffer = nuSamples.get(lastIndex);
                oldBuffer.limit(buffer.limit() + oldBuffer.limit());
            }
            else {
                nuSamples.add(buffer);
            }
        }
        return nuSamples;
    }
}
