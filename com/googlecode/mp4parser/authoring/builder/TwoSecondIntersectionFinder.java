// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.authoring.builder;

import java.util.List;
import java.util.Arrays;
import com.googlecode.mp4parser.authoring.Movie;
import java.util.Iterator;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.googlecode.mp4parser.authoring.Track;

public class TwoSecondIntersectionFinder implements FragmentIntersectionFinder
{
    protected long getDuration(final Track track) {
        long duration = 0L;
        for (final TimeToSampleBox.Entry entry : track.getDecodingTimeEntries()) {
            duration += entry.getCount() * entry.getDelta();
        }
        return duration;
    }
    
    public int[] sampleNumbers(final Track track, final Movie movie) {
        final List<TimeToSampleBox.Entry> entries = track.getDecodingTimeEntries();
        double trackLength = 0.0;
        for (final Track thisTrack : movie.getTracks()) {
            final double thisTracksLength = (double)(this.getDuration(thisTrack) / thisTrack.getTrackMetaData().getTimescale());
            if (trackLength < thisTracksLength) {
                trackLength = thisTracksLength;
            }
        }
        final int[] fragments = new int[(int)Math.ceil(trackLength / 2.0) - 1];
        Arrays.fill(fragments, -1);
        fragments[0] = 0;
        long time = 0L;
        int samples = 0;
        for (final TimeToSampleBox.Entry entry : entries) {
            for (int i = 0; i < entry.getCount(); ++i) {
                final int currentFragment = (int)(time / track.getTrackMetaData().getTimescale() / 2L) + 1;
                if (currentFragment >= fragments.length) {
                    break;
                }
                fragments[currentFragment] = samples++;
                time += entry.getDelta();
            }
        }
        int last = samples;
        for (int j = fragments.length - 1; j >= 0; --j) {
            if (fragments[j] == -1) {
                fragments[j] = last;
            }
            last = fragments[j];
        }
        return fragments;
    }
}
