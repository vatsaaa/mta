// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.util;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.util.UUID;

public class UUIDConverter
{
    public static byte[] convert(final UUID uuid) {
        final long msb = uuid.getMostSignificantBits();
        final long lsb = uuid.getLeastSignificantBits();
        final byte[] buffer = new byte[16];
        for (int i = 0; i < 8; ++i) {
            buffer[i] = (byte)(msb >>> 8 * (7 - i));
        }
        for (int i = 8; i < 16; ++i) {
            buffer[i] = (byte)(lsb >>> 8 * (7 - i));
        }
        return buffer;
    }
    
    public static UUID convert(final byte[] uuidBytes) {
        final ByteBuffer b = ByteBuffer.wrap(uuidBytes);
        b.order(ByteOrder.BIG_ENDIAN);
        return new UUID(b.getLong(), b.getLong());
    }
}
