// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.util;

import java.util.Iterator;
import java.util.regex.Matcher;
import com.coremedia.iso.boxes.ContainerBox;
import java.util.List;
import com.coremedia.iso.boxes.Box;
import java.util.regex.Pattern;
import com.coremedia.iso.IsoFile;

public class Path
{
    IsoFile isoFile;
    private static Pattern component;
    
    static {
        Path.component = Pattern.compile("(....)(\\[(.*)\\])?");
    }
    
    public Path(final IsoFile isoFile) {
        this.isoFile = isoFile;
    }
    
    public String createPath(final Box box) {
        return this.createPath(box, "");
    }
    
    private String createPath(final Box box, String path) {
        if (!(box instanceof IsoFile)) {
            final List<?> boxesOfBoxType = box.getParent().getBoxes((Class<?>)box.getClass());
            final int index = boxesOfBoxType.indexOf(box);
            if (index != 0) {
                path = String.valueOf(String.format("/%s[%d]", box.getType(), index)) + path;
            }
            else {
                path = String.valueOf(String.format("/%s", box.getType())) + path;
            }
            return this.createPath(box.getParent(), path);
        }
        assert box == this.isoFile;
        return path;
    }
    
    public Box getPath(final String path) {
        return this.getPath(this.isoFile, path);
    }
    
    private Box getPath(final Box box, String path) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        if (path.isEmpty()) {
            return box;
        }
        String later;
        String now;
        if (path.contains("/")) {
            later = path.substring(path.indexOf(47));
            now = path.substring(0, path.indexOf(47));
        }
        else {
            now = path;
            later = "";
        }
        final Matcher m = Path.component.matcher(now);
        if (m.matches()) {
            final String type = m.group(1);
            int index = 0;
            if (m.group(2) != null) {
                final String indexString = m.group(3);
                index = Integer.parseInt(indexString);
            }
            for (final Box box2 : ((ContainerBox)box).getBoxes()) {
                if (box2.getType().equals(type)) {
                    if (index == 0) {
                        return this.getPath(box2, later);
                    }
                    --index;
                }
            }
            return null;
        }
        throw new RuntimeException("invalid path.");
    }
}
