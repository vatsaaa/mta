// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264.model;

import com.googlecode.mp4parser.h264.read.CAVLCReader;
import java.io.IOException;
import com.googlecode.mp4parser.h264.write.CAVLCWriter;

public class ScalingList
{
    public int[] scalingList;
    public boolean useDefaultScalingMatrixFlag;
    
    public void write(final CAVLCWriter out) throws IOException {
        if (this.useDefaultScalingMatrixFlag) {
            out.writeSE(0, "SPS: ");
            return;
        }
        int lastScale = 8;
        final int nextScale = 8;
        for (int j = 0; j < this.scalingList.length; ++j) {
            if (nextScale != 0) {
                final int deltaScale = this.scalingList[j] - lastScale - 256;
                out.writeSE(deltaScale, "SPS: ");
            }
            lastScale = this.scalingList[j];
        }
    }
    
    public static ScalingList read(final CAVLCReader is, final int sizeOfScalingList) throws IOException {
        final ScalingList sl = new ScalingList();
        sl.scalingList = new int[sizeOfScalingList];
        int lastScale = 8;
        int nextScale = 8;
        for (int j = 0; j < sizeOfScalingList; ++j) {
            if (nextScale != 0) {
                final int deltaScale = is.readSE("deltaScale");
                nextScale = (lastScale + deltaScale + 256) % 256;
                sl.useDefaultScalingMatrixFlag = (j == 0 && nextScale == 0);
            }
            sl.scalingList[j] = ((nextScale == 0) ? lastScale : nextScale);
            lastScale = sl.scalingList[j];
        }
        return sl;
    }
    
    @Override
    public String toString() {
        return "ScalingList{scalingList=" + this.scalingList + ", useDefaultScalingMatrixFlag=" + this.useDefaultScalingMatrixFlag + '}';
    }
}
