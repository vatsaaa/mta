// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264.model;

public class AspectRatio
{
    public static final AspectRatio Extended_SAR;
    private int value;
    
    static {
        Extended_SAR = new AspectRatio(255);
    }
    
    private AspectRatio(final int value) {
        this.value = value;
    }
    
    public static AspectRatio fromValue(final int value) {
        if (value == AspectRatio.Extended_SAR.value) {
            return AspectRatio.Extended_SAR;
        }
        return new AspectRatio(value);
    }
    
    public int getValue() {
        return this.value;
    }
}
