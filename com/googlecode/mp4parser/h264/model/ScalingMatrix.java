// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264.model;

import java.util.Arrays;

public class ScalingMatrix
{
    public ScalingList[] ScalingList4x4;
    public ScalingList[] ScalingList8x8;
    
    @Override
    public String toString() {
        return "ScalingMatrix{ScalingList4x4=" + ((this.ScalingList4x4 == null) ? null : Arrays.asList(this.ScalingList4x4)) + "\n" + ", ScalingList8x8=" + ((this.ScalingList8x8 == null) ? null : Arrays.asList(this.ScalingList8x8)) + "\n" + '}';
    }
}
