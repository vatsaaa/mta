// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264.read;

import com.googlecode.mp4parser.h264.Debug;
import com.googlecode.mp4parser.h264.BTree;
import java.io.IOException;
import java.io.InputStream;

public class CAVLCReader extends BitstreamReader
{
    public CAVLCReader(final InputStream is) throws IOException {
        super(is);
    }
    
    public long readNBit(final int n, final String message) throws IOException {
        final long val = this.readNBit(n);
        this.trace(message, String.valueOf(val));
        return val;
    }
    
    private int readUE() throws IOException {
        int cnt = 0;
        while (this.read1Bit() == 0) {
            ++cnt;
        }
        int res = 0;
        if (cnt > 0) {
            final long val = this.readNBit(cnt);
            res = (int)((1 << cnt) - 1 + val);
        }
        return res;
    }
    
    public int readUE(final String message) throws IOException {
        final int res = this.readUE();
        this.trace(message, String.valueOf(res));
        return res;
    }
    
    public int readSE(final String message) throws IOException {
        int val = this.readUE();
        final int sign = ((val & 0x1) << 1) - 1;
        val = ((val >> 1) + (val & 0x1)) * sign;
        this.trace(message, String.valueOf(val));
        return val;
    }
    
    public boolean readBool(final String message) throws IOException {
        final boolean res = this.read1Bit() != 0;
        this.trace(message, res ? "1" : "0");
        return res;
    }
    
    public int readU(final int i, final String string) throws IOException {
        return (int)this.readNBit(i, string);
    }
    
    public byte[] read(final int payloadSize) throws IOException {
        final byte[] result = new byte[payloadSize];
        for (int i = 0; i < payloadSize; ++i) {
            result[i] = (byte)this.readByte();
        }
        return result;
    }
    
    public boolean readAE() {
        throw new UnsupportedOperationException("Stan");
    }
    
    public int readTE(final int max) throws IOException {
        if (max > 1) {
            return this.readUE();
        }
        return ~this.read1Bit() & 0x1;
    }
    
    public int readAEI() {
        throw new UnsupportedOperationException("Stan");
    }
    
    public int readME(final String string) throws IOException {
        return this.readUE(string);
    }
    
    public Object readCE(BTree bt, final String message) throws IOException {
        Object i;
        do {
            final int bit = this.read1Bit();
            bt = bt.down(bit);
            if (bt == null) {
                throw new RuntimeException("Illegal code");
            }
            i = bt.getValue();
        } while (i == null);
        this.trace(message, i.toString());
        return i;
    }
    
    public int readZeroBitCount(final String message) throws IOException {
        int count = 0;
        while (this.read1Bit() == 0) {
            ++count;
        }
        this.trace(message, String.valueOf(count));
        return count;
    }
    
    public void readTrailingBits() throws IOException {
        this.read1Bit();
        this.readRemainingByte();
    }
    
    private void trace(final String message, final String val) {
        final StringBuilder traceBuilder = new StringBuilder();
        final String pos = String.valueOf(BitstreamReader.bitsRead - this.debugBits.length());
        int spaces = 8 - pos.length();
        traceBuilder.append("@" + pos);
        for (int i = 0; i < spaces; ++i) {
            traceBuilder.append(' ');
        }
        traceBuilder.append(message);
        spaces = 100 - traceBuilder.length() - this.debugBits.length();
        for (int i = 0; i < spaces; ++i) {
            traceBuilder.append(' ');
        }
        traceBuilder.append(this.debugBits);
        traceBuilder.append(" (" + val + ")");
        this.debugBits.clear();
        Debug.println(traceBuilder.toString());
    }
}
