// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264.read;

import java.io.IOException;
import com.googlecode.mp4parser.h264.CharCache;
import java.io.InputStream;

public class BitstreamReader
{
    private InputStream is;
    private int curByte;
    private int nextByte;
    int nBit;
    protected static int bitsRead;
    protected CharCache debugBits;
    
    public BitstreamReader(final InputStream is) throws IOException {
        this.debugBits = new CharCache(50);
        this.is = is;
        this.curByte = is.read();
        this.nextByte = is.read();
    }
    
    public int read1Bit() throws IOException {
        if (this.nBit == 8) {
            this.advance();
            if (this.curByte == -1) {
                return -1;
            }
        }
        final int res = this.curByte >> 7 - this.nBit & 0x1;
        ++this.nBit;
        this.debugBits.append((res == 0) ? '0' : '1');
        ++BitstreamReader.bitsRead;
        return res;
    }
    
    public long readNBit(final int n) throws IOException {
        if (n > 64) {
            throw new IllegalArgumentException("Can not readByte more then 64 bit");
        }
        long val = 0L;
        for (int i = 0; i < n; ++i) {
            val <<= 1;
            val |= this.read1Bit();
        }
        return val;
    }
    
    private void advance() throws IOException {
        this.curByte = this.nextByte;
        this.nextByte = this.is.read();
        this.nBit = 0;
    }
    
    public int readByte() throws IOException {
        if (this.nBit > 0) {
            this.advance();
        }
        final int res = this.curByte;
        this.advance();
        return res;
    }
    
    public boolean moreRBSPData() throws IOException {
        if (this.nBit == 8) {
            this.advance();
        }
        final int tail = 1 << 8 - this.nBit - 1;
        final int mask = (tail << 1) - 1;
        final boolean hasTail = (this.curByte & mask) == tail;
        return this.curByte != -1 && (this.nextByte != -1 || !hasTail);
    }
    
    public long getBitPosition() {
        return BitstreamReader.bitsRead * 8 + this.nBit % 8;
    }
    
    public long readRemainingByte() throws IOException {
        return this.readNBit(8 - this.nBit);
    }
    
    public int peakNextBits(final int n) throws IOException {
        if (n > 8) {
            throw new IllegalArgumentException("N should be less then 8");
        }
        if (this.nBit == 8) {
            this.advance();
            if (this.curByte == -1) {
                return -1;
            }
        }
        final int[] bits = new int[16 - this.nBit];
        int cnt = 0;
        for (int i = this.nBit; i < 8; ++i) {
            bits[cnt++] = (this.curByte >> 7 - i & 0x1);
        }
        for (int i = 0; i < 8; ++i) {
            bits[cnt++] = (this.nextByte >> 7 - i & 0x1);
        }
        int result = 0;
        for (int j = 0; j < n; ++j) {
            result <<= 1;
            result |= bits[j];
        }
        return result;
    }
    
    public boolean isByteAligned() {
        return this.nBit % 8 == 0;
    }
    
    public void close() throws IOException {
    }
    
    public int getCurBit() {
        return this.nBit;
    }
}
