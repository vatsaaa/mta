// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264;

public class CharCache
{
    private char[] cache;
    private int pos;
    
    public CharCache(final int capacity) {
        this.cache = new char[capacity];
    }
    
    public void append(final String str) {
        final char[] chars = str.toCharArray();
        final int available = this.cache.length - this.pos;
        final int toWrite = (chars.length < available) ? chars.length : available;
        System.arraycopy(chars, 0, this.cache, this.pos, toWrite);
        this.pos += toWrite;
    }
    
    @Override
    public String toString() {
        return new String(this.cache, 0, this.pos);
    }
    
    public void clear() {
        this.pos = 0;
    }
    
    public void append(final char c) {
        if (this.pos < this.cache.length - 1) {
            this.cache[this.pos] = c;
            ++this.pos;
        }
    }
    
    public int length() {
        return this.pos;
    }
}
