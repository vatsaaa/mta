// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264.write;

import com.googlecode.mp4parser.h264.Debug;
import java.io.IOException;
import java.io.OutputStream;

public class BitstreamWriter
{
    private final OutputStream os;
    private int[] curByte;
    private int curBit;
    
    public BitstreamWriter(final OutputStream out) {
        this.curByte = new int[8];
        this.os = out;
    }
    
    public void flush() throws IOException {
        for (int i = this.curBit; i < 8; ++i) {
            this.curByte[i] = 0;
        }
        this.curBit = 0;
        this.writeCurByte();
    }
    
    private void writeCurByte() throws IOException {
        final int toWrite = this.curByte[0] << 7 | this.curByte[1] << 6 | this.curByte[2] << 5 | this.curByte[3] << 4 | this.curByte[4] << 3 | this.curByte[5] << 2 | this.curByte[6] << 1 | this.curByte[7];
        this.os.write(toWrite);
    }
    
    public void write1Bit(final int value) throws IOException {
        Debug.print(value);
        if (this.curBit == 8) {
            this.curBit = 0;
            this.writeCurByte();
        }
        this.curByte[this.curBit++] = value;
    }
    
    public void writeNBit(final long value, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.write1Bit((int)(value >> n - i - 1) & 0x1);
        }
    }
    
    public void writeRemainingZero() throws IOException {
        this.writeNBit(0L, 8 - this.curBit);
    }
    
    public void writeByte(final int b) throws IOException {
        this.os.write(b);
    }
}
