// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264.write;

import java.io.IOException;
import com.googlecode.mp4parser.h264.Debug;
import java.io.OutputStream;

public class CAVLCWriter extends BitstreamWriter
{
    public CAVLCWriter(final OutputStream out) {
        super(out);
    }
    
    public void writeU(final int value, final int n, final String string) throws IOException {
        Debug.print(String.valueOf(string) + "\t");
        this.writeNBit(value, n);
        Debug.println("\t" + value);
    }
    
    public void writeUE(final int value) throws IOException {
        int bits = 0;
        int cumul = 0;
        for (int i = 0; i < 15; ++i) {
            if (value < cumul + (1 << i)) {
                bits = i;
                break;
            }
            cumul += 1 << i;
        }
        this.writeNBit(0L, bits);
        this.write1Bit(1);
        this.writeNBit(value - cumul, bits);
    }
    
    public void writeUE(final int value, final String string) throws IOException {
        Debug.print(String.valueOf(string) + "\t");
        this.writeUE(value);
        Debug.println("\t" + value);
    }
    
    public void writeSE(final int value, final String string) throws IOException {
        Debug.print(String.valueOf(string) + "\t");
        this.writeUE((value << 1) * ((value < 0) ? -1 : 1) + ((value > 0) ? 1 : 0));
        Debug.println("\t" + value);
    }
    
    public void writeBool(final boolean value, final String string) throws IOException {
        Debug.print(String.valueOf(string) + "\t");
        this.write1Bit(value ? 1 : 0);
        Debug.println("\t" + value);
    }
    
    public void writeU(final int i, final int n) throws IOException {
        this.writeNBit(i, n);
    }
    
    public void writeNBit(final long value, final int n, final String string) throws IOException {
        Debug.print(String.valueOf(string) + "\t");
        for (int i = 0; i < n; ++i) {
            this.write1Bit((int)(value >> n - i - 1) & 0x1);
        }
        Debug.println("\t" + value);
    }
    
    public void writeTrailingBits() throws IOException {
        this.write1Bit(1);
        this.writeRemainingZero();
        this.flush();
    }
    
    public void writeSliceTrailingBits() {
        throw new IllegalStateException("todo");
    }
}
