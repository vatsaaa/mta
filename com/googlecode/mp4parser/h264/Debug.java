// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264;

import java.nio.ShortBuffer;

public class Debug
{
    public static final boolean debug = false;
    
    public static final void print8x8(final int[] output) {
        int i = 0;
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                System.out.printf("%3d, ", output[i]);
                ++i;
            }
            System.out.println();
        }
    }
    
    public static final void print8x8(final short[] output) {
        int i = 0;
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                System.out.printf("%3d, ", output[i]);
                ++i;
            }
            System.out.println();
        }
    }
    
    public static final void print8x8(final ShortBuffer output) {
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                System.out.printf("%3d, ", output.get());
            }
            System.out.println();
        }
    }
    
    public static void print(final short[] table) {
        int i = 0;
        for (int x = 0; x < 8; ++x) {
            for (int y = 0; y < 8; ++y) {
                System.out.printf("%3d, ", table[i]);
                ++i;
            }
            System.out.println();
        }
    }
    
    public static void trace(final String format, final Object... args) {
    }
    
    public static void print(final int i) {
    }
    
    public static void print(final String string) {
    }
    
    public static void println(final String string) {
    }
}
