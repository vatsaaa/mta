// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.h264;

public class BTree
{
    private BTree zero;
    private BTree one;
    private Object value;
    
    public void addString(final String path, final Object value) {
        if (path.length() == 0) {
            this.value = value;
            return;
        }
        final char charAt = path.charAt(0);
        BTree branch;
        if (charAt == '0') {
            if (this.zero == null) {
                this.zero = new BTree();
            }
            branch = this.zero;
        }
        else {
            if (this.one == null) {
                this.one = new BTree();
            }
            branch = this.one;
        }
        branch.addString(path.substring(1), value);
    }
    
    public BTree down(final int b) {
        if (b == 0) {
            return this.zero;
        }
        return this.one;
    }
    
    public Object getValue() {
        return this.value;
    }
}
