// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitWriterBuffer;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitReaderBuffer;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractBox;

public class EC3SpecificBox extends AbstractBox
{
    List<Entry> entries;
    int dataRate;
    int numIndSub;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    
    public EC3SpecificBox() {
        super("dec3");
        this.entries = new LinkedList<Entry>();
    }
    
    @Override
    protected long getContentSize() {
        long size = 2L;
        for (final Entry entry : this.entries) {
            if (entry.num_dep_sub > 0) {
                size += 4L;
            }
            else {
                size += 3L;
            }
        }
        return size;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        final BitReaderBuffer brb = new BitReaderBuffer(content);
        this.dataRate = brb.readBits(13);
        this.numIndSub = brb.readBits(3) + 1;
        for (int i = 0; i < this.numIndSub; ++i) {
            final Entry e = new Entry();
            e.fscod = brb.readBits(2);
            e.bsid = brb.readBits(5);
            e.bsmod = brb.readBits(5);
            e.acmod = brb.readBits(3);
            e.lfeon = brb.readBits(1);
            e.reserved = brb.readBits(3);
            e.num_dep_sub = brb.readBits(4);
            if (e.num_dep_sub > 0) {
                e.chan_loc = brb.readBits(9);
            }
            else {
                e.reserved2 = brb.readBits(1);
            }
            this.entries.add(e);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        final BitWriterBuffer bwb = new BitWriterBuffer(bb);
        bwb.writeBits(this.dataRate, 13);
        bwb.writeBits(this.entries.size() - 1, 3);
        for (final Entry e : this.entries) {
            bwb.writeBits(e.fscod, 2);
            bwb.writeBits(e.bsid, 5);
            bwb.writeBits(e.bsmod, 5);
            bwb.writeBits(e.acmod, 3);
            bwb.writeBits(e.lfeon, 1);
            bwb.writeBits(e.reserved, 3);
            bwb.writeBits(e.num_dep_sub, 4);
            if (e.num_dep_sub > 0) {
                bwb.writeBits(e.chan_loc, 9);
            }
            else {
                bwb.writeBits(e.reserved2, 1);
            }
        }
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EC3SpecificBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EC3SpecificBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    public void addEntry(final Entry entry) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EC3SpecificBox.ajc$tjp_2, this, this, entry));
        this.entries.add(entry);
    }
    
    public int getDataRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EC3SpecificBox.ajc$tjp_3, this, this));
        return this.dataRate;
    }
    
    public void setDataRate(final int dataRate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EC3SpecificBox.ajc$tjp_4, this, this, Conversions.intObject(dataRate)));
        this.dataRate = dataRate;
    }
    
    public int getNumIndSub() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EC3SpecificBox.ajc$tjp_5, this, this));
        return this.numIndSub;
    }
    
    public void setNumIndSub(final int numIndSub) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EC3SpecificBox.ajc$tjp_6, this, this, Conversions.intObject(numIndSub)));
        this.numIndSub = numIndSub;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("EC3SpecificBox.java", EC3SpecificBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "", "", "", "java.util.List"), 85);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "java.util.List", "entries", "", "void"), 89);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "addEntry", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "com.googlecode.mp4parser.boxes.EC3SpecificBox$Entry", "entry", "", "void"), 93);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDataRate", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "", "", "", "int"), 97);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDataRate", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "int", "dataRate", "", "void"), 101);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getNumIndSub", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "", "", "", "int"), 105);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setNumIndSub", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "int", "numIndSub", "", "void"), 109);
    }
    
    public static class Entry
    {
        public int fscod;
        public int bsid;
        public int bsmod;
        public int acmod;
        public int lfeon;
        public int reserved;
        public int num_dep_sub;
        public int chan_loc;
        public int reserved2;
        
        @Override
        public String toString() {
            return "Entry{fscod=" + this.fscod + ", bsid=" + this.bsid + ", bsmod=" + this.bsmod + ", acmod=" + this.acmod + ", lfeon=" + this.lfeon + ", reserved=" + this.reserved + ", num_dep_sub=" + this.num_dep_sub + ", chan_loc=" + this.chan_loc + ", reserved2=" + this.reserved2 + '}';
        }
    }
}
