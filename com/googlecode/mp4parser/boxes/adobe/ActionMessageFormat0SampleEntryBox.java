// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.adobe;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.sampleentry.SampleEntry;

public class ActionMessageFormat0SampleEntryBox extends SampleEntry
{
    public ActionMessageFormat0SampleEntryBox() {
        super("amf0");
    }
    
    @Override
    protected long getContentSize() {
        long size = 8L;
        for (final Box box : this.boxes) {
            size += box.getSize();
        }
        return size;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this._parseReservedAndDataReferenceIndex(content);
        this._parseChildBoxes(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this._writeReservedAndDataReferenceIndex(bb);
        this._writeChildBoxes(bb);
    }
}
