// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitWriterBuffer;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitReaderBuffer;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractBox;

public class AC3SpecificBox extends AbstractBox
{
    int fscod;
    int bsid;
    int bsmod;
    int acmod;
    int lfeon;
    int bitRateCode;
    int reserved;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    
    public AC3SpecificBox() {
        super("dac3");
    }
    
    @Override
    protected long getContentSize() {
        return 3L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        final BitReaderBuffer brb = new BitReaderBuffer(content);
        this.fscod = brb.readBits(2);
        this.bsid = brb.readBits(5);
        this.bsmod = brb.readBits(3);
        this.acmod = brb.readBits(3);
        this.lfeon = brb.readBits(1);
        this.bitRateCode = brb.readBits(5);
        this.reserved = brb.readBits(5);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        final BitWriterBuffer bwb = new BitWriterBuffer(bb);
        bwb.writeBits(this.fscod, 2);
        bwb.writeBits(this.bsid, 5);
        bwb.writeBits(this.bsmod, 3);
        bwb.writeBits(this.acmod, 3);
        bwb.writeBits(this.lfeon, 1);
        bwb.writeBits(this.bitRateCode, 5);
        bwb.writeBits(this.reserved, 5);
    }
    
    public int getFscod() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_0, this, this));
        return this.fscod;
    }
    
    public void setFscod(final int fscod) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_1, this, this, Conversions.intObject(fscod)));
        this.fscod = fscod;
    }
    
    public int getBsid() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_2, this, this));
        return this.bsid;
    }
    
    public void setBsid(final int bsid) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_3, this, this, Conversions.intObject(bsid)));
        this.bsid = bsid;
    }
    
    public int getBsmod() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_4, this, this));
        return this.bsmod;
    }
    
    public void setBsmod(final int bsmod) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_5, this, this, Conversions.intObject(bsmod)));
        this.bsmod = bsmod;
    }
    
    public int getAcmod() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_6, this, this));
        return this.acmod;
    }
    
    public void setAcmod(final int acmod) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_7, this, this, Conversions.intObject(acmod)));
        this.acmod = acmod;
    }
    
    public int getLfeon() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_8, this, this));
        return this.lfeon;
    }
    
    public void setLfeon(final int lfeon) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_9, this, this, Conversions.intObject(lfeon)));
        this.lfeon = lfeon;
    }
    
    public int getBitRateCode() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_10, this, this));
        return this.bitRateCode;
    }
    
    public void setBitRateCode(final int bitRateCode) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_11, this, this, Conversions.intObject(bitRateCode)));
        this.bitRateCode = bitRateCode;
    }
    
    public int getReserved() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_12, this, this));
        return this.reserved;
    }
    
    public void setReserved(final int reserved) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_13, this, this, Conversions.intObject(reserved)));
        this.reserved = reserved;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AC3SpecificBox.ajc$tjp_14, this, this));
        return "AC3SpecificBox{fscod=" + this.fscod + ", bsid=" + this.bsid + ", bsmod=" + this.bsmod + ", acmod=" + this.acmod + ", lfeon=" + this.lfeon + ", bitRateCode=" + this.bitRateCode + ", reserved=" + this.reserved + '}';
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AC3SpecificBox.java", AC3SpecificBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFscod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 52);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setFscod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "fscod", "", "void"), 56);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBitRateCode", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 92);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBitRateCode", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "bitRateCode", "", "void"), 96);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReserved", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 100);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setReserved", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "reserved", "", "void"), 104);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "java.lang.String"), 109);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBsid", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 60);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBsid", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "bsid", "", "void"), 64);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBsmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 68);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBsmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "bsmod", "", "void"), 72);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAcmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 76);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAcmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "acmod", "", "void"), 80);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLfeon", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 84);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLfeon", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "lfeon", "", "void"), 88);
    }
}
