// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.piff;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TfxdBox extends AbstractFullBox
{
    public long fragmentAbsoluteTime;
    public long fragmentAbsoluteDuration;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public TfxdBox() {
        super("uuid");
    }
    
    @Override
    public byte[] getUserType() {
        return new byte[] { 109, 29, -101, 5, 66, -43, 68, -26, -128, -30, 20, 29, -81, -9, 87, -78 };
    }
    
    @Override
    protected long getContentSize() {
        return (this.getVersion() == 1) ? 20 : 12;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if (this.getVersion() == 1) {
            this.fragmentAbsoluteTime = IsoTypeReader.readUInt64(content);
            this.fragmentAbsoluteDuration = IsoTypeReader.readUInt64(content);
        }
        else {
            this.fragmentAbsoluteTime = IsoTypeReader.readUInt32(content);
            this.fragmentAbsoluteDuration = IsoTypeReader.readUInt32(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt64(bb, this.fragmentAbsoluteTime);
            IsoTypeWriter.writeUInt64(bb, this.fragmentAbsoluteDuration);
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.fragmentAbsoluteTime);
            IsoTypeWriter.writeUInt32(bb, this.fragmentAbsoluteDuration);
        }
    }
    
    public long getFragmentAbsoluteTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TfxdBox.ajc$tjp_0, this, this));
        return this.fragmentAbsoluteTime;
    }
    
    public long getFragmentAbsoluteDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TfxdBox.ajc$tjp_1, this, this));
        return this.fragmentAbsoluteDuration;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TfxdBox.java", TfxdBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFragmentAbsoluteTime", "com.googlecode.mp4parser.boxes.piff.TfxdBox", "", "", "", "long"), 79);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFragmentAbsoluteDuration", "com.googlecode.mp4parser.boxes.piff.TfxdBox", "", "", "", "long"), 83);
    }
}
