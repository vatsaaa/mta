// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.piff;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TfrfBox extends AbstractFullBox
{
    public List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public TfrfBox() {
        super("uuid");
        this.entries = new ArrayList<Entry>();
    }
    
    @Override
    public byte[] getUserType() {
        return new byte[] { -44, -128, 126, -14, -54, 57, 70, -107, -114, 84, 38, -53, -98, 70, -89, -97 };
    }
    
    @Override
    protected long getContentSize() {
        return 5 + this.entries.size() * ((this.getVersion() == 1) ? 16 : 8);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt8(bb, this.entries.size());
        for (final Entry entry : this.entries) {
            if (this.getVersion() == 1) {
                IsoTypeWriter.writeUInt64(bb, entry.fragmentAbsoluteTime);
                IsoTypeWriter.writeUInt64(bb, entry.fragmentAbsoluteDuration);
            }
            else {
                IsoTypeWriter.writeUInt32(bb, entry.fragmentAbsoluteTime);
                IsoTypeWriter.writeUInt32(bb, entry.fragmentAbsoluteDuration);
            }
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        for (int fragmentCount = IsoTypeReader.readUInt8(content), i = 0; i < fragmentCount; ++i) {
            final Entry entry = new Entry();
            if (this.getVersion() == 1) {
                entry.fragmentAbsoluteTime = IsoTypeReader.readUInt64(content);
                entry.fragmentAbsoluteDuration = IsoTypeReader.readUInt64(content);
            }
            else {
                entry.fragmentAbsoluteTime = IsoTypeReader.readUInt32(content);
                entry.fragmentAbsoluteDuration = IsoTypeReader.readUInt32(content);
            }
            this.entries.add(entry);
        }
    }
    
    public long getFragmentCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TfrfBox.ajc$tjp_0, this, this));
        return this.entries.size();
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TfrfBox.ajc$tjp_1, this, this));
        return this.entries;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TfrfBox.ajc$tjp_2, this, this));
        final StringBuilder sb = new StringBuilder();
        sb.append("TfrfBox");
        sb.append("{entries=").append(this.entries);
        sb.append('}');
        return sb.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TfrfBox.java", TfrfBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFragmentCount", "com.googlecode.mp4parser.boxes.piff.TfrfBox", "", "", "", "long"), 91);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.googlecode.mp4parser.boxes.piff.TfrfBox", "", "", "", "java.util.List"), 95);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.googlecode.mp4parser.boxes.piff.TfrfBox", "", "", "", "java.lang.String"), 100);
    }
    
    public class Entry
    {
        long fragmentAbsoluteTime;
        long fragmentAbsoluteDuration;
        
        public long getFragmentAbsoluteTime() {
            return this.fragmentAbsoluteTime;
        }
        
        public long getFragmentAbsoluteDuration() {
            return this.fragmentAbsoluteDuration;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Entry");
            sb.append("{fragmentAbsoluteTime=").append(this.fragmentAbsoluteTime);
            sb.append(", fragmentAbsoluteDuration=").append(this.fragmentAbsoluteDuration);
            sb.append('}');
            return sb.toString();
        }
    }
}
