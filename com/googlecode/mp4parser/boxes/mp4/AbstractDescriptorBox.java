// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4;

import org.aspectj.lang.Signature;
import java.util.logging.Level;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.ObjectDescriptorFactory;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import org.aspectj.lang.JoinPoint;
import java.nio.ByteBuffer;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BaseDescriptor;
import java.util.logging.Logger;
import com.coremedia.iso.boxes.AbstractFullBox;

public class AbstractDescriptorBox extends AbstractFullBox
{
    private static Logger log;
    public BaseDescriptor descriptor;
    public ByteBuffer data;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    static {
        ajc$preClinit();
        AbstractDescriptorBox.log = Logger.getLogger(AbstractDescriptorBox.class.getName());
    }
    
    public AbstractDescriptorBox(final String type) {
        super(type);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        this.data.rewind();
        bb.put(this.data);
    }
    
    @Override
    protected long getContentSize() {
        return 4 + this.data.limit();
    }
    
    public BaseDescriptor getDescriptor() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractDescriptorBox.ajc$tjp_0, this, this));
        return this.descriptor;
    }
    
    public String getDescriptorAsString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractDescriptorBox.ajc$tjp_1, this, this));
        return this.descriptor.toString();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.data = content.slice();
        content.position(content.position() + content.remaining());
        try {
            this.data.rewind();
            this.descriptor = ObjectDescriptorFactory.createFrom(-1, this.data);
        }
        catch (IOException e) {
            AbstractDescriptorBox.log.log(Level.WARNING, "Error parsing ObjectDescriptor", e);
        }
        catch (IndexOutOfBoundsException e2) {
            AbstractDescriptorBox.log.log(Level.WARNING, "Error parsing ObjectDescriptor", e2);
        }
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AbstractDescriptorBox.java", AbstractDescriptorBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDescriptor", "com.googlecode.mp4parser.boxes.mp4.AbstractDescriptorBox", "", "", "", "com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BaseDescriptor"), 54);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDescriptorAsString", "com.googlecode.mp4parser.boxes.mp4.AbstractDescriptorBox", "", "", "", "java.lang.String"), 58);
    }
}
