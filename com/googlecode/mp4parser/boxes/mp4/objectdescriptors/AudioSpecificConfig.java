// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.nio.ByteBuffer;

@Descriptor(tags = { 5 }, objectTypeIndication = 64)
public class AudioSpecificConfig extends BaseDescriptor
{
    ByteBuffer configBytes;
    long endPos;
    public static Map<Integer, Integer> samplingFrequencyIndexMap;
    public static Map<Integer, String> audioObjectTypeMap;
    int audioObjectType;
    int samplingFrequencyIndex;
    int samplingFrequency;
    int channelConfiguration;
    int extensionAudioObjectType;
    int sbrPresentFlag;
    int psPresentFlag;
    int extensionSamplingFrequencyIndex;
    int extensionSamplingFrequency;
    int extensionChannelConfiguration;
    int sacPayloadEmbedding;
    int fillBits;
    int epConfig;
    int directMapping;
    int syncExtensionType;
    int frameLengthFlag;
    int dependsOnCoreCoder;
    int coreCoderDelay;
    int extensionFlag;
    int layerNr;
    int numOfSubFrame;
    int layer_length;
    int aacSectionDataResilienceFlag;
    int aacScalefactorDataResilienceFlag;
    int aacSpectralDataResilienceFlag;
    int extensionFlag3;
    boolean gaSpecificConfig;
    int isBaseLayer;
    int paraMode;
    int paraExtensionFlag;
    int hvxcVarMode;
    int hvxcRateMode;
    int erHvxcExtensionFlag;
    int var_ScalableFlag;
    int hilnQuantMode;
    int hilnMaxNumLine;
    int hilnSampleRateCode;
    int hilnFrameLength;
    int hilnContMode;
    int hilnEnhaLayer;
    int hilnEnhaQuantMode;
    boolean parametricSpecificConfig;
    
    static {
        AudioSpecificConfig.samplingFrequencyIndexMap = new HashMap<Integer, Integer>();
        AudioSpecificConfig.audioObjectTypeMap = new HashMap<Integer, String>();
        AudioSpecificConfig.samplingFrequencyIndexMap.put(0, 96000);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(1, 88200);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(2, 64000);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(3, 48000);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(4, 44100);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(5, 32000);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(6, 24000);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(7, 22050);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(8, 16000);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(9, 12000);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(10, 11025);
        AudioSpecificConfig.samplingFrequencyIndexMap.put(11, 8000);
        AudioSpecificConfig.audioObjectTypeMap.put(1, "AAC main");
        AudioSpecificConfig.audioObjectTypeMap.put(2, "AAC LC");
        AudioSpecificConfig.audioObjectTypeMap.put(3, "AAC SSR");
        AudioSpecificConfig.audioObjectTypeMap.put(4, "AAC LTP");
        AudioSpecificConfig.audioObjectTypeMap.put(5, "SBR");
        AudioSpecificConfig.audioObjectTypeMap.put(6, "AAC Scalable");
        AudioSpecificConfig.audioObjectTypeMap.put(7, "TwinVQ");
        AudioSpecificConfig.audioObjectTypeMap.put(8, "CELP");
        AudioSpecificConfig.audioObjectTypeMap.put(9, "HVXC");
        AudioSpecificConfig.audioObjectTypeMap.put(10, "(reserved)");
        AudioSpecificConfig.audioObjectTypeMap.put(11, "(reserved)");
        AudioSpecificConfig.audioObjectTypeMap.put(12, "TTSI");
        AudioSpecificConfig.audioObjectTypeMap.put(13, "Main synthetic");
        AudioSpecificConfig.audioObjectTypeMap.put(14, "Wavetable synthesis");
        AudioSpecificConfig.audioObjectTypeMap.put(15, "General MIDI");
        AudioSpecificConfig.audioObjectTypeMap.put(16, "Algorithmic Synthesis and Audio FX");
        AudioSpecificConfig.audioObjectTypeMap.put(17, "ER AAC LC");
        AudioSpecificConfig.audioObjectTypeMap.put(18, "(reserved)");
        AudioSpecificConfig.audioObjectTypeMap.put(19, "ER AAC LTP");
        AudioSpecificConfig.audioObjectTypeMap.put(20, "ER AAC Scalable");
        AudioSpecificConfig.audioObjectTypeMap.put(21, "ER TwinVQ");
        AudioSpecificConfig.audioObjectTypeMap.put(22, "ER BSAC");
        AudioSpecificConfig.audioObjectTypeMap.put(23, "ER AAC LD");
        AudioSpecificConfig.audioObjectTypeMap.put(24, "ER CELP");
        AudioSpecificConfig.audioObjectTypeMap.put(25, "ER HVXC");
        AudioSpecificConfig.audioObjectTypeMap.put(26, "ER HILN");
        AudioSpecificConfig.audioObjectTypeMap.put(27, "ER Parametric");
        AudioSpecificConfig.audioObjectTypeMap.put(28, "SSC");
        AudioSpecificConfig.audioObjectTypeMap.put(29, "PS");
        AudioSpecificConfig.audioObjectTypeMap.put(30, "MPEG Surround");
        AudioSpecificConfig.audioObjectTypeMap.put(31, "(escape)");
        AudioSpecificConfig.audioObjectTypeMap.put(32, "Layer-1");
        AudioSpecificConfig.audioObjectTypeMap.put(33, "Layer-2");
        AudioSpecificConfig.audioObjectTypeMap.put(34, "Layer-3");
        AudioSpecificConfig.audioObjectTypeMap.put(35, "DST");
        AudioSpecificConfig.audioObjectTypeMap.put(36, "ALS");
        AudioSpecificConfig.audioObjectTypeMap.put(37, "SLS");
        AudioSpecificConfig.audioObjectTypeMap.put(38, "SLS non-core");
        AudioSpecificConfig.audioObjectTypeMap.put(39, "ER AAC ELD");
        AudioSpecificConfig.audioObjectTypeMap.put(40, "SMR Simple");
        AudioSpecificConfig.audioObjectTypeMap.put(41, "SMR Main");
    }
    
    @Override
    public void parseDetail(final ByteBuffer bb) throws IOException {
        (this.configBytes = bb.slice()).limit(this.sizeOfInstance);
        bb.position(bb.position() + this.sizeOfInstance);
        final BitReaderBuffer bitReaderBuffer = new BitReaderBuffer(this.configBytes);
        this.audioObjectType = this.getAudioObjectType(bitReaderBuffer);
        this.samplingFrequencyIndex = bitReaderBuffer.readBits(4);
        if (this.samplingFrequencyIndex == 15) {
            this.samplingFrequency = bitReaderBuffer.readBits(24);
        }
        this.channelConfiguration = bitReaderBuffer.readBits(4);
        if (this.audioObjectType == 5 || this.audioObjectType == 29) {
            this.extensionAudioObjectType = 5;
            this.sbrPresentFlag = 1;
            if (this.audioObjectType == 29) {
                this.psPresentFlag = 1;
            }
            this.extensionSamplingFrequencyIndex = bitReaderBuffer.readBits(4);
            if (this.extensionSamplingFrequencyIndex == 15) {
                this.extensionSamplingFrequency = bitReaderBuffer.readBits(24);
            }
            this.audioObjectType = this.getAudioObjectType(bitReaderBuffer);
            if (this.audioObjectType == 22) {
                this.extensionChannelConfiguration = bitReaderBuffer.readBits(4);
            }
        }
        else {
            this.extensionAudioObjectType = 0;
        }
        switch (this.audioObjectType) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 7:
            case 17:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23: {
                this.parseGaSpecificConfig(this.samplingFrequencyIndex, this.channelConfiguration, this.audioObjectType, bitReaderBuffer);
                break;
            }
            case 8: {
                throw new UnsupportedOperationException("can't parse CelpSpecificConfig yet");
            }
            case 9: {
                throw new UnsupportedOperationException("can't parse HvxcSpecificConfig yet");
            }
            case 12: {
                throw new UnsupportedOperationException("can't parse TTSSpecificConfig yet");
            }
            case 13:
            case 14:
            case 15:
            case 16: {
                throw new UnsupportedOperationException("can't parse StructuredAudioSpecificConfig yet");
            }
            case 24: {
                throw new UnsupportedOperationException("can't parse ErrorResilientCelpSpecificConfig yet");
            }
            case 25: {
                throw new UnsupportedOperationException("can't parse ErrorResilientHvxcSpecificConfig yet");
            }
            case 26:
            case 27: {
                this.parseParametricSpecificConfig(this.samplingFrequencyIndex, this.channelConfiguration, this.audioObjectType, bitReaderBuffer);
                break;
            }
            case 28: {
                throw new UnsupportedOperationException("can't parse SSCSpecificConfig yet");
            }
            case 30: {
                this.sacPayloadEmbedding = bitReaderBuffer.readBits(1);
                throw new UnsupportedOperationException("can't parse SpatialSpecificConfig yet");
            }
            case 32:
            case 33:
            case 34: {
                throw new UnsupportedOperationException("can't parse MPEG_1_2_SpecificConfig yet");
            }
            case 35: {
                throw new UnsupportedOperationException("can't parse DSTSpecificConfig yet");
            }
            case 36: {
                this.fillBits = bitReaderBuffer.readBits(5);
                throw new UnsupportedOperationException("can't parse ALSSpecificConfig yet");
            }
            case 37:
            case 38: {
                throw new UnsupportedOperationException("can't parse SLSSpecificConfig yet");
            }
            case 39: {
                throw new UnsupportedOperationException("can't parse ELDSpecificConfig yet");
            }
            case 40:
            case 41: {
                throw new UnsupportedOperationException("can't parse SymbolicMusicSpecificConfig yet");
            }
        }
        switch (this.audioObjectType) {
            case 17:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 39: {
                this.epConfig = bitReaderBuffer.readBits(2);
                if (this.epConfig == 2 || this.epConfig == 3) {
                    throw new UnsupportedOperationException("can't parse ErrorProtectionSpecificConfig yet");
                }
                if (this.epConfig != 3) {
                    break;
                }
                this.directMapping = bitReaderBuffer.readBits(1);
                if (this.directMapping == 0) {
                    throw new RuntimeException("not implemented");
                }
                break;
            }
        }
        if (this.extensionAudioObjectType != 5 && bitReaderBuffer.remainingBits() >= 16) {
            this.syncExtensionType = bitReaderBuffer.readBits(11);
            if (this.syncExtensionType == 695) {
                this.extensionAudioObjectType = this.getAudioObjectType(bitReaderBuffer);
                if (this.extensionAudioObjectType == 5) {
                    this.sbrPresentFlag = bitReaderBuffer.readBits(1);
                    if (this.sbrPresentFlag == 1) {
                        this.extensionSamplingFrequencyIndex = bitReaderBuffer.readBits(4);
                        if (this.extensionSamplingFrequencyIndex == 15) {
                            this.extensionSamplingFrequency = bitReaderBuffer.readBits(24);
                        }
                        if (bitReaderBuffer.remainingBits() >= 12) {
                            this.syncExtensionType = bitReaderBuffer.readBits(11);
                            if (this.syncExtensionType == 1352) {
                                this.psPresentFlag = bitReaderBuffer.readBits(1);
                            }
                        }
                    }
                }
                if (this.extensionAudioObjectType == 22) {
                    this.sbrPresentFlag = bitReaderBuffer.readBits(1);
                    if (this.sbrPresentFlag == 1) {
                        this.extensionSamplingFrequencyIndex = bitReaderBuffer.readBits(4);
                        if (this.extensionSamplingFrequencyIndex == 15) {
                            this.extensionSamplingFrequency = bitReaderBuffer.readBits(24);
                        }
                    }
                    this.extensionChannelConfiguration = bitReaderBuffer.readBits(4);
                }
            }
        }
    }
    
    private int getAudioObjectType(final BitReaderBuffer in) throws IOException {
        int audioObjectType = in.readBits(5);
        if (audioObjectType == 31) {
            audioObjectType = 32 + in.readBits(6);
        }
        return audioObjectType;
    }
    
    private void parseGaSpecificConfig(final int samplingFrequencyIndex, final int channelConfiguration, final int audioObjectType, final BitReaderBuffer in) throws IOException {
        this.frameLengthFlag = in.readBits(1);
        this.dependsOnCoreCoder = in.readBits(1);
        if (this.dependsOnCoreCoder == 1) {
            this.coreCoderDelay = in.readBits(14);
        }
        this.extensionFlag = in.readBits(1);
        if (channelConfiguration == 0) {
            throw new UnsupportedOperationException("can't parse program_config_element yet");
        }
        if (audioObjectType == 6 || audioObjectType == 20) {
            this.layerNr = in.readBits(3);
        }
        if (this.extensionFlag == 1) {
            if (audioObjectType == 22) {
                this.numOfSubFrame = in.readBits(5);
                this.layer_length = in.readBits(11);
            }
            if (audioObjectType == 17 || audioObjectType == 19 || audioObjectType == 20 || audioObjectType == 23) {
                this.aacSectionDataResilienceFlag = in.readBits(1);
                this.aacScalefactorDataResilienceFlag = in.readBits(1);
                this.aacSpectralDataResilienceFlag = in.readBits(1);
            }
            this.extensionFlag3 = in.readBits(1);
        }
        this.gaSpecificConfig = true;
    }
    
    private void parseParametricSpecificConfig(final int samplingFrequencyIndex, final int channelConfiguration, final int audioObjectType, final BitReaderBuffer in) throws IOException {
        this.isBaseLayer = in.readBits(1);
        if (this.isBaseLayer == 1) {
            this.parseParaConfig(samplingFrequencyIndex, channelConfiguration, audioObjectType, in);
        }
        else {
            this.parseHilnEnexConfig(samplingFrequencyIndex, channelConfiguration, audioObjectType, in);
        }
    }
    
    private void parseParaConfig(final int samplingFrequencyIndex, final int channelConfiguration, final int audioObjectType, final BitReaderBuffer in) throws IOException {
        this.paraMode = in.readBits(2);
        if (this.paraMode != 1) {
            this.parseErHvxcConfig(samplingFrequencyIndex, channelConfiguration, audioObjectType, in);
        }
        if (this.paraMode != 0) {
            this.parseHilnConfig(samplingFrequencyIndex, channelConfiguration, audioObjectType, in);
        }
        this.paraExtensionFlag = in.readBits(1);
        this.parametricSpecificConfig = true;
    }
    
    private void parseErHvxcConfig(final int samplingFrequencyIndex, final int channelConfiguration, final int audioObjectType, final BitReaderBuffer in) throws IOException {
        this.hvxcVarMode = in.readBits(1);
        this.hvxcRateMode = in.readBits(2);
        this.erHvxcExtensionFlag = in.readBits(1);
        if (this.erHvxcExtensionFlag == 1) {
            this.var_ScalableFlag = in.readBits(1);
        }
    }
    
    private void parseHilnConfig(final int samplingFrequencyIndex, final int channelConfiguration, final int audioObjectType, final BitReaderBuffer in) throws IOException {
        this.hilnQuantMode = in.readBits(1);
        this.hilnMaxNumLine = in.readBits(8);
        this.hilnSampleRateCode = in.readBits(4);
        this.hilnFrameLength = in.readBits(12);
        this.hilnContMode = in.readBits(2);
    }
    
    private void parseHilnEnexConfig(final int samplingFrequencyIndex, final int channelConfiguration, final int audioObjectType, final BitReaderBuffer in) throws IOException {
        this.hilnEnhaLayer = in.readBits(1);
        if (this.hilnEnhaLayer == 1) {
            this.hilnEnhaQuantMode = in.readBits(2);
        }
    }
    
    public ByteBuffer getConfigBytes() {
        return this.configBytes;
    }
    
    public int getAudioObjectType() {
        return this.audioObjectType;
    }
    
    public int getExtensionAudioObjectType() {
        return this.extensionAudioObjectType;
    }
    
    public int getSbrPresentFlag() {
        return this.sbrPresentFlag;
    }
    
    public int getPsPresentFlag() {
        return this.psPresentFlag;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AudioSpecificConfig");
        sb.append("{configBytes=").append(this.configBytes);
        sb.append(", audioObjectType=").append(this.audioObjectType).append(" (").append(AudioSpecificConfig.audioObjectTypeMap.get(this.audioObjectType)).append(")");
        sb.append(", samplingFrequencyIndex=").append(this.samplingFrequencyIndex).append(" (").append(AudioSpecificConfig.samplingFrequencyIndexMap.get(this.samplingFrequencyIndex)).append(")");
        sb.append(", samplingFrequency=").append(this.samplingFrequency);
        sb.append(", channelConfiguration=").append(this.channelConfiguration);
        if (this.extensionAudioObjectType > 0) {
            sb.append(", extensionAudioObjectType=").append(this.extensionAudioObjectType).append(" (").append(AudioSpecificConfig.audioObjectTypeMap.get(this.extensionAudioObjectType)).append(")");
            sb.append(", sbrPresentFlag=").append(this.sbrPresentFlag);
            sb.append(", psPresentFlag=").append(this.psPresentFlag);
            sb.append(", extensionSamplingFrequencyIndex=").append(this.extensionSamplingFrequencyIndex).append(" (").append(AudioSpecificConfig.samplingFrequencyIndexMap.get(this.extensionSamplingFrequencyIndex)).append(")");
            sb.append(", extensionSamplingFrequency=").append(this.extensionSamplingFrequency);
            sb.append(", extensionChannelConfiguration=").append(this.extensionChannelConfiguration);
        }
        sb.append(", syncExtensionType=").append(this.syncExtensionType);
        if (this.gaSpecificConfig) {
            sb.append(", frameLengthFlag=").append(this.frameLengthFlag);
            sb.append(", dependsOnCoreCoder=").append(this.dependsOnCoreCoder);
            sb.append(", coreCoderDelay=").append(this.coreCoderDelay);
            sb.append(", extensionFlag=").append(this.extensionFlag);
            sb.append(", layerNr=").append(this.layerNr);
            sb.append(", numOfSubFrame=").append(this.numOfSubFrame);
            sb.append(", layer_length=").append(this.layer_length);
            sb.append(", aacSectionDataResilienceFlag=").append(this.aacSectionDataResilienceFlag);
            sb.append(", aacScalefactorDataResilienceFlag=").append(this.aacScalefactorDataResilienceFlag);
            sb.append(", aacSpectralDataResilienceFlag=").append(this.aacSpectralDataResilienceFlag);
            sb.append(", extensionFlag3=").append(this.extensionFlag3);
        }
        if (this.parametricSpecificConfig) {
            sb.append(", isBaseLayer=").append(this.isBaseLayer);
            sb.append(", paraMode=").append(this.paraMode);
            sb.append(", paraExtensionFlag=").append(this.paraExtensionFlag);
            sb.append(", hvxcVarMode=").append(this.hvxcVarMode);
            sb.append(", hvxcRateMode=").append(this.hvxcRateMode);
            sb.append(", erHvxcExtensionFlag=").append(this.erHvxcExtensionFlag);
            sb.append(", var_ScalableFlag=").append(this.var_ScalableFlag);
            sb.append(", hilnQuantMode=").append(this.hilnQuantMode);
            sb.append(", hilnMaxNumLine=").append(this.hilnMaxNumLine);
            sb.append(", hilnSampleRateCode=").append(this.hilnSampleRateCode);
            sb.append(", hilnFrameLength=").append(this.hilnFrameLength);
            sb.append(", hilnContMode=").append(this.hilnContMode);
            sb.append(", hilnEnhaLayer=").append(this.hilnEnhaLayer);
            sb.append(", hilnEnhaQuantMode=").append(this.hilnEnhaQuantMode);
        }
        sb.append('}');
        return sb.toString();
    }
    
    public int getSamplingFrequency() {
        return (this.samplingFrequencyIndex == 15) ? this.samplingFrequency : AudioSpecificConfig.samplingFrequencyIndexMap.get(this.samplingFrequencyIndex);
    }
    
    public int getChannelConfiguration() {
        return this.channelConfiguration;
    }
}
