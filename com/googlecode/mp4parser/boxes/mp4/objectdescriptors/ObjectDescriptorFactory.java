// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.io.IOException;
import java.util.logging.Level;
import java.lang.reflect.Modifier;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class ObjectDescriptorFactory
{
    protected static Logger log;
    protected static Map<Integer, Map<Integer, Class<? extends BaseDescriptor>>> descriptorRegistry;
    
    static {
        ObjectDescriptorFactory.log = Logger.getLogger(ObjectDescriptorFactory.class.getName());
        ObjectDescriptorFactory.descriptorRegistry = new HashMap<Integer, Map<Integer, Class<? extends BaseDescriptor>>>();
        final Set<Class<? extends BaseDescriptor>> annotated = new HashSet<Class<? extends BaseDescriptor>>();
        annotated.add(DecoderSpecificInfo.class);
        annotated.add(SLConfigDescriptor.class);
        annotated.add(BaseDescriptor.class);
        annotated.add(ExtensionDescriptor.class);
        annotated.add(ObjectDescriptorBase.class);
        annotated.add(ProfileLevelIndicationDescriptor.class);
        annotated.add(AudioSpecificConfig.class);
        annotated.add(ExtensionProfileLevelDescriptor.class);
        annotated.add(ESDescriptor.class);
        annotated.add(DecoderConfigDescriptor.class);
        for (final Class<? extends BaseDescriptor> clazz : annotated) {
            final Descriptor descriptor = clazz.getAnnotation(Descriptor.class);
            final int[] tags = descriptor.tags();
            final int objectTypeInd = descriptor.objectTypeIndication();
            Map<Integer, Class<? extends BaseDescriptor>> tagMap = ObjectDescriptorFactory.descriptorRegistry.get(objectTypeInd);
            if (tagMap == null) {
                tagMap = new HashMap<Integer, Class<? extends BaseDescriptor>>();
            }
            int[] array;
            for (int length = (array = tags).length, i = 0; i < length; ++i) {
                final int tag = array[i];
                tagMap.put(tag, clazz);
            }
            ObjectDescriptorFactory.descriptorRegistry.put(objectTypeInd, tagMap);
        }
    }
    
    public static BaseDescriptor createFrom(final int objectTypeIndication, final ByteBuffer bb) throws IOException {
        final int tag = IsoTypeReader.readUInt8(bb);
        Map<Integer, Class<? extends BaseDescriptor>> tagMap = ObjectDescriptorFactory.descriptorRegistry.get(objectTypeIndication);
        if (tagMap == null) {
            tagMap = ObjectDescriptorFactory.descriptorRegistry.get(-1);
        }
        final Class<? extends BaseDescriptor> aClass = tagMap.get(tag);
        BaseDescriptor baseDescriptor;
        if (aClass == null || aClass.isInterface() || Modifier.isAbstract(aClass.getModifiers())) {
            ObjectDescriptorFactory.log.warning("No ObjectDescriptor found for objectTypeIndication " + Integer.toHexString(objectTypeIndication) + " and tag " + Integer.toHexString(tag) + " found: " + aClass);
            baseDescriptor = new UnknownDescriptor();
        }
        else {
            try {
                baseDescriptor = (BaseDescriptor)aClass.newInstance();
            }
            catch (Exception e) {
                ObjectDescriptorFactory.log.log(Level.SEVERE, "Couldn't instantiate BaseDescriptor class " + aClass + " for objectTypeIndication " + objectTypeIndication + " and tag " + tag, e);
                throw new RuntimeException(e);
            }
        }
        baseDescriptor.parse(tag, bb);
        return baseDescriptor;
    }
}
