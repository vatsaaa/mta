// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import com.coremedia.iso.Hex;
import java.io.IOException;
import java.nio.ByteBuffer;

@Descriptor(tags = { 19 })
public class ExtensionProfileLevelDescriptor extends BaseDescriptor
{
    byte[] bytes;
    
    @Override
    public void parseDetail(final ByteBuffer bb) throws IOException {
        if (this.getSize() > 0) {
            bb.get(this.bytes = new byte[this.getSize()]);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ExtensionDescriptor");
        sb.append("{bytes=").append((this.bytes == null) ? "null" : Hex.encodeHex(this.bytes));
        sb.append('}');
        return sb.toString();
    }
}
