// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.io.IOException;
import java.util.logging.Logger;
import java.nio.ByteBuffer;

public class UnknownDescriptor extends BaseDescriptor
{
    private ByteBuffer data;
    private static Logger log;
    
    static {
        UnknownDescriptor.log = Logger.getLogger(UnknownDescriptor.class.getName());
    }
    
    @Override
    public void parseDetail(final ByteBuffer bb) throws IOException {
        this.data = (ByteBuffer)bb.slice().limit(this.getSizeOfInstance());
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("UnknownDescriptor");
        sb.append("{tag=").append(this.tag);
        sb.append(", sizeOfInstance=").append(this.sizeOfInstance);
        sb.append(", data=").append(this.data);
        sb.append('}');
        return sb.toString();
    }
}
