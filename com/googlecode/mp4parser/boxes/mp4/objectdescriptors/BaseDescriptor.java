// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;

@Descriptor(tags = { 0 })
public abstract class BaseDescriptor
{
    int tag;
    int sizeOfInstance;
    int sizeBytes;
    
    public int getTag() {
        return this.tag;
    }
    
    public int getSize() {
        return this.sizeOfInstance + 1 + this.sizeBytes;
    }
    
    public int getSizeOfInstance() {
        return this.sizeOfInstance;
    }
    
    public int getSizeBytes() {
        return this.sizeBytes;
    }
    
    public final void parse(final int tag, final ByteBuffer bb) throws IOException {
        this.tag = tag;
        int i = 0;
        int tmp = IsoTypeReader.readUInt8(bb);
        ++i;
        this.sizeOfInstance = (tmp & 0x7F);
        while (tmp >>> 7 == 1) {
            tmp = IsoTypeReader.readUInt8(bb);
            ++i;
            this.sizeOfInstance = (this.sizeOfInstance << 7 | (tmp & 0x7F));
        }
        this.sizeBytes = i;
        final ByteBuffer detailSource = bb.slice();
        detailSource.limit(this.sizeOfInstance);
        this.parseDetail(detailSource);
        assert detailSource.remaining() == 0 : String.valueOf(this.getClass().getSimpleName()) + " has not been fully parsed";
        bb.position(bb.position() + this.sizeOfInstance);
    }
    
    public abstract void parseDetail(final ByteBuffer p0) throws IOException;
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BaseDescriptor");
        sb.append("{tag=").append(this.tag);
        sb.append(", sizeOfInstance=").append(this.sizeOfInstance);
        sb.append('}');
        return sb.toString();
    }
}
