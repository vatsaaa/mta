// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.util.Arrays;
import com.coremedia.iso.Hex;
import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Descriptor(tags = { 4 })
public class DecoderConfigDescriptor extends BaseDescriptor
{
    private static Logger log;
    int objectTypeIndication;
    int streamType;
    int upStream;
    int bufferSizeDB;
    long maxBitRate;
    long avgBitRate;
    DecoderSpecificInfo decoderSpecificInfo;
    AudioSpecificConfig audioSpecificInfo;
    List<ProfileLevelIndicationDescriptor> profileLevelIndicationDescriptors;
    byte[] configDescriptorDeadBytes;
    
    static {
        DecoderConfigDescriptor.log = Logger.getLogger(DecoderConfigDescriptor.class.getName());
    }
    
    public DecoderConfigDescriptor() {
        this.profileLevelIndicationDescriptors = new ArrayList<ProfileLevelIndicationDescriptor>();
    }
    
    @Override
    public void parseDetail(final ByteBuffer bb) throws IOException {
        this.objectTypeIndication = IsoTypeReader.readUInt8(bb);
        final int data = IsoTypeReader.readUInt8(bb);
        this.streamType = data >>> 2;
        this.upStream = (data >> 1 & 0x1);
        this.bufferSizeDB = IsoTypeReader.readUInt24(bb);
        this.maxBitRate = IsoTypeReader.readUInt32(bb);
        this.avgBitRate = IsoTypeReader.readUInt32(bb);
        if (bb.remaining() > 2) {
            final int begin = bb.position();
            final BaseDescriptor descriptor = ObjectDescriptorFactory.createFrom(this.objectTypeIndication, bb);
            final int read = bb.position() - begin;
            DecoderConfigDescriptor.log.finer(descriptor + " - DecoderConfigDescr1 read: " + read + ", size: " + ((descriptor != null) ? Integer.valueOf(descriptor.getSize()) : null));
            if (descriptor != null) {
                final int size = descriptor.getSize();
                if (read < size) {
                    bb.get(this.configDescriptorDeadBytes = new byte[size - read]);
                }
            }
            if (descriptor instanceof DecoderSpecificInfo) {
                this.decoderSpecificInfo = (DecoderSpecificInfo)descriptor;
            }
            if (descriptor instanceof AudioSpecificConfig) {
                this.audioSpecificInfo = (AudioSpecificConfig)descriptor;
            }
        }
        while (bb.remaining() > 2) {
            final long begin2 = bb.position();
            final BaseDescriptor descriptor = ObjectDescriptorFactory.createFrom(this.objectTypeIndication, bb);
            final long read2 = bb.position() - begin2;
            DecoderConfigDescriptor.log.finer(descriptor + " - DecoderConfigDescr2 read: " + read2 + ", size: " + ((descriptor != null) ? Integer.valueOf(descriptor.getSize()) : null));
            if (descriptor instanceof ProfileLevelIndicationDescriptor) {
                this.profileLevelIndicationDescriptors.add((ProfileLevelIndicationDescriptor)descriptor);
            }
        }
    }
    
    public DecoderSpecificInfo getDecoderSpecificInfo() {
        return this.decoderSpecificInfo;
    }
    
    public AudioSpecificConfig getAudioSpecificInfo() {
        return this.audioSpecificInfo;
    }
    
    public List<ProfileLevelIndicationDescriptor> getProfileLevelIndicationDescriptors() {
        return this.profileLevelIndicationDescriptors;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DecoderConfigDescriptor");
        sb.append("{objectTypeIndication=").append(this.objectTypeIndication);
        sb.append(", streamType=").append(this.streamType);
        sb.append(", upStream=").append(this.upStream);
        sb.append(", bufferSizeDB=").append(this.bufferSizeDB);
        sb.append(", maxBitRate=").append(this.maxBitRate);
        sb.append(", avgBitRate=").append(this.avgBitRate);
        sb.append(", decoderSpecificInfo=").append(this.decoderSpecificInfo);
        sb.append(", audioSpecificInfo=").append(this.audioSpecificInfo);
        sb.append(", configDescriptorDeadBytes=").append(Hex.encodeHex((this.configDescriptorDeadBytes != null) ? this.configDescriptorDeadBytes : new byte[0]));
        sb.append(", profileLevelIndicationDescriptors=").append((this.profileLevelIndicationDescriptors == null) ? "null" : Arrays.asList(this.profileLevelIndicationDescriptors).toString());
        sb.append('}');
        return sb.toString();
    }
}
