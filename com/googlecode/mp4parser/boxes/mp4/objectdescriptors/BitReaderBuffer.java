// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.nio.ByteBuffer;

public class BitReaderBuffer
{
    private ByteBuffer buffer;
    int initialPos;
    int position;
    int readCount;
    
    public BitReaderBuffer(final ByteBuffer buffer) {
        this.buffer = buffer;
        this.initialPos = buffer.position();
    }
    
    public int readBits(final int i) {
        this.readCount += i;
        final byte b = this.buffer.get(this.initialPos + this.position / 8);
        final int v = (b < 0) ? (b + 256) : b;
        final int left = 8 - this.position % 8;
        int rc;
        if (i <= left) {
            rc = (v << this.position % 8 & 0xFF) >> this.position % 8 + (left - i);
            this.position += i;
        }
        else {
            final int now = left;
            final int then = i - left;
            rc = this.readBits(now);
            rc <<= then;
            rc += this.readBits(then);
        }
        this.buffer.position(this.initialPos + (int)Math.ceil(this.position / 8.0));
        return rc;
    }
    
    public int byteSync() {
        int left = 8 - this.readCount % 8;
        if (left == 8) {
            left = 0;
        }
        this.readBits(left);
        return left;
    }
    
    public int remainingBits() {
        return this.buffer.limit() * 8 - this.position;
    }
}
