// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Descriptor(tags = { 3 })
public class ESDescriptor extends BaseDescriptor
{
    private static Logger log;
    int esId;
    int streamDependenceFlag;
    int URLFlag;
    int oCRstreamFlag;
    int streamPriority;
    int URLLength;
    String URLString;
    int remoteODFlag;
    int dependsOnEsId;
    int oCREsId;
    DecoderConfigDescriptor decoderConfigDescriptor;
    SLConfigDescriptor slConfigDescriptor;
    List<BaseDescriptor> otherDescriptors;
    
    static {
        ESDescriptor.log = Logger.getLogger(ESDescriptor.class.getName());
    }
    
    public ESDescriptor() {
        this.URLLength = 0;
        this.otherDescriptors = new ArrayList<BaseDescriptor>();
    }
    
    @Override
    public void parseDetail(final ByteBuffer bb) throws IOException {
        this.esId = IsoTypeReader.readUInt16(bb);
        final int data = IsoTypeReader.readUInt8(bb);
        this.streamDependenceFlag = data >>> 7;
        this.URLFlag = (data >>> 6 & 0x1);
        this.oCRstreamFlag = (data >>> 5 & 0x1);
        this.streamPriority = (data & 0x1F);
        if (this.streamDependenceFlag == 1) {
            this.dependsOnEsId = IsoTypeReader.readUInt16(bb);
        }
        if (this.URLFlag == 1) {
            this.URLLength = IsoTypeReader.readUInt8(bb);
            this.URLString = IsoTypeReader.readString(bb, this.URLLength);
        }
        if (this.oCRstreamFlag == 1) {
            this.oCREsId = IsoTypeReader.readUInt16(bb);
        }
        int baseSize = 1 + this.getSizeBytes() + 2 + 1 + ((this.streamDependenceFlag == 1) ? 2 : 0) + ((this.URLFlag == 1) ? (1 + this.URLLength) : 0) + ((this.oCRstreamFlag == 1) ? 2 : 0);
        int begin = bb.position();
        if (this.getSize() > baseSize + 2) {
            final BaseDescriptor descriptor = ObjectDescriptorFactory.createFrom(-1, bb);
            final long read = bb.position() - begin;
            ESDescriptor.log.finer(descriptor + " - ESDescriptor1 read: " + read + ", size: " + ((descriptor != null) ? Integer.valueOf(descriptor.getSize()) : null));
            if (descriptor != null) {
                final int size = descriptor.getSize();
                bb.position(begin + size);
                baseSize += size;
            }
            else {
                baseSize += (int)read;
            }
            if (descriptor instanceof DecoderConfigDescriptor) {
                this.decoderConfigDescriptor = (DecoderConfigDescriptor)descriptor;
            }
        }
        begin = bb.position();
        if (this.getSize() > baseSize + 2) {
            final BaseDescriptor descriptor = ObjectDescriptorFactory.createFrom(-1, bb);
            final long read = bb.position() - begin;
            ESDescriptor.log.finer(descriptor + " - ESDescriptor2 read: " + read + ", size: " + ((descriptor != null) ? Integer.valueOf(descriptor.getSize()) : null));
            if (descriptor != null) {
                final int size = descriptor.getSize();
                bb.position(begin + size);
                baseSize += size;
            }
            else {
                baseSize += (int)read;
            }
            if (descriptor instanceof SLConfigDescriptor) {
                this.slConfigDescriptor = (SLConfigDescriptor)descriptor;
            }
        }
        else {
            ESDescriptor.log.warning("SLConfigDescriptor is missing!");
        }
        while (this.getSize() - baseSize > 2) {
            begin = bb.position();
            final BaseDescriptor descriptor = ObjectDescriptorFactory.createFrom(-1, bb);
            final long read = bb.position() - begin;
            ESDescriptor.log.finer(descriptor + " - ESDescriptor3 read: " + read + ", size: " + ((descriptor != null) ? Integer.valueOf(descriptor.getSize()) : null));
            if (descriptor != null) {
                final int size = descriptor.getSize();
                bb.position(begin + size);
                baseSize += size;
            }
            else {
                baseSize += (int)read;
            }
            this.otherDescriptors.add(descriptor);
        }
    }
    
    public DecoderConfigDescriptor getDecoderConfigDescriptor() {
        return this.decoderConfigDescriptor;
    }
    
    public SLConfigDescriptor getSlConfigDescriptor() {
        return this.slConfigDescriptor;
    }
    
    public List<BaseDescriptor> getOtherDescriptors() {
        return this.otherDescriptors;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ESDescriptor");
        sb.append("{esId=").append(this.esId);
        sb.append(", streamDependenceFlag=").append(this.streamDependenceFlag);
        sb.append(", URLFlag=").append(this.URLFlag);
        sb.append(", oCRstreamFlag=").append(this.oCRstreamFlag);
        sb.append(", streamPriority=").append(this.streamPriority);
        sb.append(", URLLength=").append(this.URLLength);
        sb.append(", URLString='").append(this.URLString).append('\'');
        sb.append(", remoteODFlag=").append(this.remoteODFlag);
        sb.append(", dependsOnEsId=").append(this.dependsOnEsId);
        sb.append(", oCREsId=").append(this.oCREsId);
        sb.append(", decoderConfigDescriptor=").append(this.decoderConfigDescriptor);
        sb.append(", slConfigDescriptor=").append(this.slConfigDescriptor);
        sb.append('}');
        return sb.toString();
    }
}
