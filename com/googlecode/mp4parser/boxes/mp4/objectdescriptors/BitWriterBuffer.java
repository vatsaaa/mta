// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.nio.ByteBuffer;

public class BitWriterBuffer
{
    private ByteBuffer buffer;
    int initialPos;
    int position;
    
    public BitWriterBuffer(final ByteBuffer buffer) {
        this.position = 0;
        this.buffer = buffer;
        this.initialPos = buffer.position();
    }
    
    public void writeBits(final int i, final int numBits) {
        final int left = 8 - this.position % 8;
        if (numBits <= left) {
            int current = this.buffer.get(this.initialPos + this.position / 8);
            current = ((current < 0) ? (current + 256) : current);
            current += i << left - numBits;
            this.buffer.put(this.initialPos + this.position / 8, (byte)((current > 127) ? (current - 256) : current));
            this.position += numBits;
        }
        else {
            final int bitsSecondWrite = numBits - left;
            this.writeBits(i >> bitsSecondWrite, left);
            this.writeBits(i & (1 << bitsSecondWrite) - 1, bitsSecondWrite);
        }
        this.buffer.position(this.initialPos + this.position / 8 + ((this.position % 8 > 0) ? 1 : 0));
    }
}
