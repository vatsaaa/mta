// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.mp4.objectdescriptors;

import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class InitialObjectDescriptor extends ObjectDescriptorBase
{
    private int objectDescriptorId;
    int urlFlag;
    int includeInlineProfileLevelFlag;
    int urlLength;
    String urlString;
    int oDProfileLevelIndication;
    int sceneProfileLevelIndication;
    int audioProfileLevelIndication;
    int visualProfileLevelIndication;
    int graphicsProfileLevelIndication;
    List<ESDescriptor> esDescriptors;
    List<ExtensionDescriptor> extensionDescriptors;
    List<BaseDescriptor> unknownDescriptors;
    
    public InitialObjectDescriptor() {
        this.esDescriptors = new ArrayList<ESDescriptor>();
        this.extensionDescriptors = new ArrayList<ExtensionDescriptor>();
        this.unknownDescriptors = new ArrayList<BaseDescriptor>();
    }
    
    @Override
    public void parseDetail(final ByteBuffer bb) throws IOException {
        final int data = IsoTypeReader.readUInt16(bb);
        this.objectDescriptorId = (data & 0xFFC0) >> 6;
        this.urlFlag = (data & 0x3F) >> 5;
        this.includeInlineProfileLevelFlag = (data & 0x1F) >> 4;
        int sizeLeft = this.getSize() - 2;
        if (this.urlFlag == 1) {
            this.urlLength = IsoTypeReader.readUInt8(bb);
            this.urlString = IsoTypeReader.readString(bb, this.urlLength);
            sizeLeft -= 1 + this.urlLength;
        }
        else {
            this.oDProfileLevelIndication = IsoTypeReader.readUInt8(bb);
            this.sceneProfileLevelIndication = IsoTypeReader.readUInt8(bb);
            this.audioProfileLevelIndication = IsoTypeReader.readUInt8(bb);
            this.visualProfileLevelIndication = IsoTypeReader.readUInt8(bb);
            this.graphicsProfileLevelIndication = IsoTypeReader.readUInt8(bb);
            sizeLeft -= 5;
            if (sizeLeft > 2) {
                final BaseDescriptor descriptor = ObjectDescriptorFactory.createFrom(-1, bb);
                sizeLeft -= descriptor.getSize();
                if (descriptor instanceof ESDescriptor) {
                    this.esDescriptors.add((ESDescriptor)descriptor);
                }
                else {
                    this.unknownDescriptors.add(descriptor);
                }
            }
        }
        if (sizeLeft > 2) {
            final BaseDescriptor descriptor = ObjectDescriptorFactory.createFrom(-1, bb);
            if (descriptor instanceof ExtensionDescriptor) {
                this.extensionDescriptors.add((ExtensionDescriptor)descriptor);
            }
            else {
                this.unknownDescriptors.add(descriptor);
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InitialObjectDescriptor");
        sb.append("{objectDescriptorId=").append(this.objectDescriptorId);
        sb.append(", urlFlag=").append(this.urlFlag);
        sb.append(", includeInlineProfileLevelFlag=").append(this.includeInlineProfileLevelFlag);
        sb.append(", urlLength=").append(this.urlLength);
        sb.append(", urlString='").append(this.urlString).append('\'');
        sb.append(", oDProfileLevelIndication=").append(this.oDProfileLevelIndication);
        sb.append(", sceneProfileLevelIndication=").append(this.sceneProfileLevelIndication);
        sb.append(", audioProfileLevelIndication=").append(this.audioProfileLevelIndication);
        sb.append(", visualProfileLevelIndication=").append(this.visualProfileLevelIndication);
        sb.append(", graphicsProfileLevelIndication=").append(this.graphicsProfileLevelIndication);
        sb.append(", esDescriptors=").append(this.esDescriptors);
        sb.append(", extensionDescriptors=").append(this.extensionDescriptors);
        sb.append(", unknownDescriptors=").append(this.unknownDescriptors);
        sb.append('}');
        return sb.toString();
    }
}
