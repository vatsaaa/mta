// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes;

import com.coremedia.iso.Hex;
import java.math.BigInteger;
import org.aspectj.lang.Signature;
import java.util.ArrayList;
import java.util.Arrays;
import java.nio.channels.WritableByteChannel;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import org.aspectj.runtime.internal.Conversions;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractFullBox;

public abstract class AbstractSampleEncryptionBox extends AbstractFullBox
{
    int algorithmId;
    int ivSize;
    byte[] kid;
    List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    
    protected AbstractSampleEncryptionBox(final String type) {
        super(type);
        this.algorithmId = -1;
        this.ivSize = -1;
        this.kid = new byte[16];
        this.entries = new LinkedList<Entry>();
    }
    
    public int getOffsetToFirstIV() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_0, this, this));
        int offset = (this.getSize() > 4294967296L) ? 16 : 8;
        offset += (this.isOverrideTrackEncryptionBoxParameters() ? 20 : 0);
        offset += 4;
        return offset;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if ((this.getFlags() & 0x1) > 0) {
            this.algorithmId = IsoTypeReader.readUInt24(content);
            this.ivSize = IsoTypeReader.readUInt8(content);
            content.get(this.kid = new byte[16]);
        }
        long numOfEntries = IsoTypeReader.readUInt32(content);
        while (numOfEntries-- > 0L) {
            final Entry e = new Entry();
            content.get(e.iv = new byte[((this.getFlags() & 0x1) > 0) ? this.ivSize : 8]);
            if ((this.getFlags() & 0x2) > 0) {
                int numOfPairs = IsoTypeReader.readUInt16(content);
                e.pairs = new LinkedList<Entry.Pair>();
                while (numOfPairs-- > 0) {
                    e.pairs.add(e.createPair(IsoTypeReader.readUInt16(content), IsoTypeReader.readUInt32(content)));
                }
            }
            this.entries.add(e);
        }
    }
    
    public int getSampleCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_1, this, this));
        return this.entries.size();
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_2, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_3, this, this, entries));
        this.entries = entries;
    }
    
    public int getAlgorithmId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_4, this, this));
        return this.algorithmId;
    }
    
    public void setAlgorithmId(final int algorithmId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_5, this, this, Conversions.intObject(algorithmId)));
        this.algorithmId = algorithmId;
    }
    
    public int getIvSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_6, this, this));
        return this.ivSize;
    }
    
    public void setIvSize(final int ivSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_7, this, this, Conversions.intObject(ivSize)));
        this.ivSize = ivSize;
    }
    
    public byte[] getKid() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_8, this, this));
        return this.kid;
    }
    
    public void setKid(final byte[] kid) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_9, this, this, kid));
        this.kid = kid;
    }
    
    public boolean isSubSampleEncryption() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_10, this, this));
        return (this.getFlags() & 0x2) > 0;
    }
    
    public boolean isOverrideTrackEncryptionBoxParameters() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_11, this, this));
        return (this.getFlags() & 0x1) > 0;
    }
    
    public void setSubSampleEncryption(final boolean b) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_12, this, this, Conversions.booleanObject(b)));
        if (b) {
            this.setFlags(this.getFlags() | 0x2);
        }
        else {
            this.setFlags(this.getFlags() & 0xFFFFFD);
        }
    }
    
    public void setOverrideTrackEncryptionBoxParameters(final boolean b) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_13, this, this, Conversions.booleanObject(b)));
        if (b) {
            this.setFlags(this.getFlags() | 0x1);
        }
        else {
            this.setFlags(this.getFlags() & 0xFFFFFE);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        if (this.isOverrideTrackEncryptionBoxParameters()) {
            IsoTypeWriter.writeUInt24(bb, this.algorithmId);
            IsoTypeWriter.writeUInt8(bb, this.ivSize);
            bb.put(this.kid);
        }
        IsoTypeWriter.writeUInt32(bb, this.entries.size());
        for (final Entry entry : this.entries) {
            if (this.isOverrideTrackEncryptionBoxParameters()) {
                final byte[] ivFull = new byte[this.ivSize];
                System.arraycopy(entry.iv, 0, ivFull, this.ivSize - entry.iv.length, entry.iv.length);
                bb.put(ivFull);
            }
            else {
                bb.put(entry.iv);
            }
            if (this.isSubSampleEncryption()) {
                IsoTypeWriter.writeUInt16(bb, entry.pairs.size());
                for (final Entry.Pair pair : entry.pairs) {
                    IsoTypeWriter.writeUInt16(bb, pair.clear);
                    IsoTypeWriter.writeUInt32(bb, pair.encrypted);
                }
            }
        }
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 4L;
        if (this.isOverrideTrackEncryptionBoxParameters()) {
            contentSize += 4L;
            contentSize += this.kid.length;
        }
        contentSize += 4L;
        for (final Entry entry : this.entries) {
            contentSize += entry.getSize();
        }
        return contentSize;
    }
    
    @Override
    public void getBox(final WritableByteChannel os) throws IOException {
        super.getBox(os);
    }
    
    public Entry createEntry() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_14, this, this));
        return new Entry();
    }
    
    @Override
    public boolean equals(final Object o) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_15, this, this, o));
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final AbstractSampleEncryptionBox that = (AbstractSampleEncryptionBox)o;
        if (this.algorithmId != that.algorithmId) {
            return false;
        }
        if (this.ivSize != that.ivSize) {
            return false;
        }
        if (this.entries != null) {
            if (this.entries.equals(that.entries)) {
                return Arrays.equals(this.kid, that.kid);
            }
        }
        else if (that.entries == null) {
            return Arrays.equals(this.kid, that.kid);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_16, this, this));
        int result = this.algorithmId;
        result = 31 * result + this.ivSize;
        result = 31 * result + ((this.kid != null) ? Arrays.hashCode(this.kid) : 0);
        result = 31 * result + ((this.entries != null) ? this.entries.hashCode() : 0);
        return result;
    }
    
    public List<Short> getEntrySizes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractSampleEncryptionBox.ajc$tjp_17, this, this));
        final List entrySizes = new ArrayList(this.entries.size());
        for (final Entry entry : this.entries) {
            short size = (short)entry.iv.length;
            if (this.isSubSampleEncryption()) {
                size += 2;
                size += (short)(entry.pairs.size() * 6);
            }
            entrySizes.add(size);
        }
        return (List<Short>)entrySizes;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AbstractSampleEncryptionBox.java", AbstractSampleEncryptionBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getOffsetToFirstIV", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "int"), 28);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleCount", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "int"), 62);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isSubSampleEncryption", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "boolean"), 99);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isOverrideTrackEncryptionBoxParameters", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "boolean"), 103);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSubSampleEncryption", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "boolean", "b", "", "void"), 107);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setOverrideTrackEncryptionBoxParameters", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "boolean", "b", "", "void"), 115);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "createEntry", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox$Entry"), 171);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "equals", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "java.lang.Object", "o", "", "boolean"), 268);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "hashCode", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "int"), 283);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntrySizes", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "java.util.List"), 291);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "java.util.List"), 66);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "java.util.List", "entries", "", "void"), 70);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAlgorithmId", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "int"), 74);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAlgorithmId", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "int", "algorithmId", "", "void"), 78);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getIvSize", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "int"), 82);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setIvSize", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "int", "ivSize", "", "void"), 86);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getKid", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "[B"), 90);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setKid", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "[B", "kid", "", "void"), 94);
    }
    
    public class Entry
    {
        public byte[] iv;
        public List<Pair> pairs;
        
        public Entry() {
            this.pairs = new LinkedList<Pair>();
        }
        
        public int getSize() {
            int size = 0;
            if (AbstractSampleEncryptionBox.this.isOverrideTrackEncryptionBoxParameters()) {
                size = AbstractSampleEncryptionBox.this.ivSize;
            }
            else {
                size = this.iv.length;
            }
            if (AbstractSampleEncryptionBox.this.isSubSampleEncryption()) {
                size += 2;
                for (final Pair pair : this.pairs) {
                    size += 6;
                }
            }
            return size;
        }
        
        public Pair createPair(final int clear, final long encrypted) {
            return new Pair(clear, encrypted);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            final Entry entry = (Entry)o;
            if (!new BigInteger(this.iv).equals(new BigInteger(entry.iv))) {
                return false;
            }
            if (this.pairs != null) {
                if (this.pairs.equals(entry.pairs)) {
                    return true;
                }
            }
            else if (entry.pairs == null) {
                return true;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            int result = (this.iv != null) ? Arrays.hashCode(this.iv) : 0;
            result = 31 * result + ((this.pairs != null) ? this.pairs.hashCode() : 0);
            return result;
        }
        
        @Override
        public String toString() {
            return "Entry{iv=" + Hex.encodeHex(this.iv) + ", pairs=" + this.pairs + '}';
        }
        
        public class Pair
        {
            public int clear;
            public long encrypted;
            
            public Pair(final int clear, final long encrypted) {
                this.clear = clear;
                this.encrypted = encrypted;
            }
            
            @Override
            public boolean equals(final Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || this.getClass() != o.getClass()) {
                    return false;
                }
                final Pair pair = (Pair)o;
                return this.clear == pair.clear && this.encrypted == pair.encrypted;
            }
            
            @Override
            public int hashCode() {
                int result = this.clear;
                result = 31 * result + (int)(this.encrypted ^ this.encrypted >>> 32);
                return result;
            }
            
            @Override
            public String toString() {
                return "clr:" + this.clear + " enc:" + this.encrypted;
            }
        }
    }
}
