// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.ultraviolet;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.Utf8;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class BaseLocationBox extends AbstractFullBox
{
    String baseLocation;
    String purchaseLocation;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    public BaseLocationBox() {
        super("bloc");
        this.baseLocation = "";
        this.purchaseLocation = "";
    }
    
    public BaseLocationBox(final String baseLocation, final String purchaseLocation) {
        super("bloc");
        this.baseLocation = "";
        this.purchaseLocation = "";
        this.baseLocation = baseLocation;
        this.purchaseLocation = purchaseLocation;
    }
    
    public String getBaseLocation() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BaseLocationBox.ajc$tjp_0, this, this));
        return this.baseLocation;
    }
    
    public void setBaseLocation(final String baseLocation) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BaseLocationBox.ajc$tjp_1, this, this, baseLocation));
        this.baseLocation = baseLocation;
    }
    
    public String getPurchaseLocation() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BaseLocationBox.ajc$tjp_2, this, this));
        return this.purchaseLocation;
    }
    
    public void setPurchaseLocation(final String purchaseLocation) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BaseLocationBox.ajc$tjp_3, this, this, purchaseLocation));
        this.purchaseLocation = purchaseLocation;
    }
    
    @Override
    protected long getContentSize() {
        return 1028L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.baseLocation = IsoTypeReader.readString(content);
        content.get(new byte[256 - Utf8.utf8StringLengthInBytes(this.baseLocation) - 1]);
        this.purchaseLocation = IsoTypeReader.readString(content);
        content.get(new byte[256 - Utf8.utf8StringLengthInBytes(this.purchaseLocation) - 1]);
        content.get(new byte[512]);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(Utf8.convert(this.baseLocation));
        bb.put(new byte[256 - Utf8.utf8StringLengthInBytes(this.baseLocation)]);
        bb.put(Utf8.convert(this.purchaseLocation));
        bb.put(new byte[256 - Utf8.utf8StringLengthInBytes(this.purchaseLocation)]);
        bb.put(new byte[512]);
    }
    
    @Override
    public boolean equals(final Object o) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BaseLocationBox.ajc$tjp_4, this, this, o));
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final BaseLocationBox that = (BaseLocationBox)o;
        Label_0083: {
            if (this.baseLocation != null) {
                if (this.baseLocation.equals(that.baseLocation)) {
                    break Label_0083;
                }
            }
            else if (that.baseLocation == null) {
                break Label_0083;
            }
            return false;
        }
        if (this.purchaseLocation != null) {
            if (this.purchaseLocation.equals(that.purchaseLocation)) {
                return true;
            }
        }
        else if (that.purchaseLocation == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BaseLocationBox.ajc$tjp_5, this, this));
        int result = (this.baseLocation != null) ? this.baseLocation.hashCode() : 0;
        result = 31 * result + ((this.purchaseLocation != null) ? this.purchaseLocation.hashCode() : 0);
        return result;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("BaseLocationBox.java", BaseLocationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBaseLocation", "com.googlecode.mp4parser.boxes.ultraviolet.BaseLocationBox", "", "", "", "java.lang.String"), 43);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBaseLocation", "com.googlecode.mp4parser.boxes.ultraviolet.BaseLocationBox", "java.lang.String", "baseLocation", "", "void"), 47);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getPurchaseLocation", "com.googlecode.mp4parser.boxes.ultraviolet.BaseLocationBox", "", "", "", "java.lang.String"), 51);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setPurchaseLocation", "com.googlecode.mp4parser.boxes.ultraviolet.BaseLocationBox", "java.lang.String", "purchaseLocation", "", "void"), 55);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "equals", "com.googlecode.mp4parser.boxes.ultraviolet.BaseLocationBox", "java.lang.Object", "o", "", "boolean"), 85);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "hashCode", "com.googlecode.mp4parser.boxes.ultraviolet.BaseLocationBox", "", "", "", "int"), 99);
    }
}
