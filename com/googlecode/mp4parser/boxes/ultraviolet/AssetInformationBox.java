// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.ultraviolet;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class AssetInformationBox extends AbstractFullBox
{
    String apid;
    String profileVersion;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    
    static {
        ajc$preClinit();
    }
    
    public AssetInformationBox() {
        super("ainf");
        this.apid = "";
        this.profileVersion = "0000";
    }
    
    @Override
    protected long getContentSize() {
        return Utf8.utf8StringLengthInBytes(this.apid) + 9;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) {
        this.writeVersionAndFlags(bb);
        bb.put(Utf8.convert(this.profileVersion), 0, 4);
        bb.put(Utf8.convert(this.apid));
        bb.put((byte)0);
    }
    
    @Override
    public void _parseDetails(ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.profileVersion = IsoTypeReader.readString(content, 4);
        this.apid = IsoTypeReader.readString(content);
        content = null;
    }
    
    public String getApid() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AssetInformationBox.ajc$tjp_0, this, this));
        return this.apid;
    }
    
    public void setApid(final String apid) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AssetInformationBox.ajc$tjp_1, this, this, apid));
        this.apid = apid;
    }
    
    public String getProfileVersion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AssetInformationBox.ajc$tjp_2, this, this));
        return this.profileVersion;
    }
    
    public void setProfileVersion(final String profileVersion) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AssetInformationBox.ajc$tjp_3, this, this, profileVersion));
        assert profileVersion != null && profileVersion.length() == 4;
        this.profileVersion = profileVersion;
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AssetInformationBox.java", AssetInformationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getApid", "com.googlecode.mp4parser.boxes.ultraviolet.AssetInformationBox", "", "", "", "java.lang.String"), 59);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setApid", "com.googlecode.mp4parser.boxes.ultraviolet.AssetInformationBox", "java.lang.String", "apid", "", "void"), 63);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getProfileVersion", "com.googlecode.mp4parser.boxes.ultraviolet.AssetInformationBox", "", "", "", "java.lang.String"), 67);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setProfileVersion", "com.googlecode.mp4parser.boxes.ultraviolet.AssetInformationBox", "java.lang.String", "profileVersion", "", "void"), 71);
    }
}
