// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitWriterBuffer;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitReaderBuffer;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractBox;

public class MLPSpecificBox extends AbstractBox
{
    int format_info;
    int peak_data_rate;
    int reserved;
    int reserved2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    
    public MLPSpecificBox() {
        super("dmlp");
    }
    
    @Override
    protected long getContentSize() {
        return 10L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        final BitReaderBuffer brb = new BitReaderBuffer(content);
        this.format_info = brb.readBits(32);
        this.peak_data_rate = brb.readBits(15);
        this.reserved = brb.readBits(1);
        this.reserved2 = brb.readBits(32);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        final BitWriterBuffer bwb = new BitWriterBuffer(bb);
        bwb.writeBits(this.format_info, 32);
        bwb.writeBits(this.peak_data_rate, 15);
        bwb.writeBits(this.reserved, 1);
        bwb.writeBits(this.reserved2, 32);
    }
    
    public int getFormat_info() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_0, this, this));
        return this.format_info;
    }
    
    public void setFormat_info(final int format_info) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_1, this, this, Conversions.intObject(format_info)));
        this.format_info = format_info;
    }
    
    public int getPeak_data_rate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_2, this, this));
        return this.peak_data_rate;
    }
    
    public void setPeak_data_rate(final int peak_data_rate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_3, this, this, Conversions.intObject(peak_data_rate)));
        this.peak_data_rate = peak_data_rate;
    }
    
    public int getReserved() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_4, this, this));
        return this.reserved;
    }
    
    public void setReserved(final int reserved) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_5, this, this, Conversions.intObject(reserved)));
        this.reserved = reserved;
    }
    
    public int getReserved2() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_6, this, this));
        return this.reserved2;
    }
    
    public void setReserved2(final int reserved2) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MLPSpecificBox.ajc$tjp_7, this, this, Conversions.intObject(reserved2)));
        this.reserved2 = reserved2;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MLPSpecificBox.java", MLPSpecificBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFormat_info", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 46);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setFormat_info", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "format_info", "", "void"), 50);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getPeak_data_rate", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 54);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setPeak_data_rate", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "peak_data_rate", "", "void"), 58);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReserved", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 62);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setReserved", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "reserved", "", "void"), 66);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReserved2", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 70);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setReserved2", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "reserved2", "", "void"), 74);
    }
}
