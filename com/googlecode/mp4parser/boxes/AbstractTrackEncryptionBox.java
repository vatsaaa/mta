// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes;

import org.aspectj.lang.Signature;
import java.util.Arrays;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.util.UUID;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public abstract class AbstractTrackEncryptionBox extends AbstractFullBox
{
    int defaultAlgorithmId;
    int defaultIvSize;
    byte[] default_KID;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    
    protected AbstractTrackEncryptionBox(final String type) {
        super(type);
    }
    
    public int getDefaultAlgorithmId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_0, this, this));
        return this.defaultAlgorithmId;
    }
    
    public void setDefaultAlgorithmId(final int defaultAlgorithmId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_1, this, this, Conversions.intObject(defaultAlgorithmId)));
        this.defaultAlgorithmId = defaultAlgorithmId;
    }
    
    public int getDefaultIvSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_2, this, this));
        return this.defaultIvSize;
    }
    
    public void setDefaultIvSize(final int defaultIvSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_3, this, this, Conversions.intObject(defaultIvSize)));
        this.defaultIvSize = defaultIvSize;
    }
    
    public String getDefault_KID() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_4, this, this));
        final ByteBuffer b = ByteBuffer.wrap(this.default_KID);
        b.order(ByteOrder.BIG_ENDIAN);
        return new UUID(b.getLong(), b.getLong()).toString();
    }
    
    public void setDefault_KID(final byte[] default_KID) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_5, this, this, default_KID));
        this.default_KID = default_KID;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.defaultAlgorithmId = IsoTypeReader.readUInt24(content);
        this.defaultIvSize = IsoTypeReader.readUInt8(content);
        content.get(this.default_KID = new byte[16]);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt24(bb, this.defaultAlgorithmId);
        IsoTypeWriter.writeUInt8(bb, this.defaultIvSize);
        bb.put(this.default_KID);
    }
    
    @Override
    protected long getContentSize() {
        return 24L;
    }
    
    @Override
    public boolean equals(final Object o) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_6, this, this, o));
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final AbstractTrackEncryptionBox that = (AbstractTrackEncryptionBox)o;
        return this.defaultAlgorithmId == that.defaultAlgorithmId && this.defaultIvSize == that.defaultIvSize && Arrays.equals(this.default_KID, that.default_KID);
    }
    
    @Override
    public int hashCode() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractTrackEncryptionBox.ajc$tjp_7, this, this));
        int result = this.defaultAlgorithmId;
        result = 31 * result + this.defaultIvSize;
        result = 31 * result + ((this.default_KID != null) ? Arrays.hashCode(this.default_KID) : 0);
        return result;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AbstractTrackEncryptionBox.java", AbstractTrackEncryptionBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultAlgorithmId", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "int"), 25);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultAlgorithmId", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "int", "defaultAlgorithmId", "", "void"), 29);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultIvSize", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "int"), 33);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultIvSize", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "int", "defaultIvSize", "", "void"), 37);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefault_KID", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "java.lang.String"), 41);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefault_KID", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "[B", "default_KID", "", "void"), 47);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "equals", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "java.lang.Object", "o", "", "boolean"), 74);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "hashCode", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "int"), 88);
    }
}
