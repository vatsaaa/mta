// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitWriterBuffer;
import com.coremedia.iso.IsoTypeWriter;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitReaderBuffer;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractBox;

public class DTSSpecificBox extends AbstractBox
{
    long DTSSamplingFrequency;
    long maxBitRate;
    long avgBitRate;
    int pcmSampleDepth;
    int frameDuration;
    int streamConstruction;
    int coreLFEPresent;
    int coreLayout;
    int coreSize;
    int stereoDownmix;
    int representationType;
    int channelLayout;
    int multiAssetFlag;
    int LBRDurationMod;
    int reservedBoxPresent;
    int reserved;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_18;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_19;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_20;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_21;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_22;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_23;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_24;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_25;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_26;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_27;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_28;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_29;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_30;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_31;
    
    public DTSSpecificBox() {
        super("ddts");
    }
    
    @Override
    protected long getContentSize() {
        return 20L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.DTSSamplingFrequency = IsoTypeReader.readUInt32(content);
        this.maxBitRate = IsoTypeReader.readUInt32(content);
        this.avgBitRate = IsoTypeReader.readUInt32(content);
        this.pcmSampleDepth = IsoTypeReader.readUInt8(content);
        final BitReaderBuffer brb = new BitReaderBuffer(content);
        this.frameDuration = brb.readBits(2);
        this.streamConstruction = brb.readBits(5);
        this.coreLFEPresent = brb.readBits(1);
        this.coreLayout = brb.readBits(6);
        this.coreSize = brb.readBits(14);
        this.stereoDownmix = brb.readBits(1);
        this.representationType = brb.readBits(3);
        this.channelLayout = brb.readBits(16);
        this.multiAssetFlag = brb.readBits(1);
        this.LBRDurationMod = brb.readBits(1);
        this.reservedBoxPresent = brb.readBits(1);
        this.reserved = brb.readBits(5);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        IsoTypeWriter.writeUInt32(bb, this.DTSSamplingFrequency);
        IsoTypeWriter.writeUInt32(bb, this.maxBitRate);
        IsoTypeWriter.writeUInt32(bb, this.avgBitRate);
        IsoTypeWriter.writeUInt8(bb, this.pcmSampleDepth);
        final BitWriterBuffer bwb = new BitWriterBuffer(bb);
        bwb.writeBits(this.frameDuration, 2);
        bwb.writeBits(this.streamConstruction, 5);
        bwb.writeBits(this.coreLFEPresent, 1);
        bwb.writeBits(this.coreLayout, 6);
        bwb.writeBits(this.coreSize, 14);
        bwb.writeBits(this.stereoDownmix, 1);
        bwb.writeBits(this.representationType, 3);
        bwb.writeBits(this.channelLayout, 16);
        bwb.writeBits(this.multiAssetFlag, 1);
        bwb.writeBits(this.LBRDurationMod, 1);
        bwb.writeBits(this.reservedBoxPresent, 1);
        bwb.writeBits(this.reserved, 5);
    }
    
    public long getAvgBitRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_0, this, this));
        return this.avgBitRate;
    }
    
    public void setAvgBitRate(final long avgBitRate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_1, this, this, Conversions.longObject(avgBitRate)));
        this.avgBitRate = avgBitRate;
    }
    
    public long getDTSSamplingFrequency() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_2, this, this));
        return this.DTSSamplingFrequency;
    }
    
    public void setDTSSamplingFrequency(final long DTSSamplingFrequency) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_3, this, this, Conversions.longObject(DTSSamplingFrequency)));
        this.DTSSamplingFrequency = DTSSamplingFrequency;
    }
    
    public long getMaxBitRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_4, this, this));
        return this.maxBitRate;
    }
    
    public void setMaxBitRate(final long maxBitRate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_5, this, this, Conversions.longObject(maxBitRate)));
        this.maxBitRate = maxBitRate;
    }
    
    public int getPcmSampleDepth() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_6, this, this));
        return this.pcmSampleDepth;
    }
    
    public void setPcmSampleDepth(final int pcmSampleDepth) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_7, this, this, Conversions.intObject(pcmSampleDepth)));
        this.pcmSampleDepth = pcmSampleDepth;
    }
    
    public int getFrameDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_8, this, this));
        return this.frameDuration;
    }
    
    public void setFrameDuration(final int frameDuration) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_9, this, this, Conversions.intObject(frameDuration)));
        this.frameDuration = frameDuration;
    }
    
    public int getStreamConstruction() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_10, this, this));
        return this.streamConstruction;
    }
    
    public void setStreamConstruction(final int streamConstruction) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_11, this, this, Conversions.intObject(streamConstruction)));
        this.streamConstruction = streamConstruction;
    }
    
    public int getCoreLFEPresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_12, this, this));
        return this.coreLFEPresent;
    }
    
    public void setCoreLFEPresent(final int coreLFEPresent) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_13, this, this, Conversions.intObject(coreLFEPresent)));
        this.coreLFEPresent = coreLFEPresent;
    }
    
    public int getCoreLayout() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_14, this, this));
        return this.coreLayout;
    }
    
    public void setCoreLayout(final int coreLayout) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_15, this, this, Conversions.intObject(coreLayout)));
        this.coreLayout = coreLayout;
    }
    
    public int getCoreSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_16, this, this));
        return this.coreSize;
    }
    
    public void setCoreSize(final int coreSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_17, this, this, Conversions.intObject(coreSize)));
        this.coreSize = coreSize;
    }
    
    public int getStereoDownmix() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_18, this, this));
        return this.stereoDownmix;
    }
    
    public void setStereoDownmix(final int stereoDownmix) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_19, this, this, Conversions.intObject(stereoDownmix)));
        this.stereoDownmix = stereoDownmix;
    }
    
    public int getRepresentationType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_20, this, this));
        return this.representationType;
    }
    
    public void setRepresentationType(final int representationType) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_21, this, this, Conversions.intObject(representationType)));
        this.representationType = representationType;
    }
    
    public int getChannelLayout() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_22, this, this));
        return this.channelLayout;
    }
    
    public void setChannelLayout(final int channelLayout) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_23, this, this, Conversions.intObject(channelLayout)));
        this.channelLayout = channelLayout;
    }
    
    public int getMultiAssetFlag() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_24, this, this));
        return this.multiAssetFlag;
    }
    
    public void setMultiAssetFlag(final int multiAssetFlag) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_25, this, this, Conversions.intObject(multiAssetFlag)));
        this.multiAssetFlag = multiAssetFlag;
    }
    
    public int getLBRDurationMod() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_26, this, this));
        return this.LBRDurationMod;
    }
    
    public void setLBRDurationMod(final int LBRDurationMod) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_27, this, this, Conversions.intObject(LBRDurationMod)));
        this.LBRDurationMod = LBRDurationMod;
    }
    
    public int getReserved() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_28, this, this));
        return this.reserved;
    }
    
    public void setReserved(final int reserved) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_29, this, this, Conversions.intObject(reserved)));
        this.reserved = reserved;
    }
    
    public int getReservedBoxPresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_30, this, this));
        return this.reservedBoxPresent;
    }
    
    public void setReservedBoxPresent(final int reservedBoxPresent) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DTSSpecificBox.ajc$tjp_31, this, this, Conversions.intObject(reservedBoxPresent)));
        this.reservedBoxPresent = reservedBoxPresent;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("DTSSpecificBox.java", DTSSpecificBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAvgBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "long"), 91);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAvgBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "long", "avgBitRate", "", "void"), 95);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getStreamConstruction", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 131);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setStreamConstruction", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "streamConstruction", "", "void"), 135);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCoreLFEPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 139);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCoreLFEPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "coreLFEPresent", "", "void"), 143);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCoreLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 147);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCoreLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "coreLayout", "", "void"), 151);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCoreSize", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 155);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCoreSize", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "coreSize", "", "void"), 159);
        ajc$tjp_18 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getStereoDownmix", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 163);
        ajc$tjp_19 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setStereoDownmix", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "stereoDownmix", "", "void"), 167);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDTSSamplingFrequency", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "long"), 99);
        ajc$tjp_20 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getRepresentationType", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 171);
        ajc$tjp_21 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setRepresentationType", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "representationType", "", "void"), 175);
        ajc$tjp_22 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getChannelLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 179);
        ajc$tjp_23 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setChannelLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "channelLayout", "", "void"), 183);
        ajc$tjp_24 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMultiAssetFlag", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 187);
        ajc$tjp_25 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMultiAssetFlag", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "multiAssetFlag", "", "void"), 191);
        ajc$tjp_26 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLBRDurationMod", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 195);
        ajc$tjp_27 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLBRDurationMod", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "LBRDurationMod", "", "void"), 199);
        ajc$tjp_28 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReserved", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 203);
        ajc$tjp_29 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setReserved", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "reserved", "", "void"), 207);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDTSSamplingFrequency", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "long", "DTSSamplingFrequency", "", "void"), 103);
        ajc$tjp_30 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReservedBoxPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 211);
        ajc$tjp_31 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setReservedBoxPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "reservedBoxPresent", "", "void"), 215);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMaxBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "long"), 107);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMaxBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "long", "maxBitRate", "", "void"), 111);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getPcmSampleDepth", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 115);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setPcmSampleDepth", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "pcmSampleDepth", "", "void"), 119);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFrameDuration", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 123);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setFrameDuration", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "frameDuration", "", "void"), 127);
    }
}
