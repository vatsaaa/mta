// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.basemediaformat;

import org.aspectj.lang.Signature;
import java.util.Arrays;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import java.nio.ByteBuffer;
import com.coremedia.iso.boxes.CastUtils;
import java.io.ByteArrayOutputStream;
import com.coremedia.iso.boxes.h264.AvcConfigurationBox;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractBox;

public class AvcNalUnitStorageBox extends AbstractBox
{
    byte[] data;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public AvcNalUnitStorageBox() {
        super("avcn");
    }
    
    public AvcNalUnitStorageBox(final AvcConfigurationBox avcConfigurationBox) {
        super("avcn");
        new ByteArrayOutputStream();
        final ByteBuffer content = ByteBuffer.allocate(CastUtils.l2i(avcConfigurationBox.getContentSize()));
        try {
            avcConfigurationBox.getContent(content);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.data = content.array();
    }
    
    @Override
    protected long getContentSize() {
        return this.data.length;
    }
    
    public void setData(final byte[] data) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcNalUnitStorageBox.ajc$tjp_0, this, this, data));
        this.data = data;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.data = new byte[content.remaining()];
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(this.data);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcNalUnitStorageBox.ajc$tjp_1, this, this));
        return "AvcNalUnitStorageBox{data=" + Arrays.toString(this.data) + '}';
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AvcNalUnitStorageBox.java", AvcNalUnitStorageBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setData", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "[B", "data", "", "void"), 60);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "java.lang.String"), 75);
    }
}
