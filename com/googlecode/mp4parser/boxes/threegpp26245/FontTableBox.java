// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser.boxes.threegpp26245;

import com.coremedia.iso.Utf8;
import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractBox;

public class FontTableBox extends AbstractBox
{
    List<FontRecord> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public FontTableBox() {
        super("ftab");
        this.entries = new LinkedList<FontRecord>();
    }
    
    @Override
    protected long getContentSize() {
        int size = 2;
        for (final FontRecord fontRecord : this.entries) {
            size += fontRecord.getSize();
        }
        return size;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        for (int numberOfRecords = IsoTypeReader.readUInt16(content), i = 0; i < numberOfRecords; ++i) {
            final FontRecord fr = new FontRecord();
            fr.parse(content);
            this.entries.add(fr);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        IsoTypeWriter.writeUInt16(bb, this.entries.size());
        for (final FontRecord record : this.entries) {
            record.getContent(bb);
        }
    }
    
    public List<FontRecord> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FontTableBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<FontRecord> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FontTableBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("FontTableBox.java", FontTableBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.googlecode.mp4parser.boxes.threegpp26245.FontTableBox", "", "", "", "java.util.List"), 51);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.googlecode.mp4parser.boxes.threegpp26245.FontTableBox", "java.util.List", "entries", "", "void"), 55);
    }
    
    public static class FontRecord
    {
        int fontId;
        String fontname;
        
        public FontRecord() {
        }
        
        public FontRecord(final int fontId, final String fontname) {
            this.fontId = fontId;
            this.fontname = fontname;
        }
        
        public void parse(final ByteBuffer bb) {
            this.fontId = IsoTypeReader.readUInt16(bb);
            final int length = IsoTypeReader.readUInt8(bb);
            this.fontname = IsoTypeReader.readString(bb, length);
        }
        
        public void getContent(final ByteBuffer bb) throws IOException {
            IsoTypeWriter.writeUInt16(bb, this.fontId);
            IsoTypeWriter.writeUInt8(bb, this.fontname.length());
            bb.put(Utf8.convert(this.fontname));
        }
        
        public int getSize() {
            return Utf8.utf8StringLengthInBytes(this.fontname) + 3;
        }
        
        @Override
        public String toString() {
            return "FontRecord{fontId=" + this.fontId + ", fontname='" + this.fontname + '\'' + '}';
        }
    }
}
