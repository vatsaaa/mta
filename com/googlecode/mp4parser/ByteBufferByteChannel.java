// 
// Decompiled by Procyon v0.5.36
// 

package com.googlecode.mp4parser;

import java.io.IOException;
import java.io.EOFException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;

public class ByteBufferByteChannel implements ByteChannel
{
    ByteBuffer byteBuffer;
    
    public ByteBufferByteChannel(final ByteBuffer byteBuffer) {
        this.byteBuffer = byteBuffer;
    }
    
    public int read(final ByteBuffer dst) throws IOException {
        final byte[] b = dst.array();
        final int r = dst.remaining();
        if (this.byteBuffer.remaining() >= r) {
            this.byteBuffer.get(b, dst.position(), r);
            return r;
        }
        throw new EOFException("Reading beyond end of stream");
    }
    
    public boolean isOpen() {
        return true;
    }
    
    public void close() throws IOException {
    }
    
    public int write(final ByteBuffer src) throws IOException {
        final int r = src.remaining();
        this.byteBuffer.put(src);
        return r;
    }
}
