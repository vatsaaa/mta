// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.lang;

import java.io.PrintWriter;
import java.io.PrintStream;

public class CompoundException extends Exception
{
    private final Throwable _innnerException;
    
    public CompoundException(final String s) {
        this(s, null);
    }
    
    public CompoundException(final Throwable t) {
        this(null, t);
    }
    
    public CompoundException(final String message, final Throwable innnerException) {
        super(message);
        this._innnerException = innnerException;
    }
    
    public Throwable getInnerException() {
        return this._innnerException;
    }
    
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(super.toString());
        if (this._innnerException != null) {
            sb.append("\n");
            sb.append("--- inner exception ---");
            sb.append("\n");
            sb.append(this._innnerException.toString());
        }
        return sb.toString();
    }
    
    public void printStackTrace(final PrintStream printStream) {
        super.printStackTrace(printStream);
        if (this._innnerException != null) {
            printStream.println("--- inner exception ---");
            this._innnerException.printStackTrace(printStream);
        }
    }
    
    public void printStackTrace(final PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        if (this._innnerException != null) {
            printWriter.println("--- inner exception ---");
            this._innnerException.printStackTrace(printWriter);
        }
    }
    
    public void printStackTrace() {
        super.printStackTrace();
        if (this._innnerException != null) {
            System.err.println("--- inner exception ---");
            this._innnerException.printStackTrace();
        }
    }
}
