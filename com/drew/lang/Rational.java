// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.lang;

import java.io.Serializable;

public class Rational extends Number implements Serializable
{
    private final int numerator;
    private final int denominator;
    private int maxSimplificationCalculations;
    
    public Rational(final int numerator, final int denominator) {
        this.maxSimplificationCalculations = 1000;
        this.numerator = numerator;
        this.denominator = denominator;
    }
    
    public double doubleValue() {
        return this.numerator / (double)this.denominator;
    }
    
    public float floatValue() {
        return this.numerator / (float)this.denominator;
    }
    
    public final byte byteValue() {
        return (byte)this.doubleValue();
    }
    
    public final int intValue() {
        return (int)this.doubleValue();
    }
    
    public final long longValue() {
        return (long)this.doubleValue();
    }
    
    public final short shortValue() {
        return (short)this.doubleValue();
    }
    
    public final int getDenominator() {
        return this.denominator;
    }
    
    public final int getNumerator() {
        return this.numerator;
    }
    
    public Rational getReciprocal() {
        return new Rational(this.denominator, this.numerator);
    }
    
    public boolean isInteger() {
        return this.denominator == 1 || (this.denominator != 0 && this.numerator % this.denominator == 0) || (this.denominator == 0 && this.numerator == 0);
    }
    
    public String toString() {
        return this.numerator + "/" + this.denominator;
    }
    
    public String toSimpleString(final boolean b) {
        if (this.denominator == 0 && this.numerator != 0) {
            return this.toString();
        }
        if (this.isInteger()) {
            return Integer.toString(this.intValue());
        }
        if (this.numerator != 1 && this.denominator % this.numerator == 0) {
            return new Rational(1, this.denominator / this.numerator).toSimpleString(b);
        }
        final Rational simplifiedInstance = this.getSimplifiedInstance();
        if (b) {
            final String string = Double.toString(simplifiedInstance.doubleValue());
            if (string.length() < 5) {
                return string;
            }
        }
        return simplifiedInstance.toString();
    }
    
    private boolean tooComplexForSimplification() {
        return (Math.min(this.denominator, this.numerator) - 1) / 5.0 + 2.0 > this.maxSimplificationCalculations;
    }
    
    public boolean equals(final Object o) {
        return o instanceof Rational && this.doubleValue() == ((Rational)o).doubleValue();
    }
    
    public Rational getSimplifiedInstance() {
        if (this.tooComplexForSimplification()) {
            return this;
        }
        for (int i = 2; i <= Math.min(this.denominator, this.numerator); ++i) {
            if (i % 2 != 0 || i <= 2) {
                if (i % 5 != 0 || i <= 5) {
                    if (this.denominator % i == 0 && this.numerator % i == 0) {
                        return new Rational(this.numerator / i, this.denominator / i);
                    }
                }
            }
        }
        return this;
    }
}
