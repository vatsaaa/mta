// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging;

import com.drew.lang.CompoundException;

public class ImageProcessingException extends CompoundException
{
    public ImageProcessingException(final String s) {
        super(s);
    }
    
    public ImageProcessingException(final String s, final Throwable t) {
        super(s, t);
    }
    
    public ImageProcessingException(final Throwable t) {
        super(t);
    }
}
