// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging;

public class PhotographicConversions
{
    public static final double ROOT_TWO;
    
    private PhotographicConversions() {
    }
    
    public static double apertureToFStop(final double b) {
        return Math.pow(PhotographicConversions.ROOT_TWO, b);
    }
    
    public static double shutterSpeedToExposureTime(final double n) {
        return (float)(1.0 / Math.exp(n * Math.log(2.0)));
    }
    
    static {
        ROOT_TWO = Math.sqrt(2.0);
    }
}
