// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging;

import java.util.Iterator;
import com.drew.metadata.exif.ExifDirectory;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Tag;
import com.drew.metadata.Directory;
import com.drew.imaging.tiff.TiffMetadataReader;
import com.drew.imaging.jpeg.JpegMetadataReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import com.drew.metadata.Metadata;
import java.io.BufferedInputStream;

public class ImageMetadataReader
{
    private static final int JPEG_FILE_MAGIC_NUMBER = 65496;
    private static final int MOTOROLLA_TIFF_MAGIC_NUMBER = 19789;
    private static final int INTEL_TIFF_MAGIC_NUMBER = 18761;
    
    public static Metadata readMetadata(final BufferedInputStream bufferedInputStream) throws ImageProcessingException {
        return readMetadata(bufferedInputStream, null, readMagicNumber(bufferedInputStream));
    }
    
    public static Metadata readMetadata(final File file) throws ImageProcessingException {
        BufferedInputStream bufferedInputStream;
        try {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
        }
        catch (FileNotFoundException ex) {
            throw new ImageProcessingException("File not found: " + file.getPath(), ex);
        }
        final int magicNumber = readMagicNumber(bufferedInputStream);
        try {
            bufferedInputStream.close();
        }
        catch (IOException ex2) {
            throw new ImageProcessingException("Error closing file: " + file.getPath(), ex2);
        }
        return readMetadata(null, file, magicNumber);
    }
    
    private static Metadata readMetadata(final BufferedInputStream bufferedInputStream, final File file, final int n) throws ImageProcessingException {
        if ((n & 0xFFD8) == 0xFFD8) {
            if (bufferedInputStream != null) {
                return JpegMetadataReader.readMetadata(bufferedInputStream);
            }
            return JpegMetadataReader.readMetadata(file);
        }
        else {
            if (n != 18761 && n != 19789) {
                throw new ImageProcessingException("File is not the correct format");
            }
            if (bufferedInputStream != null) {
                return TiffMetadataReader.readMetadata(bufferedInputStream);
            }
            return TiffMetadataReader.readMetadata(file);
        }
    }
    
    private static int readMagicNumber(final BufferedInputStream bufferedInputStream) throws ImageProcessingException {
        int n;
        try {
            bufferedInputStream.mark(2);
            n = (bufferedInputStream.read() << 8 | bufferedInputStream.read());
            bufferedInputStream.reset();
        }
        catch (IOException ex) {
            throw new ImageProcessingException("Error reading image data", ex);
        }
        return n;
    }
    
    private ImageMetadataReader() throws Exception {
        throw new Exception("Not intended for instantiation");
    }
    
    public static void main(final String[] array) throws MetadataException, IOException {
        if (array.length < 1 || array.length > 2) {
            System.out.println("Usage: java -jar metadata-extractor-a.b.c.jar <filename> [/thumb]");
            System.exit(1);
        }
        Metadata metadata = null;
        try {
            metadata = readMetadata(new File(array[0]));
        }
        catch (Exception ex) {
            ex.printStackTrace(System.err);
            System.exit(1);
        }
        final Iterator directoryIterator = metadata.getDirectoryIterator();
        while (directoryIterator.hasNext()) {
            final Directory directory = directoryIterator.next();
            final Iterator tagIterator = directory.getTagIterator();
            while (tagIterator.hasNext()) {
                final Tag tag = tagIterator.next();
                try {
                    System.out.println("[" + directory.getName() + "] " + tag.getTagName() + " = " + tag.getDescription());
                }
                catch (MetadataException ex2) {
                    System.err.println(ex2.getMessage());
                    System.err.println(tag.getDirectoryName() + " " + tag.getTagName() + " (error)");
                }
            }
            if (directory.hasErrors()) {
                final Iterator errors = directory.getErrors();
                while (errors.hasNext()) {
                    System.out.println("ERROR: " + errors.next());
                }
            }
        }
        if (array.length > 1 && array[1].trim().equals("/thumb")) {
            final ExifDirectory exifDirectory = (ExifDirectory)metadata.getDirectory(ExifDirectory.class);
            if (exifDirectory.containsThumbnail()) {
                System.out.println("Writing thumbnail...");
                exifDirectory.writeThumbnail(array[0].trim() + ".thumb.jpg");
            }
            else {
                System.out.println("No thumbnail data exists in this image");
            }
        }
    }
}
