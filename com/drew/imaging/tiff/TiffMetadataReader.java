// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging.tiff;

import java.io.ByteArrayOutputStream;
import com.drew.metadata.exif.ExifReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import com.drew.metadata.Metadata;
import java.io.File;

public class TiffMetadataReader
{
    public static Metadata readMetadata(final File file) throws TiffProcessingException {
        final Metadata metadata = new Metadata();
        DataInputStream dataInputStream;
        try {
            dataInputStream = new DataInputStream(new FileInputStream(file));
        }
        catch (FileNotFoundException ex) {
            throw new TiffProcessingException("JPEG file does not exist", ex);
        }
        final byte[] b = new byte[(int)file.length()];
        try {
            dataInputStream.readFully(b);
        }
        catch (IOException ex2) {
            throw new TiffProcessingException("Error copying file contents to byte buffer", ex2);
        }
        new ExifReader(b).extractTiff(metadata);
        return metadata;
    }
    
    public static Metadata readMetadata(final InputStream inputStream) throws TiffProcessingException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            int read;
            while ((read = inputStream.read()) != -1) {
                byteArrayOutputStream.write(read);
            }
        }
        catch (IOException ex) {
            throw new TiffProcessingException("Error processing tiff stream", ex);
        }
        final Metadata metadata = new Metadata();
        new ExifReader(byteArrayOutputStream.toByteArray()).extractTiff(metadata);
        return metadata;
    }
}
