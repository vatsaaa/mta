// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging.tiff;

import com.drew.imaging.ImageProcessingException;

public class TiffProcessingException extends ImageProcessingException
{
    public TiffProcessingException(final String s) {
        super(s);
    }
    
    public TiffProcessingException(final String s, final Throwable t) {
        super(s, t);
    }
    
    public TiffProcessingException(final Throwable t) {
        super(t);
    }
}
