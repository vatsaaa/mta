// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging.jpeg;

import com.sun.image.codec.jpeg.JPEGDecodeParam;
import com.drew.metadata.jpeg.JpegCommentReader;
import com.drew.metadata.jpeg.JpegReader;
import com.drew.metadata.iptc.IptcReader;
import com.drew.metadata.exif.ExifReader;
import java.io.File;
import com.drew.metadata.Metadata;
import java.io.InputStream;

public class JpegMetadataReader
{
    public static Metadata readMetadata(final InputStream inputStream) throws JpegProcessingException {
        return extractMetadataFromJpegSegmentReader(new JpegSegmentReader(inputStream));
    }
    
    public static Metadata readMetadata(final File file) throws JpegProcessingException {
        return extractMetadataFromJpegSegmentReader(new JpegSegmentReader(file));
    }
    
    public static Metadata extractMetadataFromJpegSegmentReader(final JpegSegmentReader jpegSegmentReader) {
        final Metadata metadata = new Metadata();
        try {
            new ExifReader(jpegSegmentReader.readSegment((byte)(-31))).extract(metadata);
        }
        catch (JpegProcessingException ex) {}
        try {
            new IptcReader(jpegSegmentReader.readSegment((byte)(-19))).extract(metadata);
        }
        catch (JpegProcessingException ex2) {}
        try {
            new JpegReader(jpegSegmentReader.readSegment((byte)(-64))).extract(metadata);
        }
        catch (JpegProcessingException ex3) {}
        try {
            new JpegCommentReader(jpegSegmentReader.readSegment((byte)(-2))).extract(metadata);
        }
        catch (JpegProcessingException ex4) {}
        return metadata;
    }
    
    public static Metadata readMetadata(final JPEGDecodeParam jpegDecodeParam) {
        final Metadata metadata = new Metadata();
        final byte[][] markerData = jpegDecodeParam.getMarkerData(225);
        if (markerData != null && markerData[0].length > 0) {
            new ExifReader(markerData[0]).extract(metadata);
        }
        final byte[][] markerData2 = jpegDecodeParam.getMarkerData(237);
        if (markerData2 != null && markerData2[0].length > 0) {
            new IptcReader(markerData2[0]).extract(metadata);
        }
        final byte[][] markerData3 = jpegDecodeParam.getMarkerData(254);
        if (markerData3 != null && markerData3[0].length > 0) {
            new JpegCommentReader(markerData3[0]).extract(metadata);
        }
        return metadata;
    }
    
    private JpegMetadataReader() throws Exception {
        throw new Exception("Not intended for instantiation");
    }
}
