// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging.jpeg;

import java.util.ArrayList;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.util.List;
import java.util.HashMap;
import java.io.Serializable;

public class JpegSegmentData implements Serializable
{
    static final long serialVersionUID = 7110175216435025451L;
    private final HashMap _segmentDataMap;
    
    public JpegSegmentData() {
        this._segmentDataMap = new HashMap(10);
    }
    
    public void addSegment(final byte b, final byte[] array) {
        this.getOrCreateSegmentList(b).add(array);
    }
    
    public byte[] getSegment(final byte b) {
        return this.getSegment(b, 0);
    }
    
    public byte[] getSegment(final byte b, final int n) {
        final List segmentList = this.getSegmentList(b);
        if (segmentList == null || segmentList.size() <= n) {
            return null;
        }
        return segmentList.get(n);
    }
    
    public int getSegmentCount(final byte b) {
        final List segmentList = this.getSegmentList(b);
        if (segmentList == null) {
            return 0;
        }
        return segmentList.size();
    }
    
    public void removeSegmentOccurrence(final byte value, final int n) {
        this._segmentDataMap.get(new Byte(value)).remove(n);
    }
    
    public void removeSegment(final byte value) {
        this._segmentDataMap.remove(new Byte(value));
    }
    
    public boolean containsSegment(final byte value) {
        return this._segmentDataMap.containsKey(new Byte(value));
    }
    
    public static void ToFile(final File file, final JpegSegmentData obj) throws IOException {
        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
            objectOutputStream.writeObject(obj);
        }
        finally {
            if (objectOutputStream != null) {
                objectOutputStream.close();
            }
        }
    }
    
    public static JpegSegmentData FromFile(final File file) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(file));
            return (JpegSegmentData)objectInputStream.readObject();
        }
        finally {
            if (objectInputStream != null) {
                objectInputStream.close();
            }
        }
    }
    
    private List getSegmentList(final byte value) {
        return this._segmentDataMap.get(new Byte(value));
    }
    
    private List getOrCreateSegmentList(final byte value) {
        final Byte key = new Byte(value);
        List value2;
        if (this._segmentDataMap.containsKey(key)) {
            value2 = this._segmentDataMap.get(key);
        }
        else {
            value2 = new ArrayList();
            this._segmentDataMap.put(key, value2);
        }
        return value2;
    }
}
