// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging.jpeg;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;

public class JpegSegmentReader
{
    private final File _file;
    private final byte[] _data;
    private final InputStream _stream;
    private JpegSegmentData _segmentData;
    private static final byte SEGMENT_SOS = -38;
    private static final byte MARKER_EOI = -39;
    public static final byte SEGMENT_APP0 = -32;
    public static final byte SEGMENT_APP1 = -31;
    public static final byte SEGMENT_APP2 = -30;
    public static final byte SEGMENT_APP3 = -29;
    public static final byte SEGMENT_APP4 = -28;
    public static final byte SEGMENT_APP5 = -27;
    public static final byte SEGMENT_APP6 = -26;
    public static final byte SEGMENT_APP7 = -25;
    public static final byte SEGMENT_APP8 = -24;
    public static final byte SEGMENT_APP9 = -23;
    public static final byte SEGMENT_APPA = -22;
    public static final byte SEGMENT_APPB = -21;
    public static final byte SEGMENT_APPC = -20;
    public static final byte SEGMENT_APPD = -19;
    public static final byte SEGMENT_APPE = -18;
    public static final byte SEGMENT_APPF = -17;
    public static final byte SEGMENT_SOI = -40;
    public static final byte SEGMENT_DQT = -37;
    public static final byte SEGMENT_DHT = -60;
    public static final byte SEGMENT_SOF0 = -64;
    public static final byte SEGMENT_COM = -2;
    
    public JpegSegmentReader(final File file) throws JpegProcessingException {
        this._file = file;
        this._data = null;
        this._stream = null;
        this.readSegments();
    }
    
    public JpegSegmentReader(final byte[] data) throws JpegProcessingException {
        this._file = null;
        this._data = data;
        this._stream = null;
        this.readSegments();
    }
    
    public JpegSegmentReader(final InputStream stream) throws JpegProcessingException {
        this._stream = stream;
        this._file = null;
        this._data = null;
        this.readSegments();
    }
    
    public JpegSegmentReader(final JpegSegmentData segmentData) {
        this._file = null;
        this._data = null;
        this._stream = null;
        this._segmentData = segmentData;
    }
    
    public byte[] readSegment(final byte b) throws JpegProcessingException {
        return this.readSegment(b, 0);
    }
    
    public byte[] readSegment(final byte b, final int n) {
        return this._segmentData.getSegment(b, n);
    }
    
    public final int getSegmentCount(final byte b) {
        return this._segmentData.getSegmentCount(b);
    }
    
    public final JpegSegmentData getSegmentData() {
        return this._segmentData;
    }
    
    private void readSegments() throws JpegProcessingException {
        this._segmentData = new JpegSegmentData();
        final BufferedInputStream jpegInputStream = this.getJpegInputStream();
        try {
            int i = 0;
            if (!this.isValidJpegHeaderBytes(jpegInputStream)) {
                throw new JpegProcessingException("not a jpeg file");
            }
            i += 2;
            while (true) {
                final byte b = (byte)(jpegInputStream.read() & 0xFF);
                if ((b & 0xFF) != 0xFF) {
                    throw new JpegProcessingException("expected jpeg segment start identifier 0xFF at offset " + i + ", not 0x" + Integer.toHexString(b & 0xFF));
                }
                ++i;
                final byte b2 = (byte)(jpegInputStream.read() & 0xFF);
                ++i;
                final byte[] b3 = new byte[2];
                jpegInputStream.read(b3, 0, 2);
                i += 2;
                int len = (b3[0] << 8 & 0xFF00) | (b3[1] & 0xFF);
                len -= 2;
                if (len > jpegInputStream.available()) {
                    throw new JpegProcessingException("segment size would extend beyond file stream length");
                }
                if (len < 0) {
                    throw new JpegProcessingException("segment size would be less than zero");
                }
                final byte[] b4 = new byte[len];
                jpegInputStream.read(b4, 0, len);
                i += len;
                if ((b2 & 0xFF) == 0xDA) {
                    return;
                }
                if ((b2 & 0xFF) == 0xD9) {
                    return;
                }
                this._segmentData.addSegment(b2, b4);
            }
        }
        catch (IOException ex) {
            throw new JpegProcessingException("IOException processing Jpeg file: " + ex.getMessage(), ex);
        }
        finally {
            try {
                if (jpegInputStream != null) {
                    jpegInputStream.close();
                }
            }
            catch (IOException ex2) {
                throw new JpegProcessingException("IOException processing Jpeg file: " + ex2.getMessage(), ex2);
            }
        }
    }
    
    private BufferedInputStream getJpegInputStream() throws JpegProcessingException {
        if (this._stream == null) {
            if (this._data == null) {
                try {
                    final InputStream inputStream = new FileInputStream(this._file);
                    return new BufferedInputStream(inputStream);
                }
                catch (FileNotFoundException ex) {
                    throw new JpegProcessingException("Jpeg file does not exist", ex);
                }
            }
            final InputStream inputStream = new ByteArrayInputStream(this._data);
            return new BufferedInputStream(inputStream);
        }
        if (this._stream instanceof BufferedInputStream) {
            return (BufferedInputStream)this._stream;
        }
        return new BufferedInputStream(this._stream);
    }
    
    private boolean isValidJpegHeaderBytes(final InputStream inputStream) throws IOException {
        final byte[] b = new byte[2];
        inputStream.read(b, 0, 2);
        return (b[0] & 0xFF) == 0xFF && (b[1] & 0xFF) == 0xD8;
    }
}
