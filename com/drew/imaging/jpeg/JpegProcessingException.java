// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.imaging.jpeg;

import com.drew.imaging.ImageProcessingException;

public class JpegProcessingException extends ImageProcessingException
{
    public JpegProcessingException(final String s) {
        super(s);
    }
    
    public JpegProcessingException(final String s, final Throwable t) {
        super(s, t);
    }
    
    public JpegProcessingException(final Throwable t) {
        super(t);
    }
}
