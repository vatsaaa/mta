// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import java.text.DecimalFormat;
import com.drew.lang.Rational;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class NikonType2MakernoteDescriptor extends TagDescriptor
{
    public NikonType2MakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    private NikonType2MakernoteDirectory getMakernoteDirectory() {
        return (NikonType2MakernoteDirectory)this._directory;
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 132: {
                return this.getLensDescription();
            }
            case 146: {
                return this.getHueAdjustmentDescription();
            }
            case 141: {
                return this.getColorModeDescription();
            }
            case 18: {
                return this.getAutoFlashCompensationDescription();
            }
            case 2: {
                return this.getIsoSettingDescription();
            }
            case 134: {
                return this.getDigitalZoomDescription();
            }
            case 136: {
                return this.getAutoFocusPositionDescription();
            }
            case 1: {
                return this.getAutoFirmwareVersionDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getAutoFocusPositionDescription() throws MetadataException {
        if (!this._directory.containsTag(136)) {
            return null;
        }
        final int[] intArray = this._directory.getIntArray(136);
        if (intArray.length != 4 || intArray[0] != 0 || intArray[2] != 0 || intArray[3] != 0) {
            return "Unknown (" + this._directory.getString(136) + ")";
        }
        switch (intArray[1]) {
            case 0: {
                return "Centre";
            }
            case 1: {
                return "Top";
            }
            case 2: {
                return "Bottom";
            }
            case 3: {
                return "Left";
            }
            case 4: {
                return "Right";
            }
            default: {
                return "Unknown (" + intArray[1] + ")";
            }
        }
    }
    
    public String getDigitalZoomDescription() throws MetadataException {
        if (!this._directory.containsTag(134)) {
            return null;
        }
        final Rational rational = this._directory.getRational(134);
        if (rational.intValue() == 1) {
            return "No digital zoom";
        }
        return rational.toSimpleString(true) + "x digital zoom";
    }
    
    public String getIsoSettingDescription() throws MetadataException {
        if (!this._directory.containsTag(2)) {
            return null;
        }
        final int[] intArray = this._directory.getIntArray(2);
        if (intArray[0] != 0 || intArray[1] == 0) {
            return "Unknown (" + this._directory.getString(2) + ")";
        }
        return "ISO " + intArray[1];
    }
    
    public String getAutoFlashCompensationDescription() throws MetadataException {
        final Rational autoFlashCompensation = this.getMakernoteDirectory().getAutoFlashCompensation();
        if (autoFlashCompensation == null) {
            return "Unknown";
        }
        return new DecimalFormat("0.##").format(autoFlashCompensation.floatValue()) + " EV";
    }
    
    public String getLensDescription() throws MetadataException {
        if (!this._directory.containsTag(132)) {
            return null;
        }
        final Rational[] rationalArray = this._directory.getRationalArray(132);
        if (rationalArray.length != 4) {
            return this._directory.getString(132);
        }
        final StringBuffer sb = new StringBuffer();
        sb.append(rationalArray[0].intValue());
        sb.append('-');
        sb.append(rationalArray[1].intValue());
        sb.append("mm f/");
        sb.append(rationalArray[2].floatValue());
        sb.append('-');
        sb.append(rationalArray[3].floatValue());
        return sb.toString();
    }
    
    public String getHueAdjustmentDescription() {
        if (!this._directory.containsTag(146)) {
            return null;
        }
        return this._directory.getString(146) + " degrees";
    }
    
    public String getColorModeDescription() {
        if (!this._directory.containsTag(141)) {
            return null;
        }
        final String string = this._directory.getString(141);
        if (string.startsWith("MODE1")) {
            return "Mode I (sRGB)";
        }
        return string;
    }
    
    public String getAutoFirmwareVersionDescription() throws MetadataException {
        if (!this._directory.containsTag(1)) {
            return null;
        }
        return ExifDescriptor.convertBytesToVersionString(this._directory.getIntArray(1));
    }
}
