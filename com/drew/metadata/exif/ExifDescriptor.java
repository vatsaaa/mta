// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.imaging.PhotographicConversions;
import java.io.UnsupportedEncodingException;
import com.drew.lang.Rational;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import java.text.DecimalFormat;
import com.drew.metadata.TagDescriptor;

public class ExifDescriptor extends TagDescriptor
{
    private boolean _allowDecimalRepresentationOfRationals;
    private static final DecimalFormat SimpleDecimalFormatter;
    
    public ExifDescriptor(final Directory directory) {
        super(directory);
        this._allowDecimalRepresentationOfRationals = true;
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 274: {
                return this.getOrientationDescription();
            }
            case 254: {
                return this.getNewSubfileTypeDescription();
            }
            case 255: {
                return this.getSubfileTypeDescription();
            }
            case 263: {
                return this.getThresholdingDescription();
            }
            case 266: {
                return this.getFillOrderDescription();
            }
            case 296: {
                return this.getResolutionDescription();
            }
            case 531: {
                return this.getYCbCrPositioningDescription();
            }
            case 33434: {
                return this.getExposureTimeDescription();
            }
            case 37377: {
                return this.getShutterSpeedDescription();
            }
            case 33437: {
                return this.getFNumberDescription();
            }
            case 282: {
                return this.getXResolutionDescription();
            }
            case 283: {
                return this.getYResolutionDescription();
            }
            case 513: {
                return this.getThumbnailOffsetDescription();
            }
            case 514: {
                return this.getThumbnailLengthDescription();
            }
            case 37122: {
                return this.getCompressionLevelDescription();
            }
            case 37382: {
                return this.getSubjectDistanceDescription();
            }
            case 37383: {
                return this.getMeteringModeDescription();
            }
            case 37384: {
                return this.getWhiteBalanceDescription();
            }
            case 37385: {
                return this.getFlashDescription();
            }
            case 37386: {
                return this.getFocalLengthDescription();
            }
            case 40961: {
                return this.getColorSpaceDescription();
            }
            case 40962: {
                return this.getExifImageWidthDescription();
            }
            case 40963: {
                return this.getExifImageHeightDescription();
            }
            case 41488: {
                return this.getFocalPlaneResolutionUnitDescription();
            }
            case 41486: {
                return this.getFocalPlaneXResolutionDescription();
            }
            case 41487: {
                return this.getFocalPlaneYResolutionDescription();
            }
            case 256: {
                return this.getThumbnailImageWidthDescription();
            }
            case 257: {
                return this.getThumbnailImageHeightDescription();
            }
            case 258: {
                return this.getBitsPerSampleDescription();
            }
            case 259: {
                return this.getCompressionDescription();
            }
            case 262: {
                return this.getPhotometricInterpretationDescription();
            }
            case 278: {
                return this.getRowsPerStripDescription();
            }
            case 279: {
                return this.getStripByteCountsDescription();
            }
            case 277: {
                return this.getSamplesPerPixelDescription();
            }
            case 284: {
                return this.getPlanarConfigurationDescription();
            }
            case 530: {
                return this.getYCbCrSubsamplingDescription();
            }
            case 34850: {
                return this.getExposureProgramDescription();
            }
            case 37378: {
                return this.getApertureValueDescription();
            }
            case 37381: {
                return this.getMaxApertureValueDescription();
            }
            case 41495: {
                return this.getSensingMethodDescription();
            }
            case 37380: {
                return this.getExposureBiasDescription();
            }
            case 41728: {
                return this.getFileSourceDescription();
            }
            case 41729: {
                return this.getSceneTypeDescription();
            }
            case 37121: {
                return this.getComponentConfigurationDescription();
            }
            case 36864: {
                return this.getExifVersionDescription();
            }
            case 40960: {
                return this.getFlashPixVersionDescription();
            }
            case 532: {
                return this.getReferenceBlackWhiteDescription();
            }
            case 34855: {
                return this.getIsoEquivalentDescription();
            }
            case 61441: {
                return this.getThumbnailDescription();
            }
            case 37510: {
                return this.getUserCommentDescription();
            }
            case 41985: {
                return this.getCustomRenderedDescription();
            }
            case 41986: {
                return this.getExposureModeDescription();
            }
            case 41987: {
                return this.getWhiteBalanceModeDescription();
            }
            case 41988: {
                return this.getDigitalZoomRatioDescription();
            }
            case 41989: {
                return this.get35mmFilmEquivFocalLengthDescription();
            }
            case 41990: {
                return this.getSceneCaptureTypeDescription();
            }
            case 41991: {
                return this.getGainControlDescription();
            }
            case 41992: {
                return this.getContrastDescription();
            }
            case 41993: {
                return this.getSaturationDescription();
            }
            case 41994: {
                return this.getSharpnessDescription();
            }
            case 41996: {
                return this.getSubjectDistanceRangeDescription();
            }
            case 40093: {
                return this.getWindowsAuthorDescription();
            }
            case 40092: {
                return this.getWindowsCommentDescription();
            }
            case 40094: {
                return this.getWindowsKeywordsDescription();
            }
            case 40095: {
                return this.getWindowsSubjectDescription();
            }
            case 40091: {
                return this.getWindowsTitleDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getNewSubfileTypeDescription() throws MetadataException {
        if (!this._directory.containsTag(254)) {
            return null;
        }
        switch (this._directory.getInt(254)) {
            case 1: {
                return "Full-resolution image";
            }
            case 2: {
                return "Reduced-resolution image";
            }
            case 3: {
                return "Single page of multi-page reduced-resolution image";
            }
            case 4: {
                return "Transparency mask";
            }
            case 5: {
                return "Transparency mask of reduced-resolution image";
            }
            case 6: {
                return "Transparency mask of multi-page image";
            }
            case 7: {
                return "Transparency mask of reduced-resolution multi-page image";
            }
            default: {
                return "Unknown (" + this._directory.getInt(254) + ")";
            }
        }
    }
    
    public String getSubfileTypeDescription() throws MetadataException {
        if (!this._directory.containsTag(255)) {
            return null;
        }
        switch (this._directory.getInt(255)) {
            case 1: {
                return "Full-resolution image";
            }
            case 2: {
                return "Reduced-resolution image";
            }
            case 3: {
                return "Single page of multi-page image";
            }
            default: {
                return "Unknown (" + this._directory.getInt(255) + ")";
            }
        }
    }
    
    public String getThresholdingDescription() throws MetadataException {
        if (!this._directory.containsTag(263)) {
            return null;
        }
        switch (this._directory.getInt(263)) {
            case 1: {
                return "No dithering or halftoning";
            }
            case 2: {
                return "Ordered dither or halftone";
            }
            case 3: {
                return "Randomized dither";
            }
            default: {
                return "Unknown (" + this._directory.getInt(263) + ")";
            }
        }
    }
    
    public String getFillOrderDescription() throws MetadataException {
        if (!this._directory.containsTag(266)) {
            return null;
        }
        switch (this._directory.getInt(266)) {
            case 1: {
                return "Normal";
            }
            case 2: {
                return "Reversed";
            }
            default: {
                return "Unknown (" + this._directory.getInt(266) + ")";
            }
        }
    }
    
    public String getSubjectDistanceRangeDescription() throws MetadataException {
        if (!this._directory.containsTag(41996)) {
            return null;
        }
        switch (this._directory.getInt(41996)) {
            case 0: {
                return "Unknown";
            }
            case 1: {
                return "Macro";
            }
            case 2: {
                return "Close view";
            }
            case 3: {
                return "Distant view";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41996) + ")";
            }
        }
    }
    
    public String getSharpnessDescription() throws MetadataException {
        if (!this._directory.containsTag(41994)) {
            return null;
        }
        switch (this._directory.getInt(41994)) {
            case 0: {
                return "None";
            }
            case 1: {
                return "Low";
            }
            case 2: {
                return "Hard";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41994) + ")";
            }
        }
    }
    
    public String getSaturationDescription() throws MetadataException {
        if (!this._directory.containsTag(41993)) {
            return null;
        }
        switch (this._directory.getInt(41993)) {
            case 0: {
                return "None";
            }
            case 1: {
                return "Low saturation";
            }
            case 2: {
                return "High saturation";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41993) + ")";
            }
        }
    }
    
    public String getContrastDescription() throws MetadataException {
        if (!this._directory.containsTag(41992)) {
            return null;
        }
        switch (this._directory.getInt(41992)) {
            case 0: {
                return "None";
            }
            case 1: {
                return "Soft";
            }
            case 2: {
                return "Hard";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41992) + ")";
            }
        }
    }
    
    public String getGainControlDescription() throws MetadataException {
        if (!this._directory.containsTag(41991)) {
            return null;
        }
        switch (this._directory.getInt(41991)) {
            case 0: {
                return "None";
            }
            case 1: {
                return "Low gain up";
            }
            case 2: {
                return "Low gain down";
            }
            case 3: {
                return "High gain up";
            }
            case 4: {
                return "High gain down";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41991) + ")";
            }
        }
    }
    
    public String getSceneCaptureTypeDescription() throws MetadataException {
        if (!this._directory.containsTag(41990)) {
            return null;
        }
        switch (this._directory.getInt(41990)) {
            case 0: {
                return "Standard";
            }
            case 1: {
                return "Landscape";
            }
            case 2: {
                return "Portrait";
            }
            case 3: {
                return "Night scene";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41990) + ")";
            }
        }
    }
    
    public String get35mmFilmEquivFocalLengthDescription() throws MetadataException {
        if (!this._directory.containsTag(41989)) {
            return null;
        }
        final int int1 = this._directory.getInt(41989);
        if (int1 == 0) {
            return "Unknown";
        }
        return ExifDescriptor.SimpleDecimalFormatter.format(int1) + "mm";
    }
    
    public String getDigitalZoomRatioDescription() throws MetadataException {
        if (!this._directory.containsTag(41988)) {
            return null;
        }
        final Rational rational = this._directory.getRational(41988);
        if (rational.getNumerator() == 0) {
            return "Digital zoom not used.";
        }
        return ExifDescriptor.SimpleDecimalFormatter.format(rational.doubleValue());
    }
    
    public String getWhiteBalanceModeDescription() throws MetadataException {
        if (!this._directory.containsTag(41987)) {
            return null;
        }
        switch (this._directory.getInt(41987)) {
            case 0: {
                return "Auto white balance";
            }
            case 1: {
                return "Manual white balance";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41987) + ")";
            }
        }
    }
    
    public String getExposureModeDescription() throws MetadataException {
        if (!this._directory.containsTag(41986)) {
            return null;
        }
        switch (this._directory.getInt(41986)) {
            case 0: {
                return "Auto exposure";
            }
            case 1: {
                return "Manual exposure";
            }
            case 2: {
                return "Auto bracket";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41986) + ")";
            }
        }
    }
    
    public String getCustomRenderedDescription() throws MetadataException {
        if (!this._directory.containsTag(41985)) {
            return null;
        }
        switch (this._directory.getInt(41985)) {
            case 0: {
                return "Normal process";
            }
            case 1: {
                return "Custom process";
            }
            default: {
                return "Unknown (" + this._directory.getInt(41985) + ")";
            }
        }
    }
    
    public String getUserCommentDescription() throws MetadataException {
        if (!this._directory.containsTag(37510)) {
            return null;
        }
        final byte[] byteArray = this._directory.getByteArray(37510);
        if (byteArray.length == 0) {
            return "";
        }
        final String[] array = { "ASCII", "UNICODE", "JIS" };
        if (byteArray.length >= 10) {
            final String s = new String(byteArray, 0, 10);
            for (int i = 0; i < array.length; ++i) {
                final String prefix = array[i];
                if (s.startsWith(prefix)) {
                    for (int j = prefix.length(); j < 10; ++j) {
                        final byte b = byteArray[j];
                        if (b != 0 && b != 32) {
                            if (prefix.equals("UNICODE")) {
                                try {
                                    return new String(byteArray, j, byteArray.length - j, "UTF-16LE").trim();
                                }
                                catch (UnsupportedEncodingException ex) {
                                    return null;
                                }
                            }
                            return new String(byteArray, j, byteArray.length - j).trim();
                        }
                    }
                    return new String(byteArray, 10, byteArray.length - 10).trim();
                }
            }
        }
        return new String(byteArray).trim();
    }
    
    public String getThumbnailDescription() throws MetadataException {
        if (!this._directory.containsTag(61441)) {
            return null;
        }
        return "[" + this._directory.getIntArray(61441).length + " bytes of thumbnail data]";
    }
    
    public String getIsoEquivalentDescription() throws MetadataException {
        if (!this._directory.containsTag(34855)) {
            return null;
        }
        int int1 = this._directory.getInt(34855);
        if (int1 < 50) {
            int1 *= 200;
        }
        return Integer.toString(int1);
    }
    
    public String getReferenceBlackWhiteDescription() throws MetadataException {
        if (!this._directory.containsTag(532)) {
            return null;
        }
        final int[] intArray = this._directory.getIntArray(532);
        return "[" + intArray[0] + "," + intArray[2] + "," + intArray[4] + "] " + "[" + intArray[1] + "," + intArray[3] + "," + intArray[5] + "]";
    }
    
    public String getExifVersionDescription() throws MetadataException {
        if (!this._directory.containsTag(36864)) {
            return null;
        }
        return convertBytesToVersionString(this._directory.getIntArray(36864));
    }
    
    public String getFlashPixVersionDescription() throws MetadataException {
        if (!this._directory.containsTag(40960)) {
            return null;
        }
        return convertBytesToVersionString(this._directory.getIntArray(40960));
    }
    
    public String getSceneTypeDescription() throws MetadataException {
        if (!this._directory.containsTag(41729)) {
            return null;
        }
        final int int1 = this._directory.getInt(41729);
        if (int1 == 1) {
            return "Directly photographed image";
        }
        return "Unknown (" + int1 + ")";
    }
    
    public String getFileSourceDescription() throws MetadataException {
        if (!this._directory.containsTag(41728)) {
            return null;
        }
        final int int1 = this._directory.getInt(41728);
        if (int1 == 3) {
            return "Digital Still Camera (DSC)";
        }
        return "Unknown (" + int1 + ")";
    }
    
    public String getExposureBiasDescription() throws MetadataException {
        if (!this._directory.containsTag(37380)) {
            return null;
        }
        return this._directory.getRational(37380).toSimpleString(true) + " EV";
    }
    
    public String getMaxApertureValueDescription() throws MetadataException {
        if (!this._directory.containsTag(37381)) {
            return null;
        }
        return "F" + ExifDescriptor.SimpleDecimalFormatter.format(PhotographicConversions.apertureToFStop(this._directory.getDouble(37381)));
    }
    
    public String getApertureValueDescription() throws MetadataException {
        if (!this._directory.containsTag(37378)) {
            return null;
        }
        return "F" + ExifDescriptor.SimpleDecimalFormatter.format(PhotographicConversions.apertureToFStop(this._directory.getDouble(37378)));
    }
    
    public String getExposureProgramDescription() throws MetadataException {
        if (!this._directory.containsTag(34850)) {
            return null;
        }
        switch (this._directory.getInt(34850)) {
            case 1: {
                return "Manual control";
            }
            case 2: {
                return "Program normal";
            }
            case 3: {
                return "Aperture priority";
            }
            case 4: {
                return "Shutter priority";
            }
            case 5: {
                return "Program creative (slow program)";
            }
            case 6: {
                return "Program action (high-speed program)";
            }
            case 7: {
                return "Portrait mode";
            }
            case 8: {
                return "Landscape mode";
            }
            default: {
                return "Unknown program (" + this._directory.getInt(34850) + ")";
            }
        }
    }
    
    public String getYCbCrSubsamplingDescription() throws MetadataException {
        if (!this._directory.containsTag(530)) {
            return null;
        }
        final int[] intArray = this._directory.getIntArray(530);
        if (intArray[0] == 2 && intArray[1] == 1) {
            return "YCbCr4:2:2";
        }
        if (intArray[0] == 2 && intArray[1] == 2) {
            return "YCbCr4:2:0";
        }
        return "(Unknown)";
    }
    
    public String getPlanarConfigurationDescription() throws MetadataException {
        if (!this._directory.containsTag(284)) {
            return null;
        }
        switch (this._directory.getInt(284)) {
            case 1: {
                return "Chunky (contiguous for each subsampling pixel)";
            }
            case 2: {
                return "Separate (Y-plane/Cb-plane/Cr-plane format)";
            }
            default: {
                return "Unknown configuration";
            }
        }
    }
    
    public String getSamplesPerPixelDescription() {
        if (!this._directory.containsTag(277)) {
            return null;
        }
        return this._directory.getString(277) + " samples/pixel";
    }
    
    public String getRowsPerStripDescription() {
        if (!this._directory.containsTag(278)) {
            return null;
        }
        return this._directory.getString(278) + " rows/strip";
    }
    
    public String getStripByteCountsDescription() {
        if (!this._directory.containsTag(279)) {
            return null;
        }
        return this._directory.getString(279) + " bytes";
    }
    
    public String getPhotometricInterpretationDescription() throws MetadataException {
        if (!this._directory.containsTag(262)) {
            return null;
        }
        switch (this._directory.getInt(262)) {
            case 0: {
                return "WhiteIsZero";
            }
            case 1: {
                return "BlackIsZero";
            }
            case 2: {
                return "RGB";
            }
            case 3: {
                return "RGB Palette";
            }
            case 4: {
                return "Transparency Mask";
            }
            case 5: {
                return "CMYK";
            }
            case 6: {
                return "YCbCr";
            }
            case 8: {
                return "CIELab";
            }
            case 9: {
                return "ICCLab";
            }
            case 10: {
                return "ITULab";
            }
            case 32803: {
                return "Color Filter Array";
            }
            case 32844: {
                return "Pixar LogL";
            }
            case 32845: {
                return "Pixar LogLuv";
            }
            case 32892: {
                return "Linear Raw";
            }
            default: {
                return "Unknown colour space";
            }
        }
    }
    
    public String getCompressionDescription() throws MetadataException {
        if (!this._directory.containsTag(259)) {
            return null;
        }
        switch (this._directory.getInt(259)) {
            case 1: {
                return "Uncompressed";
            }
            case 2: {
                return "CCITT 1D";
            }
            case 3: {
                return "T4/Group 3 Fax";
            }
            case 4: {
                return "T6/Group 4 Fax";
            }
            case 5: {
                return "LZW";
            }
            case 6: {
                return "JPEG (old-style)";
            }
            case 7: {
                return "JPEG";
            }
            case 8: {
                return "Adobe Deflate";
            }
            case 9: {
                return "JBIG B&W";
            }
            case 10: {
                return "JBIG Color";
            }
            case 32766: {
                return "Next";
            }
            case 32771: {
                return "CCIRLEW";
            }
            case 32773: {
                return "PackBits";
            }
            case 32809: {
                return "Thunderscan";
            }
            case 32895: {
                return "IT8CTPAD";
            }
            case 32896: {
                return "IT8LW";
            }
            case 32897: {
                return "IT8MP";
            }
            case 32898: {
                return "IT8BL";
            }
            case 32908: {
                return "PixarFilm";
            }
            case 32909: {
                return "PixarLog";
            }
            case 32946: {
                return "Deflate";
            }
            case 32947: {
                return "DCS";
            }
            case 32661: {
                return "JBIG";
            }
            case 32676: {
                return "SGILog";
            }
            case 32677: {
                return "SGILog24";
            }
            case 32712: {
                return "JPEG 2000";
            }
            case 32713: {
                return "Nikon NEF Compressed";
            }
            default: {
                return "Unknown compression";
            }
        }
    }
    
    public String getBitsPerSampleDescription() {
        if (!this._directory.containsTag(258)) {
            return null;
        }
        return this._directory.getString(258) + " bits/component/pixel";
    }
    
    public String getThumbnailImageWidthDescription() {
        if (!this._directory.containsTag(256)) {
            return null;
        }
        return this._directory.getString(256) + " pixels";
    }
    
    public String getThumbnailImageHeightDescription() {
        if (!this._directory.containsTag(257)) {
            return null;
        }
        return this._directory.getString(257) + " pixels";
    }
    
    public String getFocalPlaneXResolutionDescription() throws MetadataException {
        if (!this._directory.containsTag(41486)) {
            return null;
        }
        return this._directory.getRational(41486).getReciprocal().toSimpleString(this._allowDecimalRepresentationOfRationals) + " " + this.getFocalPlaneResolutionUnitDescription().toLowerCase();
    }
    
    public String getFocalPlaneYResolutionDescription() throws MetadataException {
        if (!this._directory.containsTag(41487)) {
            return null;
        }
        return this._directory.getRational(41487).getReciprocal().toSimpleString(this._allowDecimalRepresentationOfRationals) + " " + this.getFocalPlaneResolutionUnitDescription().toLowerCase();
    }
    
    public String getFocalPlaneResolutionUnitDescription() throws MetadataException {
        if (!this._directory.containsTag(41488)) {
            return null;
        }
        switch (this._directory.getInt(41488)) {
            case 1: {
                return "(No unit)";
            }
            case 2: {
                return "Inches";
            }
            case 3: {
                return "cm";
            }
            default: {
                return "";
            }
        }
    }
    
    public String getExifImageWidthDescription() throws MetadataException {
        if (!this._directory.containsTag(40962)) {
            return null;
        }
        return this._directory.getInt(40962) + " pixels";
    }
    
    public String getExifImageHeightDescription() throws MetadataException {
        if (!this._directory.containsTag(40963)) {
            return null;
        }
        return this._directory.getInt(40963) + " pixels";
    }
    
    public String getColorSpaceDescription() throws MetadataException {
        if (!this._directory.containsTag(40961)) {
            return null;
        }
        final int int1 = this._directory.getInt(40961);
        if (int1 == 1) {
            return "sRGB";
        }
        if (int1 == 65535) {
            return "Undefined";
        }
        return "Unknown";
    }
    
    public String getFocalLengthDescription() throws MetadataException {
        if (!this._directory.containsTag(37386)) {
            return null;
        }
        return new DecimalFormat("0.0##").format(this._directory.getRational(37386).doubleValue()) + " mm";
    }
    
    public String getFlashDescription() throws MetadataException {
        if (!this._directory.containsTag(37385)) {
            return null;
        }
        final int int1 = this._directory.getInt(37385);
        final StringBuffer sb = new StringBuffer();
        if ((int1 & 0x1) != 0x0) {
            sb.append("Flash fired");
        }
        else {
            sb.append("Flash did not fire");
        }
        if ((int1 & 0x4) != 0x0) {
            if ((int1 & 0x2) != 0x0) {
                sb.append(", return detected");
            }
            else {
                sb.append(", return not detected");
            }
        }
        if ((int1 & 0x10) != 0x0) {
            sb.append(", auto");
        }
        if ((int1 & 0x40) != 0x0) {
            sb.append(", red-eye reduction");
        }
        return sb.toString();
    }
    
    public String getWhiteBalanceDescription() throws MetadataException {
        if (!this._directory.containsTag(37384)) {
            return null;
        }
        switch (this._directory.getInt(37384)) {
            case 0: {
                return "Unknown";
            }
            case 1: {
                return "Daylight";
            }
            case 2: {
                return "Flourescent";
            }
            case 3: {
                return "Tungsten";
            }
            case 10: {
                return "Flash";
            }
            case 17: {
                return "Standard light";
            }
            case 18: {
                return "Standard light (B)";
            }
            case 19: {
                return "Standard light (C)";
            }
            case 20: {
                return "D55";
            }
            case 21: {
                return "D65";
            }
            case 22: {
                return "D75";
            }
            case 255: {
                return "(Other)";
            }
            default: {
                return "Unknown (" + this._directory.getInt(37384) + ")";
            }
        }
    }
    
    public String getMeteringModeDescription() throws MetadataException {
        if (!this._directory.containsTag(37383)) {
            return null;
        }
        switch (this._directory.getInt(37383)) {
            case 0: {
                return "Unknown";
            }
            case 1: {
                return "Average";
            }
            case 2: {
                return "Center weighted average";
            }
            case 3: {
                return "Spot";
            }
            case 4: {
                return "Multi-spot";
            }
            case 5: {
                return "Multi-segment";
            }
            case 6: {
                return "Partial";
            }
            case 255: {
                return "(Other)";
            }
            default: {
                return "";
            }
        }
    }
    
    public String getSubjectDistanceDescription() throws MetadataException {
        if (!this._directory.containsTag(37382)) {
            return null;
        }
        return new DecimalFormat("0.0##").format(this._directory.getRational(37382).doubleValue()) + " metres";
    }
    
    public String getCompressionLevelDescription() throws MetadataException {
        if (!this._directory.containsTag(37122)) {
            return null;
        }
        final Rational rational = this._directory.getRational(37122);
        final String simpleString = rational.toSimpleString(this._allowDecimalRepresentationOfRationals);
        if (rational.isInteger() && rational.intValue() == 1) {
            return simpleString + " bit/pixel";
        }
        return simpleString + " bits/pixel";
    }
    
    public String getThumbnailLengthDescription() {
        if (!this._directory.containsTag(514)) {
            return null;
        }
        return this._directory.getString(514) + " bytes";
    }
    
    public String getThumbnailOffsetDescription() {
        if (!this._directory.containsTag(513)) {
            return null;
        }
        return this._directory.getString(513) + " bytes";
    }
    
    public String getYResolutionDescription() throws MetadataException {
        if (!this._directory.containsTag(283)) {
            return null;
        }
        return this._directory.getRational(283).toSimpleString(this._allowDecimalRepresentationOfRationals) + " dots per " + this.getResolutionDescription().toLowerCase();
    }
    
    public String getXResolutionDescription() throws MetadataException {
        if (!this._directory.containsTag(282)) {
            return null;
        }
        return this._directory.getRational(282).toSimpleString(this._allowDecimalRepresentationOfRationals) + " dots per " + this.getResolutionDescription().toLowerCase();
    }
    
    public String getExposureTimeDescription() {
        if (!this._directory.containsTag(33434)) {
            return null;
        }
        return this._directory.getString(33434) + " sec";
    }
    
    public String getShutterSpeedDescription() throws MetadataException {
        if (!this._directory.containsTag(37377)) {
            return null;
        }
        final float float1 = this._directory.getFloat(37377);
        if (float1 <= 1.0f) {
            return Math.round((float)(1.0 / Math.exp(float1 * Math.log(2.0))) * 10.0) / 10.0f + " sec";
        }
        return "1/" + (int)Math.exp(float1 * Math.log(2.0)) + " sec";
    }
    
    public String getFNumberDescription() throws MetadataException {
        if (!this._directory.containsTag(33437)) {
            return null;
        }
        return "F" + ExifDescriptor.SimpleDecimalFormatter.format(this._directory.getRational(33437).doubleValue());
    }
    
    public String getYCbCrPositioningDescription() throws MetadataException {
        if (!this._directory.containsTag(531)) {
            return null;
        }
        final int int1 = this._directory.getInt(531);
        switch (int1) {
            case 1: {
                return "Center of pixel array";
            }
            case 2: {
                return "Datum point";
            }
            default: {
                return String.valueOf(int1);
            }
        }
    }
    
    public String getOrientationDescription() throws MetadataException {
        if (!this._directory.containsTag(274)) {
            return null;
        }
        final int int1 = this._directory.getInt(274);
        switch (int1) {
            case 1: {
                return "Top, left side (Horizontal / normal)";
            }
            case 2: {
                return "Top, right side (Mirror horizontal)";
            }
            case 3: {
                return "Bottom, right side (Rotate 180)";
            }
            case 4: {
                return "Bottom, left side (Mirror vertical)";
            }
            case 5: {
                return "Left side, top (Mirror horizontal and rotate 270 CW)";
            }
            case 6: {
                return "Right side, top (Rotate 90 CW)";
            }
            case 7: {
                return "Right side, bottom (Mirror horizontal and rotate 90 CW)";
            }
            case 8: {
                return "Left side, bottom (Rotate 270 CW)";
            }
            default: {
                return String.valueOf(int1);
            }
        }
    }
    
    public String getResolutionDescription() throws MetadataException {
        if (!this._directory.containsTag(296)) {
            return "";
        }
        switch (this._directory.getInt(296)) {
            case 1: {
                return "(No unit)";
            }
            case 2: {
                return "Inch";
            }
            case 3: {
                return "cm";
            }
            default: {
                return "";
            }
        }
    }
    
    public String getSensingMethodDescription() throws MetadataException {
        if (!this._directory.containsTag(41495)) {
            return null;
        }
        switch (this._directory.getInt(41495)) {
            case 1: {
                return "(Not defined)";
            }
            case 2: {
                return "One-chip color area sensor";
            }
            case 3: {
                return "Two-chip color area sensor";
            }
            case 4: {
                return "Three-chip color area sensor";
            }
            case 5: {
                return "Color sequential area sensor";
            }
            case 7: {
                return "Trilinear sensor";
            }
            case 8: {
                return "Color sequential linear sensor";
            }
            default: {
                return "";
            }
        }
    }
    
    public String getComponentConfigurationDescription() throws MetadataException {
        final int[] intArray = this._directory.getIntArray(37121);
        final String[] array = { "", "Y", "Cb", "Cr", "R", "G", "B" };
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < Math.min(4, intArray.length); ++i) {
            final int n = intArray[i];
            if (n > 0 && n < array.length) {
                sb.append(array[n]);
            }
        }
        return sb.toString();
    }
    
    public static String convertBytesToVersionString(final int[] array) {
        final StringBuffer sb = new StringBuffer();
        for (int n = 0; n < 4 && n < array.length; ++n) {
            if (n == 2) {
                sb.append('.');
            }
            final String value = String.valueOf((char)array[n]);
            if (n != 0 || !"0".equals(value)) {
                sb.append(value);
            }
        }
        return sb.toString();
    }
    
    private String getUnicodeDescription(final int n) throws MetadataException {
        if (!this._directory.containsTag(n)) {
            return null;
        }
        final byte[] byteArray = this._directory.getByteArray(n);
        try {
            return new String(byteArray, "UTF-16LE").trim();
        }
        catch (UnsupportedEncodingException ex) {
            return null;
        }
    }
    
    public String getWindowsAuthorDescription() throws MetadataException {
        return this.getUnicodeDescription(40093);
    }
    
    public String getWindowsCommentDescription() throws MetadataException {
        return this.getUnicodeDescription(40092);
    }
    
    public String getWindowsKeywordsDescription() throws MetadataException {
        return this.getUnicodeDescription(40094);
    }
    
    public String getWindowsTitleDescription() throws MetadataException {
        return this.getUnicodeDescription(40091);
    }
    
    public String getWindowsSubjectDescription() throws MetadataException {
        return this.getUnicodeDescription(40095);
    }
    
    static {
        SimpleDecimalFormatter = new DecimalFormat("0.#");
    }
}
