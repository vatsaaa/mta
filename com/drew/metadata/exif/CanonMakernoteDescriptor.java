// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class CanonMakernoteDescriptor extends TagDescriptor
{
    public CanonMakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 49436: {
                return this.getFlashActivityDescription();
            }
            case 49426: {
                return this.getFocusTypeDescription();
            }
            case 49420: {
                return this.getDigitalZoomDescription();
            }
            case 49411: {
                return this.getQualityDescription();
            }
            case 49409: {
                return this.getMacroModeDescription();
            }
            case 49410: {
                return this.getSelfTimerDelayDescription();
            }
            case 49412: {
                return this.getFlashModeDescription();
            }
            case 49413: {
                return this.getContinuousDriveModeDescription();
            }
            case 49415: {
                return this.getFocusMode1Description();
            }
            case 49418: {
                return this.getImageSizeDescription();
            }
            case 49419: {
                return this.getEasyShootingModeDescription();
            }
            case 49421: {
                return this.getContrastDescription();
            }
            case 49422: {
                return this.getSaturationDescription();
            }
            case 49423: {
                return this.getSharpnessDescription();
            }
            case 49424: {
                return this.getIsoDescription();
            }
            case 49425: {
                return this.getMeteringModeDescription();
            }
            case 49427: {
                return this.getAfPointSelectedDescription();
            }
            case 49428: {
                return this.getExposureModeDescription();
            }
            case 49431: {
                return this.getLongFocalLengthDescription();
            }
            case 49432: {
                return this.getShortFocalLengthDescription();
            }
            case 49433: {
                return this.getFocalUnitsPerMillimetreDescription();
            }
            case 49437: {
                return this.getFlashDetailsDescription();
            }
            case 49440: {
                return this.getFocusMode2Description();
            }
            case 49671: {
                return this.getWhiteBalanceDescription();
            }
            case 49678: {
                return this.getAfPointUsedDescription();
            }
            case 49679: {
                return this.getFlashBiasDescription();
            }
            case 49921: {
                return this.getLongExposureNoiseReductionDescription();
            }
            case 49922: {
                return this.getShutterAutoExposureLockButtonDescription();
            }
            case 49923: {
                return this.getMirrorLockupDescription();
            }
            case 49924: {
                return this.getTvAndAvExposureLevelDescription();
            }
            case 49925: {
                return this.getAutoFocusAssistLightDescription();
            }
            case 49926: {
                return this.getShutterSpeedInAvModeDescription();
            }
            case 49927: {
                return this.getAutoExposureBrackettingSequenceAndAutoCancellationDescription();
            }
            case 49928: {
                return this.getShutterCurtainSyncDescription();
            }
            case 49929: {
                return this.getLensAutoFocusStopButtonDescription();
            }
            case 49930: {
                return this.getFillFlashReductionDescription();
            }
            case 49931: {
                return this.getMenuButtonReturnPositionDescription();
            }
            case 49932: {
                return this.getSetButtonFunctionWhenShootingDescription();
            }
            case 49933: {
                return this.getSensorCleaningDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getLongExposureNoiseReductionDescription() throws MetadataException {
        if (!this._directory.containsTag(49921)) {
            return null;
        }
        final int int1 = this._directory.getInt(49921);
        switch (int1) {
            case 0: {
                return "Off";
            }
            case 1: {
                return "On";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getShutterAutoExposureLockButtonDescription() throws MetadataException {
        if (!this._directory.containsTag(49922)) {
            return null;
        }
        final int int1 = this._directory.getInt(49922);
        switch (int1) {
            case 0: {
                return "AF/AE lock";
            }
            case 1: {
                return "AE lock/AF";
            }
            case 2: {
                return "AF/AF lock";
            }
            case 3: {
                return "AE+release/AE+AF";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getMirrorLockupDescription() throws MetadataException {
        if (!this._directory.containsTag(49923)) {
            return null;
        }
        final int int1 = this._directory.getInt(49923);
        switch (int1) {
            case 0: {
                return "Disabled";
            }
            case 1: {
                return "Enabled";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getTvAndAvExposureLevelDescription() throws MetadataException {
        if (!this._directory.containsTag(49924)) {
            return null;
        }
        final int int1 = this._directory.getInt(49924);
        switch (int1) {
            case 0: {
                return "1/2 stop";
            }
            case 1: {
                return "1/3 stop";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getAutoFocusAssistLightDescription() throws MetadataException {
        if (!this._directory.containsTag(49925)) {
            return null;
        }
        final int int1 = this._directory.getInt(49925);
        switch (int1) {
            case 0: {
                return "On (Auto)";
            }
            case 1: {
                return "Off";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getShutterSpeedInAvModeDescription() throws MetadataException {
        if (!this._directory.containsTag(49926)) {
            return null;
        }
        final int int1 = this._directory.getInt(49926);
        switch (int1) {
            case 0: {
                return "Automatic";
            }
            case 1: {
                return "1/200 (fixed)";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getAutoExposureBrackettingSequenceAndAutoCancellationDescription() throws MetadataException {
        if (!this._directory.containsTag(49927)) {
            return null;
        }
        final int int1 = this._directory.getInt(49927);
        switch (int1) {
            case 0: {
                return "0,-,+ / Enabled";
            }
            case 1: {
                return "0,-,+ / Disabled";
            }
            case 2: {
                return "-,0,+ / Enabled";
            }
            case 3: {
                return "-,0,+ / Disabled";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getShutterCurtainSyncDescription() throws MetadataException {
        if (!this._directory.containsTag(49928)) {
            return null;
        }
        final int int1 = this._directory.getInt(49928);
        switch (int1) {
            case 0: {
                return "1st Curtain Sync";
            }
            case 1: {
                return "2nd Curtain Sync";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getLensAutoFocusStopButtonDescription() throws MetadataException {
        if (!this._directory.containsTag(49929)) {
            return null;
        }
        final int int1 = this._directory.getInt(49929);
        switch (int1) {
            case 0: {
                return "AF stop";
            }
            case 1: {
                return "Operate AF";
            }
            case 2: {
                return "Lock AE and start timer";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFillFlashReductionDescription() throws MetadataException {
        if (!this._directory.containsTag(49930)) {
            return null;
        }
        final int int1 = this._directory.getInt(49930);
        switch (int1) {
            case 0: {
                return "Enabled";
            }
            case 1: {
                return "Disabled";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getMenuButtonReturnPositionDescription() throws MetadataException {
        if (!this._directory.containsTag(49931)) {
            return null;
        }
        final int int1 = this._directory.getInt(49931);
        switch (int1) {
            case 0: {
                return "Top";
            }
            case 1: {
                return "Previous (volatile)";
            }
            case 2: {
                return "Previous";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSetButtonFunctionWhenShootingDescription() throws MetadataException {
        if (!this._directory.containsTag(49932)) {
            return null;
        }
        final int int1 = this._directory.getInt(49932);
        switch (int1) {
            case 0: {
                return "Not Assigned";
            }
            case 1: {
                return "Change Quality";
            }
            case 2: {
                return "Change ISO Speed";
            }
            case 3: {
                return "Select Parameters";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSensorCleaningDescription() throws MetadataException {
        if (!this._directory.containsTag(49933)) {
            return null;
        }
        final int int1 = this._directory.getInt(49933);
        switch (int1) {
            case 0: {
                return "Disabled";
            }
            case 1: {
                return "Enabled";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashBiasDescription() throws MetadataException {
        if (!this._directory.containsTag(49679)) {
            return null;
        }
        int int1 = this._directory.getInt(49679);
        boolean b = false;
        if (int1 > 61440) {
            b = true;
            int1 = 65535 - int1;
            ++int1;
        }
        return (b ? "-" : "") + Float.toString(int1 / 32.0f) + " EV";
    }
    
    public String getAfPointUsedDescription() throws MetadataException {
        if (!this._directory.containsTag(49678)) {
            return null;
        }
        final int int1 = this._directory.getInt(49678);
        if ((int1 & 0x7) == 0x0) {
            return "Right";
        }
        if ((int1 & 0x7) == 0x1) {
            return "Centre";
        }
        if ((int1 & 0x7) == 0x2) {
            return "Left";
        }
        return "Unknown (" + int1 + ")";
    }
    
    public String getWhiteBalanceDescription() throws MetadataException {
        if (!this._directory.containsTag(49671)) {
            return null;
        }
        final int int1 = this._directory.getInt(49671);
        switch (int1) {
            case 0: {
                return "Auto";
            }
            case 1: {
                return "Sunny";
            }
            case 2: {
                return "Cloudy";
            }
            case 3: {
                return "Tungsten";
            }
            case 4: {
                return "Flourescent";
            }
            case 5: {
                return "Flash";
            }
            case 6: {
                return "Custom";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusMode2Description() throws MetadataException {
        if (!this._directory.containsTag(49440)) {
            return null;
        }
        final int int1 = this._directory.getInt(49440);
        switch (int1) {
            case 0: {
                return "Single";
            }
            case 1: {
                return "Continuous";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashDetailsDescription() throws MetadataException {
        if (!this._directory.containsTag(49437)) {
            return null;
        }
        final int int1 = this._directory.getInt(49437);
        if ((int1 << 14 & 0x1) > 0) {
            return "External E-TTL";
        }
        if ((int1 << 13 & 0x1) > 0) {
            return "Internal flash";
        }
        if ((int1 << 11 & 0x1) > 0) {
            return "FP sync used";
        }
        if ((int1 << 4 & 0x1) > 0) {
            return "FP sync enabled";
        }
        return "Unknown (" + int1 + ")";
    }
    
    public String getFocalUnitsPerMillimetreDescription() throws MetadataException {
        if (!this._directory.containsTag(49433)) {
            return "";
        }
        final int int1 = this._directory.getInt(49433);
        if (int1 != 0) {
            return Integer.toString(int1);
        }
        return "";
    }
    
    public String getShortFocalLengthDescription() throws MetadataException {
        if (!this._directory.containsTag(49432)) {
            return null;
        }
        return Integer.toString(this._directory.getInt(49432)) + " " + this.getFocalUnitsPerMillimetreDescription();
    }
    
    public String getLongFocalLengthDescription() throws MetadataException {
        if (!this._directory.containsTag(49431)) {
            return null;
        }
        return Integer.toString(this._directory.getInt(49431)) + " " + this.getFocalUnitsPerMillimetreDescription();
    }
    
    public String getExposureModeDescription() throws MetadataException {
        if (!this._directory.containsTag(49428)) {
            return null;
        }
        final int int1 = this._directory.getInt(49428);
        switch (int1) {
            case 0: {
                return "Easy shooting";
            }
            case 1: {
                return "Program";
            }
            case 2: {
                return "Tv-priority";
            }
            case 3: {
                return "Av-priority";
            }
            case 4: {
                return "Manual";
            }
            case 5: {
                return "A-DEP";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getAfPointSelectedDescription() throws MetadataException {
        if (!this._directory.containsTag(49427)) {
            return null;
        }
        final int int1 = this._directory.getInt(49427);
        switch (int1) {
            case 12288: {
                return "None (MF)";
            }
            case 12289: {
                return "Auto selected";
            }
            case 12290: {
                return "Right";
            }
            case 12291: {
                return "Centre";
            }
            case 12292: {
                return "Left";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getMeteringModeDescription() throws MetadataException {
        if (!this._directory.containsTag(49425)) {
            return null;
        }
        final int int1 = this._directory.getInt(49425);
        switch (int1) {
            case 3: {
                return "Evaluative";
            }
            case 4: {
                return "Partial";
            }
            case 5: {
                return "Centre weighted";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getIsoDescription() throws MetadataException {
        if (!this._directory.containsTag(49424)) {
            return null;
        }
        final int int1 = this._directory.getInt(49424);
        switch (int1) {
            case 0: {
                return "Not specified (see ISOSpeedRatings tag)";
            }
            case 15: {
                return "Auto";
            }
            case 16: {
                return "50";
            }
            case 17: {
                return "100";
            }
            case 18: {
                return "200";
            }
            case 19: {
                return "400";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSharpnessDescription() throws MetadataException {
        if (!this._directory.containsTag(49423)) {
            return null;
        }
        final int int1 = this._directory.getInt(49423);
        switch (int1) {
            case 65535: {
                return "Low";
            }
            case 0: {
                return "Normal";
            }
            case 1: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSaturationDescription() throws MetadataException {
        if (!this._directory.containsTag(49422)) {
            return null;
        }
        final int int1 = this._directory.getInt(49422);
        switch (int1) {
            case 65535: {
                return "Low";
            }
            case 0: {
                return "Normal";
            }
            case 1: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getContrastDescription() throws MetadataException {
        if (!this._directory.containsTag(49421)) {
            return null;
        }
        final int int1 = this._directory.getInt(49421);
        switch (int1) {
            case 65535: {
                return "Low";
            }
            case 0: {
                return "Normal";
            }
            case 1: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getEasyShootingModeDescription() throws MetadataException {
        if (!this._directory.containsTag(49419)) {
            return null;
        }
        final int int1 = this._directory.getInt(49419);
        switch (int1) {
            case 0: {
                return "Full auto";
            }
            case 1: {
                return "Manual";
            }
            case 2: {
                return "Landscape";
            }
            case 3: {
                return "Fast shutter";
            }
            case 4: {
                return "Slow shutter";
            }
            case 5: {
                return "Night";
            }
            case 6: {
                return "B&W";
            }
            case 7: {
                return "Sepia";
            }
            case 8: {
                return "Portrait";
            }
            case 9: {
                return "Sports";
            }
            case 10: {
                return "Macro / Closeup";
            }
            case 11: {
                return "Pan focus";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getImageSizeDescription() throws MetadataException {
        if (!this._directory.containsTag(49418)) {
            return null;
        }
        final int int1 = this._directory.getInt(49418);
        switch (int1) {
            case 0: {
                return "Large";
            }
            case 1: {
                return "Medium";
            }
            case 2: {
                return "Small";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusMode1Description() throws MetadataException {
        if (!this._directory.containsTag(49415)) {
            return null;
        }
        final int int1 = this._directory.getInt(49415);
        switch (int1) {
            case 0: {
                return "One-shot";
            }
            case 1: {
                return "AI Servo";
            }
            case 2: {
                return "AI Focus";
            }
            case 3: {
                return "Manual Focus";
            }
            case 4: {
                return "Single";
            }
            case 5: {
                return "Continuous";
            }
            case 6: {
                return "Manual Focus";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getContinuousDriveModeDescription() throws MetadataException {
        if (!this._directory.containsTag(49413)) {
            return null;
        }
        final int int1 = this._directory.getInt(49413);
        switch (int1) {
            case 0: {
                if (this._directory.getInt(49410) == 0) {
                    return "Single shot";
                }
                return "Single shot with self-timer";
            }
            case 1: {
                return "Continuous";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashModeDescription() throws MetadataException {
        if (!this._directory.containsTag(49412)) {
            return null;
        }
        final int int1 = this._directory.getInt(49412);
        switch (int1) {
            case 0: {
                return "No flash fired";
            }
            case 1: {
                return "Auto";
            }
            case 2: {
                return "On";
            }
            case 3: {
                return "Red-eye reduction";
            }
            case 4: {
                return "Slow-synchro";
            }
            case 5: {
                return "Auto and red-eye reduction";
            }
            case 6: {
                return "On and red-eye reduction";
            }
            case 16: {
                return "Extenal flash";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSelfTimerDelayDescription() throws MetadataException {
        if (!this._directory.containsTag(49410)) {
            return null;
        }
        final int int1 = this._directory.getInt(49410);
        if (int1 == 0) {
            return "Self timer not used";
        }
        return Double.toString(int1 * 0.1) + " sec";
    }
    
    public String getMacroModeDescription() throws MetadataException {
        if (!this._directory.containsTag(49409)) {
            return null;
        }
        final int int1 = this._directory.getInt(49409);
        switch (int1) {
            case 1: {
                return "Macro";
            }
            case 2: {
                return "Normal";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getQualityDescription() throws MetadataException {
        if (!this._directory.containsTag(49411)) {
            return null;
        }
        final int int1 = this._directory.getInt(49411);
        switch (int1) {
            case 2: {
                return "Normal";
            }
            case 3: {
                return "Fine";
            }
            case 5: {
                return "Superfine";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getDigitalZoomDescription() throws MetadataException {
        if (!this._directory.containsTag(49420)) {
            return null;
        }
        final int int1 = this._directory.getInt(49420);
        switch (int1) {
            case 0: {
                return "No digital zoom";
            }
            case 1: {
                return "2x";
            }
            case 2: {
                return "4x";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusTypeDescription() throws MetadataException {
        if (!this._directory.containsTag(49426)) {
            return null;
        }
        final int int1 = this._directory.getInt(49426);
        switch (int1) {
            case 0: {
                return "Manual";
            }
            case 1: {
                return "Auto";
            }
            case 3: {
                return "Close-up (Macro)";
            }
            case 8: {
                return "Locked (Pan Mode)";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashActivityDescription() throws MetadataException {
        if (!this._directory.containsTag(49436)) {
            return null;
        }
        final int int1 = this._directory.getInt(49436);
        switch (int1) {
            case 0: {
                return "Flash did not fire";
            }
            case 1: {
                return "Flash fired";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
}
