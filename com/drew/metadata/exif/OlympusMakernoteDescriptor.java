// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class OlympusMakernoteDescriptor extends TagDescriptor
{
    public OlympusMakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 512: {
                return this.getSpecialModeDescription();
            }
            case 513: {
                return this.getJpegQualityDescription();
            }
            case 514: {
                return this.getMacroModeDescription();
            }
            case 516: {
                return this.getDigiZoomRatioDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getDigiZoomRatioDescription() throws MetadataException {
        if (!this._directory.containsTag(516)) {
            return null;
        }
        final int int1 = this._directory.getInt(516);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 2: {
                return "Digital 2x Zoom";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getMacroModeDescription() throws MetadataException {
        if (!this._directory.containsTag(514)) {
            return null;
        }
        final int int1 = this._directory.getInt(514);
        switch (int1) {
            case 0: {
                return "Normal (no macro)";
            }
            case 1: {
                return "Macro";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getJpegQualityDescription() throws MetadataException {
        if (!this._directory.containsTag(513)) {
            return null;
        }
        final int int1 = this._directory.getInt(513);
        switch (int1) {
            case 1: {
                return "SQ";
            }
            case 2: {
                return "HQ";
            }
            case 3: {
                return "SHQ";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSpecialModeDescription() throws MetadataException {
        if (!this._directory.containsTag(512)) {
            return null;
        }
        final int[] intArray = this._directory.getIntArray(512);
        final StringBuffer sb = new StringBuffer();
        switch (intArray[0]) {
            case 0: {
                sb.append("Normal picture taking mode");
                break;
            }
            case 1: {
                sb.append("Unknown picture taking mode");
                break;
            }
            case 2: {
                sb.append("Fast picture taking mode");
                break;
            }
            case 3: {
                sb.append("Panorama picture taking mode");
                break;
            }
            default: {
                sb.append("Unknown picture taking mode");
                break;
            }
        }
        sb.append(" - ");
        switch (intArray[1]) {
            case 0: {
                sb.append("Unknown sequence number");
                break;
            }
            case 1: {
                sb.append("1st in a sequnce");
                break;
            }
            case 2: {
                sb.append("2nd in a sequence");
                break;
            }
            case 3: {
                sb.append("3rd in a sequence");
                break;
            }
            default: {
                sb.append(intArray[1]);
                sb.append("th in a sequence");
                break;
            }
        }
        switch (intArray[2]) {
            case 1: {
                sb.append("Left to right panorama direction");
                break;
            }
            case 2: {
                sb.append("Right to left panorama direction");
                break;
            }
            case 3: {
                sb.append("Bottom to top panorama direction");
                break;
            }
            case 4: {
                sb.append("Top to bottom panorama direction");
                break;
            }
        }
        return sb.toString();
    }
}
