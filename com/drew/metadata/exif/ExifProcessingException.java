// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;

public class ExifProcessingException extends MetadataException
{
    public ExifProcessingException(final String s) {
        super(s);
    }
    
    public ExifProcessingException(final String s, final Throwable t) {
        super(s, t);
    }
}
