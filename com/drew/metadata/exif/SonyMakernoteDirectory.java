// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class SonyMakernoteDirectory extends Directory
{
    protected static final HashMap _tagNameMap;
    
    public SonyMakernoteDirectory() {
        this.setDescriptor(new SonyMakernoteDescriptor(this));
    }
    
    public String getName() {
        return "Sony Makernote";
    }
    
    protected HashMap getTagNameMap() {
        return SonyMakernoteDirectory._tagNameMap;
    }
    
    static {
        _tagNameMap = new HashMap();
    }
}
