// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class PentaxMakernoteDescriptor extends TagDescriptor
{
    public PentaxMakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 1: {
                return this.getCaptureModeDescription();
            }
            case 2: {
                return this.getQualityLevelDescription();
            }
            case 3: {
                return this.getFocusModeDescription();
            }
            case 4: {
                return this.getFlashModeDescription();
            }
            case 7: {
                return this.getWhiteBalanceDescription();
            }
            case 10: {
                return this.getDigitalZoomDescription();
            }
            case 11: {
                return this.getSharpnessDescription();
            }
            case 12: {
                return this.getContrastDescription();
            }
            case 13: {
                return this.getSaturationDescription();
            }
            case 20: {
                return this.getIsoSpeedDescription();
            }
            case 23: {
                return this.getColourDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getColourDescription() throws MetadataException {
        if (!this._directory.containsTag(23)) {
            return null;
        }
        final int int1 = this._directory.getInt(23);
        switch (int1) {
            case 1: {
                return "Normal";
            }
            case 2: {
                return "Black & White";
            }
            case 3: {
                return "Sepia";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getIsoSpeedDescription() throws MetadataException {
        if (!this._directory.containsTag(20)) {
            return null;
        }
        final int int1 = this._directory.getInt(20);
        switch (int1) {
            case 10: {
                return "ISO 100";
            }
            case 16: {
                return "ISO 200";
            }
            case 100: {
                return "ISO 100";
            }
            case 200: {
                return "ISO 200";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSaturationDescription() throws MetadataException {
        if (!this._directory.containsTag(13)) {
            return null;
        }
        final int int1 = this._directory.getInt(13);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Low";
            }
            case 2: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getContrastDescription() throws MetadataException {
        if (!this._directory.containsTag(12)) {
            return null;
        }
        final int int1 = this._directory.getInt(12);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Low";
            }
            case 2: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSharpnessDescription() throws MetadataException {
        if (!this._directory.containsTag(11)) {
            return null;
        }
        final int int1 = this._directory.getInt(11);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Soft";
            }
            case 2: {
                return "Hard";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getDigitalZoomDescription() throws MetadataException {
        if (!this._directory.containsTag(10)) {
            return null;
        }
        final float float1 = this._directory.getFloat(10);
        if (float1 == 0.0f) {
            return "Off";
        }
        return Float.toString(float1);
    }
    
    public String getWhiteBalanceDescription() throws MetadataException {
        if (!this._directory.containsTag(7)) {
            return null;
        }
        final int int1 = this._directory.getInt(7);
        switch (int1) {
            case 0: {
                return "Auto";
            }
            case 1: {
                return "Daylight";
            }
            case 2: {
                return "Shade";
            }
            case 3: {
                return "Tungsten";
            }
            case 4: {
                return "Fluorescent";
            }
            case 5: {
                return "Manual";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashModeDescription() throws MetadataException {
        if (!this._directory.containsTag(4)) {
            return null;
        }
        final int int1 = this._directory.getInt(4);
        switch (int1) {
            case 1: {
                return "Auto";
            }
            case 2: {
                return "Flash On";
            }
            case 4: {
                return "Flash Off";
            }
            case 6: {
                return "Red-eye Reduction";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusModeDescription() throws MetadataException {
        if (!this._directory.containsTag(3)) {
            return null;
        }
        final int int1 = this._directory.getInt(3);
        switch (int1) {
            case 2: {
                return "Custom";
            }
            case 3: {
                return "Auto";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getQualityLevelDescription() throws MetadataException {
        if (!this._directory.containsTag(2)) {
            return null;
        }
        final int int1 = this._directory.getInt(2);
        switch (int1) {
            case 0: {
                return "Good";
            }
            case 1: {
                return "Better";
            }
            case 2: {
                return "Best";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getCaptureModeDescription() throws MetadataException {
        if (!this._directory.containsTag(1)) {
            return null;
        }
        final int int1 = this._directory.getInt(1);
        switch (int1) {
            case 1: {
                return "Auto";
            }
            case 2: {
                return "Night-scene";
            }
            case 3: {
                return "Manual";
            }
            case 4: {
                return "Multiple";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
}
