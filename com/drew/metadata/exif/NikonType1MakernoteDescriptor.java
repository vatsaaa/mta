// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class NikonType1MakernoteDescriptor extends TagDescriptor
{
    public NikonType1MakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 3: {
                return this.getQualityDescription();
            }
            case 4: {
                return this.getColorModeDescription();
            }
            case 5: {
                return this.getImageAdjustmentDescription();
            }
            case 6: {
                return this.getCcdSensitivityDescription();
            }
            case 7: {
                return this.getWhiteBalanceDescription();
            }
            case 8: {
                return this.getFocusDescription();
            }
            case 10: {
                return this.getDigitalZoomDescription();
            }
            case 11: {
                return this.getConverterDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getConverterDescription() throws MetadataException {
        if (!this._directory.containsTag(11)) {
            return null;
        }
        final int int1 = this._directory.getInt(11);
        switch (int1) {
            case 0: {
                return "None";
            }
            case 1: {
                return "Fisheye converter";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getDigitalZoomDescription() throws MetadataException {
        if (!this._directory.containsTag(10)) {
            return null;
        }
        final Rational rational = this._directory.getRational(10);
        if (rational.getNumerator() == 0) {
            return "No digital zoom";
        }
        return rational.toSimpleString(true) + "x digital zoom";
    }
    
    public String getFocusDescription() throws MetadataException {
        if (!this._directory.containsTag(8)) {
            return null;
        }
        final Rational rational = this._directory.getRational(8);
        if (rational.getNumerator() == 1 && rational.getDenominator() == 0) {
            return "Infinite";
        }
        return rational.toSimpleString(true);
    }
    
    public String getWhiteBalanceDescription() throws MetadataException {
        if (!this._directory.containsTag(7)) {
            return null;
        }
        final int int1 = this._directory.getInt(7);
        switch (int1) {
            case 0: {
                return "Auto";
            }
            case 1: {
                return "Preset";
            }
            case 2: {
                return "Daylight";
            }
            case 3: {
                return "Incandescense";
            }
            case 4: {
                return "Flourescence";
            }
            case 5: {
                return "Cloudy";
            }
            case 6: {
                return "SpeedLight";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getCcdSensitivityDescription() throws MetadataException {
        if (!this._directory.containsTag(6)) {
            return null;
        }
        final int int1 = this._directory.getInt(6);
        switch (int1) {
            case 0: {
                return "ISO80";
            }
            case 2: {
                return "ISO160";
            }
            case 4: {
                return "ISO320";
            }
            case 5: {
                return "ISO100";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getImageAdjustmentDescription() throws MetadataException {
        if (!this._directory.containsTag(5)) {
            return null;
        }
        final int int1 = this._directory.getInt(5);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Bright +";
            }
            case 2: {
                return "Bright -";
            }
            case 3: {
                return "Contrast +";
            }
            case 4: {
                return "Contrast -";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getColorModeDescription() throws MetadataException {
        if (!this._directory.containsTag(4)) {
            return null;
        }
        final int int1 = this._directory.getInt(4);
        switch (int1) {
            case 1: {
                return "Color";
            }
            case 2: {
                return "Monochrome";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getQualityDescription() throws MetadataException {
        if (!this._directory.containsTag(3)) {
            return null;
        }
        final int int1 = this._directory.getInt(3);
        switch (int1) {
            case 1: {
                return "VGA Basic";
            }
            case 2: {
                return "VGA Normal";
            }
            case 3: {
                return "VGA Fine";
            }
            case 4: {
                return "SXGA Basic";
            }
            case 5: {
                return "SXGA Normal";
            }
            case 6: {
                return "SXGA Fine";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
}
