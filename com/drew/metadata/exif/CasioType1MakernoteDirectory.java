// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class CasioType1MakernoteDirectory extends Directory
{
    public static final int TAG_CASIO_RECORDING_MODE = 1;
    public static final int TAG_CASIO_QUALITY = 2;
    public static final int TAG_CASIO_FOCUSING_MODE = 3;
    public static final int TAG_CASIO_FLASH_MODE = 4;
    public static final int TAG_CASIO_FLASH_INTENSITY = 5;
    public static final int TAG_CASIO_OBJECT_DISTANCE = 6;
    public static final int TAG_CASIO_WHITE_BALANCE = 7;
    public static final int TAG_CASIO_UNKNOWN_1 = 8;
    public static final int TAG_CASIO_UNKNOWN_2 = 9;
    public static final int TAG_CASIO_DIGITAL_ZOOM = 10;
    public static final int TAG_CASIO_SHARPNESS = 11;
    public static final int TAG_CASIO_CONTRAST = 12;
    public static final int TAG_CASIO_SATURATION = 13;
    public static final int TAG_CASIO_UNKNOWN_3 = 14;
    public static final int TAG_CASIO_UNKNOWN_4 = 15;
    public static final int TAG_CASIO_UNKNOWN_5 = 16;
    public static final int TAG_CASIO_UNKNOWN_6 = 17;
    public static final int TAG_CASIO_UNKNOWN_7 = 18;
    public static final int TAG_CASIO_UNKNOWN_8 = 19;
    public static final int TAG_CASIO_CCD_SENSITIVITY = 20;
    protected static final HashMap tagNameMap;
    
    public CasioType1MakernoteDirectory() {
        this.setDescriptor(new CasioType1MakernoteDescriptor(this));
    }
    
    public String getName() {
        return "Casio Makernote";
    }
    
    protected HashMap getTagNameMap() {
        return CasioType1MakernoteDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(20), "CCD Sensitivity");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(12), "Contrast");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(10), "Digital Zoom");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(5), "Flash Intensity");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(4), "Flash Mode");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(3), "Focussing Mode");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(6), "Object Distance");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(2), "Quality");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(1), "Recording Mode");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(13), "Saturation");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(11), "Sharpness");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(8), "Makernote Unknown 1");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(9), "Makernote Unknown 2");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(14), "Makernote Unknown 3");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(15), "Makernote Unknown 4");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(16), "Makernote Unknown 5");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(17), "Makernote Unknown 6");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(18), "Makernote Unknown 7");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(19), "Makernote Unknown 8");
        CasioType1MakernoteDirectory.tagNameMap.put(new Integer(7), "White Balance");
    }
}
