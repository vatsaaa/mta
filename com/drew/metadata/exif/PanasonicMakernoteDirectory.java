// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class PanasonicMakernoteDirectory extends Directory
{
    public static final int TAG_PANASONIC_QUALITY_MODE = 1;
    public static final int TAG_PANASONIC_VERSION = 2;
    public static final int TAG_PANASONIC_MACRO_MODE = 28;
    public static final int TAG_PANASONIC_RECORD_MODE = 31;
    public static final int TAG_PANASONIC_PRINT_IMAGE_MATCHING_INFO = 3584;
    protected static final HashMap tagNameMap;
    
    public PanasonicMakernoteDirectory() {
        this.setDescriptor(new PanasonicMakernoteDescriptor(this));
    }
    
    public String getName() {
        return "Panasonic Makernote";
    }
    
    protected HashMap getTagNameMap() {
        return PanasonicMakernoteDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(1), "Quality Mode");
        PanasonicMakernoteDirectory.tagNameMap.put(new Integer(2), "Version");
        PanasonicMakernoteDirectory.tagNameMap.put(new Integer(28), "Macro Mode");
        PanasonicMakernoteDirectory.tagNameMap.put(new Integer(31), "Record Mode");
        PanasonicMakernoteDirectory.tagNameMap.put(new Integer(3584), "Print Image Matching (PIM) Info");
    }
}
