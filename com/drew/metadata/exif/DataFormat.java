// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;

public class DataFormat
{
    public static final DataFormat BYTE;
    public static final DataFormat STRING;
    public static final DataFormat USHORT;
    public static final DataFormat ULONG;
    public static final DataFormat URATIONAL;
    public static final DataFormat SBYTE;
    public static final DataFormat UNDEFINED;
    public static final DataFormat SSHORT;
    public static final DataFormat SLONG;
    public static final DataFormat SRATIONAL;
    public static final DataFormat SINGLE;
    public static final DataFormat DOUBLE;
    private final String myName;
    private final int value;
    
    public static DataFormat fromValue(final int i) throws MetadataException {
        switch (i) {
            case 1: {
                return DataFormat.BYTE;
            }
            case 2: {
                return DataFormat.STRING;
            }
            case 3: {
                return DataFormat.USHORT;
            }
            case 4: {
                return DataFormat.ULONG;
            }
            case 5: {
                return DataFormat.URATIONAL;
            }
            case 6: {
                return DataFormat.SBYTE;
            }
            case 7: {
                return DataFormat.UNDEFINED;
            }
            case 8: {
                return DataFormat.SSHORT;
            }
            case 9: {
                return DataFormat.SLONG;
            }
            case 10: {
                return DataFormat.SRATIONAL;
            }
            case 11: {
                return DataFormat.SINGLE;
            }
            case 12: {
                return DataFormat.DOUBLE;
            }
            default: {
                throw new MetadataException("value '" + i + "' does not represent a known data format.");
            }
        }
    }
    
    private DataFormat(final String myName, final int value) {
        this.myName = myName;
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public String toString() {
        return this.myName;
    }
    
    static {
        BYTE = new DataFormat("BYTE", 1);
        STRING = new DataFormat("STRING", 2);
        USHORT = new DataFormat("USHORT", 3);
        ULONG = new DataFormat("ULONG", 4);
        URATIONAL = new DataFormat("URATIONAL", 5);
        SBYTE = new DataFormat("SBYTE", 6);
        UNDEFINED = new DataFormat("UNDEFINED", 7);
        SSHORT = new DataFormat("SSHORT", 8);
        SLONG = new DataFormat("SLONG", 9);
        SRATIONAL = new DataFormat("SRATIONAL", 10);
        SINGLE = new DataFormat("SINGLE", 11);
        DOUBLE = new DataFormat("DOUBLE", 12);
    }
}
