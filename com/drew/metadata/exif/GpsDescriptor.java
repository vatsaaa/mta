// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class GpsDescriptor extends TagDescriptor
{
    public GpsDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 6: {
                return this.getGpsAltitudeDescription();
            }
            case 5: {
                return this.getGpsAltitudeRefDescription();
            }
            case 9: {
                return this.getGpsStatusDescription();
            }
            case 10: {
                return this.getGpsMeasureModeDescription();
            }
            case 12: {
                return this.getGpsSpeedRefDescription();
            }
            case 14:
            case 16:
            case 23: {
                return this.getGpsDirectionReferenceDescription(n);
            }
            case 15:
            case 17:
            case 24: {
                return this.getGpsDirectionDescription(n);
            }
            case 25: {
                return this.getGpsDestinationReferenceDescription();
            }
            case 7: {
                return this.getGpsTimeStampDescription();
            }
            case 4: {
                return this.getGpsLongitudeDescription();
            }
            case 2: {
                return this.getGpsLatitudeDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getGpsLatitudeDescription() throws MetadataException {
        if (!this._directory.containsTag(2)) {
            return null;
        }
        return this.getHoursMinutesSecondsDescription(2);
    }
    
    public String getGpsLongitudeDescription() throws MetadataException {
        if (!this._directory.containsTag(4)) {
            return null;
        }
        return this.getHoursMinutesSecondsDescription(4);
    }
    
    public String getHoursMinutesSecondsDescription(final int n) throws MetadataException {
        final Rational[] rationalArray = this._directory.getRationalArray(n);
        final int intValue = rationalArray[0].intValue();
        final float floatValue = rationalArray[1].floatValue();
        return String.valueOf(intValue) + "\"" + String.valueOf((int)floatValue) + "'" + String.valueOf(rationalArray[2].floatValue() + floatValue % 1.0f * 60.0f);
    }
    
    public String getGpsTimeStampDescription() throws MetadataException {
        if (!this._directory.containsTag(7)) {
            return null;
        }
        final int[] intArray = this._directory.getIntArray(7);
        final StringBuffer sb = new StringBuffer();
        sb.append(intArray[0]);
        sb.append(":");
        sb.append(intArray[1]);
        sb.append(":");
        sb.append(intArray[2]);
        sb.append(" UTC");
        return sb.toString();
    }
    
    public String getGpsDestinationReferenceDescription() {
        if (!this._directory.containsTag(25)) {
            return null;
        }
        final String trim = this._directory.getString(25).trim();
        if ("K".equalsIgnoreCase(trim)) {
            return "kilometers";
        }
        if ("M".equalsIgnoreCase(trim)) {
            return "miles";
        }
        if ("N".equalsIgnoreCase(trim)) {
            return "knots";
        }
        return "Unknown (" + trim + ")";
    }
    
    public String getGpsDirectionDescription(final int n) {
        if (!this._directory.containsTag(n)) {
            return null;
        }
        return this._directory.getString(n).trim() + " degrees";
    }
    
    public String getGpsDirectionReferenceDescription(final int n) {
        if (!this._directory.containsTag(n)) {
            return null;
        }
        final String trim = this._directory.getString(n).trim();
        if ("T".equalsIgnoreCase(trim)) {
            return "True direction";
        }
        if ("M".equalsIgnoreCase(trim)) {
            return "Magnetic direction";
        }
        return "Unknown (" + trim + ")";
    }
    
    public String getGpsSpeedRefDescription() {
        if (!this._directory.containsTag(12)) {
            return null;
        }
        final String trim = this._directory.getString(12).trim();
        if ("K".equalsIgnoreCase(trim)) {
            return "kph";
        }
        if ("M".equalsIgnoreCase(trim)) {
            return "mph";
        }
        if ("N".equalsIgnoreCase(trim)) {
            return "knots";
        }
        return "Unknown (" + trim + ")";
    }
    
    public String getGpsMeasureModeDescription() {
        if (!this._directory.containsTag(10)) {
            return null;
        }
        final String trim = this._directory.getString(10).trim();
        if ("2".equalsIgnoreCase(trim)) {
            return "2-dimensional measurement";
        }
        if ("3".equalsIgnoreCase(trim)) {
            return "3-dimensional measurement";
        }
        return "Unknown (" + trim + ")";
    }
    
    public String getGpsStatusDescription() {
        if (!this._directory.containsTag(9)) {
            return null;
        }
        final String trim = this._directory.getString(9).trim();
        if ("A".equalsIgnoreCase(trim)) {
            return "Measurement in progess";
        }
        if ("V".equalsIgnoreCase(trim)) {
            return "Measurement Interoperability";
        }
        return "Unknown (" + trim + ")";
    }
    
    public String getGpsAltitudeRefDescription() throws MetadataException {
        if (!this._directory.containsTag(5)) {
            return null;
        }
        final int int1 = this._directory.getInt(5);
        if (int1 == 0) {
            return "Sea level";
        }
        return "Unknown (" + int1 + ")";
    }
    
    public String getGpsAltitudeDescription() throws MetadataException {
        if (!this._directory.containsTag(6)) {
            return null;
        }
        return this._directory.getRational(6).toSimpleString(true) + " metres";
    }
}
