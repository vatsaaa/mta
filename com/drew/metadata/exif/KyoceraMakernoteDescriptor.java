// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class KyoceraMakernoteDescriptor extends TagDescriptor
{
    public KyoceraMakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 3584: {
                return this.getPrintImageMatchingInfoDescription();
            }
            case 1: {
                return this.getProprietaryThumbnailDataDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getPrintImageMatchingInfoDescription() throws MetadataException {
        if (!this._directory.containsTag(3584)) {
            return null;
        }
        return "(" + this._directory.getByteArray(3584).length + " bytes)";
    }
    
    public String getProprietaryThumbnailDataDescription() throws MetadataException {
        if (!this._directory.containsTag(1)) {
            return null;
        }
        return "(" + this._directory.getByteArray(1).length + " bytes)";
    }
}
