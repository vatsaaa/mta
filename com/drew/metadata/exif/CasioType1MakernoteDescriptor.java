// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class CasioType1MakernoteDescriptor extends TagDescriptor
{
    public CasioType1MakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 1: {
                return this.getRecordingModeDescription();
            }
            case 2: {
                return this.getQualityDescription();
            }
            case 3: {
                return this.getFocusingModeDescription();
            }
            case 4: {
                return this.getFlashModeDescription();
            }
            case 5: {
                return this.getFlashIntensityDescription();
            }
            case 6: {
                return this.getObjectDistanceDescription();
            }
            case 7: {
                return this.getWhiteBalanceDescription();
            }
            case 10: {
                return this.getDigitalZoomDescription();
            }
            case 11: {
                return this.getSharpnessDescription();
            }
            case 12: {
                return this.getContrastDescription();
            }
            case 13: {
                return this.getSaturationDescription();
            }
            case 20: {
                return this.getCcdSensitivityDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getCcdSensitivityDescription() throws MetadataException {
        if (!this._directory.containsTag(20)) {
            return null;
        }
        final int int1 = this._directory.getInt(20);
        switch (int1) {
            case 64: {
                return "Normal";
            }
            case 125: {
                return "+1.0";
            }
            case 250: {
                return "+2.0";
            }
            case 244: {
                return "+3.0";
            }
            case 80: {
                return "Normal (ISO 80 equivalent)";
            }
            case 100: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSaturationDescription() throws MetadataException {
        if (!this._directory.containsTag(13)) {
            return null;
        }
        final int int1 = this._directory.getInt(13);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Low";
            }
            case 2: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getContrastDescription() throws MetadataException {
        if (!this._directory.containsTag(12)) {
            return null;
        }
        final int int1 = this._directory.getInt(12);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Low";
            }
            case 2: {
                return "High";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSharpnessDescription() throws MetadataException {
        if (!this._directory.containsTag(11)) {
            return null;
        }
        final int int1 = this._directory.getInt(11);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Soft";
            }
            case 2: {
                return "Hard";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getDigitalZoomDescription() throws MetadataException {
        if (!this._directory.containsTag(10)) {
            return null;
        }
        final int int1 = this._directory.getInt(10);
        switch (int1) {
            case 65536: {
                return "No digital zoom";
            }
            case 65537: {
                return "2x digital zoom";
            }
            case 131072: {
                return "2x digital zoom";
            }
            case 262144: {
                return "4x digital zoom";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getWhiteBalanceDescription() throws MetadataException {
        if (!this._directory.containsTag(7)) {
            return null;
        }
        final int int1 = this._directory.getInt(7);
        switch (int1) {
            case 1: {
                return "Auto";
            }
            case 2: {
                return "Tungsten";
            }
            case 3: {
                return "Daylight";
            }
            case 4: {
                return "Flourescent";
            }
            case 5: {
                return "Shade";
            }
            case 129: {
                return "Manual";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getObjectDistanceDescription() throws MetadataException {
        if (!this._directory.containsTag(6)) {
            return null;
        }
        return this._directory.getInt(6) + " mm";
    }
    
    public String getFlashIntensityDescription() throws MetadataException {
        if (!this._directory.containsTag(5)) {
            return null;
        }
        final int int1 = this._directory.getInt(5);
        switch (int1) {
            case 11: {
                return "Weak";
            }
            case 13: {
                return "Normal";
            }
            case 15: {
                return "Strong";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashModeDescription() throws MetadataException {
        if (!this._directory.containsTag(4)) {
            return null;
        }
        final int int1 = this._directory.getInt(4);
        switch (int1) {
            case 1: {
                return "Auto";
            }
            case 2: {
                return "On";
            }
            case 3: {
                return "Off";
            }
            case 4: {
                return "Red eye reduction";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusingModeDescription() throws MetadataException {
        if (!this._directory.containsTag(3)) {
            return null;
        }
        final int int1 = this._directory.getInt(3);
        switch (int1) {
            case 2: {
                return "Macro";
            }
            case 3: {
                return "Auto focus";
            }
            case 4: {
                return "Manual focus";
            }
            case 5: {
                return "Infinity";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getQualityDescription() throws MetadataException {
        if (!this._directory.containsTag(2)) {
            return null;
        }
        final int int1 = this._directory.getInt(2);
        switch (int1) {
            case 1: {
                return "Economy";
            }
            case 2: {
                return "Normal";
            }
            case 3: {
                return "Fine";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getRecordingModeDescription() throws MetadataException {
        if (!this._directory.containsTag(1)) {
            return null;
        }
        final int int1 = this._directory.getInt(1);
        switch (int1) {
            case 1: {
                return "Single shutter";
            }
            case 2: {
                return "Panorama";
            }
            case 3: {
                return "Night scene";
            }
            case 4: {
                return "Portrait";
            }
            case 5: {
                return "Landscape";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
}
