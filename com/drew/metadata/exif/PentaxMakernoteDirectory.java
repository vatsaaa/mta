// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class PentaxMakernoteDirectory extends Directory
{
    public static final int TAG_PENTAX_CAPTURE_MODE = 1;
    public static final int TAG_PENTAX_QUALITY_LEVEL = 2;
    public static final int TAG_PENTAX_FOCUS_MODE = 3;
    public static final int TAG_PENTAX_FLASH_MODE = 4;
    public static final int TAG_PENTAX_WHITE_BALANCE = 7;
    public static final int TAG_PENTAX_DIGITAL_ZOOM = 10;
    public static final int TAG_PENTAX_SHARPNESS = 11;
    public static final int TAG_PENTAX_CONTRAST = 12;
    public static final int TAG_PENTAX_SATURATION = 13;
    public static final int TAG_PENTAX_ISO_SPEED = 20;
    public static final int TAG_PENTAX_COLOUR = 23;
    public static final int TAG_PENTAX_PRINT_IMAGE_MATCHING_INFO = 3584;
    public static final int TAG_PENTAX_TIME_ZONE = 4096;
    public static final int TAG_PENTAX_DAYLIGHT_SAVINGS = 4097;
    protected static final HashMap tagNameMap;
    
    public PentaxMakernoteDirectory() {
        this.setDescriptor(new PentaxMakernoteDescriptor(this));
    }
    
    public String getName() {
        return "Pentax Makernote";
    }
    
    protected HashMap getTagNameMap() {
        return PentaxMakernoteDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(1), "Capture Mode");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(2), "Quality Level");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(3), "Focus Mode");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(4), "Flash Mode");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(7), "White Balance");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(10), "Digital Zoom");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(11), "Sharpness");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(12), "Contrast");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(13), "Saturation");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(20), "ISO Speed");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(23), "Colour");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(3584), "Print Image Matching (PIM) Info");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(4096), "Time Zone");
        PentaxMakernoteDirectory.tagNameMap.put(new Integer(4097), "Daylight Savings");
    }
}
