// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class ExifInteropDescriptor extends TagDescriptor
{
    public ExifInteropDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 1: {
                return this.getInteropIndexDescription();
            }
            case 2: {
                return this.getInteropVersionDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getInteropVersionDescription() throws MetadataException {
        if (!this._directory.containsTag(2)) {
            return null;
        }
        return ExifDescriptor.convertBytesToVersionString(this._directory.getIntArray(2));
    }
    
    public String getInteropIndexDescription() {
        if (!this._directory.containsTag(1)) {
            return null;
        }
        final String trim = this._directory.getString(1).trim();
        if ("R98".equalsIgnoreCase(trim)) {
            return "Recommended Exif Interoperability Rules (ExifR98)";
        }
        return "Unknown (" + trim + ")";
    }
}
