// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class KyoceraMakernoteDirectory extends Directory
{
    public static final int TAG_KYOCERA_PROPRIETARY_THUMBNAIL = 1;
    public static final int TAG_KYOCERA_PRINT_IMAGE_MATCHING_INFO = 3584;
    protected static final HashMap tagNameMap;
    
    public KyoceraMakernoteDirectory() {
        this.setDescriptor(new KyoceraMakernoteDescriptor(this));
    }
    
    public String getName() {
        return "Kyocera/Contax Makernote";
    }
    
    protected HashMap getTagNameMap() {
        return KyoceraMakernoteDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(1), "Proprietary Thumbnail Format Data");
        KyoceraMakernoteDirectory.tagNameMap.put(new Integer(3584), "Print Image Matching (PIM) Info");
    }
}
