// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class FujifilmMakernoteDescriptor extends TagDescriptor
{
    public FujifilmMakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 4097: {
                return this.getSharpnessDescription();
            }
            case 4098: {
                return this.getWhiteBalanceDescription();
            }
            case 4099: {
                return this.getColorDescription();
            }
            case 4100: {
                return this.getToneDescription();
            }
            case 4112: {
                return this.getFlashModeDescription();
            }
            case 4113: {
                return this.getFlashStrengthDescription();
            }
            case 4128: {
                return this.getMacroDescription();
            }
            case 4129: {
                return this.getFocusModeDescription();
            }
            case 4144: {
                return this.getSlowSyncDescription();
            }
            case 4145: {
                return this.getPictureModeDescription();
            }
            case 4352: {
                return this.getContinuousTakingOrAutoBrackettingDescription();
            }
            case 4864: {
                return this.getBlurWarningDescription();
            }
            case 4865: {
                return this.getFocusWarningDescription();
            }
            case 4866: {
                return this.getAutoExposureWarningDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getAutoExposureWarningDescription() throws MetadataException {
        if (!this._directory.containsTag(4866)) {
            return null;
        }
        final int int1 = this._directory.getInt(4866);
        switch (int1) {
            case 0: {
                return "AE good";
            }
            case 1: {
                return "Over exposed (>1/1000s @ F11)";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusWarningDescription() throws MetadataException {
        if (!this._directory.containsTag(4865)) {
            return null;
        }
        final int int1 = this._directory.getInt(4865);
        switch (int1) {
            case 0: {
                return "Auto focus good";
            }
            case 1: {
                return "Out of focus";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getBlurWarningDescription() throws MetadataException {
        if (!this._directory.containsTag(4864)) {
            return null;
        }
        final int int1 = this._directory.getInt(4864);
        switch (int1) {
            case 0: {
                return "No blur warning";
            }
            case 1: {
                return "Blur warning";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getContinuousTakingOrAutoBrackettingDescription() throws MetadataException {
        if (!this._directory.containsTag(4352)) {
            return null;
        }
        final int int1 = this._directory.getInt(4352);
        switch (int1) {
            case 0: {
                return "Off";
            }
            case 1: {
                return "On";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getPictureModeDescription() throws MetadataException {
        if (!this._directory.containsTag(4145)) {
            return null;
        }
        final int int1 = this._directory.getInt(4145);
        switch (int1) {
            case 0: {
                return "Auto";
            }
            case 1: {
                return "Portrait scene";
            }
            case 2: {
                return "Landscape scene";
            }
            case 4: {
                return "Sports scene";
            }
            case 5: {
                return "Night scene";
            }
            case 6: {
                return "Program AE";
            }
            case 256: {
                return "Aperture priority AE";
            }
            case 512: {
                return "Shutter priority AE";
            }
            case 768: {
                return "Manual exposure";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSlowSyncDescription() throws MetadataException {
        if (!this._directory.containsTag(4144)) {
            return null;
        }
        final int int1 = this._directory.getInt(4144);
        switch (int1) {
            case 0: {
                return "Off";
            }
            case 1: {
                return "On";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusModeDescription() throws MetadataException {
        if (!this._directory.containsTag(4129)) {
            return null;
        }
        final int int1 = this._directory.getInt(4129);
        switch (int1) {
            case 0: {
                return "Auto focus";
            }
            case 1: {
                return "Manual focus";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getMacroDescription() throws MetadataException {
        if (!this._directory.containsTag(4128)) {
            return null;
        }
        final int int1 = this._directory.getInt(4128);
        switch (int1) {
            case 0: {
                return "Off";
            }
            case 1: {
                return "On";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashStrengthDescription() throws MetadataException {
        if (!this._directory.containsTag(4113)) {
            return null;
        }
        return this._directory.getRational(4113).toSimpleString(false) + " EV (Apex)";
    }
    
    public String getFlashModeDescription() throws MetadataException {
        if (!this._directory.containsTag(4112)) {
            return null;
        }
        final int int1 = this._directory.getInt(4112);
        switch (int1) {
            case 0: {
                return "Auto";
            }
            case 1: {
                return "On";
            }
            case 2: {
                return "Off";
            }
            case 3: {
                return "Red-eye reduction";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getToneDescription() throws MetadataException {
        if (!this._directory.containsTag(4100)) {
            return null;
        }
        final int int1 = this._directory.getInt(4100);
        switch (int1) {
            case 0: {
                return "Normal (STD)";
            }
            case 256: {
                return "High (HARD)";
            }
            case 512: {
                return "Low (ORG)";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getColorDescription() throws MetadataException {
        if (!this._directory.containsTag(4099)) {
            return null;
        }
        final int int1 = this._directory.getInt(4099);
        switch (int1) {
            case 0: {
                return "Normal (STD)";
            }
            case 256: {
                return "High";
            }
            case 512: {
                return "Low (ORG)";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getWhiteBalanceDescription() throws MetadataException {
        if (!this._directory.containsTag(4098)) {
            return null;
        }
        final int int1 = this._directory.getInt(4098);
        switch (int1) {
            case 0: {
                return "Auto";
            }
            case 256: {
                return "Daylight";
            }
            case 512: {
                return "Cloudy";
            }
            case 768: {
                return "DaylightColor-fluorescence";
            }
            case 769: {
                return "DaywhiteColor-fluorescence";
            }
            case 770: {
                return "White-fluorescence";
            }
            case 1024: {
                return "Incandenscense";
            }
            case 3840: {
                return "Custom white balance";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSharpnessDescription() throws MetadataException {
        if (!this._directory.containsTag(4097)) {
            return null;
        }
        final int int1 = this._directory.getInt(4097);
        switch (int1) {
            case 1: {
                return "Softest";
            }
            case 2: {
                return "Soft";
            }
            case 3: {
                return "Normal";
            }
            case 4: {
                return "Hard";
            }
            case 5: {
                return "Hardest";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
}
