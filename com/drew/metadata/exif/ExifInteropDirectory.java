// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class ExifInteropDirectory extends Directory
{
    public static final int TAG_INTEROP_INDEX = 1;
    public static final int TAG_INTEROP_VERSION = 2;
    public static final int TAG_RELATED_IMAGE_FILE_FORMAT = 4096;
    public static final int TAG_RELATED_IMAGE_WIDTH = 4097;
    public static final int TAG_RELATED_IMAGE_LENGTH = 4098;
    protected static final HashMap tagNameMap;
    
    public ExifInteropDirectory() {
        this.setDescriptor(new ExifInteropDescriptor(this));
    }
    
    public String getName() {
        return "Interoperability";
    }
    
    protected HashMap getTagNameMap() {
        return ExifInteropDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(1), "Interoperability Index");
        ExifInteropDirectory.tagNameMap.put(new Integer(2), "Interoperability Version");
        ExifInteropDirectory.tagNameMap.put(new Integer(4096), "Related Image File Format");
        ExifInteropDirectory.tagNameMap.put(new Integer(4097), "Related Image Width");
        ExifInteropDirectory.tagNameMap.put(new Integer(4098), "Related Image Length");
    }
}
