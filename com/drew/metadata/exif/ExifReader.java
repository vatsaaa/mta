// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.metadata.Directory;
import java.util.HashMap;
import java.io.InputStream;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.imaging.jpeg.JpegSegmentReader;
import java.io.File;
import com.drew.imaging.jpeg.JpegSegmentData;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataReader;

public class ExifReader implements MetadataReader
{
    private static final int[] BYTES_PER_FORMAT;
    private static final int MAX_FORMAT_CODE = 12;
    private static final int FMT_BYTE = 1;
    private static final int FMT_STRING = 2;
    private static final int FMT_USHORT = 3;
    private static final int FMT_ULONG = 4;
    private static final int FMT_URATIONAL = 5;
    private static final int FMT_SBYTE = 6;
    private static final int FMT_UNDEFINED = 7;
    private static final int FMT_SSHORT = 8;
    private static final int FMT_SLONG = 9;
    private static final int FMT_SRATIONAL = 10;
    private static final int FMT_SINGLE = 11;
    private static final int FMT_DOUBLE = 12;
    public static final int TAG_EXIF_OFFSET = 34665;
    public static final int TAG_INTEROP_OFFSET = 40965;
    public static final int TAG_GPS_INFO_OFFSET = 34853;
    public static final int TAG_MAKER_NOTE = 37500;
    public static final int TIFF_HEADER_START_OFFSET = 6;
    private final byte[] _data;
    private boolean _isMotorollaByteOrder;
    private Metadata _metadata;
    private ExifDirectory _exifDirectory;
    
    public ExifReader(final JpegSegmentData jpegSegmentData) {
        this(jpegSegmentData.getSegment((byte)(-31)));
    }
    
    public ExifReader(final File file) throws JpegProcessingException {
        this(new JpegSegmentReader(file).readSegment((byte)(-31)));
    }
    
    public ExifReader(final InputStream inputStream) throws JpegProcessingException {
        this(new JpegSegmentReader(inputStream).readSegment((byte)(-31)));
    }
    
    public ExifReader(final byte[] data) {
        this._exifDirectory = null;
        this._data = data;
    }
    
    public Metadata extract() {
        return this.extract(new Metadata());
    }
    
    public Metadata extract(final Metadata metadata) {
        this._metadata = metadata;
        if (this._data == null) {
            return this._metadata;
        }
        final ExifDirectory exifDirectory = this.getExifDirectory();
        if (this._data.length <= 14) {
            exifDirectory.addError("Exif data segment must contain at least 14 bytes");
            return this._metadata;
        }
        if (!"Exif\u0000\u0000".equals(new String(this._data, 0, 6))) {
            exifDirectory.addError("Exif data segment doesn't begin with 'Exif'");
            return this._metadata;
        }
        return this.extractIFD(this._metadata, 6);
    }
    
    private ExifDirectory getExifDirectory() {
        if (this._exifDirectory == null) {
            this._exifDirectory = (ExifDirectory)this._metadata.getDirectory(ExifDirectory.class);
        }
        return this._exifDirectory;
    }
    
    public Metadata extractTiff(final Metadata metadata) {
        return this.extractIFD(metadata, 0);
    }
    
    private Metadata extractIFD(final Metadata metadata, final int offset) {
        this._metadata = metadata;
        if (this._data == null) {
            return this._metadata;
        }
        final ExifDirectory exifDirectory = this.getExifDirectory();
        final String s = new String(this._data, offset, 2);
        if (!this.setByteOrder(s)) {
            exifDirectory.addError("Unclear distinction between Motorola/Intel byte ordering: " + s);
            return this._metadata;
        }
        if (this.get16Bits(2 + offset) != 42) {
            exifDirectory.addError("Invalid Exif start - should have 0x2A at offset 8 in Exif header");
            return this._metadata;
        }
        int n = this.get32Bits(4 + offset) + offset;
        if (n >= this._data.length - 1) {
            exifDirectory.addError("First exif directory offset is beyond end of Exif data segment");
            n = 14;
        }
        this.processDirectory(exifDirectory, new HashMap(), n, offset);
        this.storeThumbnailBytes(exifDirectory, offset);
        return this._metadata;
    }
    
    private void storeThumbnailBytes(final ExifDirectory exifDirectory, final int n) {
        if (!exifDirectory.containsTag(259)) {
            return;
        }
        if (!exifDirectory.containsTag(514) || !exifDirectory.containsTag(513)) {
            return;
        }
        try {
            final int int1 = exifDirectory.getInt(513);
            final byte[] array = new byte[exifDirectory.getInt(514)];
            for (int i = 0; i < array.length; ++i) {
                array[i] = this._data[n + int1 + i];
            }
            exifDirectory.setByteArray(61441, array);
        }
        catch (Throwable t) {
            exifDirectory.addError("Unable to extract thumbnail: " + t.getMessage());
        }
    }
    
    private boolean setByteOrder(final String s) {
        if ("MM".equals(s)) {
            this._isMotorollaByteOrder = true;
        }
        else {
            if (!"II".equals(s)) {
                return false;
            }
            this._isMotorollaByteOrder = false;
        }
        return true;
    }
    
    private void processDirectory(final Directory directory, final HashMap hashMap, final int n, final int n2) {
        if (hashMap.containsKey(new Integer(n))) {
            return;
        }
        hashMap.put(new Integer(n), "processed");
        if (n >= this._data.length || n < 0) {
            directory.addError("Ignored directory marked to start outside data segement");
            return;
        }
        if (!this.isDirectoryLengthValid(n, n2)) {
            directory.addError("Illegally sized directory");
            return;
        }
        final int get16Bits = this.get16Bits(n);
        for (int i = 0; i < get16Bits; ++i) {
            final int calculateTagOffset = this.calculateTagOffset(n, i);
            final int get16Bits2 = this.get16Bits(calculateTagOffset);
            final int get16Bits3 = this.get16Bits(calculateTagOffset + 2);
            if (get16Bits3 < 1 || get16Bits3 > 12) {
                directory.addError("Invalid format code: " + get16Bits3);
            }
            else {
                final int get32Bits = this.get32Bits(calculateTagOffset + 4);
                if (get32Bits < 0) {
                    directory.addError("Negative component count in EXIF");
                }
                else {
                    final int j = get32Bits * ExifReader.BYTES_PER_FORMAT[get16Bits3];
                    final int calculateTagValueOffset = this.calculateTagValueOffset(j, calculateTagOffset, n2);
                    if (calculateTagValueOffset < 0 || calculateTagValueOffset > this._data.length) {
                        directory.addError("Illegal pointer offset value in EXIF");
                    }
                    else if (j < 0 || calculateTagValueOffset + j > this._data.length) {
                        directory.addError("Illegal number of bytes: " + j);
                    }
                    else {
                        final int n3 = n2 + this.get32Bits(calculateTagValueOffset);
                        switch (get16Bits2) {
                            case 34665: {
                                this.processDirectory(this._metadata.getDirectory(ExifDirectory.class), hashMap, n3, n2);
                                break;
                            }
                            case 40965: {
                                this.processDirectory(this._metadata.getDirectory(ExifInteropDirectory.class), hashMap, n3, n2);
                                break;
                            }
                            case 34853: {
                                this.processDirectory(this._metadata.getDirectory(GpsDirectory.class), hashMap, n3, n2);
                                break;
                            }
                            case 37500: {
                                this.processMakerNote(calculateTagValueOffset, hashMap, n2);
                                break;
                            }
                            default: {
                                this.processTag(directory, get16Bits2, calculateTagValueOffset, get32Bits, get16Bits3);
                                break;
                            }
                        }
                    }
                }
            }
        }
        final int get32Bits2 = this.get32Bits(this.calculateTagOffset(n, get16Bits));
        if (get32Bits2 != 0) {
            final int n4 = get32Bits2 + n2;
            if (n4 >= this._data.length) {
                return;
            }
            if (n4 < n) {
                return;
            }
            this.processDirectory(directory, hashMap, n4, n2);
        }
    }
    
    private void processMakerNote(final int n, final HashMap hashMap, final int n2) {
        final Directory directory = this._metadata.getDirectory(ExifDirectory.class);
        if (directory == null) {
            return;
        }
        final String string = directory.getString(271);
        final String anObject = new String(this._data, n, 2);
        final String s = new String(this._data, n, 3);
        final String s2 = new String(this._data, n, 4);
        final String s3 = new String(this._data, n, 5);
        final String anObject2 = new String(this._data, n, 6);
        final String anObject3 = new String(this._data, n, 7);
        final String s4 = new String(this._data, n, 8);
        if ("OLYMP".equals(s3) || "EPSON".equals(s3) || "AGFA".equals(s2)) {
            this.processDirectory(this._metadata.getDirectory(OlympusMakernoteDirectory.class), hashMap, n + 8, n2);
        }
        else if (string != null && string.trim().toUpperCase().startsWith("NIKON")) {
            if ("Nikon".equals(s3)) {
                if (this._data[n + 6] == 1) {
                    this.processDirectory(this._metadata.getDirectory(NikonType1MakernoteDirectory.class), hashMap, n + 8, n2);
                }
                else if (this._data[n + 6] == 2) {
                    this.processDirectory(this._metadata.getDirectory(NikonType2MakernoteDirectory.class), hashMap, n + 18, n + 10);
                }
                else {
                    directory.addError("Unsupported makernote data ignored.");
                }
            }
            else {
                this.processDirectory(this._metadata.getDirectory(NikonType2MakernoteDirectory.class), hashMap, n, n2);
            }
        }
        else if ("SONY CAM".equals(s4) || "SONY DSC".equals(s4)) {
            this.processDirectory(this._metadata.getDirectory(SonyMakernoteDirectory.class), hashMap, n + 12, n2);
        }
        else if ("KDK".equals(s)) {
            this.processDirectory(this._metadata.getDirectory(KodakMakernoteDirectory.class), hashMap, n + 20, n2);
        }
        else if ("Canon".equalsIgnoreCase(string)) {
            this.processDirectory(this._metadata.getDirectory(CanonMakernoteDirectory.class), hashMap, n, n2);
        }
        else if (string != null && string.toUpperCase().startsWith("CASIO")) {
            if ("QVC\u0000\u0000\u0000".equals(anObject2)) {
                this.processDirectory(this._metadata.getDirectory(CasioType2MakernoteDirectory.class), hashMap, n + 6, n2);
            }
            else {
                this.processDirectory(this._metadata.getDirectory(CasioType1MakernoteDirectory.class), hashMap, n, n2);
            }
        }
        else if ("FUJIFILM".equals(s4) || "Fujifilm".equalsIgnoreCase(string)) {
            final boolean isMotorollaByteOrder = this._isMotorollaByteOrder;
            this._isMotorollaByteOrder = false;
            this.processDirectory(this._metadata.getDirectory(FujifilmMakernoteDirectory.class), hashMap, n + this.get32Bits(n + 8), n2);
            this._isMotorollaByteOrder = isMotorollaByteOrder;
        }
        else if (string != null && string.toUpperCase().startsWith("MINOLTA")) {
            this.processDirectory(this._metadata.getDirectory(OlympusMakernoteDirectory.class), hashMap, n, n2);
        }
        else if ("KC".equals(anObject) || "MINOL".equals(s3) || "MLY".equals(s) || "+M+M+M+M".equals(s4)) {
            directory.addError("Unsupported Konica/Minolta data ignored.");
        }
        else if ("KYOCERA".equals(anObject3)) {
            this.processDirectory(this._metadata.getDirectory(KyoceraMakernoteDirectory.class), hashMap, n + 22, n2);
        }
        else if ("Panasonic\u0000\u0000\u0000".equals(new String(this._data, n, 12))) {
            this.processDirectory(this._metadata.getDirectory(PanasonicMakernoteDirectory.class), hashMap, n + 12, n2);
        }
        else if ("AOC\u0000".equals(s2)) {
            this.processDirectory(this._metadata.getDirectory(CasioType2MakernoteDirectory.class), hashMap, n + 6, n);
        }
        else if (string != null && (string.toUpperCase().startsWith("PENTAX") || string.toUpperCase().startsWith("ASAHI"))) {
            this.processDirectory(this._metadata.getDirectory(PentaxMakernoteDirectory.class), hashMap, n, n);
        }
        else {
            directory.addError("Unsupported makernote data ignored.");
        }
    }
    
    private boolean isDirectoryLengthValid(final int n, final int n2) {
        return 2 + 12 * this.get16Bits(n) + 4 + n + n2 < this._data.length;
    }
    
    private void processTag(final Directory directory, final int i, final int n, final int n2, final int j) {
        switch (j) {
            case 7: {
                final byte[] array = new byte[n2];
                for (int n3 = n2 * ExifReader.BYTES_PER_FORMAT[j], k = 0; k < n3; ++k) {
                    array[k] = this._data[n + k];
                }
                directory.setByteArray(i, array);
                break;
            }
            case 2: {
                directory.setString(i, this.readString(n, n2));
                break;
            }
            case 5:
            case 10: {
                if (n2 == 1) {
                    directory.setRational(i, new Rational(this.get32Bits(n), this.get32Bits(n + 4)));
                    break;
                }
                final Rational[] array2 = new Rational[n2];
                for (int l = 0; l < n2; ++l) {
                    array2[l] = new Rational(this.get32Bits(n + 8 * l), this.get32Bits(n + 4 + 8 * l));
                }
                directory.setRationalArray(i, array2);
                break;
            }
            case 1:
            case 6: {
                if (n2 == 1) {
                    directory.setInt(i, this._data[n]);
                    break;
                }
                final int[] array3 = new int[n2];
                for (int n4 = 0; n4 < n2; ++n4) {
                    array3[n4] = this._data[n + n4];
                }
                directory.setIntArray(i, array3);
                break;
            }
            case 11:
            case 12: {
                if (n2 == 1) {
                    directory.setInt(i, this._data[n]);
                    break;
                }
                final int[] array4 = new int[n2];
                for (int n5 = 0; n5 < n2; ++n5) {
                    array4[n5] = this._data[n + n5];
                }
                directory.setIntArray(i, array4);
                break;
            }
            case 3:
            case 8: {
                if (n2 == 1) {
                    directory.setInt(i, this.get16Bits(n));
                    break;
                }
                final int[] array5 = new int[n2];
                for (int n6 = 0; n6 < n2; ++n6) {
                    array5[n6] = this.get16Bits(n + n6 * 2);
                }
                directory.setIntArray(i, array5);
                break;
            }
            case 4:
            case 9: {
                if (n2 == 1) {
                    directory.setInt(i, this.get32Bits(n));
                    break;
                }
                final int[] array6 = new int[n2];
                for (int n7 = 0; n7 < n2; ++n7) {
                    array6[n7] = this.get32Bits(n + n7 * 4);
                }
                directory.setIntArray(i, array6);
                break;
            }
            default: {
                directory.addError("Unknown format code " + j + " for tag " + i);
                break;
            }
        }
    }
    
    private int calculateTagValueOffset(final int n, final int n2, final int n3) {
        if (n <= 4) {
            return n2 + 8;
        }
        final int get32Bits = this.get32Bits(n2 + 8);
        if (get32Bits + n > this._data.length) {
            return -1;
        }
        return n3 + get32Bits;
    }
    
    private String readString(final int offset, final int n) {
        int length;
        for (length = 0; offset + length < this._data.length && this._data[offset + length] != 0 && length < n; ++length) {}
        return new String(this._data, offset, length);
    }
    
    private int calculateTagOffset(final int n, final int n2) {
        return n + 2 + 12 * n2;
    }
    
    private int get16Bits(final int i) {
        if (i < 0 || i + 2 > this._data.length) {
            throw new ArrayIndexOutOfBoundsException("attempt to read data outside of exif segment (index " + i + " where max index is " + (this._data.length - 1) + ")");
        }
        if (this._isMotorollaByteOrder) {
            return (this._data[i] << 8 & 0xFF00) | (this._data[i + 1] & 0xFF);
        }
        return (this._data[i + 1] << 8 & 0xFF00) | (this._data[i] & 0xFF);
    }
    
    private int get32Bits(final int i) {
        if (i < 0 || i + 4 > this._data.length) {
            throw new ArrayIndexOutOfBoundsException("attempt to read data outside of exif segment (index " + i + " where max index is " + (this._data.length - 1) + ")");
        }
        if (this._isMotorollaByteOrder) {
            return (this._data[i] << 24 & 0xFF000000) | (this._data[i + 1] << 16 & 0xFF0000) | (this._data[i + 2] << 8 & 0xFF00) | (this._data[i + 3] & 0xFF);
        }
        return (this._data[i + 3] << 24 & 0xFF000000) | (this._data[i + 2] << 16 & 0xFF0000) | (this._data[i + 1] << 8 & 0xFF00) | (this._data[i] & 0xFF);
    }
    
    static {
        BYTES_PER_FORMAT = new int[] { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8 };
    }
}
