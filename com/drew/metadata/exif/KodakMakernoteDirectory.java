// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import java.util.HashMap;
import com.drew.metadata.Directory;

public class KodakMakernoteDirectory extends Directory
{
    protected static final HashMap _tagNameMap;
    
    public String getName() {
        return "Kodak Makernote";
    }
    
    protected HashMap getTagNameMap() {
        return KodakMakernoteDirectory._tagNameMap;
    }
    
    static {
        _tagNameMap = new HashMap();
    }
}
