// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class CasioType2MakernoteDescriptor extends TagDescriptor
{
    public CasioType2MakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 2: {
                return this.getThumbnailDimensionsDescription();
            }
            case 3: {
                return this.getThumbnailSizeDescription();
            }
            case 4: {
                return this.getThumbnailOffsetDescription();
            }
            case 8: {
                return this.getQualityModeDescription();
            }
            case 9: {
                return this.getImageSizeDescription();
            }
            case 13: {
                return this.getFocusMode1Description();
            }
            case 20: {
                return this.getIsoSensitivityDescription();
            }
            case 25: {
                return this.getWhiteBalance1Description();
            }
            case 29: {
                return this.getFocalLengthDescription();
            }
            case 31: {
                return this.getSaturationDescription();
            }
            case 32: {
                return this.getContrastDescription();
            }
            case 33: {
                return this.getSharpnessDescription();
            }
            case 3584: {
                return this.getPrintImageMatchingInfoDescription();
            }
            case 8192: {
                return this.getCasioPreviewThumbnailDescription();
            }
            case 8209: {
                return this.getWhiteBalanceBiasDescription();
            }
            case 8210: {
                return this.getWhiteBalance2Description();
            }
            case 8226: {
                return this.getObjectDistanceDescription();
            }
            case 8244: {
                return this.getFlashDistanceDescription();
            }
            case 12288: {
                return this.getRecordModeDescription();
            }
            case 12289: {
                return this.getSelfTimerDescription();
            }
            case 12290: {
                return this.getQualityDescription();
            }
            case 12291: {
                return this.getFocusMode2Description();
            }
            case 12294: {
                return this.getTimeZoneDescription();
            }
            case 12295: {
                return this.getBestShotModeDescription();
            }
            case 12308: {
                return this.getCcdIsoSensitivityDescription();
            }
            case 12309: {
                return this.getColourModeDescription();
            }
            case 12310: {
                return this.getEnhancementDescription();
            }
            case 12311: {
                return this.getFilterDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getFilterDescription() throws MetadataException {
        if (!this._directory.containsTag(12311)) {
            return null;
        }
        final int int1 = this._directory.getInt(12311);
        switch (int1) {
            case 0: {
                return "Off";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getEnhancementDescription() throws MetadataException {
        if (!this._directory.containsTag(12310)) {
            return null;
        }
        final int int1 = this._directory.getInt(12310);
        switch (int1) {
            case 0: {
                return "Off";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getColourModeDescription() throws MetadataException {
        if (!this._directory.containsTag(12309)) {
            return null;
        }
        final int int1 = this._directory.getInt(12309);
        switch (int1) {
            case 0: {
                return "Off";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getCcdIsoSensitivityDescription() throws MetadataException {
        if (!this._directory.containsTag(12308)) {
            return null;
        }
        final int int1 = this._directory.getInt(12308);
        switch (int1) {
            case 0: {
                return "Off";
            }
            case 1: {
                return "On";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getBestShotModeDescription() throws MetadataException {
        if (!this._directory.containsTag(12295)) {
            return null;
        }
        return "Unknown (" + this._directory.getInt(12295) + ")";
    }
    
    public String getTimeZoneDescription() {
        if (!this._directory.containsTag(12294)) {
            return null;
        }
        return this._directory.getString(12294);
    }
    
    public String getFocusMode2Description() throws MetadataException {
        if (!this._directory.containsTag(12291)) {
            return null;
        }
        final int int1 = this._directory.getInt(12291);
        switch (int1) {
            case 1: {
                return "Fixation";
            }
            case 6: {
                return "Multi-Area Focus";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getQualityDescription() throws MetadataException {
        if (!this._directory.containsTag(12290)) {
            return null;
        }
        final int int1 = this._directory.getInt(12290);
        switch (int1) {
            case 3: {
                return "Fine";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSelfTimerDescription() throws MetadataException {
        if (!this._directory.containsTag(12289)) {
            return null;
        }
        final int int1 = this._directory.getInt(12289);
        switch (int1) {
            case 1: {
                return "Off";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getRecordModeDescription() throws MetadataException {
        if (!this._directory.containsTag(12288)) {
            return null;
        }
        final int int1 = this._directory.getInt(12288);
        switch (int1) {
            case 2: {
                return "Normal";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFlashDistanceDescription() throws MetadataException {
        if (!this._directory.containsTag(8244)) {
            return null;
        }
        final int int1 = this._directory.getInt(8244);
        switch (int1) {
            case 0: {
                return "Off";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getObjectDistanceDescription() throws MetadataException {
        if (!this._directory.containsTag(8226)) {
            return null;
        }
        return Integer.toString(this._directory.getInt(8226)) + " mm";
    }
    
    public String getWhiteBalance2Description() throws MetadataException {
        if (!this._directory.containsTag(8210)) {
            return null;
        }
        final int int1 = this._directory.getInt(8210);
        switch (int1) {
            case 0: {
                return "Manual";
            }
            case 1: {
                return "Auto";
            }
            case 4: {
                return "Flash";
            }
            case 12: {
                return "Flash";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getWhiteBalanceBiasDescription() {
        if (!this._directory.containsTag(8209)) {
            return null;
        }
        return this._directory.getString(8209);
    }
    
    public String getCasioPreviewThumbnailDescription() throws MetadataException {
        if (!this._directory.containsTag(8192)) {
            return null;
        }
        return "<" + this._directory.getByteArray(8192).length + " bytes of image data>";
    }
    
    public String getPrintImageMatchingInfoDescription() {
        if (!this._directory.containsTag(3584)) {
            return null;
        }
        return this._directory.getString(3584);
    }
    
    public String getSharpnessDescription() throws MetadataException {
        if (!this._directory.containsTag(33)) {
            return null;
        }
        final int int1 = this._directory.getInt(33);
        switch (int1) {
            case 0: {
                return "-1";
            }
            case 1: {
                return "Normal";
            }
            case 2: {
                return "+1";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getContrastDescription() throws MetadataException {
        if (!this._directory.containsTag(32)) {
            return null;
        }
        final int int1 = this._directory.getInt(32);
        switch (int1) {
            case 0: {
                return "-1";
            }
            case 1: {
                return "Normal";
            }
            case 2: {
                return "+1";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getSaturationDescription() throws MetadataException {
        if (!this._directory.containsTag(31)) {
            return null;
        }
        final int int1 = this._directory.getInt(31);
        switch (int1) {
            case 0: {
                return "-1";
            }
            case 1: {
                return "Normal";
            }
            case 2: {
                return "+1";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocalLengthDescription() throws MetadataException {
        if (!this._directory.containsTag(29)) {
            return null;
        }
        return Double.toString(this._directory.getDouble(29) / 10.0) + " mm";
    }
    
    public String getWhiteBalance1Description() throws MetadataException {
        if (!this._directory.containsTag(25)) {
            return null;
        }
        final int int1 = this._directory.getInt(25);
        switch (int1) {
            case 0: {
                return "Auto";
            }
            case 1: {
                return "Daylight";
            }
            case 2: {
                return "Shade";
            }
            case 3: {
                return "Tungsten";
            }
            case 4: {
                return "Flourescent";
            }
            case 5: {
                return "Manual";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getIsoSensitivityDescription() throws MetadataException {
        if (!this._directory.containsTag(20)) {
            return null;
        }
        final int int1 = this._directory.getInt(20);
        switch (int1) {
            case 3: {
                return "50";
            }
            case 4: {
                return "64";
            }
            case 6: {
                return "100";
            }
            case 9: {
                return "200";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getFocusMode1Description() throws MetadataException {
        if (!this._directory.containsTag(13)) {
            return null;
        }
        final int int1 = this._directory.getInt(13);
        switch (int1) {
            case 0: {
                return "Normal";
            }
            case 1: {
                return "Macro";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getImageSizeDescription() throws MetadataException {
        if (!this._directory.containsTag(9)) {
            return null;
        }
        final int int1 = this._directory.getInt(9);
        switch (int1) {
            case 0: {
                return "640 x 480 pixels";
            }
            case 4: {
                return "1600 x 1200 pixels";
            }
            case 5: {
                return "2048 x 1536 pixels";
            }
            case 20: {
                return "2288 x 1712 pixels";
            }
            case 21: {
                return "2592 x 1944 pixels";
            }
            case 22: {
                return "2304 x 1728 pixels";
            }
            case 36: {
                return "3008 x 2008 pixels";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getQualityModeDescription() throws MetadataException {
        if (!this._directory.containsTag(8)) {
            return null;
        }
        final int int1 = this._directory.getInt(8);
        switch (int1) {
            case 1: {
                return "Fine";
            }
            case 2: {
                return "Super Fine";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getThumbnailOffsetDescription() {
        if (!this._directory.containsTag(4)) {
            return null;
        }
        return this._directory.getString(4);
    }
    
    public String getThumbnailSizeDescription() throws MetadataException {
        if (!this._directory.containsTag(3)) {
            return null;
        }
        return Integer.toString(this._directory.getInt(3)) + " bytes";
    }
    
    public String getThumbnailDimensionsDescription() throws MetadataException {
        if (!this._directory.containsTag(2)) {
            return null;
        }
        final int[] intArray = this._directory.getIntArray(2);
        if (intArray.length != 2) {
            return this._directory.getString(2);
        }
        return intArray[0] + " x " + intArray[1] + " pixels";
    }
}
