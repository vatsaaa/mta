// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class GpsDirectory extends Directory
{
    public static final int TAG_GPS_VERSION_ID = 0;
    public static final int TAG_GPS_LATITUDE_REF = 1;
    public static final int TAG_GPS_LATITUDE = 2;
    public static final int TAG_GPS_LONGITUDE_REF = 3;
    public static final int TAG_GPS_LONGITUDE = 4;
    public static final int TAG_GPS_ALTITUDE_REF = 5;
    public static final int TAG_GPS_ALTITUDE = 6;
    public static final int TAG_GPS_TIME_STAMP = 7;
    public static final int TAG_GPS_SATELLITES = 8;
    public static final int TAG_GPS_STATUS = 9;
    public static final int TAG_GPS_MEASURE_MODE = 10;
    public static final int TAG_GPS_DOP = 11;
    public static final int TAG_GPS_SPEED_REF = 12;
    public static final int TAG_GPS_SPEED = 13;
    public static final int TAG_GPS_TRACK_REF = 14;
    public static final int TAG_GPS_TRACK = 15;
    public static final int TAG_GPS_IMG_DIRECTION_REF = 16;
    public static final int TAG_GPS_IMG_DIRECTION = 17;
    public static final int TAG_GPS_MAP_DATUM = 18;
    public static final int TAG_GPS_DEST_LATITUDE_REF = 19;
    public static final int TAG_GPS_DEST_LATITUDE = 20;
    public static final int TAG_GPS_DEST_LONGITUDE_REF = 21;
    public static final int TAG_GPS_DEST_LONGITUDE = 22;
    public static final int TAG_GPS_DEST_BEARING_REF = 23;
    public static final int TAG_GPS_DEST_BEARING = 24;
    public static final int TAG_GPS_DEST_DISTANCE_REF = 25;
    public static final int TAG_GPS_DEST_DISTANCE = 26;
    protected static final HashMap tagNameMap;
    
    public GpsDirectory() {
        this.setDescriptor(new GpsDescriptor(this));
    }
    
    public String getName() {
        return "GPS";
    }
    
    protected HashMap getTagNameMap() {
        return GpsDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(0), "GPS Version ID");
        GpsDirectory.tagNameMap.put(new Integer(1), "GPS Latitude Ref");
        GpsDirectory.tagNameMap.put(new Integer(2), "GPS Latitude");
        GpsDirectory.tagNameMap.put(new Integer(3), "GPS Longitude Ref");
        GpsDirectory.tagNameMap.put(new Integer(4), "GPS Longitude");
        GpsDirectory.tagNameMap.put(new Integer(5), "GPS Altitude Ref");
        GpsDirectory.tagNameMap.put(new Integer(6), "GPS Altitude");
        GpsDirectory.tagNameMap.put(new Integer(7), "GPS Time-Stamp");
        GpsDirectory.tagNameMap.put(new Integer(8), "GPS Satellites");
        GpsDirectory.tagNameMap.put(new Integer(9), "GPS Status");
        GpsDirectory.tagNameMap.put(new Integer(10), "GPS Measure Mode");
        GpsDirectory.tagNameMap.put(new Integer(11), "GPS DOP");
        GpsDirectory.tagNameMap.put(new Integer(12), "GPS Speed Ref");
        GpsDirectory.tagNameMap.put(new Integer(13), "GPS Speed");
        GpsDirectory.tagNameMap.put(new Integer(14), "GPS Track Ref");
        GpsDirectory.tagNameMap.put(new Integer(15), "GPS Track");
        GpsDirectory.tagNameMap.put(new Integer(16), "GPS Img Direction Ref");
        GpsDirectory.tagNameMap.put(new Integer(16), "GPS Img Direction");
        GpsDirectory.tagNameMap.put(new Integer(18), "GPS Map Datum");
        GpsDirectory.tagNameMap.put(new Integer(19), "GPS Dest Latitude Ref");
        GpsDirectory.tagNameMap.put(new Integer(20), "GPS Dest Latitude");
        GpsDirectory.tagNameMap.put(new Integer(21), "GPS Dest Longitude Ref");
        GpsDirectory.tagNameMap.put(new Integer(22), "GPS Dest Longitude");
        GpsDirectory.tagNameMap.put(new Integer(23), "GPS Dest Bearing Ref");
        GpsDirectory.tagNameMap.put(new Integer(24), "GPS Dest Bearing");
        GpsDirectory.tagNameMap.put(new Integer(25), "GPS Dest Distance Ref");
        GpsDirectory.tagNameMap.put(new Integer(26), "GPS Dest Distance");
    }
}
