// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.exif;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class PanasonicMakernoteDescriptor extends TagDescriptor
{
    public PanasonicMakernoteDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 28: {
                return this.getMacroModeDescription();
            }
            case 31: {
                return this.getRecordModeDescription();
            }
            case 3584: {
                return this.getPrintImageMatchingInfoDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getPrintImageMatchingInfoDescription() throws MetadataException {
        if (!this._directory.containsTag(3584)) {
            return null;
        }
        return "(" + this._directory.getByteArray(3584).length + " bytes)";
    }
    
    public String getMacroModeDescription() throws MetadataException {
        if (!this._directory.containsTag(28)) {
            return null;
        }
        final int int1 = this._directory.getInt(28);
        switch (int1) {
            case 1: {
                return "On";
            }
            case 2: {
                return "Off";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
    
    public String getRecordModeDescription() throws MetadataException {
        if (!this._directory.containsTag(31)) {
            return null;
        }
        final int int1 = this._directory.getInt(31);
        switch (int1) {
            case 1: {
                return "Normal";
            }
            case 2: {
                return "Portrait";
            }
            case 9: {
                return "Macro";
            }
            default: {
                return "Unknown (" + int1 + ")";
            }
        }
    }
}
