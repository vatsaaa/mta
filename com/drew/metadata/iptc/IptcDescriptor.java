// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.iptc;

import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class IptcDescriptor extends TagDescriptor
{
    public IptcDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) {
        return this._directory.getString(n);
    }
    
    public String getByLineDescription() {
        return this._directory.getString(592);
    }
    
    public String getByLineTitleDescription() {
        return this._directory.getString(597);
    }
    
    public String getCaptionDescription() {
        return this._directory.getString(632);
    }
    
    public String getCategoryDescription() {
        return this._directory.getString(527);
    }
    
    public String getCityDescription() {
        return this._directory.getString(602);
    }
    
    public String getCopyrightNoticeDescription() {
        return this._directory.getString(628);
    }
    
    public String getCountryOrPrimaryLocationDescription() {
        return this._directory.getString(613);
    }
    
    public String getCreditDescription() {
        return this._directory.getString(622);
    }
    
    public String getDateCreatedDescription() {
        return this._directory.getString(567);
    }
    
    public String getHeadlineDescription() {
        return this._directory.getString(617);
    }
    
    public String getKeywordsDescription() {
        return this._directory.getString(537);
    }
    
    public String getObjectNameDescription() {
        return this._directory.getString(517);
    }
    
    public String getOriginalTransmissionReferenceDescription() {
        return this._directory.getString(615);
    }
    
    public String getOriginatingProgramDescription() {
        return this._directory.getString(577);
    }
    
    public String getProvinceOrStateDescription() {
        return this._directory.getString(607);
    }
    
    public String getRecordVersionDescription() {
        return this._directory.getString(512);
    }
    
    public String getReleaseDateDescription() {
        return this._directory.getString(542);
    }
    
    public String getReleaseTimeDescription() {
        return this._directory.getString(547);
    }
    
    public String getSourceDescription() {
        return this._directory.getString(627);
    }
    
    public String getSpecialInstructionsDescription() {
        return this._directory.getString(552);
    }
    
    public String getSupplementalCategoriesDescription() {
        return this._directory.getString(532);
    }
    
    public String getTimeCreatedDescription() {
        return this._directory.getString(572);
    }
    
    public String getUrgencyDescription() {
        return this._directory.getString(522);
    }
    
    public String getWriterDescription() {
        return this._directory.getString(634);
    }
}
