// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.iptc;

import com.drew.metadata.MetadataException;

public class IptcProcessingException extends MetadataException
{
    public IptcProcessingException(final String s) {
        super(s);
    }
    
    public IptcProcessingException(final String s, final Throwable t) {
        super(s, t);
    }
}
