// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.iptc;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class IptcDirectory extends Directory
{
    public static final int TAG_RECORD_VERSION = 512;
    public static final int TAG_CAPTION = 632;
    public static final int TAG_WRITER = 634;
    public static final int TAG_HEADLINE = 617;
    public static final int TAG_SPECIAL_INSTRUCTIONS = 552;
    public static final int TAG_BY_LINE = 592;
    public static final int TAG_BY_LINE_TITLE = 597;
    public static final int TAG_CREDIT = 622;
    public static final int TAG_SOURCE = 627;
    public static final int TAG_OBJECT_NAME = 517;
    public static final int TAG_DATE_CREATED = 567;
    public static final int TAG_CITY = 602;
    public static final int TAG_PROVINCE_OR_STATE = 607;
    public static final int TAG_COUNTRY_OR_PRIMARY_LOCATION = 613;
    public static final int TAG_ORIGINAL_TRANSMISSION_REFERENCE = 615;
    public static final int TAG_CATEGORY = 527;
    public static final int TAG_SUPPLEMENTAL_CATEGORIES = 532;
    public static final int TAG_URGENCY = 522;
    public static final int TAG_KEYWORDS = 537;
    public static final int TAG_COPYRIGHT_NOTICE = 628;
    public static final int TAG_RELEASE_DATE = 542;
    public static final int TAG_RELEASE_TIME = 547;
    public static final int TAG_TIME_CREATED = 572;
    public static final int TAG_ORIGINATING_PROGRAM = 577;
    protected static final HashMap tagNameMap;
    
    public IptcDirectory() {
        this.setDescriptor(new IptcDescriptor(this));
    }
    
    public String getName() {
        return "Iptc";
    }
    
    protected HashMap getTagNameMap() {
        return IptcDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(512), "Directory Version");
        IptcDirectory.tagNameMap.put(new Integer(632), "Caption/Abstract");
        IptcDirectory.tagNameMap.put(new Integer(634), "Writer/Editor");
        IptcDirectory.tagNameMap.put(new Integer(617), "Headline");
        IptcDirectory.tagNameMap.put(new Integer(552), "Special Instructions");
        IptcDirectory.tagNameMap.put(new Integer(592), "By-line");
        IptcDirectory.tagNameMap.put(new Integer(597), "By-line Title");
        IptcDirectory.tagNameMap.put(new Integer(622), "Credit");
        IptcDirectory.tagNameMap.put(new Integer(627), "Source");
        IptcDirectory.tagNameMap.put(new Integer(517), "Object Name");
        IptcDirectory.tagNameMap.put(new Integer(567), "Date Created");
        IptcDirectory.tagNameMap.put(new Integer(602), "City");
        IptcDirectory.tagNameMap.put(new Integer(607), "Province/State");
        IptcDirectory.tagNameMap.put(new Integer(613), "Country/Primary Location");
        IptcDirectory.tagNameMap.put(new Integer(615), "Original Transmission Reference");
        IptcDirectory.tagNameMap.put(new Integer(527), "Category");
        IptcDirectory.tagNameMap.put(new Integer(532), "Supplemental Category(s)");
        IptcDirectory.tagNameMap.put(new Integer(522), "Urgency");
        IptcDirectory.tagNameMap.put(new Integer(537), "Keywords");
        IptcDirectory.tagNameMap.put(new Integer(628), "Copyright Notice");
        IptcDirectory.tagNameMap.put(new Integer(542), "Release Date");
        IptcDirectory.tagNameMap.put(new Integer(547), "Release Time");
        IptcDirectory.tagNameMap.put(new Integer(572), "Time Created");
        IptcDirectory.tagNameMap.put(new Integer(577), "Originating Program");
    }
}
