// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.iptc;

import java.util.GregorianCalendar;
import com.drew.metadata.Directory;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Metadata;
import java.io.InputStream;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.imaging.jpeg.JpegSegmentReader;
import java.io.File;
import com.drew.metadata.MetadataReader;

public class IptcReader implements MetadataReader
{
    private final byte[] _data;
    
    public IptcReader(final File file) throws JpegProcessingException {
        this(new JpegSegmentReader(file).readSegment((byte)(-19)));
    }
    
    public IptcReader(final InputStream inputStream) throws JpegProcessingException {
        this(new JpegSegmentReader(inputStream).readSegment((byte)(-19)));
    }
    
    public IptcReader(final byte[] data) {
        this._data = data;
    }
    
    public Metadata extract() {
        return this.extract(new Metadata());
    }
    
    public Metadata extract(final Metadata metadata) {
        if (this._data == null) {
            return metadata;
        }
        final Directory directory = metadata.getDirectory(IptcDirectory.class);
        int i = 0;
        try {
            while (i < this._data.length - 1 && this.get32Bits(i) != 7170) {
                ++i;
            }
        }
        catch (MetadataException ex) {
            directory.addError("Couldn't find start of Iptc data (invalid segment)");
            return metadata;
        }
        while (i < this._data.length) {
            if (this._data[i] != 28) {
                break;
            }
            if (i + 5 >= this._data.length) {
                break;
            }
            ++i;
            byte b;
            byte b2;
            int get32Bits;
            try {
                b = this._data[i++];
                b2 = this._data[i++];
                get32Bits = this.get32Bits(i);
            }
            catch (MetadataException ex2) {
                directory.addError("Iptc data segment ended mid-way through tag descriptor");
                return metadata;
            }
            i += 2;
            if (i + get32Bits > this._data.length) {
                directory.addError("data for tag extends beyond end of iptc segment");
                break;
            }
            this.processTag(directory, b, b2, i, get32Bits);
            i += get32Bits;
        }
        return metadata;
    }
    
    private int get32Bits(final int n) throws MetadataException {
        if (n >= this._data.length) {
            throw new MetadataException("Attempt to read bytes from outside Iptc data buffer");
        }
        return (this._data[n] & 0xFF) << 8 | (this._data[n + 1] & 0xFF);
    }
    
    private void processTag(final Directory directory, final int n, final int n2, final int n3, final int n4) {
        final int n5 = n2 | n << 8;
        switch (n5) {
            case 512: {
                directory.setInt(n5, (short)(this._data[n3] << 8 | this._data[n3 + 1]));
                return;
            }
            case 522: {
                directory.setInt(n5, this._data[n3]);
                return;
            }
            case 542:
            case 567: {
                if (n4 >= 8) {
                    final String s = new String(this._data, n3, n4);
                    try {
                        directory.setDate(n5, new GregorianCalendar(Integer.parseInt(s.substring(0, 4)), Integer.parseInt(s.substring(4, 6)) - 1, Integer.parseInt(s.substring(6, 8))).getTime());
                        return;
                    }
                    catch (NumberFormatException ex) {}
                    break;
                }
                break;
            }
        }
        String s2;
        if (n4 < 1) {
            s2 = "";
        }
        else {
            s2 = new String(this._data, n3, n4);
        }
        if (directory.containsTag(n5)) {
            String[] stringArray;
            try {
                stringArray = directory.getStringArray(n5);
            }
            catch (MetadataException ex2) {
                stringArray = null;
            }
            String[] array;
            if (stringArray == null) {
                array = new String[] { null };
            }
            else {
                array = new String[stringArray.length + 1];
                for (int i = 0; i < stringArray.length; ++i) {
                    array[i] = stringArray[i];
                }
            }
            array[array.length - 1] = s2;
            directory.setStringArray(n5, array);
        }
        else {
            directory.setString(n5, s2);
        }
    }
}
