// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.Serializable;

public final class Metadata implements Serializable
{
    private final HashMap directoryMap;
    private final ArrayList directoryList;
    
    public Metadata() {
        this.directoryMap = new HashMap();
        this.directoryList = new ArrayList();
    }
    
    public Iterator getDirectoryIterator() {
        return this.directoryList.iterator();
    }
    
    public int getDirectoryCount() {
        return this.directoryList.size();
    }
    
    public Directory getDirectory(final Class key) {
        if (!Directory.class.isAssignableFrom(key)) {
            throw new RuntimeException("Class type passed to getDirectory must be an implementation of com.drew.metadata.Directory");
        }
        if (this.directoryMap.containsKey(key)) {
            return (Directory)this.directoryMap.get(key);
        }
        Directory instance;
        try {
            instance = key.newInstance();
        }
        catch (Exception ex) {
            throw new RuntimeException("Cannot instantiate provided Directory type: " + key.toString());
        }
        this.directoryMap.put(key, instance);
        this.directoryList.add(instance);
        return instance;
    }
    
    public boolean containsDirectory(final Class key) {
        return this.directoryMap.containsKey(key);
    }
}
