// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata;

public interface MetadataReader
{
    Metadata extract();
    
    Metadata extract(final Metadata p0);
}
