// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.jpeg;

import com.drew.metadata.MetadataException;
import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class JpegDirectory extends Directory
{
    public static final int TAG_JPEG_DATA_PRECISION = 0;
    public static final int TAG_JPEG_IMAGE_HEIGHT = 1;
    public static final int TAG_JPEG_IMAGE_WIDTH = 3;
    public static final int TAG_JPEG_NUMBER_OF_COMPONENTS = 5;
    public static final int TAG_JPEG_COMPONENT_DATA_1 = 6;
    public static final int TAG_JPEG_COMPONENT_DATA_2 = 7;
    public static final int TAG_JPEG_COMPONENT_DATA_3 = 8;
    public static final int TAG_JPEG_COMPONENT_DATA_4 = 9;
    protected static final HashMap tagNameMap;
    
    public JpegDirectory() {
        this.setDescriptor(new JpegDescriptor(this));
    }
    
    public String getName() {
        return "Jpeg";
    }
    
    protected HashMap getTagNameMap() {
        return JpegDirectory.tagNameMap;
    }
    
    public JpegComponent getComponent(final int n) {
        return (JpegComponent)this.getObject(6 + n);
    }
    
    public int getImageWidth() throws MetadataException {
        return this.getInt(3);
    }
    
    public int getImageHeight() throws MetadataException {
        return this.getInt(1);
    }
    
    public int getNumberOfComponents() throws MetadataException {
        return this.getInt(5);
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(0), "Data Precision");
        JpegDirectory.tagNameMap.put(new Integer(3), "Image Width");
        JpegDirectory.tagNameMap.put(new Integer(1), "Image Height");
        JpegDirectory.tagNameMap.put(new Integer(5), "Number of Components");
        JpegDirectory.tagNameMap.put(new Integer(6), "Component 1");
        JpegDirectory.tagNameMap.put(new Integer(7), "Component 2");
        JpegDirectory.tagNameMap.put(new Integer(8), "Component 3");
        JpegDirectory.tagNameMap.put(new Integer(9), "Component 4");
    }
}
