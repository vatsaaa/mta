// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.jpeg;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class JpegDescriptor extends TagDescriptor
{
    public JpegDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) throws MetadataException {
        switch (n) {
            case 6: {
                return this.getComponentDataDescription(0);
            }
            case 7: {
                return this.getComponentDataDescription(1);
            }
            case 8: {
                return this.getComponentDataDescription(2);
            }
            case 9: {
                return this.getComponentDataDescription(3);
            }
            case 0: {
                return this.getDataPrecisionDescription();
            }
            case 1: {
                return this.getImageHeightDescription();
            }
            case 3: {
                return this.getImageWidthDescription();
            }
            default: {
                return this._directory.getString(n);
            }
        }
    }
    
    public String getImageWidthDescription() {
        return this._directory.getString(3) + " pixels";
    }
    
    public String getImageHeightDescription() {
        return this._directory.getString(1) + " pixels";
    }
    
    public String getDataPrecisionDescription() {
        return this._directory.getString(0) + " bits";
    }
    
    public String getComponentDataDescription(final int i) throws MetadataException {
        final JpegComponent component = ((JpegDirectory)this._directory).getComponent(i);
        if (component == null) {
            throw new MetadataException("No Jpeg component exists with number " + i);
        }
        final StringBuffer sb = new StringBuffer();
        sb.append(component.getComponentName());
        sb.append(" component: Quantization table ");
        sb.append(component.getQuantizationTableNumber());
        sb.append(", Sampling factors ");
        sb.append(component.getHorizontalSamplingFactor());
        sb.append(" horiz/");
        sb.append(component.getVerticalSamplingFactor());
        sb.append(" vert");
        return sb.toString();
    }
}
