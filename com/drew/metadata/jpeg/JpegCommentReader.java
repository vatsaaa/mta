// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.jpeg;

import com.drew.metadata.Metadata;
import java.io.InputStream;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.imaging.jpeg.JpegSegmentReader;
import java.io.File;
import com.drew.metadata.MetadataReader;

public class JpegCommentReader implements MetadataReader
{
    private final byte[] _data;
    
    public JpegCommentReader(final File file) throws JpegProcessingException {
        this(new JpegSegmentReader(file).readSegment((byte)(-2)));
    }
    
    public JpegCommentReader(final InputStream inputStream) throws JpegProcessingException {
        this(new JpegSegmentReader(inputStream).readSegment((byte)(-19)));
    }
    
    public JpegCommentReader(final byte[] data) {
        this._data = data;
    }
    
    public Metadata extract() {
        return this.extract(new Metadata());
    }
    
    public Metadata extract(final Metadata metadata) {
        if (this._data == null) {
            return metadata;
        }
        ((JpegCommentDirectory)metadata.getDirectory(JpegCommentDirectory.class)).setString(0, new String(this._data));
        return metadata;
    }
}
