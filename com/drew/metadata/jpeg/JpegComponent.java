// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.jpeg;

import com.drew.metadata.MetadataException;
import java.io.Serializable;

public class JpegComponent implements Serializable
{
    private final int _componentId;
    private final int _samplingFactorByte;
    private final int _quantizationTableNumber;
    
    public JpegComponent(final int componentId, final int samplingFactorByte, final int quantizationTableNumber) {
        this._componentId = componentId;
        this._samplingFactorByte = samplingFactorByte;
        this._quantizationTableNumber = quantizationTableNumber;
    }
    
    public int getComponentId() {
        return this._componentId;
    }
    
    public String getComponentName() throws MetadataException {
        switch (this._componentId) {
            case 1: {
                return "Y";
            }
            case 2: {
                return "Cb";
            }
            case 3: {
                return "Cr";
            }
            case 4: {
                return "I";
            }
            case 5: {
                return "Q";
            }
            default: {
                throw new MetadataException("Unsupported component id: " + this._componentId);
            }
        }
    }
    
    public int getQuantizationTableNumber() {
        return this._quantizationTableNumber;
    }
    
    public int getHorizontalSamplingFactor() {
        return this._samplingFactorByte & 0xF;
    }
    
    public int getVerticalSamplingFactor() {
        return this._samplingFactorByte >> 4 & 0xF;
    }
}
