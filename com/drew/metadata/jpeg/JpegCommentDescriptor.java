// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.jpeg;

import com.drew.metadata.Directory;
import com.drew.metadata.TagDescriptor;

public class JpegCommentDescriptor extends TagDescriptor
{
    public JpegCommentDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getDescription(final int n) {
        return this._directory.getString(n);
    }
    
    public String getJpegCommentDescription() {
        return this._directory.getString(0);
    }
}
