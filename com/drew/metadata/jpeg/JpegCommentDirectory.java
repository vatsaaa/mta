// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.jpeg;

import com.drew.metadata.TagDescriptor;
import java.util.HashMap;
import com.drew.metadata.Directory;

public class JpegCommentDirectory extends Directory
{
    public static final int TAG_JPEG_COMMENT = 0;
    protected static final HashMap tagNameMap;
    
    public JpegCommentDirectory() {
        this.setDescriptor(new JpegCommentDescriptor(this));
    }
    
    public String getName() {
        return "JpegComment";
    }
    
    protected HashMap getTagNameMap() {
        return JpegCommentDirectory.tagNameMap;
    }
    
    static {
        (tagNameMap = new HashMap()).put(new Integer(0), "Jpeg Comment");
    }
}
