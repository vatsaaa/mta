// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata.jpeg;

import com.drew.metadata.MetadataException;
import com.drew.metadata.Metadata;
import java.io.InputStream;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.imaging.jpeg.JpegSegmentReader;
import java.io.File;
import com.drew.metadata.MetadataReader;

public class JpegReader implements MetadataReader
{
    private final byte[] _data;
    
    public JpegReader(final File file) throws JpegProcessingException {
        this(new JpegSegmentReader(file).readSegment((byte)(-64)));
    }
    
    public JpegReader(final InputStream inputStream) throws JpegProcessingException {
        this(new JpegSegmentReader(inputStream).readSegment((byte)(-19)));
    }
    
    public JpegReader(final byte[] data) {
        this._data = data;
    }
    
    public Metadata extract() {
        return this.extract(new Metadata());
    }
    
    public Metadata extract(final Metadata metadata) {
        if (this._data == null) {
            return metadata;
        }
        final JpegDirectory jpegDirectory = (JpegDirectory)metadata.getDirectory(JpegDirectory.class);
        try {
            jpegDirectory.setInt(0, this.get16Bits(0));
            jpegDirectory.setInt(1, this.get32Bits(1));
            jpegDirectory.setInt(3, this.get32Bits(3));
            final int get16Bits = this.get16Bits(5);
            jpegDirectory.setInt(5, get16Bits);
            int n = 6;
            for (int i = 0; i < get16Bits; ++i) {
                jpegDirectory.setObject(6 + i, new JpegComponent(this.get16Bits(n++), this.get16Bits(n++), this.get16Bits(n++)));
            }
        }
        catch (MetadataException obj) {
            jpegDirectory.addError("MetadataException: " + obj);
        }
        return metadata;
    }
    
    private int get32Bits(final int n) throws MetadataException {
        if (n + 1 >= this._data.length) {
            throw new MetadataException("Attempt to read bytes from outside Jpeg segment data buffer");
        }
        return (this._data[n] & 0xFF) << 8 | (this._data[n + 1] & 0xFF);
    }
    
    private int get16Bits(final int n) throws MetadataException {
        if (n >= this._data.length) {
            throw new MetadataException("Attempt to read bytes from outside Jpeg segment data buffer");
        }
        return this._data[n] & 0xFF;
    }
}
