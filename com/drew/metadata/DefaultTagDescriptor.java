// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata;

public class DefaultTagDescriptor extends TagDescriptor
{
    public DefaultTagDescriptor(final Directory directory) {
        super(directory);
    }
    
    public String getTagName(final int i) {
        String s;
        for (s = Integer.toHexString(i).toUpperCase(); s.length() < 4; s = "0" + s) {}
        return "Unknown tag 0x" + s;
    }
    
    public String getDescription(final int n) {
        return this._directory.getString(n);
    }
}
