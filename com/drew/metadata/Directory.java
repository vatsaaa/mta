// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.drew.lang.Rational;
import java.util.Date;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.io.Serializable;

public abstract class Directory implements Serializable
{
    protected final HashMap _tagMap;
    protected TagDescriptor _descriptor;
    protected final List _definedTagList;
    private List _errorList;
    
    public abstract String getName();
    
    protected abstract HashMap getTagNameMap();
    
    public Directory() {
        this._tagMap = new HashMap();
        this._definedTagList = new ArrayList();
    }
    
    public boolean containsTag(final int value) {
        return this._tagMap.containsKey(new Integer(value));
    }
    
    public Iterator getTagIterator() {
        return this._definedTagList.iterator();
    }
    
    public int getTagCount() {
        return this._definedTagList.size();
    }
    
    public void setDescriptor(final TagDescriptor descriptor) {
        if (descriptor == null) {
            throw new NullPointerException("cannot set a null descriptor");
        }
        this._descriptor = descriptor;
    }
    
    public void addError(final String s) {
        if (this._errorList == null) {
            this._errorList = new ArrayList();
        }
        this._errorList.add(s);
    }
    
    public boolean hasErrors() {
        return this._errorList != null && this._errorList.size() > 0;
    }
    
    public Iterator getErrors() {
        return this._errorList.iterator();
    }
    
    public int getErrorCount() {
        return this._errorList.size();
    }
    
    public void setInt(final int n, final int value) {
        this.setObject(n, new Integer(value));
    }
    
    public void setDouble(final int n, final double value) {
        this.setObject(n, new Double(value));
    }
    
    public void setFloat(final int n, final float value) {
        this.setObject(n, new Float(value));
    }
    
    public void setString(final int n, final String s) {
        this.setObject(n, s);
    }
    
    public void setBoolean(final int n, final boolean value) {
        this.setObject(n, new Boolean(value));
    }
    
    public void setLong(final int n, final long value) {
        this.setObject(n, new Long(value));
    }
    
    public void setDate(final int n, final Date date) {
        this.setObject(n, date);
    }
    
    public void setRational(final int n, final Rational rational) {
        this.setObject(n, rational);
    }
    
    public void setRationalArray(final int n, final Rational[] array) {
        this.setObjectArray(n, array);
    }
    
    public void setIntArray(final int n, final int[] array) {
        this.setObjectArray(n, array);
    }
    
    public void setByteArray(final int n, final byte[] array) {
        this.setObjectArray(n, array);
    }
    
    public void setStringArray(final int n, final String[] array) {
        this.setObjectArray(n, array);
    }
    
    public void setObject(final int value, final Object value2) {
        if (value2 == null) {
            throw new NullPointerException("cannot set a null object");
        }
        final Integer n = new Integer(value);
        if (!this._tagMap.containsKey(n)) {
            this._definedTagList.add(new Tag(value, this));
        }
        this._tagMap.put(n, value2);
    }
    
    public void setObjectArray(final int n, final Object o) {
        this.setObject(n, o);
    }
    
    public int getInt(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof String) {
            try {
                return Integer.parseInt((String)object);
            }
            catch (NumberFormatException ex) {
                final byte[] bytes = ((String)object).getBytes();
                long n = 0L;
                for (int j = 0; j < bytes.length; ++j) {
                    n = (n << 8) + bytes[j];
                }
                return (int)n;
            }
        }
        if (object instanceof Number) {
            return ((Number)object).intValue();
        }
        if (object instanceof Rational[]) {
            final Rational[] array = (Rational[])object;
            if (array.length == 1) {
                return array[0].intValue();
            }
        }
        else if (object instanceof byte[]) {
            final byte[] array2 = (byte[])object;
            if (array2.length == 1) {
                return array2[0];
            }
        }
        else if (object instanceof int[]) {
            final int[] array3 = (int[])object;
            if (array3.length == 1) {
                return array3[0];
            }
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to int.  It is of type '" + ((int[])object).getClass() + "'.");
    }
    
    public String[] getStringArray(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof String[]) {
            return (String[])object;
        }
        if (object instanceof String) {
            return new String[] { (String)object };
        }
        if (object instanceof int[]) {
            final int[] array = (int[])object;
            final String[] array2 = new String[array.length];
            for (int j = 0; j < array2.length; ++j) {
                array2[j] = Integer.toString(array[j]);
            }
            return array2;
        }
        if (object instanceof byte[]) {
            final byte[] array3 = (byte[])object;
            final String[] array4 = new String[array3.length];
            for (int k = 0; k < array4.length; ++k) {
                array4[k] = Byte.toString(array3[k]);
            }
            return array4;
        }
        if (object instanceof Rational[]) {
            final Rational[] array5 = (Rational[])object;
            final String[] array6 = new String[array5.length];
            for (int l = 0; l < array6.length; ++l) {
                array6[l] = array5[l].toSimpleString(false);
            }
            return array6;
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to an String array.  It is of type '" + ((Rational[])object).getClass() + "'.");
    }
    
    public int[] getIntArray(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof Rational[]) {
            final Rational[] array = (Rational[])object;
            final int[] array2 = new int[array.length];
            for (int j = 0; j < array2.length; ++j) {
                array2[j] = array[j].intValue();
            }
            return array2;
        }
        if (object instanceof int[]) {
            return (int[])object;
        }
        if (object instanceof byte[]) {
            final byte[] array3 = (byte[])object;
            final int[] array4 = new int[array3.length];
            for (int k = 0; k < array3.length; ++k) {
                array4[k] = array3[k];
            }
            return array4;
        }
        if (object instanceof String) {
            final String s = (String)object;
            final int[] array5 = new int[s.length()];
            for (int l = 0; l < s.length(); ++l) {
                array5[l] = s.charAt(l);
            }
            return array5;
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to an int array.  It is of type '" + ((String)object).getClass() + "'.");
    }
    
    public byte[] getByteArray(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof Rational[]) {
            final Rational[] array = (Rational[])object;
            final byte[] array2 = new byte[array.length];
            for (int j = 0; j < array2.length; ++j) {
                array2[j] = array[j].byteValue();
            }
            return array2;
        }
        if (object instanceof byte[]) {
            return (byte[])object;
        }
        if (object instanceof int[]) {
            final int[] array3 = (int[])object;
            final byte[] array4 = new byte[array3.length];
            for (int k = 0; k < array3.length; ++k) {
                array4[k] = (byte)array3[k];
            }
            return array4;
        }
        if (object instanceof String) {
            final String s = (String)object;
            final byte[] array5 = new byte[s.length()];
            for (int l = 0; l < s.length(); ++l) {
                array5[l] = (byte)s.charAt(l);
            }
            return array5;
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a byte array.  It is of type '" + ((String)object).getClass() + "'.");
    }
    
    public double getDouble(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof String) {
            try {
                return Double.parseDouble((String)object);
            }
            catch (NumberFormatException ex) {
                throw new MetadataException("unable to parse string " + object + " as a double", ex);
            }
        }
        if (object instanceof Number) {
            return ((Number)object).doubleValue();
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a double.  It is of type '" + ((Number)object).getClass() + "'.");
    }
    
    public float getFloat(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof String) {
            try {
                return Float.parseFloat((String)object);
            }
            catch (NumberFormatException ex) {
                throw new MetadataException("unable to parse string " + object + " as a float", ex);
            }
        }
        if (object instanceof Number) {
            return ((Number)object).floatValue();
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a float.  It is of type '" + ((Number)object).getClass() + "'.");
    }
    
    public long getLong(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof String) {
            try {
                return Long.parseLong((String)object);
            }
            catch (NumberFormatException ex) {
                throw new MetadataException("unable to parse string " + object + " as a long", ex);
            }
        }
        if (object instanceof Number) {
            return ((Number)object).longValue();
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a long.  It is of type '" + ((Number)object).getClass() + "'.");
    }
    
    public boolean getBoolean(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof Boolean) {
            return (boolean)object;
        }
        if (object instanceof String) {
            try {
                return Boolean.getBoolean((String)object);
            }
            catch (NumberFormatException ex) {
                throw new MetadataException("unable to parse string " + object + " as a boolean", ex);
            }
        }
        if (object instanceof Number) {
            return ((Number)object).doubleValue() != 0.0;
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a boolean.  It is of type '" + ((Number)object).getClass() + "'.");
    }
    
    public Date getDate(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof Date) {
            return (Date)object;
        }
        if (object instanceof String) {
            final String[] array = { "yyyy:MM:dd HH:mm:ss", "yyyy:MM:dd HH:mm", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm" };
            final String source = (String)object;
            int j = 0;
            while (j < array.length) {
                try {
                    return new SimpleDateFormat(array[j]).parse(source);
                }
                catch (ParseException ex) {
                    ++j;
                    continue;
                }
                break;
            }
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a java.util.Date.  It is of type '" + ((String)object).getClass() + "'.");
    }
    
    public Rational getRational(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof Rational) {
            return (Rational)object;
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a Rational.  It is of type '" + ((Rational)object).getClass() + "'.");
    }
    
    public Rational[] getRationalArray(final int i) throws MetadataException {
        final Object object = this.getObject(i);
        if (object == null) {
            throw new MetadataException("Tag " + this.getTagName(i) + " has not been set -- check using containsTag() first");
        }
        if (object instanceof Rational[]) {
            return (Rational[])object;
        }
        throw new MetadataException("Tag '" + i + "' cannot be cast to a Rational array.  It is of type '" + ((Rational[])object).getClass() + "'.");
    }
    
    public String getString(final int n) {
        final Object object = this.getObject(n);
        if (object == null) {
            return null;
        }
        if (object instanceof Rational) {
            return ((Rational)object).toSimpleString(true);
        }
        if (((Rational)object).getClass().isArray()) {
            final int length = Array.getLength(object);
            final boolean startsWith = ((Rational)object).getClass().toString().startsWith("class [L");
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < length; ++i) {
                if (i != 0) {
                    sb.append(' ');
                }
                if (startsWith) {
                    sb.append(Array.get(object, i).toString());
                }
                else {
                    sb.append(Array.getInt(object, i));
                }
            }
            return sb.toString();
        }
        return object.toString();
    }
    
    public Object getObject(final int value) {
        return this._tagMap.get(new Integer(value));
    }
    
    public String getTagName(final int n) {
        final Integer n2 = new Integer(n);
        final HashMap tagNameMap = this.getTagNameMap();
        if (!tagNameMap.containsKey(n2)) {
            String s;
            for (s = Integer.toHexString(n); s.length() < 4; s = "0" + s) {}
            return "Unknown tag (0x" + s + ")";
        }
        return tagNameMap.get(n2);
    }
    
    public String getDescription(final int n) throws MetadataException {
        if (this._descriptor == null) {
            throw new MetadataException("a descriptor must be set using setDescriptor(...) before descriptions can be provided");
        }
        return this._descriptor.getDescription(n);
    }
}
