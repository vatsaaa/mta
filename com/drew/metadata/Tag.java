// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata;

import java.io.Serializable;

public class Tag implements Serializable
{
    private final int _tagType;
    private final Directory _directory;
    
    public Tag(final int tagType, final Directory directory) {
        this._tagType = tagType;
        this._directory = directory;
    }
    
    public int getTagType() {
        return this._tagType;
    }
    
    public String getTagTypeHex() {
        String s;
        for (s = Integer.toHexString(this._tagType); s.length() < 4; s = "0" + s) {}
        return "0x" + s;
    }
    
    public String getDescription() throws MetadataException {
        return this._directory.getDescription(this._tagType);
    }
    
    public String getTagName() {
        return this._directory.getTagName(this._tagType);
    }
    
    public String getDirectoryName() {
        return this._directory.getName();
    }
    
    public String toString() {
        String str;
        try {
            str = this.getDescription();
        }
        catch (MetadataException ex) {
            str = this._directory.getString(this.getTagType()) + " (unable to formulate description)";
        }
        return "[" + this._directory.getName() + "] " + this.getTagName() + " - " + str;
    }
}
