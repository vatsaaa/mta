// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata;

import java.io.Serializable;

public abstract class TagDescriptor implements Serializable
{
    protected final Directory _directory;
    
    public TagDescriptor(final Directory directory) {
        this._directory = directory;
    }
    
    public abstract String getDescription(final int p0) throws MetadataException;
}
