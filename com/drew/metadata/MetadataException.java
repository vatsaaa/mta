// 
// Decompiled by Procyon v0.5.36
// 

package com.drew.metadata;

import com.drew.lang.CompoundException;

public class MetadataException extends CompoundException
{
    public MetadataException(final String s) {
        super(s);
    }
    
    public MetadataException(final Throwable t) {
        super(t);
    }
    
    public MetadataException(final String s, final Throwable t) {
        super(s, t);
    }
}
