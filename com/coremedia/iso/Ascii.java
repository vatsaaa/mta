// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.io.UnsupportedEncodingException;

public final class Ascii
{
    public static byte[] convert(final String s) {
        try {
            if (s != null) {
                return s.getBytes("us-ascii");
            }
            return null;
        }
        catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }
    
    public static String convert(final byte[] b) {
        try {
            if (b != null) {
                return new String(b, "us-ascii");
            }
            return null;
        }
        catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }
}
