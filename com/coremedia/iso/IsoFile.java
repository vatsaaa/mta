// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import com.coremedia.iso.boxes.MovieBox;
import java.util.Iterator;
import java.io.UnsupportedEncodingException;
import com.coremedia.iso.boxes.Box;
import java.io.EOFException;
import com.coremedia.iso.boxes.ContainerBox;
import java.nio.ByteBuffer;
import java.io.IOException;
import java.nio.channels.ReadableByteChannel;
import com.googlecode.mp4parser.DoNotParseDetail;
import com.coremedia.iso.boxes.AbstractContainerBox;

@DoNotParseDetail
public class IsoFile extends AbstractContainerBox
{
    protected BoxParser boxParser;
    ReadableByteChannel byteChannel;
    
    public IsoFile() {
        super("");
        this.boxParser = new PropertyBoxParserImpl(new String[0]);
    }
    
    public IsoFile(final ReadableByteChannel byteChannel) throws IOException {
        super("");
        this.boxParser = new PropertyBoxParserImpl(new String[0]);
        this.byteChannel = byteChannel;
        this.boxParser = this.createBoxParser();
        this.parse();
    }
    
    public IsoFile(final ReadableByteChannel byteChannel, final BoxParser boxParser) throws IOException {
        super("");
        this.boxParser = new PropertyBoxParserImpl(new String[0]);
        this.byteChannel = byteChannel;
        this.boxParser = boxParser;
        this.parse();
    }
    
    protected BoxParser createBoxParser() {
        return new PropertyBoxParserImpl(new String[0]);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
    }
    
    public void parse(final ReadableByteChannel inFC, final ByteBuffer header, final long contentSize, final AbstractBoxParser abstractBoxParser) throws IOException {
        throw new IOException("This method is not meant to be called. Use #parse() directly.");
    }
    
    private void parse() throws IOException {
        boolean done = false;
        while (!done) {
            try {
                final Box box = this.boxParser.parseBox(this.byteChannel, this);
                if (box != null) {
                    this.boxes.add(box);
                }
                else {
                    done = true;
                }
            }
            catch (EOFException ex) {
                done = true;
            }
        }
    }
    
    @DoNotParseDetail
    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder();
        buffer.append("IsoFile[");
        if (this.boxes == null) {
            buffer.append("unparsed");
        }
        else {
            for (int i = 0; i < this.boxes.size(); ++i) {
                if (i > 0) {
                    buffer.append(";");
                }
                buffer.append(this.boxes.get(i).toString());
            }
        }
        buffer.append("]");
        return buffer.toString();
    }
    
    @DoNotParseDetail
    public static byte[] fourCCtoBytes(final String fourCC) {
        final byte[] result = new byte[4];
        if (fourCC != null) {
            for (int i = 0; i < Math.min(4, fourCC.length()); ++i) {
                result[i] = (byte)fourCC.charAt(i);
            }
        }
        return result;
    }
    
    @DoNotParseDetail
    public static String bytesToFourCC(final byte[] type) {
        final byte[] result = new byte[4];
        if (type != null) {
            System.arraycopy(type, 0, result, 0, Math.min(type.length, 4));
        }
        try {
            return new String(result, "ISO-8859-1");
        }
        catch (UnsupportedEncodingException e) {
            throw new Error("Required character encoding is missing", e);
        }
    }
    
    @Override
    public long getNumOfBytesToFirstChild() {
        return 0L;
    }
    
    @Override
    public long getSize() {
        long size = 0L;
        for (final Box box : this.boxes) {
            size += box.getSize();
        }
        return size;
    }
    
    @Override
    public IsoFile getIsoFile() {
        return this;
    }
    
    @DoNotParseDetail
    public MovieBox getMovieBox() {
        for (final Box box : this.boxes) {
            if (box instanceof MovieBox) {
                return (MovieBox)box;
            }
        }
        return null;
    }
    
    @Override
    public void getBox(final WritableByteChannel os) throws IOException {
        for (final Box box : this.boxes) {
            if (os instanceof FileChannel) {
                final long startPos = ((FileChannel)os).position();
                box.getBox(os);
                final long size = ((FileChannel)os).position() - startPos;
                assert size == box.getSize();
                continue;
            }
            else {
                box.getBox(os);
            }
        }
    }
}
