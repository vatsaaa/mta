// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.io.UnsupportedEncodingException;

public final class Utf8
{
    public static byte[] convert(final String s) {
        try {
            if (s != null) {
                return s.getBytes("UTF-8");
            }
            return null;
        }
        catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }
    
    public static String convert(final byte[] b) {
        try {
            if (b != null) {
                return new String(b, "UTF-8");
            }
            return null;
        }
        catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }
    
    public static int utf8StringLengthInBytes(final String utf8) {
        try {
            if (utf8 != null) {
                return utf8.getBytes("UTF-8").length;
            }
            return 0;
        }
        catch (UnsupportedEncodingException ex) {
            throw new RuntimeException();
        }
    }
}
