// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.io.IOException;
import java.nio.ByteBuffer;

public final class IsoTypeWriter
{
    public static void writeUInt64(final ByteBuffer bb, final long u) {
        writeUInt32(bb, u >> 32 & 0xFFFFFFFFL);
        writeUInt32(bb, u & 0xFFFFFFFFL);
    }
    
    public static void writeUInt32(final ByteBuffer bb, final long u) {
        assert u >= 0L && u <= 4294967296L : "The given long is not in the range of uint32 (" + u + ")";
        writeUInt16(bb, (int)(u >> 16 & 0xFFFFL));
        writeUInt16(bb, (int)u & 0xFFFF);
    }
    
    public static void writeUInt24(final ByteBuffer bb, int i) {
        i &= 0xFFFFFF;
        writeUInt16(bb, i >> 8);
        writeUInt8(bb, i);
    }
    
    public static void writeUInt16(final ByteBuffer bb, int i) {
        i &= 0xFFFF;
        writeUInt8(bb, i >> 8);
        writeUInt8(bb, i & 0xFF);
    }
    
    public static void writeUInt8(final ByteBuffer bb, final int i) {
        bb.put(int2byte(i));
    }
    
    public static void writeFixedPont1616(final ByteBuffer bb, final double v) throws IOException {
        final int result = (int)(v * 65536.0);
        bb.put((byte)((result & 0xFF000000) >> 24));
        bb.put((byte)((result & 0xFF0000) >> 16));
        bb.put((byte)((result & 0xFF00) >> 8));
        bb.put((byte)(result & 0xFF));
    }
    
    public static void writeFixedPont88(final ByteBuffer bb, final double v) throws IOException {
        final short result = (short)(v * 256.0);
        bb.put((byte)((result & 0xFF00) >> 8));
        bb.put((byte)(result & 0xFF));
    }
    
    public static byte int2byte(int i) {
        i &= 0xFF;
        return (byte)((i > 127) ? (i - 256) : i);
    }
    
    public static void writeIso639(final ByteBuffer bb, final String language) {
        int bits = 0;
        for (int i = 0; i < 3; ++i) {
            bits += language.getBytes()[i] - 96 << (2 - i) * 5;
        }
        writeUInt16(bb, bits);
    }
    
    public static void writeUtf8String(final ByteBuffer bb, final String string) {
        bb.put(Utf8.convert(string));
        writeUInt8(bb, 0);
    }
}
