// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

public class Hex
{
    private static final char[] DIGITS;
    
    static {
        DIGITS = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    }
    
    public static String encodeHex(final byte[] data) {
        final int l = data.length;
        final char[] out = new char[l << 1];
        int i = 0;
        int j = 0;
        while (i < l) {
            out[j++] = Hex.DIGITS[(0xF0 & data[i]) >>> 4];
            out[j++] = Hex.DIGITS[0xF & data[i]];
            ++i;
        }
        return new String(out);
    }
}
