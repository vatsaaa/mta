// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.ContainerBox;

public class IsoFileConvenienceHelper
{
    public static Box get(final ContainerBox containerBox, final String path) {
        final String[] parts = path.split("/");
        if (parts.length == 0) {
            return null;
        }
        final List<String> partList = new ArrayList<String>(Arrays.asList(parts));
        if ("".equals(partList.get(0))) {
            partList.remove(0);
        }
        if (partList.size() > 0) {
            return get(containerBox.getBoxes(), partList);
        }
        return null;
    }
    
    private static Box get(final List<Box> boxes, final List<String> path) {
        final String typeInPath = path.remove(0);
        for (final Box box : boxes) {
            if (box instanceof ContainerBox) {
                final ContainerBox boxContainer = (ContainerBox)box;
                final String type = boxContainer.getType();
                if (!typeInPath.equals(type)) {
                    continue;
                }
                final List<Box> children = boxContainer.getBoxes();
                if (path.size() <= 0) {
                    return box;
                }
                if (children.size() > 0) {
                    return get(children, path);
                }
                continue;
            }
            else {
                final String type2 = box.getType();
                if (path.size() == 0 && typeInPath.equals(type2)) {
                    return box;
                }
                continue;
            }
        }
        return null;
    }
}
