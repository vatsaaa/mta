// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.io.IOException;
import com.coremedia.iso.boxes.CastUtils;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import com.coremedia.iso.boxes.ContainerBox;
import java.nio.channels.ReadableByteChannel;
import com.coremedia.iso.boxes.Box;
import java.util.logging.Logger;

public abstract class AbstractBoxParser implements BoxParser
{
    private static Logger LOG;
    
    static {
        AbstractBoxParser.LOG = Logger.getLogger(AbstractBoxParser.class.getName());
    }
    
    public abstract Box createBox(final String p0, final byte[] p1, final String p2);
    
    public Box parseBox(final ReadableByteChannel byteChannel, final ContainerBox parent) throws IOException {
        ByteBuffer header = ChannelHelper.readFully(byteChannel, 8L);
        long size = IsoTypeReader.readUInt32(header);
        if (size < 8L && size > 1L) {
            AbstractBoxParser.LOG.severe("Plausibility check failed: size < 8 (size = " + size + "). Stop parsing!");
            return null;
        }
        final String type = IsoTypeReader.read4cc(header);
        String prefix = "";
        final boolean iWant = false;
        if (iWant) {
            for (ContainerBox t = parent.getParent(); t != null; t = t.getParent()) {
                prefix = String.valueOf(t.getType()) + "/" + prefix;
            }
        }
        byte[] usertype = null;
        long contentSize;
        if (size == 1L) {
            final ByteBuffer bb = ByteBuffer.allocate(8);
            byteChannel.read(bb);
            bb.rewind();
            size = IsoTypeReader.readUInt64(bb);
            contentSize = size - 16L;
        }
        else if (size == 0L) {
            if (!(byteChannel instanceof FileChannel)) {
                throw new RuntimeException("Only FileChannel inputs may use size == 0 (box reaches to the end of file)");
            }
            size = ((FileChannel)byteChannel).size() - ((FileChannel)byteChannel).position() - 8L;
            contentSize = size - 8L;
        }
        else {
            contentSize = size - 8L;
        }
        if ("uuid".equals(type)) {
            final ByteBuffer bb = ByteBuffer.allocate(16);
            byteChannel.read(bb);
            bb.rewind();
            usertype = bb.array();
            contentSize -= 16L;
        }
        final Box box = this.createBox(type, usertype, parent.getType());
        box.setParent(parent);
        AbstractBoxParser.LOG.finest("Parsing " + box.getType());
        if (CastUtils.l2i(size - contentSize) == 8) {
            header.rewind();
        }
        else if (CastUtils.l2i(size - contentSize) == 16) {
            header = ByteBuffer.allocate(16);
            IsoTypeWriter.writeUInt32(header, 1L);
            header.put(IsoFile.fourCCtoBytes(type));
            IsoTypeWriter.writeUInt64(header, size);
        }
        else if (CastUtils.l2i(size - contentSize) == 24) {
            header = ByteBuffer.allocate(24);
            IsoTypeWriter.writeUInt32(header, size);
            header.put(IsoFile.fourCCtoBytes(type));
            header.put(usertype);
        }
        else {
            if (CastUtils.l2i(size - contentSize) != 32) {
                throw new RuntimeException("I didn't expect that");
            }
            header = ByteBuffer.allocate(32);
            IsoTypeWriter.writeUInt32(header, size);
            header.put(IsoFile.fourCCtoBytes(type));
            IsoTypeWriter.writeUInt64(header, size);
            header.put(usertype);
        }
        box.parse(byteChannel, header, contentSize, this);
        assert size == box.getSize() : "Reconstructed Size is not x to the number of parsed bytes! (" + box.getType() + ")" + " Actual Box size: " + size + " Calculated size: " + box.getSize();
        return box;
    }
}
