// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.io.IOException;
import com.coremedia.iso.boxes.ContainerBox;
import java.nio.channels.ReadableByteChannel;
import com.coremedia.iso.boxes.Box;

public interface BoxParser
{
    Class<? extends Box> getClassForFourCc(final String p0, final byte[] p1, final String p2);
    
    Box parseBox(final ReadableByteChannel p0, final ContainerBox p1) throws IOException;
}
