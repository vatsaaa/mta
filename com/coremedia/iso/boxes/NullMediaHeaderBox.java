// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import java.io.IOException;
import java.nio.ByteBuffer;

public class NullMediaHeaderBox extends AbstractMediaHeaderBox
{
    public NullMediaHeaderBox() {
        super("nmhd");
    }
    
    @Override
    protected long getContentSize() {
        return 4L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
    }
}
