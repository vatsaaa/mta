// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import java.io.IOException;
import java.nio.ByteBuffer;

public class FreeBox extends AbstractBox
{
    public static final String TYPE = "free";
    ByteBuffer data;
    
    public FreeBox() {
        super("free");
    }
    
    public FreeBox(final int size) {
        super("free");
        this.data = ByteBuffer.allocate(size);
    }
    
    @Override
    protected long getContentSize() {
        return this.data.limit();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        (this.data = content).position(this.data.position() + this.data.remaining());
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.data.rewind();
        bb.put(this.data);
    }
}
