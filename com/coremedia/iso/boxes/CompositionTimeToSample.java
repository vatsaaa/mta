// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.util.ArrayList;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.Collections;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class CompositionTimeToSample extends AbstractFullBox
{
    public static final String TYPE = "ctts";
    List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    static {
        ajc$preClinit();
    }
    
    public CompositionTimeToSample() {
        super("ctts");
        this.entries = Collections.emptyList();
    }
    
    @Override
    protected long getContentSize() {
        return 8 + 8 * this.entries.size();
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionTimeToSample.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionTimeToSample.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final int numberOfEntries = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.entries = new ArrayList<Entry>(numberOfEntries);
        for (int i = 0; i < numberOfEntries; ++i) {
            final Entry e = new Entry(CastUtils.l2i(IsoTypeReader.readUInt32(content)), content.getInt());
            this.entries.add(e);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.entries.size());
        for (final Entry entry : this.entries) {
            IsoTypeWriter.writeUInt32(bb, entry.getCount());
            bb.putInt(entry.getOffset());
        }
    }
    
    public static int[] blowupCompositionTimes(final List<Entry> entries) {
        long numOfSamples = 0L;
        for (final Entry entry : entries) {
            numOfSamples += entry.getCount();
        }
        assert numOfSamples <= 2147483647L;
        final int[] decodingTime = new int[(int)numOfSamples];
        int current = 0;
        for (final Entry entry2 : entries) {
            for (int i = 0; i < entry2.getCount(); ++i) {
                decodingTime[current++] = entry2.getOffset();
            }
        }
        return decodingTime;
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("CompositionTimeToSample.java", CompositionTimeToSample.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.CompositionTimeToSample", "", "", "", "java.util.List"), 58);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.CompositionTimeToSample", "java.util.List", "entries", "", "void"), 62);
    }
    
    public static class Entry
    {
        int count;
        int offset;
        
        public Entry(final int count, final int offset) {
            this.count = count;
            this.offset = offset;
        }
        
        public int getCount() {
            return this.count;
        }
        
        public int getOffset() {
            return this.offset;
        }
        
        public void setCount(final int count) {
            this.count = count;
        }
        
        public void setOffset(final int offset) {
            this.offset = offset;
        }
        
        @Override
        public String toString() {
            return "Entry{count=" + this.count + ", offset=" + this.offset + '}';
        }
    }
}
