// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.util.Iterator;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class MediaInformationBox extends AbstractContainerBox
{
    public static final String TYPE = "minf";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public MediaInformationBox() {
        super("minf");
    }
    
    public SampleTableBox getSampleTableBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaInformationBox.ajc$tjp_0, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof SampleTableBox) {
                return (SampleTableBox)box;
            }
        }
        return null;
    }
    
    public AbstractMediaHeaderBox getMediaHeaderBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaInformationBox.ajc$tjp_1, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof AbstractMediaHeaderBox) {
                return (AbstractMediaHeaderBox)box;
            }
        }
        return null;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MediaInformationBox.java", MediaInformationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleTableBox", "com.coremedia.iso.boxes.MediaInformationBox", "", "", "", "com.coremedia.iso.boxes.SampleTableBox"), 29);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMediaHeaderBox", "com.coremedia.iso.boxes.MediaInformationBox", "", "", "", "com.coremedia.iso.boxes.AbstractMediaHeaderBox"), 38);
    }
}
