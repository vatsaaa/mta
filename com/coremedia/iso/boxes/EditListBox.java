// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class EditListBox extends AbstractFullBox
{
    private List<Entry> entries;
    public static final String TYPE = "elst";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public EditListBox() {
        super("elst");
        this.entries = new LinkedList<Entry>();
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EditListBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EditListBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 8L;
        if (this.getVersion() == 1) {
            contentSize += this.entries.size() * 20;
        }
        else {
            contentSize += this.entries.size() * 12;
        }
        return contentSize;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final int entryCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.entries = new LinkedList<Entry>();
        for (int i = 0; i < entryCount; ++i) {
            this.entries.add(new Entry(this, content));
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.entries.size());
        for (final Entry entry : this.entries) {
            entry.getContent(bb);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(EditListBox.ajc$tjp_2, this, this));
        return "EditListBox{entries=" + this.entries + '}';
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("EditListBox.java", EditListBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.EditListBox", "", "", "", "java.util.List"), 64);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.EditListBox", "java.util.List", "entries", "", "void"), 68);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.EditListBox", "", "", "", "java.lang.String"), 104);
    }
    
    public static class Entry
    {
        private long segmentDuration;
        private long mediaTime;
        private double mediaRate;
        EditListBox editListBox;
        
        public Entry(final EditListBox editListBox, final long segmentDuration, final long mediaTime, final double mediaRate) {
            this.segmentDuration = segmentDuration;
            this.mediaTime = mediaTime;
            this.mediaRate = mediaRate;
            this.editListBox = editListBox;
        }
        
        public Entry(final EditListBox editListBox, final ByteBuffer bb) {
            if (editListBox.getVersion() == 1) {
                this.segmentDuration = IsoTypeReader.readUInt64(bb);
                this.mediaTime = IsoTypeReader.readUInt64(bb);
                this.mediaRate = IsoTypeReader.readFixedPoint1616(bb);
            }
            else {
                this.segmentDuration = IsoTypeReader.readUInt32(bb);
                this.mediaTime = IsoTypeReader.readUInt32(bb);
                this.mediaRate = IsoTypeReader.readFixedPoint1616(bb);
            }
            this.editListBox = editListBox;
        }
        
        public long getSegmentDuration() {
            return this.segmentDuration;
        }
        
        public void setSegmentDuration(final long segmentDuration) {
            this.segmentDuration = segmentDuration;
        }
        
        public long getMediaTime() {
            return this.mediaTime;
        }
        
        public void setMediaTime(final long mediaTime) {
            this.mediaTime = mediaTime;
        }
        
        public double getMediaRate() {
            return this.mediaRate;
        }
        
        public void setMediaRate(final double mediaRate) {
            this.mediaRate = mediaRate;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            final Entry entry = (Entry)o;
            return this.mediaTime == entry.mediaTime && this.segmentDuration == entry.segmentDuration;
        }
        
        @Override
        public int hashCode() {
            int result = (int)(this.segmentDuration ^ this.segmentDuration >>> 32);
            result = 31 * result + (int)(this.mediaTime ^ this.mediaTime >>> 32);
            return result;
        }
        
        public void getContent(final ByteBuffer bb) throws IOException {
            if (this.editListBox.getVersion() == 1) {
                IsoTypeWriter.writeUInt64(bb, this.segmentDuration);
                IsoTypeWriter.writeUInt64(bb, this.mediaTime);
            }
            else {
                IsoTypeWriter.writeUInt32(bb, CastUtils.l2i(this.segmentDuration));
                bb.putInt(CastUtils.l2i(this.mediaTime));
            }
            IsoTypeWriter.writeFixedPont1616(bb, this.mediaRate);
        }
        
        @Override
        public String toString() {
            return "Entry{segmentDuration=" + this.segmentDuration + ", mediaTime=" + this.mediaTime + ", mediaRate=" + this.mediaRate + '}';
        }
    }
}
