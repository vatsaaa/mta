// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class SyncSampleBox extends AbstractFullBox
{
    public static final String TYPE = "stss";
    private long[] sampleNumber;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public SyncSampleBox() {
        super("stss");
    }
    
    public long[] getSampleNumber() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SyncSampleBox.ajc$tjp_0, this, this));
        return this.sampleNumber;
    }
    
    @Override
    protected long getContentSize() {
        return this.sampleNumber.length * 4 + 8;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final int entryCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.sampleNumber = new long[entryCount];
        for (int i = 0; i < entryCount; ++i) {
            this.sampleNumber[i] = IsoTypeReader.readUInt32(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer byteBuffer) throws IOException {
        this.writeVersionAndFlags(byteBuffer);
        IsoTypeWriter.writeUInt32(byteBuffer, this.sampleNumber.length);
        long[] sampleNumber;
        for (int length = (sampleNumber = this.sampleNumber).length, i = 0; i < length; ++i) {
            final long aSampleNumber = sampleNumber[i];
            IsoTypeWriter.writeUInt32(byteBuffer, aSampleNumber);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SyncSampleBox.ajc$tjp_1, this, this));
        return "SyncSampleBox[entryCount=" + this.sampleNumber.length + "]";
    }
    
    public void setSampleNumber(final long[] sampleNumber) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SyncSampleBox.ajc$tjp_2, this, this, sampleNumber));
        this.sampleNumber = sampleNumber;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SyncSampleBox.java", SyncSampleBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleNumber", "com.coremedia.iso.boxes.SyncSampleBox", "", "", "", "[J"), 45);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.SyncSampleBox", "", "", "", "java.lang.String"), 76);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleNumber", "com.coremedia.iso.boxes.SyncSampleBox", "[J", "sampleNumber", "", "void"), 80);
    }
}
