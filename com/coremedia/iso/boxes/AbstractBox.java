// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.nio.channels.WritableByteChannel;
import com.coremedia.iso.IsoTypeWriter;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.ChannelHelper;
import java.nio.channels.FileChannel;
import com.coremedia.iso.BoxParser;
import java.nio.channels.ReadableByteChannel;
import com.coremedia.iso.IsoFile;
import com.googlecode.mp4parser.DoNotParseDetail;
import org.aspectj.lang.JoinPoint;
import java.util.logging.Logger;
import java.nio.ByteBuffer;

public abstract class AbstractBox implements Box
{
    private ByteBuffer content;
    private static Logger LOG;
    protected String type;
    private byte[] userType;
    private ContainerBox parent;
    protected ByteBuffer deadBytes;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    static {
        ajc$preClinit();
        AbstractBox.LOG = Logger.getLogger(AbstractBox.class.getName());
    }
    
    public long getSize() {
        long size = (this.content == null) ? this.getContentSize() : this.content.limit();
        size += 8 + ((size >= 4294967288L) ? 8 : 0) + ("uuid".equals(this.getType()) ? 16 : 0);
        size += ((this.deadBytes == null) ? 0 : this.deadBytes.limit());
        return size;
    }
    
    public boolean isParsed() {
        return this.content == null;
    }
    
    protected abstract long getContentSize();
    
    protected AbstractBox(final String type) {
        this.deadBytes = null;
        this.type = type;
    }
    
    @DoNotParseDetail
    public String getType() {
        return this.type;
    }
    
    @DoNotParseDetail
    public byte[] getUserType() {
        return this.userType;
    }
    
    @DoNotParseDetail
    public void setUserType(final byte[] userType) {
        this.userType = userType;
    }
    
    @DoNotParseDetail
    public ContainerBox getParent() {
        return this.parent;
    }
    
    @DoNotParseDetail
    public void setParent(final ContainerBox parent) {
        this.parent = parent;
    }
    
    @DoNotParseDetail
    public IsoFile getIsoFile() {
        return this.parent.getIsoFile();
    }
    
    @DoNotParseDetail
    public void parse(final ReadableByteChannel in, final ByteBuffer header, final long contentSize, final BoxParser boxParser) throws IOException {
        if (in instanceof FileChannel && contentSize > 1048576L) {
            this.content = ((FileChannel)in).map(FileChannel.MapMode.READ_ONLY, ((FileChannel)in).position(), contentSize);
            ((FileChannel)in).position(((FileChannel)in).position() + contentSize);
        }
        else {
            assert contentSize < 2147483647L;
            this.content = ChannelHelper.readFully(in, contentSize);
        }
    }
    
    public final synchronized void parseDetails() {
        if (this.content != null) {
            final ByteBuffer content = this.content;
            this.content = null;
            content.rewind();
            this._parseDetails(content);
            if (content.remaining() > 0) {
                this.deadBytes = content.slice();
            }
            assert this.verify(content);
        }
    }
    
    private boolean verify(final ByteBuffer content) {
        final ByteBuffer bb = ByteBuffer.allocate(CastUtils.l2i(this.getContentSize() + ((this.deadBytes != null) ? this.deadBytes.limit() : 0)));
        try {
            this.getContent(bb);
            if (this.deadBytes != null) {
                this.deadBytes.rewind();
                while (this.deadBytes.remaining() > 0) {
                    bb.put(this.deadBytes);
                }
            }
        }
        catch (IOException e) {
            assert false : e.getMessage();
        }
        content.rewind();
        bb.rewind();
        if (content.remaining() != bb.remaining()) {
            AbstractBox.LOG.severe("remaining differs " + content.remaining() + " vs. " + bb.remaining());
            return false;
        }
        for (int p = content.position(), i = content.limit() - 1, j = bb.limit() - 1; i >= p; --i, --j) {
            final byte v1 = content.get(i);
            final byte v2 = bb.get(j);
            if (v1 != v2 && (v1 == v1 || v2 == v2)) {
                AbstractBox.LOG.severe("buffers differ at " + i + "/" + j);
                return false;
            }
        }
        return true;
    }
    
    public abstract void _parseDetails(final ByteBuffer p0);
    
    public ByteBuffer getDeadBytes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractBox.ajc$tjp_0, this, this));
        return this.deadBytes;
    }
    
    public void setDeadBytes(final ByteBuffer newDeadBytes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractBox.ajc$tjp_1, this, this, newDeadBytes));
        this.deadBytes = newDeadBytes;
    }
    
    public void getHeader(final ByteBuffer byteBuffer) {
        if (this.isSmallBox()) {
            IsoTypeWriter.writeUInt32(byteBuffer, this.getSize());
            byteBuffer.put(IsoFile.fourCCtoBytes(this.getType()));
        }
        else {
            IsoTypeWriter.writeUInt32(byteBuffer, 1L);
            byteBuffer.put(IsoFile.fourCCtoBytes(this.getType()));
            IsoTypeWriter.writeUInt64(byteBuffer, this.getSize());
        }
        if ("uuid".equals(this.getType())) {
            byteBuffer.put(this.getUserType());
        }
    }
    
    private boolean isSmallBox() {
        return ((this.content == null) ? (this.getContentSize() + ((this.deadBytes != null) ? this.deadBytes.limit() : 0) + 8L) : this.content.limit()) < 4294967296L;
    }
    
    public void getBox(final WritableByteChannel os) throws IOException {
        final ByteBuffer bb = ByteBuffer.allocate(CastUtils.l2i(this.getSize()));
        this.getHeader(bb);
        if (this.content == null) {
            this.getContent(bb);
            if (this.deadBytes != null) {
                this.deadBytes.rewind();
                while (this.deadBytes.remaining() > 0) {
                    bb.put(this.deadBytes);
                }
            }
        }
        else {
            this.content.rewind();
            bb.put(this.content);
        }
        bb.rewind();
        os.write(bb);
    }
    
    protected abstract void getContent(final ByteBuffer p0) throws IOException;
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AbstractBox.java", AbstractBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDeadBytes", "com.coremedia.iso.boxes.AbstractBox", "", "", "", "java.nio.ByteBuffer"), 193);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDeadBytes", "com.coremedia.iso.boxes.AbstractBox", "java.nio.ByteBuffer", "newDeadBytes", "", "void"), 197);
    }
}
