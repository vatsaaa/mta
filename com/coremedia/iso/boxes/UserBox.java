// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class UserBox extends AbstractBox
{
    byte[] data;
    public static final String TYPE = "uuid";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public UserBox(final byte[] userType) {
        super("uuid");
        this.setUserType(userType);
    }
    
    @Override
    protected long getContentSize() {
        return this.data.length;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(UserBox.ajc$tjp_0, this, this));
        return "UserBox[type=" + this.getType() + ";userType=" + new String(this.getUserType()) + ";contentLength=" + this.data.length + "]";
    }
    
    public byte[] getData() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(UserBox.ajc$tjp_1, this, this));
        return this.data;
    }
    
    public void setData(final byte[] data) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(UserBox.ajc$tjp_2, this, this, data));
        this.data = data;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        content.get(this.data = new byte[content.remaining()]);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(this.data);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("UserBox.java", UserBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.UserBox", "", "", "", "java.lang.String"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getData", "com.coremedia.iso.boxes.UserBox", "", "", "", "[B"), 46);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setData", "com.coremedia.iso.boxes.UserBox", "[B", "data", "", "void"), 50);
    }
}
