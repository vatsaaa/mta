// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.threegpp26244;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class LocationInformationBox extends AbstractFullBox
{
    public static final String TYPE = "loci";
    private String language;
    private String name;
    private int role;
    private double longitude;
    private double latitude;
    private double altitude;
    private String astronomicalBody;
    private String additionalNotes;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    
    public LocationInformationBox() {
        super("loci");
        this.name = "";
        this.astronomicalBody = "";
        this.additionalNotes = "";
    }
    
    public String getLanguage() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_0, this, this));
        return this.language;
    }
    
    public void setLanguage(final String language) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_1, this, this, language));
        this.language = language;
    }
    
    public String getName() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_2, this, this));
        return this.name;
    }
    
    public void setName(final String name) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_3, this, this, name));
        this.name = name;
    }
    
    public int getRole() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_4, this, this));
        return this.role;
    }
    
    public void setRole(final int role) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_5, this, this, Conversions.intObject(role)));
        this.role = role;
    }
    
    public double getLongitude() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_6, this, this));
        return this.longitude;
    }
    
    public void setLongitude(final double longitude) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_7, this, this, Conversions.doubleObject(longitude)));
        this.longitude = longitude;
    }
    
    public double getLatitude() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_8, this, this));
        return this.latitude;
    }
    
    public void setLatitude(final double latitude) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_9, this, this, Conversions.doubleObject(latitude)));
        this.latitude = latitude;
    }
    
    public double getAltitude() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_10, this, this));
        return this.altitude;
    }
    
    public void setAltitude(final double altitude) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_11, this, this, Conversions.doubleObject(altitude)));
        this.altitude = altitude;
    }
    
    public String getAstronomicalBody() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_12, this, this));
        return this.astronomicalBody;
    }
    
    public void setAstronomicalBody(final String astronomicalBody) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_13, this, this, astronomicalBody));
        this.astronomicalBody = astronomicalBody;
    }
    
    public String getAdditionalNotes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_14, this, this));
        return this.additionalNotes;
    }
    
    public void setAdditionalNotes(final String additionalNotes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LocationInformationBox.ajc$tjp_15, this, this, additionalNotes));
        this.additionalNotes = additionalNotes;
    }
    
    @Override
    protected long getContentSize() {
        return 22 + Utf8.convert(this.name).length + Utf8.convert(this.astronomicalBody).length + Utf8.convert(this.additionalNotes).length;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.language = IsoTypeReader.readIso639(content);
        this.name = IsoTypeReader.readString(content);
        this.role = IsoTypeReader.readUInt8(content);
        this.longitude = IsoTypeReader.readFixedPoint1616(content);
        this.latitude = IsoTypeReader.readFixedPoint1616(content);
        this.altitude = IsoTypeReader.readFixedPoint1616(content);
        this.astronomicalBody = IsoTypeReader.readString(content);
        this.additionalNotes = IsoTypeReader.readString(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeIso639(bb, this.language);
        bb.put(Utf8.convert(this.name));
        bb.put((byte)0);
        IsoTypeWriter.writeUInt8(bb, this.role);
        IsoTypeWriter.writeFixedPont1616(bb, this.longitude);
        IsoTypeWriter.writeFixedPont1616(bb, this.latitude);
        IsoTypeWriter.writeFixedPont1616(bb, this.altitude);
        bb.put(Utf8.convert(this.astronomicalBody));
        bb.put((byte)0);
        bb.put(Utf8.convert(this.additionalNotes));
        bb.put((byte)0);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("LocationInformationBox.java", LocationInformationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLanguage", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 30);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLanguage", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "language", "", "void"), 34);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAltitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "double"), 70);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAltitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "double", "altitude", "", "void"), 74);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAstronomicalBody", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 78);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAstronomicalBody", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "astronomicalBody", "", "void"), 82);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAdditionalNotes", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 86);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAdditionalNotes", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "additionalNotes", "", "void"), 90);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getName", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 38);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setName", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "name", "", "void"), 42);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getRole", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "int"), 46);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setRole", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "int", "role", "", "void"), 50);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLongitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "double"), 54);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLongitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "double", "longitude", "", "void"), 58);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLatitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "double"), 62);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLatitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "double", "latitude", "", "void"), 66);
    }
}
