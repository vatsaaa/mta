// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class RecordingYearBox extends AbstractFullBox
{
    public static final String TYPE = "yrrc";
    int recordingYear;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public RecordingYearBox() {
        super("yrrc");
    }
    
    @Override
    protected long getContentSize() {
        return 6L;
    }
    
    public int getRecordingYear() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RecordingYearBox.ajc$tjp_0, this, this));
        return this.recordingYear;
    }
    
    public void setRecordingYear(final int recordingYear) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RecordingYearBox.ajc$tjp_1, this, this, Conversions.intObject(recordingYear)));
        this.recordingYear = recordingYear;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.recordingYear = IsoTypeReader.readUInt16(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt16(bb, this.recordingYear);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("RecordingYearBox.java", RecordingYearBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getRecordingYear", "com.coremedia.iso.boxes.RecordingYearBox", "", "", "", "int"), 42);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setRecordingYear", "com.coremedia.iso.boxes.RecordingYearBox", "int", "recordingYear", "", "void"), 46);
    }
}
