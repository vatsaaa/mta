// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleSortAlbumBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "soal";
    
    public AppleSortAlbumBox() {
        super("soal");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
