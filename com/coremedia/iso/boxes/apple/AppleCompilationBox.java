// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleCompilationBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "cpil";
    
    public AppleCompilationBox() {
        super("cpil");
        this.appleDataBox = AppleDataBox.getUint8AppleDataBox();
    }
}
