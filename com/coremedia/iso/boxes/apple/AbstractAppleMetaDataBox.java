// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import java.math.BigInteger;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.Utf8;
import java.nio.channels.WritableByteChannel;
import java.io.IOException;
import com.coremedia.iso.BoxParser;
import java.nio.channels.ReadableByteChannel;
import com.googlecode.mp4parser.ByteBufferByteChannel;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import java.util.Collections;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.boxes.Box;
import java.util.List;
import org.aspectj.lang.JoinPoint;
import java.util.logging.Logger;
import com.coremedia.iso.boxes.ContainerBox;
import com.coremedia.iso.boxes.AbstractBox;

public abstract class AbstractAppleMetaDataBox extends AbstractBox implements ContainerBox
{
    private static Logger LOG;
    AppleDataBox appleDataBox;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    
    static {
        ajc$preClinit();
        AbstractAppleMetaDataBox.LOG = Logger.getLogger(AbstractAppleMetaDataBox.class.getName());
    }
    
    public List<Box> getBoxes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractAppleMetaDataBox.ajc$tjp_0, this, this));
        return (List<Box>)Collections.singletonList(this.appleDataBox);
    }
    
    public void setBoxes(final List<Box> boxes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractAppleMetaDataBox.ajc$tjp_1, this, this, boxes));
        if (boxes.size() == 1 && boxes.get(0) instanceof AppleDataBox) {
            this.appleDataBox = boxes.get(0);
            return;
        }
        throw new IllegalArgumentException("This box only accepts one AppleDataBox child");
    }
    
    public <T extends Box> List<T> getBoxes(final Class<T> clazz) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractAppleMetaDataBox.ajc$tjp_2, this, this, clazz));
        return this.getBoxes(clazz, false);
    }
    
    public <T extends Box> List<T> getBoxes(final Class<T> clazz, final boolean recursive) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractAppleMetaDataBox.ajc$tjp_3, this, this, clazz, Conversions.booleanObject(recursive)));
        if (clazz.isAssignableFrom(this.appleDataBox.getClass())) {
            return Collections.singletonList((T)this.appleDataBox);
        }
        return null;
    }
    
    public AbstractAppleMetaDataBox(final String type) {
        super(type);
        this.appleDataBox = new AppleDataBox();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        IsoTypeReader.readUInt32(content);
        final String thisShouldBeData = IsoTypeReader.read4cc(content);
        assert "data".equals(thisShouldBeData);
        this.appleDataBox = new AppleDataBox();
        try {
            this.appleDataBox.parse(new ByteBufferByteChannel(content), null, content.remaining(), null);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.appleDataBox.setParent(this);
    }
    
    @Override
    protected long getContentSize() {
        return this.appleDataBox.getSize();
    }
    
    @Override
    protected void getContent(final ByteBuffer os) throws IOException {
        this.appleDataBox.getBox(new ByteBufferByteChannel(os));
    }
    
    public long getNumOfBytesToFirstChild() {
        return this.getSize() - this.appleDataBox.getSize();
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractAppleMetaDataBox.ajc$tjp_4, this, this));
        return String.valueOf(this.getClass().getSimpleName()) + "{" + "appleDataBox=" + this.getValue() + '}';
    }
    
    static long toLong(final byte b) {
        return (b < 0) ? (b + 256) : b;
    }
    
    public void setValue(final String value) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractAppleMetaDataBox.ajc$tjp_5, this, this, value));
        if (this.appleDataBox.getFlags() == 1) {
            (this.appleDataBox = new AppleDataBox()).setVersion(0);
            this.appleDataBox.setFlags(1);
            this.appleDataBox.setFourBytes(new byte[4]);
            this.appleDataBox.setData(Utf8.convert(value));
        }
        else if (this.appleDataBox.getFlags() == 21) {
            final byte[] content = this.appleDataBox.getData();
            (this.appleDataBox = new AppleDataBox()).setVersion(0);
            this.appleDataBox.setFlags(21);
            this.appleDataBox.setFourBytes(new byte[4]);
            final ByteBuffer bb = ByteBuffer.allocate(content.length);
            if (content.length == 1) {
                IsoTypeWriter.writeUInt8(bb, Byte.parseByte(value) & 0xFF);
            }
            else if (content.length == 2) {
                IsoTypeWriter.writeUInt16(bb, Integer.parseInt(value));
            }
            else if (content.length == 4) {
                IsoTypeWriter.writeUInt32(bb, Long.parseLong(value));
            }
            else {
                if (content.length != 8) {
                    throw new Error("The content length within the appleDataBox is neither 1, 2, 4 or 8. I can't handle that!");
                }
                IsoTypeWriter.writeUInt64(bb, Long.parseLong(value));
            }
            this.appleDataBox.setData(bb.array());
        }
        else if (this.appleDataBox.getFlags() == 0) {
            (this.appleDataBox = new AppleDataBox()).setVersion(0);
            this.appleDataBox.setFlags(0);
            this.appleDataBox.setFourBytes(new byte[4]);
            this.appleDataBox.setData(hexStringToByteArray(value));
        }
        else {
            AbstractAppleMetaDataBox.LOG.warning("Don't know how to handle appleDataBox with flag=" + this.appleDataBox.getFlags());
        }
    }
    
    public String getValue() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractAppleMetaDataBox.ajc$tjp_6, this, this));
        if (this.appleDataBox.getFlags() == 1) {
            return Utf8.convert(this.appleDataBox.getData());
        }
        if (this.appleDataBox.getFlags() == 21) {
            final byte[] content = this.appleDataBox.getData();
            long l = 0L;
            int current = 1;
            final int length = content.length;
            byte[] array;
            for (int length2 = (array = content).length, i = 0; i < length2; ++i) {
                final byte b = array[i];
                l += toLong(b) << 8 * (length - current++);
            }
            return new StringBuilder().append(l).toString();
        }
        if (this.appleDataBox.getFlags() == 0) {
            return String.format("%x", new BigInteger(this.appleDataBox.getData()));
        }
        return "unknown";
    }
    
    public static byte[] hexStringToByteArray(final String s) {
        final int len = s.length();
        final byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AbstractAppleMetaDataBox.java", AbstractAppleMetaDataBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox", "", "", "", "java.util.List"), 25);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBoxes", "com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox", "java.util.List", "boxes", "", "void"), 29);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox", "java.lang.Class", "clazz", "", "java.util.List"), 37);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox", "java.lang.Class:boolean", "clazz:recursive", "", "java.util.List"), 41);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox", "", "", "", "java.lang.String"), 81);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setValue", "com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox", "java.lang.String", "value", "", "void"), 91);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getValue", "com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox", "", "", "", "java.lang.String"), 130);
    }
}
