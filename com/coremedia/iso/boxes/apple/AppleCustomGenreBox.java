// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public final class AppleCustomGenreBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©gen";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public AppleCustomGenreBox() {
        super("©gen");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
    
    public void setGenre(final String genre) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleCustomGenreBox.ajc$tjp_0, this, this, genre));
        (this.appleDataBox = new AppleDataBox()).setVersion(0);
        this.appleDataBox.setFlags(1);
        this.appleDataBox.setFourBytes(new byte[4]);
        this.appleDataBox.setData(Utf8.convert(genre));
    }
    
    public String getGenre() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleCustomGenreBox.ajc$tjp_1, this, this));
        return Utf8.convert(this.appleDataBox.getData());
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleCustomGenreBox.java", AppleCustomGenreBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setGenre", "com.coremedia.iso.boxes.apple.AppleCustomGenreBox", "java.lang.String", "genre", "", "void"), 17);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getGenre", "com.coremedia.iso.boxes.apple.AppleCustomGenreBox", "", "", "", "java.lang.String"), 25);
    }
}
