// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleTvSeasonBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "tvsn";
    
    public AppleTvSeasonBox() {
        super("tvsn");
        this.appleDataBox = AppleDataBox.getUint32AppleDataBox();
    }
}
