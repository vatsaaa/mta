// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class AppleDataRateBox extends AbstractFullBox
{
    public static final String TYPE = "rmdr";
    private long dataRate;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public AppleDataRateBox() {
        super("rmdr");
    }
    
    @Override
    protected long getContentSize() {
        return 8L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.dataRate = IsoTypeReader.readUInt32(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.dataRate);
    }
    
    public long getDataRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleDataRateBox.ajc$tjp_0, this, this));
        return this.dataRate;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleDataRateBox.java", AppleDataRateBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDataRate", "com.coremedia.iso.boxes.apple.AppleDataRateBox", "", "", "", "long"), 51);
    }
}
