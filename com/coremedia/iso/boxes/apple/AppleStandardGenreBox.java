// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleStandardGenreBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "gnre";
    
    public AppleStandardGenreBox() {
        super("gnre");
        this.appleDataBox = AppleDataBox.getUint16AppleDataBox();
    }
}
