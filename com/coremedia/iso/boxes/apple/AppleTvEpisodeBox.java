// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public class AppleTvEpisodeBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "tves";
    
    public AppleTvEpisodeBox() {
        super("tves");
        this.appleDataBox = AppleDataBox.getUint32AppleDataBox();
    }
}
