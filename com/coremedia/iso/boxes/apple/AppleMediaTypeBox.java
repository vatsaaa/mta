// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.HashMap;
import org.aspectj.lang.JoinPoint;
import java.util.Map;

public class AppleMediaTypeBox extends AbstractAppleMetaDataBox
{
    private static Map<String, String> mediaTypes;
    public static final String TYPE = "stik";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    static {
        ajc$preClinit();
        (AppleMediaTypeBox.mediaTypes = new HashMap<String, String>()).put("0", "Movie (is now 9)");
        AppleMediaTypeBox.mediaTypes.put("1", "Normal (Music)");
        AppleMediaTypeBox.mediaTypes.put("2", "Audiobook");
        AppleMediaTypeBox.mediaTypes.put("6", "Music Video");
        AppleMediaTypeBox.mediaTypes.put("9", "Movie");
        AppleMediaTypeBox.mediaTypes.put("10", "TV Show");
        AppleMediaTypeBox.mediaTypes.put("11", "Booklet");
        AppleMediaTypeBox.mediaTypes.put("14", "Ringtone");
    }
    
    public AppleMediaTypeBox() {
        super("stik");
        this.appleDataBox = AppleDataBox.getUint8AppleDataBox();
    }
    
    public String getReadableValue() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleMediaTypeBox.ajc$tjp_0, this, this));
        if (AppleMediaTypeBox.mediaTypes.containsKey(this.getValue())) {
            return AppleMediaTypeBox.mediaTypes.get(this.getValue());
        }
        return "unknown media type " + this.getValue();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleMediaTypeBox.java", AppleMediaTypeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReadableValue", "com.coremedia.iso.boxes.apple.AppleMediaTypeBox", "", "", "", "java.lang.String"), 31);
    }
}
