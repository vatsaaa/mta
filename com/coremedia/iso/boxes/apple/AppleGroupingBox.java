// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleGroupingBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©grp";
    
    public AppleGroupingBox() {
        super("©grp");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
