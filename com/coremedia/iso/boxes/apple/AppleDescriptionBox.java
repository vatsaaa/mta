// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleDescriptionBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "desc";
    
    public AppleDescriptionBox() {
        super("desc");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
