// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public final class AppleMeanBox extends AbstractFullBox
{
    public static final String TYPE = "mean";
    private String meaning;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public AppleMeanBox() {
        super("mean");
    }
    
    @Override
    protected long getContentSize() {
        return 4 + Utf8.utf8StringLengthInBytes(this.meaning);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.meaning = IsoTypeReader.readString(content, content.remaining());
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(Utf8.convert(this.meaning));
    }
    
    public String getMeaning() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleMeanBox.ajc$tjp_0, this, this));
        return this.meaning;
    }
    
    public void setMeaning(final String meaning) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleMeanBox.ajc$tjp_1, this, this, meaning));
        this.meaning = meaning;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleMeanBox.java", AppleMeanBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMeaning", "com.coremedia.iso.boxes.apple.AppleMeanBox", "", "", "", "java.lang.String"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMeaning", "com.coremedia.iso.boxes.apple.AppleMeanBox", "java.lang.String", "meaning", "", "void"), 43);
    }
}
