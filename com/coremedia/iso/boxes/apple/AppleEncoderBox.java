// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleEncoderBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©too";
    
    public AppleEncoderBox() {
        super("©too");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
