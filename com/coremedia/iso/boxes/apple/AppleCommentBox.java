// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleCommentBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©cmt";
    
    public AppleCommentBox() {
        super("©cmt");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
