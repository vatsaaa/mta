// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import com.coremedia.iso.boxes.AbstractContainerBox;

public class AppleReferenceMovieDescriptorBox extends AbstractContainerBox
{
    public static final String TYPE = "rmda";
    
    public AppleReferenceMovieDescriptorBox() {
        super("rmda");
    }
}
