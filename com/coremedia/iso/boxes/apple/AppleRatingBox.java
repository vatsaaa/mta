// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleRatingBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "rtng";
    
    public AppleRatingBox() {
        super("rtng");
        this.appleDataBox = AppleDataBox.getUint8AppleDataBox();
    }
}
