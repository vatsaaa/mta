// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import java.util.logging.Logger;

public final class AppleCoverBox extends AbstractAppleMetaDataBox
{
    private static Logger LOG;
    public static final String TYPE = "covr";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    
    static {
        ajc$preClinit();
        AppleCoverBox.LOG = Logger.getLogger(AppleCoverBox.class.getName());
    }
    
    public AppleCoverBox() {
        super("covr");
    }
    
    public void setPng(final byte[] pngData) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleCoverBox.ajc$tjp_0, this, this, pngData));
        (this.appleDataBox = new AppleDataBox()).setVersion(0);
        this.appleDataBox.setFlags(14);
        this.appleDataBox.setFourBytes(new byte[4]);
        this.appleDataBox.setData(pngData);
    }
    
    public void setJpg(final byte[] jpgData) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleCoverBox.ajc$tjp_1, this, this, jpgData));
        (this.appleDataBox = new AppleDataBox()).setVersion(0);
        this.appleDataBox.setFlags(13);
        this.appleDataBox.setFourBytes(new byte[4]);
        this.appleDataBox.setData(jpgData);
    }
    
    @Override
    public void setValue(final String value) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleCoverBox.ajc$tjp_2, this, this, value));
        AppleCoverBox.LOG.warning("---");
    }
    
    @Override
    public String getValue() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleCoverBox.ajc$tjp_3, this, this));
        return "---";
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleCoverBox.java", AppleCoverBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setPng", "com.coremedia.iso.boxes.apple.AppleCoverBox", "[B", "pngData", "", "void"), 18);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setJpg", "com.coremedia.iso.boxes.apple.AppleCoverBox", "[B", "jpgData", "", "void"), 27);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setValue", "com.coremedia.iso.boxes.apple.AppleCoverBox", "java.lang.String", "value", "", "void"), 36);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getValue", "com.coremedia.iso.boxes.apple.AppleCoverBox", "", "", "", "java.lang.String"), 41);
    }
}
