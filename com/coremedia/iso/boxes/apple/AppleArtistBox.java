// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleArtistBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©ART";
    
    public AppleArtistBox() {
        super("©ART");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
