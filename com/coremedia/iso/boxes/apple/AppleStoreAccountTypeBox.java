// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class AppleStoreAccountTypeBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "akID";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public AppleStoreAccountTypeBox() {
        super("akID");
        this.appleDataBox = AppleDataBox.getUint8AppleDataBox();
    }
    
    public String getReadableValue() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleStoreAccountTypeBox.ajc$tjp_0, this, this));
        final byte value = this.appleDataBox.getData()[0];
        switch (value) {
            case 0: {
                return "iTunes Account";
            }
            case 1: {
                return "AOL Account";
            }
            default: {
                return "unknown Account";
            }
        }
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleStoreAccountTypeBox.java", AppleStoreAccountTypeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReadableValue", "com.coremedia.iso.boxes.apple.AppleStoreAccountTypeBox", "", "", "", "java.lang.String"), 15);
    }
}
