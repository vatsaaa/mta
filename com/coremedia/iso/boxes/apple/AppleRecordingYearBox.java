// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public class AppleRecordingYearBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©day";
    
    public AppleRecordingYearBox() {
        super("©day");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
