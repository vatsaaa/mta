// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleNetworkBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "tvnn";
    
    public AppleNetworkBox() {
        super("tvnn");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
