// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import org.aspectj.runtime.internal.Conversions;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public final class AppleTrackNumberBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "trkn";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    
    public AppleTrackNumberBox() {
        super("trkn");
    }
    
    public void setTrackNumber(final byte track, final byte of) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleTrackNumberBox.ajc$tjp_0, this, this, Conversions.byteObject(track), Conversions.byteObject(of)));
        (this.appleDataBox = new AppleDataBox()).setVersion(0);
        this.appleDataBox.setFlags(0);
        this.appleDataBox.setFourBytes(new byte[4]);
        this.appleDataBox.setData(new byte[] { 0, 0, 0, track, 0, of, 0, 0 });
    }
    
    public byte getTrackNumber() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleTrackNumberBox.ajc$tjp_1, this, this));
        return this.appleDataBox.getData()[3];
    }
    
    public byte getNumberOfTracks() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleTrackNumberBox.ajc$tjp_2, this, this));
        return this.appleDataBox.getData()[5];
    }
    
    public void setNumberOfTracks(final byte numberOfTracks) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleTrackNumberBox.ajc$tjp_3, this, this, Conversions.byteObject(numberOfTracks)));
        final byte[] content = this.appleDataBox.getData();
        content[5] = numberOfTracks;
        this.appleDataBox.setData(content);
    }
    
    public void setTrackNumber(final byte trackNumber) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleTrackNumberBox.ajc$tjp_4, this, this, Conversions.byteObject(trackNumber)));
        final byte[] content = this.appleDataBox.getData();
        content[3] = trackNumber;
        this.appleDataBox.setData(content);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleTrackNumberBox.java", AppleTrackNumberBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTrackNumber", "com.coremedia.iso.boxes.apple.AppleTrackNumberBox", "byte:byte", "track:of", "", "void"), 19);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackNumber", "com.coremedia.iso.boxes.apple.AppleTrackNumberBox", "", "", "", "byte"), 27);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getNumberOfTracks", "com.coremedia.iso.boxes.apple.AppleTrackNumberBox", "", "", "", "byte"), 31);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setNumberOfTracks", "com.coremedia.iso.boxes.apple.AppleTrackNumberBox", "byte", "numberOfTracks", "", "void"), 35);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTrackNumber", "com.coremedia.iso.boxes.apple.AppleTrackNumberBox", "byte", "trackNumber", "", "void"), 41);
    }
}
