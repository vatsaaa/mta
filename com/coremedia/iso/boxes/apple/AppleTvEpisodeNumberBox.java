// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public class AppleTvEpisodeNumberBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "tven";
    
    public AppleTvEpisodeNumberBox() {
        super("tven");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
