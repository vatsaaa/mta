// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public class AppleAlbumArtistBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "aART";
    
    public AppleAlbumArtistBox() {
        super("aART");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
