// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleIdBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "apID";
    
    public AppleIdBox() {
        super("apID");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
