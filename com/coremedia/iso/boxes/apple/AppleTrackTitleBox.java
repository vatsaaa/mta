// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleTrackTitleBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©nam";
    
    public AppleTrackTitleBox() {
        super("©nam");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
