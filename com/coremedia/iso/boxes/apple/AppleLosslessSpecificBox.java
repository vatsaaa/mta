// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public final class AppleLosslessSpecificBox extends AbstractFullBox
{
    public static final String TYPE = "alac";
    private long maxSamplePerFrame;
    private int unknown1;
    private int sampleSize;
    private int historyMult;
    private int initialHistory;
    private int kModifier;
    private int channels;
    private int unknown2;
    private long maxCodedFrameSize;
    private long bitRate;
    private long sampleRate;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_18;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_19;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_20;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_21;
    
    public long getMaxSamplePerFrame() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_0, this, this));
        return this.maxSamplePerFrame;
    }
    
    public void setMaxSamplePerFrame(final int maxSamplePerFrame) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_1, this, this, Conversions.intObject(maxSamplePerFrame)));
        this.maxSamplePerFrame = maxSamplePerFrame;
    }
    
    public int getUnknown1() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_2, this, this));
        return this.unknown1;
    }
    
    public void setUnknown1(final int unknown1) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_3, this, this, Conversions.intObject(unknown1)));
        this.unknown1 = unknown1;
    }
    
    public int getSampleSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_4, this, this));
        return this.sampleSize;
    }
    
    public void setSampleSize(final int sampleSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_5, this, this, Conversions.intObject(sampleSize)));
        this.sampleSize = sampleSize;
    }
    
    public int getHistoryMult() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_6, this, this));
        return this.historyMult;
    }
    
    public void setHistoryMult(final int historyMult) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_7, this, this, Conversions.intObject(historyMult)));
        this.historyMult = historyMult;
    }
    
    public int getInitialHistory() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_8, this, this));
        return this.initialHistory;
    }
    
    public void setInitialHistory(final int initialHistory) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_9, this, this, Conversions.intObject(initialHistory)));
        this.initialHistory = initialHistory;
    }
    
    public int getKModifier() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_10, this, this));
        return this.kModifier;
    }
    
    public void setKModifier(final int kModifier) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_11, this, this, Conversions.intObject(kModifier)));
        this.kModifier = kModifier;
    }
    
    public int getChannels() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_12, this, this));
        return this.channels;
    }
    
    public void setChannels(final int channels) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_13, this, this, Conversions.intObject(channels)));
        this.channels = channels;
    }
    
    public int getUnknown2() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_14, this, this));
        return this.unknown2;
    }
    
    public void setUnknown2(final int unknown2) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_15, this, this, Conversions.intObject(unknown2)));
        this.unknown2 = unknown2;
    }
    
    public long getMaxCodedFrameSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_16, this, this));
        return this.maxCodedFrameSize;
    }
    
    public void setMaxCodedFrameSize(final int maxCodedFrameSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_17, this, this, Conversions.intObject(maxCodedFrameSize)));
        this.maxCodedFrameSize = maxCodedFrameSize;
    }
    
    public long getBitRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_18, this, this));
        return this.bitRate;
    }
    
    public void setBitRate(final int bitRate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_19, this, this, Conversions.intObject(bitRate)));
        this.bitRate = bitRate;
    }
    
    public long getSampleRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_20, this, this));
        return this.sampleRate;
    }
    
    public void setSampleRate(final int sampleRate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleLosslessSpecificBox.ajc$tjp_21, this, this, Conversions.intObject(sampleRate)));
        this.sampleRate = sampleRate;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.maxSamplePerFrame = IsoTypeReader.readUInt32(content);
        this.unknown1 = IsoTypeReader.readUInt8(content);
        this.sampleSize = IsoTypeReader.readUInt8(content);
        this.historyMult = IsoTypeReader.readUInt8(content);
        this.initialHistory = IsoTypeReader.readUInt8(content);
        this.kModifier = IsoTypeReader.readUInt8(content);
        this.channels = IsoTypeReader.readUInt8(content);
        this.unknown2 = IsoTypeReader.readUInt16(content);
        this.maxCodedFrameSize = IsoTypeReader.readUInt32(content);
        this.bitRate = IsoTypeReader.readUInt32(content);
        this.sampleRate = IsoTypeReader.readUInt32(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.maxSamplePerFrame);
        IsoTypeWriter.writeUInt8(bb, this.unknown1);
        IsoTypeWriter.writeUInt8(bb, this.sampleSize);
        IsoTypeWriter.writeUInt8(bb, this.historyMult);
        IsoTypeWriter.writeUInt8(bb, this.initialHistory);
        IsoTypeWriter.writeUInt8(bb, this.kModifier);
        IsoTypeWriter.writeUInt8(bb, this.channels);
        IsoTypeWriter.writeUInt16(bb, this.unknown2);
        IsoTypeWriter.writeUInt32(bb, this.maxCodedFrameSize);
        IsoTypeWriter.writeUInt32(bb, this.bitRate);
        IsoTypeWriter.writeUInt32(bb, this.sampleRate);
    }
    
    public AppleLosslessSpecificBox() {
        super("alac");
    }
    
    @Override
    protected long getContentSize() {
        return 28L;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleLosslessSpecificBox.java", AppleLosslessSpecificBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMaxSamplePerFrame", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 35);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMaxSamplePerFrame", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "maxSamplePerFrame", "", "void"), 39);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getKModifier", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 75);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setKModifier", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "kModifier", "", "void"), 79);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getChannels", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 83);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setChannels", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "channels", "", "void"), 87);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getUnknown2", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 91);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setUnknown2", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "unknown2", "", "void"), 95);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMaxCodedFrameSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 99);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMaxCodedFrameSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "maxCodedFrameSize", "", "void"), 103);
        ajc$tjp_18 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBitRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 107);
        ajc$tjp_19 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBitRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "bitRate", "", "void"), 111);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getUnknown1", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 43);
        ajc$tjp_20 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 115);
        ajc$tjp_21 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "sampleRate", "", "void"), 119);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setUnknown1", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "unknown1", "", "void"), 47);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 51);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "sampleSize", "", "void"), 55);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHistoryMult", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 59);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setHistoryMult", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "historyMult", "", "void"), 63);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getInitialHistory", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 67);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setInitialHistory", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "initialHistory", "", "void"), 71);
    }
}
