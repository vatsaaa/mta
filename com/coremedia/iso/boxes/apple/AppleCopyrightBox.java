// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleCopyrightBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "cprt";
    
    public AppleCopyrightBox() {
        super("cprt");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
