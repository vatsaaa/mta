// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleSynopsisBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "ldes";
    
    public AppleSynopsisBox() {
        super("ldes");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
