// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import com.coremedia.iso.boxes.AbstractContainerBox;

public class AppleReferenceMovieBox extends AbstractContainerBox
{
    public static final String TYPE = "rmra";
    
    public AppleReferenceMovieBox() {
        super("rmra");
    }
}
