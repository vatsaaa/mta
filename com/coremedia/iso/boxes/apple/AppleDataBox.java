// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public final class AppleDataBox extends AbstractFullBox
{
    public static final String TYPE = "data";
    private byte[] fourBytes;
    private byte[] data;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    
    private static AppleDataBox getEmpty() {
        final AppleDataBox appleDataBox = new AppleDataBox();
        appleDataBox.setVersion(0);
        appleDataBox.setFourBytes(new byte[4]);
        return appleDataBox;
    }
    
    public static AppleDataBox getStringAppleDataBox() {
        final AppleDataBox appleDataBox = getEmpty();
        appleDataBox.setFlags(1);
        appleDataBox.setData(new byte[1]);
        return appleDataBox;
    }
    
    public static AppleDataBox getUint8AppleDataBox() {
        final AppleDataBox appleDataBox = new AppleDataBox();
        appleDataBox.setFlags(21);
        appleDataBox.setData(new byte[1]);
        return appleDataBox;
    }
    
    public static AppleDataBox getUint16AppleDataBox() {
        final AppleDataBox appleDataBox = new AppleDataBox();
        appleDataBox.setFlags(21);
        appleDataBox.setData(new byte[2]);
        return appleDataBox;
    }
    
    public static AppleDataBox getUint32AppleDataBox() {
        final AppleDataBox appleDataBox = new AppleDataBox();
        appleDataBox.setFlags(21);
        appleDataBox.setData(new byte[4]);
        return appleDataBox;
    }
    
    public AppleDataBox() {
        super("data");
        this.fourBytes = new byte[4];
    }
    
    @Override
    protected long getContentSize() {
        return this.data.length + 8;
    }
    
    public void setData(final byte[] data) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleDataBox.ajc$tjp_0, this, this, data));
        System.arraycopy(data, 0, this.data = new byte[data.length], 0, data.length);
    }
    
    public void setFourBytes(final byte[] fourBytes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleDataBox.ajc$tjp_1, this, this, fourBytes));
        System.arraycopy(fourBytes, 0, this.fourBytes, 0, 4);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        content.get(this.fourBytes = new byte[4]);
        content.get(this.data = new byte[content.remaining()]);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(this.fourBytes, 0, 4);
        bb.put(this.data);
    }
    
    public byte[] getFourBytes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleDataBox.ajc$tjp_2, this, this));
        return this.fourBytes;
    }
    
    public byte[] getData() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleDataBox.ajc$tjp_3, this, this));
        return this.data;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleDataBox.java", AppleDataBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setData", "com.coremedia.iso.boxes.apple.AppleDataBox", "[B", "data", "", "void"), 60);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setFourBytes", "com.coremedia.iso.boxes.apple.AppleDataBox", "[B", "fourBytes", "", "void"), 65);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFourBytes", "com.coremedia.iso.boxes.apple.AppleDataBox", "", "", "", "[B"), 86);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getData", "com.coremedia.iso.boxes.apple.AppleDataBox", "", "", "", "[B"), 90);
    }
}
