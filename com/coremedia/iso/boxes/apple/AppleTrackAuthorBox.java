// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleTrackAuthorBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "©wrt";
    
    public AppleTrackAuthorBox() {
        super("©wrt");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
