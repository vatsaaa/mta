// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleShowBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "tvsh";
    
    public AppleShowBox() {
        super("tvsh");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
