// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.HashMap;
import org.aspectj.lang.JoinPoint;
import java.util.Map;

public class AppleStoreCountryCodeBox extends AbstractAppleMetaDataBox
{
    private static Map<String, String> countryCodes;
    public static final String TYPE = "sfID";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    static {
        ajc$preClinit();
        (AppleStoreCountryCodeBox.countryCodes = new HashMap<String, String>()).put("143460", "Australia");
        AppleStoreCountryCodeBox.countryCodes.put("143445", "Austria");
        AppleStoreCountryCodeBox.countryCodes.put("143446", "Belgium");
        AppleStoreCountryCodeBox.countryCodes.put("143455", "Canada");
        AppleStoreCountryCodeBox.countryCodes.put("143458", "Denmark");
        AppleStoreCountryCodeBox.countryCodes.put("143447", "Finland");
        AppleStoreCountryCodeBox.countryCodes.put("143442", "France");
        AppleStoreCountryCodeBox.countryCodes.put("143443", "Germany");
        AppleStoreCountryCodeBox.countryCodes.put("143448", "Greece");
        AppleStoreCountryCodeBox.countryCodes.put("143449", "Ireland");
        AppleStoreCountryCodeBox.countryCodes.put("143450", "Italy");
        AppleStoreCountryCodeBox.countryCodes.put("143462", "Japan");
        AppleStoreCountryCodeBox.countryCodes.put("143451", "Luxembourg");
        AppleStoreCountryCodeBox.countryCodes.put("143452", "Netherlands");
        AppleStoreCountryCodeBox.countryCodes.put("143461", "New Zealand");
        AppleStoreCountryCodeBox.countryCodes.put("143457", "Norway");
        AppleStoreCountryCodeBox.countryCodes.put("143453", "Portugal");
        AppleStoreCountryCodeBox.countryCodes.put("143454", "Spain");
        AppleStoreCountryCodeBox.countryCodes.put("143456", "Sweden");
        AppleStoreCountryCodeBox.countryCodes.put("143459", "Switzerland");
        AppleStoreCountryCodeBox.countryCodes.put("143444", "United Kingdom");
        AppleStoreCountryCodeBox.countryCodes.put("143441", "United States");
    }
    
    public AppleStoreCountryCodeBox() {
        super("sfID");
        this.appleDataBox = AppleDataBox.getUint32AppleDataBox();
    }
    
    public String getReadableValue() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleStoreCountryCodeBox.ajc$tjp_0, this, this));
        if (AppleStoreCountryCodeBox.countryCodes.containsKey(this.getValue())) {
            return AppleStoreCountryCodeBox.countryCodes.get(this.getValue());
        }
        return "unknown country code " + this.getValue();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleStoreCountryCodeBox.java", AppleStoreCountryCodeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReadableValue", "com.coremedia.iso.boxes.apple.AppleStoreCountryCodeBox", "", "", "", "java.lang.String"), 46);
    }
}
