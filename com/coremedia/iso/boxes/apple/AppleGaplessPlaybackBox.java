// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class AppleGaplessPlaybackBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "pgap";
    
    public AppleGaplessPlaybackBox() {
        super("pgap");
        this.appleDataBox = AppleDataBox.getUint8AppleDataBox();
    }
}
