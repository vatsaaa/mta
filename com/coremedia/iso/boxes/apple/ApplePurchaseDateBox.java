// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

public final class ApplePurchaseDateBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "purd";
    
    public ApplePurchaseDateBox() {
        super("purd");
        this.appleDataBox = AppleDataBox.getStringAppleDataBox();
    }
}
