// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.apple;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public final class AppleTempBox extends AbstractAppleMetaDataBox
{
    public static final String TYPE = "tmpo";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public AppleTempBox() {
        super("tmpo");
        this.appleDataBox = AppleDataBox.getUint16AppleDataBox();
    }
    
    public int getTempo() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleTempBox.ajc$tjp_0, this, this));
        return this.appleDataBox.getData()[1];
    }
    
    public void setTempo(final int tempo) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AppleTempBox.ajc$tjp_1, this, this, Conversions.intObject(tempo)));
        (this.appleDataBox = new AppleDataBox()).setVersion(0);
        this.appleDataBox.setFlags(21);
        this.appleDataBox.setFourBytes(new byte[4]);
        this.appleDataBox.setData(new byte[] { 0, (byte)(tempo & 0xFF) });
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AppleTempBox.java", AppleTempBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTempo", "com.coremedia.iso.boxes.apple.AppleTempBox", "", "", "", "int"), 16);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTempo", "com.coremedia.iso.boxes.apple.AppleTempBox", "int", "tempo", "", "void"), 20);
    }
}
