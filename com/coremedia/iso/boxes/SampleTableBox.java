// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.util.Iterator;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class SampleTableBox extends AbstractContainerBox
{
    public static final String TYPE = "stbl";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    
    public SampleTableBox() {
        super("stbl");
    }
    
    public SampleDescriptionBox getSampleDescriptionBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_0, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof SampleDescriptionBox) {
                return (SampleDescriptionBox)box;
            }
        }
        return null;
    }
    
    public SampleSizeBox getSampleSizeBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_1, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof SampleSizeBox) {
                return (SampleSizeBox)box;
            }
        }
        return null;
    }
    
    public SampleToChunkBox getSampleToChunkBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_2, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof SampleToChunkBox) {
                return (SampleToChunkBox)box;
            }
        }
        return null;
    }
    
    public ChunkOffsetBox getChunkOffsetBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_3, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof ChunkOffsetBox) {
                return (ChunkOffsetBox)box;
            }
        }
        return null;
    }
    
    public void setChunkOffsetBox(final ChunkOffsetBox b) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_4, this, this, b));
        for (int i = 0; i < this.boxes.size(); ++i) {
            final Box box = this.boxes.get(i);
            if (box instanceof ChunkOffsetBox) {
                this.boxes.set(i, b);
            }
        }
    }
    
    public TimeToSampleBox getTimeToSampleBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_5, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof TimeToSampleBox) {
                return (TimeToSampleBox)box;
            }
        }
        return null;
    }
    
    public SyncSampleBox getSyncSampleBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_6, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof SyncSampleBox) {
                return (SyncSampleBox)box;
            }
        }
        return null;
    }
    
    public CompositionTimeToSample getCompositionTimeToSample() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_7, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof CompositionTimeToSample) {
                return (CompositionTimeToSample)box;
            }
        }
        return null;
    }
    
    public SampleDependencyTypeBox getSampleDependencyTypeBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleTableBox.ajc$tjp_8, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof SampleDependencyTypeBox) {
                return (SampleDependencyTypeBox)box;
            }
        }
        return null;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleTableBox.java", SampleTableBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleDescriptionBox", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.SampleDescriptionBox"), 41);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleSizeBox", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.SampleSizeBox"), 50);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleToChunkBox", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.SampleToChunkBox"), 59);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getChunkOffsetBox", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.ChunkOffsetBox"), 68);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setChunkOffsetBox", "com.coremedia.iso.boxes.SampleTableBox", "com.coremedia.iso.boxes.ChunkOffsetBox", "b", "", "void"), 77);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTimeToSampleBox", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.TimeToSampleBox"), 86);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSyncSampleBox", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.SyncSampleBox"), 95);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCompositionTimeToSample", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.CompositionTimeToSample"), 104);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleDependencyTypeBox", "com.coremedia.iso.boxes.SampleTableBox", "", "", "", "com.coremedia.iso.boxes.SampleDependencyTypeBox"), 113);
    }
}
