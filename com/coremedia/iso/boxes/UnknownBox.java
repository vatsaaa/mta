// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import java.io.IOException;
import java.nio.ByteBuffer;

public class UnknownBox extends AbstractBox
{
    ByteBuffer data;
    
    public UnknownBox(final String type) {
        super(type);
    }
    
    @Override
    protected long getContentSize() {
        return this.data.limit();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        (this.data = content).position(content.position() + content.remaining());
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.data.rewind();
        bb.put(this.data);
    }
}
