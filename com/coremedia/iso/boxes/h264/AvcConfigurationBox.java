// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.h264;

import org.aspectj.lang.Signature;
import java.io.InputStream;
import com.googlecode.mp4parser.h264.model.SeqParameterSet;
import java.io.ByteArrayInputStream;
import com.googlecode.mp4parser.h264.model.PictureParameterSet;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import java.util.Collections;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.ArrayList;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractBox;

public final class AvcConfigurationBox extends AbstractBox
{
    public static final String TYPE = "avcC";
    private int configurationVersion;
    private int avcProfileIndicaation;
    private int profileCompatibility;
    private int avcLevelIndication;
    private int lengthSizeMinusOne;
    List<byte[]> sequenceParameterSets;
    List<byte[]> pictureParameterSets;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    
    public AvcConfigurationBox() {
        super("avcC");
        this.sequenceParameterSets = new ArrayList<byte[]>();
        this.pictureParameterSets = new ArrayList<byte[]>();
    }
    
    public int getConfigurationVersion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_0, this, this));
        return this.configurationVersion;
    }
    
    public int getAvcProfileIndicaation() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_1, this, this));
        return this.avcProfileIndicaation;
    }
    
    public int getProfileCompatibility() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_2, this, this));
        return this.profileCompatibility;
    }
    
    public int getAvcLevelIndication() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_3, this, this));
        return this.avcLevelIndication;
    }
    
    public int getLengthSizeMinusOne() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_4, this, this));
        return this.lengthSizeMinusOne;
    }
    
    public List<byte[]> getSequenceParameterSets() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_5, this, this));
        return Collections.unmodifiableList((List<? extends byte[]>)this.sequenceParameterSets);
    }
    
    public List<byte[]> getPictureParameterSets() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_6, this, this));
        return Collections.unmodifiableList((List<? extends byte[]>)this.pictureParameterSets);
    }
    
    public void setConfigurationVersion(final int configurationVersion) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_7, this, this, Conversions.intObject(configurationVersion)));
        this.configurationVersion = configurationVersion;
    }
    
    public void setAvcProfileIndicaation(final int avcProfileIndicaation) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_8, this, this, Conversions.intObject(avcProfileIndicaation)));
        this.avcProfileIndicaation = avcProfileIndicaation;
    }
    
    public void setProfileCompatibility(final int profileCompatibility) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_9, this, this, Conversions.intObject(profileCompatibility)));
        this.profileCompatibility = profileCompatibility;
    }
    
    public void setAvcLevelIndication(final int avcLevelIndication) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_10, this, this, Conversions.intObject(avcLevelIndication)));
        this.avcLevelIndication = avcLevelIndication;
    }
    
    public void setLengthSizeMinusOne(final int lengthSizeMinusOne) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_11, this, this, Conversions.intObject(lengthSizeMinusOne)));
        this.lengthSizeMinusOne = lengthSizeMinusOne;
    }
    
    public void setSequenceParameterSets(final List<byte[]> sequenceParameterSets) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_12, this, this, sequenceParameterSets));
        this.sequenceParameterSets = sequenceParameterSets;
    }
    
    public void setPictureParameterSets(final List<byte[]> pictureParameterSets) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_13, this, this, pictureParameterSets));
        this.pictureParameterSets = pictureParameterSets;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.configurationVersion = IsoTypeReader.readUInt8(content);
        this.avcProfileIndicaation = IsoTypeReader.readUInt8(content);
        this.profileCompatibility = IsoTypeReader.readUInt8(content);
        this.avcLevelIndication = IsoTypeReader.readUInt8(content);
        final int temp = IsoTypeReader.readUInt8(content);
        this.lengthSizeMinusOne = (temp & 0x3);
        final long numberOfSeuqenceParameterSets = IsoTypeReader.readUInt8(content) & 0x1F;
        for (int i = 0; i < numberOfSeuqenceParameterSets; ++i) {
            final int sequenceParameterSetLength = IsoTypeReader.readUInt16(content);
            final byte[] sequenceParameterSetNALUnit = new byte[sequenceParameterSetLength];
            content.get(sequenceParameterSetNALUnit);
            this.sequenceParameterSets.add(sequenceParameterSetNALUnit);
        }
        final long numberOfPictureParameterSets = IsoTypeReader.readUInt8(content);
        for (int j = 0; j < numberOfPictureParameterSets; ++j) {
            final int pictureParameterSetLength = IsoTypeReader.readUInt16(content);
            final byte[] pictureParameterSetNALUnit = new byte[pictureParameterSetLength];
            content.get(pictureParameterSetNALUnit);
            this.pictureParameterSets.add(pictureParameterSetNALUnit);
        }
    }
    
    public long getContentSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_14, this, this));
        long size = 5L;
        ++size;
        for (final byte[] sequenceParameterSetNALUnit : this.sequenceParameterSets) {
            size += 2L;
            size += sequenceParameterSetNALUnit.length;
        }
        ++size;
        for (final byte[] pictureParameterSetNALUnit : this.pictureParameterSets) {
            size += 2L;
            size += pictureParameterSetNALUnit.length;
        }
        return size;
    }
    
    public void getContent(final ByteBuffer bb) throws IOException {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_15, this, this, bb));
        IsoTypeWriter.writeUInt8(bb, this.configurationVersion);
        IsoTypeWriter.writeUInt8(bb, this.avcProfileIndicaation);
        IsoTypeWriter.writeUInt8(bb, this.profileCompatibility);
        IsoTypeWriter.writeUInt8(bb, this.avcLevelIndication);
        IsoTypeWriter.writeUInt8(bb, this.lengthSizeMinusOne | 0xFC);
        IsoTypeWriter.writeUInt8(bb, (this.pictureParameterSets.size() & 0x1F) | 0xE0);
        for (final byte[] sequenceParameterSetNALUnit : this.sequenceParameterSets) {
            IsoTypeWriter.writeUInt16(bb, sequenceParameterSetNALUnit.length);
            bb.put(sequenceParameterSetNALUnit);
        }
        IsoTypeWriter.writeUInt8(bb, this.pictureParameterSets.size());
        for (final byte[] pictureParameterSetNALUnit : this.pictureParameterSets) {
            IsoTypeWriter.writeUInt16(bb, pictureParameterSetNALUnit.length);
            bb.put(pictureParameterSetNALUnit);
        }
    }
    
    public String[] getPPS() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_16, this, this));
        final ArrayList l = new ArrayList();
        for (final byte[] pictureParameterSet : this.pictureParameterSets) {
            String details = "not parsable";
            try {
                details = PictureParameterSet.read(pictureParameterSet).toString();
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            l.add(details);
        }
        return l.toArray(new String[l.size()]);
    }
    
    public String[] getSPS() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AvcConfigurationBox.ajc$tjp_17, this, this));
        final ArrayList l = new ArrayList();
        for (final byte[] sequenceParameterSet : this.sequenceParameterSets) {
            String detail = "not parsable";
            try {
                detail = SeqParameterSet.read(new ByteArrayInputStream(sequenceParameterSet)).toString();
            }
            catch (IOException ex) {}
            l.add(detail);
        }
        return l.toArray(new String[l.size()]);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AvcConfigurationBox.java", AvcConfigurationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getConfigurationVersion", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "int"), 51);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAvcProfileIndicaation", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "int"), 55);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAvcLevelIndication", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "int", "avcLevelIndication", "", "void"), 91);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLengthSizeMinusOne", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "int", "lengthSizeMinusOne", "", "void"), 95);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSequenceParameterSets", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "java.util.List", "sequenceParameterSets", "", "void"), 99);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setPictureParameterSets", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "java.util.List", "pictureParameterSets", "", "void"), 103);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getContentSize", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "long"), 135);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getContent", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "java.nio.ByteBuffer", "bb", "java.io.IOException", "void"), 153);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getPPS", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "[Ljava.lang.String;"), 173);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSPS", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "[Ljava.lang.String;"), 189);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getProfileCompatibility", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "int"), 59);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAvcLevelIndication", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "int"), 63);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLengthSizeMinusOne", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "int"), 67);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSequenceParameterSets", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "java.util.List"), 71);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getPictureParameterSets", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "", "", "", "java.util.List"), 75);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setConfigurationVersion", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "int", "configurationVersion", "", "void"), 79);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAvcProfileIndicaation", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "int", "avcProfileIndicaation", "", "void"), 83);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setProfileCompatibility", "com.coremedia.iso.boxes.h264.AvcConfigurationBox", "int", "profileCompatibility", "", "void"), 87);
    }
}
