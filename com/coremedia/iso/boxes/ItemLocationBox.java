// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import com.coremedia.iso.IsoTypeWriterVariable;
import com.coremedia.iso.IsoTypeReaderVariable;
import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class ItemLocationBox extends AbstractFullBox
{
    public int offsetSize;
    public int lengthSize;
    public int baseOffsetSize;
    public int indexSize;
    public List<Item> items;
    public static final String TYPE = "iloc";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    
    public ItemLocationBox() {
        super("iloc");
        this.offsetSize = 8;
        this.lengthSize = 8;
        this.baseOffsetSize = 8;
        this.indexSize = 0;
        this.items = new LinkedList<Item>();
    }
    
    @Override
    protected long getContentSize() {
        long size = 8L;
        for (final Item item : this.items) {
            size += item.getSize();
        }
        return size;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt8(bb, this.offsetSize << 4 | this.lengthSize);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt8(bb, this.baseOffsetSize << 4 | this.indexSize);
        }
        else {
            IsoTypeWriter.writeUInt8(bb, this.baseOffsetSize << 4);
        }
        IsoTypeWriter.writeUInt16(bb, this.items.size());
        for (final Item item : this.items) {
            item.getContent(bb);
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        int tmp = IsoTypeReader.readUInt8(content);
        this.offsetSize = tmp >>> 4;
        this.lengthSize = (tmp & 0xF);
        tmp = IsoTypeReader.readUInt8(content);
        this.baseOffsetSize = tmp >>> 4;
        if (this.getVersion() == 1) {
            this.indexSize = (tmp & 0xF);
        }
        for (int itemCount = IsoTypeReader.readUInt16(content), i = 0; i < itemCount; ++i) {
            this.items.add(new Item(content));
        }
    }
    
    public int getOffsetSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_0, this, this));
        return this.offsetSize;
    }
    
    public void setOffsetSize(final int offsetSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_1, this, this, Conversions.intObject(offsetSize)));
        this.offsetSize = offsetSize;
    }
    
    public int getLengthSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_2, this, this));
        return this.lengthSize;
    }
    
    public void setLengthSize(final int lengthSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_3, this, this, Conversions.intObject(lengthSize)));
        this.lengthSize = lengthSize;
    }
    
    public int getBaseOffsetSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_4, this, this));
        return this.baseOffsetSize;
    }
    
    public void setBaseOffsetSize(final int baseOffsetSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_5, this, this, Conversions.intObject(baseOffsetSize)));
        this.baseOffsetSize = baseOffsetSize;
    }
    
    public int getIndexSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_6, this, this));
        return this.indexSize;
    }
    
    public void setIndexSize(final int indexSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_7, this, this, Conversions.intObject(indexSize)));
        this.indexSize = indexSize;
    }
    
    public List<Item> getItems() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_8, this, this));
        return this.items;
    }
    
    public void setItems(final List<Item> items) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_9, this, this, items));
        this.items = items;
    }
    
    public Item createItem(final int itemId, final int constructionMethod, final int dataReferenceIndex, final long baseOffset, final List<Extent> extents) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_10, this, this, new Object[] { Conversions.intObject(itemId), Conversions.intObject(constructionMethod), Conversions.intObject(dataReferenceIndex), Conversions.longObject(baseOffset), extents }));
        return new Item(itemId, constructionMethod, dataReferenceIndex, baseOffset, extents);
    }
    
    Item createItem(final ByteBuffer bb) {
        return new Item(bb);
    }
    
    public Extent createExtent(final long extentOffset, final long extentLength, final long extentIndex) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemLocationBox.ajc$tjp_11, this, this, new Object[] { Conversions.longObject(extentOffset), Conversions.longObject(extentLength), Conversions.longObject(extentIndex) }));
        return new Extent(extentOffset, extentLength, extentIndex);
    }
    
    Extent createExtent(final ByteBuffer bb) {
        return new Extent(bb);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("ItemLocationBox.java", ItemLocationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 117);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "offsetSize", "", "void"), 121);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "createItem", "com.coremedia.iso.boxes.ItemLocationBox", "int:int:int:long:java.util.List", "itemId:constructionMethod:dataReferenceIndex:baseOffset:extents", "", "com.coremedia.iso.boxes.ItemLocationBox$Item"), 158);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "createExtent", "com.coremedia.iso.boxes.ItemLocationBox", "long:long:long", "extentOffset:extentLength:extentIndex", "", "com.coremedia.iso.boxes.ItemLocationBox$Extent"), 283);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLengthSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 125);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLengthSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "lengthSize", "", "void"), 129);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBaseOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 133);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBaseOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "baseOffsetSize", "", "void"), 137);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getIndexSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 141);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setIndexSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "indexSize", "", "void"), 145);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getItems", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "java.util.List"), 149);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setItems", "com.coremedia.iso.boxes.ItemLocationBox", "java.util.List", "items", "", "void"), 153);
    }
    
    public class Item
    {
        public int itemId;
        public int constructionMethod;
        public int dataReferenceIndex;
        public long baseOffset;
        public List<Extent> extents;
        
        public Item(final ByteBuffer in) {
            this.extents = new LinkedList<Extent>();
            this.itemId = IsoTypeReader.readUInt16(in);
            if (ItemLocationBox.this.getVersion() == 1) {
                final int tmp = IsoTypeReader.readUInt16(in);
                this.constructionMethod = (tmp & 0xF);
            }
            this.dataReferenceIndex = IsoTypeReader.readUInt16(in);
            if (ItemLocationBox.this.baseOffsetSize > 0) {
                this.baseOffset = IsoTypeReaderVariable.read(in, ItemLocationBox.this.baseOffsetSize);
            }
            else {
                this.baseOffset = 0L;
            }
            for (int extentCount = IsoTypeReader.readUInt16(in), i = 0; i < extentCount; ++i) {
                this.extents.add(new Extent(in));
            }
        }
        
        public Item(final int itemId, final int constructionMethod, final int dataReferenceIndex, final long baseOffset, final List<Extent> extents) {
            this.extents = new LinkedList<Extent>();
            this.itemId = itemId;
            this.constructionMethod = constructionMethod;
            this.dataReferenceIndex = dataReferenceIndex;
            this.baseOffset = baseOffset;
            this.extents = extents;
        }
        
        public int getSize() {
            int size = 2;
            if (ItemLocationBox.this.getVersion() == 1) {
                size += 2;
            }
            size += 2;
            size += ItemLocationBox.this.baseOffsetSize;
            size += 2;
            for (final Extent extent : this.extents) {
                size += extent.getSize();
            }
            return size;
        }
        
        public void setBaseOffset(final long baseOffset) {
            this.baseOffset = baseOffset;
        }
        
        public void getContent(final ByteBuffer bb) throws IOException {
            IsoTypeWriter.writeUInt16(bb, this.itemId);
            if (ItemLocationBox.this.getVersion() == 1) {
                IsoTypeWriter.writeUInt16(bb, this.constructionMethod);
            }
            IsoTypeWriter.writeUInt16(bb, this.dataReferenceIndex);
            if (ItemLocationBox.this.baseOffsetSize > 0) {
                IsoTypeWriterVariable.write(this.baseOffset, bb, ItemLocationBox.this.baseOffsetSize);
            }
            IsoTypeWriter.writeUInt16(bb, this.extents.size());
            for (final Extent extent : this.extents) {
                extent.getContent(bb);
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            final Item item = (Item)o;
            if (this.baseOffset != item.baseOffset) {
                return false;
            }
            if (this.constructionMethod != item.constructionMethod) {
                return false;
            }
            if (this.dataReferenceIndex != item.dataReferenceIndex) {
                return false;
            }
            if (this.itemId != item.itemId) {
                return false;
            }
            if (this.extents != null) {
                if (this.extents.equals(item.extents)) {
                    return true;
                }
            }
            else if (item.extents == null) {
                return true;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            int result = this.itemId;
            result = 31 * result + this.constructionMethod;
            result = 31 * result + this.dataReferenceIndex;
            result = 31 * result + (int)(this.baseOffset ^ this.baseOffset >>> 32);
            result = 31 * result + ((this.extents != null) ? this.extents.hashCode() : 0);
            return result;
        }
        
        @Override
        public String toString() {
            return "Item{baseOffset=" + this.baseOffset + ", itemId=" + this.itemId + ", constructionMethod=" + this.constructionMethod + ", dataReferenceIndex=" + this.dataReferenceIndex + ", extents=" + this.extents + '}';
        }
    }
    
    public class Extent
    {
        public long extentOffset;
        public long extentLength;
        public long extentIndex;
        
        public Extent(final long extentOffset, final long extentLength, final long extentIndex) {
            this.extentOffset = extentOffset;
            this.extentLength = extentLength;
            this.extentIndex = extentIndex;
        }
        
        public Extent(final ByteBuffer in) {
            if (ItemLocationBox.this.getVersion() == 1 && ItemLocationBox.this.indexSize > 0) {
                this.extentIndex = IsoTypeReaderVariable.read(in, ItemLocationBox.this.indexSize);
            }
            this.extentOffset = IsoTypeReaderVariable.read(in, ItemLocationBox.this.offsetSize);
            this.extentLength = IsoTypeReaderVariable.read(in, ItemLocationBox.this.lengthSize);
        }
        
        public void getContent(final ByteBuffer os) throws IOException {
            if (ItemLocationBox.this.getVersion() == 1 && ItemLocationBox.this.indexSize > 0) {
                IsoTypeWriterVariable.write(this.extentIndex, os, ItemLocationBox.this.indexSize);
            }
            IsoTypeWriterVariable.write(this.extentOffset, os, ItemLocationBox.this.offsetSize);
            IsoTypeWriterVariable.write(this.extentLength, os, ItemLocationBox.this.lengthSize);
        }
        
        public int getSize() {
            return ((ItemLocationBox.this.indexSize > 0) ? ItemLocationBox.this.indexSize : 0) + ItemLocationBox.this.offsetSize + ItemLocationBox.this.lengthSize;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            final Extent extent = (Extent)o;
            return this.extentIndex == extent.extentIndex && this.extentLength == extent.extentLength && this.extentOffset == extent.extentOffset;
        }
        
        @Override
        public int hashCode() {
            int result = (int)(this.extentOffset ^ this.extentOffset >>> 32);
            result = 31 * result + (int)(this.extentLength ^ this.extentLength >>> 32);
            result = 31 * result + (int)(this.extentIndex ^ this.extentIndex >>> 32);
            return result;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Extent");
            sb.append("{extentOffset=").append(this.extentOffset);
            sb.append(", extentLength=").append(this.extentLength);
            sb.append(", extentIndex=").append(this.extentIndex);
            sb.append('}');
            return sb.toString();
        }
    }
}
