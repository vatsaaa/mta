// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class SoundMediaHeaderBox extends AbstractMediaHeaderBox
{
    public static final String TYPE = "smhd";
    private float balance;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public SoundMediaHeaderBox() {
        super("smhd");
    }
    
    public float getBalance() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SoundMediaHeaderBox.ajc$tjp_0, this, this));
        return this.balance;
    }
    
    @Override
    protected long getContentSize() {
        return 8L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.balance = IsoTypeReader.readFixedPoint88(content);
        IsoTypeReader.readUInt16(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeFixedPont88(bb, this.balance);
        IsoTypeWriter.writeUInt16(bb, 0);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SoundMediaHeaderBox.ajc$tjp_1, this, this));
        return "SoundMediaHeaderBox[balance=" + this.getBalance() + "]";
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SoundMediaHeaderBox.java", SoundMediaHeaderBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBalance", "com.coremedia.iso.boxes.SoundMediaHeaderBox", "", "", "", "float"), 34);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.SoundMediaHeaderBox", "", "", "", "java.lang.String"), 56);
    }
}
