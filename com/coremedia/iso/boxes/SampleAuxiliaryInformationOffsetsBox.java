// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class SampleAuxiliaryInformationOffsetsBox extends AbstractFullBox
{
    public static final String TYPE = "saio";
    private List<Long> offsets;
    private long auxInfoType;
    private long auxInfoTypeParameter;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    public SampleAuxiliaryInformationOffsetsBox() {
        super("saio");
        this.offsets = new LinkedList<Long>();
    }
    
    @Override
    protected long getContentSize() {
        return 8 + ((this.getVersion() == 0) ? (4 * this.offsets.size()) : (8 * this.offsets.size())) + (((this.getFlags() & 0x1) == 0x1) ? 8 : 0);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        if ((this.getFlags() & 0x1) == 0x1) {
            IsoTypeWriter.writeUInt32(bb, this.auxInfoType);
            IsoTypeWriter.writeUInt32(bb, this.auxInfoTypeParameter);
        }
        IsoTypeWriter.writeUInt32(bb, this.offsets.size());
        for (final Long offset : this.offsets) {
            if (this.getVersion() == 0) {
                IsoTypeWriter.writeUInt32(bb, offset);
            }
            else {
                IsoTypeWriter.writeUInt64(bb, offset);
            }
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if ((this.getFlags() & 0x1) == 0x1) {
            this.auxInfoType = IsoTypeReader.readUInt32(content);
            this.auxInfoTypeParameter = IsoTypeReader.readUInt32(content);
        }
        final int entryCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.offsets.clear();
        for (int i = 0; i < entryCount; ++i) {
            if (this.getVersion() == 0) {
                this.offsets.add(IsoTypeReader.readUInt32(content));
            }
            else {
                this.offsets.add(IsoTypeReader.readUInt64(content));
            }
        }
    }
    
    public long getAuxInfoType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationOffsetsBox.ajc$tjp_0, this, this));
        return this.auxInfoType;
    }
    
    public void setAuxInfoType(final long auxInfoType) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationOffsetsBox.ajc$tjp_1, this, this, Conversions.longObject(auxInfoType)));
        this.auxInfoType = auxInfoType;
    }
    
    public long getAuxInfoTypeParameter() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationOffsetsBox.ajc$tjp_2, this, this));
        return this.auxInfoTypeParameter;
    }
    
    public void setAuxInfoTypeParameter(final long auxInfoTypeParameter) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationOffsetsBox.ajc$tjp_3, this, this, Conversions.longObject(auxInfoTypeParameter)));
        this.auxInfoTypeParameter = auxInfoTypeParameter;
    }
    
    public List<Long> getOffsets() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationOffsetsBox.ajc$tjp_4, this, this));
        return this.offsets;
    }
    
    public void setOffsets(final List<Long> offsets) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationOffsetsBox.ajc$tjp_5, this, this, offsets));
        this.offsets = offsets;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleAuxiliaryInformationOffsetsBox.java", SampleAuxiliaryInformationOffsetsBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAuxInfoType", "com.coremedia.iso.boxes.SampleAuxiliaryInformationOffsetsBox", "", "", "", "long"), 104);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAuxInfoType", "com.coremedia.iso.boxes.SampleAuxiliaryInformationOffsetsBox", "long", "auxInfoType", "", "void"), 108);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAuxInfoTypeParameter", "com.coremedia.iso.boxes.SampleAuxiliaryInformationOffsetsBox", "", "", "", "long"), 112);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAuxInfoTypeParameter", "com.coremedia.iso.boxes.SampleAuxiliaryInformationOffsetsBox", "long", "auxInfoTypeParameter", "", "void"), 116);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getOffsets", "com.coremedia.iso.boxes.SampleAuxiliaryInformationOffsetsBox", "", "", "", "java.util.List"), 120);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setOffsets", "com.coremedia.iso.boxes.SampleAuxiliaryInformationOffsetsBox", "java.util.List", "offsets", "", "void"), 124);
    }
}
