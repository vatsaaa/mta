// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;

public final class BitRateBox extends AbstractBox
{
    public static final String TYPE = "btrt";
    private long bufferSizeDb;
    private long maxBitrate;
    private long avgBitrate;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    public BitRateBox() {
        super("btrt");
    }
    
    @Override
    protected long getContentSize() {
        return 12L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.bufferSizeDb = IsoTypeReader.readUInt32(content);
        this.maxBitrate = IsoTypeReader.readUInt32(content);
        this.avgBitrate = IsoTypeReader.readUInt32(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        IsoTypeWriter.writeUInt32(bb, this.bufferSizeDb);
        IsoTypeWriter.writeUInt32(bb, this.maxBitrate);
        IsoTypeWriter.writeUInt32(bb, this.avgBitrate);
    }
    
    public long getBufferSizeDb() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BitRateBox.ajc$tjp_0, this, this));
        return this.bufferSizeDb;
    }
    
    public void setBufferSizeDb(final long bufferSizeDb) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BitRateBox.ajc$tjp_1, this, this, Conversions.longObject(bufferSizeDb)));
        this.bufferSizeDb = bufferSizeDb;
    }
    
    public long getMaxBitrate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BitRateBox.ajc$tjp_2, this, this));
        return this.maxBitrate;
    }
    
    public void setMaxBitrate(final long maxBitrate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BitRateBox.ajc$tjp_3, this, this, Conversions.longObject(maxBitrate)));
        this.maxBitrate = maxBitrate;
    }
    
    public long getAvgBitrate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BitRateBox.ajc$tjp_4, this, this));
        return this.avgBitrate;
    }
    
    public void setAvgBitrate(final long avgBitrate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(BitRateBox.ajc$tjp_5, this, this, Conversions.longObject(avgBitrate)));
        this.avgBitrate = avgBitrate;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("BitRateBox.java", BitRateBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBufferSizeDb", "com.coremedia.iso.boxes.BitRateBox", "", "", "", "long"), 68);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBufferSizeDb", "com.coremedia.iso.boxes.BitRateBox", "long", "bufferSizeDb", "", "void"), 72);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMaxBitrate", "com.coremedia.iso.boxes.BitRateBox", "", "", "", "long"), 76);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMaxBitrate", "com.coremedia.iso.boxes.BitRateBox", "long", "maxBitrate", "", "void"), 80);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAvgBitrate", "com.coremedia.iso.boxes.BitRateBox", "", "", "", "long"), 84);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAvgBitrate", "com.coremedia.iso.boxes.BitRateBox", "long", "avgBitrate", "", "void"), 88);
    }
}
