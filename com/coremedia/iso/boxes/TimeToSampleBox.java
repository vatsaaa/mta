// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.util.ArrayList;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.Collections;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class TimeToSampleBox extends AbstractFullBox
{
    public static final String TYPE = "stts";
    List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    static {
        ajc$preClinit();
    }
    
    public TimeToSampleBox() {
        super("stts");
        this.entries = Collections.emptyList();
    }
    
    @Override
    protected long getContentSize() {
        return 8 + this.entries.size() * 8;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final int entryCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.entries = new ArrayList<Entry>(entryCount);
        for (int i = 0; i < entryCount; ++i) {
            this.entries.add(new Entry(IsoTypeReader.readUInt32(content), IsoTypeReader.readUInt32(content)));
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.entries.size());
        for (final Entry entry : this.entries) {
            IsoTypeWriter.writeUInt32(bb, entry.getCount());
            IsoTypeWriter.writeUInt32(bb, entry.getDelta());
        }
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TimeToSampleBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TimeToSampleBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TimeToSampleBox.ajc$tjp_2, this, this));
        return "TimeToSampleBox[entryCount=" + this.entries.size() + "]";
    }
    
    public static long[] blowupTimeToSamples(final List<Entry> entries) {
        long numOfSamples = 0L;
        for (final Entry entry : entries) {
            numOfSamples += entry.getCount();
        }
        assert numOfSamples <= 2147483647L;
        final long[] decodingTime = new long[(int)numOfSamples];
        int current = 0;
        for (final Entry entry2 : entries) {
            for (int i = 0; i < entry2.getCount(); ++i) {
                decodingTime[current++] = entry2.getDelta();
            }
        }
        return decodingTime;
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TimeToSampleBox.java", TimeToSampleBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.TimeToSampleBox", "", "", "", "java.util.List"), 80);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.TimeToSampleBox", "java.util.List", "entries", "", "void"), 84);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.TimeToSampleBox", "", "", "", "java.lang.String"), 88);
    }
    
    public static class Entry
    {
        long count;
        long delta;
        
        public Entry(final long count, final long delta) {
            this.count = count;
            this.delta = delta;
        }
        
        public long getCount() {
            return this.count;
        }
        
        public long getDelta() {
            return this.delta;
        }
        
        public void setCount(final long count) {
            this.count = count;
        }
        
        public void setDelta(final long delta) {
            this.delta = delta;
        }
        
        @Override
        public String toString() {
            return "Entry{count=" + this.count + ", delta=" + this.delta + '}';
        }
    }
}
