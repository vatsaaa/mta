// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class VideoMediaHeaderBox extends AbstractMediaHeaderBox
{
    private int graphicsmode;
    private int[] opcolor;
    public static final String TYPE = "vmhd";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    
    public VideoMediaHeaderBox() {
        super("vmhd");
        this.graphicsmode = 0;
        this.opcolor = new int[3];
        this.setFlags(1);
    }
    
    public int getGraphicsmode() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VideoMediaHeaderBox.ajc$tjp_0, this, this));
        return this.graphicsmode;
    }
    
    public int[] getOpcolor() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VideoMediaHeaderBox.ajc$tjp_1, this, this));
        return this.opcolor;
    }
    
    @Override
    protected long getContentSize() {
        return 12L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.graphicsmode = IsoTypeReader.readUInt16(content);
        this.opcolor = new int[3];
        for (int i = 0; i < 3; ++i) {
            this.opcolor[i] = IsoTypeReader.readUInt16(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt16(bb, this.graphicsmode);
        int[] opcolor;
        for (int length = (opcolor = this.opcolor).length, i = 0; i < length; ++i) {
            final int anOpcolor = opcolor[i];
            IsoTypeWriter.writeUInt16(bb, anOpcolor);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VideoMediaHeaderBox.ajc$tjp_2, this, this));
        return "VideoMediaHeaderBox[graphicsmode=" + this.getGraphicsmode() + ";opcolor0=" + this.getOpcolor()[0] + ";opcolor1=" + this.getOpcolor()[1] + ";opcolor2=" + this.getOpcolor()[2] + "]";
    }
    
    public void setOpcolor(final int[] opcolor) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VideoMediaHeaderBox.ajc$tjp_3, this, this, opcolor));
        this.opcolor = opcolor;
    }
    
    public void setGraphicsmode(final int graphicsmode) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VideoMediaHeaderBox.ajc$tjp_4, this, this, Conversions.intObject(graphicsmode)));
        this.graphicsmode = graphicsmode;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("VideoMediaHeaderBox.java", VideoMediaHeaderBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getGraphicsmode", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "", "", "", "int"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getOpcolor", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "", "", "", "[I"), 43);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "", "", "", "java.lang.String"), 71);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setOpcolor", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "[I", "opcolor", "", "void"), 75);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setGraphicsmode", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "int", "graphicsmode", "", "void"), 79);
    }
}
