// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import com.googlecode.mp4parser.DoNotParseDetail;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoFile;
import java.util.LinkedList;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class FileTypeBox extends AbstractBox
{
    public static final String TYPE = "ftyp";
    private String majorBrand;
    private long minorVersion;
    private List<String> compatibleBrands;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    public FileTypeBox() {
        super("ftyp");
    }
    
    public FileTypeBox(final String majorBrand, final long minorVersion, final List<String> compatibleBrands) {
        super("ftyp");
        this.majorBrand = majorBrand;
        this.minorVersion = minorVersion;
        this.compatibleBrands = compatibleBrands;
    }
    
    @Override
    protected long getContentSize() {
        return 8 + this.compatibleBrands.size() * 4;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.majorBrand = IsoTypeReader.read4cc(content);
        this.minorVersion = IsoTypeReader.readUInt32(content);
        final int compatibleBrandsCount = content.remaining() / 4;
        this.compatibleBrands = new LinkedList<String>();
        for (int i = 0; i < compatibleBrandsCount; ++i) {
            this.compatibleBrands.add(IsoTypeReader.read4cc(content));
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(IsoFile.fourCCtoBytes(this.majorBrand));
        IsoTypeWriter.writeUInt32(bb, this.minorVersion);
        for (final String compatibleBrand : this.compatibleBrands) {
            bb.put(IsoFile.fourCCtoBytes(compatibleBrand));
        }
    }
    
    public String getMajorBrand() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FileTypeBox.ajc$tjp_0, this, this));
        return this.majorBrand;
    }
    
    public void setMajorBrand(final String majorBrand) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FileTypeBox.ajc$tjp_1, this, this, majorBrand));
        this.majorBrand = majorBrand;
    }
    
    public void setMinorVersion(final int minorVersion) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FileTypeBox.ajc$tjp_2, this, this, Conversions.intObject(minorVersion)));
        this.minorVersion = minorVersion;
    }
    
    public long getMinorVersion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FileTypeBox.ajc$tjp_3, this, this));
        return this.minorVersion;
    }
    
    public List<String> getCompatibleBrands() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FileTypeBox.ajc$tjp_4, this, this));
        return this.compatibleBrands;
    }
    
    public void setCompatibleBrands(final List<String> compatibleBrands) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FileTypeBox.ajc$tjp_5, this, this, compatibleBrands));
        this.compatibleBrands = compatibleBrands;
    }
    
    @DoNotParseDetail
    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        result.append("FileTypeBox[");
        result.append("majorBrand=").append(this.getMajorBrand());
        result.append(";");
        result.append("minorVersion=").append(this.getMinorVersion());
        for (final String compatibleBrand : this.compatibleBrands) {
            result.append(";");
            result.append("compatibleBrand=").append(compatibleBrand);
        }
        result.append("]");
        return result.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("FileTypeBox.java", FileTypeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMajorBrand", "com.coremedia.iso.boxes.FileTypeBox", "", "", "", "java.lang.String"), 83);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMajorBrand", "com.coremedia.iso.boxes.FileTypeBox", "java.lang.String", "majorBrand", "", "void"), 92);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMinorVersion", "com.coremedia.iso.boxes.FileTypeBox", "int", "minorVersion", "", "void"), 101);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMinorVersion", "com.coremedia.iso.boxes.FileTypeBox", "", "", "", "long"), 111);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCompatibleBrands", "com.coremedia.iso.boxes.FileTypeBox", "", "", "", "java.util.List"), 120);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCompatibleBrands", "com.coremedia.iso.boxes.FileTypeBox", "java.util.List", "compatibleBrands", "", "void"), 124);
    }
}
