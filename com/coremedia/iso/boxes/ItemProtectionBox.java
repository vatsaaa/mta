// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class ItemProtectionBox extends FullContainerBox
{
    public static final String TYPE = "ipro";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public ItemProtectionBox() {
        super("ipro");
    }
    
    public SchemeInformationBox getItemProtectionScheme() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemProtectionBox.ajc$tjp_0, this, this));
        if (!this.getBoxes(SchemeInformationBox.class).isEmpty()) {
            return this.getBoxes(SchemeInformationBox.class).get(0);
        }
        return null;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        IsoTypeReader.readUInt16(content);
        this.parseChildBoxes(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt16(bb, this.getBoxes().size());
        this.writeChildBoxes(bb);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("ItemProtectionBox.java", ItemProtectionBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getItemProtectionScheme", "com.coremedia.iso.boxes.ItemProtectionBox", "", "", "", "com.coremedia.iso.boxes.SchemeInformationBox"), 38);
    }
}
