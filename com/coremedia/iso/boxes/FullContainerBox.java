// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.nio.channels.WritableByteChannel;
import com.googlecode.mp4parser.ByteBufferByteChannel;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.Iterator;
import java.util.ArrayList;
import org.aspectj.runtime.internal.Conversions;
import java.util.Collection;
import java.util.LinkedList;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.BoxParser;
import java.util.logging.Logger;
import java.util.List;

public abstract class FullContainerBox extends AbstractFullBox implements ContainerBox
{
    protected List<Box> boxes;
    private static Logger LOG;
    BoxParser boxParser;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    
    static {
        ajc$preClinit();
        FullContainerBox.LOG = Logger.getLogger(FullContainerBox.class.getName());
    }
    
    public void setBoxes(final List<Box> boxes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FullContainerBox.ajc$tjp_0, this, this, boxes));
        this.boxes = new LinkedList<Box>(boxes);
    }
    
    public <T extends Box> List<T> getBoxes(final Class<T> clazz) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FullContainerBox.ajc$tjp_1, this, this, clazz));
        return this.getBoxes(clazz, false);
    }
    
    public <T extends Box> List<T> getBoxes(final Class<T> clazz, final boolean recursive) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FullContainerBox.ajc$tjp_2, this, this, clazz, Conversions.booleanObject(recursive)));
        final List boxesToBeReturned = new ArrayList(2);
        for (final Box boxe : this.boxes) {
            if (clazz == boxe.getClass()) {
                boxesToBeReturned.add(boxe);
            }
            if (recursive && boxe instanceof ContainerBox) {
                boxesToBeReturned.addAll(((ContainerBox)boxe).getBoxes(clazz, recursive));
            }
        }
        return (List<T>)boxesToBeReturned;
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 4L;
        for (final Box boxe : this.boxes) {
            contentSize += boxe.getSize();
        }
        return contentSize;
    }
    
    public void addBox(final Box b) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FullContainerBox.ajc$tjp_3, this, this, b));
        this.boxes.add(b);
    }
    
    public void removeBox(final Box b) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FullContainerBox.ajc$tjp_4, this, this, b));
        this.boxes.remove(b);
    }
    
    public FullContainerBox(final String type) {
        super(type);
        this.boxes = new LinkedList<Box>();
    }
    
    public List<Box> getBoxes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FullContainerBox.ajc$tjp_5, this, this));
        return this.boxes;
    }
    
    @Override
    public void parse(final ReadableByteChannel in, final ByteBuffer header, final long contentSize, final BoxParser boxParser) throws IOException {
        super.parse(in, header, contentSize, boxParser);
        this.boxParser = boxParser;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.parseChildBoxes(content);
    }
    
    protected final void parseChildBoxes(final ByteBuffer content) {
        try {
            while (content.remaining() >= 8) {
                this.boxes.add(this.boxParser.parseBox(new ByteBufferByteChannel(content), this));
            }
            if (content.remaining() != 0) {
                this.deadBytes = content.slice();
                FullContainerBox.LOG.severe("Some sizes are wrong");
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FullContainerBox.ajc$tjp_6, this, this));
        final StringBuilder buffer = new StringBuilder();
        buffer.append(this.getClass().getSimpleName()).append("[");
        for (int i = 0; i < this.boxes.size(); ++i) {
            if (i > 0) {
                buffer.append(";");
            }
            buffer.append(this.boxes.get(i).toString());
        }
        buffer.append("]");
        return buffer.toString();
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        this.writeChildBoxes(bb);
    }
    
    protected final void writeChildBoxes(final ByteBuffer bb) throws IOException {
        final WritableByteChannel wbc = new ByteBufferByteChannel(bb);
        for (final Box box : this.boxes) {
            box.getBox(wbc);
        }
    }
    
    public long getNumOfBytesToFirstChild() {
        long sizeOfChildren = 0L;
        for (final Box box : this.boxes) {
            sizeOfChildren += box.getSize();
        }
        return this.getSize() - sizeOfChildren;
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("FullContainerBox.java", FullContainerBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBoxes", "com.coremedia.iso.boxes.FullContainerBox", "java.util.List", "boxes", "", "void"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.FullContainerBox", "java.lang.Class", "clazz", "", "java.util.List"), 44);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.FullContainerBox", "java.lang.Class:boolean", "clazz:recursive", "", "java.util.List"), 49);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "addBox", "com.coremedia.iso.boxes.FullContainerBox", "com.coremedia.iso.boxes.Box", "b", "", "void"), 73);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "removeBox", "com.coremedia.iso.boxes.FullContainerBox", "com.coremedia.iso.boxes.Box", "b", "", "void"), 77);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.FullContainerBox", "", "", "", "java.util.List"), 85);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.FullContainerBox", "", "", "", "java.lang.String"), 116);
    }
}
