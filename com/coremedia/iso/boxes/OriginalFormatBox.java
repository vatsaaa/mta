// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoFile;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class OriginalFormatBox extends AbstractBox
{
    public static final String TYPE = "frma";
    private String dataFormat;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    static {
        ajc$preClinit();
    }
    
    public OriginalFormatBox() {
        super("frma");
        this.dataFormat = "    ";
    }
    
    public String getDataFormat() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OriginalFormatBox.ajc$tjp_0, this, this));
        return this.dataFormat;
    }
    
    public void setDataFormat(final String dataFormat) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OriginalFormatBox.ajc$tjp_1, this, this, dataFormat));
        assert dataFormat.length() == 4;
        this.dataFormat = dataFormat;
    }
    
    @Override
    protected long getContentSize() {
        return 4L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.dataFormat = IsoTypeReader.read4cc(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(IsoFile.fourCCtoBytes(this.dataFormat));
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OriginalFormatBox.ajc$tjp_2, this, this));
        return "OriginalFormatBox[dataFormat=" + this.getDataFormat() + "]";
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("OriginalFormatBox.java", OriginalFormatBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDataFormat", "com.coremedia.iso.boxes.OriginalFormatBox", "", "", "", "java.lang.String"), 41);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDataFormat", "com.coremedia.iso.boxes.OriginalFormatBox", "java.lang.String", "dataFormat", "", "void"), 46);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.OriginalFormatBox", "", "", "", "java.lang.String"), 66);
    }
}
