// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class KeywordsBox extends AbstractFullBox
{
    public static final String TYPE = "kywd";
    private String language;
    private String[] keywords;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    
    public KeywordsBox() {
        super("kywd");
    }
    
    public String getLanguage() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(KeywordsBox.ajc$tjp_0, this, this));
        return this.language;
    }
    
    public String[] getKeywords() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(KeywordsBox.ajc$tjp_1, this, this));
        return this.keywords;
    }
    
    public void setLanguage(final String language) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(KeywordsBox.ajc$tjp_2, this, this, language));
        this.language = language;
    }
    
    public void setKeywords(final String[] keywords) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(KeywordsBox.ajc$tjp_3, this, this, (Object)keywords));
        this.keywords = keywords;
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 7L;
        String[] keywords;
        for (int length = (keywords = this.keywords).length, i = 0; i < length; ++i) {
            final String keyword = keywords[i];
            contentSize += 1 + Utf8.utf8StringLengthInBytes(keyword) + 1;
        }
        return contentSize;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.language = IsoTypeReader.readIso639(content);
        final int keywordCount = IsoTypeReader.readUInt8(content);
        this.keywords = new String[keywordCount];
        for (int i = 0; i < keywordCount; ++i) {
            IsoTypeReader.readUInt8(content);
            this.keywords[i] = IsoTypeReader.readString(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeIso639(bb, this.language);
        IsoTypeWriter.writeUInt8(bb, this.keywords.length);
        String[] keywords;
        for (int length = (keywords = this.keywords).length, i = 0; i < length; ++i) {
            final String keyword = keywords[i];
            IsoTypeWriter.writeUInt8(bb, Utf8.utf8StringLengthInBytes(keyword) + 1);
            bb.put(Utf8.convert(keyword));
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(KeywordsBox.ajc$tjp_4, this, this));
        final StringBuffer buffer = new StringBuffer();
        buffer.append("KeywordsBox[language=").append(this.getLanguage());
        for (int i = 0; i < this.keywords.length; ++i) {
            buffer.append(";keyword").append(i).append("=").append(this.keywords[i]);
        }
        buffer.append("]");
        return buffer.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("KeywordsBox.java", KeywordsBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLanguage", "com.coremedia.iso.boxes.KeywordsBox", "", "", "", "java.lang.String"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getKeywords", "com.coremedia.iso.boxes.KeywordsBox", "", "", "", "[Ljava.lang.String;"), 43);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLanguage", "com.coremedia.iso.boxes.KeywordsBox", "java.lang.String", "language", "", "void"), 47);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setKeywords", "com.coremedia.iso.boxes.KeywordsBox", "[Ljava.lang.String;", "keywords", "", "void"), 51);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.KeywordsBox", "", "", "", "java.lang.String"), 86);
    }
}
