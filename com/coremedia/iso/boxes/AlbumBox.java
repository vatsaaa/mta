// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class AlbumBox extends AbstractFullBox
{
    public static final String TYPE = "albm";
    private String language;
    private String albumTitle;
    private int trackNumber;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    
    public AlbumBox() {
        super("albm");
    }
    
    public String getLanguage() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AlbumBox.ajc$tjp_0, this, this));
        return this.language;
    }
    
    public String getAlbumTitle() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AlbumBox.ajc$tjp_1, this, this));
        return this.albumTitle;
    }
    
    public int getTrackNumber() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AlbumBox.ajc$tjp_2, this, this));
        return this.trackNumber;
    }
    
    public void setLanguage(final String language) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AlbumBox.ajc$tjp_3, this, this, language));
        this.language = language;
    }
    
    public void setAlbumTitle(final String albumTitle) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AlbumBox.ajc$tjp_4, this, this, albumTitle));
        this.albumTitle = albumTitle;
    }
    
    public void setTrackNumber(final int trackNumber) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AlbumBox.ajc$tjp_5, this, this, Conversions.intObject(trackNumber)));
        this.trackNumber = trackNumber;
    }
    
    @Override
    protected long getContentSize() {
        return 6 + Utf8.utf8StringLengthInBytes(this.albumTitle) + 1 + ((this.trackNumber != -1) ? 1 : 0);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.language = IsoTypeReader.readIso639(content);
        this.albumTitle = IsoTypeReader.readString(content);
        if (content.remaining() > 0) {
            this.trackNumber = IsoTypeReader.readUInt8(content);
        }
        else {
            this.trackNumber = -1;
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeIso639(bb, this.language);
        bb.put(Utf8.convert(this.albumTitle));
        bb.put((byte)0);
        if (this.trackNumber != -1) {
            IsoTypeWriter.writeUInt8(bb, this.trackNumber);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AlbumBox.ajc$tjp_6, this, this));
        final StringBuilder buffer = new StringBuilder();
        buffer.append("AlbumBox[language=").append(this.getLanguage()).append(";");
        buffer.append("albumTitle=").append(this.getAlbumTitle());
        if (this.trackNumber >= 0) {
            buffer.append(";trackNumber=").append(this.getTrackNumber());
        }
        buffer.append("]");
        return buffer.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AlbumBox.java", AlbumBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLanguage", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "java.lang.String"), 50);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAlbumTitle", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "java.lang.String"), 54);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackNumber", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "int"), 58);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLanguage", "com.coremedia.iso.boxes.AlbumBox", "java.lang.String", "language", "", "void"), 62);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAlbumTitle", "com.coremedia.iso.boxes.AlbumBox", "java.lang.String", "albumTitle", "", "void"), 66);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTrackNumber", "com.coremedia.iso.boxes.AlbumBox", "int", "trackNumber", "", "void"), 70);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "java.lang.String"), 102);
    }
}
