// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.util.Collection;
import java.util.LinkedList;
import org.aspectj.runtime.internal.Conversions;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.util.ArrayList;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.Collections;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class SampleToChunkBox extends AbstractFullBox
{
    List<Entry> entries;
    public static final String TYPE = "stsc";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    
    public SampleToChunkBox() {
        super("stsc");
        this.entries = Collections.emptyList();
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToChunkBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToChunkBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    @Override
    protected long getContentSize() {
        return this.entries.size() * 12 + 8;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final int entryCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.entries = new ArrayList<Entry>(entryCount);
        for (int i = 0; i < entryCount; ++i) {
            this.entries.add(new Entry(IsoTypeReader.readUInt32(content), IsoTypeReader.readUInt32(content), IsoTypeReader.readUInt32(content)));
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.entries.size());
        for (final Entry entry : this.entries) {
            IsoTypeWriter.writeUInt32(bb, entry.getFirstChunk());
            IsoTypeWriter.writeUInt32(bb, entry.getSamplesPerChunk());
            IsoTypeWriter.writeUInt32(bb, entry.getSampleDescriptionIndex());
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToChunkBox.ajc$tjp_2, this, this));
        return "SampleToChunkBox[entryCount=" + this.entries.size() + "]";
    }
    
    public long[] blowup(final int chunkCount) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToChunkBox.ajc$tjp_3, this, this, Conversions.intObject(chunkCount)));
        final long[] numberOfSamples = new long[chunkCount];
        final List sampleToChunkEntries = new LinkedList(this.entries);
        Collections.reverse(sampleToChunkEntries);
        final Iterator iterator = sampleToChunkEntries.iterator();
        Entry currentEntry = iterator.next();
        for (int i = numberOfSamples.length; i > 1; --i) {
            numberOfSamples[i - 1] = currentEntry.getSamplesPerChunk();
            if (i == currentEntry.getFirstChunk()) {
                currentEntry = iterator.next();
            }
        }
        numberOfSamples[0] = currentEntry.getSamplesPerChunk();
        return numberOfSamples;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleToChunkBox.java", SampleToChunkBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.SampleToChunkBox", "", "", "", "java.util.List"), 46);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.SampleToChunkBox", "java.util.List", "entries", "", "void"), 50);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.SampleToChunkBox", "", "", "", "java.lang.String"), 83);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "blowup", "com.coremedia.iso.boxes.SampleToChunkBox", "int", "chunkCount", "", "[J"), 94);
    }
    
    public static class Entry
    {
        long firstChunk;
        long samplesPerChunk;
        long sampleDescriptionIndex;
        
        public Entry(final long firstChunk, final long samplesPerChunk, final long sampleDescriptionIndex) {
            this.firstChunk = firstChunk;
            this.samplesPerChunk = samplesPerChunk;
            this.sampleDescriptionIndex = sampleDescriptionIndex;
        }
        
        public long getFirstChunk() {
            return this.firstChunk;
        }
        
        public void setFirstChunk(final long firstChunk) {
            this.firstChunk = firstChunk;
        }
        
        public long getSamplesPerChunk() {
            return this.samplesPerChunk;
        }
        
        public void setSamplesPerChunk(final long samplesPerChunk) {
            this.samplesPerChunk = samplesPerChunk;
        }
        
        public long getSampleDescriptionIndex() {
            return this.sampleDescriptionIndex;
        }
        
        public void setSampleDescriptionIndex(final long sampleDescriptionIndex) {
            this.sampleDescriptionIndex = sampleDescriptionIndex;
        }
        
        @Override
        public String toString() {
            return "Entry{firstChunk=" + this.firstChunk + ", samplesPerChunk=" + this.samplesPerChunk + ", sampleDescriptionIndex=" + this.sampleDescriptionIndex + '}';
        }
    }
}
