// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class MediaHeaderBox extends AbstractFullBox
{
    public static final String TYPE = "mdhd";
    private long creationTime;
    private long modificationTime;
    private long timescale;
    private long duration;
    private String language;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    
    public MediaHeaderBox() {
        super("mdhd");
    }
    
    public long getCreationTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_0, this, this));
        return this.creationTime;
    }
    
    public long getModificationTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_1, this, this));
        return this.modificationTime;
    }
    
    public long getTimescale() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_2, this, this));
        return this.timescale;
    }
    
    public long getDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_3, this, this));
        return this.duration;
    }
    
    public String getLanguage() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_4, this, this));
        return this.language;
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 4L;
        if (this.getVersion() == 1) {
            contentSize += 28L;
        }
        else {
            contentSize += 16L;
        }
        contentSize += 2L;
        contentSize += 2L;
        return contentSize;
    }
    
    public void setCreationTime(final long creationTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_5, this, this, Conversions.longObject(creationTime)));
        this.creationTime = creationTime;
    }
    
    public void setModificationTime(final long modificationTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_6, this, this, Conversions.longObject(modificationTime)));
        this.modificationTime = modificationTime;
    }
    
    public void setTimescale(final long timescale) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_7, this, this, Conversions.longObject(timescale)));
        this.timescale = timescale;
    }
    
    public void setDuration(final long duration) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_8, this, this, Conversions.longObject(duration)));
        this.duration = duration;
    }
    
    public void setLanguage(final String language) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_9, this, this, language));
        this.language = language;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if (this.getVersion() == 1) {
            this.creationTime = IsoTypeReader.readUInt64(content);
            this.modificationTime = IsoTypeReader.readUInt64(content);
            this.timescale = IsoTypeReader.readUInt32(content);
            this.duration = IsoTypeReader.readUInt64(content);
        }
        else {
            this.creationTime = IsoTypeReader.readUInt32(content);
            this.modificationTime = IsoTypeReader.readUInt32(content);
            this.timescale = IsoTypeReader.readUInt32(content);
            this.duration = IsoTypeReader.readUInt32(content);
        }
        this.language = IsoTypeReader.readIso639(content);
        IsoTypeReader.readUInt16(content);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaHeaderBox.ajc$tjp_10, this, this));
        final StringBuilder result = new StringBuilder();
        result.append("MeditHeaderBox[");
        result.append("creationTime=").append(this.getCreationTime());
        result.append(";");
        result.append("modificationTime=").append(this.getModificationTime());
        result.append(";");
        result.append("timescale=").append(this.getTimescale());
        result.append(";");
        result.append("duration=").append(this.getDuration());
        result.append(";");
        result.append("language=").append(this.getLanguage());
        result.append("]");
        return result.toString();
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt64(bb, this.creationTime);
            IsoTypeWriter.writeUInt64(bb, this.modificationTime);
            IsoTypeWriter.writeUInt32(bb, this.timescale);
            IsoTypeWriter.writeUInt64(bb, this.duration);
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.creationTime);
            IsoTypeWriter.writeUInt32(bb, this.modificationTime);
            IsoTypeWriter.writeUInt32(bb, this.timescale);
            IsoTypeWriter.writeUInt32(bb, this.duration);
        }
        IsoTypeWriter.writeIso639(bb, this.language);
        IsoTypeWriter.writeUInt16(bb, 0);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MediaHeaderBox.java", MediaHeaderBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCreationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "long"), 43);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getModificationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "long"), 47);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "java.lang.String"), 115);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTimescale", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "long"), 51);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDuration", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "long"), 55);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLanguage", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "java.lang.String"), 59);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCreationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "long", "creationTime", "", "void"), 76);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setModificationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "long", "modificationTime", "", "void"), 80);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTimescale", "com.coremedia.iso.boxes.MediaHeaderBox", "long", "timescale", "", "void"), 84);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDuration", "com.coremedia.iso.boxes.MediaHeaderBox", "long", "duration", "", "void"), 88);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLanguage", "com.coremedia.iso.boxes.MediaHeaderBox", "java.lang.String", "language", "", "void"), 92);
    }
}
