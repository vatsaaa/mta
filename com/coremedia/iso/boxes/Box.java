// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import com.coremedia.iso.BoxParser;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.io.IOException;
import java.nio.channels.WritableByteChannel;

public interface Box
{
    ContainerBox getParent();
    
    void setParent(final ContainerBox p0);
    
    long getSize();
    
    String getType();
    
    void getBox(final WritableByteChannel p0) throws IOException;
    
    void parse(final ReadableByteChannel p0, final ByteBuffer p1, final long p2, final BoxParser p3) throws IOException;
}
