// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;

public class CompositionShiftLeastGreatestAtom extends AbstractFullBox
{
    int compositionOffsetToDisplayOffsetShift;
    int leastDisplayOffset;
    int greatestDisplayOffset;
    int displayStartTime;
    int displayEndTime;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    
    public CompositionShiftLeastGreatestAtom() {
        super("cslg");
    }
    
    @Override
    protected long getContentSize() {
        return 24L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.compositionOffsetToDisplayOffsetShift = content.getInt();
        this.leastDisplayOffset = content.getInt();
        this.greatestDisplayOffset = content.getInt();
        this.displayStartTime = content.getInt();
        this.displayEndTime = content.getInt();
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.putInt(this.compositionOffsetToDisplayOffsetShift);
        bb.putInt(this.leastDisplayOffset);
        bb.putInt(this.greatestDisplayOffset);
        bb.putInt(this.displayStartTime);
        bb.putInt(this.displayEndTime);
    }
    
    public int getCompositionOffsetToDisplayOffsetShift() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_0, this, this));
        return this.compositionOffsetToDisplayOffsetShift;
    }
    
    public void setCompositionOffsetToDisplayOffsetShift(final int compositionOffsetToDisplayOffsetShift) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_1, this, this, Conversions.intObject(compositionOffsetToDisplayOffsetShift)));
        this.compositionOffsetToDisplayOffsetShift = compositionOffsetToDisplayOffsetShift;
    }
    
    public int getLeastDisplayOffset() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_2, this, this));
        return this.leastDisplayOffset;
    }
    
    public void setLeastDisplayOffset(final int leastDisplayOffset) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_3, this, this, Conversions.intObject(leastDisplayOffset)));
        this.leastDisplayOffset = leastDisplayOffset;
    }
    
    public int getGreatestDisplayOffset() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_4, this, this));
        return this.greatestDisplayOffset;
    }
    
    public void setGreatestDisplayOffset(final int greatestDisplayOffset) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_5, this, this, Conversions.intObject(greatestDisplayOffset)));
        this.greatestDisplayOffset = greatestDisplayOffset;
    }
    
    public int getDisplayStartTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_6, this, this));
        return this.displayStartTime;
    }
    
    public void setDisplayStartTime(final int displayStartTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_7, this, this, Conversions.intObject(displayStartTime)));
        this.displayStartTime = displayStartTime;
    }
    
    public int getDisplayEndTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_8, this, this));
        return this.displayEndTime;
    }
    
    public void setDisplayEndTime(final int displayEndTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CompositionShiftLeastGreatestAtom.ajc$tjp_9, this, this, Conversions.intObject(displayEndTime)));
        this.displayEndTime = displayEndTime;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("CompositionShiftLeastGreatestAtom.java", CompositionShiftLeastGreatestAtom.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCompositionOffsetToDisplayOffsetShift", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 61);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCompositionOffsetToDisplayOffsetShift", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "compositionOffsetToDisplayOffsetShift", "", "void"), 65);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLeastDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 69);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLeastDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "leastDisplayOffset", "", "void"), 73);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getGreatestDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 77);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setGreatestDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "greatestDisplayOffset", "", "void"), 81);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDisplayStartTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 85);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDisplayStartTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "displayStartTime", "", "void"), 89);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDisplayEndTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 93);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDisplayEndTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "displayEndTime", "", "void"), 97);
    }
}
