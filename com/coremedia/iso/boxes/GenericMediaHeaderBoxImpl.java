// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import java.io.IOException;
import java.nio.ByteBuffer;

public class GenericMediaHeaderBoxImpl extends AbstractMediaHeaderBox
{
    ByteBuffer data;
    
    @Override
    protected long getContentSize() {
        return 4 + this.data.limit();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.data = content.slice();
        content.position(content.remaining() + content.position());
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put((ByteBuffer)this.data.rewind());
    }
    
    public GenericMediaHeaderBoxImpl() {
        super("gmhd");
    }
}
