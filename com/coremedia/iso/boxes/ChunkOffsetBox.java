// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public abstract class ChunkOffsetBox extends AbstractFullBox
{
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public ChunkOffsetBox(final String type) {
        super(type);
    }
    
    public abstract long[] getChunkOffsets();
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ChunkOffsetBox.ajc$tjp_0, this, this));
        return String.valueOf(this.getClass().getSimpleName()) + "[entryCount=" + this.getChunkOffsets().length + "]";
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("ChunkOffsetBox.java", ChunkOffsetBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.ChunkOffsetBox", "", "", "", "java.lang.String"), 15);
    }
}
