// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class TrackHeaderBox extends AbstractFullBox
{
    public static final String TYPE = "tkhd";
    private long creationTime;
    private long modificationTime;
    private long trackId;
    private long duration;
    private int layer;
    private int alternateGroup;
    private float volume;
    private long[] matrix;
    private double width;
    private double height;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_18;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_19;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_20;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_21;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_22;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_23;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_24;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_25;
    
    public TrackHeaderBox() {
        super("tkhd");
        this.matrix = new long[] { 65536L, 0L, 0L, 0L, 65536L, 0L, 0L, 0L, 1073741824L };
    }
    
    public long getCreationTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_0, this, this));
        return this.creationTime;
    }
    
    public long getModificationTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_1, this, this));
        return this.modificationTime;
    }
    
    public long getTrackId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_2, this, this));
        return this.trackId;
    }
    
    public long getDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_3, this, this));
        return this.duration;
    }
    
    public int getLayer() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_4, this, this));
        return this.layer;
    }
    
    public int getAlternateGroup() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_5, this, this));
        return this.alternateGroup;
    }
    
    public float getVolume() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_6, this, this));
        return this.volume;
    }
    
    public long[] getMatrix() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_7, this, this));
        return this.matrix;
    }
    
    public double getWidth() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_8, this, this));
        return this.width;
    }
    
    public double getHeight() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_9, this, this));
        return this.height;
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 4L;
        if (this.getVersion() == 1) {
            contentSize += 32L;
        }
        else {
            contentSize += 20L;
        }
        contentSize += 60L;
        return contentSize;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if (this.getVersion() == 1) {
            this.creationTime = IsoTypeReader.readUInt64(content);
            this.modificationTime = IsoTypeReader.readUInt64(content);
            this.trackId = IsoTypeReader.readUInt32(content);
            IsoTypeReader.readUInt32(content);
            this.duration = IsoTypeReader.readUInt64(content);
        }
        else {
            this.creationTime = IsoTypeReader.readUInt32(content);
            this.modificationTime = IsoTypeReader.readUInt32(content);
            this.trackId = IsoTypeReader.readUInt32(content);
            IsoTypeReader.readUInt32(content);
            this.duration = IsoTypeReader.readUInt32(content);
        }
        IsoTypeReader.readUInt32(content);
        IsoTypeReader.readUInt32(content);
        this.layer = IsoTypeReader.readUInt16(content);
        this.alternateGroup = IsoTypeReader.readUInt16(content);
        this.volume = IsoTypeReader.readFixedPoint88(content);
        IsoTypeReader.readUInt16(content);
        this.matrix = new long[9];
        for (int i = 0; i < 9; ++i) {
            this.matrix[i] = IsoTypeReader.readUInt32(content);
        }
        this.width = IsoTypeReader.readFixedPoint1616(content);
        this.height = IsoTypeReader.readFixedPoint1616(content);
    }
    
    public void getContent(final ByteBuffer bb) throws IOException {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_10, this, this, bb));
        this.writeVersionAndFlags(bb);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt64(bb, this.creationTime);
            IsoTypeWriter.writeUInt64(bb, this.modificationTime);
            IsoTypeWriter.writeUInt32(bb, this.trackId);
            IsoTypeWriter.writeUInt32(bb, 0L);
            IsoTypeWriter.writeUInt64(bb, this.duration);
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.creationTime);
            IsoTypeWriter.writeUInt32(bb, this.modificationTime);
            IsoTypeWriter.writeUInt32(bb, this.trackId);
            IsoTypeWriter.writeUInt32(bb, 0L);
            IsoTypeWriter.writeUInt32(bb, this.duration);
        }
        IsoTypeWriter.writeUInt32(bb, 0L);
        IsoTypeWriter.writeUInt32(bb, 0L);
        IsoTypeWriter.writeUInt16(bb, this.layer);
        IsoTypeWriter.writeUInt16(bb, this.alternateGroup);
        IsoTypeWriter.writeFixedPont88(bb, this.volume);
        IsoTypeWriter.writeUInt16(bb, 0);
        for (int i = 0; i < 9; ++i) {
            IsoTypeWriter.writeUInt32(bb, this.matrix[i]);
        }
        IsoTypeWriter.writeFixedPont1616(bb, this.width);
        IsoTypeWriter.writeFixedPont1616(bb, this.height);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_11, this, this));
        final StringBuilder result = new StringBuilder();
        result.append("TrackHeaderBox[");
        result.append("creationTime=").append(this.getCreationTime());
        result.append(";");
        result.append("modificationTime=").append(this.getModificationTime());
        result.append(";");
        result.append("trackId=").append(this.getTrackId());
        result.append(";");
        result.append("duration=").append(this.getDuration());
        result.append(";");
        result.append("layer=").append(this.getLayer());
        result.append(";");
        result.append("alternateGroup=").append(this.getAlternateGroup());
        result.append(";");
        result.append("volume=").append(this.getVolume());
        for (int i = 0; i < this.matrix.length; ++i) {
            result.append(";");
            result.append("matrix").append(i).append("=").append(this.matrix[i]);
        }
        result.append(";");
        result.append("width=").append(this.getWidth());
        result.append(";");
        result.append("height=").append(this.getHeight());
        result.append("]");
        return result.toString();
    }
    
    public void setCreationTime(final long creationTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_12, this, this, Conversions.longObject(creationTime)));
        this.creationTime = creationTime;
    }
    
    public void setModificationTime(final long modificationTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_13, this, this, Conversions.longObject(modificationTime)));
        this.modificationTime = modificationTime;
    }
    
    public void setTrackId(final long trackId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_14, this, this, Conversions.longObject(trackId)));
        this.trackId = trackId;
    }
    
    public void setDuration(final long duration) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_15, this, this, Conversions.longObject(duration)));
        this.duration = duration;
    }
    
    public void setLayer(final int layer) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_16, this, this, Conversions.intObject(layer)));
        this.layer = layer;
    }
    
    public void setAlternateGroup(final int alternateGroup) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_17, this, this, Conversions.intObject(alternateGroup)));
        this.alternateGroup = alternateGroup;
    }
    
    public void setVolume(final float volume) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_18, this, this, Conversions.floatObject(volume)));
        this.volume = volume;
    }
    
    public void setMatrix(final long[] matrix) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_19, this, this, matrix));
        this.matrix = matrix;
    }
    
    public void setWidth(final double width) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_20, this, this, Conversions.doubleObject(width)));
        this.width = width;
    }
    
    public void setHeight(final double height) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_21, this, this, Conversions.doubleObject(height)));
        this.height = height;
    }
    
    public boolean isEnabled() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_22, this, this));
        return (this.getFlags() & 0x1) > 0;
    }
    
    public boolean isInMovie() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_23, this, this));
        return (this.getFlags() & 0x2) > 0;
    }
    
    public boolean isInPreview() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_24, this, this));
        return (this.getFlags() & 0x4) > 0;
    }
    
    public boolean isInPoster() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackHeaderBox.ajc$tjp_25, this, this));
        return (this.getFlags() & 0x8) > 0;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackHeaderBox.java", TrackHeaderBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCreationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "long"), 56);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getModificationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "long"), 60);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getContent", "com.coremedia.iso.boxes.TrackHeaderBox", "java.nio.ByteBuffer", "bb", "java.io.IOException", "void"), 137);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "java.lang.String"), 165);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCreationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "long", "creationTime", "", "void"), 193);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setModificationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "long", "modificationTime", "", "void"), 197);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTrackId", "com.coremedia.iso.boxes.TrackHeaderBox", "long", "trackId", "", "void"), 201);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDuration", "com.coremedia.iso.boxes.TrackHeaderBox", "long", "duration", "", "void"), 205);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLayer", "com.coremedia.iso.boxes.TrackHeaderBox", "int", "layer", "", "void"), 209);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAlternateGroup", "com.coremedia.iso.boxes.TrackHeaderBox", "int", "alternateGroup", "", "void"), 213);
        ajc$tjp_18 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setVolume", "com.coremedia.iso.boxes.TrackHeaderBox", "float", "volume", "", "void"), 217);
        ajc$tjp_19 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMatrix", "com.coremedia.iso.boxes.TrackHeaderBox", "[J", "matrix", "", "void"), 221);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackId", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "long"), 64);
        ajc$tjp_20 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setWidth", "com.coremedia.iso.boxes.TrackHeaderBox", "double", "width", "", "void"), 225);
        ajc$tjp_21 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setHeight", "com.coremedia.iso.boxes.TrackHeaderBox", "double", "height", "", "void"), 229);
        ajc$tjp_22 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isEnabled", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 234);
        ajc$tjp_23 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isInMovie", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 238);
        ajc$tjp_24 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isInPreview", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 242);
        ajc$tjp_25 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isInPoster", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 246);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDuration", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "long"), 68);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLayer", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "int"), 72);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAlternateGroup", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "int"), 76);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getVolume", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "float"), 80);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMatrix", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "[J"), 84);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getWidth", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "double"), 88);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHeight", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "double"), 92);
    }
}
