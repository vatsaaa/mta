// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.vodafone;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class CoverUriBox extends AbstractFullBox
{
    public static final String TYPE = "cvru";
    private String coverUri;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public CoverUriBox() {
        super("cvru");
    }
    
    public String getCoverUri() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CoverUriBox.ajc$tjp_0, this, this));
        return this.coverUri;
    }
    
    public void setCoverUri(final String coverUri) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CoverUriBox.ajc$tjp_1, this, this, coverUri));
        this.coverUri = coverUri;
    }
    
    @Override
    protected long getContentSize() {
        return Utf8.utf8StringLengthInBytes(this.coverUri) + 5;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.coverUri = IsoTypeReader.readString(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(Utf8.convert(this.coverUri));
        bb.put((byte)0);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(CoverUriBox.ajc$tjp_2, this, this));
        return "CoverUriBox[coverUri=" + this.getCoverUri() + "]";
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("CoverUriBox.java", CoverUriBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCoverUri", "com.coremedia.iso.boxes.vodafone.CoverUriBox", "", "", "", "java.lang.String"), 38);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCoverUri", "com.coremedia.iso.boxes.vodafone.CoverUriBox", "java.lang.String", "coverUri", "", "void"), 42);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.vodafone.CoverUriBox", "", "", "", "java.lang.String"), 64);
    }
}
