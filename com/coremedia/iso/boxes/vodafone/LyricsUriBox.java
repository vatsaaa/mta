// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.vodafone;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class LyricsUriBox extends AbstractFullBox
{
    public static final String TYPE = "lrcu";
    private String lyricsUri;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public LyricsUriBox() {
        super("lrcu");
    }
    
    public String getLyricsUri() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LyricsUriBox.ajc$tjp_0, this, this));
        return this.lyricsUri;
    }
    
    public void setLyricsUri(final String lyricsUri) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LyricsUriBox.ajc$tjp_1, this, this, lyricsUri));
        this.lyricsUri = lyricsUri;
    }
    
    @Override
    protected long getContentSize() {
        return Utf8.utf8StringLengthInBytes(this.lyricsUri) + 5;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.lyricsUri = IsoTypeReader.readString(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(Utf8.convert(this.lyricsUri));
        bb.put((byte)0);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(LyricsUriBox.ajc$tjp_2, this, this));
        return "LyricsUriBox[lyricsUri=" + this.getLyricsUri() + "]";
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("LyricsUriBox.java", LyricsUriBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLyricsUri", "com.coremedia.iso.boxes.vodafone.LyricsUriBox", "", "", "", "java.lang.String"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLyricsUri", "com.coremedia.iso.boxes.vodafone.LyricsUriBox", "java.lang.String", "lyricsUri", "", "void"), 43);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.vodafone.LyricsUriBox", "", "", "", "java.lang.String"), 64);
    }
}
