// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class DataEntryUrnBox extends AbstractFullBox
{
    private String name;
    private String location;
    public static final String TYPE = "urn ";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public DataEntryUrnBox() {
        super("urn ");
    }
    
    public String getName() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DataEntryUrnBox.ajc$tjp_0, this, this));
        return this.name;
    }
    
    public String getLocation() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DataEntryUrnBox.ajc$tjp_1, this, this));
        return this.location;
    }
    
    @Override
    protected long getContentSize() {
        return Utf8.utf8StringLengthInBytes(this.name) + 1 + Utf8.utf8StringLengthInBytes(this.location) + 1;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.name = IsoTypeReader.readString(content);
        this.location = IsoTypeReader.readString(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(Utf8.convert(this.name));
        bb.put((byte)0);
        bb.put(Utf8.convert(this.location));
        bb.put((byte)0);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(DataEntryUrnBox.ajc$tjp_2, this, this));
        return "DataEntryUrlBox[name=" + this.getName() + ";location=" + this.getLocation() + "]";
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("DataEntryUrnBox.java", DataEntryUrnBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getName", "com.coremedia.iso.boxes.DataEntryUrnBox", "", "", "", "java.lang.String"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLocation", "com.coremedia.iso.boxes.DataEntryUrnBox", "", "", "", "java.lang.String"), 43);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.DataEntryUrnBox", "", "", "", "java.lang.String"), 66);
    }
}
