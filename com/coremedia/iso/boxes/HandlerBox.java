// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoFile;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.Collections;
import java.util.HashMap;
import org.aspectj.lang.JoinPoint;
import java.util.Map;

public class HandlerBox extends AbstractFullBox
{
    public static final String TYPE = "hdlr";
    public static final Map<String, String> readableTypes;
    private String handlerType;
    private String name;
    private long a;
    private long b;
    private long c;
    private boolean zeroTerm;
    private long shouldBeZeroButAppleWritesHereSomeValue;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    static {
        ajc$preClinit();
        final HashMap hm = new HashMap();
        hm.put("odsm", "ObjectDescriptorStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hm.put("crsm", "ClockReferenceStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hm.put("sdsm", "SceneDescriptionStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hm.put("m7sm", "MPEG7Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hm.put("ocsm", "ObjectContentInfoStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hm.put("ipsm", "IPMP Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hm.put("mjsm", "MPEG-J Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        hm.put("mdir", "Apple Meta Data iTunes Reader");
        hm.put("mp7b", "MPEG-7 binary XML");
        hm.put("mp7t", "MPEG-7 XML");
        hm.put("vide", "Video Track");
        hm.put("soun", "Sound Track");
        hm.put("hint", "Hint Track");
        hm.put("appl", "Apple specific");
        hm.put("meta", "Timed Metadata track - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
        readableTypes = Collections.unmodifiableMap((Map<? extends String, ? extends String>)hm);
    }
    
    public HandlerBox() {
        super("hdlr");
        this.name = null;
        this.zeroTerm = true;
    }
    
    public String getHandlerType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(HandlerBox.ajc$tjp_0, this, this));
        return this.handlerType;
    }
    
    public void setName(final String name) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(HandlerBox.ajc$tjp_1, this, this, name));
        this.name = name;
    }
    
    public void setHandlerType(final String handlerType) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(HandlerBox.ajc$tjp_2, this, this, handlerType));
        this.handlerType = handlerType;
    }
    
    public String getName() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(HandlerBox.ajc$tjp_3, this, this));
        return this.name;
    }
    
    public String getHumanReadableTrackType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(HandlerBox.ajc$tjp_4, this, this));
        return (HandlerBox.readableTypes.get(this.handlerType) != null) ? HandlerBox.readableTypes.get(this.handlerType) : "Unknown Handler Type";
    }
    
    @Override
    protected long getContentSize() {
        if (this.zeroTerm) {
            return 25 + Utf8.utf8StringLengthInBytes(this.name);
        }
        return 24 + Utf8.utf8StringLengthInBytes(this.name);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.shouldBeZeroButAppleWritesHereSomeValue = IsoTypeReader.readUInt32(content);
        this.handlerType = IsoTypeReader.read4cc(content);
        this.a = IsoTypeReader.readUInt32(content);
        this.b = IsoTypeReader.readUInt32(content);
        this.c = IsoTypeReader.readUInt32(content);
        if (content.remaining() > 0) {
            this.name = IsoTypeReader.readString(content, content.remaining());
            if (this.name.endsWith("\u0000")) {
                this.name = this.name.substring(0, this.name.length() - 1);
                this.zeroTerm = true;
            }
            else {
                this.zeroTerm = false;
            }
        }
        else {
            this.zeroTerm = false;
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.shouldBeZeroButAppleWritesHereSomeValue);
        bb.put(IsoFile.fourCCtoBytes(this.handlerType));
        IsoTypeWriter.writeUInt32(bb, this.a);
        IsoTypeWriter.writeUInt32(bb, this.b);
        IsoTypeWriter.writeUInt32(bb, this.c);
        if (this.name != null) {
            bb.put(Utf8.convert(this.name));
        }
        if (this.zeroTerm) {
            bb.put((byte)0);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(HandlerBox.ajc$tjp_5, this, this));
        return "HandlerBox[handlerType=" + this.getHandlerType() + ";name=" + this.getName() + "]";
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("HandlerBox.java", HandlerBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHandlerType", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 77);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setName", "com.coremedia.iso.boxes.HandlerBox", "java.lang.String", "name", "", "void"), 86);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setHandlerType", "com.coremedia.iso.boxes.HandlerBox", "java.lang.String", "handlerType", "", "void"), 90);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getName", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 94);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHumanReadableTrackType", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 98);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 148);
    }
}
