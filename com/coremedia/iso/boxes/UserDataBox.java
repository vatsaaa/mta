// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import java.io.IOException;
import com.coremedia.iso.BoxParser;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class UserDataBox extends AbstractContainerBox
{
    public static final String TYPE = "udta";
    
    @Override
    protected long getContentSize() {
        return super.getContentSize();
    }
    
    @Override
    public void parse(final ReadableByteChannel in, final ByteBuffer header, final long contentSize, final BoxParser boxParser) throws IOException {
        super.parse(in, header, contentSize, boxParser);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        super._parseDetails(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        super.getContent(bb);
    }
    
    public UserDataBox() {
        super("udta");
    }
}
