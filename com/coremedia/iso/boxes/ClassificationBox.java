// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import com.coremedia.iso.IsoFile;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class ClassificationBox extends AbstractFullBox
{
    public static final String TYPE = "clsf";
    private String classificationEntity;
    private int classificationTableIndex;
    private String language;
    private String classificationInfo;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    
    public ClassificationBox() {
        super("clsf");
    }
    
    public String getLanguage() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_0, this, this));
        return this.language;
    }
    
    public String getClassificationEntity() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_1, this, this));
        return this.classificationEntity;
    }
    
    public int getClassificationTableIndex() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_2, this, this));
        return this.classificationTableIndex;
    }
    
    public String getClassificationInfo() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_3, this, this));
        return this.classificationInfo;
    }
    
    public void setClassificationEntity(final String classificationEntity) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_4, this, this, classificationEntity));
        this.classificationEntity = classificationEntity;
    }
    
    public void setClassificationTableIndex(final int classificationTableIndex) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_5, this, this, Conversions.intObject(classificationTableIndex)));
        this.classificationTableIndex = classificationTableIndex;
    }
    
    public void setLanguage(final String language) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_6, this, this, language));
        this.language = language;
    }
    
    public void setClassificationInfo(final String classificationInfo) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_7, this, this, classificationInfo));
        this.classificationInfo = classificationInfo;
    }
    
    @Override
    protected long getContentSize() {
        return 8 + Utf8.utf8StringLengthInBytes(this.classificationInfo) + 1;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final byte[] cE = new byte[4];
        content.get(cE);
        this.classificationEntity = IsoFile.bytesToFourCC(cE);
        this.classificationTableIndex = IsoTypeReader.readUInt16(content);
        this.language = IsoTypeReader.readIso639(content);
        this.classificationInfo = IsoTypeReader.readString(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(IsoFile.fourCCtoBytes(this.classificationEntity));
        IsoTypeWriter.writeUInt16(bb, this.classificationTableIndex);
        IsoTypeWriter.writeIso639(bb, this.language);
        bb.put(Utf8.convert(this.classificationInfo));
        bb.put((byte)0);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ClassificationBox.ajc$tjp_8, this, this));
        final StringBuilder buffer = new StringBuilder();
        buffer.append("ClassificationBox[language=").append(this.getLanguage());
        buffer.append("classificationEntity=").append(this.getClassificationEntity());
        buffer.append(";classificationTableIndex=").append(this.getClassificationTableIndex());
        buffer.append(";language=").append(this.getLanguage());
        buffer.append(";classificationInfo=").append(this.getClassificationInfo());
        buffer.append("]");
        return buffer.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("ClassificationBox.java", ClassificationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLanguage", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 43);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getClassificationEntity", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 47);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getClassificationTableIndex", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "int"), 51);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getClassificationInfo", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 55);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setClassificationEntity", "com.coremedia.iso.boxes.ClassificationBox", "java.lang.String", "classificationEntity", "", "void"), 59);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setClassificationTableIndex", "com.coremedia.iso.boxes.ClassificationBox", "int", "classificationTableIndex", "", "void"), 63);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLanguage", "com.coremedia.iso.boxes.ClassificationBox", "java.lang.String", "language", "", "void"), 67);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setClassificationInfo", "com.coremedia.iso.boxes.ClassificationBox", "java.lang.String", "classificationInfo", "", "void"), 71);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 100);
    }
}
