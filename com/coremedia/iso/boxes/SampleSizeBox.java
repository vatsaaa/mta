// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class SampleSizeBox extends AbstractFullBox
{
    private long sampleSize;
    private long[] sampleSizes;
    public static final String TYPE = "stsz";
    int sampleCount;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    
    public SampleSizeBox() {
        super("stsz");
        this.sampleSizes = new long[0];
    }
    
    public long getSampleSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleSizeBox.ajc$tjp_0, this, this));
        return this.sampleSize;
    }
    
    public void setSampleSize(final long sampleSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleSizeBox.ajc$tjp_1, this, this, Conversions.longObject(sampleSize)));
        this.sampleSize = sampleSize;
    }
    
    public long getSampleSizeAtIndex(final int index) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleSizeBox.ajc$tjp_2, this, this, Conversions.intObject(index)));
        if (this.sampleSize > 0L) {
            return this.sampleSize;
        }
        return this.sampleSizes[index];
    }
    
    public long getSampleCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleSizeBox.ajc$tjp_3, this, this));
        if (this.sampleSize > 0L) {
            return this.sampleCount;
        }
        return this.sampleSizes.length;
    }
    
    public long[] getSampleSizes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleSizeBox.ajc$tjp_4, this, this));
        return this.sampleSizes;
    }
    
    public void setSampleSizes(final long[] sampleSizes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleSizeBox.ajc$tjp_5, this, this, sampleSizes));
        this.sampleSizes = sampleSizes;
    }
    
    @Override
    protected long getContentSize() {
        return 12 + ((this.sampleSize == 0L) ? (this.sampleSizes.length * 4) : 0);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.sampleSize = IsoTypeReader.readUInt32(content);
        this.sampleCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        if (this.sampleSize == 0L) {
            this.sampleSizes = new long[this.sampleCount];
            for (int i = 0; i < this.sampleCount; ++i) {
                this.sampleSizes[i] = IsoTypeReader.readUInt32(content);
            }
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.sampleSize);
        if (this.sampleSize == 0L) {
            IsoTypeWriter.writeUInt32(bb, this.sampleSizes.length);
            long[] sampleSizes;
            for (int length = (sampleSizes = this.sampleSizes).length, i = 0; i < length; ++i) {
                final long sampleSize1 = sampleSizes[i];
                IsoTypeWriter.writeUInt32(bb, sampleSize1);
            }
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.sampleCount);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleSizeBox.ajc$tjp_6, this, this));
        return "SampleSizeBox[sampleSize=" + this.getSampleSize() + ";sampleCount=" + this.getSampleCount() + "]";
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleSizeBox.java", SampleSizeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleSize", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "long"), 49);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleSize", "com.coremedia.iso.boxes.SampleSizeBox", "long", "sampleSize", "", "void"), 53);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleSizeAtIndex", "com.coremedia.iso.boxes.SampleSizeBox", "int", "index", "", "long"), 58);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleCount", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "long"), 66);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleSizes", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "[J"), 75);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleSizes", "com.coremedia.iso.boxes.SampleSizeBox", "[J", "sampleSizes", "", "void"), 79);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "java.lang.String"), 118);
    }
}
