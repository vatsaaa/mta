// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import com.coremedia.iso.IsoTypeWriter;
import java.io.IOException;
import java.nio.channels.ReadableByteChannel;
import com.googlecode.mp4parser.ByteBufferByteChannel;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class MetaBox extends AbstractContainerBox
{
    private int version;
    private int flags;
    public static final String TYPE = "meta";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public MetaBox() {
        super("meta");
        this.version = 0;
        this.flags = 0;
    }
    
    public long getContentSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MetaBox.ajc$tjp_0, this, this));
        if (this.isMp4Box()) {
            return 4L + super.getContentSize();
        }
        return super.getContentSize();
    }
    
    @Override
    public long getNumOfBytesToFirstChild() {
        if (this.isMp4Box()) {
            return 12L;
        }
        return 8L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        final int pos = content.position();
        content.get(new byte[4]);
        final String isHdlr = IsoTypeReader.read4cc(content);
        if ("hdlr".equals(isHdlr)) {
            content.position(pos);
            this.version = -1;
            this.flags = -1;
        }
        else {
            content.position(pos);
            this.version = IsoTypeReader.readUInt8(content);
            this.flags = IsoTypeReader.readUInt24(content);
        }
        while (content.remaining() >= 8) {
            try {
                this.boxes.add(this.boxParser.parseBox(new ByteBufferByteChannel(content), this));
            }
            catch (IOException ex) {
                throw new RuntimeException("Sebastian needs to fix 7518765283");
            }
        }
        if (content.remaining() > 0) {
            throw new RuntimeException("Sebastian needs to fix it 90732r26537");
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        if (this.isMp4Box()) {
            IsoTypeWriter.writeUInt8(bb, this.version);
            IsoTypeWriter.writeUInt24(bb, this.flags);
        }
        this.writeChildBoxes(bb);
    }
    
    public boolean isMp4Box() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MetaBox.ajc$tjp_1, this, this));
        return this.version != -1 && this.flags != -1;
    }
    
    public void setMp4Box(final boolean mp4) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MetaBox.ajc$tjp_2, this, this, Conversions.booleanObject(mp4)));
        if (mp4) {
            this.version = 0;
            this.flags = 0;
        }
        else {
            this.version = -1;
            this.flags = -1;
        }
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MetaBox.java", MetaBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getContentSize", "com.coremedia.iso.boxes.MetaBox", "", "", "", "long"), 41);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isMp4Box", "com.coremedia.iso.boxes.MetaBox", "", "", "", "boolean"), 99);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMp4Box", "com.coremedia.iso.boxes.MetaBox", "boolean", "mp4", "", "void"), 103);
    }
}
