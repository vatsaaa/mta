// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoFile;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class SchemeTypeBox extends AbstractFullBox
{
    public static final String TYPE = "schm";
    String schemeType;
    long schemeVersion;
    String schemeUri;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    
    static {
        ajc$preClinit();
    }
    
    public SchemeTypeBox() {
        super("schm");
        this.schemeType = "    ";
        this.schemeUri = null;
    }
    
    public String getSchemeType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SchemeTypeBox.ajc$tjp_0, this, this));
        return this.schemeType;
    }
    
    public long getSchemeVersion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SchemeTypeBox.ajc$tjp_1, this, this));
        return this.schemeVersion;
    }
    
    public String getSchemeUri() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SchemeTypeBox.ajc$tjp_2, this, this));
        return this.schemeUri;
    }
    
    public void setSchemeType(final String schemeType) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SchemeTypeBox.ajc$tjp_3, this, this, schemeType));
        assert schemeType != null && schemeType.length() == 4 : "SchemeType may not be null or not 4 bytes long";
        this.schemeType = schemeType;
    }
    
    public void setSchemeVersion(final int schemeVersion) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SchemeTypeBox.ajc$tjp_4, this, this, Conversions.intObject(schemeVersion)));
        this.schemeVersion = schemeVersion;
    }
    
    public void setSchemeUri(final String schemeUri) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SchemeTypeBox.ajc$tjp_5, this, this, schemeUri));
        this.schemeUri = schemeUri;
    }
    
    @Override
    protected long getContentSize() {
        return 12 + (((this.getFlags() & 0x1) == 0x1) ? (Utf8.utf8StringLengthInBytes(this.schemeUri) + 1) : 0);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.schemeType = IsoTypeReader.read4cc(content);
        this.schemeVersion = IsoTypeReader.readUInt32(content);
        if ((this.getFlags() & 0x1) == 0x1) {
            this.schemeUri = IsoTypeReader.readString(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(IsoFile.fourCCtoBytes(this.schemeType));
        IsoTypeWriter.writeUInt32(bb, this.schemeVersion);
        if ((this.getFlags() & 0x1) == 0x1) {
            bb.put(Utf8.convert(this.schemeUri));
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SchemeTypeBox.ajc$tjp_6, this, this));
        final StringBuilder buffer = new StringBuilder();
        buffer.append("Schema Type Box[");
        buffer.append("schemeUri=").append(this.schemeUri).append("; ");
        buffer.append("schemeType=").append(this.schemeType).append("; ");
        buffer.append("schemeVersion=").append(this.schemeUri).append("; ");
        buffer.append("]");
        return buffer.toString();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SchemeTypeBox.java", SchemeTypeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSchemeType", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "java.lang.String"), 43);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSchemeVersion", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "long"), 47);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSchemeUri", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "java.lang.String"), 51);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSchemeType", "com.coremedia.iso.boxes.SchemeTypeBox", "java.lang.String", "schemeType", "", "void"), 55);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSchemeVersion", "com.coremedia.iso.boxes.SchemeTypeBox", "int", "schemeVersion", "", "void"), 60);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSchemeUri", "com.coremedia.iso.boxes.SchemeTypeBox", "java.lang.String", "schemeUri", "", "void"), 64);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "java.lang.String"), 92);
    }
}
