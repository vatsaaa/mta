// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import com.coremedia.iso.IsoTypeReader;
import java.util.LinkedList;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import java.util.Collections;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class ProgressiveDownloadInformationBox extends AbstractFullBox
{
    List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public ProgressiveDownloadInformationBox() {
        super("pdin");
        this.entries = Collections.emptyList();
    }
    
    @Override
    protected long getContentSize() {
        return 4 + this.entries.size() * 8;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        for (final Entry entry : this.entries) {
            IsoTypeWriter.writeUInt32(bb, entry.getRate());
            IsoTypeWriter.writeUInt32(bb, entry.getInitialDelay());
        }
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ProgressiveDownloadInformationBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ProgressiveDownloadInformationBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.entries = new LinkedList<Entry>();
        while (content.remaining() >= 8) {
            final Entry entry = new Entry(IsoTypeReader.readUInt32(content), IsoTypeReader.readUInt32(content));
            this.entries.add(entry);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ProgressiveDownloadInformationBox.ajc$tjp_2, this, this));
        return "ProgressiveDownloadInfoBox{entries=" + this.entries + '}';
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("ProgressiveDownloadInformationBox.java", ProgressiveDownloadInformationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.ProgressiveDownloadInformationBox", "", "", "", "java.util.List"), 35);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.ProgressiveDownloadInformationBox", "java.util.List", "entries", "", "void"), 39);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.ProgressiveDownloadInformationBox", "", "", "", "java.lang.String"), 89);
    }
    
    public static class Entry
    {
        long rate;
        long initialDelay;
        
        public Entry(final long rate, final long initialDelay) {
            this.rate = rate;
            this.initialDelay = initialDelay;
        }
        
        public long getRate() {
            return this.rate;
        }
        
        public void setRate(final long rate) {
            this.rate = rate;
        }
        
        public long getInitialDelay() {
            return this.initialDelay;
        }
        
        public void setInitialDelay(final long initialDelay) {
            this.initialDelay = initialDelay;
        }
        
        @Override
        public String toString() {
            return "Entry{rate=" + this.rate + ", initialDelay=" + this.initialDelay + '}';
        }
    }
}
