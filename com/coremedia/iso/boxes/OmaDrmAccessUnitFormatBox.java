// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public final class OmaDrmAccessUnitFormatBox extends AbstractFullBox
{
    public static final String TYPE = "odaf";
    private boolean selectiveEncryption;
    private byte allBits;
    private int keyIndicatorLength;
    private int initVectorLength;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    @Override
    protected long getContentSize() {
        return 7L;
    }
    
    public OmaDrmAccessUnitFormatBox() {
        super("odaf");
    }
    
    public boolean isSelectiveEncryption() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OmaDrmAccessUnitFormatBox.ajc$tjp_0, this, this));
        return this.selectiveEncryption;
    }
    
    public int getKeyIndicatorLength() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OmaDrmAccessUnitFormatBox.ajc$tjp_1, this, this));
        return this.keyIndicatorLength;
    }
    
    public int getInitVectorLength() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OmaDrmAccessUnitFormatBox.ajc$tjp_2, this, this));
        return this.initVectorLength;
    }
    
    public void setInitVectorLength(final int initVectorLength) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OmaDrmAccessUnitFormatBox.ajc$tjp_3, this, this, Conversions.intObject(initVectorLength)));
        this.initVectorLength = initVectorLength;
    }
    
    public void setKeyIndicatorLength(final int keyIndicatorLength) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OmaDrmAccessUnitFormatBox.ajc$tjp_4, this, this, Conversions.intObject(keyIndicatorLength)));
        this.keyIndicatorLength = keyIndicatorLength;
    }
    
    public void setAllBits(final byte allBits) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(OmaDrmAccessUnitFormatBox.ajc$tjp_5, this, this, Conversions.byteObject(allBits)));
        this.allBits = allBits;
        this.selectiveEncryption = ((allBits & 0x80) == 0x80);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.allBits = (byte)IsoTypeReader.readUInt8(content);
        this.selectiveEncryption = ((this.allBits & 0x80) == 0x80);
        this.keyIndicatorLength = IsoTypeReader.readUInt8(content);
        this.initVectorLength = IsoTypeReader.readUInt8(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt8(bb, this.allBits);
        IsoTypeWriter.writeUInt8(bb, this.keyIndicatorLength);
        IsoTypeWriter.writeUInt8(bb, this.initVectorLength);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("OmaDrmAccessUnitFormatBox.java", OmaDrmAccessUnitFormatBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isSelectiveEncryption", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "", "", "", "boolean"), 45);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getKeyIndicatorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "", "", "", "int"), 49);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getInitVectorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "", "", "", "int"), 53);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setInitVectorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "int", "initVectorLength", "", "void"), 57);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setKeyIndicatorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "int", "keyIndicatorLength", "", "void"), 61);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAllBits", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "byte", "allBits", "", "void"), 65);
    }
}
