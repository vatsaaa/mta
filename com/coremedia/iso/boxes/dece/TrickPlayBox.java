// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.dece;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.ArrayList;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TrickPlayBox extends AbstractFullBox
{
    public static final String TYPE = "trik";
    private List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public TrickPlayBox() {
        super("trik");
        this.entries = new ArrayList<Entry>();
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrickPlayBox.ajc$tjp_0, this, this, entries));
        this.entries = entries;
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrickPlayBox.ajc$tjp_1, this, this));
        return this.entries;
    }
    
    @Override
    protected long getContentSize() {
        return 4 + this.entries.size();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        while (content.remaining() > 0) {
            this.entries.add(new Entry(IsoTypeReader.readUInt8(content)));
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        for (final Entry entry : this.entries) {
            IsoTypeWriter.writeUInt8(bb, entry.value);
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrickPlayBox.ajc$tjp_2, this, this));
        final StringBuilder sb = new StringBuilder();
        sb.append("TrickPlayBox");
        sb.append("{entries=").append(this.entries);
        sb.append('}');
        return sb.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrickPlayBox.java", TrickPlayBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.dece.TrickPlayBox", "java.util.List", "entries", "", "void"), 31);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.dece.TrickPlayBox", "", "", "", "java.util.List"), 35);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.dece.TrickPlayBox", "", "", "", "java.lang.String"), 102);
    }
    
    public static class Entry
    {
        private int value;
        
        public Entry() {
        }
        
        public Entry(final int value) {
            this.value = value;
        }
        
        public int getPicType() {
            return this.value >> 6 & 0x3;
        }
        
        public void setPicType(final int picType) {
            this.value &= 0x1F;
            this.value |= (picType & 0x3) << 6;
        }
        
        public int getDependencyLevel() {
            return this.value & 0x3F;
        }
        
        public void setDependencyLevel(final int dependencyLevel) {
            this.value |= (dependencyLevel & 0x3F);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Entry");
            sb.append("{picType=").append(this.getPicType());
            sb.append(",dependencyLevel=").append(this.getDependencyLevel());
            sb.append('}');
            return sb.toString();
        }
    }
}
