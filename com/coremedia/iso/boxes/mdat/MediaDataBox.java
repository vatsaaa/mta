// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.mdat;

import com.coremedia.iso.ChannelHelper;
import com.coremedia.iso.boxes.CastUtils;
import java.nio.channels.FileChannel;
import com.coremedia.iso.BoxParser;
import java.nio.channels.ReadableByteChannel;
import java.io.IOException;
import java.nio.channels.WritableByteChannel;
import java.nio.ByteBuffer;
import com.coremedia.iso.boxes.ContainerBox;
import com.coremedia.iso.boxes.Box;

public final class MediaDataBox implements Box
{
    public static final String TYPE = "mdat";
    ContainerBox parent;
    ByteBuffer header;
    ByteBuffer content;
    
    public ContainerBox getParent() {
        return this.parent;
    }
    
    public void setParent(final ContainerBox parent) {
        this.parent = parent;
    }
    
    public String getType() {
        return "mdat";
    }
    
    public void getBox(final WritableByteChannel writableByteChannel) throws IOException {
        this.header.rewind();
        this.content.rewind();
        writableByteChannel.write(this.header);
        writableByteChannel.write(this.content);
    }
    
    public long getSize() {
        return this.header.limit() + this.content.limit();
    }
    
    public void parse(final ReadableByteChannel in, final ByteBuffer header, final long contentSize, final BoxParser boxParser) throws IOException {
        this.header = header;
        if (in instanceof FileChannel && contentSize > 1048576L) {
            this.content = ((FileChannel)in).map(FileChannel.MapMode.READ_ONLY, ((FileChannel)in).position(), contentSize);
            ((FileChannel)in).position(((FileChannel)in).position() + contentSize);
        }
        else {
            this.content = ChannelHelper.readFully(in, CastUtils.l2i(contentSize));
        }
    }
    
    public ByteBuffer getContent() {
        return this.content;
    }
    
    public ByteBuffer getHeader() {
        return this.header;
    }
}
