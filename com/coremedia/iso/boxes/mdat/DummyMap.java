// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.mdat;

import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;

public class DummyMap<K, V> implements Map<K, V>
{
    HashSet<K> keys;
    V value;
    
    public DummyMap(final V value) {
        this.keys = new HashSet<K>();
        this.value = value;
    }
    
    public Comparator<? super K> comparator() {
        return null;
    }
    
    public void addKeys(final K[] keys) {
        Collections.addAll(this.keys, keys);
    }
    
    public int size() {
        return this.keys.size();
    }
    
    public boolean isEmpty() {
        return this.keys.isEmpty();
    }
    
    public boolean containsKey(final Object key) {
        return this.keys.contains(key);
    }
    
    public boolean containsValue(final Object value) {
        return this.value == value;
    }
    
    public V get(final Object key) {
        return this.keys.contains(key) ? this.value : null;
    }
    
    public V put(final K key, final V value) {
        assert this.value == value;
        this.keys.add(key);
        return this.value;
    }
    
    public V remove(final Object key) {
        final V v = this.get(key);
        this.keys.remove(key);
        return v;
    }
    
    public void putAll(final Map<? extends K, ? extends V> m) {
        for (final K k : m.keySet()) {
            assert m.get(k) == this.value;
            this.keys.add(k);
        }
    }
    
    public void clear() {
        this.keys.clear();
    }
    
    public Set<K> keySet() {
        return this.keys;
    }
    
    public Collection<V> values() {
        throw new UnsupportedOperationException();
    }
    
    public Set<Entry<K, V>> entrySet() {
        throw new UnsupportedOperationException();
    }
}
