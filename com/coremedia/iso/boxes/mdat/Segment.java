// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.mdat;

public class Segment
{
    public long offset;
    public long size;
    
    public Segment(final long offset, final long size) {
        this.offset = offset;
        this.size = size;
    }
}
