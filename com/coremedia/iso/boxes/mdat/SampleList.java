// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.mdat;

import com.coremedia.iso.boxes.fragment.TrackRunBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentBox;
import com.coremedia.iso.boxes.CastUtils;
import com.coremedia.iso.boxes.SampleToChunkBox;
import com.coremedia.iso.boxes.ChunkOffsetBox;
import com.coremedia.iso.boxes.SampleSizeBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentBox;
import com.coremedia.iso.boxes.fragment.TrackExtendsBox;
import com.coremedia.iso.boxes.fragment.MovieExtendsBox;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.TrackBox;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import com.coremedia.iso.IsoFile;
import java.util.List;
import java.util.Map;
import java.nio.ByteBuffer;
import java.util.AbstractList;

public class SampleList extends AbstractList<ByteBuffer>
{
    Map<Long, Long> offsets2Sizes;
    List<Long> offsetKeys;
    IsoFile isoFile;
    HashMap<MediaDataBox, Long> mdatStartCache;
    HashMap<MediaDataBox, Long> mdatEndCache;
    ArrayList<MediaDataBox> mdats;
    
    public List<Long> getOffsetKeys() {
        if (this.offsetKeys == null) {
            final List<Long> offsetKeys = new ArrayList<Long>(this.offsets2Sizes.size());
            for (final Long aLong : this.offsets2Sizes.keySet()) {
                offsetKeys.add(aLong);
            }
            Collections.sort(offsetKeys);
            this.offsetKeys = offsetKeys;
        }
        return this.offsetKeys;
    }
    
    public SampleList(final TrackBox trackBox) {
        this.offsetKeys = null;
        this.mdatStartCache = new HashMap<MediaDataBox, Long>();
        this.mdatEndCache = new HashMap<MediaDataBox, Long>();
        this.mdats = new ArrayList<MediaDataBox>(1);
        this.isoFile = trackBox.getIsoFile();
        this.offsets2Sizes = new HashMap<Long, Long>();
        long currentOffset = 0L;
        for (final Box b : this.isoFile.getBoxes()) {
            final long currentSize = b.getSize();
            if ("mdat".equals(b.getType())) {
                if (!(b instanceof MediaDataBox)) {
                    throw new RuntimeException("Sample need to be in mdats and mdats need to be instanceof MediaDataBox");
                }
                final long contentOffset = currentOffset + ((MediaDataBox)b).getHeader().limit();
                this.mdatStartCache.put((MediaDataBox)b, contentOffset);
                this.mdatEndCache.put((MediaDataBox)b, contentOffset + currentSize);
                this.mdats.add((MediaDataBox)b);
            }
            currentOffset += currentSize;
        }
        final SampleSizeBox sampleSizeBox = trackBox.getSampleTableBox().getSampleSizeBox();
        final ChunkOffsetBox chunkOffsetBox = trackBox.getSampleTableBox().getChunkOffsetBox();
        final SampleToChunkBox sampleToChunkBox = trackBox.getSampleTableBox().getSampleToChunkBox();
        if (sampleToChunkBox != null && sampleToChunkBox.getEntries().size() > 0 && chunkOffsetBox != null && chunkOffsetBox.getChunkOffsets().length > 0 && sampleSizeBox != null && sampleSizeBox.getSampleCount() > 0L) {
            final long[] numberOfSamplesInChunk = sampleToChunkBox.blowup(chunkOffsetBox.getChunkOffsets().length);
            if (sampleSizeBox.getSampleSize() > 0L) {
                this.offsets2Sizes = new DummyMap<Long, Long>(sampleSizeBox.getSampleSize());
                final long sampleSize = sampleSizeBox.getSampleSize();
                for (int i = 0; i < numberOfSamplesInChunk.length; ++i) {
                    final long thisChunksNumberOfSamples = numberOfSamplesInChunk[i];
                    long sampleOffset = chunkOffsetBox.getChunkOffsets()[i];
                    for (int j = 0; j < thisChunksNumberOfSamples; ++j) {
                        this.offsets2Sizes.put(sampleOffset, sampleSize);
                        sampleOffset += sampleSize;
                    }
                }
            }
            else {
                int sampleIndex = 0;
                final long[] sampleSizes = sampleSizeBox.getSampleSizes();
                for (int i = 0; i < numberOfSamplesInChunk.length; ++i) {
                    final long thisChunksNumberOfSamples = numberOfSamplesInChunk[i];
                    long sampleOffset = chunkOffsetBox.getChunkOffsets()[i];
                    for (int j = 0; j < thisChunksNumberOfSamples; ++j) {
                        final long sampleSize2 = sampleSizes[sampleIndex];
                        this.offsets2Sizes.put(sampleOffset, sampleSize2);
                        sampleOffset += sampleSize2;
                        ++sampleIndex;
                    }
                }
            }
        }
        final List<MovieExtendsBox> movieExtendsBoxes = trackBox.getParent().getBoxes(MovieExtendsBox.class);
        if (movieExtendsBoxes.size() > 0) {
            final List<TrackExtendsBox> trackExtendsBoxes = movieExtendsBoxes.get(0).getBoxes(TrackExtendsBox.class);
            for (final TrackExtendsBox trackExtendsBox : trackExtendsBoxes) {
                if (trackExtendsBox.getTrackId() == trackBox.getTrackHeaderBox().getTrackId()) {
                    for (final MovieFragmentBox movieFragmentBox : trackBox.getIsoFile().getBoxes(MovieFragmentBox.class)) {
                        this.offsets2Sizes.putAll(this.getOffsets(movieFragmentBox, trackBox.getTrackHeaderBox().getTrackId()));
                    }
                }
            }
        }
    }
    
    @Override
    public int size() {
        return this.offsets2Sizes.size();
    }
    
    @Override
    public ByteBuffer get(final int index) {
        final Long offset = this.getOffsetKeys().get(index);
        final int sampleSize = CastUtils.l2i(this.offsets2Sizes.get(offset));
        for (final MediaDataBox mediaDataBox : this.mdats) {
            final long start = this.mdatStartCache.get(mediaDataBox);
            final long end = this.mdatEndCache.get(mediaDataBox);
            if (start <= offset && offset + sampleSize <= end) {
                final ByteBuffer bb = mediaDataBox.getContent();
                bb.position(CastUtils.l2i(offset - start));
                final ByteBuffer sample = bb.slice();
                sample.limit(sampleSize);
                return sample;
            }
        }
        throw new RuntimeException("The sample with offset " + offset + " and size " + sampleSize + " is NOT located within an mdat");
    }
    
    Map<Long, Long> getOffsets(final MovieFragmentBox moof, final long trackId) {
        final Map<Long, Long> offsets2Sizes = new HashMap<Long, Long>();
        final List<TrackFragmentBox> traf = moof.getBoxes(TrackFragmentBox.class);
        for (final TrackFragmentBox trackFragmentBox : traf) {
            if (trackFragmentBox.getTrackFragmentHeaderBox().getTrackId() == trackId) {
                long baseDataOffset;
                if (trackFragmentBox.getTrackFragmentHeaderBox().hasBaseDataOffset()) {
                    baseDataOffset = trackFragmentBox.getTrackFragmentHeaderBox().getBaseDataOffset();
                }
                else {
                    baseDataOffset = moof.getOffset();
                }
                for (final TrackRunBox trun : trackFragmentBox.getBoxes(TrackRunBox.class)) {
                    final long sampleBaseOffset = baseDataOffset + trun.getDataOffset();
                    final long[] sampleOffsets = trun.getSampleOffsets();
                    final long[] sampleSizes = trun.getSampleSizes();
                    for (int i = 0; i < sampleSizes.length; ++i) {
                        offsets2Sizes.put(sampleOffsets[i] + sampleBaseOffset, sampleSizes[i]);
                    }
                }
            }
        }
        return offsets2Sizes;
    }
}
