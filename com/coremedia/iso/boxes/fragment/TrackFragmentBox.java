// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractContainerBox;

public class TrackFragmentBox extends AbstractContainerBox
{
    public static final String TYPE = "traf";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public TrackFragmentBox() {
        super("traf");
    }
    
    public TrackFragmentHeaderBox getTrackFragmentHeaderBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentBox.ajc$tjp_0, this, this));
        for (final Box box : this.getBoxes()) {
            if (box instanceof TrackFragmentHeaderBox) {
                return (TrackFragmentHeaderBox)box;
            }
        }
        return null;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackFragmentBox.java", TrackFragmentBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackFragmentHeaderBox", "com.coremedia.iso.boxes.fragment.TrackFragmentBox", "", "", "", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox"), 34);
    }
}
