// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import java.io.IOException;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitWriterBuffer;
import com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BitReaderBuffer;
import java.nio.ByteBuffer;

public class SampleFlags
{
    private int reserved;
    private int sampleDependsOn;
    private int sampleIsDependedOn;
    private int sampleHasRedundancy;
    private int samplePaddingValue;
    private boolean sampleIsDifferenceSample;
    private int sampleDegradationPriority;
    
    public SampleFlags() {
    }
    
    public SampleFlags(final ByteBuffer bb) {
        final BitReaderBuffer brb = new BitReaderBuffer(bb);
        this.reserved = brb.readBits(6);
        this.sampleDependsOn = brb.readBits(2);
        this.sampleIsDependedOn = brb.readBits(2);
        this.sampleHasRedundancy = brb.readBits(2);
        this.samplePaddingValue = brb.readBits(3);
        this.sampleIsDifferenceSample = (brb.readBits(1) == 1);
        this.sampleDegradationPriority = brb.readBits(16);
    }
    
    public void getContent(final ByteBuffer os) throws IOException {
        final BitWriterBuffer bitWriterBuffer = new BitWriterBuffer(os);
        bitWriterBuffer.writeBits(this.reserved, 6);
        bitWriterBuffer.writeBits(this.sampleDependsOn, 2);
        bitWriterBuffer.writeBits(this.sampleIsDependedOn, 2);
        bitWriterBuffer.writeBits(this.sampleHasRedundancy, 2);
        bitWriterBuffer.writeBits(this.samplePaddingValue, 3);
        bitWriterBuffer.writeBits(this.sampleIsDifferenceSample ? 1 : 0, 1);
        bitWriterBuffer.writeBits(this.sampleDegradationPriority, 16);
    }
    
    public int getReserved() {
        return this.reserved;
    }
    
    public void setReserved(final int reserved) {
        this.reserved = reserved;
    }
    
    public int getSampleDependsOn() {
        return this.sampleDependsOn;
    }
    
    public void setSampleDependsOn(final int sampleDependsOn) {
        this.sampleDependsOn = sampleDependsOn;
    }
    
    public int getSampleIsDependedOn() {
        return this.sampleIsDependedOn;
    }
    
    public void setSampleIsDependedOn(final int sampleIsDependedOn) {
        this.sampleIsDependedOn = sampleIsDependedOn;
    }
    
    public int getSampleHasRedundancy() {
        return this.sampleHasRedundancy;
    }
    
    public void setSampleHasRedundancy(final int sampleHasRedundancy) {
        this.sampleHasRedundancy = sampleHasRedundancy;
    }
    
    public int getSamplePaddingValue() {
        return this.samplePaddingValue;
    }
    
    public void setSamplePaddingValue(final int samplePaddingValue) {
        this.samplePaddingValue = samplePaddingValue;
    }
    
    public boolean isSampleIsDifferenceSample() {
        return this.sampleIsDifferenceSample;
    }
    
    public void setSampleIsDifferenceSample(final boolean sampleIsDifferenceSample) {
        this.sampleIsDifferenceSample = sampleIsDifferenceSample;
    }
    
    public int getSampleDegradationPriority() {
        return this.sampleDegradationPriority;
    }
    
    public void setSampleDegradationPriority(final int sampleDegradationPriority) {
        this.sampleDegradationPriority = sampleDegradationPriority;
    }
    
    @Override
    public String toString() {
        return "SampleFlags{reserved=" + this.reserved + ", sampleDependsOn=" + this.sampleDependsOn + ", sampleHasRedundancy=" + this.sampleHasRedundancy + ", samplePaddingValue=" + this.samplePaddingValue + ", sampleIsDifferenceSample=" + this.sampleIsDifferenceSample + ", sampleDegradationPriority=" + this.sampleDegradationPriority + '}';
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final SampleFlags that = (SampleFlags)o;
        return this.reserved == that.reserved && this.sampleDegradationPriority == that.sampleDegradationPriority && this.sampleDependsOn == that.sampleDependsOn && this.sampleHasRedundancy == that.sampleHasRedundancy && this.sampleIsDependedOn == that.sampleIsDependedOn && this.sampleIsDifferenceSample == that.sampleIsDifferenceSample && this.samplePaddingValue == that.samplePaddingValue;
    }
    
    @Override
    public int hashCode() {
        int result = this.reserved;
        result = 31 * result + this.sampleDependsOn;
        result = 31 * result + this.sampleIsDependedOn;
        result = 31 * result + this.sampleHasRedundancy;
        result = 31 * result + this.samplePaddingValue;
        result = 31 * result + (this.sampleIsDifferenceSample ? 1 : 0);
        result = 31 * result + this.sampleDegradationPriority;
        return result;
    }
}
