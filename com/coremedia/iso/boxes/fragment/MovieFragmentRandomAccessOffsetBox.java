// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class MovieFragmentRandomAccessOffsetBox extends AbstractFullBox
{
    public static final String TYPE = "mfro";
    private long mfraSize;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public MovieFragmentRandomAccessOffsetBox() {
        super("mfro");
    }
    
    @Override
    protected long getContentSize() {
        return 8L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.mfraSize = IsoTypeReader.readUInt32(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.mfraSize);
    }
    
    public long getMfraSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieFragmentRandomAccessOffsetBox.ajc$tjp_0, this, this));
        return this.mfraSize;
    }
    
    public void setMfraSize(final long mfraSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieFragmentRandomAccessOffsetBox.ajc$tjp_1, this, this, Conversions.longObject(mfraSize)));
        this.mfraSize = mfraSize;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MovieFragmentRandomAccessOffsetBox.java", MovieFragmentRandomAccessOffsetBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMfraSize", "com.coremedia.iso.boxes.fragment.MovieFragmentRandomAccessOffsetBox", "", "", "", "long"), 56);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMfraSize", "com.coremedia.iso.boxes.fragment.MovieFragmentRandomAccessOffsetBox", "long", "mfraSize", "", "void"), 60);
    }
}
