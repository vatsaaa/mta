// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import org.aspectj.runtime.internal.Conversions;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriterVariable;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReaderVariable;
import java.util.ArrayList;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.Collections;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TrackFragmentRandomAccessBox extends AbstractFullBox
{
    public static final String TYPE = "tfra";
    private long trackId;
    private int reserved;
    private int lengthSizeOfTrafNum;
    private int lengthSizeOfTrunNum;
    private int lengthSizeOfSampleNum;
    private List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    
    public TrackFragmentRandomAccessBox() {
        super("tfra");
        this.lengthSizeOfTrafNum = 2;
        this.lengthSizeOfTrunNum = 2;
        this.lengthSizeOfSampleNum = 2;
        this.entries = Collections.emptyList();
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 4L;
        contentSize += 12L;
        if (this.getVersion() == 1) {
            contentSize += 16 * this.entries.size();
        }
        else {
            contentSize += 8 * this.entries.size();
        }
        contentSize += this.lengthSizeOfTrafNum * this.entries.size();
        contentSize += this.lengthSizeOfTrunNum * this.entries.size();
        contentSize += this.lengthSizeOfSampleNum * this.entries.size();
        return contentSize;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.trackId = IsoTypeReader.readUInt32(content);
        final long temp = IsoTypeReader.readUInt32(content);
        this.reserved = (int)(temp >> 6);
        this.lengthSizeOfTrafNum = ((int)(temp & 0x3FL) >> 4) + 1;
        this.lengthSizeOfTrunNum = ((int)(temp & 0xCL) >> 2) + 1;
        this.lengthSizeOfSampleNum = (int)(temp & 0x3L) + 1;
        final long numberOfEntries = IsoTypeReader.readUInt32(content);
        this.entries = new ArrayList<Entry>();
        for (int i = 0; i < numberOfEntries; ++i) {
            final Entry entry = new Entry();
            if (this.getVersion() == 1) {
                Entry.access$0(entry, IsoTypeReader.readUInt64(content));
                Entry.access$1(entry, IsoTypeReader.readUInt64(content));
            }
            else {
                Entry.access$0(entry, IsoTypeReader.readUInt32(content));
                Entry.access$1(entry, IsoTypeReader.readUInt32(content));
            }
            Entry.access$2(entry, IsoTypeReaderVariable.read(content, this.lengthSizeOfTrafNum));
            Entry.access$3(entry, IsoTypeReaderVariable.read(content, this.lengthSizeOfTrunNum));
            Entry.access$4(entry, IsoTypeReaderVariable.read(content, this.lengthSizeOfSampleNum));
            this.entries.add(entry);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.trackId);
        long temp = this.reserved << 6;
        temp |= (this.lengthSizeOfTrafNum - 1 & 0x3) << 4;
        temp |= (this.lengthSizeOfTrunNum - 1 & 0x3) << 2;
        temp |= (this.lengthSizeOfSampleNum - 1 & 0x3);
        IsoTypeWriter.writeUInt32(bb, temp);
        IsoTypeWriter.writeUInt32(bb, this.entries.size());
        for (final Entry entry : this.entries) {
            if (this.getVersion() == 1) {
                IsoTypeWriter.writeUInt64(bb, entry.time);
                IsoTypeWriter.writeUInt64(bb, entry.moofOffset);
            }
            else {
                IsoTypeWriter.writeUInt32(bb, entry.time);
                IsoTypeWriter.writeUInt32(bb, entry.moofOffset);
            }
            IsoTypeWriterVariable.write(entry.trafNumber, bb, this.lengthSizeOfTrafNum);
            IsoTypeWriterVariable.write(entry.trunNumber, bb, this.lengthSizeOfTrunNum);
            IsoTypeWriterVariable.write(entry.sampleNumber, bb, this.lengthSizeOfSampleNum);
        }
    }
    
    public void setTrackId(final long trackId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_0, this, this, Conversions.longObject(trackId)));
        this.trackId = trackId;
    }
    
    public void setLengthSizeOfTrafNum(final int lengthSizeOfTrafNum) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_1, this, this, Conversions.intObject(lengthSizeOfTrafNum)));
        this.lengthSizeOfTrafNum = lengthSizeOfTrafNum;
    }
    
    public void setLengthSizeOfTrunNum(final int lengthSizeOfTrunNum) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_2, this, this, Conversions.intObject(lengthSizeOfTrunNum)));
        this.lengthSizeOfTrunNum = lengthSizeOfTrunNum;
    }
    
    public void setLengthSizeOfSampleNum(final int lengthSizeOfSampleNum) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_3, this, this, Conversions.intObject(lengthSizeOfSampleNum)));
        this.lengthSizeOfSampleNum = lengthSizeOfSampleNum;
    }
    
    public long getTrackId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_4, this, this));
        return this.trackId;
    }
    
    public int getReserved() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_5, this, this));
        return this.reserved;
    }
    
    public int getLengthSizeOfTrafNum() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_6, this, this));
        return this.lengthSizeOfTrafNum;
    }
    
    public int getLengthSizeOfTrunNum() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_7, this, this));
        return this.lengthSizeOfTrunNum;
    }
    
    public int getLengthSizeOfSampleNum() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_8, this, this));
        return this.lengthSizeOfSampleNum;
    }
    
    public long getNumberOfEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_9, this, this));
        return this.entries.size();
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_10, this, this));
        return Collections.unmodifiableList((List<? extends Entry>)this.entries);
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentRandomAccessBox.ajc$tjp_11, this, this, entries));
        this.entries = entries;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackFragmentRandomAccessBox.java", TrackFragmentRandomAccessBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "long", "trackId", "", "void"), 144);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLengthSizeOfTrafNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "int", "lengthSizeOfTrafNum", "", "void"), 148);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "java.util.List"), 184);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "java.util.List", "entries", "", "void"), 188);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLengthSizeOfTrunNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "int", "lengthSizeOfTrunNum", "", "void"), 152);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLengthSizeOfSampleNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "int", "lengthSizeOfSampleNum", "", "void"), 156);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "long"), 160);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getReserved", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 164);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLengthSizeOfTrafNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 168);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLengthSizeOfTrunNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 172);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLengthSizeOfSampleNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 176);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getNumberOfEntries", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "long"), 180);
    }
    
    public static class Entry
    {
        private long time;
        private long moofOffset;
        private long trafNumber;
        private long trunNumber;
        private long sampleNumber;
        
        public Entry() {
        }
        
        public Entry(final long time, final long moofOffset, final long trafNumber, final long trunNumber, final long sampleNumber) {
            this.moofOffset = moofOffset;
            this.sampleNumber = sampleNumber;
            this.time = time;
            this.trafNumber = trafNumber;
            this.trunNumber = trunNumber;
        }
        
        public long getTime() {
            return this.time;
        }
        
        public long getMoofOffset() {
            return this.moofOffset;
        }
        
        public long getTrafNumber() {
            return this.trafNumber;
        }
        
        public long getTrunNumber() {
            return this.trunNumber;
        }
        
        public long getSampleNumber() {
            return this.sampleNumber;
        }
        
        public void setTime(final long time) {
            this.time = time;
        }
        
        public void setMoofOffset(final long moofOffset) {
            this.moofOffset = moofOffset;
        }
        
        public void setTrafNumber(final long trafNumber) {
            this.trafNumber = trafNumber;
        }
        
        public void setTrunNumber(final long trunNumber) {
            this.trunNumber = trunNumber;
        }
        
        public void setSampleNumber(final long sampleNumber) {
            this.sampleNumber = sampleNumber;
        }
        
        @Override
        public String toString() {
            return "Entry{time=" + this.time + ", moofOffset=" + this.moofOffset + ", trafNumber=" + this.trafNumber + ", trunNumber=" + this.trunNumber + ", sampleNumber=" + this.sampleNumber + '}';
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            final Entry entry = (Entry)o;
            return this.moofOffset == entry.moofOffset && this.sampleNumber == entry.sampleNumber && this.time == entry.time && this.trafNumber == entry.trafNumber && this.trunNumber == entry.trunNumber;
        }
        
        @Override
        public int hashCode() {
            int result = (int)(this.time ^ this.time >>> 32);
            result = 31 * result + (int)(this.moofOffset ^ this.moofOffset >>> 32);
            result = 31 * result + (int)(this.trafNumber ^ this.trafNumber >>> 32);
            result = 31 * result + (int)(this.trunNumber ^ this.trunNumber >>> 32);
            result = 31 * result + (int)(this.sampleNumber ^ this.sampleNumber >>> 32);
            return result;
        }
        
        static /* synthetic */ void access$0(final Entry entry, final long time) {
            entry.time = time;
        }
        
        static /* synthetic */ void access$1(final Entry entry, final long moofOffset) {
            entry.moofOffset = moofOffset;
        }
        
        static /* synthetic */ void access$2(final Entry entry, final long trafNumber) {
            entry.trafNumber = trafNumber;
        }
        
        static /* synthetic */ void access$3(final Entry entry, final long trunNumber) {
            entry.trunNumber = trunNumber;
        }
        
        static /* synthetic */ void access$4(final Entry entry, final long sampleNumber) {
            entry.sampleNumber = sampleNumber;
        }
    }
}
