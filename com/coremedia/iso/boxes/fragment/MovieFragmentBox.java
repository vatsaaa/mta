// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import com.googlecode.mp4parser.DoNotParseDetail;
import com.coremedia.iso.boxes.Box;
import java.util.Iterator;
import java.util.ArrayList;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.List;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractContainerBox;

public class MovieFragmentBox extends AbstractContainerBox
{
    public static final String TYPE = "moof";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    
    public MovieFragmentBox() {
        super("moof");
    }
    
    public List<Long> getSyncSamples(final SampleDependencyTypeBox sdtp) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieFragmentBox.ajc$tjp_0, this, this, sdtp));
        final List result = new ArrayList();
        final List sampleEntries = sdtp.getEntries();
        long i = 1L;
        for (final SampleDependencyTypeBox.Entry sampleEntry : sampleEntries) {
            if (sampleEntry.getSampleDependsOn() == 2) {
                result.add(i);
            }
            ++i;
        }
        return (List<Long>)result;
    }
    
    @DoNotParseDetail
    public long getOffset() {
        Box b = this;
        long offset = 0L;
        while (b.getParent() != null) {
            for (final Box box : b.getParent().getBoxes()) {
                if (b == box) {
                    break;
                }
                offset += box.getSize();
            }
            b = b.getParent();
        }
        return offset;
    }
    
    public int getTrackCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieFragmentBox.ajc$tjp_1, this, this));
        return this.getBoxes(TrackFragmentBox.class, false).size();
    }
    
    public long[] getTrackNumbers() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieFragmentBox.ajc$tjp_2, this, this));
        final List trackBoxes = this.getBoxes(TrackFragmentBox.class, false);
        final long[] trackNumbers = new long[trackBoxes.size()];
        for (int trackCounter = 0; trackCounter < trackBoxes.size(); ++trackCounter) {
            final TrackFragmentBox trackBoxe = trackBoxes.get(trackCounter);
            trackNumbers[trackCounter] = trackBoxe.getTrackFragmentHeaderBox().getTrackId();
        }
        return trackNumbers;
    }
    
    public List<TrackRunBox> getTrackRunBoxes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieFragmentBox.ajc$tjp_3, this, this));
        return this.getBoxes(TrackRunBox.class, true);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MovieFragmentBox.java", MovieFragmentBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSyncSamples", "com.coremedia.iso.boxes.fragment.MovieFragmentBox", "com.coremedia.iso.boxes.SampleDependencyTypeBox", "sdtp", "", "java.util.List"), 40);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackCount", "com.coremedia.iso.boxes.fragment.MovieFragmentBox", "", "", "", "int"), 72);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackNumbers", "com.coremedia.iso.boxes.fragment.MovieFragmentBox", "", "", "", "[J"), 82);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackRunBoxes", "com.coremedia.iso.boxes.fragment.MovieFragmentBox", "", "", "", "java.util.List"), 93);
    }
}
