// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TrackFragmentBaseMediaDecodeTimeBox extends AbstractFullBox
{
    public static final String TYPE = "tfdt";
    private long baseMediaDecodeTime;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public TrackFragmentBaseMediaDecodeTimeBox() {
        super("tfdt");
    }
    
    @Override
    protected long getContentSize() {
        return (this.getVersion() == 0) ? 8 : 12;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt64(bb, this.baseMediaDecodeTime);
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.baseMediaDecodeTime);
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if (this.getVersion() == 1) {
            this.baseMediaDecodeTime = IsoTypeReader.readUInt64(content);
        }
        else {
            this.baseMediaDecodeTime = IsoTypeReader.readUInt32(content);
        }
    }
    
    public long getBaseMediaDecodeTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentBaseMediaDecodeTimeBox.ajc$tjp_0, this, this));
        return this.baseMediaDecodeTime;
    }
    
    public void setBaseMediaDecodeTime(final long baseMediaDecodeTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentBaseMediaDecodeTimeBox.ajc$tjp_1, this, this, Conversions.longObject(baseMediaDecodeTime)));
        this.baseMediaDecodeTime = baseMediaDecodeTime;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentBaseMediaDecodeTimeBox.ajc$tjp_2, this, this));
        return "TrackFragmentBaseMediaDecodeTimeBox{baseMediaDecodeTime=" + this.baseMediaDecodeTime + '}';
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackFragmentBaseMediaDecodeTimeBox.java", TrackFragmentBaseMediaDecodeTimeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBaseMediaDecodeTime", "com.coremedia.iso.boxes.fragment.TrackFragmentBaseMediaDecodeTimeBox", "", "", "", "long"), 63);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBaseMediaDecodeTime", "com.coremedia.iso.boxes.fragment.TrackFragmentBaseMediaDecodeTimeBox", "long", "baseMediaDecodeTime", "", "void"), 67);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.fragment.TrackFragmentBaseMediaDecodeTimeBox", "", "", "", "java.lang.String"), 72);
    }
}
