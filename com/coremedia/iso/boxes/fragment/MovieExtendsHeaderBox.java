// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class MovieExtendsHeaderBox extends AbstractFullBox
{
    public static final String TYPE = "mehd";
    private long fragmentDuration;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public MovieExtendsHeaderBox() {
        super("mehd");
    }
    
    @Override
    protected long getContentSize() {
        return (this.getVersion() == 1) ? 12 : 8;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.fragmentDuration = ((this.getVersion() == 1) ? IsoTypeReader.readUInt64(content) : IsoTypeReader.readUInt32(content));
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt64(bb, this.fragmentDuration);
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.fragmentDuration);
        }
    }
    
    public long getFragmentDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieExtendsHeaderBox.ajc$tjp_0, this, this));
        return this.fragmentDuration;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MovieExtendsHeaderBox.java", MovieExtendsHeaderBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFragmentDuration", "com.coremedia.iso.boxes.fragment.MovieExtendsHeaderBox", "", "", "", "long"), 65);
    }
}
