// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TrackFragmentHeaderBox extends AbstractFullBox
{
    public static final String TYPE = "tfhd";
    private long trackId;
    private long baseDataOffset;
    private long sampleDescriptionIndex;
    private long defaultSampleDuration;
    private long defaultSampleSize;
    private SampleFlags defaultSampleFlags;
    private boolean durationIsEmpty;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    
    public TrackFragmentHeaderBox() {
        super("tfhd");
        this.baseDataOffset = -1L;
        this.defaultSampleDuration = -1L;
        this.defaultSampleSize = -1L;
    }
    
    @Override
    protected long getContentSize() {
        long size = 8L;
        final int flags = this.getFlags();
        if ((flags & 0x1) == 0x1) {
            size += 8L;
        }
        if ((flags & 0x2) == 0x2) {
            size += 4L;
        }
        if ((flags & 0x8) == 0x8) {
            size += 4L;
        }
        if ((flags & 0x10) == 0x10) {
            size += 4L;
        }
        if ((flags & 0x20) == 0x20) {
            size += 4L;
        }
        return size;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.trackId);
        if ((this.getFlags() & 0x1) == 0x1) {
            IsoTypeWriter.writeUInt64(bb, this.getBaseDataOffset());
        }
        if ((this.getFlags() & 0x2) == 0x2) {
            IsoTypeWriter.writeUInt32(bb, this.getSampleDescriptionIndex());
        }
        if ((this.getFlags() & 0x8) == 0x8) {
            IsoTypeWriter.writeUInt32(bb, this.getDefaultSampleDuration());
        }
        if ((this.getFlags() & 0x10) == 0x10) {
            IsoTypeWriter.writeUInt32(bb, this.getDefaultSampleSize());
        }
        if ((this.getFlags() & 0x20) == 0x20) {
            this.defaultSampleFlags.getContent(bb);
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.trackId = IsoTypeReader.readUInt32(content);
        if ((this.getFlags() & 0x1) == 0x1) {
            this.baseDataOffset = IsoTypeReader.readUInt64(content);
        }
        if ((this.getFlags() & 0x2) == 0x2) {
            this.sampleDescriptionIndex = IsoTypeReader.readUInt32(content);
        }
        if ((this.getFlags() & 0x8) == 0x8) {
            this.defaultSampleDuration = IsoTypeReader.readUInt32(content);
        }
        if ((this.getFlags() & 0x10) == 0x10) {
            this.defaultSampleSize = IsoTypeReader.readUInt32(content);
        }
        if ((this.getFlags() & 0x20) == 0x20) {
            this.defaultSampleFlags = new SampleFlags(content);
        }
        if ((this.getFlags() & 0x10000) == 0x10000) {
            this.durationIsEmpty = true;
        }
    }
    
    public boolean hasBaseDataOffset() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_0, this, this));
        return (this.getFlags() & 0x1) != 0x0;
    }
    
    public long getTrackId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_1, this, this));
        return this.trackId;
    }
    
    public long getBaseDataOffset() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_2, this, this));
        return this.baseDataOffset;
    }
    
    public long getSampleDescriptionIndex() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_3, this, this));
        return this.sampleDescriptionIndex;
    }
    
    public long getDefaultSampleDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_4, this, this));
        return this.defaultSampleDuration;
    }
    
    public long getDefaultSampleSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_5, this, this));
        return this.defaultSampleSize;
    }
    
    public SampleFlags getDefaultSampleFlags() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_6, this, this));
        return this.defaultSampleFlags;
    }
    
    public boolean isDurationIsEmpty() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_7, this, this));
        return this.durationIsEmpty;
    }
    
    public void setTrackId(final long trackId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_8, this, this, Conversions.longObject(trackId)));
        this.trackId = trackId;
    }
    
    public void setBaseDataOffset(final long baseDataOffset) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_9, this, this, Conversions.longObject(baseDataOffset)));
        if (baseDataOffset == -1L) {
            this.setFlags(this.getFlags() & 0x7FFFFFFE);
        }
        else {
            this.setFlags(this.getFlags() | 0x1);
        }
        this.baseDataOffset = baseDataOffset;
    }
    
    public void setSampleDescriptionIndex(final long sampleDescriptionIndex) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_10, this, this, Conversions.longObject(sampleDescriptionIndex)));
        if (sampleDescriptionIndex == -1L) {
            this.setFlags(this.getFlags() & 0x7FFFFFFD);
        }
        else {
            this.setFlags(this.getFlags() | 0x2);
        }
        this.sampleDescriptionIndex = sampleDescriptionIndex;
    }
    
    public void setDefaultSampleDuration(final long defaultSampleDuration) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_11, this, this, Conversions.longObject(defaultSampleDuration)));
        this.setFlags(this.getFlags() | 0x8);
        this.defaultSampleDuration = defaultSampleDuration;
    }
    
    public void setDefaultSampleSize(final long defaultSampleSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_12, this, this, Conversions.longObject(defaultSampleSize)));
        this.setFlags(this.getFlags() | 0x10);
        this.defaultSampleSize = defaultSampleSize;
    }
    
    public void setDefaultSampleFlags(final SampleFlags defaultSampleFlags) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_13, this, this, defaultSampleFlags));
        this.setFlags(this.getFlags() | 0x20);
        this.defaultSampleFlags = defaultSampleFlags;
    }
    
    public void setDurationIsEmpty(final boolean durationIsEmpty) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_14, this, this, Conversions.booleanObject(durationIsEmpty)));
        this.setFlags(this.getFlags() | 0x10000);
        this.durationIsEmpty = durationIsEmpty;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackFragmentHeaderBox.ajc$tjp_15, this, this));
        final StringBuilder sb = new StringBuilder();
        sb.append("TrackFragmentHeaderBox");
        sb.append("{trackId=").append(this.trackId);
        sb.append(", baseDataOffset=").append(this.baseDataOffset);
        sb.append(", sampleDescriptionIndex=").append(this.sampleDescriptionIndex);
        sb.append(", defaultSampleDuration=").append(this.defaultSampleDuration);
        sb.append(", defaultSampleSize=").append(this.defaultSampleSize);
        sb.append(", defaultSampleFlags=").append(this.defaultSampleFlags);
        sb.append(", durationIsEmpty=").append(this.durationIsEmpty);
        sb.append('}');
        return sb.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackFragmentHeaderBox.java", TrackFragmentHeaderBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "hasBaseDataOffset", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 120);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 124);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "sampleDescriptionIndex", "", "void"), 165);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "defaultSampleDuration", "", "void"), 174);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "defaultSampleSize", "", "void"), 179);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "com.coremedia.iso.boxes.fragment.SampleFlags", "defaultSampleFlags", "", "void"), 184);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDurationIsEmpty", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "boolean", "durationIsEmpty", "", "void"), 189);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "java.lang.String"), 195);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBaseDataOffset", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 128);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 132);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 136);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 140);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "com.coremedia.iso.boxes.fragment.SampleFlags"), 144);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isDurationIsEmpty", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 148);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "trackId", "", "void"), 152);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBaseDataOffset", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "baseDataOffset", "", "void"), 156);
    }
}
