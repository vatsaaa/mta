// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.boxes.AbstractContainerBox;

public class MovieFragmentRandomAccessBox extends AbstractContainerBox
{
    public static final String TYPE = "mfra";
    
    public MovieFragmentRandomAccessBox() {
        super("mfra");
    }
}
