// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TrackExtendsBox extends AbstractFullBox
{
    public static final String TYPE = "trex";
    private long trackId;
    private long defaultSampleDescriptionIndex;
    private long defaultSampleDuration;
    private long defaultSampleSize;
    private SampleFlags defaultSampleFlags;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    
    public TrackExtendsBox() {
        super("trex");
    }
    
    @Override
    protected long getContentSize() {
        return 24L;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.trackId);
        IsoTypeWriter.writeUInt32(bb, this.defaultSampleDescriptionIndex);
        IsoTypeWriter.writeUInt32(bb, this.defaultSampleDuration);
        IsoTypeWriter.writeUInt32(bb, this.defaultSampleSize);
        this.defaultSampleFlags.getContent(bb);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.trackId = IsoTypeReader.readUInt32(content);
        this.defaultSampleDescriptionIndex = IsoTypeReader.readUInt32(content);
        this.defaultSampleDuration = IsoTypeReader.readUInt32(content);
        this.defaultSampleSize = IsoTypeReader.readUInt32(content);
        this.defaultSampleFlags = new SampleFlags(content);
    }
    
    public long getTrackId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_0, this, this));
        return this.trackId;
    }
    
    public long getDefaultSampleDescriptionIndex() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_1, this, this));
        return this.defaultSampleDescriptionIndex;
    }
    
    public long getDefaultSampleDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_2, this, this));
        return this.defaultSampleDuration;
    }
    
    public long getDefaultSampleSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_3, this, this));
        return this.defaultSampleSize;
    }
    
    public SampleFlags getDefaultSampleFlags() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_4, this, this));
        return this.defaultSampleFlags;
    }
    
    public String getDefaultSampleFlagsStr() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_5, this, this));
        return this.defaultSampleFlags.toString();
    }
    
    public void setTrackId(final long trackId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_6, this, this, Conversions.longObject(trackId)));
        this.trackId = trackId;
    }
    
    public void setDefaultSampleDescriptionIndex(final long defaultSampleDescriptionIndex) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_7, this, this, Conversions.longObject(defaultSampleDescriptionIndex)));
        this.defaultSampleDescriptionIndex = defaultSampleDescriptionIndex;
    }
    
    public void setDefaultSampleDuration(final long defaultSampleDuration) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_8, this, this, Conversions.longObject(defaultSampleDuration)));
        this.defaultSampleDuration = defaultSampleDuration;
    }
    
    public void setDefaultSampleSize(final long defaultSampleSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_9, this, this, Conversions.longObject(defaultSampleSize)));
        this.defaultSampleSize = defaultSampleSize;
    }
    
    public void setDefaultSampleFlags(final SampleFlags defaultSampleFlags) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackExtendsBox.ajc$tjp_10, this, this, defaultSampleFlags));
        this.defaultSampleFlags = defaultSampleFlags;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackExtendsBox.java", TrackExtendsBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackId", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 72);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 76);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "com.coremedia.iso.boxes.fragment.SampleFlags", "defaultSampleFlags", "", "void"), 112);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 80);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 84);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "com.coremedia.iso.boxes.fragment.SampleFlags"), 88);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleFlagsStr", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "java.lang.String"), 92);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTrackId", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "trackId", "", "void"), 96);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "defaultSampleDescriptionIndex", "", "void"), 100);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "defaultSampleDuration", "", "void"), 104);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "defaultSampleSize", "", "void"), 108);
    }
}
