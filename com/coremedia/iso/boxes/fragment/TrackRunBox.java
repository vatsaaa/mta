// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.fragment;

import org.aspectj.lang.Signature;
import com.coremedia.iso.boxes.CastUtils;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import java.util.List;
import com.coremedia.iso.boxes.AbstractFullBox;

public class TrackRunBox extends AbstractFullBox
{
    public static final String TYPE = "trun";
    private int dataOffset;
    private SampleFlags firstSampleFlags;
    private List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_18;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_19;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_20;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_21;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_22;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_23;
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setDataOffset(final int dataOffset) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_1, this, this, Conversions.intObject(dataOffset)));
        if (dataOffset == -1) {
            this.setFlags(this.getFlags() & 0xFFFFFE);
        }
        else {
            this.setFlags(this.getFlags() | 0x1);
        }
        this.dataOffset = dataOffset;
    }
    
    public long[] getSampleOffsets() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_2, this, this));
        final long[] result = new long[this.entries.size()];
        long offset = 0L;
        for (int i = 0; i < result.length; ++i) {
            result[i] = offset;
            if (this.isSampleSizePresent()) {
                offset += this.entries.get(i).getSampleSize();
            }
            else {
                offset += ((TrackFragmentBox)this.getParent()).getTrackFragmentHeaderBox().getDefaultSampleSize();
            }
        }
        return result;
    }
    
    public long[] getSampleSizes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_3, this, this));
        final long[] result = new long[this.entries.size()];
        for (int i = 0; i < result.length; ++i) {
            if (this.isSampleSizePresent()) {
                result[i] = this.entries.get(i).getSampleSize();
            }
            else {
                result[i] = ((TrackFragmentBox)this.getParent()).getTrackFragmentHeaderBox().getDefaultSampleSize();
            }
        }
        return result;
    }
    
    public long[] getSampleCompositionTimeOffsets() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_4, this, this));
        if (this.isSampleCompositionTimeOffsetPresent()) {
            final long[] result = new long[this.entries.size()];
            for (int i = 0; i < result.length; ++i) {
                result[i] = this.entries.get(i).getSampleCompositionTimeOffset();
            }
            return result;
        }
        return null;
    }
    
    public long[] getSampleDurations() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_5, this, this));
        final long[] result = new long[this.entries.size()];
        for (int i = 0; i < result.length; ++i) {
            if (this.isSampleDurationPresent()) {
                result[i] = this.entries.get(i).getSampleDuration();
            }
            else {
                result[i] = ((TrackFragmentBox)this.getParent()).getTrackFragmentHeaderBox().getDefaultSampleDuration();
            }
        }
        return result;
    }
    
    public TrackRunBox() {
        super("trun");
        this.entries = new ArrayList<Entry>();
    }
    
    @Override
    protected long getContentSize() {
        long size = 8L;
        final int flags = this.getFlags();
        if ((flags & 0x1) == 0x1) {
            size += 4L;
        }
        if ((flags & 0x4) == 0x4) {
            size += 4L;
        }
        long entrySize = 0L;
        if ((flags & 0x100) == 0x100) {
            entrySize += 4L;
        }
        if ((flags & 0x200) == 0x200) {
            entrySize += 4L;
        }
        if ((flags & 0x400) == 0x400) {
            entrySize += 4L;
        }
        if ((flags & 0x800) == 0x800) {
            entrySize += 4L;
        }
        size += entrySize * this.entries.size();
        return size;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.entries.size());
        final int flags = this.getFlags();
        if ((flags & 0x1) == 0x1) {
            IsoTypeWriter.writeUInt32(bb, this.dataOffset);
        }
        if ((flags & 0x4) == 0x4) {
            this.firstSampleFlags.getContent(bb);
        }
        for (final Entry entry : this.entries) {
            if ((flags & 0x100) == 0x100) {
                IsoTypeWriter.writeUInt32(bb, entry.sampleDuration);
            }
            if ((flags & 0x200) == 0x200) {
                IsoTypeWriter.writeUInt32(bb, entry.sampleSize);
            }
            if ((flags & 0x400) == 0x400) {
                entry.sampleFlags.getContent(bb);
            }
            if ((flags & 0x800) == 0x800) {
                bb.putInt(entry.sampleCompositionTimeOffset);
            }
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final long sampleCount = IsoTypeReader.readUInt32(content);
        if ((this.getFlags() & 0x1) == 0x1) {
            this.dataOffset = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        }
        else {
            this.dataOffset = -1;
        }
        if ((this.getFlags() & 0x4) == 0x4) {
            this.firstSampleFlags = new SampleFlags(content);
        }
        for (int i = 0; i < sampleCount; ++i) {
            final Entry entry = new Entry();
            if ((this.getFlags() & 0x100) == 0x100) {
                Entry.access$4(entry, IsoTypeReader.readUInt32(content));
            }
            if ((this.getFlags() & 0x200) == 0x200) {
                Entry.access$5(entry, IsoTypeReader.readUInt32(content));
            }
            if ((this.getFlags() & 0x400) == 0x400) {
                Entry.access$6(entry, new SampleFlags(content));
            }
            if ((this.getFlags() & 0x800) == 0x800) {
                Entry.access$7(entry, content.getInt());
            }
            this.entries.add(entry);
        }
    }
    
    public long getSampleCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_6, this, this));
        return this.entries.size();
    }
    
    public boolean isDataOffsetPresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_7, this, this));
        return (this.getFlags() & 0x1) == 0x1;
    }
    
    public boolean isFirstSampleFlagsPresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_8, this, this));
        return (this.getFlags() & 0x4) == 0x4;
    }
    
    public boolean isSampleSizePresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_9, this, this));
        return (this.getFlags() & 0x200) == 0x200;
    }
    
    public boolean isSampleDurationPresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_10, this, this));
        return (this.getFlags() & 0x100) == 0x100;
    }
    
    public boolean isSampleFlagsPresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_11, this, this));
        return (this.getFlags() & 0x400) == 0x400;
    }
    
    public boolean isSampleCompositionTimeOffsetPresent() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_12, this, this));
        return (this.getFlags() & 0x800) == 0x800;
    }
    
    public void setDataOffsetPresent(final boolean v) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_13, this, this, Conversions.booleanObject(v)));
        if (v) {
            this.setFlags(this.getFlags() | 0x1);
        }
        else {
            this.setFlags(this.getFlags() & 0xFFFFFE);
        }
    }
    
    public void setSampleSizePresent(final boolean v) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_14, this, this, Conversions.booleanObject(v)));
        if (v) {
            this.setFlags(this.getFlags() | 0x200);
        }
        else {
            this.setFlags(this.getFlags() & 0xFFFDFF);
        }
    }
    
    public void setSampleDurationPresent(final boolean v) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_15, this, this, Conversions.booleanObject(v)));
        if (v) {
            this.setFlags(this.getFlags() | 0x100);
        }
        else {
            this.setFlags(this.getFlags() & 0xFFFEFF);
        }
    }
    
    public void setSampleFlagsPresent(final boolean v) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_16, this, this, Conversions.booleanObject(v)));
        if (v) {
            this.setFlags(this.getFlags() | 0x400);
        }
        else {
            this.setFlags(this.getFlags() & 0xFFFBFF);
        }
    }
    
    public void setSampleCompositionTimeOffsetPresent(final boolean v) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_17, this, this, Conversions.booleanObject(v)));
        if (v) {
            this.setFlags(this.getFlags() | 0x800);
        }
        else {
            this.setFlags(this.getFlags() & 0xFFF7FF);
        }
    }
    
    public int getDataOffset() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_18, this, this));
        return this.dataOffset;
    }
    
    public SampleFlags getFirstSampleFlags() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_19, this, this));
        return this.firstSampleFlags;
    }
    
    public String getFirstSampleFlags4View() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_20, this, this));
        return (this.firstSampleFlags != null) ? this.firstSampleFlags.toString() : "";
    }
    
    public void setFirstSampleFlags(final SampleFlags firstSampleFlags) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_21, this, this, firstSampleFlags));
        if (firstSampleFlags == null) {
            this.setFlags(this.getFlags() & 0xFFFFFB);
        }
        else {
            this.setFlags(this.getFlags() | 0x4);
        }
        this.firstSampleFlags = firstSampleFlags;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_22, this, this));
        final StringBuilder sb = new StringBuilder();
        sb.append("TrackRunBox");
        sb.append("{sampleCount=").append(this.entries.size());
        sb.append(", dataOffset=").append(this.dataOffset);
        sb.append(", dataOffsetPresent=").append(this.isDataOffsetPresent());
        sb.append(", sampleSizePresent=").append(this.isSampleSizePresent());
        sb.append(", sampleDurationPresent=").append(this.isSampleDurationPresent());
        sb.append(", sampleFlagsPresentPresent=").append(this.isSampleFlagsPresent());
        sb.append(", sampleCompositionTimeOffsetPresent=").append(this.isSampleCompositionTimeOffsetPresent());
        sb.append(", firstSampleFlags=").append(this.firstSampleFlags);
        sb.append('}');
        return sb.toString();
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackRunBox.ajc$tjp_23, this, this, entries));
        this.entries = entries;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackRunBox.java", TrackRunBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "java.util.List"), 54);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDataOffset", "com.coremedia.iso.boxes.fragment.TrackRunBox", "int", "dataOffset", "", "void"), 117);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isSampleDurationPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 292);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isSampleFlagsPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 296);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isSampleCompositionTimeOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 300);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDataOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 304);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleSizePresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 312);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleDurationPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 320);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleFlagsPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 329);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleCompositionTimeOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 337);
        ajc$tjp_18 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDataOffset", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "int"), 346);
        ajc$tjp_19 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFirstSampleFlags", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "com.coremedia.iso.boxes.fragment.SampleFlags"), 350);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleOffsets", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "[J"), 126);
        ajc$tjp_20 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFirstSampleFlags4View", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "java.lang.String"), 354);
        ajc$tjp_21 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setFirstSampleFlags", "com.coremedia.iso.boxes.fragment.TrackRunBox", "com.coremedia.iso.boxes.fragment.SampleFlags", "firstSampleFlags", "", "void"), 358);
        ajc$tjp_22 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "java.lang.String"), 368);
        ajc$tjp_23 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.fragment.TrackRunBox", "java.util.List", "entries", "", "void"), 383);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleSizes", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "[J"), 142);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleCompositionTimeOffsets", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "[J"), 156);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleDurations", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "[J"), 168);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleCount", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "long"), 275);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isDataOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 279);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isFirstSampleFlagsPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 283);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isSampleSizePresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 288);
    }
    
    public static class Entry
    {
        private long sampleDuration;
        private long sampleSize;
        private SampleFlags sampleFlags;
        private int sampleCompositionTimeOffset;
        
        public Entry() {
        }
        
        public Entry(final long sampleDuration, final long sampleSize, final SampleFlags sampleFlags, final int sampleCompositionTimeOffset) {
            this.sampleDuration = sampleDuration;
            this.sampleSize = sampleSize;
            this.sampleFlags = sampleFlags;
            this.sampleCompositionTimeOffset = sampleCompositionTimeOffset;
        }
        
        public long getSampleDuration() {
            return this.sampleDuration;
        }
        
        public long getSampleSize() {
            return this.sampleSize;
        }
        
        public SampleFlags getSampleFlags() {
            return this.sampleFlags;
        }
        
        public int getSampleCompositionTimeOffset() {
            return this.sampleCompositionTimeOffset;
        }
        
        public void setSampleDuration(final long sampleDuration) {
            this.sampleDuration = sampleDuration;
        }
        
        public void setSampleSize(final long sampleSize) {
            this.sampleSize = sampleSize;
        }
        
        public void setSampleFlags(final SampleFlags sampleFlags) {
            this.sampleFlags = sampleFlags;
        }
        
        public void setSampleCompositionTimeOffset(final int sampleCompositionTimeOffset) {
            this.sampleCompositionTimeOffset = sampleCompositionTimeOffset;
        }
        
        @Override
        public String toString() {
            return "Entry{sampleDuration=" + this.sampleDuration + ", sampleSize=" + this.sampleSize + ", sampleFlags=" + this.sampleFlags + ", sampleCompositionTimeOffset=" + this.sampleCompositionTimeOffset + '}';
        }
        
        static /* synthetic */ void access$4(final Entry entry, final long sampleDuration) {
            entry.sampleDuration = sampleDuration;
        }
        
        static /* synthetic */ void access$5(final Entry entry, final long sampleSize) {
            entry.sampleSize = sampleSize;
        }
        
        static /* synthetic */ void access$6(final Entry entry, final SampleFlags sampleFlags) {
            entry.sampleFlags = sampleFlags;
        }
        
        static /* synthetic */ void access$7(final Entry entry, final int sampleCompositionTimeOffset) {
            entry.sampleCompositionTimeOffset = sampleCompositionTimeOffset;
        }
    }
}
