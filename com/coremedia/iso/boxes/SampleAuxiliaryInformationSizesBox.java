// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoFile;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class SampleAuxiliaryInformationSizesBox extends AbstractFullBox
{
    public static final String TYPE = "saiz";
    private int defaultSampleInfoSize;
    private List<Short> sampleInfoSizes;
    private int sampleCount;
    private String auxInfoType;
    private String auxInfoTypeParameter;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    
    static {
        ajc$preClinit();
    }
    
    public SampleAuxiliaryInformationSizesBox() {
        super("saiz");
        this.sampleInfoSizes = new LinkedList<Short>();
    }
    
    @Override
    protected long getContentSize() {
        int size = 4;
        if ((this.getFlags() & 0x1) == 0x1) {
            size += 8;
        }
        size += 5;
        size += this.sampleInfoSizes.size();
        return size;
    }
    
    @Override
    protected void getContent(final ByteBuffer os) throws IOException {
        this.writeVersionAndFlags(os);
        if ((this.getFlags() & 0x1) == 0x1) {
            os.put(IsoFile.fourCCtoBytes(this.auxInfoType));
            os.put(IsoFile.fourCCtoBytes(this.auxInfoTypeParameter));
        }
        IsoTypeWriter.writeUInt8(os, this.defaultSampleInfoSize);
        if (this.defaultSampleInfoSize == 0) {
            IsoTypeWriter.writeUInt32(os, this.sampleInfoSizes.size());
            for (final short sampleInfoSize : this.sampleInfoSizes) {
                IsoTypeWriter.writeUInt8(os, sampleInfoSize);
            }
        }
        else {
            IsoTypeWriter.writeUInt32(os, this.sampleCount);
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if ((this.getFlags() & 0x1) == 0x1) {
            this.auxInfoType = IsoTypeReader.read4cc(content);
            this.auxInfoTypeParameter = IsoTypeReader.read4cc(content);
        }
        this.defaultSampleInfoSize = (short)IsoTypeReader.readUInt8(content);
        final int sampleCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.sampleInfoSizes.clear();
        for (int i = 0; i < sampleCount; ++i) {
            this.sampleInfoSizes.add((short)IsoTypeReader.readUInt8(content));
        }
    }
    
    public String getAuxInfoType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_0, this, this));
        return this.auxInfoType;
    }
    
    public void setAuxInfoType(final String auxInfoType) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_1, this, this, auxInfoType));
        this.auxInfoType = auxInfoType;
    }
    
    public String getAuxInfoTypeParameter() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_2, this, this));
        return this.auxInfoTypeParameter;
    }
    
    public void setAuxInfoTypeParameter(final String auxInfoTypeParameter) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_3, this, this, auxInfoTypeParameter));
        this.auxInfoTypeParameter = auxInfoTypeParameter;
    }
    
    public int getDefaultSampleInfoSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_4, this, this));
        return this.defaultSampleInfoSize;
    }
    
    public void setDefaultSampleInfoSize(final int defaultSampleInfoSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_5, this, this, Conversions.intObject(defaultSampleInfoSize)));
        assert defaultSampleInfoSize <= 255;
        assert defaultSampleInfoSize > 0;
        this.defaultSampleInfoSize = defaultSampleInfoSize;
    }
    
    public List<Short> getSampleInfoSizes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_6, this, this));
        return this.sampleInfoSizes;
    }
    
    public void setSampleInfoSizes(final List<Short> sampleInfoSizes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_7, this, this, sampleInfoSizes));
        this.sampleInfoSizes = sampleInfoSizes;
    }
    
    public int getSampleCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_8, this, this));
        return this.sampleCount;
    }
    
    public void setSampleCount(final int sampleCount) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleAuxiliaryInformationSizesBox.ajc$tjp_9, this, this, Conversions.intObject(sampleCount)));
        this.sampleCount = sampleCount;
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleAuxiliaryInformationSizesBox.java", SampleAuxiliaryInformationSizesBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAuxInfoType", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "", "", "", "java.lang.String"), 92);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAuxInfoType", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "java.lang.String", "auxInfoType", "", "void"), 96);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getAuxInfoTypeParameter", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "", "", "", "java.lang.String"), 100);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setAuxInfoTypeParameter", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "java.lang.String", "auxInfoTypeParameter", "", "void"), 104);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDefaultSampleInfoSize", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "", "", "", "int"), 108);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDefaultSampleInfoSize", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "int", "defaultSampleInfoSize", "", "void"), 112);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleInfoSizes", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "", "", "", "java.util.List"), 119);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleInfoSizes", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "java.util.List", "sampleInfoSizes", "", "void"), 123);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleCount", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "", "", "", "int"), 127);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleCount", "com.coremedia.iso.boxes.SampleAuxiliaryInformationSizesBox", "int", "sampleCount", "", "void"), 131);
    }
}
