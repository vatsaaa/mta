// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class TrackReferenceTypeBox extends AbstractBox
{
    public static final String TYPE1 = "hint";
    public static final String TYPE2 = "cdsc";
    private long[] trackIds;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public TrackReferenceTypeBox(final String type) {
        super(type);
    }
    
    public long[] getTrackIds() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackReferenceTypeBox.ajc$tjp_0, this, this));
        return this.trackIds;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        final int count = content.remaining() / 4;
        this.trackIds = new long[count];
        for (int i = 0; i < count; ++i) {
            this.trackIds[i] = IsoTypeReader.readUInt32(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        long[] trackIds;
        for (int length = (trackIds = this.trackIds).length, i = 0; i < length; ++i) {
            final long trackId = trackIds[i];
            IsoTypeWriter.writeUInt32(bb, trackId);
        }
    }
    
    @Override
    protected long getContentSize() {
        return this.trackIds.length * 4;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackReferenceTypeBox.ajc$tjp_1, this, this));
        final StringBuilder buffer = new StringBuilder();
        buffer.append("TrackReferenceTypeBox[type=").append(this.getType());
        for (int i = 0; i < this.trackIds.length; ++i) {
            buffer.append(";trackId");
            buffer.append(i);
            buffer.append("=");
            buffer.append(this.trackIds[i]);
        }
        buffer.append("]");
        return buffer.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackReferenceTypeBox.java", TrackReferenceTypeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackIds", "com.coremedia.iso.boxes.TrackReferenceTypeBox", "", "", "", "[J"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.TrackReferenceTypeBox", "", "", "", "java.lang.String"), 64);
    }
}
