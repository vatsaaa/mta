// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public abstract class AbstractFullBox extends AbstractBox implements FullBox
{
    private int version;
    private int flags;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    
    protected AbstractFullBox(final String type) {
        super(type);
    }
    
    public int getVersion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractFullBox.ajc$tjp_0, this, this));
        return this.version;
    }
    
    public void setVersion(final int version) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractFullBox.ajc$tjp_1, this, this, Conversions.intObject(version)));
        this.version = version;
    }
    
    public int getFlags() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractFullBox.ajc$tjp_2, this, this));
        return this.flags;
    }
    
    public void setFlags(final int flags) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AbstractFullBox.ajc$tjp_3, this, this, Conversions.intObject(flags)));
        this.flags = flags;
    }
    
    protected final long parseVersionAndFlags(final ByteBuffer content) {
        this.version = IsoTypeReader.readUInt8(content);
        this.flags = IsoTypeReader.readUInt24(content);
        return 4L;
    }
    
    protected final void writeVersionAndFlags(final ByteBuffer bb) {
        IsoTypeWriter.writeUInt8(bb, this.version);
        IsoTypeWriter.writeUInt24(bb, this.flags);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AbstractFullBox.java", AbstractFullBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getVersion", "com.coremedia.iso.boxes.AbstractFullBox", "", "", "", "int"), 37);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setVersion", "com.coremedia.iso.boxes.AbstractFullBox", "int", "version", "", "void"), 41);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFlags", "com.coremedia.iso.boxes.AbstractFullBox", "", "", "", "int"), 45);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setFlags", "com.coremedia.iso.boxes.AbstractFullBox", "int", "flags", "", "void"), 49);
    }
}
