// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;

public class SubtitleSampleEntry extends SampleEntry
{
    public static final String TYPE1 = "stpp";
    public static final String TYPE_ENCRYPTED = "";
    private String namespace;
    private String schemaLocation;
    private String imageMimeType;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    public SubtitleSampleEntry(final String type) {
        super(type);
    }
    
    @Override
    protected long getContentSize() {
        final long contentSize = 8 + this.namespace.length() + this.schemaLocation.length() + this.imageMimeType.length() + 3;
        return contentSize;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this._parseReservedAndDataReferenceIndex(content);
        this.namespace = IsoTypeReader.readString(content);
        this.schemaLocation = IsoTypeReader.readString(content);
        this.imageMimeType = IsoTypeReader.readString(content);
        this._parseChildBoxes(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this._writeReservedAndDataReferenceIndex(bb);
        IsoTypeWriter.writeUtf8String(bb, this.namespace);
        IsoTypeWriter.writeUtf8String(bb, this.schemaLocation);
        IsoTypeWriter.writeUtf8String(bb, this.imageMimeType);
    }
    
    public String getNamespace() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubtitleSampleEntry.ajc$tjp_0, this, this));
        return this.namespace;
    }
    
    public void setNamespace(final String namespace) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubtitleSampleEntry.ajc$tjp_1, this, this, namespace));
        this.namespace = namespace;
    }
    
    public String getSchemaLocation() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubtitleSampleEntry.ajc$tjp_2, this, this));
        return this.schemaLocation;
    }
    
    public void setSchemaLocation(final String schemaLocation) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubtitleSampleEntry.ajc$tjp_3, this, this, schemaLocation));
        this.schemaLocation = schemaLocation;
    }
    
    public String getImageMimeType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubtitleSampleEntry.ajc$tjp_4, this, this));
        return this.imageMimeType;
    }
    
    public void setImageMimeType(final String imageMimeType) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubtitleSampleEntry.ajc$tjp_5, this, this, imageMimeType));
        this.imageMimeType = imageMimeType;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SubtitleSampleEntry.java", SubtitleSampleEntry.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getNamespace", "com.coremedia.iso.boxes.sampleentry.SubtitleSampleEntry", "", "", "", "java.lang.String"), 53);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setNamespace", "com.coremedia.iso.boxes.sampleentry.SubtitleSampleEntry", "java.lang.String", "namespace", "", "void"), 57);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSchemaLocation", "com.coremedia.iso.boxes.sampleentry.SubtitleSampleEntry", "", "", "", "java.lang.String"), 61);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSchemaLocation", "com.coremedia.iso.boxes.sampleentry.SubtitleSampleEntry", "java.lang.String", "schemaLocation", "", "void"), 65);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getImageMimeType", "com.coremedia.iso.boxes.sampleentry.SubtitleSampleEntry", "", "", "", "java.lang.String"), 69);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setImageMimeType", "com.coremedia.iso.boxes.sampleentry.SubtitleSampleEntry", "java.lang.String", "imageMimeType", "", "void"), 73);
    }
}
