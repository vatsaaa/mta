// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;

public class TextSampleEntry extends SampleEntry
{
    public static final String TYPE1 = "tx3g";
    public static final String TYPE_ENCRYPTED = "enct";
    private long displayFlags;
    private int horizontalJustification;
    private int verticalJustification;
    private int[] backgroundColorRgba;
    private BoxRecord boxRecord;
    private StyleRecord styleRecord;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_18;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_19;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_20;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_21;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_22;
    
    public TextSampleEntry(final String type) {
        super(type);
        this.backgroundColorRgba = new int[4];
        this.boxRecord = new BoxRecord();
        this.styleRecord = new StyleRecord();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this._parseReservedAndDataReferenceIndex(content);
        this.displayFlags = IsoTypeReader.readUInt32(content);
        this.horizontalJustification = IsoTypeReader.readUInt8(content);
        this.verticalJustification = IsoTypeReader.readUInt8(content);
        (this.backgroundColorRgba = new int[4])[0] = IsoTypeReader.readUInt8(content);
        this.backgroundColorRgba[1] = IsoTypeReader.readUInt8(content);
        this.backgroundColorRgba[2] = IsoTypeReader.readUInt8(content);
        this.backgroundColorRgba[3] = IsoTypeReader.readUInt8(content);
        (this.boxRecord = new BoxRecord()).parse(content);
        (this.styleRecord = new StyleRecord()).parse(content);
        this._parseChildBoxes(content);
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 18L;
        contentSize += this.boxRecord.getSize();
        contentSize += this.styleRecord.getSize();
        for (final Box boxe : this.boxes) {
            contentSize += boxe.getSize();
        }
        return contentSize;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_0, this, this));
        return "TextSampleEntry";
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this._writeReservedAndDataReferenceIndex(bb);
        IsoTypeWriter.writeUInt32(bb, this.displayFlags);
        IsoTypeWriter.writeUInt8(bb, this.horizontalJustification);
        IsoTypeWriter.writeUInt8(bb, this.verticalJustification);
        IsoTypeWriter.writeUInt8(bb, this.backgroundColorRgba[0]);
        IsoTypeWriter.writeUInt8(bb, this.backgroundColorRgba[1]);
        IsoTypeWriter.writeUInt8(bb, this.backgroundColorRgba[2]);
        IsoTypeWriter.writeUInt8(bb, this.backgroundColorRgba[3]);
        this.boxRecord.getContent(bb);
        this.styleRecord.getContent(bb);
        this._writeChildBoxes(bb);
    }
    
    public BoxRecord getBoxRecord() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_1, this, this));
        return this.boxRecord;
    }
    
    public void setBoxRecord(final BoxRecord boxRecord) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_2, this, this, boxRecord));
        this.boxRecord = boxRecord;
    }
    
    public StyleRecord getStyleRecord() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_3, this, this));
        return this.styleRecord;
    }
    
    public void setStyleRecord(final StyleRecord styleRecord) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_4, this, this, styleRecord));
        this.styleRecord = styleRecord;
    }
    
    public boolean isScrollIn() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_5, this, this));
        return (this.displayFlags & 0x20L) == 0x20L;
    }
    
    public void setScrollIn(final boolean scrollIn) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_6, this, this, Conversions.booleanObject(scrollIn)));
        if (scrollIn) {
            this.displayFlags |= 0x20L;
        }
        else {
            this.displayFlags &= 0xFFFFFFFFFFFFFFDFL;
        }
    }
    
    public boolean isScrollOut() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_7, this, this));
        return (this.displayFlags & 0x40L) == 0x40L;
    }
    
    public void setScrollOut(final boolean scrollOutIn) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_8, this, this, Conversions.booleanObject(scrollOutIn)));
        if (scrollOutIn) {
            this.displayFlags |= 0x40L;
        }
        else {
            this.displayFlags &= 0xFFFFFFFFFFFFFFBFL;
        }
    }
    
    public boolean isScrollDirection() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_9, this, this));
        return (this.displayFlags & 0x180L) == 0x180L;
    }
    
    public void setScrollDirection(final boolean scrollOutIn) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_10, this, this, Conversions.booleanObject(scrollOutIn)));
        if (scrollOutIn) {
            this.displayFlags |= 0x180L;
        }
        else {
            this.displayFlags &= 0xFFFFFFFFFFFFFE7FL;
        }
    }
    
    public boolean isContinuousKaraoke() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_11, this, this));
        return (this.displayFlags & 0x800L) == 0x800L;
    }
    
    public void setContinuousKaraoke(final boolean continuousKaraoke) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_12, this, this, Conversions.booleanObject(continuousKaraoke)));
        if (continuousKaraoke) {
            this.displayFlags |= 0x800L;
        }
        else {
            this.displayFlags &= 0xFFFFFFFFFFFFF7FFL;
        }
    }
    
    public boolean isWriteTextVertically() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_13, this, this));
        return (this.displayFlags & 0x20000L) == 0x20000L;
    }
    
    public void setWriteTextVertically(final boolean writeTextVertically) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_14, this, this, Conversions.booleanObject(writeTextVertically)));
        if (writeTextVertically) {
            this.displayFlags |= 0x20000L;
        }
        else {
            this.displayFlags &= 0xFFFFFFFFFFFDFFFFL;
        }
    }
    
    public boolean isFillTextRegion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_15, this, this));
        return (this.displayFlags & 0x40000L) == 0x40000L;
    }
    
    public void setFillTextRegion(final boolean fillTextRegion) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_16, this, this, Conversions.booleanObject(fillTextRegion)));
        if (fillTextRegion) {
            this.displayFlags |= 0x40000L;
        }
        else {
            this.displayFlags &= 0xFFFFFFFFFFFBFFFFL;
        }
    }
    
    public int getHorizontalJustification() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_17, this, this));
        return this.horizontalJustification;
    }
    
    public void setHorizontalJustification(final int horizontalJustification) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_18, this, this, Conversions.intObject(horizontalJustification)));
        this.horizontalJustification = horizontalJustification;
    }
    
    public int getVerticalJustification() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_19, this, this));
        return this.verticalJustification;
    }
    
    public void setVerticalJustification(final int verticalJustification) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_20, this, this, Conversions.intObject(verticalJustification)));
        this.verticalJustification = verticalJustification;
    }
    
    public int[] getBackgroundColorRgba() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_21, this, this));
        return this.backgroundColorRgba;
    }
    
    public void setBackgroundColorRgba(final int[] backgroundColorRgba) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TextSampleEntry.ajc$tjp_22, this, this, backgroundColorRgba));
        this.backgroundColorRgba = backgroundColorRgba;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TextSampleEntry.java", TextSampleEntry.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "java.lang.String"), 87);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxRecord", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry$BoxRecord"), 107);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setScrollDirection", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "boolean", "scrollOutIn", "", "void"), 151);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isContinuousKaraoke", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "boolean"), 159);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setContinuousKaraoke", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "boolean", "continuousKaraoke", "", "void"), 163);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isWriteTextVertically", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "boolean"), 171);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setWriteTextVertically", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "boolean", "writeTextVertically", "", "void"), 175);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isFillTextRegion", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "boolean"), 184);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setFillTextRegion", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "boolean", "fillTextRegion", "", "void"), 188);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHorizontalJustification", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "int"), 197);
        ajc$tjp_18 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setHorizontalJustification", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "int", "horizontalJustification", "", "void"), 201);
        ajc$tjp_19 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getVerticalJustification", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "int"), 205);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBoxRecord", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry$BoxRecord", "boxRecord", "", "void"), 111);
        ajc$tjp_20 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setVerticalJustification", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "int", "verticalJustification", "", "void"), 209);
        ajc$tjp_21 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBackgroundColorRgba", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "[I"), 213);
        ajc$tjp_22 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBackgroundColorRgba", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "[I", "backgroundColorRgba", "", "void"), 217);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getStyleRecord", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry$StyleRecord"), 115);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setStyleRecord", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry$StyleRecord", "styleRecord", "", "void"), 119);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isScrollIn", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "boolean"), 123);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setScrollIn", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "boolean", "scrollIn", "", "void"), 127);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isScrollOut", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "boolean"), 135);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setScrollOut", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "boolean", "scrollOutIn", "", "void"), 139);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "isScrollDirection", "com.coremedia.iso.boxes.sampleentry.TextSampleEntry", "", "", "", "boolean"), 147);
    }
    
    public static class BoxRecord
    {
        int top;
        int left;
        int bottom;
        int right;
        
        public void parse(final ByteBuffer in) {
            this.top = IsoTypeReader.readUInt16(in);
            this.left = IsoTypeReader.readUInt16(in);
            this.bottom = IsoTypeReader.readUInt16(in);
            this.right = IsoTypeReader.readUInt16(in);
        }
        
        public void getContent(final ByteBuffer bb) throws IOException {
            IsoTypeWriter.writeUInt16(bb, this.top);
            IsoTypeWriter.writeUInt16(bb, this.left);
            IsoTypeWriter.writeUInt16(bb, this.bottom);
            IsoTypeWriter.writeUInt16(bb, this.right);
        }
        
        public int getSize() {
            return 8;
        }
    }
    
    public static class StyleRecord
    {
        int startChar;
        int endChar;
        int fontId;
        int faceStyleFlags;
        int fontSize;
        int[] textColor;
        
        public StyleRecord() {
            this.textColor = new int[] { 255, 255, 255, 255 };
        }
        
        public void parse(final ByteBuffer in) {
            this.startChar = IsoTypeReader.readUInt16(in);
            this.endChar = IsoTypeReader.readUInt16(in);
            this.fontId = IsoTypeReader.readUInt16(in);
            this.faceStyleFlags = IsoTypeReader.readUInt8(in);
            this.fontSize = IsoTypeReader.readUInt8(in);
            (this.textColor = new int[4])[0] = IsoTypeReader.readUInt8(in);
            this.textColor[1] = IsoTypeReader.readUInt8(in);
            this.textColor[2] = IsoTypeReader.readUInt8(in);
            this.textColor[3] = IsoTypeReader.readUInt8(in);
        }
        
        public void getContent(final ByteBuffer bb) throws IOException {
            IsoTypeWriter.writeUInt16(bb, this.startChar);
            IsoTypeWriter.writeUInt16(bb, this.endChar);
            IsoTypeWriter.writeUInt16(bb, this.fontId);
            IsoTypeWriter.writeUInt8(bb, this.faceStyleFlags);
            IsoTypeWriter.writeUInt8(bb, this.fontSize);
            IsoTypeWriter.writeUInt8(bb, this.textColor[0]);
            IsoTypeWriter.writeUInt8(bb, this.textColor[1]);
            IsoTypeWriter.writeUInt8(bb, this.textColor[2]);
            IsoTypeWriter.writeUInt8(bb, this.textColor[3]);
        }
        
        public int getSize() {
            return 12;
        }
    }
}
