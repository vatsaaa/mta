// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.BoxParser;
import com.coremedia.iso.boxes.ContainerBox;

public class AudioSampleEntry extends SampleEntry implements ContainerBox
{
    public static final String TYPE1 = "samr";
    public static final String TYPE2 = "sawb";
    public static final String TYPE3 = "mp4a";
    public static final String TYPE4 = "drms";
    public static final String TYPE5 = "alac";
    public static final String TYPE7 = "owma";
    public static final String TYPE8 = "ac-3";
    public static final String TYPE9 = "ec-3";
    public static final String TYPE10 = "mlpa";
    public static final String TYPE_ENCRYPTED = "enca";
    private int channelCount;
    private int sampleSize;
    private long sampleRate;
    private int soundVersion;
    private int compressionId;
    private int packetSize;
    private long samplesPerPacket;
    private long bytesPerPacket;
    private long bytesPerFrame;
    private long bytesPerSample;
    private int reserved1;
    private long reserved2;
    private byte[] soundVersion2Data;
    private BoxParser boxParser;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_17;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_18;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_19;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_20;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_21;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_22;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_23;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_24;
    
    public AudioSampleEntry(final String type) {
        super(type);
    }
    
    public int getChannelCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_0, this, this));
        return this.channelCount;
    }
    
    public int getSampleSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_1, this, this));
        return this.sampleSize;
    }
    
    public long getSampleRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_2, this, this));
        return this.sampleRate;
    }
    
    public int getSoundVersion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_3, this, this));
        return this.soundVersion;
    }
    
    public int getCompressionId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_4, this, this));
        return this.compressionId;
    }
    
    public int getPacketSize() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_5, this, this));
        return this.packetSize;
    }
    
    public long getSamplesPerPacket() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_6, this, this));
        return this.samplesPerPacket;
    }
    
    public long getBytesPerPacket() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_7, this, this));
        return this.bytesPerPacket;
    }
    
    public long getBytesPerFrame() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_8, this, this));
        return this.bytesPerFrame;
    }
    
    public long getBytesPerSample() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_9, this, this));
        return this.bytesPerSample;
    }
    
    public void setChannelCount(final int channelCount) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_10, this, this, Conversions.intObject(channelCount)));
        this.channelCount = channelCount;
    }
    
    public void setSampleSize(final int sampleSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_11, this, this, Conversions.intObject(sampleSize)));
        this.sampleSize = sampleSize;
    }
    
    public void setSampleRate(final long sampleRate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_12, this, this, Conversions.longObject(sampleRate)));
        this.sampleRate = sampleRate;
    }
    
    public void setSoundVersion(final int soundVersion) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_13, this, this, Conversions.intObject(soundVersion)));
        this.soundVersion = soundVersion;
    }
    
    public void setCompressionId(final int compressionId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_14, this, this, Conversions.intObject(compressionId)));
        this.compressionId = compressionId;
    }
    
    public void setPacketSize(final int packetSize) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_15, this, this, Conversions.intObject(packetSize)));
        this.packetSize = packetSize;
    }
    
    public void setSamplesPerPacket(final long samplesPerPacket) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_16, this, this, Conversions.longObject(samplesPerPacket)));
        this.samplesPerPacket = samplesPerPacket;
    }
    
    public void setBytesPerPacket(final long bytesPerPacket) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_17, this, this, Conversions.longObject(bytesPerPacket)));
        this.bytesPerPacket = bytesPerPacket;
    }
    
    public void setBytesPerFrame(final long bytesPerFrame) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_18, this, this, Conversions.longObject(bytesPerFrame)));
        this.bytesPerFrame = bytesPerFrame;
    }
    
    public void setBytesPerSample(final long bytesPerSample) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_19, this, this, Conversions.longObject(bytesPerSample)));
        this.bytesPerSample = bytesPerSample;
    }
    
    public void setReserved1(final int reserved1) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_20, this, this, Conversions.intObject(reserved1)));
        this.reserved1 = reserved1;
    }
    
    public void setReserved2(final long reserved2) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_21, this, this, Conversions.longObject(reserved2)));
        this.reserved2 = reserved2;
    }
    
    public void setSoundVersion2Data(final byte[] soundVersion2Data) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_22, this, this, soundVersion2Data));
        this.soundVersion2Data = soundVersion2Data;
    }
    
    public void setBoxParser(final BoxParser boxParser) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_23, this, this, boxParser));
        this.boxParser = boxParser;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this._parseReservedAndDataReferenceIndex(content);
        this.soundVersion = IsoTypeReader.readUInt16(content);
        this.reserved1 = IsoTypeReader.readUInt16(content);
        this.reserved2 = IsoTypeReader.readUInt32(content);
        this.channelCount = IsoTypeReader.readUInt16(content);
        this.sampleSize = IsoTypeReader.readUInt16(content);
        this.compressionId = IsoTypeReader.readUInt16(content);
        this.packetSize = IsoTypeReader.readUInt16(content);
        this.sampleRate = IsoTypeReader.readUInt32(content);
        if (!this.type.equals("mlpa")) {
            this.sampleRate >>>= 16;
        }
        if (this.soundVersion > 0) {
            this.samplesPerPacket = IsoTypeReader.readUInt32(content);
            this.bytesPerPacket = IsoTypeReader.readUInt32(content);
            this.bytesPerFrame = IsoTypeReader.readUInt32(content);
            this.bytesPerSample = IsoTypeReader.readUInt32(content);
        }
        if (this.soundVersion == 2) {
            this.soundVersion2Data = new byte[20];
            content.get(20);
        }
        this._parseChildBoxes(content);
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 28L;
        contentSize += ((this.soundVersion > 0) ? 16 : 0);
        contentSize += ((this.soundVersion == 2) ? 20 : 0);
        for (final Box boxe : this.boxes) {
            contentSize += boxe.getSize();
        }
        return contentSize;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AudioSampleEntry.ajc$tjp_24, this, this));
        return "AudioSampleEntry";
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this._writeReservedAndDataReferenceIndex(bb);
        IsoTypeWriter.writeUInt16(bb, this.soundVersion);
        IsoTypeWriter.writeUInt16(bb, this.reserved1);
        IsoTypeWriter.writeUInt32(bb, this.reserved2);
        IsoTypeWriter.writeUInt16(bb, this.channelCount);
        IsoTypeWriter.writeUInt16(bb, this.sampleSize);
        IsoTypeWriter.writeUInt16(bb, this.compressionId);
        IsoTypeWriter.writeUInt16(bb, this.packetSize);
        if (this.type.equals("mlpa")) {
            IsoTypeWriter.writeUInt32(bb, this.getSampleRate());
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.getSampleRate() << 16);
        }
        if (this.soundVersion > 0) {
            IsoTypeWriter.writeUInt32(bb, this.samplesPerPacket);
            IsoTypeWriter.writeUInt32(bb, this.bytesPerPacket);
            IsoTypeWriter.writeUInt32(bb, this.bytesPerFrame);
            IsoTypeWriter.writeUInt32(bb, this.bytesPerSample);
        }
        if (this.soundVersion == 2) {
            bb.put(this.soundVersion2Data);
        }
        this._writeChildBoxes(bb);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AudioSampleEntry.java", AudioSampleEntry.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getChannelCount", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "int"), 71);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleSize", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "int"), 75);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setChannelCount", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "int", "channelCount", "", "void"), 111);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleSize", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "int", "sampleSize", "", "void"), 115);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSampleRate", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "long", "sampleRate", "", "void"), 119);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSoundVersion", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "int", "soundVersion", "", "void"), 123);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCompressionId", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "int", "compressionId", "", "void"), 127);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setPacketSize", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "int", "packetSize", "", "void"), 131);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSamplesPerPacket", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "long", "samplesPerPacket", "", "void"), 135);
        ajc$tjp_17 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBytesPerPacket", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "long", "bytesPerPacket", "", "void"), 139);
        ajc$tjp_18 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBytesPerFrame", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "long", "bytesPerFrame", "", "void"), 143);
        ajc$tjp_19 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBytesPerSample", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "long", "bytesPerSample", "", "void"), 147);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleRate", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "long"), 79);
        ajc$tjp_20 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setReserved1", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "int", "reserved1", "", "void"), 151);
        ajc$tjp_21 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setReserved2", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "long", "reserved2", "", "void"), 155);
        ajc$tjp_22 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setSoundVersion2Data", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "[B", "soundVersion2Data", "", "void"), 159);
        ajc$tjp_23 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBoxParser", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "com.coremedia.iso.BoxParser", "boxParser", "", "void"), 163);
        ajc$tjp_24 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "java.lang.String"), 219);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSoundVersion", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "int"), 83);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCompressionId", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "int"), 87);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getPacketSize", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "int"), 91);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSamplesPerPacket", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "long"), 95);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBytesPerPacket", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "long"), 99);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBytesPerFrame", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "long"), 103);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBytesPerSample", "com.coremedia.iso.boxes.sampleentry.AudioSampleEntry", "", "", "", "long"), 107);
    }
}
