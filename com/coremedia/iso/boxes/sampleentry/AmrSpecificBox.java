// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import com.coremedia.iso.IsoFile;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.AbstractBox;

public class AmrSpecificBox extends AbstractBox
{
    public static final String TYPE = "damr";
    private String vendor;
    private int decoderVersion;
    private int modeSet;
    private int modeChangePeriod;
    private int framesPerSample;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    
    public AmrSpecificBox() {
        super("damr");
    }
    
    public String getVendor() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AmrSpecificBox.ajc$tjp_0, this, this));
        return this.vendor;
    }
    
    public int getDecoderVersion() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AmrSpecificBox.ajc$tjp_1, this, this));
        return this.decoderVersion;
    }
    
    public int getModeSet() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AmrSpecificBox.ajc$tjp_2, this, this));
        return this.modeSet;
    }
    
    public int getModeChangePeriod() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AmrSpecificBox.ajc$tjp_3, this, this));
        return this.modeChangePeriod;
    }
    
    public int getFramesPerSample() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AmrSpecificBox.ajc$tjp_4, this, this));
        return this.framesPerSample;
    }
    
    @Override
    protected long getContentSize() {
        return 9L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        final byte[] v = new byte[4];
        content.get(v);
        this.vendor = IsoFile.bytesToFourCC(v);
        this.decoderVersion = IsoTypeReader.readUInt8(content);
        this.modeSet = IsoTypeReader.readUInt16(content);
        this.modeChangePeriod = IsoTypeReader.readUInt8(content);
        this.framesPerSample = IsoTypeReader.readUInt8(content);
    }
    
    public void getContent(final ByteBuffer bb) throws IOException {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AmrSpecificBox.ajc$tjp_5, this, this, bb));
        bb.put(IsoFile.fourCCtoBytes(this.vendor));
        IsoTypeWriter.writeUInt8(bb, this.decoderVersion);
        IsoTypeWriter.writeUInt16(bb, this.modeSet);
        IsoTypeWriter.writeUInt8(bb, this.modeChangePeriod);
        IsoTypeWriter.writeUInt8(bb, this.framesPerSample);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(AmrSpecificBox.ajc$tjp_6, this, this));
        final StringBuilder buffer = new StringBuilder();
        buffer.append("AmrSpecificBox[vendor=").append(this.getVendor());
        buffer.append(";decoderVersion=").append(this.getDecoderVersion());
        buffer.append(";modeSet=").append(this.getModeSet());
        buffer.append(";modeChangePeriod=").append(this.getModeChangePeriod());
        buffer.append(";framesPerSample=").append(this.getFramesPerSample());
        buffer.append("]");
        return buffer.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("AmrSpecificBox.java", AmrSpecificBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getVendor", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "java.lang.String"), 46);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDecoderVersion", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 50);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getModeSet", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 54);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getModeChangePeriod", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 58);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFramesPerSample", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 62);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getContent", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "java.nio.ByteBuffer", "bb", "java.io.IOException", "void"), 84);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "java.lang.String"), 92);
    }
}
