// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.BoxParser;
import com.coremedia.iso.boxes.ContainerBox;

public class MpegSampleEntry extends SampleEntry implements ContainerBox
{
    private BoxParser boxParser;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public MpegSampleEntry(final String type) {
        super(type);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this._parseReservedAndDataReferenceIndex(content);
        this._parseChildBoxes(content);
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 8L;
        for (final Box boxe : this.boxes) {
            contentSize += boxe.getSize();
        }
        return contentSize;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MpegSampleEntry.ajc$tjp_0, this, this));
        return "MpegSampleEntry" + Arrays.asList(this.getBoxes());
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this._writeReservedAndDataReferenceIndex(bb);
        this._writeChildBoxes(bb);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MpegSampleEntry.java", MpegSampleEntry.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.sampleentry.MpegSampleEntry", "", "", "", "java.lang.String"), 35);
    }
}
