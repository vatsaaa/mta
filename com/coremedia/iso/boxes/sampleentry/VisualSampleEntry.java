// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.Utf8;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.boxes.ContainerBox;

public class VisualSampleEntry extends SampleEntry implements ContainerBox
{
    public static final String TYPE1 = "mp4v";
    public static final String TYPE2 = "s263";
    public static final String TYPE3 = "avc1";
    public static final String TYPE_ENCRYPTED = "encv";
    private int width;
    private int height;
    private double horizresolution;
    private double vertresolution;
    private int frameCount;
    private String compressorname;
    private int depth;
    private long[] predefined;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    
    static {
        ajc$preClinit();
    }
    
    public VisualSampleEntry(final String type) {
        super(type);
        this.predefined = new long[3];
    }
    
    public int getWidth() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_0, this, this));
        return this.width;
    }
    
    public int getHeight() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_1, this, this));
        return this.height;
    }
    
    public double getHorizresolution() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_2, this, this));
        return this.horizresolution;
    }
    
    public double getVertresolution() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_3, this, this));
        return this.vertresolution;
    }
    
    public int getFrameCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_4, this, this));
        return this.frameCount;
    }
    
    public String getCompressorname() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_5, this, this));
        return this.compressorname;
    }
    
    public int getDepth() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_6, this, this));
        return this.depth;
    }
    
    public void setCompressorname(final String compressorname) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(VisualSampleEntry.ajc$tjp_7, this, this, compressorname));
        this.compressorname = compressorname;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this._parseReservedAndDataReferenceIndex(content);
        long tmp = IsoTypeReader.readUInt16(content);
        assert 0L == tmp : "reserved byte not 0";
        tmp = IsoTypeReader.readUInt16(content);
        assert 0L == tmp : "reserved byte not 0";
        this.predefined[0] = IsoTypeReader.readUInt32(content);
        this.predefined[1] = IsoTypeReader.readUInt32(content);
        this.predefined[2] = IsoTypeReader.readUInt32(content);
        this.width = IsoTypeReader.readUInt16(content);
        this.height = IsoTypeReader.readUInt16(content);
        this.horizresolution = IsoTypeReader.readFixedPoint1616(content);
        this.vertresolution = IsoTypeReader.readFixedPoint1616(content);
        tmp = IsoTypeReader.readUInt32(content);
        assert 0L == tmp : "reserved byte not 0";
        this.frameCount = IsoTypeReader.readUInt16(content);
        int compressornameDisplayAbleData = IsoTypeReader.readUInt8(content);
        if (compressornameDisplayAbleData > 31) {
            System.out.println("invalid compressor name displayable data: " + compressornameDisplayAbleData);
            compressornameDisplayAbleData = 31;
        }
        final byte[] bytes = new byte[compressornameDisplayAbleData];
        content.get(bytes);
        this.compressorname = Utf8.convert(bytes);
        if (compressornameDisplayAbleData < 31) {
            final byte[] zeros = new byte[31 - compressornameDisplayAbleData];
            content.get(zeros);
        }
        this.depth = IsoTypeReader.readUInt16(content);
        tmp = IsoTypeReader.readUInt16(content);
        assert 65535L == tmp;
        this._parseChildBoxes(content);
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 78L;
        for (final Box boxe : this.boxes) {
            contentSize += boxe.getSize();
        }
        return contentSize;
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this._writeReservedAndDataReferenceIndex(bb);
        IsoTypeWriter.writeUInt16(bb, 0);
        IsoTypeWriter.writeUInt16(bb, 0);
        IsoTypeWriter.writeUInt32(bb, this.predefined[0]);
        IsoTypeWriter.writeUInt32(bb, this.predefined[1]);
        IsoTypeWriter.writeUInt32(bb, this.predefined[2]);
        IsoTypeWriter.writeUInt16(bb, this.getWidth());
        IsoTypeWriter.writeUInt16(bb, this.getHeight());
        IsoTypeWriter.writeFixedPont1616(bb, this.getHorizresolution());
        IsoTypeWriter.writeFixedPont1616(bb, this.getVertresolution());
        IsoTypeWriter.writeUInt32(bb, 0L);
        IsoTypeWriter.writeUInt16(bb, this.getFrameCount());
        IsoTypeWriter.writeUInt8(bb, Utf8.utf8StringLengthInBytes(this.getCompressorname()));
        bb.put(Utf8.convert(this.getCompressorname()));
        int a = Utf8.utf8StringLengthInBytes(this.getCompressorname());
        while (a < 31) {
            ++a;
            bb.put((byte)0);
        }
        IsoTypeWriter.writeUInt16(bb, this.getDepth());
        IsoTypeWriter.writeUInt16(bb, 65535);
        this._writeChildBoxes(bb);
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("VisualSampleEntry.java", VisualSampleEntry.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getWidth", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "", "", "", "int"), 79);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHeight", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "", "", "", "int"), 83);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHorizresolution", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "", "", "", "double"), 87);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getVertresolution", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "", "", "", "double"), 91);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getFrameCount", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "", "", "", "int"), 95);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCompressorname", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "", "", "", "java.lang.String"), 99);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDepth", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "", "", "", "int"), 103);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCompressorname", "com.coremedia.iso.boxes.sampleentry.VisualSampleEntry", "java.lang.String", "compressorname", "", "void"), 107);
    }
}
