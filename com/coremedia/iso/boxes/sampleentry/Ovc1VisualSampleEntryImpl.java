// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;

public class Ovc1VisualSampleEntryImpl extends SampleEntry
{
    private byte[] vc1Content;
    public static final String TYPE = "ovc1";
    
    @Override
    protected long getContentSize() {
        long size = 8L;
        for (final Box box : this.boxes) {
            size += box.getSize();
        }
        size += this.vc1Content.length;
        return size;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this._parseReservedAndDataReferenceIndex(content);
        content.get(this.vc1Content = new byte[content.remaining()]);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(new byte[6]);
        IsoTypeWriter.writeUInt16(bb, this.getDataReferenceIndex());
        bb.put(this.vc1Content);
    }
    
    protected Ovc1VisualSampleEntryImpl() {
        super("ovc1");
    }
}
