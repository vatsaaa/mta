// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes.sampleentry;

import org.aspectj.lang.Signature;
import java.nio.channels.WritableByteChannel;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.io.ByteArrayOutputStream;
import com.coremedia.iso.IsoTypeWriter;
import com.googlecode.mp4parser.ByteBufferByteChannel;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.LinkedList;
import org.aspectj.lang.JoinPoint;
import com.coremedia.iso.BoxParser;
import com.coremedia.iso.boxes.Box;
import java.util.List;
import com.coremedia.iso.boxes.ContainerBox;
import com.coremedia.iso.boxes.AbstractBox;

public abstract class SampleEntry extends AbstractBox implements ContainerBox
{
    private int dataReferenceIndex;
    protected List<Box> boxes;
    private BoxParser boxParser;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    
    protected SampleEntry(final String type) {
        super(type);
        this.boxes = new LinkedList<Box>();
    }
    
    public void setType(final String type) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_0, this, this, type));
        this.type = type;
    }
    
    public int getDataReferenceIndex() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_1, this, this));
        return this.dataReferenceIndex;
    }
    
    public void setDataReferenceIndex(final int dataReferenceIndex) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_2, this, this, Conversions.intObject(dataReferenceIndex)));
        this.dataReferenceIndex = dataReferenceIndex;
    }
    
    public void setBoxes(final List<Box> boxes) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_3, this, this, boxes));
        this.boxes = new LinkedList<Box>(boxes);
    }
    
    public void addBox(final AbstractBox b) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_4, this, this, b));
        this.boxes.add(b);
    }
    
    public boolean removeBox(final Box b) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_5, this, this, b));
        return this.boxes.remove(b);
    }
    
    public List<Box> getBoxes() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_6, this, this));
        return this.boxes;
    }
    
    public <T extends Box> List<T> getBoxes(final Class<T> clazz, final boolean recursive) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_7, this, this, clazz, Conversions.booleanObject(recursive)));
        final List boxesToBeReturned = new ArrayList(2);
        for (final Box boxe : this.boxes) {
            if (clazz == boxe.getClass()) {
                boxesToBeReturned.add(boxe);
            }
            if (recursive && boxe instanceof ContainerBox) {
                boxesToBeReturned.addAll(((ContainerBox)boxe).getBoxes(clazz, recursive));
            }
        }
        return (List<T>)boxesToBeReturned;
    }
    
    public <T extends Box> List<T> getBoxes(final Class<T> clazz) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_8, this, this, clazz));
        return this.getBoxes(clazz, false);
    }
    
    @Override
    public void parse(final ReadableByteChannel in, final ByteBuffer header, final long contentSize, final BoxParser boxParser) throws IOException {
        super.parse(in, header, contentSize, boxParser);
        this.boxParser = boxParser;
    }
    
    public void _parseReservedAndDataReferenceIndex(final ByteBuffer content) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_9, this, this, content));
        content.get(new byte[6]);
        this.dataReferenceIndex = IsoTypeReader.readUInt16(content);
    }
    
    public void _parseChildBoxes(final ByteBuffer content) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_10, this, this, content));
        while (content.remaining() > 8) {
            try {
                this.boxes.add(this.boxParser.parseBox(new ByteBufferByteChannel(content), this));
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        this.deadBytes = content.slice();
    }
    
    public void _writeReservedAndDataReferenceIndex(final ByteBuffer bb) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_11, this, this, bb));
        bb.put(new byte[6]);
        IsoTypeWriter.writeUInt16(bb, this.dataReferenceIndex);
    }
    
    public void _writeChildBoxes(final ByteBuffer bb) throws IOException {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleEntry.ajc$tjp_12, this, this, bb));
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final WritableByteChannel wbc = Channels.newChannel(baos);
        for (final Box box : this.boxes) {
            box.getBox(wbc);
        }
        wbc.close();
        bb.put(baos.toByteArray());
    }
    
    public long getNumOfBytesToFirstChild() {
        long sizeOfChildren = 0L;
        for (final Box box : this.boxes) {
            sizeOfChildren += box.getSize();
        }
        return this.getSize() - sizeOfChildren;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleEntry.java", SampleEntry.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setType", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.lang.String", "type", "", "void"), 56);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDataReferenceIndex", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "", "", "", "int"), 60);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "_parseChildBoxes", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.nio.ByteBuffer", "content", "", "void"), 118);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "_writeReservedAndDataReferenceIndex", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.nio.ByteBuffer", "bb", "", "void"), 130);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "_writeChildBoxes", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.nio.ByteBuffer", "bb", "java.io.IOException", "void"), 135);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDataReferenceIndex", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "int", "dataReferenceIndex", "", "void"), 64);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setBoxes", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.util.List", "boxes", "", "void"), 68);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "addBox", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "com.coremedia.iso.boxes.AbstractBox", "b", "", "void"), 72);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "removeBox", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "com.coremedia.iso.boxes.Box", "b", "", "boolean"), 76);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "", "", "", "java.util.List"), 80);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.lang.Class:boolean", "clazz:recursive", "", "java.util.List"), 85);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getBoxes", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.lang.Class", "clazz", "", "java.util.List"), 102);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "_parseReservedAndDataReferenceIndex", "com.coremedia.iso.boxes.sampleentry.SampleEntry", "java.nio.ByteBuffer", "content", "", "void"), 113);
    }
}
