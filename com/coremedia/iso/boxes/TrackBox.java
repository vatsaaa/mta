// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.util.Iterator;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class TrackBox extends AbstractContainerBox
{
    public static final String TYPE = "trak";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public TrackBox() {
        super("trak");
    }
    
    public TrackHeaderBox getTrackHeaderBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackBox.ajc$tjp_0, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof TrackHeaderBox) {
                return (TrackHeaderBox)box;
            }
        }
        return null;
    }
    
    public SampleTableBox getSampleTableBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackBox.ajc$tjp_1, this, this));
        final MediaBox mdia = this.getMediaBox();
        if (mdia != null) {
            final MediaInformationBox minf = mdia.getMediaInformationBox();
            if (minf != null) {
                return minf.getSampleTableBox();
            }
        }
        return null;
    }
    
    public MediaBox getMediaBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(TrackBox.ajc$tjp_2, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof MediaBox) {
                return (MediaBox)box;
            }
        }
        return null;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("TrackBox.java", TrackBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackHeaderBox", "com.coremedia.iso.boxes.TrackBox", "", "", "", "com.coremedia.iso.boxes.TrackHeaderBox"), 33);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleTableBox", "com.coremedia.iso.boxes.TrackBox", "", "", "", "com.coremedia.iso.boxes.SampleTableBox"), 47);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMediaBox", "com.coremedia.iso.boxes.TrackBox", "", "", "", "com.coremedia.iso.boxes.MediaBox"), 60);
    }
}
