// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;

public class DataReferenceBox extends FullContainerBox
{
    public static final String TYPE = "dref";
    
    public DataReferenceBox() {
        super("dref");
    }
    
    @Override
    protected long getContentSize() {
        return super.getContentSize() + 4L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        content.get(new byte[4]);
        this.parseChildBoxes(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.getBoxes().size());
        this.writeChildBoxes(bb);
    }
}
