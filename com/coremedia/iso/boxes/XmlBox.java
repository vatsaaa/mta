// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class XmlBox extends AbstractFullBox
{
    String xml;
    public static final String TYPE = "xml ";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public XmlBox() {
        super("xml ");
        this.xml = "";
    }
    
    public String getXml() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(XmlBox.ajc$tjp_0, this, this));
        return this.xml;
    }
    
    public void setXml(final String xml) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(XmlBox.ajc$tjp_1, this, this, xml));
        this.xml = xml;
    }
    
    @Override
    protected long getContentSize() {
        return 4 + Utf8.utf8StringLengthInBytes(this.xml);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.xml = IsoTypeReader.readString(content, content.remaining());
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(Utf8.convert(this.xml));
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("XmlBox.java", XmlBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getXml", "com.coremedia.iso.boxes.XmlBox", "", "", "", "java.lang.String"), 20);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setXml", "com.coremedia.iso.boxes.XmlBox", "java.lang.String", "xml", "", "void"), 24);
    }
}
