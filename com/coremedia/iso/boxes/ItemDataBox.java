// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;
import java.nio.ByteBuffer;

public class ItemDataBox extends AbstractBox
{
    ByteBuffer data;
    public static final String TYPE = "idat";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public ItemDataBox() {
        super("idat");
        this.data = ByteBuffer.allocate(0);
    }
    
    public ByteBuffer getData() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemDataBox.ajc$tjp_0, this, this));
        return this.data;
    }
    
    public void setData(final ByteBuffer data) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ItemDataBox.ajc$tjp_1, this, this, data));
        this.data = data;
    }
    
    @Override
    protected long getContentSize() {
        return this.data.limit();
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.data = content.slice();
        content.position(content.position() + content.remaining());
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(this.data);
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("ItemDataBox.java", ItemDataBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getData", "com.coremedia.iso.boxes.ItemDataBox", "", "", "", "java.nio.ByteBuffer"), 18);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setData", "com.coremedia.iso.boxes.ItemDataBox", "java.nio.ByteBuffer", "data", "", "void"), 22);
    }
}
