// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class ChunkOffset64BitBox extends ChunkOffsetBox
{
    public static final String TYPE = "co64";
    private long[] chunkOffsets;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public ChunkOffset64BitBox() {
        super("co64");
    }
    
    @Override
    public long[] getChunkOffsets() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(ChunkOffset64BitBox.ajc$tjp_0, this, this));
        return this.chunkOffsets;
    }
    
    @Override
    protected long getContentSize() {
        return 8 + 8 * this.chunkOffsets.length;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final int entryCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.chunkOffsets = new long[entryCount];
        for (int i = 0; i < entryCount; ++i) {
            this.chunkOffsets[i] = IsoTypeReader.readUInt64(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.chunkOffsets.length);
        long[] chunkOffsets;
        for (int length = (chunkOffsets = this.chunkOffsets).length, i = 0; i < length; ++i) {
            final long chunkOffset = chunkOffsets[i];
            IsoTypeWriter.writeUInt64(bb, chunkOffset);
        }
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("ChunkOffset64BitBox.java", ChunkOffset64BitBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getChunkOffsets", "com.coremedia.iso.boxes.ChunkOffset64BitBox", "", "", "", "[J"), 23);
    }
}
