// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.util.Iterator;
import java.util.List;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class MovieBox extends AbstractContainerBox
{
    public static final String TYPE = "moov";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public MovieBox() {
        super("moov");
    }
    
    public int getTrackCount() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieBox.ajc$tjp_0, this, this));
        return this.getBoxes(TrackBox.class).size();
    }
    
    public long[] getTrackNumbers() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieBox.ajc$tjp_1, this, this));
        final List trackBoxes = this.getBoxes(TrackBox.class);
        final long[] trackNumbers = new long[trackBoxes.size()];
        for (int trackCounter = 0; trackCounter < trackBoxes.size(); ++trackCounter) {
            final AbstractBox trackBoxe = trackBoxes.get(trackCounter);
            final TrackBox trackBox = (TrackBox)trackBoxe;
            trackNumbers[trackCounter] = trackBox.getTrackHeaderBox().getTrackId();
        }
        return trackNumbers;
    }
    
    public MovieHeaderBox getMovieHeaderBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieBox.ajc$tjp_2, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof MovieHeaderBox) {
                return (MovieHeaderBox)box;
            }
        }
        return null;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MovieBox.java", MovieBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackCount", "com.coremedia.iso.boxes.MovieBox", "", "", "", "int"), 33);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTrackNumbers", "com.coremedia.iso.boxes.MovieBox", "", "", "", "[J"), 43);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMovieHeaderBox", "com.coremedia.iso.boxes.MovieBox", "", "", "", "com.coremedia.iso.boxes.MovieHeaderBox"), 55);
    }
}
