// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.IsoTypeReader;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class SampleDependencyTypeBox extends AbstractFullBox
{
    public static final String TYPE = "sdtp";
    private List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public SampleDependencyTypeBox() {
        super("sdtp");
        this.entries = new ArrayList<Entry>();
    }
    
    @Override
    protected long getContentSize() {
        return 4 + this.entries.size();
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        for (final Entry entry : this.entries) {
            IsoTypeWriter.writeUInt8(bb, entry.value);
        }
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        while (content.remaining() > 0) {
            this.entries.add(new Entry(IsoTypeReader.readUInt8(content)));
        }
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleDependencyTypeBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleDependencyTypeBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleDependencyTypeBox.ajc$tjp_2, this, this));
        final StringBuilder sb = new StringBuilder();
        sb.append("SampleDependencyTypeBox");
        sb.append("{entries=").append(this.entries);
        sb.append('}');
        return sb.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleDependencyTypeBox.java", SampleDependencyTypeBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.SampleDependencyTypeBox", "", "", "", "java.util.List"), 120);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.SampleDependencyTypeBox", "java.util.List", "entries", "", "void"), 124);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.SampleDependencyTypeBox", "", "", "", "java.lang.String"), 129);
    }
    
    public static class Entry
    {
        private int value;
        
        public Entry(final int value) {
            this.value = value;
        }
        
        public int getReserved() {
            return this.value >> 6 & 0x3;
        }
        
        public void setReserved(final int res) {
            this.value = ((res & 0x3) << 6 | (this.value & 0x3F));
        }
        
        public int getSampleDependsOn() {
            return this.value >> 4 & 0x3;
        }
        
        public void setSampleDependsOn(final int sdo) {
            this.value = ((sdo & 0x3) << 4 | (this.value & 0xCF));
        }
        
        public int getSampleIsDependentOn() {
            return this.value >> 2 & 0x3;
        }
        
        public void setSampleIsDependentOn(final int sido) {
            this.value = ((sido & 0x3) << 2 | (this.value & 0xF3));
        }
        
        public int getSampleHasRedundancy() {
            return this.value & 0x3;
        }
        
        public void setSampleHasRedundancy(final int shr) {
            this.value = ((shr & 0x3) | (this.value & 0xFC));
        }
        
        @Override
        public String toString() {
            return "Entry{reserved=" + this.getReserved() + ", sampleDependsOn=" + this.getSampleDependsOn() + ", sampleIsDependentOn=" + this.getSampleIsDependentOn() + ", sampleHasRedundancy=" + this.getSampleHasRedundancy() + '}';
        }
    }
}
