// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.util.Iterator;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class MediaBox extends AbstractContainerBox
{
    public static final String TYPE = "mdia";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public MediaBox() {
        super("mdia");
    }
    
    public MediaInformationBox getMediaInformationBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaBox.ajc$tjp_0, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof MediaInformationBox) {
                return (MediaInformationBox)box;
            }
        }
        return null;
    }
    
    public MediaHeaderBox getMediaHeaderBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaBox.ajc$tjp_1, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof MediaHeaderBox) {
                return (MediaHeaderBox)box;
            }
        }
        return null;
    }
    
    public HandlerBox getHandlerBox() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MediaBox.ajc$tjp_2, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof HandlerBox) {
                return (HandlerBox)box;
            }
        }
        return null;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MediaBox.java", MediaBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMediaInformationBox", "com.coremedia.iso.boxes.MediaBox", "", "", "", "com.coremedia.iso.boxes.MediaInformationBox"), 31);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMediaHeaderBox", "com.coremedia.iso.boxes.MediaBox", "", "", "", "com.coremedia.iso.boxes.MediaHeaderBox"), 40);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getHandlerBox", "com.coremedia.iso.boxes.MediaBox", "", "", "", "com.coremedia.iso.boxes.HandlerBox"), 49);
    }
}
