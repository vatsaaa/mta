// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

public class CastUtils
{
    public static int l2i(final long l) {
        if (l > 2147483647L || l < -2147483648L) {
            throw new RuntimeException("A cast to int has gone wrong. Please contact the mp4parser discussion group (" + l + ")");
        }
        return (int)l;
    }
}
