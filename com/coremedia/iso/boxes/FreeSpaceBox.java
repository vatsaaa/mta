// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class FreeSpaceBox extends AbstractBox
{
    public static final String TYPE = "skip";
    byte[] data;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    @Override
    protected long getContentSize() {
        return this.data.length;
    }
    
    public FreeSpaceBox() {
        super("skip");
    }
    
    public void setData(final byte[] data) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FreeSpaceBox.ajc$tjp_0, this, this, data));
        this.data = data;
    }
    
    public byte[] getData() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FreeSpaceBox.ajc$tjp_1, this, this));
        return this.data;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        content.get(this.data = new byte[content.remaining()]);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        bb.put(this.data);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(FreeSpaceBox.ajc$tjp_2, this, this));
        return "FreeSpaceBox[size=" + this.data.length + ";type=" + this.getType() + "]";
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("FreeSpaceBox.java", FreeSpaceBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setData", "com.coremedia.iso.boxes.FreeSpaceBox", "[B", "data", "", "void"), 40);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getData", "com.coremedia.iso.boxes.FreeSpaceBox", "", "", "", "[B"), 44);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.FreeSpaceBox", "", "", "", "java.lang.String"), 59);
    }
}
