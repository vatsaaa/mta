// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.util.Iterator;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import com.coremedia.iso.boxes.sampleentry.SampleEntry;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import java.nio.ByteBuffer;
import org.aspectj.lang.JoinPoint;

public class SampleDescriptionBox extends FullContainerBox
{
    public static final String TYPE = "stsd";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    
    public SampleDescriptionBox() {
        super("stsd");
    }
    
    @Override
    protected long getContentSize() {
        return super.getContentSize() + 4L;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        content.get(new byte[4]);
        this.parseChildBoxes(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.boxes.size());
        this.writeChildBoxes(bb);
    }
    
    public SampleEntry getSampleEntry() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleDescriptionBox.ajc$tjp_0, this, this));
        for (final Box box : this.boxes) {
            if (box instanceof SampleEntry) {
                return (SampleEntry)box;
            }
        }
        return null;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleDescriptionBox.java", SampleDescriptionBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getSampleEntry", "com.coremedia.iso.boxes.SampleDescriptionBox", "", "", "", "com.coremedia.iso.boxes.sampleentry.SampleEntry"), 73);
    }
}
