// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class StaticChunkOffsetBox extends ChunkOffsetBox
{
    public static final String TYPE = "stco";
    private long[] chunkOffsets;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    
    public StaticChunkOffsetBox() {
        super("stco");
        this.chunkOffsets = new long[0];
    }
    
    @Override
    public long[] getChunkOffsets() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(StaticChunkOffsetBox.ajc$tjp_0, this, this));
        return this.chunkOffsets;
    }
    
    @Override
    protected long getContentSize() {
        return 8 + this.chunkOffsets.length * 4;
    }
    
    public void setChunkOffsets(final long[] chunkOffsets) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(StaticChunkOffsetBox.ajc$tjp_1, this, this, chunkOffsets));
        this.chunkOffsets = chunkOffsets;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        final int entryCount = CastUtils.l2i(IsoTypeReader.readUInt32(content));
        this.chunkOffsets = new long[entryCount];
        for (int i = 0; i < entryCount; ++i) {
            this.chunkOffsets[i] = IsoTypeReader.readUInt32(content);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.chunkOffsets.length);
        long[] chunkOffsets;
        for (int length = (chunkOffsets = this.chunkOffsets).length, i = 0; i < length; ++i) {
            final long chunkOffset = chunkOffsets[i];
            IsoTypeWriter.writeUInt32(bb, chunkOffset);
        }
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("StaticChunkOffsetBox.java", StaticChunkOffsetBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getChunkOffsets", "com.coremedia.iso.boxes.StaticChunkOffsetBox", "", "", "", "[J"), 39);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setChunkOffsets", "com.coremedia.iso.boxes.StaticChunkOffsetBox", "[J", "chunkOffsets", "", "void"), 47);
    }
}
