// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.Conversions;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class MovieHeaderBox extends AbstractFullBox
{
    private long creationTime;
    private long modificationTime;
    private long timescale;
    private long duration;
    private double rate;
    private float volume;
    private long[] matrix;
    private long nextTrackId;
    public static final String TYPE = "mvhd";
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_9;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_10;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_11;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_12;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_13;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_14;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_15;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_16;
    
    public MovieHeaderBox() {
        super("mvhd");
        this.rate = 1.0;
        this.volume = 1.0f;
        this.matrix = new long[] { 65536L, 0L, 0L, 0L, 65536L, 0L, 0L, 0L, 1073741824L };
    }
    
    public long getCreationTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_0, this, this));
        return this.creationTime;
    }
    
    public long getModificationTime() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_1, this, this));
        return this.modificationTime;
    }
    
    public long getTimescale() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_2, this, this));
        return this.timescale;
    }
    
    public long getDuration() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_3, this, this));
        return this.duration;
    }
    
    public double getRate() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_4, this, this));
        return this.rate;
    }
    
    public float getVolume() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_5, this, this));
        return this.volume;
    }
    
    public long[] getMatrix() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_6, this, this));
        return this.matrix;
    }
    
    public long getNextTrackId() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_7, this, this));
        return this.nextTrackId;
    }
    
    @Override
    protected long getContentSize() {
        long contentSize = 4L;
        if (this.getVersion() == 1) {
            contentSize += 28L;
        }
        else {
            contentSize += 16L;
        }
        contentSize += 80L;
        return contentSize;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        if (this.getVersion() == 1) {
            this.creationTime = IsoTypeReader.readUInt64(content);
            this.modificationTime = IsoTypeReader.readUInt64(content);
            this.timescale = IsoTypeReader.readUInt32(content);
            this.duration = IsoTypeReader.readUInt64(content);
        }
        else {
            this.creationTime = IsoTypeReader.readUInt32(content);
            this.modificationTime = IsoTypeReader.readUInt32(content);
            this.timescale = IsoTypeReader.readUInt32(content);
            this.duration = IsoTypeReader.readUInt32(content);
        }
        this.rate = IsoTypeReader.readFixedPoint1616(content);
        this.volume = IsoTypeReader.readFixedPoint88(content);
        IsoTypeReader.readUInt16(content);
        IsoTypeReader.readUInt32(content);
        IsoTypeReader.readUInt32(content);
        this.matrix = new long[9];
        for (int i = 0; i < 9; ++i) {
            this.matrix[i] = IsoTypeReader.readUInt32(content);
        }
        for (int i = 0; i < 6; ++i) {
            IsoTypeReader.readUInt32(content);
        }
        this.nextTrackId = IsoTypeReader.readUInt32(content);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_8, this, this));
        final StringBuilder result = new StringBuilder();
        result.append("MovieHeaderBox[");
        result.append("creationTime=").append(this.getCreationTime());
        result.append(";");
        result.append("modificationTime=").append(this.getModificationTime());
        result.append(";");
        result.append("timescale=").append(this.getTimescale());
        result.append(";");
        result.append("duration=").append(this.getDuration());
        result.append(";");
        result.append("rate=").append(this.getRate());
        result.append(";");
        result.append("volume=").append(this.getVolume());
        for (int i = 0; i < this.matrix.length; ++i) {
            result.append(";");
            result.append("matrix").append(i).append("=").append(this.matrix[i]);
        }
        result.append(";");
        result.append("nextTrackId=").append(this.getNextTrackId());
        result.append("]");
        return result.toString();
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt64(bb, this.creationTime);
            IsoTypeWriter.writeUInt64(bb, this.modificationTime);
            IsoTypeWriter.writeUInt32(bb, this.timescale);
            IsoTypeWriter.writeUInt64(bb, this.duration);
        }
        else {
            IsoTypeWriter.writeUInt32(bb, this.creationTime);
            IsoTypeWriter.writeUInt32(bb, this.modificationTime);
            IsoTypeWriter.writeUInt32(bb, this.timescale);
            IsoTypeWriter.writeUInt32(bb, this.duration);
        }
        IsoTypeWriter.writeFixedPont1616(bb, this.rate);
        IsoTypeWriter.writeFixedPont88(bb, this.volume);
        IsoTypeWriter.writeUInt16(bb, 0);
        IsoTypeWriter.writeUInt32(bb, 0L);
        IsoTypeWriter.writeUInt32(bb, 0L);
        for (int i = 0; i < 9; ++i) {
            IsoTypeWriter.writeUInt32(bb, this.matrix[i]);
        }
        for (int i = 0; i < 6; ++i) {
            IsoTypeWriter.writeUInt32(bb, 0L);
        }
        IsoTypeWriter.writeUInt32(bb, this.nextTrackId);
    }
    
    public void setCreationTime(final long creationTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_9, this, this, Conversions.longObject(creationTime)));
        this.creationTime = creationTime;
    }
    
    public void setModificationTime(final long modificationTime) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_10, this, this, Conversions.longObject(modificationTime)));
        this.modificationTime = modificationTime;
    }
    
    public void setTimescale(final long timescale) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_11, this, this, Conversions.longObject(timescale)));
        this.timescale = timescale;
    }
    
    public void setDuration(final long duration) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_12, this, this, Conversions.longObject(duration)));
        this.duration = duration;
    }
    
    public void setRate(final double rate) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_13, this, this, Conversions.doubleObject(rate)));
        this.rate = rate;
    }
    
    public void setVolume(final float volume) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_14, this, this, Conversions.floatObject(volume)));
        this.volume = volume;
    }
    
    public void setMatrix(final long[] matrix) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_15, this, this, matrix));
        this.matrix = matrix;
    }
    
    public void setNextTrackId(final long nextTrackId) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(MovieHeaderBox.ajc$tjp_16, this, this, Conversions.longObject(nextTrackId)));
        this.nextTrackId = nextTrackId;
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("MovieHeaderBox.java", MovieHeaderBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getCreationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 50);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getModificationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 54);
        ajc$tjp_10 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setModificationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "modificationTime", "", "void"), 183);
        ajc$tjp_11 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setTimescale", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "timescale", "", "void"), 187);
        ajc$tjp_12 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "duration", "", "void"), 191);
        ajc$tjp_13 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setRate", "com.coremedia.iso.boxes.MovieHeaderBox", "double", "rate", "", "void"), 195);
        ajc$tjp_14 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setVolume", "com.coremedia.iso.boxes.MovieHeaderBox", "float", "volume", "", "void"), 199);
        ajc$tjp_15 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setMatrix", "com.coremedia.iso.boxes.MovieHeaderBox", "[J", "matrix", "", "void"), 203);
        ajc$tjp_16 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setNextTrackId", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "nextTrackId", "", "void"), 207);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getTimescale", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 58);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 62);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getRate", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "double"), 66);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getVolume", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "float"), 70);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getMatrix", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "[J"), 74);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getNextTrackId", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 78);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "java.lang.String"), 123);
        ajc$tjp_9 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setCreationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "creationTime", "", "void"), 179);
    }
}
