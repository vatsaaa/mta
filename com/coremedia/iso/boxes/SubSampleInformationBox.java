// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import java.util.Iterator;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.ArrayList;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class SubSampleInformationBox extends AbstractFullBox
{
    public static final String TYPE = "subs";
    private long entryCount;
    private List<SampleEntry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    
    public SubSampleInformationBox() {
        super("subs");
        this.entries = new ArrayList<SampleEntry>();
    }
    
    public List<SampleEntry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubSampleInformationBox.ajc$tjp_0, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<SampleEntry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubSampleInformationBox.ajc$tjp_1, this, this, entries));
        this.entries = entries;
    }
    
    @Override
    protected long getContentSize() {
        final long entries = 8L + 6L * this.entryCount;
        int subsampleEntries = 0;
        for (final SampleEntry sampleEntry : this.entries) {
            subsampleEntries += sampleEntry.getSubsampleCount() * (((this.getVersion() == 1) ? 4 : 2) + 1 + 1 + 4);
        }
        return entries + subsampleEntries;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.entryCount = IsoTypeReader.readUInt32(content);
        for (int i = 0; i < this.entryCount; ++i) {
            final SampleEntry sampleEntry = new SampleEntry();
            sampleEntry.setSampleDelta(IsoTypeReader.readUInt32(content));
            final int subsampleCount = IsoTypeReader.readUInt16(content);
            sampleEntry.setSubsampleCount(subsampleCount);
            for (int j = 0; j < subsampleCount; ++j) {
                final SampleEntry.SubsampleEntry subsampleEntry = new SampleEntry.SubsampleEntry();
                subsampleEntry.setSubsampleSize((this.getVersion() == 1) ? IsoTypeReader.readUInt32(content) : ((long)IsoTypeReader.readUInt16(content)));
                subsampleEntry.setSubsamplePriority(IsoTypeReader.readUInt8(content));
                subsampleEntry.setDiscardable(IsoTypeReader.readUInt8(content));
                subsampleEntry.setReserved(IsoTypeReader.readUInt32(content));
                sampleEntry.addSubsampleEntry(subsampleEntry);
            }
            this.entries.add(sampleEntry);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.entryCount);
        for (final SampleEntry sampleEntry : this.entries) {
            IsoTypeWriter.writeUInt32(bb, sampleEntry.getSampleDelta());
            IsoTypeWriter.writeUInt16(bb, sampleEntry.getSubsampleCount());
            final List subsampleEntries = sampleEntry.getSubsampleEntries();
            for (final SampleEntry.SubsampleEntry subsampleEntry : subsampleEntries) {
                if (this.getVersion() == 1) {
                    IsoTypeWriter.writeUInt32(bb, subsampleEntry.getSubsampleSize());
                }
                else {
                    IsoTypeWriter.writeUInt16(bb, CastUtils.l2i(subsampleEntry.getSubsampleSize()));
                }
                IsoTypeWriter.writeUInt8(bb, subsampleEntry.getSubsamplePriority());
                IsoTypeWriter.writeUInt8(bb, subsampleEntry.getDiscardable());
                IsoTypeWriter.writeUInt32(bb, subsampleEntry.getReserved());
            }
        }
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SubSampleInformationBox.ajc$tjp_2, this, this));
        return "SubSampleInformationBox{entryCount=" + this.entryCount + ", entries=" + this.entries + '}';
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SubSampleInformationBox.java", SubSampleInformationBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.SubSampleInformationBox", "", "", "", "java.util.List"), 49);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.SubSampleInformationBox", "java.util.List", "entries", "", "void"), 53);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.SubSampleInformationBox", "", "", "", "java.lang.String"), 113);
    }
    
    public static class SampleEntry
    {
        private long sampleDelta;
        private int subsampleCount;
        private List<SubsampleEntry> subsampleEntries;
        
        public SampleEntry() {
            this.subsampleEntries = new ArrayList<SubsampleEntry>();
        }
        
        public long getSampleDelta() {
            return this.sampleDelta;
        }
        
        public void setSampleDelta(final long sampleDelta) {
            this.sampleDelta = sampleDelta;
        }
        
        public int getSubsampleCount() {
            return this.subsampleCount;
        }
        
        public void setSubsampleCount(final int subsampleCount) {
            this.subsampleCount = subsampleCount;
        }
        
        public List<SubsampleEntry> getSubsampleEntries() {
            return this.subsampleEntries;
        }
        
        public void addSubsampleEntry(final SubsampleEntry subsampleEntry) {
            this.subsampleEntries.add(subsampleEntry);
        }
        
        @Override
        public String toString() {
            return "SampleEntry{sampleDelta=" + this.sampleDelta + ", subsampleCount=" + this.subsampleCount + ", subsampleEntries=" + this.subsampleEntries + '}';
        }
        
        public static class SubsampleEntry
        {
            private long subsampleSize;
            private int subsamplePriority;
            private int discardable;
            private long reserved;
            
            public long getSubsampleSize() {
                return this.subsampleSize;
            }
            
            public void setSubsampleSize(final long subsampleSize) {
                this.subsampleSize = subsampleSize;
            }
            
            public int getSubsamplePriority() {
                return this.subsamplePriority;
            }
            
            public void setSubsamplePriority(final int subsamplePriority) {
                this.subsamplePriority = subsamplePriority;
            }
            
            public int getDiscardable() {
                return this.discardable;
            }
            
            public void setDiscardable(final int discardable) {
                this.discardable = discardable;
            }
            
            public long getReserved() {
                return this.reserved;
            }
            
            public void setReserved(final long reserved) {
                this.reserved = reserved;
            }
            
            @Override
            public String toString() {
                return "SubsampleEntry{subsampleSize=" + this.subsampleSize + ", subsamplePriority=" + this.subsamplePriority + ", discardable=" + this.discardable + ", reserved=" + this.reserved + '}';
            }
        }
    }
}
