// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import com.coremedia.iso.IsoFile;
import java.util.List;

public interface ContainerBox extends Box
{
    List<Box> getBoxes();
    
    void setBoxes(final List<Box> p0);
    
     <T extends Box> List<T> getBoxes(final Class<T> p0);
    
     <T extends Box> List<T> getBoxes(final Class<T> p0, final boolean p1);
    
    ContainerBox getParent();
    
    long getNumOfBytesToFirstChild();
    
    IsoFile getIsoFile();
}
