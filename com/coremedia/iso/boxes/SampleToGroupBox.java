// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import java.util.Iterator;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import org.aspectj.runtime.internal.Conversions;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import java.util.ArrayList;
import org.aspectj.lang.JoinPoint;
import java.util.List;

public class SampleToGroupBox extends AbstractFullBox
{
    public static final String TYPE = "sbgp";
    private long groupingType;
    private long entryCount;
    private long groupingTypeParameter;
    private List<Entry> entries;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    
    public SampleToGroupBox() {
        super("sbgp");
        this.entries = new ArrayList<Entry>();
    }
    
    @Override
    protected long getContentSize() {
        return 12L + this.entryCount * 8L;
    }
    
    public long getGroupingTypeParameter() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToGroupBox.ajc$tjp_0, this, this));
        return this.groupingTypeParameter;
    }
    
    public void setGroupingTypeParameter(final long groupingTypeParameter) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToGroupBox.ajc$tjp_1, this, this, Conversions.longObject(groupingTypeParameter)));
        this.groupingTypeParameter = groupingTypeParameter;
    }
    
    public List<Entry> getEntries() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToGroupBox.ajc$tjp_2, this, this));
        return this.entries;
    }
    
    public void setEntries(final List<Entry> entries) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToGroupBox.ajc$tjp_3, this, this, entries));
        this.entries = entries;
    }
    
    public long getGroupingType() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToGroupBox.ajc$tjp_4, this, this));
        return this.groupingType;
    }
    
    public void setGroupingType(final long groupingType) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(SampleToGroupBox.ajc$tjp_5, this, this, Conversions.longObject(groupingType)));
        this.groupingType = groupingType;
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.groupingType = IsoTypeReader.readUInt32(content);
        if (this.getVersion() == 1) {
            this.groupingTypeParameter = IsoTypeReader.readUInt32(content);
        }
        else {
            this.groupingTypeParameter = -1L;
        }
        this.entryCount = IsoTypeReader.readUInt32(content);
        for (int i = 0; i < this.entryCount; ++i) {
            final Entry entry = new Entry();
            entry.setSampleCount(IsoTypeReader.readUInt32(content));
            entry.setGroupDescriptionIndex(IsoTypeReader.readUInt32(content));
            this.entries.add(entry);
        }
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        IsoTypeWriter.writeUInt32(bb, this.groupingType);
        if (this.getVersion() == 1) {
            IsoTypeWriter.writeUInt32(bb, this.groupingTypeParameter);
        }
        IsoTypeWriter.writeUInt32(bb, this.entryCount);
        for (final Entry entry : this.entries) {
            IsoTypeWriter.writeUInt32(bb, entry.getSampleCount());
            IsoTypeWriter.writeUInt32(bb, entry.getGroupDescriptionIndex());
        }
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("SampleToGroupBox.java", SampleToGroupBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getGroupingTypeParameter", "com.coremedia.iso.boxes.SampleToGroupBox", "", "", "", "long"), 40);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setGroupingTypeParameter", "com.coremedia.iso.boxes.SampleToGroupBox", "long", "groupingTypeParameter", "", "void"), 47);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getEntries", "com.coremedia.iso.boxes.SampleToGroupBox", "", "", "", "java.util.List"), 51);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setEntries", "com.coremedia.iso.boxes.SampleToGroupBox", "java.util.List", "entries", "", "void"), 55);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getGroupingType", "com.coremedia.iso.boxes.SampleToGroupBox", "", "", "", "long"), 59);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setGroupingType", "com.coremedia.iso.boxes.SampleToGroupBox", "long", "groupingType", "", "void"), 64);
    }
    
    public static class Entry
    {
        private long sampleCount;
        private long groupDescriptionIndex;
        
        public long getSampleCount() {
            return this.sampleCount;
        }
        
        public void setSampleCount(final long sampleCount) {
            this.sampleCount = sampleCount;
        }
        
        public long getGroupDescriptionIndex() {
            return this.groupDescriptionIndex;
        }
        
        public void setGroupDescriptionIndex(final long groupDescriptionIndex) {
            this.groupDescriptionIndex = groupDescriptionIndex;
        }
    }
}
