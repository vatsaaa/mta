// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

import org.aspectj.lang.Signature;
import java.io.IOException;
import com.coremedia.iso.IsoTypeWriter;
import com.coremedia.iso.IsoFile;
import com.coremedia.iso.IsoTypeReader;
import java.nio.ByteBuffer;
import com.coremedia.iso.Utf8;
import org.aspectj.runtime.reflect.Factory;
import com.googlecode.mp4parser.RequiresParseDetailAspect;
import org.aspectj.lang.JoinPoint;

public class RatingBox extends AbstractFullBox
{
    public static final String TYPE = "rtng";
    private String ratingEntity;
    private String ratingCriteria;
    private String language;
    private String ratingInfo;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_0;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_1;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_2;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_3;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_4;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_5;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_6;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_7;
    private static final /* synthetic */ JoinPoint.StaticPart ajc$tjp_8;
    
    public RatingBox() {
        super("rtng");
    }
    
    public void setRatingEntity(final String ratingEntity) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_0, this, this, ratingEntity));
        this.ratingEntity = ratingEntity;
    }
    
    public void setRatingCriteria(final String ratingCriteria) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_1, this, this, ratingCriteria));
        this.ratingCriteria = ratingCriteria;
    }
    
    public void setLanguage(final String language) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_2, this, this, language));
        this.language = language;
    }
    
    public void setRatingInfo(final String ratingInfo) {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_3, this, this, ratingInfo));
        this.ratingInfo = ratingInfo;
    }
    
    public String getLanguage() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_4, this, this));
        return this.language;
    }
    
    public String getRatingEntity() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_5, this, this));
        return this.ratingEntity;
    }
    
    public String getRatingCriteria() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_6, this, this));
        return this.ratingCriteria;
    }
    
    public String getRatingInfo() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_7, this, this));
        return this.ratingInfo;
    }
    
    @Override
    protected long getContentSize() {
        return 15 + Utf8.utf8StringLengthInBytes(this.ratingInfo);
    }
    
    @Override
    public void _parseDetails(final ByteBuffer content) {
        this.parseVersionAndFlags(content);
        this.ratingEntity = IsoTypeReader.read4cc(content);
        this.ratingCriteria = IsoTypeReader.read4cc(content);
        this.language = IsoTypeReader.readIso639(content);
        this.ratingInfo = IsoTypeReader.readString(content);
    }
    
    @Override
    protected void getContent(final ByteBuffer bb) throws IOException {
        this.writeVersionAndFlags(bb);
        bb.put(IsoFile.fourCCtoBytes(this.ratingEntity));
        bb.put(IsoFile.fourCCtoBytes(this.ratingCriteria));
        IsoTypeWriter.writeIso639(bb, this.language);
        bb.put(Utf8.convert(this.ratingInfo));
        bb.put((byte)0);
    }
    
    @Override
    public String toString() {
        RequiresParseDetailAspect.aspectOf().before(Factory.makeJP(RatingBox.ajc$tjp_8, this, this));
        final StringBuilder buffer = new StringBuilder();
        buffer.append("RatingBox[language=").append(this.getLanguage());
        buffer.append("ratingEntity=").append(this.getRatingEntity());
        buffer.append(";ratingCriteria=").append(this.getRatingCriteria());
        buffer.append(";language=").append(this.getLanguage());
        buffer.append(";ratingInfo=").append(this.getRatingInfo());
        buffer.append("]");
        return buffer.toString();
    }
    
    static {
        ajc$preClinit();
    }
    
    private static /* synthetic */ void ajc$preClinit() {
        final Factory factory = new Factory("RatingBox.java", RatingBox.class);
        ajc$tjp_0 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setRatingEntity", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "ratingEntity", "", "void"), 45);
        ajc$tjp_1 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setRatingCriteria", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "ratingCriteria", "", "void"), 49);
        ajc$tjp_2 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setLanguage", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "language", "", "void"), 53);
        ajc$tjp_3 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "setRatingInfo", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "ratingInfo", "", "void"), 57);
        ajc$tjp_4 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getLanguage", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 61);
        ajc$tjp_5 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getRatingEntity", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 72);
        ajc$tjp_6 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getRatingCriteria", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 82);
        ajc$tjp_7 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "getRatingInfo", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 86);
        ajc$tjp_8 = factory.makeSJP("method-execution", factory.makeMethodSig("1", "toString", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 114);
    }
}
