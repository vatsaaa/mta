// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso.boxes;

public interface FullBox extends Box
{
    int getVersion();
    
    void setVersion(final int p0);
    
    int getFlags();
    
    void setFlags(final int p0);
}
