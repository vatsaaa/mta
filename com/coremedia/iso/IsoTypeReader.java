// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

public final class IsoTypeReader
{
    public static long readUInt32(final ByteBuffer bb) {
        final long ch1 = readUInt8(bb);
        final long ch2 = readUInt8(bb);
        final long ch3 = readUInt8(bb);
        final long ch4 = readUInt8(bb);
        return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0);
    }
    
    public static int readUInt24(final ByteBuffer bb) {
        int result = 0;
        result += readUInt16(bb) << 8;
        result += byte2int(bb.get());
        return result;
    }
    
    public static int readUInt16(final ByteBuffer bb) {
        int result = 0;
        result += byte2int(bb.get()) << 8;
        result += byte2int(bb.get());
        return result;
    }
    
    public static int readUInt8(final ByteBuffer bb) {
        return byte2int(bb.get());
    }
    
    public static int byte2int(final byte b) {
        return (b < 0) ? (b + 256) : b;
    }
    
    public static String readString(final ByteBuffer byteBuffer) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        int read;
        while ((read = byteBuffer.get()) != 0) {
            out.write(read);
        }
        return Utf8.convert(out.toByteArray());
    }
    
    public static String readString(final ByteBuffer byteBuffer, final int length) {
        final byte[] buffer = new byte[length];
        byteBuffer.get(buffer);
        return Utf8.convert(buffer);
    }
    
    public static long readUInt64(final ByteBuffer byteBuffer) {
        long result = 0L;
        result += readUInt32(byteBuffer) << 32;
        if (result < 0L) {
            throw new RuntimeException("I don't know how to deal with UInt64! long is not sufficient and I don't want to use BigInt");
        }
        result += readUInt32(byteBuffer);
        return result;
    }
    
    public static double readFixedPoint1616(final ByteBuffer bb) {
        final byte[] bytes = new byte[4];
        bb.get(bytes);
        int result = 0;
        result |= (bytes[0] << 24 & 0xFF000000);
        result |= (bytes[1] << 16 & 0xFF0000);
        result |= (bytes[2] << 8 & 0xFF00);
        result |= (bytes[3] & 0xFF);
        return result / 65536.0;
    }
    
    public static float readFixedPoint88(final ByteBuffer bb) {
        final byte[] bytes = new byte[2];
        bb.get(bytes);
        short result = 0;
        result |= (short)(bytes[0] << 8 & 0xFF00);
        result |= (short)(bytes[1] & 0xFF);
        return result / 256.0f;
    }
    
    public static String readIso639(final ByteBuffer bb) {
        final int bits = readUInt16(bb);
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < 3; ++i) {
            final int c = bits >> (2 - i) * 5 & 0x1F;
            result.append((char)(c + 96));
        }
        return result.toString();
    }
    
    public static String read4cc(final ByteBuffer bb) {
        final byte[] b = new byte[4];
        bb.get(b);
        return IsoFile.bytesToFourCC(b);
    }
}
