// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.nio.channels.SelectionKey;
import java.nio.channels.WritableByteChannel;
import java.io.EOFException;
import java.io.IOException;
import com.coremedia.iso.boxes.CastUtils;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class ChannelHelper
{
    public static ByteBuffer readFully(final ReadableByteChannel channel, final long size) throws IOException {
        if (channel instanceof FileChannel && size > 1048576L) {
            final ByteBuffer bb = ((FileChannel)channel).map(FileChannel.MapMode.READ_ONLY, ((FileChannel)channel).position(), size);
            ((FileChannel)channel).position(((FileChannel)channel).position() + size);
            return bb;
        }
        final ByteBuffer buf = ByteBuffer.allocate(CastUtils.l2i(size));
        readFully(channel, buf, buf.limit());
        buf.rewind();
        assert buf.limit() == size;
        return buf;
    }
    
    public static void readFully(final ReadableByteChannel channel, final ByteBuffer buf) throws IOException {
        readFully(channel, buf, buf.remaining());
    }
    
    public static int readFully(final ReadableByteChannel channel, final ByteBuffer buf, final int length) throws IOException {
        int count = 0;
        int n;
        while (-1 != (n = channel.read(buf))) {
            count += n;
            if (count == length) {
                break;
            }
        }
        if (n == -1) {
            throw new EOFException("End of file. No more boxes.");
        }
        return count;
    }
    
    public static void writeFully(final WritableByteChannel channel, final ByteBuffer buf) throws IOException {
        do {
            final int written = channel.write(buf);
            if (written < 0) {
                throw new EOFException();
            }
        } while (buf.hasRemaining());
    }
    
    public static void close(final SelectionKey key) {
        try {
            key.channel().close();
        }
        catch (IOException ex) {}
    }
}
