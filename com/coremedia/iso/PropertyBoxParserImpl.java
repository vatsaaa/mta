// 
// Decompiled by Procyon v0.5.36
// 

package com.coremedia.iso;

import java.util.regex.Matcher;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import com.coremedia.iso.boxes.AbstractBox;
import com.coremedia.iso.boxes.Box;
import java.util.Enumeration;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.io.BufferedInputStream;
import java.util.regex.Pattern;
import java.util.Properties;

public class PropertyBoxParserImpl extends AbstractBoxParser
{
    Properties mapping;
    Pattern p;
    
    public PropertyBoxParserImpl(final String... customProperties) {
        this.p = Pattern.compile("(.*)\\((.*?)\\)");
        final InputStream is = new BufferedInputStream(this.getClass().getResourceAsStream("/isoparser-default.properties"));
        try {
            this.mapping = new Properties();
            try {
                this.mapping.load(is);
                final Enumeration<URL> enumeration = Thread.currentThread().getContextClassLoader().getResources("isoparser-custom.properties");
                while (enumeration.hasMoreElements()) {
                    final URL url = enumeration.nextElement();
                    final InputStream customIS = new BufferedInputStream(url.openStream());
                    try {
                        this.mapping.load(customIS);
                    }
                    finally {
                        customIS.close();
                    }
                    customIS.close();
                }
                for (final String customProperty : customProperties) {
                    this.mapping.load(new BufferedInputStream(this.getClass().getResourceAsStream(customProperty)));
                }
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        finally {
            try {
                is.close();
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        try {
            is.close();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    public PropertyBoxParserImpl(final Properties mapping) {
        this.p = Pattern.compile("(.*)\\((.*?)\\)");
        this.mapping = mapping;
    }
    
    public Class<? extends Box> getClassForFourCc(final String type, final byte[] userType, final String parent) {
        final FourCcToBox fourCcToBox = new FourCcToBox(type, userType, parent).invoke();
        try {
            return (Class<? extends Box>)Class.forName(fourCcToBox.clazzName);
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public Box createBox(final String type, final byte[] userType, final String parent) {
        final FourCcToBox fourCcToBox = new FourCcToBox(type, userType, parent).invoke();
        String[] param = fourCcToBox.getParam();
        final String clazzName = fourCcToBox.getClazzName();
        try {
            if (param[0].trim().length() == 0) {
                param = new String[0];
            }
            final Class clazz = Class.forName(clazzName);
            final Class[] constructorArgsClazz = new Class[param.length];
            final Object[] constructorArgs = new Object[param.length];
            for (int i = 0; i < param.length; ++i) {
                if ("userType".equals(param[i])) {
                    constructorArgs[i] = userType;
                    constructorArgsClazz[i] = byte[].class;
                }
                else if ("type".equals(param[i])) {
                    constructorArgs[i] = type;
                    constructorArgsClazz[i] = String.class;
                }
                else {
                    if (!"parent".equals(param[i])) {
                        throw new InternalError("No such param: " + param[i]);
                    }
                    constructorArgs[i] = parent;
                    constructorArgsClazz[i] = String.class;
                }
            }
            try {
                Constructor<AbstractBox> constructorObject;
                if (param.length > 0) {
                    constructorObject = clazz.getConstructor((Class<?>[])constructorArgsClazz);
                }
                else {
                    constructorObject = clazz.getConstructor((Class<?>[])new Class[0]);
                }
                return constructorObject.newInstance(constructorArgs);
            }
            catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
            catch (InvocationTargetException e2) {
                throw new RuntimeException(e2);
            }
            catch (InstantiationException e3) {
                throw new RuntimeException(e3);
            }
            catch (IllegalAccessException e4) {
                throw new RuntimeException(e4);
            }
        }
        catch (ClassNotFoundException e5) {
            throw new RuntimeException(e5);
        }
    }
    
    private class FourCcToBox
    {
        private String type;
        private byte[] userType;
        private String parent;
        private String clazzName;
        private String[] param;
        
        public FourCcToBox(final String type, final byte[] userType, final String parent) {
            this.type = type;
            this.parent = parent;
            this.userType = userType;
        }
        
        public String getClazzName() {
            return this.clazzName;
        }
        
        public String[] getParam() {
            return this.param;
        }
        
        public FourCcToBox invoke() {
            String constructor;
            if (this.userType != null) {
                if (!"uuid".equals(this.type)) {
                    throw new RuntimeException("we have a userType but no uuid box type. Something's wrong");
                }
                constructor = PropertyBoxParserImpl.this.mapping.getProperty(String.valueOf(this.parent) + "-uuid[" + Hex.encodeHex(this.userType).toUpperCase() + "]");
                if (constructor == null) {
                    constructor = PropertyBoxParserImpl.this.mapping.getProperty("uuid[" + Hex.encodeHex(this.userType).toUpperCase() + "]");
                }
                if (constructor == null) {
                    constructor = PropertyBoxParserImpl.this.mapping.getProperty("uuid");
                }
            }
            else {
                constructor = PropertyBoxParserImpl.this.mapping.getProperty(String.valueOf(this.parent) + "-" + this.type);
                if (constructor == null) {
                    constructor = PropertyBoxParserImpl.this.mapping.getProperty(this.type);
                }
            }
            if (constructor == null) {
                constructor = PropertyBoxParserImpl.this.mapping.getProperty("default");
            }
            if (constructor == null) {
                throw new RuntimeException("No box object found for " + this.type);
            }
            final Matcher m = PropertyBoxParserImpl.this.p.matcher(constructor);
            final boolean matches = m.matches();
            if (!matches) {
                throw new RuntimeException("Cannot work with that constructor: " + constructor);
            }
            this.clazzName = m.group(1);
            this.param = m.group(2).split(",");
            return this;
        }
    }
}
