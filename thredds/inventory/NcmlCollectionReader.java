// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import org.slf4j.LoggerFactory;
import ucar.nc2.util.URLnaming;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.JDOMException;
import java.io.IOException;
import org.jdom.input.SAXBuilder;
import java.net.URL;
import java.util.Formatter;
import org.jdom.Element;
import org.slf4j.Logger;
import org.jdom.Namespace;

public class NcmlCollectionReader
{
    public static final Namespace ncNS;
    private static Logger log;
    private static boolean debugURL;
    private static boolean debugXML;
    private static boolean showParsedXML;
    private static final boolean validate = false;
    private DatasetCollectionManager datasetManager;
    private boolean hasInner;
    private boolean hasOuter;
    private Element netcdfElem;
    private Element aggElem;
    
    public static NcmlCollectionReader open(String ncmlLocation, final Formatter errlog) throws IOException {
        if (!ncmlLocation.startsWith("http:") && !ncmlLocation.startsWith("file:")) {
            ncmlLocation = "file:" + ncmlLocation;
        }
        final URL url = new URL(ncmlLocation);
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(false);
            if (NcmlCollectionReader.debugURL) {
                System.out.println(" NetcdfDataset URL = <" + url + ">");
            }
            doc = builder.build(url);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        if (NcmlCollectionReader.debugXML) {
            System.out.println(" SAXBuilder done");
        }
        if (NcmlCollectionReader.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** NetcdfDataset/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        final Element netcdfElem = doc.getRootElement();
        final Namespace use = netcdfElem.getNamespace();
        if (!use.equals(NcmlCollectionReader.ncNS)) {
            errlog.format("Incorrect namespace specified in NcML= %s must be %s%n", use.getURI(), NcmlCollectionReader.ncNS.getURI());
            return null;
        }
        final Element aggElem = netcdfElem.getChild("aggregation", NcmlCollectionReader.ncNS);
        if (aggElem == null) {
            errlog.format("NcML must have aggregation element", new Object[0]);
            return null;
        }
        final String type = aggElem.getAttributeValue("type");
        if (!type.equals("forecastModelRunCollection") && !type.equals("forecastModelRunSingleCollection") && !type.equals("fmrc")) {
            errlog.format("NcML aggregation must be of type fmrc", new Object[0]);
            return null;
        }
        Element scanElem = aggElem.getChild("scan", NcmlCollectionReader.ncNS);
        if (scanElem == null) {
            scanElem = aggElem.getChild("scanFmrc", NcmlCollectionReader.ncNS);
        }
        if (scanElem == null) {
            errlog.format("NcML must have aggregation scan or scanFmrc element", new Object[0]);
            return null;
        }
        return new NcmlCollectionReader(ncmlLocation, netcdfElem);
    }
    
    NcmlCollectionReader(final String ncmlLocation, final Element netcdfElem) {
        final Element aggElem = netcdfElem.getChild("aggregation", NcmlCollectionReader.ncNS);
        final String recheck = aggElem.getAttributeValue("recheckEvery");
        Element scanElem = aggElem.getChild("scan", NcmlCollectionReader.ncNS);
        if (scanElem == null) {
            scanElem = aggElem.getChild("scanFmrc", NcmlCollectionReader.ncNS);
        }
        String dirLocation = scanElem.getAttributeValue("location");
        dirLocation = URLnaming.resolve(ncmlLocation, dirLocation);
        final String regexpPatternString = scanElem.getAttributeValue("regExp");
        final String suffix = scanElem.getAttributeValue("suffix");
        final String subdirs = scanElem.getAttributeValue("subdirs");
        final String olderThan = scanElem.getAttributeValue("olderThan");
        (this.datasetManager = new DatasetCollectionManager(recheck)).addDirectoryScan(dirLocation, suffix, regexpPatternString, subdirs, olderThan, null);
        final String dateFormatMark = scanElem.getAttributeValue("dateFormatMark");
        DateExtractor dateExtractor = null;
        if (dateFormatMark != null) {
            dateExtractor = new DateExtractorFromName(dateFormatMark, true);
        }
        else {
            final String runDateMatcher = scanElem.getAttributeValue("runDateMatcher");
            if (runDateMatcher != null) {
                dateExtractor = new DateExtractorFromName(runDateMatcher, false);
            }
        }
        this.datasetManager.setDateExtractor(dateExtractor);
        this.hasOuter = this.hasMods(netcdfElem);
        this.hasInner = this.hasMods(aggElem);
        if (this.hasOuter) {
            this.netcdfElem = netcdfElem;
        }
        if (this.hasInner) {
            this.aggElem = aggElem;
        }
    }
    
    private boolean hasMods(final Element elem) {
        return elem.getChildren("attribute", NcmlCollectionReader.ncNS).size() > 0 || elem.getChildren("variable", NcmlCollectionReader.ncNS).size() > 0 || elem.getChildren("dimension", NcmlCollectionReader.ncNS).size() > 0 || elem.getChildren("group", NcmlCollectionReader.ncNS).size() > 0 || elem.getChildren("remove", NcmlCollectionReader.ncNS).size() > 0;
    }
    
    public Element getNcmlOuter() {
        return this.netcdfElem;
    }
    
    public Element getNcmlInner() {
        return this.aggElem;
    }
    
    public DatasetCollectionManager getDatasetManager() {
        return this.datasetManager;
    }
    
    static {
        ncNS = Namespace.getNamespace("nc", "http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2");
        NcmlCollectionReader.log = LoggerFactory.getLogger(NcmlCollectionReader.class);
        NcmlCollectionReader.debugURL = false;
        NcmlCollectionReader.debugXML = false;
        NcmlCollectionReader.showParsedXML = false;
    }
}
