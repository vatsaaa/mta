// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import net.jcip.annotations.Immutable;

@Immutable
public class MCollection
{
    private final String name;
    private final String dirName;
    private final boolean wantSubdirs;
    private final MFileFilter ff;
    private final Object auxInfo;
    
    public MCollection(final String name, final String dirName, final boolean wantSubdirs, final MFileFilter ff, final Object auxInfo) {
        this.name = name;
        this.dirName = dirName;
        this.wantSubdirs = wantSubdirs;
        this.ff = ff;
        this.auxInfo = auxInfo;
    }
    
    public MCollection subdir(final MFile child) {
        return new MCollection(this.name + "/" + child.getName(), this.dirName + "/" + child.getName(), this.wantSubdirs, this.ff, child.getAuxInfo());
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getDirectoryName() {
        return this.dirName;
    }
    
    public boolean wantSubdirs() {
        return this.wantSubdirs;
    }
    
    public MFileFilter getFileFilter() {
        return this.ff;
    }
    
    public boolean accept(final MFile file) {
        return this.ff == null || this.ff.accept(file);
    }
    
    @Override
    public String toString() {
        return "MCollection{name='" + this.name + '\'' + ", dirName='" + this.dirName + '\'' + ", wantSubdirs=" + this.wantSubdirs + ", ff=" + this.ff + '}';
    }
    
    public Object getAuxInfo() {
        return this.auxInfo;
    }
}
