// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import java.util.Iterator;
import ucar.nc2.units.DateType;
import java.util.Date;
import org.slf4j.LoggerFactory;
import ucar.nc2.ft.fmrc.Fmrc;
import java.util.Formatter;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvAccess;
import ucar.nc2.thredds.ThreddsDataFactory;
import thredds.catalog.InvDataset;
import java.io.IOException;
import thredds.catalog.InvCatalogImpl;
import java.io.PrintStream;
import thredds.catalog.InvCatalogFactory;
import ucar.nc2.util.CancelTask;
import java.util.Map;
import org.slf4j.Logger;
import net.jcip.annotations.ThreadSafe;
import thredds.catalog.crawl.CatalogCrawler;

@ThreadSafe
public class DatasetCollectionFromCatalog extends DatasetCollectionManager implements CatalogCrawler.Listener
{
    private static final Logger log;
    private final String catalogUrl;
    private boolean debug;
    
    public DatasetCollectionFromCatalog(String collection) {
        this.debug = false;
        this.collectionName = collection;
        if (collection.startsWith("catalog:")) {
            collection = collection.substring("catalog:".length());
        }
        final int pos = collection.indexOf(63);
        if (pos > 0) {
            this.dateExtractor = new DateExtractorFromName(collection.substring(pos + 1), true);
            collection = collection.substring(0, pos);
        }
        this.catalogUrl = collection;
    }
    
    @Override
    protected void scan(final Map<String, MFile> map, final CancelTask cancelTask) throws IOException {
        final InvCatalogFactory catFactory = InvCatalogFactory.getDefaultFactory(true);
        final InvCatalogImpl cat = catFactory.readXML(this.catalogUrl);
        final StringBuilder buff = new StringBuilder();
        final boolean isValid = cat.check(buff, false);
        if (!isValid) {
            DatasetCollectionFromCatalog.log.warn("Catalog invalid= " + this.catalogUrl + " validation output= " + (Object)buff);
            return;
        }
        final CatalogCrawler crawler = new CatalogCrawler(1, false, this);
        final long start = System.currentTimeMillis();
        try {
            crawler.crawl(cat, null, null, map);
        }
        finally {
            final long took = System.currentTimeMillis() - start;
            if (this.debug) {
                System.out.format("***Done " + this.catalogUrl + " took = " + took + " msecs\n", new Object[0]);
            }
        }
    }
    
    public void getDataset(final InvDataset ds, final Object context) {
        if (ds.hasAccess()) {
            final ThreddsDataFactory tdataFactory = new ThreddsDataFactory();
            final InvAccess access = tdataFactory.chooseDatasetAccess(ds.getAccess());
            final MFileRemote mfile = new MFileRemote(access);
            if (mfile.getPath().endsWith(".xml")) {
                return;
            }
            final Map<String, MFile> map = (Map<String, MFile>)context;
            map.put(mfile.getPath(), mfile);
            if (this.debug) {
                System.out.format("add %s %n", mfile.getPath());
            }
        }
    }
    
    public boolean getCatalogRef(final InvCatalogRef dd, final Object context) {
        return true;
    }
    
    public static void main(final String[] arg) throws IOException {
        final String catUrl = "http://motherlode.ucar.edu:8080/thredds/catalog/fmrc/NCEP/NDFD/CONUS_5km/files/catalog.xml";
        final DatasetCollectionFromCatalog man = new DatasetCollectionFromCatalog(catUrl);
        man.debug = true;
        man.scan(null);
        final Formatter errlog = new Formatter();
        final Fmrc fmrc = Fmrc.open("catalog:" + catUrl, errlog);
        System.out.printf("errlog = %s %n", errlog);
    }
    
    static {
        log = LoggerFactory.getLogger(DatasetCollectionFromCatalog.class);
    }
    
    private class MFileRemote implements MFile
    {
        private Object info;
        private final InvAccess access;
        private Date lastModified;
        
        MFileRemote(final InvAccess access) {
            this.access = access;
            for (final DateType dateType : access.getDataset().getDates()) {
                if (dateType.getType().equals("modified")) {
                    this.lastModified = dateType.getDate();
                }
            }
        }
        
        public long getLastModified() {
            return (this.lastModified == null) ? -1L : this.lastModified.getTime();
        }
        
        public long getLength() {
            return (long)this.access.getDataSize();
        }
        
        public boolean isDirectory() {
            return false;
        }
        
        public String getPath() {
            return this.access.getStandardUrlName();
        }
        
        public String getName() {
            return this.access.getDataset().getName();
        }
        
        public int compareTo(final MFile o) {
            return this.getPath().compareTo(o.getPath());
        }
        
        public Object getAuxInfo() {
            return this.info;
        }
        
        public void setAuxInfo(final Object info) {
            this.info = info;
        }
    }
}
