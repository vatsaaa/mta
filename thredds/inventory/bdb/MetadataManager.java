// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory.bdb;

import org.slf4j.LoggerFactory;
import com.sleepycat.je.DatabaseStats;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.CursorConfig;
import thredds.inventory.MFile;
import java.util.Map;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.LockMode;
import java.io.UnsupportedEncodingException;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseConfig;
import java.io.IOException;
import com.sleepycat.je.Transaction;
import com.sleepycat.je.EnvironmentStats;
import com.sleepycat.je.StatsConfig;
import java.util.Formatter;
import java.util.Iterator;
import java.util.ArrayList;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.EnvironmentLockedException;
import java.io.File;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.Database;
import java.util.List;
import com.sleepycat.je.Environment;
import org.slf4j.Logger;

public class MetadataManager
{
    private static final Logger logger;
    private static final String UTF8 = "UTF-8";
    private static String root;
    private static Environment myEnv;
    private static List<MetadataManager> openDatabases;
    private static boolean readOnly;
    private static boolean debug;
    private static boolean debugDelete;
    private String collectionName;
    private Database database;
    
    public static void setCacheDirectory(final String dir) {
        MetadataManager.root = dir;
    }
    
    private static void setup() throws DatabaseException {
        final EnvironmentConfig myEnvConfig = new EnvironmentConfig();
        myEnvConfig.setReadOnly(false);
        myEnvConfig.setAllowCreate(false);
        myEnvConfig.setSharedCache(true);
        final File dir = new File(MetadataManager.root);
        if (!dir.mkdirs()) {
            MetadataManager.logger.warn("MetadataManager failed to make directory " + MetadataManager.root);
        }
        try {
            MetadataManager.myEnv = new Environment(dir, myEnvConfig);
            MetadataManager.logger.info("MetadataManager opened bdb in directory=" + dir);
            MetadataManager.readOnly = false;
        }
        catch (EnvironmentLockedException e) {
            myEnvConfig.setReadOnly(MetadataManager.readOnly = true);
            myEnvConfig.setAllowCreate(false);
            MetadataManager.myEnv = new Environment(dir, myEnvConfig);
        }
        if (MetadataManager.debug) {
            System.out.printf("MetadataManager: open bdb at root %s readOnly = %s%n", MetadataManager.root, MetadataManager.readOnly);
        }
    }
    
    public static void closeAll() {
        if (MetadataManager.debug) {
            System.out.println("close MetadataManager");
        }
        for (final MetadataManager mm : MetadataManager.openDatabases) {
            if (MetadataManager.debug) {
                System.out.println("  close database " + mm.collectionName);
            }
            mm.close();
        }
        MetadataManager.openDatabases = new ArrayList<MetadataManager>();
        if (MetadataManager.myEnv != null) {
            try {
                MetadataManager.myEnv.close();
                MetadataManager.myEnv = null;
            }
            catch (DatabaseException dbe) {
                MetadataManager.logger.error("Error closing MyDbEnv: " + dbe.toString());
            }
        }
    }
    
    public static void showEnvStats(final Formatter f) {
        if (MetadataManager.myEnv == null) {
            setup();
        }
        try {
            final EnvironmentStats stats = MetadataManager.myEnv.getStats((StatsConfig)null);
            f.format("EnvironmentStats%n%s%n", stats);
            f.format("%nDatabaseNames%n", new Object[0]);
            for (final String dbName : MetadataManager.myEnv.getDatabaseNames()) {
                f.format(" %s%n", dbName);
            }
        }
        catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
    
    public static String getCacheLocation() {
        return MetadataManager.root;
    }
    
    public static List<String> getCollectionNames() {
        if (MetadataManager.myEnv == null) {
            setup();
        }
        return (List<String>)MetadataManager.myEnv.getDatabaseNames();
    }
    
    public static void deleteCollection(final String collectionName) throws Exception {
        for (final MetadataManager mm : MetadataManager.openDatabases) {
            if (mm.collectionName.equals(collectionName) && mm.database != null) {
                mm.database.close();
            }
        }
        MetadataManager.myEnv.removeDatabase((Transaction)null, collectionName);
    }
    
    public static void delete(final String collectionName, final String key) {
        try {
            final MetadataManager mm = new MetadataManager(collectionName);
            mm.delete(key);
            mm.close();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public MetadataManager(final String collectionName) throws DatabaseException, IOException {
        this.collectionName = collectionName;
        if (MetadataManager.myEnv == null) {
            setup();
        }
    }
    
    private void openDatabase() {
        if (this.database != null) {
            return;
        }
        final DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setReadOnly(MetadataManager.readOnly);
        dbConfig.setAllowCreate(!MetadataManager.readOnly);
        if (!MetadataManager.readOnly) {
            dbConfig.setDeferredWrite(true);
        }
        this.database = MetadataManager.myEnv.openDatabase((Transaction)null, this.collectionName, dbConfig);
        MetadataManager.openDatabases.add(this);
    }
    
    public void put(final String key, final String value) {
        if (MetadataManager.readOnly) {
            return;
        }
        this.openDatabase();
        try {
            this.database.put((Transaction)null, new DatabaseEntry(key.getBytes("UTF-8")), new DatabaseEntry(value.getBytes("UTF-8")));
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public void put(final byte[] key, final byte[] value) {
        if (MetadataManager.readOnly) {
            return;
        }
        this.openDatabase();
        this.database.put((Transaction)null, new DatabaseEntry(key), new DatabaseEntry(value));
    }
    
    public void put(final String key, final byte[] value) {
        if (MetadataManager.readOnly) {
            return;
        }
        this.openDatabase();
        try {
            this.database.put((Transaction)null, new DatabaseEntry(key.getBytes("UTF-8")), new DatabaseEntry(value));
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public byte[] get(final byte[] key) {
        this.openDatabase();
        final DatabaseEntry value = new DatabaseEntry();
        this.database.get((Transaction)null, new DatabaseEntry(key), value, LockMode.DEFAULT);
        return value.getData();
    }
    
    public byte[] getBytes(final String key) {
        this.openDatabase();
        try {
            final DatabaseEntry value = new DatabaseEntry();
            this.database.get((Transaction)null, new DatabaseEntry(key.getBytes("UTF-8")), value, LockMode.DEFAULT);
            return value.getData();
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public String get(final String key) {
        this.openDatabase();
        try {
            final DatabaseEntry value = new DatabaseEntry();
            final OperationStatus status = this.database.get((Transaction)null, new DatabaseEntry(key.getBytes("UTF-8")), value, LockMode.DEFAULT);
            if (status == OperationStatus.SUCCESS) {
                return new String(value.getData(), "UTF-8");
            }
            return null;
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void delete(final String theKey) {
        this.openDatabase();
        try {
            final DatabaseEntry entry = new DatabaseEntry(theKey.getBytes("UTF-8"));
            this.database.delete((Transaction)null, entry);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void delete(final Map<String, MFile> current) {
        this.openDatabase();
        final List<DatabaseEntry> result = new ArrayList<DatabaseEntry>();
        Cursor myCursor = null;
        try {
            myCursor = this.database.openCursor((Transaction)null, (CursorConfig)null);
            final DatabaseEntry foundKey = new DatabaseEntry();
            final DatabaseEntry foundData = new DatabaseEntry();
            int count = 0;
            while (myCursor.getNext(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
                final String key = new String(foundKey.getData(), "UTF-8");
                final int pos = key.indexOf("#");
                if (pos > 0) {
                    final String filename = key.substring(0, pos);
                    if (null != current.get(filename)) {
                        continue;
                    }
                    result.add(new DatabaseEntry(foundKey.getData()));
                    ++count;
                }
            }
            if (MetadataManager.debugDelete) {
                for (final DatabaseEntry entry : result) {
                    final OperationStatus status = this.database.delete((Transaction)null, entry);
                    final String key2 = new String(entry.getData(), "UTF-8");
                    System.out.printf("%s deleted %s%n", status, key2);
                }
            }
        }
        catch (UnsupportedOperationException e) {
            MetadataManager.logger.error("Trying to delete " + this.collectionName, e);
        }
        catch (UnsupportedEncodingException e2) {
            MetadataManager.logger.error("Trying to delete " + this.collectionName, e2);
        }
        finally {
            if (null != myCursor) {
                myCursor.close();
            }
        }
    }
    
    public void close() {
        if (this.database != null) {
            this.database.close();
            MetadataManager.openDatabases.remove(this);
            this.database = null;
        }
    }
    
    public void showStats(final Formatter f) {
        this.openDatabase();
        try {
            final DatabaseStats dstats = this.database.getStats((StatsConfig)null);
            f.format("primary stats %n%s%n", dstats);
        }
        catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
    
    public List<KeyValue> getContent() throws DatabaseException, UnsupportedEncodingException {
        this.openDatabase();
        final List<KeyValue> result = new ArrayList<KeyValue>();
        Cursor myCursor = null;
        try {
            myCursor = this.database.openCursor((Transaction)null, (CursorConfig)null);
            final DatabaseEntry foundKey = new DatabaseEntry();
            final DatabaseEntry foundData = new DatabaseEntry();
            while (myCursor.getNext(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
                final String key = new String(foundKey.getData(), "UTF-8");
                final String data = new String(foundData.getData(), "UTF-8");
                result.add(new KeyValue(key, data));
            }
        }
        finally {
            if (null != myCursor) {
                myCursor.close();
            }
        }
        return result;
    }
    
    public static void main(final String[] args) throws Exception {
        final MetadataManager indexer = new MetadataManager("dummy");
        indexer.showStats(new Formatter(System.out));
        closeAll();
    }
    
    static {
        logger = LoggerFactory.getLogger(MetadataManager.class);
        MetadataManager.root = null;
        MetadataManager.myEnv = null;
        MetadataManager.openDatabases = new ArrayList<MetadataManager>();
        MetadataManager.readOnly = false;
        MetadataManager.debug = false;
        MetadataManager.debugDelete = false;
        String home = System.getProperty("user.home");
        if (home == null) {
            home = System.getProperty("user.dir");
        }
        if (home == null) {
            home = ".";
        }
        MetadataManager.root = home + "/.unidata/bdb/";
    }
    
    public class KeyValue
    {
        public String key;
        public String value;
        
        KeyValue(final String key, final String value) {
            this.key = key;
            this.value = value;
        }
    }
}
