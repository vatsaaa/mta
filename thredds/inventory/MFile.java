// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

public interface MFile extends Comparable<MFile>
{
    long getLastModified();
    
    long getLength();
    
    boolean isDirectory();
    
    String getPath();
    
    String getName();
    
    int compareTo(final MFile p0);
    
    Object getAuxInfo();
    
    void setAuxInfo(final Object p0);
}
