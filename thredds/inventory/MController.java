// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import java.util.Iterator;

public interface MController
{
    Iterator<MFile> getInventory(final MCollection p0);
    
    Iterator<MFile> getInventoryNoSubdirs(final MCollection p0);
    
    Iterator<MFile> getInventory(final MCollection p0, final boolean p1);
    
    Iterator<MFile> getInventoryNoSubdirs(final MCollection p0, final boolean p1);
    
    void close();
}
