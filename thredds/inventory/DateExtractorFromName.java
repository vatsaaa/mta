// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import ucar.nc2.units.DateFromString;
import java.util.Date;

public class DateExtractorFromName implements DateExtractor
{
    private String dateFormatMark;
    private boolean useName;
    
    public DateExtractorFromName(final String dateFormatMark, final boolean useName) {
        this.dateFormatMark = dateFormatMark;
        this.useName = useName;
    }
    
    public Date getDate(final MFile mfile) {
        if (this.useName) {
            return DateFromString.getDateUsingDemarkatedCount(mfile.getName(), this.dateFormatMark, '#');
        }
        return DateFromString.getDateUsingDemarkatedMatch(mfile.getPath(), this.dateFormatMark, '#');
    }
}
