// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import java.util.Date;
import java.util.List;
import ucar.nc2.units.TimeUnit;
import java.io.IOException;
import ucar.nc2.util.CancelTask;

public interface CollectionManager
{
    String getCollectionName();
    
    void scan(final CancelTask p0) throws IOException;
    
    boolean isRescanNeeded();
    
    boolean rescan() throws IOException;
    
    TimeUnit getRecheck();
    
    long getLastScanned();
    
    String getRoot();
    
    List<MFile> getFiles();
    
    Date extractRunDate(final MFile p0);
    
    void close();
    
    void putMetadata(final MFile p0, final String p1, final byte[] p2);
    
    byte[] getMetadata(final MFile p0, final String p1);
}
