// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import java.util.Date;

public interface DateExtractor
{
    Date getDate(final MFile p0);
}
