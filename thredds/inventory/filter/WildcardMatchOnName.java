// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory.filter;

import java.util.regex.Matcher;
import thredds.inventory.MFile;
import java.util.regex.Pattern;

public class WildcardMatchOnName extends WildcardMatchOnPath
{
    public WildcardMatchOnName(final String wildcardString) {
        super(wildcardString);
    }
    
    public WildcardMatchOnName(final Pattern pattern) {
        super(pattern);
    }
    
    @Override
    public boolean accept(final MFile file) {
        final Matcher matcher = this.pattern.matcher(file.getName());
        return matcher.matches();
    }
}
