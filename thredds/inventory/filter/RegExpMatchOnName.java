// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory.filter;

import java.util.regex.Matcher;
import thredds.inventory.MFile;
import java.util.regex.Pattern;
import thredds.inventory.MFileFilter;

public class RegExpMatchOnName implements MFileFilter
{
    private String regExpString;
    private Pattern pattern;
    
    public RegExpMatchOnName(final String regExpString) {
        this.regExpString = regExpString;
        this.pattern = Pattern.compile(regExpString);
    }
    
    public boolean accept(final MFile file) {
        final Matcher matcher = this.pattern.matcher(file.getName());
        return matcher.matches();
    }
}
