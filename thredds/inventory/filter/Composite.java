// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory.filter;

import java.util.Iterator;
import thredds.inventory.MFile;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import thredds.inventory.MFileFilter;

public class Composite implements MFileFilter
{
    private List<MFileFilter> filters;
    
    public Composite() {
        this.filters = new ArrayList<MFileFilter>();
    }
    
    public Composite(final List<MFileFilter> filters) {
        this.filters = new ArrayList<MFileFilter>(filters);
    }
    
    public void addFilter(final MFileFilter filter) {
        this.filters.add(filter);
    }
    
    public boolean accept(final MFile mfile) {
        for (final MFileFilter filter : this.filters) {
            if (!filter.accept(mfile)) {
                return false;
            }
        }
        return true;
    }
}
