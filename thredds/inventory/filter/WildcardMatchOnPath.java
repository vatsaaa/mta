// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory.filter;

import java.util.regex.Matcher;
import thredds.inventory.MFile;
import java.util.regex.Pattern;
import thredds.inventory.MFileFilter;

public class WildcardMatchOnPath implements MFileFilter
{
    protected String wildcardString;
    protected Pattern pattern;
    
    public WildcardMatchOnPath(final String wildcardString) {
        this.wildcardString = wildcardString;
        String regExp = wildcardString.replaceAll("\\.", "\\\\.");
        regExp = regExp.replaceAll("\\*", ".*");
        regExp = regExp.replaceAll("\\?", ".?");
        this.pattern = Pattern.compile(regExp);
    }
    
    public WildcardMatchOnPath(final Pattern pattern) {
        this.pattern = pattern;
    }
    
    public boolean accept(final MFile file) {
        final Matcher matcher = this.pattern.matcher(file.getPath());
        return matcher.matches();
    }
    
    @Override
    public String toString() {
        return "WildcardMatchOnPath{wildcardString='" + this.wildcardString + '}';
    }
}
