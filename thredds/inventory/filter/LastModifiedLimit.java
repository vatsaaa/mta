// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory.filter;

import thredds.inventory.MFile;
import thredds.inventory.MFileFilter;

public class LastModifiedLimit implements MFileFilter
{
    private long lastModifiedLimitInMillis;
    
    public LastModifiedLimit(final long lastModifiedLimitInMillis) {
        this.lastModifiedLimitInMillis = lastModifiedLimitInMillis;
    }
    
    public boolean accept(final MFile dataset) {
        final long lastModified = dataset.getLastModified();
        if (lastModified < 0L) {
            return true;
        }
        final long now = System.currentTimeMillis();
        return now - lastModified > this.lastModifiedLimitInMillis;
    }
    
    public long getLastModifiedLimitInMillis() {
        return this.lastModifiedLimitInMillis;
    }
}
