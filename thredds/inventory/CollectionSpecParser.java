// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import ucar.unidata.util.StringUtil;
import java.io.File;
import java.util.Formatter;
import java.util.regex.Pattern;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class CollectionSpecParser
{
    private String spec;
    private String topDir;
    private boolean subdirs;
    private boolean error;
    private String dateFormatMark;
    private Pattern pattern;
    
    public CollectionSpecParser(final String collectionSpec, final Formatter errlog) {
        this.subdirs = false;
        this.error = false;
        this.spec = collectionSpec.trim();
        int posFilter = -1;
        final int posGlob = collectionSpec.indexOf("/**/");
        if (posGlob > 0) {
            this.topDir = collectionSpec.substring(0, posGlob);
            posFilter = posGlob + 3;
            this.subdirs = true;
        }
        else {
            posFilter = collectionSpec.lastIndexOf(47);
            this.topDir = collectionSpec.substring(0, posFilter);
        }
        final File locFile = new File(this.topDir);
        if (!locFile.exists()) {
            errlog.format(" Directory %s does not exist %n", this.topDir);
            this.error = true;
        }
        String filter = null;
        if (posFilter < collectionSpec.length() - 2) {
            filter = collectionSpec.substring(posFilter + 1);
        }
        if (filter != null) {
            final int posFormat = filter.indexOf(35);
            if (posFormat >= 0) {
                final int posFormat2 = filter.lastIndexOf(35);
                if (posFormat != posFormat2) {
                    this.dateFormatMark = filter.substring(0, posFormat2);
                    filter = StringUtil.remove(filter, 35);
                    final StringBuilder sb = new StringBuilder(filter);
                    for (int i = posFormat; i < posFormat2 - 1; ++i) {
                        sb.setCharAt(i, '.');
                    }
                    final String regExp = sb.toString();
                    this.pattern = Pattern.compile(regExp);
                }
                else {
                    this.dateFormatMark = filter;
                    final String regExp2 = filter.substring(0, posFormat) + "*";
                    this.pattern = Pattern.compile(regExp2);
                }
            }
            else {
                this.pattern = Pattern.compile(filter);
            }
        }
    }
    
    public String getSpec() {
        return this.spec;
    }
    
    public String getTopDir() {
        return this.topDir;
    }
    
    public boolean wantSubdirs() {
        return this.subdirs;
    }
    
    public Pattern getFilter() {
        return this.pattern;
    }
    
    public String getDateFormatMark() {
        return this.dateFormatMark;
    }
    
    public boolean isError() {
        return this.error;
    }
    
    @Override
    public String toString() {
        return "CollectionSpecParser{\n   topDir='" + this.topDir + '\'' + "\n   subdirs=" + this.subdirs + "\n   regExp='" + this.pattern + '\'' + "\n   dateFormatMark='" + this.dateFormatMark + '\'' + "\n}";
    }
    
    private static void doit(final String spec, final Formatter errlog) {
        final CollectionSpecParser specp = new CollectionSpecParser(spec, errlog);
        System.out.printf("spec= %s%n%s%n", spec, specp);
        final String err = errlog.toString();
        if (err.length() > 0) {
            System.out.printf("%s%n", err);
        }
        System.out.printf("-----------------------------------%n", new Object[0]);
    }
    
    public static void main(final String[] arg) {
        doit("C:/data/formats/gempak/surface/#yyyyMMdd#_sao.gem", new Formatter());
    }
    
    public static void main2(final String[] arg) {
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/**/Surface_METAR_#yyyyMMdd_HHmm#\\.nc", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/**/Surface_METAR_#yyyyMMdd_HHmm#.nc", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/**/Surface_METAR_#yyyyMMdd_HHmm", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/Surface_METAR_#yyyyMMdd_HHmm", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/Surface_METAR_#yyyyMMdd_HHmm#.nc", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/Surface_METAR_yyyyMMdd_HHmm.nc", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/**/", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/**/*", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/*", new Formatter());
        doit("/data/ldm/pub/decoded/netcdf/surface/metar/T*.T", new Formatter());
    }
}
