// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import org.slf4j.LoggerFactory;
import thredds.filesystem.ControllerOS;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import java.util.Date;
import java.util.HashMap;
import ucar.nc2.util.CancelTask;
import thredds.inventory.filter.WildcardMatchOnPath;
import thredds.inventory.filter.RegExpMatchOnName;
import thredds.inventory.filter.Composite;
import thredds.inventory.filter.LastModifiedLimit;
import thredds.inventory.filter.WildcardMatchOnName;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Formatter;
import thredds.inventory.bdb.MetadataManager;
import java.util.Map;
import ucar.nc2.units.TimeUnit;
import java.util.List;
import org.slf4j.Logger;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class DatasetCollectionManager implements CollectionManager
{
    public static final String CATALOG = "catalog:";
    private static final Logger logger;
    private static MController controller;
    private static boolean enableMetadataManager;
    protected String collectionName;
    private CollectionSpecParser sp;
    protected DateExtractor dateExtractor;
    private final List<MCollection> scanList;
    protected TimeUnit recheck;
    private double olderThanFilterInSecs;
    private Map<String, MFile> map;
    private long lastScanned;
    private MetadataManager mm;
    
    public static void setController(final MController _controller) {
        DatasetCollectionManager.controller = _controller;
    }
    
    public static DatasetCollectionManager open(final String collection, final String olderThan, final Formatter errlog) throws IOException {
        if (collection.startsWith("catalog:")) {
            return new DatasetCollectionFromCatalog(collection);
        }
        return new DatasetCollectionManager(collection, errlog);
    }
    
    public static void enableMetadataManager() {
        DatasetCollectionManager.enableMetadataManager = true;
    }
    
    public DatasetCollectionManager(final String collectionSpec, final Formatter errlog) {
        this.scanList = new ArrayList<MCollection>();
        this.recheck = null;
        this.olderThanFilterInSecs = -1.0;
        this.collectionName = collectionSpec;
        this.sp = new CollectionSpecParser(collectionSpec, errlog);
        final MFileFilter mfilter = (null == this.sp.getFilter()) ? null : new WildcardMatchOnName(this.sp.getFilter());
        this.dateExtractor = ((this.sp.getDateFormatMark() == null) ? null : new DateExtractorFromName(this.sp.getDateFormatMark(), true));
        this.scanList.add(new MCollection(this.sp.getTopDir(), this.sp.getTopDir(), this.sp.wantSubdirs(), mfilter, null));
    }
    
    public DatasetCollectionManager(final CollectionSpecParser sp, final Formatter errlog) {
        this.scanList = new ArrayList<MCollection>();
        this.recheck = null;
        this.olderThanFilterInSecs = -1.0;
        this.collectionName = sp.getSpec();
        this.sp = sp;
        final MFileFilter mfilter = (null == sp.getFilter()) ? null : new WildcardMatchOnName(sp.getFilter());
        this.dateExtractor = ((sp.getDateFormatMark() == null) ? null : new DateExtractorFromName(sp.getDateFormatMark(), true));
        this.scanList.add(new MCollection(sp.getTopDir(), sp.getTopDir(), sp.wantSubdirs(), mfilter, null));
    }
    
    public DatasetCollectionManager(final FeatureCollectionConfig.Config config, final Formatter errlog) {
        this.scanList = new ArrayList<MCollection>();
        this.recheck = null;
        this.olderThanFilterInSecs = -1.0;
        this.sp = new CollectionSpecParser(config.spec, errlog);
        this.collectionName = ((config.name != null) ? config.name : config.spec);
        this.dateExtractor = ((this.sp.getDateFormatMark() == null) ? new DateExtractorNone() : new DateExtractorFromName(this.sp.getDateFormatMark(), true));
        final List<MFileFilter> filters = new ArrayList<MFileFilter>(3);
        if (null != this.sp.getFilter()) {
            filters.add(new WildcardMatchOnName(this.sp.getFilter()));
        }
        if (config.olderThan != null) {
            try {
                final TimeUnit tu = new TimeUnit(config.olderThan);
                this.olderThanFilterInSecs = tu.getValueInSeconds();
                filters.add(new LastModifiedLimit((long)(1000.0 * this.olderThanFilterInSecs)));
            }
            catch (Exception e) {
                DatasetCollectionManager.logger.error(this.collectionName + ": Invalid time unit for olderThan = {}", config.olderThan);
            }
        }
        final MFileFilter mfilter = (filters.size() == 0) ? null : ((filters.size() == 1) ? filters.get(0) : new Composite(filters));
        this.dateExtractor = ((this.sp.getDateFormatMark() == null) ? null : new DateExtractorFromName(this.sp.getDateFormatMark(), true));
        this.scanList.add(new MCollection(this.sp.getTopDir(), this.sp.getTopDir(), this.sp.wantSubdirs(), mfilter, null));
        if (DatasetCollectionManager.logger.isDebugEnabled()) {
            DatasetCollectionManager.logger.debug(this.collectionName + " init, config= " + config);
        }
    }
    
    public void close() {
        if (this.mm != null) {
            this.mm.close();
        }
    }
    
    protected DatasetCollectionManager() {
        this.scanList = new ArrayList<MCollection>();
        this.recheck = null;
        this.olderThanFilterInSecs = -1.0;
    }
    
    public DatasetCollectionManager(final String recheckS) {
        this.scanList = new ArrayList<MCollection>();
        this.recheck = null;
        this.olderThanFilterInSecs = -1.0;
        this.setRecheck(recheckS);
    }
    
    public void setRecheck(final String recheckS) {
        if (recheckS != null) {
            try {
                this.recheck = new TimeUnit(recheckS);
            }
            catch (Exception e) {
                DatasetCollectionManager.logger.error(this.collectionName + ": Invalid time unit for recheckEvery = {}", recheckS);
            }
        }
    }
    
    public void setDateExtractor(final DateExtractor dateExtractor) {
        this.dateExtractor = dateExtractor;
    }
    
    public void addDirectoryScan(final String dirName, final String suffix, final String regexpPatternString, final String subdirsS, final String olderS, final Object auxInfo) {
        final List<MFileFilter> filters = new ArrayList<MFileFilter>(3);
        if (null != regexpPatternString) {
            filters.add(new RegExpMatchOnName(regexpPatternString));
        }
        else if (suffix != null) {
            filters.add(new WildcardMatchOnPath("*" + suffix));
        }
        if (olderS != null) {
            try {
                final TimeUnit tu = new TimeUnit(olderS);
                filters.add(new LastModifiedLimit((long)(1000.0 * tu.getValueInSeconds())));
            }
            catch (Exception e) {
                DatasetCollectionManager.logger.error(this.collectionName + ": Invalid time unit for olderThan = {}", olderS);
            }
        }
        boolean wantSubdirs = true;
        if (subdirsS != null && subdirsS.equalsIgnoreCase("false")) {
            wantSubdirs = false;
        }
        final MFileFilter filter = (filters.size() == 0) ? null : ((filters.size() == 1) ? filters.get(0) : new Composite(filters));
        final MCollection mc = new MCollection(dirName, dirName, wantSubdirs, filter, auxInfo);
        final StringBuilder sb = new StringBuilder(dirName);
        if (wantSubdirs) {
            sb.append("**/");
        }
        if (null != regexpPatternString) {
            sb.append(regexpPatternString);
        }
        else if (suffix != null) {
            sb.append(suffix);
        }
        else {
            sb.append("noFilter");
        }
        this.collectionName = sb.toString();
        this.scanList.add(mc);
    }
    
    public String getCollectionName() {
        return "fmrc:" + this.collectionName;
    }
    
    public String getRoot() {
        return (this.sp == null) ? null : this.sp.getTopDir();
    }
    
    public CollectionSpecParser getCollectionSpecParser() {
        return this.sp;
    }
    
    public double getOlderThanFilterInSecs() {
        return this.olderThanFilterInSecs;
    }
    
    public void scan(final CancelTask cancelTask) throws IOException {
        final Map<String, MFile> newMap = new HashMap<String, MFile>();
        this.scan(newMap, cancelTask);
        this.deleteOld(newMap);
        synchronized (this) {
            this.map = newMap;
            this.lastScanned = System.currentTimeMillis();
        }
        if (DatasetCollectionManager.logger.isInfoEnabled()) {
            DatasetCollectionManager.logger.info(this.collectionName + ": initial scan found n datasets = " + this.map.keySet().size());
        }
    }
    
    public boolean isRescanNeeded() {
        if (this.scanList.isEmpty()) {
            if (DatasetCollectionManager.logger.isDebugEnabled()) {
                DatasetCollectionManager.logger.debug(this.collectionName + ": rescan not needed, no scanners");
            }
            return false;
        }
        if (this.recheck == null) {
            if (DatasetCollectionManager.logger.isDebugEnabled()) {
                DatasetCollectionManager.logger.debug(this.collectionName + ": rescan not needed, recheck null");
            }
            return false;
        }
        final Date now = new Date();
        final Date lastCheckedDate = new Date(this.lastScanned);
        final Date need = this.recheck.add(lastCheckedDate);
        if (now.before(need)) {
            if (DatasetCollectionManager.logger.isDebugEnabled()) {
                DatasetCollectionManager.logger.debug(this.collectionName + ": rescan not needed, last= " + lastCheckedDate + " now = " + now);
            }
            return false;
        }
        return true;
    }
    
    public boolean rescan() throws IOException {
        if (DatasetCollectionManager.logger.isInfoEnabled()) {
            DatasetCollectionManager.logger.info(this.collectionName + ": rescan at " + new Date());
        }
        final Map<String, MFile> oldMap = this.map;
        final Map<String, MFile> newMap = new HashMap<String, MFile>();
        this.scan(newMap, null);
        int nnew = 0;
        for (final MFile newDataset : newMap.values()) {
            final String path = newDataset.getPath();
            final MFile oldDataset = oldMap.get(path);
            if (oldDataset != null) {
                newMap.put(path, oldDataset);
                if (!DatasetCollectionManager.logger.isDebugEnabled()) {
                    continue;
                }
                DatasetCollectionManager.logger.debug(this.collectionName + ": rescan retains old Dataset= " + path);
            }
            else {
                ++nnew;
                if (!DatasetCollectionManager.logger.isDebugEnabled()) {
                    continue;
                }
                DatasetCollectionManager.logger.debug(this.collectionName + ": rescan found new Dataset= " + path);
            }
        }
        int ndelete = 0;
        for (final MFile oldDataset2 : oldMap.values()) {
            final String path2 = oldDataset2.getPath();
            final MFile newDataset2 = newMap.get(path2);
            if (newDataset2 == null) {
                ++ndelete;
                if (!DatasetCollectionManager.logger.isDebugEnabled()) {
                    continue;
                }
                DatasetCollectionManager.logger.debug(this.collectionName + ": rescan found deleted Dataset= " + path2);
            }
        }
        final boolean changed = nnew > 0 || ndelete > 0;
        if (changed) {
            if (DatasetCollectionManager.logger.isInfoEnabled()) {
                DatasetCollectionManager.logger.info(this.collectionName + ": rescan found changes new = " + nnew + " delete= " + ndelete);
            }
            synchronized (this) {
                this.map = newMap;
                this.lastScanned = System.currentTimeMillis();
            }
        }
        else {
            synchronized (this) {
                this.lastScanned = System.currentTimeMillis();
            }
        }
        return changed;
    }
    
    public TimeUnit getRecheck() {
        return this.recheck;
    }
    
    public long getLastScanned() {
        return this.lastScanned;
    }
    
    public List<MFile> getFiles() {
        final List<MFile> result = new ArrayList<MFile>(this.map.values());
        Collections.sort(result);
        return result;
    }
    
    public Date extractRunDate(final MFile mfile) {
        return (this.dateExtractor == null) ? null : this.dateExtractor.getDate(mfile);
    }
    
    protected void scan(final Map<String, MFile> map, final CancelTask cancelTask) throws IOException {
        if (null == DatasetCollectionManager.controller) {
            DatasetCollectionManager.controller = new ControllerOS();
        }
        for (final MCollection mc : this.scanList) {
            final Iterator<MFile> iter = mc.wantSubdirs() ? DatasetCollectionManager.controller.getInventory(mc) : DatasetCollectionManager.controller.getInventoryNoSubdirs(mc);
            if (iter == null) {
                DatasetCollectionManager.logger.error(this.collectionName + ": DatasetCollectionManager Invalid collection= " + mc);
            }
            else {
                while (iter.hasNext()) {
                    final MFile mfile = iter.next();
                    mfile.setAuxInfo(mc.getAuxInfo());
                    map.put(mfile.getPath(), mfile);
                }
                if (cancelTask != null && cancelTask.isCancel()) {
                    return;
                }
                continue;
            }
        }
    }
    
    @Override
    public String toString() {
        return "DatasetCollectionManager{collectionName='" + this.collectionName + '\'' + ", scanList=" + this.scanList + ", recheck=" + this.recheck + ", lastScanned=" + this.lastScanned + ", mm=" + this.mm + '}';
    }
    
    private void initMM() {
        if (this.collectionName == null) {
            return;
        }
        try {
            this.mm = new MetadataManager(this.collectionName);
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
    
    private void deleteOld(final Map<String, MFile> newMap) {
        if (this.mm == null && DatasetCollectionManager.enableMetadataManager) {
            this.initMM();
        }
        if (this.mm != null) {
            this.mm.delete(newMap);
        }
    }
    
    public void putMetadata(final MFile file, final String key, final byte[] value) {
        if (this.mm == null) {
            this.initMM();
        }
        if (this.mm != null) {
            this.mm.put(file.getPath() + "#" + key, value);
        }
    }
    
    public byte[] getMetadata(final MFile file, final String key) {
        if (this.mm == null) {
            this.initMM();
        }
        return (byte[])((this.mm == null) ? null : this.mm.getBytes(file.getPath() + "#" + key));
    }
    
    static {
        logger = LoggerFactory.getLogger(DatasetCollectionManager.class);
        DatasetCollectionManager.enableMetadataManager = false;
    }
}
