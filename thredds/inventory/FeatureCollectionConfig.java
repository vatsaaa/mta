// 
// Decompiled by Procyon v0.5.36
// 

package thredds.inventory;

import java.util.Iterator;
import java.util.Formatter;
import java.util.ArrayList;
import ucar.unidata.util.StringUtil;
import java.util.List;
import org.jdom.Element;
import java.util.Collections;
import java.util.EnumSet;
import org.slf4j.LoggerFactory;
import java.util.Set;
import org.slf4j.Logger;

public class FeatureCollectionConfig
{
    private static boolean regularizeDefault;
    private static Logger log;
    private static Set<FmrcDatasetType> defaultDatasetTypes;
    
    public static void setRegularizeDefault(final boolean t) {
        FeatureCollectionConfig.regularizeDefault = t;
    }
    
    public static boolean getRegularizeDefault() {
        return FeatureCollectionConfig.regularizeDefault;
    }
    
    static {
        FeatureCollectionConfig.regularizeDefault = false;
        FeatureCollectionConfig.log = LoggerFactory.getLogger(FeatureCollectionConfig.class);
        FeatureCollectionConfig.defaultDatasetTypes = Collections.unmodifiableSet((Set<? extends FmrcDatasetType>)EnumSet.of(FmrcDatasetType.TwoD, FmrcDatasetType.Best, FmrcDatasetType.Files, FmrcDatasetType.Runs));
    }
    
    public enum ProtoChoice
    {
        First, 
        Random, 
        Latest, 
        Penultimate, 
        Run;
    }
    
    public enum FmrcDatasetType
    {
        TwoD, 
        Best, 
        Files, 
        Runs, 
        ConstantForecasts, 
        ConstantOffsets;
    }
    
    public static class Config
    {
        public String name;
        public String spec;
        public String olderThan;
        public String recheckAfter;
        public UpdateConfig updateConfig;
        public ProtoConfig protoConfig;
        public FmrcConfig fmrcConfig;
        public Element innerNcml;
        
        public Config() {
            this.updateConfig = new UpdateConfig();
            this.protoConfig = new ProtoConfig();
            this.fmrcConfig = new FmrcConfig();
            this.innerNcml = null;
        }
        
        public Config(final String name, final String spec, final String olderThan, final String recheckAfter, final Element innerNcml) {
            this.updateConfig = new UpdateConfig();
            this.protoConfig = new ProtoConfig();
            this.fmrcConfig = new FmrcConfig();
            this.innerNcml = null;
            this.name = name;
            this.spec = spec.trim();
            this.olderThan = olderThan;
            this.recheckAfter = recheckAfter;
            this.innerNcml = innerNcml;
        }
        
        @Override
        public String toString() {
            return "Config{name='" + this.name + '\'' + "spec='" + this.spec + '\'' + ", olderThan='" + this.olderThan + '\'' + ", recheckAfter='" + this.recheckAfter + '\'' + "\n " + this.updateConfig + "\n " + this.protoConfig + "\n " + this.fmrcConfig + '}';
        }
    }
    
    public static class UpdateConfig
    {
        public boolean startup;
        public String rescan;
        public boolean triggerOk;
        
        public UpdateConfig() {
            this.rescan = null;
        }
        
        public UpdateConfig(final String startup, final String rescan, final String trigger) {
            this.rescan = null;
            if (startup != null) {
                this.startup = startup.equalsIgnoreCase("true");
            }
            if (trigger != null) {
                this.triggerOk = trigger.equalsIgnoreCase("allow");
            }
            this.rescan = rescan;
        }
        
        @Override
        public String toString() {
            return "UpdateConfig{startup=" + this.startup + ", rescan='" + this.rescan + '\'' + ", triggerOk=" + this.triggerOk + '}';
        }
    }
    
    public static class ProtoConfig
    {
        public ProtoChoice choice;
        public String param;
        public String change;
        public Element outerNcml;
        public boolean cacheAll;
        
        public ProtoConfig() {
            this.choice = ProtoChoice.Penultimate;
            this.param = null;
            this.change = null;
            this.outerNcml = null;
            this.cacheAll = true;
        }
        
        public ProtoConfig(final String choice, final String change, final String param, final Element ncml) {
            this.choice = ProtoChoice.Penultimate;
            this.param = null;
            this.change = null;
            this.outerNcml = null;
            this.cacheAll = true;
            if (choice != null) {
                try {
                    this.choice = ProtoChoice.valueOf(choice);
                }
                catch (Exception e) {
                    FeatureCollectionConfig.log.warn("Dont recognize ProtoChoice " + choice);
                }
            }
            this.change = change;
            this.param = param;
            this.outerNcml = ncml;
        }
        
        @Override
        public String toString() {
            return "ProtoConfig{choice=" + this.choice + ", change='" + this.change + '\'' + ", param='" + this.param + '\'' + ", outerNcml='" + this.outerNcml + '\'' + ", cacheAll=" + this.cacheAll + '}';
        }
    }
    
    public static class FmrcConfig
    {
        public boolean regularize;
        public Set<FmrcDatasetType> datasets;
        private boolean explicit;
        private List<BestDataset> bestDatasets;
        
        public FmrcConfig() {
            this.regularize = FeatureCollectionConfig.regularizeDefault;
            this.datasets = FeatureCollectionConfig.defaultDatasetTypes;
            this.explicit = false;
            this.bestDatasets = null;
        }
        
        public FmrcConfig(final String regularize) {
            this.regularize = FeatureCollectionConfig.regularizeDefault;
            this.datasets = FeatureCollectionConfig.defaultDatasetTypes;
            this.explicit = false;
            this.bestDatasets = null;
            this.regularize = (regularize != null && regularize.equalsIgnoreCase("true"));
        }
        
        public void addDatasetType(final String datasetTypes) {
            if (!this.explicit) {
                this.datasets = EnumSet.noneOf(FmrcDatasetType.class);
            }
            this.explicit = true;
            final String[] arr$;
            final String[] types = arr$ = StringUtil.splitString(datasetTypes);
            for (final String type : arr$) {
                try {
                    final FmrcDatasetType fdt = FmrcDatasetType.valueOf(type);
                    this.datasets.add(fdt);
                }
                catch (Exception e) {
                    FeatureCollectionConfig.log.warn("Dont recognize FmrcDatasetType " + type);
                }
            }
        }
        
        public void addBestDataset(final String name, final double greaterEqual) {
            if (this.bestDatasets == null) {
                this.bestDatasets = new ArrayList<BestDataset>(2);
            }
            this.bestDatasets.add(new BestDataset(name, greaterEqual));
        }
        
        public List<BestDataset> getBestDatasets() {
            return this.bestDatasets;
        }
        
        @Override
        public String toString() {
            final Formatter f = new Formatter();
            f.format("FmrcConfig: regularize=%s datasetTypes=%s", this.regularize, this.datasets);
            if (this.bestDatasets != null) {
                for (final BestDataset bd : this.bestDatasets) {
                    f.format("best = (%s, %f) ", bd.name, bd.greaterThan);
                }
            }
            return f.toString();
        }
    }
    
    public static class BestDataset
    {
        public String name;
        public double greaterThan;
        
        public BestDataset(final String name, final double greaterThan) {
            this.name = name;
            this.greaterThan = greaterThan;
        }
    }
}
