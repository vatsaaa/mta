// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.net.URI;

public interface AccessUriBuilder
{
    URI buildAccessUri(final Access p0, final URI p1);
}
