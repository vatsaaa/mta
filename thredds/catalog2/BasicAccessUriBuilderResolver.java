// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

public class BasicAccessUriBuilderResolver implements AccessUriBuilderResolver
{
    public AccessUriBuilder resolveAccessUriBuilder(final Dataset dataset, final Access access) {
        return new BasicAccessUriBuilder();
    }
}
