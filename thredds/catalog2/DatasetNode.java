// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.util.List;

public interface DatasetNode
{
    String getId();
    
    String getIdAuthority();
    
    String getName();
    
    List<Property> getProperties();
    
    Property getPropertyByName(final String p0);
    
    ThreddsMetadata getThreddsMetadata();
    
    List<Metadata> getMetadata();
    
    Catalog getParentCatalog();
    
    DatasetNode getParent();
    
    boolean isCollection();
    
    List<DatasetNode> getDatasets();
    
    DatasetNode getDatasetById(final String p0);
}
