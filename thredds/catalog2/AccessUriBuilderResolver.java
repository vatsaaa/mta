// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

public interface AccessUriBuilderResolver
{
    AccessUriBuilder resolveAccessUriBuilder(final Dataset p0, final Access p1);
}
