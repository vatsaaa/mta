// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.util;

import ucar.nc2.units.TimeDuration;
import ucar.nc2.units.DateRange;
import thredds.catalog2.builder.ThreddsMetadataBuilder;
import java.text.ParseException;
import ucar.nc2.units.DateType;
import thredds.catalog2.ThreddsMetadata;

public class ThreddsCatalogUtils
{
    private ThreddsCatalogUtils() {
    }
    
    public static DateType datePointToDateType(final ThreddsMetadata.DatePoint datePoint) throws ParseException {
        return new DateType(datePoint.getDate(), datePoint.getDateFormat(), datePoint.getType());
    }
    
    public static DateType datePointBuilderToDateType(final ThreddsMetadataBuilder.DatePointBuilder datePoint) throws ParseException {
        return new DateType(datePoint.getDate(), datePoint.getDateFormat(), datePoint.getType());
    }
    
    public static DateRange dateRangeToNcDateRange(final ThreddsMetadata.DateRange dateRange) throws ParseException {
        final DateType startDate = new DateType(dateRange.getStartDate(), dateRange.getStartDateFormat(), null);
        final DateType endDate = new DateType(dateRange.getEndDate(), dateRange.getEndDateFormat(), null);
        return new DateRange(startDate, endDate, new TimeDuration(dateRange.getDuration()), null);
    }
    
    public static DateRange dateRangeBuilderToNcDateRange(final ThreddsMetadataBuilder.DateRangeBuilder dateRange) throws ParseException {
        final DateType startDate = new DateType(dateRange.getStartDate(), dateRange.getStartDateFormat(), null);
        final DateType endDate = new DateType(dateRange.getEndDate(), dateRange.getEndDateFormat(), null);
        return new DateRange(startDate, endDate, new TimeDuration(dateRange.getDuration()), null);
    }
}
