// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.net.URI;

public interface Metadata
{
    boolean isContainedContent();
    
    String getTitle();
    
    URI getExternalReference();
    
    String getContent();
}
