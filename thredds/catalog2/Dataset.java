// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import thredds.catalog.ServiceType;
import java.util.List;

public interface Dataset extends DatasetNode
{
    boolean isAccessible();
    
    List<Access> getAccesses();
    
    List<Access> getAccessesByType(final ServiceType p0);
}
