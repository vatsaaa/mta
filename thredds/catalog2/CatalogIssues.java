// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

public interface CatalogIssues
{
    boolean isValid();
    
    int getNumFatalIssues();
    
    int getNumErrorIssues();
    
    int getNumWarningIssues();
    
    String getIssuesMessage();
    
    String getFatalIssuesMessage();
    
    String getErrorIssuesMessage();
    
    String getWarningIssuesMessage();
    
    String toString();
}
