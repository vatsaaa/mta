// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.writer;

public class ThreddsXmlWriterException extends Exception
{
    public ThreddsXmlWriterException() {
    }
    
    public ThreddsXmlWriterException(final String message) {
        super(message);
    }
    
    public ThreddsXmlWriterException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public ThreddsXmlWriterException(final Throwable cause) {
        super(cause);
    }
}
