// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.writer;

import thredds.catalog2.Metadata;
import thredds.catalog2.Dataset;
import java.io.OutputStream;
import java.io.Writer;
import java.io.IOException;
import java.io.File;
import thredds.catalog2.Catalog;

public interface ThreddsXmlWriter
{
    void writeCatalog(final Catalog p0, final File p1) throws ThreddsXmlWriterException, IOException;
    
    void writeCatalog(final Catalog p0, final Writer p1) throws ThreddsXmlWriterException;
    
    void writeCatalog(final Catalog p0, final OutputStream p1) throws ThreddsXmlWriterException;
    
    void writeDataset(final Dataset p0, final File p1) throws ThreddsXmlWriterException, IOException;
    
    void writeDataset(final Dataset p0, final Writer p1) throws ThreddsXmlWriterException;
    
    void writeDataset(final Dataset p0, final OutputStream p1) throws ThreddsXmlWriterException;
    
    void writeMetadata(final Metadata p0, final File p1) throws ThreddsXmlWriterException, IOException;
    
    void writeMetadata(final Metadata p0, final Writer p1) throws ThreddsXmlWriterException;
    
    void writeMetadata(final Metadata p0, final OutputStream p1) throws ThreddsXmlWriterException;
}
