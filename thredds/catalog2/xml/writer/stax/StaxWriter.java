// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.writer.stax;

import javax.xml.stream.XMLStreamException;
import thredds.catalog2.Metadata;
import thredds.catalog2.Dataset;
import java.io.Writer;
import java.io.IOException;
import thredds.catalog2.xml.writer.ThreddsXmlWriterException;
import javax.xml.stream.XMLStreamWriter;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.File;
import thredds.catalog2.Catalog;
import org.slf4j.LoggerFactory;
import javax.xml.stream.XMLOutputFactory;
import org.slf4j.Logger;
import thredds.catalog2.xml.writer.ThreddsXmlWriter;

public class StaxWriter implements ThreddsXmlWriter
{
    private Logger logger;
    private static final String defaultCharEncoding = "UTF-8";
    private static final String indentString = "  ";
    private final XMLOutputFactory factory;
    
    public static String getIndentString(final int nestLevel) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nestLevel; ++i) {
            sb.append("  ");
        }
        return sb.toString();
    }
    
    public StaxWriter() {
        this.logger = LoggerFactory.getLogger(this.getClass());
        (this.factory = XMLOutputFactory.newInstance()).setProperty("javax.xml.stream.isRepairingNamespaces", Boolean.FALSE);
        if (this.factory.isPropertySupported("javax.xml.stream.isPrefixDefaulting")) {
            this.factory.setProperty("javax.xml.stream.isPrefixDefaulting", Boolean.TRUE);
        }
    }
    
    public void writeCatalog(final Catalog catalog, final File file) throws ThreddsXmlWriterException, IOException {
        if (file == null) {
            throw new IllegalArgumentException("File must not be null.");
        }
        final OutputStream os = new FileOutputStream(file);
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(os);
        final CatalogElementWriter catalogWriter = new CatalogElementWriter();
        catalogWriter.writeElement(catalog, xmlStreamWriter, 0);
        os.close();
    }
    
    public void writeCatalog(final Catalog catalog, final Writer writer) throws ThreddsXmlWriterException {
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(writer);
        final CatalogElementWriter catalogWriter = new CatalogElementWriter();
        catalogWriter.writeElement(catalog, xmlStreamWriter, 0);
    }
    
    public void writeCatalog(final Catalog catalog, final OutputStream os) throws ThreddsXmlWriterException {
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(os);
        final CatalogElementWriter catalogWriter = new CatalogElementWriter();
        catalogWriter.writeElement(catalog, xmlStreamWriter, 0);
    }
    
    public void writeDataset(final Dataset dataset, final File file) throws ThreddsXmlWriterException, IOException {
        if (file == null) {
            throw new IllegalArgumentException("File must not be null.");
        }
        final OutputStream os = new FileOutputStream(file);
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(os);
        os.close();
    }
    
    public void writeDataset(final Dataset dataset, final Writer writer) throws ThreddsXmlWriterException {
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(writer);
    }
    
    public void writeDataset(final Dataset dataset, final OutputStream os) throws ThreddsXmlWriterException {
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(os);
    }
    
    public void writeMetadata(final Metadata metadata, final File file) throws ThreddsXmlWriterException, IOException {
        if (file == null) {
            throw new IllegalArgumentException("File must not be null.");
        }
        final OutputStream os = new FileOutputStream(file);
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(os);
        os.close();
    }
    
    public void writeMetadata(final Metadata metadata, final Writer writer) throws ThreddsXmlWriterException {
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(writer);
    }
    
    public void writeMetadata(final Metadata metadata, final OutputStream os) throws ThreddsXmlWriterException {
        final XMLStreamWriter xmlStreamWriter = this.getXmlStreamWriter(os);
    }
    
    private XMLStreamWriter getXmlStreamWriter(final Writer writer) throws ThreddsXmlWriterException {
        if (writer == null) {
            throw new IllegalArgumentException("Writer may not be null.");
        }
        try {
            return this.factory.createXMLStreamWriter(writer);
        }
        catch (XMLStreamException e) {
            this.logger.error("getXmlStreamWriter(): Failed to create XMLStreamWriter: " + e.getMessage());
            throw new ThreddsXmlWriterException("Failed to create XMLStreamWriter: " + e.getMessage(), e);
        }
    }
    
    private XMLStreamWriter getXmlStreamWriter(final OutputStream os) throws ThreddsXmlWriterException {
        if (os == null) {
            throw new IllegalArgumentException("OutputStream must not be null.");
        }
        try {
            return this.factory.createXMLStreamWriter(os, "UTF-8");
        }
        catch (XMLStreamException e) {
            this.logger.error("getXmlStreamWriter(): Failed to create XMLStreamWriter: " + e.getMessage());
            throw new ThreddsXmlWriterException("Failed to create XMLStreamWriter: " + e.getMessage(), e);
        }
    }
}
