// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.writer.stax;

import java.util.Iterator;
import javax.xml.stream.XMLStreamException;
import thredds.catalog2.xml.writer.ThreddsXmlWriterException;
import thredds.catalog2.Property;
import thredds.catalog2.Service;
import thredds.catalog2.xml.names.CatalogNamespace;
import thredds.catalog2.xml.names.CatalogElementNames;
import javax.xml.stream.XMLStreamWriter;
import thredds.catalog2.Catalog;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class CatalogElementWriter implements AbstractElementWriter
{
    private Logger log;
    
    public CatalogElementWriter() {
        this.log = LoggerFactory.getLogger(this.getClass());
    }
    
    public void writeElement(final Catalog catalog, final XMLStreamWriter writer, final int nestLevel) throws ThreddsXmlWriterException {
        final String indentString = StaxWriter.getIndentString(nestLevel);
        try {
            if (nestLevel == 0) {
                writer.writeStartDocument();
                writer.writeCharacters("\n");
            }
            else {
                writer.writeCharacters(indentString);
            }
            final boolean isEmptyElement = catalog.getServices().isEmpty() && catalog.getProperties().isEmpty() && catalog.getDatasets().isEmpty();
            if (isEmptyElement) {
                writer.writeEmptyElement(CatalogElementNames.CatalogElement.toString());
            }
            else {
                writer.writeStartElement(CatalogElementNames.CatalogElement.toString());
            }
            if (nestLevel == 0) {
                writer.writeNamespace(CatalogNamespace.CATALOG_1_0.getStandardPrefix(), CatalogNamespace.CATALOG_1_0.getNamespaceUri());
                writer.writeNamespace(CatalogNamespace.XLINK.getStandardPrefix(), CatalogNamespace.XLINK.getNamespaceUri());
            }
            if (catalog.getName() != null) {
                writer.writeAttribute(CatalogElementNames.CatalogElement_Name.toString(), catalog.getName());
            }
            if (catalog.getVersion() != null) {
                writer.writeAttribute(CatalogElementNames.CatalogElement_Version.toString(), catalog.getVersion());
            }
            if (catalog.getExpires() != null) {
                writer.writeAttribute(CatalogElementNames.CatalogElement_Expires.toString(), catalog.getExpires().toDateTimeStringISO());
            }
            if (catalog.getLastModified() != null) {
                writer.writeAttribute(CatalogElementNames.CatalogElement_LastModified.toString(), catalog.getLastModified().toDateTimeStringISO());
            }
            writer.writeCharacters("\n");
            for (final Service curService : catalog.getServices()) {
                new ServiceElementWriter().writeElement(curService, writer, nestLevel + 1);
            }
            for (final Property curProperty : catalog.getProperties()) {
                new PropertyElementWriter().writeElement(curProperty, writer, nestLevel + 1);
            }
            if (!isEmptyElement) {
                writer.writeCharacters(indentString);
                writer.writeEndElement();
                writer.writeCharacters("\n");
            }
            if (nestLevel == 0) {
                writer.writeEndDocument();
            }
            writer.flush();
            if (nestLevel == 0) {
                writer.close();
            }
        }
        catch (XMLStreamException e) {
            this.log.error("writeElement(): Failed while writing to XMLStreamWriter: " + e.getMessage());
            throw new ThreddsXmlWriterException("Failed while writing to XMLStreamWriter: " + e.getMessage(), e);
        }
    }
}
