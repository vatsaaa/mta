// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.writer.stax;

import javax.xml.stream.XMLStreamException;
import thredds.catalog2.xml.writer.ThreddsXmlWriterException;
import thredds.catalog2.xml.names.CatalogNamespace;
import thredds.catalog2.xml.names.PropertyElementNames;
import javax.xml.stream.XMLStreamWriter;
import thredds.catalog2.Property;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class PropertyElementWriter implements AbstractElementWriter
{
    private Logger log;
    
    public PropertyElementWriter() {
        this.log = LoggerFactory.getLogger(this.getClass());
    }
    
    public void writeElement(final Property property, final XMLStreamWriter writer, final int nestLevel) throws ThreddsXmlWriterException {
        final String indentString = StaxWriter.getIndentString(nestLevel);
        try {
            if (nestLevel == 0) {
                writer.writeStartDocument();
                writer.writeCharacters("\n");
            }
            else {
                writer.writeCharacters(indentString);
            }
            writer.writeEmptyElement(PropertyElementNames.PropertyElement.getLocalPart());
            if (nestLevel == 0) {
                writer.writeNamespace(CatalogNamespace.CATALOG_1_0.getStandardPrefix(), CatalogNamespace.CATALOG_1_0.getNamespaceUri());
                writer.writeNamespace(CatalogNamespace.XLINK.getStandardPrefix(), CatalogNamespace.XLINK.getNamespaceUri());
            }
            writer.writeAttribute(PropertyElementNames.PropertyElement_Name.getLocalPart(), property.getName());
            writer.writeAttribute(PropertyElementNames.PropertyElement_Value.getLocalPart(), property.getValue());
            writer.writeCharacters("\n");
            if (nestLevel == 0) {
                writer.writeEndDocument();
            }
            writer.flush();
            if (nestLevel == 0) {
                writer.close();
            }
        }
        catch (XMLStreamException e) {
            this.log.error("writeElement(): Failed while writing to XMLStreamWriter: " + e.getMessage());
            throw new ThreddsXmlWriterException("Failed while writing to XMLStreamWriter: " + e.getMessage(), e);
        }
    }
}
