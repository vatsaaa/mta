// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.writer.stax;

import java.util.Iterator;
import javax.xml.stream.XMLStreamException;
import thredds.catalog2.xml.writer.ThreddsXmlWriterException;
import thredds.catalog2.Property;
import thredds.catalog2.xml.names.CatalogNamespace;
import thredds.catalog2.xml.names.ServiceElementNames;
import javax.xml.stream.XMLStreamWriter;
import thredds.catalog2.Service;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class ServiceElementWriter implements AbstractElementWriter
{
    private Logger log;
    
    public ServiceElementWriter() {
        this.log = LoggerFactory.getLogger(this.getClass());
    }
    
    public void writeElement(final Service service, final XMLStreamWriter writer, final int nestLevel) throws ThreddsXmlWriterException {
        final String indentString = StaxWriter.getIndentString(nestLevel);
        try {
            if (nestLevel == 0) {
                writer.writeStartDocument();
                writer.writeCharacters("\n");
            }
            else {
                writer.writeCharacters(indentString);
            }
            final boolean isEmptyElement = service.getProperties().isEmpty() && service.getServices().isEmpty();
            if (isEmptyElement) {
                writer.writeEmptyElement(ServiceElementNames.ServiceElement.getLocalPart());
            }
            else {
                writer.writeStartElement(ServiceElementNames.ServiceElement.getLocalPart());
            }
            if (nestLevel == 0) {
                writer.writeNamespace(CatalogNamespace.CATALOG_1_0.getStandardPrefix(), CatalogNamespace.CATALOG_1_0.getNamespaceUri());
                writer.writeNamespace(CatalogNamespace.XLINK.getStandardPrefix(), CatalogNamespace.XLINK.getNamespaceUri());
            }
            writer.writeAttribute(ServiceElementNames.ServiceElement_Name.getLocalPart(), service.getName());
            writer.writeAttribute(ServiceElementNames.ServiceElement_ServiceType.getLocalPart(), service.getType().toString());
            writer.writeAttribute(ServiceElementNames.ServiceElement_Base.getLocalPart(), service.getBaseUri().toString());
            if (service.getSuffix() != null && !service.getSuffix().equals("")) {
                writer.writeAttribute(ServiceElementNames.ServiceElement_Suffix.getLocalPart(), service.getSuffix());
            }
            if (service.getDescription() != null && !service.getDescription().equals("")) {
                writer.writeAttribute(ServiceElementNames.ServiceElement_Description.getLocalPart(), service.getDescription());
            }
            writer.writeCharacters("\n");
            for (final Property curProperty : service.getProperties()) {
                new PropertyElementWriter().writeElement(curProperty, writer, nestLevel + 1);
            }
            for (final Service curService : service.getServices()) {
                new ServiceElementWriter().writeElement(curService, writer, nestLevel + 1);
            }
            if (!isEmptyElement) {
                writer.writeCharacters(indentString);
                writer.writeEndElement();
                writer.writeCharacters("\n");
            }
            if (nestLevel == 0) {
                writer.writeEndDocument();
            }
            writer.flush();
            if (nestLevel == 0) {
                writer.close();
            }
        }
        catch (XMLStreamException e) {
            this.log.error("writeElement(): Failed while writing to XMLStreamWriter: " + e.getMessage());
            throw new ThreddsXmlWriterException("Failed while writing to XMLStreamWriter: " + e.getMessage(), e);
        }
    }
}
