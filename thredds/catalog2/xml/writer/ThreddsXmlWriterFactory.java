// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.writer;

import thredds.catalog2.xml.writer.stax.StaxWriter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class ThreddsXmlWriterFactory
{
    private Logger logger;
    
    private ThreddsXmlWriterFactory() {
        this.logger = LoggerFactory.getLogger(ThreddsXmlWriterFactory.class);
    }
    
    public static ThreddsXmlWriterFactory newInstance() {
        return new ThreddsXmlWriterFactory();
    }
    
    public ThreddsXmlWriter createThreddsXmlWriter() {
        return new StaxWriter();
    }
}
