// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser;

import thredds.catalog2.builder.CatalogBuilder;
import java.io.InputStream;
import java.io.Reader;
import java.io.File;
import thredds.catalog2.Catalog;
import java.net.URI;

public interface ThreddsXmlParser
{
    Catalog parse(final URI p0) throws ThreddsXmlParserException;
    
    Catalog parse(final File p0, final URI p1) throws ThreddsXmlParserException;
    
    Catalog parse(final Reader p0, final URI p1) throws ThreddsXmlParserException;
    
    Catalog parse(final InputStream p0, final URI p1) throws ThreddsXmlParserException;
    
    CatalogBuilder parseIntoBuilder(final URI p0) throws ThreddsXmlParserException;
    
    CatalogBuilder parseIntoBuilder(final File p0, final URI p1) throws ThreddsXmlParserException;
    
    CatalogBuilder parseIntoBuilder(final Reader p0, final URI p1) throws ThreddsXmlParserException;
    
    CatalogBuilder parseIntoBuilder(final InputStream p0, final URI p1) throws ThreddsXmlParserException;
}
