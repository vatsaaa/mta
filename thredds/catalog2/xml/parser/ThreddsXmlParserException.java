// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser;

import java.util.Iterator;
import java.util.Collections;
import java.util.List;

public class ThreddsXmlParserException extends Exception
{
    private final List<ThreddsXmlParserIssue> issues;
    
    public ThreddsXmlParserException(final ThreddsXmlParserIssue issue) {
        this.issues = Collections.singletonList(issue);
    }
    
    public ThreddsXmlParserException(final List<ThreddsXmlParserIssue> issues) {
        this.issues = issues;
    }
    
    public ThreddsXmlParserException(final String message) {
        super(message);
        this.issues = Collections.emptyList();
    }
    
    public ThreddsXmlParserException(final String message, final Throwable cause) {
        super(message, cause);
        this.issues = Collections.emptyList();
    }
    
    public List<ThreddsXmlParserIssue> getSources() {
        return Collections.unmodifiableList((List<? extends ThreddsXmlParserIssue>)this.issues);
    }
    
    @Override
    public String getMessage() {
        if (this.issues == null || this.issues.isEmpty()) {
            return super.getMessage();
        }
        final StringBuilder sb = new StringBuilder();
        for (final ThreddsXmlParserIssue txpi : this.issues) {
            sb.append(txpi.getMessage()).append("\n");
        }
        return sb.toString();
    }
}
