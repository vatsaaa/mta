// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser;

import thredds.catalog2.xml.parser.stax.StaxThreddsXmlParser;

public class ThreddsXmlParserFactory
{
    private boolean wantValidating;
    
    private ThreddsXmlParserFactory() {
        this.wantValidating = false;
    }
    
    public static ThreddsXmlParserFactory newFactory() {
        return new ThreddsXmlParserFactory();
    }
    
    public boolean getWantValidating() {
        return this.wantValidating;
    }
    
    public void setWantValidating(final boolean wantValidating) {
        this.wantValidating = wantValidating;
    }
    
    public ThreddsXmlParser getCatalogParser() {
        final StaxThreddsXmlParser catParser = StaxThreddsXmlParser.newInstance();
        return catParser;
    }
}
