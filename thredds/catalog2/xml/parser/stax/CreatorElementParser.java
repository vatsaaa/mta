// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.Attribute;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

class CreatorElementParser extends AbstractElementParser
{
    private final ThreddsMetadataBuilder parentBuilder;
    private ThreddsMetadataBuilder.ContributorBuilder selfBuilder;
    private final NameElementParser.Factory nameElemParserFactory;
    private final ContactElementParser.Factory contactElemParserFactory;
    
    private CreatorElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
        super(elementName, reader, builderFactory);
        this.parentBuilder = parentBuilder;
        this.nameElemParserFactory = new NameElementParser.Factory();
        this.contactElemParserFactory = new ContactElementParser.Factory();
    }
    
    @Override
    ThreddsBuilder getSelfBuilder() {
        return null;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        this.getNextEventIfStartElementIsMine();
        this.selfBuilder = this.parentBuilder.addCreator();
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.nameElemParserFactory.isEventMyStartElement(startElement)) {
            final NameElementParser elementParser = this.nameElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            elementParser.parse();
        }
        else {
            if (!this.contactElemParserFactory.isEventMyStartElement(startElement)) {
                final String unexpectedElement = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
                final ThreddsXmlParserIssue issue = new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.ERROR, "Unrecognized element: " + unexpectedElement, this.selfBuilder, null);
                throw new ThreddsXmlParserException(issue);
            }
            final ContactElementParser elementParser2 = this.contactElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            elementParser2.parse();
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class NameElementParser extends AbstractElementParser
    {
        private final ThreddsMetadataBuilder.ContributorBuilder parentBuilder;
        
        private NameElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
            super(elementName, reader, builderFactory);
            this.parentBuilder = (ThreddsMetadataBuilder.ContributorBuilder)parentBuilder;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            final StartElement startElement = this.getNextEventIfStartElementIsMine();
            final Attribute namingAuthAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.CreatorElement_NameElement_NamingAuthority);
            final String namingAuth = (namingAuthAtt != null) ? namingAuthAtt.getValue() : null;
            final String name = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
            this.parentBuilder.setName(name);
            this.parentBuilder.setNamingAuthority(namingAuth);
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
            final String unexpectedElement = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
            final ThreddsXmlParserIssue issue = new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.ERROR, "Unrecognized element: " + unexpectedElement, this.parentBuilder, null);
            throw new ThreddsXmlParserException(issue);
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.CreatorElement_NameElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            NameElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
                return new NameElementParser(this.elementName, reader, builderFactory, parentBuilder);
            }
        }
    }
    
    static class ContactElementParser extends AbstractElementParser
    {
        private final ThreddsMetadataBuilder.ContributorBuilder parentBuilder;
        
        ContactElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
            super(elementName, reader, builderFactory);
            this.parentBuilder = (ThreddsMetadataBuilder.ContributorBuilder)parentBuilder;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            final StartElement startElement = this.getNextEventIfStartElementIsMine();
            final Attribute emailAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.CreatorElement_ContactElement_Email);
            final String emailAuth = (emailAtt != null) ? emailAtt.getValue() : null;
            final Attribute urlAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.CreatorElement_ContactElement_Url);
            final String url = (urlAtt != null) ? urlAtt.getValue() : null;
            this.parentBuilder.setEmail(emailAuth);
            this.parentBuilder.setWebPage(url);
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
            final String unexpectedElement = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
            final ThreddsXmlParserIssue issue = new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.ERROR, "Unrecognized element: " + unexpectedElement, this.parentBuilder, null);
            throw new ThreddsXmlParserException(issue);
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.CreatorElement_ContactElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            ContactElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
                return new ContactElementParser(this.elementName, reader, builderFactory, parentBuilder);
            }
        }
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = ThreddsMetadataElementNames.CreatorElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        CreatorElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
            return new CreatorElementParser(this.elementName, reader, builderFactory, parentBuilder, null);
        }
    }
}
