// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.builder.ThreddsBuilder;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import java.net.URISyntaxException;
import java.net.URI;
import java.text.ParseException;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import ucar.nc2.units.DateType;
import thredds.catalog2.xml.names.CatalogElementNames;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.CatalogBuilder;

class CatalogElementParser extends AbstractElementParser
{
    private final String docBaseUriString;
    private final PropertyElementParser.Factory propertyElemParserFactory;
    private final ServiceElementParser.Factory serviceElemParserFactory;
    private final DatasetElementParser.Factory datasetElemParserFactory;
    CatalogBuilder selfBuilder;
    
    private CatalogElementParser(final QName elementName, final String docBaseUriString, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory) {
        super(elementName, reader, builderFactory);
        this.docBaseUriString = docBaseUriString;
        this.propertyElemParserFactory = new PropertyElementParser.Factory();
        this.serviceElemParserFactory = new ServiceElementParser.Factory();
        this.datasetElemParserFactory = new DatasetElementParser.Factory();
    }
    
    @Override
    CatalogBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute nameAtt = startElement.getAttributeByName(CatalogElementNames.CatalogElement_Name);
        final String nameString = (nameAtt != null) ? nameAtt.getValue() : null;
        final Attribute versionAtt = startElement.getAttributeByName(CatalogElementNames.CatalogElement_Version);
        final String versionString = (versionAtt != null) ? versionAtt.getValue() : null;
        final Attribute expiresAtt = startElement.getAttributeByName(CatalogElementNames.CatalogElement_Expires);
        final String expiresString = (expiresAtt != null) ? expiresAtt.getValue() : null;
        DateType expires = null;
        try {
            expires = ((expiresString != null) ? new DateType(expiresString, null, null) : null);
        }
        catch (ParseException e) {
            final String msg = "Failed to parse catalog expires date [" + expiresString + "].";
            final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForException(msg, this.reader, e);
            this.log.warn("parseStartElement(): " + issue.getMessage(), e);
            throw new ThreddsXmlParserException(issue);
        }
        final Attribute lastModifiedAtt = startElement.getAttributeByName(CatalogElementNames.CatalogElement_LastModified);
        final String lastModifiedString = (lastModifiedAtt != null) ? lastModifiedAtt.getValue() : null;
        DateType lastModified = null;
        try {
            lastModified = ((lastModifiedString != null) ? new DateType(lastModifiedString, null, null) : null);
        }
        catch (ParseException e2) {
            final String msg2 = "Failed to parse catalog lastModified date [" + lastModifiedString + "].";
            final ThreddsXmlParserIssue issue2 = StaxThreddsXmlParserUtils.createIssueForException(msg2, this.reader, e2);
            this.log.warn("parseStartElement(): " + issue2.getMessage(), e2);
            throw new ThreddsXmlParserException(issue2);
        }
        URI docBaseUri = null;
        try {
            docBaseUri = new URI(this.docBaseUriString);
        }
        catch (URISyntaxException e3) {
            final String msg3 = "Bad catalog base URI [" + this.docBaseUriString + "].";
            final ThreddsXmlParserIssue issue3 = StaxThreddsXmlParserUtils.createIssueForException(msg3, this.reader, e3);
            this.log.warn("parseStartElement(): " + issue3.getMessage(), e3);
            throw new ThreddsXmlParserException(issue3);
        }
        this.selfBuilder = this.builderFactory.newCatalogBuilder(nameString, docBaseUri, versionString, expires, lastModified);
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.serviceElemParserFactory.isEventMyStartElement(startElement)) {
            final ServiceElementParser serviceElemParser = this.serviceElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            serviceElemParser.parse();
        }
        else if (this.propertyElemParserFactory.isEventMyStartElement(startElement)) {
            final PropertyElementParser parser = this.propertyElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            parser.parse();
        }
        else if (this.datasetElemParserFactory.isEventMyStartElement(startElement)) {
            final DatasetElementParser parser2 = this.datasetElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder, null);
            parser2.parse();
        }
        else {
            StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = CatalogElementNames.CatalogElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        CatalogElementParser getNewParser(final String docBaseUriString, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory) {
            return new CatalogElementParser(this.elementName, docBaseUriString, reader, builderFactory, null);
        }
    }
}
