// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.Reader;
import java.io.File;
import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.Catalog;
import java.net.URI;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.simpleImpl.ThreddsBuilderFactoryImpl;
import thredds.catalog2.builder.CatalogBuilder;
import javax.xml.transform.Source;
import javax.xml.stream.XMLInputFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import thredds.catalog2.xml.parser.ThreddsXmlParser;

public class StaxThreddsXmlParser implements ThreddsXmlParser
{
    private Logger log;
    private CatalogElementParser.Factory catElemParserFactory;
    
    public static StaxThreddsXmlParser newInstance() {
        return new StaxThreddsXmlParser();
    }
    
    private StaxThreddsXmlParser() {
        this.log = LoggerFactory.getLogger(this.getClass());
        this.catElemParserFactory = new CatalogElementParser.Factory();
    }
    
    private XMLInputFactory getFactory() {
        final XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        factory.setProperty("javax.xml.stream.supportDTD", Boolean.FALSE);
        return factory;
    }
    
    private CatalogBuilder readCatalogXML(final Source source) throws ThreddsXmlParserException {
        try {
            final XMLInputFactory factory = this.getFactory();
            final XMLEventReader eventReader = factory.createXMLEventReader(source);
            final ThreddsBuilderFactory catBuilderFac = new ThreddsBuilderFactoryImpl();
            ThreddsBuilder threddsBuilder = null;
            while (eventReader.hasNext()) {
                final XMLEvent event = eventReader.peek();
                if (event.isEndDocument()) {
                    eventReader.next();
                    break;
                }
                if (event.isStartDocument()) {
                    eventReader.next();
                }
                else if (event.isStartElement()) {
                    if (this.catElemParserFactory.isEventMyStartElement(event.asStartElement())) {
                        final CatalogElementParser catElemParser = this.catElemParserFactory.getNewParser(source.getSystemId(), eventReader, catBuilderFac);
                        threddsBuilder = catElemParser.parse();
                    }
                    else {
                        StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(eventReader);
                        this.log.warn("readCatalogXML(): Unrecognized start element [" + event.asStartElement().getName() + "].");
                    }
                }
                else {
                    if (event.isEndElement()) {
                        this.log.error("readCatalogXML(): Unrecognized end element [" + event.asEndElement().getName() + "].");
                        break;
                    }
                    this.log.debug("readCatalogXML(): Unhandled event [" + event.getLocation() + "--" + event + "].");
                    eventReader.next();
                }
            }
            eventReader.close();
            if (threddsBuilder == null) {
                return null;
            }
            return (CatalogBuilder)threddsBuilder;
        }
        catch (XMLStreamException e) {
            this.log.error("readCatalogXML(): Failed to parse catalog document: " + e.getMessage(), e);
            throw new ThreddsXmlParserException("Failed to parse catalog document: " + e.getMessage(), e);
        }
    }
    
    public Catalog parse(final URI documentUri) throws ThreddsXmlParserException {
        final Source s = StaxThreddsXmlParserUtils.getSourceFromUri(documentUri);
        try {
            return this.readCatalogXML(s).build();
        }
        catch (BuilderException e) {
            this.log.error("parse(): Failed to parse catalog document.", e);
            throw new ThreddsXmlParserException("Failed to parse catalog document.", e);
        }
    }
    
    public Catalog parse(final File file, final URI docBaseUri) throws ThreddsXmlParserException {
        try {
            return this.parseIntoBuilder(file, docBaseUri).build();
        }
        catch (BuilderException e) {
            this.log.error("parse(): Failed to parse catalog document: " + e.getMessage(), e);
            throw new ThreddsXmlParserException("Failed to parse catalog document: " + e.getMessage(), e);
        }
    }
    
    public Catalog parse(final Reader reader, final URI docBaseUri) throws ThreddsXmlParserException {
        try {
            return this.parseIntoBuilder(reader, docBaseUri).build();
        }
        catch (BuilderException e) {
            this.log.error("parse(): Failed to parse catalog document: " + e.getMessage(), e);
            throw new ThreddsXmlParserException("Failed to parse catalog document: " + e.getMessage(), e);
        }
    }
    
    public Catalog parse(final InputStream is, final URI docBaseUri) throws ThreddsXmlParserException {
        try {
            return this.parseIntoBuilder(is, docBaseUri).build();
        }
        catch (BuilderException e) {
            this.log.error("parse(): Failed to parse catalog document: " + e.getMessage(), e);
            throw new ThreddsXmlParserException("Failed to parse catalog document: " + e.getMessage(), e);
        }
    }
    
    public CatalogBuilder parseIntoBuilder(final URI documentUri) throws ThreddsXmlParserException {
        final Source s = StaxThreddsXmlParserUtils.getSourceFromUri(documentUri);
        return this.readCatalogXML(s);
    }
    
    public CatalogBuilder parseIntoBuilder(final File file, final URI docBaseUri) throws ThreddsXmlParserException {
        final Source s = StaxThreddsXmlParserUtils.getSourceFromFile(file, docBaseUri);
        return this.readCatalogXML(s);
    }
    
    public CatalogBuilder parseIntoBuilder(final Reader reader, final URI docBaseUri) throws ThreddsXmlParserException {
        final Source source = new StreamSource(reader, docBaseUri.toString());
        return this.readCatalogXML(source);
    }
    
    public CatalogBuilder parseIntoBuilder(final InputStream is, final URI docBaseUri) throws ThreddsXmlParserException {
        final Source source = new StreamSource(is, docBaseUri.toString());
        return this.readCatalogXML(source);
    }
}
