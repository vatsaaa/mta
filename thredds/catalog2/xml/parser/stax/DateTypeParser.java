// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;

class DateTypeParser
{
    private QName elementName;
    private String format;
    private String type;
    private String value;
    
    private DateTypeParser(final QName elementName) {
        this.elementName = elementName;
    }
    
    String getFormat() {
        return this.format;
    }
    
    String getType() {
        return this.type;
    }
    
    String getValue() {
        return this.value;
    }
    
    void parseElement(final XMLEventReader reader) throws ThreddsXmlParserException {
        final StartElement startElement = StaxThreddsXmlParserUtils.readNextEventCheckItIsStartElementWithExpectedName(reader, this.elementName);
        final Attribute formatAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.DateType_Format);
        this.format = ((formatAtt != null) ? formatAtt.getValue() : null);
        final Attribute typeAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.DateType_Type);
        this.type = ((typeAtt != null) ? typeAtt.getValue() : null);
        this.value = StaxThreddsXmlParserUtils.getCharacterContent(reader, this.elementName);
        StaxThreddsXmlParserUtils.readNextEventCheckItIsEndElementWithExpectedName(reader, this.elementName);
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory(final QName elementName) {
            this.elementName = elementName;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        DateTypeParser getNewDateTypeParser() {
            return new DateTypeParser(this.elementName, null);
        }
    }
}
