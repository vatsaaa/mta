// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ServiceBuilder;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog.DataFormatType;
import thredds.catalog2.xml.names.AccessElementNames;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.AccessBuilder;
import thredds.catalog2.builder.DatasetBuilder;

class AccessElementParser extends AbstractElementParser
{
    private final DatasetBuilder parentDatasetBuilder;
    private AccessBuilder selfBuilder;
    
    private AccessElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetBuilder parentDatasetBuilder) {
        super(elementName, reader, builderFactory);
        this.parentDatasetBuilder = parentDatasetBuilder;
    }
    
    @Override
    AccessBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        if (this.parentDatasetBuilder != null) {
            this.selfBuilder = this.parentDatasetBuilder.addAccessBuilder();
            final Attribute serviceNameAtt = startElement.getAttributeByName(AccessElementNames.AccessElement_ServiceName);
            if (serviceNameAtt != null) {
                final String serviceName = serviceNameAtt.getValue();
                final ServiceBuilder serviceBuilder = this.parentDatasetBuilder.getParentCatalogBuilder().findServiceBuilderByNameGlobally(serviceName);
                this.selfBuilder.setServiceBuilder(serviceBuilder);
            }
            final Attribute urlPathAtt = startElement.getAttributeByName(AccessElementNames.AccessElement_UrlPath);
            final String urlPath = urlPathAtt.getValue();
            this.selfBuilder.setUrlPath(urlPath);
            final Attribute dataFormatAtt = startElement.getAttributeByName(AccessElementNames.AccessElement_DataFormat);
            if (dataFormatAtt != null) {
                this.selfBuilder.setDataFormat(DataFormatType.getType(dataFormatAtt.getValue()));
            }
            return;
        }
        throw new ThreddsXmlParserException("");
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = AccessElementNames.AccessElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        AccessElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetBuilder parentDatasetBuilder) {
            return new AccessElementParser(this.elementName, reader, builderFactory, parentDatasetBuilder, null);
        }
    }
}
