// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.builder.ThreddsBuilder;
import javax.xml.stream.events.XMLEvent;

interface ElementParser
{
    boolean isSelfElement(final XMLEvent p0);
    
    ThreddsBuilder parse() throws ThreddsXmlParserException;
    
    void parseStartElement() throws ThreddsXmlParserException;
    
    void handleChildStartElement() throws ThreddsXmlParserException;
    
    void postProcessingAfterEndElement() throws ThreddsXmlParserException;
    
    ThreddsBuilder getSelfBuilder();
    
    public interface Factory
    {
        boolean isEventMyStartElement(final XMLEvent p0);
        
        ElementParser getNewParser(final String p0, final XMLEventReader p1, final ThreddsBuilderFactory p2);
        
        ElementParser getNewParser(final XMLEventReader p0, final ThreddsBuilderFactory p1, final ThreddsBuilder p2);
    }
}
