// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.builder.ThreddsBuilder;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import java.net.URISyntaxException;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import java.net.URI;
import thredds.catalog2.xml.names.CatalogRefElementNames;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.CatalogRefBuilder;
import thredds.catalog2.builder.DatasetNodeBuilder;
import thredds.catalog2.builder.CatalogBuilder;

class CatalogRefElementParser extends AbstractElementParser
{
    private final CatalogBuilder parentCatalogBuilder;
    private final DatasetNodeBuilder parentDatasetNodeBuilder;
    private final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper;
    private DatasetNodeElementParserHelper datasetNodeElementParserHelper;
    private CatalogRefBuilder selfBuilder;
    
    private CatalogRefElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder parentCatalogBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
        super(elementName, reader, builderFactory);
        this.parentCatalogBuilder = parentCatalogBuilder;
        this.parentDatasetNodeBuilder = null;
        this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
    }
    
    private CatalogRefElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
        super(elementName, reader, builderFactory);
        this.parentCatalogBuilder = null;
        this.parentDatasetNodeBuilder = parentDatasetNodeBuilder;
        this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
    }
    
    @Override
    CatalogRefBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute titleAtt = startElement.getAttributeByName(CatalogRefElementNames.CatalogRefElement_XlinkTitle);
        final String title = titleAtt.getValue();
        final Attribute hrefAtt = startElement.getAttributeByName(CatalogRefElementNames.CatalogRefElement_XlinkHref);
        final String href = hrefAtt.getValue();
        URI hrefUri = null;
        try {
            hrefUri = new URI(href);
        }
        catch (URISyntaxException e) {
            this.log.error("parseElement(): Bad catalog base URI [" + href + "]: " + e.getMessage(), e);
            throw new ThreddsXmlParserException("Bad catalog base URI [" + href + "]: " + e.getMessage(), e);
        }
        if (this.parentCatalogBuilder != null) {
            this.selfBuilder = this.parentCatalogBuilder.addCatalogRef(title, hrefUri);
        }
        else {
            if (this.parentDatasetNodeBuilder == null) {
                throw new ThreddsXmlParserException("");
            }
            this.selfBuilder = this.parentDatasetNodeBuilder.addCatalogRef(title, hrefUri);
        }
        (this.datasetNodeElementParserHelper = new DatasetNodeElementParserHelper(this.parentDatasetNodeElementParserHelper, this.selfBuilder, this.builderFactory)).parseStartElementIdAttribute(startElement);
        this.datasetNodeElementParserHelper.parseStartElementIdAuthorityAttribute(startElement);
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.datasetNodeElementParserHelper.handleBasicChildStartElement(startElement, this.reader, this.selfBuilder)) {
            return;
        }
        StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        this.datasetNodeElementParserHelper.postProcessingAfterEndElement();
        this.datasetNodeElementParserHelper.addFinalThreddsMetadataToDatasetNodeBuilder(this.selfBuilder);
        this.datasetNodeElementParserHelper.addFinalMetadataToDatasetNodeBuilder(this.selfBuilder);
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = CatalogRefElementNames.CatalogRefElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        CatalogRefElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder parentCatalogBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
            return new CatalogRefElementParser(this.elementName, reader, builderFactory, parentCatalogBuilder, parentDatasetNodeElementParserHelper, null);
        }
        
        CatalogRefElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
            return new CatalogRefElementParser(this.elementName, reader, builderFactory, parentDatasetNodeBuilder, parentDatasetNodeElementParserHelper, null);
        }
    }
}
