// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.Attribute;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import thredds.catalog2.ThreddsMetadata;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;
import thredds.catalog2.builder.DatasetNodeBuilder;

class ThreddsMetadataElementParser extends AbstractElementParser
{
    private final DatasetNodeBuilder parentDatasetNodeBuilder;
    private final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper;
    private final boolean inheritedByDescendants;
    private final ThreddsMetadataBuilder selfBuilder;
    private AbstractElementParser delegate;
    private final ServiceNameElementParser.Factory serviceNameParserFactory;
    private final DataFormatElementParser.Factory dataFormatParserFactory;
    private final DataTypeElementParser.Factory dataTypeParserFactory;
    private final DateElementParser.Factory dateParserFactory;
    private final AuthorityElementParser.Factory authorityParserFactory;
    private final DocumentationElementParser.Factory documentationParserFactory;
    private final KeyphraseElementParser.Factory keyphraseParserFactory;
    private final ProjectElementParser.Factory projectNameParserFactory;
    private final CreatorElementParser.Factory creatorParserFactory;
    private final PublisherElementParser.Factory publisherParserFactory;
    private final ContributorElementParser.Factory contribParserFactory;
    private final TimeCoverageElementParser.Factory timeCovParserFactory;
    private final VariableGroupElementParser.Factory variableGroupParserFactory;
    
    private ThreddsMetadataElementParser(final QName elementName, final ServiceNameElementParser.Factory serviceNameParserFactory, final DataFormatElementParser.Factory dataFormatParserFactory, final DataTypeElementParser.Factory dataTypeParserFactory, final DateElementParser.Factory dateParserFactory, final AuthorityElementParser.Factory authorityParserFactory, final DocumentationElementParser.Factory documentationParserFactory, final KeyphraseElementParser.Factory keyphraseParserFactory, final ProjectElementParser.Factory projectNameParserFactory, final CreatorElementParser.Factory creatorParserFactory, final PublisherElementParser.Factory publisherParserFactory, final ContributorElementParser.Factory contribParserFactory, final TimeCoverageElementParser.Factory timeCovParserFactory, final VariableGroupElementParser.Factory variableGroupParserFactory, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper, final boolean inheritedByDescendants) {
        super(elementName, reader, builderFactory);
        this.delegate = null;
        this.parentDatasetNodeBuilder = parentDatasetNodeBuilder;
        this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
        this.inheritedByDescendants = inheritedByDescendants;
        this.selfBuilder = builderFactory.newThreddsMetadataBuilder();
        this.serviceNameParserFactory = serviceNameParserFactory;
        this.dataFormatParserFactory = dataFormatParserFactory;
        this.dataTypeParserFactory = dataTypeParserFactory;
        this.dateParserFactory = dateParserFactory;
        this.authorityParserFactory = authorityParserFactory;
        this.documentationParserFactory = documentationParserFactory;
        this.keyphraseParserFactory = keyphraseParserFactory;
        this.projectNameParserFactory = projectNameParserFactory;
        this.creatorParserFactory = creatorParserFactory;
        this.publisherParserFactory = publisherParserFactory;
        this.contribParserFactory = contribParserFactory;
        this.variableGroupParserFactory = variableGroupParserFactory;
        this.timeCovParserFactory = timeCovParserFactory;
    }
    
    @Override
    boolean isSelfElement(final XMLEvent event) {
        return this.delegate.isSelfElement(event);
    }
    
    @Override
    ThreddsMetadataBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.serviceNameParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.serviceNameParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder, this.parentDatasetNodeElementParserHelper, this.inheritedByDescendants);
        }
        else if (this.dataFormatParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.dataFormatParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.dataTypeParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.dataTypeParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.dateParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.dateParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.authorityParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.authorityParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder, this.parentDatasetNodeElementParserHelper, this.inheritedByDescendants);
        }
        else if (this.documentationParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.documentationParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.keyphraseParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.keyphraseParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.projectNameParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.projectNameParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.creatorParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.creatorParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.publisherParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.publisherParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.contribParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.contribParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else if (this.timeCovParserFactory.isEventMyStartElement(startElement)) {
            this.delegate = this.timeCovParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        else {
            if (!this.variableGroupParserFactory.isEventMyStartElement(startElement)) {
                throw new ThreddsXmlParserException("Not a recognized ThreddsMetadata child element [" + startElement.getName().getLocalPart() + "].");
            }
            this.delegate = this.variableGroupParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
        }
        this.delegate.parseStartElement();
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        if (this.delegate == null) {
            throw new IllegalStateException("Proxy delegate is null: " + StaxThreddsXmlParserUtils.getLocationInfo(this.reader));
        }
        this.delegate.handleChildStartElement();
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        if (this.delegate == null) {
            throw new IllegalStateException("Proxy delegate is null: " + StaxThreddsXmlParserUtils.getLocationInfo(this.reader));
        }
        this.delegate.postProcessingAfterEndElement();
    }
    
    static class Factory
    {
        private final QName elementName;
        private final ServiceNameElementParser.Factory serviceNameParserFactory;
        private final DataFormatElementParser.Factory dataFormatParserFactory;
        private final DataTypeElementParser.Factory dataTypeParserFactory;
        private final DateElementParser.Factory dateParserFactory;
        private final AuthorityElementParser.Factory authorityParserFactory;
        private final DocumentationElementParser.Factory documentationParserFactory;
        private final KeyphraseElementParser.Factory keyphraseParserFactory;
        private final ProjectElementParser.Factory projectNameParserFactory;
        private final CreatorElementParser.Factory creatorParserFactory;
        private final PublisherElementParser.Factory publisherParserFactory;
        private final ContributorElementParser.Factory contribParserFactory;
        private final TimeCoverageElementParser.Factory timeCovParserFactory;
        private final VariableGroupElementParser.Factory varGroupParserFactory;
        
        Factory() {
            this.elementName = ThreddsMetadataElementNames.ThreddsMetadataElement;
            this.serviceNameParserFactory = new ServiceNameElementParser.Factory();
            this.dataFormatParserFactory = new DataFormatElementParser.Factory();
            this.dataTypeParserFactory = new DataTypeElementParser.Factory();
            this.dateParserFactory = new DateElementParser.Factory();
            this.authorityParserFactory = new AuthorityElementParser.Factory();
            this.documentationParserFactory = new DocumentationElementParser.Factory();
            this.keyphraseParserFactory = new KeyphraseElementParser.Factory();
            this.projectNameParserFactory = new ProjectElementParser.Factory();
            this.creatorParserFactory = new CreatorElementParser.Factory();
            this.publisherParserFactory = new PublisherElementParser.Factory();
            this.contribParserFactory = new ContributorElementParser.Factory();
            this.timeCovParserFactory = new TimeCoverageElementParser.Factory();
            this.varGroupParserFactory = new VariableGroupElementParser.Factory();
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return this.serviceNameParserFactory.isEventMyStartElement(event) || this.dataFormatParserFactory.isEventMyStartElement(event) || this.dataTypeParserFactory.isEventMyStartElement(event) || this.dateParserFactory.isEventMyStartElement(event) || this.authorityParserFactory.isEventMyStartElement(event) || this.documentationParserFactory.isEventMyStartElement(event) || this.keyphraseParserFactory.isEventMyStartElement(event) || this.projectNameParserFactory.isEventMyStartElement(event) || this.creatorParserFactory.isEventMyStartElement(event) || this.publisherParserFactory.isEventMyStartElement(event) || this.contribParserFactory.isEventMyStartElement(event) || this.timeCovParserFactory.isEventMyStartElement(event) || this.varGroupParserFactory.isEventMyStartElement(event);
        }
        
        ThreddsMetadataElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper, final boolean inheritedByDescendants) {
            return new ThreddsMetadataElementParser(this.elementName, this.serviceNameParserFactory, this.dataFormatParserFactory, this.dataTypeParserFactory, this.dateParserFactory, this.authorityParserFactory, this.documentationParserFactory, this.keyphraseParserFactory, this.projectNameParserFactory, this.creatorParserFactory, this.publisherParserFactory, this.contribParserFactory, this.timeCovParserFactory, this.varGroupParserFactory, reader, builderFactory, parentDatasetNodeBuilder, parentDatasetNodeElementParserHelper, inheritedByDescendants, null);
        }
    }
    
    static class ServiceNameElementParser extends AbstractElementParser
    {
        private Logger log;
        private final ThreddsMetadataBuilder threddsMetadataBuilder;
        private final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper;
        private final boolean inheritedByDescendants;
        private String serviceName;
        
        private ServiceNameElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder threddsMetadataBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper, final boolean inheritedByDescendants) {
            super(elementName, reader, builderFactory);
            this.log = LoggerFactory.getLogger(this.getClass());
            this.threddsMetadataBuilder = threddsMetadataBuilder;
            this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
            this.inheritedByDescendants = inheritedByDescendants;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            this.getNextEventIfStartElementIsMine();
            this.serviceName = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
            this.parentDatasetNodeElementParserHelper.setDefaultServiceNameSpecifiedInSelf(this.serviceName);
            if (this.inheritedByDescendants) {
                this.parentDatasetNodeElementParserHelper.setDefaultServiceNameToBeInheritedByDescendants(this.serviceName);
            }
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.ServiceNameElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            ServiceNameElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder threddsMetadataBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper, final boolean inheritedByDescendants) {
                return new ServiceNameElementParser(this.elementName, reader, builderFactory, threddsMetadataBuilder, parentDatasetNodeElementParserHelper, inheritedByDescendants);
            }
        }
    }
    
    static class DataFormatElementParser extends AbstractElementParser
    {
        private final ThreddsMetadataBuilder threddsMetadataBuilder;
        
        private DataFormatElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder threddsMetadataBuilder) {
            super(elementName, reader, builderFactory);
            this.threddsMetadataBuilder = threddsMetadataBuilder;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            final StartElement startElement = this.getNextEventIfStartElementIsMine();
            final String dataFormat = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, ThreddsMetadataElementNames.DataFormatElement);
            this.threddsMetadataBuilder.setDataFormat(dataFormat);
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.DataFormatElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            DataFormatElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
                return new DataFormatElementParser(this.elementName, reader, builderFactory, parentBuilder);
            }
        }
    }
    
    static class DataTypeElementParser extends AbstractElementParser
    {
        private final ThreddsMetadataBuilder threddsMetadataBuilder;
        
        private DataTypeElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder threddsMetadataBuilder) {
            super(elementName, reader, builderFactory);
            this.threddsMetadataBuilder = threddsMetadataBuilder;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            final StartElement startElement = this.getNextEventIfStartElementIsMine();
            final String dataType = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, ThreddsMetadataElementNames.DataTypeElement);
            this.threddsMetadataBuilder.setDataType(dataType);
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.DataTypeElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            DataTypeElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
                return new DataTypeElementParser(this.elementName, reader, builderFactory, parentBuilder);
            }
        }
    }
    
    static class DateElementParser extends AbstractElementParser
    {
        private final ThreddsMetadataBuilder threddsMetadataBuilder;
        
        private DateElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder threddsMetadataBuilder) {
            super(elementName, reader, builderFactory);
            this.threddsMetadataBuilder = threddsMetadataBuilder;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            final StartElement startElement = this.getNextEventIfStartElementIsMine();
            final Attribute typeAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.DateElement_Type);
            final String typeString = (typeAtt != null) ? typeAtt.getValue() : null;
            final Attribute formatAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.DateElement_Format);
            final String formatString = (formatAtt != null) ? formatAtt.getValue() : null;
            final String date = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
            final ThreddsMetadata.DatePointType type = ThreddsMetadata.DatePointType.getTypeForLabel(typeString);
            if (type.equals(ThreddsMetadata.DatePointType.Untyped) || type.equals(ThreddsMetadata.DatePointType.Other)) {
                this.threddsMetadataBuilder.addOtherDatePointBuilder(date, formatString, typeString);
            }
            else if (type.equals(ThreddsMetadata.DatePointType.Created)) {
                this.threddsMetadataBuilder.setCreatedDatePointBuilder(date, formatString);
            }
            else if (type.equals(ThreddsMetadata.DatePointType.Modified)) {
                this.threddsMetadataBuilder.setModifiedDatePointBuilder(date, formatString);
            }
            else if (type.equals(ThreddsMetadata.DatePointType.Valid)) {
                this.threddsMetadataBuilder.setValidDatePointBuilder(date, formatString);
            }
            else if (type.equals(ThreddsMetadata.DatePointType.Issued)) {
                this.threddsMetadataBuilder.setIssuedDatePointBuilder(date, formatString);
            }
            else if (type.equals(ThreddsMetadata.DatePointType.Available)) {
                this.threddsMetadataBuilder.setAvailableDatePointBuilder(date, formatString);
            }
            else if (type.equals(ThreddsMetadata.DatePointType.MetadataCreated)) {
                this.threddsMetadataBuilder.setMetadataCreatedDatePointBuilder(date, formatString);
            }
            else {
                if (!type.equals(ThreddsMetadata.DatePointType.MetadataModified)) {
                    final String msg = "Unsupported DatePointType [" + typeString + "].";
                    final ThreddsXmlParserIssue parserIssue = StaxThreddsXmlParserUtils.createIssueForUnexpectedEvent(msg, ThreddsXmlParserIssue.Severity.WARNING, this.reader, startElement);
                    this.log.error("parseStartElement(): " + parserIssue.getMessage());
                    throw new ThreddsXmlParserException(parserIssue);
                }
                this.threddsMetadataBuilder.setMetadataModifiedDatePointBuilder(date, formatString);
            }
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.DateElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            DateElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
                return new DateElementParser(this.elementName, reader, builderFactory, parentBuilder);
            }
        }
    }
    
    static class AuthorityElementParser extends AbstractElementParser
    {
        private Logger log;
        private final ThreddsMetadataBuilder threddsMetadataBuilder;
        private final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper;
        private final boolean inheritedByDescendants;
        private String idAuthority;
        
        private AuthorityElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder threddsMetadataBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper, final boolean inheritedByDescendants) {
            super(elementName, reader, builderFactory);
            this.log = LoggerFactory.getLogger(this.getClass());
            this.threddsMetadataBuilder = threddsMetadataBuilder;
            this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
            this.inheritedByDescendants = inheritedByDescendants;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            this.getNextEventIfStartElementIsMine();
            this.idAuthority = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
            this.parentDatasetNodeElementParserHelper.setIdAuthoritySpecifiedInSelf(this.idAuthority);
            if (this.inheritedByDescendants) {
                this.parentDatasetNodeElementParserHelper.setIdAuthorityToBeInheritedByDescendants(this.idAuthority);
            }
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.AuthorityElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            AuthorityElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper, final boolean inheritedByDescendants) {
                return new AuthorityElementParser(this.elementName, reader, builderFactory, parentBuilder, parentDatasetNodeElementParserHelper, inheritedByDescendants);
            }
        }
    }
    
    static class DocumentationElementParser extends AbstractElementParser
    {
        private final ThreddsMetadataBuilder threddsMetadataBuilder;
        
        private DocumentationElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder threddsMetadataBuilder) {
            super(elementName, reader, builderFactory);
            this.threddsMetadataBuilder = threddsMetadataBuilder;
        }
        
        @Override
        ThreddsBuilder getSelfBuilder() {
            return null;
        }
        
        @Override
        void parseStartElement() throws ThreddsXmlParserException {
            final StartElement startElement = this.getNextEventIfStartElementIsMine();
            final Attribute typeAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.DocumentationElement_Type);
            final Attribute xlinkTitleAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.DocumentationElement_XlinkTitle);
            final Attribute xlinkExternalRefAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.DocumentationElement_XlinkHref);
            final String type = (typeAtt != null) ? typeAtt.getValue() : null;
            final String xlinkTitle = (xlinkTitleAtt != null) ? xlinkTitleAtt.getValue() : null;
            final String xlinkExternalRef = (xlinkExternalRefAtt != null) ? xlinkExternalRefAtt.getValue() : null;
            final String content = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
            if (xlinkTitle == null && xlinkExternalRef == null) {
                this.threddsMetadataBuilder.addDocumentation(type, content);
                return;
            }
            this.threddsMetadataBuilder.addDocumentation(type, xlinkTitle, xlinkExternalRef);
        }
        
        @Override
        void handleChildStartElement() throws ThreddsXmlParserException {
        }
        
        @Override
        void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.DocumentationElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            DocumentationElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
                return new DocumentationElementParser(this.elementName, reader, builderFactory, parentBuilder);
            }
        }
    }
}
