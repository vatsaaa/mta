// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.builder.ThreddsBuilder;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import java.net.URISyntaxException;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import java.net.URI;
import thredds.catalog.ServiceType;
import thredds.catalog2.xml.names.ServiceElementNames;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ServiceBuilder;
import thredds.catalog2.builder.CatalogBuilder;

class ServiceElementParser extends AbstractElementParser
{
    private final CatalogBuilder parentCatalogBuilder;
    private final ServiceBuilder parentServiceBuilder;
    private final Factory serviceElemParserFactory;
    private final PropertyElementParser.Factory propertyElemParserFactory;
    private ServiceBuilder selfBuilder;
    
    private ServiceElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder parentCatalogBuilder) {
        super(elementName, reader, builderFactory);
        this.parentCatalogBuilder = parentCatalogBuilder;
        this.parentServiceBuilder = null;
        this.serviceElemParserFactory = new Factory();
        this.propertyElemParserFactory = new PropertyElementParser.Factory();
    }
    
    private ServiceElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ServiceBuilder parentServiceBuilder) {
        super(elementName, reader, builderFactory);
        this.parentCatalogBuilder = null;
        this.parentServiceBuilder = parentServiceBuilder;
        this.serviceElemParserFactory = new Factory();
        this.propertyElemParserFactory = new PropertyElementParser.Factory();
    }
    
    @Override
    ServiceBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute nameAtt = startElement.getAttributeByName(ServiceElementNames.ServiceElement_Name);
        final String name = nameAtt.getValue();
        final Attribute serviceTypeAtt = startElement.getAttributeByName(ServiceElementNames.ServiceElement_ServiceType);
        final ServiceType serviceType = ServiceType.getType(serviceTypeAtt.getValue());
        final Attribute baseUriAtt = startElement.getAttributeByName(ServiceElementNames.ServiceElement_Base);
        final String baseUriString = baseUriAtt.getValue();
        URI baseUri = null;
        try {
            baseUri = new URI(baseUriString);
        }
        catch (URISyntaxException e) {
            this.log.error("parseElement(): Bad service base URI [" + baseUriString + "]: " + e.getMessage(), e);
            throw new ThreddsXmlParserException("Bad service base URI [" + baseUriString + "]", e);
        }
        if (this.parentCatalogBuilder != null) {
            this.selfBuilder = this.parentCatalogBuilder.addService(name, serviceType, baseUri);
        }
        else {
            if (this.parentServiceBuilder == null) {
                throw new ThreddsXmlParserException("");
            }
            this.selfBuilder = this.parentServiceBuilder.addService(name, serviceType, baseUri);
        }
        final Attribute suffixAtt = startElement.getAttributeByName(ServiceElementNames.ServiceElement_Suffix);
        if (suffixAtt != null) {
            this.selfBuilder.setSuffix(suffixAtt.getValue());
        }
        final Attribute descriptionAtt = startElement.getAttributeByName(ServiceElementNames.ServiceElement_Description);
        if (descriptionAtt != null) {
            this.selfBuilder.setSuffix(descriptionAtt.getValue());
        }
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.serviceElemParserFactory.isEventMyStartElement(startElement)) {
            final ServiceElementParser serviceElemParser = this.serviceElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            serviceElemParser.parse();
        }
        else if (this.propertyElemParserFactory.isEventMyStartElement(startElement)) {
            final PropertyElementParser parser = this.propertyElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            parser.parse();
        }
        else {
            StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = ServiceElementNames.ServiceElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        ServiceElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder parentCatalogBuilder) {
            return new ServiceElementParser(this.elementName, reader, builderFactory, parentCatalogBuilder, null);
        }
        
        ServiceElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ServiceBuilder parentServiceBuilder) {
            return new ServiceElementParser(this.elementName, reader, builderFactory, parentServiceBuilder, null);
        }
    }
}
