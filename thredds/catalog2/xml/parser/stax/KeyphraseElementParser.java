// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

class KeyphraseElementParser extends AbstractElementParser
{
    private final ThreddsMetadataBuilder parentBuilder;
    private ThreddsMetadataBuilder.KeyphraseBuilder selfBuilder;
    
    private KeyphraseElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
        super(elementName, reader, builderFactory);
        this.parentBuilder = parentBuilder;
    }
    
    @Override
    ThreddsBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute roleAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.ControlledVocabType_Authority);
        final String role = (roleAtt != null) ? roleAtt.getValue() : null;
        final String name = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
        this.selfBuilder = this.parentBuilder.addKeyphrase(role, name);
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final String unexpectedElement = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
        final ThreddsXmlParserIssue issue = new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.ERROR, "Unrecognized element: " + unexpectedElement, this.selfBuilder, null);
        throw new ThreddsXmlParserException(issue);
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = ThreddsMetadataElementNames.KeywordElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        KeyphraseElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
            return new KeyphraseElementParser(this.elementName, reader, builderFactory, parentBuilder, null);
        }
    }
}
