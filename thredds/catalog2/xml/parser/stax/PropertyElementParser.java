// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.builder.BuilderIssue;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.xml.names.PropertyElementNames;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ServiceBuilder;
import thredds.catalog2.builder.DatasetNodeBuilder;
import thredds.catalog2.builder.CatalogBuilder;

class PropertyElementParser extends AbstractElementParser
{
    private final CatalogBuilder catBuilder;
    private final DatasetNodeBuilder datasetNodeBuilder;
    private final ServiceBuilder serviceBuilder;
    
    private PropertyElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder catBuilder) {
        super(elementName, reader, builderFactory);
        this.catBuilder = catBuilder;
        this.datasetNodeBuilder = null;
        this.serviceBuilder = null;
    }
    
    private PropertyElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder datasetNodeBuilder) {
        super(elementName, reader, builderFactory);
        this.catBuilder = null;
        this.datasetNodeBuilder = datasetNodeBuilder;
        this.serviceBuilder = null;
    }
    
    private PropertyElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ServiceBuilder serviceBuilder) {
        super(elementName, reader, builderFactory);
        this.catBuilder = null;
        this.datasetNodeBuilder = null;
        this.serviceBuilder = serviceBuilder;
    }
    
    @Override
    ThreddsBuilder getSelfBuilder() {
        return null;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute nameAtt = startElement.getAttributeByName(PropertyElementNames.PropertyElement_Name);
        final String name = nameAtt.getValue();
        final Attribute valueAtt = startElement.getAttributeByName(PropertyElementNames.PropertyElement_Value);
        final String value = valueAtt.getValue();
        if (this.catBuilder != null) {
            this.catBuilder.addProperty(name, value);
        }
        else if (this.datasetNodeBuilder != null) {
            this.datasetNodeBuilder.addProperty(name, value);
        }
        else {
            if (this.serviceBuilder == null) {
                throw new ThreddsXmlParserException("Unknown builder - for addProperty().");
            }
            this.serviceBuilder.addProperty(name, value);
        }
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final String unexpectedChildElementAsString = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
        ThreddsBuilder parentBuilder;
        if (this.catBuilder != null) {
            parentBuilder = this.catBuilder;
        }
        else if (this.datasetNodeBuilder != null) {
            parentBuilder = this.datasetNodeBuilder;
        }
        else {
            if (this.serviceBuilder == null) {
                throw new ThreddsXmlParserException("Unknown parent builder.");
            }
            parentBuilder = this.serviceBuilder;
        }
        final BuilderIssue issue = new BuilderIssue(BuilderIssue.Severity.WARNING, "Unexpected child element: " + unexpectedChildElementAsString, parentBuilder, null);
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = PropertyElementNames.PropertyElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        PropertyElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder parentCatalogBuilder) {
            return new PropertyElementParser(this.elementName, reader, builderFactory, parentCatalogBuilder, null);
        }
        
        PropertyElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder) {
            return new PropertyElementParser(this.elementName, reader, builderFactory, parentDatasetNodeBuilder, null);
        }
        
        PropertyElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ServiceBuilder parentServiceBuilder) {
            return new PropertyElementParser(this.elementName, reader, builderFactory, parentServiceBuilder, null);
        }
    }
}
