// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.builder.ThreddsBuilder;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

class GeospatialRangeTypeParser extends AbstractElementParser
{
    private final ThreddsMetadataBuilder.GeospatialCoverageBuilder parentBuilder;
    private ThreddsMetadataBuilder.GeospatialRangeBuilder selfBuilder;
    private CharContentOnlyElementParser.Factory startFac;
    private CharContentOnlyElementParser.Factory sizeFac;
    private CharContentOnlyElementParser.Factory resolutionFac;
    private CharContentOnlyElementParser.Factory unitsFac;
    private String startAsString;
    private String sizeAsString;
    private String resolutionAsString;
    private String unitsAsString;
    
    private GeospatialRangeTypeParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder.GeospatialCoverageBuilder parentBuilder) {
        super(elementName, reader, builderFactory);
        this.parentBuilder = parentBuilder;
        this.startFac = new CharContentOnlyElementParser.Factory(ThreddsMetadataElementNames.SpatialRangeType_Start);
        this.sizeFac = new CharContentOnlyElementParser.Factory(ThreddsMetadataElementNames.SpatialRangeType_Size);
        this.resolutionFac = new CharContentOnlyElementParser.Factory(ThreddsMetadataElementNames.SpatialRangeType_Resolution);
        this.unitsFac = new CharContentOnlyElementParser.Factory(ThreddsMetadataElementNames.SpatialRangeType_Units);
    }
    
    @Override
    ThreddsMetadataBuilder.GeospatialRangeBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        StaxThreddsXmlParserUtils.readNextEventCheckItIsStartElementWithExpectedName(this.reader, this.elementName);
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.startFac.isEventMyStartElement(startElement)) {
            final CharContentOnlyElementParser startParser = this.startFac.getParser();
            startParser.parseElement(this.reader);
            this.startAsString = startParser.getValue();
        }
        else if (this.sizeFac.isEventMyStartElement(startElement)) {
            final CharContentOnlyElementParser parser = this.sizeFac.getParser();
            parser.parseElement(this.reader);
            this.sizeAsString = parser.getValue();
        }
        else if (this.resolutionFac.isEventMyStartElement(startElement)) {
            final CharContentOnlyElementParser parser = this.resolutionFac.getParser();
            parser.parseElement(this.reader);
            this.resolutionAsString = parser.getValue();
        }
        else if (this.unitsFac.isEventMyStartElement(startElement)) {
            final CharContentOnlyElementParser parser = this.unitsFac.getParser();
            parser.parseElement(this.reader);
            this.unitsAsString = parser.getValue();
        }
        else {
            StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        if (this.startAsString != null) {
            this.selfBuilder.setStart(this.parseDouble(this.startAsString));
        }
        if (this.sizeAsString != null) {
            this.selfBuilder.setSize(this.parseDouble(this.sizeAsString));
        }
        if (this.resolutionAsString != null) {
            this.selfBuilder.setResolution(this.parseDouble(this.resolutionAsString));
        }
    }
    
    private double parseDouble(final String doubleAsString) {
        if (doubleAsString == null) {
            return Double.NaN;
        }
        try {
            return Double.parseDouble(doubleAsString);
        }
        catch (NumberFormatException e) {
            return Double.NaN;
        }
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory(final QName elementName) {
            this.elementName = elementName;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        GeospatialRangeTypeParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder.GeospatialCoverageBuilder parentBuilder) {
            return new GeospatialRangeTypeParser(this.elementName, reader, builderFactory, parentBuilder, null);
        }
    }
    
    private static class CharContentOnlyElementParser
    {
        private QName elementName;
        private String value;
        
        private CharContentOnlyElementParser(final QName elementName) {
            this.elementName = elementName;
        }
        
        String getValue() {
            return this.value;
        }
        
        void parseElement(final XMLEventReader reader) throws ThreddsXmlParserException {
            final StartElement startElement = StaxThreddsXmlParserUtils.readNextEventCheckItIsStartElementWithExpectedName(reader, this.elementName);
            this.value = StaxThreddsXmlParserUtils.getCharacterContent(reader, this.elementName);
            StaxThreddsXmlParserUtils.readNextEventCheckItIsEndElementWithExpectedName(reader, this.elementName);
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory(final QName elementName) {
                this.elementName = elementName;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            CharContentOnlyElementParser getParser() {
                return new CharContentOnlyElementParser(this.elementName);
            }
        }
    }
}
