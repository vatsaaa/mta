// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.builder.ThreddsBuilder;
import java.util.Iterator;
import thredds.catalog2.builder.ServiceBuilder;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.builder.AccessBuilder;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.xml.names.DatasetElementNames;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.DatasetBuilder;
import thredds.catalog2.builder.DatasetNodeBuilder;
import thredds.catalog2.builder.CatalogBuilder;

class DatasetElementParser extends AbstractElementParser
{
    private final CatalogBuilder parentCatalogBuilder;
    private final DatasetNodeBuilder parentDatasetNodeBuilder;
    private final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper;
    private final AccessElementParser.Factory accessElementParserFactory;
    private DatasetNodeElementParserHelper datasetNodeElementParserHelper;
    private DatasetBuilder selfBuilder;
    
    private DatasetElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder parentCatalogBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
        super(elementName, reader, builderFactory);
        this.parentCatalogBuilder = parentCatalogBuilder;
        this.parentDatasetNodeBuilder = null;
        this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
        this.accessElementParserFactory = new AccessElementParser.Factory();
    }
    
    private DatasetElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
        super(elementName, reader, builderFactory);
        this.parentCatalogBuilder = null;
        this.parentDatasetNodeBuilder = parentDatasetNodeBuilder;
        this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
        this.accessElementParserFactory = new AccessElementParser.Factory();
    }
    
    void setDefaultServiceName(final String defaultServiceName) {
        this.datasetNodeElementParserHelper.setDefaultServiceNameSpecifiedInSelf(defaultServiceName);
    }
    
    String getDefaultServiceName() {
        return (this.datasetNodeElementParserHelper.getDefaultServiceNameSpecifiedInSelf() != null) ? this.datasetNodeElementParserHelper.getDefaultServiceNameSpecifiedInSelf() : this.datasetNodeElementParserHelper.getDefaultServiceNameInheritedFromAncestors();
    }
    
    @Override
    DatasetBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute nameAtt = startElement.getAttributeByName(DatasetElementNames.DatasetElement_Name);
        final String name = nameAtt.getValue();
        if (this.parentCatalogBuilder != null) {
            this.selfBuilder = this.parentCatalogBuilder.addDataset(name);
        }
        else if (this.parentDatasetNodeBuilder != null) {
            this.selfBuilder = this.parentDatasetNodeBuilder.addDataset(name);
        }
        else {
            if (this.builderFactory == null) {
                throw new ThreddsXmlParserException("");
            }
            this.selfBuilder = this.builderFactory.newDatasetBuilder(name);
        }
        (this.datasetNodeElementParserHelper = new DatasetNodeElementParserHelper(this.parentDatasetNodeElementParserHelper, this.selfBuilder, this.builderFactory)).parseStartElementIdAttribute(startElement);
        this.datasetNodeElementParserHelper.parseStartElementIdAuthorityAttribute(startElement);
        final Attribute serviceNameAtt = startElement.getAttributeByName(DatasetElementNames.DatasetElement_ServiceName);
        if (serviceNameAtt != null) {
            this.setDefaultServiceName(serviceNameAtt.getValue());
        }
        final Attribute urlPathAtt = startElement.getAttributeByName(DatasetElementNames.DatasetElement_UrlPath);
        if (urlPathAtt != null) {
            final AccessBuilder accessBuilder = this.selfBuilder.addAccessBuilder();
            accessBuilder.setUrlPath(urlPathAtt.getValue());
        }
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.datasetNodeElementParserHelper.handleBasicChildStartElement(startElement, this.reader, this.selfBuilder)) {
            return;
        }
        if (this.datasetNodeElementParserHelper.handleCollectionChildStartElement(startElement, this.reader, this.selfBuilder)) {
            return;
        }
        if (this.accessElementParserFactory.isEventMyStartElement(startElement)) {
            final AccessElementParser parser = this.accessElementParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            parser.parse();
            return;
        }
        StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        this.datasetNodeElementParserHelper.postProcessingAfterEndElement();
        if (!this.selfBuilder.getAccessBuilders().isEmpty()) {
            ServiceBuilder defaultServiceBuilder = null;
            if (this.getDefaultServiceName() != null) {
                defaultServiceBuilder = this.selfBuilder.getParentCatalogBuilder().findServiceBuilderByNameGlobally(this.getDefaultServiceName());
            }
            for (final AccessBuilder curAB : this.selfBuilder.getAccessBuilders()) {
                if (curAB.getServiceBuilder() == null) {
                    if (defaultServiceBuilder != null) {
                        curAB.setServiceBuilder(defaultServiceBuilder);
                    }
                    else {
                        this.selfBuilder.removeAccessBuilder(curAB);
                    }
                }
            }
        }
        this.datasetNodeElementParserHelper.addFinalThreddsMetadataToDatasetNodeBuilder(this.selfBuilder);
        this.datasetNodeElementParserHelper.addFinalMetadataToDatasetNodeBuilder(this.selfBuilder);
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = DatasetElementNames.DatasetElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        DatasetElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final CatalogBuilder parentCatalogBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
            return new DatasetElementParser(this.elementName, reader, builderFactory, parentCatalogBuilder, parentDatasetNodeElementParserHelper, null);
        }
        
        DatasetElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
            return new DatasetElementParser(this.elementName, reader, builderFactory, parentDatasetNodeBuilder, parentDatasetNodeElementParserHelper, null);
        }
    }
}
