// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.builder.MetadataBuilder;
import thredds.catalog2.builder.util.MetadataBuilderUtils;
import java.util.Iterator;
import thredds.catalog2.builder.ThreddsMetadataBuilder;
import thredds.catalog2.builder.util.ThreddsMetadataBuilderUtils;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.XMLEventReader;
import thredds.catalog2.xml.names.DatasetNodeElementNames;
import javax.xml.stream.events.Attribute;
import thredds.catalog2.xml.names.DatasetElementNames;
import javax.xml.stream.events.StartElement;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;
import java.util.List;
import thredds.catalog2.builder.DatasetNodeBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import org.slf4j.Logger;

class DatasetNodeElementParserHelper
{
    private Logger log;
    private final ThreddsBuilderFactory builderFactory;
    private final PropertyElementParser.Factory propertyElemParserFactory;
    private final DatasetElementParser.Factory datasetElemParserFactory;
    private final CatalogRefElementParser.Factory catRefElemParserFactory;
    private final MetadataElementParser.Factory metadataElemParserFactory;
    private final ThreddsMetadataElementParser.Factory threddsMetadataElemParserFactory;
    private final DatasetNodeBuilder datasetNodeBuilder;
    private String defaultServiceNameInheritedFromAncestors;
    private String defaultServiceNameSpecifiedInSelf;
    private String defaultServiceNameToBeInheritedByDescendants;
    private String idAuthorityInheritedFromAncestors;
    private String idAuthoritySpecifiedInSelf;
    private String idAuthorityToBeInheritedByDescendants;
    private List<MetadataElementParser> metadataForThisDataset;
    private List<MetadataElementParser> metadataInheritedByDescendants;
    private ThreddsMetadataElementParser threddsMetadataElementParser;
    private SplitMetadata finalSplitMetadata;
    
    DatasetNodeElementParserHelper(final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper, final DatasetNodeBuilder datasetNodeBuilder, final ThreddsBuilderFactory builderFactory) {
        this.log = LoggerFactory.getLogger(this.getClass());
        this.datasetNodeBuilder = datasetNodeBuilder;
        this.builderFactory = builderFactory;
        this.propertyElemParserFactory = new PropertyElementParser.Factory();
        this.datasetElemParserFactory = new DatasetElementParser.Factory();
        this.catRefElemParserFactory = new CatalogRefElementParser.Factory();
        this.metadataElemParserFactory = new MetadataElementParser.Factory();
        this.threddsMetadataElemParserFactory = new ThreddsMetadataElementParser.Factory();
        if (parentDatasetNodeElementParserHelper != null) {
            final List<MetadataElementParser> metadataInheritedFromAncestors = parentDatasetNodeElementParserHelper.getMetadataInheritedByDescendants();
            if (metadataInheritedFromAncestors != null && !metadataInheritedFromAncestors.isEmpty()) {
                (this.metadataInheritedByDescendants = new ArrayList<MetadataElementParser>()).addAll(metadataInheritedFromAncestors);
                (this.metadataForThisDataset = new ArrayList<MetadataElementParser>()).addAll(metadataInheritedFromAncestors);
            }
            this.defaultServiceNameInheritedFromAncestors = parentDatasetNodeElementParserHelper.getDefaultServiceNameToBeInheritedByDescendants();
            this.idAuthorityInheritedFromAncestors = parentDatasetNodeElementParserHelper.getIdAuthorityToBeInheritedByDescendants();
        }
    }
    
    String getIdAuthorityInheritedFromAncestors() {
        return this.idAuthorityInheritedFromAncestors;
    }
    
    void setIdAuthorityInheritedFromAncestors(final String idAuthorityInheritedFromAncestors) {
        this.idAuthorityInheritedFromAncestors = idAuthorityInheritedFromAncestors;
    }
    
    String getIdAuthoritySpecifiedInSelf() {
        return this.idAuthoritySpecifiedInSelf;
    }
    
    void setIdAuthoritySpecifiedInSelf(final String idAuthoritySpecifiedInSelf) {
        this.idAuthoritySpecifiedInSelf = idAuthoritySpecifiedInSelf;
    }
    
    String getIdAuthorityToBeInheritedByDescendants() {
        return this.idAuthorityToBeInheritedByDescendants;
    }
    
    void setIdAuthorityToBeInheritedByDescendants(final String idAuthorityToBeInheritedByDescendants) {
        this.idAuthorityToBeInheritedByDescendants = idAuthorityToBeInheritedByDescendants;
    }
    
    String getDefaultServiceNameInheritedFromAncestors() {
        return this.defaultServiceNameInheritedFromAncestors;
    }
    
    void setDefaultServiceNameInheritedFromAncestors(final String defaultServiceNameInheritedFromAncestors) {
        this.defaultServiceNameInheritedFromAncestors = defaultServiceNameInheritedFromAncestors;
    }
    
    String getDefaultServiceNameSpecifiedInSelf() {
        return this.defaultServiceNameSpecifiedInSelf;
    }
    
    void setDefaultServiceNameSpecifiedInSelf(final String defaultServiceNameSpecifiedInSelf) {
        this.defaultServiceNameSpecifiedInSelf = defaultServiceNameSpecifiedInSelf;
    }
    
    String getDefaultServiceNameToBeInheritedByDescendants() {
        return this.defaultServiceNameToBeInheritedByDescendants;
    }
    
    void setDefaultServiceNameToBeInheritedByDescendants(final String defaultServiceNameToBeInheritedByDescendants) {
        this.defaultServiceNameToBeInheritedByDescendants = defaultServiceNameToBeInheritedByDescendants;
    }
    
    public List<MetadataElementParser> getMetadataForThisDataset() {
        if (this.metadataForThisDataset == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends MetadataElementParser>)this.metadataForThisDataset);
    }
    
    List<MetadataElementParser> getMetadataInheritedByDescendants() {
        if (this.metadataInheritedByDescendants == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends MetadataElementParser>)this.metadataInheritedByDescendants);
    }
    
    void parseStartElementNameAttribute(final StartElement startElement) {
        final Attribute att = startElement.getAttributeByName(DatasetElementNames.DatasetElement_Name);
        if (att != null) {
            this.datasetNodeBuilder.setName(att.getValue());
        }
    }
    
    void parseStartElementIdAttribute(final StartElement startElement) {
        final Attribute att = startElement.getAttributeByName(DatasetNodeElementNames.DatasetNodeElement_Id);
        if (att != null) {
            this.datasetNodeBuilder.setId(att.getValue());
        }
    }
    
    void parseStartElementIdAuthorityAttribute(final StartElement startElement) {
        final Attribute att = startElement.getAttributeByName(DatasetNodeElementNames.DatasetNodeElement_Authority);
        if (att != null) {
            this.setIdAuthoritySpecifiedInSelf(att.getValue());
        }
    }
    
    boolean handleBasicChildStartElement(final StartElement startElement, final XMLEventReader reader, final DatasetNodeBuilder dsNodeBuilder) throws ThreddsXmlParserException {
        if (this.propertyElemParserFactory.isEventMyStartElement(startElement)) {
            final PropertyElementParser parser = this.propertyElemParserFactory.getNewParser(reader, this.builderFactory, dsNodeBuilder);
            parser.parse();
            return true;
        }
        if (this.metadataElemParserFactory.isEventMyStartElement(startElement)) {
            final MetadataElementParser parser2 = this.metadataElemParserFactory.getNewParser(reader, this.builderFactory, dsNodeBuilder, this);
            parser2.parse();
            if (this.metadataForThisDataset == null) {
                this.metadataForThisDataset = new ArrayList<MetadataElementParser>();
            }
            this.metadataForThisDataset.add(parser2);
            if (parser2.doesMetadataElementGetInherited()) {
                if (this.metadataInheritedByDescendants == null) {
                    this.metadataInheritedByDescendants = new ArrayList<MetadataElementParser>();
                }
                this.metadataInheritedByDescendants.add(parser2);
            }
            return true;
        }
        if (this.threddsMetadataElemParserFactory.isEventMyStartElement(startElement)) {
            if (this.threddsMetadataElementParser == null) {
                this.threddsMetadataElementParser = this.threddsMetadataElemParserFactory.getNewParser(reader, this.builderFactory, dsNodeBuilder, this, false);
            }
            this.threddsMetadataElementParser.parse();
            return true;
        }
        return false;
    }
    
    boolean handleCollectionChildStartElement(final StartElement startElement, final XMLEventReader reader, final DatasetNodeBuilder dsNodeBuilder) throws ThreddsXmlParserException {
        if (this.datasetElemParserFactory.isEventMyStartElement(startElement)) {
            final DatasetElementParser parser = this.datasetElemParserFactory.getNewParser(reader, this.builderFactory, dsNodeBuilder, this);
            parser.parse();
            return true;
        }
        if (this.catRefElemParserFactory.isEventMyStartElement(startElement)) {
            final CatalogRefElementParser parser2 = this.catRefElemParserFactory.getNewParser(reader, this.builderFactory, dsNodeBuilder, this);
            parser2.parse();
            return true;
        }
        return false;
    }
    
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        if (this.getDefaultServiceNameToBeInheritedByDescendants() == null) {
            this.setDefaultServiceNameToBeInheritedByDescendants(this.getDefaultServiceNameInheritedFromAncestors());
        }
        if (this.getIdAuthorityToBeInheritedByDescendants() == null) {
            this.setIdAuthorityToBeInheritedByDescendants(this.getIdAuthorityInheritedFromAncestors());
        }
        this.datasetNodeBuilder.setIdAuthority((this.getIdAuthoritySpecifiedInSelf() != null) ? this.getIdAuthoritySpecifiedInSelf() : this.getIdAuthorityInheritedFromAncestors());
        if (this.threddsMetadataElementParser != null) {
            this.threddsMetadataElementParser.postProcessingAfterEndElement();
        }
        this.finalSplitMetadata = new SplitMetadata(this.metadataForThisDataset);
    }
    
    void addFinalThreddsMetadataToDatasetNodeBuilder(final DatasetNodeBuilder dsNodeBuilder) {
        ThreddsMetadataBuilder unwrappedThreddsMetadataBuilder = null;
        boolean isUnwrappedEmpty = true;
        if (this.threddsMetadataElementParser != null) {
            unwrappedThreddsMetadataBuilder = this.threddsMetadataElementParser.getSelfBuilder();
            isUnwrappedEmpty = unwrappedThreddsMetadataBuilder.isEmpty();
        }
        if (!isUnwrappedEmpty || !this.finalSplitMetadata.threddsMetadata.isEmpty()) {
            final ThreddsMetadataBuilder result = dsNodeBuilder.setNewThreddsMetadataBuilder();
            if (!isUnwrappedEmpty) {
                ThreddsMetadataBuilderUtils.copyThreddsMetadataBuilder(unwrappedThreddsMetadataBuilder, result);
            }
            for (final MetadataElementParser mdElemParser : this.finalSplitMetadata.threddsMetadata) {
                ThreddsMetadataBuilderUtils.copyThreddsMetadataBuilder(mdElemParser.getThreddsMetadataBuilder(), result);
            }
        }
    }
    
    void addFinalMetadataToDatasetNodeBuilder(final DatasetNodeBuilder dsNodeBuilder) {
        for (final MetadataElementParser currentMetadataElemParser : this.finalSplitMetadata.nonThreddsMetadata) {
            final MetadataBuilder newMetadataBuilder = dsNodeBuilder.addMetadata();
            MetadataBuilderUtils.copyMetadataBuilder(currentMetadataElemParser.getSelfBuilder(), newMetadataBuilder);
        }
    }
    
    private class SplitMetadata
    {
        final List<MetadataElementParser> threddsMetadata;
        final List<MetadataElementParser> nonThreddsMetadata;
        
        SplitMetadata(final List<MetadataElementParser> metadata) {
            if (metadata == null || metadata.isEmpty()) {
                this.threddsMetadata = Collections.emptyList();
                this.nonThreddsMetadata = Collections.emptyList();
                return;
            }
            this.threddsMetadata = new ArrayList<MetadataElementParser>();
            this.nonThreddsMetadata = new ArrayList<MetadataElementParser>();
            for (final MetadataElementParser current : metadata) {
                if (current.isContainsThreddsMetadata()) {
                    this.threddsMetadata.add(current);
                }
                else {
                    this.nonThreddsMetadata.add(current);
                }
            }
        }
    }
}
