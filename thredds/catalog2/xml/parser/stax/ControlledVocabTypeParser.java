// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;

class ControlledVocabTypeParser
{
    private QName elementName;
    private String vocabAuth;
    private String value;
    
    private ControlledVocabTypeParser(final QName elementName) {
        this.elementName = elementName;
    }
    
    String getVocabAuth() {
        return this.vocabAuth;
    }
    
    String getValue() {
        return this.value;
    }
    
    void parseElement(final XMLEventReader reader) throws ThreddsXmlParserException {
        final StartElement startElement = StaxThreddsXmlParserUtils.readNextEventCheckItIsStartElementWithExpectedName(reader, this.elementName);
        final Attribute vocabAuthAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.ControlledVocabType_Authority);
        this.vocabAuth = ((vocabAuthAtt != null) ? vocabAuthAtt.getValue() : null);
        this.value = StaxThreddsXmlParserUtils.getCharacterContent(reader, this.elementName);
        StaxThreddsXmlParserUtils.readNextEventCheckItIsEndElementWithExpectedName(reader, this.elementName);
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory(final QName elementName) {
            this.elementName = elementName;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        ControlledVocabTypeParser getNewDateTypeParser() {
            return new ControlledVocabTypeParser(this.elementName, null);
        }
    }
}
