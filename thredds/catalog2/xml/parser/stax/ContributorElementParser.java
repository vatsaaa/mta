// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

class ContributorElementParser extends AbstractElementParser
{
    private final ThreddsMetadataBuilder parentBuilder;
    private ThreddsMetadataBuilder.ContributorBuilder selfBuilder;
    
    private ContributorElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
        super(elementName, reader, builderFactory);
        this.parentBuilder = parentBuilder;
    }
    
    @Override
    ThreddsBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute roleAtt = startElement.getAttributeByName(ThreddsMetadataElementNames.ContributorElement_Role);
        final String role = (roleAtt != null) ? roleAtt.getValue() : null;
        final String name = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
        (this.selfBuilder = this.parentBuilder.addContributor()).setName(name);
        this.selfBuilder.setRole(role);
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final String unexpectedElement = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
        final ThreddsXmlParserIssue issue = new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.ERROR, "Unrecognized element: " + unexpectedElement, this.selfBuilder, null);
        throw new ThreddsXmlParserException(issue);
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = ThreddsMetadataElementNames.ContributorElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        ContributorElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
            return new ContributorElementParser(this.elementName, reader, builderFactory, parentBuilder, null);
        }
    }
}
