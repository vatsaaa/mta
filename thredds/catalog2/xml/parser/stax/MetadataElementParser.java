// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import java.net.URISyntaxException;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.names.MetadataElementNames;
import java.util.List;
import thredds.catalog2.builder.ThreddsMetadataBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import java.net.URI;
import thredds.catalog2.builder.MetadataBuilder;
import thredds.catalog2.builder.DatasetNodeBuilder;

class MetadataElementParser extends AbstractElementParser
{
    private final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper;
    private final DatasetNodeBuilder parentDatasetNodeBuilder;
    private final MetadataBuilder selfBuilder;
    private boolean isInheritedByDescendants;
    private boolean containsThreddsMetadata;
    private String title;
    private URI externalRefUri;
    private boolean isContainedContent;
    private StringBuilder content;
    private ThreddsMetadataElementParser.Factory threddsMetadataElementParserFactory;
    private ThreddsMetadataElementParser threddsMetadataElementParser;
    
    private MetadataElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
        super(elementName, reader, builderFactory);
        this.isInheritedByDescendants = false;
        this.containsThreddsMetadata = false;
        this.isContainedContent = false;
        this.parentDatasetNodeBuilder = parentDatasetNodeBuilder;
        this.parentDatasetNodeElementParserHelper = parentDatasetNodeElementParserHelper;
        this.threddsMetadataElementParserFactory = new ThreddsMetadataElementParser.Factory();
        this.selfBuilder = builderFactory.newMetadataBuilder();
    }
    
    @Override
    MetadataBuilder getSelfBuilder() {
        if (this.containsThreddsMetadata) {
            return null;
        }
        return this.selfBuilder;
    }
    
    boolean doesMetadataElementGetInherited() {
        return this.isInheritedByDescendants;
    }
    
    boolean isContainsThreddsMetadata() {
        return this.containsThreddsMetadata;
    }
    
    ThreddsMetadataBuilder getThreddsMetadataBuilder() {
        if (!this.containsThreddsMetadata) {
            return this.builderFactory.newThreddsMetadataBuilder();
        }
        return this.threddsMetadataElementParser.getSelfBuilder();
    }
    
    boolean addThreddsMetadataBuilderToList(final List<ThreddsMetadataBuilder> tmBuilders) {
        return this.getSelfBuilder() != null && tmBuilders.add(this.threddsMetadataElementParser.getSelfBuilder());
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        final Attribute inheritedAtt = startElement.getAttributeByName(MetadataElementNames.MetadataElement_Inherited);
        if (inheritedAtt != null && inheritedAtt.getValue().equalsIgnoreCase("true")) {
            this.isInheritedByDescendants = true;
        }
        final StartElement nextElement = this.peekAtNextEventIfStartElement();
        if (this.threddsMetadataElementParserFactory.isEventMyStartElement(nextElement)) {
            this.containsThreddsMetadata = true;
            return;
        }
        final Attribute titleAtt = startElement.getAttributeByName(MetadataElementNames.MetadataElement_XlinkTitle);
        final Attribute externalRefAtt = startElement.getAttributeByName(MetadataElementNames.MetadataElement_XlinkHref);
        if (titleAtt == null && externalRefAtt == null) {
            this.selfBuilder.setContainedContent(true);
            return;
        }
        if (titleAtt == null || externalRefAtt == null) {
            final String msg = "External reference metadata element has null title or URI.";
            final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForUnexpectedElement(msg, this.reader);
            this.log.warn("parseStartElement(): " + issue.getMessage());
            throw new ThreddsXmlParserException(issue);
        }
        this.selfBuilder.setTitle(titleAtt.getValue());
        final String uriString = externalRefAtt.getValue();
        try {
            this.selfBuilder.setExternalReference(new URI(uriString));
        }
        catch (URISyntaxException e) {
            final String msg2 = "External reference metadata element with bad URI syntax [" + uriString + "].";
            final ThreddsXmlParserIssue issue2 = StaxThreddsXmlParserUtils.createIssueForException(msg2, this.reader, e);
            this.log.warn("parseStartElement(): " + issue2.getMessage(), e);
            throw new ThreddsXmlParserException(issue2);
        }
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.containsThreddsMetadata) {
            if (!this.threddsMetadataElementParserFactory.isEventMyStartElement(startElement)) {
                final String msg = "Expecting THREDDS Metadata, got non-THREDDS Metadata";
                final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForUnexpectedElement(msg, this.reader);
                throw new ThreddsXmlParserException(issue);
            }
            if (this.threddsMetadataElementParser == null) {
                this.threddsMetadataElementParser = this.threddsMetadataElementParserFactory.getNewParser(this.reader, this.builderFactory, this.parentDatasetNodeBuilder, this.parentDatasetNodeElementParserHelper, this.isInheritedByDescendants);
            }
            this.threddsMetadataElementParser.parse();
        }
        else {
            if (this.threddsMetadataElementParserFactory.isEventMyStartElement(startElement)) {
                final String msg = "Unexpected THREDDS Metadata";
                final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForUnexpectedElement(msg, this.reader);
                throw new ThreddsXmlParserException(issue);
            }
            if (!this.isContainedContent) {
                final String msg = "Unexpected content";
                final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForUnexpectedElement(msg, this.reader);
                throw new ThreddsXmlParserException(issue);
            }
            if (this.content == null) {
                this.content = new StringBuilder();
            }
            this.content.append(StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader));
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        if (!this.containsThreddsMetadata && this.content != null) {
            this.selfBuilder.setContent(this.content.toString());
        }
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = MetadataElementNames.MetadataElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        MetadataElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final DatasetNodeBuilder parentDatasetNodeBuilder, final DatasetNodeElementParserHelper parentDatasetNodeElementParserHelper) {
            return new MetadataElementParser(this.elementName, reader, builderFactory, parentDatasetNodeBuilder, parentDatasetNodeElementParserHelper, null);
        }
    }
}
