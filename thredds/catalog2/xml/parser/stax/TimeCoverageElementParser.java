// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

class TimeCoverageElementParser extends AbstractElementParser
{
    private final ThreddsMetadataBuilder parentBuilder;
    private ThreddsMetadataBuilder.DateRangeBuilder selfBuilder;
    private DateTypeParser.Factory startElementParserFactory;
    private DateTypeParser startElementParser;
    private DateTypeParser.Factory endElementParserFactory;
    private DateTypeParser endElementParser;
    private DurationParser.Factory durationParserFactory;
    private DurationParser durationParser;
    private DurationParser.Factory resolutionParserFactory;
    private DurationParser resolutionParser;
    
    private TimeCoverageElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
        super(elementName, reader, builderFactory);
        this.parentBuilder = parentBuilder;
        this.startElementParserFactory = new DateTypeParser.Factory(ThreddsMetadataElementNames.DateRangeType_StartElement);
        this.endElementParserFactory = new DateTypeParser.Factory(ThreddsMetadataElementNames.DateRangeType_EndElement);
        this.durationParserFactory = new DurationParser.Factory(ThreddsMetadataElementNames.DateRangeType_DurationElement);
        this.resolutionParserFactory = new DurationParser.Factory(ThreddsMetadataElementNames.DateRangeType_ResolutionElement);
    }
    
    @Override
    ThreddsBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        this.getNextEventIfStartElementIsMine();
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.startElementParserFactory.isEventMyStartElement(startElement)) {
            (this.startElementParser = this.startElementParserFactory.getNewDateTypeParser()).parseElement(this.reader);
        }
        else if (this.endElementParserFactory.isEventMyStartElement(startElement)) {
            (this.endElementParser = this.endElementParserFactory.getNewDateTypeParser()).parseElement(this.reader);
        }
        else if (this.durationParserFactory.isEventMyStartElement(startElement)) {
            (this.durationParser = this.durationParserFactory.getNewParser()).parseElement(this.reader);
        }
        else {
            if (!this.resolutionParserFactory.isEventMyStartElement(startElement)) {
                final String unexpectedElement = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
                final ThreddsXmlParserIssue issue = new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.ERROR, "Unrecognized element: " + unexpectedElement, this.parentBuilder, null);
                throw new ThreddsXmlParserException(issue);
            }
            (this.resolutionParser = this.resolutionParserFactory.getNewParser()).parseElement(this.reader);
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
        final String startDate = (this.startElementParser != null) ? this.startElementParser.getValue() : null;
        final String startDateFormat = (this.startElementParser != null) ? this.startElementParser.getFormat() : null;
        final String endDate = (this.endElementParser != null) ? this.endElementParser.getValue() : null;
        final String endDateFormat = (this.endElementParser != null) ? this.endElementParser.getFormat() : null;
        final String duration = (this.durationParser != null) ? this.durationParser.getValue() : null;
        final String resolution = (this.resolutionParser != null) ? this.resolutionParser.getValue() : null;
        this.selfBuilder = this.parentBuilder.setTemporalCoverageBuilder(startDate, startDateFormat, endDate, endDateFormat, duration, resolution);
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = ThreddsMetadataElementNames.TimeCoverageElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        TimeCoverageElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
            return new TimeCoverageElementParser(this.elementName, reader, builderFactory, parentBuilder, null);
        }
    }
    
    static class DurationParser
    {
        private QName elementName;
        private String value;
        
        private DurationParser(final QName elementName) {
            this.elementName = elementName;
        }
        
        String getValue() {
            return this.value;
        }
        
        void parseElement(final XMLEventReader reader) throws ThreddsXmlParserException {
            StaxThreddsXmlParserUtils.readNextEventCheckItIsStartElementWithExpectedName(reader, this.elementName);
            this.value = StaxThreddsXmlParserUtils.getCharacterContent(reader, this.elementName);
            StaxThreddsXmlParserUtils.readNextEventCheckItIsEndElementWithExpectedName(reader, this.elementName);
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory(final QName elementName) {
                this.elementName = elementName;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            DurationParser getNewParser() {
                return new DurationParser(this.elementName);
            }
        }
    }
}
