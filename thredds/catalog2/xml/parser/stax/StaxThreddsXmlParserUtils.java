// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import org.slf4j.LoggerFactory;
import javax.xml.stream.XMLInputFactory;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.File;
import java.io.InputStream;
import thredds.util.HttpUriResolver;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import thredds.util.HttpUriResolverFactory;
import javax.xml.transform.Source;
import java.net.URI;
import java.util.List;
import java.io.Writer;
import java.util.ArrayList;
import java.io.StringWriter;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import javax.xml.stream.events.XMLEvent;
import org.slf4j.Logger;

class StaxThreddsXmlParserUtils
{
    private static Logger log;
    
    private StaxThreddsXmlParserUtils() {
    }
    
    static boolean isEventStartOrEndElementWithMatchingName(final XMLEvent event, final QName elementName) {
        if (event == null) {
            throw new IllegalArgumentException("Event may not be null.");
        }
        QName eventElementName = null;
        if (event.isStartElement()) {
            eventElementName = event.asStartElement().getName();
        }
        else {
            if (!event.isEndElement()) {
                return false;
            }
            eventElementName = event.asEndElement().getName();
        }
        return eventElementName.equals(elementName);
    }
    
    public static StartElement readNextEventCheckItIsStartElementWithExpectedName(final XMLEventReader xmlEventReader, final QName startElementName) throws ThreddsXmlParserException {
        if (!xmlEventReader.hasNext()) {
            throw new IllegalStateException("XMLEventReader has no further events.");
        }
        StartElement startElement = null;
        try {
            final XMLEvent event = xmlEventReader.peek();
            if (!event.isStartElement()) {
                throw new IllegalStateException("Next event must be StartElement.");
            }
            if (!event.asStartElement().getName().equals(startElementName)) {
                throw new IllegalStateException("Start element must be an '" + startElementName.getLocalPart() + "' element.");
            }
            startElement = xmlEventReader.nextEvent().asStartElement();
        }
        catch (XMLStreamException e) {
            final String msg = "Problem reading XML stream.";
            StaxThreddsXmlParserUtils.log.warn("readNextEventCheckItIsStartElementWithExpectedName(): " + msg, e);
            throw new ThreddsXmlParserException(new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.FATAL, msg, null, e));
        }
        return startElement;
    }
    
    public static void readNextEventCheckItIsEndElementWithExpectedName(final XMLEventReader xmlEventReader, final QName elementName) throws ThreddsXmlParserException {
        if (!xmlEventReader.hasNext()) {
            throw new IllegalStateException("XMLEventReader has no further events.");
        }
        try {
            final XMLEvent event = xmlEventReader.peek();
            if (!event.isEndElement()) {
                throw new IllegalStateException("Next event must be EndElement.");
            }
            if (!event.asEndElement().getName().equals(elementName)) {
                throw new IllegalStateException("End element must be an '" + elementName.getLocalPart() + "' element.");
            }
            xmlEventReader.nextEvent();
        }
        catch (XMLStreamException e) {
            final String msg = "Problem reading XML stream.";
            StaxThreddsXmlParserUtils.log.warn("readNextEventCheckItIsEndElementWithExpectedName(): " + msg, e);
            throw new ThreddsXmlParserException(new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.FATAL, msg, null, e));
        }
    }
    
    static String getLocationInfo(final XMLEventReader xmlEventReader) {
        final Location location = getLocation(xmlEventReader);
        final StringBuilder sb = new StringBuilder().append("Location: SysId[").append(location.getSystemId()).append("] line[").append(location.getLineNumber()).append("] column[").append(location.getColumnNumber()).append("] charOffset[").append(location.getCharacterOffset()).append("].");
        return sb.toString();
    }
    
    static Location getLocation(final XMLEventReader xmlEventReader) {
        if (xmlEventReader == null) {
            throw new IllegalArgumentException("XMLEventReader may not be null.");
        }
        if (!xmlEventReader.hasNext()) {
            throw new IllegalArgumentException("XMLEventReader must have next event.");
        }
        XMLEvent nextEvent = null;
        try {
            nextEvent = xmlEventReader.peek();
        }
        catch (XMLStreamException e) {
            throw new IllegalArgumentException("Could not peek() next event.");
        }
        return nextEvent.getLocation();
    }
    
    static ThreddsXmlParserIssue createIssueForException(final String message, final XMLEventReader xmlEventReader, final Exception e) throws ThreddsXmlParserException {
        final String locationInfo = getLocationInfo(xmlEventReader);
        final String msg = message + ":\n    " + locationInfo + ": " + e.getMessage();
        StaxThreddsXmlParserUtils.log.debug("createIssueForException(): " + msg);
        return new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.WARNING, msg, null, e);
    }
    
    static ThreddsXmlParserIssue createIssueForUnexpectedElement(final String message, final XMLEventReader xmlEventReader) throws ThreddsXmlParserException {
        final String locationInfo = getLocationInfo(xmlEventReader);
        final String unexpectedElemAsString = consumeElementAndConvertToXmlString(xmlEventReader);
        final String msg = message + ":\n    " + locationInfo + ":\n" + unexpectedElemAsString;
        StaxThreddsXmlParserUtils.log.debug("createIssueForUnexpectedElement(): " + msg);
        return new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.WARNING, msg, null, null);
    }
    
    static ThreddsXmlParserIssue createIssueForUnexpectedEvent(final String message, final ThreddsXmlParserIssue.Severity severity, final XMLEventReader xmlEventReader, final XMLEvent event) throws ThreddsXmlParserException {
        final String locationInfo = getLocationInfo(xmlEventReader);
        final String msg = message + " [" + severity.toString() + "]:\n    " + locationInfo + ":\n";
        StaxThreddsXmlParserUtils.log.debug("createIssueForUnexpectedElement(): " + msg);
        return new ThreddsXmlParserIssue(severity, msg, null, null);
    }
    
    static String consumeElementAndConvertToXmlString(final XMLEventReader xmlEventReader) throws ThreddsXmlParserException {
        if (xmlEventReader == null) {
            throw new IllegalArgumentException("XMLEventReader may not be null.");
        }
        final StringWriter writerUsingWriteAsEncodedUnicode = new StringWriter();
        final StringWriter writerUsingToString = new StringWriter();
        Location startLocation = null;
        try {
            XMLEvent event = xmlEventReader.peek();
            if (!event.isStartElement()) {
                throw new IllegalArgumentException("Next event in reader must be start element.");
            }
            startLocation = event.getLocation();
            final List<QName> nameList = new ArrayList<QName>();
            while (xmlEventReader.hasNext()) {
                event = xmlEventReader.nextEvent();
                if (event.isStartElement()) {
                    nameList.add(event.asStartElement().getName());
                }
                else if (event.isEndElement()) {
                    final QName endElemName = event.asEndElement().getName();
                    final QName lastName = nameList.get(nameList.size() - 1);
                    if (!lastName.equals(endElemName)) {
                        final String msg = "Badly formed XML? End element [" + endElemName.getLocalPart() + "] doesn't match expected start element [" + lastName.getLocalPart() + "].";
                        StaxThreddsXmlParserUtils.log.error("consumeElementAndConvertToXmlString(): " + msg);
                        throw new ThreddsXmlParserException("FATAL? " + msg);
                    }
                    nameList.remove(nameList.size() - 1);
                }
                event.writeAsEncodedUnicode(writerUsingWriteAsEncodedUnicode);
                writerUsingToString.write(event.toString());
                if (nameList.isEmpty()) {
                    break;
                }
            }
        }
        catch (XMLStreamException e) {
            throw new ThreddsXmlParserException("Problem reading unknown element [" + startLocation + "]. Underlying cause: " + e.getMessage(), e);
        }
        String result = writerUsingWriteAsEncodedUnicode.toString();
        if (result == null || result.equals("")) {
            result = writerUsingToString.toString();
        }
        return result;
    }
    
    static String getCharacterContent(final XMLEventReader xmlEventReader, final QName containingElementName) throws ThreddsXmlParserException {
        if (xmlEventReader == null) {
            throw new IllegalArgumentException("XMLEventReader may not be null.");
        }
        if (containingElementName == null) {
            throw new IllegalArgumentException("Containing element name may not be null.");
        }
        if (!xmlEventReader.hasNext()) {
            throw new IllegalStateException("XMLEventReader must have next.");
        }
        final StringBuilder stringBuilder = new StringBuilder();
        Location location = null;
        try {
            while (xmlEventReader.hasNext()) {
                XMLEvent event = xmlEventReader.peek();
                location = event.getLocation();
                if (event.isCharacters()) {
                    event = xmlEventReader.nextEvent();
                    stringBuilder.append(event.asCharacters().getData());
                }
                else if (event.isEndElement()) {
                    if (event.asEndElement().getName().equals(containingElementName)) {
                        return stringBuilder.toString().trim();
                    }
                    throw new IllegalStateException("Badly formed XML? Unexpected end element [" + event.asEndElement().getName().getLocalPart() + "][" + location + "] doesn't match expected start element [" + containingElementName.getLocalPart() + "].");
                }
                else {
                    if (event.isStartElement()) {
                        throw new IllegalStateException("Badly formed XML? Unexpected start element [" + event.asStartElement().getName().getLocalPart() + "][" + location + "] when characters expected.");
                    }
                    xmlEventReader.next();
                }
            }
        }
        catch (XMLStreamException e) {
            throw new ThreddsXmlParserException("Problem reading unknown event [" + location + "]. Underlying cause: " + e.getMessage(), e);
        }
        throw new ThreddsXmlParserException("Unexpected end of XMLEventReader.");
    }
    
    static Source getSourceFromUri(final URI documentUri) throws ThreddsXmlParserException {
        final HttpUriResolver httpUriResolver = HttpUriResolverFactory.getDefaultHttpUriResolver(documentUri);
        InputStream is = null;
        try {
            httpUriResolver.makeRequest();
            is = httpUriResolver.getResponseBodyAsInputStream();
        }
        catch (IOException e) {
            throw new ThreddsXmlParserException("Problem accessing resource [" + documentUri.toString() + "].", e);
        }
        return new StreamSource(is, documentUri.toString());
    }
    
    static Source getSourceFromFile(final File file, final URI docBaseUri) throws ThreddsXmlParserException {
        if (file == null) {
            throw new IllegalArgumentException("File may not be null.");
        }
        Source source = null;
        if (docBaseUri == null) {
            source = new StreamSource(file);
        }
        else {
            InputStream is = null;
            try {
                is = new FileInputStream(file);
            }
            catch (FileNotFoundException e) {
                final String message = "Couldn't find file [" + file.getPath() + "].";
                StaxThreddsXmlParserUtils.log.error("parseIntoBuilder(): " + message, e);
                throw new ThreddsXmlParserException(message, e);
            }
            source = new StreamSource(is, docBaseUri.toString());
        }
        return source;
    }
    
    static XMLEventReader getEventReaderFromSource(final Source source, final XMLInputFactory factory) throws ThreddsXmlParserException {
        XMLEventReader reader;
        try {
            reader = factory.createXMLEventReader(source);
        }
        catch (XMLStreamException e) {
            final String message = "Problems reading stream [" + source.getSystemId() + "].";
            StaxThreddsXmlParserUtils.log.error("getEventReaderFromSource(): " + message, e);
            throw new ThreddsXmlParserException(message, e);
        }
        return reader;
    }
    
    static {
        StaxThreddsXmlParserUtils.log = LoggerFactory.getLogger(StaxThreddsXmlParserUtils.class);
    }
}
