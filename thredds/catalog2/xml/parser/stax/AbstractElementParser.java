// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.StartElement;
import javax.xml.stream.XMLStreamException;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.XMLEvent;
import org.slf4j.LoggerFactory;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import org.slf4j.Logger;

abstract class AbstractElementParser
{
    Logger log;
    final QName elementName;
    final XMLEventReader reader;
    final ThreddsBuilderFactory builderFactory;
    
    AbstractElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory) {
        this.log = LoggerFactory.getLogger(this.getClass());
        if (elementName == null || reader == null || builderFactory == null) {
            throw new IllegalArgumentException("Element name, XMLEventReader, and/or BuilderFactory may not be null.");
        }
        this.elementName = elementName;
        this.reader = reader;
        this.builderFactory = builderFactory;
    }
    
    boolean isSelfElement(final XMLEvent event) {
        return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
    }
    
    abstract void parseStartElement() throws ThreddsXmlParserException;
    
    abstract void handleChildStartElement() throws ThreddsXmlParserException;
    
    abstract void postProcessingAfterEndElement() throws ThreddsXmlParserException;
    
    abstract ThreddsBuilder getSelfBuilder();
    
    final ThreddsBuilder parse() throws ThreddsXmlParserException {
        try {
            this.parseStartElement();
            while (this.reader.hasNext()) {
                final XMLEvent event = this.reader.peek();
                if (event.isStartElement()) {
                    this.handleChildStartElement();
                }
                else if (event.isEndElement()) {
                    if (this.isSelfElement(event.asEndElement())) {
                        this.reader.next();
                        break;
                    }
                    if (!(this instanceof ThreddsMetadataElementParser)) {
                        final String msg = "Unrecognized end element [" + event.asEndElement().getName() + "].";
                        final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForUnexpectedEvent(msg, ThreddsXmlParserIssue.Severity.FATAL, this.reader, event);
                        this.log.error(this.getClass().getName() + ".parse(): " + issue.getMessage());
                        throw new ThreddsXmlParserException(issue);
                    }
                    if (this.log.isDebugEnabled()) {
                        final String msg = "End element probably parent of ThreddsMetadata [" + event.asEndElement().getName() + "].";
                        final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForUnexpectedEvent(msg, ThreddsXmlParserIssue.Severity.WARNING, this.reader, event);
                        this.log.debug("parse(): " + issue.getMessage());
                        break;
                    }
                    break;
                }
                else {
                    this.log.debug(this.getClass().getName() + ".parse(): Unhandled event [" + event.getLocation() + "--" + event + "].");
                    this.reader.next();
                }
            }
            this.postProcessingAfterEndElement();
            return this.getSelfBuilder();
        }
        catch (XMLStreamException e) {
            this.log.error("parse(): Failed to parse " + this.elementName + " element: " + e.getMessage(), e);
            throw new ThreddsXmlParserException("Failed to parse " + this.elementName + " element: " + e.getMessage(), e);
        }
    }
    
    StartElement getNextEventIfStartElementIsMine() throws ThreddsXmlParserException {
        if (!this.reader.hasNext()) {
            throw new ThreddsXmlParserException("XMLEventReader has no further events.");
        }
        StartElement startElement = null;
        try {
            final XMLEvent event = this.reader.peek();
            if (!event.isStartElement()) {
                throw new ThreddsXmlParserException("Next event must be StartElement.");
            }
            if (!event.asStartElement().getName().equals(this.elementName)) {
                throw new ThreddsXmlParserException("Start element must be an '" + this.elementName.getLocalPart() + "' element.");
            }
            startElement = this.reader.nextEvent().asStartElement();
        }
        catch (XMLStreamException e) {
            throw new ThreddsXmlParserException("Problem reading from XMLEventReader.");
        }
        return startElement;
    }
    
    StartElement peekAtNextEventIfStartElement() throws ThreddsXmlParserException {
        if (!this.reader.hasNext()) {
            throw new ThreddsXmlParserException("XMLEventReader has no further events.");
        }
        StartElement startElement = null;
        while (this.reader.hasNext()) {
            XMLEvent event = null;
            try {
                event = this.reader.peek();
            }
            catch (XMLStreamException e) {
                final String msg = "Problem reading from XMLEventReader.";
                final ThreddsXmlParserIssue issue = StaxThreddsXmlParserUtils.createIssueForException(msg, this.reader, e);
                this.log.error("peekAtNextEventIfStartElement(): " + issue.getMessage());
                throw new ThreddsXmlParserException(issue);
            }
            if (event.isStartElement()) {
                startElement = event.asStartElement();
                break;
            }
            if (!event.isCharacters() || !event.asCharacters().isWhiteSpace()) {
                final String msg2 = "Expecting StartElement for next event [" + event.getClass().getName() + "]";
                final ThreddsXmlParserIssue issue2 = StaxThreddsXmlParserUtils.createIssueForUnexpectedEvent(msg2, ThreddsXmlParserIssue.Severity.FATAL, this.reader, event);
                this.log.error("peekAtNextEventIfStartElement(): " + issue2.getMessage());
                throw new ThreddsXmlParserException(issue2);
            }
            this.reader.next();
        }
        return startElement;
    }
}
