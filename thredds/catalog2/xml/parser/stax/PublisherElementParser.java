// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.parser.ThreddsXmlParserIssue;
import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

class PublisherElementParser extends AbstractElementParser
{
    private final ThreddsMetadataBuilder parentBuilder;
    private ThreddsMetadataBuilder.ContributorBuilder selfBuilder;
    private final CreatorElementParser.NameElementParser.Factory nameElemParserFactory;
    private final CreatorElementParser.ContactElementParser.Factory contactElemParserFactory;
    
    private PublisherElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
        super(elementName, reader, builderFactory);
        this.parentBuilder = parentBuilder;
        this.nameElemParserFactory = new CreatorElementParser.NameElementParser.Factory();
        this.contactElemParserFactory = new CreatorElementParser.ContactElementParser.Factory();
    }
    
    @Override
    ThreddsBuilder getSelfBuilder() {
        return null;
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        this.getNextEventIfStartElementIsMine();
        this.selfBuilder = this.parentBuilder.addPublisher();
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.nameElemParserFactory.isEventMyStartElement(startElement)) {
            final CreatorElementParser.NameElementParser elementParser = this.nameElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            elementParser.parse();
        }
        else {
            if (!this.contactElemParserFactory.isEventMyStartElement(startElement)) {
                final String unexpectedElement = StaxThreddsXmlParserUtils.consumeElementAndConvertToXmlString(this.reader);
                final ThreddsXmlParserIssue issue = new ThreddsXmlParserIssue(ThreddsXmlParserIssue.Severity.ERROR, "Unrecognized element: " + unexpectedElement, this.selfBuilder, null);
                throw new ThreddsXmlParserException(issue);
            }
            final CreatorElementParser.ContactElementParser elementParser2 = this.contactElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder);
            elementParser2.parse();
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = ThreddsMetadataElementNames.PublisherElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        PublisherElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsMetadataBuilder parentBuilder) {
            return new PublisherElementParser(this.elementName, reader, builderFactory, parentBuilder, null);
        }
    }
}
