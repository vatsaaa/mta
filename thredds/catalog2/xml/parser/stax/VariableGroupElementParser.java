// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.parser.stax;

import javax.xml.stream.events.XMLEvent;
import thredds.catalog2.xml.parser.ThreddsXmlParserException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import thredds.catalog2.xml.names.ThreddsMetadataElementNames;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.namespace.QName;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

class VariableGroupElementParser extends AbstractElementParser
{
    private final ThreddsMetadataBuilder parentBuilder;
    private ThreddsMetadataBuilder.VariableGroupBuilder selfBuilder;
    private VariableElementParser.Factory varElemParserFactory;
    private VariableElementParser varElemParser;
    
    private VariableGroupElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
        super(elementName, reader, builderFactory);
        this.parentBuilder = (ThreddsMetadataBuilder)parentBuilder;
        this.varElemParserFactory = new VariableElementParser.Factory();
    }
    
    @Override
    void parseStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.getNextEventIfStartElementIsMine();
        this.selfBuilder = this.parentBuilder.addVariableGroupBuilder();
        Attribute att = startElement.getAttributeByName(ThreddsMetadataElementNames.VariablesElement_vocabAuthorityId);
        if (att != null) {
            this.selfBuilder.setVocabularyAuthorityId(att.getValue());
        }
        att = startElement.getAttributeByName(ThreddsMetadataElementNames.VariablesElement_vocabAuthorityUrl);
        if (att != null) {
            this.selfBuilder.setVocabularyAuthorityUrl(att.getValue());
        }
    }
    
    @Override
    void handleChildStartElement() throws ThreddsXmlParserException {
        final StartElement startElement = this.peekAtNextEventIfStartElement();
        if (this.varElemParserFactory.isEventMyStartElement(startElement)) {
            (this.varElemParser = this.varElemParserFactory.getNewParser(this.reader, this.builderFactory, this.selfBuilder)).parseElement();
        }
    }
    
    @Override
    void postProcessingAfterEndElement() throws ThreddsXmlParserException {
    }
    
    @Override
    ThreddsBuilder getSelfBuilder() {
        return this.selfBuilder;
    }
    
    static class Factory
    {
        private QName elementName;
        
        Factory() {
            this.elementName = ThreddsMetadataElementNames.VariablesElement;
        }
        
        boolean isEventMyStartElement(final XMLEvent event) {
            return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
        }
        
        VariableGroupElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
            return new VariableGroupElementParser(this.elementName, reader, builderFactory, parentBuilder, null);
        }
    }
    
    static class VariableElementParser
    {
        private final QName elementName;
        private final XMLEventReader reader;
        private final ThreddsBuilderFactory builderFactory;
        private final ThreddsMetadataBuilder.VariableGroupBuilder parentBuilder;
        private ThreddsMetadataBuilder.VariableBuilder selfBuilder;
        
        private VariableElementParser(final QName elementName, final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
            this.elementName = elementName;
            this.reader = reader;
            this.builderFactory = builderFactory;
            this.parentBuilder = (ThreddsMetadataBuilder.VariableGroupBuilder)parentBuilder;
        }
        
        void parseElement() throws ThreddsXmlParserException {
            final StartElement startElement = StaxThreddsXmlParserUtils.readNextEventCheckItIsStartElementWithExpectedName(this.reader, this.elementName);
            Attribute att = startElement.getAttributeByName(ThreddsMetadataElementNames.VariablesElement_VariableElement_name);
            final String name = (att != null) ? att.getValue() : null;
            att = startElement.getAttributeByName(ThreddsMetadataElementNames.VariablesElement_VariableElement_units);
            final String units = (att != null) ? att.getValue() : null;
            att = startElement.getAttributeByName(ThreddsMetadataElementNames.VariablesElement_VariableElement_vocabularyId);
            final String vocabularyId = (att != null) ? att.getValue() : null;
            att = startElement.getAttributeByName(ThreddsMetadataElementNames.VariablesElement_VariableElement_vocabularyName);
            final String vocabularyName = (att != null) ? att.getValue() : null;
            final String description = StaxThreddsXmlParserUtils.getCharacterContent(this.reader, this.elementName);
            this.selfBuilder = this.parentBuilder.addVariableBuilder(name, description, units, vocabularyId, vocabularyName);
            StaxThreddsXmlParserUtils.readNextEventCheckItIsEndElementWithExpectedName(this.reader, this.elementName);
        }
        
        static class Factory
        {
            private QName elementName;
            
            Factory() {
                this.elementName = ThreddsMetadataElementNames.VariablesElement_VariableElement;
            }
            
            boolean isEventMyStartElement(final XMLEvent event) {
                return StaxThreddsXmlParserUtils.isEventStartOrEndElementWithMatchingName(event, this.elementName);
            }
            
            VariableElementParser getNewParser(final XMLEventReader reader, final ThreddsBuilderFactory builderFactory, final ThreddsBuilder parentBuilder) {
                return new VariableElementParser(this.elementName, reader, builderFactory, parentBuilder);
            }
        }
    }
}
