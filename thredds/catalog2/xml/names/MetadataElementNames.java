// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class MetadataElementNames
{
    public static final QName MetadataElement;
    public static final QName MetadataElement_Inherited;
    public static final QName MetadataElement_XlinkTitle;
    public static final QName MetadataElement_XlinkHref;
    public static final QName MetadataElement_XlinkType;
    public static final QName MetadataElement_metadataType;
    
    private MetadataElementNames() {
    }
    
    static {
        MetadataElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "metadata");
        MetadataElement_Inherited = new QName("", "inherited");
        MetadataElement_XlinkTitle = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "title");
        MetadataElement_XlinkHref = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "href");
        MetadataElement_XlinkType = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "type");
        MetadataElement_metadataType = new QName("", "metadataType");
    }
}
