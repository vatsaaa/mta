// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class PropertyElementNames
{
    public static final QName PropertyElement;
    public static final QName PropertyElement_Name;
    public static final QName PropertyElement_Value;
    
    private PropertyElementNames() {
    }
    
    static {
        PropertyElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "property");
        PropertyElement_Name = new QName("", "name");
        PropertyElement_Value = new QName("", "value");
    }
}
