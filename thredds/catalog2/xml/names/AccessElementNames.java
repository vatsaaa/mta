// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class AccessElementNames
{
    public static final QName AccessElement;
    public static final QName AccessElement_ServiceName;
    public static final QName AccessElement_UrlPath;
    public static final QName AccessElement_DataFormat;
    
    private AccessElementNames() {
    }
    
    static {
        AccessElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "access");
        AccessElement_ServiceName = new QName("", "serviceName");
        AccessElement_UrlPath = new QName("", "urlPath");
        AccessElement_DataFormat = new QName("", "dataFormat");
    }
}
