// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class DatasetNodeElementNames
{
    public static final QName DatasetNodeElement_Id;
    @Deprecated
    public static final QName DatasetNodeElement_Authority;
    public static final QName DatasetNodeElement_CollectionType;
    public static final QName DatasetNodeElement_Harvest;
    
    private DatasetNodeElementNames() {
    }
    
    static {
        DatasetNodeElement_Id = new QName("", "ID");
        DatasetNodeElement_Authority = new QName("", "authority");
        DatasetNodeElement_CollectionType = new QName("", "collectionType");
        DatasetNodeElement_Harvest = new QName("", "harvest");
    }
}
