// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class CatalogRefElementNames
{
    public static final QName CatalogRefElement;
    public static final QName CatalogRefElement_XlinkTitle;
    public static final QName CatalogRefElement_XlinkHref;
    public static final QName CatalogRefElement_XlinkType;
    
    private CatalogRefElementNames() {
    }
    
    static {
        CatalogRefElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "catalogRef");
        CatalogRefElement_XlinkTitle = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "title");
        CatalogRefElement_XlinkHref = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "href");
        CatalogRefElement_XlinkType = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "type");
    }
}
