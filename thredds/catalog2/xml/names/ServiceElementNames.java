// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class ServiceElementNames
{
    public static final QName ServiceElement;
    public static final QName ServiceElement_Name;
    public static final QName ServiceElement_Base;
    public static final QName ServiceElement_ServiceType;
    public static final QName ServiceElement_Description;
    public static final QName ServiceElement_Suffix;
    
    private ServiceElementNames() {
    }
    
    static {
        ServiceElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "service");
        ServiceElement_Name = new QName("", "name");
        ServiceElement_Base = new QName("", "base");
        ServiceElement_ServiceType = new QName("", "serviceType");
        ServiceElement_Description = new QName("", "desc");
        ServiceElement_Suffix = new QName("", "suffix");
    }
}
