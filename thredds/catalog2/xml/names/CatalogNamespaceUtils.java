// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class CatalogNamespaceUtils
{
    private CatalogNamespaceUtils() {
    }
    
    public static QName getThreddsCatalogElementQualifiedName(final String localName) {
        return new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), localName);
    }
    
    public static QName getThreddsCatalogAttributeQName(final String localName) {
        return new QName("", localName);
    }
}
