// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class CatalogElementNames
{
    public static final QName CatalogElement;
    public static final QName CatalogElement_Name;
    public static final QName CatalogElement_Expires;
    public static final QName CatalogElement_LastModified;
    public static final QName CatalogElement_Version;
    
    private CatalogElementNames() {
    }
    
    static {
        CatalogElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "catalog");
        CatalogElement_Name = new QName("", "name");
        CatalogElement_Expires = new QName("", "expires");
        CatalogElement_LastModified = new QName("", "lastModified");
        CatalogElement_Version = new QName("", "version");
    }
}
