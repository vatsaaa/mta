// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import javax.xml.namespace.QName;

public class DatasetElementNames
{
    public static final QName DatasetElement;
    public static final QName DatasetElement_Name;
    @Deprecated
    public static final QName DatasetElement_DataType;
    public static final QName DatasetElement_ResourceControl;
    @Deprecated
    public static final QName DatasetElement_ServiceName;
    public static final QName DatasetElement_UrlPath;
    
    private DatasetElementNames() {
    }
    
    static {
        DatasetElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "dataset");
        DatasetElement_Name = new QName("", "name");
        DatasetElement_DataType = new QName("", "dataType");
        DatasetElement_ResourceControl = new QName("", "resourceControl");
        DatasetElement_ServiceName = new QName("", "serviceName");
        DatasetElement_UrlPath = new QName("", "urlPath");
    }
}
