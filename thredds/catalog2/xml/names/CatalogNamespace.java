// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import org.xml.sax.SAXException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Schema;
import java.io.IOException;
import thredds.util.HttpUriResolver;
import thredds.util.HttpUriResolverFactory;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URI;

public enum CatalogNamespace
{
    CATALOG_1_0("", "http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", "/resources/thredds/schemas/InvCatalog.1.0.2.xsd", "http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.1.0.2.xsd"), 
    CATALOG_0_6("oldThredds", "http://www.unidata.ucar.edu/thredds", "/resources/thredds/schemas/InvCatalog.0.6.xsd", "http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.0.6.xsd"), 
    XLINK("xlink", "http://www.w3.org/1999/xlink", "/resources/thredds/schemas/xlink.xsd", "");
    
    private String standardPrefix;
    private String namespaceUri;
    private String schemaLocalResourceName;
    private URI schemaRemoteResourceUri;
    
    public static CatalogNamespace getNamespace(final String namespaceUri) {
        if (namespaceUri == null) {
            return null;
        }
        for (final CatalogNamespace curNs : values()) {
            if (curNs.namespaceUri.equals(namespaceUri)) {
                return curNs;
            }
        }
        return null;
    }
    
    private CatalogNamespace(final String standardPrefix, final String namespaceUri, final String schemaLocalResourceName, final String schemaRemoteResourceUri) {
        if (namespaceUri == null) {
            throw new IllegalArgumentException("The XML Namespace URI may not be null.");
        }
        if (schemaRemoteResourceUri == null) {
            throw new IllegalArgumentException("The remote resourc URL for the XML Schema may not be null.");
        }
        this.standardPrefix = standardPrefix;
        this.namespaceUri = namespaceUri;
        this.schemaLocalResourceName = schemaLocalResourceName;
        try {
            this.schemaRemoteResourceUri = new URI(schemaRemoteResourceUri);
        }
        catch (URISyntaxException e) {
            throw new IllegalArgumentException("Badly formed resource URI [" + schemaRemoteResourceUri + "].", e);
        }
    }
    
    public String getStandardPrefix() {
        return this.standardPrefix;
    }
    
    public String getNamespaceUri() {
        return this.namespaceUri;
    }
    
    public String getSchemaLocalResourceName() {
        return this.schemaLocalResourceName;
    }
    
    public URI getSchemaRemoteResourceUri() {
        return this.schemaRemoteResourceUri;
    }
    
    public InputStream getSchemaAsInputStream() throws IOException {
        InputStream inStream = null;
        if (this.getSchemaLocalResourceName() != null) {
            inStream = this.getClass().getClassLoader().getResourceAsStream(this.getSchemaLocalResourceName());
        }
        if (inStream == null && this.getSchemaRemoteResourceUri() != null) {
            final HttpUriResolver httpUriResolver = HttpUriResolverFactory.getDefaultHttpUriResolver(this.getSchemaRemoteResourceUri());
            httpUriResolver.makeRequest();
            inStream = httpUriResolver.getResponseBodyAsInputStream();
        }
        return inStream;
    }
    
    public Schema getSchema() throws IOException, SAXException {
        final SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        final StreamSource source = new StreamSource(this.getSchemaAsInputStream());
        source.setSystemId(this.getSchemaRemoteResourceUri().toString());
        return schemaFactory.newSchema(source);
    }
}
