// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.xml.names;

import thredds.catalog2.ThreddsMetadata;
import javax.xml.namespace.QName;

public class ThreddsMetadataElementNames
{
    public static final QName ThreddsMetadataElement;
    public static final QName ServiceNameElement;
    public static final QName AuthorityElement;
    public static final QName DocumentationElement;
    public static final QName DocumentationElement_Type;
    public static final String DocumentationElement_Type_Funding = "funding";
    public static final String DocumentationElement_Type_History = "history";
    public static final String DocumentationElement_Type_ProcessingLevel = "processing_level";
    public static final String DocumentationElement_Type_Rights = "rights";
    public static final String DocumentationElement_Type_Summary = "summary";
    public static final QName DocumentationElement_XlinkTitle;
    public static final QName DocumentationElement_XlinkHref;
    public static final QName DocumentationElement_XlinkType;
    public static final QName KeywordElement;
    public static final QName ProjectElement;
    public static final QName ControlledVocabType_Authority;
    public static final QName DateElement;
    public static final QName DateElement_Type;
    public static final String DateElement_Type_Created;
    public static final String DateElement_Type_Modified;
    public static final String DateElement_Type_Valid;
    public static final String DateElement_Type_Issued;
    public static final String DateElement_Type_Available;
    public static final String DateElement_Type_MetadataCreated;
    public static final String DateElement_Type_MetadataModified;
    public static final QName DateElement_Format;
    public static final QName CreatorElement;
    public static final QName CreatorElement_NameElement;
    public static final QName CreatorElement_NameElement_NamingAuthority;
    public static final QName CreatorElement_ContactElement;
    public static final QName CreatorElement_ContactElement_Email;
    public static final QName CreatorElement_ContactElement_Url;
    public static final QName ContributorElement;
    public static final QName ContributorElement_Role;
    public static final QName PublisherElement;
    public static final QName PublisherElement_NameElement;
    public static final QName PublisherElement_NameElement_NamingAuthority;
    public static final QName PublisherElement_ContactElement;
    public static final QName PublisherElement_ContactElement_Email;
    public static final QName PublisherElement_ContactElement_Url;
    public static final QName GeospatialCoverageElement;
    public static final QName GeospatialCoverageElement_NameElement;
    public static final QName GeospatialCoverageElement_NorthsouthElement;
    public static final QName GeospatialCoverageElement_EastwestElement;
    public static final QName GeospatialCoverageElement_UpdownElement;
    public static final QName GeospatialCoverageElement_Zpositive;
    public static final QName SpatialRangeType_Start;
    public static final QName SpatialRangeType_Size;
    public static final QName SpatialRangeType_Resolution;
    public static final QName SpatialRangeType_Units;
    public static final QName TimeCoverageElement;
    public static final QName DateRangeType_StartElement;
    public static final QName DateRangeType_EndElement;
    public static final QName DateRangeType_DurationElement;
    public static final QName DateRangeType_ResolutionElement;
    public static final QName DateType_Format;
    public static final QName DateType_Type;
    public static final QName VariablesElement;
    public static final QName VariablesElement_vocabAuthorityId;
    public static final QName VariablesElement_vocabAuthorityUrl;
    public static final QName VariablesElement_VariableElement;
    public static final QName VariablesElement_VariableElement_name;
    public static final QName VariablesElement_VariableElement_units;
    public static final QName VariablesElement_VariableElement_vocabularyId;
    public static final QName VariablesElement_VariableElement_vocabularyName;
    public static final QName VariablesElement_VariableMapElement;
    public static final QName VariablesElement_VariableMapElement_XlinkHref;
    public static final QName DataSizeElement;
    public static final QName DataFormatElement;
    public static final QName DataTypeElement;
    
    private ThreddsMetadataElementNames() {
    }
    
    static {
        ThreddsMetadataElement = new QName("", "_proxy_");
        ServiceNameElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "serviceName");
        AuthorityElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "authority");
        DocumentationElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "documentation");
        DocumentationElement_Type = new QName("", "type");
        DocumentationElement_XlinkTitle = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "title");
        DocumentationElement_XlinkHref = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "href");
        DocumentationElement_XlinkType = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "type");
        KeywordElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "keyword");
        ProjectElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "project");
        ControlledVocabType_Authority = new QName("", "vocabulary");
        DateElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "date");
        DateElement_Type = new QName("", "type");
        DateElement_Type_Created = ThreddsMetadata.DatePointType.Created.toString();
        DateElement_Type_Modified = ThreddsMetadata.DatePointType.Modified.toString();
        DateElement_Type_Valid = ThreddsMetadata.DatePointType.Valid.toString();
        DateElement_Type_Issued = ThreddsMetadata.DatePointType.Issued.toString();
        DateElement_Type_Available = ThreddsMetadata.DatePointType.Available.toString();
        DateElement_Type_MetadataCreated = ThreddsMetadata.DatePointType.MetadataCreated.toString();
        DateElement_Type_MetadataModified = ThreddsMetadata.DatePointType.MetadataModified.toString();
        DateElement_Format = new QName("", "format");
        CreatorElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "creator");
        CreatorElement_NameElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "name");
        CreatorElement_NameElement_NamingAuthority = new QName("", "vocabulary");
        CreatorElement_ContactElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "contact");
        CreatorElement_ContactElement_Email = new QName("", "email");
        CreatorElement_ContactElement_Url = new QName("", "url");
        ContributorElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "contributor");
        ContributorElement_Role = new QName("", "role");
        PublisherElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "publisher");
        PublisherElement_NameElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "name");
        PublisherElement_NameElement_NamingAuthority = new QName("", "vocabulary");
        PublisherElement_ContactElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "contact");
        PublisherElement_ContactElement_Email = new QName("", "email");
        PublisherElement_ContactElement_Url = new QName("", "url");
        GeospatialCoverageElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "geospatialCoverage");
        GeospatialCoverageElement_NameElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "name");
        GeospatialCoverageElement_NorthsouthElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "northsouth");
        GeospatialCoverageElement_EastwestElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "eastwest");
        GeospatialCoverageElement_UpdownElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "updown");
        GeospatialCoverageElement_Zpositive = new QName("", "zpositive");
        SpatialRangeType_Start = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "start");
        SpatialRangeType_Size = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "size");
        SpatialRangeType_Resolution = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "resolution");
        SpatialRangeType_Units = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "units");
        TimeCoverageElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "timeCoverage");
        DateRangeType_StartElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "start");
        DateRangeType_EndElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "end");
        DateRangeType_DurationElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "duration");
        DateRangeType_ResolutionElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "resolution");
        DateType_Format = new QName("", "format");
        DateType_Type = new QName("", "type");
        VariablesElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "variables");
        VariablesElement_vocabAuthorityId = new QName("", "vocabulary");
        VariablesElement_vocabAuthorityUrl = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "href");
        VariablesElement_VariableElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "variable");
        VariablesElement_VariableElement_name = new QName("", "name");
        VariablesElement_VariableElement_units = new QName("", "units");
        VariablesElement_VariableElement_vocabularyId = new QName("", "vocabulary_id");
        VariablesElement_VariableElement_vocabularyName = new QName("", "vocabulary_name");
        VariablesElement_VariableMapElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "variableMap");
        VariablesElement_VariableMapElement_XlinkHref = new QName(CatalogNamespace.XLINK.getNamespaceUri(), "href");
        DataSizeElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "dataSize");
        DataFormatElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "dataFormat");
        DataTypeElement = new QName(CatalogNamespace.CATALOG_1_0.getNamespaceUri(), "dataType");
    }
}
