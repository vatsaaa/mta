// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.net.URISyntaxException;
import java.net.URI;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class BasicAccessUriBuilder implements AccessUriBuilder
{
    private Logger log;
    
    public BasicAccessUriBuilder() {
        this.log = LoggerFactory.getLogger(this.getClass());
    }
    
    public URI buildAccessUri(final Access access, final URI docBaseUri) {
        if (access == null) {
            throw new IllegalArgumentException("Access must not be null.");
        }
        URI baseServiceUri = access.getService().getBaseUri();
        if (!baseServiceUri.isAbsolute()) {
            if (docBaseUri == null) {
                throw new IllegalStateException("Document base URI must not be null if service base URI is not absolute.");
            }
            baseServiceUri = docBaseUri.resolve(baseServiceUri);
        }
        final StringBuilder sb = new StringBuilder(baseServiceUri.toString());
        sb.append(access.getUrlPath());
        final String suffix = access.getService().getSuffix();
        if (suffix != null && !suffix.equals("")) {
            sb.append(suffix);
        }
        try {
            return new URI(sb.toString());
        }
        catch (URISyntaxException e) {
            this.log.error("buildAccessUri(): URI syntax exception [" + sb.toString() + "].", e);
            return null;
        }
    }
}
