// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.Dataset;
import thredds.catalog.ServiceType;
import java.util.List;

public interface DatasetBuilder extends DatasetNodeBuilder
{
    AccessBuilder addAccessBuilder();
    
    boolean removeAccessBuilder(final AccessBuilder p0);
    
    boolean isAccessible();
    
    List<AccessBuilder> getAccessBuilders();
    
    List<AccessBuilder> getAccessBuildersByType(final ServiceType p0);
    
    Dataset build() throws BuilderException;
}
