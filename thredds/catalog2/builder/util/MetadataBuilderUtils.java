// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder.util;

import thredds.catalog2.builder.ThreddsBuilderFactory;
import thredds.catalog2.builder.MetadataBuilder;

public class MetadataBuilderUtils
{
    private MetadataBuilderUtils() {
    }
    
    public static MetadataBuilder copyIntoNewMetadataBuilder(final MetadataBuilder source, final ThreddsBuilderFactory builderFactory) {
        if (source == null) {
            throw new IllegalArgumentException("Source builder may not be null.");
        }
        if (builderFactory == null) {
            throw new IllegalArgumentException("Builder factory may not be null.");
        }
        final MetadataBuilder result = builderFactory.newMetadataBuilder();
        copyMetadataBuilder(source, result);
        return result;
    }
    
    public static MetadataBuilder copyMetadataBuilder(final MetadataBuilder source, final MetadataBuilder recipient) {
        if (source == null) {
            throw new IllegalArgumentException("Source builder may not be null.");
        }
        if (recipient == null) {
            throw new IllegalArgumentException("Recipient builder may not be null.");
        }
        recipient.setContainedContent(source.isContainedContent());
        if (source.isContainedContent()) {
            recipient.setContent(source.getContent());
        }
        else {
            recipient.setTitle(source.getTitle());
            recipient.setExternalReference(source.getExternalReference());
        }
        return recipient;
    }
}
