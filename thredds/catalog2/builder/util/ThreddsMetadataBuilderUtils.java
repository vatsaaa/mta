// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder.util;

import thredds.catalog.DataFormatType;
import ucar.nc2.constants.FeatureType;
import java.util.Iterator;
import thredds.catalog2.builder.ThreddsBuilderFactory;
import thredds.catalog2.builder.ThreddsMetadataBuilder;

public class ThreddsMetadataBuilderUtils
{
    private ThreddsMetadataBuilderUtils() {
    }
    
    public static ThreddsMetadataBuilder copyIntoNewThreddsMetadataBuilder(final ThreddsMetadataBuilder source, final ThreddsBuilderFactory builderFactory) {
        if (source == null) {
            throw new IllegalArgumentException("Source builder may not be null.");
        }
        if (builderFactory == null) {
            throw new IllegalArgumentException("Builder factory may not be null.");
        }
        final ThreddsMetadataBuilder result = builderFactory.newThreddsMetadataBuilder();
        copyThreddsMetadataBuilder(source, result);
        return result;
    }
    
    public static ThreddsMetadataBuilder copyThreddsMetadataBuilder(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder recipient) {
        if (source == null) {
            throw new IllegalArgumentException("Source builder may not be null.");
        }
        if (recipient == null) {
            throw new IllegalArgumentException("Recipient builder may not be null.");
        }
        if (source.getCollectionType() != null) {
            recipient.setCollectionType(source.getCollectionType());
        }
        if (source.getDataFormat() != null) {
            recipient.setDataFormat(source.getDataFormat());
        }
        recipient.setDataSizeInBytes(source.getDataSizeInBytes());
        if (source.getDataType() != null) {
            recipient.setDataType(source.getDataType());
        }
        if (source.getAvailableDatePointBuilder() != null) {
            final ThreddsMetadataBuilder.DatePointBuilder dpb = source.getAvailableDatePointBuilder();
            recipient.setAvailableDatePointBuilder(dpb.getDate(), dpb.getDateFormat());
        }
        if (source.getCreatedDatePointBuilder() != null) {
            final ThreddsMetadataBuilder.DatePointBuilder dpb = source.getCreatedDatePointBuilder();
            recipient.setCreatedDatePointBuilder(dpb.getDate(), dpb.getDateFormat());
        }
        if (source.getIssuedDatePointBuilder() != null) {
            final ThreddsMetadataBuilder.DatePointBuilder dpb = source.getIssuedDatePointBuilder();
            recipient.setIssuedDatePointBuilder(dpb.getDate(), dpb.getDateFormat());
        }
        if (source.getMetadataCreatedDatePointBuilder() != null) {
            final ThreddsMetadataBuilder.DatePointBuilder dpb = source.getMetadataCreatedDatePointBuilder();
            recipient.setMetadataCreatedDatePointBuilder(dpb.getDate(), dpb.getDateFormat());
        }
        if (source.getMetadataModifiedDatePointBuilder() != null) {
            final ThreddsMetadataBuilder.DatePointBuilder dpb = source.getMetadataModifiedDatePointBuilder();
            recipient.setMetadataModifiedDatePointBuilder(dpb.getDate(), dpb.getDateFormat());
        }
        if (source.getModifiedDatePointBuilder() != null) {
            final ThreddsMetadataBuilder.DatePointBuilder dpb = source.getModifiedDatePointBuilder();
            recipient.setModifiedDatePointBuilder(dpb.getDate(), dpb.getDateFormat());
        }
        if (source.getValidDatePointBuilder() != null) {
            final ThreddsMetadataBuilder.DatePointBuilder dpb = source.getValidDatePointBuilder();
            recipient.setValidDatePointBuilder(dpb.getDate(), dpb.getDateFormat());
        }
        final ThreddsMetadataBuilder.GeospatialCoverageBuilder geoCovBuilder = source.getGeospatialCoverageBuilder();
        if (geoCovBuilder != null && geoCovBuilder.getCRS() != null) {
            recipient.setNewGeospatialCoverageBuilder(geoCovBuilder.getCRS());
        }
        if (source.getTemporalCoverageBuilder() != null) {
            final ThreddsMetadataBuilder.DateRangeBuilder drb = source.getTemporalCoverageBuilder();
            recipient.setTemporalCoverageBuilder(drb.getStartDate(), drb.getStartDateFormat(), drb.getEndDate(), drb.getEndDateFormat(), drb.getDuration(), drb.getResolution());
        }
        addCopiesOfContributorBuilders(source, recipient);
        addCopiesOfCreatorBuilders(source, recipient);
        addCopiesOfDocumentationBuilders(source, recipient);
        addCopiesOfKeyphraseBuilders(source, recipient);
        addCopiesOfProjectNameBuilders(source, recipient);
        addCopiesOfPublisherBuilders(source, recipient);
        addCopiesOfVariableGroupBuilders(source, recipient);
        return recipient;
    }
    
    public static ThreddsMetadataBuilder mergeTwoThreddsMetadata(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsBuilderFactory builderFactory) {
        final ThreddsMetadataBuilder mergedResults = builderFactory.newThreddsMetadataBuilder();
        mergeTwoThreddsMetadata(first, second, mergedResults);
        return mergedResults;
    }
    
    public static void mergeTwoThreddsMetadata(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedResults) {
        mergeOverwriteCollectionType(first, second, mergedResults);
        mergeOverwriteDataFormat(first, second, mergedResults);
        mergeOverwriteDataSizeInBytes(first, second, mergedResults);
        mergeOverwriteDataType(first, second, mergedResults);
        mergeOverwriteDateAvailable(first, second, mergedResults);
        mergeOverwriteDateCreated(first, second, mergedResults);
        mergeOverwriteDateIssued(first, second, mergedResults);
        mergeOverwriteDateMetadataCreated(first, second, mergedResults);
        mergeOverwriteDateMetadataModified(first, second, mergedResults);
        mergeOverwriteDateModified(first, second, mergedResults);
        mergeOverwriteDateValid(first, second, mergedResults);
        mergeOverwriteGeospatialCoverage(first, second, mergedResults);
        mergeOverwriteTemporalCoverage(first, second, mergedResults);
        addCopiesOfContributorBuilders(first, mergedResults);
        addCopiesOfContributorBuilders(second, mergedResults);
        addCopiesOfCreatorBuilders(first, mergedResults);
        addCopiesOfCreatorBuilders(second, mergedResults);
        addCopiesOfDocumentationBuilders(first, mergedResults);
        addCopiesOfDocumentationBuilders(second, mergedResults);
        addCopiesOfKeyphraseBuilders(first, mergedResults);
        addCopiesOfKeyphraseBuilders(second, mergedResults);
        addCopiesOfProjectNameBuilders(first, mergedResults);
        addCopiesOfProjectNameBuilders(second, mergedResults);
        addCopiesOfPublisherBuilders(first, mergedResults);
        addCopiesOfPublisherBuilders(second, mergedResults);
        addCopiesOfVariableGroupBuilders(first, mergedResults);
        addCopiesOfVariableGroupBuilders(second, mergedResults);
    }
    
    private static void addCopiesOfDocumentationBuilders(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder result) {
        for (final ThreddsMetadataBuilder.DocumentationBuilder curDoc : source.getDocumentationBuilders()) {
            if (curDoc.isContainedContent()) {
                result.addDocumentation(curDoc.getDocType(), curDoc.getContent());
            }
            else {
                result.addDocumentation(curDoc.getDocType(), curDoc.getTitle(), curDoc.getExternalReference());
            }
        }
    }
    
    private static void addCopiesOfCreatorBuilders(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder result) {
        for (final ThreddsMetadataBuilder.ContributorBuilder curSourceCreator : source.getCreatorBuilder()) {
            final ThreddsMetadataBuilder.ContributorBuilder curResultCreator = result.addCreator();
            copySingleContributorBuilder(curSourceCreator, curResultCreator);
        }
    }
    
    private static void addCopiesOfContributorBuilders(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder result) {
        for (final ThreddsMetadataBuilder.ContributorBuilder curSourceContrib : source.getContributorBuilder()) {
            final ThreddsMetadataBuilder.ContributorBuilder curResultContrib = result.addContributor();
            copySingleContributorBuilder(curSourceContrib, curResultContrib);
        }
    }
    
    private static void addCopiesOfPublisherBuilders(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder result) {
        for (final ThreddsMetadataBuilder.ContributorBuilder curSourcePublisher : source.getPublisherBuilder()) {
            final ThreddsMetadataBuilder.ContributorBuilder curResultPublisher = result.addPublisher();
            copySingleContributorBuilder(curSourcePublisher, curResultPublisher);
        }
    }
    
    private static void addCopiesOfKeyphraseBuilders(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder result) {
        for (final ThreddsMetadataBuilder.KeyphraseBuilder curKeyphrase : source.getKeyphraseBuilders()) {
            result.addKeyphrase(curKeyphrase.getAuthority(), curKeyphrase.getPhrase());
        }
    }
    
    private static void addCopiesOfProjectNameBuilders(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder result) {
        for (final ThreddsMetadataBuilder.ProjectNameBuilder curProjName : source.getProjectNameBuilders()) {
            result.addProjectName(curProjName.getNamingAuthority(), curProjName.getName());
        }
    }
    
    private static void addCopiesOfVariableGroupBuilders(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder result) {
        for (final ThreddsMetadataBuilder.VariableGroupBuilder curSourceVarGroupBuilder : source.getVariableGroupBuilders()) {
            final ThreddsMetadataBuilder.VariableGroupBuilder curResultVarGroupBuilder = result.addVariableGroupBuilder();
            if (curSourceVarGroupBuilder.getVocabularyAuthorityId() != null) {
                curResultVarGroupBuilder.setVocabularyAuthorityId(curSourceVarGroupBuilder.getVocabularyAuthorityId());
            }
            if (curSourceVarGroupBuilder.getVocabularyAuthorityUrl() != null) {
                curResultVarGroupBuilder.setVocabularyAuthorityUrl(curSourceVarGroupBuilder.getVocabularyAuthorityUrl());
            }
            if (curSourceVarGroupBuilder.getVariableMapUrl() != null) {
                curResultVarGroupBuilder.setVariableMapUrl(curSourceVarGroupBuilder.getVariableMapUrl());
            }
            if (curSourceVarGroupBuilder.getVariableBuilders() != null) {
                for (final ThreddsMetadataBuilder.VariableBuilder curSourceVarBuilder : curSourceVarGroupBuilder.getVariableBuilders()) {
                    curResultVarGroupBuilder.addVariableBuilder(curSourceVarBuilder.getName(), curSourceVarBuilder.getDescription(), curSourceVarBuilder.getUnits(), curSourceVarBuilder.getVocabularyId(), curSourceVarBuilder.getVocabularyName());
                }
            }
        }
    }
    
    private static void mergeOverwriteGeospatialCoverage(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        if (!setGeospatialCoverateIfNotNull(second, mergedThreddsMetadata)) {
            setGeospatialCoverateIfNotNull(first, mergedThreddsMetadata);
        }
    }
    
    private static void mergeOverwriteTemporalCoverage(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DateRangeBuilder temporalCov = (second.getTemporalCoverageBuilder() != null) ? second.getTemporalCoverageBuilder() : first.getTemporalCoverageBuilder();
        if (temporalCov != null) {
            mergedThreddsMetadata.setTemporalCoverageBuilder(temporalCov.getStartDate(), temporalCov.getStartDateFormat(), temporalCov.getEndDate(), temporalCov.getEndDateFormat(), temporalCov.getDuration(), temporalCov.getResolution());
        }
    }
    
    private static void mergeOverwriteDateValid(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DatePointBuilder date = (second.getValidDatePointBuilder() != null) ? second.getValidDatePointBuilder() : first.getValidDatePointBuilder();
        if (date != null) {
            mergedThreddsMetadata.setValidDatePointBuilder(date.getDate(), date.getDateFormat());
        }
    }
    
    private static void mergeOverwriteDateModified(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DatePointBuilder date = (second.getModifiedDatePointBuilder() != null) ? second.getModifiedDatePointBuilder() : first.getModifiedDatePointBuilder();
        if (date != null) {
            mergedThreddsMetadata.setModifiedDatePointBuilder(date.getDate(), date.getDateFormat());
        }
    }
    
    private static void mergeOverwriteDateMetadataModified(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DatePointBuilder date = (second.getMetadataModifiedDatePointBuilder() != null) ? second.getMetadataModifiedDatePointBuilder() : first.getMetadataModifiedDatePointBuilder();
        if (date != null) {
            mergedThreddsMetadata.setMetadataModifiedDatePointBuilder(date.getDate(), date.getDateFormat());
        }
    }
    
    private static void mergeOverwriteDateMetadataCreated(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DatePointBuilder date = (second.getMetadataCreatedDatePointBuilder() != null) ? second.getMetadataCreatedDatePointBuilder() : first.getMetadataCreatedDatePointBuilder();
        if (date != null) {
            mergedThreddsMetadata.setMetadataCreatedDatePointBuilder(date.getDate(), date.getDateFormat());
        }
    }
    
    private static void mergeOverwriteDateIssued(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DatePointBuilder date = (second.getIssuedDatePointBuilder() != null) ? second.getIssuedDatePointBuilder() : first.getIssuedDatePointBuilder();
        if (date != null) {
            mergedThreddsMetadata.setIssuedDatePointBuilder(date.getDate(), date.getDateFormat());
        }
    }
    
    private static void mergeOverwriteDateCreated(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DatePointBuilder date = (second.getCreatedDatePointBuilder() != null) ? second.getCreatedDatePointBuilder() : first.getCreatedDatePointBuilder();
        if (date != null) {
            mergedThreddsMetadata.setCreatedDatePointBuilder(date.getDate(), date.getDateFormat());
        }
    }
    
    private static void mergeOverwriteDateAvailable(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final ThreddsMetadataBuilder.DatePointBuilder date = (second.getAvailableDatePointBuilder() != null) ? second.getAvailableDatePointBuilder() : first.getAvailableDatePointBuilder();
        if (date != null) {
            mergedThreddsMetadata.setAvailableDatePointBuilder(date.getDate(), date.getDateFormat());
        }
    }
    
    private static void mergeOverwriteDataType(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final FeatureType dataType = (second.getDataType() != null) ? second.getDataType() : first.getDataType();
        if (dataType != null) {
            mergedThreddsMetadata.setDataType(dataType);
        }
    }
    
    private static void mergeOverwriteDataSizeInBytes(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final long dataSizeInBytes = (second.getDataSizeInBytes() != -1L) ? second.getDataSizeInBytes() : first.getDataSizeInBytes();
        mergedThreddsMetadata.setDataSizeInBytes(dataSizeInBytes);
    }
    
    private static void mergeOverwriteDataFormat(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final DataFormatType dataFormat = (second.getDataFormat() != null) ? second.getDataFormat() : first.getDataFormat();
        if (dataFormat != null) {
            mergedThreddsMetadata.setDataFormat(dataFormat);
        }
    }
    
    private static void mergeOverwriteCollectionType(final ThreddsMetadataBuilder first, final ThreddsMetadataBuilder second, final ThreddsMetadataBuilder mergedThreddsMetadata) {
        final String collectionType = (second.getCollectionType() != null) ? second.getCollectionType() : first.getCollectionType();
        if (collectionType != null) {
            mergedThreddsMetadata.setCollectionType(collectionType);
        }
    }
    
    private static void copySingleContributorBuilder(final ThreddsMetadataBuilder.ContributorBuilder source, final ThreddsMetadataBuilder.ContributorBuilder recipient) {
        if (source.getNamingAuthority() != null) {
            recipient.setNamingAuthority(source.getNamingAuthority());
        }
        if (source.getName() != null) {
            recipient.setName(source.getName());
        }
        if (source.getEmail() != null) {
            recipient.setEmail(source.getEmail());
        }
        if (source.getWebPage() != null) {
            recipient.setWebPage(source.getWebPage());
        }
    }
    
    private static boolean setGeospatialCoverateIfNotNull(final ThreddsMetadataBuilder source, final ThreddsMetadataBuilder recipient) {
        final ThreddsMetadataBuilder.GeospatialCoverageBuilder geoCovBuilder = source.getGeospatialCoverageBuilder();
        if (geoCovBuilder != null && geoCovBuilder.getCRS() != null) {
            recipient.setNewGeospatialCoverageBuilder(geoCovBuilder.getCRS());
            return true;
        }
        return false;
    }
}
