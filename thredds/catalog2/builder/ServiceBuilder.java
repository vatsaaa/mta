// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.Service;
import java.util.List;
import java.net.URI;
import thredds.catalog.ServiceType;

public interface ServiceBuilder extends ThreddsBuilder
{
    String getName();
    
    String getDescription();
    
    void setDescription(final String p0);
    
    ServiceType getType();
    
    void setType(final ServiceType p0);
    
    URI getBaseUri();
    
    void setBaseUri(final URI p0);
    
    String getSuffix();
    
    void setSuffix(final String p0);
    
    void addProperty(final String p0, final String p1);
    
    boolean removeProperty(final String p0);
    
    List<String> getPropertyNames();
    
    String getPropertyValue(final String p0);
    
    ServiceBuilder addService(final String p0, final ServiceType p1, final URI p2);
    
    boolean removeService(final ServiceBuilder p0);
    
    List<ServiceBuilder> getServiceBuilders();
    
    ServiceBuilder getServiceBuilderByName(final String p0);
    
    ServiceBuilder findServiceBuilderByNameGlobally(final String p0);
    
    Service build() throws BuilderException;
}
