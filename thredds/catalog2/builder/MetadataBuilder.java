// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.Metadata;
import java.net.URI;

public interface MetadataBuilder extends ThreddsBuilder
{
    void setContainedContent(final boolean p0);
    
    boolean isContainedContent();
    
    void setTitle(final String p0);
    
    String getTitle();
    
    void setExternalReference(final URI p0);
    
    URI getExternalReference();
    
    void setContent(final String p0);
    
    String getContent();
    
    Metadata build() throws BuilderException;
}
