// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import java.net.URISyntaxException;
import thredds.catalog2.ThreddsMetadata;
import ucar.nc2.constants.FeatureType;
import thredds.catalog.DataFormatType;
import java.net.URI;
import java.util.List;

public interface ThreddsMetadataBuilder extends ThreddsBuilder
{
    boolean isEmpty();
    
    DocumentationBuilder addDocumentation(final String p0, final String p1, final String p2);
    
    DocumentationBuilder addDocumentation(final String p0, final String p1);
    
    boolean removeDocumentation(final DocumentationBuilder p0);
    
    List<DocumentationBuilder> getDocumentationBuilders();
    
    KeyphraseBuilder addKeyphrase(final String p0, final String p1);
    
    boolean removeKeyphrase(final KeyphraseBuilder p0);
    
    List<KeyphraseBuilder> getKeyphraseBuilders();
    
    ProjectNameBuilder addProjectName(final String p0, final String p1);
    
    boolean removeProjectName(final ProjectNameBuilder p0);
    
    List<ProjectNameBuilder> getProjectNameBuilders();
    
    ContributorBuilder addCreator();
    
    boolean removeCreator(final ContributorBuilder p0);
    
    List<ContributorBuilder> getCreatorBuilder();
    
    ContributorBuilder addContributor();
    
    boolean removeContributor(final ContributorBuilder p0);
    
    List<ContributorBuilder> getContributorBuilder();
    
    ContributorBuilder addPublisher();
    
    boolean removePublisher(final ContributorBuilder p0);
    
    List<ContributorBuilder> getPublisherBuilder();
    
    DatePointBuilder addOtherDatePointBuilder(final String p0, final String p1, final String p2);
    
    boolean removeOtherDatePointBuilder(final DatePointBuilder p0);
    
    List<DatePointBuilder> getOtherDatePointBuilders();
    
    DatePointBuilder setCreatedDatePointBuilder(final String p0, final String p1);
    
    DatePointBuilder getCreatedDatePointBuilder();
    
    DatePointBuilder setModifiedDatePointBuilder(final String p0, final String p1);
    
    DatePointBuilder getModifiedDatePointBuilder();
    
    DatePointBuilder setIssuedDatePointBuilder(final String p0, final String p1);
    
    DatePointBuilder getIssuedDatePointBuilder();
    
    DatePointBuilder setValidDatePointBuilder(final String p0, final String p1);
    
    DatePointBuilder getValidDatePointBuilder();
    
    DatePointBuilder setAvailableDatePointBuilder(final String p0, final String p1);
    
    DatePointBuilder getAvailableDatePointBuilder();
    
    DatePointBuilder setMetadataCreatedDatePointBuilder(final String p0, final String p1);
    
    DatePointBuilder getMetadataCreatedDatePointBuilder();
    
    DatePointBuilder setMetadataModifiedDatePointBuilder(final String p0, final String p1);
    
    DatePointBuilder getMetadataModifiedDatePointBuilder();
    
    GeospatialCoverageBuilder setNewGeospatialCoverageBuilder(final URI p0);
    
    void removeGeospatialCoverageBuilder();
    
    GeospatialCoverageBuilder getGeospatialCoverageBuilder();
    
    DateRangeBuilder setTemporalCoverageBuilder(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);
    
    DateRangeBuilder getTemporalCoverageBuilder();
    
    VariableGroupBuilder addVariableGroupBuilder();
    
    boolean removeVariableGroupBuilder(final VariableGroupBuilder p0);
    
    List<VariableGroupBuilder> getVariableGroupBuilders();
    
    void setDataSizeInBytes(final long p0);
    
    long getDataSizeInBytes();
    
    void setDataFormat(final DataFormatType p0);
    
    void setDataFormat(final String p0);
    
    DataFormatType getDataFormat();
    
    void setDataType(final FeatureType p0);
    
    void setDataType(final String p0);
    
    FeatureType getDataType();
    
    void setCollectionType(final String p0);
    
    String getCollectionType();
    
    ThreddsMetadata build() throws BuilderException;
    
    public interface GeospatialRangeBuilder extends ThreddsBuilder
    {
        void setHorizontal(final boolean p0);
        
        boolean isHorizontal();
        
        void setStart(final double p0);
        
        double getStart();
        
        void setSize(final double p0);
        
        double getSize();
        
        void setResolution(final double p0);
        
        double getResolution();
        
        void setUnits(final String p0);
        
        String getUnits();
        
        ThreddsMetadata.GeospatialRange build() throws BuilderException;
    }
    
    public interface GeospatialCoverageBuilder extends ThreddsBuilder
    {
        void setCRS(final URI p0);
        
        URI getCRS();
        
        void setGlobal(final boolean p0);
        
        boolean isGlobal();
        
        void setZPositiveUp(final boolean p0);
        
        boolean isZPositiveUp();
        
        GeospatialRangeBuilder addExtentBuilder();
        
        boolean removeExtentBuilder(final GeospatialRangeBuilder p0);
        
        List<GeospatialRangeBuilder> getExtentBuilders();
        
        ThreddsMetadata.GeospatialCoverage build() throws BuilderException;
    }
    
    public interface VariableBuilder extends ThreddsBuilder
    {
        String getName();
        
        void setName(final String p0);
        
        String getDescription();
        
        void setDescription(final String p0);
        
        String getUnits();
        
        void setUnits(final String p0);
        
        String getVocabularyId();
        
        void setVocabularyId(final String p0);
        
        String getVocabularyName();
        
        void setVocabularyName(final String p0);
        
        String getVocabularyAuthorityId();
        
        String getVocabularyAuthorityUrl();
        
        ThreddsMetadata.Variable build() throws BuilderException;
    }
    
    public interface VariableGroupBuilder extends ThreddsBuilder
    {
        String getVocabularyAuthorityId();
        
        void setVocabularyAuthorityId(final String p0);
        
        String getVocabularyAuthorityUrl();
        
        void setVocabularyAuthorityUrl(final String p0);
        
        List<VariableBuilder> getVariableBuilders();
        
        VariableBuilder addVariableBuilder(final String p0, final String p1, final String p2, final String p3, final String p4);
        
        String getVariableMapUrl();
        
        void setVariableMapUrl(final String p0);
        
        boolean isEmpty();
    }
    
    public interface ContributorBuilder extends ThreddsBuilder
    {
        String getName();
        
        void setName(final String p0);
        
        String getNamingAuthority();
        
        void setNamingAuthority(final String p0);
        
        String getRole();
        
        void setRole(final String p0);
        
        String getEmail();
        
        void setEmail(final String p0);
        
        String getWebPage();
        
        void setWebPage(final String p0);
        
        ThreddsMetadata.Contributor build() throws BuilderException;
    }
    
    public interface DateRangeBuilder extends ThreddsBuilder
    {
        String getStartDateFormat();
        
        String getStartDate();
        
        String getEndDateFormat();
        
        String getEndDate();
        
        String getDuration();
        
        String getResolution();
        
        ThreddsMetadata.DateRange build() throws BuilderException;
    }
    
    public interface DatePointBuilder extends ThreddsBuilder
    {
        String getDate();
        
        String getDateFormat();
        
        boolean isTyped();
        
        String getType();
        
        ThreddsMetadata.DatePoint build() throws BuilderException;
    }
    
    public interface ProjectNameBuilder extends ThreddsBuilder
    {
        String getNamingAuthority();
        
        String getName();
        
        ThreddsMetadata.ProjectName build() throws BuilderException;
    }
    
    public interface KeyphraseBuilder extends ThreddsBuilder
    {
        String getAuthority();
        
        String getPhrase();
        
        ThreddsMetadata.Keyphrase build() throws BuilderException;
    }
    
    public interface DocumentationBuilder extends ThreddsBuilder
    {
        boolean isContainedContent();
        
        String getDocType();
        
        String getContent();
        
        String getTitle();
        
        String getExternalReference();
        
        URI getExternalReferenceAsUri() throws URISyntaxException;
        
        ThreddsMetadata.Documentation build() throws BuilderException;
    }
}
