// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.Catalog;
import java.util.List;
import thredds.catalog.ServiceType;
import ucar.nc2.units.DateType;
import java.net.URI;

public interface CatalogBuilder extends ThreddsBuilder
{
    String getName();
    
    void setName(final String p0);
    
    URI getDocBaseUri();
    
    void setDocBaseUri(final URI p0);
    
    String getVersion();
    
    void setVersion(final String p0);
    
    DateType getExpires();
    
    void setExpires(final DateType p0);
    
    DateType getLastModified();
    
    void setLastModified(final DateType p0);
    
    ServiceBuilder addService(final String p0, final ServiceType p1, final URI p2);
    
    boolean removeService(final ServiceBuilder p0);
    
    List<ServiceBuilder> getServiceBuilders();
    
    ServiceBuilder getServiceBuilderByName(final String p0);
    
    ServiceBuilder findServiceBuilderByNameGlobally(final String p0);
    
    DatasetBuilder addDataset(final String p0);
    
    CatalogRefBuilder addCatalogRef(final String p0, final URI p1);
    
    boolean removeDataset(final DatasetNodeBuilder p0);
    
    List<DatasetNodeBuilder> getDatasetNodeBuilders();
    
    DatasetNodeBuilder getDatasetNodeBuilderById(final String p0);
    
    DatasetNodeBuilder findDatasetNodeBuilderByIdGlobally(final String p0);
    
    void addProperty(final String p0, final String p1);
    
    boolean removeProperty(final String p0);
    
    List<String> getPropertyNames();
    
    String getPropertyValue(final String p0);
    
    Catalog build() throws BuilderException;
}
