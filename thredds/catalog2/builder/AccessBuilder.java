// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.Access;
import thredds.catalog.DataFormatType;

public interface AccessBuilder extends ThreddsBuilder
{
    ServiceBuilder getServiceBuilder();
    
    void setServiceBuilder(final ServiceBuilder p0);
    
    String getUrlPath();
    
    void setUrlPath(final String p0);
    
    DataFormatType getDataFormat();
    
    void setDataFormat(final DataFormatType p0);
    
    long getDataSize();
    
    void setDataSize(final long p0);
    
    Access build() throws BuilderException;
}
