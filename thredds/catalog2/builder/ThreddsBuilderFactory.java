// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.CatalogRef;
import thredds.catalog2.Dataset;
import thredds.catalog2.Service;
import thredds.catalog.ServiceType;
import thredds.catalog2.Catalog;
import ucar.nc2.units.DateType;
import java.net.URI;

public interface ThreddsBuilderFactory
{
    CatalogBuilder newCatalogBuilder(final String p0, final URI p1, final String p2, final DateType p3, final DateType p4);
    
    CatalogBuilder newCatalogBuilder(final Catalog p0);
    
    ServiceBuilder newServiceBuilder(final String p0, final ServiceType p1, final URI p2);
    
    ServiceBuilder newServiceBuilder(final Service p0);
    
    DatasetBuilder newDatasetBuilder(final String p0);
    
    DatasetBuilder newDatasetBuilder(final Dataset p0);
    
    CatalogRefBuilder newCatalogRefBuilder(final String p0, final URI p1);
    
    CatalogRefBuilder newCatalogRefBuilder(final CatalogRef p0);
    
    MetadataBuilder newMetadataBuilder();
    
    ThreddsMetadataBuilder newThreddsMetadataBuilder();
}
