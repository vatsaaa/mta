// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.CatalogRef;
import java.net.URI;

public interface CatalogRefBuilder extends DatasetNodeBuilder
{
    URI getReference();
    
    void setReference(final URI p0);
    
    CatalogRef build() throws BuilderException;
}
