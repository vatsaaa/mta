// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

public class BuilderIssue
{
    private final Severity severity;
    private final String message;
    private final ThreddsBuilder builder;
    private final Exception cause;
    
    public BuilderIssue(final Severity severity, final String message, final ThreddsBuilder builder, final Exception cause) {
        if (severity == null || message == null || builder == null) {
            throw new IllegalArgumentException("Null severity level, message, and/or builder.");
        }
        this.severity = severity;
        this.message = message;
        this.builder = builder;
        this.cause = cause;
    }
    
    public Severity getSeverity() {
        return this.severity;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public ThreddsBuilder getBuilder() {
        return this.builder;
    }
    
    public Exception getCause() {
        return this.cause;
    }
    
    public enum Severity
    {
        FATAL, 
        ERROR, 
        WARNING;
    }
}
