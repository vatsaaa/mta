// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

public interface ThreddsBuilder
{
    boolean isBuilt();
    
    BuilderIssues getIssues();
    
    Object build() throws BuilderException;
}
