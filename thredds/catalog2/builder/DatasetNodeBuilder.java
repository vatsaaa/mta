// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import thredds.catalog2.DatasetNode;
import java.net.URI;
import java.util.List;

public interface DatasetNodeBuilder extends ThreddsBuilder
{
    String getId();
    
    void setId(final String p0);
    
    String getIdAuthority();
    
    void setIdAuthority(final String p0);
    
    String getName();
    
    void setName(final String p0);
    
    void addProperty(final String p0, final String p1);
    
    boolean removeProperty(final String p0);
    
    List<String> getPropertyNames();
    
    String getPropertyValue(final String p0);
    
    ThreddsMetadataBuilder setNewThreddsMetadataBuilder();
    
    boolean removeThreddsMetadataBuilder();
    
    ThreddsMetadataBuilder getThreddsMetadataBuilder();
    
    MetadataBuilder addMetadata();
    
    boolean removeMetadata(final MetadataBuilder p0);
    
    List<MetadataBuilder> getMetadataBuilders();
    
    CatalogBuilder getParentCatalogBuilder();
    
    DatasetNodeBuilder getParentDatasetBuilder();
    
    boolean isCollection();
    
    DatasetBuilder addDataset(final String p0);
    
    CatalogRefBuilder addCatalogRef(final String p0, final URI p1);
    
    boolean removeDatasetNode(final DatasetNodeBuilder p0);
    
    List<DatasetNodeBuilder> getDatasetNodeBuilders();
    
    DatasetNodeBuilder getDatasetNodeBuilderById(final String p0);
    
    DatasetNodeBuilder findDatasetNodeBuilderByIdGlobally(final String p0);
    
    boolean isDatasetIdInUseGlobally(final String p0);
    
    DatasetNode build() throws BuilderException;
}
