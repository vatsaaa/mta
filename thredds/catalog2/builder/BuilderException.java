// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

public class BuilderException extends Exception
{
    private final BuilderIssues issues;
    
    public BuilderException(final BuilderIssue issue) {
        if (issue == null) {
            throw new IllegalArgumentException("Issue may not be null.");
        }
        this.issues = new BuilderIssues(issue);
    }
    
    public BuilderException(final BuilderIssues issues) {
        if (issues == null) {
            throw new IllegalArgumentException("Issues may not be null.");
        }
        this.issues = issues;
    }
    
    public BuilderException(final BuilderIssue issue, final Throwable cause) {
        super(cause);
        this.issues = new BuilderIssues(issue);
    }
    
    public BuilderException(final BuilderIssues issues, final Throwable cause) {
        super(cause);
        this.issues = issues;
    }
    
    public BuilderIssues getIssues() {
        return this.issues;
    }
    
    @Override
    public String getMessage() {
        return this.issues.toString();
    }
}
