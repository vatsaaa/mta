// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.builder;

import java.util.Collections;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class BuilderIssues
{
    private final List<BuilderIssue> issues;
    private int numFatalIssues;
    private int numErrorIssues;
    private int numWarningIssues;
    
    public BuilderIssues() {
        this.numFatalIssues = 0;
        this.numErrorIssues = 0;
        this.numWarningIssues = 0;
        this.issues = new ArrayList<BuilderIssue>();
    }
    
    public BuilderIssues(final BuilderIssue issue) {
        this();
        if (issue == null) {
            throw new IllegalArgumentException("Issue may not be null.");
        }
        this.issues.add(issue);
    }
    
    public BuilderIssues(final BuilderIssue.Severity severity, final String message, final ThreddsBuilder builder, final Exception cause) {
        this();
        this.issues.add(new BuilderIssue(severity, message, builder, cause));
        this.trackSeverity(severity);
    }
    
    public void addIssue(final BuilderIssue.Severity severity, final String message, final ThreddsBuilder builder, final Exception cause) {
        this.issues.add(new BuilderIssue(severity, message, builder, cause));
        this.trackSeverity(severity);
    }
    
    public void addIssue(final BuilderIssue issue) {
        if (issue == null) {
            return;
        }
        this.issues.add(issue);
        this.trackSeverity(issue.getSeverity());
    }
    
    public void addAllIssues(final BuilderIssues issues) {
        if (issues == null) {
            return;
        }
        if (issues.isEmpty()) {
            return;
        }
        this.issues.addAll(issues.getIssues());
        for (final BuilderIssue curIssue : issues.getIssues()) {
            this.trackSeverity(curIssue.getSeverity());
        }
    }
    
    public boolean isEmpty() {
        return this.issues.isEmpty();
    }
    
    public int size() {
        return this.issues.size();
    }
    
    public List<BuilderIssue> getIssues() {
        if (this.issues.isEmpty()) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends BuilderIssue>)this.issues);
    }
    
    public boolean isValid() {
        return this.numFatalIssues <= 0 && this.numErrorIssues <= 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (final BuilderIssue bfi : this.issues) {
            sb.append(bfi.getMessage()).append("\n");
        }
        return sb.toString();
    }
    
    private void trackSeverity(final BuilderIssue.Severity severity) {
        if (severity.equals(BuilderIssue.Severity.FATAL)) {
            ++this.numFatalIssues;
        }
        else if (severity.equals(BuilderIssue.Severity.ERROR)) {
            ++this.numErrorIssues;
        }
        else if (severity.equals(BuilderIssue.Severity.WARNING)) {
            ++this.numWarningIssues;
        }
    }
}
