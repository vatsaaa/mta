// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import thredds.catalog.DataFormatType;

public interface Access
{
    Service getService();
    
    String getUrlPath();
    
    DataFormatType getDataFormat();
    
    long getDataSize();
}
