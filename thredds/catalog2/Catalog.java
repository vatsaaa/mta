// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.util.List;
import ucar.nc2.units.DateType;
import java.net.URI;

public interface Catalog
{
    String getName();
    
    URI getDocBaseUri();
    
    String getVersion();
    
    DateType getExpires();
    
    DateType getLastModified();
    
    List<Service> getServices();
    
    Service getServiceByName(final String p0);
    
    Service findServiceByNameGlobally(final String p0);
    
    List<DatasetNode> getDatasets();
    
    DatasetNode getDatasetById(final String p0);
    
    DatasetNode findDatasetByIdGlobally(final String p0);
    
    List<Property> getProperties();
    
    Property getPropertyByName(final String p0);
}
