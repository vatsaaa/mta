// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.BuilderIssues;
import thredds.catalog2.DatasetNode;
import thredds.catalog2.builder.DatasetNodeBuilder;
import thredds.catalog2.builder.CatalogRefBuilder;
import thredds.catalog2.builder.DatasetBuilder;
import thredds.catalog2.Property;
import thredds.catalog2.Service;
import java.util.List;
import thredds.catalog2.builder.ServiceBuilder;
import thredds.catalog.ServiceType;
import ucar.nc2.units.DateType;
import java.net.URI;
import thredds.catalog2.builder.CatalogBuilder;
import thredds.catalog2.Catalog;

class CatalogImpl implements Catalog, CatalogBuilder
{
    private String name;
    private URI docBaseUri;
    private String version;
    private DateType expires;
    private DateType lastModified;
    private final ServiceContainer serviceContainer;
    private final GlobalServiceContainer globalServiceContainer;
    private final DatasetNodeContainer datasetContainer;
    private final PropertyContainer propertyContainer;
    private boolean isBuilt;
    
    CatalogImpl(final String name, final URI docBaseUri, final String version, final DateType expires, final DateType lastModified) {
        this.isBuilt = false;
        if (docBaseUri == null) {
            throw new IllegalArgumentException("Catalog base URI must not be null.");
        }
        this.name = name;
        this.docBaseUri = docBaseUri;
        this.version = version;
        this.expires = expires;
        this.lastModified = lastModified;
        this.globalServiceContainer = new GlobalServiceContainer();
        this.serviceContainer = new ServiceContainer(this.globalServiceContainer);
        this.datasetContainer = new DatasetNodeContainer(null);
        this.propertyContainer = new PropertyContainer();
    }
    
    DatasetNodeContainer getDatasetNodeContainer() {
        return this.datasetContainer;
    }
    
    public void setName(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setDocBaseUri(final URI docBaseUri) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        if (docBaseUri == null) {
            throw new IllegalArgumentException("Catalog base URI must not be null.");
        }
        this.docBaseUri = docBaseUri;
    }
    
    public URI getDocBaseUri() {
        return this.docBaseUri;
    }
    
    public void setVersion(final String version) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        this.version = version;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    public void setExpires(final DateType expires) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        this.expires = expires;
    }
    
    public DateType getExpires() {
        return this.expires;
    }
    
    public void setLastModified(final DateType lastModified) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        this.lastModified = lastModified;
    }
    
    public DateType getLastModified() {
        return this.lastModified;
    }
    
    public ServiceBuilder addService(final String name, final ServiceType type, final URI baseUri) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.serviceContainer.addService(name, type, baseUri);
    }
    
    public boolean removeService(final ServiceBuilder serviceBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return serviceBuilder != null && this.serviceContainer.removeService((ServiceImpl)serviceBuilder);
    }
    
    public List<Service> getServices() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped its CatalogBuilder without build() being called.");
        }
        return this.serviceContainer.getServices();
    }
    
    public Service getServiceByName(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped its CatalogBuilder without being build()-ed.");
        }
        return this.serviceContainer.getServiceByName(name);
    }
    
    public Service findServiceByNameGlobally(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped its CatalogBuilder without being build()-ed.");
        }
        return this.globalServiceContainer.getServiceByGloballyUniqueName(name);
    }
    
    public List<ServiceBuilder> getServiceBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.serviceContainer.getServiceBuilders();
    }
    
    public ServiceBuilder getServiceBuilderByName(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.serviceContainer.getServiceBuilderByName(name);
    }
    
    public ServiceBuilder findServiceBuilderByNameGlobally(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.globalServiceContainer.getServiceByGloballyUniqueName(name);
    }
    
    public void addProperty(final String name, final String value) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        this.propertyContainer.addProperty(name, value);
    }
    
    public boolean removeProperty(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.propertyContainer.removeProperty(name);
    }
    
    public List<String> getPropertyNames() {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.propertyContainer.getPropertyNames();
    }
    
    public String getPropertyValue(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.propertyContainer.getPropertyValue(name);
    }
    
    public List<Property> getProperties() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped from its CatalogBuilder before build() was called.");
        }
        return this.propertyContainer.getProperties();
    }
    
    public Property getPropertyByName(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped from its CatalogBuilder before build() was called.");
        }
        return this.propertyContainer.getPropertyByName(name);
    }
    
    public DatasetBuilder addDataset(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        final DatasetImpl di = new DatasetImpl(name, this, null);
        this.datasetContainer.addDatasetNode(di);
        return di;
    }
    
    public CatalogRefBuilder addCatalogRef(final String name, final URI reference) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        final CatalogRefImpl crb = new CatalogRefImpl(name, reference, this, null);
        this.datasetContainer.addDatasetNode(crb);
        return crb;
    }
    
    public boolean removeDataset(final DatasetNodeBuilder builder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        if (builder == null) {
            throw new IllegalArgumentException("DatasetNodeBuilder may not be null.");
        }
        return this.datasetContainer.removeDatasetNode((DatasetNodeImpl)builder);
    }
    
    public List<DatasetNode> getDatasets() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped its CatalogBuilder without being build()-ed.");
        }
        return this.datasetContainer.getDatasets();
    }
    
    public DatasetNode getDatasetById(final String id) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped its CatalogBuilder without being build()-ed.");
        }
        return this.datasetContainer.getDatasetById(id);
    }
    
    public DatasetNode findDatasetByIdGlobally(final String id) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Catalog has escaped its CatalogBuilder without being build()-ed.");
        }
        return this.datasetContainer.getDatasetNodeByGloballyUniqueId(id);
    }
    
    public List<DatasetNodeBuilder> getDatasetNodeBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.datasetContainer.getDatasetNodeBuilders();
    }
    
    public DatasetNodeBuilder getDatasetNodeBuilderById(final String id) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.datasetContainer.getDatasetNodeBuilderById(id);
    }
    
    public DatasetNodeBuilder findDatasetNodeBuilderByIdGlobally(final String id) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogBuilder has been built.");
        }
        return this.datasetContainer.getDatasetNodeByGloballyUniqueId(id);
    }
    
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    public BuilderIssues getIssues() {
        final BuilderIssues issues = new BuilderIssues();
        issues.addAllIssues(this.globalServiceContainer.getIssues(this));
        issues.addAllIssues(this.serviceContainer.getIssues());
        issues.addAllIssues(this.datasetContainer.getIssues());
        issues.addAllIssues(this.propertyContainer.getIssues());
        return issues;
    }
    
    public Catalog build() throws BuilderException {
        if (this.isBuilt) {
            return this;
        }
        this.serviceContainer.build();
        this.datasetContainer.build();
        this.propertyContainer.build();
        this.isBuilt = true;
        return this;
    }
}
