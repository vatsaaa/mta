// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.ThreddsMetadataBuilder;
import thredds.catalog2.builder.MetadataBuilder;
import thredds.catalog2.CatalogRef;
import thredds.catalog2.builder.CatalogRefBuilder;
import thredds.catalog2.Dataset;
import thredds.catalog2.builder.DatasetBuilder;
import thredds.catalog2.Service;
import thredds.catalog2.builder.ServiceBuilder;
import thredds.catalog.ServiceType;
import thredds.catalog2.Catalog;
import thredds.catalog2.builder.CatalogBuilder;
import ucar.nc2.units.DateType;
import java.net.URI;
import thredds.catalog2.builder.ThreddsBuilderFactory;

public class ThreddsBuilderFactoryImpl implements ThreddsBuilderFactory
{
    public CatalogBuilder newCatalogBuilder(final String name, final URI docBaseUri, final String version, final DateType expires, final DateType lastModified) {
        return new CatalogImpl(name, docBaseUri, version, expires, lastModified);
    }
    
    public CatalogBuilder newCatalogBuilder(final Catalog catalog) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }
    
    public ServiceBuilder newServiceBuilder(final String name, final ServiceType type, final URI baseUri) {
        return new ServiceImpl(name, type, baseUri, null);
    }
    
    public ServiceBuilder newServiceBuilder(final Service service) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }
    
    public DatasetBuilder newDatasetBuilder(final String name) {
        return new DatasetImpl(name, null, null);
    }
    
    public DatasetBuilder newDatasetBuilder(final Dataset dataset) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }
    
    public CatalogRefBuilder newCatalogRefBuilder(final String name, final URI reference) {
        return new CatalogRefImpl(name, reference, null, null);
    }
    
    public CatalogRefBuilder newCatalogRefBuilder(final CatalogRef catRef) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }
    
    public MetadataBuilder newMetadataBuilder() {
        return new MetadataImpl();
    }
    
    public ThreddsMetadataBuilder newThreddsMetadataBuilder() {
        return new ThreddsMetadataImpl();
    }
}
