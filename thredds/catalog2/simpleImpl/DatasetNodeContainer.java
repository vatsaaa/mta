// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderException;
import java.util.Iterator;
import thredds.catalog2.builder.BuilderIssues;
import thredds.catalog2.builder.DatasetNodeBuilder;
import java.util.Collection;
import java.util.Collections;
import thredds.catalog2.DatasetNode;
import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.LoggerFactory;
import java.util.Map;
import java.util.List;
import org.slf4j.Logger;

class DatasetNodeContainer
{
    private Logger log;
    private List<DatasetNodeImpl> datasetNodeImplList;
    private List<String> localIdList;
    private final DatasetNodeContainer rootContainer;
    private Map<String, DatasetNodeImpl> datasetNodeImplMapByGloballyUniqueId;
    private boolean isBuilt;
    
    DatasetNodeContainer(final DatasetNodeContainer rootContainer) {
        this.log = LoggerFactory.getLogger(this.getClass());
        this.isBuilt = false;
        this.datasetNodeImplList = null;
        this.rootContainer = rootContainer;
    }
    
    DatasetNodeContainer getRootContainer() {
        if (this.rootContainer != null) {
            return this.rootContainer;
        }
        return this;
    }
    
    boolean isDatasetNodeIdInUseGlobally(final String id) {
        return this.getDatasetNodeByGloballyUniqueId(id) != null;
    }
    
    boolean addDatasetNodeByGloballyUniqueId(final DatasetNodeImpl datasetNode) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        if (datasetNode == null) {
            return false;
        }
        if (datasetNode.getId() == null) {
            return false;
        }
        if (this.rootContainer != null) {
            return this.rootContainer.addDatasetNodeByGloballyUniqueId(datasetNode);
        }
        if (this.datasetNodeImplMapByGloballyUniqueId == null) {
            this.datasetNodeImplMapByGloballyUniqueId = new HashMap<String, DatasetNodeImpl>();
        }
        if (this.datasetNodeImplMapByGloballyUniqueId.containsKey(datasetNode.getId())) {
            return false;
        }
        final DatasetNodeImpl replacedDatasetNode = this.datasetNodeImplMapByGloballyUniqueId.put(datasetNode.getId(), datasetNode);
        if (replacedDatasetNode == null) {
            return true;
        }
        final String msg = "DatasetNodeContainer in bad state [MapByGloballyUniqueId: containsKey(" + datasetNode.getId() + ")==false then put()!=null].";
        this.log.error("addDatasetNodeByGloballyUniqueId(): " + msg);
        throw new IllegalStateException(msg);
    }
    
    boolean removeDatasetNodeByGloballyUniqueId(final String id) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        if (id == null) {
            return false;
        }
        if (this.rootContainer != null) {
            return this.rootContainer.removeDatasetNodeByGloballyUniqueId(id);
        }
        if (this.datasetNodeImplMapByGloballyUniqueId == null) {
            return false;
        }
        final DatasetNodeImpl removedDatasetNode = this.datasetNodeImplMapByGloballyUniqueId.remove(id);
        return removedDatasetNode != null;
    }
    
    boolean addDatasetNodeToLocalById(final DatasetNodeImpl datasetNode) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        if (datasetNode == null) {
            return false;
        }
        if (datasetNode.getId() == null) {
            return false;
        }
        if (this.localIdList == null) {
            this.localIdList = new ArrayList<String>();
        }
        return this.localIdList.add(datasetNode.getId());
    }
    
    boolean removeDatasetNodeFromLocalById(final String id) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        return id != null && this.localIdList != null && this.localIdList.remove(id);
    }
    
    DatasetNodeImpl getDatasetNodeByGloballyUniqueId(final String id) {
        if (id == null) {
            return null;
        }
        if (this.rootContainer != null) {
            return this.rootContainer.getDatasetNodeByGloballyUniqueId(id);
        }
        if (this.datasetNodeImplMapByGloballyUniqueId == null) {
            return null;
        }
        return this.datasetNodeImplMapByGloballyUniqueId.get(id);
    }
    
    boolean isEmpty() {
        return this.datasetNodeImplList == null || this.datasetNodeImplList.isEmpty();
    }
    
    int size() {
        if (this.datasetNodeImplList == null) {
            return 0;
        }
        return this.datasetNodeImplList.size();
    }
    
    void addDatasetNode(final DatasetNodeImpl datasetNode) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        if (datasetNode.getId() != null) {
            if (!this.addDatasetNodeByGloballyUniqueId(datasetNode)) {
                throw new IllegalStateException("Globally unique DatasetNode ID is already being used.");
            }
            if (this.localIdList == null) {
                this.localIdList = new ArrayList<String>();
            }
            this.localIdList.add(datasetNode.getId());
        }
        if (this.datasetNodeImplList == null) {
            this.datasetNodeImplList = new ArrayList<DatasetNodeImpl>();
        }
        if (!this.datasetNodeImplList.add(datasetNode)) {
            this.log.error("addDatasetNode(): failed to add datasetNode name [" + datasetNode.getName() + "].");
        }
    }
    
    boolean removeDatasetNode(final DatasetNodeImpl datasetNode) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        if (datasetNode == null) {
            return false;
        }
        if (this.datasetNodeImplList == null) {
            return false;
        }
        if (!this.datasetNodeImplList.remove(datasetNode)) {
            return false;
        }
        final String id = datasetNode.getId();
        if (id != null && this.localIdList != null && this.localIdList.remove(id) && !this.removeDatasetNodeByGloballyUniqueId(id)) {
            final String msg = "Removal from DatasetNode by global ID inconsistent with DatasetNode removal [" + datasetNode.getName() + "].";
            this.log.error("removeDatasetNode(): " + msg);
            throw new IllegalStateException(msg);
        }
        return true;
    }
    
    List<DatasetNode> getDatasets() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeCollection has escaped its Builder before being built.");
        }
        if (this.datasetNodeImplList == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends DatasetNode>)new ArrayList<DatasetNode>(this.datasetNodeImplList));
    }
    
    DatasetNode getDatasetById(final String id) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeCollection has escaped its Builder before being built.");
        }
        if (id == null) {
            return null;
        }
        if (this.datasetNodeImplList == null) {
            return null;
        }
        if (this.localIdList != null && this.localIdList.contains(id)) {
            return this.getDatasetNodeByGloballyUniqueId(id);
        }
        return null;
    }
    
    List<DatasetNodeBuilder> getDatasetNodeBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        if (this.datasetNodeImplList == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends DatasetNodeBuilder>)new ArrayList<DatasetNodeBuilder>(this.datasetNodeImplList));
    }
    
    DatasetNodeBuilder getDatasetNodeBuilderById(final String id) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeContainer has been built.");
        }
        if (id == null) {
            return null;
        }
        if (this.datasetNodeImplList == null) {
            return null;
        }
        if (this.localIdList != null && this.localIdList.contains(id)) {
            return this.getDatasetNodeByGloballyUniqueId(id);
        }
        return null;
    }
    
    BuilderIssues getIssues() {
        final BuilderIssues issues = new BuilderIssues();
        if (this.datasetNodeImplList != null) {
            for (final DatasetNodeBuilder dnb : this.datasetNodeImplList) {
                issues.addAllIssues(dnb.getIssues());
            }
        }
        return issues;
    }
    
    void build() throws BuilderException {
        if (this.isBuilt) {
            return;
        }
        if (this.datasetNodeImplList != null) {
            for (final DatasetNodeBuilder dnb : this.datasetNodeImplList) {
                dnb.build();
            }
        }
        this.isBuilt = true;
    }
}
