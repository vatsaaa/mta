// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.DatasetNode;
import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.builder.BuilderIssues;
import java.net.URI;
import thredds.catalog2.builder.CatalogRefBuilder;
import thredds.catalog2.CatalogRef;

class CatalogRefImpl extends DatasetNodeImpl implements CatalogRef, CatalogRefBuilder
{
    private URI reference;
    private boolean isBuilt;
    
    CatalogRefImpl(final String name, final URI reference, final CatalogImpl parentCatalog, final DatasetNodeImpl parent) {
        super(name, parentCatalog, parent);
        this.isBuilt = false;
        if (reference == null) {
            throw new IllegalArgumentException("CatalogRef reference URI must not be null.");
        }
        this.reference = reference;
    }
    
    public void setReference(final URI reference) {
        if (this.isBuilt) {
            throw new IllegalStateException("This CatalogRefBuilder has been built.");
        }
        if (reference == null) {
            throw new IllegalArgumentException("CatalogRef reference URI must not be null.");
        }
        this.reference = reference;
    }
    
    public URI getReference() {
        return this.reference;
    }
    
    @Override
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    @Override
    public BuilderIssues getIssues() {
        final BuilderIssues issues = super.getIssues();
        return issues;
    }
    
    @Override
    public CatalogRef build() throws BuilderException {
        if (this.isBuilt) {
            return this;
        }
        super.build();
        this.isBuilt = true;
        return this;
    }
}
