// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderIssues;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.LinkedHashMap;
import org.slf4j.LoggerFactory;
import thredds.catalog2.Property;
import java.util.Map;
import org.slf4j.Logger;

class PropertyContainer
{
    private Logger log;
    private Map<String, Property> propertiesMap;
    private boolean isBuilt;
    
    PropertyContainer() {
        this.log = LoggerFactory.getLogger(this.getClass());
        this.isBuilt = false;
        this.propertiesMap = null;
    }
    
    boolean isEmpty() {
        return this.propertiesMap == null || this.propertiesMap.isEmpty();
    }
    
    int size() {
        if (this.propertiesMap == null) {
            return 0;
        }
        return this.propertiesMap.size();
    }
    
    void addProperty(final String name, final String value) {
        if (this.isBuilt) {
            throw new IllegalStateException("This PropertyContainer has been built.");
        }
        if (this.propertiesMap == null) {
            this.propertiesMap = new LinkedHashMap<String, Property>();
        }
        final PropertyImpl property = new PropertyImpl(name, value);
        if (null != this.propertiesMap.put(name, property) && this.log.isDebugEnabled()) {
            this.log.debug("addProperty(): reseting property [" + name + "].");
        }
    }
    
    boolean removeProperty(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This PropertyContainer has been built.");
        }
        if (name == null) {
            throw new IllegalArgumentException("Given name may not be null.");
        }
        if (this.propertiesMap == null) {
            return false;
        }
        final Property property = this.propertiesMap.remove(name);
        return property != null;
    }
    
    List<String> getPropertyNames() {
        if (this.propertiesMap == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends String>)new ArrayList<String>(this.propertiesMap.keySet()));
    }
    
    boolean containsPropertyName(final String name) {
        return name != null && this.propertiesMap != null && this.propertiesMap.get(name) != null;
    }
    
    String getPropertyValue(final String name) {
        if (name == null) {
            return null;
        }
        if (this.propertiesMap == null) {
            return null;
        }
        final Property property = this.propertiesMap.get(name);
        if (property == null) {
            return null;
        }
        return property.getValue();
    }
    
    List<Property> getProperties() {
        if (this.propertiesMap == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Property>)new ArrayList<Property>(this.propertiesMap.values()));
    }
    
    Property getPropertyByName(final String name) {
        if (name == null) {
            return null;
        }
        if (this.propertiesMap == null) {
            return null;
        }
        return this.propertiesMap.get(name);
    }
    
    boolean isBuilt() {
        return this.isBuilt;
    }
    
    BuilderIssues getIssues() {
        return null;
    }
    
    void build() {
        this.isBuilt = true;
    }
}
