// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.BuilderIssue;
import thredds.catalog2.builder.BuilderIssues;
import java.net.URI;
import thredds.catalog2.builder.MetadataBuilder;
import thredds.catalog2.Metadata;

class MetadataImpl implements Metadata, MetadataBuilder
{
    private boolean isContainedContent;
    private String title;
    private URI externalReference;
    private String content;
    private boolean isBuilt;
    
    MetadataImpl() {
        this.isContainedContent = true;
        this.isBuilt = false;
    }
    
    MetadataImpl(final boolean isContainedContent) {
        this.isContainedContent = isContainedContent;
        this.isBuilt = false;
    }
    
    MetadataImpl(final String title, final URI externalReference) {
        if (title == null) {
            throw new IllegalArgumentException("Title may not be null.");
        }
        if (externalReference == null) {
            throw new IllegalArgumentException("External reference URI may not be null.");
        }
        this.isContainedContent = false;
        this.title = title;
        this.externalReference = externalReference;
        this.content = null;
        this.isBuilt = false;
    }
    
    MetadataImpl(final String content) {
        if (content == null) {
            throw new IllegalArgumentException("Content string may not be null.");
        }
        this.isContainedContent = true;
        this.title = null;
        this.externalReference = null;
        this.content = content;
        this.isBuilt = false;
    }
    
    public void setContainedContent(final boolean isContainedContent) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has already been built.");
        }
        this.isContainedContent = isContainedContent;
    }
    
    public boolean isContainedContent() {
        return this.isContainedContent;
    }
    
    public void setTitle(final String title) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has already been built.");
        }
        if (this.isContainedContent) {
            throw new IllegalStateException("This MetadataBuilder contains content, cannot set title.");
        }
        if (title == null) {
            throw new IllegalArgumentException("Title may not be null.");
        }
        this.title = title;
    }
    
    public String getTitle() {
        if (this.isContainedContent) {
            throw new IllegalStateException("Metadata with contained content has no title.");
        }
        return this.title;
    }
    
    public void setExternalReference(final URI externalReference) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has already been built.");
        }
        if (this.isContainedContent) {
            throw new IllegalStateException("This MetadataBuilder contains content, cannot set external reference.");
        }
        if (externalReference == null) {
            throw new IllegalArgumentException("External reference may not be null.");
        }
        this.externalReference = externalReference;
    }
    
    public URI getExternalReference() {
        if (this.isContainedContent) {
            throw new IllegalStateException("Metadata with contained content has no external reference.");
        }
        return this.externalReference;
    }
    
    public void setContent(final String content) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has already been built.");
        }
        if (!this.isContainedContent) {
            throw new IllegalStateException("This MetadataBuilder has external reference, cannot set content.");
        }
        this.content = content;
    }
    
    public String getContent() {
        if (!this.isContainedContent) {
            throw new IllegalStateException("Metadata with external reference has no content, dereference external reference to obtain metadata content.");
        }
        return this.content;
    }
    
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    public BuilderIssues getIssues() {
        if (this.isContainedContent) {
            if (this.content == null) {
                return new BuilderIssues(BuilderIssue.Severity.WARNING, "MetadataBuilder contains null content.", this, null);
            }
        }
        else if (this.title == null || this.externalReference == null) {
            return new BuilderIssues(BuilderIssue.Severity.WARNING, "MetadataBuilder with link has null title and/or link URI.", this, null);
        }
        return new BuilderIssues();
    }
    
    public Metadata build() throws BuilderException {
        this.isBuilt = true;
        return this;
    }
}
