// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderIssue;
import thredds.catalog2.builder.BuilderIssues;
import thredds.catalog2.builder.ThreddsBuilder;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class GlobalServiceContainer
{
    private Map<String, ServiceImpl> servicesByGloballyUniqueName;
    private List<ServiceImpl> servicesWithDuplicateName;
    
    GlobalServiceContainer() {
        this.servicesByGloballyUniqueName = new HashMap<String, ServiceImpl>();
        this.servicesWithDuplicateName = new ArrayList<ServiceImpl>();
    }
    
    boolean isServiceNameInUseGlobally(final String name) {
        return this.servicesByGloballyUniqueName.containsKey(name);
    }
    
    ServiceImpl getServiceByGloballyUniqueName(final String name) {
        if (name == null) {
            return null;
        }
        return this.servicesByGloballyUniqueName.get(name);
    }
    
    void addService(final ServiceImpl service) {
        if (service == null) {
            return;
        }
        if (this.servicesByGloballyUniqueName.containsKey(service.getName())) {
            final boolean success = this.servicesWithDuplicateName.add(service);
            assert success;
        }
        else {
            final ServiceImpl replacedService = this.servicesByGloballyUniqueName.put(service.getName(), service);
            assert null == replacedService;
        }
    }
    
    boolean removeService(final ServiceImpl service) {
        if (service == null) {
            return false;
        }
        if (this.servicesWithDuplicateName.remove(service)) {
            return true;
        }
        if (!this.servicesByGloballyUniqueName.containsValue(service)) {
            return false;
        }
        final ServiceImpl removedService = this.servicesByGloballyUniqueName.remove(service.getName());
        assert service == removedService;
        final boolean promoted = this.promoteFirstServiceWithDuplicateName(service.getName());
        assert promoted;
        return true;
    }
    
    private boolean promoteFirstServiceWithDuplicateName(final String name) {
        for (final ServiceImpl service : this.servicesWithDuplicateName) {
            if (service.getName().equals(name)) {
                final boolean success = this.servicesWithDuplicateName.remove(service);
                assert success;
                final ServiceImpl replacedService = this.servicesByGloballyUniqueName.put(service.getName(), service);
                assert null == replacedService;
                return true;
            }
        }
        return false;
    }
    
    boolean isEmpty() {
        return this.servicesByGloballyUniqueName.isEmpty();
    }
    
    int numberOfServicesWithGloballyUniqueNames() {
        return this.servicesByGloballyUniqueName.size();
    }
    
    int numberOfServicesWithDuplicateNames() {
        return this.servicesWithDuplicateName.size();
    }
    
    BuilderIssues getIssues(final ThreddsBuilder responsibleBuilder) {
        final BuilderIssues issues = new BuilderIssues();
        if (!this.servicesWithDuplicateName.isEmpty()) {
            for (final ServiceImpl s : this.servicesWithDuplicateName) {
                issues.addIssue(BuilderIssue.Severity.WARNING, "Catalog contains duplicate service name [" + s.getName() + "].", responsibleBuilder, null);
            }
        }
        return issues;
    }
}
