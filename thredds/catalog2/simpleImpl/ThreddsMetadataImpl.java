// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.BuilderIssue;
import java.net.URISyntaxException;
import thredds.catalog2.builder.BuilderException;
import java.util.Iterator;
import thredds.catalog2.builder.BuilderIssues;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import ucar.nc2.constants.FeatureType;
import thredds.catalog.DataFormatType;
import java.util.List;
import thredds.catalog2.builder.ThreddsMetadataBuilder;
import thredds.catalog2.ThreddsMetadata;

class ThreddsMetadataImpl implements ThreddsMetadata, ThreddsMetadataBuilder
{
    private boolean isBuilt;
    private List<DocumentationImpl> docs;
    private List<KeyphraseImpl> keyphrases;
    private List<ProjectNameImpl> projectNames;
    private List<ContributorImpl> creators;
    private List<ContributorImpl> contributors;
    private List<ContributorImpl> publishers;
    private List<DatePointImpl> otherDates;
    private DatePointImpl createdDate;
    private DatePointImpl modifiedDate;
    private DatePointImpl issuedDate;
    private DatePointImpl validDate;
    private DatePointImpl availableDate;
    private DatePointImpl metadataCreatedDate;
    private DatePointImpl metadataModifiedDate;
    private GeospatialCoverageImpl geospatialCoverage;
    private DateRangeImpl temporalCoverage;
    private List<VariableGroupImpl> variableGroups;
    private long dataSizeInBytes;
    private DataFormatType dataFormat;
    private FeatureType dataType;
    private String collectionType;
    
    ThreddsMetadataImpl() {
        this.isBuilt = false;
        this.dataSizeInBytes = -1L;
    }
    
    public boolean isEmpty() {
        return (this.docs == null || this.docs.isEmpty()) && (this.keyphrases == null || this.keyphrases.isEmpty()) && (this.projectNames == null || this.projectNames.isEmpty()) && (this.creators == null || this.creators.isEmpty()) && (this.contributors == null || this.contributors.isEmpty()) && (this.publishers == null || this.publishers.isEmpty()) && this.createdDate == null && this.modifiedDate == null && this.issuedDate == null && this.validDate == null && this.availableDate == null && this.metadataCreatedDate == null && this.metadataModifiedDate == null && this.geospatialCoverage == null && this.temporalCoverage == null && (this.variableGroups == null || this.variableGroups.isEmpty()) && this.dataSizeInBytes == -1L && this.dataFormat == null && this.dataType == null && this.collectionType == null;
    }
    
    public DocumentationBuilder addDocumentation(final String docType, final String title, final String externalReference) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.docs == null) {
            this.docs = new ArrayList<DocumentationImpl>();
        }
        final DocumentationImpl doc = new DocumentationImpl(docType, title, externalReference);
        this.docs.add(doc);
        return doc;
    }
    
    public DocumentationBuilder addDocumentation(final String docType, final String content) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (content == null) {
            throw new IllegalArgumentException("Content may not be null.");
        }
        if (this.docs == null) {
            this.docs = new ArrayList<DocumentationImpl>();
        }
        final DocumentationImpl doc = new DocumentationImpl(docType, content);
        this.docs.add(doc);
        return doc;
    }
    
    public boolean removeDocumentation(final DocumentationBuilder docBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return docBuilder != null && this.docs != null && this.docs.remove(docBuilder);
    }
    
    public List<DocumentationBuilder> getDocumentationBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.docs == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends DocumentationBuilder>)new ArrayList<DocumentationBuilder>(this.docs));
    }
    
    public List<Documentation> getDocumentation() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.docs == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Documentation>)new ArrayList<Documentation>(this.docs));
    }
    
    public KeyphraseBuilder addKeyphrase(final String authority, final String phrase) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (phrase == null) {
            throw new IllegalArgumentException("Phrase may not be null.");
        }
        if (this.keyphrases == null) {
            this.keyphrases = new ArrayList<KeyphraseImpl>();
        }
        final KeyphraseImpl keyphrase = new KeyphraseImpl(authority, phrase);
        this.keyphrases.add(keyphrase);
        return keyphrase;
    }
    
    public boolean removeKeyphrase(final KeyphraseBuilder keyphraseBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return keyphraseBuilder != null && this.keyphrases != null && this.keyphrases.remove(keyphraseBuilder);
    }
    
    public List<KeyphraseBuilder> getKeyphraseBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.keyphrases == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends KeyphraseBuilder>)new ArrayList<KeyphraseBuilder>(this.keyphrases));
    }
    
    public List<Keyphrase> getKeyphrases() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.keyphrases == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Keyphrase>)new ArrayList<Keyphrase>(this.keyphrases));
    }
    
    public ProjectNameBuilder addProjectName(final String namingAuthority, final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (name == null) {
            throw new IllegalArgumentException("Project name may not be null.");
        }
        if (this.projectNames == null) {
            this.projectNames = new ArrayList<ProjectNameImpl>();
        }
        final ProjectNameImpl projectName = new ProjectNameImpl(namingAuthority, name);
        this.projectNames.add(projectName);
        return projectName;
    }
    
    public boolean removeProjectName(final ProjectNameBuilder projectNameBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return projectNameBuilder != null && this.projectNames != null && this.projectNames.remove(projectNameBuilder);
    }
    
    public List<ProjectNameBuilder> getProjectNameBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.projectNames == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends ProjectNameBuilder>)new ArrayList<ProjectNameBuilder>(this.projectNames));
    }
    
    public List<ProjectName> getProjectNames() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.projectNames == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends ProjectName>)new ArrayList<ProjectName>(this.projectNames));
    }
    
    public ContributorBuilder addCreator() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.creators == null) {
            this.creators = new ArrayList<ContributorImpl>();
        }
        final ContributorImpl contributor = new ContributorImpl();
        this.creators.add(contributor);
        return contributor;
    }
    
    public boolean removeCreator(final ContributorBuilder creatorBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return creatorBuilder != null && this.creators != null && this.creators.remove(creatorBuilder);
    }
    
    public List<ContributorBuilder> getCreatorBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.creators == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends ContributorBuilder>)new ArrayList<ContributorBuilder>(this.creators));
    }
    
    public List<Contributor> getCreator() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.creators == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Contributor>)new ArrayList<Contributor>(this.creators));
    }
    
    public ContributorBuilder addContributor() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.contributors == null) {
            this.contributors = new ArrayList<ContributorImpl>();
        }
        final ContributorImpl contributor = new ContributorImpl();
        this.contributors.add(contributor);
        return contributor;
    }
    
    public boolean removeContributor(final ContributorBuilder contributorBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return contributorBuilder != null && this.contributors != null && this.contributors.remove(contributorBuilder);
    }
    
    public List<ContributorBuilder> getContributorBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.contributors == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends ContributorBuilder>)new ArrayList<ContributorBuilder>(this.contributors));
    }
    
    public List<Contributor> getContributor() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.contributors == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Contributor>)new ArrayList<Contributor>(this.contributors));
    }
    
    public ContributorBuilder addPublisher() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.publishers == null) {
            this.publishers = new ArrayList<ContributorImpl>();
        }
        final ContributorImpl contributor = new ContributorImpl();
        this.publishers.add(contributor);
        return contributor;
    }
    
    public boolean removePublisher(final ContributorBuilder publisherBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return publisherBuilder != null && this.publishers != null && this.publishers.remove(publisherBuilder);
    }
    
    public List<ContributorBuilder> getPublisherBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.publishers == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends ContributorBuilder>)new ArrayList<ContributorBuilder>(this.publishers));
    }
    
    public List<Contributor> getPublisher() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.publishers == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Contributor>)new ArrayList<Contributor>(this.publishers));
    }
    
    public DatePointBuilder addOtherDatePointBuilder(final String date, final String format, final String type) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        final DatePointType datePointType = DatePointType.getTypeForLabel(type);
        if (datePointType != DatePointType.Other && datePointType != DatePointType.Untyped) {
            throw new IllegalArgumentException("Must use explicit setter method for given type [" + type + "].");
        }
        if (this.otherDates == null) {
            this.otherDates = new ArrayList<DatePointImpl>();
        }
        final DatePointImpl dp = new DatePointImpl(date, format, type);
        this.otherDates.add(dp);
        return dp;
    }
    
    public boolean removeOtherDatePointBuilder(final DatePointBuilder builder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return builder != null && this.otherDates != null && this.otherDates.remove(builder);
    }
    
    public List<DatePointBuilder> getOtherDatePointBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.otherDates == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends DatePointBuilder>)new ArrayList<DatePointBuilder>(this.otherDates));
    }
    
    public List<DatePoint> getOtherDates() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.otherDates == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends DatePoint>)new ArrayList<DatePoint>(this.otherDates));
    }
    
    public DatePointBuilder setCreatedDatePointBuilder(final String date, final String format) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.createdDate = new DatePointImpl(date, format, DatePointType.Created.toString());
    }
    
    public DatePointBuilder getCreatedDatePointBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.createdDate;
    }
    
    public DatePoint getCreatedDate() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.createdDate;
    }
    
    public DatePointBuilder setModifiedDatePointBuilder(final String date, final String format) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.modifiedDate = new DatePointImpl(date, format, DatePointType.Modified.toString());
    }
    
    public DatePointBuilder getModifiedDatePointBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.modifiedDate;
    }
    
    public DatePoint getModifiedDate() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.modifiedDate;
    }
    
    public DatePointBuilder setIssuedDatePointBuilder(final String date, final String format) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.issuedDate = new DatePointImpl(date, format, DatePointType.Issued.toString());
    }
    
    public DatePointBuilder getIssuedDatePointBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.issuedDate;
    }
    
    public DatePoint getIssuedDate() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.issuedDate;
    }
    
    public DatePointBuilder setValidDatePointBuilder(final String date, final String format) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.validDate = new DatePointImpl(date, format, DatePointType.Valid.toString());
    }
    
    public DatePointBuilder getValidDatePointBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.validDate;
    }
    
    public DatePoint getValidDate() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.validDate;
    }
    
    public DatePointBuilder setAvailableDatePointBuilder(final String date, final String format) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.availableDate = new DatePointImpl(date, format, DatePointType.Available.toString());
    }
    
    public DatePointBuilder getAvailableDatePointBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.availableDate;
    }
    
    public DatePoint getAvailableDate() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.availableDate;
    }
    
    public DatePointBuilder setMetadataCreatedDatePointBuilder(final String date, final String format) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.metadataCreatedDate = new DatePointImpl(date, format, DatePointType.MetadataCreated.toString());
    }
    
    public DatePointBuilder getMetadataCreatedDatePointBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.metadataCreatedDate;
    }
    
    public DatePoint getMetadataCreatedDate() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.metadataCreatedDate;
    }
    
    public DatePointBuilder setMetadataModifiedDatePointBuilder(final String date, final String format) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.metadataModifiedDate = new DatePointImpl(date, format, DatePointType.MetadataModified.toString());
    }
    
    public DatePointBuilder getMetadataModifiedDatePointBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.metadataModifiedDate;
    }
    
    public DatePoint getMetadataModifiedDate() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.metadataModifiedDate;
    }
    
    public GeospatialCoverageBuilder setNewGeospatialCoverageBuilder(final URI crsUri) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        final GeospatialCoverageImpl gci = new GeospatialCoverageImpl();
        gci.setCRS(crsUri);
        this.geospatialCoverage = gci;
        return null;
    }
    
    public void removeGeospatialCoverageBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        this.geospatialCoverage = null;
    }
    
    public GeospatialCoverageBuilder getGeospatialCoverageBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.geospatialCoverage;
    }
    
    public GeospatialCoverage getGeospatialCoverage() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.geospatialCoverage;
    }
    
    public DateRangeBuilder setTemporalCoverageBuilder(final String startDate, final String startDateFormat, final String endDate, final String endDateFormat, final String duration, final String resolution) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.temporalCoverage = new DateRangeImpl(startDate, startDateFormat, endDate, endDateFormat, duration, resolution);
    }
    
    public DateRangeBuilder getTemporalCoverageBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return this.temporalCoverage;
    }
    
    public DateRange getTemporalCoverage() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        return this.temporalCoverage;
    }
    
    public VariableGroupBuilder addVariableGroupBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.variableGroups == null) {
            this.variableGroups = new ArrayList<VariableGroupImpl>();
        }
        final VariableGroupImpl varGroup = new VariableGroupImpl();
        this.variableGroups.add(varGroup);
        return varGroup;
    }
    
    public boolean removeVariableGroupBuilder(final VariableGroupBuilder variableGroupBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        return variableGroupBuilder != null && this.variableGroups != null && this.variableGroups.remove(variableGroupBuilder);
    }
    
    public List<VariableGroupBuilder> getVariableGroupBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        if (this.variableGroups == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends VariableGroupBuilder>)new ArrayList<VariableGroupBuilder>(this.variableGroups));
    }
    
    public List<VariableGroup> getVariableGroups() {
        if (!this.isBuilt) {
            throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
        }
        if (this.variableGroups == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends VariableGroup>)new ArrayList<VariableGroup>(this.variableGroups));
    }
    
    public void setDataSizeInBytes(final long dataSizeInBytes) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        this.dataSizeInBytes = dataSizeInBytes;
    }
    
    public long getDataSizeInBytes() {
        return this.dataSizeInBytes;
    }
    
    public void setDataFormat(final DataFormatType dataFormat) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        this.dataFormat = dataFormat;
    }
    
    public void setDataFormat(final String dataFormat) {
        this.setDataFormat(DataFormatType.getType(dataFormat));
    }
    
    public DataFormatType getDataFormat() {
        return this.dataFormat;
    }
    
    public void setDataType(final FeatureType dataType) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        this.dataType = dataType;
    }
    
    public void setDataType(final String dataType) {
        this.setDataType(FeatureType.getType(dataType));
    }
    
    public FeatureType getDataType() {
        return this.dataType;
    }
    
    public void setCollectionType(final String collectionType) {
        if (this.isBuilt) {
            throw new IllegalStateException("This Builder has been built.");
        }
        this.collectionType = collectionType;
    }
    
    public String getCollectionType() {
        return this.collectionType;
    }
    
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    public BuilderIssues getIssues() {
        final BuilderIssues issues = new BuilderIssues();
        if (this.docs != null) {
            for (final DocumentationImpl doc : this.docs) {
                issues.addAllIssues(doc.getIssues());
            }
        }
        if (this.keyphrases != null) {
            for (final KeyphraseImpl keyphrase : this.keyphrases) {
                issues.addAllIssues(keyphrase.getIssues());
            }
        }
        if (this.creators != null) {
            for (final ContributorImpl creator : this.creators) {
                issues.addAllIssues(creator.getIssues());
            }
        }
        if (this.contributors != null) {
            for (final ContributorImpl contributor : this.contributors) {
                issues.addAllIssues(contributor.getIssues());
            }
        }
        if (this.publishers != null) {
            for (final ContributorImpl publisher : this.publishers) {
                issues.addAllIssues(publisher.getIssues());
            }
        }
        if (this.otherDates != null) {
            for (final DatePointImpl date : this.otherDates) {
                issues.addAllIssues(date.getIssues());
            }
        }
        if (this.createdDate != null) {
            issues.addAllIssues(this.createdDate.getIssues());
        }
        if (this.modifiedDate != null) {
            issues.addAllIssues(this.modifiedDate.getIssues());
        }
        if (this.issuedDate != null) {
            issues.addAllIssues(this.issuedDate.getIssues());
        }
        if (this.validDate != null) {
            issues.addAllIssues(this.validDate.getIssues());
        }
        if (this.availableDate != null) {
            issues.addAllIssues(this.availableDate.getIssues());
        }
        if (this.metadataCreatedDate != null) {
            issues.addAllIssues(this.metadataCreatedDate.getIssues());
        }
        if (this.metadataModifiedDate != null) {
            issues.addAllIssues(this.metadataModifiedDate.getIssues());
        }
        if (this.geospatialCoverage != null) {
            issues.addAllIssues(this.geospatialCoverage.getIssues());
        }
        if (this.temporalCoverage != null) {
            issues.addAllIssues(this.temporalCoverage.getIssues());
        }
        if (this.variableGroups != null) {
            for (final VariableGroupImpl variableGroup : this.variableGroups) {
                issues.addAllIssues(variableGroup.getIssues());
            }
        }
        return issues;
    }
    
    public ThreddsMetadata build() throws BuilderException {
        if (this.isBuilt) {
            return this;
        }
        if (this.docs != null) {
            for (final DocumentationImpl doc : this.docs) {
                doc.build();
            }
        }
        if (this.keyphrases != null) {
            for (final KeyphraseImpl keyphrase : this.keyphrases) {
                keyphrase.build();
            }
        }
        if (this.creators != null) {
            for (final ContributorImpl creator : this.creators) {
                creator.build();
            }
        }
        if (this.contributors != null) {
            for (final ContributorImpl contributor : this.contributors) {
                contributor.build();
            }
        }
        if (this.publishers != null) {
            for (final ContributorImpl publisher : this.publishers) {
                publisher.build();
            }
        }
        if (this.otherDates != null) {
            for (final DatePointImpl date : this.otherDates) {
                date.build();
            }
        }
        if (this.createdDate != null) {
            this.createdDate.build();
        }
        if (this.modifiedDate != null) {
            this.modifiedDate.build();
        }
        if (this.issuedDate != null) {
            this.issuedDate.build();
        }
        if (this.validDate != null) {
            this.validDate.build();
        }
        if (this.availableDate != null) {
            this.availableDate.build();
        }
        if (this.metadataCreatedDate != null) {
            this.metadataCreatedDate.build();
        }
        if (this.metadataModifiedDate != null) {
            this.metadataModifiedDate.build();
        }
        if (this.geospatialCoverage != null) {
            this.geospatialCoverage.build();
        }
        if (this.temporalCoverage != null) {
            this.temporalCoverage.build();
        }
        if (this.variableGroups != null) {
            for (final VariableGroupImpl variableGroup : this.variableGroups) {
                variableGroup.build();
            }
        }
        this.isBuilt = true;
        return this;
    }
    
    static class DocumentationImpl implements Documentation, DocumentationBuilder
    {
        private boolean isBuilt;
        private final boolean isContainedContent;
        private final String docType;
        private final String title;
        private final String externalReference;
        private final String content;
        
        DocumentationImpl(final String docType, final String title, final String externalReference) {
            this.isBuilt = false;
            this.isContainedContent = false;
            this.docType = docType;
            this.title = title;
            this.externalReference = externalReference;
            this.content = null;
        }
        
        DocumentationImpl(final String docType, final String content) {
            this.isBuilt = false;
            if (content == null) {
                throw new IllegalArgumentException("Content may not be null.");
            }
            this.isContainedContent = true;
            this.docType = docType;
            this.title = null;
            this.externalReference = null;
            this.content = content;
        }
        
        public boolean isContainedContent() {
            return this.isContainedContent;
        }
        
        public String getDocType() {
            return this.docType;
        }
        
        public String getContent() {
            if (!this.isContainedContent) {
                throw new IllegalStateException("No contained content, use externally reference to access documentation content.");
            }
            return this.content;
        }
        
        public String getTitle() {
            if (this.isContainedContent) {
                throw new IllegalStateException("Documentation with contained content has no title.");
            }
            return this.title;
        }
        
        public String getExternalReference() {
            if (this.isContainedContent) {
                throw new IllegalStateException("Documentation with contained content has no external reference.");
            }
            return this.externalReference;
        }
        
        public URI getExternalReferenceAsUri() throws URISyntaxException {
            if (this.isContainedContent) {
                throw new IllegalStateException("Documentation with contained content has no external reference.");
            }
            return (this.externalReference != null) ? new URI(this.externalReference) : null;
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            return new BuilderIssues();
        }
        
        public Documentation build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class KeyphraseImpl implements Keyphrase, KeyphraseBuilder
    {
        private boolean isBuilt;
        private final String authority;
        private final String phrase;
        
        KeyphraseImpl(final String authority, final String phrase) {
            if (phrase == null || phrase.equals("")) {
                throw new IllegalArgumentException("Phrase may not be null.");
            }
            this.authority = authority;
            this.phrase = phrase;
            this.isBuilt = false;
        }
        
        public String getAuthority() {
            return this.authority;
        }
        
        public String getPhrase() {
            return this.phrase;
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            if (this.phrase == null || this.phrase.equals("")) {
                return new BuilderIssues(BuilderIssue.Severity.WARNING, "Phrase may not be null or empty.", this, null);
            }
            return new BuilderIssues();
        }
        
        public Keyphrase build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class ProjectNameImpl implements ProjectName, ProjectNameBuilder
    {
        private boolean isBuilt;
        private String namingAuthority;
        private String projectName;
        
        ProjectNameImpl(final String namingAuthority, final String projectName) {
            if (projectName == null || projectName.equals("")) {
                throw new IllegalArgumentException("Phrase may not be null.");
            }
            this.namingAuthority = namingAuthority;
            this.projectName = projectName;
            this.isBuilt = false;
        }
        
        public String getNamingAuthority() {
            return this.namingAuthority;
        }
        
        public String getName() {
            return this.projectName;
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            if (this.projectName == null || this.projectName.equals("")) {
                return new BuilderIssues(BuilderIssue.Severity.WARNING, "Phrase may not be null or empty.", this, null);
            }
            return new BuilderIssues();
        }
        
        public ProjectName build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class DatePointImpl implements DatePoint, DatePointBuilder
    {
        private boolean isBuilt;
        private final String date;
        private final String format;
        private final String type;
        
        DatePointImpl(final String date, final String format, final String type) {
            this.isBuilt = false;
            if (date == null) {
                throw new IllegalArgumentException("Date may not be null.");
            }
            this.date = date;
            this.format = format;
            this.type = type;
        }
        
        public String getDate() {
            return this.date;
        }
        
        public String getDateFormat() {
            return this.format;
        }
        
        public boolean isTyped() {
            return this.type != null || this.type.equals("");
        }
        
        public String getType() {
            return this.type;
        }
        
        @Override
        public boolean equals(final Object obj) {
            return this == obj || (obj instanceof DatePointImpl && obj.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            int result = 17;
            if (this.date != null) {
                result = 37 * result + this.date.hashCode();
            }
            if (this.format != null) {
                result = 37 * result + this.format.hashCode();
            }
            if (this.type != null) {
                result = 37 * result + this.type.hashCode();
            }
            return result;
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            if (this.date == null) {
                return new BuilderIssues(BuilderIssue.Severity.ERROR, "Date may not be null.", this, null);
            }
            return new BuilderIssues();
        }
        
        public DatePoint build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class DateRangeImpl implements DateRange, DateRangeBuilder
    {
        private boolean isBuilt;
        private final String startDateFormat;
        private final String startDate;
        private final String endDateFormat;
        private final String endDate;
        private final String duration;
        private final String resolution;
        
        DateRangeImpl(final String startDate, final String startDateFormat, final String endDate, final String endDateFormat, final String duration, final String resolution) {
            this.isBuilt = false;
            this.startDateFormat = startDateFormat;
            this.startDate = startDate;
            this.endDateFormat = endDateFormat;
            this.endDate = endDate;
            this.duration = duration;
            this.resolution = resolution;
        }
        
        public String getStartDateFormat() {
            return this.startDateFormat;
        }
        
        public String getStartDate() {
            return this.startDate;
        }
        
        public String getEndDateFormat() {
            return this.endDateFormat;
        }
        
        public String getEndDate() {
            return this.endDate;
        }
        
        public String getDuration() {
            return this.duration;
        }
        
        public String getResolution() {
            return this.resolution;
        }
        
        @Override
        public String toString() {
            return (this.isBuilt ? "DateRange" : "DateRangeBuilder") + " [" + this.startDate + " <-- " + this.duration + " --> " + this.endDate + "]";
        }
        
        @Override
        public boolean equals(final Object obj) {
            return this == obj || (obj instanceof DateRangeImpl && obj.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            int result = 17;
            if (this.startDate != null) {
                result = 37 * result + this.startDate.hashCode();
            }
            if (this.startDateFormat != null) {
                result = 37 * result + this.startDateFormat.hashCode();
            }
            if (this.endDate != null) {
                result = 37 * result + this.endDate.hashCode();
            }
            if (this.endDateFormat != null) {
                result = 37 * result + this.endDateFormat.hashCode();
            }
            if (this.duration != null) {
                result = 37 * result + this.duration.hashCode();
            }
            return result;
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            int specified = 3;
            if (this.startDate == null || this.startDate.equals("")) {
                --specified;
            }
            if (this.endDate == null || this.endDate.equals("")) {
                --specified;
            }
            if (this.duration == null || this.duration.equals("")) {
                --specified;
            }
            if (specified == 2) {
                return new BuilderIssues();
            }
            if (specified < 2) {
                return new BuilderIssues(BuilderIssue.Severity.ERROR, "Underspecified " + this.toString(), this, null);
            }
            return new BuilderIssues(BuilderIssue.Severity.ERROR, "Overspecified " + this.toString(), this, null);
        }
        
        public DateRange build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class ContributorImpl implements Contributor, ContributorBuilder
    {
        private boolean isBuilt;
        private String authority;
        private String name;
        private String role;
        private String email;
        private String webPage;
        
        public String getNamingAuthority() {
            return this.authority;
        }
        
        public void setNamingAuthority(final String authority) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.authority = authority;
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            if (name == null) {
                throw new IllegalArgumentException("Name may not be null.");
            }
            this.name = name;
        }
        
        public String getRole() {
            return this.role;
        }
        
        public void setRole(final String role) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.role = role;
        }
        
        public String getEmail() {
            return this.email;
        }
        
        public void setEmail(final String email) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.email = email;
        }
        
        public String getWebPage() {
            return this.webPage;
        }
        
        public void setWebPage(final String webPage) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.webPage = webPage;
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            if (this.name == null) {
                return new BuilderIssues(BuilderIssue.Severity.ERROR, "Name may not be null.", this, null);
            }
            return new BuilderIssues();
        }
        
        public Contributor build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class VariableGroupImpl implements VariableGroup, VariableGroupBuilder
    {
        private boolean isBuilt;
        private String vocabularyAuthorityId;
        private String vocabularyAuthorityUrl;
        private List<VariableImpl> variables;
        private String variableMapUrl;
        
        VariableGroupImpl() {
            this.isBuilt = false;
        }
        
        public String getVocabularyAuthorityId() {
            return this.vocabularyAuthorityId;
        }
        
        public void setVocabularyAuthorityId(final String vocabAuthId) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has already been built.");
            }
            this.vocabularyAuthorityId = vocabAuthId;
        }
        
        public String getVocabularyAuthorityUrl() {
            return this.vocabularyAuthorityUrl;
        }
        
        public void setVocabularyAuthorityUrl(final String vocabAuthUrl) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has already been built.");
            }
            this.vocabularyAuthorityUrl = vocabAuthUrl;
        }
        
        public List<Variable> getVariables() {
            if (!this.isBuilt) {
                throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
            }
            if (this.variables == null) {
                return Collections.emptyList();
            }
            return Collections.unmodifiableList((List<? extends Variable>)new ArrayList<Variable>(this.variables));
        }
        
        public List<VariableBuilder> getVariableBuilders() {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has already been built.");
            }
            if (this.variables == null) {
                return Collections.emptyList();
            }
            return Collections.unmodifiableList((List<? extends VariableBuilder>)new ArrayList<VariableBuilder>(this.variables));
        }
        
        public VariableBuilder addVariableBuilder(final String name, final String description, final String units, final String vocabId, final String vocabName) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has already been built.");
            }
            if (this.variableMapUrl != null) {
                throw new IllegalStateException("Already contains variableMap, can't add variables.");
            }
            final VariableImpl newVar = new VariableImpl(name, description, units, vocabId, vocabName, this);
            if (this.variables == null) {
                this.variables = new ArrayList<VariableImpl>();
            }
            this.variables.add(newVar);
            return newVar;
        }
        
        public String getVariableMapUrl() {
            return this.variableMapUrl;
        }
        
        public void setVariableMapUrl(final String variableMapUrl) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has already been built.");
            }
            if (variableMapUrl != null && this.variables != null && !this.variables.isEmpty()) {
                throw new IllegalStateException("Already contains variables, can't set variableMap.");
            }
            this.variableMapUrl = variableMapUrl;
        }
        
        public boolean isEmpty() {
            return this.variableMapUrl == null && (this.variables == null || this.variables.isEmpty());
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            if (this.variableMapUrl != null && this.variables != null && !this.variables.isEmpty()) {
                return new BuilderIssues(BuilderIssue.Severity.ERROR, "This VariableGroupBuilder has variables and variableMap.", this, null);
            }
            return new BuilderIssues();
        }
        
        public Object build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class VariableImpl implements Variable, VariableBuilder
    {
        private boolean isBuilt;
        private String name;
        private String description;
        private String units;
        private String vocabularyId;
        private String vocabularyName;
        private VariableGroupBuilder parent;
        
        VariableImpl(final String name, final String description, final String units, final String vocabId, final String vocabName, final VariableGroupBuilder parent) {
            this.name = name;
            this.description = description;
            this.units = units;
            this.vocabularyId = vocabId;
            this.vocabularyName = vocabName;
            this.parent = parent;
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.name = name;
        }
        
        public String getDescription() {
            return this.description;
        }
        
        public void setDescription(final String description) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.description = description;
        }
        
        public String getUnits() {
            return this.units;
        }
        
        public void setUnits(final String units) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.units = units;
        }
        
        public String getVocabularyId() {
            return this.vocabularyId;
        }
        
        public void setVocabularyId(final String vocabularyId) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.vocabularyId = vocabularyId;
        }
        
        public String getVocabularyName() {
            return this.vocabularyName;
        }
        
        public void setVocabularyName(final String vocabularyName) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.vocabularyName = vocabularyName;
        }
        
        public String getVocabularyAuthorityId() {
            return this.parent.getVocabularyAuthorityId();
        }
        
        public String getVocabularyAuthorityUrl() {
            return this.parent.getVocabularyAuthorityUrl();
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            if (this.name == null || this.name.length() == 0) {
                return new BuilderIssues(BuilderIssue.Severity.WARNING, "Variable name is null or empty.", this, null);
            }
            return new BuilderIssues();
        }
        
        public Variable build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
    
    static class GeospatialCoverageImpl implements GeospatialCoverage, GeospatialCoverageBuilder
    {
        private boolean isBuilt;
        private URI defaultCrsUri;
        private URI crsUri;
        private boolean isZPositiveUp;
        private boolean isGlobal;
        private List<GeospatialRangeImpl> extent;
        
        GeospatialCoverageImpl() {
            this.isBuilt = false;
            final String defaultCrsUriString = "urn:x-mycrs:2D-WGS84-ellipsoid";
            try {
                this.defaultCrsUri = new URI(defaultCrsUriString);
            }
            catch (URISyntaxException e) {
                throw new IllegalStateException("Bad URI syntax for default CRS URI [" + defaultCrsUriString + "]: " + e.getMessage());
            }
            this.crsUri = this.defaultCrsUri;
        }
        
        public void setCRS(final URI crsUri) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            if (crsUri == null) {
                this.crsUri = this.defaultCrsUri;
            }
            this.crsUri = crsUri;
        }
        
        public URI getCRS() {
            return this.crsUri;
        }
        
        public void setGlobal(final boolean isGlobal) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.isGlobal = isGlobal;
        }
        
        public boolean isGlobal() {
            return this.isGlobal;
        }
        
        public void setZPositiveUp(final boolean isZPositiveUp) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.isZPositiveUp = isZPositiveUp;
        }
        
        public boolean isZPositiveUp() {
            return this.isZPositiveUp;
        }
        
        public GeospatialRangeBuilder addExtentBuilder() {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            if (this.extent == null) {
                this.extent = new ArrayList<GeospatialRangeImpl>();
            }
            final GeospatialRangeImpl gri = new GeospatialRangeImpl();
            this.extent.add(gri);
            return gri;
        }
        
        public boolean removeExtentBuilder(final GeospatialRangeBuilder geospatialRangeBuilder) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            return geospatialRangeBuilder == null || (this.extent != null && this.extent.remove(geospatialRangeBuilder));
        }
        
        public List<GeospatialRangeBuilder> getExtentBuilders() {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            if (this.extent == null) {
                return Collections.emptyList();
            }
            return Collections.unmodifiableList((List<? extends GeospatialRangeBuilder>)new ArrayList<GeospatialRangeBuilder>(this.extent));
        }
        
        public List<GeospatialRange> getExtent() {
            if (!this.isBuilt) {
                throw new IllegalStateException("Sorry, I've escaped from my Builder before being built.");
            }
            if (this.extent == null) {
                return Collections.emptyList();
            }
            return Collections.unmodifiableList((List<? extends GeospatialRange>)new ArrayList<GeospatialRange>(this.extent));
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            return new BuilderIssues();
        }
        
        public GeospatialCoverage build() throws BuilderException {
            return this;
        }
    }
    
    static class GeospatialRangeImpl implements GeospatialRange, GeospatialRangeBuilder
    {
        private boolean isBuilt;
        private boolean isHorizontal;
        private double start;
        private double size;
        private double resolution;
        private String units;
        
        GeospatialRangeImpl() {
            this.isBuilt = false;
            this.isHorizontal = false;
            this.start = 0.0;
            this.size = 0.0;
            this.resolution = 0.0;
            this.units = "";
        }
        
        public void setHorizontal(final boolean isHorizontal) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.isHorizontal = isHorizontal;
        }
        
        public boolean isHorizontal() {
            return this.isHorizontal;
        }
        
        public void setStart(final double start) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.start = start;
        }
        
        public double getStart() {
            return this.start;
        }
        
        public void setSize(final double size) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.size = size;
        }
        
        public double getSize() {
            return this.size;
        }
        
        public void setResolution(final double resolution) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.resolution = resolution;
        }
        
        public double getResolution() {
            return this.resolution;
        }
        
        public void setUnits(final String units) {
            if (this.isBuilt) {
                throw new IllegalStateException("This Builder has been built.");
            }
            this.units = ((units == null) ? "" : units);
        }
        
        public String getUnits() {
            return this.units;
        }
        
        public boolean isBuilt() {
            return this.isBuilt;
        }
        
        public BuilderIssues getIssues() {
            return new BuilderIssues();
        }
        
        public GeospatialRange build() throws BuilderException {
            this.isBuilt = true;
            return this;
        }
    }
}
