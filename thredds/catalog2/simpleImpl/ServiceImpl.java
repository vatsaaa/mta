// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.builder.BuilderIssue;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.BuilderIssues;
import thredds.catalog2.Property;
import java.util.List;
import java.net.URI;
import thredds.catalog.ServiceType;
import thredds.catalog2.builder.ServiceBuilder;
import thredds.catalog2.Service;

class ServiceImpl implements Service, ServiceBuilder
{
    private String name;
    private String description;
    private ServiceType type;
    private URI baseUri;
    private String suffix;
    private PropertyContainer propertyContainer;
    private ServiceContainer serviceContainer;
    private final GlobalServiceContainer globalServiceContainer;
    private final boolean isRootContainer;
    private boolean isBuilt;
    
    ServiceImpl(final String name, final ServiceType type, final URI baseUri, final GlobalServiceContainer globalServiceContainer) {
        this.isBuilt = false;
        if (name == null) {
            throw new IllegalArgumentException("Name must not be null.");
        }
        if (type == null) {
            throw new IllegalArgumentException("Service type must not be null.");
        }
        if (baseUri == null) {
            throw new IllegalArgumentException("Base URI must not be null.");
        }
        this.name = name;
        this.description = "";
        this.type = type;
        this.baseUri = baseUri;
        this.suffix = "";
        this.propertyContainer = new PropertyContainer();
        if (globalServiceContainer == null) {
            this.isRootContainer = true;
            this.globalServiceContainer = new GlobalServiceContainer();
        }
        else {
            this.isRootContainer = false;
            this.globalServiceContainer = globalServiceContainer;
        }
        this.serviceContainer = new ServiceContainer(this.globalServiceContainer);
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setDescription(final String description) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        this.description = ((description != null) ? description : "");
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setType(final ServiceType type) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        if (type == null) {
            throw new IllegalArgumentException("Service type must not be null.");
        }
        this.type = type;
    }
    
    public ServiceType getType() {
        return this.type;
    }
    
    public void setBaseUri(final URI baseUri) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        if (baseUri == null) {
            throw new IllegalArgumentException("Base URI must not be null.");
        }
        this.baseUri = baseUri;
    }
    
    public URI getBaseUri() {
        return this.baseUri;
    }
    
    public void setSuffix(final String suffix) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        this.suffix = ((suffix != null) ? suffix : "");
    }
    
    public String getSuffix() {
        return this.suffix;
    }
    
    public void addProperty(final String name, final String value) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        this.propertyContainer.addProperty(name, value);
    }
    
    public boolean removeProperty(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        return this.propertyContainer.removeProperty(name);
    }
    
    public List<String> getPropertyNames() {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        return this.propertyContainer.getPropertyNames();
    }
    
    public String getPropertyValue(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        return this.propertyContainer.getPropertyValue(name);
    }
    
    public List<Property> getProperties() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Service has escaped from its ServiceBuilder before build() was called.");
        }
        return this.propertyContainer.getProperties();
    }
    
    public Property getPropertyByName(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Service has escaped from its ServiceBuilder before build() was called.");
        }
        return this.propertyContainer.getPropertyByName(name);
    }
    
    public ServiceBuilder addService(final String name, final ServiceType type, final URI baseUri) {
        return this.serviceContainer.addService(name, type, baseUri);
    }
    
    public boolean removeService(final ServiceBuilder serviceBuilder) {
        return serviceBuilder != null && this.serviceContainer.removeService((ServiceImpl)serviceBuilder);
    }
    
    public List<Service> getServices() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Service has escaped from its ServiceBuilder without being built.");
        }
        return this.serviceContainer.getServices();
    }
    
    public Service getServiceByName(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Service has escaped from its ServiceBuilder without being built.");
        }
        return this.serviceContainer.getServiceByName(name);
    }
    
    public Service findServiceByNameGlobally(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Service has escaped its Builder before being built.");
        }
        return this.globalServiceContainer.getServiceByGloballyUniqueName(name);
    }
    
    public List<ServiceBuilder> getServiceBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        return this.serviceContainer.getServiceBuilders();
    }
    
    public ServiceBuilder getServiceBuilderByName(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        return this.serviceContainer.getServiceBuilderByName(name);
    }
    
    public ServiceBuilder findServiceBuilderByNameGlobally(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been built.");
        }
        return this.globalServiceContainer.getServiceByGloballyUniqueName(name);
    }
    
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    public BuilderIssues getIssues() {
        final BuilderIssues issues = this.serviceContainer.getIssues();
        if (this.isRootContainer) {
            issues.addAllIssues(this.globalServiceContainer.getIssues(this));
        }
        issues.addAllIssues(this.propertyContainer.getIssues());
        if (this.serviceContainer.isEmpty() && this.baseUri == null) {
            issues.addIssue(BuilderIssue.Severity.WARNING, "Non-compound services must have base URI.", this, null);
        }
        return issues;
    }
    
    public Service build() throws BuilderException {
        if (this.isBuilt) {
            return this;
        }
        this.propertyContainer.build();
        this.serviceContainer.build();
        this.isBuilt = true;
        return this;
    }
}
