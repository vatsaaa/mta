// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderException;
import java.util.Iterator;
import thredds.catalog2.builder.BuilderIssues;
import thredds.catalog2.builder.CatalogRefBuilder;
import java.net.URI;
import thredds.catalog2.builder.DatasetBuilder;
import thredds.catalog2.builder.CatalogBuilder;
import thredds.catalog2.Catalog;
import java.util.Collection;
import java.util.Collections;
import thredds.catalog2.Metadata;
import java.util.ArrayList;
import thredds.catalog2.builder.MetadataBuilder;
import thredds.catalog2.ThreddsMetadata;
import thredds.catalog2.builder.ThreddsMetadataBuilder;
import thredds.catalog2.Property;
import java.util.List;
import thredds.catalog2.builder.DatasetNodeBuilder;
import thredds.catalog2.DatasetNode;

class DatasetNodeImpl implements DatasetNode, DatasetNodeBuilder
{
    private String id;
    private String idAuthority;
    private String name;
    private PropertyContainer propertyContainer;
    private ThreddsMetadataImpl threddsMetadataImpl;
    private List<MetadataImpl> metadataImplList;
    private CatalogImpl parentCatalog;
    protected DatasetNodeImpl parent;
    private DatasetNodeContainer parentDatasetContainer;
    private DatasetNodeContainer datasetContainer;
    private boolean isBuilt;
    
    DatasetNodeImpl(final String name, final CatalogImpl parentCatalog, final DatasetNodeImpl parent) {
        this.isBuilt = false;
        if (name == null) {
            throw new IllegalArgumentException("DatasetNode name must not be null.");
        }
        this.name = name;
        this.parentCatalog = parentCatalog;
        this.parent = parent;
        this.propertyContainer = new PropertyContainer();
        if (this.parent != null) {
            this.parentDatasetContainer = this.parent.getDatasetNodeContainer();
        }
        else if (this.parentCatalog != null) {
            this.parentDatasetContainer = this.parentCatalog.getDatasetNodeContainer();
        }
        else {
            this.parentDatasetContainer = null;
        }
        final DatasetNodeContainer rootContainer = (this.parentDatasetContainer != null) ? this.parentDatasetContainer.getRootContainer() : null;
        this.datasetContainer = new DatasetNodeContainer(rootContainer);
    }
    
    DatasetNodeContainer getDatasetNodeContainer() {
        return this.datasetContainer;
    }
    
    public void setId(final String id) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been finished().");
        }
        if (this.id != null) {
            if (!this.id.equals(id)) {
                if (this.isDatasetIdInUseGlobally(id)) {
                    throw new IllegalStateException();
                }
                if (this.parentDatasetContainer != null) {
                    this.parentDatasetContainer.removeDatasetNodeByGloballyUniqueId(this.id);
                    this.parentDatasetContainer.removeDatasetNodeFromLocalById(this.id);
                }
                if (id == null) {
                    this.id = null;
                    return;
                }
                this.id = id;
                if (this.parentDatasetContainer != null) {
                    this.parentDatasetContainer.addDatasetNodeByGloballyUniqueId(this);
                    this.parentDatasetContainer.addDatasetNodeToLocalById(this);
                }
            }
        }
        else {
            if (id == null) {
                return;
            }
            if (this.isDatasetIdInUseGlobally(id)) {
                throw new IllegalStateException();
            }
            this.id = id;
            if (this.parentDatasetContainer != null) {
                this.parentDatasetContainer.addDatasetNodeByGloballyUniqueId(this);
                this.parentDatasetContainer.addDatasetNodeToLocalById(this);
            }
        }
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setIdAuthority(final String idAuthority) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been finished().");
        }
        this.idAuthority = idAuthority;
    }
    
    public String getIdAuthority() {
        return this.idAuthority;
    }
    
    public void setName(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been finished().");
        }
        if (name == null) {
            throw new IllegalArgumentException("DatasetNode name must not be null.");
        }
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void addProperty(final String name, final String value) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        this.propertyContainer.addProperty(name, value);
    }
    
    public boolean removeProperty(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.propertyContainer.removeProperty(name);
    }
    
    public List<String> getPropertyNames() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been finished().");
        }
        return this.propertyContainer.getPropertyNames();
    }
    
    public String getPropertyValue(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been finished().");
        }
        return this.propertyContainer.getPropertyValue(name);
    }
    
    public List<Property> getProperties() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped from its DatasetNodeBuilder before build() was called.");
        }
        return this.propertyContainer.getProperties();
    }
    
    public Property getPropertyByName(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped from its ServiceBuilder before build() was called.");
        }
        return this.propertyContainer.getPropertyByName(name);
    }
    
    public ThreddsMetadataBuilder setNewThreddsMetadataBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.threddsMetadataImpl = new ThreddsMetadataImpl();
    }
    
    public boolean removeThreddsMetadataBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        this.threddsMetadataImpl = null;
        return true;
    }
    
    public ThreddsMetadataBuilder getThreddsMetadataBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.threddsMetadataImpl;
    }
    
    public ThreddsMetadata getThreddsMetadata() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped its DatasetNodeBuilder before being built.");
        }
        return this.threddsMetadataImpl;
    }
    
    public MetadataBuilder addMetadata() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        final MetadataImpl mbi = new MetadataImpl();
        if (this.metadataImplList == null) {
            this.metadataImplList = new ArrayList<MetadataImpl>();
        }
        this.metadataImplList.add(mbi);
        return mbi;
    }
    
    public boolean removeMetadata(final MetadataBuilder metadataBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return metadataBuilder != null && this.metadataImplList != null && this.metadataImplList.remove(metadataBuilder);
    }
    
    public List<Metadata> getMetadata() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped its DatasetNodeBuilder before being built.");
        }
        if (this.metadataImplList == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Metadata>)new ArrayList<Metadata>(this.metadataImplList));
    }
    
    public List<MetadataBuilder> getMetadataBuilders() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped its DatasetNodeBuilder before being built.");
        }
        if (this.metadataImplList == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends MetadataBuilder>)new ArrayList<MetadataBuilder>(this.metadataImplList));
    }
    
    public Catalog getParentCatalog() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped its DatasetNodeBuilder before being built.");
        }
        return this.parentCatalog;
    }
    
    public DatasetNode getParent() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped its DatasetNodeBuilder before being built.");
        }
        return this.parent;
    }
    
    public CatalogBuilder getParentCatalogBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.parentCatalog;
    }
    
    public DatasetNodeBuilder getParentDatasetBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.parent;
    }
    
    public boolean isCollection() {
        return !this.datasetContainer.isEmpty();
    }
    
    public boolean isDatasetIdInUseGlobally(final String id) {
        return this.datasetContainer.isDatasetNodeIdInUseGlobally(id);
    }
    
    public DatasetBuilder addDataset(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        final DatasetImpl ds = new DatasetImpl(name, this.parentCatalog, this);
        this.datasetContainer.addDatasetNode(ds);
        return ds;
    }
    
    public CatalogRefBuilder addCatalogRef(final String name, final URI reference) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        final CatalogRefImpl catRef = new CatalogRefImpl(name, reference, this.parentCatalog, this);
        this.datasetContainer.addDatasetNode(catRef);
        return catRef;
    }
    
    public boolean removeDatasetNode(final DatasetNodeBuilder datasetBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.datasetContainer.removeDatasetNode((DatasetNodeImpl)datasetBuilder);
    }
    
    public List<DatasetNode> getDatasets() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped its DatasetNodeBuilder before being built.");
        }
        return this.datasetContainer.getDatasets();
    }
    
    public DatasetNode getDatasetById(final String id) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This DatasetNode has escaped its DatasetNodeBuilder before being built.");
        }
        return this.datasetContainer.getDatasetById(id);
    }
    
    public List<DatasetNodeBuilder> getDatasetNodeBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.datasetContainer.getDatasetNodeBuilders();
    }
    
    public DatasetNodeBuilder getDatasetNodeBuilderById(final String id) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetNodeBuilder has been built.");
        }
        return this.datasetContainer.getDatasetNodeBuilderById(id);
    }
    
    public DatasetNodeBuilder findDatasetNodeBuilderByIdGlobally(final String id) {
        return this.datasetContainer.getDatasetNodeByGloballyUniqueId(id);
    }
    
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    public BuilderIssues getIssues() {
        final BuilderIssues issues = this.datasetContainer.getIssues();
        if (this.metadataImplList != null) {
            for (final MetadataBuilder mb : this.metadataImplList) {
                issues.addAllIssues(mb.getIssues());
            }
        }
        return issues;
    }
    
    public DatasetNode build() throws BuilderException {
        if (this.isBuilt) {
            return this;
        }
        if (this.metadataImplList != null) {
            for (final MetadataBuilder mb : this.metadataImplList) {
                mb.build();
            }
        }
        this.datasetContainer.build();
        this.isBuilt = true;
        return this;
    }
}
