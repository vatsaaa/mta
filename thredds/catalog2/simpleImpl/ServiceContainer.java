// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.builder.BuilderIssues;
import thredds.catalog2.builder.ServiceBuilder;
import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import thredds.catalog2.Service;
import java.util.ArrayList;
import java.net.URI;
import thredds.catalog.ServiceType;
import java.util.List;

class ServiceContainer
{
    private List<ServiceImpl> services;
    private final GlobalServiceContainer globalServiceContainer;
    private boolean isBuilt;
    
    ServiceContainer(final GlobalServiceContainer globalServiceContainer) {
        if (globalServiceContainer == null) {
            throw new IllegalArgumentException("");
        }
        this.isBuilt = false;
        this.globalServiceContainer = globalServiceContainer;
    }
    
    ServiceImpl getServiceByGloballyUniqueName(final String name) {
        return this.globalServiceContainer.getServiceByGloballyUniqueName(name);
    }
    
    boolean isEmpty() {
        return this.services == null || this.services.isEmpty();
    }
    
    int size() {
        if (this.services == null) {
            return 0;
        }
        return this.services.size();
    }
    
    ServiceImpl addService(final String name, final ServiceType type, final URI baseUri) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceContainer has been built.");
        }
        if (this.services == null) {
            this.services = new ArrayList<ServiceImpl>();
        }
        final ServiceImpl service = new ServiceImpl(name, type, baseUri, this.globalServiceContainer);
        final boolean addedService = this.services.add(service);
        assert addedService;
        this.globalServiceContainer.addService(service);
        return service;
    }
    
    boolean removeService(final ServiceImpl service) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceContainer has been built.");
        }
        if (service == null) {
            return false;
        }
        if (!this.services.remove(service)) {
            return false;
        }
        final boolean success = this.globalServiceContainer.removeService(service);
        assert success;
        return true;
    }
    
    List<Service> getServices() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Service has escaped from its ServiceBuilder without being finished().");
        }
        if (this.services == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Service>)new ArrayList<Service>(this.services));
    }
    
    boolean containsServiceName(final String name) {
        return name != null && this.services != null && null != this.getServiceImplByName(name);
    }
    
    Service getServiceByName(final String name) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Service has escaped from its ServiceBuilder without being finished().");
        }
        if (name == null) {
            return null;
        }
        return this.getServiceImplByName(name);
    }
    
    private ServiceImpl getServiceImplByName(final String name) {
        if (this.services != null) {
            for (final ServiceImpl s : this.services) {
                if (s.getName().equals(name)) {
                    return s;
                }
            }
        }
        return null;
    }
    
    List<ServiceBuilder> getServiceBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been finished().");
        }
        if (this.services == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends ServiceBuilder>)new ArrayList<ServiceBuilder>(this.services));
    }
    
    ServiceBuilder getServiceBuilderByName(final String name) {
        if (this.isBuilt) {
            throw new IllegalStateException("This ServiceBuilder has been finished().");
        }
        if (name == null) {
            return null;
        }
        return this.getServiceImplByName(name);
    }
    
    boolean isBuilt() {
        return this.isBuilt;
    }
    
    BuilderIssues getIssues() {
        final BuilderIssues issues = new BuilderIssues();
        if (this.services != null) {
            for (final ServiceImpl sb : this.services) {
                issues.addAllIssues(sb.getIssues());
            }
        }
        return issues;
    }
    
    void build() throws BuilderException {
        if (this.isBuilt) {
            return;
        }
        if (this.services != null) {
            for (final ServiceImpl sb : this.services) {
                sb.build();
            }
        }
        this.isBuilt = true;
    }
}
