// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.DatasetNode;
import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.builder.BuilderIssues;
import java.util.Iterator;
import thredds.catalog.ServiceType;
import java.util.Collection;
import java.util.Collections;
import thredds.catalog2.Access;
import java.util.ArrayList;
import thredds.catalog2.builder.AccessBuilder;
import java.util.List;
import thredds.catalog2.builder.DatasetBuilder;
import thredds.catalog2.Dataset;

class DatasetImpl extends DatasetNodeImpl implements Dataset, DatasetBuilder
{
    private List<AccessImpl> accessImplList;
    private boolean isBuilt;
    
    DatasetImpl(final String name, final CatalogImpl parentCatalog, final DatasetNodeImpl parent) {
        super(name, parentCatalog, parent);
        this.isBuilt = false;
    }
    
    public AccessBuilder addAccessBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetBuilder has been built.");
        }
        final AccessImpl a = new AccessImpl(this);
        if (this.accessImplList == null) {
            this.accessImplList = new ArrayList<AccessImpl>();
        }
        this.accessImplList.add(a);
        return a;
    }
    
    public boolean removeAccessBuilder(final AccessBuilder accessBuilder) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetBuilder has been built.");
        }
        return this.accessImplList != null && this.accessImplList.remove(accessBuilder);
    }
    
    public boolean isAccessible() {
        return this.accessImplList != null && !this.accessImplList.isEmpty();
    }
    
    public List<Access> getAccesses() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Dataset has escaped its DatasetBuilder before build() was called.");
        }
        if (this.accessImplList == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends Access>)new ArrayList<Access>(this.accessImplList));
    }
    
    public List<Access> getAccessesByType(final ServiceType type) {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Dataset has escaped its DatasetBuilder before build() was called.");
        }
        final List<Access> list = new ArrayList<Access>();
        if (this.accessImplList != null) {
            for (final Access a : this.accessImplList) {
                if (a.getService().getType().equals(type)) {
                    list.add(a);
                }
            }
        }
        return list;
    }
    
    public List<AccessBuilder> getAccessBuilders() {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetBuilder has been built.");
        }
        if (this.accessImplList == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList((List<? extends AccessBuilder>)new ArrayList<AccessBuilder>(this.accessImplList));
    }
    
    public List<AccessBuilder> getAccessBuildersByType(final ServiceType type) {
        if (this.isBuilt) {
            throw new IllegalStateException("This DatasetBuilder has been built.");
        }
        final List<AccessBuilder> list = new ArrayList<AccessBuilder>();
        if (this.accessImplList != null) {
            for (final AccessBuilder a : this.accessImplList) {
                if (a.getServiceBuilder().getType().equals(type)) {
                    list.add(a);
                }
            }
        }
        return list;
    }
    
    @Override
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    @Override
    public BuilderIssues getIssues() {
        final BuilderIssues issues = super.getIssues();
        if (this.accessImplList != null) {
            for (final AccessBuilder ab : this.accessImplList) {
                issues.addAllIssues(ab.getIssues());
            }
        }
        return issues;
    }
    
    @Override
    public Dataset build() throws BuilderException {
        if (this.isBuilt) {
            return this;
        }
        super.build();
        if (this.accessImplList != null) {
            for (final AccessBuilder ab : this.accessImplList) {
                ab.build();
            }
        }
        this.isBuilt = true;
        return this;
    }
}
