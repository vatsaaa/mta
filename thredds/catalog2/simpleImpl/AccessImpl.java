// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2.simpleImpl;

import thredds.catalog2.builder.BuilderException;
import thredds.catalog2.builder.ThreddsBuilder;
import thredds.catalog2.builder.BuilderIssue;
import thredds.catalog2.builder.BuilderIssues;
import thredds.catalog2.Service;
import thredds.catalog2.builder.ServiceBuilder;
import thredds.catalog.DataFormatType;
import thredds.catalog2.builder.AccessBuilder;
import thredds.catalog2.Access;

class AccessImpl implements Access, AccessBuilder
{
    private final DatasetImpl parentDs;
    private ServiceImpl service;
    private String urlPath;
    private DataFormatType dataFormat;
    private long dataSize;
    private boolean isBuilt;
    
    AccessImpl(final DatasetImpl parentDataset) {
        this.isBuilt = false;
        this.parentDs = parentDataset;
    }
    
    public void setServiceBuilder(final ServiceBuilder service) {
        if (this.isBuilt) {
            throw new IllegalStateException("This AccessBuilder has been built.");
        }
        if (service == null) {
            throw new IllegalArgumentException("Service must not be null.");
        }
        this.service = (ServiceImpl)service;
    }
    
    public void setUrlPath(final String urlPath) {
        if (this.isBuilt) {
            throw new IllegalStateException("This AccessBuilder has been built.");
        }
        if (urlPath == null) {
            throw new IllegalArgumentException("Path must not be null.");
        }
        this.urlPath = urlPath;
    }
    
    public void setDataFormat(final DataFormatType dataFormat) {
        if (this.isBuilt) {
            throw new IllegalStateException("This AccessBuilder has been built.");
        }
        this.dataFormat = ((dataFormat != null) ? dataFormat : DataFormatType.NONE);
    }
    
    public void setDataSize(final long dataSize) {
        if (this.isBuilt) {
            throw new IllegalStateException("This AccessBuilder has been built.");
        }
        if (dataSize < -1L) {
            throw new IllegalArgumentException("Value must be zero or greater, or -1 if unknown.");
        }
        this.dataSize = dataSize;
    }
    
    public Service getService() {
        if (!this.isBuilt) {
            throw new IllegalStateException("This Access has escaped its AccessBuilder before build() was called.");
        }
        return this.service;
    }
    
    public ServiceBuilder getServiceBuilder() {
        if (this.isBuilt) {
            throw new IllegalStateException("This AccessBuilder has been built.");
        }
        return this.service;
    }
    
    public String getUrlPath() {
        return this.urlPath;
    }
    
    public DataFormatType getDataFormat() {
        return this.dataFormat;
    }
    
    public long getDataSize() {
        return this.dataSize;
    }
    
    public boolean isBuilt() {
        return this.isBuilt;
    }
    
    public BuilderIssues getIssues() {
        final BuilderIssues issues = new BuilderIssues();
        if (this.service == null) {
            issues.addIssue(BuilderIssue.Severity.ERROR, "Dataset[\"" + this.parentDs.getName() + "\"] not accessible[\"" + this.urlPath + "\"] due to null service.", this, null);
        }
        if (this.urlPath == null) {
            issues.addIssue(BuilderIssue.Severity.ERROR, ("Dataset[\"" + this.parentDs.getName() + "\"] not accessible[\"" + this.service != null) ? this.service.getName() : "\"] due to null urlPath.", this, null);
        }
        return issues;
    }
    
    public Access build() throws BuilderException {
        this.isBuilt = true;
        return this;
    }
}
