// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.net.URISyntaxException;
import java.net.URI;
import ucar.nc2.constants.FeatureType;
import thredds.catalog.DataFormatType;
import java.util.List;

public interface ThreddsMetadata
{
    List<Documentation> getDocumentation();
    
    List<Keyphrase> getKeyphrases();
    
    List<ProjectName> getProjectNames();
    
    List<Contributor> getCreator();
    
    List<Contributor> getContributor();
    
    List<Contributor> getPublisher();
    
    List<DatePoint> getOtherDates();
    
    DatePoint getCreatedDate();
    
    DatePoint getModifiedDate();
    
    DatePoint getIssuedDate();
    
    DatePoint getValidDate();
    
    DatePoint getAvailableDate();
    
    DatePoint getMetadataCreatedDate();
    
    DatePoint getMetadataModifiedDate();
    
    GeospatialCoverage getGeospatialCoverage();
    
    DateRange getTemporalCoverage();
    
    List<VariableGroup> getVariableGroups();
    
    long getDataSizeInBytes();
    
    DataFormatType getDataFormat();
    
    FeatureType getDataType();
    
    String getCollectionType();
    
    public enum DatePointType
    {
        Created("created"), 
        Modified("modified"), 
        Valid("valid"), 
        Issued("issued"), 
        Available("available"), 
        MetadataCreated("metadataCreated"), 
        MetadataModified("metadataModified"), 
        Other(""), 
        Untyped("");
        
        private final String label;
        
        private DatePointType(final String label) {
            this.label = label;
        }
        
        public static DatePointType getTypeForLabel(final String label) {
            if (label == null || label.equals("")) {
                return DatePointType.Untyped;
            }
            for (final DatePointType dpt : values()) {
                if (dpt.label.equalsIgnoreCase(label)) {
                    return dpt;
                }
            }
            return DatePointType.Other;
        }
        
        @Override
        public String toString() {
            return this.label;
        }
    }
    
    public interface GeospatialRange
    {
        boolean isHorizontal();
        
        double getStart();
        
        double getSize();
        
        double getResolution();
        
        String getUnits();
    }
    
    public interface GeospatialCoverage
    {
        URI getCRS();
        
        boolean isGlobal();
        
        boolean isZPositiveUp();
        
        List<GeospatialRange> getExtent();
    }
    
    public interface Variable
    {
        String getName();
        
        String getDescription();
        
        String getUnits();
        
        String getVocabularyId();
        
        String getVocabularyName();
        
        String getVocabularyAuthorityId();
        
        String getVocabularyAuthorityUrl();
    }
    
    public interface VariableGroup
    {
        String getVocabularyAuthorityId();
        
        String getVocabularyAuthorityUrl();
        
        List<Variable> getVariables();
        
        String getVariableMapUrl();
        
        boolean isEmpty();
    }
    
    public interface Contributor
    {
        String getName();
        
        String getNamingAuthority();
        
        String getRole();
        
        String getEmail();
        
        String getWebPage();
    }
    
    public interface DateRange
    {
        String getStartDateFormat();
        
        String getStartDate();
        
        String getEndDateFormat();
        
        String getEndDate();
        
        String getDuration();
        
        String getResolution();
    }
    
    public interface DatePoint
    {
        String getDate();
        
        String getDateFormat();
        
        boolean isTyped();
        
        String getType();
    }
    
    public interface ProjectName
    {
        String getNamingAuthority();
        
        String getName();
    }
    
    public interface Keyphrase
    {
        String getAuthority();
        
        String getPhrase();
    }
    
    public interface Documentation
    {
        boolean isContainedContent();
        
        String getDocType();
        
        String getContent();
        
        String getTitle();
        
        String getExternalReference();
        
        URI getExternalReferenceAsUri() throws URISyntaxException;
    }
}
