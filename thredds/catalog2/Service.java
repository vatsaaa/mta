// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.util.List;
import java.net.URI;
import thredds.catalog.ServiceType;

public interface Service
{
    String getName();
    
    String getDescription();
    
    ServiceType getType();
    
    URI getBaseUri();
    
    String getSuffix();
    
    List<Property> getProperties();
    
    Property getPropertyByName(final String p0);
    
    List<Service> getServices();
    
    Service getServiceByName(final String p0);
    
    Service findServiceByNameGlobally(final String p0);
}
