// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog2;

import java.util.List;
import thredds.catalog.ServiceType;

public interface CatalogSearchHelper
{
    Service findServiceByName(final String p0);
    
    List<Service> findServiceByType(final ServiceType p0);
    
    DatasetNode findDatasetById(final String p0);
    
    List<Dataset> findAccessibleDatasets();
    
    List<Dataset> findAccessibleDatasetsByType(final ServiceType p0);
    
    List<CatalogRef> findCatalogRefs();
}
