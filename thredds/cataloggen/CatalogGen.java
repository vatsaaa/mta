// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import org.slf4j.LoggerFactory;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import thredds.catalog.parser.jdom.InvCatalogFactory10;
import java.io.OutputStream;
import ucar.nc2.units.DateType;
import java.util.Iterator;
import thredds.cataloggen.config.DatasetSource;
import thredds.catalog.ThreddsMetadata;
import thredds.catalog.InvService;
import thredds.catalog.InvCatalogImpl;
import java.util.Collection;
import java.io.IOException;
import thredds.catalog.InvDataset;
import thredds.cataloggen.config.CatalogGenConfig;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.InvMetadata;
import java.net.URI;
import java.io.InputStream;
import thredds.catalog.MetadataConverterIF;
import thredds.cataloggen.config.CatGenConfigMetadataFactory;
import thredds.catalog.MetadataType;
import java.util.ArrayList;
import java.net.URL;
import java.util.List;
import thredds.catalog.InvCatalogFactory;
import thredds.catalog.InvCatalog;
import org.slf4j.Logger;

public class CatalogGen
{
    private static Logger log;
    private InvCatalog catalog;
    protected InvCatalogFactory catFactory;
    private List catalogRefInfoList;
    
    public List getCatalogRefInfoList() {
        return this.catalogRefInfoList;
    }
    
    public CatalogGen(final URL configDocURL) {
        this.catalog = null;
        this.catFactory = null;
        this.catalogRefInfoList = new ArrayList();
        CatalogGen.log.debug("CatalogGen(URL): create catalog and CatalogGenConfig converter.");
        (this.catFactory = InvCatalogFactory.getDefaultFactory(true)).registerMetadataConverter(MetadataType.CATALOG_GEN_CONFIG.toString(), new CatGenConfigMetadataFactory());
        CatalogGen.log.debug("CatalogGen(URL): reading the config doc <" + configDocURL.toString() + ">.");
        this.catalog = this.catFactory.readXML(configDocURL.toString());
        CatalogGen.log.debug("CatalogGen(URL): done.");
    }
    
    public CatalogGen(final InputStream configDocInputStream, final URL configDocURL) {
        this.catalog = null;
        this.catFactory = null;
        this.catalogRefInfoList = new ArrayList();
        CatalogGen.log.debug("CatalogGen(InputStream): create catalog and CatalogGenConfig converter.");
        (this.catFactory = new InvCatalogFactory("default", true)).registerMetadataConverter(MetadataType.CATALOG_GEN_CONFIG.toString(), new CatGenConfigMetadataFactory());
        CatalogGen.log.debug("CatalogGen(InputStream): reading the config doc <" + configDocURL.toString() + ">.");
        this.catalog = this.catFactory.readXML(configDocInputStream, URI.create(configDocURL.toExternalForm()));
        CatalogGen.log.debug("CatalogGen(InputStream): CatalogGenConfig doc <" + this.catalog.getName() + "> read.");
    }
    
    public boolean isValid(final StringBuilder out) {
        CatalogGen.log.debug("isValid(): start");
        return this.catalog.check(out);
    }
    
    public InvCatalog expand() {
        CatalogGenConfig tmpCgc = null;
        List cgcList = null;
        DatasetSource dss = null;
        final List mdataList = this.findCatGenConfigMdata(this.catalog.getDatasets());
        for (int i = 0; i < mdataList.size(); ++i) {
            final InvMetadata curMdata = mdataList.get(i);
            final InvDatasetImpl curParentDataset = (InvDatasetImpl)curMdata.getParentDataset();
            cgcList = (List)curMdata.getContentObject();
            for (int j = 0; j < cgcList.size(); ++j) {
                tmpCgc = cgcList.get(j);
                CatalogGen.log.debug("expand(): mdata # " + i + " and catGenConfig # " + j + ".");
                dss = tmpCgc.getDatasetSource();
                InvCatalog generatedCat = null;
                try {
                    generatedCat = dss.fullExpand();
                }
                catch (IOException e) {
                    final String tmpMsg = "Error: IOException on fullExpand() of DatasetSource <" + dss.getName() + ">: " + e.getMessage();
                    CatalogGen.log.error("expand(): " + tmpMsg);
                    curParentDataset.addDataset(new InvDatasetImpl(curParentDataset, tmpMsg));
                    break;
                }
                this.catalogRefInfoList.addAll(dss.getCatalogRefInfoList());
                final InvDataset genTopDs = generatedCat.getDatasets().get(0);
                Iterator it = generatedCat.getServices().iterator();
                while (it.hasNext()) {
                    ((InvCatalogImpl)curParentDataset.getParentCatalog()).addService(it.next());
                }
                it = genTopDs.getDatasets().iterator();
                while (it.hasNext()) {
                    final InvDatasetImpl curGenDataset = it.next();
                    if (curGenDataset.hasNestedDatasets()) {
                        final ThreddsMetadata tm = new ThreddsMetadata(false);
                        tm.setServiceName(genTopDs.getServiceDefault().getName());
                        final InvMetadata md = new InvMetadata(genTopDs, null, "http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", "", true, true, null, tm);
                        curGenDataset.getLocalMetadata().addMetadata(md);
                    }
                    else {
                        curGenDataset.getLocalMetadata().setServiceName(genTopDs.getServiceDefault().getName());
                    }
                    curParentDataset.addDataset(curGenDataset);
                }
                curParentDataset.finish();
            }
            CatalogGen.log.debug("expand(): Remove metadata record CGCM(" + i + ").");
            curParentDataset.removeLocalMetadata(curMdata);
        }
        ((InvCatalogImpl)this.catalog).finish();
        return this.catalog;
    }
    
    public void setCatalogExpiresDate(final DateType expiresDate) {
        ((InvCatalogImpl)this.catalog).setExpires(expiresDate);
    }
    
    public void writeCatalog(final String outFileName) throws IOException {
        CatalogGen.log.debug("writeCatalog(): writing catalog to " + outFileName + ".");
        final String invCatDTD = "http://www.unidata.ucar.edu/projects/THREDDS/xml/InvCatalog.0.6.dtd";
        CatalogGen.log.debug("writeCatalog(): set the catalogs DTD (" + invCatDTD + ").");
        if (outFileName == null) {
            CatalogGen.log.debug("writeCatalog(): write catalog to System.out.");
            this.catFactory.writeXML((InvCatalogImpl)this.catalog, System.out);
        }
        else {
            CatalogGen.log.debug("writeCatalog(): try writing catalog to the output file (" + outFileName + ").");
            if (!this.catalog.getVersion().equals("1.0")) {
                this.catFactory.writeXML((InvCatalogImpl)this.catalog, outFileName);
            }
            else {
                final InvCatalogFactory10 fac10 = (InvCatalogFactory10)this.catFactory.getCatalogConverter("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0");
                fac10.setVersion(this.catalog.getVersion());
                final BufferedOutputStream osCat = new BufferedOutputStream(new FileOutputStream(outFileName));
                fac10.writeXML((InvCatalogImpl)this.catalog, osCat);
                osCat.close();
            }
            CatalogGen.log.debug("writeCatalog(): catalog written to " + outFileName + ".");
        }
    }
    
    InvCatalog getCatalog() {
        return this.catalog;
    }
    
    private List findCatGenConfigMdata(final List datasets) {
        final List mdataList = new ArrayList();
        if (datasets == null) {
            return mdataList;
        }
        final Iterator it = datasets.iterator();
        InvDataset curDataset = null;
        while (it.hasNext()) {
            curDataset = it.next();
            final ThreddsMetadata tm = ((InvDatasetImpl)curDataset).getLocalMetadata();
            final Iterator itMdata = tm.getMetadata().iterator();
            InvMetadata curMetadata = null;
            while (itMdata.hasNext()) {
                curMetadata = itMdata.next();
                if (curMetadata.getMetadataType() != null && curMetadata.getMetadataType().equals(MetadataType.CATALOG_GEN_CONFIG.toString())) {
                    mdataList.add(curMetadata);
                }
                else {
                    if (curMetadata.getNamespaceURI() == null || !curMetadata.getNamespaceURI().equals("http://www.unidata.ucar.edu/namespaces/thredds/CatalogGenConfig/v0.5")) {
                        continue;
                    }
                    mdataList.add(curMetadata);
                }
            }
            mdataList.addAll(this.findCatGenConfigMdata(curDataset.getDatasets()));
        }
        return mdataList;
    }
    
    static {
        CatalogGen.log = LoggerFactory.getLogger(CatalogGen.class);
    }
}
