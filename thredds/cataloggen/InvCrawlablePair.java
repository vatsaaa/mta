// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import thredds.catalog.InvDataset;
import thredds.crawlabledataset.CrawlableDataset;

public class InvCrawlablePair
{
    private CrawlableDataset crawlableDataset;
    private InvDataset invDataset;
    
    public InvCrawlablePair(final CrawlableDataset crawlableDataset, final InvDataset invDataset) {
        this.crawlableDataset = crawlableDataset;
        this.invDataset = invDataset;
    }
    
    public CrawlableDataset getCrawlableDataset() {
        return this.crawlableDataset;
    }
    
    public InvDataset getInvDataset() {
        return this.invDataset;
    }
}
