// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import java.util.regex.Matcher;
import java.text.ParseException;
import ucar.nc2.units.DateRange;
import ucar.nc2.units.TimeDuration;
import ucar.nc2.units.DateType;
import thredds.catalog.InvDatasetImpl;
import java.util.regex.Pattern;
import org.slf4j.LoggerFactory;
import java.util.Iterator;
import thredds.catalog.InvDatasetScan;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvDataset;
import org.slf4j.Logger;

public class DatasetEnhancer1
{
    private static Logger logger;
    private DatasetMetadataAdder mdataAdder;
    private boolean applyToLeafNodesOnly;
    
    private DatasetEnhancer1(final DatasetMetadataAdder metadataAdder, final boolean applyToLeafNodesOnly) {
        if (metadataAdder == null) {
            throw new IllegalArgumentException("MetadataAdder must not be null.");
        }
        this.mdataAdder = metadataAdder;
        this.applyToLeafNodesOnly = applyToLeafNodesOnly;
    }
    
    public static DatasetEnhancer1 createDatasetEnhancer(final DatasetMetadataAdder metadataAdder, final boolean applyToLeafNodesOnly) {
        return new DatasetEnhancer1(metadataAdder, applyToLeafNodesOnly);
    }
    
    public static DatasetEnhancer1 createAddTimeCoverageEnhancer(final String dsNameMatchPattern, final String startTimeSubstitutionPattern, final String duration) {
        return new DatasetEnhancer1(new AddTimeCoverageModels(dsNameMatchPattern, startTimeSubstitutionPattern, duration), true);
    }
    
    public static DatasetEnhancer1 createAddIdEnhancer(final String baseID) {
        return new DatasetEnhancer1(new AddId(baseID), false);
    }
    
    public void addMetadata(final InvDataset dataset) {
        boolean doEnhanceChildren;
        boolean doEnhance;
        if (dataset.getClass().equals(InvCatalogRef.class) || dataset.getClass().equals(InvDatasetScan.class)) {
            doEnhanceChildren = false;
            doEnhance = !this.applyToLeafNodesOnly;
        }
        else if (dataset.hasNestedDatasets()) {
            doEnhanceChildren = true;
            doEnhance = !this.applyToLeafNodesOnly;
        }
        else {
            doEnhanceChildren = false;
            doEnhance = true;
        }
        if (doEnhance && !this.mdataAdder.addMetadata(dataset)) {
            DatasetEnhancer1.logger.debug("addMetadata(): failed to enhance dataset <{}>.", dataset.getName());
        }
        if (doEnhanceChildren) {
            final Iterator dsIter = dataset.getDatasets().iterator();
            while (dsIter.hasNext()) {
                this.addMetadata(dsIter.next());
            }
        }
    }
    
    static {
        DatasetEnhancer1.logger = LoggerFactory.getLogger(DatasetEnhancer1.class);
    }
    
    protected static class AddTimeCoverageModels implements DatasetMetadataAdder
    {
        private String substitutionPattern;
        private String duration;
        private Pattern pattern;
        
        public AddTimeCoverageModels(final String matchPattern, final String substitutionPattern, final String duration) {
            this.substitutionPattern = substitutionPattern;
            this.duration = duration;
            this.pattern = Pattern.compile(matchPattern);
        }
        
        public boolean addMetadata(final InvDataset dataset) {
            final Matcher matcher = this.pattern.matcher(dataset.getName());
            if (!matcher.find()) {
                return false;
            }
            final StringBuffer startTime = new StringBuffer();
            matcher.appendReplacement(startTime, this.substitutionPattern);
            startTime.delete(0, matcher.start());
            try {
                ((InvDatasetImpl)dataset).setTimeCoverage(new DateRange(new DateType(startTime.toString(), null, null), null, new TimeDuration(this.duration), null));
            }
            catch (ParseException e) {
                DatasetEnhancer1.logger.debug("Start time <" + startTime.toString() + "> or duration <" + this.duration + "> not parsable: " + e.getMessage());
                return false;
            }
            ((InvDatasetImpl)dataset).finish();
            return true;
        }
    }
    
    protected static class AddId implements DatasetMetadataAdder
    {
        private String baseId;
        
        public AddId(final String baseId) {
            if (baseId == null) {
                throw new IllegalArgumentException("Base Id must not be null.");
            }
            this.baseId = baseId;
        }
        
        public boolean addMetadata(final InvDataset dataset) {
            final InvDataset parentDs = dataset.getParent();
            String curId = (parentDs == null) ? this.baseId : parentDs.getID();
            if (curId == null) {
                curId = this.baseId;
            }
            if (dataset.getName() != null && !dataset.getName().equals("")) {
                curId = curId + "/" + dataset.getName();
            }
            ((InvDatasetImpl)dataset).setID(curId);
            return true;
        }
    }
    
    public interface DatasetMetadataAdder
    {
        boolean addMetadata(final InvDataset p0);
    }
}
