// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import thredds.catalog.InvCatalog;
import org.jdom.Document;
import thredds.catalog.InvCatalogImpl;
import java.io.IOException;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.InvService;
import thredds.crawlabledataset.CrawlableDataset;
import thredds.catalog.InvDatasetScan;

public class DatasetScanCatalogBuilder implements CatalogBuilder
{
    private CatalogBuilder stdCatBuilder;
    
    public DatasetScanCatalogBuilder(final InvDatasetScan datasetScan, final CrawlableDataset collectionCrDs, final InvService service) {
        String baseID = null;
        if (datasetScan.getID() != null) {
            baseID = datasetScan.getID();
        }
        else if (datasetScan.getPath() != null) {
            baseID = datasetScan.getPath();
        }
        this.stdCatBuilder = new StandardCatalogBuilder(datasetScan.getPath(), datasetScan.getName(), collectionCrDs, datasetScan.getFilter(), service, baseID, datasetScan.getIdentifier(), datasetScan.getNamer(), datasetScan.getAddDatasetSize(), datasetScan.getSorter(), datasetScan.getProxyDatasetHandlers(), datasetScan.getChildEnhancerList(), datasetScan, datasetScan.getCatalogRefExpander());
    }
    
    public CrawlableDataset requestCrawlableDataset(final String path) throws IOException {
        return this.stdCatBuilder.requestCrawlableDataset(path);
    }
    
    public InvCatalogImpl generateCatalog(final CrawlableDataset catalogCrDs) throws IOException {
        return this.stdCatBuilder.generateCatalog(catalogCrDs);
    }
    
    public InvCatalogImpl generateProxyDsResolverCatalog(final CrawlableDataset catalogCrDs, final ProxyDatasetHandler pdh) throws IOException {
        final InvCatalogImpl catalog = this.stdCatBuilder.generateProxyDsResolverCatalog(catalogCrDs, pdh);
        return catalog;
    }
    
    public Document generateCatalogAsDocument(final CrawlableDataset catalogCrDs) throws IOException {
        return CatalogBuilderHelper.convertCatalogToDocument(this.generateCatalog(catalogCrDs));
    }
    
    public String generateCatalogAsString(final CrawlableDataset catalogCrDs) throws IOException {
        return CatalogBuilderHelper.convertCatalogToString(this.generateCatalog(catalogCrDs));
    }
}
