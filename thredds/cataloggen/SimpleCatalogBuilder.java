// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import thredds.catalog.InvCatalog;
import org.jdom.Document;
import java.io.IOException;
import thredds.catalog.InvCatalogImpl;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import thredds.catalog.InvService;
import thredds.crawlabledataset.CrawlableDataset;

public class SimpleCatalogBuilder implements CatalogBuilder
{
    private String collectionPath;
    private CrawlableDataset collectionCrDs;
    private InvService service;
    
    public SimpleCatalogBuilder(final String collectionPath, final CrawlableDataset collectionCrDs, final String serviceName, final String serviceTypeName, final String serviceURL) {
        this.collectionPath = ((collectionPath == null) ? collectionCrDs.getName() : collectionPath);
        this.collectionCrDs = collectionCrDs;
        this.service = new InvService(serviceName, serviceTypeName, serviceURL, null, null);
    }
    
    public CrawlableDataset requestCrawlableDataset(final String path) {
        return CatalogBuilderHelper.verifyDescendantDataset(this.collectionCrDs, path, null);
    }
    
    public InvCatalogImpl generateCatalog(final CrawlableDataset catalogCrDs) throws IOException {
        final CollectionLevelScanner scanner = new CollectionLevelScanner(this.collectionPath, this.collectionCrDs, catalogCrDs, null, null, this.service);
        scanner.scan();
        return scanner.generateCatalog();
    }
    
    public InvCatalogImpl generateProxyDsResolverCatalog(final CrawlableDataset catalogCrDs, final ProxyDatasetHandler pdh) throws IOException {
        throw new UnsupportedOperationException("This method not supported by SimpleCatalogBuilder.");
    }
    
    public Document generateCatalogAsDocument(final CrawlableDataset catalogCrDs) throws IOException {
        return CatalogBuilderHelper.convertCatalogToDocument(this.generateCatalog(catalogCrDs));
    }
    
    public String generateCatalogAsString(final CrawlableDataset catalogCrDs) throws IOException {
        return CatalogBuilderHelper.convertCatalogToString(this.generateCatalog(catalogCrDs));
    }
}
