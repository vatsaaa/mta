// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import org.slf4j.LoggerFactory;
import thredds.catalog.InvDataset;
import java.util.List;
import thredds.catalog.InvDatasetImpl;
import java.util.Map;
import thredds.crawlabledataset.CrawlableDatasetSorter;
import thredds.crawlabledataset.CrawlableDatasetLabeler;
import thredds.cataloggen.catalogrefexpander.BooleanCatalogRefExpander;
import thredds.crawlabledataset.sorter.LexigraphicByNameSorter;
import thredds.cataloggen.datasetenhancer.RegExpAndDurationTimeCoverageEnhancer;
import java.util.ArrayList;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import thredds.crawlabledataset.filter.MultiSelectorFilter;
import thredds.crawlabledataset.filter.RegExpMatchOnNameFilter;
import thredds.catalog.InvCatalog;
import java.lang.reflect.InvocationTargetException;
import java.io.IOException;
import thredds.crawlabledataset.CrawlableDatasetFactory;
import thredds.catalog.InvService;
import thredds.crawlabledataset.CrawlableDataset;
import java.io.File;
import org.slf4j.Logger;

public class DirectoryScanner
{
    private static Logger logger;
    private String serviceTitle;
    private File serviceBaseUrlDir;
    private CrawlableDataset collectionCrDs;
    private String prefixPath;
    private boolean createCatalogRefs;
    private InvService service;
    
    public DirectoryScanner(final InvService service, final String serviceTitle, final File serviceBaseUrlDir, final String prefixPath, final boolean createCatalogRefs) {
        this.createCatalogRefs = true;
        this.service = service;
        this.serviceTitle = serviceTitle;
        this.serviceBaseUrlDir = serviceBaseUrlDir;
        try {
            this.collectionCrDs = CrawlableDatasetFactory.createCrawlableDataset(serviceBaseUrlDir.getAbsolutePath(), null, null);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("IOException while creating dataset: " + e.getMessage());
        }
        catch (ClassNotFoundException e2) {
            throw new IllegalArgumentException("Did not find class: " + e2.getMessage());
        }
        catch (NoSuchMethodException e3) {
            throw new IllegalArgumentException("Required constructor not found in class: " + e3.getMessage());
        }
        catch (IllegalAccessException e4) {
            throw new IllegalArgumentException("Did not have necessary access to class: " + e4.getMessage());
        }
        catch (InvocationTargetException e5) {
            throw new IllegalArgumentException("Could not invoke required method in class: " + e5.getMessage());
        }
        catch (InstantiationException e6) {
            throw new IllegalArgumentException("Could not instatiate class: " + e6.getMessage());
        }
        if (!this.collectionCrDs.isCollection()) {
            throw new IllegalArgumentException("Base URL directory is not a directory <" + serviceBaseUrlDir.getAbsolutePath() + ">.");
        }
        this.prefixPath = prefixPath;
        this.createCatalogRefs = createCatalogRefs;
    }
    
    public InvCatalog getDirCatalog(final File directory, final String filterPattern, final boolean sortInIncreasingOrder, final boolean addDatasetSize) {
        return this.getDirCatalog(directory, filterPattern, sortInIncreasingOrder, null, addDatasetSize, null, null, null);
    }
    
    public InvCatalog getDirCatalog(final File directory, final String filterPattern, final boolean sortInIncreasingOrder, final String addIdBase, final boolean addDatasetSize, final String dsNameMatchPattern, final String startTimeSubstitutionPattern, final String duration) {
        CrawlableDataset catalogCrDs;
        try {
            catalogCrDs = CrawlableDatasetFactory.createCrawlableDataset(directory.getAbsolutePath(), null, null);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("IOException while creating dataset: " + e.getMessage());
        }
        catch (ClassNotFoundException e2) {
            throw new IllegalArgumentException("Did not find class: " + e2.getMessage());
        }
        catch (NoSuchMethodException e3) {
            throw new IllegalArgumentException("Required constructor not found in class: " + e3.getMessage());
        }
        catch (IllegalAccessException e4) {
            throw new IllegalArgumentException("Did not have necessary access to class: " + e4.getMessage());
        }
        catch (InvocationTargetException e5) {
            throw new IllegalArgumentException("Could not invoke required method in class: " + e5.getMessage());
        }
        catch (InstantiationException e6) {
            throw new IllegalArgumentException("Could not instatiate class: " + e6.getMessage());
        }
        if (!catalogCrDs.isCollection()) {
            throw new IllegalArgumentException("catalog directory is not a directory <" + this.serviceBaseUrlDir.getAbsolutePath() + ">.");
        }
        return this.getDirCatalog(catalogCrDs, filterPattern, sortInIncreasingOrder, addIdBase, addDatasetSize, dsNameMatchPattern, startTimeSubstitutionPattern, duration);
    }
    
    public InvCatalog getDirCatalog(final CrawlableDataset catalogCrDs, final String filterPattern, final boolean sortInIncreasingOrder, final String addIdBase, final boolean addDatasetSize, final String dsNameMatchPattern, final String startTimeSubstitutionPattern, final String duration) {
        CrawlableDatasetFilter filter = null;
        if (filterPattern != null) {
            final MultiSelectorFilter.Selector selector = new MultiSelectorFilter.Selector(new RegExpMatchOnNameFilter(filterPattern), true, true, false);
            filter = new MultiSelectorFilter(selector);
        }
        else {
            filter = new RegExpMatchOnNameFilter(".*");
        }
        List enhancerList = null;
        if (dsNameMatchPattern != null && startTimeSubstitutionPattern != null && duration != null) {
            enhancerList = new ArrayList();
            enhancerList.add(RegExpAndDurationTimeCoverageEnhancer.getInstanceToMatchOnDatasetName(dsNameMatchPattern, startTimeSubstitutionPattern, duration));
        }
        final CatalogBuilder catBuilder = new StandardCatalogBuilder(this.prefixPath, null, this.collectionCrDs, filter, this.service, addIdBase, null, null, addDatasetSize, new LexigraphicByNameSorter(sortInIncreasingOrder), null, enhancerList, null, new BooleanCatalogRefExpander(!this.createCatalogRefs));
        InvCatalog catalog;
        try {
            catalog = catBuilder.generateCatalog(catalogCrDs);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Could not generate catalog: " + e.getMessage());
        }
        final InvDataset topDs = catalog.getDataset();
        if (this.collectionCrDs.getPath().equals(catalogCrDs.getPath()) && this.serviceTitle != null) {
            DirectoryScanner.logger.warn("getDirCatalog(): top dataset name is null, setting to serviceTitle <" + this.serviceTitle + ">");
            ((InvDatasetImpl)topDs).setName(this.serviceTitle);
        }
        return catalog;
    }
    
    static {
        DirectoryScanner.logger = LoggerFactory.getLogger(DirectoryScanner.class);
    }
}
