// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import java.util.List;
import thredds.catalog.InvService;
import thredds.crawlabledataset.CrawlableDataset;

public interface ProxyDatasetHandler
{
    String getProxyDatasetName();
    
    CrawlableDataset createProxyDataset(final CrawlableDataset p0);
    
    InvService getProxyDatasetService(final CrawlableDataset p0);
    
    int getProxyDatasetLocation(final CrawlableDataset p0, final int p1);
    
    boolean isProxyDatasetResolver();
    
    InvCrawlablePair getActualDataset(final List p0);
    
    String getActualDatasetName(final InvCrawlablePair p0, final String p1);
    
    Object getConfigObject();
}
