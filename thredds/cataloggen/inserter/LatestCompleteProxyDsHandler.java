// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.inserter;

import java.util.Date;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import java.util.Iterator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Collection;
import java.util.ArrayList;
import thredds.cataloggen.InvCrawlablePair;
import java.util.List;
import thredds.crawlabledataset.CrawlableDataset;
import thredds.catalog.InvService;
import thredds.cataloggen.ProxyDatasetHandler;

public class LatestCompleteProxyDsHandler implements ProxyDatasetHandler
{
    private final String latestName;
    private final boolean locateAtTopOrBottom;
    private final InvService service;
    private final boolean isResolver;
    private final long lastModifiedLimit;
    
    public LatestCompleteProxyDsHandler(final String latestName, final boolean locateAtTopOrBottom, final InvService service, final boolean isResolver, final long lastModifiedLimit) {
        this.latestName = latestName;
        this.locateAtTopOrBottom = locateAtTopOrBottom;
        this.service = service;
        this.isResolver = isResolver;
        this.lastModifiedLimit = lastModifiedLimit;
    }
    
    public String getName() {
        return this.latestName;
    }
    
    public boolean isLocateAtTopOrBottom() {
        return this.locateAtTopOrBottom;
    }
    
    public String getServiceName() {
        return this.service.getName();
    }
    
    public long getLastModifiedLimit() {
        return this.lastModifiedLimit;
    }
    
    public String getProxyDatasetName() {
        return this.latestName;
    }
    
    public Object getConfigObject() {
        return null;
    }
    
    public CrawlableDataset createProxyDataset(final CrawlableDataset parent) {
        return new MyCrawlableDataset(parent, this.latestName);
    }
    
    public InvService getProxyDatasetService(final CrawlableDataset parent) {
        return this.service;
    }
    
    public int getProxyDatasetLocation(final CrawlableDataset parent, final int collectionDatasetSize) {
        if (this.locateAtTopOrBottom) {
            return 0;
        }
        return collectionDatasetSize;
    }
    
    public boolean isProxyDatasetResolver() {
        return this.isResolver;
    }
    
    public InvCrawlablePair getActualDataset(final List atomicDsInfo) {
        if (atomicDsInfo == null || atomicDsInfo.isEmpty()) {
            return null;
        }
        final long targetTime = System.currentTimeMillis() - this.lastModifiedLimit * 60L * 1000L;
        final List tmpList = new ArrayList(atomicDsInfo);
        final Iterator it = tmpList.iterator();
        while (it.hasNext()) {
            final InvCrawlablePair curDsInfo = it.next();
            final CrawlableDataset curCrDs = curDsInfo.getCrawlableDataset();
            if (curCrDs.lastModified().getTime() > targetTime) {
                it.remove();
            }
        }
        return Collections.max((Collection<? extends InvCrawlablePair>)tmpList, (Comparator<? super InvCrawlablePair>)new Comparator() {
            public int compare(final Object obj1, final Object obj2) {
                final InvCrawlablePair dsInfo1 = (InvCrawlablePair)obj1;
                final InvCrawlablePair dsInfo2 = (InvCrawlablePair)obj2;
                return dsInfo1.getInvDataset().getName().compareTo(dsInfo2.getInvDataset().getName());
            }
        });
    }
    
    public String getActualDatasetName(final InvCrawlablePair actualDataset, String baseName) {
        if (baseName == null) {
            baseName = "";
        }
        return baseName.equals("") ? "Latest" : ("Latest " + baseName);
    }
    
    private static class MyCrawlableDataset implements CrawlableDataset
    {
        private CrawlableDataset parent;
        private String name;
        
        MyCrawlableDataset(final CrawlableDataset parent, final String name) {
            this.parent = parent;
            this.name = name;
        }
        
        public Object getConfigObject() {
            return null;
        }
        
        public String getPath() {
            return this.parent.getPath() + "/" + this.name;
        }
        
        public String getName() {
            return this.name;
        }
        
        public boolean exists() {
            return true;
        }
        
        public boolean isCollection() {
            return false;
        }
        
        public CrawlableDataset getDescendant(final String childPath) {
            return null;
        }
        
        public CrawlableDataset getParentDataset() {
            return this.parent;
        }
        
        public List listDatasets() {
            return null;
        }
        
        public List listDatasets(final CrawlableDatasetFilter filter) {
            return null;
        }
        
        public long length() {
            return -1L;
        }
        
        public Date lastModified() {
            return null;
        }
    }
}
