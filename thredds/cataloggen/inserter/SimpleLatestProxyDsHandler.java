// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.inserter;

import java.util.Date;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import thredds.cataloggen.InvCrawlablePair;
import java.util.List;
import thredds.crawlabledataset.CrawlableDataset;
import thredds.catalog.InvService;
import thredds.cataloggen.ProxyDatasetHandler;

public class SimpleLatestProxyDsHandler implements ProxyDatasetHandler
{
    private String latestName;
    private boolean locateAtTopOrBottom;
    private InvService service;
    private final boolean isResolver;
    
    public SimpleLatestProxyDsHandler(final String latestName, final boolean locateAtTopOrBottom, final InvService service, final boolean isResolver) {
        this.latestName = latestName;
        this.locateAtTopOrBottom = locateAtTopOrBottom;
        this.service = service;
        this.isResolver = isResolver;
    }
    
    public boolean isLocateAtTopOrBottom() {
        return this.locateAtTopOrBottom;
    }
    
    public Object getConfigObject() {
        return null;
    }
    
    public String getProxyDatasetName() {
        return this.latestName;
    }
    
    public CrawlableDataset createProxyDataset(final CrawlableDataset parent) {
        return new MyCrawlableDataset(parent, this.latestName);
    }
    
    public InvService getProxyDatasetService(final CrawlableDataset parent) {
        return this.service;
    }
    
    public int getProxyDatasetLocation(final CrawlableDataset parent, final int collectionDatasetSize) {
        if (this.locateAtTopOrBottom) {
            return 0;
        }
        return collectionDatasetSize;
    }
    
    public boolean isProxyDatasetResolver() {
        return this.isResolver;
    }
    
    public InvCrawlablePair getActualDataset(final List atomicDsInfo) {
        if (atomicDsInfo == null || atomicDsInfo.isEmpty()) {
            return null;
        }
        return Collections.max((Collection<? extends InvCrawlablePair>)atomicDsInfo, (Comparator<? super InvCrawlablePair>)new Comparator() {
            public int compare(final Object obj1, final Object obj2) {
                final InvCrawlablePair dsInfo1 = (InvCrawlablePair)obj1;
                final InvCrawlablePair dsInfo2 = (InvCrawlablePair)obj2;
                return dsInfo1.getInvDataset().getName().compareTo(dsInfo2.getInvDataset().getName());
            }
        });
    }
    
    public String getActualDatasetName(final InvCrawlablePair actualDataset, String baseName) {
        if (baseName == null) {
            baseName = "";
        }
        return baseName.equals("") ? "Latest" : ("Latest " + baseName);
    }
    
    private static class MyCrawlableDataset implements CrawlableDataset
    {
        private CrawlableDataset parent;
        private String name;
        
        MyCrawlableDataset(final CrawlableDataset parent, final String name) {
            this.parent = parent;
            this.name = name;
        }
        
        public Object getConfigObject() {
            return null;
        }
        
        public String getPath() {
            return this.parent.getPath() + "/" + this.name;
        }
        
        public String getName() {
            return this.name;
        }
        
        public CrawlableDataset getParentDataset() {
            return this.parent;
        }
        
        public boolean exists() {
            return true;
        }
        
        public boolean isCollection() {
            return false;
        }
        
        public CrawlableDataset getDescendant(final String childPath) {
            return null;
        }
        
        public List listDatasets() {
            return null;
        }
        
        public List listDatasets(final CrawlableDatasetFilter filter) {
            return null;
        }
        
        public long length() {
            return -1L;
        }
        
        public Date lastModified() {
            return null;
        }
    }
}
