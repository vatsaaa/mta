// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import thredds.crawlabledataset.CrawlableDataset;
import thredds.catalog.InvDataset;

public interface DatasetEnhancer
{
    boolean addMetadata(final InvDataset p0, final CrawlableDataset p1);
    
    Object getConfigObject();
}
