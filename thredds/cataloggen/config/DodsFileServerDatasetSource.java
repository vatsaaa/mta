// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import java.util.List;
import thredds.catalog.InvCatalog;
import thredds.catalog.InvDataset;
import org.slf4j.Logger;

public class DodsFileServerDatasetSource extends DatasetSource
{
    private static Logger log;
    
    public DodsFileServerDatasetSource() {
        this.type = DatasetSourceType.getType("DodsFileServer");
    }
    
    @Override
    protected InvDataset createDataset(final String datasetLocation, final String prefixUrlPath) {
        throw new UnsupportedOperationException("DodsFileServerDatasetSource class not implemented.");
    }
    
    @Override
    protected InvCatalog createSkeletonCatalog(final String prefixUrlPath) {
        throw new UnsupportedOperationException("DodsFileServerDatasetSource class not implemented.");
    }
    
    @Override
    protected boolean isCollection(final InvDataset dataset) {
        throw new UnsupportedOperationException("DodsFileServerDatasetSource class not implemented.");
    }
    
    @Override
    protected List expandThisLevel(final InvDataset collectionDataset, final String prefixUrlPath) {
        throw new UnsupportedOperationException("DodsFileServerDatasetSource class not implemented.");
    }
    
    static {
        DodsFileServerDatasetSource.log = LoggerFactory.getLogger(DodsFileServerDatasetSource.class);
    }
}
