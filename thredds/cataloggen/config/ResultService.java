// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import java.util.Iterator;
import thredds.catalog.InvProperty;
import thredds.catalog.ServiceType;
import thredds.catalog.InvService;

public class ResultService extends InvService
{
    private String accessPointHeader;
    private boolean isValid;
    private StringBuffer log;
    
    public ResultService(final String name, final ServiceType serviceType, final String base, final String suffix, final String accessPointHeader) {
        super(name, serviceType.toString(), base, suffix, null);
        this.accessPointHeader = null;
        this.isValid = true;
        this.log = new StringBuffer();
        this.accessPointHeader = accessPointHeader;
    }
    
    protected ResultService(final ResultService service) {
        this(service, service.getAccessPointHeader());
    }
    
    public ResultService(final InvService service, final String accessPointHeader) {
        super(service.getName(), service.getServiceType().toString(), service.getBase(), service.getSuffix(), service.getDescription());
        this.accessPointHeader = null;
        this.isValid = true;
        this.log = new StringBuffer();
        for (final InvProperty prop : service.getProperties()) {
            this.addProperty(new InvProperty(prop.getName(), prop.getValue()));
        }
        if (service.getServiceType() == ServiceType.COMPOUND) {
            final Iterator it = service.getServices().iterator();
            while (it.hasNext()) {
                this.addService(it.next());
            }
        }
        this.accessPointHeader = accessPointHeader;
    }
    
    public String getAccessPointHeader() {
        return this.accessPointHeader;
    }
    
    public void setAccessPointHeader(final String accessPointHeader) {
        this.accessPointHeader = accessPointHeader;
    }
    
    protected boolean validate(final StringBuilder out) {
        this.isValid = true;
        if (this.log.length() > 0) {
            out.append(this.log);
        }
        if (this.getAccessPointHeader() == null) {
            this.isValid = false;
            out.append(" ** ResultService (1): a null 'accessPointHeader' is invalid.");
        }
        return this.isValid;
    }
    
    @Override
    public String toString() {
        return "ResultService[" + super.toString() + " accessPointHeader:<" + this.getAccessPointHeader() + ">]";
    }
}
