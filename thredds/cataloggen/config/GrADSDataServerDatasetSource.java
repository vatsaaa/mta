// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import java.util.List;
import thredds.catalog.InvCatalog;
import thredds.catalog.InvDataset;
import org.slf4j.Logger;

public class GrADSDataServerDatasetSource extends DatasetSource
{
    private static Logger logger;
    
    public GrADSDataServerDatasetSource() {
        this.type = DatasetSourceType.getType("GrADSDataServer");
    }
    
    @Override
    protected InvDataset createDataset(final String datasetLocation, final String prefixUrlPath) {
        throw new UnsupportedOperationException("GrADSDataServerDatasetSource class not implemented.");
    }
    
    @Override
    protected InvCatalog createSkeletonCatalog(final String prefixUrlPath) {
        throw new UnsupportedOperationException("GrADSDataServerDatasetSource class not implemented.");
    }
    
    @Override
    protected boolean isCollection(final InvDataset dataset) {
        throw new UnsupportedOperationException("GrADSDataServerDatasetSource class not implemented.");
    }
    
    @Override
    protected List expandThisLevel(final InvDataset collectionDataset, final String prefixUrlPath) {
        throw new UnsupportedOperationException("GrADSDataServerDatasetSource class not implemented.");
    }
    
    static {
        GrADSDataServerDatasetSource.logger = LoggerFactory.getLogger(GrADSDataServerDatasetSource.class);
    }
}
