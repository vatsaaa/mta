// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import java.util.Comparator;
import ucar.nc2.constants.FeatureType;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.InvCatalogImpl;
import java.io.IOException;
import java.util.Iterator;
import thredds.cataloggen.DatasetEnhancer1;
import java.util.ArrayList;
import java.util.List;
import thredds.catalog.InvDataset;
import thredds.catalog.InvCatalog;
import org.slf4j.Logger;

public abstract class DatasetSource
{
    private static Logger logger;
    protected InvCatalog resultingCatalog;
    protected InvDataset accessPointDataset;
    protected List catalogRefInfoList;
    private String name;
    protected DatasetSourceType type;
    private DatasetSourceStructure structure;
    private boolean flatten;
    private String accessPoint;
    private String prefixUrlPath;
    protected boolean createCatalogRefs;
    private ResultService resultService;
    protected List datasetNamerList;
    protected List datasetFilterList;
    protected DatasetSorter datasetSorter;
    protected List datasetEnhancerList;
    protected boolean addDatasetSize;
    protected boolean isValid;
    protected StringBuffer msgLog;
    private volatile int hashCode;
    
    protected DatasetSource() {
        this.catalogRefInfoList = new ArrayList();
        this.flatten = false;
        this.createCatalogRefs = false;
        this.datasetNamerList = new ArrayList();
        this.datasetFilterList = new ArrayList();
        this.datasetSorter = null;
        this.datasetEnhancerList = new ArrayList();
        this.isValid = true;
        this.msgLog = new StringBuffer();
        this.hashCode = 0;
    }
    
    public static final DatasetSource newDatasetSource(final String name, final DatasetSourceType type, final DatasetSourceStructure structure, final String accessPoint, final ResultService resultService) {
        if (type == null) {
            final String tmpMsg = "DatasetSource type cannot be null";
            DatasetSource.logger.error("newDatasetSource(): " + tmpMsg);
            throw new IllegalArgumentException(tmpMsg);
        }
        DatasetSource tmpDsSource = null;
        if (type == DatasetSourceType.getType("Local")) {
            tmpDsSource = new LocalDatasetSource();
        }
        else if (type == DatasetSourceType.getType("DodsDir")) {
            tmpDsSource = new DodsDirDatasetSource();
        }
        else if (type == DatasetSourceType.getType("DodsFileServer")) {
            tmpDsSource = new DodsFileServerDatasetSource();
        }
        else {
            if (type != DatasetSourceType.getType("GrADSDataServer")) {
                final String tmpMsg2 = "Unsupported DatasetSource type <" + type.toString() + ">.";
                DatasetSource.logger.error("newDatasetSource(): " + tmpMsg2);
                throw new IllegalArgumentException(tmpMsg2);
            }
            tmpDsSource = new GrADSDataServerDatasetSource();
        }
        tmpDsSource.setName(name);
        tmpDsSource.setStructure(structure);
        tmpDsSource.setAccessPoint(accessPoint);
        tmpDsSource.setResultService(resultService);
        DatasetSource.logger.debug("DatasetSource(): constructor done.");
        final StringBuilder log = new StringBuilder();
        if (tmpDsSource.validate(log)) {
            DatasetSource.logger.debug("DatasetSource(): new DatasetSource is valid: {}", log.toString());
        }
        else {
            DatasetSource.logger.debug("DatasetSource(): new DatasetSource is invalid: {}", log.toString());
        }
        return tmpDsSource;
    }
    
    public InvCatalog getResultingCatalog() {
        return this.resultingCatalog;
    }
    
    public List getCatalogRefInfoList() {
        return this.catalogRefInfoList;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public DatasetSourceType getType() {
        return this.type;
    }
    
    public DatasetSourceStructure getStructure() {
        return this.structure;
    }
    
    public void setStructure(final DatasetSourceStructure structure) {
        this.structure = structure;
        if (this.structure == DatasetSourceStructure.FLAT) {
            this.flatten = true;
        }
        else {
            this.flatten = false;
        }
    }
    
    public boolean isFlatten() {
        return this.flatten;
    }
    
    public String getAccessPoint() {
        return this.accessPoint;
    }
    
    public void setAccessPoint(final String accessPoint) {
        this.accessPoint = accessPoint;
    }
    
    public String getPrefixUrlPath() {
        return this.prefixUrlPath;
    }
    
    public void setPrefixUrlPath(final String prefixUrlPath) {
        this.prefixUrlPath = prefixUrlPath;
    }
    
    public ResultService getResultService() {
        return this.resultService;
    }
    
    public void setResultService(final ResultService resultService) {
        this.resultService = resultService;
    }
    
    public boolean isCreateCatalogRefs() {
        return this.createCatalogRefs;
    }
    
    public void setCreateCatalogRefs(final boolean createCatalogRefs) {
        this.createCatalogRefs = createCatalogRefs;
    }
    
    public List getDatasetNamerList() {
        return this.datasetNamerList;
    }
    
    public void addDatasetNamer(final DatasetNamer datasetNamer) {
        this.datasetNamerList.add(datasetNamer);
    }
    
    public List getDatasetFilterList() {
        return this.datasetFilterList;
    }
    
    public void addDatasetFilter(final DatasetFilter datasetFilter) {
        this.datasetFilterList.add(datasetFilter);
    }
    
    public DatasetSorter getDatasetSorter() {
        return this.datasetSorter;
    }
    
    public void setDatasetSorter(final DatasetSorter datasetSorter) {
        this.datasetSorter = datasetSorter;
    }
    
    public List getDatasetEnhancerList() {
        return this.datasetEnhancerList;
    }
    
    public void addDatasetEnhancer(final DatasetEnhancer1 dsEnhancer) {
        if (this.datasetEnhancerList == null) {
            this.datasetEnhancerList = new ArrayList();
        }
        this.datasetEnhancerList.add(dsEnhancer);
    }
    
    public boolean isAddDatasetSize() {
        return this.addDatasetSize;
    }
    
    public void setAddDatasetSize(final boolean addDatasetSize) {
        this.addDatasetSize = addDatasetSize;
    }
    
    public boolean validate(final StringBuilder out) {
        this.isValid = true;
        if (this.msgLog.length() > 0) {
            out.append(this.msgLog);
        }
        if (this.getName() == null) {
            this.isValid = false;
            out.append(" ** DatasetSource (5): null value for name is not valid.");
        }
        if (this.getType() == null) {
            this.isValid = false;
            out.append(" ** DatasetSource (6): null value for type is not valid (set with bad string?).");
        }
        if (this.getStructure() == null) {
            this.isValid = false;
            out.append(" ** DatasetSource (7): null value for structure is not valid (set with bad string?).");
        }
        if (this.getResultService() != null) {
            this.isValid &= this.getResultService().validate(out);
        }
        final Iterator dsnIter = this.getDatasetNamerList().iterator();
        while (dsnIter.hasNext()) {
            this.isValid &= dsnIter.next().validate(out);
        }
        final Iterator dsfIter = this.getDatasetFilterList().iterator();
        while (dsfIter.hasNext()) {
            this.isValid &= dsfIter.next().validate(out);
        }
        return this.isValid;
    }
    
    @Override
    public String toString() {
        final StringBuffer tmp = new StringBuffer();
        tmp.append("DatasetSource[name:<" + this.getName() + "> type:<" + this.getType() + "> structure:<" + this.getStructure() + "> accessPoint:<" + this.getAccessPoint() + "> and children - " + "ResultService(" + this.getResultService().getName() + ") - " + "DatasetNamer(" + this.getDatasetNamerList().size() + ") - " + "DatasetFilter(" + this.getDatasetFilterList().size() + ")]");
        return tmp.toString();
    }
    
    public InvDataset expand() throws IOException {
        this.resultingCatalog = this.createSkeletonCatalog(this.prefixUrlPath);
        this.accessPointDataset = this.resultingCatalog.getDatasets().get(0);
        if (!this.isCollection(this.accessPointDataset)) {
            final String tmpMsg = "The access point dataset <" + this.accessPointDataset.getName() + "> must be a collection dataset.";
            DatasetSource.logger.warn("expand(): {}", tmpMsg);
            throw new IOException(tmpMsg);
        }
        this.expandRecursive(this.accessPointDataset);
        ((InvCatalogImpl)this.resultingCatalog).finish();
        this.recursivelyRemoveEmptyCollectionDatasets(this.accessPointDataset);
        return this.accessPointDataset;
    }
    
    public InvDataset expand(final List accessPoints) throws IOException {
        this.resultingCatalog = this.createSkeletonCatalog(this.prefixUrlPath);
        this.accessPointDataset = this.resultingCatalog.getDatasets().get(0);
        final Iterator it = accessPoints.iterator();
        while (it.hasNext()) {
            final InvDataset curDs = this.createDataset(it.next(), this.prefixUrlPath);
            this.expandRecursiveCollection(this.accessPointDataset, curDs);
        }
        ((InvCatalogImpl)this.resultingCatalog).finish();
        this.recursivelyRemoveEmptyCollectionDatasets(this.accessPointDataset);
        return this.accessPointDataset;
    }
    
    private void expandRecursive(final InvDataset collectionDataset) {
        final List listAllDatasets = this.expandThisLevel(collectionDataset, this.prefixUrlPath);
        InvDataset curChildDs = null;
        final Iterator i = listAllDatasets.iterator();
        while (i.hasNext()) {
            curChildDs = i.next();
            if (!DatasetFilter.acceptDatasetByFilterGroup(this.getDatasetFilterList(), curChildDs, this.isCollection(curChildDs))) {
                continue;
            }
            if (this.isCollection(curChildDs)) {
                this.expandRecursiveCollection(collectionDataset, curChildDs);
            }
            else if (!this.isFlatten()) {
                ((InvDatasetImpl)collectionDataset).addDataset((InvDatasetImpl)curChildDs);
            }
            else {
                ((InvDatasetImpl)this.accessPointDataset).addDataset((InvDatasetImpl)curChildDs);
            }
        }
    }
    
    private void expandRecursiveCollection(final InvDataset collectionDataset, final InvDataset childDs) {
        if (!this.createCatalogRefs) {
            if (!this.isFlatten()) {
                ((InvDatasetImpl)collectionDataset).addDataset((InvDatasetImpl)childDs);
            }
            this.expandRecursive(childDs);
        }
        else {
            final String title = childDs.getName();
            final String docName = (childDs.getName() == null || childDs.getName().equals("")) ? "catalog.xml" : (childDs.getName() + "/catalog.xml");
            final InvCatalogRef curCatRef = new InvCatalogRef((InvDatasetImpl)collectionDataset, title, docName);
            ((InvDatasetImpl)collectionDataset).addDataset(curCatRef);
            final DatasetSource catRefDsSrc = newDatasetSource(childDs.getName(), this.getType(), this.getStructure(), childDs.getName(), new ResultService(this.getResultService()));
            this.catalogRefInfoList.add(new CatalogRefInfo(title, docName, childDs, catRefDsSrc));
        }
    }
    
    public InvCatalog fullExpand() throws IOException {
        DatasetSource.logger.debug("fullExpand(): expanding DatasetSource named \"{}\"", this.getName());
        final InvDataset topDs = this.expand();
        final InvCatalog generatedCat = topDs.getParentCatalog();
        for (final DatasetEnhancer1 dsE : this.getDatasetEnhancerList()) {
            dsE.addMetadata(topDs);
        }
        DatasetSource.logger.debug("fullExpand(): naming the datasets.");
        this.nameDatasets((InvDatasetImpl)topDs);
        DatasetSource.logger.debug("fullExpand(): sorting the datasets.");
        this.sortDatasets(topDs);
        ((InvCatalogImpl)generatedCat).finish();
        return generatedCat;
    }
    
    protected abstract InvDataset createDataset(final String p0, final String p1) throws IOException;
    
    protected abstract boolean isCollection(final InvDataset p0);
    
    protected abstract InvCatalog createSkeletonCatalog(final String p0) throws IOException;
    
    protected abstract List expandThisLevel(final InvDataset p0, final String p1);
    
    private void nameDatasets(final InvDatasetImpl datasetContainer) {
        if (this.getDatasetNamerList().isEmpty()) {
            return;
        }
        if (this.isFlatten()) {
            DatasetSource.logger.debug("nameDatasets(): structure is FLAT calling nameDatasetList()");
            this.nameDatasetList(datasetContainer);
        }
        else {
            DatasetSource.logger.debug("nameDatasets(): structure is DIRECTORY_TREE calling nameDatasetTree() on each dataset in dataset container");
            InvDatasetImpl curDs = null;
            for (int j = 0; j < datasetContainer.getDatasets().size(); ++j) {
                curDs = datasetContainer.getDatasets().get(j);
                this.nameDatasetTree(curDs);
            }
        }
    }
    
    private void nameDatasetList(final InvDatasetImpl dataset) {
        final InvDatasetImpl namedDs = new InvDatasetImpl(dataset, "nameDatastList() temp dataset", null, null, null);
        dataset.addDataset(namedDs);
        DatasetNamer curNamer = null;
        for (int i = 0; i < this.datasetNamerList.size(); ++i) {
            curNamer = this.datasetNamerList.get(i);
            DatasetSource.logger.debug("nameDatasetList(): trying namer ({})", curNamer.getName());
            InvDatasetImpl addLevelDs = null;
            if (curNamer.getAddLevel()) {
                addLevelDs = new InvDatasetImpl(null, curNamer.getName(), null, null, null);
            }
            InvDatasetImpl curDs = null;
            final Iterator dsIter = dataset.getDatasets().iterator();
            while (dsIter.hasNext()) {
                curDs = dsIter.next();
                DatasetSource.logger.debug("nameDatasetList(): try namer on this ds ({}-{})", curDs.getName(), curDs.getUrlPath());
                if (curNamer.nameDataset(curDs)) {
                    DatasetSource.logger.debug("nameDatasetList(): ds named ({})", curDs.getName());
                    if (curNamer.getAddLevel()) {
                        addLevelDs.addDataset(curDs);
                    }
                    else {
                        namedDs.addDataset(curDs);
                    }
                    dsIter.remove();
                }
            }
            if (curNamer.getAddLevel() && addLevelDs.hasNestedDatasets()) {
                namedDs.addDataset(addLevelDs);
            }
        }
        namedDs.finish();
        if (DatasetSource.logger.isDebugEnabled()) {
            DatasetSource.logger.debug("nameDatasetList(): number of unnamed datasets is " + dataset.getDatasets().size() + ".");
            DatasetSource.logger.debug("nameDatasetList(): add named datasets back to container.");
        }
        for (int i = 0; i < namedDs.getDatasets().size(); ++i) {
            dataset.addDataset(namedDs.getDatasets().get(i));
        }
        dataset.removeDataset(namedDs);
    }
    
    private void nameDatasetTree(final InvDatasetImpl dataset) {
        if (dataset.getName().equals("") || !dataset.hasAccess()) {
            DatasetSource.logger.debug("nameDatasetTree(): naming dataset ({})...", dataset.getUrlPath());
            DatasetNamer dsN = null;
            for (int i = 0; i < this.datasetNamerList.size(); ++i) {
                dsN = this.datasetNamerList.get(i);
                if (dsN.nameDataset(dataset)) {
                    DatasetSource.logger.debug("nameDatasetTree(): ... used namer ({})", dsN.getName());
                    break;
                }
            }
        }
        InvDatasetImpl curDs = null;
        for (int j = 0; j < dataset.getDatasets().size(); ++j) {
            curDs = dataset.getDatasets().get(j);
            DatasetSource.logger.debug("nameDatasetTree(): recurse to name child dataset ({})", curDs.getUrlPath());
            this.nameDatasetTree(curDs);
        }
    }
    
    private void sortDatasets(final InvDataset dataset) {
        if (this.getDatasetSorter() == null) {
            final DatasetSorter defaultSorter = new DatasetSorter(new Comparator() {
                public int compare(final Object obj1, final Object obj2) {
                    final InvDataset ds1 = (InvDataset)obj1;
                    final InvDataset ds2 = (InvDataset)obj2;
                    return -ds1.getName().compareTo(ds2.getName());
                }
            });
            defaultSorter.sortNestedDatasets(dataset);
        }
        else {
            this.getDatasetSorter().sortNestedDatasets(dataset);
        }
    }
    
    private void recursivelyRemoveEmptyCollectionDatasets(final InvDataset parentDataset) {
        InvDataset curDs = null;
        final Iterator it = parentDataset.getDatasets().iterator();
        while (it.hasNext()) {
            curDs = it.next();
            if (curDs.hasAccess()) {
                continue;
            }
            if (curDs instanceof InvCatalogRef) {
                continue;
            }
            if (curDs.hasNestedDatasets()) {
                this.recursivelyRemoveEmptyCollectionDatasets(curDs);
            }
            else {
                it.remove();
            }
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DatasetSource)) {
            return false;
        }
        final DatasetSource datasetSource = (DatasetSource)o;
        if (this.createCatalogRefs != datasetSource.createCatalogRefs) {
            return false;
        }
        if (this.flatten != datasetSource.flatten) {
            return false;
        }
        Label_0080: {
            if (this.accessPoint != null) {
                if (this.accessPoint.equals(datasetSource.accessPoint)) {
                    break Label_0080;
                }
            }
            else if (datasetSource.accessPoint == null) {
                break Label_0080;
            }
            return false;
        }
        Label_0113: {
            if (this.datasetFilterList != null) {
                if (this.datasetFilterList.equals(datasetSource.datasetFilterList)) {
                    break Label_0113;
                }
            }
            else if (datasetSource.datasetFilterList == null) {
                break Label_0113;
            }
            return false;
        }
        Label_0146: {
            if (this.datasetNamerList != null) {
                if (this.datasetNamerList.equals(datasetSource.datasetNamerList)) {
                    break Label_0146;
                }
            }
            else if (datasetSource.datasetNamerList == null) {
                break Label_0146;
            }
            return false;
        }
        Label_0179: {
            if (this.name != null) {
                if (this.name.equals(datasetSource.name)) {
                    break Label_0179;
                }
            }
            else if (datasetSource.name == null) {
                break Label_0179;
            }
            return false;
        }
        Label_0212: {
            if (this.resultService != null) {
                if (this.resultService.equals(datasetSource.resultService)) {
                    break Label_0212;
                }
            }
            else if (datasetSource.resultService == null) {
                break Label_0212;
            }
            return false;
        }
        if (this.type != null) {
            if (this.type.equals(datasetSource.type)) {
                return true;
            }
        }
        else if (datasetSource.type == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = ((this.name != null) ? this.name.hashCode() : 0);
            result = 29 * result + ((this.type != null) ? this.type.hashCode() : 0);
            result = 29 * result + (this.flatten ? 1 : 0);
            result = 29 * result + ((this.accessPoint != null) ? this.accessPoint.hashCode() : 0);
            result = 29 * result + (this.createCatalogRefs ? 1 : 0);
            result = 29 * result + ((this.resultService != null) ? this.resultService.hashCode() : 0);
            result = 29 * result + ((this.datasetNamerList != null) ? this.datasetNamerList.hashCode() : 0);
            result = 29 * result + ((this.datasetFilterList != null) ? this.datasetFilterList.hashCode() : 0);
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    static {
        DatasetSource.logger = LoggerFactory.getLogger(DatasetSource.class);
    }
}
