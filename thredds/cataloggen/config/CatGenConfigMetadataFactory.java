// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import thredds.catalog.ServiceType;
import java.util.List;
import java.util.Iterator;
import org.jdom.Content;
import java.util.ArrayList;
import java.net.URI;
import org.jdom.Element;
import java.net.MalformedURLException;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import org.jdom.JDOMException;
import java.io.IOException;
import org.jdom.input.SAXBuilder;
import thredds.catalog.InvDataset;
import org.jdom.Namespace;
import org.slf4j.Logger;
import thredds.catalog.MetadataConverterIF;

public class CatGenConfigMetadataFactory implements MetadataConverterIF
{
    private static Logger log;
    private static boolean showParsedXML;
    private static boolean debug;
    private static final Namespace CATALOG_GEN_CONFIG_NAMESPACE_0_5;
    
    public CatGenConfigMetadataFactory() {
        CatGenConfigMetadataFactory.log.debug("CatGenConfigMetadataFactory(): .");
    }
    
    private Object readMetadataContentFromURL(final InvDataset dataset, final String urlString) throws MalformedURLException, IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder(true);
            doc = builder.build(urlString);
        }
        catch (JDOMException e) {
            CatGenConfigMetadataFactory.log.error("CatGenConfigMetadataFactory parsing error= \n" + e.getMessage());
            throw new IOException("CatGenConfigMetadataFactory parsing error= " + e.getMessage());
        }
        if (CatGenConfigMetadataFactory.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter(Format.getPrettyFormat());
            System.out.println("*** catalog/showParsedXML = \n" + xmlOut.outputString(doc) + "\n*******");
        }
        return this.readMetadataContentJdom(dataset, doc.getRootElement());
    }
    
    public Object readMetadataContent(final InvDataset dataset, final Element mdataElement) {
        CatGenConfigMetadataFactory.log.debug("readMetadataContent(): .");
        return this.readMetadataContentJdom(dataset, mdataElement);
    }
    
    public Object readMetadataContentFromURL(final InvDataset dataset, final URI uri) throws IOException {
        return null;
    }
    
    public void addMetadataContent(final Element mdataJdomElement, final Object contentObject) {
        final ArrayList catGenConfigList = (ArrayList)contentObject;
        for (final CatalogGenConfig cgc : catGenConfigList) {
            mdataJdomElement.addContent(this.createCatGenConfigElement(cgc));
        }
    }
    
    public boolean validateMetadataContent(final Object contentObject, final StringBuilder out) {
        boolean ok = true;
        final ArrayList catGenConfigList = (ArrayList)contentObject;
        for (final CatalogGenConfig catGenConf : catGenConfigList) {
            ok &= catGenConf.validate(out);
        }
        return ok;
    }
    
    private Object readMetadataContentJdom(final InvDataset dataset, final Element mdataElement) {
        final Namespace catGenConfigNamespace = null;
        final ArrayList catGenConfigList = new ArrayList();
        Iterator iter = mdataElement.getChildren("catalogGenConfig", CatGenConfigMetadataFactory.CATALOG_GEN_CONFIG_NAMESPACE_0_5).iterator();
        if (!iter.hasNext()) {
            iter = mdataElement.getChildren("catalogGenConfig", mdataElement.getNamespace()).iterator();
        }
        while (iter.hasNext()) {
            final Element catGenConfigElement = iter.next();
            if (CatGenConfigMetadataFactory.debug) {
                CatGenConfigMetadataFactory.log.debug("readMetadataContent=" + catGenConfigElement);
            }
            catGenConfigList.add(this.readCatGenConfigElement(dataset, catGenConfigElement));
        }
        return catGenConfigList;
    }
    
    private CatalogGenConfig readCatGenConfigElement(final InvDataset parentDataset, final Element catGenConfElement) {
        final String type = catGenConfElement.getAttributeValue("type");
        final CatalogGenConfig catGenConf = new CatalogGenConfig(parentDataset, type);
        final List list = catGenConfElement.getChildren("datasetSource", catGenConfElement.getNamespace());
        for (int i = 0; i < list.size(); ++i) {
            final Element dsSourceElement = list.get(i);
            catGenConf.setDatasetSource(this.readDatasetSourceElement(parentDataset, dsSourceElement));
        }
        return catGenConf;
    }
    
    private DatasetSource readDatasetSourceElement(final InvDataset parentDataset, final Element dsSourceElement) {
        final String name = dsSourceElement.getAttributeValue("name");
        final String type = dsSourceElement.getAttributeValue("type");
        final String structure = dsSourceElement.getAttributeValue("structure");
        final String accessPoint = dsSourceElement.getAttributeValue("accessPoint");
        final String createCatalogRefs = dsSourceElement.getAttributeValue("createCatalogRefs");
        final Element resultServiceElement = dsSourceElement.getChild("resultService", dsSourceElement.getNamespace());
        final ResultService resultService = this.readResultServiceElement(parentDataset, resultServiceElement);
        final DatasetSource dsSource = DatasetSource.newDatasetSource(name, DatasetSourceType.getType(type), DatasetSourceStructure.getStructure(structure), accessPoint, resultService);
        if (createCatalogRefs != null) {
            dsSource.setCreateCatalogRefs(Boolean.valueOf(createCatalogRefs));
        }
        List list = dsSourceElement.getChildren("datasetNamer", dsSourceElement.getNamespace());
        for (int i = 0; i < list.size(); ++i) {
            final Element dsNamerElement = list.get(i);
            dsSource.addDatasetNamer(this.readDatasetNamerElement(parentDataset, dsNamerElement));
        }
        list = dsSourceElement.getChildren("datasetFilter", dsSourceElement.getNamespace());
        for (int i = 0; i < list.size(); ++i) {
            final Element dsFilterElement = list.get(i);
            dsSource.addDatasetFilter(this.readDatasetFilterElement(dsSource, dsFilterElement));
        }
        return dsSource;
    }
    
    private DatasetNamer readDatasetNamerElement(final InvDataset parentDataset, final Element dsNamerElement) {
        final String name = dsNamerElement.getAttributeValue("name");
        final String addLevel = dsNamerElement.getAttributeValue("addLevel");
        final String type = dsNamerElement.getAttributeValue("type");
        final String matchPattern = dsNamerElement.getAttributeValue("matchPattern");
        final String substitutePattern = dsNamerElement.getAttributeValue("substitutePattern");
        final String attribContainer = dsNamerElement.getAttributeValue("attribContainer");
        final String attribName = dsNamerElement.getAttributeValue("attribName");
        final DatasetNamer dsNamer = new DatasetNamer(parentDataset, name, addLevel, type, matchPattern, substitutePattern, attribContainer, attribName);
        return dsNamer;
    }
    
    private DatasetFilter readDatasetFilterElement(final DatasetSource parentDatasetSource, final Element dsFilterElement) {
        final String name = dsFilterElement.getAttributeValue("name");
        final String type = dsFilterElement.getAttributeValue("type");
        final String matchPattern = dsFilterElement.getAttributeValue("matchPattern");
        final DatasetFilter dsFilter = new DatasetFilter(parentDatasetSource, name, DatasetFilter.Type.getType(type), matchPattern);
        final String matchPatternTarget = dsFilterElement.getAttributeValue("matchPatternTarget");
        dsFilter.setMatchPatternTarget(matchPatternTarget);
        if (dsFilterElement.getAttributeValue("applyToCollectionDatasets") != null) {
            final boolean applyToCollectionDatasets = Boolean.valueOf(dsFilterElement.getAttributeValue("applyToCollectionDatasets"));
            dsFilter.setApplyToCollectionDatasets(applyToCollectionDatasets);
        }
        if (dsFilterElement.getAttributeValue("applyToAtomicDatasets") != null) {
            final boolean applyToAtomicDatasets = Boolean.valueOf(dsFilterElement.getAttributeValue("applyToAtomicDatasets"));
            dsFilter.setApplyToAtomicDatasets(applyToAtomicDatasets);
        }
        if (dsFilterElement.getAttributeValue("rejectMatchingDatasets") != null) {
            final boolean rejectMatchingDatasets = Boolean.valueOf(dsFilterElement.getAttributeValue("rejectMatchingDatasets"));
            dsFilter.setRejectMatchingDatasets(rejectMatchingDatasets);
        }
        return dsFilter;
    }
    
    private ResultService readResultServiceElement(final InvDataset parentDataset, final Element resultServiceElement) {
        final String name = resultServiceElement.getAttributeValue("name");
        final String serviceType = resultServiceElement.getAttributeValue("serviceType");
        final String base = resultServiceElement.getAttributeValue("base");
        final String suffix = resultServiceElement.getAttributeValue("suffix");
        final String accessPointHeader = resultServiceElement.getAttributeValue("accessPointHeader");
        return new ResultService(name, ServiceType.getType(serviceType), base, suffix, accessPointHeader);
    }
    
    private Element createCatGenConfigElement(final CatalogGenConfig cgc) {
        final Element cgcElem = new Element("catalogGenConfig", CatGenConfigMetadataFactory.CATALOG_GEN_CONFIG_NAMESPACE_0_5);
        if (cgc != null) {
            if (cgc.getType() != null) {
                cgcElem.setAttribute("type", cgc.getType().toString());
            }
            final DatasetSource dsSource = cgc.getDatasetSource();
            cgcElem.addContent(this.createDatasetSourceElement(dsSource));
        }
        return cgcElem;
    }
    
    private Element createDatasetSourceElement(final DatasetSource dsSource) {
        final Element dssElem = new Element("datasetSource", CatGenConfigMetadataFactory.CATALOG_GEN_CONFIG_NAMESPACE_0_5);
        if (dsSource != null) {
            if (dsSource.getName() != null) {
                dssElem.setAttribute("name", dsSource.getName());
            }
            if (dsSource.getType() != null) {
                dssElem.setAttribute("type", dsSource.getType().toString());
            }
            if (dsSource.getStructure() != null) {
                dssElem.setAttribute("structure", dsSource.getStructure().toString());
            }
            if (dsSource.getAccessPoint() != null) {
                dssElem.setAttribute("accessPoint", dsSource.getAccessPoint());
            }
            dssElem.setAttribute("createCatalogRefs", Boolean.toString(dsSource.isCreateCatalogRefs()));
            final ResultService rs = dsSource.getResultService();
            dssElem.addContent(this.createResultServiceElement(rs));
            List list = dsSource.getDatasetNamerList();
            for (int j = 0; j < list.size(); ++j) {
                final DatasetNamer dsNamer = list.get(j);
                dssElem.addContent(this.createDatasetNamerElement(dsNamer));
            }
            list = dsSource.getDatasetFilterList();
            for (int j = 0; j < list.size(); ++j) {
                final DatasetFilter dsFilter = list.get(j);
                dssElem.addContent(this.createDatasetFilterElement(dsFilter));
            }
        }
        return dssElem;
    }
    
    private Element createDatasetNamerElement(final DatasetNamer dsNamer) {
        final Element dsnElem = new Element("datasetNamer", CatGenConfigMetadataFactory.CATALOG_GEN_CONFIG_NAMESPACE_0_5);
        if (dsNamer != null) {
            if (dsNamer.getName() != null) {
                dsnElem.setAttribute("name", dsNamer.getName());
            }
            dsnElem.setAttribute("addLevel", Boolean.toString(dsNamer.getAddLevel()));
            if (dsNamer.getType() != null) {
                dsnElem.setAttribute("type", dsNamer.getType().toString());
            }
            if (dsNamer.getMatchPattern() != null) {
                dsnElem.setAttribute("matchPattern", dsNamer.getMatchPattern());
            }
            if (dsNamer.getSubstitutePattern() != null) {
                dsnElem.setAttribute("substitutePattern", dsNamer.getSubstitutePattern());
            }
            if (dsNamer.getAttribContainer() != null) {
                dsnElem.setAttribute("attribContainer", dsNamer.getAttribContainer());
            }
            if (dsNamer.getAttribName() != null) {
                dsnElem.setAttribute("attribName", dsNamer.getAttribName());
            }
        }
        return dsnElem;
    }
    
    private Element createDatasetFilterElement(final DatasetFilter dsFilter) {
        final Element dsfElem = new Element("datasetFilter", CatGenConfigMetadataFactory.CATALOG_GEN_CONFIG_NAMESPACE_0_5);
        if (dsFilter != null) {
            if (dsFilter.getName() != null) {
                dsfElem.setAttribute("name", dsFilter.getName());
            }
            if (dsFilter.getType() != null) {
                dsfElem.setAttribute("type", dsFilter.getType().toString());
            }
            if (dsFilter.getMatchPattern() != null) {
                dsfElem.setAttribute("matchPattern", dsFilter.getMatchPattern());
            }
            if (dsFilter.getMatchPatternTarget() != null) {
                dsfElem.setAttribute("matchPatternTarget", dsFilter.getMatchPatternTarget());
            }
            dsfElem.setAttribute("applyToCollectionDatasets", String.valueOf(dsFilter.isApplyToCollectionDatasets()));
            dsfElem.setAttribute("applyToAtomicDatasets", String.valueOf(dsFilter.isApplyToAtomicDatasets()));
            dsfElem.setAttribute("rejectMatchingDatasets", String.valueOf(dsFilter.isRejectMatchingDatasets()));
        }
        return dsfElem;
    }
    
    private Element createResultServiceElement(final ResultService resultService) {
        final Element rsElem = new Element("resultService", CatGenConfigMetadataFactory.CATALOG_GEN_CONFIG_NAMESPACE_0_5);
        if (resultService != null) {
            if (resultService.getName() != null) {
                rsElem.setAttribute("name", resultService.getName());
            }
            if (resultService.getServiceType() != null) {
                rsElem.setAttribute("serviceType", resultService.getServiceType().toString());
            }
            if (resultService.getBase() != null) {
                rsElem.setAttribute("base", resultService.getBase());
            }
            if (resultService.getSuffix() != null) {
                rsElem.setAttribute("suffix", resultService.getSuffix());
            }
            if (resultService.getAccessPointHeader() != null) {
                rsElem.setAttribute("accessPointHeader", resultService.getAccessPointHeader());
            }
        }
        return rsElem;
    }
    
    static {
        CatGenConfigMetadataFactory.log = LoggerFactory.getLogger(CatGenConfigMetadataFactory.class);
        CatGenConfigMetadataFactory.showParsedXML = false;
        CatGenConfigMetadataFactory.debug = false;
        CATALOG_GEN_CONFIG_NAMESPACE_0_5 = Namespace.getNamespace("http://www.unidata.ucar.edu/namespaces/thredds/CatalogGenConfig/v0.5");
    }
}
