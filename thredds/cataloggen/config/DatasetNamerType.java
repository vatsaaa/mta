// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

enum DatasetNamerType
{
    REGULAR_EXPRESSION("RegExp"), 
    DODS_ATTRIBUTE("DodsAttrib");
    
    private String altId;
    
    private DatasetNamerType(final String altId) {
        this.altId = altId;
    }
    
    @Override
    public String toString() {
        return this.altId;
    }
    
    public static DatasetNamerType getType(final String altId) {
        if (altId == null) {
            return null;
        }
        for (final DatasetNamerType curType : values()) {
            if (curType.altId.equals(altId)) {
                return curType;
            }
        }
        return null;
    }
}
