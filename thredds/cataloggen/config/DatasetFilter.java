// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.InvDataset;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Pattern;

public class DatasetFilter
{
    private DatasetSource parentDatasetSource;
    private String name;
    private Type type;
    private String matchPattern;
    protected Pattern regExpPattern;
    private String matchPatternTarget;
    private boolean applyToCollectionDatasets;
    private boolean applyToAtomicDatasets;
    private boolean rejectMatchingDatasets;
    private boolean isValid;
    private StringBuffer log;
    
    public DatasetFilter(final DatasetSource parentDsSource, final String name, final Type type, final String matchPattern) {
        this.parentDatasetSource = null;
        this.name = null;
        this.type = null;
        this.matchPattern = null;
        this.matchPatternTarget = null;
        this.applyToCollectionDatasets = false;
        this.applyToAtomicDatasets = true;
        this.rejectMatchingDatasets = false;
        this.isValid = true;
        this.log = new StringBuffer();
        if (type == null) {
            this.isValid = false;
            this.log.append(" ** DatasetFilter (1): invalid type for datasetFilter (" + name + ")");
        }
        this.parentDatasetSource = parentDsSource;
        this.name = name;
        this.type = type;
        if (matchPattern == null) {
            this.isValid = false;
            this.log.append(" ** DatasetFilter (2): null matchPattern not allowed.");
        }
        else {
            this.matchPattern = matchPattern;
            try {
                this.regExpPattern = Pattern.compile(this.matchPattern);
            }
            catch (PatternSyntaxException e) {
                this.isValid = false;
                this.log.append(" ** DatasetFilter (3): invalid matchPattern [" + this.matchPattern + "].");
            }
        }
    }
    
    public DatasetFilter(final DatasetSource parentDsSource, final String name, final Type type, final String matchPattern, final boolean applyToCollectionDatasets, final boolean applyToAtomicDatasets, final boolean rejectMatchingDatasets) {
        this(parentDsSource, name, type, matchPattern);
        this.applyToCollectionDatasets = applyToCollectionDatasets;
        this.applyToAtomicDatasets = applyToAtomicDatasets;
        this.rejectMatchingDatasets = rejectMatchingDatasets;
    }
    
    public DatasetSource getParentDatasetSource() {
        return this.parentDatasetSource;
    }
    
    public String getName() {
        return this.name;
    }
    
    public Type getType() {
        return this.type;
    }
    
    public String getMatchPattern() {
        return this.matchPattern;
    }
    
    public String getMatchPatternTarget() {
        return this.matchPatternTarget;
    }
    
    public void setMatchPatternTarget(final String matchPatternTarget) {
        this.matchPatternTarget = matchPatternTarget;
    }
    
    public boolean isApplyToCollectionDatasets() {
        return this.applyToCollectionDatasets;
    }
    
    public void setApplyToCollectionDatasets(final boolean applyToCollectionDatasets) {
        this.applyToCollectionDatasets = applyToCollectionDatasets;
    }
    
    public boolean isApplyToAtomicDatasets() {
        return this.applyToAtomicDatasets;
    }
    
    public void setApplyToAtomicDatasets(final boolean applyToAtomicDatasets) {
        this.applyToAtomicDatasets = applyToAtomicDatasets;
    }
    
    public boolean isRejectMatchingDatasets() {
        return this.rejectMatchingDatasets;
    }
    
    public boolean isAcceptMatchingDatasets() {
        return !this.rejectMatchingDatasets;
    }
    
    public void setRejectMatchingDatasets(final boolean rejectMatchingDatasets) {
        this.rejectMatchingDatasets = rejectMatchingDatasets;
    }
    
    boolean validate(final StringBuilder out) {
        this.isValid = true;
        if (this.log.length() > 0) {
            out.append(this.log);
        }
        if (this.getName() == null) {
            this.isValid = false;
            out.append(" ** DatasetFilter (4): null value for name is not valid.");
        }
        if (this.getType() == null) {
            this.isValid = false;
            out.append(" ** DatasetFilter (5): null value for type is not valid (set with bad string?).");
        }
        if (this.type == Type.REGULAR_EXPRESSION && this.matchPattern == null) {
            this.isValid = false;
            out.append(" ** DatasetFilter (6): null value for matchPattern not valid when type is 'RegExp'.");
        }
        if (this.type != Type.REGULAR_EXPRESSION && this.type != null && this.matchPattern != null) {
            this.isValid = false;
            out.append(" ** DatasetFilter (7): matchPattern value (" + this.matchPattern + ") must be null if type is not 'RegExp'.");
        }
        return this.isValid;
    }
    
    @Override
    public String toString() {
        final StringBuffer tmp = new StringBuffer();
        tmp.append("DatasetFilter[name:<" + this.getName() + "> type:<" + this.getType() + "> matchPattern:<" + this.getMatchPattern() + ">");
        return tmp.toString();
    }
    
    public boolean reject(final InvDataset dataset) {
        if (this.isAcceptMatchingDatasets()) {
            throw new IllegalStateException("Accept filter <" + this.getName() + "> does not allow call to reject().");
        }
        return this.match(dataset);
    }
    
    public boolean accept(final InvDataset dataset) {
        if (this.isRejectMatchingDatasets()) {
            throw new IllegalStateException("Reject filter <" + this.getName() + "> does not allow call to accept().");
        }
        return this.match(dataset);
    }
    
    protected boolean appliesToDataset(final InvDataset dataset) {
        return (!this.getParentDatasetSource().isCollection(dataset) || this.applyToCollectionDatasets) && (this.getParentDatasetSource().isCollection(dataset) || this.applyToAtomicDatasets);
    }
    
    private boolean match(final InvDataset dataset) {
        if (this.getParentDatasetSource().isCollection(dataset) && !this.applyToCollectionDatasets) {
            return false;
        }
        if (!this.getParentDatasetSource().isCollection(dataset) && !this.applyToAtomicDatasets) {
            return false;
        }
        if (this.matchPatternTarget == null) {
            if (this.getParentDatasetSource().isCollection(dataset)) {
                this.setMatchPatternTarget("name");
            }
            else {
                this.setMatchPatternTarget("urlPath");
            }
        }
        if (this.type == Type.REGULAR_EXPRESSION) {
            boolean isMatch;
            if (this.getMatchPatternTarget().equals("name")) {
                final Matcher matcher = this.regExpPattern.matcher(dataset.getName());
                isMatch = matcher.find();
            }
            else if (this.getMatchPatternTarget().equals("urlPath")) {
                final Matcher matcher = this.regExpPattern.matcher(((InvDatasetImpl)dataset).getUrlPath());
                isMatch = matcher.find();
            }
            else {
                isMatch = false;
            }
            return isMatch;
        }
        System.err.println("WARNING -- DatasetFilter.accept(): unsupported type <" + this.type.toString() + ">.");
        return false;
    }
    
    public static boolean acceptDatasetByFilterGroup(final List filters, final InvDataset dataset, final boolean isCollectionDataset) {
        if (filters == null) {
            throw new NullPointerException("Given null list of filters.");
        }
        if (dataset == null) {
            throw new NullPointerException("Given null dataset.");
        }
        if (filters.isEmpty()) {
            return true;
        }
        boolean accept = false;
        boolean anyApplyToAtomic = false;
        boolean anyApplyToCollection = false;
        for (final DatasetFilter curFilter : filters) {
            anyApplyToAtomic |= curFilter.isApplyToAtomicDatasets();
            anyApplyToCollection |= curFilter.isApplyToCollectionDatasets();
            if (curFilter.isAcceptMatchingDatasets()) {
                if (!curFilter.accept(dataset)) {
                    continue;
                }
                accept = true;
            }
            else {
                if (curFilter.reject(dataset)) {
                    return false;
                }
                continue;
            }
        }
        if (accept) {
            return true;
        }
        if (isCollectionDataset) {
            if (!anyApplyToCollection) {
                return true;
            }
        }
        else if (!anyApplyToAtomic) {
            return true;
        }
        return false;
    }
    
    enum Type
    {
        REGULAR_EXPRESSION("RegExp");
        
        private String altId;
        
        private Type(final String altId) {
            this.altId = altId;
        }
        
        @Override
        public String toString() {
            return this.altId;
        }
        
        public static Type getType(final String altId) {
            if (altId == null) {
                return null;
            }
            for (final Type curType : values()) {
                if (curType.altId.equals(altId)) {
                    return curType;
                }
            }
            return null;
        }
    }
}
