// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import thredds.catalog.InvDatasetScan;
import thredds.catalog.InvCatalogRef;
import java.util.Collections;
import java.util.List;
import thredds.catalog.InvDataset;
import java.util.Comparator;
import org.slf4j.Logger;

public class DatasetSorter
{
    private static Logger logger;
    private String targetLevel;
    private boolean increasingOrder;
    private String type;
    private String attributeName;
    private Comparator invDatasetComparator;
    
    public DatasetSorter(final boolean increasingOrder) {
        this.increasingOrder = true;
        this.type = "lexigraphic";
        this.increasingOrder = increasingOrder;
        this.invDatasetComparator = new Comparator() {
            public int compare(final Object obj1, final Object obj2) {
                final InvDataset ds1 = (InvDataset)obj1;
                final InvDataset ds2 = (InvDataset)obj2;
                final int compareVal = ds1.getName().compareTo(ds2.getName());
                return DatasetSorter.this.increasingOrder ? compareVal : (-compareVal);
            }
        };
    }
    
    public DatasetSorter(final Comparator invDatasetComparator) {
        this.increasingOrder = true;
        this.type = "lexigraphic";
        this.invDatasetComparator = invDatasetComparator;
    }
    
    public void sortDatasets(final InvDataset collectionDs) {
        this.sortDatasets(collectionDs.getDatasets());
    }
    
    public void sortDatasets(final List datasets) {
        Collections.sort((List<Object>)datasets, this.invDatasetComparator);
    }
    
    public void sortNestedDatasets(final InvDataset collectionDs) {
        InvDataset curDs = null;
        final Iterator dsIter = collectionDs.getDatasets().iterator();
        while (dsIter.hasNext()) {
            curDs = dsIter.next();
            if (!curDs.getClass().equals(InvCatalogRef.class) && !curDs.getClass().equals(InvDatasetScan.class)) {
                this.sortDatasets(curDs);
            }
        }
        DatasetSorter.logger.debug("sortDatasets(): sort the datasets contained by dataset ({})", collectionDs.getName());
        this.sortDatasets(collectionDs.getDatasets());
    }
    
    static {
        DatasetSorter.logger = LoggerFactory.getLogger(DatasetSorter.class);
    }
}
