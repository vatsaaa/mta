// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import thredds.catalog.InvDataset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;

public class CatalogRefExpander
{
    private static Logger logger;
    private String name;
    private String directoryMatchPattern;
    private String catalogTitleSubstitutionPattern;
    private String catalogFilenameSubstitutionPattern;
    private boolean expand;
    private boolean flattenCatalog;
    private Pattern pattern;
    private Matcher matcher;
    
    public CatalogRefExpander(final String name, final String directoryMatchPattern, final String catalogTitleSubstitutionPattern, final String catalogFilenameSubstitutionPattern, final boolean expand, final boolean flattenCatalog) {
        this.expand = true;
        this.flattenCatalog = false;
        this.name = name;
        this.directoryMatchPattern = directoryMatchPattern;
        this.catalogTitleSubstitutionPattern = catalogTitleSubstitutionPattern;
        this.catalogFilenameSubstitutionPattern = catalogFilenameSubstitutionPattern;
        this.expand = expand;
        this.flattenCatalog = flattenCatalog;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getDirectoryMatchPattern() {
        return this.directoryMatchPattern;
    }
    
    public void setDirectoryMatchPattern(final String directoryMatchPattern) {
        this.directoryMatchPattern = directoryMatchPattern;
    }
    
    public String getCatalogTitleSubstitutionPattern() {
        return this.catalogTitleSubstitutionPattern;
    }
    
    public void setCatalogTitleSubstitutionPattern(final String catalogTitleSubstitutionPattern) {
        this.catalogTitleSubstitutionPattern = catalogTitleSubstitutionPattern;
    }
    
    public String getCatalogFilenameSubstitutionPattern() {
        return this.catalogFilenameSubstitutionPattern;
    }
    
    public void setCatalogFilenameSubstitutionPattern(final String catalogFilenameSubstitutionPattern) {
        this.catalogFilenameSubstitutionPattern = catalogFilenameSubstitutionPattern;
    }
    
    public boolean isExpand() {
        return this.expand;
    }
    
    public void setExpand(final boolean expand) {
        this.expand = expand;
    }
    
    public boolean isFlattenCatalog() {
        return this.flattenCatalog;
    }
    
    public void setFlattenCatalog(final boolean flattenCatalog) {
        this.flattenCatalog = flattenCatalog;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CatalogRefExpander)) {
            return false;
        }
        final CatalogRefExpander catalogRefExpander = (CatalogRefExpander)o;
        if (this.flattenCatalog != catalogRefExpander.flattenCatalog) {
            return false;
        }
        if (this.expand != catalogRefExpander.expand) {
            return false;
        }
        Label_0080: {
            if (this.catalogFilenameSubstitutionPattern != null) {
                if (this.catalogFilenameSubstitutionPattern.equals(catalogRefExpander.catalogFilenameSubstitutionPattern)) {
                    break Label_0080;
                }
            }
            else if (catalogRefExpander.catalogFilenameSubstitutionPattern == null) {
                break Label_0080;
            }
            return false;
        }
        Label_0113: {
            if (this.catalogTitleSubstitutionPattern != null) {
                if (this.catalogTitleSubstitutionPattern.equals(catalogRefExpander.catalogTitleSubstitutionPattern)) {
                    break Label_0113;
                }
            }
            else if (catalogRefExpander.catalogTitleSubstitutionPattern == null) {
                break Label_0113;
            }
            return false;
        }
        Label_0146: {
            if (this.directoryMatchPattern != null) {
                if (this.directoryMatchPattern.equals(catalogRefExpander.directoryMatchPattern)) {
                    break Label_0146;
                }
            }
            else if (catalogRefExpander.directoryMatchPattern == null) {
                break Label_0146;
            }
            return false;
        }
        if (this.name != null) {
            if (this.name.equals(catalogRefExpander.name)) {
                return true;
            }
        }
        else if (catalogRefExpander.name == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = (this.name != null) ? this.name.hashCode() : 0;
        result = 29 * result + ((this.directoryMatchPattern != null) ? this.directoryMatchPattern.hashCode() : 0);
        result = 29 * result + ((this.catalogTitleSubstitutionPattern != null) ? this.catalogTitleSubstitutionPattern.hashCode() : 0);
        result = 29 * result + ((this.catalogFilenameSubstitutionPattern != null) ? this.catalogFilenameSubstitutionPattern.hashCode() : 0);
        result = 29 * result + (this.expand ? 1 : 0);
        result = 29 * result + (this.flattenCatalog ? 1 : 0);
        return result;
    }
    
    public boolean makeCatalogRef(final InvDataset dataset) {
        this.pattern = Pattern.compile(this.directoryMatchPattern);
        this.matcher = this.pattern.matcher(dataset.getName());
        return this.matcher.matches();
    }
    
    public String catalogRefTitle() {
        final StringBuffer val = new StringBuffer();
        this.matcher.appendReplacement(val, this.catalogTitleSubstitutionPattern);
        return val.toString();
    }
    
    public String catalogRefFilename() {
        final StringBuffer val = new StringBuffer();
        this.matcher.appendReplacement(val, this.catalogFilenameSubstitutionPattern);
        return val.toString();
    }
    
    static {
        CatalogRefExpander.logger = LoggerFactory.getLogger(CatalogRefExpander.class);
    }
}
