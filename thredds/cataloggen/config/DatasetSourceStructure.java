// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import java.util.HashMap;

public final class DatasetSourceStructure
{
    private static HashMap hash;
    public static final DatasetSourceStructure FLAT;
    public static final DatasetSourceStructure DIRECTORY_TREE;
    private String structureName;
    
    private DatasetSourceStructure(final String name) {
        this.structureName = name;
        DatasetSourceStructure.hash.put(name, this);
    }
    
    public static DatasetSourceStructure getStructure(final String name) {
        if (name == null) {
            return null;
        }
        return DatasetSourceStructure.hash.get(name);
    }
    
    @Override
    public String toString() {
        return this.structureName;
    }
    
    static {
        DatasetSourceStructure.hash = new HashMap(20);
        FLAT = new DatasetSourceStructure("Flat");
        DIRECTORY_TREE = new DatasetSourceStructure("DirTree");
    }
}
