// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import thredds.catalog.InvDataset;
import org.slf4j.Logger;

public class CatalogGenConfig
{
    private static Logger log;
    private InvDataset parentDataset;
    private Type type;
    private DatasetSource datasetSource;
    private boolean isValid;
    private StringBuffer msgLog;
    public static final String CATALOG_GEN_CONFIG_NAMESPACE_URI_0_5 = "http://www.unidata.ucar.edu/namespaces/thredds/CatalogGenConfig/v0.5";
    
    public CatalogGenConfig(final InvDataset parentDataset, final String typeName) {
        this(parentDataset, Type.getType(typeName));
    }
    
    public CatalogGenConfig(final InvDataset parentDataset, final Type type) {
        this.parentDataset = null;
        this.type = null;
        this.datasetSource = null;
        this.isValid = true;
        this.msgLog = new StringBuffer();
        CatalogGenConfig.log.debug("CatalogGenConfig(): type " + type.toString() + ".");
        this.parentDataset = parentDataset;
        this.type = type;
    }
    
    public InvDataset getParentDataset() {
        return this.parentDataset;
    }
    
    public void setParentDataset(final InvDataset parentDataset) {
        this.parentDataset = parentDataset;
    }
    
    public Type getType() {
        return this.type;
    }
    
    public void setType(final Type type) {
        this.type = type;
    }
    
    public DatasetSource getDatasetSource() {
        return this.datasetSource;
    }
    
    public void setDatasetSource(final DatasetSource dsSource) {
        this.datasetSource = dsSource;
    }
    
    public boolean validate(final StringBuilder out) {
        CatalogGenConfig.log.debug("validate(): checking if valid");
        this.isValid = true;
        if (this.msgLog.length() > 0) {
            out.append(this.msgLog);
        }
        if (this.getType() == null) {
            this.isValid = false;
            out.append(" ** CatalogGenConfig (3): null value for type is not valid (set with bad string?).");
        }
        this.isValid &= this.getDatasetSource().validate(out);
        CatalogGenConfig.log.debug("validate(): isValid=" + this.isValid + " message is\n" + out.toString());
        return this.isValid;
    }
    
    @Override
    public String toString() {
        final StringBuffer tmp = new StringBuffer();
        tmp.append("CatalogGenConfig[type:<").append(this.getType()).append("> child ").append(this.getDatasetSource().toString() + ")]");
        return tmp.toString();
    }
    
    static {
        CatalogGenConfig.log = LoggerFactory.getLogger(CatalogGenConfig.class);
    }
    
    enum Type
    {
        CATALOG("Catalog"), 
        AGGREGATION("Aggregation");
        
        private String altId;
        
        private Type(final String altId) {
            this.altId = altId;
        }
        
        @Override
        public String toString() {
            return this.altId;
        }
        
        public static Type getType(final String altId) {
            if (altId == null) {
                return null;
            }
            for (final Type curType : values()) {
                if (curType.altId.equals(altId)) {
                    return curType;
                }
            }
            return null;
        }
    }
}
