// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import thredds.catalog.ServiceType;
import ucar.nc2.constants.FeatureType;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import thredds.catalog.InvDatasetImpl;
import java.util.Iterator;
import thredds.catalog.MetadataConverterIF;
import thredds.catalog.InvMetadata;
import thredds.catalog.ThreddsMetadata;
import thredds.catalog.InvProperty;
import thredds.catalog.InvService;
import java.net.URI;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvCatalog;
import java.io.IOException;
import thredds.catalog.InvDataset;
import java.io.File;
import org.slf4j.Logger;

public class LocalDatasetSource extends DatasetSource
{
    private static Logger logger;
    private File accessPointHeaderFile;
    private File accessPointFile;
    
    LocalDatasetSource() {
        this.accessPointHeaderFile = null;
        this.accessPointFile = null;
        this.type = DatasetSourceType.getType("Local");
    }
    
    @Override
    protected InvDataset createDataset(final String datasetLocation, final String prefixUrlPath) throws IOException {
        if (datasetLocation == null) {
            throw new NullPointerException("Dataset location cannot be null.");
        }
        this.checkAccessPoint();
        return new LocalInvDataset(null, new File(datasetLocation), prefixUrlPath);
    }
    
    @Override
    protected InvCatalog createSkeletonCatalog(final String prefixUrlPath) throws IOException {
        this.checkAccessPoint();
        final InvCatalogImpl catalog = new InvCatalogImpl(null, null, null);
        final InvService service = new InvService(this.getResultService().getName(), this.getResultService().getServiceType().toString(), this.getResultService().getBase(), this.getResultService().getSuffix(), this.getResultService().getDescription());
        Iterator it = this.getResultService().getProperties().iterator();
        while (it.hasNext()) {
            service.addProperty(it.next());
        }
        it = this.getResultService().getServices().iterator();
        while (it.hasNext()) {
            service.addService(it.next());
        }
        catalog.addService(service);
        final File apFile = new File(this.getAccessPoint());
        final InvDatasetImpl topDs = new LocalInvDataset(null, apFile, prefixUrlPath);
        final ThreddsMetadata tm = new ThreddsMetadata(false);
        tm.setServiceName(service.getName());
        final InvMetadata md = new InvMetadata(topDs, null, "http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", "", true, true, null, tm);
        final ThreddsMetadata tm2 = new ThreddsMetadata(false);
        tm2.addMetadata(md);
        topDs.setLocalMetadata(tm2);
        catalog.addDataset(topDs);
        catalog.finish();
        return catalog;
    }
    
    private void checkAccessPoint() throws IOException {
        if (this.accessPointHeaderFile == null) {
            final File aphFile = new File(this.getResultService().getAccessPointHeader());
            if (!aphFile.exists()) {
                throw new IOException("The accessPointHeader local file does not exist <" + aphFile.getPath() + ">.");
            }
            final File apFile = new File(this.getAccessPoint());
            if (!apFile.exists()) {
                throw new IOException("The accessPoint local file does not exist <" + apFile.getPath() + ">.");
            }
            if (!apFile.isDirectory()) {
                throw new IOException("The accessPoint local file is not a directory <" + apFile.getPath() + ">.");
            }
            if (!apFile.getPath().startsWith(aphFile.getPath()) && !apFile.getCanonicalPath().startsWith(aphFile.getCanonicalPath())) {
                final String tmpMsg = "The accessPoint <" + apFile.getPath() + " or " + apFile.getCanonicalPath() + "> must start with the accessPointHeader <" + aphFile.getPath() + " or " + aphFile.getCanonicalPath() + ">.";
                LocalDatasetSource.logger.debug("checkAccessPoint(): {}", tmpMsg);
                throw new IOException(tmpMsg);
            }
            this.accessPointHeaderFile = aphFile;
            this.accessPointFile = apFile;
        }
    }
    
    @Override
    protected boolean isCollection(final InvDataset dataset) {
        return ((LocalInvDataset)dataset).isDirectory();
    }
    
    @Override
    protected List expandThisLevel(final InvDataset dataset, final String prefixUrlPath) {
        if (dataset == null) {
            throw new NullPointerException("Given dataset cannot be null.");
        }
        if (!this.isCollection(dataset)) {
            throw new IllegalArgumentException("Dataset \"" + dataset.getName() + "\" is not a collection dataset.");
        }
        final File theDir = new File(((LocalInvDataset)dataset).getLocalPath());
        final File[] allFiles = theDir.listFiles();
        InvDataset curDs = null;
        final ArrayList list = new ArrayList();
        for (int i = 0; i < allFiles.length; ++i) {
            try {
                curDs = new LocalInvDataset(dataset, allFiles[i], prefixUrlPath);
            }
            catch (IOException e) {
                continue;
            }
            list.add(curDs);
        }
        return list;
    }
    
    static {
        LocalDatasetSource.logger = LoggerFactory.getLogger(LocalDatasetSource.class);
    }
    
    private class LocalInvDataset extends InvDatasetImpl
    {
        private String relativePath;
        private String catRelativePath;
        private String localPath;
        private boolean directory;
        
        LocalInvDataset(final InvDataset parent, final File file, final String prefixUrlPath) throws IOException {
            super((InvDatasetImpl)parent, null, null, null, null);
            if (LocalDatasetSource.logger.isDebugEnabled()) {
                LocalDatasetSource.logger.debug("LocalInvDataset(): parent=" + ((parent == null) ? "" : parent.getName()) + "; file=" + file.getPath());
            }
            if (!file.exists()) {
                throw new IOException("File <" + file.getPath() + "> does not exist.");
            }
            this.directory = file.isDirectory();
            final String uriStringFromFile = file.toURI().toString();
            final String uriStringFromCanonicalFile = file.getCanonicalFile().toURI().toString();
            final String aphUriStringFromFile = LocalDatasetSource.this.accessPointHeaderFile.toURI().toString();
            final String aphUriStringFromCanonicalFile = LocalDatasetSource.this.accessPointHeaderFile.getCanonicalFile().toURI().toString();
            final String apUriStringFromFile = LocalDatasetSource.this.accessPointFile.toURI().toString();
            final String apUriStringFromCanonicalFile = LocalDatasetSource.this.accessPointFile.getCanonicalFile().toURI().toString();
            this.relativePath = null;
            if (LocalDatasetSource.logger.isDebugEnabled()) {
                LocalDatasetSource.logger.debug("LocalInvDataset():               file URI=" + uriStringFromFile);
                LocalDatasetSource.logger.debug("LocalInvDataset():     canonical file URI=" + uriStringFromCanonicalFile);
                LocalDatasetSource.logger.debug("LocalInvDataset():           aph file URI=" + aphUriStringFromFile);
                LocalDatasetSource.logger.debug("LocalInvDataset(): aph canonical file URI=" + aphUriStringFromCanonicalFile);
            }
            if (!uriStringFromFile.startsWith(aphUriStringFromFile)) {
                if (!uriStringFromCanonicalFile.startsWith(aphUriStringFromCanonicalFile)) {
                    final String tmpMsg = "File <" + uriStringFromFile + " or " + uriStringFromCanonicalFile + "> must start with accessPointHeader <" + aphUriStringFromFile + " or " + aphUriStringFromCanonicalFile + ">.";
                    throw new IOException(tmpMsg);
                }
                this.relativePath = uriStringFromCanonicalFile.substring(aphUriStringFromCanonicalFile.length());
                this.catRelativePath = uriStringFromCanonicalFile.substring(apUriStringFromCanonicalFile.length());
                this.localPath = file.getCanonicalPath();
            }
            else {
                this.relativePath = uriStringFromFile.substring(aphUriStringFromFile.length());
                this.catRelativePath = uriStringFromFile.substring(apUriStringFromFile.length());
                this.localPath = file.getAbsolutePath();
            }
            if (LocalDatasetSource.logger.isDebugEnabled()) {
                LocalDatasetSource.logger.debug("LocalInvDataset():           relativePath=" + this.relativePath);
                LocalDatasetSource.logger.debug("LocalInvDataset():              localPath=" + this.localPath);
            }
            if (!this.directory) {
                if (LocalDatasetSource.this.getResultService().getBase().equals("") && !LocalDatasetSource.this.getResultService().getServiceType().equals(ServiceType.COMPOUND)) {
                    this.setUrlPath(this.catRelativePath);
                }
                else {
                    final String tmpUrlPath = (prefixUrlPath == null || prefixUrlPath.equals("")) ? this.relativePath : (prefixUrlPath + "/" + this.relativePath);
                    this.setUrlPath(tmpUrlPath);
                }
                if (LocalDatasetSource.this.isAddDatasetSize()) {
                    this.setDataSize((double)file.length());
                }
            }
            String dsName = this.relativePath.endsWith("/") ? this.relativePath.substring(0, this.relativePath.length() - 1) : this.relativePath;
            final int index = dsName.lastIndexOf("/");
            if (index != -1) {
                dsName = dsName.substring(index + 1);
            }
            this.setName(dsName);
        }
        
        String getLocalPath() {
            return this.localPath;
        }
        
        void setLocalPath(final String localPath) {
            this.localPath = localPath;
        }
        
        public boolean isDirectory() {
            return this.directory;
        }
    }
}
