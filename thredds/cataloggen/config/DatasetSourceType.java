// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import java.util.HashMap;

public final class DatasetSourceType
{
    private static HashMap hash;
    public static final DatasetSourceType LOCAL;
    public static final DatasetSourceType DODS_FILE_SERVER;
    public static final DatasetSourceType DODS_DIR;
    public static final DatasetSourceType GRADS_DATA_SERVER;
    private String typeName;
    
    private DatasetSourceType(final String name) {
        this.typeName = name;
        DatasetSourceType.hash.put(name, this);
    }
    
    public static DatasetSourceType getType(final String name) {
        if (name == null) {
            return null;
        }
        return DatasetSourceType.hash.get(name);
    }
    
    @Override
    public String toString() {
        return this.typeName;
    }
    
    static {
        DatasetSourceType.hash = new HashMap(20);
        LOCAL = new DatasetSourceType("Local");
        DODS_FILE_SERVER = new DatasetSourceType("DodsFileServer");
        DODS_DIR = new DatasetSourceType("DodsDir");
        GRADS_DATA_SERVER = new DatasetSourceType("GrADSDataServer");
    }
}
