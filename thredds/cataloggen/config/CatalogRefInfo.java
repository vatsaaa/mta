// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import thredds.catalog.InvDataset;

public class CatalogRefInfo
{
    private String title;
    private String fileName;
    private InvDataset accessPointDataset;
    private DatasetSource datasetSource;
    
    public CatalogRefInfo(final String title, final String fileName, final InvDataset accessPointDataset, final DatasetSource dsSource) {
        if (title == null || fileName == null || accessPointDataset == null || dsSource == null) {
            throw new IllegalArgumentException("Null arguments not allowed.");
        }
        this.title = title;
        this.fileName = fileName;
        this.accessPointDataset = accessPointDataset;
        this.datasetSource = dsSource;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public String getFileName() {
        return this.fileName;
    }
    
    public InvDataset getAccessPointDataset() {
        return this.accessPointDataset;
    }
    
    public DatasetSource getDatasetSource() {
        return this.datasetSource;
    }
}
