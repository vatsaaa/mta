// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import ucar.nc2.constants.FeatureType;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.MetadataConverterIF;
import thredds.catalog.InvMetadata;
import thredds.catalog.ThreddsMetadata;
import thredds.catalog.InvProperty;
import thredds.catalog.InvService;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvCatalog;
import java.net.URISyntaxException;
import java.io.IOException;
import thredds.catalog.InvDataset;
import thredds.util.DodsURLExtractor;
import java.net.URI;
import org.slf4j.Logger;

public class DodsDirDatasetSource extends DatasetSource
{
    private static Logger log;
    private URI accessPointHeaderUri;
    private DodsURLExtractor urlExtractor;
    
    public DodsDirDatasetSource() {
        this.accessPointHeaderUri = null;
        this.urlExtractor = null;
        this.type = DatasetSourceType.getType("DodsDir");
        this.urlExtractor = new DodsURLExtractor();
    }
    
    @Override
    protected InvDataset createDataset(final String datasetLocation, final String prefixUrlPath) throws IOException {
        URI dsLocUri = null;
        try {
            dsLocUri = new URI(datasetLocation);
        }
        catch (URISyntaxException e) {
            throw new IOException("URISyntaxException for dataset location <" + datasetLocation + ">: " + e.getMessage());
        }
        return new DodsDirInvDataset(null, dsLocUri);
    }
    
    @Override
    protected InvCatalog createSkeletonCatalog(final String prefixUrlPath) throws IOException {
        final String aphString = this.getResultService().getAccessPointHeader();
        final String apString = this.getAccessPoint();
        if (!apString.startsWith(aphString)) {
            throw new IOException("The accessPoint <" + apString + "> must start with the accessPointHeader <" + aphString + ">.");
        }
        if (!apString.endsWith("/")) {
            throw new IOException("The accessPoint URL must end with a \"/\" <" + apString + ">.");
        }
        final String apVersionString = apString + "version";
        String apVersionResultContent = null;
        try {
            apVersionResultContent = this.urlExtractor.getTextContent(apVersionString);
        }
        catch (IOException e) {
            final String tmpMsg = "The accessPoint URL is not an OPeNDAP server URL (no version info) <" + apVersionString + ">";
            DodsDirDatasetSource.log.error("expandThisType(): " + tmpMsg, e);
            final IOException myE = new IOException(tmpMsg + e.getMessage());
            myE.initCause(e);
            throw myE;
        }
        if (apVersionResultContent.indexOf("DODS") == -1 && apVersionResultContent.indexOf("OPeNDAP") == -1 && apVersionResultContent.indexOf("DAP") == -1) {
            final String tmpMsg2 = "The accessPoint URL version info is not valid <" + apVersionResultContent + ">";
            DodsDirDatasetSource.log.error("expandThisType(): " + tmpMsg2);
            throw new IOException(tmpMsg2);
        }
        try {
            this.accessPointHeaderUri = new URI(aphString);
        }
        catch (URISyntaxException e2) {
            throw new IOException("The accessPointHeader URL failed to map to a URI <" + aphString + ">.");
        }
        final InvCatalogImpl catalog = new InvCatalogImpl(null, null, null);
        final InvService service = new InvService(this.getResultService().getName(), this.getResultService().getServiceType().toString(), this.getResultService().getBase(), this.getResultService().getSuffix(), this.getResultService().getDescription());
        Iterator it = this.getResultService().getProperties().iterator();
        while (it.hasNext()) {
            service.addProperty(it.next());
        }
        it = this.getResultService().getServices().iterator();
        while (it.hasNext()) {
            service.addService(it.next());
        }
        catalog.addService(service);
        DodsDirInvDataset topDs = null;
        try {
            topDs = new DodsDirInvDataset(null, new URI(apString));
        }
        catch (URISyntaxException e3) {
            throw new IOException("The accessPoint URL failed to map to a URI <" + apString + ">.");
        }
        final ThreddsMetadata tm = new ThreddsMetadata(false);
        tm.setServiceName(service.getName());
        final InvMetadata md = new InvMetadata(topDs, null, "http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", "", true, true, null, tm);
        final ThreddsMetadata tm2 = new ThreddsMetadata(false);
        tm2.addMetadata(md);
        topDs.setLocalMetadata(tm2);
        catalog.addDataset(topDs);
        catalog.finish();
        return catalog;
    }
    
    @Override
    protected boolean isCollection(final InvDataset dataset) {
        return ((DodsDirInvDataset)dataset).isDirectory();
    }
    
    @Override
    protected List expandThisLevel(final InvDataset dataset, final String prefixUrlPath) {
        if (dataset == null) {
            throw new NullPointerException("Given dataset cannot be null.");
        }
        if (!this.isCollection(dataset)) {
            throw new IllegalArgumentException("Dataset \"" + dataset.getName() + "\" is not a collection dataset.");
        }
        final List dsList = new ArrayList();
        List possibleDsList = null;
        try {
            possibleDsList = this.urlExtractor.extract(dataset.getName());
        }
        catch (IOException e) {
            DodsDirDatasetSource.log.warn("expandThisLevel(): IOException while extracting dataset info from given OPeNDAP directory <" + dataset.getName() + ">, return empty list: " + e.getMessage());
            return dsList;
        }
        String curDsUrlString = null;
        URI curDsUri = null;
        InvDataset curDs = null;
        final Iterator it = possibleDsList.iterator();
        while (it.hasNext()) {
            curDsUrlString = it.next();
            if (!curDsUrlString.endsWith(".html") && !curDsUrlString.endsWith("/")) {
                DodsDirDatasetSource.log.warn("expandThisLevel(): Dataset isn't an OPeNDAP dataset or collection dataset, skip <" + dataset.getName() + ">.");
            }
            else {
                if (curDsUrlString.endsWith(".html")) {
                    curDsUrlString = curDsUrlString.substring(0, curDsUrlString.length() - 5);
                }
                if (!curDsUrlString.startsWith(this.accessPointHeaderUri.toString())) {
                    DodsDirDatasetSource.log.debug("expandThisLevel(): current path <" + curDsUrlString + "> not child of given" + " location <" + this.accessPointHeaderUri.toString() + ">, skip.");
                }
                else {
                    try {
                        curDsUri = new URI(curDsUrlString);
                    }
                    catch (URISyntaxException e2) {
                        DodsDirDatasetSource.log.error("expandThisLevel(): Skipping dataset  <" + curDsUrlString + "> due to URISyntaxException: " + e2.getMessage());
                        continue;
                    }
                    DodsDirDatasetSource.log.debug("expandThisLevel(): handle dataset (" + curDsUrlString + ")");
                    try {
                        curDs = new DodsDirInvDataset(null, curDsUri);
                    }
                    catch (IOException e3) {
                        DodsDirDatasetSource.log.warn("expandThisLevel(): skipping dataset <" + curDsUri.toString() + ">, not under accessPointHeader: " + e3.getMessage());
                        continue;
                    }
                    dsList.add(curDs);
                }
            }
        }
        return dsList;
    }
    
    static {
        DodsDirDatasetSource.log = LoggerFactory.getLogger(DodsDirDatasetSource.class);
    }
    
    private class DodsDirInvDataset extends InvDatasetImpl
    {
        private URI uri;
        private boolean directory;
        
        DodsDirInvDataset(final InvDataset parent, final URI uri) throws IOException {
            super((InvDatasetImpl)parent, null, null, null, null);
            this.uri = null;
            this.directory = false;
            this.uri = uri;
            this.directory = uri.toString().endsWith("/");
            final String dsAbsolutePath = uri.toString();
            String dsRelativePath = null;
            if (dsAbsolutePath.startsWith(DodsDirDatasetSource.this.accessPointHeaderUri.toString())) {
                dsRelativePath = dsAbsolutePath.substring(DodsDirDatasetSource.this.accessPointHeaderUri.toString().length());
                if (!this.directory) {
                    this.setUrlPath(dsRelativePath);
                }
                this.setName(dsRelativePath);
                return;
            }
            throw new IOException("URI <" + dsAbsolutePath + "> not under accessPointHeader directory <" + DodsDirDatasetSource.this.accessPointHeaderUri.toString() + ">.");
        }
        
        URI getUri() {
            return this.uri;
        }
        
        void setUri(final URI uri) {
            this.uri = uri;
        }
        
        boolean isDirectory() {
            return this.directory;
        }
    }
}
