// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.config;

import org.slf4j.LoggerFactory;
import java.util.Enumeration;
import opendap.dap.Attribute;
import opendap.dap.AttributeTable;
import thredds.catalog.InvAccess;
import opendap.dap.DAS;
import opendap.dap.NoSuchAttributeException;
import opendap.dap.parser.ParseException;
import opendap.dap.DAP2Exception;
import java.io.FileNotFoundException;
import opendap.dap.DConnect;
import thredds.catalog.ServiceType;
import java.util.regex.Matcher;
import java.util.List;
import thredds.catalog.InvDatasetImpl;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Pattern;
import thredds.catalog.InvDataset;
import org.slf4j.Logger;

public class DatasetNamer
{
    private static Logger logger;
    private InvDataset parentDataset;
    private String name;
    private boolean addLevel;
    private DatasetNamerType type;
    private String matchPattern;
    protected Pattern regExpPattern;
    private String substitutePattern;
    private String attribContainer;
    private String attribName;
    private boolean isValid;
    private StringBuffer msgLog;
    
    public DatasetNamer(final InvDataset parentDs, final String name, final String addLevelBoolean, final String typeName, final String matchPattern, final String substitutePattern, final String attribContainer, final String attribName) {
        this(parentDs, name, new Boolean(addLevelBoolean), DatasetNamerType.getType(typeName), matchPattern, substitutePattern, attribContainer, attribName);
        if (this.getType() == null) {
            this.isValid = false;
            this.msgLog.append(" ** DatasetNamer (1): invalid type [" + typeName + "] for datasetNamer [" + name + "].");
        }
    }
    
    public DatasetNamer(final InvDataset parentDs, final String name, final boolean addLevel, final DatasetNamerType type, final String matchPattern, final String substitutePattern, final String attribContainer, final String attribName) {
        this.parentDataset = null;
        this.name = null;
        this.addLevel = false;
        this.type = null;
        this.matchPattern = null;
        this.substitutePattern = null;
        this.attribContainer = null;
        this.attribName = null;
        this.isValid = true;
        this.msgLog = new StringBuffer();
        this.parentDataset = parentDs;
        this.name = name;
        this.addLevel = addLevel;
        if (type == null) {
            this.isValid = false;
            this.msgLog.append(" ** DatasetNamer (1): null type for datasetNamer [" + name + "].");
        }
        this.type = type;
        if (matchPattern != null) {
            this.matchPattern = matchPattern;
            if (DatasetNamerType.REGULAR_EXPRESSION.equals(this.type)) {
                try {
                    this.regExpPattern = Pattern.compile(this.matchPattern);
                }
                catch (PatternSyntaxException e) {
                    this.isValid = false;
                    this.msgLog.append(" ** DatasetNamer (3): invalid matchPattern [" + this.matchPattern + "].");
                }
            }
        }
        this.substitutePattern = substitutePattern;
        this.attribContainer = attribContainer;
        this.attribName = attribName;
    }
    
    public InvDataset getParentDataset() {
        return this.parentDataset;
    }
    
    public String getName() {
        return this.name;
    }
    
    public boolean getAddLevel() {
        return this.addLevel;
    }
    
    public DatasetNamerType getType() {
        return this.type;
    }
    
    public String getMatchPattern() {
        return this.matchPattern;
    }
    
    public String getSubstitutePattern() {
        return this.substitutePattern;
    }
    
    public String getAttribContainer() {
        return this.attribContainer;
    }
    
    public String getAttribName() {
        return this.attribName;
    }
    
    boolean validate(final StringBuilder out) {
        this.isValid = true;
        if (this.msgLog.length() > 0) {
            out.append(this.msgLog);
        }
        if (this.getName() == null) {
            this.isValid = false;
            out.append(" ** DatasetNamer (1): null value for name is not valid.");
        }
        if (this.getType() == null) {
            this.isValid = false;
            out.append(" ** DatasetNamer (3): null value for type is not valid (set with bad string?).");
        }
        if (this.getType() == DatasetNamerType.REGULAR_EXPRESSION && (this.getMatchPattern() == null || this.getSubstitutePattern() == null)) {
            this.isValid = false;
            out.append(" ** DatasetNamer (4): invalid datasetNamer <" + this.getName() + ">;" + " type is " + this.getType().toString() + ": matchPattern(" + this.getMatchPattern() + ") and substitutionPattern(" + this.getSubstitutePattern() + ") " + "must not be null.");
        }
        if (this.getType() == DatasetNamerType.DODS_ATTRIBUTE && (this.getAttribContainer() == null || this.getAttribName() == null)) {
            this.isValid = false;
            out.append(" ** DatasetNamer (5): invalid datasetNamer <" + this.getName() + ">;" + " type is " + this.getType().toString() + ": attriuteContainer(" + this.getAttribContainer() + ") and attributeName(" + this.getAttribName() + ") must not be null.");
        }
        return this.isValid;
    }
    
    @Override
    public String toString() {
        final StringBuffer tmp = new StringBuffer();
        tmp.append("DatasetNamer[name:<" + this.getName() + "> addLevel:<" + this.getAddLevel() + "> type:<" + this.getType() + "> matchPattern:<" + this.getMatchPattern() + "> substitutePatter:<" + this.getSubstitutePattern() + "> attribContainer:<" + this.getAttribContainer() + "> attribName:<" + this.getAttribName() + ">]");
        return tmp.toString();
    }
    
    public boolean nameDataset(final InvDatasetImpl dataset) {
        if (this.type == DatasetNamerType.REGULAR_EXPRESSION) {
            return this.nameDatasetRegExp(dataset);
        }
        if (this.type == DatasetNamerType.DODS_ATTRIBUTE) {
            return this.nameDatasetDodsAttrib(dataset);
        }
        final String tmpMsg = "This DatasetNamer <" + this.getName() + "> has unsupported type <" + this.type.toString() + ">.";
        DatasetNamer.logger.error("nameDataset(): " + tmpMsg);
        throw new IllegalStateException(tmpMsg);
    }
    
    public boolean nameDatasetList(final List datasetList) throws Exception {
        boolean returnValue = false;
        InvDatasetImpl curDataset = null;
        for (int i = 0; i < datasetList.size(); ++i) {
            curDataset = datasetList.get(i);
            returnValue &= this.nameDataset(curDataset);
        }
        return returnValue;
    }
    
    private boolean nameDatasetRegExp(final InvDatasetImpl dataset) {
        Matcher matcher;
        boolean isMatch;
        if (dataset.getUrlPath() != null) {
            DatasetNamer.logger.debug("nameDatasetRegExp(): try naming on urlPath <{}>", dataset.getUrlPath());
            matcher = this.regExpPattern.matcher(dataset.getUrlPath());
            isMatch = matcher.find();
        }
        else {
            matcher = this.regExpPattern.matcher(dataset.getName());
            isMatch = matcher.find();
        }
        if (!isMatch) {
            if (DatasetNamer.logger.isDebugEnabled()) {
                DatasetNamer.logger.debug("nameDatasetRegExp(): Neither URL <" + dataset.getUrlPath() + "> or name <" + dataset.getName() + "> matched pattern <" + this.matchPattern + "> .");
            }
            return false;
        }
        final StringBuffer resultingName = new StringBuffer();
        matcher.appendReplacement(resultingName, this.substitutePattern);
        resultingName.delete(0, matcher.start());
        if (resultingName.length() != 0) {
            DatasetNamer.logger.debug("nameDatasetRegExp(): Setting name to \"" + (Object)resultingName + "\".");
            dataset.setName(resultingName.toString());
            return true;
        }
        DatasetNamer.logger.debug("nameDatasetRegExp(): No name for regEx substitution.");
        return false;
    }
    
    private boolean nameDatasetDodsAttrib(final InvDatasetImpl dataset) {
        DConnect dodsConnection = null;
        DAS das = null;
        final boolean acceptDeflate = true;
        String newDatasetName = null;
        final InvAccess access = dataset.getAccess(ServiceType.DODS);
        if (access == null) {
            DatasetNamer.logger.warn("nameDatasetDodsAttrib(): dataset is not DODS accessible and so cannot be named using DODS attributes.");
            return false;
        }
        final String url = access.getStandardUrlName();
        try {
            dodsConnection = new DConnect(url, acceptDeflate);
        }
        catch (FileNotFoundException e) {
            DatasetNamer.logger.error("nameDatasetDodsAttrib(): URL <" + url + "> not found: " + e.getMessage());
            return false;
        }
        catch (Exception e2) {
            DatasetNamer.logger.error("nameDatasetDodsAttrib(): Failed DODS connect: " + e2.getMessage());
            return false;
        }
        DatasetNamer.logger.debug("nameDatasetDodsAttrib(): Got DODS Connect <url={}>", url);
        try {
            das = dodsConnection.getDAS();
        }
        catch (DAP2Exception e3) {
            DatasetNamer.logger.error("nameDatasetDodsAttrib(): Failed to get DAS: " + e3.getMessage());
            return false;
        }
        catch (ParseException e4) {
            DatasetNamer.logger.error("nameDatasetDodsAttrib(): Failed to get DAS: " + e4.getMessage());
            return false;
        }
        catch (Exception e2) {
            DatasetNamer.logger.error("nameDatasetDodsAttrib(): Failed to get DAS: " + e2.getMessage());
            return false;
        }
        DatasetNamer.logger.debug("nameDatasetDodsAttrib(): Got DAS");
        AttributeTable dodsAttTable = null;
        try {
            dodsAttTable = das.getAttributeTable(this.attribContainer);
            if (dodsAttTable == null) {
                DatasetNamer.logger.debug("nameDatasetDodsAttrib(): attribute container does not exist.");
                return false;
            }
            final Attribute desiredAtt = dodsAttTable.getAttribute(this.attribName);
            if (desiredAtt.getType() != 10) {
                DatasetNamer.logger.debug("nameDatasetDodsAttrib(): attribute value is not a string.");
                return false;
            }
            final Enumeration enumValues = desiredAtt.getValues();
            if (!enumValues.hasMoreElements()) {
                DatasetNamer.logger.debug("nameDatasetDodsAttrib(): attribute has no value");
                return false;
            }
            newDatasetName = enumValues.nextElement();
            newDatasetName = newDatasetName.substring(1, newDatasetName.length() - 1);
            if (enumValues.hasMoreElements()) {
                DatasetNamer.logger.warn("nameDatasetDodsAttrib(): attribute has multiple values, only using first value <" + newDatasetName + ">");
                dataset.setName(newDatasetName);
                return true;
            }
            DatasetNamer.logger.debug("nameDatasetDodsAttrib(): setting dataset name to <{}>.", newDatasetName);
            dataset.setName(newDatasetName);
            return true;
        }
        catch (NoSuchAttributeException e5) {
            DatasetNamer.logger.debug("nameDatasetDodsAttrib(): attribute container does not exist.");
            return false;
        }
    }
    
    static {
        DatasetNamer.logger = LoggerFactory.getLogger(DatasetNamer.class);
    }
}
