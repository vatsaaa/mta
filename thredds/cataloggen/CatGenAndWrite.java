// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import thredds.catalog.InvCatalogImpl;
import java.io.IOException;
import thredds.crawlabledataset.CrawlableDatasetFile;
import java.util.List;
import java.util.Map;
import thredds.crawlabledataset.CrawlableDatasetSorter;
import thredds.crawlabledataset.CrawlableDatasetLabeler;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.InvService;
import thredds.catalog.InvCatalogFactory;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import java.io.File;
import thredds.crawlabledataset.CrawlableDataset;
import org.slf4j.Logger;

public class CatGenAndWrite
{
    private static Logger log;
    private CatalogBuilder catBuilder;
    private CrawlableDataset collectionCrDs;
    private CrawlableDataset topCatCrDs;
    private File topCatWriteDir;
    private CrawlableDatasetFilter collectionOnlyFilter;
    private InvCatalogFactory factory;
    
    public CatGenAndWrite(final String collectionId, final String collectionTitle, final String collectionUrlId, final InvService service, final CrawlableDataset collectionCrDs, final CrawlableDataset topCatCrDs, final CrawlableDatasetFilter filter, final InvDatasetImpl topLevelMetadataContainer, final File catWriteDir) {
        final String topCatWritePath = topCatCrDs.getPath().substring(collectionCrDs.getPath().length());
        final File topCatWriteDir = new File(catWriteDir, topCatWritePath);
        if (!topCatWriteDir.exists() && !topCatWriteDir.mkdirs()) {
            CatGenAndWrite.log.error("CatGenAndWrite(): could not create directory(s) for " + topCatWriteDir.getPath());
            throw new IllegalArgumentException("Could not create directory(s) for " + topCatWriteDir.getPath());
        }
        this.topCatWriteDir = topCatWriteDir;
        this.catBuilder = new StandardCatalogBuilder(collectionUrlId, collectionTitle, collectionCrDs, filter, service, collectionId, null, null, true, null, null, null, topLevelMetadataContainer, null);
        this.collectionCrDs = collectionCrDs;
        this.topCatCrDs = topCatCrDs;
        this.collectionOnlyFilter = new CollectionOnlyCrDsFilter(filter);
        this.factory = InvCatalogFactory.getDefaultFactory(false);
    }
    
    public static void main1(final String[] args) {
        String collectionPath = "C:/Ethan/data/mlode";
        String startPath = "grid/NCEP";
        String catWriteDirPath = "C:/Ethan/data/tmpTest";
        if (args.length == 3) {
            collectionPath = args[0];
            startPath = args[1];
            catWriteDirPath = args[2];
        }
        final File catWriteDir = new File(catWriteDirPath);
        final File collectionFile = new File(collectionPath);
        final CrawlableDataset collectionCrDs = new CrawlableDatasetFile(collectionFile);
        final InvService service = new InvService("myServer", "File", collectionCrDs.getPath() + "/", null, null);
        final CrawlableDatasetFilter filter = null;
        final CrawlableDataset topCatCrDs = collectionCrDs.getDescendant(startPath);
        final CatGenAndWrite cgaw = new CatGenAndWrite("DATA", "My data", "", service, collectionCrDs, topCatCrDs, filter, null, catWriteDir);
        try {
            cgaw.genCatAndSubCats(topCatCrDs);
        }
        catch (IOException e) {
            CatGenAndWrite.log.error("I/O error generating and writing catalogs at and under \"" + topCatCrDs.getPath() + "\": " + e.getMessage());
        }
    }
    
    public static void main(final String[] args) {
        String collectionPath = "C:/Ethan/data/mlode";
        String startPath = "grid/NCEP";
        String catWriteDirPath = "C:/Ethan/code/svnThredds/tds/content/thredds/catGenAndWrite";
        if (args.length == 3) {
            collectionPath = args[0];
            startPath = args[1];
            catWriteDirPath = args[2];
        }
        final File catWriteDir = new File(catWriteDirPath);
        final File collectionFile = new File(collectionPath);
        final CrawlableDataset collectionCrDs = new CrawlableDatasetFile(collectionFile);
        final InvService service = new InvService("myServer", "OPENDAP", "/thredds/dodsC/", null, null);
        final CrawlableDatasetFilter filter = null;
        final CrawlableDataset topCatCrDs = collectionCrDs.getDescendant(startPath);
        final CatGenAndWrite cgaw = new CatGenAndWrite("DATA", "My data", "mlode", service, collectionCrDs, topCatCrDs, filter, null, catWriteDir);
        try {
            cgaw.genCatAndSubCats(topCatCrDs);
        }
        catch (IOException e) {
            CatGenAndWrite.log.error("I/O error generating and writing catalogs at and under \"" + topCatCrDs.getPath() + "\": " + e.getMessage());
        }
    }
    
    public void genAndWriteCatalogTree() throws IOException {
        this.genCatAndSubCats(this.topCatCrDs);
    }
    
    private void genCatAndSubCats(final CrawlableDataset catCrDs) throws IOException {
        final String catWritePath = catCrDs.getPath().substring(this.topCatCrDs.getPath().length());
        final File catWriteDir = new File(this.topCatWriteDir, catWritePath);
        if (!catWriteDir.exists() && !catWriteDir.mkdirs()) {
            CatGenAndWrite.log.error("genCatAndSubCats(): could not create directory(s) for " + catWriteDir.getPath());
            throw new IOException("Could not create directory(s) for " + catWriteDir.getPath());
        }
        final File catFile = new File(catWriteDir, "catalog.xml");
        final InvCatalogImpl cat = this.catBuilder.generateCatalog(catCrDs);
        this.factory.writeXML(cat, catFile.getAbsolutePath());
        final List collectionChildren = catCrDs.listDatasets(this.collectionOnlyFilter);
        for (final CrawlableDataset curCrDs : collectionChildren) {
            this.genCatAndSubCats(curCrDs);
        }
    }
    
    static {
        CatGenAndWrite.log = LoggerFactory.getLogger(CatGenAndWrite.class);
    }
    
    static class CollectionOnlyCrDsFilter implements CrawlableDatasetFilter
    {
        private CrawlableDatasetFilter filter;
        
        CollectionOnlyCrDsFilter(final CrawlableDatasetFilter filter) {
            this.filter = filter;
        }
        
        public boolean accept(final CrawlableDataset dataset) {
            return dataset.isCollection() && (this.filter == null || this.filter.accept(dataset));
        }
        
        public Object getConfigObject() {
            return null;
        }
    }
}
