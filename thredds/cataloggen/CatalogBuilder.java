// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import org.jdom.Document;
import thredds.catalog.InvCatalogImpl;
import java.io.IOException;
import thredds.crawlabledataset.CrawlableDataset;

public interface CatalogBuilder
{
    CrawlableDataset requestCrawlableDataset(final String p0) throws IOException;
    
    InvCatalogImpl generateCatalog(final CrawlableDataset p0) throws IOException;
    
    InvCatalogImpl generateProxyDsResolverCatalog(final CrawlableDataset p0, final ProxyDatasetHandler p1) throws IOException;
    
    Document generateCatalogAsDocument(final CrawlableDataset p0) throws IOException;
    
    String generateCatalogAsString(final CrawlableDataset p0) throws IOException;
}
