// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import org.slf4j.LoggerFactory;
import thredds.catalog.InvCatalog;
import org.jdom.Document;
import thredds.catalog.InvDataset;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import thredds.catalog.InvCatalogImpl;
import java.io.IOException;
import thredds.catalog.InvDatasetImpl;
import java.util.List;
import java.util.Map;
import thredds.crawlabledataset.CrawlableDatasetSorter;
import thredds.crawlabledataset.CrawlableDatasetLabeler;
import thredds.catalog.InvService;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import thredds.crawlabledataset.CrawlableDataset;
import org.slf4j.Logger;

public class StandardCatalogBuilder implements CatalogBuilder
{
    private static Logger log;
    private String collectionPath;
    private String collectionName;
    private CrawlableDataset collectionCrDs;
    private CrawlableDatasetFilter filter;
    private InvService service;
    private String collectionId;
    private CrawlableDatasetLabeler identifier;
    private CrawlableDatasetLabeler namer;
    private boolean doAddDatasetSize;
    private CrawlableDatasetSorter sorter;
    private Map proxyDatasetHandlers;
    private List childEnhancerList;
    private InvDatasetImpl topLevelMetadataContainer;
    private CatalogRefExpander catalogRefExpander;
    
    public StandardCatalogBuilder(final String collectionPath, final String collectionName, final CrawlableDataset collectionCrDs, final CrawlableDatasetFilter filter, final InvService service, final String collectionId, final CrawlableDatasetLabeler identifier, final CrawlableDatasetLabeler namer, final boolean doAddDatasetSize, final CrawlableDatasetSorter sorter, final Map proxyDatasetHandlers, final List childEnhancerList, final InvDatasetImpl topLevelMetadataContainer, final CatalogRefExpander catalogRefExpander) {
        this.collectionPath = collectionPath;
        this.collectionName = collectionName;
        this.collectionCrDs = collectionCrDs;
        this.filter = filter;
        this.service = service;
        this.collectionId = collectionId;
        this.identifier = identifier;
        this.namer = namer;
        this.doAddDatasetSize = doAddDatasetSize;
        this.sorter = sorter;
        this.proxyDatasetHandlers = proxyDatasetHandlers;
        this.childEnhancerList = childEnhancerList;
        this.topLevelMetadataContainer = topLevelMetadataContainer;
        this.catalogRefExpander = catalogRefExpander;
    }
    
    public CrawlableDataset requestCrawlableDataset(final String path) throws IOException {
        final CrawlableDataset crDs = CatalogBuilderHelper.verifyDescendantDataset(this.collectionCrDs, path, this.filter);
        if (crDs != null) {
            return crDs;
        }
        final int indexLastSlash = path.lastIndexOf("/");
        String dsName;
        String parentPath;
        if (indexLastSlash != -1) {
            dsName = path.substring(indexLastSlash + 1);
            parentPath = path.substring(0, indexLastSlash);
        }
        else {
            dsName = path;
            parentPath = "";
        }
        final CrawlableDataset parentCrDs = CatalogBuilderHelper.verifyDescendantDataset(this.collectionCrDs, parentPath, this.filter);
        if (parentCrDs == null) {
            StandardCatalogBuilder.log.debug("requestCrawlableDataset(): Parent dataset <" + parentPath + "> not allowed by filter.");
            return null;
        }
        if (!parentCrDs.isCollection()) {
            StandardCatalogBuilder.log.debug("requestCrawlableDataset(): Parent dataset <" + parentPath + "> is not a collection dataset.");
            return null;
        }
        final ProxyDatasetHandler pdh = this.proxyDatasetHandlers.get(dsName);
        if (pdh == null) {
            StandardCatalogBuilder.log.debug("requestCrawlableDataset(): Dataset name <" + dsName + "> has no corresponding proxy dataset handler.");
            return null;
        }
        if (pdh.isProxyDatasetResolver()) {
            StandardCatalogBuilder.log.debug("requestCrawlableDataset(): Proxy dataset <" + dsName + "> is a resolver, not valid dataset request.");
            return null;
        }
        final CollectionLevelScanner scanner = this.setupAndScan(parentCrDs, null);
        final List atomicDsInfo = scanner.getAtomicDsInfo();
        final InvCrawlablePair dsInfo = pdh.getActualDataset(atomicDsInfo);
        return dsInfo.getCrawlableDataset();
    }
    
    public InvCatalogImpl generateCatalog(final CrawlableDataset catalogCrDs) throws IOException {
        final CollectionLevelScanner scanner = this.setupAndScan(catalogCrDs, null);
        final InvCatalogImpl catalog = scanner.generateCatalog();
        boolean needFinish = false;
        if (this.catalogRefExpander != null) {
            final List catRefInfoList = new ArrayList(scanner.getCatRefInfo());
            while (catRefInfoList.size() > 0) {
                final InvCrawlablePair curCatRefInfo = catRefInfoList.get(0);
                if (this.catalogRefExpander.expandCatalogRef(curCatRefInfo)) {
                    final CollectionLevelScanner curScanner = new CollectionLevelScanner(this.collectionPath, this.collectionCrDs, catalogCrDs, curCatRefInfo.getCrawlableDataset(), this.filter, this.service);
                    curScanner.setCollectionId(this.collectionId);
                    curScanner.setIdentifier(this.identifier);
                    curScanner.setNamer(this.namer);
                    curScanner.setDoAddDataSize(this.doAddDatasetSize);
                    curScanner.setSorter(this.sorter);
                    curScanner.setProxyDsHandlers(this.proxyDatasetHandlers);
                    if (this.childEnhancerList != null) {
                        final Iterator it = this.childEnhancerList.iterator();
                        while (it.hasNext()) {
                            curScanner.addChildEnhancer(it.next());
                        }
                    }
                    curScanner.scan();
                    final InvCatalogImpl curCatalog = curScanner.generateCatalog();
                    catRefInfoList.addAll(curScanner.getCatRefInfo());
                    final InvDataset curTopDs = curCatalog.getDataset();
                    final InvDataset targetDs = curCatRefInfo.getInvDataset();
                    final InvDataset targetParentDs = curCatRefInfo.getInvDataset().getParent();
                    ((InvDatasetImpl)targetParentDs).removeDataset((InvDatasetImpl)targetDs);
                    ((InvDatasetImpl)targetParentDs).addDataset((InvDatasetImpl)curTopDs);
                    needFinish = true;
                }
                catRefInfoList.remove(0);
            }
        }
        if (needFinish) {
            catalog.finish();
        }
        return catalog;
    }
    
    public InvCatalogImpl generateProxyDsResolverCatalog(final CrawlableDataset catalogCrDs, final ProxyDatasetHandler pdh) throws IOException {
        if (catalogCrDs == null || pdh == null) {
            throw new IllegalArgumentException("Null parameters not allowed.");
        }
        if (!this.proxyDatasetHandlers.containsValue(pdh)) {
            throw new IllegalArgumentException("Unknown ProxyDatasetHandler.");
        }
        final CollectionLevelScanner scanner = this.setupAndScan(catalogCrDs, null);
        return scanner.generateProxyDsResolverCatalog(pdh);
    }
    
    private CollectionLevelScanner setupAndScan(final CrawlableDataset catalogCrDs, final CrawlableDataset currentCrDs) throws IOException {
        final CollectionLevelScanner scanner = new CollectionLevelScanner(this.collectionPath, this.collectionCrDs, catalogCrDs, currentCrDs, this.filter, this.service);
        scanner.setCollectionId(this.collectionId);
        scanner.setCollectionName(this.collectionName);
        scanner.setIdentifier(this.identifier);
        scanner.setNamer(this.namer);
        scanner.setDoAddDataSize(this.doAddDatasetSize);
        scanner.setSorter(this.sorter);
        scanner.setProxyDsHandlers(this.proxyDatasetHandlers);
        if (this.childEnhancerList != null) {
            final Iterator it = this.childEnhancerList.iterator();
            while (it.hasNext()) {
                scanner.addChildEnhancer(it.next());
            }
        }
        scanner.setTopLevelMetadataContainer(this.topLevelMetadataContainer);
        scanner.scan();
        return scanner;
    }
    
    public Document generateCatalogAsDocument(final CrawlableDataset catalogCrDs) throws IOException {
        return CatalogBuilderHelper.convertCatalogToDocument(this.generateCatalog(catalogCrDs));
    }
    
    public String generateCatalogAsString(final CrawlableDataset catalogCrDs) throws IOException {
        return CatalogBuilderHelper.convertCatalogToString(this.generateCatalog(catalogCrDs));
    }
    
    static {
        StandardCatalogBuilder.log = LoggerFactory.getLogger(StandardCatalogBuilder.class);
    }
}
