// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import java.io.IOException;
import thredds.catalog.InvCatalogConvertIF;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.parser.jdom.InvCatalogFactory10;
import thredds.catalog.InvCatalogFactory;
import org.jdom.Document;
import thredds.catalog.InvCatalog;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import thredds.crawlabledataset.CrawlableDataset;

class CatalogBuilderHelper
{
    static CrawlableDataset verifyDescendantDataset(final CrawlableDataset ancestorCrDs, final String path, final CrawlableDatasetFilter filter) {
        if (!ancestorCrDs.isCollection()) {
            throw new IllegalArgumentException("Ancestor dataset <" + ancestorCrDs.getPath() + "> not a collection.");
        }
        if (!path.startsWith(ancestorCrDs.getPath())) {
            throw new IllegalArgumentException("Dataset path <" + path + "> not descendant of given dataset <" + ancestorCrDs.getPath() + ">.");
        }
        if (path.length() == ancestorCrDs.getPath().length()) {
            return ancestorCrDs;
        }
        String remainingPath = path.substring(ancestorCrDs.getPath().length());
        if (remainingPath.startsWith("/")) {
            remainingPath = remainingPath.substring(1);
        }
        final String[] pathSegments = remainingPath.split("/");
        CrawlableDataset curCrDs = ancestorCrDs;
        for (int i = 0; i < pathSegments.length; ++i) {
            curCrDs = curCrDs.getDescendant(pathSegments[i]);
            if (filter != null && !filter.accept(curCrDs)) {
                return null;
            }
        }
        if (!curCrDs.exists()) {
            return null;
        }
        return curCrDs;
    }
    
    static Document convertCatalogToDocument(final InvCatalog catalog) {
        final InvCatalogFactory fac = InvCatalogFactory.getDefaultFactory(false);
        final InvCatalogConvertIF converter = fac.getCatalogConverter("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0");
        final InvCatalogFactory10 fac2 = (InvCatalogFactory10)converter;
        return fac2.writeCatalog((InvCatalogImpl)catalog);
    }
    
    static String convertCatalogToString(final InvCatalog catalog) {
        final InvCatalogFactory fac = InvCatalogFactory.getDefaultFactory(false);
        try {
            return fac.writeXML((InvCatalogImpl)catalog);
        }
        catch (IOException e) {
            return null;
        }
    }
}
