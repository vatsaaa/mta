// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen;

import thredds.catalog.ServiceType;
import java.util.Date;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.MetadataConverterIF;
import thredds.catalog.InvMetadata;
import thredds.catalog.ThreddsMetadata;
import ucar.nc2.constants.FeatureType;
import java.net.URI;
import thredds.catalog.InvAccess;
import java.io.IOException;
import java.util.Iterator;
import thredds.catalog.InvCatalog;
import thredds.catalog.InvDataset;
import java.util.HashMap;
import java.util.Collections;
import java.util.ArrayList;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvDatasetImpl;
import java.util.List;
import thredds.crawlabledataset.CrawlableDatasetLabeler;
import java.util.Map;
import thredds.crawlabledataset.CrawlableDatasetSorter;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import thredds.catalog.InvService;
import thredds.crawlabledataset.CrawlableDataset;

public class CollectionLevelScanner
{
    private final String collectionPath;
    private String collectionName;
    private String collectionId;
    private final CrawlableDataset collectionLevel;
    private final CrawlableDataset catalogLevel;
    private final CrawlableDataset currentLevel;
    private final InvService service;
    private final CrawlableDatasetFilter filter;
    private CrawlableDatasetSorter sorter;
    private Map proxyDsHandlers;
    private CrawlableDatasetLabeler identifier;
    private CrawlableDatasetLabeler namer;
    private boolean doAddDataSize;
    private boolean addLastModified;
    private List childEnhancerList;
    private InvDatasetImpl topLevelMetadataContainer;
    private List catRefInfo;
    private List atomicDsInfo;
    private List proxyDsInfo;
    private InvCatalogImpl genCatalog;
    private int state;
    
    public CollectionLevelScanner(final String collectionPath, final CrawlableDataset collectionLevel, final CrawlableDataset catalogLevel, final CrawlableDataset currentLevel, final CrawlableDatasetFilter filter, final InvService service) {
        this.collectionId = null;
        this.doAddDataSize = false;
        this.addLastModified = true;
        this.collectionPath = ((collectionPath != null) ? collectionPath : "");
        if (collectionLevel == null) {
            throw new IllegalArgumentException("The collection root must not be null.");
        }
        if (catalogLevel == null) {
            throw new IllegalArgumentException("The catalog root must not be null.");
        }
        if (service == null) {
            throw new IllegalArgumentException("The service must not be null.");
        }
        if (!catalogLevel.getPath().startsWith(collectionLevel.getPath())) {
            throw new IllegalArgumentException("The catalog root <" + catalogLevel.getPath() + "> must be under the collection root <" + collectionLevel.getPath() + ">.");
        }
        if (!collectionLevel.isCollection()) {
            throw new IllegalArgumentException("The collection root <" + collectionLevel.getPath() + "> must be a collection.");
        }
        if (!catalogLevel.isCollection()) {
            throw new IllegalArgumentException("The catalog root <" + catalogLevel.getPath() + "> must be a collection.");
        }
        this.collectionLevel = collectionLevel;
        this.catalogLevel = catalogLevel;
        if (currentLevel != null) {
            if (!currentLevel.isCollection()) {
                throw new IllegalArgumentException("The current root <" + currentLevel.getPath() + "> must be a collection.");
            }
            if (!currentLevel.getPath().startsWith(catalogLevel.getPath())) {
                throw new IllegalArgumentException("The current root <" + currentLevel.getPath() + "> must be under the catalog root <" + catalogLevel.getPath() + ">.");
            }
            this.currentLevel = currentLevel;
        }
        else {
            this.currentLevel = catalogLevel;
        }
        this.filter = filter;
        this.service = service;
        this.childEnhancerList = new ArrayList();
        this.catRefInfo = new ArrayList();
        this.atomicDsInfo = new ArrayList();
        this.proxyDsInfo = new ArrayList();
        this.collectionId = null;
        this.collectionName = null;
        this.state = 0;
    }
    
    public CollectionLevelScanner(final CollectionLevelScanner cs) {
        this(cs.collectionPath, cs.collectionLevel, cs.catalogLevel, cs.currentLevel, cs.filter, cs.service);
        this.setSorter(cs.getSorter());
        this.setProxyDsHandlers(cs.getProxyDsHandlers());
        this.setCollectionId(cs.getCollectionId());
        this.setCollectionName(cs.getCollectionName());
        this.setIdentifier(cs.getIdentifier());
        this.setNamer(cs.getNamer());
        this.setDoAddDataSize(cs.getDoAddDataSize());
        this.childEnhancerList.add(cs.getChildEnhancerList());
    }
    
    public CrawlableDatasetSorter getSorter() {
        return this.sorter;
    }
    
    public void setSorter(final CrawlableDatasetSorter sorter) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        this.sorter = sorter;
    }
    
    public Map getProxyDsHandlers() {
        if (this.proxyDsHandlers.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        return Collections.unmodifiableMap((Map<?, ?>)this.proxyDsHandlers);
    }
    
    public void setProxyDsHandlers(final Map proxyDsHandlers) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        if (proxyDsHandlers == null) {
            this.proxyDsHandlers = Collections.EMPTY_MAP;
        }
        else {
            this.proxyDsHandlers = new HashMap(proxyDsHandlers);
        }
    }
    
    public void setCollectionId(final String collectionId) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        this.collectionId = collectionId;
    }
    
    protected String getCollectionId() {
        return this.collectionId;
    }
    
    public void setCollectionName(final String collectionName) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        this.collectionName = collectionName;
    }
    
    protected String getCollectionName() {
        return this.collectionName;
    }
    
    public void setIdentifier(final CrawlableDatasetLabeler identifier) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        this.identifier = identifier;
    }
    
    protected CrawlableDatasetLabeler getIdentifier() {
        return this.identifier;
    }
    
    public void setNamer(final CrawlableDatasetLabeler namer) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        this.namer = namer;
    }
    
    protected CrawlableDatasetLabeler getNamer() {
        return this.namer;
    }
    
    public void setDoAddDataSize(final boolean doAddDataSize) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        this.doAddDataSize = doAddDataSize;
    }
    
    protected boolean getDoAddDataSize() {
        return this.doAddDataSize;
    }
    
    public void addChildEnhancer(final DatasetEnhancer childEnhancer) {
        if (this.state != 0) {
            throw new IllegalStateException("No further setup allowed, scan in progress or completed.");
        }
        this.childEnhancerList.add(childEnhancer);
    }
    
    List getChildEnhancerList() {
        return Collections.unmodifiableList((List<?>)this.childEnhancerList);
    }
    
    public void setTopLevelMetadataContainer(final InvDatasetImpl topLevelMetadataContainer) {
        this.topLevelMetadataContainer = topLevelMetadataContainer;
    }
    
    public void scan() throws IOException {
        if (this.state == 1) {
            throw new IllegalStateException("Scan already underway.");
        }
        if (this.state >= 2) {
            throw new IllegalStateException("Scan has already been generated.");
        }
        this.state = 1;
        if (this.proxyDsHandlers == null) {
            this.proxyDsHandlers = Collections.EMPTY_MAP;
        }
        this.genCatalog = this.createSkeletonCatalog(this.currentLevel);
        final InvDatasetImpl topInvDs = this.genCatalog.getDatasets().get(0);
        final List crDsList = this.currentLevel.listDatasets(this.filter);
        if (this.sorter != null) {
            this.sorter.sort(crDsList);
        }
        for (int i = 0; i < crDsList.size(); ++i) {
            final CrawlableDataset curCrDs = crDsList.get(i);
            final InvDatasetImpl curInvDs = this.createInvDatasetFromCrawlableDataset(curCrDs, topInvDs, null);
            final InvCrawlablePair dsInfo = new InvCrawlablePair(curCrDs, curInvDs);
            if (curCrDs.isCollection()) {
                this.catRefInfo.add(dsInfo);
            }
            else {
                this.atomicDsInfo.add(dsInfo);
            }
            topInvDs.addDataset(curInvDs);
        }
        this.genCatalog.finish();
        if (this.atomicDsInfo.size() > 0) {
            boolean anyProxiesAdded = false;
            for (final ProxyDatasetHandler curProxy : this.proxyDsHandlers.values()) {
                final InvService proxyService = curProxy.getProxyDatasetService(this.currentLevel);
                if (proxyService != null) {
                    final CrawlableDataset crDsToAdd = curProxy.createProxyDataset(this.currentLevel);
                    final InvDatasetImpl invDsToAdd = this.createInvDatasetFromCrawlableDataset(crDsToAdd, topInvDs, proxyService);
                    final InvCrawlablePair dsInfo2 = new InvCrawlablePair(crDsToAdd, invDsToAdd);
                    this.proxyDsInfo.add(dsInfo2);
                    final int index = curProxy.getProxyDatasetLocation(this.currentLevel, topInvDs.getDatasets().size());
                    topInvDs.addDataset(index, invDsToAdd);
                    this.genCatalog.addService(proxyService);
                    anyProxiesAdded = true;
                }
            }
            if (anyProxiesAdded) {
                this.genCatalog.finish();
            }
        }
        this.addTopLevelMetadata(this.genCatalog, true);
        this.state = 2;
    }
    
    List getCatRefInfo() {
        if (this.state != 2) {
            throw new IllegalStateException("Scan has not been performed.");
        }
        return Collections.unmodifiableList((List<?>)this.catRefInfo);
    }
    
    List getAtomicDsInfo() {
        if (this.state != 2) {
            throw new IllegalStateException("Scan has not been performed.");
        }
        return Collections.unmodifiableList((List<?>)this.atomicDsInfo);
    }
    
    public InvCatalogImpl generateCatalog() throws IOException {
        if (this.state != 2) {
            throw new IllegalStateException("Scan has not been performed.");
        }
        return this.genCatalog;
    }
    
    public InvCatalogImpl generateProxyDsResolverCatalog(final ProxyDatasetHandler pdh) {
        if (this.state != 2) {
            throw new IllegalStateException("Scan has not been performed.");
        }
        if (!this.proxyDsHandlers.containsValue(pdh)) {
            throw new IllegalArgumentException("Unknown ProxyDatasetHandler.");
        }
        final InvCatalogImpl catalog = this.createSkeletonCatalog(this.currentLevel);
        final InvDatasetImpl topDs = catalog.getDatasets().get(0);
        final InvCrawlablePair actualDsInfo = pdh.getActualDataset(this.atomicDsInfo);
        final InvDatasetImpl actualInvDs = (InvDatasetImpl)actualDsInfo.getInvDataset();
        actualInvDs.setName(pdh.getActualDatasetName(actualDsInfo, topDs.getName()));
        catalog.removeDataset(topDs);
        catalog.addDataset(actualInvDs);
        catalog.finish();
        this.addTopLevelMetadata(catalog, false);
        return catalog;
    }
    
    private void addTopLevelMetadata(final InvCatalog catalog, final boolean isRegularCatalog) {
        if (this.topLevelMetadataContainer == null) {
            return;
        }
        if (!this.catalogLevel.getPath().equals(this.currentLevel.getPath())) {
            return;
        }
        final InvDatasetImpl topInvDs = (InvDatasetImpl)catalog.getDataset();
        topInvDs.transferMetadata(this.topLevelMetadataContainer, true);
        for (final InvAccess invAccess : this.topLevelMetadataContainer.getAccess()) {
            topInvDs.addAccess(invAccess);
            final InvService s = invAccess.getService();
            ((InvCatalogImpl)catalog).addService(s);
        }
        final boolean isCollectionLevel = this.catalogLevel.getPath().equals(this.collectionLevel.getPath());
        if (isCollectionLevel && isRegularCatalog) {
            topInvDs.setHarvest(this.topLevelMetadataContainer.isHarvest());
            topInvDs.setCollectionType(this.topLevelMetadataContainer.getCollectionType());
        }
        ((InvCatalogImpl)catalog).finish();
    }
    
    private InvCatalogImpl createSkeletonCatalog(final CrawlableDataset topCrDs) {
        final InvCatalogImpl catalog = new InvCatalogImpl(null, null, null);
        catalog.addService(this.service);
        String newName = null;
        if (this.namer != null) {
            newName = this.namer.getLabel(topCrDs);
        }
        if (newName == null && this.collectionLevel.getPath().equals(this.catalogLevel.getPath())) {
            newName = this.collectionName;
        }
        if (newName == null) {
            newName = this.getName(topCrDs);
        }
        final InvDatasetImpl topDs = new InvDatasetImpl(null, newName, null, null, null);
        String newId = null;
        if (this.identifier != null) {
            newId = this.identifier.getLabel(topCrDs);
        }
        else {
            newId = this.getID(topCrDs);
        }
        if (newId != null) {
            topDs.setID(newId);
        }
        final ThreddsMetadata tm = new ThreddsMetadata(false);
        tm.setServiceName(this.service.getName());
        final InvMetadata md = new InvMetadata(topDs, null, "http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", "", true, true, null, tm);
        final ThreddsMetadata tm2 = new ThreddsMetadata(false);
        tm2.addMetadata(md);
        topDs.setLocalMetadata(tm2);
        catalog.addDataset(topDs);
        return catalog;
    }
    
    private InvDatasetImpl createInvDatasetFromCrawlableDataset(final CrawlableDataset crawlableDs, final InvDatasetImpl parentInvDs, final InvService service) {
        String newName = null;
        if (this.namer != null) {
            newName = this.namer.getLabel(crawlableDs);
        }
        if (newName == null) {
            newName = this.getName(crawlableDs);
        }
        InvDatasetImpl curInvDs;
        if (crawlableDs.isCollection()) {
            curInvDs = new InvCatalogRef(parentInvDs, newName, this.getXlinkHref(crawlableDs));
        }
        else {
            curInvDs = new InvDatasetImpl(parentInvDs, newName, null, (service != null) ? service.getName() : null, this.getUrlPath(crawlableDs, service));
            if (this.doAddDataSize && crawlableDs.length() != -1L) {
                curInvDs.setDataSize((double)crawlableDs.length());
            }
            if (this.addLastModified) {
                final Date lastModDate = crawlableDs.lastModified();
                if (lastModDate != null) {
                    curInvDs.setLastModifiedDate(lastModDate);
                }
            }
        }
        String newId = null;
        if (this.identifier != null) {
            newId = this.identifier.getLabel(crawlableDs);
        }
        else {
            newId = this.getID(crawlableDs);
        }
        if (newId != null) {
            curInvDs.setID(newId);
        }
        final Iterator it2 = this.childEnhancerList.iterator();
        while (it2.hasNext()) {
            it2.next().addMetadata(curInvDs, crawlableDs);
        }
        return curInvDs;
    }
    
    private String getName(final CrawlableDataset dataset) {
        return dataset.getName();
    }
    
    private String getID(final CrawlableDataset dataset) {
        if (dataset == null) {
            return null;
        }
        if (this.collectionId == null) {
            return null;
        }
        final int i = this.collectionLevel.getPath().length();
        String id = dataset.getPath().substring(i);
        if (id.startsWith("/")) {
            id = id.substring(1);
        }
        if (this.collectionId.equals("")) {
            if (id.equals("")) {
                return null;
            }
            return id;
        }
        else {
            if (id.equals("")) {
                return this.collectionId;
            }
            return this.collectionId + "/" + id;
        }
    }
    
    private String getUrlPath(final CrawlableDataset dataset, final InvService service) {
        final InvService serviceForThisDs = (service != null) ? service : this.service;
        if (serviceForThisDs.getBase().equals("") && !serviceForThisDs.getServiceType().equals(ServiceType.COMPOUND)) {
            String urlPath = dataset.getPath().substring(this.catalogLevel.getPath().length());
            if (urlPath.startsWith("/")) {
                urlPath = urlPath.substring(1);
            }
            return urlPath;
        }
        if (serviceForThisDs.isRelativeBase()) {
            String relPath = dataset.getPath().substring(this.collectionLevel.getPath().length());
            if (relPath.startsWith("/")) {
                relPath = relPath.substring(1);
            }
            return (this.collectionPath.equals("") ? "" : (this.collectionPath + "/")) + relPath;
        }
        String relPath = dataset.getPath().substring(this.collectionLevel.getPath().length());
        if (relPath.startsWith("/")) {
            relPath = relPath.substring(1);
        }
        return relPath;
    }
    
    private String getXlinkHref(final CrawlableDataset dataset) {
        String path = dataset.getPath().substring(this.catalogLevel.getPath().length());
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        if (path.endsWith("/")) {
            path += "catalog.xml";
        }
        else {
            path += "/catalog.xml";
        }
        return path;
    }
}
