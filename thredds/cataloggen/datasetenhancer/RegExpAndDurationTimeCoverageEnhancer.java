// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.datasetenhancer;

import org.slf4j.LoggerFactory;
import java.util.regex.Matcher;
import ucar.nc2.units.DateRange;
import ucar.nc2.units.TimeDuration;
import ucar.nc2.units.DateType;
import thredds.catalog.InvDatasetImpl;
import thredds.crawlabledataset.CrawlableDataset;
import thredds.catalog.InvDataset;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import thredds.cataloggen.DatasetEnhancer;

public class RegExpAndDurationTimeCoverageEnhancer implements DatasetEnhancer
{
    private static Logger log;
    private final String matchPattern;
    private final String substitutionPattern;
    private final String duration;
    private final MatchTarget matchTarget;
    private Pattern pattern;
    
    public static RegExpAndDurationTimeCoverageEnhancer getInstanceToMatchOnDatasetName(final String matchPattern, final String substitutionPattern, final String duration) {
        return new RegExpAndDurationTimeCoverageEnhancer(matchPattern, substitutionPattern, duration, MatchTarget.DATASET_NAME);
    }
    
    public static RegExpAndDurationTimeCoverageEnhancer getInstanceToMatchOnDatasetPath(final String matchPattern, final String substitutionPattern, final String duration) {
        return new RegExpAndDurationTimeCoverageEnhancer(matchPattern, substitutionPattern, duration, MatchTarget.DATASET_PATH);
    }
    
    private RegExpAndDurationTimeCoverageEnhancer(final String matchPattern, final String substitutionPattern, final String duration, final MatchTarget matchTarget) {
        if (matchPattern == null) {
            throw new IllegalArgumentException("Null match pattern not allowed.");
        }
        if (substitutionPattern == null) {
            throw new IllegalArgumentException("Null substitution pattern not allowed.");
        }
        if (duration == null) {
            throw new IllegalArgumentException("Null duration not allowed.");
        }
        if (matchTarget == null) {
            throw new IllegalArgumentException("Null match target not allowed.");
        }
        this.matchPattern = matchPattern;
        this.substitutionPattern = substitutionPattern;
        this.duration = duration;
        this.matchTarget = matchTarget;
        try {
            this.pattern = Pattern.compile(matchPattern);
        }
        catch (PatternSyntaxException e) {
            RegExpAndDurationTimeCoverageEnhancer.log.error("ctor(): bad match pattern <" + this.matchPattern + ">, failed to compile: " + e.getMessage());
            this.pattern = null;
        }
    }
    
    public MatchTarget getMatchTarget() {
        return this.matchTarget;
    }
    
    public String getMatchPattern() {
        return this.matchPattern;
    }
    
    public String getSubstitutionPattern() {
        return this.substitutionPattern;
    }
    
    public String getDuration() {
        return this.duration;
    }
    
    public Object getConfigObject() {
        return null;
    }
    
    public boolean addMetadata(final InvDataset dataset, final CrawlableDataset crDataset) {
        if (this.pattern == null) {
            RegExpAndDurationTimeCoverageEnhancer.log.error("addMetadata(): bad match pattern <" + this.matchPattern + ">.");
            return false;
        }
        String matchTargetString;
        if (this.matchTarget.equals(MatchTarget.DATASET_NAME)) {
            matchTargetString = crDataset.getName();
        }
        else {
            if (!this.matchTarget.equals(MatchTarget.DATASET_PATH)) {
                throw new IllegalStateException("Unknown match target [" + this.matchTarget.toString() + "].");
            }
            matchTargetString = crDataset.getPath();
        }
        final Matcher matcher = this.pattern.matcher(matchTargetString);
        if (!matcher.find()) {
            return false;
        }
        final StringBuffer startTime = new StringBuffer();
        try {
            matcher.appendReplacement(startTime, this.substitutionPattern);
        }
        catch (IndexOutOfBoundsException e) {
            RegExpAndDurationTimeCoverageEnhancer.log.error("addMetadata(): capture group mismatch between match pattern <" + this.matchPattern + "> and substitution pattern <" + this.substitutionPattern + ">: " + e.getMessage());
            return false;
        }
        startTime.delete(0, matcher.start());
        try {
            ((InvDatasetImpl)dataset).setTimeCoverage(new DateRange(new DateType(startTime.toString(), null, null), null, new TimeDuration(this.duration), null));
        }
        catch (Exception e2) {
            RegExpAndDurationTimeCoverageEnhancer.log.warn("addMetadata(): Start time <" + startTime.toString() + "> or duration <" + this.duration + "> not parsable" + " (crDataset.getName() <" + crDataset.getName() + ">, this.matchPattern() <" + this.matchPattern + ">, this.substitutionPattern() <" + this.substitutionPattern + ">): " + e2.getMessage());
            return false;
        }
        return true;
    }
    
    static {
        RegExpAndDurationTimeCoverageEnhancer.log = LoggerFactory.getLogger(RegExpAndDurationTimeCoverageEnhancer.class);
    }
    
    private enum MatchTarget
    {
        DATASET_NAME, 
        DATASET_PATH;
    }
}
