// 
// Decompiled by Procyon v0.5.36
// 

package thredds.cataloggen.catalogrefexpander;

import thredds.cataloggen.InvCrawlablePair;
import thredds.cataloggen.CatalogRefExpander;

public class BooleanCatalogRefExpander implements CatalogRefExpander
{
    private boolean expandAll;
    
    public BooleanCatalogRefExpander(final boolean expandAll) {
        this.expandAll = false;
        this.expandAll = expandAll;
    }
    
    public boolean expandCatalogRef(final InvCrawlablePair catRefInfo) {
        return this.expandAll;
    }
}
