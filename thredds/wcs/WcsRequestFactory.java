// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs;

import ucar.nc2.dt.GridDataset;

public class WcsRequestFactory
{
    private Request request;
    private String versionString;
    private Request.Operation operation;
    private GridDataset dataset;
    
    public static WcsRequestFactory newWcsRequestFactory(final String versionString, final Request.Operation operation, final GridDataset dataset) {
        return new WcsRequestFactory(versionString, operation, dataset);
    }
    
    private WcsRequestFactory(final String versionString, final Request.Operation operation, final GridDataset dataset) {
        if (versionString == null) {
            throw new IllegalArgumentException("Version may not be null.");
        }
        if (operation == null) {
            throw new IllegalArgumentException("Operation may not be null.");
        }
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset may not be null.");
        }
        this.versionString = versionString;
        this.operation = operation;
        this.dataset = dataset;
    }
    
    public Request getRequest() {
        return this.request;
    }
}
