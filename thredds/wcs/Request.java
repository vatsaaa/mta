// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs;

import ucar.nc2.dt.GridDataset;

public interface Request
{
    String getVersionString();
    
    Operation getOperation();
    
    RequestEncoding getRequestEncoding();
    
    GridDataset getDataset();
    
    String getDatasetPath();
    
    public enum Operation
    {
        GetCapabilities, 
        DescribeCoverage, 
        GetCoverage;
    }
    
    public enum RequestEncoding
    {
        GET_KVP, 
        POST_XML, 
        POST_SOAP;
    }
    
    public enum Format
    {
        NONE(""), 
        GeoTIFF("image/tiff"), 
        GeoTIFF_Float("image/tiff"), 
        NetCDF3("application/x-netcdf");
        
        private String mimeType;
        
        private Format(final String mimeType) {
            this.mimeType = mimeType;
        }
        
        public String getMimeType() {
            return this.mimeType;
        }
        
        public static Format getFormat(final String mimeType) {
            for (final Format curSection : values()) {
                if (curSection.mimeType.equals(mimeType)) {
                    return curSection;
                }
            }
            throw new IllegalArgumentException("No such instance <" + mimeType + ">.");
        }
    }
    
    public static class BoundingBox
    {
        private int dimensionLength;
        private double[] minPoint;
        private double[] maxPoint;
        private String minPointString;
        private String maxPointString;
        
        public BoundingBox(final double[] minPoint, final double[] maxPoint) {
            if (minPoint.length != maxPoint.length) {
                throw new IllegalArgumentException("The dimension length of the minimum point [" + minPoint.length + "] and maximum point [" + maxPoint.length + "] must be equal.");
            }
            boolean minMaxOk = true;
            final StringBuilder minPSB = new StringBuilder("[");
            final StringBuilder maxPSB = new StringBuilder("[");
            for (int i = 0; i < minPoint.length; ++i) {
                minPSB.append(minPoint[i]).append(",");
                maxPSB.append(maxPoint[i]).append(",");
                if (minPoint[i] >= maxPoint[i]) {
                    minMaxOk = false;
                }
            }
            int indx = minPSB.lastIndexOf(",");
            minPSB.replace(indx, indx + 1, "]");
            indx = maxPSB.lastIndexOf(",");
            maxPSB.replace(indx, indx + 1, "]");
            if (!minMaxOk) {
                throw new IllegalArgumentException("Minimum point " + minPSB.toString() + " not always smaller than maximum point " + maxPSB.toString() + ".");
            }
            this.dimensionLength = minPoint.length;
            this.minPoint = minPoint;
            this.maxPoint = maxPoint;
            this.minPointString = minPSB.toString();
            this.maxPointString = maxPSB.toString();
        }
        
        public int getDimensionLength() {
            return this.dimensionLength;
        }
        
        public double getMinPointValue(final int index) {
            return this.minPoint[index];
        }
        
        public double getMaxPointValue(final int index) {
            return this.maxPoint[index];
        }
        
        @Override
        public String toString() {
            return "Min " + this.minPointString + "; Max " + this.maxPointString;
        }
    }
}
