// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs;

import ucar.nc2.dt.GridDataset;

public interface Dataset
{
    GridDataset getDataset();
    
    String getDatasetPath();
    
    String getDatasetName();
}
