// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs;

import ucar.nc2.dt.GridCoordSystem;

public interface Coverage
{
    String getName();
    
    String getLabel();
    
    String getDescription();
    
    GridCoordSystem getCoordinateSystem();
}
