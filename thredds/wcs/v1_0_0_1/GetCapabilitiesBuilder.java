// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import org.slf4j.LoggerFactory;
import ucar.nc2.dt.GridDataset;
import thredds.wcs.Request;
import java.net.URI;
import org.slf4j.Logger;

public class GetCapabilitiesBuilder extends WcsRequestBuilder
{
    private Logger logger;
    private URI serverUri;
    private GetCapabilities.Section section;
    private String updateSequence;
    private GetCapabilities.ServiceInfo serviceInfo;
    
    GetCapabilitiesBuilder(final String versionString, final Request.Operation operation, final GridDataset dataset, final String datasetPath) {
        super(versionString, operation, dataset, datasetPath);
        this.logger = LoggerFactory.getLogger(GetCapabilitiesBuilder.class);
    }
    
    public URI getServerUri() {
        return this.serverUri;
    }
    
    public GetCapabilitiesBuilder setServerUri(final URI serverUri) {
        this.serverUri = serverUri;
        return this;
    }
    
    public GetCapabilities.Section getSection() {
        return this.section;
    }
    
    public GetCapabilitiesBuilder setSection(final GetCapabilities.Section section) {
        this.section = section;
        return this;
    }
    
    public String getUpdateSequence() {
        return this.updateSequence;
    }
    
    public GetCapabilitiesBuilder setUpdateSequence(final String updateSequence) {
        this.updateSequence = updateSequence;
        return this;
    }
    
    public GetCapabilities.ServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }
    
    public GetCapabilitiesBuilder setServiceInfo(final GetCapabilities.ServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
        return this;
    }
    
    public GetCapabilities buildGetCapabilities() {
        if (this.serverUri == null) {
            throw new IllegalStateException("Null server URI not allowed.");
        }
        if (this.section == null) {
            throw new IllegalStateException("Null section not allowed.");
        }
        return new GetCapabilities(this.getOperation(), this.getVersionString(), this.getWcsDataset(), this.serverUri, this.section, this.updateSequence, this.serviceInfo);
    }
}
