// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import org.jdom.Content;
import org.jdom.Element;
import java.io.IOException;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.PrintWriter;
import thredds.wcs.Request;
import org.jdom.Document;
import java.net.URI;

public class GetCapabilities extends WcsRequest
{
    private URI serverURI;
    private Section section;
    private ServiceInfo serviceInfo;
    private String updateSequence;
    private Document capabilitiesReport;
    
    public GetCapabilities(final Request.Operation operation, final String version, final WcsDataset dataset, final URI serverURI, final Section section, final String updateSequence, final ServiceInfo serviceInfo) {
        super(operation, version, dataset);
        this.serverURI = serverURI;
        this.section = section;
        this.serviceInfo = serviceInfo;
        this.updateSequence = updateSequence;
        if (this.serverURI == null) {
            throw new IllegalArgumentException("Null server URI not allowed.");
        }
        if (this.section == null) {
            throw new IllegalArgumentException("Null section not allowed.");
        }
    }
    
    String getCurrentUpdateSequence() {
        if (this.updateSequence == null) {
            return null;
        }
        return null;
    }
    
    public Document getCapabilitiesReport() throws WcsException {
        if (this.capabilitiesReport == null) {
            this.capabilitiesReport = this.generateCapabilities();
        }
        return this.capabilitiesReport;
    }
    
    public void writeCapabilitiesReport(final PrintWriter pw) throws WcsException, IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(this.getCapabilitiesReport(), pw);
    }
    
    public String writeCapabilitiesReportAsString() throws WcsException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        return xmlOutputter.outputString(this.getCapabilitiesReport());
    }
    
    Document generateCapabilities() throws WcsException {
        Element rootElem;
        if (this.section.equals(Section.All)) {
            rootElem = new Element("WCS_Capabilities", GetCapabilities.wcsNS);
            rootElem.addContent(this.generateServiceSection(this.serviceInfo));
            rootElem.addContent(this.generateCapabilitySection());
            rootElem.addContent(this.generateContentMetadataSection());
        }
        else if (this.section.equals(Section.Service)) {
            rootElem = this.generateServiceSection(this.serviceInfo);
        }
        else if (this.section.equals(Section.Capability)) {
            rootElem = this.generateCapabilitySection();
        }
        else {
            if (!this.section.equals(Section.ContentMetadata)) {
                throw new WcsException();
            }
            rootElem = this.generateContentMetadataSection();
        }
        rootElem.addNamespaceDeclaration(GetCapabilities.gmlNS);
        rootElem.addNamespaceDeclaration(GetCapabilities.xlinkNS);
        rootElem.setAttribute("version", this.getVersion());
        if (this.getCurrentUpdateSequence() != null) {
            rootElem.setAttribute("updateSequence", this.getCurrentUpdateSequence());
        }
        return new Document(rootElem);
    }
    
    public Element generateServiceSection(final ServiceInfo serviceInfo) {
        final Element serviceElem = new Element("Service", GetCapabilities.wcsNS);
        if (serviceInfo != null) {
            if (serviceInfo.getDescription() != null) {
                serviceElem.addContent(new Element("description", GetCapabilities.wcsNS).addContent(serviceInfo.getDescription()));
            }
            if (serviceInfo.getName() != null) {
                serviceElem.addContent(new Element("name", GetCapabilities.wcsNS).addContent(serviceInfo.getName()));
            }
            if (serviceInfo.getLabel() != null) {
                serviceElem.addContent(new Element("label", GetCapabilities.wcsNS).addContent(serviceInfo.getLabel()));
            }
            if (serviceInfo.getKeywords() != null && serviceInfo.getKeywords().size() > 0) {
                final Element keywordsElem = new Element("keywords", GetCapabilities.wcsNS);
                for (final String curKey : serviceInfo.getKeywords()) {
                    keywordsElem.addContent(new Element("keyword", GetCapabilities.wcsNS).addContent(curKey));
                }
                serviceElem.addContent(keywordsElem);
            }
            final ResponsibleParty respParty = serviceInfo.getResponsibleParty();
            if (respParty != null) {
                final Element respPartyElem = new Element("responsibleParty", GetCapabilities.wcsNS);
                if (respParty.getIndividualName() != null) {
                    respPartyElem.addContent(new Element("individualName", GetCapabilities.wcsNS).addContent(respParty.getIndividualName()));
                }
                if (respParty.getOrganizationName() != null) {
                    respPartyElem.addContent(new Element("organisationName", GetCapabilities.wcsNS).addContent(respParty.getOrganizationName()));
                }
                if (respParty.getPositionName() != null) {
                    respPartyElem.addContent(new Element("positionName", GetCapabilities.wcsNS).addContent(respParty.getPositionName()));
                }
                if (respParty.getContact() != null) {
                    final Element contactElem = new Element("contactInfo", GetCapabilities.wcsNS);
                    final Element phoneElem = new Element("phone", GetCapabilities.wcsNS);
                    if (respParty.getContact().getVoicePhone() != null) {
                        for (final String curVoicePhone : respParty.getContact().getVoicePhone()) {
                            phoneElem.addContent(new Element("voice", GetCapabilities.wcsNS).addContent(curVoicePhone));
                        }
                    }
                    if (respParty.getContact().getFaxPhone() != null) {
                        for (final String curFaxPhone : respParty.getContact().getFaxPhone()) {
                            phoneElem.addContent(new Element("facsimile", GetCapabilities.wcsNS).addContent(curFaxPhone));
                        }
                    }
                    if (phoneElem.getContentSize() > 0) {
                        contactElem.addContent(phoneElem);
                    }
                    final ResponsibleParty.Address contactAddress = respParty.getContact().getAddress();
                    if (contactAddress != null) {
                        final Element addressElem = new Element("address", GetCapabilities.wcsNS);
                        if (contactAddress.getDeliveryPoint() != null) {
                            for (final String curDP : contactAddress.getDeliveryPoint()) {
                                addressElem.addContent(new Element("deliveryPoint", GetCapabilities.wcsNS).addContent(curDP));
                            }
                        }
                        if (contactAddress.getCity() != null) {
                            addressElem.addContent(new Element("city", GetCapabilities.wcsNS).addContent(contactAddress.getCity()));
                        }
                        if (contactAddress.getAdminArea() != null) {
                            addressElem.addContent(new Element("administrativeArea", GetCapabilities.wcsNS).addContent(contactAddress.getAdminArea()));
                        }
                        if (contactAddress.getPostalCode() != null) {
                            addressElem.addContent(new Element("postalCode", GetCapabilities.wcsNS).addContent(contactAddress.getPostalCode()));
                        }
                        if (contactAddress.getCountry() != null) {
                            addressElem.addContent(new Element("country", GetCapabilities.wcsNS).addContent(contactAddress.getCountry()));
                        }
                        if (contactAddress.getEmail() != null) {
                            for (final String curEmail : contactAddress.getEmail()) {
                                addressElem.addContent(new Element("electronicMailAddress", GetCapabilities.wcsNS).addContent(curEmail));
                            }
                        }
                        contactElem.addContent(addressElem);
                    }
                    final ResponsibleParty.OnlineResource onlineRes = respParty.getContact().getOnlineResource();
                    if (onlineRes != null) {
                        final Element onlineResElem = new Element("onlineResource", GetCapabilities.wcsNS);
                        onlineResElem.setAttribute("type", "simple");
                        if (onlineRes.getTitle() != null) {
                            onlineResElem.setAttribute("title", onlineRes.getTitle(), GetCapabilities.xlinkNS);
                        }
                        if (onlineRes.getLink() != null) {
                            onlineResElem.setAttribute("href", onlineRes.getLink().toString(), GetCapabilities.xlinkNS);
                        }
                        contactElem.addContent(onlineResElem);
                    }
                    respPartyElem.addContent(contactElem);
                }
                serviceElem.addContent(respPartyElem);
            }
        }
        serviceElem.addContent(new Element("fees", GetCapabilities.wcsNS).addContent("NONE"));
        serviceElem.addContent(new Element("accessConstraints", GetCapabilities.wcsNS).addContent("NONE"));
        return serviceElem;
    }
    
    public Element generateCapabilitySection() {
        final Element capElem = new Element("Capability", GetCapabilities.wcsNS);
        final Element requestElem = new Element("Request", GetCapabilities.wcsNS);
        requestElem.addContent(this.genCapabilityOperationElem(Request.Operation.GetCapabilities.toString()));
        requestElem.addContent(this.genCapabilityOperationElem(Request.Operation.DescribeCoverage.toString()));
        requestElem.addContent(this.genCapabilityOperationElem(Request.Operation.GetCoverage.toString()));
        capElem.addContent(requestElem);
        capElem.addContent(new Element("Exception", GetCapabilities.wcsNS).addContent(new Element("Format", GetCapabilities.wcsNS).addContent("application/vnd.ogc.se_xml")));
        return capElem;
    }
    
    private Element genCapabilityOperationElem(final String operationAsString) {
        final Element getCapOpsElem = new Element(operationAsString, GetCapabilities.wcsNS);
        getCapOpsElem.addContent(new Element("DCPType", GetCapabilities.wcsNS).addContent(new Element("HTTP", GetCapabilities.wcsNS).addContent(new Element("Get", GetCapabilities.wcsNS).addContent(new Element("OnlineResource", GetCapabilities.wcsNS).setAttribute("href", this.serverURI.toString(), GetCapabilities.xlinkNS)))));
        return getCapOpsElem;
    }
    
    public Element generateContentMetadataSection() {
        final Element contMdElem = new Element("ContentMetadata", GetCapabilities.wcsNS);
        for (final WcsCoverage curCoverage : this.getDataset().getAvailableCoverageCollection()) {
            contMdElem.addContent(this.genCoverageOfferingBriefElem("CoverageOfferingBrief", curCoverage.getName(), curCoverage.getLabel(), curCoverage.getDescription(), curCoverage.getCoordinateSystem()));
        }
        return contMdElem;
    }
    
    public enum Section
    {
        All(""), 
        Service("WCS_Capabilities/Service"), 
        Capability("WCS_Capabilities/Capability"), 
        ContentMetadata("WCS_Capabilities/ContentMetadata");
        
        private final String altId;
        
        private Section(final String altId) {
            this.altId = altId;
        }
        
        @Override
        public String toString() {
            return this.altId;
        }
        
        public static Section getSection(final String altId) {
            for (final Section curSection : values()) {
                if (curSection.altId.equals(altId)) {
                    return curSection;
                }
            }
            throw new IllegalArgumentException("No such instance [" + altId + "].");
        }
    }
    
    public static class ServiceInfo
    {
        private String name;
        private String label;
        private String description;
        private List<String> keywords;
        private ResponsibleParty responsibleParty;
        private String fees;
        private List<String> accessConstraints;
        
        public ServiceInfo(final String name, final String label, final String description, final List<String> keywords, final ResponsibleParty responsibleParty, final String fees, final List<String> accessConstraints) {
            this.name = name;
            this.label = label;
            this.description = description;
            this.keywords = new ArrayList<String>(keywords);
            this.responsibleParty = responsibleParty;
            this.fees = fees;
            this.accessConstraints = new ArrayList<String>(accessConstraints);
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getLabel() {
            return this.label;
        }
        
        public String getDescription() {
            return this.description;
        }
        
        public List<String> getKeywords() {
            return Collections.unmodifiableList((List<? extends String>)this.keywords);
        }
        
        public ResponsibleParty getResponsibleParty() {
            return this.responsibleParty;
        }
        
        public String getFees() {
            return this.fees;
        }
        
        public List<String> getAccessConstraints() {
            return Collections.unmodifiableList((List<? extends String>)this.accessConstraints);
        }
    }
    
    public static class ResponsibleParty
    {
        private String individualName;
        private String organizationName;
        private String positionName;
        private ContactInfo contactInfo;
        
        public ResponsibleParty(final String individualName, final String organizationName, final String positionName, final ContactInfo contactInfo) {
            this.individualName = individualName;
            this.organizationName = organizationName;
            this.positionName = positionName;
            this.contactInfo = contactInfo;
        }
        
        public String getIndividualName() {
            return this.individualName;
        }
        
        public String getOrganizationName() {
            return this.organizationName;
        }
        
        public String getPositionName() {
            return this.positionName;
        }
        
        public ContactInfo getContact() {
            return this.contactInfo;
        }
        
        public static class ContactInfo
        {
            private List<String> voicePhone;
            private List<String> faxPhone;
            private Address address;
            private OnlineResource onlineResource;
            
            public ContactInfo(final List<String> voicePhone, final List<String> faxPhone, final Address address, final OnlineResource onlineResource) {
                this.voicePhone = new ArrayList<String>(voicePhone);
                this.faxPhone = new ArrayList<String>(faxPhone);
                this.address = address;
                this.onlineResource = onlineResource;
            }
            
            public List<String> getVoicePhone() {
                return Collections.unmodifiableList((List<? extends String>)this.voicePhone);
            }
            
            public List<String> getFaxPhone() {
                return Collections.unmodifiableList((List<? extends String>)this.faxPhone);
            }
            
            public Address getAddress() {
                return this.address;
            }
            
            public OnlineResource getOnlineResource() {
                return this.onlineResource;
            }
        }
        
        public static class Address
        {
            private List<String> deliveryPoint;
            private String city;
            private String adminArea;
            private String postalCode;
            private String country;
            private List<String> email;
            
            public Address(final List<String> deliveryPoint, final String city, final String adminArea, final String postalCode, final String country, final List<String> email) {
                this.deliveryPoint = new ArrayList<String>(deliveryPoint);
                this.city = city;
                this.adminArea = adminArea;
                this.postalCode = postalCode;
                this.country = country;
                this.email = new ArrayList<String>(email);
            }
            
            public List<String> getDeliveryPoint() {
                return Collections.unmodifiableList((List<? extends String>)this.deliveryPoint);
            }
            
            public String getCity() {
                return this.city;
            }
            
            public String getAdminArea() {
                return this.adminArea;
            }
            
            public String getPostalCode() {
                return this.postalCode;
            }
            
            public String getCountry() {
                return this.country;
            }
            
            public List<String> getEmail() {
                return Collections.unmodifiableList((List<? extends String>)this.email);
            }
        }
        
        public static class OnlineResource
        {
            private URI link;
            private String title;
            
            public OnlineResource(final URI link, final String title) {
                this.link = link;
                this.title = title;
            }
            
            public URI getLink() {
                return this.link;
            }
            
            public String getTitle() {
                return this.title;
            }
        }
    }
}
