// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import org.slf4j.LoggerFactory;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.dt.GridCoordSystem;
import java.io.File;
import thredds.wcs.Request;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import org.slf4j.Logger;

public class GetCoverage extends WcsRequest
{
    private static Logger log;
    private WcsCoverage coverage;
    private LatLonRect bboxLatLonRect;
    private DateRange timeRange;
    private WcsCoverage.VerticalRange rangeSetAxisValueRange;
    private Request.Format format;
    private boolean isSingleTimeRequest;
    private boolean isSingleVerticalRequest;
    
    public GetCoverage(final Request.Operation operation, final String version, final WcsDataset dataset, final String coverageId, String crs, String responseCRS, final Request.BoundingBox bbox, final DateRange timeRange, final WcsCoverage.VerticalRange verticalRange, final Request.Format format) throws WcsException {
        super(operation, version, dataset);
        this.bboxLatLonRect = null;
        this.isSingleTimeRequest = false;
        this.isSingleVerticalRequest = false;
        if (coverageId == null) {
            throw new WcsException(WcsException.Code.MissingParameterValue, "coverage", "Coverage identifier required.");
        }
        if (!this.getDataset().isAvailableCoverageName(coverageId)) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "coverage", "Unknown coverage identifier [" + coverageId + "].");
        }
        this.coverage = this.getDataset().getAvailableCoverage(coverageId);
        if (this.coverage == null) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "coverage", "Unknown coverage identifier [" + coverageId + "].");
        }
        if (crs == null) {
            crs = this.coverage.getDefaultRequestCrs();
        }
        if (!crs.equalsIgnoreCase(this.coverage.getDefaultRequestCrs())) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "CRS", "Request CRS [" + crs + "] not allowed [" + this.coverage.getDefaultRequestCrs() + "].");
        }
        if (responseCRS == null) {
            responseCRS = this.coverage.getNativeCrs();
        }
        else if (!responseCRS.equalsIgnoreCase(this.coverage.getNativeCrs())) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "response_CRS", "Response CRS [" + responseCRS + "] not the supported CRS [" + this.coverage.getNativeCrs() + "].");
        }
        if (bbox != null) {
            this.bboxLatLonRect = this.convertBoundingBox(bbox, this.coverage.getCoordinateSystem());
        }
        if ((this.timeRange = timeRange) != null) {
            this.isSingleTimeRequest = timeRange.isPoint();
        }
        else {
            this.isSingleTimeRequest = (null == this.coverage.getCoordinateSystem().getTimeAxis());
        }
        this.rangeSetAxisValueRange = verticalRange;
        if (verticalRange != null) {
            this.isSingleVerticalRequest = verticalRange.isSinglePoint();
        }
        else {
            this.isSingleVerticalRequest = (null == this.coverage.getCoordinateSystem().getVerticalAxis());
        }
        if (format == null) {
            GetCoverage.log.debug("GetCoverage(): FORMAT parameter required.");
            throw new WcsException(WcsException.Code.InvalidParameterValue, "FORMAT", "FORMAT parameter required.");
        }
        if (!this.coverage.isSupportedCoverageFormat(format)) {
            final String msg = "Unsupported format value [" + format + "].";
            GetCoverage.log.debug("GetCoverage(): " + msg);
            throw new WcsException(WcsException.Code.InvalidParameterValue, "FORMAT", msg);
        }
        this.format = format;
        if ((this.format == Request.Format.GeoTIFF || this.format == Request.Format.GeoTIFF_Float) && !this.isSingleTimeRequest && !this.isSingleVerticalRequest) {
            final StringBuffer msgB = new StringBuffer("GeoTIFF supported only for requests at a single time [");
            if (this.timeRange != null) {
                msgB.append(this.timeRange);
            }
            msgB.append("] and a single vertical level [");
            if (verticalRange != null) {
                msgB.append(verticalRange);
            }
            msgB.append("].");
            GetCoverage.log.debug("GetCoverage(): " + (Object)msgB);
            throw new WcsException(WcsException.Code.InvalidParameterValue, "FORMAT", msgB.toString());
        }
    }
    
    public Request.Format getFormat() {
        return this.format;
    }
    
    public File writeCoverageDataToFile() throws WcsException {
        return this.coverage.writeCoverageDataToFile(this.format, this.bboxLatLonRect, this.rangeSetAxisValueRange, this.timeRange);
    }
    
    private LatLonRect convertBoundingBox(final Request.BoundingBox bbox, final GridCoordSystem gcs) throws WcsException {
        if (bbox == null) {
            return null;
        }
        final LatLonPointImpl minll = new LatLonPointImpl(bbox.getMinPointValue(1), bbox.getMinPointValue(0));
        final LatLonPointImpl maxll = new LatLonPointImpl(bbox.getMaxPointValue(1), bbox.getMaxPointValue(0));
        final LatLonRect bboxLatLonRect = new LatLonRect(minll, maxll);
        return bboxLatLonRect;
    }
    
    static {
        GetCoverage.log = LoggerFactory.getLogger(GetCoverage.class);
    }
}
