// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import org.jdom.Content;
import org.jdom.Element;
import ucar.nc2.dt.GridCoordSystem;
import thredds.wcs.Request;
import org.jdom.Namespace;

public abstract class WcsRequest
{
    protected static final Namespace wcsNS;
    protected static final Namespace gmlNS;
    protected static final Namespace xlinkNS;
    private Request.Operation operation;
    private String version;
    private WcsDataset dataset;
    
    WcsRequest(final Request.Operation operation, final String version, final WcsDataset dataset) {
        this.operation = operation;
        this.version = version;
        this.dataset = dataset;
        if (operation == null) {
            throw new IllegalArgumentException("Non-null operation required.");
        }
        if (this.dataset == null) {
            throw new IllegalArgumentException("Non-null dataset required.");
        }
    }
    
    public Request.Operation getOperation() {
        return this.operation;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    public WcsDataset getDataset() {
        return this.dataset;
    }
    
    protected Element genCoverageOfferingBriefElem(final String elemName, final String covName, final String covLabel, final String covDescription, final GridCoordSystem gridCoordSys) {
        final Element briefElem = new Element(elemName, WcsRequest.wcsNS);
        if (covDescription != null && !covDescription.equals("")) {
            briefElem.addContent(new Element("description", WcsRequest.wcsNS).addContent(covDescription));
        }
        briefElem.addContent(new Element("name", WcsRequest.wcsNS).addContent(covName));
        briefElem.addContent(new Element("label", WcsRequest.wcsNS).addContent(covLabel));
        briefElem.addContent(this.genLonLatEnvelope(gridCoordSys));
        return briefElem;
    }
    
    protected Element genLonLatEnvelope(final GridCoordSystem gcs) {
        final Element lonLatEnvelopeElem = new Element("lonLatEnvelope", WcsRequest.wcsNS);
        lonLatEnvelopeElem.setAttribute("srsName", "urn:ogc:def:crs:OGC:1.3:CRS84");
        final LatLonRect llbb = gcs.getLatLonBoundingBox();
        final LatLonPoint llpt = llbb.getLowerLeftPoint();
        final LatLonPoint urpt = llbb.getUpperRightPoint();
        final String firstPosition = llpt.getLongitude() + " " + llpt.getLatitude();
        final double lon = llpt.getLongitude() + llbb.getWidth();
        final String secondPosition = lon + " " + urpt.getLatitude();
        lonLatEnvelopeElem.addContent(new Element("pos", WcsRequest.gmlNS).addContent(firstPosition));
        lonLatEnvelopeElem.addContent(new Element("pos", WcsRequest.gmlNS).addContent(secondPosition));
        if (gcs.hasTimeAxis()) {
            lonLatEnvelopeElem.addContent(new Element("timePosition", WcsRequest.gmlNS).addContent(gcs.getDateRange().getStart().toDateTimeStringISO()));
            lonLatEnvelopeElem.addContent(new Element("timePosition", WcsRequest.gmlNS).addContent(gcs.getDateRange().getEnd().toDateTimeStringISO()));
        }
        return lonLatEnvelopeElem;
    }
    
    static {
        wcsNS = Namespace.getNamespace("http://www.opengis.net/wcs");
        gmlNS = Namespace.getNamespace("gml", "http://www.opengis.net/gml");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
    }
}
