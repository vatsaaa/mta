// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import ucar.nc2.dt.GridDataset;
import thredds.wcs.Request;

public abstract class WcsRequestBuilder
{
    private String versionString;
    private Request.Operation operation;
    private GridDataset dataset;
    private String datasetPath;
    private WcsDataset wcsDataset;
    
    public static WcsRequestBuilder newWcsRequestBuilder(final String versionString, final Request.Operation operation, final GridDataset dataset, final String datasetPath) {
        if (operation == null) {
            throw new IllegalArgumentException("Null operation not allowed.");
        }
        if (operation.equals(Request.Operation.GetCapabilities)) {
            return new GetCapabilitiesBuilder(versionString, operation, dataset, datasetPath);
        }
        if (operation.equals(Request.Operation.DescribeCoverage)) {
            return new DescribeCoverageBuilder(versionString, operation, dataset, datasetPath);
        }
        if (operation.equals(Request.Operation.GetCoverage)) {
            return new GetCoverageBuilder(versionString, operation, dataset, datasetPath);
        }
        throw new IllegalArgumentException("Unknown operation [" + operation.name() + "].");
    }
    
    WcsRequestBuilder(final String versionString, final Request.Operation operation, final GridDataset dataset, final String datasetPath) {
        if (versionString == null || versionString.equals("")) {
            throw new IllegalArgumentException("Versions string may not be null or empty string.");
        }
        if (operation == null) {
            throw new IllegalArgumentException("Operation may not be null.");
        }
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset may not be null.");
        }
        if (datasetPath == null) {
            throw new IllegalArgumentException("Dataset path may not be null.");
        }
        this.versionString = versionString;
        this.operation = operation;
        this.dataset = dataset;
        this.datasetPath = datasetPath;
        this.wcsDataset = new WcsDataset(this.dataset, this.datasetPath);
    }
    
    public Request.Operation getOperation() {
        return this.operation;
    }
    
    public boolean isGetCapabilitiesOperation() {
        return this.operation.equals(Request.Operation.GetCapabilities);
    }
    
    public boolean isDescribeCoverageOperation() {
        return this.operation.equals(Request.Operation.DescribeCoverage);
    }
    
    public boolean isGetCoverageOperation() {
        return this.operation.equals(Request.Operation.GetCoverage);
    }
    
    public String getVersionString() {
        return this.versionString;
    }
    
    public GridDataset getDataset() {
        return this.dataset;
    }
    
    public String getDatasetPath() {
        return this.datasetPath;
    }
    
    public WcsDataset getWcsDataset() {
        return this.wcsDataset;
    }
}
