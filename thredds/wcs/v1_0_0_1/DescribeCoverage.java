// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import java.util.Date;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import org.jdom.Attribute;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dt.GridCoordSystem;
import org.jdom.Content;
import org.jdom.Element;
import java.io.IOException;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.PrintWriter;
import java.util.Iterator;
import thredds.wcs.Request;
import org.jdom.Document;
import java.util.List;

public class DescribeCoverage extends WcsRequest
{
    private List<String> coverages;
    private Document describeCoverageDoc;
    
    public DescribeCoverage(final Request.Operation operation, final String version, final WcsDataset dataset, final List<String> coverages) throws WcsException {
        super(operation, version, dataset);
        this.coverages = coverages;
        if (this.coverages == null) {
            throw new IllegalArgumentException("Non-null coverage list required.");
        }
        if (this.coverages.size() < 1) {
            throw new IllegalArgumentException("Coverage list must contain at least one ID [" + this.coverages.size() + "].");
        }
        String badCovIds = "";
        for (final String curCov : coverages) {
            if (!this.getDataset().isAvailableCoverageName(curCov)) {
                badCovIds = badCovIds + ((badCovIds.length() > 0) ? ", " : "") + curCov;
            }
        }
        if (badCovIds.length() > 0) {
            throw new WcsException("Coverage ID list contains one or more unknown IDs [" + badCovIds + "].");
        }
    }
    
    public Document getDescribeCoverageDoc() {
        if (this.describeCoverageDoc == null) {
            this.describeCoverageDoc = this.generateDescribeCoverageDoc();
        }
        return this.describeCoverageDoc;
    }
    
    public void writeDescribeCoverageDoc(final PrintWriter pw) throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(this.getDescribeCoverageDoc(), pw);
    }
    
    public String writeDescribeCoverageDocAsString() throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        return xmlOutputter.outputString(this.getDescribeCoverageDoc());
    }
    
    Document generateDescribeCoverageDoc() {
        final Element coverageDescriptionsElem = new Element("CoverageDescription", DescribeCoverage.wcsNS);
        coverageDescriptionsElem.addNamespaceDeclaration(DescribeCoverage.gmlNS);
        coverageDescriptionsElem.addNamespaceDeclaration(DescribeCoverage.xlinkNS);
        coverageDescriptionsElem.setAttribute("version", this.getVersion());
        for (final String curCoverageId : this.coverages) {
            coverageDescriptionsElem.addContent(this.genCoverageOfferingElem(curCoverageId));
        }
        return new Document(coverageDescriptionsElem);
    }
    
    public Element genCoverageOfferingElem(final String covId) {
        final WcsCoverage coverage = this.getDataset().getAvailableCoverage(covId);
        final GridCoordSystem gridCoordSystem = coverage.getCoordinateSystem();
        final Element covDescripElem = this.genCoverageOfferingBriefElem("CoverageOffering", covId, coverage.getLabel(), coverage.getDescription(), gridCoordSystem);
        covDescripElem.addContent(this.genDomainSetElem(coverage));
        covDescripElem.addContent(this.genRangeSetElem(coverage));
        covDescripElem.addContent(this.genSupportedCRSsElem(coverage));
        covDescripElem.addContent(this.genSupportedFormatsElem(coverage));
        covDescripElem.addContent(this.genSupportedInterpolationsElem());
        return covDescripElem;
    }
    
    private Element genDomainSetElem(final WcsCoverage coverage) {
        final Element domainSetElem = new Element("domainSet", DescribeCoverage.wcsNS);
        domainSetElem.addContent(this.genSpatialDomainElem(coverage));
        if (coverage.getCoordinateSystem().hasTimeAxis()) {
            domainSetElem.addContent(this.genTemporalDomainElem(coverage.getCoordinateSystem().getTimeAxis1D()));
        }
        return domainSetElem;
    }
    
    private Element genSpatialDomainElem(final WcsCoverage coverage) {
        final Element spatialDomainElem = new Element("spatialDomain", DescribeCoverage.wcsNS);
        spatialDomainElem.addContent(this.genEnvelopeElem(coverage.getCoordinateSystem()));
        spatialDomainElem.addContent(this.genRectifiedGridElem(coverage));
        return spatialDomainElem;
    }
    
    private Element genRectifiedGridElem(final WcsCoverage coverage) {
        final Element rectifiedGridElem = new Element("RectifiedGrid", DescribeCoverage.gmlNS);
        final CoordinateAxis1D xaxis = (CoordinateAxis1D)coverage.getCoordinateSystem().getXHorizAxis();
        final CoordinateAxis1D yaxis = (CoordinateAxis1D)coverage.getCoordinateSystem().getYHorizAxis();
        final CoordinateAxis1D zaxis = coverage.getCoordinateSystem().getVerticalAxis();
        rectifiedGridElem.setAttribute("srsName", coverage.getNativeCrs());
        final int ndim = (zaxis != null) ? 3 : 2;
        rectifiedGridElem.setAttribute("dimension", Integer.toString(ndim));
        final int[] minValues = new int[ndim];
        final int[] maxValues = new int[ndim];
        maxValues[0] = (int)(xaxis.getSize() - 1L);
        maxValues[1] = (int)(yaxis.getSize() - 1L);
        if (zaxis != null) {
            maxValues[2] = (int)(zaxis.getSize() - 1L);
        }
        final Element limitsElem = new Element("limits", DescribeCoverage.gmlNS);
        limitsElem.addContent(new Element("GridEnvelope", DescribeCoverage.gmlNS).addContent(new Element("low", DescribeCoverage.gmlNS).addContent(this.genIntegerListString(minValues))).addContent(new Element("high", DescribeCoverage.gmlNS).addContent(this.genIntegerListString(maxValues))));
        rectifiedGridElem.addContent(limitsElem);
        rectifiedGridElem.addContent(new Element("axisName", DescribeCoverage.gmlNS).addContent("x"));
        rectifiedGridElem.addContent(new Element("axisName", DescribeCoverage.gmlNS).addContent("y"));
        if (zaxis != null) {
            rectifiedGridElem.addContent(new Element("axisName", DescribeCoverage.gmlNS).addContent("z"));
        }
        final double[] origin = new double[ndim];
        origin[0] = xaxis.getStart();
        origin[1] = yaxis.getStart();
        if (zaxis != null) {
            origin[2] = zaxis.getStart();
        }
        rectifiedGridElem.addContent(new Element("origin", DescribeCoverage.gmlNS).addContent(new Element("pos", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(origin))));
        final double[] xoffset = new double[ndim];
        xoffset[0] = xaxis.getIncrement();
        rectifiedGridElem.addContent(new Element("offsetVector", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(xoffset)));
        final double[] yoffset = new double[ndim];
        yoffset[1] = yaxis.getIncrement();
        rectifiedGridElem.addContent(new Element("offsetVector", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(yoffset)));
        if (zaxis != null) {
            final double[] zoffset = new double[ndim];
            zoffset[2] = zaxis.getIncrement();
            rectifiedGridElem.addContent(new Element("offsetVector", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(zoffset)));
        }
        return rectifiedGridElem;
    }
    
    private String genIntegerListString(final int[] values) {
        final StringBuffer buf = new StringBuffer();
        for (final int intValue : values) {
            if (buf.length() > 0) {
                buf.append(" ");
            }
            buf.append(intValue);
        }
        return buf.toString();
    }
    
    private String genDoubleListString(final double[] values) {
        final StringBuffer buf = new StringBuffer();
        for (final double doubleValue : values) {
            if (buf.length() > 0) {
                buf.append(" ");
            }
            buf.append(doubleValue);
        }
        return buf.toString();
    }
    
    private Element genEnvelopeElem(final GridCoordSystem gcs) {
        Element envelopeElem;
        if (gcs.hasTimeAxis()) {
            envelopeElem = new Element("EnvelopeWithTimePeriod", DescribeCoverage.wcsNS);
        }
        else {
            envelopeElem = new Element("Envelope", DescribeCoverage.wcsNS);
        }
        envelopeElem.setAttribute("srsName", "urn:ogc:def:crs:OGC:1.3:CRS84");
        final LatLonRect llbb = gcs.getLatLonBoundingBox();
        final LatLonPoint llpt = llbb.getLowerLeftPoint();
        final LatLonPoint urpt = llbb.getUpperRightPoint();
        final double lon = llpt.getLongitude() + llbb.getWidth();
        final int posDim = 2;
        final String firstPosition = llpt.getLongitude() + " " + llpt.getLatitude();
        final String secondPosition = lon + " " + urpt.getLatitude();
        final String posDimString = Integer.toString(posDim);
        envelopeElem.addContent(new Element("pos", DescribeCoverage.gmlNS).addContent(firstPosition).setAttribute(new Attribute("dimension", posDimString)));
        envelopeElem.addContent(new Element("pos", DescribeCoverage.gmlNS).addContent(secondPosition).setAttribute(new Attribute("dimension", posDimString)));
        if (gcs.hasTimeAxis()) {
            envelopeElem.addContent(new Element("timePosition", DescribeCoverage.gmlNS).addContent(gcs.getDateRange().getStart().toDateTimeStringISO()));
            envelopeElem.addContent(new Element("timePosition", DescribeCoverage.gmlNS).addContent(gcs.getDateRange().getEnd().toDateTimeStringISO()));
        }
        return envelopeElem;
    }
    
    private Element genTemporalDomainElem(final CoordinateAxis1DTime timeAxis) {
        final Element temporalDomainElem = new Element("temporalDomain", DescribeCoverage.wcsNS);
        final Date[] dates = timeAxis.getTimeDates();
        final DateFormatter formatter = new DateFormatter();
        for (final Date curDate : dates) {
            temporalDomainElem.addContent(new Element("timePosition", DescribeCoverage.gmlNS).addContent(formatter.toDateTimeStringISO(curDate)));
        }
        return temporalDomainElem;
    }
    
    private Element genRangeSetElem(final WcsCoverage coverage) {
        final WcsRangeField rangeField = coverage.getRangeField();
        final Element rangeSetElem = new Element("rangeSet", DescribeCoverage.wcsNS);
        final Element innerRangeSetElem = new Element("RangeSet", DescribeCoverage.wcsNS);
        if (rangeField.getDescription() != null) {
            innerRangeSetElem.addContent(new Element("description").addContent(rangeField.getDescription()));
        }
        innerRangeSetElem.addContent(new Element("name", DescribeCoverage.wcsNS).addContent(rangeField.getName()));
        innerRangeSetElem.addContent(new Element("label", DescribeCoverage.wcsNS).addContent(rangeField.getLabel()));
        final WcsRangeField.Axis vertAxis = rangeField.getAxis();
        if (vertAxis != null) {
            final Element axisDescElem = new Element("axisDescription", DescribeCoverage.wcsNS);
            final Element innerAxisDescElem = new Element("AxisDescription", DescribeCoverage.wcsNS);
            innerAxisDescElem.addContent(new Element("name", DescribeCoverage.wcsNS).addContent(vertAxis.getName()));
            innerAxisDescElem.addContent(new Element("label", DescribeCoverage.wcsNS).addContent(vertAxis.getLabel()));
            final Element valuesElem = new Element("values", DescribeCoverage.wcsNS);
            for (final String curVal : vertAxis.getValues()) {
                valuesElem.addContent(new Element("singleValue", DescribeCoverage.wcsNS).addContent(curVal));
            }
            innerAxisDescElem.addContent(valuesElem);
            axisDescElem.addContent(innerAxisDescElem);
            innerRangeSetElem.addContent(axisDescElem);
        }
        if (coverage.hasMissingData()) {
            innerRangeSetElem.addContent(new Element("nullValues", DescribeCoverage.wcsNS).addContent(new Element("singleValue", DescribeCoverage.wcsNS).addContent("NaN")));
        }
        return rangeSetElem.addContent(innerRangeSetElem);
    }
    
    private Element genSupportedCRSsElem(final WcsCoverage coverage) {
        final Element supportedCRSsElem = new Element("supportedCRSs", DescribeCoverage.wcsNS);
        supportedCRSsElem.addContent(new Element("requestCRSs", DescribeCoverage.wcsNS).addContent(coverage.getDefaultRequestCrs()));
        supportedCRSsElem.addContent(new Element("responseCRSs", DescribeCoverage.wcsNS).addContent(coverage.getNativeCrs()));
        return supportedCRSsElem;
    }
    
    private Element genSupportedFormatsElem(final WcsCoverage coverage) {
        final Element supportedFormatsElem = new Element("supportedFormats", DescribeCoverage.wcsNS);
        for (final Request.Format curFormat : coverage.getSupportedCoverageFormatList()) {
            supportedFormatsElem.addContent(new Element("formats", DescribeCoverage.wcsNS).addContent(curFormat.toString()));
        }
        return supportedFormatsElem;
    }
    
    private Element genSupportedInterpolationsElem() {
        final Element supportedInterpolationsElem = new Element("supportedInterpolations", DescribeCoverage.wcsNS);
        supportedInterpolationsElem.addContent(new Element("interpolationMethod", DescribeCoverage.wcsNS).addContent("none"));
        return supportedInterpolationsElem;
    }
}
