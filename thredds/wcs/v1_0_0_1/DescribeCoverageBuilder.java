// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import org.slf4j.LoggerFactory;
import ucar.nc2.dt.GridDataset;
import thredds.wcs.Request;
import java.util.List;
import org.slf4j.Logger;

public class DescribeCoverageBuilder extends WcsRequestBuilder
{
    private Logger logger;
    private List<String> coverageIdList;
    
    DescribeCoverageBuilder(final String versionString, final Request.Operation operation, final GridDataset dataset, final String datasetPath) {
        super(versionString, operation, dataset, datasetPath);
        this.logger = LoggerFactory.getLogger(DescribeCoverageBuilder.class);
    }
    
    public List<String> getCoverageIdList() {
        return this.coverageIdList;
    }
    
    public DescribeCoverageBuilder setCoverageIdList(final List<String> coverageIdList) {
        this.coverageIdList = coverageIdList;
        return this;
    }
    
    public DescribeCoverage buildDescribeCoverage() throws WcsException {
        if (this.coverageIdList == null) {
            throw new IllegalStateException("Null coverage list not allowed.");
        }
        if (this.coverageIdList.isEmpty()) {
            throw new IllegalStateException("Empty coverage list not allowed.");
        }
        return new DescribeCoverage(this.getOperation(), this.getVersionString(), this.getWcsDataset(), this.coverageIdList);
    }
}
