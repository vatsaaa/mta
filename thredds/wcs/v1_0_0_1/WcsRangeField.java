// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class WcsRangeField
{
    private String name;
    private String label;
    private String description;
    private Axis axis;
    
    public WcsRangeField(final String name, final String label, final String description, final Axis axis) {
        if (name == null) {
            throw new IllegalArgumentException("Range name must be non-null.");
        }
        if (label == null) {
            throw new IllegalArgumentException("Range label must be non-null.");
        }
        this.name = name;
        this.label = label;
        this.description = description;
        this.axis = axis;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public Axis getAxis() {
        return this.axis;
    }
    
    public static class Axis
    {
        private String name;
        private String label;
        private String description;
        private boolean isNumeric;
        private List<String> values;
        
        public Axis(final String name, final String label, final String description, final boolean isNumeric, final List<String> values) {
            this.name = name;
            this.label = label;
            this.description = description;
            this.isNumeric = isNumeric;
            this.values = new ArrayList<String>(values);
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getLabel() {
            return this.label;
        }
        
        public String getDescription() {
            return this.description;
        }
        
        public boolean isNumeric() {
            return this.isNumeric;
        }
        
        public List<String> getValues() {
            return Collections.unmodifiableList((List<? extends String>)this.values);
        }
    }
}
