// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import java.util.Collections;
import java.util.Collection;
import java.io.IOException;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Iterator;
import ucar.nc2.dt.GridDatatype;
import java.util.HashMap;
import ucar.nc2.dt.GridDataset;

public class WcsDataset
{
    private String datasetPath;
    private String datasetName;
    private GridDataset dataset;
    private HashMap<String, WcsCoverage> availableCoverages;
    
    public WcsDataset(final GridDataset dataset, final String datasetPath) {
        this.datasetPath = datasetPath;
        final int pos = datasetPath.lastIndexOf("/");
        this.datasetName = ((pos > 0) ? datasetPath.substring(pos + 1) : datasetPath);
        this.dataset = dataset;
        this.availableCoverages = new HashMap<String, WcsCoverage>();
        for (final GridDatatype curGridDatatype : this.dataset.getGrids()) {
            final GridCoordSystem gcs = curGridDatatype.getCoordinateSystem();
            if (!gcs.isRegularSpatial()) {
                continue;
            }
            this.availableCoverages.put(curGridDatatype.getName(), new WcsCoverage(curGridDatatype, this));
        }
    }
    
    public String getDatasetPath() {
        return this.datasetPath;
    }
    
    public String getDatasetName() {
        return this.datasetName;
    }
    
    public GridDataset getDataset() {
        return this.dataset;
    }
    
    public void close() throws IOException {
        if (this.dataset != null) {
            this.dataset.close();
        }
    }
    
    public boolean isAvailableCoverageName(final String name) {
        return this.availableCoverages.containsKey(name);
    }
    
    public WcsCoverage getAvailableCoverage(final String name) {
        return this.availableCoverages.get(name);
    }
    
    public Collection<WcsCoverage> getAvailableCoverageCollection() {
        return Collections.unmodifiableCollection((Collection<? extends WcsCoverage>)this.availableCoverages.values());
    }
}
