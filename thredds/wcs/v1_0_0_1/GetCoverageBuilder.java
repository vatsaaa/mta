// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import org.slf4j.LoggerFactory;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.units.DateRange;
import thredds.wcs.Request;
import org.slf4j.Logger;

public class GetCoverageBuilder extends WcsRequestBuilder
{
    private Logger logger;
    private String coverageId;
    private String crs;
    private String responseCRS;
    private Request.BoundingBox bbox;
    private DateRange timeRange;
    private WcsCoverage.VerticalRange verticalRange;
    private Request.Format format;
    
    GetCoverageBuilder(final String versionString, final Request.Operation operation, final GridDataset dataset, final String datasetPath) {
        super(versionString, operation, dataset, datasetPath);
        this.logger = LoggerFactory.getLogger(GetCoverageBuilder.class);
    }
    
    public String getCoverageId() {
        return this.coverageId;
    }
    
    public GetCoverageBuilder setCoverageId(final String coverageId) {
        this.coverageId = coverageId;
        return this;
    }
    
    public String getCrs() {
        return this.crs;
    }
    
    public GetCoverageBuilder setCrs(final String crs) {
        this.crs = crs;
        return this;
    }
    
    public String getResponseCRS() {
        return this.responseCRS;
    }
    
    public GetCoverageBuilder setResponseCRS(final String responseCRS) {
        this.responseCRS = responseCRS;
        return this;
    }
    
    public Request.BoundingBox getBbox() {
        return this.bbox;
    }
    
    public GetCoverageBuilder setBbox(final Request.BoundingBox bbox) {
        this.bbox = bbox;
        return this;
    }
    
    public DateRange getTimeRange() {
        return this.timeRange;
    }
    
    public GetCoverageBuilder setTimeRange(final DateRange timeRange) {
        this.timeRange = timeRange;
        return this;
    }
    
    public WcsCoverage.VerticalRange getVerticalRange() {
        return this.verticalRange;
    }
    
    public GetCoverageBuilder setVerticalRange(final WcsCoverage.VerticalRange verticalRange) {
        this.verticalRange = verticalRange;
        return this;
    }
    
    public Request.Format getFormat() {
        return this.format;
    }
    
    public GetCoverageBuilder setFormat(final Request.Format format) {
        this.format = format;
        return this;
    }
    
    public GetCoverage buildGetCoverage() throws WcsException {
        return new GetCoverage(this.getOperation(), this.getVersionString(), this.getWcsDataset(), this.coverageId, this.crs, this.responseCRS, this.bbox, this.timeRange, this.verticalRange, this.format);
    }
}
