// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_1;

import org.slf4j.LoggerFactory;
import ucar.ma2.Array;
import ucar.nc2.units.DateType;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import java.util.Collections;
import ucar.nc2.dt.grid.NetcdfCFWriter;
import java.io.IOException;
import ucar.nc2.geotiff.GeotiffWriter;
import java.io.File;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.nc2.dataset.CoordinateAxis1D;
import java.util.ArrayList;
import ucar.unidata.geoloc.Projection;
import ucar.unidata.geoloc.ogc.EPSG_OGC_CF_Helper;
import ucar.nc2.util.DiskCache2;
import thredds.wcs.Request;
import java.util.List;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dt.GridDatatype;
import org.slf4j.Logger;

public class WcsCoverage
{
    private static Logger log;
    private GridDatatype coverage;
    private WcsDataset dataset;
    private GridCoordSystem coordSys;
    private String nativeCRS;
    private String defaultRequestCrs;
    private List<Request.Format> supportedCoverageFormatList;
    private WcsRangeField range;
    private static DiskCache2 diskCache;
    
    public WcsCoverage(final GridDatatype coverage, final WcsDataset dataset) {
        this.dataset = dataset;
        if (this.dataset == null) {
            WcsCoverage.log.error("WcsCoverage(): non-null dataset required.");
            throw new IllegalArgumentException("Non-null dataset required.");
        }
        this.coverage = coverage;
        if (this.coverage == null) {
            WcsCoverage.log.error("WcsCoverage(): non-null coverage required.");
            throw new IllegalArgumentException("Non-null coverage required.");
        }
        this.coordSys = coverage.getCoordinateSystem();
        if (this.coordSys == null) {
            WcsCoverage.log.error("WcsCoverage(): Coverage must have non-null coordinate system.");
            throw new IllegalArgumentException("Non-null coordinate system required.");
        }
        this.nativeCRS = EPSG_OGC_CF_Helper.getWcs1_0CrsId(this.coordSys.getProjection());
        this.defaultRequestCrs = "OGC:CRS84";
        (this.supportedCoverageFormatList = new ArrayList<Request.Format>()).add(Request.Format.GeoTIFF);
        this.supportedCoverageFormatList.add(Request.Format.GeoTIFF_Float);
        this.supportedCoverageFormatList.add(Request.Format.NetCDF3);
        final CoordinateAxis1D zaxis = this.coordSys.getVerticalAxis();
        WcsRangeField.Axis vertAxis;
        if (zaxis != null) {
            final List<String> vals = new ArrayList<String>();
            for (int z = 0; z < zaxis.getSize(); ++z) {
                vals.add(zaxis.getCoordName(z).trim());
            }
            vertAxis = new WcsRangeField.Axis("Vertical", zaxis.getName(), zaxis.getDescription(), zaxis.isNumeric(), vals);
        }
        else {
            vertAxis = null;
        }
        this.range = new WcsRangeField(this.getName(), this.getLabel(), this.getDescription(), vertAxis);
    }
    
    GridDatatype getGridDatatype() {
        return this.coverage;
    }
    
    public String getName() {
        return this.coverage.getName();
    }
    
    public String getLabel() {
        return this.coverage.getDescription();
    }
    
    public String getDescription() {
        return this.coverage.getInfo();
    }
    
    public GridCoordSystem getCoordinateSystem() {
        return this.coordSys;
    }
    
    public boolean hasMissingData() {
        return this.coverage.hasMissingData();
    }
    
    public String getDefaultRequestCrs() {
        return this.defaultRequestCrs;
    }
    
    public String getNativeCrs() {
        return this.nativeCRS;
    }
    
    public List<Request.Format> getSupportedCoverageFormatList() {
        return this.supportedCoverageFormatList;
    }
    
    public boolean isSupportedCoverageFormat(final Request.Format covFormat) {
        return this.supportedCoverageFormatList.contains(covFormat);
    }
    
    public WcsRangeField getRangeField() {
        return this.range;
    }
    
    public Range getRangeSetAxisRange(final double minValue, final double maxValue) {
        if (minValue > maxValue) {
            WcsCoverage.log.error("getRangeSetAxisRange(): Min is greater than max <" + minValue + ", " + maxValue + ">.");
            throw new IllegalArgumentException("Min is greater than max <" + minValue + ", " + maxValue + ">.");
        }
        final CoordinateAxis1D zaxis = this.coordSys.getVerticalAxis();
        if (zaxis != null) {
            final int minIndex = zaxis.findCoordElement(minValue);
            final int maxIndex = zaxis.findCoordElement(maxValue);
            if (minIndex == -1 || maxIndex == -1) {
                return null;
            }
            try {
                return new Range(minIndex, maxIndex);
            }
            catch (InvalidRangeException e) {
                return null;
            }
        }
        return null;
    }
    
    public static void setDiskCache(final DiskCache2 _diskCache) {
        WcsCoverage.diskCache = _diskCache;
    }
    
    private static DiskCache2 getDiskCache() {
        if (WcsCoverage.diskCache == null) {
            WcsCoverage.log.error("getDiskCache(): Disk cache has not been set.");
            throw new IllegalStateException("Disk cache must be set before calling GetCoverage.getDiskCache().");
        }
        return WcsCoverage.diskCache;
    }
    
    public File writeCoverageDataToFile(final Request.Format format, final LatLonRect bboxLatLonRect, final VerticalRange verticalRange, final DateRange timeRange) throws WcsException {
        Range zRange = null;
        try {
            zRange = ((verticalRange != null) ? verticalRange.getRange(this.coordSys) : null);
        }
        catch (InvalidRangeException e) {
            WcsCoverage.log.error("writeCoverageDataToFile(): Failed to subset coverage <" + this.coverage.getName() + "> along vertical range <" + verticalRange + ">: " + e.getMessage());
            throw new WcsException(WcsException.Code.CoverageNotDefined, "Vertical", "Failed to subset coverage [" + this.coverage.getName() + "] along vertical range.");
        }
        Range tRange = null;
        if (timeRange != null) {
            final CoordinateAxis1DTime timeAxis = this.coordSys.getTimeAxis1D();
            final DateType requestStartTime = timeRange.getStart();
            final DateType requestEndTime = timeRange.getEnd();
            final int startIndex = timeAxis.findTimeIndexFromDate(requestStartTime.getDate());
            final int endIndex = timeAxis.findTimeIndexFromDate(requestEndTime.getDate());
            if (startIndex < 0 || startIndex > timeAxis.getSize() - 1L || endIndex < 0 || endIndex > timeAxis.getSize() - 1L) {
                final String availStart = timeAxis.getDateRange().getStart().toDateTimeStringISO();
                final String availEnd = timeAxis.getDateRange().getEnd().toDateTimeStringISO();
                final String msg = "Requested temporal range [" + requestStartTime.toDateTimeStringISO() + " - " + requestEndTime.toDateTimeStringISO() + "] not in available range [" + availStart + " - " + availEnd + "].";
                WcsCoverage.log.debug("writeCoverageDataToFile(): " + msg);
                throw new WcsException(WcsException.Code.CoverageNotDefined, "Time", msg);
            }
            try {
                tRange = new Range(startIndex, endIndex);
            }
            catch (InvalidRangeException e2) {
                WcsCoverage.log.error("writeCoverageDataToFile(): Failed to subset coverage [" + this.coverage.getName() + "] along time axis [" + timeRange + "]: " + e2.getMessage());
                throw new WcsException(WcsException.Code.CoverageNotDefined, "Time", "Failed to subset coverage [" + this.coverage.getName() + "] along time axis [" + timeRange + "].");
            }
        }
        try {
            if (format == Request.Format.GeoTIFF || format == Request.Format.GeoTIFF_Float) {
                final File dir = new File(getDiskCache().getRootDirectory());
                final File tifFile = File.createTempFile("WCS", ".tif", dir);
                if (WcsCoverage.log.isDebugEnabled()) {
                    WcsCoverage.log.debug("writeCoverageDataToFile(): tifFile=" + tifFile.getPath());
                }
                try {
                    final GridDatatype subset = this.coverage.makeSubset(tRange, zRange, bboxLatLonRect, 1, 1, 1);
                    final Array data = subset.readDataSlice(0, 0, -1, -1);
                    final GeotiffWriter writer = new GeotiffWriter(tifFile.getPath());
                    writer.writeGrid(this.dataset.getDataset(), subset, data, format == Request.Format.GeoTIFF);
                    writer.close();
                }
                catch (InvalidRangeException e3) {
                    WcsCoverage.log.error("writeCoverageDataToFile(): Failed to subset coverage <" + this.coverage.getName() + "> along time axis <" + timeRange + ">: " + e3.getMessage());
                    throw new WcsException(WcsException.Code.CoverageNotDefined, "", "Failed to subset coverage [" + this.coverage.getName() + "].");
                }
                catch (IOException e4) {
                    WcsCoverage.log.error("writeCoverageDataToFile(): Failed to write file for requested coverage <" + this.coverage.getName() + ">: " + e4.getMessage());
                    throw new WcsException(WcsException.Code.UNKNOWN, "", "Problem creating coverage [" + this.coverage.getName() + "].");
                }
                return tifFile;
            }
            if (format == Request.Format.NetCDF3) {
                final File dir = new File(getDiskCache().getRootDirectory());
                final File ncFile = File.createTempFile("WCS", ".nc", dir);
                if (WcsCoverage.log.isDebugEnabled()) {
                    WcsCoverage.log.debug("writeCoverageDataToFile(): ncFile=" + ncFile.getPath());
                }
                final NetcdfCFWriter writer2 = new NetcdfCFWriter();
                writer2.makeFile(ncFile.getPath(), this.dataset.getDataset(), Collections.singletonList(this.coverage.getName()), bboxLatLonRect, 1, zRange, timeRange, 1, true);
                return ncFile;
            }
            WcsCoverage.log.error("writeCoverageDataToFile(): Unsupported response encoding format [" + format + "].");
            throw new WcsException(WcsException.Code.InvalidFormat, "Format", "Unsupported response encoding format [" + format + "].");
        }
        catch (InvalidRangeException e5) {
            WcsCoverage.log.error("writeCoverageDataToFile(): Failed to subset coverage <" + this.coverage.getName() + ">: " + e5.getMessage());
            throw new WcsException(WcsException.Code.CoverageNotDefined, "", "Failed to subset coverage [" + this.coverage.getName() + "].");
        }
        catch (IOException e6) {
            WcsCoverage.log.error("writeCoverageDataToFile(): Failed to create or write temporary file for requested coverage <" + this.coverage.getName() + ">: " + e6.getMessage());
            throw new WcsException(WcsException.Code.UNKNOWN, "", "Problem creating coverage [" + this.coverage.getName() + "].");
        }
    }
    
    static {
        WcsCoverage.log = LoggerFactory.getLogger(WcsCoverage.class);
        WcsCoverage.diskCache = null;
    }
    
    public static class VerticalRange
    {
        private double min;
        private double max;
        private int stride;
        private boolean singlePoint;
        
        public VerticalRange(final double point, final int stride) {
            this(point, point, stride);
            this.singlePoint = true;
        }
        
        public VerticalRange(final double minimum, final double maximum, final int stride) {
            this.singlePoint = false;
            if (minimum > maximum) {
                WcsCoverage.log.error("VerticalRange(): Minimum <" + minimum + "> is greater than maximum <" + maximum + ">.");
                throw new IllegalArgumentException("VerticalRange minimum <" + minimum + "> greater than maximum <" + maximum + ">.");
            }
            if (stride < 1) {
                WcsCoverage.log.error("VerticalRange(): stride <" + stride + "> less than one (1 means all points).");
                throw new IllegalArgumentException("VerticalRange stride <" + stride + "> less than one (1 means all points).");
            }
            this.min = minimum;
            this.max = maximum;
            this.stride = stride;
        }
        
        public double getMinimum() {
            return this.min;
        }
        
        public double getMaximum() {
            return this.max;
        }
        
        public int getStride() {
            return this.stride;
        }
        
        public boolean isSinglePoint() {
            return this.singlePoint;
        }
        
        @Override
        public String toString() {
            return "[min=" + this.min + ",max=" + this.max + ",stride=" + this.stride + "]";
        }
        
        public Range getRange(final GridCoordSystem gcs) throws InvalidRangeException {
            if (gcs == null) {
                WcsCoverage.log.error("getRange(): GridCoordSystem must be non-null.");
                throw new IllegalArgumentException("GridCoordSystem must be non-null.");
            }
            final CoordinateAxis1D vertAxis = gcs.getVerticalAxis();
            if (vertAxis == null) {
                WcsCoverage.log.error("getRange(): GridCoordSystem must have vertical axis.");
                throw new IllegalArgumentException("GridCoordSystem must have vertical axis.");
            }
            if (!vertAxis.isNumeric()) {
                WcsCoverage.log.error("getRange(): GridCoordSystem must have numeric vertical axis to support min/max range.");
                throw new IllegalArgumentException("GridCoordSystem must have numeric vertical axis to support min/max range.");
            }
            final int minIndex = vertAxis.findCoordElement(this.min);
            final int maxIndex = vertAxis.findCoordElement(this.max);
            if (minIndex == -1 || maxIndex == -1) {
                WcsCoverage.log.error("getRange(): GridCoordSystem vertical axis does not contain min/max points.");
                throw new IllegalArgumentException("GridCoordSystem vertical axis does not contain min/max points.");
            }
            if (vertAxis.getPositive().equalsIgnoreCase("down")) {
                return new Range(maxIndex, minIndex, this.stride);
            }
            return new Range(minIndex, maxIndex, this.stride);
        }
    }
}
