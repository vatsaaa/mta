// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_Plus;

import org.slf4j.LoggerFactory;
import ucar.ma2.Array;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import java.io.IOException;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.dt.grid.NetcdfCFWriter;
import ucar.nc2.geotiff.GeotiffWriter;
import ucar.ma2.Range;
import java.io.File;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Collection;
import java.util.Set;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import ucar.unidata.geoloc.Projection;
import ucar.unidata.geoloc.ogc.EPSG_OGC_CF_Helper;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.util.DiskCache2;
import java.util.HashMap;
import java.util.List;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.dt.GridDataset;
import org.slf4j.Logger;

public class WcsCoverage
{
    private static Logger log;
    private GridDataset.Gridset coverage;
    private WcsDataset dataset;
    private String name;
    private String label;
    private String description;
    private GridCoordSystem coordSys;
    private String nativeCRS;
    private String defaultRequestCrs;
    private List<WcsRequest.Format> supportedCoverageFormatList;
    private HashMap<String, WcsRangeField> range;
    private static DiskCache2 diskCache;
    
    public WcsCoverage(final GridDataset.Gridset coverage, final WcsDataset dataset) {
        this.dataset = dataset;
        if (this.dataset == null) {
            WcsCoverage.log.error("WcsCoverage(): non-null dataset required.");
            throw new IllegalArgumentException("Non-null dataset required.");
        }
        this.coverage = coverage;
        if (this.coverage == null) {
            WcsCoverage.log.error("WcsCoverage(): non-null coverage required.");
            throw new IllegalArgumentException("Non-null coverage required.");
        }
        this.coordSys = coverage.getGeoCoordSystem();
        if (this.coordSys == null) {
            WcsCoverage.log.error("WcsCoverage(): Coverage must have non-null coordinate system.");
            throw new IllegalArgumentException("Non-null coordinate system required.");
        }
        this.name = this.coordSys.getName();
        this.label = this.coordSys.getName();
        this.range = new HashMap<String, WcsRangeField>();
        final StringBuffer descripSB = new StringBuffer("All parameters on the \"").append(this.name).append("\" coordinate system: ");
        for (final GridDatatype curField : this.coverage.getGrids()) {
            final String stdName = curField.findAttValueIgnoreCase("standard_name", "");
            descripSB.append(stdName.equals("") ? curField.getName() : stdName).append(",");
            final WcsRangeField field = new WcsRangeField(curField);
            this.range.put(field.getName(), field);
        }
        descripSB.setCharAt(descripSB.length() - 1, '.');
        this.description = descripSB.toString();
        this.nativeCRS = EPSG_OGC_CF_Helper.getWcs1_0CrsId(this.coordSys.getProjection());
        this.defaultRequestCrs = "OGC:CRS84";
        (this.supportedCoverageFormatList = new ArrayList<WcsRequest.Format>()).add(WcsRequest.Format.GeoTIFF);
        this.supportedCoverageFormatList.add(WcsRequest.Format.GeoTIFF_Float);
        this.supportedCoverageFormatList.add(WcsRequest.Format.NetCDF3);
    }
    
    GridDataset.Gridset getGridset() {
        return this.coverage;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public GridCoordSystem getCoordinateSystem() {
        return this.coordSys;
    }
    
    public String getDefaultRequestCrs() {
        return this.defaultRequestCrs;
    }
    
    public String getNativeCrs() {
        return this.nativeCRS;
    }
    
    public List<WcsRequest.Format> getSupportedCoverageFormatList() {
        return Collections.unmodifiableList((List<? extends WcsRequest.Format>)this.supportedCoverageFormatList);
    }
    
    public boolean isSupportedCoverageFormat(final WcsRequest.Format covFormat) {
        return this.supportedCoverageFormatList.contains(covFormat);
    }
    
    public boolean isRangeFieldName(final String fieldName) {
        return this.range.containsKey(fieldName);
    }
    
    public Set<String> getRangeFieldNames() {
        return this.range.keySet();
    }
    
    public Collection<WcsRangeField> getRange() {
        return this.range.values();
    }
    
    public static void setDiskCache(final DiskCache2 _diskCache) {
        WcsCoverage.diskCache = _diskCache;
    }
    
    private static DiskCache2 getDiskCache() {
        if (WcsCoverage.diskCache == null) {
            WcsCoverage.log.error("getDiskCache(): Disk cache has not been set.");
            throw new IllegalStateException("Disk cache must be set before calling GetCoverage.getDiskCache().");
        }
        return WcsCoverage.diskCache;
    }
    
    public File writeCoverageDataToFile(final WcsRequest.Format format, final LatLonRect bboxLatLonRect, final AxisSubset vertSubset, final List<String> rangeSubset, final DateRange timeRange) throws WcsException {
        boolean zRangeDone = false;
        boolean tRangeDone = false;
        try {
            final Range zRange = (vertSubset != null) ? vertSubset.getRange() : null;
            zRangeDone = true;
            Range tRange = null;
            if (timeRange != null) {
                final CoordinateAxis1DTime timeAxis = this.coordSys.getTimeAxis1D();
                final int startIndex = timeAxis.findTimeIndexFromDate(timeRange.getStart().getDate());
                final int endIndex = timeAxis.findTimeIndexFromDate(timeRange.getEnd().getDate());
                tRange = new Range(startIndex, endIndex);
                tRangeDone = true;
            }
            if (format == WcsRequest.Format.GeoTIFF || format == WcsRequest.Format.GeoTIFF_Float) {
                if (rangeSubset.size() != 1) {
                    final String msg = "GeoTIFF response encoding only available for single range field selection [" + rangeSubset + "].";
                    WcsCoverage.log.error("writeCoverageDataToFile(): " + msg);
                    throw new WcsException(WcsException.Code.InvalidParameterValue, "RangeSubset", msg);
                }
                final String reqRangeFieldName = rangeSubset.get(0);
                final File dir = new File(getDiskCache().getRootDirectory());
                final File tifFile = File.createTempFile("WCS", ".tif", dir);
                if (WcsCoverage.log.isDebugEnabled()) {
                    WcsCoverage.log.debug("writeCoverageDataToFile(): tifFile=" + tifFile.getPath());
                }
                final WcsRangeField rangeField = this.range.get(reqRangeFieldName);
                final GridDatatype subset = rangeField.getGridDatatype().makeSubset(tRange, zRange, bboxLatLonRect, 1, 1, 1);
                final Array data = subset.readDataSlice(0, 0, -1, -1);
                final GeotiffWriter writer = new GeotiffWriter(tifFile.getPath());
                writer.writeGrid(this.dataset.getDataset(), subset, data, format == WcsRequest.Format.GeoTIFF);
                writer.close();
                return tifFile;
            }
            else {
                if (format == WcsRequest.Format.NetCDF3) {
                    final File dir2 = new File(getDiskCache().getRootDirectory());
                    final File ncFile = File.createTempFile("WCS", ".nc", dir2);
                    if (WcsCoverage.log.isDebugEnabled()) {
                        WcsCoverage.log.debug("writeCoverageDataToFile(): ncFile=" + ncFile.getPath());
                    }
                    final NetcdfCFWriter writer2 = new NetcdfCFWriter();
                    this.coordSys.getVerticalAxis().isNumeric();
                    writer2.makeFile(ncFile.getPath(), this.dataset.getDataset(), rangeSubset, bboxLatLonRect, 1, zRange, timeRange, 1, true);
                    return ncFile;
                }
                WcsCoverage.log.error("writeCoverageDataToFile(): Unsupported response encoding format [" + format + "].");
                throw new WcsException(WcsException.Code.InvalidFormat, "Format", "Unsupported response encoding format [" + format + "].");
            }
        }
        catch (InvalidRangeException e) {
            String msg2 = "Failed to subset coverage [" + this.getName();
            if (!zRangeDone) {
                msg2 = msg2 + "] along vertical axis [" + vertSubset + "]. ";
            }
            else if (!tRangeDone) {
                msg2 = msg2 + "] along time axis [" + timeRange + "]. ";
            }
            else {
                msg2 = msg2 + "] in horizontal plane [" + bboxLatLonRect + "]. ";
            }
            WcsCoverage.log.error("writeCoverageDataToFile(): " + msg2 + e.getMessage());
            throw new WcsException(WcsException.Code.CoverageNotDefined, "", msg2);
        }
        catch (IOException e2) {
            WcsCoverage.log.error("writeCoverageDataToFile(): Failed to write file for requested coverage <" + this.getName() + ">: " + e2.getMessage());
            throw new WcsException(WcsException.Code.UNKNOWN, "", "Problem creating coverage [" + this.getName() + "].");
        }
    }
    
    static {
        WcsCoverage.log = LoggerFactory.getLogger(WcsCoverage.class);
        WcsCoverage.diskCache = null;
    }
}
