// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_Plus;

import java.io.IOException;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.PrintWriter;
import java.util.Iterator;
import org.jdom.Content;
import org.jdom.Element;
import java.util.List;
import java.util.Collections;
import org.jdom.Document;
import org.jdom.Namespace;

public class ExceptionReport
{
    protected static final Namespace ogcNS;
    private Document exceptionReport;
    
    public ExceptionReport(final WcsException exception) {
        this(Collections.singletonList(exception));
    }
    
    public ExceptionReport(final List<WcsException> exceptions) {
        final Element rootElem = new Element("ServiceExceptionReport", ExceptionReport.ogcNS);
        rootElem.addNamespaceDeclaration(ExceptionReport.ogcNS);
        rootElem.setAttribute("version", "1.2.0");
        if (exceptions != null) {
            for (final WcsException curException : exceptions) {
                final Element exceptionElem = new Element("ServiceException", ExceptionReport.ogcNS);
                if (curException.getCode() != null && !curException.getCode().equals(WcsException.Code.UNKNOWN)) {
                    exceptionElem.setAttribute("code", curException.getCode().toString());
                }
                if (curException.getLocator() != null && !curException.getLocator().equals("")) {
                    exceptionElem.setAttribute("locator", curException.getLocator());
                }
                if (curException.getTextMessages() != null) {
                    for (final String curMessage : curException.getTextMessages()) {
                        exceptionElem.addContent(curMessage);
                    }
                }
                rootElem.addContent(exceptionElem);
            }
        }
        this.exceptionReport = new Document(rootElem);
    }
    
    public Document getExceptionReport() {
        return this.exceptionReport;
    }
    
    public void writeExceptionReport(final PrintWriter pw) throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(this.exceptionReport, pw);
    }
    
    static {
        ogcNS = Namespace.getNamespace("http://www.opengis.net/ogc");
    }
}
