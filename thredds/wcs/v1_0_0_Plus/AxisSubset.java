// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_Plus;

import org.slf4j.LoggerFactory;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.nc2.dataset.CoordinateAxis1D;
import org.slf4j.Logger;

public class AxisSubset
{
    private static Logger log;
    private CoordinateAxis1D coordAxis;
    private double min;
    private double max;
    private int stride;
    
    public AxisSubset(final CoordinateAxis1D coordAxis, final double minimum, final double maximum, final int stride) {
        if (coordAxis == null) {
            AxisSubset.log.error("AxisSubset(): Minimum <" + minimum + "> is greater than maximum <" + maximum + ">.");
            throw new IllegalArgumentException("AxisSubset minimum <" + minimum + "> greater than maximum <" + maximum + ">.");
        }
        this.coordAxis = coordAxis;
        if (minimum > maximum) {
            AxisSubset.log.error("AxisSubset(): Minimum <" + minimum + "> is greater than maximum <" + maximum + ">.");
            throw new IllegalArgumentException("AxisSubset minimum <" + minimum + "> greater than maximum <" + maximum + ">.");
        }
        if (stride < 1) {
            AxisSubset.log.error("AxisSubset(): stride <" + stride + "> less than one (1 means all points).");
            throw new IllegalArgumentException("AxisSubset stride <" + stride + "> less than one (1 means all points).");
        }
        this.min = minimum;
        this.max = maximum;
        this.stride = stride;
    }
    
    public double getMinimum() {
        return this.min;
    }
    
    public double getMaximum() {
        return this.max;
    }
    
    public int getStride() {
        return this.stride;
    }
    
    @Override
    public String toString() {
        return "[min=" + this.min + ",max=" + this.max + ",stride=" + this.stride + "]";
    }
    
    public Range getRange() throws InvalidRangeException {
        if (!this.coordAxis.isNumeric()) {
            AxisSubset.log.error("getRange(): GridCoordSystem must have numeric vertical axis to support min/max range.");
            throw new IllegalArgumentException("GridCoordSystem must have numeric vertical axis to support min/max range.");
        }
        final int minIndex = this.coordAxis.findCoordElement(this.min);
        final int maxIndex = this.coordAxis.findCoordElement(this.max);
        if (minIndex == -1 || maxIndex == -1) {
            AxisSubset.log.error("getRange(): GridCoordSystem vertical axis does not contain min/max points.");
            throw new IllegalArgumentException("GridCoordSystem vertical axis does not contain min/max points.");
        }
        if (this.coordAxis.getPositive().equalsIgnoreCase("down")) {
            return new Range(maxIndex, minIndex, this.stride);
        }
        return new Range(minIndex, maxIndex, this.stride);
    }
    
    static {
        AxisSubset.log = LoggerFactory.getLogger(AxisSubset.class);
    }
}
