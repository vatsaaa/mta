// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_Plus;

import org.slf4j.LoggerFactory;
import java.util.Collection;
import java.util.ArrayList;
import java.text.ParseException;
import ucar.nc2.units.TimeDuration;
import ucar.nc2.units.DateType;
import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.dt.GridCoordSystem;
import java.io.File;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.unidata.geoloc.Projection;
import ucar.unidata.geoloc.ogc.EPSG_OGC_CF_Helper;
import java.util.List;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import org.slf4j.Logger;

public class GetCoverage extends WcsRequest
{
    private static Logger log;
    private WcsCoverage coverage;
    private LatLonRect requestLatLonBBox;
    private AxisSubset requestVertSubset;
    private DateRange timeRange;
    private List<String> rangeSubset;
    private Format format;
    private boolean isSingleTimeRequest;
    private boolean isSingleVerticalRequest;
    private boolean isSingleRangeFieldRequest;
    
    public GetCoverage(final Operation operation, final String version, final WcsDataset dataset, final String coverageId, final String crs, final String responseCRS, final String bbox, final String time, final String rangeSubset, final String format) throws WcsException {
        super(operation, version, dataset);
        this.isSingleTimeRequest = false;
        this.isSingleVerticalRequest = false;
        this.isSingleRangeFieldRequest = false;
        if (coverageId == null) {
            throw new WcsException(WcsException.Code.MissingParameterValue, "coverage", "Coverage identifier required.");
        }
        if (!this.getDataset().isAvailableCoverageName(coverageId)) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "coverage", "Unknown coverage identifier <" + coverageId + ">.");
        }
        this.coverage = this.getDataset().getAvailableCoverage(coverageId);
        if (this.coverage == null) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "coverage", "Unknown coverage identifier <" + coverageId + ">.");
        }
        if (crs == null) {
            throw new WcsException(WcsException.Code.MissingParameterValue, "CRS", "Request CRS required.");
        }
        if (!crs.equalsIgnoreCase(this.coverage.getDefaultRequestCrs())) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "CRS", "Request CRS <" + crs + "> not allowed <" + this.coverage.getDefaultRequestCrs() + ">.");
        }
        final String nativeCRS = EPSG_OGC_CF_Helper.getWcs1_0CrsId(this.coverage.getCoordinateSystem().getProjection());
        if (nativeCRS == null) {
            throw new WcsException(WcsException.Code.CoverageNotDefined, "", "Coverage not in recognized CRS. (???)");
        }
        if (responseCRS == null) {
            if (!nativeCRS.equalsIgnoreCase(this.coverage.getDefaultRequestCrs())) {
                throw new WcsException(WcsException.Code.MissingParameterValue, "Response_CRS", "Response CRS required.");
            }
        }
        else if (!responseCRS.equalsIgnoreCase(nativeCRS)) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "response_CRS", "Respnse CRS <" + responseCRS + "> not allowed <" + nativeCRS + ">.");
        }
        if (bbox != null && !bbox.equals("")) {
            final String[] bboxSplit = this.splitBoundingBox(bbox);
            this.requestLatLonBBox = this.genRequestLatLonBoundingBox(bboxSplit, this.coverage.getCoordinateSystem());
            final CoordinateAxis1D vertAxis = this.coverage.getCoordinateSystem().getVerticalAxis();
            if (vertAxis != null) {
                this.requestVertSubset = this.genRequestVertSubset(bboxSplit, vertAxis);
            }
        }
        if (time != null && !time.equals("")) {
            this.timeRange = this.parseTime(time);
        }
        this.rangeSubset = this.parseRangeSubset(rangeSubset);
        if (format == null || format.equals("")) {
            GetCoverage.log.error("GetCoverage(): FORMAT parameter required.");
            throw new WcsException(WcsException.Code.InvalidParameterValue, "FORMAT", "FORMAT parameter required.");
        }
        try {
            this.format = Format.valueOf(format.trim());
        }
        catch (IllegalArgumentException e) {
            final String msg = "Unknown format value [" + format + "].";
            GetCoverage.log.error("GetCoverage(): " + msg);
            throw new WcsException(WcsException.Code.InvalidParameterValue, "FORMAT", msg);
        }
        if (!this.coverage.isSupportedCoverageFormat(this.format)) {
            final String msg2 = "Unsupported format value [" + format + "].";
            GetCoverage.log.error("GetCoverage(): " + msg2);
            throw new WcsException(WcsException.Code.InvalidParameterValue, "FORMAT", msg2);
        }
        if ((this.format == Format.GeoTIFF || this.format == Format.GeoTIFF_Float) && !this.isSingleTimeRequest && !this.isSingleVerticalRequest && !this.isSingleRangeFieldRequest) {
            final StringBuffer msgB = new StringBuffer("GeoTIFF supported only for requests at a single time [");
            if (time != null) {
                msgB.append(time);
            }
            msgB.append("] and a single vertical level [");
            if (bbox != null) {
                msgB.append(bbox);
            }
            msgB.append("] and a single range field [");
            if (rangeSubset != null) {
                msgB.append(rangeSubset);
            }
            msgB.append("].");
            GetCoverage.log.error("GetCoverage(): " + (Object)msgB);
            throw new WcsException(WcsException.Code.InvalidParameterValue, "FORMAT", msgB.toString());
        }
    }
    
    public Format getFormat() {
        return this.format;
    }
    
    public File writeCoverageDataToFile() throws WcsException {
        return this.coverage.writeCoverageDataToFile(this.format, this.requestLatLonBBox, this.requestVertSubset, this.rangeSubset, this.timeRange);
    }
    
    private String[] splitBoundingBox(final String bbox) throws WcsException {
        if (bbox == null || bbox.equals("")) {
            return null;
        }
        final String[] bboxSplit = bbox.split(",");
        if (bboxSplit.length != 4 && bboxSplit.length != 6) {
            GetCoverage.log.error("splitBoundingBox(): BBOX <" + bbox + "> must be \"minx,miny,maxx,maxy[,minz,maxz]\".");
            throw new WcsException(WcsException.Code.InvalidParameterValue, "BBOX", "BBOX <" + bbox + "> not in expected format \"minx,miny,maxx,maxy[,minz,maxz]\".");
        }
        return bboxSplit;
    }
    
    private LatLonRect genRequestLatLonBoundingBox(final String[] bboxSplit, final GridCoordSystem gcs) throws WcsException {
        if (bboxSplit == null || gcs == null) {
            return null;
        }
        if (bboxSplit.length < 4) {
            throw new IllegalArgumentException("BBOX contains fewer than four items \"" + bboxSplit.toString() + "\".");
        }
        double minx = 0.0;
        double miny = 0.0;
        double maxx = 0.0;
        double maxy = 0.0;
        try {
            minx = Double.parseDouble(bboxSplit[0]);
            miny = Double.parseDouble(bboxSplit[1]);
            maxx = Double.parseDouble(bboxSplit[2]);
            maxy = Double.parseDouble(bboxSplit[3]);
        }
        catch (NumberFormatException e) {
            final String message = "BBOX item(s) have incorrect number format [not double] <" + bboxSplit.toString() + ">.";
            GetCoverage.log.error("genRequestLatLonBoundingBox(): " + message + " - " + e.getMessage());
            throw new WcsException(WcsException.Code.InvalidParameterValue, "BBOX", message);
        }
        final LatLonPointImpl minll = new LatLonPointImpl(miny, minx);
        final LatLonPointImpl maxll = new LatLonPointImpl(maxy, maxx);
        final LatLonRect requestLatLonRect = new LatLonRect(minll, maxll);
        final LatLonRect covLatLonRect = gcs.getLatLonBoundingBox();
        return requestLatLonRect;
    }
    
    private AxisSubset genRequestVertSubset(final String[] bboxSplit, final CoordinateAxis1D vertAxis) throws WcsException {
        if (bboxSplit == null || bboxSplit.length == 4) {
            if (vertAxis == null || vertAxis.getShape(0) == 1) {
                this.isSingleVerticalRequest = true;
            }
            return null;
        }
        if (bboxSplit.length != 6) {
            final String message = "BBOX must have 4 or 6 items [" + bboxSplit.toString() + "].";
            GetCoverage.log.error("genRequestVertSubset(): " + message);
            throw new WcsException(WcsException.Code.InvalidParameterValue, "BBOX", message);
        }
        if (vertAxis == null || vertAxis.getShape(0) == 1) {
            this.isSingleVerticalRequest = true;
            return null;
        }
        double minz = 0.0;
        double maxz = 0.0;
        try {
            minz = Double.parseDouble(bboxSplit[4]);
            maxz = Double.parseDouble(bboxSplit[5]);
        }
        catch (NumberFormatException e) {
            final String message2 = "BBOX item(s) have incorrect number format (not double) [" + bboxSplit.toString() + "].";
            GetCoverage.log.error("genRequestVertSubset(): " + message2 + " - " + e.getMessage());
            throw new WcsException(WcsException.Code.InvalidParameterValue, "BBOX", message2);
        }
        final AxisSubset axisSubset = new AxisSubset(vertAxis, minz, maxz, 1);
        Range range = null;
        try {
            range = axisSubset.getRange();
        }
        catch (InvalidRangeException e2) {
            final String message3 = "BBOX results in invalid array index range [" + bboxSplit.toString() + "].";
            GetCoverage.log.error("genRequestVertSubset(): " + message3 + " - " + e2.getMessage());
            throw new WcsException(WcsException.Code.InvalidParameterValue, "BBOX", message3);
        }
        if (range.length() == 1) {
            this.isSingleVerticalRequest = true;
            return null;
        }
        return axisSubset;
    }
    
    private DateRange parseTime(final String time) throws WcsException {
        if (time == null || time.equals("")) {
            return null;
        }
        DateRange dateRange;
        try {
            if (time.indexOf(",") != -1) {
                GetCoverage.log.error("parseTime(): Unsupported time parameter (list) <" + time + ">.");
                throw new WcsException(WcsException.Code.InvalidParameterValue, "TIME", "Not currently supporting time list.");
            }
            if (time.indexOf("/") != -1) {
                final String[] timeRange = time.split("/");
                if (timeRange.length != 2) {
                    GetCoverage.log.error("parseTime(): Unsupported time parameter (time range with resolution) <" + time + ">.");
                    throw new WcsException(WcsException.Code.InvalidParameterValue, "TIME", "Not currently supporting time range with resolution.");
                }
                dateRange = new DateRange(new DateType(timeRange[0], null, null), new DateType(timeRange[1], null, null), null, null);
            }
            else {
                final DateType date = new DateType(time, null, null);
                dateRange = new DateRange(date, date, null, null);
                this.isSingleTimeRequest = true;
            }
        }
        catch (ParseException e) {
            GetCoverage.log.error("parseTime(): Failed to parse time parameter <" + time + ">: " + e.getMessage());
            throw new WcsException(WcsException.Code.InvalidParameterValue, "TIME", "Invalid time format <" + time + ">.");
        }
        return dateRange;
    }
    
    private List<String> parseRangeSubset(final String rangeSubset) throws WcsException {
        final List<String> response = new ArrayList<String>();
        if (rangeSubset == null || rangeSubset.equals("")) {
            response.addAll(this.coverage.getRangeFieldNames());
            if (response.size() == 1) {
                this.isSingleRangeFieldRequest = true;
            }
            return response;
        }
        String[] fieldSubsetArray;
        if (rangeSubset.indexOf(";") == -1) {
            fieldSubsetArray = new String[] { rangeSubset };
        }
        else {
            fieldSubsetArray = rangeSubset.split(";");
        }
        for (final String curFieldSubset : fieldSubsetArray) {
            if (!this.coverage.isRangeFieldName(curFieldSubset)) {
                final String message = "Requested range field <" + curFieldSubset + "> not available.";
                GetCoverage.log.warn("parseRangeSubset(): " + message);
                throw new WcsException(WcsException.Code.InvalidParameterValue, "RangeSubset", message);
            }
            response.add(curFieldSubset);
        }
        if (response.size() == 1) {
            this.isSingleRangeFieldRequest = true;
        }
        return response;
    }
    
    static {
        GetCoverage.log = LoggerFactory.getLogger(GetCoverage.class);
    }
}
