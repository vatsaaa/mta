// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_Plus;

import java.util.Collections;
import java.util.Collection;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Iterator;
import java.util.HashMap;
import ucar.nc2.dt.GridDataset;

public class WcsDataset
{
    private String datasetPath;
    private String datasetName;
    private GridDataset dataset;
    private HashMap<String, WcsCoverage> availableCoverages;
    
    public WcsDataset(final GridDataset dataset, final String datasetPath) {
        this.datasetPath = datasetPath;
        final int pos = datasetPath.lastIndexOf("/");
        this.datasetName = ((pos > 0) ? datasetPath.substring(pos + 1) : datasetPath);
        this.dataset = dataset;
        this.availableCoverages = new HashMap<String, WcsCoverage>();
        for (final GridDataset.Gridset curGridset : this.dataset.getGridsets()) {
            final GridCoordSystem gcs = curGridset.getGeoCoordSystem();
            if (!gcs.isRegularSpatial()) {
                continue;
            }
            this.availableCoverages.put(gcs.getName(), new WcsCoverage(curGridset, this));
        }
    }
    
    public String getDatasetPath() {
        return this.datasetPath;
    }
    
    public String getDatasetName() {
        return this.datasetName;
    }
    
    public GridDataset getDataset() {
        return this.dataset;
    }
    
    public boolean isAvailableCoverageName(final String name) {
        return this.availableCoverages.containsKey(name);
    }
    
    public WcsCoverage getAvailableCoverage(final String name) {
        return this.availableCoverages.get(name);
    }
    
    public Collection<WcsCoverage> getAvailableCoverageCollection() {
        return Collections.unmodifiableCollection((Collection<? extends WcsCoverage>)this.availableCoverages.values());
    }
}
