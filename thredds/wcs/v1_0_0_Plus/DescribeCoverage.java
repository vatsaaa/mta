// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_Plus;

import java.util.Date;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import org.jdom.Attribute;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dt.GridCoordSystem;
import org.jdom.Content;
import org.jdom.Element;
import java.io.IOException;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.PrintWriter;
import java.util.Iterator;
import org.jdom.Document;
import java.util.List;

public class DescribeCoverage extends WcsRequest
{
    private List<String> coverages;
    private Document describeCoverageDoc;
    
    public DescribeCoverage(final Operation operation, final String version, final WcsDataset dataset, final List<String> coverages) {
        super(operation, version, dataset);
        this.coverages = coverages;
        if (this.coverages == null) {
            throw new IllegalArgumentException("Non-null coverage list required.");
        }
        if (this.coverages.size() < 1) {
            throw new IllegalArgumentException("Coverage list must contain at least one ID <" + this.coverages.size() + ">.");
        }
        String badCovIds = "";
        for (final String curCov : coverages) {
            if (!this.getDataset().isAvailableCoverageName(curCov)) {
                badCovIds = badCovIds + ((badCovIds.length() > 0) ? ", " : "") + curCov;
            }
        }
        if (badCovIds.length() > 0) {
            throw new IllegalArgumentException("Coverage ID list contains one or more unknown IDs <" + badCovIds + ">.");
        }
    }
    
    public Document getDescribeCoverageDoc() {
        if (this.describeCoverageDoc == null) {
            this.describeCoverageDoc = this.generateDescribeCoverageDoc();
        }
        return this.describeCoverageDoc;
    }
    
    public void writeDescribeCoverageDoc(final PrintWriter pw) throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(org.jdom.output.Format.getPrettyFormat());
        xmlOutputter.output(this.getDescribeCoverageDoc(), pw);
    }
    
    public Document generateDescribeCoverageDoc() {
        final Element coverageDescriptionsElem = new Element("CoverageDescription", DescribeCoverage.wcsNS);
        coverageDescriptionsElem.addNamespaceDeclaration(DescribeCoverage.gmlNS);
        coverageDescriptionsElem.addNamespaceDeclaration(DescribeCoverage.xlinkNS);
        coverageDescriptionsElem.setAttribute("version", this.getVersion());
        for (final String curCoverageId : this.coverages) {
            coverageDescriptionsElem.addContent(this.genCoverageOfferingElem(curCoverageId));
        }
        return new Document(coverageDescriptionsElem);
    }
    
    public Element genCoverageOfferingElem(final String covId) {
        final WcsCoverage coverage = this.getDataset().getAvailableCoverage(covId);
        final GridCoordSystem gridCoordSystem = coverage.getCoordinateSystem();
        final Element covDescripElem = this.genCoverageOfferingBriefElem("CoverageOffering", covId, coverage.getLabel(), coverage.getDescription(), gridCoordSystem);
        covDescripElem.addContent(this.genDomainSetElem(coverage));
        covDescripElem.addContent(this.genRangeSetElem(coverage));
        covDescripElem.addContent(this.genSupportedCRSsElem(coverage));
        covDescripElem.addContent(this.genSupportedFormatsElem(coverage));
        covDescripElem.addContent(this.genSupportedInterpolationsElem());
        return covDescripElem;
    }
    
    private Element genDomainSetElem(final WcsCoverage coverage) {
        final Element domainSetElem = new Element("domainSet", DescribeCoverage.wcsNS);
        domainSetElem.addContent(this.genSpatialDomainElem(coverage));
        if (coverage.getCoordinateSystem().hasTimeAxis()) {
            domainSetElem.addContent(this.genTemporalDomainElem(coverage.getCoordinateSystem().getTimeAxis1D()));
        }
        return domainSetElem;
    }
    
    private Element genSpatialDomainElem(final WcsCoverage coverage) {
        final Element spatialDomainElem = new Element("spatialDomain", DescribeCoverage.wcsNS);
        spatialDomainElem.addContent(this.genEnvelopeElem(coverage.getCoordinateSystem()));
        spatialDomainElem.addContent(this.genRectifiedGridElem(coverage));
        return spatialDomainElem;
    }
    
    private Element genRectifiedGridElem(final WcsCoverage coverage) {
        final Element rectifiedGridElem = new Element("RectifiedGrid", DescribeCoverage.gmlNS);
        final CoordinateAxis1D xaxis = (CoordinateAxis1D)coverage.getCoordinateSystem().getXHorizAxis();
        final CoordinateAxis1D yaxis = (CoordinateAxis1D)coverage.getCoordinateSystem().getYHorizAxis();
        final CoordinateAxis1D zaxis = coverage.getCoordinateSystem().getVerticalAxis();
        rectifiedGridElem.setAttribute("srsName", coverage.getNativeCrs());
        final int ndim = (zaxis != null) ? 3 : 2;
        rectifiedGridElem.setAttribute("dimension", Integer.toString(ndim));
        final int[] minValues = new int[ndim];
        final int[] maxValues = new int[ndim];
        maxValues[0] = (int)(xaxis.getSize() - 1L);
        maxValues[1] = (int)(yaxis.getSize() - 1L);
        if (zaxis != null) {
            maxValues[2] = (int)(zaxis.getSize() - 1L);
        }
        final Element limitsElem = new Element("limits", DescribeCoverage.gmlNS);
        limitsElem.addContent(new Element("GridEnvelope", DescribeCoverage.gmlNS).addContent(new Element("low", DescribeCoverage.gmlNS).addContent(this.genIntegerListString(minValues))).addContent(new Element("high", DescribeCoverage.gmlNS).addContent(this.genIntegerListString(maxValues))));
        rectifiedGridElem.addContent(limitsElem);
        rectifiedGridElem.addContent(new Element("axisName", DescribeCoverage.gmlNS).addContent("x"));
        rectifiedGridElem.addContent(new Element("axisName", DescribeCoverage.gmlNS).addContent("y"));
        if (zaxis != null) {
            rectifiedGridElem.addContent(new Element("axisName", DescribeCoverage.gmlNS).addContent("z"));
        }
        final double[] origin = new double[ndim];
        origin[0] = xaxis.getStart();
        origin[1] = yaxis.getStart();
        if (zaxis != null) {
            origin[2] = zaxis.getStart();
        }
        rectifiedGridElem.addContent(new Element("origin", DescribeCoverage.gmlNS).addContent(new Element("pos", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(origin))));
        final double[] xoffset = new double[ndim];
        xoffset[0] = xaxis.getIncrement();
        rectifiedGridElem.addContent(new Element("offsetVector", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(xoffset)));
        final double[] yoffset = new double[ndim];
        yoffset[1] = yaxis.getIncrement();
        rectifiedGridElem.addContent(new Element("offsetVector", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(yoffset)));
        if (zaxis != null) {
            final double[] zoffset = new double[ndim];
            zoffset[2] = zaxis.getIncrement();
            rectifiedGridElem.addContent(new Element("offsetVector", DescribeCoverage.gmlNS).addContent(this.genDoubleListString(zoffset)));
        }
        return rectifiedGridElem;
    }
    
    private String genIntegerListString(final int[] values) {
        final StringBuffer buf = new StringBuffer();
        for (final int intValue : values) {
            if (buf.length() > 0) {
                buf.append(" ");
            }
            buf.append(intValue);
        }
        return buf.toString();
    }
    
    private String genDoubleListString(final double[] values) {
        final StringBuffer buf = new StringBuffer();
        for (final double doubleValue : values) {
            if (buf.length() > 0) {
                buf.append(" ");
            }
            buf.append(doubleValue);
        }
        return buf.toString();
    }
    
    private Element genEnvelopeElem(final GridCoordSystem gcs) {
        Element envelopeElem;
        if (gcs.hasTimeAxis()) {
            envelopeElem = new Element("EnvelopeWithTimePeriod", DescribeCoverage.wcsNS);
        }
        else {
            envelopeElem = new Element("Envelope", DescribeCoverage.wcsNS);
        }
        envelopeElem.setAttribute("srsName", "urn:ogc:def:crs:OGC:1.3:CRS84");
        final LatLonRect llbb = gcs.getLatLonBoundingBox();
        final LatLonPoint llpt = llbb.getLowerLeftPoint();
        final LatLonPoint urpt = llbb.getUpperRightPoint();
        final double lon = llpt.getLongitude() + llbb.getWidth();
        int posDim = 2;
        String firstPosition = llpt.getLongitude() + " " + llpt.getLatitude();
        String secondPosition = lon + " " + urpt.getLatitude();
        final CoordinateAxis1D vertAxis = gcs.getVerticalAxis();
        if (vertAxis != null) {
            ++posDim;
            final double zeroIndexValue = vertAxis.getCoordValue(0);
            final double sizeIndexValue = vertAxis.getCoordValue((int)vertAxis.getSize() - 1);
            if (vertAxis.getPositive().equals("up")) {
                firstPosition = firstPosition + " " + zeroIndexValue;
                secondPosition = secondPosition + " " + sizeIndexValue;
            }
            else {
                firstPosition = firstPosition + " " + sizeIndexValue;
                secondPosition = secondPosition + " " + zeroIndexValue;
            }
        }
        final String posDimString = Integer.toString(posDim);
        envelopeElem.addContent(new Element("pos", DescribeCoverage.gmlNS).addContent(firstPosition).setAttribute(new Attribute("dimension", posDimString)));
        envelopeElem.addContent(new Element("pos", DescribeCoverage.gmlNS).addContent(secondPosition).setAttribute(new Attribute("dimension", posDimString)));
        if (gcs.hasTimeAxis()) {
            envelopeElem.addContent(new Element("timePosition", DescribeCoverage.gmlNS).addContent(gcs.getDateRange().getStart().toDateTimeStringISO()));
            envelopeElem.addContent(new Element("timePosition", DescribeCoverage.gmlNS).addContent(gcs.getDateRange().getEnd().toDateTimeStringISO()));
        }
        return envelopeElem;
    }
    
    private Element genTemporalDomainElem(final CoordinateAxis1DTime timeAxis) {
        final Element temporalDomainElem = new Element("temporalDomain", DescribeCoverage.wcsNS);
        final Date[] dates = timeAxis.getTimeDates();
        final DateFormatter formatter = new DateFormatter();
        for (final Date curDate : dates) {
            temporalDomainElem.addContent(new Element("timePosition", DescribeCoverage.gmlNS).addContent(formatter.toDateTimeStringISO(curDate)));
        }
        return temporalDomainElem;
    }
    
    private Element genRangeSetElem(final WcsCoverage coverage) {
        final Element rangeSetElem = new Element("rangeSet", DescribeCoverage.wcsNS);
        for (final WcsRangeField curField : coverage.getRange()) {
            final Element fieldElem = new Element("Field", DescribeCoverage.wcsNS);
            if (curField.getDescription() != null) {
                fieldElem.addContent(new Element("description").addContent(curField.getDescription()));
            }
            fieldElem.addContent(new Element("name", DescribeCoverage.wcsNS).addContent(curField.getName()));
            fieldElem.addContent(new Element("label", DescribeCoverage.wcsNS).addContent(curField.getLabel()));
            fieldElem.addContent(new Element("dataType", DescribeCoverage.wcsNS).addContent(curField.getDatatypeString()));
            fieldElem.addContent(new Element("units", DescribeCoverage.wcsNS).addContent(curField.getUnitsString()));
            final Element valuesElem = new Element("AllowedValues", DescribeCoverage.wcsNS);
            final Element minElem = new Element("MinimumValue", DescribeCoverage.wcsNS).addContent(Double.toString(curField.getValidMin()));
            final Element maxElem = new Element("MaximumValue", DescribeCoverage.wcsNS).addContent(Double.toString(curField.getValidMin()));
            valuesElem.addContent(new Element("Range", DescribeCoverage.wcsNS).addContent(minElem).addContent(maxElem));
            final List<WcsRangeField.Axis> axes = curField.getAxes();
            for (final WcsRangeField.Axis curAxis : axes) {
                if (curAxis != null) {
                    final Element axisDescElem = new Element("Axis", DescribeCoverage.wcsNS);
                    axisDescElem.addContent(new Element("name", DescribeCoverage.wcsNS).addContent(curAxis.getName()));
                    axisDescElem.addContent(new Element("label", DescribeCoverage.wcsNS).addContent(curAxis.getLabel()));
                    final Element axisValuesElem = new Element("values", DescribeCoverage.wcsNS);
                    for (final String curVal : curAxis.getValues()) {
                        axisValuesElem.addContent(new Element("singleValue", DescribeCoverage.wcsNS).addContent(curVal));
                    }
                    axisDescElem.addContent(axisValuesElem);
                    fieldElem.addContent(axisDescElem);
                }
            }
            if (curField.hasMissingData()) {
                fieldElem.addContent(new Element("nullValues", DescribeCoverage.wcsNS).addContent(new Element("singleValue", DescribeCoverage.wcsNS).addContent("NaN")));
            }
            rangeSetElem.addContent(fieldElem);
        }
        return rangeSetElem;
    }
    
    private Element genSupportedCRSsElem(final WcsCoverage coverage) {
        final Element supportedCRSsElem = new Element("supportedCRSs", DescribeCoverage.wcsNS);
        supportedCRSsElem.addContent(new Element("requestCRSs", DescribeCoverage.wcsNS).addContent(coverage.getDefaultRequestCrs()));
        supportedCRSsElem.addContent(new Element("responseCRSs", DescribeCoverage.wcsNS).addContent(coverage.getNativeCrs()));
        return supportedCRSsElem;
    }
    
    private Element genSupportedFormatsElem(final WcsCoverage coverage) {
        final Element supportedFormatsElem = new Element("supportedFormats", DescribeCoverage.wcsNS);
        for (final Format curFormat : coverage.getSupportedCoverageFormatList()) {
            supportedFormatsElem.addContent(new Element("formats", DescribeCoverage.wcsNS).addContent(curFormat.toString()));
        }
        return supportedFormatsElem;
    }
    
    private Element genSupportedInterpolationsElem() {
        final Element supportedInterpolationsElem = new Element("supportedInterpolations", DescribeCoverage.wcsNS);
        supportedInterpolationsElem.addContent(new Element("interpolationMethod", DescribeCoverage.wcsNS).addContent("none"));
        return supportedInterpolationsElem;
    }
}
