// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_0_0_Plus;

import java.util.ArrayList;
import ucar.nc2.dataset.CoordinateAxis1D;
import java.util.Collections;
import java.util.List;
import ucar.nc2.dt.GridDatatype;

public class WcsRangeField
{
    private GridDatatype gridDatatype;
    private String name;
    private String label;
    private String description;
    private String datatypeString;
    private String unitsString;
    private double validMin;
    private double validMax;
    private List<Axis> axes;
    
    public WcsRangeField(final GridDatatype gridDatatype) {
        if (gridDatatype == null) {
            throw new IllegalArgumentException("Range field must be non-null.");
        }
        this.gridDatatype = gridDatatype;
        this.name = this.gridDatatype.getName();
        this.label = this.gridDatatype.getInfo();
        this.description = this.gridDatatype.getDescription();
        this.datatypeString = this.gridDatatype.getDataType().toString();
        this.unitsString = this.gridDatatype.getUnitsString();
        this.validMin = this.gridDatatype.getVariable().getValidMin();
        this.validMax = this.gridDatatype.getVariable().getValidMax();
        this.axes = Collections.emptyList();
    }
    
    GridDatatype getGridDatatype() {
        return this.gridDatatype;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public String getDatatypeString() {
        return this.datatypeString;
    }
    
    public String getUnitsString() {
        return this.unitsString;
    }
    
    public double getValidMin() {
        return this.validMin;
    }
    
    public double getValidMax() {
        return this.validMax;
    }
    
    public boolean hasMissingData() {
        return this.gridDatatype.hasMissingData();
    }
    
    public List<Axis> getAxes() {
        return this.axes;
    }
    
    public static class Axis
    {
        private CoordinateAxis1D coordAxis;
        private String name;
        private String label;
        private String description;
        private boolean isNumeric;
        private List<String> values;
        
        public Axis(final CoordinateAxis1D coordAxis) {
            this.coordAxis = coordAxis;
            this.name = this.coordAxis.getName();
            this.label = this.coordAxis.getName();
            this.description = this.coordAxis.getDescription();
            this.isNumeric = this.coordAxis.isNumeric();
            this.values = new ArrayList<String>();
            for (int i = 0; i < this.coordAxis.getSize(); ++i) {
                this.values.add(this.coordAxis.getCoordName(i).trim());
            }
        }
        
        CoordinateAxis1D getCoordAxis() {
            return this.coordAxis;
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getLabel() {
            return this.label;
        }
        
        public String getDescription() {
            return this.description;
        }
        
        public boolean isNumeric() {
            return this.isNumeric;
        }
        
        public List<String> getValues() {
            return Collections.unmodifiableList((List<? extends String>)this.values);
        }
    }
}
