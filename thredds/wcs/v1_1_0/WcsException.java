// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_1_0;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WcsException extends Exception
{
    private Code code;
    private String locator;
    private List<String> textMessages;
    
    public WcsException() {
        this.code = Code.NoApplicableCode;
        this.locator = null;
        this.textMessages = Collections.emptyList();
    }
    
    public WcsException(final String message) {
        super(message);
        this.code = Code.NoApplicableCode;
        this.locator = null;
        this.textMessages = Collections.singletonList(message);
    }
    
    public WcsException(final String message, final Throwable cause) {
        super(message, cause);
        this.code = Code.NoApplicableCode;
        this.locator = null;
        this.textMessages = Collections.singletonList(message);
    }
    
    public WcsException(final Throwable cause) {
        super(cause);
        this.code = Code.NoApplicableCode;
        this.locator = null;
        this.textMessages = Collections.singletonList(cause.getMessage());
    }
    
    public WcsException(final Code code, final String locator, final List<String> messages) {
        super(messages.get(0));
        this.code = code;
        this.locator = locator;
        this.textMessages = new ArrayList<String>(messages);
    }
    
    public WcsException(final Code code, final String locator, final String message) {
        super(message);
        this.code = code;
        this.locator = locator;
        this.textMessages = Collections.singletonList(message);
    }
    
    public Code getCode() {
        return this.code;
    }
    
    public String getLocator() {
        return this.locator;
    }
    
    public List<String> getTextMessages() {
        return Collections.unmodifiableList((List<? extends String>)this.textMessages);
    }
    
    public enum Code
    {
        OperationNotSupported, 
        MissingParameterValue, 
        InvalidParameterValue, 
        VersionNegotiationFailed, 
        InvalidUpdateSequence, 
        NoApplicableCode, 
        UnsupportedCombination, 
        NotEnoughStorage;
    }
}
