// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_1_0;

import org.slf4j.LoggerFactory;
import org.jdom.Content;
import org.jdom.Element;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.PrintWriter;
import java.io.IOException;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Collections;
import ucar.nc2.dt.grid.NetcdfCFWriter;
import java.io.File;
import ucar.nc2.util.DiskCache2;
import org.jdom.Document;
import ucar.nc2.dt.GridDataset;
import java.net.URI;
import org.jdom.Namespace;
import org.slf4j.Logger;

public class GetCoverage
{
    private static Logger log;
    protected static final Namespace owcsNS;
    protected static final Namespace owsNS;
    protected static final Namespace xlinkNS;
    private URI serverURI;
    private String identifier;
    private String version;
    private String datasetPath;
    private GridDataset dataset;
    private Document getCoverageDoc;
    private boolean dataOnlyRequest;
    private static DiskCache2 diskCache;
    
    public GetCoverage(final URI serverURI, final String identifier, final String datasetPath, final GridDataset dataset) {
        this.version = "1.1.0";
        this.dataOnlyRequest = true;
        this.serverURI = serverURI;
        this.identifier = identifier;
        this.datasetPath = datasetPath;
        this.dataset = dataset;
        if (this.serverURI == null) {
            throw new IllegalArgumentException("Non-null server URI required.");
        }
        if (this.identifier == null) {
            throw new IllegalArgumentException("Non-null coverage identifier required.");
        }
        if (this.datasetPath == null) {
            throw new IllegalArgumentException("Non-null dataset path required.");
        }
        if (this.dataset == null) {
            throw new IllegalArgumentException("Non-null dataset required.");
        }
    }
    
    public boolean isDataOnlyRequest() {
        return this.dataOnlyRequest;
    }
    
    public static void setDiskCache(final DiskCache2 _diskCache) {
        GetCoverage.diskCache = _diskCache;
    }
    
    private static DiskCache2 getDiskCache() {
        if (GetCoverage.diskCache == null) {
            GetCoverage.log.error("getDiskCache(): Disk cache has not been set.");
            throw new IllegalStateException("Disk cache must be set before calling GetCoverage.getDiskCache().");
        }
        return GetCoverage.diskCache;
    }
    
    public File writeCoverageDataToFile() throws WcsException {
        try {
            final File dir = new File(getDiskCache().getRootDirectory());
            final File ncFile = File.createTempFile("WCS", ".nc", dir);
            final NetcdfCFWriter writer = new NetcdfCFWriter();
            writer.makeFile(ncFile.getPath(), this.dataset, Collections.singletonList(this.identifier), null, null, true, 1, 1, 1);
            return ncFile;
        }
        catch (InvalidRangeException e) {
            GetCoverage.log.error("writeCoverageDataToFile(): Failed to subset coverage <" + this.identifier + ">: " + e.getMessage());
            throw new WcsException(WcsException.Code.UnsupportedCombination, "", "Failed to subset coverage <" + this.identifier + ">.");
        }
        catch (IOException e2) {
            GetCoverage.log.error("writeCoverageDataToFile(): Failed to write file for requested coverage <" + this.identifier + ">: " + e2.getMessage());
            throw new WcsException(WcsException.Code.NoApplicableCode, "", "Problem creating coverage <" + this.identifier + ">.");
        }
    }
    
    public Document getGetCoverageDoc() {
        if (this.getCoverageDoc == null) {
            this.getCoverageDoc = this.generateGetCoverageDoc();
        }
        return this.getCoverageDoc;
    }
    
    public void writeGetCoverageDoc(final PrintWriter pw) throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(this.getGetCoverageDoc(), pw);
    }
    
    public Document generateGetCoverageDoc() {
        final Element coveragesElem = new Element("Coverages", GetCoverage.owcsNS);
        coveragesElem.addNamespaceDeclaration(GetCoverage.owsNS);
        coveragesElem.addNamespaceDeclaration(GetCoverage.xlinkNS);
        coveragesElem.addContent(this.genCoverage(this.identifier));
        return new Document(coveragesElem);
    }
    
    public Element genCoverage(final String covId) {
        final Element covDescripElem = new Element("Coverage", GetCoverage.owcsNS);
        return covDescripElem;
    }
    
    static {
        GetCoverage.log = LoggerFactory.getLogger(GetCoverage.class);
        owcsNS = Namespace.getNamespace("http://www.opengis.net/wcs/1.1/ows");
        owsNS = Namespace.getNamespace("ows", "http://www.opengis.net/ows");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
        GetCoverage.diskCache = null;
    }
}
