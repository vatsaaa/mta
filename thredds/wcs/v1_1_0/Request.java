// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_1_0;

import org.slf4j.LoggerFactory;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import ucar.nc2.dt.GridDataset;
import java.util.List;
import org.slf4j.Logger;

public class Request
{
    private static Logger log;
    private Operation operation;
    private String negotiatedVersion;
    private String expectedVersion;
    private List<GetCapabilities.Section> sections;
    private List<String> identifierList;
    private String identifier;
    private String datasetPath;
    private GridDataset dataset;
    private List<String> availableCoverageNames;
    
    public static Request getGetCapabilitiesRequest(final Operation operation, final String negotiatedVersion, final List<GetCapabilities.Section> sections, final String datasetPath, final GridDataset dataset) {
        final Request req = new Request(operation, negotiatedVersion, datasetPath, dataset);
        if (!operation.equals(Operation.GetCapabilities)) {
            throw new IllegalArgumentException("The \"" + operation.toString() + "\" operation not supported by this method.");
        }
        req.sections = sections;
        if (req.sections == null) {
            throw new IllegalArgumentException("Non-null section list required.");
        }
        return req;
    }
    
    public static Request getDescribeCoverageRequest(final Operation operation, final String negotiatedVersion, final List<String> identifiers, final String datasetPath, final GridDataset dataset) throws WcsException {
        final Request req = new Request(operation, negotiatedVersion, datasetPath, dataset);
        if (!operation.equals(Operation.DescribeCoverage)) {
            throw new IllegalArgumentException("The \"" + operation.toString() + "\" operation not supported by this method.");
        }
        if (!req.availableCoverageNames.containsAll(identifiers)) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "identifiers", "The \"identifiers\" parameter contains unrecognized values: " + identifiers);
        }
        req.identifierList = identifiers;
        return req;
    }
    
    public static Request getGetCoverageRequest(final Operation operation, final String negotiatedVersion, final String identifier, final String datasetPath, final GridDataset dataset) throws WcsException {
        final Request req = new Request(operation, negotiatedVersion, datasetPath, dataset);
        if (!operation.equals(Operation.GetCoverage)) {
            throw new IllegalArgumentException("The \"" + operation.toString() + "\" operation not supported by this method.");
        }
        if (!req.availableCoverageNames.contains(identifier)) {
            throw new WcsException(WcsException.Code.InvalidParameterValue, "identifier", "Unrecognized value in \"identifier\" parameter: " + identifier);
        }
        req.identifier = identifier;
        return req;
    }
    
    Request(final Operation operation, final String negotiatedVersion, final String datasetPath, final GridDataset dataset) {
        this.expectedVersion = "1.1.0";
        this.operation = operation;
        this.negotiatedVersion = negotiatedVersion;
        this.datasetPath = datasetPath;
        this.dataset = dataset;
        this.availableCoverageNames = new ArrayList<String>();
        for (final GridDataset.Gridset gs : this.dataset.getGridsets()) {
            this.availableCoverageNames.add(gs.getGeoCoordSystem().getName());
        }
        if (operation == null) {
            throw new IllegalArgumentException("Non-null operation required.");
        }
        if (this.negotiatedVersion == null) {
            throw new IllegalArgumentException("Non-null negotiated version required.");
        }
        if (!this.negotiatedVersion.equals(this.expectedVersion)) {
            throw new IllegalArgumentException("Version <" + negotiatedVersion + "> not as expected <" + this.expectedVersion + ">.");
        }
        if (this.datasetPath == null) {
            throw new IllegalArgumentException("Non-null dataset path required.");
        }
        if (this.dataset == null) {
            throw new IllegalArgumentException("Non-null dataset required.");
        }
    }
    
    public Operation getOperation() {
        return this.operation;
    }
    
    public String getDatasetName() {
        final int pos = this.datasetPath.lastIndexOf("/");
        return (pos == -1) ? this.datasetPath : this.datasetPath.substring(pos + 1);
    }
    
    public String getDatasetPath() {
        return this.datasetPath;
    }
    
    public GridDataset getDataset() {
        return this.dataset;
    }
    
    public List<String> getAvailableCoverageNames() {
        return this.availableCoverageNames;
    }
    
    public List<GetCapabilities.Section> getSections() {
        return Collections.unmodifiableList((List<? extends GetCapabilities.Section>)this.sections);
    }
    
    public List<String> getIdentifierList() {
        return this.identifierList;
    }
    
    public String getIdentifier() {
        return this.identifier;
    }
    
    static {
        Request.log = LoggerFactory.getLogger(Request.class);
    }
    
    public enum Operation
    {
        GetCapabilities, 
        DescribeCoverage, 
        GetCoverage;
    }
    
    public enum RequestEncoding
    {
        GET_KVP, 
        POST_XML, 
        POST_SOAP;
    }
    
    public enum Format
    {
        NONE(""), 
        GeoTIFF("image/tiff"), 
        GeoTIFF_Float("image/tiff"), 
        NetCDF3("application/x-netcdf");
        
        private String mimeType;
        
        private Format(final String mimeType) {
            this.mimeType = mimeType;
        }
        
        public String getMimeType() {
            return this.mimeType;
        }
        
        public static Format getFormat(final String mimeType) {
            for (final Format curSection : values()) {
                if (curSection.mimeType.equals(mimeType)) {
                    return curSection;
                }
            }
            throw new IllegalArgumentException("No such instance <" + mimeType + ">.");
        }
    }
}
