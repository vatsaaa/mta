// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_1_0;

import java.util.Date;
import ucar.nc2.units.DateFormatter;
import java.util.List;
import ucar.nc2.dt.GridDataset;
import org.jdom.Content;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.Namespace;

public class XMLwriter
{
    protected static final Namespace wcsNS;
    protected static final Namespace owcsNS;
    protected static final Namespace owsNS;
    private XMLOutputter xmlOutputter;
    public static String seqDate;
    
    public XMLwriter() {
        this.xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
    }
    
    public Document generateExceptionReport(final ExceptionCodes code, final String locator, final String message) {
        final Element rootElem = new Element("ExceptionReport", XMLwriter.owsNS);
        rootElem.addNamespaceDeclaration(XMLwriter.owsNS);
        rootElem.setAttribute("version", "1.0.0");
        final Element exceptionElem = new Element("Exception", XMLwriter.owsNS);
        exceptionElem.setAttribute("code", code.toString());
        if (locator != null && !locator.equals("")) {
            exceptionElem.setAttribute("locator", locator);
        }
        if (message != null && !message.equals("")) {
            final Element excTextElem = new Element("ExceptionText", XMLwriter.owsNS);
            excTextElem.addContent(message);
            exceptionElem.addContent(excTextElem);
        }
        rootElem.addContent(exceptionElem);
        return new Document(rootElem);
    }
    
    public Document generateCapabilities(final String serverURL, final GridDataset dataset, final List<GetCapabilities.Section> sections) {
        final Element capabilitiesElem = new Element("Capabilities", XMLwriter.owsNS);
        capabilitiesElem.addNamespaceDeclaration(XMLwriter.owsNS);
        return new Document(capabilitiesElem);
    }
    
    static {
        wcsNS = Namespace.getNamespace("http://www.opengis.net/wcs/1.1");
        owcsNS = Namespace.getNamespace("http://www.opengis.net/wcs/1.1/ows");
        owsNS = Namespace.getNamespace("http://www.opengis.net/ows");
        final DateFormatter formatter = new DateFormatter();
        XMLwriter.seqDate = formatter.toDateTimeStringISO(new Date());
    }
    
    public enum ExceptionCodes
    {
        MissingParameterValue, 
        InvalidParameterValue, 
        VersionNegotiationFailed, 
        InvalidUpdateSequence, 
        NoApplicableCode;
    }
}
