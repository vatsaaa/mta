// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_1_0;

import java.util.Iterator;
import org.jdom.Content;
import org.jdom.Element;
import java.io.IOException;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.PrintWriter;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import ucar.nc2.dt.GridDataset;
import java.util.List;
import java.net.URI;
import org.jdom.Namespace;
import org.slf4j.Logger;

public class DescribeCoverage
{
    private Logger logger;
    protected static final Namespace wcsNS;
    protected static final Namespace owcsNS;
    protected static final Namespace owsNS;
    protected static final Namespace xlinkNS;
    private URI serverURI;
    private List<String> identifiers;
    private String version;
    private GridDataset dataset;
    private Document describeCoverageDoc;
    
    public DescribeCoverage(final URI serverURI, final List<String> identifiers, final GridDataset dataset) {
        this.logger = LoggerFactory.getLogger(DescribeCoverage.class);
        this.version = "1.1.0";
        this.serverURI = serverURI;
        this.identifiers = identifiers;
        this.dataset = dataset;
        if (this.serverURI == null) {
            throw new IllegalArgumentException("Non-null server URI required.");
        }
        if (this.identifiers == null) {
            throw new IllegalArgumentException("Non-null identifier list required.");
        }
        if (this.identifiers.size() < 1) {
            throw new IllegalArgumentException("Identifier list must contain at least one ID <" + this.identifiers.size() + ">.");
        }
        if (this.dataset == null) {
            throw new IllegalArgumentException("Non-null dataset required.");
        }
    }
    
    public Document getDescribeCoverageDoc() {
        if (this.describeCoverageDoc == null) {
            this.describeCoverageDoc = this.generateDescribeCoverageDoc();
        }
        return this.describeCoverageDoc;
    }
    
    public void writeDescribeCoverageDoc(final PrintWriter pw) throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(this.getDescribeCoverageDoc(), pw);
    }
    
    public Document generateDescribeCoverageDoc() {
        final Element coverageDescriptionsElem = new Element("CoverageDescriptions", DescribeCoverage.wcsNS);
        coverageDescriptionsElem.addNamespaceDeclaration(DescribeCoverage.owcsNS);
        coverageDescriptionsElem.addNamespaceDeclaration(DescribeCoverage.owsNS);
        coverageDescriptionsElem.addNamespaceDeclaration(DescribeCoverage.xlinkNS);
        for (final String curId : this.identifiers) {
            coverageDescriptionsElem.addContent(this.genCovDescrip(curId));
        }
        return new Document(coverageDescriptionsElem);
    }
    
    public Element genCovDescrip(final String covId) {
        final Element covDescripElem = new Element("CoverageDescription", DescribeCoverage.wcsNS);
        covDescripElem.addContent(new Element("Identifier", DescribeCoverage.wcsNS).addContent(covId));
        return covDescripElem;
    }
    
    static {
        wcsNS = Namespace.getNamespace("http://www.opengis.net/wcs/1.1");
        owcsNS = Namespace.getNamespace("owcs", "http://www.opengis.net/wcs/1.1/ows");
        owsNS = Namespace.getNamespace("ows", "http://www.opengis.net/ows");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
    }
}
