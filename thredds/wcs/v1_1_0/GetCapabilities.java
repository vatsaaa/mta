// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_1_0;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import org.jdom.Content;
import org.jdom.Element;
import java.io.IOException;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.PrintWriter;
import org.slf4j.LoggerFactory;
import org.jdom.Document;
import ucar.nc2.dt.GridDataset;
import java.util.List;
import java.net.URI;
import org.jdom.Namespace;
import org.slf4j.Logger;

public class GetCapabilities
{
    private Logger logger;
    protected static final Namespace wcsNS;
    protected static final Namespace owcsNS;
    protected static final Namespace owsNS;
    protected static final Namespace xlinkNS;
    private URI serverURI;
    private List<Section> sections;
    private String version;
    private ServiceId serviceId;
    private ServiceProvider serviceProvider;
    private GridDataset dataset;
    private Document capabilitiesReport;
    
    public GetCapabilities(final URI serverURI, final List<Section> sections, final ServiceId serviceId, final ServiceProvider serviceProvider, final GridDataset dataset) {
        this.logger = LoggerFactory.getLogger(GetCapabilities.class);
        this.version = "1.1.0";
        this.serverURI = serverURI;
        this.sections = sections;
        this.serviceId = serviceId;
        this.serviceProvider = serviceProvider;
        this.dataset = dataset;
        if (this.serverURI == null) {
            throw new IllegalArgumentException("Non-null server URI required.");
        }
        if (this.sections == null) {
            throw new IllegalArgumentException("Non-null sections list required (may be empty).");
        }
        if (this.dataset == null) {
            throw new IllegalArgumentException("Non-null dataset required.");
        }
    }
    
    public Document getCapabilitiesReport() {
        if (this.capabilitiesReport == null) {
            this.capabilitiesReport = this.generateCapabilities();
        }
        return this.capabilitiesReport;
    }
    
    public void writeCapabilitiesReport(final PrintWriter pw) throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(this.getCapabilitiesReport(), pw);
    }
    
    public Document generateCapabilities() {
        final Element capabilitiesElem = new Element("Capabilities", GetCapabilities.wcsNS);
        capabilitiesElem.addNamespaceDeclaration(GetCapabilities.owcsNS);
        capabilitiesElem.addNamespaceDeclaration(GetCapabilities.owsNS);
        capabilitiesElem.addNamespaceDeclaration(GetCapabilities.xlinkNS);
        capabilitiesElem.setAttribute("version", this.version);
        boolean allSections = false;
        if (this.sections == null || this.sections.size() == 0 || (this.sections.size() == 1 && this.sections.get(0).equals(Section.All))) {
            allSections = true;
        }
        if (allSections || this.sections.contains(Section.ServiceIdentification)) {
            capabilitiesElem.addContent(this.generateServiceIdentification(this.serviceId));
        }
        if (allSections || this.sections.contains(Section.ServiceProvider)) {
            capabilitiesElem.addContent(this.generateServiceProvider(this.serviceProvider));
        }
        if (allSections || this.sections.contains(Section.OperationsMetadata)) {
            capabilitiesElem.addContent(this.generateOperationsMetadata());
        }
        if (allSections || this.sections.contains(Section.Contents)) {
            capabilitiesElem.addContent(this.generateContents());
        }
        return new Document(capabilitiesElem);
    }
    
    public Element generateServiceIdentification(final ServiceId serviceId) {
        final Element serviceIdElem = new Element("ServiceIdentification", GetCapabilities.owcsNS);
        if (serviceId != null) {
            if (serviceId.getTitle() != null) {
                final Element titleElem = new Element("Title", GetCapabilities.owsNS);
                titleElem.addContent(serviceId.getTitle());
                serviceIdElem.addContent(titleElem);
            }
            if (serviceId.getAbstract() != null) {
                final Element abstractElem = new Element("Abstract", GetCapabilities.owsNS);
                abstractElem.addContent(serviceId.getAbstract());
                serviceIdElem.addContent(abstractElem);
            }
            if (serviceId.getKeywords() != null && serviceId.getKeywords().size() > 0) {
                final Element keywordsElem = new Element("Keywords", GetCapabilities.owsNS);
                for (final String curKey : serviceId.getKeywords()) {
                    final Element keywordElem = new Element("Keyword", GetCapabilities.owsNS);
                    keywordElem.addContent(curKey);
                    keywordsElem.addContent(keywordElem);
                }
                serviceIdElem.addContent(keywordsElem);
            }
            if (serviceId.getServiceType() != null) {
                final Element serviceTypeElem = new Element("ServiceType", GetCapabilities.owcsNS);
                serviceTypeElem.addContent(serviceId.getServiceType());
                serviceIdElem.addContent(serviceTypeElem);
            }
            if (serviceId.getServiceTypeVersion() != null && serviceId.getServiceTypeVersion().size() > 0) {
                for (final String curVer : serviceId.getServiceTypeVersion()) {
                    final Element serviceTypeVersionElem = new Element("ServiceTypeVersion", GetCapabilities.owcsNS);
                    serviceTypeVersionElem.addContent(curVer);
                    serviceIdElem.addContent(serviceTypeVersionElem);
                }
            }
            if (serviceId.getFees() != null) {
                final Element feesElem = new Element("Fees", GetCapabilities.owcsNS);
                feesElem.addContent(serviceId.getFees());
                serviceIdElem.addContent(feesElem);
            }
            if (serviceId.getAccessConstraints() != null && serviceId.getAccessConstraints().size() > 0) {
                for (final String curAC : serviceId.getAccessConstraints()) {
                    final Element accessConstraintsElem = new Element("AccessConstraints", GetCapabilities.owcsNS);
                    accessConstraintsElem.addContent(curAC);
                    serviceIdElem.addContent(accessConstraintsElem);
                }
            }
        }
        return serviceIdElem;
    }
    
    public Element generateServiceProvider(final ServiceProvider serviceProvider) {
        final Element servProvElem = new Element("ServiceProvider", GetCapabilities.owsNS);
        if (serviceProvider != null) {
            if (serviceProvider.name != null) {
                servProvElem.addContent(new Element("ProviderName", GetCapabilities.owsNS).addContent(serviceProvider.name));
            }
            if (serviceProvider.site != null) {
                final Element provSiteElem = new Element("ProviderSite", GetCapabilities.owsNS);
                provSiteElem.setAttribute("type", "simple");
                if (serviceProvider.site.title != null) {
                    provSiteElem.setAttribute("title", serviceProvider.site.title, GetCapabilities.xlinkNS);
                }
                if (serviceProvider.site.link != null) {
                    provSiteElem.setAttribute("href", serviceProvider.site.link.toString(), GetCapabilities.xlinkNS);
                }
                servProvElem.addContent(provSiteElem);
            }
            if (serviceProvider.contact != null) {
                final Element servContactElem = new Element("ServiceContact", GetCapabilities.owsNS);
                if (serviceProvider.contact.individualName != null) {
                    final Element individualNameElem = new Element("IndividualName", GetCapabilities.owsNS);
                    individualNameElem.addContent(serviceProvider.contact.individualName);
                    servContactElem.addContent(individualNameElem);
                }
                if (serviceProvider.contact.positionName != null) {
                    final Element positionNameElem = new Element("PositionName", GetCapabilities.owsNS);
                    positionNameElem.addContent(serviceProvider.contact.positionName);
                    servContactElem.addContent(positionNameElem);
                }
                if (serviceProvider.contact.contactInfo != null) {
                    final Element contactInfoElem = new Element("ContactInfo", GetCapabilities.owsNS);
                    if (serviceProvider.contact.contactInfo.voicePhone != null || serviceProvider.contact.contactInfo.faxPhone != null) {
                        final Element phoneElem = new Element("Phone", GetCapabilities.owsNS);
                        if (serviceProvider.contact.contactInfo.voicePhone != null) {
                            for (final String curPhone : serviceProvider.contact.contactInfo.voicePhone) {
                                phoneElem.addContent(new Element("Voice", GetCapabilities.owsNS).addContent(curPhone));
                            }
                        }
                        if (serviceProvider.contact.contactInfo.faxPhone != null) {
                            for (final String curPhone : serviceProvider.contact.contactInfo.faxPhone) {
                                phoneElem.addContent(new Element("Facsimile", GetCapabilities.owsNS).addContent(curPhone));
                            }
                        }
                        contactInfoElem.addContent(phoneElem);
                    }
                    if (serviceProvider.contact.contactInfo.address != null) {
                        final Element addressElem = new Element("Address", GetCapabilities.owsNS);
                        if (serviceProvider.contact.contactInfo.address.deliveryPoint != null) {
                            for (final String curDP : serviceProvider.contact.contactInfo.address.deliveryPoint) {
                                addressElem.addContent(new Element("DeliveryPoint", GetCapabilities.owsNS).addContent(curDP));
                            }
                        }
                        if (serviceProvider.contact.contactInfo.address.city != null) {
                            addressElem.addContent(new Element("City", GetCapabilities.owsNS).addContent(serviceProvider.contact.contactInfo.address.city));
                        }
                        if (serviceProvider.contact.contactInfo.address.adminArea != null) {
                            addressElem.addContent(new Element("AdministrativeArea", GetCapabilities.owsNS).addContent(serviceProvider.contact.contactInfo.address.adminArea));
                        }
                        if (serviceProvider.contact.contactInfo.address.postalCode != null) {
                            addressElem.addContent(new Element("PostalCode", GetCapabilities.owsNS).addContent(serviceProvider.contact.contactInfo.address.postalCode));
                        }
                        if (serviceProvider.contact.contactInfo.address.country != null) {
                            addressElem.addContent(new Element("Country", GetCapabilities.owsNS).addContent(serviceProvider.contact.contactInfo.address.country));
                        }
                        if (serviceProvider.contact.contactInfo.address.email != null) {
                            for (final String curEmail : serviceProvider.contact.contactInfo.address.email) {
                                addressElem.addContent(new Element("ElectronicMailAddress", GetCapabilities.owsNS).addContent(curEmail));
                            }
                        }
                        contactInfoElem.addContent(addressElem);
                    }
                    if (serviceProvider.contact.contactInfo.onlineResource != null) {
                        final Element onlineResourceElem = new Element("OnlineResource", GetCapabilities.owsNS);
                        onlineResourceElem.setAttribute("type", "simple");
                        if (serviceProvider.contact.contactInfo.onlineResource.title != null) {
                            onlineResourceElem.setAttribute("title", serviceProvider.contact.contactInfo.onlineResource.title, GetCapabilities.xlinkNS);
                        }
                        if (serviceProvider.contact.contactInfo.onlineResource.link != null) {
                            onlineResourceElem.setAttribute("href", serviceProvider.contact.contactInfo.onlineResource.link.toString(), GetCapabilities.xlinkNS);
                        }
                        contactInfoElem.addContent(onlineResourceElem);
                    }
                    if (serviceProvider.contact.contactInfo.hoursOfService != null) {
                        contactInfoElem.addContent(new Element("HoursOfService", GetCapabilities.owsNS).addContent(serviceProvider.contact.contactInfo.hoursOfService));
                    }
                    if (serviceProvider.contact.contactInfo.contactInstructions != null) {
                        contactInfoElem.addContent(new Element("ContactInstructions", GetCapabilities.owsNS).addContent(serviceProvider.contact.contactInfo.contactInstructions));
                    }
                    servContactElem.addContent(contactInfoElem);
                }
                if (serviceProvider.contact.role != null) {
                    servContactElem.addContent(new Element("Role", GetCapabilities.owsNS).addContent(serviceProvider.contact.role));
                }
                servProvElem.addContent(servContactElem);
            }
        }
        return servProvElem;
    }
    
    public Element generateOperationsMetadata() {
        final Element opsMetadataElem = new Element("OperationsMetadata", GetCapabilities.owcsNS);
        opsMetadataElem.addContent(this.genGetCapOpsElement());
        opsMetadataElem.addContent(this.genDescCovOpsElement());
        opsMetadataElem.addContent(this.genGetCovOpsElement());
        return opsMetadataElem;
    }
    
    private Element genGetCapOpsElement() {
        final Element getCapOpsElem = new Element("Operation", GetCapabilities.owcsNS);
        getCapOpsElem.setAttribute("name", Request.Operation.GetCapabilities.toString());
        getCapOpsElem.addContent(new Element("DCP", GetCapabilities.owcsNS).addContent(new Element("HTTP", GetCapabilities.owcsNS).addContent(new Element("GET", GetCapabilities.owcsNS).setAttribute("href", this.serverURI.toString(), GetCapabilities.xlinkNS))));
        getCapOpsElem.addContent(this.genParamElement("service", Collections.singletonList("WCS")));
        final List<String> allowedValList = new ArrayList<String>();
        allowedValList.add("1.1.0");
        allowedValList.add("1.0.0");
        getCapOpsElem.addContent(this.genParamElement("AcceptVersions", allowedValList));
        final List<String> sectList = new ArrayList<String>();
        sectList.add("ServiceIdentification");
        sectList.add("ServiceProvider");
        sectList.add("OperationsMetadata");
        sectList.add("Content");
        sectList.add("All");
        getCapOpsElem.addContent(this.genParamElement("Sections", sectList));
        return getCapOpsElem;
    }
    
    private Element genDescCovOpsElement() {
        final Element descCovOpsElem = new Element("Operation", GetCapabilities.owcsNS);
        descCovOpsElem.setAttribute("name", Request.Operation.DescribeCoverage.toString());
        descCovOpsElem.addContent(new Element("DCP", GetCapabilities.owcsNS).addContent(new Element("HTTP", GetCapabilities.owcsNS).addContent(new Element("GET", GetCapabilities.owcsNS).setAttribute("href", this.serverURI.toString(), GetCapabilities.xlinkNS))));
        descCovOpsElem.addContent(this.genParamElement("service", Collections.singletonList("WCS")));
        descCovOpsElem.addContent(this.genParamElement("version", Collections.singletonList("1.1.0")));
        final List<String> idList = new ArrayList<String>();
        for (final GridDataset.Gridset gs : this.dataset.getGridsets()) {
            idList.add(gs.getGeoCoordSystem().getName());
        }
        descCovOpsElem.addContent(this.genParamElement("Identifier", idList));
        return descCovOpsElem;
    }
    
    private Element genGetCovOpsElement() {
        final Element getCovOpsElem = new Element("Operation", GetCapabilities.owcsNS);
        getCovOpsElem.setAttribute("name", Request.Operation.GetCoverage.toString());
        getCovOpsElem.addContent(new Element("DCP", GetCapabilities.owcsNS).addContent(new Element("HTTP", GetCapabilities.owcsNS).addContent(new Element("GET", GetCapabilities.owcsNS).setAttribute("href", this.serverURI.toString(), GetCapabilities.xlinkNS))));
        getCovOpsElem.addContent(this.genParamElement("service", Collections.singletonList("WCS")));
        getCovOpsElem.addContent(this.genParamElement("version", Collections.singletonList("1.1.0")));
        getCovOpsElem.addContent(this.genParamElement("store", Collections.singletonList("False")));
        final List<String> idList = new ArrayList<String>();
        for (final GridDataset.Gridset gs : this.dataset.getGridsets()) {
            idList.add(gs.getGeoCoordSystem().getName());
        }
        getCovOpsElem.addContent(this.genParamElement("Identifier", idList));
        return getCovOpsElem;
    }
    
    private Element genParamElement(final String name, final List<String> allowedValues) {
        final Element paramElem = new Element("Parameter", GetCapabilities.owcsNS).setAttribute("name", name);
        final Element allowedValuesElem = new Element("AllowedValues", GetCapabilities.owcsNS);
        for (final String curVal : allowedValues) {
            allowedValuesElem.addContent(new Element("Value", GetCapabilities.owcsNS).addContent(curVal));
        }
        return paramElem.addContent(allowedValuesElem);
    }
    
    public Element generateContents() {
        final Element contentElem = new Element("Contents", GetCapabilities.wcsNS);
        for (final GridDataset.Gridset gs : this.dataset.getGridsets()) {
            final Element curCovSum = new Element("CoverageSummary", GetCapabilities.wcsNS);
            curCovSum.addContent(new Element("Title", GetCapabilities.owsNS).addContent(gs.getGeoCoordSystem().getName()));
            curCovSum.addContent(new Element("SupportedFormats", GetCapabilities.owsNS).addContent("application/x-netcdf"));
            curCovSum.addContent(new Element("Identifier", GetCapabilities.wcsNS).addContent(gs.getGeoCoordSystem().getName()));
            contentElem.addContent(curCovSum);
        }
        return contentElem;
    }
    
    static {
        wcsNS = Namespace.getNamespace("http://www.opengis.net/wcs/1.1");
        owcsNS = Namespace.getNamespace("owcs", "http://www.opengis.net/wcs/1.1/ows");
        owsNS = Namespace.getNamespace("ows", "http://www.opengis.net/ows");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
    }
    
    public enum Section
    {
        ServiceIdentification, 
        ServiceProvider, 
        OperationsMetadata, 
        Contents, 
        All;
    }
    
    public static class ServiceId
    {
        private String title;
        private String _abstract;
        private List<String> keywords;
        private String serviceType;
        private List<String> serviceTypeVersion;
        private String fees;
        private List<String> accessConstraints;
        
        public ServiceId(final String title, final String anAbstract, final List<String> keywords, final String serviceType, final List<String> serviceTypeVersion, final String fees, final List<String> accessConstraints) {
            this.title = title;
            this._abstract = anAbstract;
            this.keywords = new ArrayList<String>(keywords);
            this.serviceType = serviceType;
            this.serviceTypeVersion = new ArrayList<String>(serviceTypeVersion);
            this.fees = fees;
            this.accessConstraints = new ArrayList<String>(accessConstraints);
        }
        
        public String getTitle() {
            return this.title;
        }
        
        public String getAbstract() {
            return this._abstract;
        }
        
        public List<String> getKeywords() {
            return Collections.unmodifiableList((List<? extends String>)this.keywords);
        }
        
        public String getServiceType() {
            return this.serviceType;
        }
        
        public List<String> getServiceTypeVersion() {
            return Collections.unmodifiableList((List<? extends String>)this.serviceTypeVersion);
        }
        
        public String getFees() {
            return this.fees;
        }
        
        public List<String> getAccessConstraints() {
            return Collections.unmodifiableList((List<? extends String>)this.accessConstraints);
        }
    }
    
    public static class ServiceProvider
    {
        private String name;
        private OnlineResource site;
        private ServiceContact contact;
        
        public ServiceProvider(final String name, final OnlineResource site, final ServiceContact contact) {
            this.name = name;
            this.site = site;
            this.contact = contact;
        }
        
        public String getName() {
            return this.name;
        }
        
        public OnlineResource getSite() {
            return this.site;
        }
        
        public ServiceContact getContact() {
            return this.contact;
        }
        
        public static class OnlineResource
        {
            private URI link;
            private String title;
            
            public OnlineResource(final URI link, final String title) {
                this.link = link;
                this.title = title;
            }
            
            public URI getLink() {
                return this.link;
            }
            
            public String getTitle() {
                return this.title;
            }
        }
        
        public static class ServiceContact
        {
            private String individualName;
            private String positionName;
            private ContactInfo contactInfo;
            private String role;
            
            public ServiceContact(final String individualName, final String positionName, final ContactInfo contactInfo, final String role) {
                this.individualName = individualName;
                this.positionName = positionName;
                this.contactInfo = contactInfo;
                this.role = role;
            }
            
            public String getIndividualName() {
                return this.individualName;
            }
            
            public String getPositionName() {
                return this.positionName;
            }
            
            public ContactInfo getContactInfo() {
                return this.contactInfo;
            }
            
            public String getRole() {
                return this.role;
            }
        }
        
        public static class ContactInfo
        {
            private List<String> voicePhone;
            private List<String> faxPhone;
            private Address address;
            private OnlineResource onlineResource;
            private String hoursOfService;
            private String contactInstructions;
            
            public ContactInfo(final List<String> voicePhone, final List<String> faxPhone, final Address address, final OnlineResource onlineResource, final String hoursOfService, final String contactInstructions) {
                this.voicePhone = new ArrayList<String>(voicePhone);
                this.faxPhone = new ArrayList<String>(faxPhone);
                this.address = address;
                this.onlineResource = onlineResource;
                this.hoursOfService = hoursOfService;
                this.contactInstructions = contactInstructions;
            }
            
            public List<String> getVoicePhone() {
                return Collections.unmodifiableList((List<? extends String>)this.voicePhone);
            }
            
            public List<String> getFaxPhone() {
                return Collections.unmodifiableList((List<? extends String>)this.faxPhone);
            }
            
            public Address getAddress() {
                return this.address;
            }
            
            public OnlineResource getOnlineResource() {
                return this.onlineResource;
            }
            
            public String getHoursOfService() {
                return this.hoursOfService;
            }
            
            public String getContactInstructions() {
                return this.contactInstructions;
            }
        }
        
        public static class Address
        {
            private List<String> deliveryPoint;
            private String city;
            private String adminArea;
            private String postalCode;
            private String country;
            private List<String> email;
            
            public Address(final List<String> deliveryPoint, final String city, final String adminArea, final String postalCode, final String country, final List<String> email) {
                this.deliveryPoint = new ArrayList<String>(deliveryPoint);
                this.city = city;
                this.adminArea = adminArea;
                this.postalCode = postalCode;
                this.country = country;
                this.email = new ArrayList<String>(email);
            }
            
            public List<String> getDeliveryPoint() {
                return Collections.unmodifiableList((List<? extends String>)this.deliveryPoint);
            }
            
            public String getCity() {
                return this.city;
            }
            
            public String getAdminArea() {
                return this.adminArea;
            }
            
            public String getPostalCode() {
                return this.postalCode;
            }
            
            public String getCountry() {
                return this.country;
            }
            
            public List<String> getEmail() {
                return Collections.unmodifiableList((List<? extends String>)this.email);
            }
        }
    }
}
