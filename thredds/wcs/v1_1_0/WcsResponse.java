// 
// Decompiled by Procyon v0.5.36
// 

package thredds.wcs.v1_1_0;

import org.jdom.output.Format;
import org.slf4j.LoggerFactory;
import org.jdom.output.XMLOutputter;
import org.jdom.Namespace;
import org.slf4j.Logger;

public abstract class WcsResponse
{
    private Logger logger;
    protected static final Namespace wcsNS;
    protected static final Namespace owcsNS;
    protected static final Namespace owsNS;
    private XMLOutputter xmlOutputter;
    
    public WcsResponse() {
        this.logger = LoggerFactory.getLogger(WcsResponse.class);
        this.xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
    }
    
    static {
        wcsNS = Namespace.getNamespace("http://www.opengis.net/wcs/1.1");
        owcsNS = Namespace.getNamespace("http://www.opengis.net/wcs/1.1/ows");
        owsNS = Namespace.getNamespace("http://www.opengis.net/ows");
    }
}
