// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import ucar.unidata.util.StringUtil;
import java.io.File;
import net.jcip.annotations.ThreadSafe;
import thredds.inventory.MFile;

@ThreadSafe
public class MFileOS implements MFile
{
    private final File file;
    private Object auxInfo;
    
    MFileOS(final File file) {
        this.file = file;
    }
    
    public long getLastModified() {
        return this.file.lastModified();
    }
    
    public long getLength() {
        return this.file.length();
    }
    
    public boolean isDirectory() {
        return this.file.isDirectory();
    }
    
    public String getPath() {
        return StringUtil.replace(this.file.getPath(), '\\', "/");
    }
    
    public String getName() {
        return this.file.getName();
    }
    
    public int compareTo(final MFile o) {
        return this.getPath().compareTo(o.getPath());
    }
    
    public Object getAuxInfo() {
        return this.auxInfo;
    }
    
    public void setAuxInfo(final Object auxInfo) {
        this.auxInfo = auxInfo;
    }
}
