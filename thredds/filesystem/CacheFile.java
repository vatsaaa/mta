// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import java.util.Iterator;
import java.util.Formatter;
import java.io.File;
import java.util.HashMap;
import java.io.Serializable;

public class CacheFile implements Serializable
{
    protected String shortName;
    protected long lastModified;
    protected long length;
    protected boolean isDirectory;
    private HashMap<String, Object> att;
    
    public String getShortName() {
        return this.shortName;
    }
    
    public long getLastModified() {
        return this.lastModified;
    }
    
    public long getLength() {
        return this.length;
    }
    
    public boolean isDirectory() {
        return this.isDirectory;
    }
    
    public CacheFile() {
    }
    
    public CacheFile(final File f) {
        this.shortName = f.getName();
        this.lastModified = f.lastModified();
        this.length = f.length();
        this.isDirectory = f.isDirectory();
    }
    
    public void setAttribute(final String key, final Object value) {
        if (this.att == null) {
            this.att = new HashMap<String, Object>(5);
        }
        this.att.put(key, value);
    }
    
    public Object getAttribute(final String key) {
        if (this.att == null) {
            return null;
        }
        return this.att.get(key);
    }
    
    @Override
    public String toString() {
        final Formatter f = new Formatter();
        f.format("CacheFile{ shortName='%s' lastModified=%d length=%d isDirectory=%s%n", this.shortName, this.lastModified, this.length, this.isDirectory);
        if (this.att != null) {
            f.format(" attributes:%n", new Object[0]);
            for (final String key : this.att.keySet()) {
                f.format("  %s = %s %n", key, this.att.get(key));
            }
        }
        return f.toString();
    }
}
