// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import java.util.Formatter;
import ucar.unidata.util.StringUtil;
import java.io.File;
import java.io.Serializable;

public class CacheDirectory extends CacheFile implements Serializable
{
    private String parentDirName;
    private CacheFile[] children;
    
    public CacheDirectory(final File dir) {
        super(dir);
        this.parentDirName = StringUtil.replace(dir.getParent(), '\\', "/");
        File[] subs = dir.listFiles();
        if (subs == null) {
            subs = new File[0];
        }
        this.children = new CacheFile[subs.length];
        int count = 0;
        for (final File f : subs) {
            this.children[count++] = new CacheFile(f);
        }
    }
    
    public String getPath() {
        return this.parentDirName + "/" + this.getShortName();
    }
    
    public CacheFile[] getChildren() {
        return this.children;
    }
    
    @Override
    public String toString() {
        final Formatter p = new Formatter();
        p.format("CacheDirector path=%s num=%d %n", this.getPath(), this.children.length);
        for (final CacheFile f : this.children) {
            p.format("  %s%n", f);
        }
        return p.toString();
    }
}
