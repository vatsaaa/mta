// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import java.io.ObjectInput;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.File;
import java.util.HashMap;
import java.io.Externalizable;

public class CacheFileProto implements Externalizable
{
    public static int countRead;
    public static int countReadSize;
    public static int countWrite;
    public static int countWriteSize;
    private static final long serialVersionUID = 7526472295622776147L;
    protected String shortName;
    protected long lastModified;
    protected long length;
    protected boolean isDirectory;
    private HashMap<String, Object> att;
    
    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }
    
    public void setLastModified(final long lastModified) {
        this.lastModified = lastModified;
    }
    
    public void setLength(final long length) {
        this.length = length;
    }
    
    public void setDirectory(final boolean directory) {
        this.isDirectory = directory;
    }
    
    public String getShortName() {
        return this.shortName;
    }
    
    public long getLastModified() {
        return this.lastModified;
    }
    
    public long getLength() {
        return this.length;
    }
    
    public boolean isDirectory() {
        return this.isDirectory;
    }
    
    public void setAttribute(final String key, final Object value) {
        if (this.att == null) {
            this.att = new HashMap<String, Object>(5);
        }
        this.att.put(key, value);
    }
    
    public Object getAttribute(final String key) {
        if (this.att == null) {
            return null;
        }
        return this.att.get(key);
    }
    
    @Override
    public String toString() {
        return "CacheFile{shortName='" + this.shortName + '\'' + ", lastModified=" + this.lastModified + ", length=" + this.length + ", isDirectory=" + this.isDirectory + ", att=" + this.att + '}';
    }
    
    public CacheFileProto(final File f) {
        this.shortName = f.getName();
        this.lastModified = f.lastModified();
        this.length = f.length();
        this.isDirectory = f.isDirectory();
    }
    
    public CacheFileProto() {
    }
    
    public void writeExternal(final ObjectOutput out) throws IOException {
        final FileSystemProto.File.Builder fileBuilder = FileSystemProto.File.newBuilder();
        fileBuilder.setName(this.getShortName());
        fileBuilder.setIsDirectory(this.isDirectory());
        fileBuilder.setLastModified(this.getLastModified());
        fileBuilder.setLength(this.getLength());
        final FileSystemProto.File fileProto = fileBuilder.build();
        final byte[] b = fileProto.toByteArray();
        out.writeInt(b.length);
        out.write(b);
        ++CacheFileProto.countWrite;
        CacheFileProto.countWriteSize += b.length + 4;
    }
    
    public void readExternal(final ObjectInput in) throws IOException, ClassNotFoundException {
        final int len = in.readInt();
        final byte[] b = new byte[len];
        int got;
        for (int done = 0; done < len; done += got) {
            got = in.read(b, done, len - done);
            if (got < 0) {
                throw new IOException();
            }
        }
        final FileSystemProto.File proto = FileSystemProto.File.parseFrom(b);
        this.shortName = proto.getName();
        this.lastModified = proto.getLastModified();
        this.length = proto.getLength();
        this.isDirectory = proto.getIsDirectory();
        ++CacheFileProto.countRead;
        CacheFileProto.countReadSize += len + 4;
    }
    
    static {
        CacheFileProto.countRead = 0;
        CacheFileProto.countReadSize = 0;
        CacheFileProto.countWrite = 0;
        CacheFileProto.countWriteSize = 0;
    }
}
