// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import java.util.List;
import java.io.ObjectInput;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.File;
import java.util.HashMap;
import java.io.Externalizable;

public class CacheDirectoryProto implements Externalizable
{
    private static final long serialVersionUID = 7526472295622776147L;
    protected String path;
    protected long lastModified;
    private CacheFileProto[] children;
    private HashMap<String, Object> att;
    
    public String getPath() {
        return this.path;
    }
    
    public long getLastModified() {
        return this.lastModified;
    }
    
    public CacheDirectoryProto() {
    }
    
    public CacheDirectoryProto(final File dir) {
        this.path = dir.getPath();
        this.lastModified = dir.lastModified();
        File[] subs = dir.listFiles();
        if (subs == null) {
            subs = new File[0];
        }
        this.children = new CacheFileProto[subs.length];
        int count = 0;
        for (final File f : subs) {
            this.children[count++] = new CacheFileProto(f);
        }
    }
    
    public boolean notModified() {
        final File f = new File(this.getPath());
        return f.lastModified() <= this.lastModified;
    }
    
    public CacheFileProto[] getChildren() {
        return this.children;
    }
    
    @Override
    public String toString() {
        return "CacheDirectoryProto {path='" + this.path + '\'' + ", num children=" + ((this.children == null) ? 0 : this.children.length) + '}';
    }
    
    public void writeExternal(final ObjectOutput out) throws IOException {
        final FileSystemProto.Directory.Builder dirBuilder = FileSystemProto.Directory.newBuilder();
        dirBuilder.setPath(this.getPath());
        dirBuilder.setLastModified(this.getLastModified());
        final FileSystemProto.File.Builder fileBuilder = FileSystemProto.File.newBuilder();
        for (final CacheFileProto child : this.children) {
            fileBuilder.clear();
            fileBuilder.setName(child.getShortName());
            fileBuilder.setIsDirectory(child.isDirectory());
            fileBuilder.setLastModified(child.getLastModified());
            fileBuilder.setLength(child.getLength());
            dirBuilder.addFiles(fileBuilder);
        }
        final FileSystemProto.Directory dirProto = dirBuilder.build();
        final byte[] b = dirProto.toByteArray();
        out.writeInt(b.length);
        out.write(b);
    }
    
    public void readExternal(final ObjectInput in) throws IOException, ClassNotFoundException {
        final int len = in.readInt();
        final byte[] b = new byte[len];
        int got;
        for (int done = 0; done < len; done += got) {
            got = in.read(b, done, len - done);
            if (got < 0) {
                throw new IOException();
            }
        }
        final FileSystemProto.Directory proto = FileSystemProto.Directory.parseFrom(b);
        this.path = proto.getPath();
        this.lastModified = proto.getLastModified();
        final List<FileSystemProto.File> files = proto.getFilesList();
        this.children = new CacheFileProto[files.size()];
        for (int i = 0; i < files.size(); ++i) {
            final FileSystemProto.File fp = files.get(i);
            final CacheFileProto cf = new CacheFileProto();
            cf.setShortName(fp.getName());
            cf.setDirectory(fp.getIsDirectory());
            cf.setLastModified(fp.getLastModified());
            cf.setLength(fp.getLength());
            this.children[i] = cf;
        }
    }
}
