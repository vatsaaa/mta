// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import net.jcip.annotations.ThreadSafe;
import thredds.inventory.MFile;

@ThreadSafe
class MFileCached implements MFile
{
    private final String parentDirName;
    private final CacheFile cfile;
    private Object auxInfo;
    
    MFileCached(final String parentDirName, final CacheFile cfile) {
        this.parentDirName = parentDirName;
        this.cfile = cfile;
    }
    
    public long getLastModified() {
        return this.cfile.lastModified;
    }
    
    public long getLength() {
        return this.cfile.length;
    }
    
    public boolean isDirectory() {
        return this.cfile.isDirectory;
    }
    
    public String getPath() {
        return this.parentDirName + "/" + this.cfile.getShortName();
    }
    
    public String getName() {
        return this.cfile.getShortName();
    }
    
    public int compareTo(final MFile o) {
        return this.getPath().compareTo(o.getPath());
    }
    
    public Object getAuxInfo() {
        return this.auxInfo;
    }
    
    public void setAuxInfo(final Object auxInfo) {
        this.auxInfo = auxInfo;
    }
}
