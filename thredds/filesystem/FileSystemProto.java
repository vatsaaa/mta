// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import com.google.protobuf.ProtocolMessageEnum;
import com.google.protobuf.AbstractMessage;
import com.google.protobuf.UnknownFieldSet;
import java.util.Collection;
import java.util.ArrayList;
import com.google.protobuf.UninitializedMessageException;
import com.google.protobuf.CodedInputStream;
import java.io.InputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ByteString;
import java.io.IOException;
import com.google.protobuf.Message;
import com.google.protobuf.CodedOutputStream;
import java.util.Iterator;
import java.util.Collections;
import java.util.List;
import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.Descriptors;

public final class FileSystemProto
{
    private static Descriptors.Descriptor internal_static_filesystem_Directory_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_filesystem_Directory_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_filesystem_File_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_filesystem_File_fieldAccessorTable;
    private static Descriptors.Descriptor internal_static_filesystem_Attribute_descriptor;
    private static GeneratedMessage.FieldAccessorTable internal_static_filesystem_Attribute_fieldAccessorTable;
    private static Descriptors.FileDescriptor descriptor;
    
    private FileSystemProto() {
    }
    
    public static void registerAllExtensions(final ExtensionRegistry registry) {
    }
    
    public static Descriptors.FileDescriptor getDescriptor() {
        return FileSystemProto.descriptor;
    }
    
    static {
        final String descriptorData = "\n#thredds/filesystem/FileSystem.proto\u0012\nfilesystem\"u\n\tDirectory\u0012\f\n\u0004path\u0018\u0001 \u0002(\t\u0012\u0014\n\flastModified\u0018\u0002 \u0001(\u0004\u0012\u001f\n\u0005files\u0018\u0003 \u0003(\u000b2\u0010.filesystem.File\u0012#\n\u0004atts\u0018\u0005 \u0003(\u000b2\u0015.filesystem.Attribute\"t\n\u0004File\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012\u0014\n\flastModified\u0018\u0002 \u0001(\u0004\u0012\u000e\n\u0006length\u0018\u0003 \u0001(\u0004\u0012\u0013\n\u000bisDirectory\u0018\u0004 \u0002(\b\u0012#\n\u0004atts\u0018\u0005 \u0003(\u000b2\u0015.filesystem.Attribute\"±\u0001\n\tAttribute\u0012\f\n\u0004name\u0018\u0001 \u0002(\t\u0012(\n\u0004type\u0018\u0002 \u0002(\u000e2\u001a.filesystem.Attribute.Type\u0012\u000b\n\u0003len\u0018\u0003 \u0002(\r\u0012\f\n\u0004data\u0018\u0004 \u0002(\f\"Q\n\u0004Type\u0012\n\n\u0006STRING\u0010\u0000\u0012\b\n\u0004BYTE\u0010\u0001\u0012\t\n\u0005SHORT\u0010\u0002\u0012\u0007\n\u0003INT\u0010\u0003\u0012\b\n\u0004LONG\u0010\u0004\u0012\t\n\u0005FLOAT\u0010\u0005\u0012\n\n\u0006DOUBLE\u0010\u0006B%\n\u0012thredds.filesystemB\u000fFileSystemProto";
        final Descriptors.FileDescriptor.InternalDescriptorAssigner assigner = (Descriptors.FileDescriptor.InternalDescriptorAssigner)new Descriptors.FileDescriptor.InternalDescriptorAssigner() {
            public ExtensionRegistry assignDescriptors(final Descriptors.FileDescriptor root) {
                FileSystemProto.descriptor = root;
                FileSystemProto.internal_static_filesystem_Directory_descriptor = FileSystemProto.getDescriptor().getMessageTypes().get(0);
                FileSystemProto.internal_static_filesystem_Directory_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(FileSystemProto.internal_static_filesystem_Directory_descriptor, new String[] { "Path", "LastModified", "Files", "Atts" }, (Class)Directory.class, (Class)Directory.Builder.class);
                FileSystemProto.internal_static_filesystem_File_descriptor = FileSystemProto.getDescriptor().getMessageTypes().get(1);
                FileSystemProto.internal_static_filesystem_File_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(FileSystemProto.internal_static_filesystem_File_descriptor, new String[] { "Name", "LastModified", "Length", "IsDirectory", "Atts" }, (Class)File.class, (Class)File.Builder.class);
                FileSystemProto.internal_static_filesystem_Attribute_descriptor = FileSystemProto.getDescriptor().getMessageTypes().get(2);
                FileSystemProto.internal_static_filesystem_Attribute_fieldAccessorTable = new GeneratedMessage.FieldAccessorTable(FileSystemProto.internal_static_filesystem_Attribute_descriptor, new String[] { "Name", "Type", "Len", "Data" }, (Class)Attribute.class, (Class)Attribute.Builder.class);
                return null;
            }
        };
        Descriptors.FileDescriptor.internalBuildGeneratedFileFrom(descriptorData, new Descriptors.FileDescriptor[0], assigner);
    }
    
    public static final class Directory extends GeneratedMessage
    {
        private static final Directory defaultInstance;
        public static final int PATH_FIELD_NUMBER = 1;
        private boolean hasPath;
        private String path_;
        public static final int LASTMODIFIED_FIELD_NUMBER = 2;
        private boolean hasLastModified;
        private long lastModified_;
        public static final int FILES_FIELD_NUMBER = 3;
        private List<File> files_;
        public static final int ATTS_FIELD_NUMBER = 5;
        private List<Attribute> atts_;
        private int memoizedSerializedSize;
        
        private Directory() {
            this.path_ = "";
            this.lastModified_ = 0L;
            this.files_ = Collections.emptyList();
            this.atts_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static Directory getDefaultInstance() {
            return Directory.defaultInstance;
        }
        
        public Directory getDefaultInstanceForType() {
            return Directory.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return FileSystemProto.internal_static_filesystem_Directory_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return FileSystemProto.internal_static_filesystem_Directory_fieldAccessorTable;
        }
        
        public boolean hasPath() {
            return this.hasPath;
        }
        
        public String getPath() {
            return this.path_;
        }
        
        public boolean hasLastModified() {
            return this.hasLastModified;
        }
        
        public long getLastModified() {
            return this.lastModified_;
        }
        
        public List<File> getFilesList() {
            return this.files_;
        }
        
        public int getFilesCount() {
            return this.files_.size();
        }
        
        public File getFiles(final int index) {
            return this.files_.get(index);
        }
        
        public List<Attribute> getAttsList() {
            return this.atts_;
        }
        
        public int getAttsCount() {
            return this.atts_.size();
        }
        
        public Attribute getAtts(final int index) {
            return this.atts_.get(index);
        }
        
        public final boolean isInitialized() {
            if (!this.hasPath) {
                return false;
            }
            for (final File element : this.getFilesList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            for (final Attribute element2 : this.getAttsList()) {
                if (!element2.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasPath()) {
                output.writeString(1, this.getPath());
            }
            if (this.hasLastModified()) {
                output.writeUInt64(2, this.getLastModified());
            }
            for (final File element : this.getFilesList()) {
                output.writeMessage(3, (Message)element);
            }
            for (final Attribute element2 : this.getAttsList()) {
                output.writeMessage(5, (Message)element2);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasPath()) {
                size += CodedOutputStream.computeStringSize(1, this.getPath());
            }
            if (this.hasLastModified()) {
                size += CodedOutputStream.computeUInt64Size(2, this.getLastModified());
            }
            for (final File element : this.getFilesList()) {
                size += CodedOutputStream.computeMessageSize(3, (Message)element);
            }
            for (final Attribute element2 : this.getAttsList()) {
                size += CodedOutputStream.computeMessageSize(5, (Message)element2);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Directory parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Directory parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Directory parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Directory parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Directory parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Directory parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Directory parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Directory parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Directory parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Directory parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Directory prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Directory();
            FileSystemProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Directory result;
            
            private Builder() {
                this.result = new Directory();
            }
            
            protected Directory internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Directory();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Directory.getDescriptor();
            }
            
            public Directory getDefaultInstanceForType() {
                return Directory.getDefaultInstance();
            }
            
            public Directory build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Directory buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Directory buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.files_ != Collections.EMPTY_LIST) {
                    this.result.files_ = (List<File>)Collections.unmodifiableList((List<?>)this.result.files_);
                }
                if (this.result.atts_ != Collections.EMPTY_LIST) {
                    this.result.atts_ = (List<Attribute>)Collections.unmodifiableList((List<?>)this.result.atts_);
                }
                final Directory returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Directory) {
                    return this.mergeFrom((Directory)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Directory other) {
                if (other == Directory.getDefaultInstance()) {
                    return this;
                }
                if (other.hasPath()) {
                    this.setPath(other.getPath());
                }
                if (other.hasLastModified()) {
                    this.setLastModified(other.getLastModified());
                }
                if (!other.files_.isEmpty()) {
                    if (this.result.files_.isEmpty()) {
                        this.result.files_ = (List<File>)new ArrayList();
                    }
                    this.result.files_.addAll(other.files_);
                }
                if (!other.atts_.isEmpty()) {
                    if (this.result.atts_.isEmpty()) {
                        this.result.atts_ = (List<Attribute>)new ArrayList();
                    }
                    this.result.atts_.addAll(other.atts_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setPath(input.readString());
                            continue;
                        }
                        case 16: {
                            this.setLastModified(input.readUInt64());
                            continue;
                        }
                        case 26: {
                            final File.Builder subBuilder = File.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addFiles(subBuilder.buildPartial());
                            continue;
                        }
                        case 42: {
                            final Attribute.Builder subBuilder2 = Attribute.newBuilder();
                            input.readMessage((Message.Builder)subBuilder2, extensionRegistry);
                            this.addAtts(subBuilder2.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasPath() {
                return this.result.hasPath();
            }
            
            public String getPath() {
                return this.result.getPath();
            }
            
            public Builder setPath(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasPath = true;
                this.result.path_ = value;
                return this;
            }
            
            public Builder clearPath() {
                this.result.hasPath = false;
                this.result.path_ = "";
                return this;
            }
            
            public boolean hasLastModified() {
                return this.result.hasLastModified();
            }
            
            public long getLastModified() {
                return this.result.getLastModified();
            }
            
            public Builder setLastModified(final long value) {
                this.result.hasLastModified = true;
                this.result.lastModified_ = value;
                return this;
            }
            
            public Builder clearLastModified() {
                this.result.hasLastModified = false;
                this.result.lastModified_ = 0L;
                return this;
            }
            
            public List<File> getFilesList() {
                return Collections.unmodifiableList((List<? extends File>)this.result.files_);
            }
            
            public int getFilesCount() {
                return this.result.getFilesCount();
            }
            
            public File getFiles(final int index) {
                return this.result.getFiles(index);
            }
            
            public Builder setFiles(final int index, final File value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.files_.set(index, value);
                return this;
            }
            
            public Builder setFiles(final int index, final File.Builder builderForValue) {
                this.result.files_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addFiles(final File value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.files_.isEmpty()) {
                    this.result.files_ = (List<File>)new ArrayList();
                }
                this.result.files_.add(value);
                return this;
            }
            
            public Builder addFiles(final File.Builder builderForValue) {
                if (this.result.files_.isEmpty()) {
                    this.result.files_ = (List<File>)new ArrayList();
                }
                this.result.files_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllFiles(final Iterable<? extends File> values) {
                if (this.result.files_.isEmpty()) {
                    this.result.files_ = (List<File>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.files_);
                return this;
            }
            
            public Builder clearFiles() {
                this.result.files_ = (List<File>)Collections.emptyList();
                return this;
            }
            
            public List<Attribute> getAttsList() {
                return Collections.unmodifiableList((List<? extends Attribute>)this.result.atts_);
            }
            
            public int getAttsCount() {
                return this.result.getAttsCount();
            }
            
            public Attribute getAtts(final int index) {
                return this.result.getAtts(index);
            }
            
            public Builder setAtts(final int index, final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.atts_.set(index, value);
                return this;
            }
            
            public Builder setAtts(final int index, final Attribute.Builder builderForValue) {
                this.result.atts_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addAtts(final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(value);
                return this;
            }
            
            public Builder addAtts(final Attribute.Builder builderForValue) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllAtts(final Iterable<? extends Attribute> values) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.atts_);
                return this;
            }
            
            public Builder clearAtts() {
                this.result.atts_ = (List<Attribute>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class File extends GeneratedMessage
    {
        private static final File defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int LASTMODIFIED_FIELD_NUMBER = 2;
        private boolean hasLastModified;
        private long lastModified_;
        public static final int LENGTH_FIELD_NUMBER = 3;
        private boolean hasLength;
        private long length_;
        public static final int ISDIRECTORY_FIELD_NUMBER = 4;
        private boolean hasIsDirectory;
        private boolean isDirectory_;
        public static final int ATTS_FIELD_NUMBER = 5;
        private List<Attribute> atts_;
        private int memoizedSerializedSize;
        
        private File() {
            this.name_ = "";
            this.lastModified_ = 0L;
            this.length_ = 0L;
            this.isDirectory_ = false;
            this.atts_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }
        
        public static File getDefaultInstance() {
            return File.defaultInstance;
        }
        
        public File getDefaultInstanceForType() {
            return File.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return FileSystemProto.internal_static_filesystem_File_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return FileSystemProto.internal_static_filesystem_File_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasLastModified() {
            return this.hasLastModified;
        }
        
        public long getLastModified() {
            return this.lastModified_;
        }
        
        public boolean hasLength() {
            return this.hasLength;
        }
        
        public long getLength() {
            return this.length_;
        }
        
        public boolean hasIsDirectory() {
            return this.hasIsDirectory;
        }
        
        public boolean getIsDirectory() {
            return this.isDirectory_;
        }
        
        public List<Attribute> getAttsList() {
            return this.atts_;
        }
        
        public int getAttsCount() {
            return this.atts_.size();
        }
        
        public Attribute getAtts(final int index) {
            return this.atts_.get(index);
        }
        
        public final boolean isInitialized() {
            if (!this.hasName) {
                return false;
            }
            if (!this.hasIsDirectory) {
                return false;
            }
            for (final Attribute element : this.getAttsList()) {
                if (!element.isInitialized()) {
                    return false;
                }
            }
            return true;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasLastModified()) {
                output.writeUInt64(2, this.getLastModified());
            }
            if (this.hasLength()) {
                output.writeUInt64(3, this.getLength());
            }
            if (this.hasIsDirectory()) {
                output.writeBool(4, this.getIsDirectory());
            }
            for (final Attribute element : this.getAttsList()) {
                output.writeMessage(5, (Message)element);
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasLastModified()) {
                size += CodedOutputStream.computeUInt64Size(2, this.getLastModified());
            }
            if (this.hasLength()) {
                size += CodedOutputStream.computeUInt64Size(3, this.getLength());
            }
            if (this.hasIsDirectory()) {
                size += CodedOutputStream.computeBoolSize(4, this.getIsDirectory());
            }
            for (final Attribute element : this.getAttsList()) {
                size += CodedOutputStream.computeMessageSize(5, (Message)element);
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static File parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static File parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static File parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static File parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static File parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static File parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static File parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static File parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static File parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static File parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final File prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new File();
            FileSystemProto.getDescriptor();
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            File result;
            
            private Builder() {
                this.result = new File();
            }
            
            protected File internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new File();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return File.getDescriptor();
            }
            
            public File getDefaultInstanceForType() {
                return File.getDefaultInstance();
            }
            
            public File build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private File buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public File buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.atts_ != Collections.EMPTY_LIST) {
                    this.result.atts_ = (List<Attribute>)Collections.unmodifiableList((List<?>)this.result.atts_);
                }
                final File returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof File) {
                    return this.mergeFrom((File)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final File other) {
                if (other == File.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasLastModified()) {
                    this.setLastModified(other.getLastModified());
                }
                if (other.hasLength()) {
                    this.setLength(other.getLength());
                }
                if (other.hasIsDirectory()) {
                    this.setIsDirectory(other.getIsDirectory());
                }
                if (!other.atts_.isEmpty()) {
                    if (this.result.atts_.isEmpty()) {
                        this.result.atts_ = (List<Attribute>)new ArrayList();
                    }
                    this.result.atts_.addAll(other.atts_);
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 16: {
                            this.setLastModified(input.readUInt64());
                            continue;
                        }
                        case 24: {
                            this.setLength(input.readUInt64());
                            continue;
                        }
                        case 32: {
                            this.setIsDirectory(input.readBool());
                            continue;
                        }
                        case 42: {
                            final Attribute.Builder subBuilder = Attribute.newBuilder();
                            input.readMessage((Message.Builder)subBuilder, extensionRegistry);
                            this.addAtts(subBuilder.buildPartial());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasLastModified() {
                return this.result.hasLastModified();
            }
            
            public long getLastModified() {
                return this.result.getLastModified();
            }
            
            public Builder setLastModified(final long value) {
                this.result.hasLastModified = true;
                this.result.lastModified_ = value;
                return this;
            }
            
            public Builder clearLastModified() {
                this.result.hasLastModified = false;
                this.result.lastModified_ = 0L;
                return this;
            }
            
            public boolean hasLength() {
                return this.result.hasLength();
            }
            
            public long getLength() {
                return this.result.getLength();
            }
            
            public Builder setLength(final long value) {
                this.result.hasLength = true;
                this.result.length_ = value;
                return this;
            }
            
            public Builder clearLength() {
                this.result.hasLength = false;
                this.result.length_ = 0L;
                return this;
            }
            
            public boolean hasIsDirectory() {
                return this.result.hasIsDirectory();
            }
            
            public boolean getIsDirectory() {
                return this.result.getIsDirectory();
            }
            
            public Builder setIsDirectory(final boolean value) {
                this.result.hasIsDirectory = true;
                this.result.isDirectory_ = value;
                return this;
            }
            
            public Builder clearIsDirectory() {
                this.result.hasIsDirectory = false;
                this.result.isDirectory_ = false;
                return this;
            }
            
            public List<Attribute> getAttsList() {
                return Collections.unmodifiableList((List<? extends Attribute>)this.result.atts_);
            }
            
            public int getAttsCount() {
                return this.result.getAttsCount();
            }
            
            public Attribute getAtts(final int index) {
                return this.result.getAtts(index);
            }
            
            public Builder setAtts(final int index, final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.atts_.set(index, value);
                return this;
            }
            
            public Builder setAtts(final int index, final Attribute.Builder builderForValue) {
                this.result.atts_.set(index, builderForValue.build());
                return this;
            }
            
            public Builder addAtts(final Attribute value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(value);
                return this;
            }
            
            public Builder addAtts(final Attribute.Builder builderForValue) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                this.result.atts_.add(builderForValue.build());
                return this;
            }
            
            public Builder addAllAtts(final Iterable<? extends Attribute> values) {
                if (this.result.atts_.isEmpty()) {
                    this.result.atts_ = (List<Attribute>)new ArrayList();
                }
                super.addAll((Iterable)values, (Collection)this.result.atts_);
                return this;
            }
            
            public Builder clearAtts() {
                this.result.atts_ = (List<Attribute>)Collections.emptyList();
                return this;
            }
        }
    }
    
    public static final class Attribute extends GeneratedMessage
    {
        private static final Attribute defaultInstance;
        public static final int NAME_FIELD_NUMBER = 1;
        private boolean hasName;
        private String name_;
        public static final int TYPE_FIELD_NUMBER = 2;
        private boolean hasType;
        private Type type_;
        public static final int LEN_FIELD_NUMBER = 3;
        private boolean hasLen;
        private int len_;
        public static final int DATA_FIELD_NUMBER = 4;
        private boolean hasData;
        private ByteString data_;
        private int memoizedSerializedSize;
        
        private Attribute() {
            this.name_ = "";
            this.type_ = Type.STRING;
            this.len_ = 0;
            this.data_ = ByteString.EMPTY;
            this.memoizedSerializedSize = -1;
        }
        
        public static Attribute getDefaultInstance() {
            return Attribute.defaultInstance;
        }
        
        public Attribute getDefaultInstanceForType() {
            return Attribute.defaultInstance;
        }
        
        public static final Descriptors.Descriptor getDescriptor() {
            return FileSystemProto.internal_static_filesystem_Attribute_descriptor;
        }
        
        protected GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return FileSystemProto.internal_static_filesystem_Attribute_fieldAccessorTable;
        }
        
        public boolean hasName() {
            return this.hasName;
        }
        
        public String getName() {
            return this.name_;
        }
        
        public boolean hasType() {
            return this.hasType;
        }
        
        public Type getType() {
            return this.type_;
        }
        
        public boolean hasLen() {
            return this.hasLen;
        }
        
        public int getLen() {
            return this.len_;
        }
        
        public boolean hasData() {
            return this.hasData;
        }
        
        public ByteString getData() {
            return this.data_;
        }
        
        public final boolean isInitialized() {
            return this.hasName && this.hasType && this.hasLen && this.hasData;
        }
        
        public void writeTo(final CodedOutputStream output) throws IOException {
            if (this.hasName()) {
                output.writeString(1, this.getName());
            }
            if (this.hasType()) {
                output.writeEnum(2, this.getType().getNumber());
            }
            if (this.hasLen()) {
                output.writeUInt32(3, this.getLen());
            }
            if (this.hasData()) {
                output.writeBytes(4, this.getData());
            }
            this.getUnknownFields().writeTo(output);
        }
        
        public int getSerializedSize() {
            int size = this.memoizedSerializedSize;
            if (size != -1) {
                return size;
            }
            size = 0;
            if (this.hasName()) {
                size += CodedOutputStream.computeStringSize(1, this.getName());
            }
            if (this.hasType()) {
                size += CodedOutputStream.computeEnumSize(2, this.getType().getNumber());
            }
            if (this.hasLen()) {
                size += CodedOutputStream.computeUInt32Size(3, this.getLen());
            }
            if (this.hasData()) {
                size += CodedOutputStream.computeBytesSize(4, this.getData());
            }
            size += this.getUnknownFields().getSerializedSize();
            return this.memoizedSerializedSize = size;
        }
        
        public static Attribute parseFrom(final ByteString data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Attribute parseFrom(final ByteString data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseFrom(final byte[] data) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data)).buildParsed();
        }
        
        public static Attribute parseFrom(final byte[] data, final ExtensionRegistry extensionRegistry) throws InvalidProtocolBufferException {
            return ((Builder)newBuilder().mergeFrom(data, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input)).buildParsed();
        }
        
        public static Attribute parseFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseDelimitedFrom(final InputStream input) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input)).buildParsed();
        }
        
        public static Attribute parseDelimitedFrom(final InputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return ((Builder)newBuilder().mergeDelimitedFrom(input, extensionRegistry)).buildParsed();
        }
        
        public static Attribute parseFrom(final CodedInputStream input) throws IOException {
            return newBuilder().mergeFrom(input).buildParsed();
        }
        
        public static Attribute parseFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
            return newBuilder().mergeFrom(input, extensionRegistry).buildParsed();
        }
        
        public static Builder newBuilder() {
            return new Builder();
        }
        
        public Builder newBuilderForType() {
            return new Builder();
        }
        
        public static Builder newBuilder(final Attribute prototype) {
            return new Builder().mergeFrom(prototype);
        }
        
        public Builder toBuilder() {
            return newBuilder(this);
        }
        
        static {
            defaultInstance = new Attribute();
            FileSystemProto.getDescriptor();
        }
        
        public enum Type implements ProtocolMessageEnum
        {
            STRING(0, 0), 
            BYTE(1, 1), 
            SHORT(2, 2), 
            INT(3, 3), 
            LONG(4, 4), 
            FLOAT(5, 5), 
            DOUBLE(6, 6);
            
            private static final Type[] VALUES;
            private final int index;
            private final int value;
            
            public final int getNumber() {
                return this.value;
            }
            
            public static Type valueOf(final int value) {
                switch (value) {
                    case 0: {
                        return Type.STRING;
                    }
                    case 1: {
                        return Type.BYTE;
                    }
                    case 2: {
                        return Type.SHORT;
                    }
                    case 3: {
                        return Type.INT;
                    }
                    case 4: {
                        return Type.LONG;
                    }
                    case 5: {
                        return Type.FLOAT;
                    }
                    case 6: {
                        return Type.DOUBLE;
                    }
                    default: {
                        return null;
                    }
                }
            }
            
            public final Descriptors.EnumValueDescriptor getValueDescriptor() {
                return getDescriptor().getValues().get(this.index);
            }
            
            public final Descriptors.EnumDescriptor getDescriptorForType() {
                return getDescriptor();
            }
            
            public static final Descriptors.EnumDescriptor getDescriptor() {
                return Attribute.getDescriptor().getEnumTypes().get(0);
            }
            
            public static Type valueOf(final Descriptors.EnumValueDescriptor desc) {
                if (desc.getType() != getDescriptor()) {
                    throw new IllegalArgumentException("EnumValueDescriptor is not for this type.");
                }
                return Type.VALUES[desc.getIndex()];
            }
            
            private Type(final int index, final int value) {
                this.index = index;
                this.value = value;
            }
            
            static {
                VALUES = new Type[] { Type.STRING, Type.BYTE, Type.SHORT, Type.INT, Type.LONG, Type.FLOAT, Type.DOUBLE };
                FileSystemProto.getDescriptor();
            }
        }
        
        public static final class Builder extends GeneratedMessage.Builder<Builder>
        {
            Attribute result;
            
            private Builder() {
                this.result = new Attribute();
            }
            
            protected Attribute internalGetResult() {
                return this.result;
            }
            
            public Builder clear() {
                this.result = new Attribute();
                return this;
            }
            
            public Builder clone() {
                return new Builder().mergeFrom(this.result);
            }
            
            public Descriptors.Descriptor getDescriptorForType() {
                return Attribute.getDescriptor();
            }
            
            public Attribute getDefaultInstanceForType() {
                return Attribute.getDefaultInstance();
            }
            
            public Attribute build() {
                if (this.result != null && !this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result);
                }
                return this.buildPartial();
            }
            
            private Attribute buildParsed() throws InvalidProtocolBufferException {
                if (!this.isInitialized()) {
                    throw new UninitializedMessageException((Message)this.result).asInvalidProtocolBufferException();
                }
                return this.buildPartial();
            }
            
            public Attribute buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                final Attribute returnMe = this.result;
                this.result = null;
                return returnMe;
            }
            
            public Builder mergeFrom(final Message other) {
                if (other instanceof Attribute) {
                    return this.mergeFrom((Attribute)other);
                }
                super.mergeFrom(other);
                return this;
            }
            
            public Builder mergeFrom(final Attribute other) {
                if (other == Attribute.getDefaultInstance()) {
                    return this;
                }
                if (other.hasName()) {
                    this.setName(other.getName());
                }
                if (other.hasType()) {
                    this.setType(other.getType());
                }
                if (other.hasLen()) {
                    this.setLen(other.getLen());
                }
                if (other.hasData()) {
                    this.setData(other.getData());
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }
            
            public Builder mergeFrom(final CodedInputStream input) throws IOException {
                return this.mergeFrom(input, ExtensionRegistry.getEmptyRegistry());
            }
            
            public Builder mergeFrom(final CodedInputStream input, final ExtensionRegistry extensionRegistry) throws IOException {
                final UnknownFieldSet.Builder unknownFields = UnknownFieldSet.newBuilder(this.getUnknownFields());
                while (true) {
                    final int tag = input.readTag();
                    switch (tag) {
                        case 0: {
                            this.setUnknownFields(unknownFields.build());
                            return this;
                        }
                        default: {
                            if (!this.parseUnknownField(input, unknownFields, extensionRegistry, tag)) {
                                this.setUnknownFields(unknownFields.build());
                                return this;
                            }
                            continue;
                        }
                        case 10: {
                            this.setName(input.readString());
                            continue;
                        }
                        case 16: {
                            final int rawValue = input.readEnum();
                            final Type value = Type.valueOf(rawValue);
                            if (value == null) {
                                unknownFields.mergeVarintField(2, rawValue);
                                continue;
                            }
                            this.setType(value);
                            continue;
                        }
                        case 24: {
                            this.setLen(input.readUInt32());
                            continue;
                        }
                        case 34: {
                            this.setData(input.readBytes());
                            continue;
                        }
                    }
                }
            }
            
            public boolean hasName() {
                return this.result.hasName();
            }
            
            public String getName() {
                return this.result.getName();
            }
            
            public Builder setName(final String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasName = true;
                this.result.name_ = value;
                return this;
            }
            
            public Builder clearName() {
                this.result.hasName = false;
                this.result.name_ = "";
                return this;
            }
            
            public boolean hasType() {
                return this.result.hasType();
            }
            
            public Type getType() {
                return this.result.getType();
            }
            
            public Builder setType(final Type value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasType = true;
                this.result.type_ = value;
                return this;
            }
            
            public Builder clearType() {
                this.result.hasType = false;
                this.result.type_ = Type.STRING;
                return this;
            }
            
            public boolean hasLen() {
                return this.result.hasLen();
            }
            
            public int getLen() {
                return this.result.getLen();
            }
            
            public Builder setLen(final int value) {
                this.result.hasLen = true;
                this.result.len_ = value;
                return this;
            }
            
            public Builder clearLen() {
                this.result.hasLen = false;
                this.result.len_ = 0;
                return this;
            }
            
            public boolean hasData() {
                return this.result.hasData();
            }
            
            public ByteString getData() {
                return this.result.getData();
            }
            
            public Builder setData(final ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                this.result.hasData = true;
                this.result.data_ = value;
                return this;
            }
            
            public Builder clearData() {
                this.result.hasData = false;
                this.result.data_ = ByteString.EMPTY;
                return this;
            }
        }
    }
}
