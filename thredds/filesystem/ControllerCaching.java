// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Arrays;
import java.util.List;
import thredds.inventory.MFile;
import java.util.Iterator;
import thredds.inventory.MCollection;
import java.io.IOException;
import net.jcip.annotations.ThreadSafe;
import thredds.inventory.MController;

@ThreadSafe
public class ControllerCaching implements MController
{
    private CacheManager cacheManager;
    
    public static MController makeStandardController(final String configFile, final String cacheDir) throws IOException {
        CacheManager.makeStandardCacheManager(configFile, cacheDir);
        final CacheManager cm = new CacheManager("directories");
        return new ControllerCaching(cm);
    }
    
    public static MController makeTestController(final String cacheDir) {
        CacheManager.makeTestCacheManager(cacheDir);
        final CacheManager cm = new CacheManager("directories");
        return new ControllerCaching(cm);
    }
    
    private ControllerCaching(final CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
    
    public Iterator<MFile> getInventory(final MCollection mc) {
        return this.getInventory(mc, true);
    }
    
    public Iterator<MFile> getInventoryNoSubdirs(final MCollection mc) {
        return this.getInventoryNoSubdirs(mc, true);
    }
    
    public Iterator<MFile> getInventory(final MCollection mc, final boolean recheck) {
        String path = mc.getDirectoryName();
        if (path.startsWith("file:")) {
            path = path.substring(5);
        }
        final CacheDirectory cd = this.cacheManager.get(path, recheck);
        if (cd == null) {
            return null;
        }
        if (!cd.isDirectory()) {
            return null;
        }
        return new FilteredIterator(mc, new MFileIteratorWithSubdirs(cd, recheck));
    }
    
    public Iterator<MFile> getInventoryNoSubdirs(final MCollection mc, final boolean recheck) {
        String path = mc.getDirectoryName();
        if (path.startsWith("file:")) {
            path = path.substring(5);
        }
        final CacheDirectory cd = this.cacheManager.get(path, recheck);
        if (cd == null) {
            return null;
        }
        if (!cd.isDirectory()) {
            return null;
        }
        return new FilteredIterator(mc, new MFileIterator(cd));
    }
    
    public void close() {
        if (this.cacheManager != null) {
            this.cacheManager.close();
        }
        this.cacheManager = null;
    }
    
    private class FilteredIterator implements Iterator<MFile>
    {
        private final Iterator<MFile> orgIter;
        private final MCollection mc;
        private MFile next;
        
        FilteredIterator(final MCollection mc, final Iterator<MFile> iter) {
            this.orgIter = iter;
            this.mc = mc;
        }
        
        public boolean hasNext() {
            this.next = this.nextFilteredFile();
            return this.next != null;
        }
        
        public MFile next() {
            return this.next;
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
        
        private MFile nextFilteredFile() {
            if (this.orgIter == null) {
                return null;
            }
            if (!this.orgIter.hasNext()) {
                return null;
            }
            MFile pdata;
            for (pdata = this.orgIter.next(); pdata.isDirectory() || !this.mc.accept(pdata); pdata = this.orgIter.next()) {
                if (!this.orgIter.hasNext()) {
                    return null;
                }
            }
            return pdata;
        }
    }
    
    private class MFileIterator implements Iterator<MFile>
    {
        final String path;
        final List<CacheFile> files;
        int count;
        
        MFileIterator(final CacheDirectory cd) {
            this.count = 0;
            this.path = cd.getPath();
            this.files = Arrays.asList(cd.getChildren());
        }
        
        MFileIterator(final String path, final List<CacheFile> files) {
            this.count = 0;
            this.path = path;
            this.files = files;
        }
        
        public boolean hasNext() {
            return this.count < this.files.size();
        }
        
        public MFile next() {
            final CacheFile cfile = this.files.get(this.count++);
            return new MFileCached(this.path, cfile);
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    private class MFileIteratorWithSubdirs implements Iterator<MFile>
    {
        final boolean recheck;
        final Queue<Traversal> traverse;
        Traversal currTraversal;
        Iterator<MFile> currIter;
        
        MFileIteratorWithSubdirs(final CacheDirectory top, final boolean recheck) {
            this.traverse = new LinkedList<Traversal>();
            this.currTraversal = new Traversal(top);
            this.recheck = recheck;
        }
        
        public boolean hasNext() {
            if (this.currIter == null) {
                this.currIter = this.getNextIterator();
                if (this.currIter == null) {
                    return false;
                }
            }
            if (!this.currIter.hasNext()) {
                this.currIter = this.getNextIterator();
                return this.hasNext();
            }
            return true;
        }
        
        public MFile next() {
            return this.currIter.next();
        }
        
        private Iterator<MFile> getNextIterator() {
            if (!this.currTraversal.leavesAreDone) {
                this.currTraversal.leavesAreDone = true;
                return new MFileIterator(this.currTraversal.dir.getPath(), this.currTraversal.fileList);
            }
            if (this.currTraversal.subdirIterator != null && this.currTraversal.subdirIterator.hasNext()) {
                final CacheFile nextDir = this.currTraversal.subdirIterator.next();
                final CacheDirectory cd = ControllerCaching.this.cacheManager.get(this.currTraversal.dir.getPath() + "/" + nextDir.getShortName(), this.recheck);
                if (cd == null) {
                    return this.getNextIterator();
                }
                this.traverse.add(this.currTraversal);
                this.currTraversal = new Traversal(cd);
                return this.getNextIterator();
            }
            else {
                if (this.traverse.peek() == null) {
                    return null;
                }
                this.currTraversal = this.traverse.remove();
                return this.getNextIterator();
            }
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    private class Traversal
    {
        final CacheDirectory dir;
        final List<CacheFile> fileList;
        Iterator<CacheFile> subdirIterator;
        boolean leavesAreDone;
        
        Traversal(final CacheDirectory dir) {
            this.leavesAreDone = false;
            this.dir = dir;
            this.fileList = new ArrayList<CacheFile>();
            final List<CacheFile> subdirList = new ArrayList<CacheFile>();
            for (final CacheFile f : dir.getChildren()) {
                if (f.isDirectory()) {
                    subdirList.add(f);
                }
                else {
                    this.fileList.add(f);
                }
            }
            if (subdirList.size() > 0) {
                this.subdirIterator = subdirList.iterator();
            }
        }
    }
}
