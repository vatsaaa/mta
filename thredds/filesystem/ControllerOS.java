// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Arrays;
import java.util.List;
import java.io.File;
import thredds.inventory.MFile;
import java.util.Iterator;
import thredds.inventory.MCollection;
import net.jcip.annotations.ThreadSafe;
import thredds.inventory.MController;

@ThreadSafe
public class ControllerOS implements MController
{
    public Iterator<MFile> getInventory(final MCollection mc) {
        return this.getInventory(mc, true);
    }
    
    public Iterator<MFile> getInventoryNoSubdirs(final MCollection mc) {
        return this.getInventoryNoSubdirs(mc, true);
    }
    
    public Iterator<MFile> getInventory(final MCollection mc, final boolean recheck) {
        String path = mc.getDirectoryName();
        if (path.startsWith("file:")) {
            path = path.substring(5);
        }
        final File cd = new File(path);
        if (!cd.exists()) {
            return null;
        }
        if (!cd.isDirectory()) {
            return null;
        }
        return new FilteredIterator(mc, new MFileIteratorWithSubdirs(cd));
    }
    
    public Iterator<MFile> getInventoryNoSubdirs(final MCollection mc, final boolean recheck) {
        String path = mc.getDirectoryName();
        if (path.startsWith("file:")) {
            path = path.substring(5);
        }
        final File cd = new File(path);
        if (!cd.exists()) {
            return null;
        }
        if (!cd.isDirectory()) {
            return null;
        }
        return new FilteredIterator(mc, new MFileIterator(cd));
    }
    
    public void close() {
    }
    
    private class FilteredIterator implements Iterator<MFile>
    {
        private Iterator<MFile> orgIter;
        private MCollection mc;
        private MFile next;
        
        FilteredIterator(final MCollection mc, final Iterator<MFile> iter) {
            this.orgIter = iter;
            this.mc = mc;
        }
        
        public boolean hasNext() {
            this.next = this.nextFilteredFile();
            return this.next != null;
        }
        
        public MFile next() {
            return this.next;
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
        
        private MFile nextFilteredFile() {
            if (this.orgIter == null) {
                return null;
            }
            if (!this.orgIter.hasNext()) {
                return null;
            }
            MFile pdata;
            for (pdata = this.orgIter.next(); pdata.isDirectory() || !this.mc.accept(pdata); pdata = this.orgIter.next()) {
                if (!this.orgIter.hasNext()) {
                    return null;
                }
            }
            return pdata;
        }
    }
    
    private class MFileIterator implements Iterator<MFile>
    {
        List<File> files;
        int count;
        
        MFileIterator(final File dir) {
            this.count = 0;
            this.files = Arrays.asList(dir.listFiles());
        }
        
        MFileIterator(final List<File> files) {
            this.count = 0;
            this.files = files;
        }
        
        public boolean hasNext() {
            return this.count < this.files.size();
        }
        
        public MFile next() {
            final File cfile = this.files.get(this.count++);
            return new MFileOS(cfile);
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    private class MFileIteratorWithSubdirs implements Iterator<MFile>
    {
        Queue<Traversal> traverse;
        Traversal currTraversal;
        Iterator<MFile> currIter;
        
        MFileIteratorWithSubdirs(final File top) {
            this.traverse = new LinkedList<Traversal>();
            this.currTraversal = new Traversal(top);
        }
        
        public boolean hasNext() {
            if (this.currIter == null) {
                this.currIter = this.getNextIterator();
                if (this.currIter == null) {
                    return false;
                }
            }
            if (!this.currIter.hasNext()) {
                this.currIter = this.getNextIterator();
                return this.hasNext();
            }
            return true;
        }
        
        public MFile next() {
            return this.currIter.next();
        }
        
        private Iterator<MFile> getNextIterator() {
            if (!this.currTraversal.leavesAreDone) {
                this.currTraversal.leavesAreDone = true;
                return new MFileIterator(this.currTraversal.fileList);
            }
            if (this.currTraversal.subdirIterator != null && this.currTraversal.subdirIterator.hasNext()) {
                final File nextDir = this.currTraversal.subdirIterator.next();
                this.traverse.add(this.currTraversal);
                this.currTraversal = new Traversal(nextDir);
                return this.getNextIterator();
            }
            if (this.traverse.peek() == null) {
                return null;
            }
            this.currTraversal = this.traverse.remove();
            return this.getNextIterator();
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    private class Traversal
    {
        File dir;
        List<File> fileList;
        Iterator<File> subdirIterator;
        boolean leavesAreDone;
        
        Traversal(final File dir) {
            this.leavesAreDone = false;
            this.dir = dir;
            this.fileList = new ArrayList<File>();
            final List<File> subdirList = new ArrayList<File>();
            for (final File f : dir.listFiles()) {
                if (f.isDirectory()) {
                    subdirList.add(f);
                }
                else {
                    this.fileList.add(f);
                }
            }
            if (subdirList.size() > 0) {
                this.subdirIterator = subdirList.iterator();
            }
        }
    }
}
