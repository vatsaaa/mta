// 
// Decompiled by Procyon v0.5.36
// 

package thredds.filesystem;

import org.slf4j.LoggerFactory;
import net.sf.ehcache.Statistics;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.io.File;
import java.util.Formatter;
import net.sf.ehcache.Element;
import java.io.Serializable;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import ucar.unidata.util.StringUtil;
import ucar.nc2.util.IO;
import java.util.concurrent.atomic.AtomicLong;
import net.sf.ehcache.Cache;
import org.slf4j.Logger;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class CacheManager
{
    private static Logger cacheLog;
    private static net.sf.ehcache.CacheManager cacheManager;
    private static boolean debugConfig;
    private Cache cache;
    private AtomicLong addElements;
    private AtomicLong hits;
    private AtomicLong requests;
    private static String configReadOnly;
    private static String config;
    
    public static net.sf.ehcache.CacheManager getEhcache() {
        return CacheManager.cacheManager;
    }
    
    public static void makeStandardCacheManager(final String configFile, final String cacheDir) throws IOException {
        final String config = IO.readFile(configFile);
        final String configString = StringUtil.substitute(config, "${cacheDir}", cacheDir);
        CacheManager.cacheLog.info("thredds.filesystem.CacheManager configuraton " + configString);
        CacheManager.cacheManager = new net.sf.ehcache.CacheManager((InputStream)new StringBufferInputStream(configString));
    }
    
    public static void makeTestCacheManager(final String cacheDir) {
        final String configString = StringUtil.substitute(CacheManager.config, "${cacheDir}", cacheDir);
        if (CacheManager.debugConfig) {
            System.out.printf("CacheManager test=%n %s %n", configString);
        }
        CacheManager.cacheManager = new net.sf.ehcache.CacheManager((InputStream)new StringBufferInputStream(configString));
    }
    
    public static void makeReadOnlyCacheManager(final String cacheDir, final String cacheName) {
        String configString = StringUtil.substitute(CacheManager.configReadOnly, "${cacheDir}", cacheDir);
        configString = StringUtil.substitute(configString, "${cacheName}", cacheName);
        if (CacheManager.debugConfig) {
            System.out.printf("CacheManager readonly =%n %s %n", configString);
        }
        CacheManager.cacheManager = new net.sf.ehcache.CacheManager((InputStream)new StringBufferInputStream(configString));
    }
    
    public static void shutdown() {
        if (CacheManager.cacheManager != null) {
            CacheManager.cacheLog.info("thredds.filesystem.CacheManager shutdown");
            CacheManager.cacheManager.shutdown();
        }
        CacheManager.cacheManager = null;
    }
    
    public CacheManager(final String cacheName) {
        this.addElements = new AtomicLong();
        this.hits = new AtomicLong();
        this.requests = new AtomicLong();
        this.cache = CacheManager.cacheManager.getCache(cacheName);
        CacheManager.cacheLog.info("thredds.filesystem.CacheManager " + this.cache);
        CacheManager.cacheLog.info("thredds.filesystem.CacheManager " + this.cache.getStatistics().toString());
    }
    
    public void add(final Serializable path, final Serializable value) {
        if (this.cache == null) {
            return;
        }
        this.cache.put(new Element(path, value));
        this.addElements.incrementAndGet();
    }
    
    public static String show(final String cacheName) {
        if (CacheManager.cacheManager == null) {
            return "no cacheManager set";
        }
        final Cache cache = CacheManager.cacheManager.getCache(cacheName);
        if (cache == null) {
            return "no cache named " + cacheName;
        }
        final Formatter f = new Formatter();
        f.format("Cache %s%n %s%n", cache, cache.getStatistics().toString());
        return f.toString();
    }
    
    public CacheDirectory get(final String path, final boolean recheck) {
        this.requests.incrementAndGet();
        final Element e = this.cache.get((Serializable)path);
        if (e != null) {
            if (CacheManager.cacheLog.isDebugEnabled()) {
                CacheManager.cacheLog.debug("thredds.filesystem.CacheManager found in cache; path =" + path);
            }
            final CacheDirectory m = (CacheDirectory)e.getValue();
            if (m != null) {
                if (!recheck) {
                    return m;
                }
                final File f = new File(m.getPath());
                if (!f.exists()) {
                    this.cache.put(new Element((Serializable)path, (Serializable)null));
                    return null;
                }
                final boolean modified = f.lastModified() > m.lastModified;
                if (CacheManager.cacheLog.isDebugEnabled()) {
                    CacheManager.cacheLog.debug("thredds.filesystem.CacheManager modified diff = " + (f.lastModified() - m.lastModified) + "; path=" + path);
                }
                if (!modified) {
                    this.hits.incrementAndGet();
                    return m;
                }
                this.cache.put(new Element((Serializable)path, (Serializable)null));
            }
        }
        final File p = new File(path);
        if (!p.exists()) {
            return null;
        }
        if (CacheManager.cacheLog.isDebugEnabled()) {
            CacheManager.cacheLog.debug("thredds.filesystem.CacheManager read from filesystem; path=" + path);
        }
        final CacheDirectory i = new CacheDirectory(p);
        this.add(path, i);
        return i;
    }
    
    public void close() {
        if (CacheManager.cacheManager != null) {
            CacheManager.cacheLog.info("thredds.filesystem.CacheManager shutdown");
            CacheManager.cacheManager.shutdown();
        }
        CacheManager.cacheManager = null;
    }
    
    public void showKeys() {
        final List keys = this.cache.getKeys();
        Collections.sort((List<Comparable>)keys);
        for (final Object key : keys) {
            final Element elem = this.cache.get(key);
            System.out.printf(" %40s == %s%n", key, elem);
        }
    }
    
    public void stats() {
        System.out.printf(" elems added= %s%n", this.addElements.get());
        System.out.printf(" reqs= %d%n", this.requests.get());
        System.out.printf(" hits= %d%n", this.hits.get());
        if (this.cache != null) {
            System.out.printf(" cache= %s%n", this.cache.toString());
            System.out.printf(" cache.size= %d%n", this.cache.getSize());
            System.out.printf(" cache.memorySize= %d%n", this.cache.getMemoryStoreSize());
            final Statistics stats = this.cache.getStatistics();
            System.out.printf(" stats= %s%n", stats.toString());
        }
    }
    
    public void populateFiles(final String root) {
        final long startCount = this.addElements.get();
        final long start = System.nanoTime();
        this.addRecursiveFiles(new File(root));
        final long end = System.nanoTime();
        final long total = this.addElements.get() - startCount;
        System.out.printf("populate %n%-20s total %d took %d msecs %n", root, total, (end - start) / 1000L / 1000L);
    }
    
    private void addRecursiveFiles(final File dir) {
        for (final File f : dir.listFiles()) {
            if (f.isDirectory()) {
                this.addRecursiveFiles(f);
            }
            else {
                this.add(f.getPath(), new CacheFile(f));
            }
        }
    }
    
    public void populateFilesProto(final String root) {
        final long startCount = this.addElements.get();
        final long start = System.nanoTime();
        this.addRecursiveFilesProto(new File(root));
        final long end = System.nanoTime();
        final long total = this.addElements.get() - startCount;
        System.out.printf("populate %n%-20s total %d took %d msecs %n", root, total, (end - start) / 1000L / 1000L);
    }
    
    private void addRecursiveFilesProto(final File dir) {
        for (final File f : dir.listFiles()) {
            if (f.isDirectory()) {
                this.addRecursiveFilesProto(f);
            }
            else {
                this.add(f.getPath(), new CacheFileProto(f));
            }
        }
    }
    
    public void populateDirs(final String root) {
        final long startCount = this.addElements.get();
        final long start = System.nanoTime();
        this.addRecursiveDirs(new File(root));
        final long end = System.nanoTime();
        final long total = this.addElements.get() - startCount;
        System.out.printf("populate %n%-20s total %d took %d msecs %n", root, total, (end - start) / 1000L / 1000L);
    }
    
    private void addRecursiveDirs(final File dir) {
        this.add(dir.getPath(), new CacheDirectory(dir));
        for (final File f : dir.listFiles()) {
            if (f.isDirectory()) {
                this.addRecursiveDirs(f);
            }
        }
    }
    
    public void populateDirsProto(final String root) {
        final long startCount = this.addElements.get();
        final long start = System.nanoTime();
        this.addRecursiveDirsProto(new File(root));
        final long end = System.nanoTime();
        final long total = this.addElements.get() - startCount;
        System.out.printf("populate %n%-20s total %d took %d msecs %n", root, total, (end - start) / 1000L / 1000L);
    }
    
    private void addRecursiveDirsProto(final File dir) {
        this.add(dir.getPath(), new CacheDirectoryProto(dir));
        for (final File f : dir.listFiles()) {
            if (f.isDirectory()) {
                this.addRecursiveDirsProto(f);
            }
        }
    }
    
    public static void main(final String[] args) throws IOException {
        makeTestCacheManager("C:/data/ehcache/");
        System.out.printf("=====================%n", new Object[0]);
        final CacheManager dirProto = new CacheManager("dirsProto");
        dirProto.populateDirsProto("C:/data/");
        dirProto.stats();
        shutdown();
        final Formatter f = new Formatter(System.out);
        f.format(" Proto count = %d size = %d %n", CacheFileProto.countWrite, CacheFileProto.countWriteSize);
        final int avg = (CacheFileProto.countWrite == 0) ? 0 : (CacheFileProto.countWriteSize / CacheFileProto.countWrite);
        f.format("       avg = %d %n", avg);
        f.flush();
    }
    
    static {
        CacheManager.cacheLog = LoggerFactory.getLogger(CacheManager.class);
        CacheManager.debugConfig = false;
        CacheManager.configReadOnly = "<ehcache>\n    <diskStore path='${cacheDir}' />\n    <defaultCache\n              maxElementsInMemory='10000'\n              eternal='false'\n              timeToIdleSeconds='120'\n              timeToLiveSeconds='120'\n              overflowToDisk='true'\n              maxElementsOnDisk='10000000'\n              diskPersistent='false'\n              diskExpiryThreadIntervalSeconds='120'\n              memoryStoreEvictionPolicy='LRU'\n              />\n    <cache name='${cacheName}'\n            maxElementsInMemory='10000'\n            eternal='false'\n            timeToIdleSeconds='864000'\n            timeToLiveSeconds='0'\n            overflowToDisk='true'\n            maxElementsOnDisk='100000'\n            diskPersistent='true'\n            diskExpiryThreadIntervalSeconds='3600'\n            memoryStoreEvictionPolicy='LRU'\n            />\n</ehcache>";
        CacheManager.config = "<ehcache>\n    <diskStore path='${cacheDir}' />\n    <defaultCache\n              maxElementsInMemory='10000'\n              eternal='false'\n              timeToIdleSeconds='120'\n              timeToLiveSeconds='120'\n              overflowToDisk='true'\n              maxElementsOnDisk='10000000'\n              diskPersistent='false'\n              diskExpiryThreadIntervalSeconds='120'\n              memoryStoreEvictionPolicy='LRU'\n              />\n    <cache name='directories'\n            maxElementsInMemory='1000'\n            eternal='true'\n            timeToIdleSeconds='864000'\n            timeToLiveSeconds='0'\n            overflowToDisk='true'\n            maxElementsOnDisk='0'\n            diskPersistent='true'\n            diskExpiryThreadIntervalSeconds='3600'\n            memoryStoreEvictionPolicy='LRU'\n            />\n    <cache name='files'\n            maxElementsInMemory='1000'\n            eternal='true'\n            timeToIdleSeconds='864000'\n            timeToLiveSeconds='0'\n            overflowToDisk='true'\n            maxElementsOnDisk='0'\n            diskPersistent='true'\n            diskExpiryThreadIntervalSeconds='3600'\n            memoryStoreEvictionPolicy='LRU'\n            />\n    <cache name='filesProto'\n            maxElementsInMemory='1000'\n            eternal='true'\n            timeToIdleSeconds='864000'\n            timeToLiveSeconds='0'\n            overflowToDisk='true'\n            maxElementsOnDisk='0'\n            diskPersistent='true'\n            diskExpiryThreadIntervalSeconds='3600'\n            memoryStoreEvictionPolicy='LRU'\n            />\n    <cache name='dirsProto'\n            maxElementsInMemory='1000'\n            eternal='true'\n            timeToIdleSeconds='864000'\n            timeToLiveSeconds='0'\n            overflowToDisk='true'\n            maxElementsOnDisk='0'\n            diskPersistent='true'\n            diskExpiryThreadIntervalSeconds='3600'\n            memoryStoreEvictionPolicy='LRU'\n            />\n</ehcache>";
    }
}
