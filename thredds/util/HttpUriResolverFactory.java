// 
// Decompiled by Procyon v0.5.36
// 

package thredds.util;

import java.net.URI;

public class HttpUriResolverFactory
{
    private static long defaultConnectionTimeout;
    private static int defaultSocketTimeout;
    private static boolean defaultAllowContentEncoding;
    private static boolean defaultFollowRedirects;
    private long connectionTimeout;
    private int socketTimeout;
    private boolean allowContentEncoding;
    private boolean followRedirects;
    
    public static HttpUriResolver getDefaultHttpUriResolver(final URI uri) {
        return new HttpUriResolver(uri, HttpUriResolverFactory.defaultConnectionTimeout, HttpUriResolverFactory.defaultSocketTimeout, HttpUriResolverFactory.defaultAllowContentEncoding, HttpUriResolverFactory.defaultFollowRedirects);
    }
    
    public HttpUriResolverFactory() {
        this.connectionTimeout = HttpUriResolverFactory.defaultConnectionTimeout;
        this.socketTimeout = HttpUriResolverFactory.defaultSocketTimeout;
        this.allowContentEncoding = HttpUriResolverFactory.defaultAllowContentEncoding;
        this.followRedirects = HttpUriResolverFactory.defaultFollowRedirects;
    }
    
    public long getConnectionTimeout() {
        return this.connectionTimeout;
    }
    
    public void setConnectionTimeout(final long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }
    
    public int getSocketTimeout() {
        return this.socketTimeout;
    }
    
    public void setSocketTimeout(final int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }
    
    public boolean getAllowContentEncoding() {
        return this.allowContentEncoding;
    }
    
    public void setAllowContentEncoding(final boolean allowContentEncoding) {
        this.allowContentEncoding = allowContentEncoding;
    }
    
    public boolean getFollowRedirects() {
        return this.followRedirects;
    }
    
    public void setFollowRedirects(final boolean followRedirects) {
        this.followRedirects = followRedirects;
    }
    
    public HttpUriResolver newHttpUriResolver(final URI uri) {
        return new HttpUriResolver(uri, this.connectionTimeout, this.socketTimeout, this.allowContentEncoding, this.followRedirects);
    }
    
    static {
        HttpUriResolverFactory.defaultConnectionTimeout = 30000L;
        HttpUriResolverFactory.defaultSocketTimeout = 30000;
        HttpUriResolverFactory.defaultAllowContentEncoding = true;
        HttpUriResolverFactory.defaultFollowRedirects = true;
    }
}
