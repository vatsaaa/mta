// 
// Decompiled by Procyon v0.5.36
// 

package thredds.util;

import java.util.Enumeration;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.text.html.HTMLEditorKit;

public class DodsURLExtractor
{
    private HTMLEditorKit.Parser parser;
    private ArrayList urlList;
    private URL baseURL;
    private boolean wantURLS;
    private String title;
    private boolean isTitle;
    private StringBuffer textBuffer;
    private boolean wantText;
    private boolean debug;
    
    public DodsURLExtractor() {
        this.wantURLS = false;
        this.wantText = false;
        this.debug = false;
        final ParserGetter kit = new ParserGetter();
        this.parser = kit.getParser();
    }
    
    public ArrayList extract(final String url) throws IOException {
        if (this.debug) {
            System.out.println(" URLextract=" + url);
        }
        this.baseURL = new URL(url);
        final InputStream in = this.baseURL.openStream();
        final InputStreamReader r = new InputStreamReader(this.filterTag(in));
        final HTMLEditorKit.ParserCallback callback = new CallerBacker();
        this.urlList = new ArrayList();
        this.wantURLS = true;
        this.wantText = false;
        this.parser.parse(r, callback, false);
        return this.urlList;
    }
    
    public String getTextContent(final String url) throws IOException {
        if (this.debug) {
            System.out.println(" URL.getTextContent=" + url);
        }
        this.baseURL = new URL(url);
        final InputStream in = this.baseURL.openStream();
        final InputStreamReader r = new InputStreamReader(this.filterTag(in));
        final HTMLEditorKit.ParserCallback callback = new CallerBacker();
        this.textBuffer = new StringBuffer(3000);
        this.wantURLS = false;
        this.wantText = true;
        this.parser.parse(r, callback, false);
        return this.textBuffer.toString();
    }
    
    private InputStream filterTag(final InputStream in) throws IOException {
        final BufferedReader buffIn = new BufferedReader(new InputStreamReader(in));
        final ByteArrayOutputStream bos = new ByteArrayOutputStream(10000);
        String line = buffIn.readLine();
        while (line != null) {
            final String lline = line.toLowerCase();
            if (0 <= lline.indexOf("<meta ")) {
                continue;
            }
            bos.write(line.getBytes());
            line = buffIn.readLine();
        }
        buffIn.close();
        return new ByteArrayInputStream(bos.toByteArray());
    }
    
    private class CallerBacker extends HTMLEditorKit.ParserCallback
    {
        private boolean wantTag(final HTML.Tag tag) {
            return tag == HTML.Tag.H1 || tag == HTML.Tag.H2 || tag == HTML.Tag.H3 || tag == HTML.Tag.H4 || tag == HTML.Tag.H5 || tag == HTML.Tag.H6;
        }
        
        @Override
        public void handleStartTag(final HTML.Tag tag, final MutableAttributeSet attributes, final int position) {
            DodsURLExtractor.this.isTitle = (tag == HTML.Tag.TITLE);
            if (DodsURLExtractor.this.wantURLS && tag == HTML.Tag.A) {
                this.extractHREF(attributes);
            }
        }
        
        @Override
        public void handleEndTag(final HTML.Tag tag, final int position) {
            DodsURLExtractor.this.isTitle = false;
        }
        
        @Override
        public void handleSimpleTag(final HTML.Tag tag, final MutableAttributeSet attributes, final int position) {
            DodsURLExtractor.this.isTitle = false;
            if (DodsURLExtractor.this.wantURLS && tag == HTML.Tag.A) {
                this.extractHREF(attributes);
            }
        }
        
        private void extractHREF(final AttributeSet attributes) {
            final Enumeration e = attributes.getAttributeNames();
            while (e.hasMoreElements()) {
                final Object name = e.nextElement();
                final String value = (String)attributes.getAttribute(name);
                try {
                    if (name != HTML.Attribute.HREF) {
                        continue;
                    }
                    final URL u = DodsURLExtractor.this.baseURL.toURI().resolve(value).toURL();
                    final String urlName = u.toString();
                    if (DodsURLExtractor.this.urlList != null) {
                        DodsURLExtractor.this.urlList.add(u.toString());
                    }
                    if (!DodsURLExtractor.this.debug) {
                        continue;
                    }
                    System.out.println(" extracted URL= <" + urlName + ">");
                }
                catch (MalformedURLException ex) {
                    System.err.println(ex);
                    System.err.println(DodsURLExtractor.this.baseURL);
                    System.err.println(value);
                    ex.printStackTrace();
                }
                catch (URISyntaxException ex2) {
                    System.err.println(ex2);
                    System.err.println(DodsURLExtractor.this.baseURL);
                    System.err.println(value);
                    ex2.printStackTrace();
                }
            }
        }
        
        @Override
        public void handleText(final char[] text, final int position) {
            if (DodsURLExtractor.this.isTitle) {
                DodsURLExtractor.this.title = new String(text);
            }
            if (DodsURLExtractor.this.wantText) {
                DodsURLExtractor.this.textBuffer.append(text);
                DodsURLExtractor.this.textBuffer.append(' ');
            }
        }
    }
    
    private class ParserGetter extends HTMLEditorKit
    {
        public Parser getParser() {
            return super.getParser();
        }
    }
}
