// 
// Decompiled by Procyon v0.5.36
// 

package thredds.util;

public interface PathAliasReplacement
{
    boolean containsPathAlias(final String p0);
    
    String replacePathAlias(final String p0);
}
