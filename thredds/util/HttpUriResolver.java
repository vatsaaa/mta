// 
// Decompiled by Procyon v0.5.36
// 

package thredds.util;

import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.HttpClient;
import java.util.zip.InflaterInputStream;
import java.util.zip.GZIPInputStream;
import java.io.InputStream;
import org.apache.commons.httpclient.Header;
import java.util.HashMap;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import java.util.Map;
import org.apache.commons.httpclient.HttpMethod;
import java.net.URI;
import org.slf4j.Logger;

public class HttpUriResolver
{
    private Logger logger;
    private URI uri;
    private long connectionTimeout;
    private int socketTimeout;
    private String contentEncoding;
    private boolean allowContentEncoding;
    private boolean followRedirects;
    private HttpMethod method;
    private Map<String, String> respHeaders;
    
    HttpUriResolver(final URI uri, final long connectionTimeout, final int socketTimeout, final boolean allowContentEncoding, final boolean followRedirects) {
        this.logger = LoggerFactory.getLogger(HttpUriResolver.class);
        this.contentEncoding = "gzip,deflate";
        this.method = null;
        if (!uri.getScheme().equalsIgnoreCase("http")) {
            throw new IllegalArgumentException("Given a Non-HTTP URI [" + uri.toString() + "].");
        }
        this.uri = uri;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
        this.allowContentEncoding = allowContentEncoding;
        this.followRedirects = followRedirects;
    }
    
    public URI getUri() {
        return this.uri;
    }
    
    public long getConnectionTimeout() {
        return this.connectionTimeout;
    }
    
    public int getSocketTimeout() {
        return this.socketTimeout;
    }
    
    public String getContentEncoding() {
        return this.contentEncoding;
    }
    
    public boolean getAllowContentEncoding() {
        return this.allowContentEncoding;
    }
    
    public boolean getFollowRedirects() {
        return this.followRedirects;
    }
    
    public void makeRequest() throws IOException {
        if (this.method != null) {
            throw new IllegalStateException("Request already made.");
        }
        this.method = this.getHttpResponse(this.uri);
    }
    
    public int getResponseStatusCode() {
        if (this.method == null) {
            throw new IllegalStateException("Request has not been made.");
        }
        return this.method.getStatusCode();
    }
    
    public String getResponseStatusText() {
        if (this.method == null) {
            throw new IllegalStateException("Request has not been made.");
        }
        return this.method.getStatusText();
    }
    
    public Map<String, String> getResponseHeaders() {
        if (this.method == null) {
            throw new IllegalStateException("Request has not been made.");
        }
        if (this.respHeaders == null) {
            this.respHeaders = new HashMap<String, String>();
            final Header[] arr$;
            final Header[] headers = arr$ = this.method.getResponseHeaders();
            for (final Header h : arr$) {
                this.respHeaders.put(h.getName(), h.getValue());
            }
        }
        return this.respHeaders;
    }
    
    public String getResponseHeaderValue(final String name) {
        if (this.method == null) {
            throw new IllegalStateException("Request has not been made.");
        }
        final Header responseHeader = this.method.getResponseHeader(name);
        return (responseHeader == null) ? null : responseHeader.getValue();
    }
    
    public InputStream getResponseBodyAsInputStream() throws IOException {
        if (this.method == null) {
            throw new IllegalStateException("Request has not been made.");
        }
        final InputStream is = this.method.getResponseBodyAsStream();
        final Header contentEncodingHeader = this.method.getResponseHeader("Content-Encoding");
        if (contentEncodingHeader != null) {
            final String contentEncoding = contentEncodingHeader.getValue();
            if (contentEncoding != null) {
                if (contentEncoding.equalsIgnoreCase("gzip")) {
                    return new GZIPInputStream(is);
                }
                if (contentEncoding.equalsIgnoreCase("deflate")) {
                    return new InflaterInputStream(is);
                }
            }
        }
        return is;
    }
    
    private HttpMethod getHttpResponse(final URI uri) throws IOException {
        final HttpClient client = new HttpClient();
        final HttpClientParams params = client.getParams();
        params.setConnectionManagerTimeout(this.connectionTimeout);
        params.setSoTimeout(this.socketTimeout);
        final HttpMethod method = (HttpMethod)new GetMethod(uri.toString());
        method.setFollowRedirects(this.followRedirects);
        method.addRequestHeader("Accept-Encoding", this.contentEncoding);
        client.executeMethod(method);
        final int statusCode = method.getStatusCode();
        if (statusCode == 200 || statusCode == 201) {
            return method;
        }
        return null;
    }
}
