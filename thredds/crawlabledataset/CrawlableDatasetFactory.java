// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.io.IOException;

public class CrawlableDatasetFactory
{
    private static String defaultClassName;
    
    public static CrawlableDataset createCrawlableDataset(final String path, final String className, final Object configObj) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, IllegalArgumentException, NullPointerException {
        if (path == null) {
            throw new NullPointerException("Given path must not be null.");
        }
        final String tmpClassName = (className == null) ? CrawlableDatasetFactory.defaultClassName : className;
        final Class crDsClass = Class.forName(tmpClassName);
        if (!CrawlableDataset.class.isAssignableFrom(crDsClass)) {
            throw new IllegalArgumentException("Requested class <" + className + "> not an implementation of thredds.crawlabledataset.CrawlableDataset.");
        }
        final Class[] argTypes = { String.class, Object.class };
        final Object[] args = { path, configObj };
        final Constructor constructor = crDsClass.getDeclaredConstructor((Class[])argTypes);
        try {
            return constructor.newInstance(args);
        }
        catch (InvocationTargetException e) {
            if (IOException.class.isAssignableFrom(e.getCause().getClass())) {
                throw (IOException)e.getCause();
            }
            throw e;
        }
    }
    
    public static String normalizePath(final String path) {
        String newPath;
        for (newPath = path.replaceAll("\\\\", "/"); newPath.endsWith("/") && !newPath.equals("/"); newPath = newPath.substring(0, newPath.length() - 1)) {}
        return newPath;
    }
    
    static {
        CrawlableDatasetFactory.defaultClassName = "thredds.crawlabledataset.CrawlableDatasetFile";
    }
}
