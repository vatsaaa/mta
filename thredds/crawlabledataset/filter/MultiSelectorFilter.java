// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.filter;

import java.util.Iterator;
import thredds.crawlabledataset.CrawlableDataset;
import java.util.Collections;
import java.util.List;
import thredds.crawlabledataset.CrawlableDatasetFilter;

public class MultiSelectorFilter implements CrawlableDatasetFilter
{
    private List<Selector> selectorGroup;
    
    public MultiSelectorFilter(final List<Selector> selectorGroup) {
        if (selectorGroup == null) {
            throw new IllegalArgumentException("Selector group parameter must not be null.");
        }
        this.selectorGroup = selectorGroup;
    }
    
    public MultiSelectorFilter(final Selector selector) {
        if (selector == null) {
            this.selectorGroup = Collections.emptyList();
        }
        else {
            this.selectorGroup = Collections.singletonList(selector);
        }
    }
    
    public Object getConfigObject() {
        return this.selectorGroup;
    }
    
    public boolean accept(final CrawlableDataset dataset) {
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset parameter must not be null.");
        }
        if (this.selectorGroup.isEmpty()) {
            return true;
        }
        boolean accept = false;
        boolean doAnyIncludersApply = false;
        boolean doAnySelectorsApply = false;
        for (final Selector curSelector : this.selectorGroup) {
            if (curSelector.isApplicable(dataset)) {
                doAnySelectorsApply = true;
                if (curSelector.isIncluder()) {
                    doAnyIncludersApply = true;
                    if (!curSelector.match(dataset)) {
                        continue;
                    }
                    accept = true;
                }
                else {
                    if (curSelector.match(dataset)) {
                        return false;
                    }
                    continue;
                }
            }
        }
        return accept || !doAnySelectorsApply || !doAnyIncludersApply;
    }
    
    public static class Selector
    {
        private boolean includer;
        private boolean applyToAtomicDataset;
        private boolean applyToCollectionDataset;
        private CrawlableDatasetFilter filter;
        
        public Selector(final CrawlableDatasetFilter filter, final boolean includer, final boolean applyToAtomicDataset, final boolean applyToCollectionDataset) {
            this.filter = filter;
            this.includer = includer;
            this.applyToAtomicDataset = applyToAtomicDataset;
            this.applyToCollectionDataset = applyToCollectionDataset;
        }
        
        public CrawlableDatasetFilter getFilter() {
            return this.filter;
        }
        
        public boolean isApplyToAtomicDataset() {
            return this.applyToAtomicDataset;
        }
        
        public boolean isApplyToCollectionDataset() {
            return this.applyToCollectionDataset;
        }
        
        public boolean match(final CrawlableDataset dataset) {
            return this.filter.accept(dataset);
        }
        
        public boolean isApplicable(final CrawlableDataset dataset) {
            return (this.applyToAtomicDataset && !dataset.isCollection()) || (this.applyToCollectionDataset && dataset.isCollection());
        }
        
        public boolean isIncluder() {
            return this.includer;
        }
        
        public boolean isExcluder() {
            return !this.includer;
        }
    }
}
