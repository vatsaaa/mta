// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.filter;

import thredds.crawlabledataset.CrawlableDataset;
import thredds.crawlabledataset.CrawlableDatasetFilter;

public class LogicalFilterComposer
{
    public static CrawlableDatasetFilter getAndFilter(final CrawlableDatasetFilter filter1, final CrawlableDatasetFilter filter2) {
        return new AndFilter(filter1, filter2);
    }
    
    public static CrawlableDatasetFilter getOrFilter(final CrawlableDatasetFilter filter1, final CrawlableDatasetFilter filter2) {
        return new OrFilter(filter1, filter2);
    }
    
    public static CrawlableDatasetFilter getNotFilter(final CrawlableDatasetFilter filter) {
        return new NotFilter(filter);
    }
    
    private static class AndFilter implements CrawlableDatasetFilter
    {
        private CrawlableDatasetFilter filter1;
        private CrawlableDatasetFilter filter2;
        
        AndFilter(final CrawlableDatasetFilter filter1, final CrawlableDatasetFilter filter2) {
            this.filter1 = filter1;
            this.filter2 = filter2;
        }
        
        public boolean accept(final CrawlableDataset dataset) {
            return this.filter1.accept(dataset) && this.filter2.accept(dataset);
        }
        
        public Object getConfigObject() {
            return null;
        }
    }
    
    private static class OrFilter implements CrawlableDatasetFilter
    {
        private CrawlableDatasetFilter filter1;
        private CrawlableDatasetFilter filter2;
        
        OrFilter(final CrawlableDatasetFilter filter1, final CrawlableDatasetFilter filter2) {
            this.filter1 = filter1;
            this.filter2 = filter2;
        }
        
        public boolean accept(final CrawlableDataset dataset) {
            return this.filter1.accept(dataset) || this.filter2.accept(dataset);
        }
        
        public Object getConfigObject() {
            return null;
        }
    }
    
    private static class NotFilter implements CrawlableDatasetFilter
    {
        private CrawlableDatasetFilter filter;
        
        NotFilter(final CrawlableDatasetFilter filter1) {
            this.filter = filter1;
        }
        
        public boolean accept(final CrawlableDataset dataset) {
            return !this.filter.accept(dataset);
        }
        
        public Object getConfigObject() {
            return null;
        }
    }
}
