// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.filter;

import java.util.regex.Matcher;
import thredds.crawlabledataset.CrawlableDataset;

public class RegExpMatchOnPathFilter extends RegExpMatchOnNameFilter
{
    public RegExpMatchOnPathFilter(final String regExpString) {
        super(regExpString);
    }
    
    @Override
    public boolean accept(final CrawlableDataset dataset) {
        final Matcher matcher = this.pattern.matcher(dataset.getPath());
        return matcher.matches();
    }
}
