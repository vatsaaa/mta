// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.filter;

import java.util.Date;
import thredds.crawlabledataset.CrawlableDataset;
import thredds.crawlabledataset.CrawlableDatasetFilter;

public class LastModifiedLimitFilter implements CrawlableDatasetFilter
{
    private long lastModifiedLimitInMillis;
    
    public LastModifiedLimitFilter(final long lastModifiedLimitInMillis) {
        this.lastModifiedLimitInMillis = lastModifiedLimitInMillis;
    }
    
    public boolean accept(final CrawlableDataset dataset) {
        final Date lastModDate = dataset.lastModified();
        if (lastModDate != null) {
            final long now = System.currentTimeMillis();
            if (now - lastModDate.getTime() > this.lastModifiedLimitInMillis) {
                return true;
            }
        }
        return false;
    }
    
    public Object getConfigObject() {
        return null;
    }
    
    public long getLastModifiedLimitInMillis() {
        return this.lastModifiedLimitInMillis;
    }
}
