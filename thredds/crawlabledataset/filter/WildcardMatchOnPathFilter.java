// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.filter;

import java.util.regex.Matcher;
import thredds.crawlabledataset.CrawlableDataset;

public class WildcardMatchOnPathFilter extends WildcardMatchOnNameFilter
{
    public WildcardMatchOnPathFilter(final String wildcardString) {
        super(wildcardString);
    }
    
    @Override
    public boolean accept(final CrawlableDataset dataset) {
        final Matcher matcher = this.pattern.matcher(dataset.getPath());
        return matcher.matches();
    }
}
