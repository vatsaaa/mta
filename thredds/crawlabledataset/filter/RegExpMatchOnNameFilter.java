// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.filter;

import java.util.regex.Matcher;
import thredds.crawlabledataset.CrawlableDataset;
import java.util.regex.Pattern;
import thredds.crawlabledataset.CrawlableDatasetFilter;

public class RegExpMatchOnNameFilter implements CrawlableDatasetFilter
{
    private String regExpString;
    protected Pattern pattern;
    
    public RegExpMatchOnNameFilter(final String regExpString) {
        this.regExpString = regExpString;
        this.pattern = Pattern.compile(regExpString);
    }
    
    public Object getConfigObject() {
        return this.regExpString;
    }
    
    public String getRegExpString() {
        return this.regExpString;
    }
    
    public boolean accept(final CrawlableDataset dataset) {
        final Matcher matcher = this.pattern.matcher(dataset.getName());
        return matcher.matches();
    }
}
