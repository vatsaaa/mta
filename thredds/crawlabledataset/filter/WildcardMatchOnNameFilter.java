// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.filter;

import java.util.regex.Matcher;
import thredds.crawlabledataset.CrawlableDataset;
import java.util.regex.Pattern;
import thredds.crawlabledataset.CrawlableDatasetFilter;

public class WildcardMatchOnNameFilter implements CrawlableDatasetFilter
{
    protected String wildcardString;
    protected Pattern pattern;
    
    public WildcardMatchOnNameFilter(final String wildcardString) {
        this.wildcardString = wildcardString;
        final String regExp = this.mapWildcardToRegExp(wildcardString);
        this.pattern = Pattern.compile(regExp);
    }
    
    private String mapWildcardToRegExp(String wildcardString) {
        wildcardString = wildcardString.replaceAll("\\.", "\\\\.");
        wildcardString = wildcardString.replaceAll("\\*", ".*");
        wildcardString = wildcardString.replaceAll("\\?", ".?");
        return wildcardString;
    }
    
    public Object getConfigObject() {
        return this.wildcardString;
    }
    
    public String getWildcardString() {
        return this.wildcardString;
    }
    
    public boolean accept(final CrawlableDataset dataset) {
        final Matcher matcher = this.pattern.matcher(dataset.getName());
        return matcher.matches();
    }
}
