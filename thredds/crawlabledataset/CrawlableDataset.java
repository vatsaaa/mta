// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import java.util.Date;
import java.io.IOException;
import java.util.List;

public interface CrawlableDataset
{
    Object getConfigObject();
    
    String getPath();
    
    String getName();
    
    CrawlableDataset getParentDataset();
    
    boolean exists();
    
    boolean isCollection();
    
    CrawlableDataset getDescendant(final String p0);
    
    List<CrawlableDataset> listDatasets() throws IOException;
    
    List<CrawlableDataset> listDatasets(final CrawlableDatasetFilter p0) throws IOException;
    
    long length();
    
    Date lastModified();
}
