// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

public interface CrawlableDatasetLabeler
{
    String getLabel(final CrawlableDataset p0);
    
    Object getConfigObject();
}
