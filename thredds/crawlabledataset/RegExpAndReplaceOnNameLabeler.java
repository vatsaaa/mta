// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpAndReplaceOnNameLabeler implements CrawlableDatasetLabeler
{
    private String regExp;
    private Pattern pattern;
    private String replaceString;
    
    public RegExpAndReplaceOnNameLabeler(final String regExp, final String replaceString) {
        this.regExp = regExp;
        this.pattern = Pattern.compile(regExp);
        this.replaceString = replaceString;
    }
    
    public Object getConfigObject() {
        return null;
    }
    
    public String getRegExp() {
        return this.regExp;
    }
    
    public String getReplaceString() {
        return this.replaceString;
    }
    
    public String getLabel(final CrawlableDataset dataset) {
        final Matcher matcher = this.pattern.matcher(dataset.getName());
        if (!matcher.find()) {
            return null;
        }
        final StringBuffer startTime = new StringBuffer();
        matcher.appendReplacement(startTime, this.replaceString);
        startTime.delete(0, matcher.start());
        if (startTime.length() == 0) {
            return null;
        }
        return startTime.toString();
    }
}
