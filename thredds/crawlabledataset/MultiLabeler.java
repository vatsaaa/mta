// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import java.util.Iterator;
import java.util.List;

public class MultiLabeler implements CrawlableDatasetLabeler
{
    private List<CrawlableDatasetLabeler> labelerList;
    
    public MultiLabeler(final List<CrawlableDatasetLabeler> labelerList) {
        this.labelerList = labelerList;
    }
    
    public Object getConfigObject() {
        return null;
    }
    
    public List<CrawlableDatasetLabeler> getLabelerList() {
        return this.labelerList;
    }
    
    public String getLabel(final CrawlableDataset dataset) {
        for (final CrawlableDatasetLabeler curNamer : this.labelerList) {
            final String name = curNamer.getLabel(dataset);
            if (name != null) {
                return name;
            }
        }
        return null;
    }
}
