// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset.sorter;

import java.util.Collections;
import java.util.List;
import thredds.crawlabledataset.CrawlableDataset;
import java.util.Comparator;
import thredds.crawlabledataset.CrawlableDatasetSorter;

public class LexigraphicByNameSorter implements CrawlableDatasetSorter
{
    private boolean increasingOrder;
    private Comparator<CrawlableDataset> comparator;
    
    public LexigraphicByNameSorter(final boolean increasingOrder) {
        this.increasingOrder = false;
        this.increasingOrder = increasingOrder;
        this.comparator = new Comparator<CrawlableDataset>() {
            public int compare(final CrawlableDataset crDs1, final CrawlableDataset crDs2) {
                final int compareVal = crDs1.getName().compareTo(crDs2.getName());
                return LexigraphicByNameSorter.this.increasingOrder ? compareVal : (-compareVal);
            }
        };
    }
    
    public Object getConfigObject() {
        return null;
    }
    
    public boolean isIncreasing() {
        return this.increasingOrder;
    }
    
    public void sort(final List<CrawlableDataset> datasetList) {
        Collections.sort(datasetList, this.comparator);
    }
}
