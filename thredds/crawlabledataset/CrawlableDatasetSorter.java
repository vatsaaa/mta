// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import java.util.List;

public interface CrawlableDatasetSorter
{
    void sort(final List<CrawlableDataset> p0);
    
    Object getConfigObject();
}
