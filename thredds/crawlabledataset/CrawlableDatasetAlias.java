// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import thredds.crawlabledataset.filter.WildcardMatchOnNameFilter;
import org.slf4j.LoggerFactory;
import java.util.Date;
import java.io.IOException;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;

public class CrawlableDatasetAlias implements CrawlableDataset
{
    private static Logger log;
    private String path;
    private String name;
    private String wildcardPattern;
    private String postWildcardPath;
    private CrawlableDataset startDs;
    private String className;
    private Object configObj;
    
    public static boolean isAlias(final String path) {
        return path.indexOf("*") != -1;
    }
    
    public CrawlableDatasetAlias(final String path, final String className, final Object configObj) {
        if (!isAlias(path)) {
            throw new IllegalArgumentException("No wildcard in path <" + path + ">.");
        }
        this.path = path;
        this.className = className;
        this.configObj = configObj;
        final int preWildcardIndex = this.path.lastIndexOf("/", this.path.indexOf("*"));
        final int postWildcardIndex = this.path.indexOf("/", preWildcardIndex + 1);
        CrawlableDatasetAlias.log.debug("[" + preWildcardIndex + "] - [" + postWildcardIndex + "]");
        final String preWildcardPath = this.path.substring(0, preWildcardIndex);
        this.wildcardPattern = ((postWildcardIndex == -1) ? this.path.substring(preWildcardIndex + 1) : this.path.substring(preWildcardIndex + 1, postWildcardIndex));
        this.postWildcardPath = ((postWildcardIndex == -1) ? null : this.path.substring(postWildcardIndex + 1));
        CrawlableDatasetAlias.log.debug("dirPattern <" + this.path + ">=<" + preWildcardPath + "[" + preWildcardIndex + "]" + this.wildcardPattern + "[" + postWildcardIndex + "]" + this.postWildcardPath + ">");
        this.name = this.path.substring(preWildcardIndex + 1);
        try {
            this.startDs = CrawlableDatasetFactory.createCrawlableDataset(preWildcardPath, this.className, this.configObj);
        }
        catch (Exception e) {
            final String tmpMsg = "Pre-wildcard path <" + preWildcardPath + "> not a CrawlableDataset of expected type <" + this.className + ">: " + e.getMessage();
            CrawlableDatasetAlias.log.warn("CrawlableDatasetAlias(): " + tmpMsg);
            throw new IllegalArgumentException(tmpMsg);
        }
        if (!this.startDs.isCollection()) {
            final String tmpMsg2 = "Pre-wildcard path not a directory <" + this.startDs.getPath() + ">";
            CrawlableDatasetAlias.log.warn("CrawlableDatasetAlias(): " + tmpMsg2);
            throw new IllegalArgumentException(tmpMsg2);
        }
    }
    
    public Object getConfigObject() {
        return this.configObj;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public String getName() {
        return this.name;
    }
    
    public boolean exists() {
        return true;
    }
    
    public boolean isCollection() {
        return true;
    }
    
    public CrawlableDataset getDescendant(final String childPath) {
        return null;
    }
    
    public CrawlableDataset getParentDataset() {
        return null;
    }
    
    public List<CrawlableDataset> listDatasets() throws IOException {
        final List<CrawlableDataset> curMatchDatasets = this.startDs.listDatasets(new MyFilter(this.wildcardPattern, this.postWildcardPath != null));
        if (this.postWildcardPath == null) {
            return curMatchDatasets;
        }
        final List<CrawlableDataset> list = new ArrayList<CrawlableDataset>();
        for (final CrawlableDataset curDs : curMatchDatasets) {
            final String curMatchPathName = curDs.getPath() + "/" + this.postWildcardPath;
            CrawlableDataset newCrawlableDs = null;
            try {
                newCrawlableDs = CrawlableDatasetFactory.createCrawlableDataset(curMatchPathName, this.className, this.configObj);
            }
            catch (Exception e) {
                final String tmpMsg = "Couldn't create CrawlableDataset for path <" + curMatchPathName + "> and given class name <" + this.className + ">: " + e.getMessage();
                CrawlableDatasetAlias.log.warn("listDatasets(): " + tmpMsg);
                continue;
            }
            if (isAlias(this.postWildcardPath)) {
                list.addAll(newCrawlableDs.listDatasets());
            }
            else {
                list.add(newCrawlableDs);
            }
        }
        return list;
    }
    
    public List<CrawlableDataset> listDatasets(final CrawlableDatasetFilter filter) throws IOException {
        final List<CrawlableDataset> list = this.listDatasets();
        if (filter == null) {
            return list;
        }
        for (final CrawlableDataset curDs : list) {
            if (!filter.accept(curDs)) {
                list.remove(curDs);
            }
        }
        return list;
    }
    
    public long length() {
        return -1L;
    }
    
    public Date lastModified() {
        return null;
    }
    
    static {
        CrawlableDatasetAlias.log = LoggerFactory.getLogger(CrawlableDatasetAlias.class);
    }
    
    private class MyFilter implements CrawlableDatasetFilter
    {
        private boolean mustBeCollection;
        private WildcardMatchOnNameFilter proxyFilter;
        
        MyFilter(final String wildcardString, final boolean mustBeCollection) {
            this.proxyFilter = new WildcardMatchOnNameFilter(wildcardString);
            this.mustBeCollection = mustBeCollection;
        }
        
        public Object getConfigObject() {
            return null;
        }
        
        public boolean accept(final CrawlableDataset dataset) {
            return (!this.mustBeCollection || dataset.isCollection()) && this.proxyFilter.accept(dataset);
        }
    }
}
