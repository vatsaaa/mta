// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import org.slf4j.LoggerFactory;
import java.util.Calendar;
import java.util.Date;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.lang.reflect.InvocationTargetException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.net.URISyntaxException;
import java.net.URI;
import java.util.HashMap;
import java.net.URLConnection;
import java.util.Map;
import thredds.util.DodsURLExtractor;
import org.slf4j.Logger;

public class CrawlableDatasetDods implements CrawlableDataset
{
    private static Logger log;
    private static DodsURLExtractor urlExtractor;
    private static Map listDatasetsMap;
    private String path;
    private URLConnection pathUrlConnection;
    private String name;
    private Object configObj;
    private static String[] knownFileExtensions;
    private static String[] dodsExtensions;
    
    protected CrawlableDatasetDods() {
        this.pathUrlConnection = null;
        this.configObj = null;
    }
    
    protected CrawlableDatasetDods(final String path, final Object configObj) {
        this.pathUrlConnection = null;
        this.configObj = null;
        if (CrawlableDatasetDods.urlExtractor == null) {
            CrawlableDatasetDods.urlExtractor = new DodsURLExtractor();
        }
        if (CrawlableDatasetDods.listDatasetsMap == null) {
            CrawlableDatasetDods.listDatasetsMap = new HashMap();
        }
        if (configObj != null) {
            CrawlableDatasetDods.log.debug("CrawlableDatasetDods(): config object not null, it will be ignored <" + configObj.toString() + ">.");
            this.configObj = configObj;
        }
        if (path.startsWith("http:")) {
            this.path = path;
            try {
                new URI(path);
                this.name = this.getName(path);
                return;
            }
            catch (URISyntaxException e) {
                final String tmpMsg = "Bad URI syntax for path <" + path + ">: " + e.getMessage();
                CrawlableDatasetDods.log.debug("CrawlableDatasetDods(): " + tmpMsg);
                throw new IllegalArgumentException(tmpMsg);
            }
        }
        final String tmpMsg2 = "Invalid url <" + path + ">.";
        CrawlableDatasetDods.log.debug("CrawlableDatasetDods(): " + tmpMsg2);
        throw new IllegalArgumentException(tmpMsg2);
    }
    
    private CrawlableDatasetDods(final CrawlableDatasetDods parent, final String childPath) {
        this.pathUrlConnection = null;
        this.configObj = null;
        final String normalChildPath = childPath.startsWith("/") ? childPath.substring(1) : childPath;
        this.path = parent.getPath();
        this.path += (this.path.endsWith("/") ? normalChildPath : ("/" + normalChildPath));
        this.name = this.getName(this.path);
        this.configObj = null;
    }
    
    private String getName(final String path) {
        if (!path.equals("/")) {
            String tmpName = path.endsWith("/") ? path.substring(0, path.length() - 1) : path;
            final int index = tmpName.lastIndexOf("/");
            if (index != -1) {
                tmpName = tmpName.substring(index + 1);
            }
            return tmpName;
        }
        return path;
    }
    
    public URI getUri() {
        try {
            return new URI(this.path);
        }
        catch (URISyntaxException e) {
            return null;
        }
    }
    
    public Object getConfigObject() {
        return this.configObj;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public String getName() {
        return this.name;
    }
    
    public boolean isCollection() {
        return isCollection(this.path);
    }
    
    public CrawlableDataset getDescendant(final String relativePath) {
        if (relativePath.startsWith("/")) {
            throw new IllegalArgumentException("Path must be relative <" + relativePath + ">.");
        }
        return new CrawlableDatasetDods(this, relativePath);
    }
    
    private static boolean isCollection(final String path) {
        final String testPath = path.toLowerCase();
        if (isDodsDataset(testPath)) {
            return false;
        }
        int i;
        for (i = 0; i < CrawlableDatasetDods.knownFileExtensions.length && !testPath.endsWith(CrawlableDatasetDods.knownFileExtensions[i]); ++i) {}
        return i >= CrawlableDatasetDods.knownFileExtensions.length;
    }
    
    private static String getDodsExtension(final String path) {
        String extension = "";
        String testPath;
        int i;
        for (testPath = path.toLowerCase(), i = 0; i < CrawlableDatasetDods.dodsExtensions.length && !testPath.endsWith(CrawlableDatasetDods.dodsExtensions[i]); ++i) {}
        if (i < CrawlableDatasetDods.dodsExtensions.length) {
            extension = CrawlableDatasetDods.dodsExtensions[i];
        }
        return extension;
    }
    
    private static boolean isDodsDataset(final String path) {
        return getDodsExtension(path).length() > 0;
    }
    
    private static String removeDodsExtension(String path) {
        final String dodsExtension = getDodsExtension(path);
        if (dodsExtension.length() > 0) {
            path = path.substring(0, path.length() - dodsExtension.length());
        }
        return path;
    }
    
    private String forceChild(final String url) {
        String prefix = this.path;
        if (prefix.endsWith("/")) {
            prefix = this.path.substring(0, this.path.length() - 1);
        }
        final int j = url.substring(0, url.length() - 1).lastIndexOf(47);
        if (j >= 0) {
            final String ret = prefix + url.substring(j);
            return ret;
        }
        return url;
    }
    
    public List listDatasets() throws IOException {
        if (!this.isCollection()) {
            final String tmpMsg = "This dataset <" + this.getPath() + "> is not a collection dataset.";
            CrawlableDatasetDods.log.error("listDatasets(): " + tmpMsg);
            throw new IllegalStateException(tmpMsg);
        }
        if (CrawlableDatasetDods.listDatasetsMap.containsKey(this.path)) {
            return CrawlableDatasetDods.listDatasetsMap.get(this.path);
        }
        final List list = new ArrayList();
        final List pathList = new ArrayList();
        List possibleDsList = null;
        try {
            String openPath = this.path;
            if (!openPath.endsWith("/")) {
                openPath += "/";
            }
            possibleDsList = CrawlableDatasetDods.urlExtractor.extract(openPath);
        }
        catch (IOException e) {
            CrawlableDatasetDods.log.warn("listDatasets(): IOException while extracting dataset info from given OPeNDAP directory <" + this.path + ">, return empty list: " + e.getMessage());
            return list;
        }
        String curDsUrlString = null;
        final Iterator it = possibleDsList.iterator();
        while (it.hasNext()) {
            curDsUrlString = it.next();
            if (!isDodsDataset(curDsUrlString) && !isCollection(curDsUrlString)) {
                CrawlableDatasetDods.log.warn("expandThisLevel(): Dataset isn't an OPeNDAP dataset or collection dataset, skip <" + this.path + ">.");
            }
            else {
                curDsUrlString = removeDodsExtension(curDsUrlString);
                if (!curDsUrlString.startsWith(this.path)) {
                    CrawlableDatasetDods.log.debug("listDatasets(): skipping URL <" + curDsUrlString + ">, not child of this CrDs <" + this.path + ">.");
                }
                else {
                    if (pathList.contains(curDsUrlString)) {
                        continue;
                    }
                    pathList.add(curDsUrlString);
                    if (!curDsUrlString.startsWith(this.path)) {
                        CrawlableDatasetDods.log.debug("listDatasets(): current path <" + curDsUrlString + "> not child of given" + " location <" + this.path + ">, skip.");
                    }
                    else {
                        try {
                            new URI(curDsUrlString);
                        }
                        catch (URISyntaxException e2) {
                            CrawlableDatasetDods.log.error("listDatasets(): Skipping dataset  <" + curDsUrlString + "> due to URISyntaxException: " + e2.getMessage());
                            continue;
                        }
                        CrawlableDatasetDods.log.debug("listDatasets(): handle dataset (" + curDsUrlString + ")");
                        try {
                            list.add(CrawlableDatasetFactory.createCrawlableDataset(curDsUrlString, this.getClass().getName(), null));
                        }
                        catch (ClassNotFoundException e3) {
                            CrawlableDatasetDods.log.warn("listDatasets(): Can't make CrawlableDataset for child url <" + curDsUrlString + ">: " + e3.getMessage());
                        }
                        catch (NoSuchMethodException e4) {
                            CrawlableDatasetDods.log.warn("listDatasets(): Can't make CrawlableDataset for child url <" + curDsUrlString + ">: " + e4.getMessage());
                        }
                        catch (IllegalAccessException e5) {
                            CrawlableDatasetDods.log.warn("listDatasets(): Can't make CrawlableDataset for child url <" + curDsUrlString + ">: " + e5.getMessage());
                        }
                        catch (InvocationTargetException e6) {
                            CrawlableDatasetDods.log.warn("listDatasets(): Can't make CrawlableDataset for child url <" + curDsUrlString + ">: " + e6.getMessage());
                        }
                        catch (InstantiationException e7) {
                            CrawlableDatasetDods.log.warn("listDatasets(): Can't make CrawlableDataset for child url <" + curDsUrlString + ">: " + e7.getMessage());
                        }
                    }
                }
            }
        }
        CrawlableDatasetDods.listDatasetsMap.put(this.path, list);
        return list;
    }
    
    public List listDatasets(final CrawlableDatasetFilter filter) throws IOException {
        final List list = this.listDatasets();
        if (filter == null) {
            return list;
        }
        final List retList = new ArrayList();
        for (final CrawlableDataset curDs : list) {
            if (filter.accept(curDs)) {
                retList.add(curDs);
            }
        }
        return retList;
    }
    
    public CrawlableDataset getParentDataset() {
        if (!this.path.equals("/")) {
            String parentPath = this.path;
            final int index = parentPath.lastIndexOf("/", parentPath.endsWith("/") ? (parentPath.length() - 2) : (parentPath.length() - 1));
            if (index != -1) {
                parentPath = parentPath.substring(0, index + 1);
            }
            return new CrawlableDatasetDods(parentPath, null);
        }
        return null;
    }
    
    public boolean exists() {
        if (this.pathUrlConnection == null) {
            try {
                final URL u = new URL(this.path);
                this.pathUrlConnection = u.openConnection();
            }
            catch (MalformedURLException e) {}
            catch (IOException ex) {}
        }
        if (this.pathUrlConnection != null) {
            try {
                final int responseCode = ((HttpURLConnection)this.pathUrlConnection).getResponseCode();
                if (responseCode >= 200 && responseCode < 300) {
                    return true;
                }
            }
            catch (IOException ex2) {}
        }
        return false;
    }
    
    public long length() {
        if (this.isCollection()) {
            return 0L;
        }
        if (this.pathUrlConnection == null) {
            try {
                final URL u = new URL(this.path);
                this.pathUrlConnection = u.openConnection();
            }
            catch (MalformedURLException e) {}
            catch (IOException ex) {}
        }
        if (this.pathUrlConnection != null) {
            return this.pathUrlConnection.getContentLength();
        }
        return -1L;
    }
    
    public Date lastModified() {
        if (this.pathUrlConnection == null) {
            try {
                final URL u = new URL(this.path);
                this.pathUrlConnection = u.openConnection();
            }
            catch (MalformedURLException e) {}
            catch (IOException ex) {}
        }
        if (this.pathUrlConnection == null) {
            return null;
        }
        final long lastModified = this.pathUrlConnection.getLastModified();
        if (lastModified != 0L) {
            final Calendar cal = Calendar.getInstance();
            cal.clear();
            cal.setTimeInMillis(lastModified);
            return cal.getTime();
        }
        return null;
    }
    
    @Override
    public String toString() {
        return this.path;
    }
    
    static {
        CrawlableDatasetDods.log = LoggerFactory.getLogger(CrawlableDatasetDods.class);
        CrawlableDatasetDods.urlExtractor = null;
        CrawlableDatasetDods.listDatasetsMap = null;
        CrawlableDatasetDods.knownFileExtensions = new String[] { ".hdf", ".xml", ".nc", ".bz2", ".cdp", ".jpg" };
        CrawlableDatasetDods.dodsExtensions = new String[] { ".html", ".htm", ".das", ".dds", ".info" };
    }
}
