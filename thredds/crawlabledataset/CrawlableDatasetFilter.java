// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

public interface CrawlableDatasetFilter
{
    boolean accept(final CrawlableDataset p0);
    
    Object getConfigObject();
}
