// 
// Decompiled by Procyon v0.5.36
// 

package thredds.crawlabledataset;

import org.slf4j.LoggerFactory;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.io.IOException;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import org.slf4j.Logger;

public class CrawlableDatasetFile implements CrawlableDataset
{
    private static Logger log;
    private final File file;
    private final Object configObj;
    
    public CrawlableDatasetFile(String path, final Object configObj) {
        if (path.startsWith("file:")) {
            path = path.substring(5);
        }
        this.file = new File(path);
        if (configObj != null) {
            CrawlableDatasetFile.log.warn("CrawlableDatasetFile(): config object not null, it will be ignored <" + configObj.toString() + ">.");
            this.configObj = configObj;
        }
        else {
            this.configObj = null;
        }
    }
    
    private CrawlableDatasetFile(final CrawlableDatasetFile parent, final String childPath) {
        this.file = new File(parent.getFile(), childPath);
        this.configObj = null;
    }
    
    public CrawlableDatasetFile(final File file) {
        this.file = file;
        this.configObj = null;
    }
    
    private String normalizePath(final String path) {
        return path.replaceAll("\\\\", "/");
    }
    
    public File getFile() {
        return this.file;
    }
    
    public Object getConfigObject() {
        return this.configObj;
    }
    
    public String getPath() {
        return this.normalizePath(this.file.getPath());
    }
    
    public String getName() {
        return this.file.getName();
    }
    
    public boolean exists() {
        return this.file.exists() && this.file.canRead();
    }
    
    public boolean isCollection() {
        return this.file.isDirectory();
    }
    
    public CrawlableDataset getDescendant(final String relativePath) {
        if (relativePath.startsWith("/")) {
            throw new IllegalArgumentException("Path must be relative <" + relativePath + ">.");
        }
        return new CrawlableDatasetFile(this, relativePath);
    }
    
    public List<CrawlableDataset> listDatasets() throws IOException {
        if (!this.exists()) {
            final String tmpMsg = "This dataset <" + this.getPath() + "> does not exist.";
            CrawlableDatasetFile.log.error("listDatasets(): " + tmpMsg);
            throw new IllegalStateException(tmpMsg);
        }
        if (!this.isCollection()) {
            final String tmpMsg = "This dataset <" + this.getPath() + "> is not a collection dataset.";
            CrawlableDatasetFile.log.error("listDatasets(): " + tmpMsg);
            throw new IllegalStateException(tmpMsg);
        }
        final List<CrawlableDataset> list = new ArrayList<CrawlableDataset>();
        final File[] files = this.file.listFiles();
        if (files == null) {
            CrawlableDatasetFile.log.error("listDatasets(): the underlying file [" + this.file.getPath() + "] exists, is a directory, and canRead()==true but listFiles() returns null. This may be a problem Java has on Windows XP (Java 7 should fix).");
            return Collections.emptyList();
        }
        for (final File allFile : files) {
            final CrawlableDatasetFile crDs = new CrawlableDatasetFile(this, allFile.getName());
            if (crDs.exists()) {
                list.add(crDs);
            }
        }
        return list;
    }
    
    public List<CrawlableDataset> listDatasets(final CrawlableDatasetFilter filter) throws IOException {
        final List<CrawlableDataset> list = this.listDatasets();
        if (filter == null) {
            return list;
        }
        final List<CrawlableDataset> retList = new ArrayList<CrawlableDataset>();
        for (final CrawlableDataset curDs : list) {
            if (filter.accept(curDs)) {
                retList.add(curDs);
            }
        }
        return retList;
    }
    
    public CrawlableDataset getParentDataset() {
        final File parentFile = this.file.getParentFile();
        if (parentFile == null) {
            return null;
        }
        return new CrawlableDatasetFile(parentFile);
    }
    
    public long length() {
        if (this.isCollection()) {
            return 0L;
        }
        return this.file.length();
    }
    
    public Date lastModified() {
        final long lastModDate = this.file.lastModified();
        if (lastModDate == 0L) {
            return null;
        }
        final Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTimeInMillis(lastModDate);
        return cal.getTime();
    }
    
    @Override
    public String toString() {
        return this.getPath();
    }
    
    static {
        CrawlableDatasetFile.log = LoggerFactory.getLogger(CrawlableDatasetFile.class);
    }
}
