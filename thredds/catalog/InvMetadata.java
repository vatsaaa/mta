// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URI;

public class InvMetadata
{
    private InvDataset dataset;
    private String title;
    private String type;
    private String xlinkHref;
    private URI xlinkUri;
    private String namespaceURI;
    private String prefix;
    private boolean isInherited;
    private boolean isThreddsMetadata;
    private MetadataConverterIF converter;
    private Object contentObject;
    private ThreddsMetadata tm;
    private StringBuilder log;
    private boolean init;
    private volatile int hashCode;
    
    public InvMetadata(final InvDataset dataset, final String xlinkHref, final String title, final String type, final String namespaceURI, final String prefix, final boolean inherited, final boolean isThreddsMetadata, final MetadataConverterIF converter) {
        this.xlinkUri = null;
        this.isThreddsMetadata = true;
        this.converter = null;
        this.contentObject = null;
        this.tm = null;
        this.log = new StringBuilder();
        this.init = false;
        this.hashCode = 0;
        this.dataset = dataset;
        this.xlinkHref = xlinkHref;
        this.title = title;
        this.type = type;
        this.namespaceURI = namespaceURI;
        this.prefix = prefix;
        this.isInherited = inherited;
        this.isThreddsMetadata = isThreddsMetadata;
        this.converter = converter;
    }
    
    public InvMetadata(final InvDataset dataset, final String mtype, final String namespaceURI, final String namespacePrefix, final boolean inherited, final boolean isThreddsMetadata, final MetadataConverterIF converter, final Object contentObject) {
        this.xlinkUri = null;
        this.isThreddsMetadata = true;
        this.converter = null;
        this.contentObject = null;
        this.tm = null;
        this.log = new StringBuilder();
        this.init = false;
        this.hashCode = 0;
        this.dataset = dataset;
        this.type = mtype;
        this.namespaceURI = namespaceURI;
        this.prefix = namespacePrefix;
        this.isInherited = inherited;
        this.isThreddsMetadata = isThreddsMetadata;
        this.converter = converter;
        this.contentObject = contentObject;
        if (isThreddsMetadata) {
            this.tm = (ThreddsMetadata)contentObject;
        }
        this.init = true;
    }
    
    public InvMetadata(final InvDataset dataset, final boolean inherited, final ThreddsMetadata tm) {
        this.xlinkUri = null;
        this.isThreddsMetadata = true;
        this.converter = null;
        this.contentObject = null;
        this.tm = null;
        this.log = new StringBuilder();
        this.init = false;
        this.hashCode = 0;
        this.dataset = dataset;
        this.isInherited = inherited;
        this.isThreddsMetadata = true;
        this.contentObject = tm;
        this.tm = tm;
        this.init = true;
    }
    
    public InvDataset getParentDataset() {
        return this.dataset;
    }
    
    public MetadataConverterIF getConverter() {
        return this.converter;
    }
    
    public String getMetadataType() {
        return this.type;
    }
    
    public String getNamespaceURI() {
        return this.namespaceURI;
    }
    
    public String getNamespacePrefix() {
        return this.prefix;
    }
    
    public boolean hasXlink() {
        return this.xlinkHref != null;
    }
    
    public String getXlinkHref() {
        return this.xlinkHref;
    }
    
    public URI getXlinkURI() {
        return this.xlinkUri;
    }
    
    public String getXlinkTitle() {
        return this.title;
    }
    
    public boolean isInherited() {
        return this.isInherited;
    }
    
    public boolean isThreddsMetadata() {
        return this.isThreddsMetadata;
    }
    
    public void setThreddsMetadata(final boolean isThreddsMetadata) {
        this.isThreddsMetadata = isThreddsMetadata;
    }
    
    public void setNamespaceURI(final String namespaceURI) {
        this.namespaceURI = namespaceURI;
        this.hashCode = 0;
    }
    
    public Object getContentObject() {
        this.finish();
        return this.contentObject;
    }
    
    public void setThreddsMetadata(final ThreddsMetadata tmd) {
        this.tm = tmd;
    }
    
    public ThreddsMetadata getThreddsMetadata() {
        return this.tm;
    }
    
    public void finish() {
        if (this.init) {
            return;
        }
        this.init = true;
        if (this.xlinkHref == null) {
            return;
        }
        this.xlinkHref = this.xlinkHref.trim();
        try {
            this.xlinkUri = this.dataset.getParentCatalog().resolveUri(this.xlinkHref);
        }
        catch (URISyntaxException e2) {
            this.log.append(" ** Error: Bad URL in metadata href = ").append(this.xlinkHref).append("\n");
            return;
        }
        try {
            if (this.converter == null) {
                this.log.append("  **InvMetadata on = (").append(this).append("): has no converter\n");
                return;
            }
            this.contentObject = this.converter.readMetadataContentFromURL(this.dataset, this.xlinkUri);
            if (this.isThreddsMetadata) {
                this.tm = (ThreddsMetadata)this.contentObject;
            }
        }
        catch (IOException e) {
            this.log.append("  **InvMetadata on = (").append(this.xlinkUri).append("): Exception (").append(e.getMessage()).append(")\n");
        }
    }
    
    boolean check(final StringBuilder out) {
        boolean isValid = true;
        if (this.log.length() > 0) {
            isValid = false;
            out.append((CharSequence)this.log);
        }
        if (this.contentObject != null && this.converter != null) {
            isValid &= this.converter.validateMetadataContent(this.contentObject, out);
        }
        return isValid;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof InvMetadata && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (null != this.getNamespaceURI()) {
                result = 37 * result + this.getNamespaceURI().hashCode();
            }
            if (null != this.getXlinkHref()) {
                result = 37 * result + this.getXlinkHref().hashCode();
            }
            if (null != this.getXlinkTitle()) {
                result = 37 * result + this.getXlinkTitle().hashCode();
            }
            if (null != this.getMetadataType()) {
                result = 37 * result + this.getMetadataType().hashCode();
            }
            result = 37 * result + (this.isInherited() ? 1 : 0);
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
