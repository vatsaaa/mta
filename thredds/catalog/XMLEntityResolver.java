// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.io.Reader;
import java.io.StringReader;
import org.xml.sax.SAXParseException;
import java.util.HashMap;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.ErrorHandler;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.lang.reflect.Method;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import ucar.nc2.util.IO;
import java.io.ByteArrayOutputStream;
import org.jdom.input.SAXBuilder;
import org.jdom.Namespace;
import java.util.Map;
import org.xml.sax.EntityResolver;

public class XMLEntityResolver implements EntityResolver
{
    private static boolean debugEntityResolution;
    private static Map<String, String> entityHash;
    private static boolean schemaValidationOk;
    private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    public static final String W3C_XML_NAMESPACE = "http://www.w3.org/2001/XMLSchema";
    private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    public static final String CATALOG_NAMESPACE_06 = "http://www.unidata.ucar.edu/thredds";
    public static final String CATALOG_NAMESPACE_10 = "http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0";
    public static final String DQC_NAMESPACE_02 = "http://www.unidata.ucar.edu/schemas/thredds/queryCapability";
    public static final String DQC_NAMESPACE_03 = "http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.3";
    public static final String DQC_NAMESPACE_04 = "http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.4";
    public static final String CATGEN_NAMESPACE_05 = "http://www.unidata.ucar.edu/namespaces/thredds/CatalogGenConfig/v0.5";
    public static final String NJ22_NAMESPACE = "http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2";
    public static final String XLINK_NAMESPACE = "http://www.w3.org/1999/xlink";
    public static final Namespace xlinkNS;
    public static final Namespace xsiNS;
    private static boolean hasXerces;
    private static String externalSchemas;
    private SAXBuilder saxBuilder;
    private StringBuilder warnMessages;
    private StringBuilder errMessages;
    private StringBuilder fatalMessages;
    
    public static String getExternalSchemas() {
        if (XMLEntityResolver.externalSchemas == null) {
            XMLEntityResolver.externalSchemas = "http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0 http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.1.0.3.xsd http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2 http://www.unidata.ucar.edu/schemas/netcdf/ncml-2.2.xsd http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.4 http://www.unidata.ucar.edu/schemas/thredds/queryCapability.0.4.xsd http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.3 http://www.unidata.ucar.edu/schemas/thredds/queryCapability.0.3.xsd http://www.unidata.ucar.edu/schemas/thredds/queryCapability http://www.unidata.ucar.edu/schemas/thredds/queryCapability.0.2.xsd http://www.unidata.ucar.edu/thredds http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.0.6.xsd";
        }
        return XMLEntityResolver.externalSchemas;
    }
    
    public static void initEntity(final String entityName, final String resourceName, final String urlName) {
        String entity = null;
        try {
            final ByteArrayOutputStream sbuff = new ByteArrayOutputStream(3000);
            final InputStream is = IO.getFileResource(resourceName);
            if (is != null) {
                IO.copy(is, sbuff);
                entity = sbuff.toString();
                if (XMLEntityResolver.debugEntityResolution) {
                    System.out.println(" *** entity " + entityName + " mapped to local resource at " + resourceName);
                }
            }
            else if (urlName != null) {
                entity = IO.readURLcontentsWithException(urlName);
                if (XMLEntityResolver.debugEntityResolution) {
                    System.out.println(" *** entity " + entityName + " mapped to remote URL at " + urlName);
                }
            }
        }
        catch (IOException e) {
            System.out.println(" *** FAILED to map entity " + entityName + " locally at " + resourceName + " or remotely at " + urlName);
        }
        XMLEntityResolver.entityHash.put(entityName, entity);
        XMLEntityResolver.entityHash.put(urlName, entity);
    }
    
    public static String getDocumentBuilderFactoryVersion() {
        try {
            final Class version = Class.forName("org.apache.xerces.impl.Version");
            final Method m = version.getMethod("getVersion", (Class[])null);
            return (String)m.invoke(null, (Object[])null);
        }
        catch (Exception e) {
            return "Error= " + e.getMessage();
        }
    }
    
    private static void showFactoryInfo(final DocumentBuilderFactory factory) {
        System.out.println("------------------------");
        System.out.println("DocumentBuilderFactory class= " + factory.getClass().getName());
        try {
            final Class version = Class.forName("org.apache.xerces.impl.Version");
            final Method m = version.getMethod("getVersion", (Class[])null);
            System.out.println(" org.apache.xerces.impl.Version.version()=" + m.invoke(null, (Object[])null));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println();
        System.out.println(" factory.isValidating()=" + factory.isValidating());
        System.out.println(" factory.isNamespaceAware()=" + factory.isNamespaceAware());
        System.out.println(" factory.isIgnoringElementContentWhitespace()=" + factory.isIgnoringElementContentWhitespace());
        System.out.println(" factory.isExpandEntityReferences()=" + factory.isExpandEntityReferences());
        System.out.println(" factory.isIgnoringComments()=" + factory.isIgnoringComments());
        System.out.println(" factory.isCoalescing()=" + factory.isCoalescing());
    }
    
    private static void showBuilderInfo(final DocumentBuilder builder) {
        System.out.println("-----------------------");
        System.out.println(" builder.isValidating()=" + builder.isValidating());
        System.out.println(" builder.isNamespaceAware()=" + builder.isNamespaceAware());
        System.out.println("DocumentBuilder class= " + builder.getClass().getName());
    }
    
    public XMLEntityResolver(final boolean validate) {
        this.warnMessages = new StringBuilder();
        this.errMessages = new StringBuilder();
        this.fatalMessages = new StringBuilder();
        (this.saxBuilder = (XMLEntityResolver.hasXerces ? new SAXBuilder(validate) : new SAXBuilder("org.apache.xerces.parsers.SAXParser", validate))).setErrorHandler(new MyErrorHandler());
        if (validate) {
            this.saxBuilder.setFeature("http://apache.org/xml/features/validation/schema", true);
            this.saxBuilder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation", getExternalSchemas());
        }
        this.saxBuilder.setEntityResolver(this);
    }
    
    public SAXBuilder getSAXBuilder() {
        return this.saxBuilder;
    }
    
    public StringBuilder getWarningMessages() {
        return this.warnMessages;
    }
    
    public StringBuilder getErrorMessages() {
        return this.errMessages;
    }
    
    public StringBuilder getFatalMessages() {
        return this.fatalMessages;
    }
    
    public InputSource resolveEntity(final String publicId, final String systemId) throws SAXException, IOException {
        if (XMLEntityResolver.debugEntityResolution) {
            System.out.print("  publicId=" + publicId + " systemId=" + systemId);
        }
        String entity = XMLEntityResolver.entityHash.get(systemId);
        if (entity != null) {
            if (XMLEntityResolver.debugEntityResolution) {
                System.out.println(" *** resolved  with local copy");
            }
            return new MyInputSource(entity);
        }
        if (systemId.indexOf("InvCatalog.0.6.dtd") >= 0) {
            entity = XMLEntityResolver.entityHash.get("http://www.unidata.ucar.edu/projects/THREDDS/xml/InvCatalog.0.6.dtd");
            if (entity != null) {
                if (XMLEntityResolver.debugEntityResolution) {
                    System.out.println(" *** resolved2 with local copy");
                }
                return new MyInputSource(entity);
            }
        }
        if (XMLEntityResolver.debugEntityResolution) {
            System.out.println(" *** not resolved");
        }
        return null;
    }
    
    static {
        XMLEntityResolver.debugEntityResolution = false;
        XMLEntityResolver.entityHash = new HashMap<String, String>();
        XMLEntityResolver.schemaValidationOk = true;
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
        xsiNS = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        initEntity("http://www.unidata.ucar.edu/projects/THREDDS/xml/InvCatalog.0.6.dtd", "/resources/thredds/schemas/InvCatalog.0.6.dtd", "http://www.unidata.ucar.edu/projects/THREDDS/xml/InvCatalog.0.6.dtd");
        initEntity("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", "/resources/thredds/schemas/InvCatalog.1.0.3.xsd", "http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.1.0.3.xsd");
        initEntity("http://www.unidata.ucar.edu/thredds", "/resources/thredds/schemas/InvCatalog.0.6.xsd", "http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.0.6.xsd");
        initEntity("http://www.unidata.ucar.edu/schemas/thredds/queryCapability", "/resources/thredds/schemas/queryCapability.0.2.xsd", "http://www.unidata.ucar.edu/schemas/thredds/queryCapability.0.2.xsd");
        initEntity("http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.3", "/resources/thredds/schemas/queryCapability.0.3.xsd", "http://www.unidata.ucar.edu/schemas/thredds/queryCapability.0.3.xsd");
        initEntity("http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.4", "/resources/thredds/schemas/queryCapability.0.4.xsd", "http://www.unidata.ucar.edu/schemas/thredds/queryCapability.0.4.xsd");
        initEntity("http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2", "/resources/nj22/schemas/ncml-2.2.xsd", "http://www.unidata.ucar.edu/schemas/netcdf/ncml-2.2.xsd");
        initEntity("http://www.w3.org/1999/xlink", "/resources/thredds/schemas/xlink.xsd", "http://www.unidata.ucar.edu/schemas/other/xlink.xsd");
        initEntity("http://www.unidata.ucar.edu/projects/THREDDS/xml/CatalogGenConfig.0.5.dtd", "/resources/thredds/schemas/CatalogGenConfig.0.5.dtd", "http://www.unidata.ucar.edu/projects/THREDDS/xml/CatalogGenConfig.0.5.dtd");
        final String javaVersion = System.getProperty("java.version").substring(2, 3);
        final int v = Integer.parseInt(javaVersion);
        XMLEntityResolver.hasXerces = (v >= 5);
        XMLEntityResolver.hasXerces = true;
    }
    
    private class MyErrorHandler implements ErrorHandler
    {
        public void warning(final SAXParseException e) throws SAXException {
            XMLEntityResolver.this.warnMessages.append("*** XML parser warning ").append(this.showError(e)).append("\n");
        }
        
        public void error(final SAXParseException e) throws SAXException {
            XMLEntityResolver.this.errMessages.append("*** XML parser error ").append(this.showError(e)).append("\n");
        }
        
        public void fatalError(final SAXParseException e) throws SAXException {
            XMLEntityResolver.this.fatalMessages.append("*** XML parser fatalError ").append(this.showError(e)).append("\n");
        }
        
        private String showError(final SAXParseException e) {
            return "(" + e.getLineNumber() + ":" + e.getColumnNumber() + ")= " + e.getMessage();
        }
    }
    
    private class MyInputSource extends InputSource
    {
        MyInputSource(final String entity) {
            this.setCharacterStream(new StringReader(entity));
        }
    }
}
