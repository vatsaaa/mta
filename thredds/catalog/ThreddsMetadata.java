// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.util.Collections;
import ucar.nc2.Attribute;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import thredds.catalog.parser.jdom.InvCatalogFactory10;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import java.io.IOException;
import java.io.OutputStream;
import ucar.unidata.geoloc.LatLonRect;
import java.net.URI;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.units.DateRange;
import ucar.nc2.units.DateType;
import java.util.List;

public class ThreddsMetadata
{
    protected boolean inherited;
    protected List<Source> creators;
    protected List<Contributor> contributors;
    protected List<DateType> dates;
    protected List<InvDocumentation> docs;
    protected List<Vocab> keywords;
    protected List<InvMetadata> metadata;
    protected List<Vocab> projects;
    protected List<InvProperty> properties;
    protected List<Source> publishers;
    protected List<Variables> variables;
    protected GeospatialCoverage gc;
    protected DateRange timeCoverage;
    protected String authorityName;
    protected String serviceName;
    protected FeatureType dataType;
    protected DataFormatType dataFormat;
    protected double dataSize;
    private volatile int hashCode;
    
    public ThreddsMetadata(final boolean inherited) {
        this.creators = new ArrayList<Source>();
        this.contributors = new ArrayList<Contributor>();
        this.dates = new ArrayList<DateType>();
        this.docs = new ArrayList<InvDocumentation>();
        this.keywords = new ArrayList<Vocab>();
        this.metadata = new ArrayList<InvMetadata>();
        this.projects = new ArrayList<Vocab>();
        this.properties = new ArrayList<InvProperty>();
        this.publishers = new ArrayList<Source>();
        this.variables = new ArrayList<Variables>();
        this.dataSize = 0.0;
        this.hashCode = 0;
        this.inherited = inherited;
    }
    
    public ThreddsMetadata(final ThreddsMetadata from) {
        this.creators = new ArrayList<Source>();
        this.contributors = new ArrayList<Contributor>();
        this.dates = new ArrayList<DateType>();
        this.docs = new ArrayList<InvDocumentation>();
        this.keywords = new ArrayList<Vocab>();
        this.metadata = new ArrayList<InvMetadata>();
        this.projects = new ArrayList<Vocab>();
        this.properties = new ArrayList<InvProperty>();
        this.publishers = new ArrayList<Source>();
        this.variables = new ArrayList<Variables>();
        this.dataSize = 0.0;
        this.hashCode = 0;
        this.inherited = from.inherited;
        this.add(from, true);
    }
    
    public void add(final ThreddsMetadata tmd, final boolean includeInherited) {
        this.creators.addAll(tmd.getCreators());
        this.contributors.addAll(tmd.getContributors());
        this.dates.addAll(tmd.getDates());
        this.docs.addAll(tmd.getDocumentation());
        this.keywords.addAll(tmd.getKeywords());
        this.projects.addAll(tmd.getProjects());
        this.properties.addAll(tmd.getProperties());
        this.publishers.addAll(tmd.getPublishers());
        this.variables.addAll(tmd.getVariables());
        if (includeInherited) {
            this.metadata.addAll(tmd.getMetadata());
        }
        else {
            for (final InvMetadata mdata : tmd.getMetadata()) {
                if (!mdata.isInherited()) {
                    this.metadata.add(mdata);
                }
            }
        }
        if (this.gc == null) {
            this.gc = tmd.getGeospatialCoverage();
        }
        if (this.timeCoverage == null) {
            this.timeCoverage = tmd.getTimeCoverage();
        }
        if (this.serviceName == null) {
            this.serviceName = tmd.getServiceName();
        }
        if (this.dataType == null) {
            this.dataType = tmd.getDataType();
        }
        if (this.dataSize == 0.0) {
            this.dataSize = tmd.getDataSize();
        }
        if (this.dataFormat == null) {
            this.dataFormat = tmd.getDataFormatType();
        }
        if (this.authorityName == null) {
            this.authorityName = tmd.getAuthority();
        }
    }
    
    public void addCreator(final Source c) {
        if (c != null) {
            this.creators.add(c);
        }
    }
    
    public List<Source> getCreators() {
        return this.creators;
    }
    
    public void setCreators(final List<Source> creators) {
        this.creators = creators;
    }
    
    public void addContributor(final Contributor c) {
        if (c != null) {
            this.contributors.add(c);
        }
    }
    
    public List<Contributor> getContributors() {
        return this.contributors;
    }
    
    public void setContributors(final List<Contributor> contributors) {
        this.contributors = contributors;
    }
    
    public void addDate(final DateType d) {
        if (d != null) {
            this.dates.add(d);
        }
    }
    
    public List<DateType> getDates() {
        return this.dates;
    }
    
    public void addDocumentation(final InvDocumentation d) {
        if (d != null) {
            this.docs.add(d);
        }
    }
    
    public List<InvDocumentation> getDocumentation() {
        return this.docs;
    }
    
    public void addKeyword(final Vocab keyword) {
        if (keyword != null) {
            this.keywords.add(keyword);
        }
    }
    
    public List<Vocab> getKeywords() {
        return this.keywords;
    }
    
    public void setKeywords(final List<Vocab> keywords) {
        this.keywords = keywords;
    }
    
    public void addMetadata(final InvMetadata m) {
        if (m != null) {
            this.metadata.add(m);
        }
    }
    
    public void removeMetadata(final InvMetadata m) {
        this.metadata.remove(m);
    }
    
    public List<InvMetadata> getMetadata() {
        return this.metadata;
    }
    
    public void addProject(final Vocab project) {
        if (project != null) {
            this.projects.add(project);
        }
    }
    
    public List<Vocab> getProjects() {
        return this.projects;
    }
    
    public void setProjects(final List<Vocab> projects) {
        this.projects = projects;
    }
    
    public void addProperty(final InvProperty p) {
        if (p != null) {
            this.properties.add(p);
        }
    }
    
    public List<InvProperty> getProperties() {
        return this.properties;
    }
    
    public void addPublisher(final Source p) {
        if (p != null) {
            this.publishers.add(p);
        }
    }
    
    public List<Source> getPublishers() {
        return this.publishers;
    }
    
    public void setPublishers(final List<Source> publishers) {
        this.publishers = publishers;
    }
    
    public void addVariables(final Variables vs) {
        if (vs != null) {
            this.variables.add(vs);
        }
    }
    
    public List<Variables> getVariables() {
        return this.variables;
    }
    
    public void setGeospatialCoverage(final GeospatialCoverage gc) {
        this.gc = gc;
    }
    
    public GeospatialCoverage getGeospatialCoverage() {
        return this.gc;
    }
    
    public void setTimeCoverage(final DateRange tc) {
        this.timeCoverage = tc;
    }
    
    public DateRange getTimeCoverage() {
        return this.timeCoverage;
    }
    
    public boolean isInherited() {
        return this.inherited;
    }
    
    public void setInherited(final boolean inherited) {
        this.inherited = inherited;
    }
    
    public String getServiceName() {
        return this.serviceName;
    }
    
    public void setServiceName(final String serviceName) {
        this.serviceName = serviceName;
    }
    
    public FeatureType getDataType() {
        return this.dataType;
    }
    
    public void setDataType(final FeatureType dataType) {
        this.dataType = dataType;
    }
    
    public DataFormatType getDataFormatType() {
        return this.dataFormat;
    }
    
    public void setDataFormatType(final DataFormatType dataFormat) {
        this.dataFormat = dataFormat;
    }
    
    public String getAuthority() {
        return this.authorityName;
    }
    
    public void setAuthority(final String authorityName) {
        this.authorityName = authorityName;
    }
    
    public String getDocumentation(final String type) {
        for (final InvDocumentation doc : this.getDocumentation()) {
            final String dtype = doc.getType();
            if (dtype != null && dtype.equalsIgnoreCase(type)) {
                return doc.getInlineContent();
            }
        }
        return null;
    }
    
    public String getHistory() {
        return this.getDocumentation("history");
    }
    
    public void setHistory(final String history) {
        this.addDocumentation("history", history);
    }
    
    public String getProcessing() {
        return this.getDocumentation("processing_level");
    }
    
    public void setProcessing(final String processing) {
        this.addDocumentation("processing_level", processing);
    }
    
    public String getRights() {
        return this.getDocumentation("rights");
    }
    
    public void setRights(final String rights) {
        this.addDocumentation("rights", rights);
    }
    
    public String getSummary() {
        return this.getDocumentation("summary");
    }
    
    public void setSummary(final String summary) {
        this.addDocumentation("summary", summary);
    }
    
    public double getDataSize() {
        return this.dataSize;
    }
    
    public void setDataSize(final double size) {
        this.dataSize = size;
    }
    
    public boolean hasDataSize() {
        return this.dataSize != 0.0 && !Double.isNaN(this.dataSize);
    }
    
    public void addDocumentation(final String type, String content) {
        if (content == null) {
            this.removeDocumentation(type);
            return;
        }
        content = content.trim();
        for (final InvDocumentation doc : this.getDocumentation()) {
            final String dtype = doc.getType();
            if (dtype != null && dtype.equalsIgnoreCase(type)) {
                doc.setInlineContent(content);
                return;
            }
        }
        if (content.length() > 0) {
            this.addDocumentation(new InvDocumentation(null, null, null, type, content));
        }
    }
    
    public void removeDocumentation(final String type) {
        final Iterator iter = this.docs.iterator();
        while (iter.hasNext()) {
            final InvDocumentation doc = iter.next();
            final String dtype = doc.getType();
            if (dtype != null && dtype.equalsIgnoreCase(type)) {
                iter.remove();
            }
        }
    }
    
    String dump(final int n) {
        final StringBuilder buff = new StringBuilder(100);
        if (this.docs.size() > 0) {
            final String indent = InvDatasetImpl.indent(n + 2);
            buff.append(indent);
            buff.append("Docs:\n");
            for (final InvDocumentation doc : this.docs) {
                buff.append(InvDatasetImpl.indent(n + 4)).append(doc).append("\n");
            }
        }
        if (this.metadata.size() > 0) {
            final String indent = InvDatasetImpl.indent(n + 2);
            buff.append(indent);
            buff.append("Metadata:\n");
            for (final InvMetadata m : this.metadata) {
                buff.append(InvDatasetImpl.indent(n + 4)).append(m).append("\n");
            }
        }
        if (this.properties.size() > 0) {
            final String indent = InvDatasetImpl.indent(n + 2);
            buff.append(indent);
            buff.append("Properties:\n");
            for (final InvProperty p : this.properties) {
                buff.append(InvDatasetImpl.indent(n + 4)).append(p).append("\n");
            }
        }
        return buff.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof ThreddsMetadata && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + (this.inherited ? 1 : 0);
            result = 29 * result + ((this.creators != null) ? this.creators.hashCode() : 0);
            result = 29 * result + ((this.contributors != null) ? this.contributors.hashCode() : 0);
            result = 29 * result + ((this.dates != null) ? this.dates.hashCode() : 0);
            result = 29 * result + ((this.docs != null) ? this.docs.hashCode() : 0);
            result = 29 * result + ((this.keywords != null) ? this.keywords.hashCode() : 0);
            result = 29 * result + ((this.metadata != null) ? this.metadata.hashCode() : 0);
            result = 29 * result + ((this.projects != null) ? this.projects.hashCode() : 0);
            result = 29 * result + ((this.properties != null) ? this.properties.hashCode() : 0);
            result = 29 * result + ((this.publishers != null) ? this.publishers.hashCode() : 0);
            result = 29 * result + ((this.variables != null) ? this.variables.hashCode() : 0);
            result = 29 * result + ((this.gc != null) ? this.gc.hashCode() : 0);
            result = 29 * result + ((this.timeCoverage != null) ? this.timeCoverage.hashCode() : 0);
            result = 29 * result + ((this.authorityName != null) ? this.authorityName.hashCode() : 0);
            result = 29 * result + ((this.serviceName != null) ? this.serviceName.hashCode() : 0);
            result = 29 * result + ((this.dataType != null) ? this.dataType.hashCode() : 0);
            result = 29 * result + ((this.dataFormat != null) ? this.dataFormat.hashCode() : 0);
            final long temp = (this.dataSize != 0.0) ? Double.doubleToLongBits(this.dataSize) : 0L;
            result = 29 * result + (int)(temp ^ temp >>> 32);
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    public static void main(final String[] args) throws IOException {
        final GeospatialCoverage gc = new GeospatialCoverage();
        final LatLonRect bb = new LatLonRect();
        gc.setBoundingBox(bb);
        gc.toXML(System.out);
    }
    
    public static class Contributor
    {
        private String name;
        private String role;
        private volatile int hashCode;
        
        public Contributor() {
            this.hashCode = 0;
        }
        
        public Contributor(final String name, final String role) {
            this.hashCode = 0;
            this.name = name;
            this.role = role;
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            this.name = name;
            this.hashCode = 0;
        }
        
        public String getRole() {
            return this.role;
        }
        
        public void setRole(final String role) {
            this.role = role;
            this.hashCode = 0;
        }
        
        public static String editableProperties() {
            return "role name";
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof Contributor && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                result = 37 * result + this.getName().hashCode();
                if (null != this.getRole()) {
                    result = 37 * result + this.getRole().hashCode();
                }
                this.hashCode = result;
            }
            return this.hashCode;
        }
    }
    
    public static class Source
    {
        private Vocab name;
        private Vocab long_name;
        private String url;
        private String email;
        private volatile int hashCode;
        
        public Source() {
            this.hashCode = 0;
            this.name = new Vocab();
            this.long_name = new Vocab();
        }
        
        public Source(final Vocab name, final String url, final String email) {
            this.hashCode = 0;
            this.name = name;
            this.url = url;
            this.email = email;
        }
        
        public Vocab getNameVocab() {
            return this.name;
        }
        
        public String getName() {
            return this.name.getText();
        }
        
        public void setName(final String name) {
            this.name.setText(name);
            this.hashCode = 0;
        }
        
        public String getUrl() {
            return this.url;
        }
        
        public void setUrl(final String url) {
            this.url = url;
            this.hashCode = 0;
        }
        
        public String getEmail() {
            return this.email;
        }
        
        public void setEmail(final String email) {
            this.email = email;
            this.hashCode = 0;
        }
        
        public String getVocabulary() {
            return this.name.getVocabulary();
        }
        
        public void setVocabulary(final String vocabulary) {
            this.name.setVocabulary(vocabulary);
            this.long_name.setVocabulary(vocabulary);
            this.hashCode = 0;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof Source && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                result = 37 * result + this.getName().hashCode();
                if (null != this.getVocabulary()) {
                    result = 37 * result + this.getVocabulary().hashCode();
                }
                if (null != this.getUrl()) {
                    result = 37 * result + this.getUrl().hashCode();
                }
                if (null != this.getEmail()) {
                    result = 37 * result + this.getEmail().hashCode();
                }
                this.hashCode = result;
            }
            return this.hashCode;
        }
        
        public static String hiddenProperties() {
            return "nameVocab";
        }
        
        public static String editableProperties() {
            return "name email url vocabulary";
        }
    }
    
    public static class Vocab
    {
        private String text;
        private String vocabulary;
        private volatile int hashCode;
        
        public Vocab() {
            this.hashCode = 0;
        }
        
        public Vocab(final String text, final String vocabulary) {
            this.hashCode = 0;
            this.text = text;
            this.vocabulary = vocabulary;
        }
        
        public String getText() {
            return this.text;
        }
        
        public void setText(final String text) {
            this.text = text;
            this.hashCode = 0;
        }
        
        public String getVocabulary() {
            return this.vocabulary;
        }
        
        public void setVocabulary(final String vocabulary) {
            this.vocabulary = vocabulary;
            this.hashCode = 0;
        }
        
        public static String editableProperties() {
            return "text vocabulary";
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof Vocab && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                if (null != this.getText()) {
                    result = 37 * result + this.getText().hashCode();
                }
                if (null != this.getVocabulary()) {
                    result = 37 * result + this.getVocabulary().hashCode();
                }
                this.hashCode = result;
            }
            return this.hashCode;
        }
    }
    
    public static class GeospatialCoverage
    {
        private static Range defaultEastwest;
        private static Range defaultNorthsouth;
        private static Range defaultUpdown;
        private static GeospatialCoverage empty;
        private Range eastwest;
        private Range northsouth;
        private Range updown;
        private boolean isGlobal;
        private String zpositive;
        private List<Vocab> names;
        private volatile int hashCode;
        
        public GeospatialCoverage() {
            this.isGlobal = false;
            this.zpositive = "up";
            this.names = new ArrayList<Vocab>();
            this.hashCode = 0;
        }
        
        public GeospatialCoverage(final Range eastwest, final Range northsouth, final Range updown, final List<Vocab> names, final String zpositive) {
            this.isGlobal = false;
            this.zpositive = "up";
            this.names = new ArrayList<Vocab>();
            this.hashCode = 0;
            this.eastwest = eastwest;
            this.northsouth = northsouth;
            this.updown = updown;
            if (names != null) {
                this.names = new ArrayList<Vocab>(names);
            }
            if (zpositive != null) {
                this.zpositive = zpositive;
            }
            if (names != null) {
                for (final Vocab name : names) {
                    final String elem = name.getText();
                    if (elem.equalsIgnoreCase("global")) {
                        this.isGlobal = true;
                    }
                }
            }
        }
        
        public boolean isEmpty() {
            return this.equals(GeospatialCoverage.empty);
        }
        
        public Range getEastWestRange() {
            return this.eastwest;
        }
        
        public Range getNorthSouthRange() {
            return this.northsouth;
        }
        
        public Range getUpDownRange() {
            return this.updown;
        }
        
        public List<Vocab> getNames() {
            return this.names;
        }
        
        public String getZPositive() {
            return this.zpositive;
        }
        
        public void setZPositive(final String positive) {
            this.zpositive = positive;
            this.hashCode = 0;
        }
        
        public boolean getZPositiveUp() {
            return this.zpositive.equalsIgnoreCase("up");
        }
        
        public void setZPositiveUp(final boolean positive) {
            this.zpositive = (positive ? "up" : "down");
            this.hashCode = 0;
        }
        
        public boolean isValid() {
            return this.isGlobal || (this.eastwest != null && this.northsouth != null);
        }
        
        public boolean isGlobal() {
            return this.isGlobal;
        }
        
        public void setGlobal(final boolean isGlobal) {
            this.isGlobal = isGlobal;
            this.hashCode = 0;
        }
        
        public double getLatStart() {
            return (this.northsouth == null) ? Double.NaN : this.northsouth.start;
        }
        
        public void setLatStart(final double start) {
            if (this.northsouth == null) {
                this.northsouth = new Range(GeospatialCoverage.defaultNorthsouth);
            }
            this.northsouth.start = start;
            this.hashCode = 0;
        }
        
        public double getLatExtent() {
            return (this.northsouth == null) ? Double.NaN : this.northsouth.size;
        }
        
        public void setLatExtent(final double size) {
            if (this.northsouth == null) {
                this.northsouth = new Range(GeospatialCoverage.defaultNorthsouth);
            }
            this.northsouth.size = size;
            this.hashCode = 0;
        }
        
        public double getLatResolution() {
            return (this.northsouth == null) ? Double.NaN : this.northsouth.resolution;
        }
        
        public void setLatResolution(final double resolution) {
            if (this.northsouth == null) {
                this.northsouth = new Range(GeospatialCoverage.defaultNorthsouth);
            }
            this.northsouth.resolution = resolution;
            this.hashCode = 0;
        }
        
        public String getLatUnits() {
            return (this.northsouth == null) ? null : this.northsouth.units;
        }
        
        public void setLatUnits(final String units) {
            if (this.northsouth == null) {
                this.northsouth = new Range(GeospatialCoverage.defaultNorthsouth);
            }
            this.northsouth.units = units;
            this.hashCode = 0;
        }
        
        public double getLatNorth() {
            return Math.max(this.northsouth.start, this.northsouth.start + this.northsouth.size);
        }
        
        public double getLatSouth() {
            return Math.min(this.northsouth.start, this.northsouth.start + this.northsouth.size);
        }
        
        public double getLonStart() {
            return (this.eastwest == null) ? Double.NaN : this.eastwest.start;
        }
        
        public void setLonStart(final double start) {
            if (this.eastwest == null) {
                this.eastwest = new Range(GeospatialCoverage.defaultEastwest);
            }
            this.eastwest.start = start;
            this.hashCode = 0;
        }
        
        public double getLonExtent() {
            return (this.eastwest == null) ? Double.NaN : this.eastwest.size;
        }
        
        public void setLonExtent(final double size) {
            if (this.eastwest == null) {
                this.eastwest = new Range(GeospatialCoverage.defaultEastwest);
            }
            this.eastwest.size = size;
            this.hashCode = 0;
        }
        
        public double getLonResolution() {
            return (this.eastwest == null) ? Double.NaN : this.eastwest.resolution;
        }
        
        public void setLonResolution(final double resolution) {
            if (this.eastwest == null) {
                this.eastwest = new Range(GeospatialCoverage.defaultEastwest);
            }
            this.eastwest.resolution = resolution;
            this.hashCode = 0;
        }
        
        public String getLonUnits() {
            return (this.eastwest == null) ? null : this.eastwest.units;
        }
        
        public void setLonUnits(final String units) {
            if (this.eastwest == null) {
                this.eastwest = new Range(GeospatialCoverage.defaultEastwest);
            }
            this.eastwest.units = units;
            this.hashCode = 0;
        }
        
        public double getLonEast() {
            if (this.eastwest == null) {
                return Double.NaN;
            }
            return Math.max(this.eastwest.start, this.eastwest.start + this.eastwest.size);
        }
        
        public double getLonWest() {
            if (this.eastwest == null) {
                return Double.NaN;
            }
            return Math.min(this.eastwest.start, this.eastwest.start + this.eastwest.size);
        }
        
        public double getHeightStart() {
            return (this.updown == null) ? 0.0 : this.updown.start;
        }
        
        public void setHeightStart(final double start) {
            if (this.updown == null) {
                this.updown = new Range(GeospatialCoverage.defaultUpdown);
            }
            this.updown.start = start;
            this.hashCode = 0;
        }
        
        public double getHeightExtent() {
            return (this.updown == null) ? 0.0 : this.updown.size;
        }
        
        public void setHeightExtent(final double size) {
            if (this.updown == null) {
                this.updown = new Range(GeospatialCoverage.defaultUpdown);
            }
            this.updown.size = size;
            this.hashCode = 0;
        }
        
        public double getHeightResolution() {
            return (this.updown == null) ? 0.0 : this.updown.resolution;
        }
        
        public void setHeightResolution(final double resolution) {
            if (this.updown == null) {
                this.updown = new Range(GeospatialCoverage.defaultUpdown);
            }
            this.updown.resolution = resolution;
            this.hashCode = 0;
        }
        
        public String getHeightUnits() {
            return (this.updown == null) ? null : this.updown.units;
        }
        
        public void setHeightUnits(final String units) {
            if (this.updown == null) {
                this.updown = new Range(GeospatialCoverage.defaultUpdown);
            }
            this.updown.units = units;
            this.hashCode = 0;
        }
        
        public LatLonRect getBoundingBox() {
            return this.isGlobal ? new LatLonRect() : new LatLonRect(new LatLonPointImpl(this.getLatStart(), this.getLonStart()), this.getLatExtent(), this.getLonExtent());
        }
        
        public void setBoundingBox(final LatLonRect bb) {
            final LatLonPointImpl llpt = bb.getLowerLeftPoint();
            final LatLonPointImpl urpt = bb.getUpperRightPoint();
            final double height = urpt.getLatitude() - llpt.getLatitude();
            this.eastwest = new Range(llpt.getLongitude(), bb.getWidth(), 0.0, "degrees_east");
            this.northsouth = new Range(llpt.getLatitude(), height, 0.0, "degrees_north");
        }
        
        public void setVertical(final CoordinateAxis1D vaxis) {
            final int n = (int)vaxis.getSize();
            final double size = vaxis.getCoordValue(n - 1) - vaxis.getCoordValue(0);
            final double resolution = vaxis.getIncrement();
            final String units = vaxis.getUnitsString();
            this.updown = new Range(vaxis.getCoordValue(0), size, resolution, units);
            if (units != null) {
                this.setZPositiveUp(SimpleUnit.isCompatible("m", units));
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof GeospatialCoverage && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                if (null != this.getEastWestRange()) {
                    result = 37 * result + this.getEastWestRange().hashCode();
                }
                if (null != this.getNorthSouthRange()) {
                    result = 37 * result + this.getNorthSouthRange().hashCode();
                }
                if (null != this.getUpDownRange()) {
                    result = 37 * result + this.getUpDownRange().hashCode();
                }
                if (null != this.getNames()) {
                    result = 37 * result + this.getNames().hashCode();
                }
                if (null != this.getZPositive()) {
                    result = 2 * result + this.getZPositive().hashCode();
                }
                result = 2 * result + (this.isGlobal() ? 1 : 0);
                this.hashCode = result;
            }
            return this.hashCode;
        }
        
        public void toXML(final OutputStream out) throws IOException {
            final InvCatalogFactory10 converter = new InvCatalogFactory10();
            final Element elem = converter.writeGeospatialCoverage(this);
            final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
            fmt.output(elem, out);
        }
        
        static {
            GeospatialCoverage.defaultEastwest = new Range(0.0, 0.0, Double.NaN, "degrees_east");
            GeospatialCoverage.defaultNorthsouth = new Range(0.0, 0.0, Double.NaN, "degrees_north");
            GeospatialCoverage.defaultUpdown = new Range(0.0, 0.0, Double.NaN, "km");
            GeospatialCoverage.empty = new GeospatialCoverage();
        }
    }
    
    public static class Range
    {
        private double start;
        private double size;
        private double resolution;
        private String units;
        private volatile int hashCode;
        
        public Range(final double start, final double size, final double resolution, final String units) {
            this.hashCode = 0;
            this.start = start;
            this.size = size;
            this.resolution = resolution;
            this.units = units;
        }
        
        public Range(final Range from) {
            this.hashCode = 0;
            this.start = from.start;
            this.size = from.size;
            this.resolution = from.resolution;
            this.units = from.units;
        }
        
        public double getStart() {
            return this.start;
        }
        
        public double getSize() {
            return this.size;
        }
        
        public double getResolution() {
            return this.resolution;
        }
        
        public String getUnits() {
            return this.units;
        }
        
        public boolean hasResolution() {
            return this.resolution != 0.0 && !Double.isNaN(this.resolution);
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof Range && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                if (null != this.getUnits()) {
                    result = 37 * result + this.getUnits().hashCode();
                }
                result = 37 * result + (int)(this.getStart() * 1000.0);
                result = 37 * result + (int)(this.getSize() * 1000.0);
                if (this.hasResolution()) {
                    result = 37 * result + (int)(this.getResolution() * 1000.0);
                }
                this.hashCode = result;
            }
            return this.hashCode;
        }
    }
    
    public static class Variable implements Comparable
    {
        private String name;
        private String desc;
        private String vocabulary_name;
        private String units;
        private String id;
        private volatile int hashCode;
        
        public Variable() {
            this.hashCode = 0;
            this.name = "";
            this.desc = "";
            this.vocabulary_name = "";
            this.units = "";
            this.id = "";
        }
        
        public Variable(final String name, final String desc, final String vocabulary_name, final String units, final String id) {
            this.hashCode = 0;
            this.name = name;
            this.desc = desc;
            this.vocabulary_name = vocabulary_name;
            this.units = units;
            this.id = id;
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            this.name = name;
        }
        
        public String getDescription() {
            return this.desc;
        }
        
        public void setDescription(final String desc) {
            this.desc = desc;
        }
        
        public String getVocabularyName() {
            return this.vocabulary_name;
        }
        
        public void setVocabularyName(final String vocabulary_name) {
            this.vocabulary_name = vocabulary_name;
        }
        
        public String getVocabularyId() {
            return this.id;
        }
        
        public void setVocabularyId(final String id) {
            this.id = id;
        }
        
        public void setVocabularyId(final Attribute id) {
            if (id == null) {
                return;
            }
            final StringBuilder sbuff = new StringBuilder();
            for (int i = 0; i < id.getLength(); ++i) {
                if (i > 0) {
                    sbuff.append(",");
                }
                sbuff.append(id.getNumericValue(i));
            }
            this.id = sbuff.toString();
        }
        
        public String getUnits() {
            return this.units;
        }
        
        public void setUnits(final String units) {
            this.units = units;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof Variable && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                if (null != this.getName()) {
                    result = 37 * result + this.getName().hashCode();
                }
                if (null != this.getDescription()) {
                    result = 37 * result + this.getDescription().hashCode();
                }
                if (null != this.getVocabularyName()) {
                    result = 37 * result + this.getVocabularyName().hashCode();
                }
                if (null != this.getUnits()) {
                    result = 37 * result + this.getUnits().hashCode();
                }
                this.hashCode = result;
            }
            return this.hashCode;
        }
        
        @Override
        public String toString() {
            return "Variable [" + this.name + "]";
        }
        
        public int compareTo(final Object o) {
            final Variable ov = (Variable)o;
            return this.name.compareTo(ov.name);
        }
        
        public static String hiddenProperties() {
            return "nameVocab";
        }
        
        public static String editableProperties() {
            return "name description units vocabularyName";
        }
    }
    
    public static class Variables
    {
        private String vocabulary;
        private String vocabHref;
        private String mapHref;
        private URI vocabUri;
        private URI mapUri;
        private List<Variable> variables;
        private boolean isInit;
        private volatile int hashCode;
        
        public Variables(final String vocab) {
            this.variables = new ArrayList<Variable>();
            this.isInit = false;
            this.hashCode = 0;
            this.vocabulary = vocab;
        }
        
        public Variables(final String vocab, final String vocabHref, final URI vocabUri, final String mapHref, final URI mapUri) {
            this.variables = new ArrayList<Variable>();
            this.isInit = false;
            this.hashCode = 0;
            this.vocabulary = vocab;
            this.vocabHref = vocabHref;
            this.vocabUri = vocabUri;
            this.mapHref = mapHref;
            this.mapUri = mapUri;
        }
        
        public String getVocabulary() {
            return this.vocabulary;
        }
        
        public String getVocabHref() {
            return this.vocabHref;
        }
        
        public URI getVocabUri() {
            return this.vocabUri;
        }
        
        public URI getMapUri() {
            return this.mapUri;
        }
        
        public String getMapHref() {
            return this.mapHref;
        }
        
        public void addVariable(final Variable v) {
            this.variables.add(v);
        }
        
        public List<Variable> getVariableList() {
            this.init();
            return this.variables;
        }
        
        public void sort() {
            Collections.sort(this.variables);
        }
        
        private void init() {
            if (this.isInit || this.mapUri != null) {
                return;
            }
            this.isInit = true;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof Variables && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                if (null != this.getVocabulary()) {
                    result = 37 * result + this.getVocabulary().hashCode();
                }
                if (null != this.getVocabUri()) {
                    result = 37 * result + this.getVocabUri().hashCode();
                }
                if (null != this.getMapUri()) {
                    result = 37 * result + this.getMapUri().hashCode();
                }
                if (null != this.getVariableList()) {
                    result = 37 * result + this.getVariableList().hashCode();
                }
                this.hashCode = result;
            }
            return this.hashCode;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Variables");
            sb.append(" [").append((this.getVocabulary() != null) ? this.getVocabulary() : "").append("]");
            if (this.mapUri != null) {
                sb.append("map [").append(this.mapUri.toString()).append("]");
            }
            else {
                for (final Variable v : this.variables) {
                    sb.append(" ").append(v.toString());
                }
            }
            return sb.toString();
        }
    }
}
