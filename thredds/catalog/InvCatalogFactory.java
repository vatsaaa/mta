// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import org.slf4j.LoggerFactory;
import ucar.nc2.util.IO;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ByteArrayOutputStream;
import org.jdom.Element;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import org.jdom.Document;
import java.io.InputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URI;
import java.util.Iterator;
import thredds.catalog.parser.jdom.InvCatalogFactory10;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import thredds.util.PathAliasReplacement;
import java.util.List;
import java.util.Map;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;

public class InvCatalogFactory
{
    private static Logger log;
    public static boolean debugURL;
    public static boolean debugOpen;
    public static boolean debugVersion;
    public static boolean showParsedXML;
    public static boolean showStackTrace;
    public static boolean debugXML;
    public static boolean debugDBurl;
    public static boolean debugXMLopen;
    public static boolean showCatalogXML;
    private String name;
    private InvCatalogConvertIF defaultConverter;
    private SAXBuilder saxBuilder;
    private StringBuilder warnMessages;
    private StringBuilder errMessages;
    private StringBuilder fatalMessages;
    private Map<String, InvCatalogConvertIF> converters;
    private Map<String, MetadataConverterIF> metadataConverters;
    private List<PathAliasReplacement> dataRootLocAliasExpanders;
    
    public static InvCatalogFactory getDefaultFactory(final boolean validate) {
        return new InvCatalogFactory("default", validate);
    }
    
    public String getName() {
        return this.name;
    }
    
    public InvCatalogFactory(final String name, final boolean validate) {
        this.converters = new HashMap<String, InvCatalogConvertIF>(10);
        this.metadataConverters = new HashMap<String, MetadataConverterIF>(10);
        this.dataRootLocAliasExpanders = Collections.emptyList();
        this.name = name;
        final XMLEntityResolver xml = new XMLEntityResolver(validate);
        this.saxBuilder = xml.getSAXBuilder();
        this.warnMessages = xml.getWarningMessages();
        this.errMessages = xml.getErrorMessages();
        this.fatalMessages = xml.getFatalMessages();
        this.setDefaults();
    }
    
    public void setDataRootLocationAliasExpanders(final List<PathAliasReplacement> dataRootLocAliasExpanders) {
        if (dataRootLocAliasExpanders == null) {
            this.dataRootLocAliasExpanders = Collections.emptyList();
        }
        else {
            this.dataRootLocAliasExpanders = new ArrayList<PathAliasReplacement>(dataRootLocAliasExpanders);
        }
        for (final InvCatalogConvertIF catConv : this.converters.values()) {
            if (catConv instanceof InvCatalogFactory10) {
                ((InvCatalogFactory10)catConv).setDataRootLocationAliasExpanders(this.dataRootLocAliasExpanders);
            }
        }
    }
    
    public List<PathAliasReplacement> getDataRootLocationAliasExpanders() {
        return Collections.unmodifiableList((List<? extends PathAliasReplacement>)this.dataRootLocAliasExpanders);
    }
    
    private void setDefaults() {
        try {
            final Class fac1 = Class.forName("thredds.catalog.parser.jdom.InvCatalogFactory10");
            final Object fac1o = fac1.newInstance();
            this.defaultConverter = (InvCatalogConvertIF)fac1o;
            this.registerCatalogConverter("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", (InvCatalogConvertIF)fac1o);
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException("InvCatalogFactory: no implementing class found: " + e.getMessage());
        }
        catch (InstantiationException e2) {
            throw new RuntimeException("InvCatalogFactory: instantition failed: " + e2.getMessage());
        }
        catch (IllegalAccessException e3) {
            throw new RuntimeException("InvCatalogFactory: access failed: " + e3.getMessage());
        }
    }
    
    public void registerCatalogConverter(final String namespace, final InvCatalogConvertIF converter) {
        this.converters.put(namespace, converter);
    }
    
    public InvCatalogConvertIF getCatalogConverter(final String namespace) {
        return this.converters.get(namespace);
    }
    
    public void setCatalogConverter(final InvCatalogImpl cat, final String namespace) {
        cat.setCatalogConverter(this.getCatalogConverter(namespace));
    }
    
    public void registerMetadataConverter(final String key, final MetadataConverterIF converter) {
        this.metadataConverters.put(key, converter);
    }
    
    public void readXMLasynch(final String uriString, final CatalogSetCallback callback) {
        final InvCatalogImpl cat = this.readXML(uriString);
        callback.setCatalog(cat);
    }
    
    public InvCatalogImpl readXML(final String uriString) {
        URI uri;
        try {
            uri = new URI(uriString);
        }
        catch (URISyntaxException e) {
            final InvCatalogImpl cat = new InvCatalogImpl(uriString, null, null);
            cat.appendErrorMessage("**Fatal:  InvCatalogFactory.readXML URISyntaxException on URL (" + uriString + ") " + e.getMessage() + "\n", true);
            return cat;
        }
        return this.readXML(uri);
    }
    
    public InvCatalogImpl readXML(final URI uri) {
        this.warnMessages.setLength(0);
        this.errMessages.setLength(0);
        this.fatalMessages.setLength(0);
        final InputStream is = null;
        Document jdomDoc;
        try {
            jdomDoc = this.saxBuilder.build(uri.toURL());
        }
        catch (Exception e) {
            final InvCatalogImpl cat = new InvCatalogImpl(uri.toString(), null, null);
            cat.appendErrorMessage("**Fatal:  InvCatalogFactory.readXML failed\n Exception= " + e.getClass().getName() + " " + e.getMessage() + "\n fatalMessages= " + this.fatalMessages.toString() + "\n errMessages= " + this.errMessages.toString() + "\n warnMessages= " + this.warnMessages.toString() + "\n", true);
            return cat;
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e2) {
                    InvCatalogFactory.log.warn("Failed to close input stream [" + uri.toString() + "].");
                }
            }
        }
        if (this.fatalMessages.length() > 0) {
            final InvCatalogImpl cat2 = new InvCatalogImpl(uri.toString(), null, null);
            cat2.appendErrorMessage("**Fatal:  InvCatalogFactory.readXML XML Fatal error(s) =\n" + this.fatalMessages.toString() + "\n", true);
            return cat2;
        }
        return this.readXML(jdomDoc, uri);
    }
    
    public InvCatalogImpl readXML(final String catAsString, final URI baseUri) {
        return this.readXML(new StringReader(catAsString), baseUri);
    }
    
    public InvCatalogImpl readXML(final StringReader catAsStringReader, final URI baseUri) {
        final XMLEntityResolver resolver = new XMLEntityResolver(false);
        final SAXBuilder builder = resolver.getSAXBuilder();
        Document inDoc;
        try {
            inDoc = builder.build(catAsStringReader);
        }
        catch (Exception e) {
            final InvCatalogImpl cat = new InvCatalogImpl(baseUri.toString(), null, null);
            cat.appendErrorMessage("**Fatal:  InvCatalogFactory.readXML(String catAsString, URI uri) failed:\n  Exception= " + e.getClass().getName() + " " + e.getMessage() + "\n  fatalMessages= " + this.fatalMessages.toString() + "\n  errMessages= " + this.errMessages.toString() + "\n  warnMessages= " + this.warnMessages.toString() + "\n", true);
            return cat;
        }
        return this.readXML(inDoc, baseUri);
    }
    
    public InvCatalogImpl readXML(final InputStream docIs, final URI uri) {
        this.warnMessages.setLength(0);
        this.errMessages.setLength(0);
        this.fatalMessages.setLength(0);
        Document jdomDoc;
        try {
            jdomDoc = this.saxBuilder.build(docIs);
        }
        catch (Exception e) {
            final InvCatalogImpl cat = new InvCatalogImpl(uri.toString(), null, uri);
            cat.appendErrorMessage("**Fatal:  InvCatalogFactory.readXML failed\n Exception= " + e.getClass().getName() + " " + e.getMessage() + "\n fatalMessages= " + this.fatalMessages.toString() + "\n errMessages= " + this.errMessages.toString() + "\n warnMessages= " + this.warnMessages.toString() + "\n", true);
            return cat;
        }
        if (this.fatalMessages.length() > 0) {
            final InvCatalogImpl cat2 = new InvCatalogImpl(uri.toString(), null, uri);
            cat2.appendErrorMessage("**Fatal:  InvCatalogFactory.readXML XML Fatal error(s) =\n" + this.fatalMessages.toString() + "\n", true);
            return cat2;
        }
        return this.readXML(jdomDoc, uri);
    }
    
    public InvCatalogImpl readXML(final Document jdomDoc, final URI uri) {
        final Element root = jdomDoc.getRootElement();
        if (!root.getName().equalsIgnoreCase("catalog")) {
            throw new IllegalArgumentException("not a catalog");
        }
        final String namespace = root.getNamespaceURI();
        InvCatalogConvertIF fac = this.converters.get(namespace);
        if (fac == null) {
            fac = this.defaultConverter;
            if (InvCatalogFactory.debugVersion) {
                System.out.println("use default converter " + fac.getClass().getName() + "; no namespace " + namespace);
            }
        }
        else if (InvCatalogFactory.debugVersion) {
            System.out.println("use converter " + fac.getClass().getName() + " based on namespace " + namespace);
        }
        final InvCatalogImpl cat = fac.parseXML(this, jdomDoc, uri);
        cat.setCreateFrom(uri.toString());
        cat.setCatalogFactory(this);
        cat.setCatalogConverter(fac);
        cat.finish();
        if (InvCatalogFactory.showCatalogXML) {
            System.out.println("*** catalog/showCatalogXML");
            try {
                this.writeXML(cat, System.out);
            }
            catch (IOException ex) {
                InvCatalogFactory.log.warn("Error writing catalog for debugging", ex);
            }
        }
        if (this.fatalMessages.length() > 0) {
            cat.appendErrorMessage(this.fatalMessages.toString(), true);
        }
        if (this.errMessages.length() > 0) {
            cat.appendErrorMessage(this.errMessages.toString(), false);
        }
        if (this.warnMessages.length() > 0) {
            cat.appendErrorMessage(this.warnMessages.toString(), false);
        }
        return cat;
    }
    
    public String writeXML(final InvCatalogImpl catalog) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream(10000);
        this.writeXML(catalog, os);
        return os.toString();
    }
    
    public void writeXML(final InvCatalogImpl catalog, final OutputStream os) throws IOException {
        InvCatalogConvertIF fac = catalog.getCatalogConverter();
        if (fac == null) {
            fac = this.defaultConverter;
        }
        fac.writeXML(catalog, os);
    }
    
    public void writeXML(final InvCatalogImpl catalog, final OutputStream os, final boolean raw) throws IOException {
        InvCatalogConvertIF fac = catalog.getCatalogConverter();
        if (fac == null) {
            fac = this.defaultConverter;
        }
        fac.writeXML(catalog, os, raw);
    }
    
    public void writeXML(final InvCatalogImpl catalog, final String filename) throws IOException {
        final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(filename));
        this.writeXML(catalog, os);
        os.close();
    }
    
    public String writeXML_1_0(final InvCatalogImpl catalog) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream(10000);
        this.writeXML_1_0(catalog, os);
        return os.toString();
    }
    
    public String writeXML_0_6(final InvCatalogImpl catalog) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream(10000);
        this.writeXML_0_6(catalog, os);
        return os.toString();
    }
    
    public void writeXML_1_0(final InvCatalogImpl catalog, final OutputStream os) throws IOException {
        this.writeXML_ver("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0", catalog, os);
    }
    
    public void writeXML_0_6(final InvCatalogImpl catalog, final OutputStream os) throws IOException {
        this.writeXML_ver("http://www.unidata.ucar.edu/thredds", catalog, os);
    }
    
    private void writeXML_ver(final String namespace, final InvCatalogImpl catalog, final OutputStream os) throws IOException {
        final InvCatalogConvertIF converter = this.getCatalogConverter(namespace);
        if (converter == null) {
            final String tmpMsg = "This Factory <" + this.getName() + "> does not have a converter for the requested namespace <" + namespace + ">.";
            throw new IllegalStateException(tmpMsg);
        }
        converter.writeXML(catalog, os);
    }
    
    public void appendErr(final String err) {
        this.errMessages.append(err);
    }
    
    public void appendFatalErr(final String err) {
        this.fatalMessages.append(err);
    }
    
    public void appendWarning(final String err) {
        this.warnMessages.append(err);
    }
    
    public MetadataConverterIF getMetadataConverter(final String key) {
        if (key == null) {
            return null;
        }
        return this.metadataConverters.get(key);
    }
    
    private static InvCatalogImpl doOne(final InvCatalogFactory fac, final String urlString, final boolean show) {
        System.out.println("***read " + urlString);
        if (show) {
            System.out.println(" original catalog=\n" + IO.readURLcontents(urlString));
        }
        try {
            final InvCatalogImpl cat = fac.readXML(new URI(urlString));
            final StringBuilder buff = new StringBuilder();
            final boolean isValid = cat.check(buff, false);
            System.out.println("catalog <" + cat.getName() + "> " + (isValid ? "is" : "is not") + " valid");
            System.out.println(" validation output=\n" + (Object)buff);
            if (show) {
                System.out.println(" parsed catalog=\n" + fac.writeXML(cat));
            }
            return cat;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static void main(final String[] args) throws Exception {
        final InvCatalogFactory catFactory = getDefaultFactory(false);
        doOne(catFactory, "file:C:/data/work/maurer/atm_mod.xml", true);
    }
    
    static {
        InvCatalogFactory.log = LoggerFactory.getLogger(InvCatalogFactory.class);
        InvCatalogFactory.debugURL = false;
        InvCatalogFactory.debugOpen = false;
        InvCatalogFactory.debugVersion = false;
        InvCatalogFactory.showParsedXML = false;
        InvCatalogFactory.showStackTrace = false;
        InvCatalogFactory.debugXML = false;
        InvCatalogFactory.debugDBurl = false;
        InvCatalogFactory.debugXMLopen = false;
        InvCatalogFactory.showCatalogXML = false;
    }
}
