// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.net.URISyntaxException;
import java.net.URI;

public abstract class InvAccess
{
    protected InvDataset dataset;
    protected ServiceType type;
    protected DataFormatType dataFormat;
    protected InvService service;
    protected String urlPath;
    protected double dataSize;
    
    public InvAccess() {
        this.dataSize = Double.NaN;
    }
    
    public InvDataset getDataset() {
        return this.dataset;
    }
    
    public InvService getService() {
        return this.service;
    }
    
    public String getUrlPath() {
        return this.urlPath;
    }
    
    public DataFormatType getDataFormatType() {
        return (this.dataFormat != null) ? this.dataFormat : this.dataset.getDataFormatType();
    }
    
    public double getDataSize() {
        return this.dataSize;
    }
    
    public boolean hasDataSize() {
        return this.dataSize != 0.0 && !Double.isNaN(this.dataSize);
    }
    
    public String getStandardUrlName() {
        final URI uri = this.getStandardUri();
        if (uri == null) {
            return null;
        }
        return this.wrap(uri.toString());
    }
    
    public URI getStandardUri() {
        try {
            final InvCatalog cat = this.dataset.getParentCatalog();
            if (cat == null) {
                return new URI(this.getUnresolvedUrlName());
            }
            return cat.resolveUri(this.getUnresolvedUrlName());
        }
        catch (URISyntaxException e) {
            System.err.println("Error parsing URL= " + this.getUnresolvedUrlName());
            return null;
        }
    }
    
    public String getUnresolvedUrlName() {
        return this.service.getBase() + this.getUrlPath() + this.service.getSuffix();
    }
    
    private String wrap(final String url) {
        if (this.service.getServiceType() == ServiceType.THREDDS) {
            return "thredds:" + url;
        }
        if (this.service.getServiceType() == ServiceType.CdmRemote) {
            return "cdmremote:" + url;
        }
        return url;
    }
}
