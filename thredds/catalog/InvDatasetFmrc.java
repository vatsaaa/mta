// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import ucar.nc2.util.cache.FileCacheNOP;
import org.slf4j.LoggerFactory;
import java.util.Set;
import ucar.nc2.util.cache.FileFactory;
import java.util.Collections;
import java.util.Date;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.dataset.NetcdfDataset;
import org.jdom.Element;
import ucar.nc2.dt.fmrc.FmrcImpl;
import ucar.nc2.util.CancelTask;
import ucar.nc2.ncml.NcMLReader;
import java.util.Collection;
import java.net.URISyntaxException;
import java.util.Iterator;
import ucar.nc2.units.DateRange;
import ucar.nc2.thredds.MetadataExtractor;
import java.io.IOException;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import java.util.regex.Pattern;
import ucar.unidata.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import java.net.URI;
import ucar.nc2.units.TimeUnit;
import java.io.File;
import ucar.nc2.dt.fmrc.ForecastModelRunCollection;
import ucar.nc2.util.cache.FileCache;
import org.slf4j.Logger;

public class InvDatasetFmrc extends InvCatalogRef
{
    private static Logger logger;
    private static final String FMRC = "fmrc.ncd";
    private static final String BEST = "best.ncd";
    private static final String RUNS = "runs";
    private static final String RUN_NAME = "RUN_";
    private static final String FORECAST = "forecast";
    private static final String FORECAST_NAME = "ConstantForecast_";
    private static final String OFFSET = "offset";
    private static final String OFFSET_NAME = "Offset_";
    private static final String TITLE_RUNS = "Forecast Model Run";
    private static final String TITLE_OFFSET = "Constant Forecast Offset";
    private static final String TITLE_FORECAST = "Constant Forecast Date";
    private static final String SCAN = "files";
    private static FileCache fileCache;
    private volatile boolean madeDatasets;
    private volatile boolean madeFmrc;
    private final String path;
    private final boolean runsOnly;
    private InventoryParams params;
    private String dodsService;
    private ForecastModelRunCollection fmrc;
    private InvCatalogImpl catalog;
    private InvCatalogImpl catalogRuns;
    private InvCatalogImpl catalogOffsets;
    private InvCatalogImpl catalogForecasts;
    private InvDatasetScan scan;
    
    public InvDatasetFmrc(final InvDatasetImpl parent, final String name, final String path, final boolean runsOnly) {
        super(parent, name, "/thredds/catalog/" + path + "/catalog.xml");
        this.madeDatasets = false;
        this.madeFmrc = false;
        this.path = path;
        this.runsOnly = runsOnly;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public boolean isRunsOnly() {
        return this.runsOnly;
    }
    
    public InvDatasetScan getRawFileScan() {
        if (!this.madeDatasets) {
            this.getDatasets();
        }
        return this.scan;
    }
    
    public InventoryParams getFmrcInventoryParams() {
        return this.params;
    }
    
    public File getFile(final String remaining) {
        if (null == this.params) {
            return null;
        }
        final int pos = remaining.indexOf("files");
        final StringBuilder fname = new StringBuilder(this.params.location);
        if (!this.params.location.endsWith("/")) {
            fname.append("/");
        }
        fname.append((pos > -1) ? remaining.substring(pos + "files".length() + 1) : remaining);
        return new File(fname.toString());
    }
    
    public void setFmrcInventoryParams(final String location, final String def, final String suffix, final String olderThanS, final String subdirs) {
        this.params = new InventoryParams();
        this.params.location = location;
        this.params.def = def;
        this.params.suffix = suffix;
        this.params.subdirs = (subdirs != null && subdirs.equalsIgnoreCase("true"));
        if (olderThanS != null) {
            try {
                final TimeUnit tu = new TimeUnit(olderThanS);
                this.params.lastModifiedLimit = (long)(1000.0 * tu.getValueInSeconds());
            }
            catch (Exception e) {
                InvDatasetFmrc.logger.error("Invalid TimeUnit = " + olderThanS);
                throw new IllegalArgumentException("Invalid TimeUnit = " + olderThanS);
            }
        }
    }
    
    @Override
    public boolean hasAccess() {
        return false;
    }
    
    @Override
    public boolean hasNestedDatasets() {
        return true;
    }
    
    public InvCatalogImpl makeCatalog(final String match, final String orgPath, final URI baseURI) {
        InvDatasetFmrc.logger.debug("FMRC make catalog for " + match + " " + baseURI);
        try {
            if (match == null || match.length() == 0) {
                return this.makeCatalog(baseURI);
            }
            if (match.equals("runs")) {
                return this.makeCatalogRuns(baseURI);
            }
            if (match.equals("offset")) {
                return this.makeCatalogOffsets(baseURI);
            }
            if (match.equals("forecast")) {
                return this.makeCatalogForecasts(baseURI);
            }
            if (match.equals("files")) {
                return this.makeCatalogScan(orgPath, baseURI);
            }
            return null;
        }
        catch (Exception e) {
            InvDatasetFmrc.logger.error("Error making catalog for " + this.path, e);
            return null;
        }
    }
    
    @Override
    public List<InvDataset> getDatasets() {
        if (!this.madeDatasets) {
            final List<InvDataset> datasets = new ArrayList<InvDataset>();
            if (this.runsOnly) {
                final InvDatasetImpl ds = new InvCatalogRef(this, "Forecast Model Run", this.getCatalogHref("runs"));
                ds.finish();
                datasets.add(ds);
            }
            else {
                String id = this.getID();
                if (id == null) {
                    id = this.getPath();
                }
                InvDatasetImpl ds2 = new InvDatasetImpl(this, "Forecast Model Run Collection (2D time coordinates)");
                String name = this.getName() + "_" + "fmrc.ncd";
                name = StringUtil.replace(name, ' ', "_");
                ds2.setUrlPath(this.path + "/" + name);
                ds2.setID(id + "/" + name);
                ThreddsMetadata tm = ds2.getLocalMetadata();
                tm.addDocumentation("summary", "Forecast Model Run Collection (2D time coordinates).");
                ds2.getLocalMetadataInheritable().setServiceName(this.dodsService);
                ds2.finish();
                datasets.add(ds2);
                ds2 = new InvDatasetImpl(this, "Best Time Series");
                name = this.getName() + "_" + "best.ncd";
                name = StringUtil.replace(name, ' ', "_");
                ds2.setUrlPath(this.path + "/" + name);
                ds2.setID(id + "/" + name);
                tm = ds2.getLocalMetadata();
                tm.addDocumentation("summary", "Best time series, taking the data from the most recent run available.");
                ds2.finish();
                datasets.add(ds2);
                ds2 = new InvCatalogRef(this, "Forecast Model Run", this.getCatalogHref("runs"));
                ds2.finish();
                datasets.add(ds2);
                ds2 = new InvCatalogRef(this, "Constant Forecast Offset", this.getCatalogHref("offset"));
                ds2.finish();
                datasets.add(ds2);
                ds2 = new InvCatalogRef(this, "Constant Forecast Date", this.getCatalogHref("forecast"));
                ds2.finish();
                datasets.add(ds2);
                if (this.params != null) {
                    final InvDatasetFeatureCollection.ScanFilter filter = new InvDatasetFeatureCollection.ScanFilter(Pattern.compile(".*" + this.params.suffix), this.params.lastModifiedLimit);
                    this.scan = new InvDatasetScan((InvCatalogImpl)this.getParentCatalog(), this, "File_Access", this.path + "/" + "files", this.params.location, filter, true, "true", false, null, null, null);
                    final ThreddsMetadata tmi = this.scan.getLocalMetadataInheritable();
                    tmi.setServiceName("fileServices");
                    tmi.addDocumentation("summary", "Individual data file, which comprise the Forecast Model Run Collection.");
                    this.scan.finish();
                    datasets.add(this.scan);
                }
            }
            this.datasets = datasets;
            this.madeDatasets = true;
        }
        this.finish();
        return this.datasets;
    }
    
    private String getCatalogHref(final String what) {
        return "/thredds/catalog/" + this.path + "/" + what + "/catalog.xml";
    }
    
    private synchronized boolean checkIfChanged() throws IOException {
        final boolean changed = this.madeFmrc && this.fmrc.sync();
        if (changed) {
            this.catalog = null;
            this.catalogRuns = null;
            this.catalogOffsets = null;
            this.catalogForecasts = null;
        }
        return changed;
    }
    
    private InvCatalogImpl makeCatalog(final URI baseURI) throws IOException, URISyntaxException {
        if (this.catalog == null || this.checkIfChanged()) {
            final InvCatalogImpl parentCatalog = (InvCatalogImpl)this.getParentCatalog();
            final URI myURI = baseURI.resolve(this.getXlinkHref());
            final InvCatalogImpl mainCatalog = new InvCatalogImpl(this.getFullName(), parentCatalog.getVersion(), myURI);
            final InvDatasetImpl top = new InvDatasetImpl(this);
            top.setParent(null);
            final InvDatasetImpl parent = (InvDatasetImpl)this.getParent();
            if (parent != null) {
                top.transferMetadata(parent, true);
            }
            String id = this.getID();
            if (id == null) {
                id = this.getPath();
            }
            top.setID(id);
            this.makeFmrc();
            final ThreddsMetadata tmi = top.getLocalMetadataInheritable();
            if (tmi.getVariables().size() == 0) {
                final ThreddsMetadata.Variables vars = MetadataExtractor.extractVariables(this, this.fmrc.getGridDataset());
                if (vars != null) {
                    tmi.addVariables(vars);
                }
            }
            if (tmi.getGeospatialCoverage() == null) {
                final ThreddsMetadata.GeospatialCoverage gc = MetadataExtractor.extractGeospatial(this.fmrc.getGridDataset());
                if (gc != null) {
                    tmi.setGeospatialCoverage(gc);
                }
            }
            if (tmi.getTimeCoverage() == null) {
                final DateRange dateRange = MetadataExtractor.extractDateRange(this.fmrc.getGridDataset());
                if (dateRange != null) {
                    tmi.setTimeCoverage(dateRange);
                }
            }
            if (null != this.params) {
                final ThreddsMetadata tm = top.getLocalMetadata();
                final InvDocumentation doc = new InvDocumentation();
                String path = this.getPath();
                if (!path.endsWith("/")) {
                    path += "/";
                }
                doc.setXlinkHref("/thredds/modelInventory/" + path);
                doc.setXlinkTitle("Available Inventory");
                tm.addDocumentation(doc);
            }
            mainCatalog.addDataset(top);
            final List serviceLocal = this.getServicesLocal();
            for (final InvService service : parentCatalog.getServices()) {
                if (!serviceLocal.contains(service)) {
                    mainCatalog.addService(service);
                }
            }
            this.findDODSService(parentCatalog.getServices());
            for (final InvDataset ds : this.getDatasets()) {
                top.addDataset((InvDatasetImpl)ds);
            }
            mainCatalog.finish();
            this.catalog = mainCatalog;
        }
        return this.catalog;
    }
    
    private void findDODSService(final List<InvService> services) {
        for (final InvService service : services) {
            if (this.dodsService == null && service.getServiceType() == ServiceType.OPENDAP) {
                this.dodsService = service.getName();
                return;
            }
            if (service.getServiceType() != ServiceType.COMPOUND) {
                continue;
            }
            this.findDODSService(service.getServices());
        }
    }
    
    private InvCatalogImpl makeCatalogRuns(final URI baseURI) throws IOException {
        if (this.catalogRuns == null || this.checkIfChanged()) {
            final InvCatalogImpl parent = (InvCatalogImpl)this.getParentCatalog();
            final URI myURI = baseURI.resolve(this.getCatalogHref("runs"));
            final InvCatalogImpl runCatalog = new InvCatalogImpl(this.getFullName(), parent.getVersion(), myURI);
            final InvDatasetImpl top = new InvDatasetImpl(this);
            top.setParent(null);
            top.transferMetadata((InvDatasetImpl)this.getParent(), true);
            top.setName("Forecast Model Run");
            runCatalog.addDataset(top);
            final List<InvService> services = new ArrayList<InvService>(this.getServicesLocal());
            final InvService service = this.getServiceDefault();
            if (service != null && !services.contains(service)) {
                runCatalog.addService(service);
            }
            for (final InvDatasetImpl ds : this.makeRunDatasets()) {
                top.addDataset(ds);
            }
            runCatalog.finish();
            this.catalogRuns = runCatalog;
        }
        return this.catalogRuns;
    }
    
    private InvCatalogImpl makeCatalogOffsets(final URI baseURI) throws IOException {
        if (this.catalogOffsets == null || this.checkIfChanged()) {
            final InvCatalogImpl parent = (InvCatalogImpl)this.getParentCatalog();
            final URI myURI = baseURI.resolve(this.getCatalogHref("offset"));
            final InvCatalogImpl offCatalog = new InvCatalogImpl(this.getFullName(), parent.getVersion(), myURI);
            final InvDatasetImpl top = new InvDatasetImpl(this);
            top.setParent(null);
            top.transferMetadata((InvDatasetImpl)this.getParent(), true);
            top.setName("Constant Forecast Offset");
            offCatalog.addDataset(top);
            final List<InvService> services = this.getServicesLocal();
            final InvService service = this.getServiceDefault();
            if (service != null && !services.contains(service)) {
                offCatalog.addService(service);
            }
            for (final InvDatasetImpl ds : this.makeOffsetDatasets()) {
                top.addDataset(ds);
            }
            offCatalog.finish();
            this.catalogOffsets = offCatalog;
        }
        return this.catalogOffsets;
    }
    
    private InvCatalogImpl makeCatalogForecasts(final URI baseURI) throws IOException {
        if (this.catalogForecasts == null || this.checkIfChanged()) {
            final InvCatalogImpl parent = (InvCatalogImpl)this.getParentCatalog();
            final URI myURI = baseURI.resolve(this.getCatalogHref("forecast"));
            final InvCatalogImpl foreCatalog = new InvCatalogImpl(this.getFullName(), parent.getVersion(), myURI);
            final InvDatasetImpl top = new InvDatasetImpl(this);
            top.setParent(null);
            top.transferMetadata((InvDatasetImpl)this.getParent(), true);
            top.setName("Constant Forecast Date");
            foreCatalog.addDataset(top);
            final List<InvService> services = this.getServicesLocal();
            final InvService service = this.getServiceDefault();
            if (service != null && !services.contains(service)) {
                foreCatalog.addService(service);
            }
            for (final InvDatasetImpl ds : this.makeForecastDatasets()) {
                top.addDataset(ds);
            }
            foreCatalog.finish();
            this.catalogForecasts = foreCatalog;
        }
        return this.catalogForecasts;
    }
    
    private InvCatalogImpl makeCatalogScan(final String orgPath, final URI baseURI) {
        if (!this.madeDatasets) {
            this.getDatasets();
        }
        return this.scan.makeCatalogForDirectory(orgPath, baseURI);
    }
    
    private synchronized void makeFmrc() throws IOException {
        if (this.madeFmrc) {
            this.checkIfChanged();
            return;
        }
        final Element ncml = this.getNcmlElement();
        final NetcdfDataset ncd = NcMLReader.readNcML(this.path, ncml, null);
        ncd.setFileCache(InvDatasetFmrc.fileCache);
        this.fmrc = new FmrcImpl(ncd);
        this.madeFmrc = true;
    }
    
    private List<InvDatasetImpl> makeRunDatasets() throws IOException {
        this.makeFmrc();
        final List<InvDatasetImpl> datasets = new ArrayList<InvDatasetImpl>();
        final DateFormatter formatter = new DateFormatter();
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        for (final Date runDate : this.fmrc.getRunDates()) {
            String name = this.getName() + "_" + "RUN_" + formatter.toDateTimeStringISO(runDate);
            name = StringUtil.replace(name, ' ', "_");
            final InvDatasetImpl nested = new InvDatasetImpl(this, name);
            nested.setUrlPath(this.path + "/" + "runs" + "/" + name);
            nested.setID(id + "/" + "runs" + "/" + name);
            final ThreddsMetadata tm = nested.getLocalMetadata();
            tm.addDocumentation("summary", "Data from Run " + name);
            datasets.add(nested);
        }
        Collections.reverse(datasets);
        return datasets;
    }
    
    private List<InvDatasetImpl> makeOffsetDatasets() throws IOException {
        this.makeFmrc();
        final List<InvDatasetImpl> datasets = new ArrayList<InvDatasetImpl>();
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        for (final Double offset : this.fmrc.getForecastOffsets()) {
            String name = this.getName() + "_" + "Offset_" + offset + "hr";
            name = StringUtil.replace(name, ' ', "_");
            final InvDatasetImpl nested = new InvDatasetImpl(this, name);
            nested.setUrlPath(this.path + "/" + "offset" + "/" + name);
            nested.setID(id + "/" + "offset" + "/" + name);
            final ThreddsMetadata tm = nested.getLocalMetadata();
            tm.addDocumentation("summary", "Data from the " + offset + " hour forecasts, across different model runs.");
            datasets.add(nested);
        }
        return datasets;
    }
    
    private List<InvDatasetImpl> makeForecastDatasets() throws IOException {
        this.makeFmrc();
        final List<InvDatasetImpl> datasets = new ArrayList<InvDatasetImpl>();
        final DateFormatter formatter = new DateFormatter();
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        for (final Date forecastDate : this.fmrc.getForecastDates()) {
            String name = this.getName() + "_" + "ConstantForecast_" + formatter.toDateTimeStringISO(forecastDate);
            name = StringUtil.replace(name, ' ', "_");
            final InvDatasetImpl nested = new InvDatasetImpl(this, name);
            nested.setUrlPath(this.path + "/" + "forecast" + "/" + name);
            nested.setID(id + "/" + "forecast" + "/" + name);
            final ThreddsMetadata tm = nested.getLocalMetadata();
            tm.addDocumentation("summary", "Data with the same forecast date, " + name + ", across different model runs.");
            datasets.add(nested);
        }
        return datasets;
    }
    
    public NetcdfDataset getDataset(final String path) throws IOException {
        final int pos = path.indexOf("/");
        final String type = (pos > -1) ? path.substring(0, pos) : path;
        final String name = (pos > -1) ? path.substring(pos + 1) : "";
        if (type.equals("files") && this.params != null) {
            final String filename = this.params.location + (this.params.location.endsWith("/") ? "" : "/") + name;
            return NetcdfDataset.acquireDataset(null, filename, null, -1, null, null);
        }
        this.makeFmrc();
        NetcdfDataset result = null;
        String location = path;
        if (path.endsWith("fmrc.ncd")) {
            result = this.fmrc.getFmrcDataset();
        }
        else if (path.endsWith("best.ncd")) {
            result = this.fmrc.getBestTimeSeries();
        }
        else {
            location = name;
            if (type.equals("offset")) {
                final int pos2 = name.indexOf("Offset_");
                final int pos3 = name.indexOf("hr");
                if (pos2 < 0 || pos3 < 0) {
                    return null;
                }
                final String id = name.substring(pos2 + "Offset_".length(), pos3);
                final double hour = Double.parseDouble(id);
                result = this.fmrc.getForecastOffsetDataset(hour);
            }
            else if (type.equals("runs")) {
                final int pos2 = name.indexOf("RUN_");
                if (pos2 < 0) {
                    return null;
                }
                final String id2 = name.substring(pos2 + "RUN_".length());
                final DateFormatter formatter = new DateFormatter();
                final Date date = formatter.getISODate(id2);
                result = this.fmrc.getRunTimeDataset(date);
            }
            else if (type.equals("forecast")) {
                final int pos2 = name.indexOf("ConstantForecast_");
                if (pos2 < 0) {
                    return null;
                }
                final String id2 = name.substring(pos2 + "ConstantForecast_".length());
                final DateFormatter formatter = new DateFormatter();
                final Date date = formatter.getISODate(id2);
                result = this.fmrc.getForecastTimeDataset(date);
            }
        }
        if (null != result) {
            result.setLocation(location);
        }
        return result;
    }
    
    static {
        InvDatasetFmrc.logger = LoggerFactory.getLogger(InvDatasetFmrc.class);
        InvDatasetFmrc.fileCache = new FileCacheNOP();
    }
    
    public class InventoryParams
    {
        public String location;
        public String def;
        public String suffix;
        public boolean subdirs;
        public long lastModifiedLimit;
        
        public InventoryParams() {
            this.lastModifiedLimit = 0L;
        }
        
        @Override
        public String toString() {
            return "def=" + this.def + " location=" + this.location + " suffix=" + this.suffix + " lastModifiedLimit=" + this.lastModifiedLimit + " subdirs=" + this.subdirs;
        }
    }
}
