// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.net.URISyntaxException;
import ucar.nc2.units.TimeDuration;
import java.net.URI;
import ucar.unidata.util.Format;
import ucar.unidata.util.StringUtil;
import java.util.Date;
import ucar.nc2.units.DateRange;
import java.util.Collection;
import java.util.Iterator;
import ucar.nc2.units.DateType;
import java.util.ArrayList;
import ucar.nc2.constants.FeatureType;
import java.util.HashMap;
import org.jdom.Element;
import java.util.List;

public class InvDatasetImpl extends InvDataset
{
    private String urlPath;
    private String alias;
    private double size;
    private List<InvAccess> accessLocal;
    private List<InvService> servicesLocal;
    protected ThreddsMetadata tm;
    protected ThreddsMetadata tmi;
    protected ThreddsMetadata tmi6;
    protected Element ncmlElement;
    protected StringBuilder log;
    protected boolean mark;
    private boolean debugInherit;
    private boolean debugInherit2;
    private HashMap<Object, Object> userMap;
    private volatile int hashCode;
    
    public InvDatasetImpl(final InvDatasetImpl parent, final String name, final FeatureType dataType, final String serviceName, final String urlPath) {
        super(parent, name);
        this.size = 0.0;
        this.accessLocal = new ArrayList<InvAccess>();
        this.servicesLocal = new ArrayList<InvService>();
        this.tm = new ThreddsMetadata(false);
        this.tmi = new ThreddsMetadata(true);
        this.tmi6 = new ThreddsMetadata(true);
        this.log = new StringBuilder();
        this.mark = false;
        this.debugInherit = false;
        this.debugInherit2 = false;
        this.userMap = null;
        this.hashCode = 0;
        this.tm.setDataType(dataType);
        this.tm.setServiceName(serviceName);
        this.urlPath = urlPath;
    }
    
    public boolean finish() {
        boolean ok = true;
        if (this.debugInherit) {
            System.out.println("Now finish " + this.getName() + " id= " + this.getID());
        }
        this.authorityName = null;
        this.dataType = null;
        this.dataFormatType = null;
        this.defaultService = null;
        this.gc = null;
        this.tc = null;
        this.docs = new ArrayList<InvDocumentation>();
        this.metadata = new ArrayList<InvMetadata>();
        this.properties = new ArrayList<InvProperty>();
        this.creators = new ArrayList<ThreddsMetadata.Source>();
        this.contributors = new ArrayList<ThreddsMetadata.Contributor>();
        this.dates = new ArrayList<DateType>();
        this.keywords = new ArrayList<ThreddsMetadata.Vocab>();
        this.projects = new ArrayList<ThreddsMetadata.Vocab>();
        this.publishers = new ArrayList<ThreddsMetadata.Source>();
        this.variables = new ArrayList<ThreddsMetadata.Variables>();
        this.canonicalize();
        this.transfer2PublicMetadata(this.tm, true);
        this.transfer2PublicMetadata(this.tmi, true);
        this.transfer2PublicMetadata(this.tmi6, true);
        this.transferInheritable2PublicMetadata((InvDatasetImpl)this.getParent());
        this.access = new ArrayList<InvAccess>();
        if (this.urlPath != null && this.getServiceDefault() != null) {
            final InvAccessImpl a = new InvAccessImpl(this, this.urlPath, this.getServiceDefault());
            a.setSize(this.size);
            a.finish();
            this.addExpandedAccess(a);
        }
        final Iterator iter = this.accessLocal.iterator();
        while (iter.hasNext()) {
            final InvAccessImpl a = iter.next();
            a.finish();
            this.addExpandedAccess(a);
        }
        if (!(this instanceof InvCatalogRef)) {
            for (final InvDataset invDataset : this.getDatasets()) {
                final InvDatasetImpl curDs = (InvDatasetImpl)invDataset;
                ok &= curDs.finish();
            }
        }
        return ok;
    }
    
    private void transferInheritable2PublicMetadata(final InvDatasetImpl parent) {
        if (parent == null) {
            return;
        }
        if (this.debugInherit) {
            System.out.println(" inheritFromParent= " + parent.getID());
        }
        this.transfer2PublicMetadata(parent.getLocalMetadataInheritable(), true);
        this.transferInheritable2PublicMetadata((InvDatasetImpl)parent.getParent());
    }
    
    private void transfer2PublicMetadata(final ThreddsMetadata tmd, final boolean inheritAll) {
        if (tmd == null) {
            return;
        }
        if (this.debugInherit) {
            System.out.println("  transferMetadata " + tmd);
        }
        if (this.authorityName == null) {
            this.authorityName = tmd.getAuthority();
        }
        if (this.dataType == null || this.dataType == FeatureType.ANY || this.dataType == FeatureType.NONE) {
            this.dataType = tmd.getDataType();
        }
        if (this.dataFormatType == null || this.dataFormatType == DataFormatType.NONE) {
            this.dataFormatType = tmd.getDataFormatType();
        }
        if (this.defaultService == null) {
            this.defaultService = this.findService(tmd.getServiceName());
        }
        if (this.gc == null) {
            final ThreddsMetadata.GeospatialCoverage tgc = tmd.getGeospatialCoverage();
            if (tgc != null && !tgc.isEmpty()) {
                this.gc = tgc;
            }
        }
        if (this.tc == null) {
            final DateRange ttc = tmd.getTimeCoverage();
            if (ttc != null) {
                this.tc = ttc;
            }
        }
        if (this.tc == null) {
            this.tc = tmd.getTimeCoverage();
        }
        for (final InvProperty item : tmd.getProperties()) {
            if (!this.properties.contains(item)) {
                if (this.debugInherit) {
                    System.out.println("  add Property " + item + " to " + this.getID());
                }
                this.properties.add(item);
            }
        }
        this.creators.addAll(tmd.getCreators());
        this.contributors.addAll(tmd.getContributors());
        this.dates.addAll(tmd.getDates());
        this.docs.addAll(tmd.getDocumentation());
        this.keywords.addAll(tmd.getKeywords());
        this.projects.addAll(tmd.getProjects());
        this.publishers.addAll(tmd.getPublishers());
        this.variables.addAll(tmd.getVariables());
        for (final InvMetadata meta : tmd.getMetadata()) {
            if (meta.isInherited() || inheritAll) {
                if (!meta.isThreddsMetadata()) {
                    this.metadata.add(meta);
                }
                else {
                    if (this.debugInherit) {
                        System.out.println("  add metadata Element " + tmd.isInherited() + " " + meta);
                    }
                    meta.finish();
                    this.transfer2PublicMetadata(meta.getThreddsMetadata(), inheritAll);
                    this.metadata.add(meta);
                }
            }
        }
    }
    
    public void transferMetadata(final InvDatasetImpl fromDs, final boolean copyInheritedMetadataFromParents) {
        if (this.debugInherit2) {
            System.out.println(" transferMetadata= " + fromDs.getName());
        }
        if (this != fromDs) {
            this.getLocalMetadata().add(fromDs.getLocalMetadata(), false);
        }
        this.transferInheritableMetadata(fromDs, this.getLocalMetadataInheritable(), copyInheritedMetadataFromParents);
        this.setResourceControl(fromDs.getRestrictAccess());
    }
    
    private void transferInheritableMetadata(final InvDatasetImpl fromDs, final ThreddsMetadata target, final boolean copyInheritedMetadataFromParents) {
        if (fromDs == null) {
            return;
        }
        if (this.debugInherit2) {
            System.out.println(" transferInheritedMetadata= " + fromDs.getName());
        }
        target.add(fromDs.getLocalMetadataInheritable(), true);
        if (copyInheritedMetadataFromParents) {
            this.transferInheritableMetadata((InvDatasetImpl)fromDs.getParent(), target, true);
        }
    }
    
    private void addExpandedAccess(final InvAccessImpl a) {
        final InvService service = a.getService();
        if (null == service) {
            a.check(this.log, false);
            return;
        }
        if (service.getServiceType() == ServiceType.COMPOUND) {
            for (final InvService nestedService : service.getServices()) {
                final InvAccessImpl nestedAccess = new InvAccessImpl(this, a.getUrlPath(), nestedService);
                this.addExpandedAccess(nestedAccess);
            }
        }
        else {
            this.access.add(a);
        }
    }
    
    protected void canonicalize() {
        Iterator iter = this.tm.metadata.iterator();
        while (iter.hasNext()) {
            final InvMetadata m = iter.next();
            if (m.isThreddsMetadata() && !m.isInherited() && !m.hasXlink()) {
                final ThreddsMetadata nested = m.getThreddsMetadata();
                this.tm.add(nested, false);
                iter.remove();
            }
        }
        iter = this.tm.metadata.iterator();
        while (iter.hasNext()) {
            final InvMetadata m = iter.next();
            if (m.isThreddsMetadata() && m.isInherited() && !m.hasXlink()) {
                final ThreddsMetadata nested = m.getThreddsMetadata();
                this.tmi.add(nested, true);
                iter.remove();
            }
        }
    }
    
    public InvDatasetImpl(final String urlPath, final FeatureType dataType, final ServiceType stype) {
        super(null, "local file");
        this.size = 0.0;
        this.accessLocal = new ArrayList<InvAccess>();
        this.servicesLocal = new ArrayList<InvService>();
        this.tm = new ThreddsMetadata(false);
        this.tmi = new ThreddsMetadata(true);
        this.tmi6 = new ThreddsMetadata(true);
        this.log = new StringBuilder();
        this.mark = false;
        this.debugInherit = false;
        this.debugInherit2 = false;
        this.userMap = null;
        this.hashCode = 0;
        this.tm.setDataType(dataType);
        this.tm.setServiceName("anon");
        this.urlPath = urlPath;
        this.addService(new InvService(this.tm.getServiceName(), stype.toString(), "", "", null));
        this.finish();
    }
    
    public InvDatasetImpl(final InvDataset parent, final String name) {
        super(parent, name);
        this.size = 0.0;
        this.accessLocal = new ArrayList<InvAccess>();
        this.servicesLocal = new ArrayList<InvService>();
        this.tm = new ThreddsMetadata(false);
        this.tmi = new ThreddsMetadata(true);
        this.tmi6 = new ThreddsMetadata(true);
        this.log = new StringBuilder();
        this.mark = false;
        this.debugInherit = false;
        this.debugInherit2 = false;
        this.userMap = null;
        this.hashCode = 0;
    }
    
    public InvDatasetImpl(final InvDatasetImpl from) {
        super(from.getParent(), from.getName());
        this.size = 0.0;
        this.accessLocal = new ArrayList<InvAccess>();
        this.servicesLocal = new ArrayList<InvService>();
        this.tm = new ThreddsMetadata(false);
        this.tmi = new ThreddsMetadata(true);
        this.tmi6 = new ThreddsMetadata(true);
        this.log = new StringBuilder();
        this.mark = false;
        this.debugInherit = false;
        this.debugInherit2 = false;
        this.userMap = null;
        this.hashCode = 0;
        this.tm = new ThreddsMetadata(from.getLocalMetadata());
        this.tmi = new ThreddsMetadata(from.getLocalMetadataInheritable());
        this.accessLocal = new ArrayList<InvAccess>(from.getAccessLocal());
        this.servicesLocal = new ArrayList<InvService>(from.getServicesLocal());
        this.harvest = from.harvest;
        this.collectionType = from.collectionType;
    }
    
    public String getAlias() {
        return this.alias;
    }
    
    public void setAlias(final String alias) {
        this.alias = alias;
        this.hashCode = 0;
    }
    
    public void setCatalog(final InvCatalog catalog) {
        this.catalog = catalog;
        this.hashCode = 0;
    }
    
    public InvDataset getParentReal() {
        return this.parent;
    }
    
    public String getUrlPath() {
        return this.urlPath;
    }
    
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
        this.hashCode = 0;
    }
    
    public void setAuthority(final String authorityName) {
        this.tm.setAuthority(authorityName);
        this.hashCode = 0;
    }
    
    public void setCollectionType(final CollectionType collectionType) {
        this.collectionType = collectionType;
        this.hashCode = 0;
    }
    
    public void setHarvest(final boolean harvest) {
        this.harvest = harvest;
        this.hashCode = 0;
    }
    
    public void setID(final String id) {
        this.id = id;
        this.hashCode = 0;
    }
    
    public void setName(final String name) {
        this.name = name;
        this.hashCode = 0;
    }
    
    public void setParent(final InvDatasetImpl parent) {
        this.parent = parent;
        this.hashCode = 0;
    }
    
    public void setGeospatialCoverage(final ThreddsMetadata.GeospatialCoverage gc) {
        this.tm.setGeospatialCoverage(gc);
        this.hashCode = 0;
    }
    
    public void setTimeCoverage(final DateRange tc) {
        this.tm.setTimeCoverage(tc);
        this.hashCode = 0;
    }
    
    public void setDataFormatType(final DataFormatType dataFormatType) {
        this.tm.setDataFormatType(dataFormatType);
        this.hashCode = 0;
    }
    
    public void setDataType(final FeatureType dataType) {
        this.tm.setDataType(dataType);
        this.hashCode = 0;
    }
    
    public double getDataSize() {
        return this.tm.getDataSize();
    }
    
    public void setDataSize(final double dataSize) {
        this.tm.setDataSize(dataSize);
        this.hashCode = 0;
    }
    
    public DateType getLastModifiedDate() {
        for (final DateType dateType : this.tm.getDates()) {
            if (dateType.getType() != null && dateType.getType().equals("modified")) {
                return dateType;
            }
        }
        return null;
    }
    
    public void setLastModifiedDate(final DateType lastModDate) {
        if (lastModDate == null) {
            throw new IllegalArgumentException("Last modified date can't be null.");
        }
        if (lastModDate.getType() == null || !lastModDate.getType().equals("modified")) {
            throw new IllegalArgumentException("Date type must be \"modified\" (was \"" + lastModDate.getType() + "\").");
        }
        final DateType curLastModDateType = this.getLastModifiedDate();
        if (curLastModDateType != null) {
            this.tm.getDates().remove(curLastModDateType);
        }
        this.tm.addDate(lastModDate);
        this.hashCode = 0;
    }
    
    public void setLastModifiedDate(final Date lastModDate) {
        if (lastModDate == null) {
            throw new IllegalArgumentException("Last modified date can't be null.");
        }
        final DateType lastModDateType = new DateType(false, lastModDate);
        lastModDateType.setType("modified");
        this.setLastModifiedDate(lastModDateType);
    }
    
    public void setServiceName(final String serviceName) {
        this.tm.setServiceName(serviceName);
        this.hashCode = 0;
    }
    
    public void setContributors(final List<ThreddsMetadata.Contributor> a) {
        final List<ThreddsMetadata.Contributor> dest = this.tm.getContributors();
        for (final ThreddsMetadata.Contributor item : a) {
            if (!dest.contains(item)) {
                dest.add(item);
            }
        }
        this.hashCode = 0;
    }
    
    public void setKeywords(final List<ThreddsMetadata.Vocab> a) {
        final List<ThreddsMetadata.Vocab> dest = this.tm.getKeywords();
        for (final ThreddsMetadata.Vocab item : a) {
            if (!dest.contains(item)) {
                dest.add(item);
            }
        }
        this.hashCode = 0;
    }
    
    public void setProjects(final List<ThreddsMetadata.Vocab> a) {
        final List<ThreddsMetadata.Vocab> dest = this.tm.getProjects();
        for (final ThreddsMetadata.Vocab item : a) {
            if (!dest.contains(item)) {
                dest.add(item);
            }
        }
        this.hashCode = 0;
    }
    
    public void setPublishers(final List<ThreddsMetadata.Source> a) {
        final List<ThreddsMetadata.Source> dest = this.tm.getPublishers();
        for (final ThreddsMetadata.Source item : a) {
            if (!dest.contains(item)) {
                dest.add(item);
            }
        }
        this.hashCode = 0;
    }
    
    public void setResourceControl(final String restrictAccess) {
        this.restrictAccess = restrictAccess;
    }
    
    public void addAccess(final InvAccess a) {
        this.accessLocal.add(a);
        this.hashCode = 0;
    }
    
    public void addAccess(final List<InvAccess> a) {
        this.accessLocal.addAll(a);
        this.hashCode = 0;
    }
    
    public List<InvAccess> getAccessLocal() {
        return this.accessLocal;
    }
    
    public Element getNcmlElement() {
        return this.ncmlElement;
    }
    
    public void setNcmlElement(final Element ncmlElement) {
        this.ncmlElement = ncmlElement;
    }
    
    public void addDataset(final InvDatasetImpl ds) {
        if (ds == null) {
            return;
        }
        ds.setParent(this);
        this.datasets.add(ds);
        this.hashCode = 0;
    }
    
    public void addDataset(final int index, final InvDatasetImpl ds) {
        if (ds == null) {
            return;
        }
        ds.setParent(this);
        this.datasets.add(index, ds);
        this.hashCode = 0;
    }
    
    public boolean removeDataset(final InvDatasetImpl ds) {
        if (this.datasets.remove(ds)) {
            ds.setParent(null);
            final InvCatalogImpl cat = (InvCatalogImpl)this.getParentCatalog();
            if (cat != null) {
                cat.removeDatasetByID(ds);
            }
            return true;
        }
        return false;
    }
    
    public boolean replaceDataset(final InvDatasetImpl remove, final InvDatasetImpl add) {
        for (int i = 0; i < this.datasets.size(); ++i) {
            final InvDataset dataset = this.datasets.get(i);
            if (dataset.equals(remove)) {
                this.datasets.set(i, add);
                final InvCatalogImpl cat = (InvCatalogImpl)this.getParentCatalog();
                if (cat != null) {
                    cat.removeDatasetByID(remove);
                    cat.addDatasetByID(add);
                }
                return true;
            }
        }
        return false;
    }
    
    public void addDocumentation(final InvDocumentation doc) {
        this.tm.addDocumentation(doc);
        this.hashCode = 0;
    }
    
    public void addProperty(final InvProperty p) {
        this.tm.addProperty(p);
        this.hashCode = 0;
    }
    
    @Deprecated
    public void addService(final InvService service) {
        this.servicesLocal.add(service);
        this.services.add(service);
        for (final InvService nested : service.getServices()) {
            this.services.add(nested);
        }
        this.hashCode = 0;
    }
    
    @Deprecated
    public void removeService(final InvService service) {
        this.servicesLocal.remove(service);
        this.services.remove(service);
        for (final InvService nested : service.getServices()) {
            this.services.remove(nested);
        }
    }
    
    public List<InvService> getServicesLocal() {
        return this.servicesLocal;
    }
    
    public void setServicesLocal(final List<InvService> s) {
        this.services = new ArrayList<InvService>();
        this.servicesLocal = new ArrayList<InvService>();
        for (final InvService elem : s) {
            this.addService(elem);
        }
        this.hashCode = 0;
    }
    
    public ThreddsMetadata getLocalMetadata() {
        return this.tm;
    }
    
    public void setLocalMetadata(final ThreddsMetadata tm) {
        this.tm = tm;
        this.hashCode = 0;
    }
    
    public ThreddsMetadata getLocalMetadataInheritable() {
        return this.tmi;
    }
    
    public boolean removeLocalMetadata(final InvMetadata metadata) {
        final InvDatasetImpl parentDataset = (InvDatasetImpl)metadata.getParentDataset();
        final List localMdata = parentDataset.getLocalMetadata().getMetadata();
        if (localMdata.contains(metadata) && localMdata.remove(metadata)) {
            this.hashCode = 0;
            return true;
        }
        return false;
    }
    
    public String getServiceName() {
        if (this.defaultService != null) {
            return this.defaultService.getName();
        }
        return null;
    }
    
    protected boolean getMark() {
        return this.mark;
    }
    
    protected void setMark(final boolean mark) {
        this.mark = mark;
    }
    
    public Object getUserProperty(final Object key) {
        if (this.userMap == null) {
            return null;
        }
        return this.userMap.get(key);
    }
    
    public void setUserProperty(final Object key, final Object value) {
        if (this.userMap == null) {
            this.userMap = new HashMap<Object, Object>();
        }
        this.userMap.put(key, value);
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    @Deprecated
    public static void writeHtmlDescription(final StringBuilder buff, final InvDatasetImpl ds, final boolean complete, final boolean isServer, final boolean datasetEvents, final boolean catrefEvents) {
        writeHtmlDescription(buff, ds, complete, isServer, datasetEvents, catrefEvents, true);
    }
    
    public static void writeHtmlDescription(final StringBuilder buff, final InvDatasetImpl ds, final boolean complete, final boolean isServer, final boolean datasetEvents, final boolean catrefEvents, final boolean resolveRelativeUrls) {
        if (ds == null) {
            return;
        }
        if (complete) {
            buff.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n").append("        \"http://www.w3.org/TR/html4/loose.dtd\">\n").append("<html>\n");
            buff.append("<head>");
            buff.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
            buff.append("</head>");
            buff.append("<body>\n");
        }
        buff.append("<h2>Dataset: ").append(ds.getFullName()).append("</h2>\n<ul>\n");
        if (ds.getDataFormatType() != null && ds.getDataFormatType() != DataFormatType.NONE) {
            buff.append(" <li><em>Data format: </em>").append(StringUtil.quoteHtmlContent(ds.getDataFormatType().toString())).append("</li>\n");
        }
        if (ds.getDataSize() != 0.0 && !Double.isNaN(ds.getDataSize())) {
            buff.append(" <li><em>Data size: </em>").append(Format.formatByteSize(ds.getDataSize())).append("</li>\n");
        }
        if (ds.getDataType() != null && ds.getDataType() != FeatureType.ANY && ds.getDataType() != FeatureType.NONE) {
            buff.append(" <li><em>Data type: </em>").append(StringUtil.quoteHtmlContent(ds.getDataType().toString())).append("</li>\n");
        }
        if (ds.getCollectionType() != null && ds.getCollectionType() != CollectionType.NONE) {
            buff.append(" <li><em>Collection type: </em>").append(StringUtil.quoteHtmlContent(ds.getCollectionType().toString())).append("</li>\n");
        }
        if (ds.isHarvest()) {
            buff.append(" <li><em>Harvest: </em>").append(ds.isHarvest()).append("</li>\n");
        }
        if (ds.getAuthority() != null) {
            buff.append(" <li><em>Naming Authority: </em>").append(StringUtil.quoteHtmlContent(ds.getAuthority())).append("</li>\n");
        }
        if (ds.getID() != null) {
            buff.append(" <li><em>ID: </em>").append(StringUtil.quoteHtmlContent(ds.getID())).append("</li>\n");
        }
        if (ds.getRestrictAccess() != null) {
            buff.append(" <li><em>RestrictAccess: </em>").append(StringUtil.quoteHtmlContent(ds.getRestrictAccess())).append("</li>\n");
        }
        if (ds instanceof InvCatalogRef) {
            final InvCatalogRef catref = (InvCatalogRef)ds;
            String href = (resolveRelativeUrls || catrefEvents) ? resolve(ds, catref.getXlinkHref()) : catref.getXlinkHref();
            if (catrefEvents) {
                href = "catref:" + href;
            }
            buff.append(" <li><em>CatalogRef: </em>").append(makeHref(href, null)).append("</li>\n");
        }
        buff.append("</ul>\n");
        final List<InvDocumentation> docs = ds.getDocumentation();
        if (docs.size() > 0) {
            buff.append("<h3>Documentation:</h3>\n<ul>\n");
            for (final InvDocumentation doc : docs) {
                final String type = (doc.getType() == null) ? "" : ("<strong>" + StringUtil.quoteHtmlContent(doc.getType()) + ":</strong> ");
                final String inline = doc.getInlineContent();
                if (inline != null && inline.length() > 0) {
                    buff.append(" <li>").append(type).append(StringUtil.quoteHtmlContent(inline)).append("</li>\n");
                }
                if (doc.hasXlink()) {
                    buff.append(" <li>").append(type).append(makeHref(doc.getXlinkHref(), doc.getXlinkTitle())).append("</li>\n");
                }
            }
            buff.append("</ul>\n");
        }
        final List<InvAccess> access = ds.getAccess();
        if (access.size() > 0) {
            buff.append("<h3>Access:</h3>\n<ol>\n");
            for (final InvAccess a : access) {
                final InvService s = a.getService();
                String fullUrlString;
                final String urlString = fullUrlString = ((resolveRelativeUrls || datasetEvents) ? a.getStandardUrlName() : a.getUnresolvedUrlName());
                if (datasetEvents) {
                    fullUrlString = "dataset:" + fullUrlString;
                }
                if (isServer) {
                    final ServiceType stype = s.getServiceType();
                    if (stype == ServiceType.OPENDAP || stype == ServiceType.DODS) {
                        fullUrlString += ".html";
                    }
                    else if (stype == ServiceType.WCS) {
                        fullUrlString += "?service=WCS&version=1.0.0&request=GetCapabilities";
                    }
                    else if (stype == ServiceType.WMS) {
                        fullUrlString += "?service=WMS&version=1.3.0&request=GetCapabilities";
                    }
                    else if (stype == ServiceType.NetcdfSubset) {
                        fullUrlString += "/dataset.html";
                    }
                    else if (stype == ServiceType.CdmRemote) {
                        fullUrlString += "?req=form";
                    }
                }
                buff.append(" <li> <b>").append(StringUtil.quoteHtmlContent(s.getServiceType().toString()));
                buff.append(":</b> ").append(makeHref(fullUrlString, urlString)).append("</li>\n");
            }
            buff.append("</ol>\n");
        }
        final List<ThreddsMetadata.Contributor> contributors = ds.getContributors();
        if (contributors.size() > 0) {
            buff.append("<h3>Contributors:</h3>\n<ul>\n");
            for (final ThreddsMetadata.Contributor t : contributors) {
                final String role = (t.getRole() == null) ? "" : ("<strong> (" + StringUtil.quoteHtmlContent(t.getRole()) + ")</strong> ");
                buff.append(" <li>").append(StringUtil.quoteHtmlContent(t.getName())).append(role).append("</li>\n");
            }
            buff.append("</ul>\n");
        }
        final List<ThreddsMetadata.Vocab> keywords = ds.getKeywords();
        if (keywords.size() > 0) {
            buff.append("<h3>Keywords:</h3>\n<ul>\n");
            for (final ThreddsMetadata.Vocab t2 : keywords) {
                final String vocab = (t2.getVocabulary() == null) ? "" : (" <strong>(" + StringUtil.quoteHtmlContent(t2.getVocabulary()) + ")</strong> ");
                buff.append(" <li>").append(StringUtil.quoteHtmlContent(t2.getText())).append(vocab).append("</li>\n");
            }
            buff.append("</ul>\n");
        }
        final List<DateType> dates = ds.getDates();
        if (dates.size() > 0) {
            buff.append("<h3>Dates:</h3>\n<ul>\n");
            for (final DateType d : dates) {
                final String type2 = (d.getType() == null) ? "" : (" <strong>(" + StringUtil.quoteHtmlContent(d.getType()) + ")</strong> ");
                buff.append(" <li>").append(StringUtil.quoteHtmlContent(d.getText())).append(type2).append("</li>\n");
            }
            buff.append("</ul>\n");
        }
        final List<ThreddsMetadata.Vocab> projects = ds.getProjects();
        if (projects.size() > 0) {
            buff.append("<h3>Projects:</h3>\n<ul>\n");
            for (final ThreddsMetadata.Vocab t3 : projects) {
                final String vocab2 = (t3.getVocabulary() == null) ? "" : (" <strong>(" + StringUtil.quoteHtmlContent(t3.getVocabulary()) + ")</strong> ");
                buff.append(" <li>").append(StringUtil.quoteHtmlContent(t3.getText())).append(vocab2).append("</li>\n");
            }
            buff.append("</ul>\n");
        }
        final List<ThreddsMetadata.Source> creators = ds.getCreators();
        if (creators.size() > 0) {
            buff.append("<h3>Creators:</h3>\n<ul>\n");
            for (final ThreddsMetadata.Source t4 : creators) {
                buff.append(" <li><strong>").append(StringUtil.quoteHtmlContent(t4.getName())).append("</strong><ul>\n");
                buff.append(" <li><em>email: </em>").append(StringUtil.quoteHtmlContent(t4.getEmail())).append("</li>\n");
                if (t4.getUrl() != null) {
                    final String newUrl = resolveRelativeUrls ? makeHrefResolve(ds, t4.getUrl(), null) : makeHref(t4.getUrl(), null);
                    buff.append(" <li> <em>").append(newUrl).append("</em></li>\n");
                }
                buff.append(" </ul></li>\n");
            }
            buff.append("</ul>\n");
        }
        final List<ThreddsMetadata.Source> publishers = ds.getPublishers();
        if (publishers.size() > 0) {
            buff.append("<h3>Publishers:</h3>\n<ul>\n");
            for (final ThreddsMetadata.Source t5 : publishers) {
                buff.append(" <li><strong>").append(StringUtil.quoteHtmlContent(t5.getName())).append("</strong><ul>\n");
                buff.append(" <li><em>email: </em>").append(StringUtil.quoteHtmlContent(t5.getEmail())).append("\n");
                if (t5.getUrl() != null) {
                    final String urlLink = resolveRelativeUrls ? makeHrefResolve(ds, t5.getUrl(), null) : makeHref(t5.getUrl(), null);
                    buff.append(" <li> <em>").append(urlLink).append("</em>\n");
                }
                buff.append(" </ul>\n");
            }
            buff.append("</ul>\n");
        }
        final List<ThreddsMetadata.Variables> vars = ds.getVariables();
        if (vars.size() > 0) {
            buff.append("<h3>Variables:</h3>\n<ul>\n");
            for (final ThreddsMetadata.Variables t6 : vars) {
                buff.append("<li><em>Vocabulary</em> [");
                if (t6.getVocabUri() != null) {
                    final URI uri = t6.getVocabUri();
                    final String vocabLink = resolveRelativeUrls ? makeHrefResolve(ds, uri.toString(), t6.getVocabulary()) : makeHref(uri.toString(), t6.getVocabulary());
                    buff.append(vocabLink);
                }
                else {
                    buff.append(StringUtil.quoteHtmlContent(t6.getVocabulary()));
                }
                buff.append("]:\n<ul>\n");
                final List<ThreddsMetadata.Variable> vlist = t6.getVariableList();
                if (vlist.size() > 0) {
                    for (final ThreddsMetadata.Variable v : vlist) {
                        buff.append(" <li><strong>").append(StringUtil.quoteHtmlContent(v.getName())).append("</strong> = ");
                        final String desc = (v.getDescription() == null) ? "" : (" <i>" + StringUtil.quoteHtmlContent(v.getDescription()) + "</i> = ");
                        buff.append(desc);
                        final String units = (v.getUnits() == null || v.getUnits().length() == 0) ? "" : (" (" + v.getUnits() + ") ");
                        buff.append(StringUtil.quoteHtmlContent(v.getVocabularyName() + units)).append("\n");
                    }
                    buff.append(" </ul>\n");
                }
                buff.append(" </ul>\n");
            }
            buff.append("</ul>\n");
        }
        final ThreddsMetadata.GeospatialCoverage gc = ds.getGeospatialCoverage();
        if (gc != null && !gc.isEmpty()) {
            buff.append("<h3>GeospatialCoverage:</h3>\n<ul>\n");
            if (gc.isGlobal()) {
                buff.append(" <li><em> Global </em></ul>\n");
            }
            else {
                buff.append(" <li><em> Longitude: </em> ").append(rangeString(gc.getEastWestRange())).append("</li>\n");
                buff.append(" <li><em> Latitude: </em> ").append(rangeString(gc.getNorthSouthRange())).append("</li>\n");
                if (gc.getUpDownRange() != null) {
                    buff.append(" <li><em> Altitude: </em> ").append(rangeString(gc.getUpDownRange())).append(" (positive is <strong>").append(StringUtil.quoteHtmlContent(gc.getZPositive())).append(")</strong></li>\n");
                }
                final List<ThreddsMetadata.Vocab> nlist = gc.getNames();
                if (nlist != null && nlist.size() > 0) {
                    buff.append(" <li><em>  Names: </em> <ul>\n");
                    for (final ThreddsMetadata.Vocab elem : nlist) {
                        buff.append(" <li>").append(StringUtil.quoteHtmlContent(elem.getText())).append("\n");
                    }
                    buff.append(" </ul>\n");
                }
                buff.append(" </ul>\n");
            }
        }
        final DateRange tc = ds.getTimeCoverage();
        if (tc != null) {
            buff.append("<h3>TimeCoverage:</h3>\n<ul>\n");
            final DateType start = tc.getStart();
            if (start != null && !start.isBlank()) {
                buff.append(" <li><em>  Start: </em> ").append(start.toDateTimeString()).append("\n");
            }
            final DateType end = tc.getEnd();
            if (end != null && !end.isBlank()) {
                buff.append(" <li><em>  End: </em> ").append(end.toDateTimeString()).append("\n");
            }
            final TimeDuration duration = tc.getDuration();
            if (duration != null && !duration.isBlank()) {
                buff.append(" <li><em>  Duration: </em> ").append(StringUtil.quoteHtmlContent(duration.toString())).append("\n");
            }
            final TimeDuration resolution = tc.getResolution();
            if (tc.useResolution() && resolution != null && !resolution.isBlank()) {
                buff.append(" <li><em>  Resolution: </em> ").append(StringUtil.quoteHtmlContent(resolution.toString())).append("\n");
            }
            buff.append(" </ul>\n");
        }
        final List<InvMetadata> metadata = ds.getMetadata();
        boolean gotSomeMetadata = false;
        for (final InvMetadata m : metadata) {
            if (m.hasXlink()) {
                gotSomeMetadata = true;
            }
        }
        if (gotSomeMetadata) {
            buff.append("<h3>Metadata:</h3>\n<ul>\n");
            for (final InvMetadata m : metadata) {
                final String type3 = (m.getMetadataType() == null) ? "" : m.getMetadataType();
                if (m.hasXlink()) {
                    final String title = (m.getXlinkTitle() == null) ? ("Type " + type3) : m.getXlinkTitle();
                    final String mdLink = resolveRelativeUrls ? makeHrefResolve(ds, m.getXlinkHref(), title) : makeHref(m.getXlinkHref(), title);
                    buff.append(" <li> ").append(mdLink).append("\n");
                }
            }
            buff.append("</ul>\n");
        }
        final List<InvProperty> props = ds.getProperties();
        if (props.size() > 0) {
            buff.append("<h3>Properties:</h3>\n<ul>\n");
            for (final InvProperty p : props) {
                if (p.getName().equals("attachments")) {
                    final String attachLink = resolveRelativeUrls ? makeHrefResolve(ds, p.getValue(), p.getName()) : makeHref(p.getValue(), p.getName());
                    buff.append(" <li>").append(attachLink).append("\n");
                }
                else {
                    buff.append(" <li>").append(StringUtil.quoteHtmlContent(p.getName() + " = \"" + p.getValue())).append("\"\n");
                }
            }
            buff.append("</ul>\n");
        }
        if (complete) {
            buff.append("</body></html>");
        }
    }
    
    private static String rangeString(final ThreddsMetadata.Range r) {
        if (r == null) {
            return "";
        }
        final String units = (r.getUnits() == null) ? "" : (" " + r.getUnits());
        final String resolution = r.hasResolution() ? (" Resolution=" + r.getResolution()) : "";
        return StringUtil.quoteHtmlContent(r.getStart() + " to " + (r.getStart() + r.getSize()) + resolution + units);
    }
    
    public static String resolve(final InvDataset ds, String href) {
        final InvCatalog cat = ds.getParentCatalog();
        if (cat != null) {
            try {
                final URI uri = cat.resolveUri(href);
                href = uri.toString();
            }
            catch (URISyntaxException e) {
                System.err.println("InvDatasetImpl.writeHtml: error parsing URL= " + href);
            }
        }
        return href;
    }
    
    private static String makeHref(final String href, String title) {
        if (title == null) {
            title = href;
        }
        return "<a href='" + StringUtil.quoteHtmlContent(href) + "'>" + StringUtil.quoteHtmlContent(title) + "</a>";
    }
    
    private static String makeHrefResolve(final InvDatasetImpl ds, String href, String title) {
        if (title == null) {
            title = href;
        }
        href = resolve(ds, href);
        return makeHref(href, title);
    }
    
    public String dump() {
        return this.dump(0);
    }
    
    String dump(final int n) {
        final StringBuilder buff = new StringBuilder(100);
        buff.append(indent(n));
        buff.append("Dataset name:<").append(this.getName());
        if (this.dataType != null) {
            buff.append("> dataType:<").append(this.dataType);
        }
        if (this.urlPath != null) {
            buff.append("> urlPath:<").append(this.urlPath);
        }
        if (this.defaultService != null) {
            buff.append("> defaultService <").append(this.defaultService);
        }
        buff.append("> uID:<").append(this.getUniqueID());
        buff.append(">\n");
        final List<InvService> svcs = this.getServicesLocal();
        if (svcs.size() > 0) {
            final String indent = indent(n + 2);
            buff.append(indent);
            buff.append("Services:\n");
            for (final InvService s : svcs) {
                buff.append(s.dump(n + 4));
            }
        }
        if (this.access.size() > 0) {
            final String indent = indent(n + 2);
            buff.append(indent);
            if (this.access.size() == 1) {
                buff.append("Access: ").append(this.access.get(0)).append("\n");
            }
            else if (this.access.size() > 1) {
                buff.append("Access:\n");
                for (final InvAccess a : this.access) {
                    buff.append(indent(n + 4)).append(a).append("\n");
                }
            }
        }
        buff.append(indent(n)).append("Thredds Metadata\n");
        buff.append(this.tm.dump(n + 4)).append("\n");
        buff.append(indent(n)).append("Thredds Metadata Inherited\n");
        buff.append(this.tmi.dump(n + 4)).append("\n");
        buff.append(indent(n)).append("Thredds Metadata Cat6\n");
        buff.append(this.tmi6.dump(n + 4)).append("\n");
        if (this.datasets.size() > 0) {
            final String indent = indent(n + 2);
            buff.append(indent);
            buff.append("Datasets:\n");
            for (final InvDataset ds : this.datasets) {
                final InvDatasetImpl dsi = (InvDatasetImpl)ds;
                buff.append(dsi.dump(n + 4));
            }
        }
        return buff.toString();
    }
    
    static String indent(final int n) {
        final StringBuilder blanks = new StringBuilder(n);
        for (int i = 0; i < n; ++i) {
            blanks.append(" ");
        }
        return blanks.toString();
    }
    
    boolean check(final StringBuilder out, final boolean show) {
        boolean isValid = true;
        if (this.log.length() > 0) {
            out.append((CharSequence)this.log);
        }
        for (final InvAccess acces : this.access) {
            final InvAccessImpl a = (InvAccessImpl)acces;
            isValid &= a.check(out, show);
        }
        for (final InvDataset dataset : this.datasets) {
            final InvDatasetImpl ds = (InvDatasetImpl)dataset;
            isValid &= ds.check(out, show);
        }
        for (final InvMetadata m : this.getMetadata()) {
            m.check(out);
        }
        for (final InvService s : this.getServicesLocal()) {
            isValid &= s.check(out);
        }
        if (this.hasAccess() && this.getDataType() == null) {
            out.append("**Warning: Dataset (").append(this.getFullName()).append("): is selectable but no data type declared in it or in a parent element\n");
        }
        if (!this.hasAccess() && !this.hasNestedDatasets()) {
            out.append("**Warning: Dataset (").append(this.getFullName()).append("): is not selectable and does not have nested datasets\n");
        }
        if (show) {
            System.out.println("  dataset " + this.name + " valid = " + isValid);
        }
        return isValid;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof InvDatasetImpl && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getServicesLocal().hashCode();
            result = 37 * result + this.getDatasets().hashCode();
            result = 37 * result + this.getAccessLocal().hashCode();
            result = 37 * result + (this.isHarvest() ? 1 : 0);
            if (null != this.getCollectionType()) {
                result = 37 * result + this.getCollectionType().hashCode();
            }
            result = 37 * result + this.getDocumentation().hashCode();
            result = 37 * result + this.getProperties().hashCode();
            result = 37 * result + this.getMetadata().hashCode();
            result = 37 * result + this.getCreators().hashCode();
            result = 37 * result + this.getContributors().hashCode();
            result = 37 * result + this.getDates().hashCode();
            result = 37 * result + this.getKeywords().hashCode();
            result = 37 * result + this.getProjects().hashCode();
            result = 37 * result + this.getPublishers().hashCode();
            result = 37 * result + this.getVariables().hashCode();
            if (null != this.getID()) {
                result = 37 * result + this.getID().hashCode();
            }
            if (null != this.getAlias()) {
                result = 37 * result + this.getAlias().hashCode();
            }
            if (null != this.getAuthority()) {
                result = 37 * result + this.getAuthority().hashCode();
            }
            if (null != this.getDataType()) {
                result = 37 * result + this.getDataType().hashCode();
            }
            if (null != this.getDataFormatType()) {
                result = 37 * result + this.getDataFormatType().hashCode();
            }
            if (null != this.getServiceDefault()) {
                result = 37 * result + this.getServiceDefault().hashCode();
            }
            if (null != this.getUrlPath()) {
                result = 37 * result + this.getUrlPath().hashCode();
            }
            if (null != this.getGeospatialCoverage()) {
                result = 37 * result + this.getGeospatialCoverage().hashCode();
            }
            if (null != this.getTimeCoverage()) {
                result = 37 * result + this.getTimeCoverage().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    public static void main(final String[] args) {
        final InvDatasetImpl topDs = new InvDatasetImpl(null, "topDs", FeatureType.valueOf("GRID"), "myService", "myUrlPath/");
        final InvService myS = new InvService("myService", ServiceType.DODS.toString(), "http://motherlode.ucar.edu/cgi-bin/dods/nph-dods", "", null);
        topDs.addService(myS);
        topDs.getLocalMetadata().setServiceName("myService");
        final InvDatasetImpl childDs = new InvDatasetImpl(null, "childDs", null, null, "myUrlPath/");
        topDs.addDataset(childDs);
        final InvService ts = childDs.findService("myService");
        System.out.println("InvDatasetImpl.main(): " + childDs.getAccess(ServiceType.DODS).toString());
    }
}
