// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import org.jdom.Document;

public interface InvCatalogConvertIF
{
    InvCatalogImpl parseXML(final InvCatalogFactory p0, final Document p1, final URI p2);
    
    void writeXML(final InvCatalogImpl p0, final OutputStream p1) throws IOException;
    
    void writeXML(final InvCatalogImpl p0, final OutputStream p1, final boolean p2) throws IOException;
}
