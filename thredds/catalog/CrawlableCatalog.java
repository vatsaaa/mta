// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import ucar.nc2.units.DateType;
import java.util.Date;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import java.io.IOException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import ucar.nc2.ncml.NcMLReader;
import org.jdom.Element;
import thredds.crawlabledataset.CrawlableDataset;

public class CrawlableCatalog implements CrawlableDataset
{
    private String catalogURL;
    private Object configObj;
    private ServiceType serviceType;
    private InvCatalogImpl catalog;
    private InvDatasetImpl dataset;
    private CrawlableCatalog parent;
    private boolean isCollection;
    
    public CrawlableCatalog(final String catalogURL, final Object configObj) {
        this.catalogURL = catalogURL;
        this.configObj = configObj;
        if (configObj instanceof Element) {
            final Element configElement = (Element)configObj;
            final Element serviceElement = configElement.getChild("serviceType", NcMLReader.ncNS);
            if (null != serviceElement) {
                final String service = serviceElement.getTextTrim();
                this.serviceType = ServiceType.getType(service);
            }
        }
        final InvCatalogFactory catFactory = InvCatalogFactory.getDefaultFactory(true);
        this.catalog = catFactory.readXML(catalogURL);
        this.dataset = (InvDatasetImpl)this.catalog.getDataset();
        this.isCollection = true;
    }
    
    CrawlableCatalog(final CrawlableCatalog parent, final InvDatasetImpl dataset) {
        this.parent = parent;
        this.dataset = dataset;
        this.serviceType = parent.serviceType;
        if (dataset instanceof InvCatalogRef) {
            this.isCollection = true;
        }
        else {
            this.isCollection = dataset.hasNestedDatasets();
        }
    }
    
    public Object getConfigObject() {
        return this.configObj;
    }
    
    public String getPath() {
        if (this.serviceType != null) {
            return this.dataset.getAccess(this.serviceType).getStandardUrlName();
        }
        return this.dataset.getCatalogUrl();
    }
    
    public String getName() {
        return this.dataset.getName();
    }
    
    public CrawlableDataset getParentDataset() {
        return this.parent;
    }
    
    public boolean exists() {
        return this.catalog != null;
    }
    
    public boolean isCollection() {
        return this.isCollection;
    }
    
    public CrawlableDataset getDescendant(final String relativePath) {
        return null;
    }
    
    public List<CrawlableDataset> listDatasets() throws IOException {
        final List<InvDataset> datasets = this.dataset.getDatasets();
        final List<CrawlableDataset> result = new ArrayList<CrawlableDataset>();
        for (final InvDataset d : datasets) {
            if (this.filter(d)) {
                result.add(new CrawlableCatalog(this, (InvDatasetImpl)d));
            }
        }
        return result;
    }
    
    public List<CrawlableDataset> listDatasets(final CrawlableDatasetFilter filter) throws IOException {
        final List<InvDataset> datasets = this.dataset.getDatasets();
        final List<CrawlableDataset> result = new ArrayList<CrawlableDataset>();
        for (final InvDataset d : datasets) {
            if (!this.filter(d)) {
                continue;
            }
            final CrawlableCatalog cc = new CrawlableCatalog(this, (InvDatasetImpl)d);
            if (!filter.accept(cc)) {
                continue;
            }
            result.add(cc);
        }
        return result;
    }
    
    private boolean filter(final InvDataset d) {
        return this.serviceType == null || d.getAccess(this.serviceType) != null;
    }
    
    public long length() {
        final double size = this.dataset.getDataSize();
        if (size == 0.0 || Double.isNaN(size)) {
            return 0L;
        }
        return (long)size;
    }
    
    public Date lastModified() {
        final DateType dt = this.dataset.getLastModifiedDate();
        return (dt == null) ? null : dt.getDate();
    }
}
