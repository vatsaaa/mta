// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

public class InvProperty
{
    private String name;
    private String value;
    
    public InvProperty() {
    }
    
    public InvProperty(final String name, final String value) {
        this.name = name;
        this.value = value;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setValue(final String value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return "<" + this.name + "> <" + this.value + ">";
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof InvProperty && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        return this.getName().hashCode();
    }
}
