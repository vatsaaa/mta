// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.util;

import java.util.Iterator;
import java.util.Collection;
import java.net.URISyntaxException;
import java.net.URI;
import thredds.catalog.InvDatasetFmrc;
import thredds.catalog.InvDatasetScan;
import thredds.catalog.InvDatasetImpl;
import java.util.ArrayList;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvDataset;
import java.util.List;

public class CatalogUtils
{
    private CatalogUtils() {
    }
    
    public static List<InvCatalogRef> findAllCatRefsInDatasetTree(final List<InvDataset> datasets, final StringBuilder log, final boolean onlyRelativeUrls) {
        final List<InvCatalogRef> catRefList = new ArrayList<InvCatalogRef>();
        for (final InvDataset invds : datasets) {
            final InvDatasetImpl curDs = (InvDatasetImpl)invds;
            if (!(curDs instanceof InvDatasetScan)) {
                if (curDs instanceof InvDatasetFmrc) {
                    continue;
                }
                if (curDs instanceof InvCatalogRef) {
                    final InvCatalogRef catRef = (InvCatalogRef)curDs;
                    final String name = catRef.getName();
                    final String href = catRef.getXlinkHref();
                    URI uri;
                    try {
                        uri = new URI(href);
                    }
                    catch (URISyntaxException e) {
                        log.append((log.length() > 0) ? "\n" : "").append("***WARN - CatalogRef [").append(name).append("] with bad HREF [").append(href).append("] ");
                        continue;
                    }
                    if (onlyRelativeUrls && uri.isAbsolute()) {
                        continue;
                    }
                    catRefList.add(catRef);
                }
                else {
                    if (!curDs.hasNestedDatasets()) {
                        continue;
                    }
                    catRefList.addAll(findAllCatRefsInDatasetTree(curDs.getDatasets(), log, onlyRelativeUrls));
                }
            }
        }
        return catRefList;
    }
}
