// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.util;

import org.slf4j.LoggerFactory;
import thredds.catalog.InvProperty;
import thredds.catalog.DataFormatType;
import thredds.catalog.InvAccessImpl;
import thredds.catalog.InvAccess;
import thredds.catalog.InvCatalogRef;
import java.net.URISyntaxException;
import java.net.URI;
import java.util.Iterator;
import thredds.catalog.InvService;
import java.util.List;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.InvDataset;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvCatalog;
import org.slf4j.Logger;

public class DeepCopyUtils
{
    private static Logger logger;
    
    private DeepCopyUtils() {
    }
    
    public static InvCatalog copyCatalog(final InvCatalog catalog) {
        if (catalog == null) {
            throw new IllegalArgumentException("Catalog may not be null.");
        }
        final InvCatalogImpl resultCatalog = new InvCatalogImpl(catalog.getName(), "1.0", catalog.getExpires(), ((InvCatalogImpl)catalog).getBaseURI());
        final List<InvService> copiedServices = copyServicesIntoCopiedCatalog(catalog, resultCatalog);
        for (final InvDataset curDs : catalog.getDatasets()) {
            resultCatalog.addDataset((InvDatasetImpl)copyDataset(curDs, copiedServices, false));
        }
        resultCatalog.finish();
        return resultCatalog;
    }
    
    private static List<InvService> copyServicesIntoCopiedCatalog(final InvCatalog catalog, final InvCatalogImpl resultCatalog) {
        final List<InvService> services = catalog.getServices();
        for (final InvService service : services) {
            resultCatalog.addService(copyService(service));
        }
        final List<InvService> copiedServices = resultCatalog.getServices();
        return copiedServices;
    }
    
    public static InvCatalog subsetCatalogOnDataset(final InvCatalog catalog, final String datasetId) {
        if (catalog == null) {
            throw new IllegalArgumentException("Catalog may not be null.");
        }
        if (datasetId == null) {
            throw new IllegalArgumentException("Dataset ID may not be null.");
        }
        final InvDataset ds = catalog.findDatasetByID(datasetId);
        if (ds == null) {
            throw new IllegalArgumentException("The dataset ID [" + datasetId + "] does not match the ID of a dataset in the catalog.");
        }
        return subsetCatalogOnDataset(catalog, ds);
    }
    
    public static InvCatalog subsetCatalogOnDataset(final InvCatalog catalog, final InvDataset dataset) {
        if (catalog == null) {
            throw new IllegalArgumentException("Catalog may not be null.");
        }
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset may not be null.");
        }
        if (dataset.getParentCatalog() != catalog) {
            throw new IllegalArgumentException("Catalog must contain the dataset.");
        }
        final URI docBaseUri = formDocBaseUriForSubsetCatalog(catalog, dataset);
        final InvCatalogImpl resultCatalog = new InvCatalogImpl(dataset.getName(), "1.0", docBaseUri);
        final List<InvService> copiedServices = copyServicesIntoCopiedCatalog(catalog, resultCatalog);
        final InvDataset topDs = copyDataset(dataset, copiedServices, true);
        resultCatalog.addDataset((InvDatasetImpl)topDs);
        resultCatalog.finish();
        return resultCatalog;
    }
    
    private static URI formDocBaseUriForSubsetCatalog(final InvCatalog catalog, final InvDataset dataset) {
        final String catDocBaseUri = catalog.getUriString();
        final String subsetDocBaseUriString = catDocBaseUri + "/" + ((dataset.getID() != null) ? dataset.getID() : dataset.getName());
        final URI thisDocBaseUri = ((InvCatalogImpl)catalog).getBaseURI();
        try {
            final URI subsetDocBaseUri = new URI(subsetDocBaseUriString);
            return subsetDocBaseUri;
        }
        catch (URISyntaxException e) {
            throw new IllegalStateException("Bad document Base URI for new catalog [" + catalog.getUriString() + "/" + ((dataset.getID() != null) ? dataset.getID() : dataset.getName()) + "].", e);
        }
    }
    
    public static InvDataset copyDataset(final InvDataset dataset, final List<InvService> availableServices, final boolean copyInheritedMetadataFromParents) {
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset may not be null.");
        }
        if (availableServices == null) {
            throw new IllegalArgumentException("List of available services may not be null.");
        }
        InvDatasetImpl resultDs = null;
        if (dataset instanceof InvCatalogRef) {
            final InvCatalogRef catRef = (InvCatalogRef)dataset;
            resultDs = new InvCatalogRef(null, catRef.getName(), catRef.getXlinkHref());
        }
        else {
            resultDs = new InvDatasetImpl(null, dataset.getName());
        }
        resultDs.setID(dataset.getID());
        resultDs.transferMetadata((InvDatasetImpl)dataset, copyInheritedMetadataFromParents);
        if (!(dataset instanceof InvCatalogRef)) {
            final String urlPath = ((InvDatasetImpl)dataset).getUrlPath();
            if (urlPath != null) {
                resultDs.setUrlPath(urlPath);
            }
            else {
                for (final InvAccess curAccess : dataset.getAccess()) {
                    final InvAccess access = copyAccess(curAccess, resultDs, availableServices);
                    if (access != null) {
                        resultDs.addAccess(access);
                    }
                }
            }
        }
        if (!(dataset instanceof InvCatalogRef)) {
            for (final InvDataset curDs : dataset.getDatasets()) {
                final InvDatasetImpl curDsCopy = (InvDatasetImpl)copyDataset(curDs, availableServices, false);
                curDsCopy.setParent(resultDs);
                resultDs.addDataset(curDsCopy);
            }
        }
        return resultDs;
    }
    
    public static InvAccess copyAccess(final InvAccess access, final InvDataset parentDataset, final List<InvService> availableServices) {
        if (parentDataset == null) {
            throw new IllegalArgumentException("Parent dataset may not be null.");
        }
        final String serviceName = access.getService().getName();
        final InvService service = findServiceByName(serviceName, availableServices);
        if (service == null) {
            DeepCopyUtils.logger.warn("Access service [" + serviceName + "] not in available service list.");
            return null;
        }
        final DataFormatType dataFormatType = access.getDataFormatType();
        InvAccessImpl resultAccess = null;
        if (dataFormatType == null) {
            resultAccess = new InvAccessImpl(parentDataset, access.getUrlPath(), service);
            resultAccess.setSize(access.getDataSize());
        }
        else {
            resultAccess = new InvAccessImpl(parentDataset, access.getUrlPath(), service.getName(), null, dataFormatType.toString(), access.getDataSize());
        }
        return resultAccess;
    }
    
    static InvService findServiceByName(final String name, final List<InvService> servicePool) {
        if (servicePool == null) {
            return null;
        }
        for (final InvService curService : servicePool) {
            if (curService.getName().equals(name)) {
                return curService;
            }
            final List<InvService> nestedServices = curService.getServices();
            final InvService target = (nestedServices != null) ? findServiceByName(name, nestedServices) : null;
            if (target != null) {
                return target;
            }
        }
        return null;
    }
    
    public static InvService copyService(final InvService service) {
        if (service == null) {
            throw new IllegalArgumentException("Service may not be null.");
        }
        final InvService resultService = new InvService(service.getName(), service.getServiceType().toString(), service.getBase(), service.getSuffix(), service.getDescription());
        for (final InvService curService : service.getServices()) {
            resultService.addService(copyService(curService));
        }
        for (final InvProperty curProperty : service.getProperties()) {
            resultService.addProperty(copyProperty(curProperty));
        }
        for (final InvProperty curDatasetRoot : service.getDatasetRoots()) {
            resultService.addDatasetRoot(copyProperty(curDatasetRoot));
        }
        return resultService;
    }
    
    public static InvProperty copyProperty(final InvProperty property) {
        return new InvProperty(property.getName(), property.getValue());
    }
    
    static {
        DeepCopyUtils.logger = LoggerFactory.getLogger(DeepCopyUtils.class);
    }
}
