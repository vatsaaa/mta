// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.util.List;

public final class MetadataType
{
    private static List<MetadataType> members;
    public static final MetadataType NONE;
    public static final MetadataType THREDDS;
    public static final MetadataType ADN;
    public static final MetadataType AGGREGATION;
    public static final MetadataType CATALOG_GEN_CONFIG;
    public static final MetadataType DUBLIN_CORE;
    public static final MetadataType DIF;
    public static final MetadataType FGDC;
    public static final MetadataType LAS;
    public static final MetadataType ESG;
    public static final MetadataType NETCDF;
    public static final MetadataType NcML;
    public static final MetadataType THREDDS_DLEntry;
    public static final MetadataType THREDDS_DLCollection;
    private String name;
    
    private MetadataType(final String s) {
        this.name = s;
        MetadataType.members.add(this);
    }
    
    private MetadataType(final String name, final boolean fake) {
        this.name = name;
    }
    
    public static Collection<MetadataType> getAllTypes() {
        return MetadataType.members;
    }
    
    public static MetadataType findType(final String name) {
        if (name == null) {
            return null;
        }
        for (final MetadataType m : MetadataType.members) {
            if (m.name.equalsIgnoreCase(name)) {
                return m;
            }
        }
        return null;
    }
    
    public static MetadataType getType(final String name) {
        if (name == null) {
            return null;
        }
        final MetadataType type = findType(name);
        return (type != null) ? type : new MetadataType(name, false);
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof MetadataType && o.hashCode() == this.hashCode());
    }
    
    static {
        MetadataType.members = new ArrayList<MetadataType>(20);
        NONE = new MetadataType("");
        THREDDS = new MetadataType("THREDDS");
        ADN = new MetadataType("ADN");
        AGGREGATION = new MetadataType("Aggregation");
        CATALOG_GEN_CONFIG = new MetadataType("CatalogGenConfig");
        DUBLIN_CORE = new MetadataType("DublinCore");
        DIF = new MetadataType("DIF");
        FGDC = new MetadataType("FGDC");
        LAS = new MetadataType("LAS");
        ESG = new MetadataType("ESG");
        NETCDF = new MetadataType("NetCDF");
        NcML = new MetadataType("NcML");
        THREDDS_DLEntry = new MetadataType("THREDDS_DLEntry");
        THREDDS_DLCollection = new MetadataType("THREDDS_DLCollection");
    }
}
