// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.io.ByteArrayOutputStream;
import java.net.URISyntaxException;
import java.net.URI;

public class InvDocumentation
{
    private String href;
    private String title;
    private String type;
    private String inlineContent;
    private URI uri;
    private String content;
    private volatile int hashCode;
    
    public InvDocumentation(final String href, final URI uri, final String title, final String type, final String inlineContent) {
        this.content = null;
        this.hashCode = 0;
        this.href = href;
        this.uri = uri;
        this.title = title;
        this.type = type;
        this.inlineContent = inlineContent;
        if (uri != null && title == null) {
            this.title = uri.toString();
        }
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
        this.hashCode = 0;
    }
    
    public boolean hasXlink() {
        return this.uri != null;
    }
    
    public URI getURI() {
        return this.uri;
    }
    
    public String getXlinkTitle() {
        return this.title;
    }
    
    public void setXlinkTitle(final String title) {
        this.title = title;
    }
    
    public String getXlinkHref() {
        return this.href;
    }
    
    public void setXlinkHref(final String href) throws URISyntaxException {
        this.href = href;
        this.uri = new URI(href);
    }
    
    public String getXlinkContent() throws IOException {
        if (this.content != null) {
            return this.content;
        }
        if (this.uri == null) {
            return "";
        }
        final URL url = this.uri.toURL();
        final InputStream is = url.openStream();
        final ByteArrayOutputStream os = new ByteArrayOutputStream(is.available());
        final byte[] buffer = new byte[1024];
        while (true) {
            final int bytesRead = is.read(buffer);
            if (bytesRead == -1) {
                break;
            }
            os.write(buffer, 0, bytesRead);
        }
        is.close();
        return this.content = os.toString();
    }
    
    public String getInlineContent() {
        return this.inlineContent;
    }
    
    public void setInlineContent(final String s) {
        this.inlineContent = s;
        this.hashCode = 0;
    }
    
    @Override
    public String toString() {
        if (this.hasXlink()) {
            return "<" + this.uri + "> <" + this.title + "> <" + this.type + ">" + ((this.content == null) ? "" : (" <" + this.content + ">"));
        }
        return "<" + this.inlineContent + ">";
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof InvDocumentation && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (null != this.getURI()) {
                result = 37 * result + this.getURI().hashCode();
            }
            if (null != this.getInlineContent()) {
                result = 37 * result + this.getInlineContent().hashCode();
            }
            if (null != this.getXlinkTitle()) {
                result = 37 * result + this.getXlinkTitle().hashCode();
            }
            if (null != this.getType()) {
                result = 37 * result + this.getType().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    public InvDocumentation() {
        this.content = null;
        this.hashCode = 0;
    }
    
    public static String hiddenProperties() {
        return "inlineContent type URI xlinkContent";
    }
    
    public static String editableProperties() {
        return "xlinkTitle xlinkHref";
    }
}
