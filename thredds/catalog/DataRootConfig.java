// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

public class DataRootConfig extends InvProperty
{
    private boolean nocache;
    
    public DataRootConfig(final String name, final String value, final String cacheS) {
        super(name, value);
        this.nocache = false;
        this.nocache = (cacheS != null && cacheS.equalsIgnoreCase("false"));
    }
    
    public boolean isCache() {
        return !this.nocache;
    }
}
