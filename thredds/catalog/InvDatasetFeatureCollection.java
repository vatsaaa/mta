// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.util.regex.Matcher;
import thredds.crawlabledataset.CrawlableDataset;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileNotFoundException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.util.cache.FileFactory;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.units.DateRange;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.thredds.MetadataExtractor;
import java.util.Collections;
import ucar.unidata.util.StringUtil;
import java.util.Date;
import ucar.nc2.units.DateFormatter;
import java.util.ArrayList;
import java.net.URISyntaxException;
import java.net.URI;
import java.util.List;
import java.io.IOException;
import java.util.Iterator;
import thredds.inventory.CollectionSpecParser;
import thredds.inventory.CollectionManager;
import thredds.inventory.DatasetCollectionManager;
import java.util.Formatter;
import net.jcip.annotations.GuardedBy;
import java.util.regex.Pattern;
import java.util.Set;
import ucar.nc2.ft.fmrc.Fmrc;
import thredds.inventory.FeatureCollectionConfig;
import ucar.nc2.constants.FeatureType;
import org.slf4j.Logger;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class InvDatasetFeatureCollection extends InvCatalogRef
{
    private static final Logger logger;
    private static final String FMRC = "fmrc.ncd";
    private static final String BEST = "best.ncd";
    private static final String SCAN = "files";
    private static final String RUNS = "runs";
    private static final String RUN_NAME = "RUN_";
    private static final String RUN_TITLE = "Forecast Model Run";
    private static final String FORECAST = "forecast";
    private static final String FORECAST_NAME = "ConstantForecast_";
    private static final String FORECAST_TITLE = "Constant Forecast Date";
    private static final String OFFSET = "offset";
    private static final String OFFSET_NAME = "Offset_";
    private static final String OFFSET_TITLE = "Constant Forecast Offset";
    private static final String Virtual_Services = "VirtualServices";
    private final String path;
    private final FeatureType featureType;
    private final FeatureCollectionConfig.Config config;
    private final Fmrc fmrc;
    private final Set<FeatureCollectionConfig.FmrcDatasetType> wantDatasets;
    private final String topDirectory;
    private final Pattern filter;
    private InvService orgService;
    private InvService virtualService;
    @GuardedBy("lock")
    private State state;
    private Object lock;
    
    public InvDatasetFeatureCollection(final InvDatasetImpl parent, final String name, final String path, final String featureType, final FeatureCollectionConfig.Config config) {
        super(parent, name, "/thredds/catalog/" + path + "/catalog.xml");
        this.lock = new Object();
        this.path = path;
        this.featureType = FeatureType.getType(featureType);
        if (featureType.equalsIgnoreCase("FMRC")) {
            this.getLocalMetadataInheritable().setDataType(FeatureType.GRID);
        }
        this.config = config;
        this.wantDatasets = config.fmrcConfig.datasets;
        final Formatter errlog = new Formatter();
        try {
            this.fmrc = Fmrc.open(config, errlog);
        }
        catch (Exception e) {
            throw new RuntimeException(errlog.toString());
        }
        final CollectionManager cm = this.fmrc.getManager();
        if (cm instanceof DatasetCollectionManager) {
            final CollectionSpecParser sp = ((DatasetCollectionManager)cm).getCollectionSpecParser();
            this.topDirectory = sp.getTopDir();
            this.filter = sp.getFilter();
        }
        else {
            this.topDirectory = null;
            this.filter = null;
        }
    }
    
    private InvService makeVirtualService(final InvService org) {
        if (org.getServiceType() != ServiceType.COMPOUND) {
            return org;
        }
        final InvService result = new InvService("VirtualServices", ServiceType.COMPOUND.toString(), null, null, null);
        for (final InvService service : org.getServices()) {
            if (service.getServiceType() != ServiceType.HTTPServer) {
                result.addService(service);
            }
        }
        return result;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public String getTopDirectoryLocation() {
        return this.topDirectory;
    }
    
    public FeatureCollectionConfig.Config getConfig() {
        return this.config;
    }
    
    public InvDatasetScan getRawFileScan() {
        try {
            this.checkState();
        }
        catch (IOException e) {
            InvDatasetFeatureCollection.logger.error("Error in checkState", e);
        }
        return this.state.scan;
    }
    
    @Override
    public List<InvDataset> getDatasets() {
        try {
            this.checkState();
        }
        catch (Exception e) {
            InvDatasetFeatureCollection.logger.error("Error in checkState", e);
        }
        return this.state.datasets;
    }
    
    public void triggerRescan() throws IOException {
        this.fmrc.triggerRescan();
    }
    
    public void triggerProto() throws IOException {
        this.fmrc.triggerProto();
    }
    
    public InvCatalogImpl makeCatalog(final String match, final String orgPath, final URI baseURI) {
        InvDatasetFeatureCollection.logger.debug("FMRC make catalog for " + match + " " + baseURI);
        State localState = null;
        try {
            localState = this.checkState();
        }
        catch (IOException e) {
            InvDatasetFeatureCollection.logger.error("Error in checkState", e);
            return null;
        }
        try {
            if (match == null || match.length() == 0) {
                return this.makeCatalogTop(baseURI, localState);
            }
            if (match.equals("runs") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.Runs)) {
                return this.makeCatalogRuns(baseURI, localState);
            }
            if (match.equals("offset") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.ConstantOffsets)) {
                return this.makeCatalogOffsets(baseURI, localState);
            }
            if (match.equals("forecast") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.ConstantForecasts)) {
                return this.makeCatalogForecasts(baseURI, localState);
            }
            if (match.startsWith("files") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.Files)) {
                return localState.scan.makeCatalogForDirectory(orgPath, baseURI);
            }
        }
        catch (Exception e2) {
            InvDatasetFeatureCollection.logger.error("Error making catalog for " + this.path, e2);
        }
        return null;
    }
    
    private InvCatalogImpl makeCatalogTop(final URI baseURI, final State localState) throws IOException, URISyntaxException {
        final InvCatalogImpl parentCatalog = (InvCatalogImpl)this.getParentCatalog();
        final URI myURI = baseURI.resolve(this.getXlinkHref());
        final InvCatalogImpl mainCatalog = new InvCatalogImpl(this.getName(), parentCatalog.getVersion(), myURI);
        final InvDatasetImpl top = new InvDatasetImpl(this);
        top.setParent(null);
        final InvDatasetImpl parent = (InvDatasetImpl)this.getParent();
        if (parent != null) {
            top.transferMetadata(parent, true);
        }
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        top.setID(id);
        final ThreddsMetadata tmi = top.getLocalMetadataInheritable();
        if (localState.vars != null) {
            tmi.addVariables(localState.vars);
        }
        if (localState.gc != null) {
            tmi.setGeospatialCoverage(localState.gc);
        }
        if (localState.dateRange != null) {
            tmi.setTimeCoverage(localState.dateRange);
        }
        mainCatalog.addDataset(top);
        mainCatalog.addService(this.virtualService);
        top.getLocalMetadataInheritable().setServiceName(this.virtualService.getName());
        for (final InvDataset ds : this.getDatasets()) {
            top.addDataset((InvDatasetImpl)ds);
        }
        mainCatalog.finish();
        return mainCatalog;
    }
    
    private InvCatalogImpl makeCatalogRuns(final URI baseURI, final State localState) throws IOException {
        final InvCatalogImpl parent = (InvCatalogImpl)this.getParentCatalog();
        final URI myURI = baseURI.resolve(this.getCatalogHref("runs"));
        final InvCatalogImpl runCatalog = new InvCatalogImpl(this.getFullName(), parent.getVersion(), myURI);
        final InvDatasetImpl top = new InvDatasetImpl(this);
        top.setParent(null);
        top.transferMetadata((InvDatasetImpl)this.getParent(), true);
        top.setName("Forecast Model Run");
        final ThreddsMetadata tmi = top.getLocalMetadataInheritable();
        if (localState.vars != null) {
            tmi.addVariables(localState.vars);
        }
        if (localState.gc != null) {
            tmi.setGeospatialCoverage(localState.gc);
        }
        if (localState.dateRange != null) {
            tmi.setTimeCoverage(localState.dateRange);
        }
        runCatalog.addDataset(top);
        runCatalog.addService(this.virtualService);
        top.getLocalMetadataInheritable().setServiceName(this.virtualService.getName());
        for (final InvDatasetImpl ds : this.makeRunDatasets()) {
            top.addDataset(ds);
        }
        runCatalog.finish();
        return runCatalog;
    }
    
    private InvCatalogImpl makeCatalogOffsets(final URI baseURI, final State localState) throws IOException {
        final InvCatalogImpl parent = (InvCatalogImpl)this.getParentCatalog();
        final URI myURI = baseURI.resolve(this.getCatalogHref("offset"));
        final InvCatalogImpl offCatalog = new InvCatalogImpl(this.getFullName(), parent.getVersion(), myURI);
        final InvDatasetImpl top = new InvDatasetImpl(this);
        top.setParent(null);
        top.transferMetadata((InvDatasetImpl)this.getParent(), true);
        final ThreddsMetadata tmi = top.getLocalMetadataInheritable();
        if (localState.vars != null) {
            tmi.addVariables(localState.vars);
        }
        if (localState.gc != null) {
            tmi.setGeospatialCoverage(localState.gc);
        }
        if (localState.dateRange != null) {
            tmi.setTimeCoverage(localState.dateRange);
        }
        top.setName("Constant Forecast Offset");
        offCatalog.addDataset(top);
        offCatalog.addService(this.virtualService);
        top.getLocalMetadataInheritable().setServiceName(this.virtualService.getName());
        for (final InvDatasetImpl ds : this.makeOffsetDatasets()) {
            top.addDataset(ds);
        }
        offCatalog.finish();
        return offCatalog;
    }
    
    private InvCatalogImpl makeCatalogForecasts(final URI baseURI, final State localState) throws IOException {
        final InvCatalogImpl parent = (InvCatalogImpl)this.getParentCatalog();
        final URI myURI = baseURI.resolve(this.getCatalogHref("forecast"));
        final InvCatalogImpl foreCatalog = new InvCatalogImpl(this.getFullName(), parent.getVersion(), myURI);
        final InvDatasetImpl top = new InvDatasetImpl(this);
        top.setParent(null);
        top.transferMetadata((InvDatasetImpl)this.getParent(), true);
        top.setName("Constant Forecast Date");
        final ThreddsMetadata tmi = top.getLocalMetadataInheritable();
        if (localState.vars != null) {
            tmi.addVariables(localState.vars);
        }
        if (localState.gc != null) {
            tmi.setGeospatialCoverage(localState.gc);
        }
        if (localState.dateRange != null) {
            tmi.setTimeCoverage(localState.dateRange);
        }
        foreCatalog.addDataset(top);
        foreCatalog.addService(this.virtualService);
        top.getLocalMetadataInheritable().setServiceName(this.virtualService.getName());
        for (final InvDatasetImpl ds : this.makeForecastDatasets()) {
            top.addDataset(ds);
        }
        foreCatalog.finish();
        return foreCatalog;
    }
    
    private List<InvDatasetImpl> makeRunDatasets() throws IOException {
        final List<InvDatasetImpl> datasets = new ArrayList<InvDatasetImpl>();
        final DateFormatter formatter = new DateFormatter();
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        for (final Date runDate : this.fmrc.getRunDates()) {
            String name = this.getName() + "_" + "RUN_" + formatter.toDateTimeStringISO(runDate);
            name = StringUtil.replace(name, ' ', "_");
            final InvDatasetImpl nested = new InvDatasetImpl(this, name);
            nested.setUrlPath(this.path + "/" + "runs" + "/" + name);
            nested.setID(id + "/" + "runs" + "/" + name);
            final ThreddsMetadata tm = nested.getLocalMetadata();
            tm.addDocumentation("summary", "Data from Run " + name);
            datasets.add(nested);
        }
        Collections.reverse(datasets);
        return datasets;
    }
    
    private List<InvDatasetImpl> makeOffsetDatasets() throws IOException {
        final List<InvDatasetImpl> datasets = new ArrayList<InvDatasetImpl>();
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        for (final double offset : this.fmrc.getForecastOffsets()) {
            String name = this.getName() + "_" + "Offset_" + offset + "hr";
            name = StringUtil.replace(name, ' ', "_");
            final InvDatasetImpl nested = new InvDatasetImpl(this, name);
            nested.setUrlPath(this.path + "/" + "offset" + "/" + name);
            nested.setID(id + "/" + "offset" + "/" + name);
            final ThreddsMetadata tm = nested.getLocalMetadata();
            tm.addDocumentation("summary", "Data from the " + offset + " hour forecasts, across different model runs.");
            datasets.add(nested);
        }
        return datasets;
    }
    
    private List<InvDatasetImpl> makeForecastDatasets() throws IOException {
        final List<InvDatasetImpl> datasets = new ArrayList<InvDatasetImpl>();
        final DateFormatter formatter = new DateFormatter();
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        for (final Date forecastDate : this.fmrc.getForecastDates()) {
            String name = this.getName() + "_" + "ConstantForecast_" + formatter.toDateTimeStringISO(forecastDate);
            name = StringUtil.replace(name, ' ', "_");
            final InvDatasetImpl nested = new InvDatasetImpl(this, name);
            nested.setUrlPath(this.path + "/" + "forecast" + "/" + name);
            nested.setID(id + "/" + "forecast" + "/" + name);
            final ThreddsMetadata tm = nested.getLocalMetadata();
            tm.addDocumentation("summary", "Data with the same forecast date, " + name + ", across different model runs.");
            datasets.add(nested);
        }
        return datasets;
    }
    
    private State checkState() throws IOException {
        synchronized (this.lock) {
            boolean checkInv = true;
            boolean checkProto = true;
            if (this.state == null) {
                this.orgService = this.getServiceDefault();
                this.virtualService = this.makeVirtualService(this.orgService);
            }
            else {
                checkInv = this.fmrc.checkInvState(this.state.lastInvChange);
                checkProto = this.fmrc.checkProtoState(this.state.lastProtoChange);
                if (!checkInv && !checkProto) {
                    return this.state;
                }
            }
            final State localState = new State(this.state);
            if (checkProto) {
                final GridDataset gds = this.getGridDataset("fmrc.ncd");
                if (null != gds) {
                    localState.vars = MetadataExtractor.extractVariables(this, gds);
                    localState.gc = MetadataExtractor.extractGeospatial(gds);
                    localState.dateRange = MetadataExtractor.extractDateRange(gds);
                }
                localState.lastProtoChange = new Date();
            }
            if (checkInv) {
                this.makeDatasets(localState);
                localState.lastInvChange = new Date();
            }
            return this.state = localState;
        }
    }
    
    private void makeDatasets(final State localState) {
        final List<InvDataset> datasets = new ArrayList<InvDataset>();
        String id = this.getID();
        if (id == null) {
            id = this.getPath();
        }
        if (this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.TwoD)) {
            final InvDatasetImpl ds = new InvDatasetImpl(this, "Forecast Model Run Collection (2D time coordinates)");
            String name = this.getName() + "_" + "fmrc.ncd";
            name = StringUtil.replace(name, ' ', "_");
            ds.setUrlPath(this.path + "/" + name);
            ds.setID(id + "/" + name);
            final ThreddsMetadata tm = ds.getLocalMetadata();
            tm.addDocumentation("summary", "Forecast Model Run Collection (2D time coordinates).");
            ds.getLocalMetadataInheritable().setServiceName(this.virtualService.getName());
            ds.finish();
            datasets.add(ds);
        }
        if (this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.Best)) {
            final InvDatasetImpl ds = new InvDatasetImpl(this, "Best Time Series");
            String name = this.getName() + "_" + "best.ncd";
            name = StringUtil.replace(name, ' ', "_");
            ds.setUrlPath(this.path + "/" + name);
            ds.setID(id + "/" + name);
            final ThreddsMetadata tm = ds.getLocalMetadata();
            tm.addDocumentation("summary", "Best time series, taking the data from the most recent run available.");
            ds.getLocalMetadataInheritable().setServiceName(this.virtualService.getName());
            ds.finish();
            datasets.add(ds);
        }
        if (this.config.fmrcConfig.getBestDatasets() != null) {
            for (final FeatureCollectionConfig.BestDataset bd : this.config.fmrcConfig.getBestDatasets()) {
                final InvDatasetImpl ds2 = new InvDatasetImpl(this, bd.name);
                String name2 = this.getName() + "_" + bd.name;
                name2 = StringUtil.replace(name2, ' ', "_");
                ds2.setUrlPath(this.path + "/" + name2);
                ds2.setID(id + "/" + name2);
                final ThreddsMetadata tm2 = ds2.getLocalMetadata();
                tm2.addDocumentation("summary", "Best time series, excluding offset hours less than " + bd.greaterThan);
                ds2.getLocalMetadataInheritable().setServiceName(this.virtualService.getName());
                ds2.finish();
                datasets.add(ds2);
            }
        }
        if (this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.Runs)) {
            final InvDatasetImpl ds = new InvCatalogRef(this, "Forecast Model Run", this.getCatalogHref("runs"));
            ds.finish();
            datasets.add(ds);
        }
        if (this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.ConstantForecasts)) {
            final InvDatasetImpl ds = new InvCatalogRef(this, "Constant Forecast Date", this.getCatalogHref("forecast"));
            ds.finish();
            datasets.add(ds);
        }
        if (this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.ConstantOffsets)) {
            final InvDatasetImpl ds = new InvCatalogRef(this, "Constant Forecast Offset", this.getCatalogHref("offset"));
            ds.finish();
            datasets.add(ds);
        }
        if (this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.Files) && this.topDirectory != null) {
            final long olderThan = (long)(1000.0 * this.fmrc.getOlderThanFilterInSecs());
            final ScanFilter scanFilter = new ScanFilter(this.filter, olderThan);
            final InvDatasetScan scanDataset = new InvDatasetScan((InvCatalogImpl)this.getParentCatalog(), this, "File_Access", this.path + "/" + "files", this.topDirectory, scanFilter, true, "true", false, null, null, null);
            scanDataset.addService(this.orgService);
            final ThreddsMetadata tmi = scanDataset.getLocalMetadataInheritable();
            tmi.setServiceName(this.orgService.getName());
            tmi.addDocumentation("summary", "Individual data file, which comprise the Forecast Model Run Collection.");
            tmi.setGeospatialCoverage(null);
            tmi.setTimeCoverage(null);
            scanDataset.setServiceName(this.orgService.getName());
            scanDataset.finish();
            datasets.add(scanDataset);
            localState.scan = scanDataset;
        }
        localState.datasets = datasets;
        this.datasets = datasets;
        this.finish();
    }
    
    private String getCatalogHref(final String what) {
        return "/thredds/catalog/" + this.path + "/" + what + "/catalog.xml";
    }
    
    public NetcdfDataset getNetcdfDataset(final String matchPath) throws IOException {
        final int pos = matchPath.indexOf("/");
        final String type = (pos > -1) ? matchPath.substring(0, pos) : matchPath;
        final String name = (pos > -1) ? matchPath.substring(pos + 1) : "";
        if (!type.equals("files")) {
            final GridDataset gds = this.getGridDataset(matchPath);
            return (gds == null) ? null : ((NetcdfDataset)gds.getNetcdfFile());
        }
        if (this.topDirectory == null) {
            return null;
        }
        final String filename = this.topDirectory + (this.topDirectory.endsWith("/") ? "" : "/") + name;
        return NetcdfDataset.acquireDataset(null, filename, null, -1, null, null);
    }
    
    public GridDataset getGridDataset(final String matchPath) throws IOException {
        final int pos = matchPath.indexOf("/");
        final String wantType = (pos > -1) ? matchPath.substring(0, pos) : matchPath;
        final String wantName = (pos > -1) ? matchPath.substring(pos + 1) : matchPath;
        final String hasName = StringUtil.replace(this.name, ' ', "_") + "_";
        try {
            if (wantType.equals("files")) {
                final NetcdfDataset ncd = this.getNetcdfDataset(matchPath);
                return (ncd == null) ? null : new ucar.nc2.dt.grid.GridDataset(ncd);
            }
            if (wantName.equals(hasName + "fmrc.ncd") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.TwoD)) {
                return this.fmrc.getDataset2D(null);
            }
            if (wantName.equals(hasName + "best.ncd") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.Best)) {
                return this.fmrc.getDatasetBest();
            }
            if (wantType.equals("offset") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.ConstantOffsets)) {
                final int pos2 = wantName.indexOf("Offset_");
                final int pos3 = wantName.indexOf("hr");
                if (pos2 < 0 || pos3 < 0) {
                    return null;
                }
                final String id = wantName.substring(pos2 + "Offset_".length(), pos3);
                final double hour = Double.parseDouble(id);
                return this.fmrc.getConstantOffsetDataset(hour);
            }
            else if (wantType.equals("runs") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.Runs)) {
                final int pos2 = wantName.indexOf("RUN_");
                if (pos2 < 0) {
                    return null;
                }
                final String id2 = wantName.substring(pos2 + "RUN_".length());
                final DateFormatter formatter = new DateFormatter();
                final Date date = formatter.getISODate(id2);
                return this.fmrc.getRunTimeDataset(date);
            }
            else if (wantType.equals("forecast") && this.wantDatasets.contains(FeatureCollectionConfig.FmrcDatasetType.ConstantForecasts)) {
                final int pos2 = wantName.indexOf("ConstantForecast_");
                if (pos2 < 0) {
                    return null;
                }
                final String id2 = wantName.substring(pos2 + "ConstantForecast_".length());
                final DateFormatter formatter = new DateFormatter();
                final Date date = formatter.getISODate(id2);
                return this.fmrc.getConstantForecastDataset(date);
            }
            else if (this.config.fmrcConfig.getBestDatasets() != null) {
                for (final FeatureCollectionConfig.BestDataset bd : this.config.fmrcConfig.getBestDatasets()) {
                    if (wantName.endsWith(bd.name)) {
                        return this.fmrc.getDatasetBest(bd);
                    }
                }
            }
        }
        catch (FileNotFoundException e) {
            return null;
        }
        return null;
    }
    
    public File getFile(final String remaining) {
        if (null == this.topDirectory) {
            return null;
        }
        final int pos = remaining.indexOf("files");
        final StringBuilder fname = new StringBuilder(this.topDirectory);
        if (!this.topDirectory.endsWith("/")) {
            fname.append("/");
        }
        fname.append((pos > -1) ? remaining.substring(pos + "files".length() + 1) : remaining);
        return new File(fname.toString());
    }
    
    static {
        logger = LoggerFactory.getLogger(InvDatasetFeatureCollection.class);
    }
    
    private class State
    {
        ThreddsMetadata.Variables vars;
        ThreddsMetadata.GeospatialCoverage gc;
        DateRange dateRange;
        Date lastProtoChange;
        InvDatasetScan scan;
        List<InvDataset> datasets;
        Date lastInvChange;
        
        State(final State from) {
            if (from != null) {
                this.vars = from.vars;
                this.gc = from.gc;
                this.dateRange = from.dateRange;
                this.lastProtoChange = from.lastProtoChange;
                this.scan = from.scan;
                this.datasets = from.datasets;
                this.lastInvChange = from.lastInvChange;
            }
        }
    }
    
    public static class ScanFilter implements CrawlableDatasetFilter
    {
        private Pattern p;
        private long olderThan;
        
        public ScanFilter(final Pattern p, final long olderThan) {
            this.p = p;
            this.olderThan = olderThan;
        }
        
        public boolean accept(final CrawlableDataset dataset) {
            if (dataset.isCollection()) {
                return true;
            }
            if (this.p != null) {
                final Matcher matcher = this.p.matcher(dataset.getName());
                if (!matcher.matches()) {
                    return false;
                }
            }
            if (this.olderThan > 0L) {
                final Date lastModDate = dataset.lastModified();
                if (lastModDate != null) {
                    final long now = System.currentTimeMillis();
                    if (now - lastModDate.getTime() <= this.olderThan) {
                        return false;
                    }
                }
            }
            return true;
        }
        
        public Object getConfigObject() {
            return null;
        }
    }
}
