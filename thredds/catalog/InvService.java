// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.net.URI;

public class InvService
{
    private String name;
    private String base;
    private String suffix;
    private String desc;
    private ServiceType type;
    private URI uri;
    private List<InvService> nestedServices;
    private List<InvProperty> properties;
    private List<InvProperty> roots;
    private StringBuilder log;
    private volatile int hashCode;
    
    public InvService(final String name, final String serviceTypeName, final String base, final String suffix, final String desc) {
        this.type = null;
        this.uri = null;
        this.nestedServices = new ArrayList<InvService>();
        this.properties = new ArrayList<InvProperty>();
        this.roots = new ArrayList<InvProperty>();
        this.log = new StringBuilder();
        this.hashCode = 0;
        this.name = name;
        this.type = ServiceType.findType(serviceTypeName);
        this.base = ((base == null) ? "" : base.trim());
        this.suffix = ((suffix == null) ? "" : suffix.trim());
        this.desc = desc;
        if (this.type == null) {
            this.log.append(" ** InvService: non-standard type =(").append(serviceTypeName).append(") for service (").append(name).append(")");
            this.type = ServiceType.getType(serviceTypeName);
        }
        if (name == null) {
            this.log.append(" ** InvService has no name");
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getBase() {
        return this.base;
    }
    
    public ServiceType getServiceType() {
        return this.type;
    }
    
    public String getSuffix() {
        return this.suffix;
    }
    
    public String getDescription() {
        return (this.desc != null) ? this.desc : this.type.toString();
    }
    
    public List<InvProperty> getProperties() {
        return this.properties;
    }
    
    public List<InvProperty> getDatasetRoots() {
        return this.roots;
    }
    
    public String findProperty(final String name) {
        InvProperty result = null;
        for (final InvProperty p : this.properties) {
            if (p.getName().equals(name)) {
                result = p;
            }
        }
        return (result == null) ? null : result.getValue();
    }
    
    @Override
    public String toString() {
        return "name:(" + this.name + ") type:(" + this.type + ") base:(" + this.base + ") suffix:(" + this.suffix + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof InvService && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getBase().hashCode();
            result = 37 * result + this.getServiceType().hashCode();
            if (null != this.getSuffix()) {
                result = 37 * result + this.getSuffix().hashCode();
            }
            result = 37 * result + this.getProperties().hashCode();
            result = 37 * result + this.nestedServices.hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    public String dump() {
        return this.dump(0);
    }
    
    String dump(final int n) {
        final StringBuilder buff = new StringBuilder(200);
        buff.setLength(0);
        buff.append(InvDatasetImpl.indent(n)).append("Service ").append(this).append("\n");
        for (final InvService s : this.getServices()) {
            buff.append(s.dump(n + 2));
        }
        final List<InvProperty> props = this.getProperties();
        if (props.size() > 0) {
            final String indent = InvDatasetImpl.indent(n + 2);
            buff.append(indent);
            buff.append("Properties:\n");
            for (final InvProperty p : props) {
                buff.append(InvDatasetImpl.indent(n + 4)).append(p).append("\n");
            }
        }
        return buff.toString();
    }
    
    public void addService(final InvService service) {
        this.nestedServices.add(service);
    }
    
    public void addProperty(final InvProperty p) {
        this.properties.add(p);
    }
    
    @Deprecated
    public void addDatasetRoot(final InvProperty root) {
        this.roots.add(root);
    }
    
    public List<InvService> getServices() {
        return this.nestedServices;
    }
    
    @Deprecated
    public String getFullName() {
        return this.name;
    }
    
    protected boolean check(final StringBuilder out) {
        boolean isValid = true;
        if (this.log.length() > 0) {
            out.append((CharSequence)this.log);
        }
        if (this.getServiceType() == ServiceType.COMPOUND) {
            if (this.getServices().size() < 1) {
                out.append(" ** InvService (").append(this.getName()).append(") type COMPOUND must have a nested service\n");
                isValid = false;
            }
        }
        else if (this.getServices().size() > 0) {
            out.append(" ** InvService(").append(this.getName()).append(") type ").append(this.getServiceType()).append(" may not have nested services\n");
            isValid = false;
        }
        try {
            this.uri = new URI(this.base);
        }
        catch (URISyntaxException e) {
            out.append(" ** InvService(").append(this.getName()).append(") invalid base URL =(").append(this.base).append(")");
            isValid = false;
        }
        return isValid;
    }
    
    public boolean isRelativeBase() {
        if (this.getServiceType() == ServiceType.COMPOUND) {
            return true;
        }
        if (this.uri == null) {
            try {
                this.uri = new URI(this.base);
            }
            catch (URISyntaxException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
        return !this.uri.isAbsolute();
    }
}
