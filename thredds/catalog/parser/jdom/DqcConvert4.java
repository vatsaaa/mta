// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.parser.jdom;

import org.jdom.output.Format;
import thredds.catalog.query.Station;
import thredds.catalog.query.Query;
import thredds.catalog.query.SelectStation;
import thredds.catalog.query.SelectGeoRegion;
import thredds.catalog.query.SelectRangeDate;
import thredds.catalog.query.SelectRange;
import thredds.catalog.query.SelectService;
import java.util.ArrayList;
import java.util.Iterator;
import thredds.catalog.query.Location;
import thredds.catalog.InvDocumentation;
import thredds.catalog.query.SelectList;
import org.jdom.Content;
import java.util.List;
import thredds.catalog.query.ListChoice;
import org.jdom.Element;
import thredds.catalog.query.Selector;
import java.io.IOException;
import java.io.OutputStream;
import org.jdom.output.XMLOutputter;
import thredds.catalog.query.QueryCapability;
import org.jdom.Document;
import java.net.URI;
import thredds.catalog.query.DqcFactory;
import org.jdom.Namespace;
import thredds.catalog.query.DqcConvertIF;

public class DqcConvert4 implements DqcConvertIF
{
    public static boolean debugURL;
    public static boolean debugXML;
    public static boolean debugDBurl;
    public static boolean debugXMLopen;
    public static boolean showParsedXML;
    public static boolean showXMLoutput;
    protected static final Namespace defNS;
    protected static final Namespace xlinkNS;
    private DqcFactory factory;
    private URI docURI;
    
    public QueryCapability parseXML(final DqcFactory fac, final Document jdomDoc, final URI uri) throws IOException {
        this.factory = fac;
        this.docURI = uri;
        if (DqcConvert4.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** DqcConvert3/showParsedXML = \n" + xmlOut.outputString(jdomDoc) + "\n*******");
        }
        final QueryCapability qc = this.readQC(jdomDoc.getRootElement());
        if (DqcConvert4.showXMLoutput) {
            System.out.println("*** DqcConvert3/showXMLoutput");
            this.writeXML(qc, System.out);
        }
        return qc;
    }
    
    private ListChoice readChoice(final QueryCapability qc, final Selector parent, final Element elem) {
        final String name = elem.getAttributeValue("name");
        final String value = elem.getAttributeValue("value");
        final ListChoice c = new ListChoice(parent, name, value, null);
        final List children = elem.getChildren();
        for (int j = 0; j < children.size(); ++j) {
            final Element child = children.get(j);
            final String childName = child.getName();
            if (childName.equals("selectList")) {
                c.addNestedSelector(this.readSelectList(qc, child));
            }
        }
        final Element descElem = elem.getChild("description", DqcConvert4.defNS);
        if (descElem != null) {
            c.setDescription(this.readDocumentation(descElem));
        }
        return c;
    }
    
    private Element writeChoice(final ListChoice c) {
        final Element elem = new Element("choice", DqcConvert4.defNS);
        elem.setAttribute("name", c.getName());
        elem.setAttribute("value", c.getValue());
        if (c.getDescription() != null) {
            elem.addContent(this.writeDocumentation(c.getDescription(), "description"));
        }
        final List selectors = c.getNestedSelectors();
        for (int i = 0; i < selectors.size(); ++i) {
            final Selector s = selectors.get(i);
            if (s instanceof SelectList) {
                elem.addContent(this.writeSelectList((SelectList)s));
            }
        }
        return elem;
    }
    
    protected InvDocumentation readDocumentation(final Element s) {
        final String href = s.getAttributeValue("href", DqcConvert4.xlinkNS);
        final String title = s.getAttributeValue("title", DqcConvert4.xlinkNS);
        final String type = s.getAttributeValue("type");
        final String content = s.getTextNormalize();
        URI uriResolved = null;
        if (href != null) {
            try {
                uriResolved = this.docURI.resolve(href);
            }
            catch (Exception e) {
                this.factory.appendErr(" ** Invalid documentation href = " + href + " " + e.getMessage() + "\n");
            }
        }
        return new InvDocumentation(href, uriResolved, title, type, content);
    }
    
    private Element writeDocumentation(final InvDocumentation doc, final String name) {
        final Element docElem = new Element(name, DqcConvert4.defNS);
        if (doc.getType() != null) {
            docElem.setAttribute("type", doc.getType());
        }
        if (doc.hasXlink()) {
            docElem.setAttribute("href", doc.getURI().toString(), DqcConvert4.xlinkNS);
            if (!doc.getXlinkTitle().equals(doc.getURI().toString())) {
                docElem.setAttribute("title", doc.getXlinkTitle(), DqcConvert4.xlinkNS);
            }
        }
        final String inline = doc.getInlineContent();
        if (inline != null) {
            docElem.addContent(inline);
        }
        return docElem;
    }
    
    private Location readLocation(final Element locationElem) {
        if (locationElem == null) {
            return null;
        }
        final String latitude = locationElem.getAttributeValue("latitude");
        final String longitude = locationElem.getAttributeValue("longitude");
        final String elevation = locationElem.getAttributeValue("elevation");
        final String latitude_units = locationElem.getAttributeValue("latitude_units");
        final String longitude_units = locationElem.getAttributeValue("longitude_units");
        final String elevation_units = locationElem.getAttributeValue("elevation_units");
        final Location location = new Location(latitude, longitude, elevation, latitude_units, longitude_units, elevation_units);
        return location;
    }
    
    private Element writeLocation(final Location l, final String elemName) {
        final Element locationElem = new Element(l.hasElevation() ? "location3D" : elemName, DqcConvert4.defNS);
        locationElem.setAttribute("latitude", Double.toString(l.getLatitude()));
        locationElem.setAttribute("longitude", Double.toString(l.getLongitude()));
        if (!l.isDefaultLatitudeUnits()) {
            locationElem.setAttribute("latitude_units", l.getLatitudeUnits());
        }
        if (!l.isDefaultLongitudeUnits()) {
            locationElem.setAttribute("longitude_units", l.getLongitudeUnits());
        }
        if (l.hasElevation()) {
            locationElem.setAttribute("elevation", Double.toString(l.getElevation()));
            if (!l.isDefaultElevationUnits()) {
                locationElem.setAttribute("elevation_units", l.getElevationUnits());
            }
        }
        return locationElem;
    }
    
    private QueryCapability readQC(final Element qcElem) {
        final String name = qcElem.getAttributeValue("name");
        final String version = qcElem.getAttributeValue("version");
        final QueryCapability qc = new QueryCapability(this.docURI.toString(), name, version);
        qc.setQuery(this.readQuery(qcElem.getChild("query", DqcConvert4.defNS)));
        final Selector s;
        if (null != (s = this.readSelectService(qcElem.getChild("selectService", DqcConvert4.defNS)))) {
            qc.addUniqueSelector(s);
            qc.setServiceSelector(s);
        }
        List list = qcElem.getChildren("userInterface", DqcConvert4.defNS);
        for (int j = 0; j < list.size(); ++j) {
            final Element child = list.get(j);
            qc.addUserInterface(child);
        }
        this.readSelectors(qc, qcElem);
        list = qcElem.getChildren("compound", DqcConvert4.defNS);
        for (int j = 0; j < list.size(); ++j) {
            final Element compoundElem = list.get(j);
            final List compound = this.readSelectors(qc, compoundElem);
            for (final Selector sel : compound) {
                sel.setCompoundSelectors(compound);
            }
        }
        return qc;
    }
    
    private List readSelectors(final QueryCapability qc, final Element elem) {
        final ArrayList compound = new ArrayList();
        List list = elem.getChildren("selectList", DqcConvert4.defNS);
        for (int j = 0; j < list.size(); ++j) {
            final Selector s;
            if (null != (s = this.readSelectList(qc, list.get(j)))) {
                qc.addUniqueSelector(s);
                compound.add(s);
            }
        }
        list = elem.getChildren("selectStation", DqcConvert4.defNS);
        for (int j = 0; j < list.size(); ++j) {
            final Selector s;
            if (null != (s = this.readSelectStation(list.get(j)))) {
                qc.addUniqueSelector(s);
                compound.add(s);
            }
        }
        list = elem.getChildren("selectFromRange", DqcConvert4.defNS);
        for (int j = 0; j < list.size(); ++j) {
            final Selector s;
            if (null != (s = this.readSelectRange(list.get(j)))) {
                qc.addUniqueSelector(s);
                compound.add(s);
            }
        }
        list = elem.getChildren("selectFromDateRange", DqcConvert4.defNS);
        for (int j = 0; j < list.size(); ++j) {
            final Selector s;
            if (null != (s = this.readSelectRangeDate(list.get(j)))) {
                qc.addUniqueSelector(s);
                compound.add(s);
            }
        }
        list = elem.getChildren("selectFromGeoRegion", DqcConvert4.defNS);
        for (int j = 0; j < list.size(); ++j) {
            final Selector s;
            if (null != (s = this.readSelectGeoRegion(list.get(j)))) {
                qc.addUniqueSelector(s);
                compound.add(s);
            }
        }
        return compound;
    }
    
    public Document writeQC(final QueryCapability qc) {
        final Element rootElem = new Element("queryCapability", DqcConvert4.defNS);
        final Document doc = new Document(rootElem);
        if (null != qc.getName()) {
            rootElem.setAttribute("name", qc.getName());
        }
        rootElem.setAttribute("version", qc.getVersion());
        rootElem.addContent(this.writeQuery(qc.getQuery()));
        Selector s = qc.getServiceSelector();
        final Element elem = this.writeSelectService((SelectService)s);
        if (elem != null) {
            rootElem.addContent(elem);
        }
        final List selectors = qc.getSelectors();
        for (int i = 0; i < selectors.size(); ++i) {
            s = selectors.get(i);
            if (s instanceof SelectList) {
                rootElem.addContent(this.writeSelectList((SelectList)s));
            }
            else if (s instanceof SelectRange) {
                rootElem.addContent(this.writeSelectRange((SelectRange)s));
            }
            else if (s instanceof SelectRangeDate) {
                rootElem.addContent(this.writeSelectRangeDate((SelectRangeDate)s));
            }
            else if (s instanceof SelectGeoRegion) {
                rootElem.addContent(this.writeSelectGeoRegion((SelectGeoRegion)s));
            }
            else if (!(s instanceof SelectService)) {
                if (s instanceof SelectStation) {
                    rootElem.addContent(this.writeSelectStation((SelectStation)s));
                }
            }
        }
        return doc;
    }
    
    private Query readQuery(final Element s) {
        final String base = s.getAttributeValue("base");
        URI uriResolved = null;
        if (base != null) {
            try {
                uriResolved = this.docURI.resolve(base);
            }
            catch (Exception e) {
                this.factory.appendFatalErr(" ** Invalid query base = " + base + " " + e.getMessage() + "\n");
            }
        }
        return new Query(base, uriResolved, null);
    }
    
    private Element writeQuery(final Query q) {
        final Element elem = new Element("query", DqcConvert4.defNS);
        elem.setAttribute("base", q.getBase());
        return elem;
    }
    
    private void readSelector(final Element elem, final Selector s) {
        s.setTitle(elem.getAttributeValue("title"));
        s.setId(elem.getAttributeValue("id"));
        s.setTemplate(elem.getAttributeValue("template"));
        s.setRequired(elem.getAttributeValue("required"));
        s.setMultiple(elem.getAttributeValue("multiple"));
        final Element descElem = elem.getChild("description", DqcConvert4.defNS);
        if (descElem != null) {
            s.setDescription(this.readDocumentation(descElem));
        }
    }
    
    private void writeSelector(final Element elem, final Selector s) {
        if (s.getId() != null) {
            elem.setAttribute("id", s.getId());
        }
        if (s.getTitle() != null) {
            elem.setAttribute("title", s.getTitle());
        }
        if (s.getTemplate() != null) {
            elem.setAttribute("template", s.getTemplate());
        }
        if (!s.isRequired()) {
            elem.setAttribute("required", "false");
        }
        if (s.isMultiple()) {
            elem.setAttribute("multiple", "true");
        }
        if (s.getDescription() != null) {
            elem.addContent(this.writeDocumentation(s.getDescription(), "description"));
        }
    }
    
    private SelectList readSelectList(final QueryCapability qc, final Element elem) {
        final SelectList slist = new SelectList();
        this.readSelector(elem, slist);
        final List choices = elem.getChildren("choice", DqcConvert4.defNS);
        for (int j = 0; j < choices.size(); ++j) {
            final ListChoice choice = this.readChoice(qc, slist, choices.get(j));
            slist.addChoice(choice);
        }
        return slist;
    }
    
    private Element writeSelectList(final SelectList s) {
        final Element elem = new Element("selectList", DqcConvert4.defNS);
        this.writeSelector(elem, s);
        final List choices = s.getChoices();
        for (int i = 0; i < choices.size(); ++i) {
            elem.addContent(this.writeChoice(choices.get(i)));
        }
        return elem;
    }
    
    private SelectRange readSelectRange(final Element elem) {
        final String min = elem.getAttributeValue("min");
        final String max = elem.getAttributeValue("max");
        final String units = elem.getAttributeValue("units");
        final String modulo = elem.getAttributeValue("modulo");
        final String resolution = elem.getAttributeValue("resolution");
        final String selectType = elem.getAttributeValue("selectType");
        final SelectRange sr = new SelectRange(min, max, units, modulo, resolution, selectType);
        this.readSelector(elem, sr);
        return sr;
    }
    
    private Element writeSelectRange(final SelectRange s) {
        final Element elem = new Element("selectFromRange", DqcConvert4.defNS);
        this.writeSelector(elem, s);
        if (s.getMin() != null) {
            elem.setAttribute("min", s.getMin());
        }
        if (s.getMax() != null) {
            elem.setAttribute("max", s.getMax());
        }
        if (s.getUnits() != null) {
            elem.setAttribute("units", s.getUnits());
        }
        if (s.isModulo()) {
            elem.setAttribute("modulo", "true");
        }
        if (s.getResolution() != null) {
            elem.setAttribute("resolution", s.getResolution());
        }
        if (s.getSelectType() != null) {
            elem.setAttribute("selectType", s.getSelectType());
        }
        return elem;
    }
    
    private SelectRangeDate readSelectRangeDate(final Element elem) {
        final String start = elem.getAttributeValue("start");
        final String end = elem.getAttributeValue("end");
        final String duration = elem.getAttributeValue("duration");
        final String resolution = elem.getAttributeValue("resolution");
        final String selectType = elem.getAttributeValue("selectType");
        final SelectRangeDate srd = new SelectRangeDate(start, end, duration, resolution, selectType);
        this.readSelector(elem, srd);
        return srd;
    }
    
    private Element writeSelectRangeDate(final SelectRangeDate s) {
        final Element elem = new Element("selectFromDateRange", DqcConvert4.defNS);
        this.writeSelector(elem, s);
        if (s.getStart() != null) {
            elem.setAttribute("start", s.getStart());
        }
        if (s.getEnd() != null) {
            elem.setAttribute("end", s.getEnd());
        }
        if (s.getDuration() != null) {
            elem.setAttribute("duration", s.getDuration());
        }
        if (s.getResolution() != null) {
            elem.setAttribute("resolution", s.getResolution());
        }
        if (s.getSelectType() != null) {
            elem.setAttribute("selectType", s.getSelectType());
        }
        return elem;
    }
    
    private SelectGeoRegion readSelectGeoRegion(final Element elem) {
        final Element geoBB = elem.getChild("geoBoundingBox", DqcConvert4.defNS);
        if (geoBB == null) {
            this.factory.appendErr("No geoBoundingBox in selectFromGeoRegion");
            return null;
        }
        final Location lowerLeft = this.readLocation(geoBB.getChild("lowerLeft", DqcConvert4.defNS));
        final Location upperRight = this.readLocation(geoBB.getChild("upperRight", DqcConvert4.defNS));
        final SelectGeoRegion sr = new SelectGeoRegion(lowerLeft, upperRight);
        this.readSelector(elem, sr);
        return sr;
    }
    
    private Element writeSelectGeoRegion(final SelectGeoRegion s) {
        final Element elem = new Element("selectFromGeoRegion", DqcConvert4.defNS);
        this.writeSelector(elem, s);
        final Element geoBB = new Element("geoBoundingBox", DqcConvert4.defNS);
        elem.addContent(geoBB);
        if (s.getLowerLeft() != null) {
            geoBB.addContent(this.writeLocation(s.getLowerLeft(), "lowerLeft"));
        }
        if (s.getUpperRight() != null) {
            geoBB.addContent(this.writeLocation(s.getUpperRight(), "upperRight"));
        }
        return elem;
    }
    
    private SelectService readSelectService(final Element elem) {
        if (elem == null) {
            return null;
        }
        final SelectService ss = new SelectService(null, null);
        this.readSelector(elem, ss);
        final List choices = elem.getChildren("serviceType", DqcConvert4.defNS);
        for (int j = 0; j < choices.size(); ++j) {
            final Element choice = choices.get(j);
            final String name = choice.getText();
            final String title = choice.getAttributeValue("title");
            final String dataFormatType = choice.getAttributeValue("dataFormatType");
            final String returns = choice.getAttributeValue("returns");
            final String value = choice.getAttributeValue("value");
            ss.addServiceChoice(name, title, dataFormatType, returns, value);
        }
        return ss;
    }
    
    private Element writeSelectService(final SelectService ss) {
        if (ss == null) {
            return null;
        }
        final Element elem = new Element("selectService", DqcConvert4.defNS);
        this.writeSelector(elem, ss);
        final List choices = ss.getChoices();
        for (int i = 0; i < choices.size(); ++i) {
            final SelectService.ServiceChoice s = choices.get(i);
            final Element selem = new Element("serviceType", DqcConvert4.defNS);
            if (s.getTitle() != null) {
                selem.setAttribute("title", s.getTitle());
            }
            if (s.getDataFormat() != null) {
                selem.setAttribute("dataFormatType", s.getDataFormat());
            }
            if (s.getReturns() != null) {
                selem.setAttribute("returns", s.getReturns());
            }
            if (s.getValue2() != null) {
                selem.setAttribute("value", s.getValue2());
            }
            selem.addContent(s.getService());
            elem.addContent(selem);
        }
        return elem;
    }
    
    private SelectStation readSelectStation(final Element elem) {
        final SelectStation ss = new SelectStation();
        this.readSelector(elem, ss);
        final List stations = elem.getChildren("station", DqcConvert4.defNS);
        for (int j = 0; j < stations.size(); ++j) {
            final Station station = this.readStation(ss, stations.get(j));
            ss.addStation(station);
        }
        return ss;
    }
    
    private Element writeSelectStation(final SelectStation s) {
        final Element elem = new Element("selectStation", DqcConvert4.defNS);
        this.writeSelector(elem, s);
        final List stations = s.getStations();
        for (int i = 0; i < stations.size(); ++i) {
            elem.addContent(this.writeStation(stations.get(i)));
        }
        return elem;
    }
    
    private Station readStation(final Selector parent, final Element elem) {
        final String name = elem.getAttributeValue("name");
        final String value = elem.getAttributeValue("value");
        final Station station = new Station(parent, name, value, null);
        Element locationElem = elem.getChild("location", DqcConvert4.defNS);
        if (null == locationElem) {
            locationElem = elem.getChild("location3D", DqcConvert4.defNS);
        }
        station.setLocation(this.readLocation(locationElem));
        final Element descElem = elem.getChild("description", DqcConvert4.defNS);
        if (descElem != null) {
            station.setDescription(this.readDocumentation(descElem));
        }
        return station;
    }
    
    private Element writeStation(final Station c) {
        final Element elem = new Element("station", DqcConvert4.defNS);
        elem.setAttribute("name", c.getName());
        elem.setAttribute("value", c.getValue());
        if (c.getDescription() != null) {
            elem.addContent(this.writeDocumentation(c.getDescription(), "description"));
        }
        elem.addContent(this.writeLocation(c.getLocation(), "location"));
        return elem;
    }
    
    public void writeXML(final QueryCapability qc, final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.writeQC(qc), os);
    }
    
    static {
        DqcConvert4.debugURL = false;
        DqcConvert4.debugXML = false;
        DqcConvert4.debugDBurl = false;
        DqcConvert4.debugXMLopen = false;
        DqcConvert4.showParsedXML = false;
        DqcConvert4.showXMLoutput = false;
        defNS = Namespace.getNamespace("http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.4");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
    }
}
