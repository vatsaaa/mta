// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.parser.jdom;

import thredds.catalog.query.Location;
import thredds.catalog.query.Station;
import thredds.catalog.query.ListChoice;
import thredds.catalog.query.Query;
import thredds.catalog.query.SelectStation;
import thredds.catalog.query.SelectList;
import org.jdom.Content;
import java.util.List;
import thredds.catalog.query.Selector;
import org.jdom.Element;
import java.io.IOException;
import java.io.OutputStream;
import org.jdom.output.XMLOutputter;
import thredds.catalog.query.QueryCapability;
import java.net.URI;
import org.jdom.Document;
import thredds.catalog.query.DqcFactory;
import org.jdom.Namespace;
import thredds.catalog.query.DqcConvertIF;

public class DqcConvert2 implements DqcConvertIF
{
    public static boolean debugURL;
    public static boolean debugXML;
    public static boolean debugDBurl;
    public static boolean debugXMLopen;
    public static boolean showParsedXML;
    public static boolean showXMLoutput;
    protected static final Namespace defNS;
    protected static final Namespace xlinkNS;
    
    public QueryCapability parseXML(final DqcFactory fac, final Document jdomDoc, final URI uri) throws IOException {
        if (DqcConvert2.showParsedXML) {
            final XMLOutputter xmlOut = new XMLOutputter();
            System.out.println("*** queryFactory/showParsedXML = \n" + xmlOut.outputString(jdomDoc) + "\n*******");
        }
        final QueryCapability qc = this.readQC(jdomDoc.getRootElement(), uri);
        if (DqcConvert2.showXMLoutput) {
            System.out.println("*** queryFactory/showXMLoutput");
            this.writeXML(qc, System.out);
        }
        return qc;
    }
    
    private QueryCapability readQC(final Element qcElem, final URI uri) {
        final String name = qcElem.getAttributeValue("name");
        final String version = qcElem.getAttributeValue("version");
        final QueryCapability qc = new QueryCapability(uri.toString(), name, version);
        qc.setQuery(this.readQuery(qcElem.getChild("query", DqcConvert2.defNS), uri));
        final Element selections = qcElem.getChild("selections", DqcConvert2.defNS);
        final List list = selections.getChildren();
        for (int j = 0; j < list.size(); ++j) {
            final Element child = list.get(j);
            final String childName = child.getName();
            if (childName.equals("selectList")) {
                qc.addSelector(this.readSelectList(qc, child));
            }
            if (childName.equals("selectStation")) {
                qc.addSelector(this.readSelectStation(qc, child));
            }
        }
        return qc;
    }
    
    public Document makeQC(final QueryCapability qc) {
        final Element rootElem = new Element("queryCapability", DqcConvert2.defNS);
        final Document doc = new Document(rootElem);
        if (null != qc.getName()) {
            rootElem.setAttribute("name", qc.getName());
        }
        rootElem.setAttribute("version", qc.getVersion());
        rootElem.addContent(this.makeQuery(qc.getQuery()));
        final Element elem = new Element("selections", DqcConvert2.defNS);
        final List selectors = qc.getSelectors();
        for (int i = 0; i < selectors.size(); ++i) {
            final Selector s = selectors.get(i);
            if (s instanceof SelectList) {
                elem.addContent(this.makeSelectList((SelectList)s));
            }
            else if (s instanceof SelectStation) {
                elem.addContent(this.makeSelectStation((SelectStation)s));
            }
        }
        rootElem.addContent(elem);
        return doc;
    }
    
    private Query readQuery(final Element s, final URI base) {
        final String action = s.getAttributeValue("action");
        final String construct = s.getAttributeValue("construct");
        final String returns = s.getAttributeValue("returns");
        final URI uri = base.resolve(action);
        return new Query(action, uri, construct);
    }
    
    private Element makeQuery(final Query q) {
        final Element elem = new Element("query", DqcConvert2.defNS);
        elem.setAttribute("action", q.getBase());
        elem.setAttribute("construct", q.getConstruct());
        elem.setAttribute("returns", q.getReturns());
        return elem;
    }
    
    private SelectList readSelectList(final QueryCapability qc, final Element s) {
        final String label = s.getAttributeValue("label");
        final String id = s.getAttributeValue("id");
        final String template = s.getAttributeValue("template");
        final String selectType = s.getAttributeValue("selectType");
        final String required = s.getAttributeValue("required");
        String multiple = "false";
        if (selectType.equals("multiple")) {
            multiple = "true";
        }
        final SelectList slist = new SelectList(label, id, template, required, multiple);
        qc.addUniqueSelector(slist);
        final List choices = s.getChildren("choice", DqcConvert2.defNS);
        for (int j = 0; j < choices.size(); ++j) {
            final ListChoice choice = this.readChoice(qc, slist, choices.get(j));
            slist.addChoice(choice);
        }
        return slist;
    }
    
    private Element makeSelectList(final SelectList s) {
        final Element elem = new Element("selectList", DqcConvert2.defNS);
        elem.setAttribute("label", s.getTitle());
        if (null != s.getId()) {
            elem.setAttribute("id", s.getId());
        }
        if (null != s.getTemplate()) {
            elem.setAttribute("template", s.getTemplate());
        }
        elem.setAttribute("selectType", s.getSelectType());
        elem.setAttribute("required", Boolean.toString(s.isRequired()));
        final List choices = s.getChoices();
        for (int i = 0; i < choices.size(); ++i) {
            elem.addContent(this.makeChoice(choices.get(i)));
        }
        return elem;
    }
    
    private SelectStation readSelectStation(final QueryCapability qc, final Element s) {
        final String label = s.getAttributeValue("label");
        final String id = s.getAttributeValue("id");
        final String template = s.getAttributeValue("template");
        final String selectType = s.getAttributeValue("selectType");
        final String required = s.getAttributeValue("required");
        String multiple = "false";
        if (selectType.equals("multiple")) {
            multiple = "true";
        }
        final SelectStation ss = new SelectStation(label, id, template, required, multiple);
        qc.addUniqueSelector(ss);
        final List stations = s.getChildren("station", DqcConvert2.defNS);
        for (int j = 0; j < stations.size(); ++j) {
            final Station station = this.readStation(ss, stations.get(j));
            ss.addStation(station);
        }
        return ss;
    }
    
    private Element makeSelectStation(final SelectStation s) {
        final Element elem = new Element("selectStation", DqcConvert2.defNS);
        elem.setAttribute("label", s.getTitle());
        if (null != s.getId()) {
            elem.setAttribute("id", s.getId());
        }
        if (null != s.getTemplate()) {
            elem.setAttribute("template", s.getTemplate());
        }
        elem.setAttribute("selectType", s.getSelectType());
        elem.setAttribute("required", Boolean.toString(s.isRequired()));
        final List stations = s.getStations();
        for (int i = 0; i < stations.size(); ++i) {
            elem.addContent(this.makeStation(stations.get(i)));
        }
        return elem;
    }
    
    private ListChoice readChoice(final QueryCapability qc, final Selector parent, final Element elem) {
        final String name = elem.getAttributeValue("name");
        final String value = elem.getAttributeValue("value");
        final String description = elem.getAttributeValue("description");
        final ListChoice c = new ListChoice(parent, name, value, description);
        final List children = elem.getChildren();
        for (int j = 0; j < children.size(); ++j) {
            final Element child = children.get(j);
            final String childName = child.getName();
            if (childName.equals("selectList")) {
                c.addNestedSelector(this.readSelectList(qc, child));
            }
        }
        return c;
    }
    
    private Element makeChoice(final ListChoice c) {
        final Element elem = new Element("choice", DqcConvert2.defNS);
        elem.setAttribute("name", c.getName());
        elem.setAttribute("value", c.getValue());
        final List selectors = c.getNestedSelectors();
        for (int i = 0; i < selectors.size(); ++i) {
            final Selector s = selectors.get(i);
            if (s instanceof SelectList) {
                elem.addContent(this.makeSelectList((SelectList)s));
            }
        }
        return elem;
    }
    
    private Station readStation(final Selector parent, final Element elem) {
        final String name = elem.getAttributeValue("name");
        final String value = elem.getAttributeValue("value");
        final String description = elem.getAttributeValue("description");
        final Station station = new Station(parent, name, value, description);
        Element locationElem = elem.getChild("location", DqcConvert2.defNS);
        if (null == locationElem) {
            locationElem = elem.getChild("location3D", DqcConvert2.defNS);
        }
        final String latitude = locationElem.getAttributeValue("latitude");
        final String longitude = locationElem.getAttributeValue("longitude");
        final String elevation = locationElem.getAttributeValue("elevation");
        final String latitude_units = locationElem.getAttributeValue("latitude_units");
        final String longitude_units = locationElem.getAttributeValue("longitude_units");
        final String elevation_units = locationElem.getAttributeValue("elevation_units");
        final Location location = new Location(latitude, longitude, elevation, latitude_units, longitude_units, elevation_units);
        station.setLocation(location);
        return station;
    }
    
    private Element makeStation(final Station c) {
        final Element elem = new Element("station", DqcConvert2.defNS);
        elem.setAttribute("name", c.getName());
        elem.setAttribute("value", c.getValue());
        final Location l = c.getLocation();
        final Element locationElem = new Element(l.hasElevation() ? "location3D" : "location", DqcConvert2.defNS);
        locationElem.setAttribute("latitude", Double.toString(l.getLatitude()));
        locationElem.setAttribute("longitude", Double.toString(l.getLongitude()));
        if (null != l.getLatitudeUnits()) {
            locationElem.setAttribute("latitude_units", l.getLatitudeUnits());
        }
        if (null != l.getLongitudeUnits()) {
            locationElem.setAttribute("longitude_units", l.getLongitudeUnits());
        }
        if (l.hasElevation()) {
            locationElem.setAttribute("elevation", Double.toString(l.getElevation()));
            if (null != l.getElevationUnits()) {
                locationElem.setAttribute("elevation_units", l.getElevationUnits());
            }
        }
        elem.addContent(locationElem);
        return elem;
    }
    
    public void writeXML(final QueryCapability qc, final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter();
        fmt.output(this.makeQC(qc), os);
    }
    
    static {
        DqcConvert2.debugURL = false;
        DqcConvert2.debugXML = false;
        DqcConvert2.debugDBurl = false;
        DqcConvert2.debugXMLopen = false;
        DqcConvert2.showParsedXML = false;
        DqcConvert2.showXMLoutput = false;
        defNS = Namespace.getNamespace("http://www.unidata.ucar.edu/schemas/thredds/queryCapability");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
    }
}
