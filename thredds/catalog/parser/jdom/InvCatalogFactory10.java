// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.parser.jdom;

import org.slf4j.LoggerFactory;
import thredds.crawlabledataset.CrawlableDataset;
import org.jdom.Comment;
import org.jdom.Content;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.OutputStream;
import thredds.catalog.DataFormatType;
import ucar.nc2.units.DateRange;
import org.jdom.JDOMException;
import java.io.IOException;
import thredds.catalog.InvMetadata;
import thredds.catalog.InvDocumentation;
import ucar.nc2.units.TimeDuration;
import java.text.ParseException;
import ucar.nc2.units.DateType;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import thredds.cataloggen.datasetenhancer.RegExpAndDurationTimeCoverageEnhancer;
import thredds.cataloggen.inserter.SimpleLatestProxyDsHandler;
import thredds.cataloggen.ProxyDatasetHandler;
import thredds.cataloggen.inserter.LatestCompleteProxyDsHandler;
import thredds.crawlabledataset.MultiLabeler;
import thredds.crawlabledataset.RegExpAndReplaceOnPathLabeler;
import thredds.crawlabledataset.RegExpAndReplaceOnNameLabeler;
import org.jdom.Attribute;
import thredds.crawlabledataset.filter.WildcardMatchOnNameFilter;
import thredds.crawlabledataset.filter.MultiSelectorFilter;
import thredds.crawlabledataset.filter.RegExpMatchOnNameFilter;
import thredds.crawlabledataset.filter.LogicalFilterComposer;
import thredds.crawlabledataset.filter.LastModifiedLimitFilter;
import org.jdom.DataConversionException;
import thredds.cataloggen.CatalogRefExpander;
import thredds.crawlabledataset.CrawlableDatasetSorter;
import thredds.crawlabledataset.CrawlableDatasetLabeler;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import thredds.cataloggen.DatasetEnhancer;
import thredds.crawlabledataset.sorter.LexigraphicByNameSorter;
import thredds.catalog.InvDatasetScan;
import thredds.catalog.InvDatasetFmrc;
import thredds.catalog.InvDatasetFeatureCollection;
import thredds.inventory.FeatureCollectionConfig;
import thredds.catalog.InvAccess;
import thredds.catalog.InvCatalog;
import thredds.catalog.CollectionType;
import ucar.nc2.constants.FeatureType;
import thredds.catalog.InvDatasetImplProxy;
import thredds.catalog.ThreddsMetadata;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.DataRootConfig;
import thredds.catalog.InvProperty;
import thredds.catalog.InvService;
import java.net.URISyntaxException;
import thredds.catalog.InvDataset;
import thredds.catalog.InvAccessImpl;
import org.jdom.Element;
import thredds.catalog.InvDatasetImpl;
import thredds.catalog.InvCatalogImpl;
import java.net.URI;
import org.jdom.Document;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;
import org.jdom.input.SAXBuilder;
import thredds.catalog.MetadataType;
import java.util.Map;
import thredds.util.PathAliasReplacement;
import java.util.List;
import thredds.catalog.InvCatalogFactory;
import org.jdom.Namespace;
import org.slf4j.Logger;
import thredds.catalog.MetadataConverterIF;
import thredds.catalog.InvCatalogConvertIF;

public class InvCatalogFactory10 implements InvCatalogConvertIF, MetadataConverterIF
{
    private static Logger logger;
    private static final Namespace defNS;
    private static final Namespace xlinkNS;
    private static final Namespace ncmlNS;
    private static boolean useBytesForDataSize;
    private InvCatalogFactory factory;
    private String version;
    private boolean debugMetadataRead;
    private List<PathAliasReplacement> dataRootLocAliasExpanders;
    private Map<MetadataType, MetadataConverterIF> metadataHash;
    private SAXBuilder saxBuilder;
    private boolean raw;
    
    public InvCatalogFactory10() {
        this.factory = null;
        this.version = "1.0.1";
        this.debugMetadataRead = false;
        this.dataRootLocAliasExpanders = Collections.emptyList();
        this.metadataHash = new HashMap<MetadataType, MetadataConverterIF>(10);
        this.raw = false;
    }
    
    public static void useBytesForDataSize(final boolean b) {
        InvCatalogFactory10.useBytesForDataSize = b;
    }
    
    public void setDataRootLocationAliasExpanders(final List<PathAliasReplacement> dataRootLocAliasExpanders) {
        if (dataRootLocAliasExpanders == null) {
            this.dataRootLocAliasExpanders = Collections.emptyList();
        }
        else {
            this.dataRootLocAliasExpanders = new ArrayList<PathAliasReplacement>(dataRootLocAliasExpanders);
        }
    }
    
    public List<PathAliasReplacement> getDataRootLocationAliasExpanders() {
        return Collections.unmodifiableList((List<? extends PathAliasReplacement>)this.dataRootLocAliasExpanders);
    }
    
    private String expandDataRootLocationAlias(final String location) {
        for (final PathAliasReplacement par : this.dataRootLocAliasExpanders) {
            if (par.containsPathAlias(location)) {
                return par.replacePathAlias(location);
            }
        }
        return location;
    }
    
    public InvCatalogImpl parseXML(final InvCatalogFactory fac, final Document jdomDoc, final URI uri) {
        this.factory = fac;
        return this.readCatalog(jdomDoc.getRootElement(), uri);
    }
    
    public void registerMetadataConverter(final MetadataType type, final MetadataConverterIF converter) {
        this.metadataHash.put(type, converter);
    }
    
    public void setVersion(final String version) {
        this.version = version;
    }
    
    protected InvAccessImpl readAccess(final InvDatasetImpl dataset, final Element accessElem) {
        final String urlPath = accessElem.getAttributeValue("urlPath");
        final String serviceName = accessElem.getAttributeValue("serviceName");
        final String dataFormat = accessElem.getAttributeValue("dataFormat");
        return new InvAccessImpl(dataset, urlPath, serviceName, null, dataFormat, this.readDataSize(accessElem));
    }
    
    protected InvCatalogImpl readCatalog(final Element catalogElem, final URI docBaseURI) {
        final String name = catalogElem.getAttributeValue("name");
        final String catSpecifiedBaseURL = catalogElem.getAttributeValue("base");
        final String expires = catalogElem.getAttributeValue("expires");
        final String version = catalogElem.getAttributeValue("version");
        URI baseURI = docBaseURI;
        if (catSpecifiedBaseURL != null) {
            try {
                baseURI = new URI(catSpecifiedBaseURL);
            }
            catch (URISyntaxException e) {
                InvCatalogFactory10.logger.debug("readCatalog(): bad catalog specified base URI <" + catSpecifiedBaseURL + ">: " + e.getMessage(), e);
                baseURI = docBaseURI;
            }
        }
        final InvCatalogImpl catalog = new InvCatalogImpl(name, version, this.makeDateType(expires, null, null), baseURI);
        final List<Element> sList = (List<Element>)catalogElem.getChildren("service", InvCatalogFactory10.defNS);
        for (final Element e2 : sList) {
            final InvService s = this.readService(e2, baseURI);
            catalog.addService(s);
        }
        final List<Element> pList = (List<Element>)catalogElem.getChildren("property", InvCatalogFactory10.defNS);
        for (final Element e3 : pList) {
            final InvProperty s2 = this.readProperty(e3);
            catalog.addProperty(s2);
        }
        final List<Element> rootList = (List<Element>)catalogElem.getChildren("datasetRoot", InvCatalogFactory10.defNS);
        for (final Element e4 : rootList) {
            final DataRootConfig root = this.readDatasetRoot(e4);
            catalog.addDatasetRoot(root);
        }
        final List<Element> allChildren = (List<Element>)catalogElem.getChildren();
        for (final Element e5 : allChildren) {
            if (e5.getName().equals("dataset")) {
                catalog.addDataset(this.readDataset(catalog, null, e5, baseURI));
            }
            else if (e5.getName().equals("featureCollection")) {
                catalog.addDataset(this.readFeatureCollection(catalog, null, e5, baseURI));
            }
            else if (e5.getName().equals("datasetFmrc")) {
                catalog.addDataset(this.readDatasetFmrc(catalog, null, e5, baseURI));
            }
            else if (e5.getName().equals("datasetScan")) {
                catalog.addDataset(this.readDatasetScan(catalog, null, e5, baseURI));
            }
            else {
                if (!e5.getName().equals("catalogRef")) {
                    continue;
                }
                catalog.addDataset(this.readCatalogRef(catalog, null, e5, baseURI));
            }
        }
        return catalog;
    }
    
    protected InvCatalogRef readCatalogRef(final InvCatalogImpl cat, final InvDatasetImpl parent, final Element catRefElem, final URI baseURI) {
        String title = catRefElem.getAttributeValue("title", InvCatalogFactory10.xlinkNS);
        if (title == null) {
            title = catRefElem.getAttributeValue("name");
        }
        final String href = catRefElem.getAttributeValue("href", InvCatalogFactory10.xlinkNS);
        final InvCatalogRef catRef = new InvCatalogRef(parent, title, href);
        this.readDatasetInfo(cat, catRef, catRefElem, baseURI);
        return catRef;
    }
    
    protected ThreddsMetadata.Contributor readContributor(final Element elem) {
        if (elem == null) {
            return null;
        }
        return new ThreddsMetadata.Contributor(elem.getText(), elem.getAttributeValue("role"));
    }
    
    protected ThreddsMetadata.Vocab readControlledVocabulary(final Element elem) {
        if (elem == null) {
            return null;
        }
        return new ThreddsMetadata.Vocab(elem.getText(), elem.getAttributeValue("vocabulary"));
    }
    
    protected InvDatasetImpl readDataset(final InvCatalogImpl catalog, final InvDatasetImpl parent, final Element dsElem, final URI base) {
        final String name = dsElem.getAttributeValue("name");
        final String alias = dsElem.getAttributeValue("alias");
        if (alias != null) {
            final InvDatasetImpl ds = (InvDatasetImpl)catalog.findDatasetByID(alias);
            if (ds == null) {
                this.factory.appendErr(" ** Parse error: dataset named " + name + " has illegal alias = " + alias + "\n");
            }
            return new InvDatasetImplProxy(name, ds);
        }
        final InvDatasetImpl dataset = new InvDatasetImpl(parent, name);
        this.readDatasetInfo(catalog, dataset, dsElem, base);
        if (InvCatalogFactory.debugXML) {
            System.out.println(" Dataset added: " + dataset.dump());
        }
        return dataset;
    }
    
    protected void readDatasetInfo(final InvCatalogImpl catalog, final InvDatasetImpl dataset, final Element dsElem, final URI base) {
        final String authority = dsElem.getAttributeValue("authority");
        final String collectionTypeName = dsElem.getAttributeValue("collectionType");
        final String dataTypeName = dsElem.getAttributeValue("dataType");
        final String harvest = dsElem.getAttributeValue("harvest");
        final String id = dsElem.getAttributeValue("ID");
        final String serviceName = dsElem.getAttributeValue("serviceName");
        final String urlPath = dsElem.getAttributeValue("urlPath");
        final String restrictAccess = dsElem.getAttributeValue("restrictAccess");
        FeatureType dataType = null;
        if (dataTypeName != null) {
            dataType = FeatureType.getType(dataTypeName.toUpperCase());
            if (dataType == null) {
                this.factory.appendWarning(" ** warning: non-standard data type = " + dataTypeName + "\n");
            }
        }
        if (dataType != null) {
            dataset.setDataType(dataType);
        }
        if (serviceName != null) {
            dataset.setServiceName(serviceName);
        }
        if (urlPath != null) {
            dataset.setUrlPath(urlPath);
        }
        if (authority != null) {
            dataset.setAuthority(authority);
        }
        if (id != null) {
            dataset.setID(id);
        }
        if (harvest != null) {
            dataset.setHarvest(harvest.equalsIgnoreCase("true"));
        }
        if (restrictAccess != null) {
            dataset.setResourceControl(restrictAccess);
        }
        if (collectionTypeName != null) {
            CollectionType collectionType = CollectionType.findType(collectionTypeName);
            if (collectionType == null) {
                collectionType = CollectionType.getType(collectionTypeName);
                this.factory.appendWarning(" ** warning: non-standard collection type = " + collectionTypeName + "\n");
            }
            dataset.setCollectionType(collectionType);
        }
        catalog.addDatasetByID(dataset);
        final List<Element> serviceList = (List<Element>)dsElem.getChildren("service", InvCatalogFactory10.defNS);
        for (final Element curElem : serviceList) {
            final InvService s = this.readService(curElem, base);
            dataset.addService(s);
        }
        final ThreddsMetadata tmg = dataset.getLocalMetadata();
        this.readThreddsMetadata(catalog, dataset, dsElem, tmg);
        final List<Element> aList = (List<Element>)dsElem.getChildren("access", InvCatalogFactory10.defNS);
        for (final Element e : aList) {
            final InvAccessImpl a = this.readAccess(dataset, e);
            dataset.addAccess(a);
        }
        final Element ncmlElem = dsElem.getChild("netcdf", InvCatalogFactory10.ncmlNS);
        if (ncmlElem != null) {
            ncmlElem.detach();
            dataset.setNcmlElement(ncmlElem);
        }
        final List<Element> allChildren = (List<Element>)dsElem.getChildren();
        for (final Element e2 : allChildren) {
            if (e2.getName().equals("dataset")) {
                final InvDatasetImpl ds = this.readDataset(catalog, dataset, e2, base);
                if (ds == null) {
                    continue;
                }
                dataset.addDataset(ds);
            }
            else if (e2.getName().equals("catalogRef")) {
                final InvDatasetImpl ds = this.readCatalogRef(catalog, dataset, e2, base);
                dataset.addDataset(ds);
            }
            else if (e2.getName().equals("datasetScan")) {
                dataset.addDataset(this.readDatasetScan(catalog, dataset, e2, base));
            }
            else if (e2.getName().equals("datasetFmrc")) {
                dataset.addDataset(this.readDatasetFmrc(catalog, dataset, e2, base));
            }
            else {
                if (!e2.getName().equals("featureCollection")) {
                    continue;
                }
                dataset.addDataset(this.readFeatureCollection(catalog, dataset, e2, base));
            }
        }
    }
    
    protected InvDatasetImpl readFeatureCollection(final InvCatalogImpl catalog, final InvDatasetImpl parent, final Element dsElem, final URI base) {
        final String name = dsElem.getAttributeValue("name");
        final String path = dsElem.getAttributeValue("path");
        final String featureType = dsElem.getAttributeValue("featureType");
        final Element collElem = dsElem.getChild("collection", InvCatalogFactory10.defNS);
        if (collElem == null) {
            InvCatalogFactory10.logger.error("featureCollection " + name + " must have a <collection> element.");
            return null;
        }
        final String specName = collElem.getAttributeValue("name");
        final String spec = collElem.getAttributeValue("spec");
        final String olderThan = collElem.getAttributeValue("olderThan");
        String recheckAfter = collElem.getAttributeValue("recheckAfter");
        if (recheckAfter == null) {
            recheckAfter = collElem.getAttributeValue("recheckEvery");
        }
        if (spec == null) {
            InvCatalogFactory10.logger.error("featureCollection " + name + " must have a spec attribute.");
            return null;
        }
        final String collName = (specName != null) ? specName : name;
        final Element innerNcml = dsElem.getChild("netcdf", InvCatalogFactory10.ncmlNS);
        final FeatureCollectionConfig.Config config = new FeatureCollectionConfig.Config(collName, spec, olderThan, recheckAfter, innerNcml);
        final Element updateElem = dsElem.getChild("update", InvCatalogFactory10.defNS);
        if (updateElem != null) {
            final String startup = updateElem.getAttributeValue("startup");
            final String rescan = updateElem.getAttributeValue("rescan");
            final String trigger = updateElem.getAttributeValue("trigger");
            config.updateConfig = new FeatureCollectionConfig.UpdateConfig(startup, rescan, trigger);
        }
        final Element protoElem = dsElem.getChild("protoDataset", InvCatalogFactory10.defNS);
        if (protoElem != null) {
            final String choice = protoElem.getAttributeValue("choice");
            final String change = protoElem.getAttributeValue("change");
            final String param = protoElem.getAttributeValue("param");
            final Element ncmlElem = protoElem.getChild("netcdf", InvCatalogFactory10.ncmlNS);
            config.protoConfig = new FeatureCollectionConfig.ProtoConfig(choice, change, param, ncmlElem);
        }
        final Element fmrcElem = dsElem.getChild("fmrcConfig", InvCatalogFactory10.defNS);
        if (fmrcElem != null) {
            final String regularize = fmrcElem.getAttributeValue("regularize");
            config.fmrcConfig = new FeatureCollectionConfig.FmrcConfig(regularize);
            final String datasetTypes = fmrcElem.getAttributeValue("datasetTypes");
            if (null != datasetTypes) {
                config.fmrcConfig.addDatasetType(datasetTypes);
            }
            final List<Element> bestElems = (List<Element>)fmrcElem.getChildren("dataset", InvCatalogFactory10.defNS);
            for (final Element best : bestElems) {
                final String bestName = best.getAttributeValue("name");
                final String offs = best.getAttributeValue("offsetsGreaterEqual");
                final double off = Double.parseDouble(offs);
                config.fmrcConfig.addBestDataset(bestName, off);
            }
        }
        final InvDatasetFeatureCollection ds = new InvDatasetFeatureCollection(parent, name, path, featureType, config);
        this.readDatasetInfo(catalog, ds, dsElem, base);
        return ds;
    }
    
    protected InvDatasetImpl readDatasetFmrc(final InvCatalogImpl catalog, final InvDatasetImpl parent, final Element dsElem, final URI base) {
        final String name = dsElem.getAttributeValue("name");
        final String path = dsElem.getAttributeValue("path");
        final String runsOnly = dsElem.getAttributeValue("runsOnly");
        final InvDatasetFmrc dsFmrc = new InvDatasetFmrc(parent, name, path, "true".equals(runsOnly));
        final Element fmrcElem = dsElem.getChild("fmrcInventory", InvCatalogFactory10.defNS);
        if (fmrcElem != null) {
            final String location = this.expandDataRootLocationAlias(fmrcElem.getAttributeValue("location"));
            final String def = fmrcElem.getAttributeValue("fmrcDefinition");
            final String suffix = fmrcElem.getAttributeValue("suffix");
            final String olderThan = fmrcElem.getAttributeValue("olderThan");
            final String subdirs = fmrcElem.getAttributeValue("subdirs");
            dsFmrc.setFmrcInventoryParams(location, def, suffix, olderThan, subdirs);
        }
        this.readDatasetInfo(catalog, dsFmrc, dsElem, base);
        return dsFmrc;
    }
    
    protected InvDatasetScan readDatasetScan(final InvCatalogImpl catalog, final InvDatasetImpl parent, final Element dsElem, final URI base) {
        InvDatasetScan datasetScan = null;
        if (dsElem.getAttributeValue("dirLocation") == null) {
            if (dsElem.getAttributeValue("location") != null) {
                return this.readDatasetScanNew(catalog, parent, dsElem, base);
            }
            InvCatalogFactory10.logger.error("readDatasetScan(): datasetScan has neither a \"location\" nor a \"dirLocation\" attribute.");
            datasetScan = null;
        }
        else {
            final String name = dsElem.getAttributeValue("name");
            this.factory.appendWarning("**Warning: Dataset " + name + " using old form of DatasetScan (dirLocation instead of location)\n");
            String path = dsElem.getAttributeValue("path");
            String scanDir = this.expandDataRootLocationAlias(dsElem.getAttributeValue("dirLocation"));
            final String filter = dsElem.getAttributeValue("filter");
            final String addDatasetSizeString = dsElem.getAttributeValue("addDatasetSize");
            final String addLatest = dsElem.getAttributeValue("addLatest");
            final String sortOrderIncreasingString = dsElem.getAttributeValue("sortOrderIncreasing");
            boolean sortOrderIncreasing = false;
            if (sortOrderIncreasingString != null && sortOrderIncreasingString.equalsIgnoreCase("true")) {
                sortOrderIncreasing = true;
            }
            boolean addDatasetSize = false;
            if (addDatasetSizeString != null && addDatasetSizeString.equalsIgnoreCase("true")) {
                addDatasetSize = true;
            }
            if (path != null) {
                if (path.charAt(0) == '/') {
                    path = path.substring(1);
                }
                final int last = path.length() - 1;
                if (path.charAt(last) == '/') {
                    path = path.substring(0, last);
                }
            }
            if (scanDir != null) {
                final int last = scanDir.length() - 1;
                if (scanDir.charAt(last) != '/') {
                    scanDir += '/';
                }
            }
            final Element atcElem = dsElem.getChild("addTimeCoverage", InvCatalogFactory10.defNS);
            String dsNameMatchPattern = null;
            String startTimeSubstitutionPattern = null;
            String duration = null;
            if (atcElem != null) {
                dsNameMatchPattern = atcElem.getAttributeValue("datasetNameMatchPattern");
                startTimeSubstitutionPattern = atcElem.getAttributeValue("startTimeSubstitutionPattern");
                duration = atcElem.getAttributeValue("duration");
            }
            try {
                datasetScan = new InvDatasetScan(catalog, parent, name, path, scanDir, filter, addDatasetSize, addLatest, sortOrderIncreasing, dsNameMatchPattern, startTimeSubstitutionPattern, duration);
                this.readDatasetInfo(catalog, datasetScan, dsElem, base);
                if (InvCatalogFactory.debugXML) {
                    System.out.println(" Dataset added: " + datasetScan.dump());
                }
            }
            catch (Exception e) {
                InvCatalogFactory10.logger.error("Reading DatasetScan", e);
                datasetScan = null;
            }
        }
        return datasetScan;
    }
    
    protected InvDatasetScan readDatasetScanNew(final InvCatalogImpl catalog, final InvDatasetImpl parent, final Element dsElem, final URI base) {
        final String name = dsElem.getAttributeValue("name");
        final String path = dsElem.getAttributeValue("path");
        final String scanDir = this.expandDataRootLocationAlias(dsElem.getAttributeValue("location"));
        String configClassName = null;
        Object configObj = null;
        final Element dsConfigElem = dsElem.getChild("crawlableDatasetImpl", InvCatalogFactory10.defNS);
        if (dsConfigElem != null) {
            configClassName = dsConfigElem.getAttributeValue("className");
            final List children = dsConfigElem.getChildren();
            if (children.size() == 1) {
                configObj = children.get(0);
            }
            else if (children.size() != 0) {
                InvCatalogFactory10.logger.warn("readDatasetScanNew(): content of datasetConfig element not a single element, using first element.");
                configObj = children.get(0);
            }
            else {
                InvCatalogFactory10.logger.debug("readDatasetScanNew(): datasetConfig element has no children.");
                configObj = null;
            }
        }
        final Element filterElem = dsElem.getChild("filter", InvCatalogFactory10.defNS);
        CrawlableDatasetFilter filter = null;
        if (filterElem != null) {
            filter = this.readDatasetScanFilter(filterElem);
        }
        final Element identifierElem = dsElem.getChild("addID", InvCatalogFactory10.defNS);
        CrawlableDatasetLabeler identifier = null;
        if (identifierElem != null) {
            identifier = this.readDatasetScanIdentifier(identifierElem);
        }
        final Element namerElem = dsElem.getChild("namer", InvCatalogFactory10.defNS);
        CrawlableDatasetLabeler namer = null;
        if (namerElem != null) {
            namer = this.readDatasetScanNamer(namerElem);
        }
        final Element sorterElem = dsElem.getChild("sort", InvCatalogFactory10.defNS);
        CrawlableDatasetSorter sorter = new LexigraphicByNameSorter(false);
        if (sorterElem != null) {
            sorter = this.readDatasetScanSorter(sorterElem);
        }
        final Element addLatestElem = dsElem.getChild("addLatest", InvCatalogFactory10.defNS);
        final Element addProxiesElem = dsElem.getChild("addProxies", InvCatalogFactory10.defNS);
        Map allProxyDsHandlers;
        if (addLatestElem != null || addProxiesElem != null) {
            allProxyDsHandlers = this.readDatasetScanAddProxies(addProxiesElem, addLatestElem, catalog);
        }
        else {
            allProxyDsHandlers = new HashMap();
        }
        final Element addDsSizeElem = dsElem.getChild("addDatasetSize", InvCatalogFactory10.defNS);
        boolean addDatasetSize = false;
        if (addDsSizeElem != null) {
            addDatasetSize = true;
        }
        final List childEnhancerList = new ArrayList();
        final Element addTimeCovElem = dsElem.getChild("addTimeCoverage", InvCatalogFactory10.defNS);
        if (addTimeCovElem != null) {
            final DatasetEnhancer addTimeCovEnhancer = this.readDatasetScanAddTimeCoverage(addTimeCovElem);
            if (addTimeCovEnhancer != null) {
                childEnhancerList.add(addTimeCovEnhancer);
            }
        }
        final List dsEnhancerElemList = dsElem.getChildren("datasetEnhancerImpl", InvCatalogFactory10.defNS);
        final Iterator it = dsEnhancerElemList.iterator();
        while (it.hasNext()) {
            final Object o = this.readDatasetScanUserDefined(it.next(), DatasetEnhancer.class);
            if (o != null) {
                childEnhancerList.add(o);
            }
        }
        final CatalogRefExpander catalogRefExpander = null;
        InvDatasetScan datasetScan = null;
        try {
            datasetScan = new InvDatasetScan(parent, name, path, scanDir, configClassName, configObj, filter, identifier, namer, addDatasetSize, sorter, allProxyDsHandlers, childEnhancerList, catalogRefExpander);
            this.readDatasetInfo(catalog, datasetScan, dsElem, base);
            if (InvCatalogFactory.debugXML) {
                System.out.println(" Dataset added: " + datasetScan.dump());
            }
        }
        catch (Exception e) {
            InvCatalogFactory10.logger.error("readDatasetScanNew(): failed to create DatasetScan", e);
            datasetScan = null;
        }
        return datasetScan;
    }
    
    CrawlableDatasetFilter readDatasetScanFilter(final Element filterElem) {
        CrawlableDatasetFilter filter = null;
        final Attribute lastModLimitAtt = filterElem.getAttribute("lastModifiedLimit");
        if (lastModLimitAtt != null) {
            long lastModLimit;
            try {
                lastModLimit = lastModLimitAtt.getLongValue();
            }
            catch (DataConversionException e) {
                final String tmpMsg = "readDatasetScanFilter(): bad lastModifedLimit value <" + lastModLimitAtt.getValue() + ">, couldn't parse into long: " + e.getMessage();
                this.factory.appendErr(tmpMsg);
                InvCatalogFactory10.logger.warn(tmpMsg);
                return null;
            }
            return new LastModifiedLimitFilter(lastModLimit);
        }
        final String compType = filterElem.getAttributeValue("logicalComp");
        if (compType != null) {
            final List filters = filterElem.getChildren("filter", InvCatalogFactory10.defNS);
            if (compType.equalsIgnoreCase("AND")) {
                if (filters.size() != 2) {
                    final String tmpMsg2 = "readDatasetScanFilter(): wrong number of filters <" + filters.size() + "> for AND (2 expected).";
                    this.factory.appendErr(tmpMsg2);
                    InvCatalogFactory10.logger.warn(tmpMsg2);
                    return null;
                }
                filter = LogicalFilterComposer.getAndFilter(this.readDatasetScanFilter(filters.get(0)), this.readDatasetScanFilter(filters.get(1)));
            }
            else if (compType.equalsIgnoreCase("OR")) {
                if (filters.size() != 2) {
                    final String tmpMsg2 = "readDatasetScanFilter(): wrong number of filters <" + filters.size() + "> for OR (2 expected).";
                    this.factory.appendErr(tmpMsg2);
                    InvCatalogFactory10.logger.warn(tmpMsg2);
                    return null;
                }
                filter = LogicalFilterComposer.getOrFilter(this.readDatasetScanFilter(filters.get(0)), this.readDatasetScanFilter(filters.get(1)));
            }
            else if (compType.equalsIgnoreCase("NOT")) {
                if (filters.size() != 1) {
                    final String tmpMsg2 = "readDatasetScanFilter(): wrong number of filters <" + filters.size() + "> for NOT (1 expected).";
                    this.factory.appendErr(tmpMsg2);
                    InvCatalogFactory10.logger.warn(tmpMsg2);
                    return null;
                }
                filter = LogicalFilterComposer.getNotFilter(this.readDatasetScanFilter(filters.get(0)));
            }
            return filter;
        }
        final Element userDefElem = filterElem.getChild("crawlableDatasetFilterImpl", InvCatalogFactory10.defNS);
        if (userDefElem != null) {
            filter = (CrawlableDatasetFilter)this.readDatasetScanUserDefined(userDefElem, CrawlableDatasetFilter.class);
        }
        else {
            final List selectorList = new ArrayList();
            for (final Element curElem : filterElem.getChildren()) {
                final String regExpAttVal = curElem.getAttributeValue("regExp");
                final String wildcardAttVal = curElem.getAttributeValue("wildcard");
                final String lastModLimitAttVal = curElem.getAttributeValue("lastModLimitInMillis");
                if (regExpAttVal == null && wildcardAttVal == null && lastModLimitAttVal == null) {
                    InvCatalogFactory10.logger.warn("readDatasetScanFilter(): no regExp, wildcard, or lastModLimitInMillis attribute in filter child <" + curElem.getName() + ">.");
                }
                else {
                    boolean atomic = true;
                    final String atomicAttVal = curElem.getAttributeValue("atomic");
                    if (atomicAttVal != null && !atomicAttVal.equalsIgnoreCase("true")) {
                        atomic = false;
                    }
                    boolean collection = false;
                    final String collectionAttVal = curElem.getAttributeValue("collection");
                    if (collectionAttVal != null && !collectionAttVal.equalsIgnoreCase("false")) {
                        collection = true;
                    }
                    boolean includer = true;
                    if (curElem.getName().equals("exclude")) {
                        includer = false;
                    }
                    else if (!curElem.getName().equals("include")) {
                        InvCatalogFactory10.logger.warn("readDatasetScanFilter(): unhandled filter child <" + curElem.getName() + ">.");
                        continue;
                    }
                    if (regExpAttVal != null) {
                        selectorList.add(new MultiSelectorFilter.Selector(new RegExpMatchOnNameFilter(regExpAttVal), includer, atomic, collection));
                    }
                    else if (wildcardAttVal != null) {
                        selectorList.add(new MultiSelectorFilter.Selector(new WildcardMatchOnNameFilter(wildcardAttVal), includer, atomic, collection));
                    }
                    else {
                        if (lastModLimitAttVal == null) {
                            continue;
                        }
                        selectorList.add(new MultiSelectorFilter.Selector(new LastModifiedLimitFilter(Long.parseLong(lastModLimitAttVal)), includer, atomic, collection));
                    }
                }
            }
            filter = new MultiSelectorFilter(selectorList);
        }
        return filter;
    }
    
    protected CrawlableDatasetLabeler readDatasetScanIdentifier(final Element identifierElem) {
        CrawlableDatasetLabeler identifier = null;
        final Element userDefElem = identifierElem.getChild("crawlableDatasetLabelerImpl", InvCatalogFactory10.defNS);
        if (userDefElem != null) {
            identifier = (CrawlableDatasetLabeler)this.readDatasetScanUserDefined(userDefElem, CrawlableDatasetLabeler.class);
            return identifier;
        }
        return null;
    }
    
    protected CrawlableDatasetLabeler readDatasetScanNamer(final Element namerElem) {
        CrawlableDatasetLabeler namer = null;
        final List labelerList = new ArrayList();
        for (final Element curElem : namerElem.getChildren()) {
            final String regExp = curElem.getAttributeValue("regExp");
            final String replaceString = curElem.getAttributeValue("replaceString");
            CrawlableDatasetLabeler curLabeler;
            if (curElem.getName().equals("regExpOnName")) {
                curLabeler = new RegExpAndReplaceOnNameLabeler(regExp, replaceString);
            }
            else {
                if (!curElem.getName().equals("regExpOnPath")) {
                    InvCatalogFactory10.logger.warn("readDatasetScanNamer(): unhandled namer child <" + curElem.getName() + ">.");
                    continue;
                }
                curLabeler = new RegExpAndReplaceOnPathLabeler(regExp, replaceString);
            }
            labelerList.add(curLabeler);
        }
        namer = new MultiLabeler(labelerList);
        return namer;
    }
    
    protected CrawlableDatasetSorter readDatasetScanSorter(final Element sorterElem) {
        CrawlableDatasetSorter sorter = null;
        final Element userDefElem = sorterElem.getChild("crawlableDatasetSorterImpl", InvCatalogFactory10.defNS);
        if (userDefElem != null) {
            sorter = (CrawlableDatasetSorter)this.readDatasetScanUserDefined(userDefElem, CrawlableDatasetSorter.class);
        }
        else {
            final Element lexSortElem = sorterElem.getChild("lexigraphicByName", InvCatalogFactory10.defNS);
            if (lexSortElem != null) {
                final String increasingString = lexSortElem.getAttributeValue("increasing");
                final boolean increasing = increasingString.equalsIgnoreCase("true");
                sorter = new LexigraphicByNameSorter(increasing);
            }
        }
        return sorter;
    }
    
    protected Map readDatasetScanAddProxies(final Element addProxiesElem, final Element addLatestElem, final InvCatalogImpl catalog) {
        final Map allProxyDsHandlers = new HashMap();
        if (addLatestElem != null) {
            final Element simpleLatestElem = addLatestElem.getChild("simpleLatest", InvCatalogFactory10.defNS);
            final ProxyDatasetHandler pdh = this.readDatasetScanAddLatest(simpleLatestElem, catalog);
            if (pdh != null) {
                allProxyDsHandlers.put(pdh.getProxyDatasetName(), pdh);
            }
        }
        if (addProxiesElem != null) {
            for (final Element curChildElem : addProxiesElem.getChildren()) {
                ProxyDatasetHandler curPdh;
                if (curChildElem.getName().equals("simpleLatest") && curChildElem.getNamespace().equals(InvCatalogFactory10.defNS)) {
                    curPdh = this.readDatasetScanAddLatest(curChildElem, catalog);
                }
                else if (curChildElem.getName().equals("latestComplete") && curChildElem.getNamespace().equals(InvCatalogFactory10.defNS)) {
                    final String latestName = curChildElem.getAttributeValue("name");
                    if (latestName == null) {
                        InvCatalogFactory10.logger.warn("readDatasetScanAddProxies(): unnamed latestComplete, skipping.");
                        continue;
                    }
                    final Attribute topAtt = curChildElem.getAttribute("top");
                    boolean latestOnTop = true;
                    if (topAtt != null) {
                        try {
                            latestOnTop = topAtt.getBooleanValue();
                        }
                        catch (DataConversionException e) {
                            latestOnTop = true;
                        }
                    }
                    final String serviceName = curChildElem.getAttributeValue("serviceName");
                    if (serviceName == null) {
                        InvCatalogFactory10.logger.warn("readDatasetScanAddProxies(): no service name given in latestComplete.");
                        continue;
                    }
                    final InvService service = catalog.findService(serviceName);
                    if (service == null) {
                        InvCatalogFactory10.logger.warn("readDatasetScanAddProxies(): named service <" + serviceName + "> not found.");
                        continue;
                    }
                    final String lastModLimitVal = curChildElem.getAttributeValue("lastModifiedLimit");
                    long lastModLimit;
                    if (lastModLimitVal == null) {
                        lastModLimit = 60L;
                    }
                    else {
                        lastModLimit = Long.parseLong(lastModLimitVal);
                    }
                    final String isResolverString = curChildElem.getAttributeValue("isResolver");
                    boolean isResolver = true;
                    if (isResolverString != null && isResolverString.equalsIgnoreCase("false")) {
                        isResolver = false;
                    }
                    curPdh = new LatestCompleteProxyDsHandler(latestName, latestOnTop, service, isResolver, lastModLimit);
                }
                else {
                    curPdh = null;
                }
                if (curPdh != null) {
                    if (allProxyDsHandlers.containsKey(curPdh.getProxyDatasetName())) {
                        InvCatalogFactory10.logger.warn("readDatasetScanAddProxies(): proxy map already contains key <" + curPdh.getProxyDatasetName() + ">, skipping.");
                    }
                    else {
                        allProxyDsHandlers.put(curPdh.getProxyDatasetName(), curPdh);
                    }
                }
            }
        }
        return allProxyDsHandlers;
    }
    
    private ProxyDatasetHandler readDatasetScanAddLatest(final Element simpleLatestElem, final InvCatalogImpl catalog) {
        ProxyDatasetHandler latestAdder = null;
        String latestName = "latest.xml";
        boolean latestOnTop = true;
        String latestServiceName = "latest";
        boolean isResolver = true;
        if (simpleLatestElem != null) {
            final String tmpLatestName = simpleLatestElem.getAttributeValue("name");
            if (tmpLatestName != null) {
                latestName = tmpLatestName;
            }
            final Attribute topAtt = simpleLatestElem.getAttribute("top");
            if (topAtt != null) {
                try {
                    latestOnTop = topAtt.getBooleanValue();
                }
                catch (DataConversionException e) {
                    latestOnTop = true;
                }
            }
            final String tmpLatestServiceName = simpleLatestElem.getAttributeValue("serviceName");
            if (tmpLatestServiceName != null) {
                latestServiceName = tmpLatestServiceName;
            }
            final String isResolverString = simpleLatestElem.getAttributeValue("isResolver");
            if (isResolverString != null && isResolverString.equalsIgnoreCase("false")) {
                isResolver = false;
            }
        }
        final InvService service = catalog.findService(latestServiceName);
        if (service == null) {
            InvCatalogFactory10.logger.warn("readDatasetScanAddLatest(): named service <" + latestServiceName + "> not found.");
        }
        else {
            latestAdder = new SimpleLatestProxyDsHandler(latestName, latestOnTop, service, isResolver);
        }
        return latestAdder;
    }
    
    protected DatasetEnhancer readDatasetScanAddTimeCoverage(final Element addTimeCovElem) {
        DatasetEnhancer timeCovEnhancer = null;
        final String matchName = addTimeCovElem.getAttributeValue("datasetNameMatchPattern");
        final String matchPath = addTimeCovElem.getAttributeValue("datasetPathMatchPattern");
        final String subst = addTimeCovElem.getAttributeValue("startTimeSubstitutionPattern");
        final String duration = addTimeCovElem.getAttributeValue("duration");
        if (matchName != null && subst != null && duration != null) {
            timeCovEnhancer = RegExpAndDurationTimeCoverageEnhancer.getInstanceToMatchOnDatasetName(matchName, subst, duration);
        }
        else if (matchPath != null && subst != null && duration != null) {
            timeCovEnhancer = RegExpAndDurationTimeCoverageEnhancer.getInstanceToMatchOnDatasetPath(matchPath, subst, duration);
        }
        return timeCovEnhancer;
    }
    
    private Object readDatasetScanUserDefined(final Element userDefElem, final Class targetClass) {
        final String className = userDefElem.getAttributeValue("className");
        Element configElem = null;
        final List childrenElemList = userDefElem.getChildren();
        if (childrenElemList.size() == 1) {
            configElem = childrenElemList.get(0);
        }
        else if (childrenElemList.size() != 0) {
            InvCatalogFactory10.logger.warn("readDatasetScanUserDefined(): config XML not a single element, using first element.");
            configElem = childrenElemList.get(0);
        }
        else {
            InvCatalogFactory10.logger.debug("readDatasetScanUserDefined(): no config XML elements.");
            configElem = null;
        }
        try {
            final Class requestedClass = Class.forName(className);
            if (!targetClass.isAssignableFrom(requestedClass)) {
                throw new IllegalArgumentException("Requested class <" + className + "> not an implementation of " + targetClass.getName() + ".");
            }
            final Class[] argTypes = { Object.class };
            final Object[] args = { configElem };
            final Constructor constructor = requestedClass.getConstructor((Class[])argTypes);
            return constructor.newInstance(args);
        }
        catch (ClassNotFoundException e) {
            InvCatalogFactory10.logger.warn("readDatasetScanUserDefined(): exception creating user defined object <" + className + ">", e);
            return null;
        }
        catch (NoSuchMethodException e2) {
            InvCatalogFactory10.logger.warn("readDatasetScanUserDefined(): exception creating user defined object <" + className + ">", e2);
            return null;
        }
        catch (InstantiationException e3) {
            InvCatalogFactory10.logger.warn("readDatasetScanUserDefined(): exception creating user defined object <" + className + ">", e3);
            return null;
        }
        catch (IllegalAccessException e4) {
            InvCatalogFactory10.logger.warn("readDatasetScanUserDefined(): exception creating user defined object <" + className + ">", e4);
            return null;
        }
        catch (InvocationTargetException e5) {
            InvCatalogFactory10.logger.warn("readDatasetScanUserDefined(): exception creating user defined object <" + className + ">", e5);
            return null;
        }
    }
    
    protected DataRootConfig readDatasetRoot(final Element s) {
        String path = s.getAttributeValue("path");
        String dirLocation = s.getAttributeValue("location");
        if (dirLocation == null) {
            dirLocation = s.getAttributeValue("dirLocation");
        }
        dirLocation = this.expandDataRootLocationAlias(dirLocation);
        if (path != null) {
            if (path.charAt(0) == '/') {
                path = path.substring(1);
            }
            final int last = path.length() - 1;
            if (path.charAt(last) == '/') {
                path = path.substring(0, last);
            }
        }
        if (dirLocation != null) {
            final int last = dirLocation.length() - 1;
            if (dirLocation.charAt(last) != '/') {
                dirLocation += '/';
            }
        }
        return new DataRootConfig(path, dirLocation, s.getAttributeValue("cache"));
    }
    
    protected DateType readDate(final Element elem) {
        if (elem == null) {
            return null;
        }
        final String format = elem.getAttributeValue("format");
        final String type = elem.getAttributeValue("type");
        return this.makeDateType(elem.getText(), format, type);
    }
    
    protected DateType makeDateType(final String text, final String format, final String type) {
        if (text == null) {
            return null;
        }
        try {
            return new DateType(text, format, type);
        }
        catch (ParseException e) {
            this.factory.appendErr(" ** Parse error: Bad date format = " + text + "\n");
            return null;
        }
    }
    
    protected TimeDuration readDuration(final Element elem) {
        if (elem == null) {
            return null;
        }
        String text = null;
        try {
            text = elem.getText();
            return new TimeDuration(text);
        }
        catch (ParseException e) {
            this.factory.appendErr(" ** Parse error: Bad duration format = " + text + "\n");
            return null;
        }
    }
    
    protected InvDocumentation readDocumentation(final InvCatalog cat, final Element s) {
        final String href = s.getAttributeValue("href", InvCatalogFactory10.xlinkNS);
        final String title = s.getAttributeValue("title", InvCatalogFactory10.xlinkNS);
        final String type = s.getAttributeValue("type");
        final String content = s.getTextNormalize();
        URI uri = null;
        if (href != null) {
            try {
                uri = cat.resolveUri(href);
            }
            catch (Exception e) {
                this.factory.appendErr(" ** Invalid documentation href = " + href + " " + e.getMessage() + "\n");
            }
        }
        final InvDocumentation doc = new InvDocumentation(href, uri, title, type, content);
        if (InvCatalogFactory.debugXML) {
            System.out.println(" Documentation added: " + doc);
        }
        return doc;
    }
    
    protected double readDouble(final Element elem) {
        if (elem == null) {
            return Double.NaN;
        }
        final String text = elem.getText();
        try {
            return Double.parseDouble(text);
        }
        catch (NumberFormatException e) {
            this.factory.appendErr(" ** Parse error: Bad double format = " + text + "\n");
            return Double.NaN;
        }
    }
    
    protected ThreddsMetadata.GeospatialCoverage readGeospatialCoverage(final Element gcElem) {
        if (gcElem == null) {
            return null;
        }
        final String zpositive = gcElem.getAttributeValue("zpositive");
        final ThreddsMetadata.Range northsouth = this.readGeospatialRange(gcElem.getChild("northsouth", InvCatalogFactory10.defNS), "degrees_north");
        final ThreddsMetadata.Range eastwest = this.readGeospatialRange(gcElem.getChild("eastwest", InvCatalogFactory10.defNS), "degrees_east");
        final ThreddsMetadata.Range updown = this.readGeospatialRange(gcElem.getChild("updown", InvCatalogFactory10.defNS), "m");
        final ArrayList names = new ArrayList();
        final List<Element> list = (List<Element>)gcElem.getChildren("name", InvCatalogFactory10.defNS);
        for (final Element e : list) {
            final ThreddsMetadata.Vocab name = this.readControlledVocabulary(e);
            names.add(name);
        }
        final ThreddsMetadata.GeospatialCoverage gc = new ThreddsMetadata.GeospatialCoverage(eastwest, northsouth, updown, names, zpositive);
        return gc;
    }
    
    protected ThreddsMetadata.Range readGeospatialRange(final Element spElem, final String defUnits) {
        if (spElem == null) {
            return null;
        }
        final double start = this.readDouble(spElem.getChild("start", InvCatalogFactory10.defNS));
        final double size = this.readDouble(spElem.getChild("size", InvCatalogFactory10.defNS));
        final double resolution = this.readDouble(spElem.getChild("resolution", InvCatalogFactory10.defNS));
        String units = spElem.getChildText("units", InvCatalogFactory10.defNS);
        if (units == null) {
            units = defUnits;
        }
        return new ThreddsMetadata.Range(start, size, resolution, units);
    }
    
    protected InvMetadata readMetadata(final InvCatalog catalog, final InvDatasetImpl dataset, final Element mdataElement) {
        final List inlineElements = mdataElement.getChildren();
        Namespace namespace;
        if (inlineElements.size() > 0) {
            namespace = inlineElements.get(0).getNamespace();
        }
        else {
            namespace = mdataElement.getNamespace();
        }
        final String mtype = mdataElement.getAttributeValue("metadataType");
        final String href = mdataElement.getAttributeValue("href", InvCatalogFactory10.xlinkNS);
        final String title = mdataElement.getAttributeValue("title", InvCatalogFactory10.xlinkNS);
        final String inheritedS = mdataElement.getAttributeValue("inherited");
        final boolean inherited = inheritedS != null && inheritedS.equalsIgnoreCase("true");
        final boolean isThreddsNamespace = (mtype == null || mtype.equalsIgnoreCase("THREDDS")) && namespace.getURI().equals("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0");
        MetadataConverterIF metaConverter = this.factory.getMetadataConverter(namespace.getURI());
        if (metaConverter == null) {
            metaConverter = this.factory.getMetadataConverter(mtype);
        }
        if (metaConverter != null) {
            if (this.debugMetadataRead) {
                System.out.println("found factory for metadata type = " + mtype + " namespace = " + namespace + "=" + metaConverter.getClass().getName());
            }
            Object contentObj = null;
            if (inlineElements.size() > 0) {
                contentObj = metaConverter.readMetadataContent(dataset, mdataElement);
                return new InvMetadata(dataset, mtype, namespace.getURI(), namespace.getPrefix(), inherited, false, metaConverter, contentObj);
            }
            return new InvMetadata(dataset, href, title, mtype, namespace.getURI(), namespace.getPrefix(), inherited, false, metaConverter);
        }
        else if (!isThreddsNamespace) {
            if (inlineElements.size() > 0) {
                return new InvMetadata(dataset, mtype, namespace.getURI(), namespace.getPrefix(), inherited, false, this, mdataElement);
            }
            return new InvMetadata(dataset, href, title, mtype, namespace.getURI(), namespace.getPrefix(), inherited, false, null);
        }
        else {
            if (inlineElements.size() > 0) {
                final ThreddsMetadata tmg = new ThreddsMetadata(false);
                this.readThreddsMetadata(catalog, dataset, mdataElement, tmg);
                return new InvMetadata(dataset, mtype, namespace.getURI(), namespace.getPrefix(), inherited, true, this, tmg);
            }
            return new InvMetadata(dataset, href, title, mtype, namespace.getURI(), namespace.getPrefix(), inherited, true, this);
        }
    }
    
    public Object readMetadataContent(final InvDataset dataset, final Element mdataElement) {
        final InvMetadata m = this.readMetadata(dataset.getParentCatalog(), (InvDatasetImpl)dataset, mdataElement);
        return m.getThreddsMetadata();
    }
    
    private Element readContentFromURL(final URI uri) throws IOException {
        if (this.saxBuilder == null) {
            this.saxBuilder = new SAXBuilder();
        }
        Document doc;
        try {
            doc = this.saxBuilder.build(uri.toURL());
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        return doc.getRootElement();
    }
    
    public Object readMetadataContentFromURL(final InvDataset dataset, final URI uri) throws IOException {
        final Element elem = this.readContentFromURL(uri);
        final Object contentObject = this.readMetadataContent(dataset, elem);
        if (this.debugMetadataRead) {
            System.out.println(" convert to " + contentObject.getClass().getName());
        }
        return contentObject;
    }
    
    public boolean validateMetadataContent(final Object contentObject, final StringBuilder out) {
        return true;
    }
    
    public void addMetadataContent(final Element mdataElement, final Object contentObject) {
    }
    
    protected InvProperty readProperty(final Element s) {
        final String name = s.getAttributeValue("name");
        final String value = s.getAttributeValue("value");
        return new InvProperty(name, value);
    }
    
    protected ThreddsMetadata.Source readSource(final Element elem) {
        if (elem == null) {
            return null;
        }
        final ThreddsMetadata.Vocab name = this.readControlledVocabulary(elem.getChild("name", InvCatalogFactory10.defNS));
        final Element contact = elem.getChild("contact", InvCatalogFactory10.defNS);
        if (contact == null) {
            this.factory.appendErr(" ** Parse error: Missing contact element in = " + elem.getName() + "\n");
            return null;
        }
        return new ThreddsMetadata.Source(name, contact.getAttributeValue("url"), contact.getAttributeValue("email"));
    }
    
    protected InvService readService(final Element s, final URI baseURI) {
        final String name = s.getAttributeValue("name");
        final String type = s.getAttributeValue("serviceType");
        final String serviceBase = s.getAttributeValue("base");
        final String suffix = s.getAttributeValue("suffix");
        final String desc = s.getAttributeValue("desc");
        final InvService service = new InvService(name, type, serviceBase, suffix, desc);
        final List<Element> propertyList = (List<Element>)s.getChildren("property", InvCatalogFactory10.defNS);
        for (final Element e : propertyList) {
            final InvProperty p = this.readProperty(e);
            service.addProperty(p);
        }
        final List<Element> rootList = (List<Element>)s.getChildren("datasetRoot", InvCatalogFactory10.defNS);
        for (final Element e2 : rootList) {
            final InvProperty root = this.readDatasetRoot(e2);
            service.addDatasetRoot(root);
        }
        final List<Element> serviceList = (List<Element>)s.getChildren("service", InvCatalogFactory10.defNS);
        for (final Element e3 : serviceList) {
            final InvService ss = this.readService(e3, baseURI);
            service.addService(ss);
        }
        if (InvCatalogFactory.debugXML) {
            System.out.println(" Service added: " + service);
        }
        return service;
    }
    
    protected double readDataSize(final Element parent) {
        final Element elem = parent.getChild("dataSize", InvCatalogFactory10.defNS);
        if (elem == null) {
            return Double.NaN;
        }
        final String sizeS = elem.getText();
        double size;
        try {
            size = Double.parseDouble(sizeS);
        }
        catch (NumberFormatException e) {
            this.factory.appendErr(" ** Parse error: Bad double format in size element = " + sizeS + "\n");
            return Double.NaN;
        }
        final String units = elem.getAttributeValue("units");
        final char c = Character.toUpperCase(units.charAt(0));
        if (c == 'K') {
            size *= 1000.0;
        }
        else if (c == 'M') {
            size *= 1000000.0;
        }
        else if (c == 'G') {
            size *= 1.0E9;
        }
        else if (c == 'T') {
            size *= 1.0E12;
        }
        else if (c == 'P') {
            size *= 1.0E15;
        }
        return size;
    }
    
    protected DateRange readTimeCoverage(final Element tElem) {
        if (tElem == null) {
            return null;
        }
        final DateType start = this.readDate(tElem.getChild("start", InvCatalogFactory10.defNS));
        final DateType end = this.readDate(tElem.getChild("end", InvCatalogFactory10.defNS));
        final TimeDuration duration = this.readDuration(tElem.getChild("duration", InvCatalogFactory10.defNS));
        final TimeDuration resolution = this.readDuration(tElem.getChild("resolution", InvCatalogFactory10.defNS));
        try {
            final DateRange tc = new DateRange(start, end, duration, resolution);
            return tc;
        }
        catch (IllegalArgumentException e) {
            this.factory.appendWarning(" ** warning: TimeCoverage error = " + e.getMessage() + "\n");
            return null;
        }
    }
    
    protected void readThreddsMetadata(final InvCatalog catalog, final InvDatasetImpl dataset, final Element parent, final ThreddsMetadata tmg) {
        List<Element> list = (List<Element>)parent.getChildren("creator", InvCatalogFactory10.defNS);
        for (final Element e : list) {
            tmg.addCreator(this.readSource(e));
        }
        list = (List<Element>)parent.getChildren("contributor", InvCatalogFactory10.defNS);
        for (final Element e : list) {
            tmg.addContributor(this.readContributor(e));
        }
        list = (List<Element>)parent.getChildren("date", InvCatalogFactory10.defNS);
        for (final Element e : list) {
            final DateType d = this.readDate(e);
            tmg.addDate(d);
        }
        list = (List<Element>)parent.getChildren("documentation", InvCatalogFactory10.defNS);
        for (final Element e : list) {
            final InvDocumentation doc = this.readDocumentation(catalog, e);
            tmg.addDocumentation(doc);
        }
        list = (List<Element>)parent.getChildren("keyword", InvCatalogFactory10.defNS);
        for (final Element e : list) {
            tmg.addKeyword(this.readControlledVocabulary(e));
        }
        final List<Element> mList = (List<Element>)parent.getChildren("metadata", InvCatalogFactory10.defNS);
        for (final Element e2 : mList) {
            final InvMetadata m = this.readMetadata(catalog, dataset, e2);
            if (m != null) {
                tmg.addMetadata(m);
            }
        }
        list = (List<Element>)parent.getChildren("project", InvCatalogFactory10.defNS);
        for (final Element e2 : list) {
            tmg.addProject(this.readControlledVocabulary(e2));
        }
        list = (List<Element>)parent.getChildren("property", InvCatalogFactory10.defNS);
        for (final Element e2 : list) {
            final InvProperty p = this.readProperty(e2);
            tmg.addProperty(p);
        }
        list = (List<Element>)parent.getChildren("publisher", InvCatalogFactory10.defNS);
        for (final Element e2 : list) {
            tmg.addPublisher(this.readSource(e2));
        }
        list = (List<Element>)parent.getChildren("variables", InvCatalogFactory10.defNS);
        for (final Element e2 : list) {
            final ThreddsMetadata.Variables vars = this.readVariables(catalog, dataset, e2);
            tmg.addVariables(vars);
        }
        final ThreddsMetadata.GeospatialCoverage gc = this.readGeospatialCoverage(parent.getChild("geospatialCoverage", InvCatalogFactory10.defNS));
        if (gc != null) {
            tmg.setGeospatialCoverage(gc);
        }
        final DateRange tc = this.readTimeCoverage(parent.getChild("timeCoverage", InvCatalogFactory10.defNS));
        if (tc != null) {
            tmg.setTimeCoverage(tc);
        }
        final Element serviceNameElem = parent.getChild("serviceName", InvCatalogFactory10.defNS);
        if (serviceNameElem != null) {
            tmg.setServiceName(serviceNameElem.getText());
        }
        final Element authElem = parent.getChild("authority", InvCatalogFactory10.defNS);
        if (authElem != null) {
            tmg.setAuthority(authElem.getText());
        }
        final Element dataTypeElem = parent.getChild("dataType", InvCatalogFactory10.defNS);
        if (dataTypeElem != null) {
            final String dataTypeName = dataTypeElem.getText();
            if (dataTypeName != null && dataTypeName.length() > 0) {
                final FeatureType dataType = FeatureType.getType(dataTypeName.toUpperCase());
                if (dataType == null) {
                    this.factory.appendWarning(" ** warning: non-standard data type = " + dataTypeName + "\n");
                }
                tmg.setDataType(dataType);
            }
        }
        final Element dataFormatElem = parent.getChild("dataFormat", InvCatalogFactory10.defNS);
        if (dataFormatElem != null) {
            final String dataFormatTypeName = dataFormatElem.getText();
            if (dataFormatTypeName != null && dataFormatTypeName.length() > 0) {
                DataFormatType dataFormatType = DataFormatType.findType(dataFormatTypeName);
                if (dataFormatType == null) {
                    dataFormatType = DataFormatType.getType(dataFormatTypeName);
                    this.factory.appendWarning(" ** warning: non-standard dataFormat type = " + dataFormatTypeName + "\n");
                }
                tmg.setDataFormatType(dataFormatType);
            }
        }
        final double size = this.readDataSize(parent);
        if (!Double.isNaN(size)) {
            tmg.setDataSize(size);
        }
    }
    
    protected ThreddsMetadata.Variable readVariable(final Element varElem) {
        if (varElem == null) {
            return null;
        }
        final String name = varElem.getAttributeValue("name");
        final String desc = varElem.getText();
        final String vocabulary_name = varElem.getAttributeValue("vocabulary_name");
        final String units = varElem.getAttributeValue("units");
        final String id = varElem.getAttributeValue("vocabulary_id");
        return new ThreddsMetadata.Variable(name, desc, vocabulary_name, units, id);
    }
    
    protected ThreddsMetadata.Variables readVariables(final InvCatalog cat, final InvDataset ds, final Element varsElem) {
        if (varsElem == null) {
            return null;
        }
        final String vocab = varsElem.getAttributeValue("vocabulary");
        final String vocabHref = varsElem.getAttributeValue("href", InvCatalogFactory10.xlinkNS);
        URI vocabUri = null;
        if (vocabHref != null) {
            try {
                vocabUri = cat.resolveUri(vocabHref);
            }
            catch (Exception e) {
                this.factory.appendErr(" ** Invalid Variables vocabulary URI = " + vocabHref + " " + e.getMessage() + "\n");
            }
        }
        final List<Element> vlist = (List<Element>)varsElem.getChildren("variable", InvCatalogFactory10.defNS);
        String mapHref = null;
        URI mapUri = null;
        final Element map = varsElem.getChild("variableMap", InvCatalogFactory10.defNS);
        if (map != null) {
            mapHref = map.getAttributeValue("href", InvCatalogFactory10.xlinkNS);
            try {
                mapUri = cat.resolveUri(mapHref);
            }
            catch (Exception e2) {
                this.factory.appendErr(" ** Invalid Variables map URI = " + mapHref + " " + e2.getMessage() + "\n");
            }
        }
        if (mapUri != null && vlist.size() > 0) {
            this.factory.appendErr(" ** Catalog error: cant have variableMap and variable in same element (dataset = " + ds.getName() + "\n");
            mapUri = null;
        }
        final ThreddsMetadata.Variables variables = new ThreddsMetadata.Variables(vocab, vocabHref, vocabUri, mapHref, mapUri);
        for (final Element e3 : vlist) {
            final ThreddsMetadata.Variable v = this.readVariable(e3);
            variables.addVariable(v);
        }
        if (mapUri != null) {
            try {
                final Element varsElement = this.readContentFromURL(mapUri);
                final List<Element> list = (List<Element>)varsElement.getChildren("variable", InvCatalogFactory10.defNS);
                for (final Element e4 : list) {
                    final ThreddsMetadata.Variable v2 = this.readVariable(e4);
                    variables.addVariable(v2);
                }
            }
            catch (IOException e5) {
                InvCatalogFactory10.logger.warn("Failure reading vaiable mapUri ", e5);
            }
        }
        return variables;
    }
    
    public void writeXML(final InvCatalogImpl catalog, final OutputStream os, final boolean raw) throws IOException {
        this.raw = raw;
        this.writeXML(catalog, os);
        this.raw = false;
    }
    
    public void writeXML(final InvCatalogImpl catalog, final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.writeCatalog(catalog), os);
    }
    
    public Document writeCatalog(final InvCatalogImpl cat) {
        final Element rootElem = new Element("catalog", InvCatalogFactory10.defNS);
        final Document doc = new Document(rootElem);
        if (cat.getName() != null) {
            rootElem.setAttribute("name", cat.getName());
        }
        rootElem.setAttribute("version", this.version);
        rootElem.addNamespaceDeclaration(InvCatalogFactory10.xlinkNS);
        if (cat.getExpires() != null) {
            rootElem.setAttribute("expires", cat.getExpires().toString());
        }
        for (final InvService service : cat.getServices()) {
            rootElem.addContent(this.writeService(service));
        }
        if (this.raw) {
            for (final InvProperty p : cat.getDatasetRoots()) {
                rootElem.addContent(this.writeDatasetRoot(p));
            }
        }
        for (final InvProperty p : cat.getProperties()) {
            rootElem.addContent(this.writeProperty(p));
        }
        for (final InvDatasetImpl ds : cat.getDatasets()) {
            if (ds instanceof InvDatasetScan) {
                rootElem.addContent(this.writeDatasetScan((InvDatasetScan)ds));
            }
            else if (ds instanceof InvCatalogRef) {
                rootElem.addContent(this.writeCatalogRef((InvCatalogRef)ds));
            }
            else {
                rootElem.addContent(this.writeDataset(ds));
            }
        }
        return doc;
    }
    
    private Element writeAccess(final InvAccessImpl access) {
        final Element accessElem = new Element("access", InvCatalogFactory10.defNS);
        accessElem.setAttribute("urlPath", access.getUrlPath());
        if (access.getServiceName() != null) {
            accessElem.setAttribute("serviceName", access.getServiceName());
        }
        if (access.getDataFormatType() != null) {
            accessElem.setAttribute("dataFormat", access.getDataFormatType().toString());
        }
        if (access.hasDataSize()) {
            accessElem.addContent(this.writeDataSize(access.getDataSize()));
        }
        return accessElem;
    }
    
    private Element writeCatalogRef(final InvCatalogRef catRef) {
        final Element catrefElem = new Element("catalogRef", InvCatalogFactory10.defNS);
        catrefElem.setAttribute("href", catRef.getXlinkHref(), InvCatalogFactory10.xlinkNS);
        final String name = (catRef.getName() == null) ? "" : catRef.getName();
        catrefElem.setAttribute("title", name, InvCatalogFactory10.xlinkNS);
        if (catRef.getID() != null) {
            catrefElem.setAttribute("ID", catRef.getID());
        }
        if (catRef.getRestrictAccess() != null) {
            catrefElem.setAttribute("restrictAccess", catRef.getRestrictAccess());
        }
        catrefElem.setAttribute("name", "");
        return catrefElem;
    }
    
    protected Element writeContributor(final ThreddsMetadata.Contributor c) {
        final Element elem = new Element("contributor", InvCatalogFactory10.defNS);
        if (c.getRole() != null) {
            elem.setAttribute("role", c.getRole());
        }
        elem.setText(c.getName());
        return elem;
    }
    
    private Element writeControlledVocabulary(final ThreddsMetadata.Vocab v, final String name) {
        final Element elem = new Element(name, InvCatalogFactory10.defNS);
        if (v.getVocabulary() != null) {
            elem.setAttribute("vocabulary", v.getVocabulary());
        }
        elem.addContent(v.getText());
        return elem;
    }
    
    private Element writeDataset(final InvDatasetImpl ds) {
        final Element dsElem = new Element("dataset", InvCatalogFactory10.defNS);
        if (ds instanceof InvDatasetImplProxy) {
            dsElem.setAttribute("name", ((InvDatasetImplProxy)ds).getAliasName());
            dsElem.setAttribute("alias", ds.getID());
            return dsElem;
        }
        this.writeDatasetInfo(ds, dsElem, true, this.raw);
        return dsElem;
    }
    
    private Element writeDatasetFmrc(final InvDatasetFmrc ds) {
        Element dsElem;
        if (this.raw) {
            dsElem = new Element("datasetFmrc", InvCatalogFactory10.defNS);
            dsElem.setAttribute("name", ds.getName());
            dsElem.setAttribute("path", ds.getPath());
            if (ds.isRunsOnly()) {
                dsElem.setAttribute("runsOnly", "true");
            }
            this.writeDatasetInfo(ds, dsElem, false, true);
        }
        else {
            dsElem = this.writeCatalogRef(ds);
        }
        return dsElem;
    }
    
    private Element writeDatasetRoot(final InvProperty prop) {
        final Element drootElem = new Element("datasetRoot", InvCatalogFactory10.defNS);
        drootElem.setAttribute("path", prop.getName());
        drootElem.setAttribute("location", prop.getValue());
        return drootElem;
    }
    
    private Element writeDatasetScan(final InvDatasetScan ds) {
        Element dsElem;
        if (this.raw) {
            dsElem = new Element("datasetScan", InvCatalogFactory10.defNS);
            this.writeDatasetInfo(ds, dsElem, false, true);
            dsElem.setAttribute("path", ds.getPath());
            dsElem.setAttribute("location", ds.getScanLocation());
            if (ds.getCrDsClassName() != null) {
                final Element configElem = new Element("crawlableDatasetImpl", InvCatalogFactory10.defNS);
                configElem.setAttribute("className", ds.getCrDsClassName());
                if (ds.getCrDsConfigObj() != null && ds.getCrDsConfigObj() instanceof Element) {
                    configElem.addContent((Content)ds.getCrDsConfigObj());
                }
            }
            if (ds.getFilter() != null) {
                dsElem.addContent(this.writeDatasetScanFilter(ds.getFilter()));
            }
            dsElem.addContent(this.writeDatasetScanIdentifier(ds.getIdentifier()));
            if (ds.getNamer() != null) {
                dsElem.addContent(this.writeDatasetScanNamer(ds.getNamer()));
            }
            if (ds.getSorter() != null) {
                dsElem.addContent(this.writeDatasetScanSorter(ds.getSorter()));
            }
            if (!ds.getProxyDatasetHandlers().isEmpty()) {
                dsElem.addContent(this.writeDatasetScanAddProxies(ds.getProxyDatasetHandlers()));
            }
            if (ds.getAddDatasetSize()) {
                dsElem.addContent(new Element("addDatasetSize", InvCatalogFactory10.defNS));
            }
            if (ds.getChildEnhancerList() != null) {
                dsElem.addContent(this.writeDatasetScanEnhancer(ds.getChildEnhancerList()));
            }
        }
        else if (ds.isValid()) {
            dsElem = new Element("catalogRef", InvCatalogFactory10.defNS);
            this.writeDatasetInfo(ds, dsElem, false, false);
            dsElem.setAttribute("href", ds.getXlinkHref(), InvCatalogFactory10.xlinkNS);
            dsElem.setAttribute("title", ds.getName(), InvCatalogFactory10.xlinkNS);
            dsElem.setAttribute("name", "");
            dsElem.addContent(this.writeProperty(new InvProperty("DatasetScan", "true")));
        }
        else {
            dsElem = new Element("dataset", InvCatalogFactory10.defNS);
            dsElem.setAttribute("name", "** Misconfigured DatasetScan <" + ds.getPath() + "> **");
            dsElem.addContent(new Comment(ds.getInvalidMessage()));
        }
        return dsElem;
    }
    
    Element writeDatasetScanFilter(final CrawlableDatasetFilter filter) {
        final Element filterElem = new Element("filter", InvCatalogFactory10.defNS);
        if (filter.getClass().isAssignableFrom(MultiSelectorFilter.class)) {
            for (final MultiSelectorFilter.Selector curSelector : (List)filter.getConfigObject()) {
                Element curSelectorElem;
                if (curSelector.isIncluder()) {
                    curSelectorElem = new Element("include", InvCatalogFactory10.defNS);
                }
                else {
                    curSelectorElem = new Element("exclude", InvCatalogFactory10.defNS);
                }
                final CrawlableDatasetFilter curFilter = curSelector.getFilter();
                if (curFilter instanceof WildcardMatchOnNameFilter) {
                    curSelectorElem.setAttribute("wildcard", ((WildcardMatchOnNameFilter)curFilter).getWildcardString());
                    curSelectorElem.setAttribute("atomic", curSelector.isApplyToAtomicDataset() ? "true" : "false");
                    curSelectorElem.setAttribute("collection", curSelector.isApplyToCollectionDataset() ? "true" : "false");
                }
                else if (curFilter instanceof RegExpMatchOnNameFilter) {
                    curSelectorElem.setAttribute("regExp", ((RegExpMatchOnNameFilter)curFilter).getRegExpString());
                    curSelectorElem.setAttribute("atomic", curSelector.isApplyToAtomicDataset() ? "true" : "false");
                    curSelectorElem.setAttribute("collection", curSelector.isApplyToCollectionDataset() ? "true" : "false");
                }
                else if (curFilter instanceof LastModifiedLimitFilter) {
                    curSelectorElem.setAttribute("lastModLimitInMillis", Long.toString(((LastModifiedLimitFilter)curFilter).getLastModifiedLimitInMillis()));
                    curSelectorElem.setAttribute("atomic", curSelector.isApplyToAtomicDataset() ? "true" : "false");
                    curSelectorElem.setAttribute("collection", curSelector.isApplyToCollectionDataset() ? "true" : "false");
                }
                else {
                    curSelectorElem.addContent(new Comment("Unknown selector type <" + curSelector.getClass().getName() + ">."));
                }
                filterElem.addContent(curSelectorElem);
            }
        }
        else {
            filterElem.addContent(this.writeDatasetScanUserDefined("crawlableDatasetFilterImpl", filter.getClass().getName(), filter.getConfigObject()));
        }
        return filterElem;
    }
    
    private Element writeDatasetScanNamer(final CrawlableDatasetLabeler namer) {
        Element namerElem = null;
        if (namer != null) {
            namerElem = new Element("namer", InvCatalogFactory10.defNS);
            if (namer instanceof MultiLabeler) {
                for (final CrawlableDatasetLabeler curNamer : ((MultiLabeler)namer).getLabelerList()) {
                    if (curNamer instanceof RegExpAndReplaceOnNameLabeler) {
                        final Element curNamerElem = new Element("regExpOnName", InvCatalogFactory10.defNS);
                        curNamerElem.setAttribute("regExp", ((RegExpAndReplaceOnNameLabeler)curNamer).getRegExp());
                        curNamerElem.setAttribute("replaceString", ((RegExpAndReplaceOnNameLabeler)curNamer).getReplaceString());
                        namerElem.addContent(curNamerElem);
                    }
                    else if (curNamer instanceof RegExpAndReplaceOnPathLabeler) {
                        final Element curNamerElem = new Element("regExpOnPath", InvCatalogFactory10.defNS);
                        curNamerElem.setAttribute("regExp", ((RegExpAndReplaceOnPathLabeler)curNamer).getRegExp());
                        curNamerElem.setAttribute("replaceString", ((RegExpAndReplaceOnPathLabeler)curNamer).getReplaceString());
                        namerElem.addContent(curNamerElem);
                    }
                    else {
                        final String tmpMsg = "writeDatasetScanNamer(): unsupported namer <" + curNamer.getClass().getName() + ">.";
                        InvCatalogFactory10.logger.warn(tmpMsg);
                        namerElem.addContent(new Comment(tmpMsg));
                    }
                }
            }
            else {
                namerElem.addContent(this.writeDatasetScanUserDefined("crawlableDatasetLabelerImpl", namer.getClass().getName(), namer.getConfigObject()));
            }
        }
        return namerElem;
    }
    
    private Element writeDatasetScanIdentifier(final CrawlableDatasetLabeler identifier) {
        Element identifierElem = new Element("addID", InvCatalogFactory10.defNS);
        if (identifier != null) {
            if (identifier instanceof SimpleLatestProxyDsHandler) {
                return identifierElem;
            }
            identifierElem = new Element("addID", InvCatalogFactory10.defNS);
            identifierElem.addContent(this.writeDatasetScanUserDefined("crawlableDatasetLabelerImpl", identifier.getClass().getName(), identifier.getConfigObject()));
        }
        return identifierElem;
    }
    
    private Element writeDatasetScanAddProxies(final Map proxyDsHandlers) {
        if (proxyDsHandlers.size() == 1 && proxyDsHandlers.containsKey("latest.xml")) {
            final Object o = proxyDsHandlers.get("latest.xml");
            if (o instanceof SimpleLatestProxyDsHandler) {
                final SimpleLatestProxyDsHandler pdh = (SimpleLatestProxyDsHandler)o;
                final String name = pdh.getProxyDatasetName();
                final boolean top = pdh.isLocateAtTopOrBottom();
                final String serviceName = pdh.getProxyDatasetService(null).getName();
                final Element addProxiesElem = new Element("addLatest", InvCatalogFactory10.defNS);
                if (name.equals("latest.xml") && top && serviceName.equals("latest")) {
                    return addProxiesElem;
                }
                final Element simpleLatestElem = new Element("simpleLatest", InvCatalogFactory10.defNS);
                simpleLatestElem.setAttribute("name", name);
                simpleLatestElem.setAttribute("top", top ? "true" : "false");
                simpleLatestElem.setAttribute("servicName", serviceName);
                addProxiesElem.addContent(simpleLatestElem);
                return addProxiesElem;
            }
        }
        final Element addProxiesElem = new Element("addProxies", InvCatalogFactory10.defNS);
        for (final String curName : proxyDsHandlers.keySet()) {
            final ProxyDatasetHandler curPdh = proxyDsHandlers.get(curName);
            if (curPdh instanceof SimpleLatestProxyDsHandler) {
                final SimpleLatestProxyDsHandler sPdh = (SimpleLatestProxyDsHandler)curPdh;
                final Element simpleLatestElem2 = new Element("simpleLatest", InvCatalogFactory10.defNS);
                simpleLatestElem2.setAttribute("name", sPdh.getProxyDatasetName());
                simpleLatestElem2.setAttribute("top", sPdh.isLocateAtTopOrBottom() ? "true" : "false");
                simpleLatestElem2.setAttribute("servicName", sPdh.getProxyDatasetService(null).getName());
                addProxiesElem.addContent(simpleLatestElem2);
            }
            else if (curPdh instanceof LatestCompleteProxyDsHandler) {
                final LatestCompleteProxyDsHandler lcPdh = (LatestCompleteProxyDsHandler)curPdh;
                final Element latestElem = new Element("latestComplete", InvCatalogFactory10.defNS);
                latestElem.setAttribute("name", lcPdh.getProxyDatasetName());
                latestElem.setAttribute("top", lcPdh.isLocateAtTopOrBottom() ? "true" : "false");
                latestElem.setAttribute("servicName", lcPdh.getProxyDatasetService(null).getName());
                latestElem.setAttribute("lastModifiedLimit", Long.toString(lcPdh.getLastModifiedLimit()));
                addProxiesElem.addContent(latestElem);
            }
            else {
                InvCatalogFactory10.logger.warn("writeDatasetScanAddProxies(): unknown type of ProxyDatasetHandler <" + curPdh.getProxyDatasetName() + ">.");
            }
        }
        return addProxiesElem;
    }
    
    private Element writeDatasetScanSorter(final CrawlableDatasetSorter sorter) {
        final Element sorterElem = new Element("sort", InvCatalogFactory10.defNS);
        if (sorter instanceof LexigraphicByNameSorter) {
            final Element lexElem = new Element("lexigraphicByName", InvCatalogFactory10.defNS);
            lexElem.setAttribute("increasing", ((LexigraphicByNameSorter)sorter).isIncreasing() ? "true" : "false");
            sorterElem.addContent(lexElem);
        }
        else {
            sorterElem.addContent(this.writeDatasetScanUserDefined("crawlableDatasetSorterImpl", sorter.getClass().getName(), sorter.getConfigObject()));
        }
        return sorterElem;
    }
    
    private List writeDatasetScanEnhancer(final List enhancerList) {
        final List enhancerElemList = new ArrayList();
        int timeCovCount = 0;
        for (final DatasetEnhancer curEnhancer : enhancerList) {
            if (curEnhancer instanceof RegExpAndDurationTimeCoverageEnhancer) {
                if (timeCovCount > 0) {
                    InvCatalogFactory10.logger.warn("writeDatasetScanEnhancer(): More than one addTimeCoverage element, skipping.");
                }
                else {
                    ++timeCovCount;
                    final Element timeCovElem = new Element("addTimeCoverage", InvCatalogFactory10.defNS);
                    final RegExpAndDurationTimeCoverageEnhancer timeCovEnhancer = (RegExpAndDurationTimeCoverageEnhancer)curEnhancer;
                    timeCovElem.setAttribute("datasetNameMatchPattern", timeCovEnhancer.getMatchPattern());
                    timeCovElem.setAttribute("startTimeSubstitutionPattern", timeCovEnhancer.getSubstitutionPattern());
                    timeCovElem.setAttribute("duration", timeCovEnhancer.getDuration());
                    enhancerElemList.add(timeCovElem);
                }
            }
            else {
                enhancerElemList.add(this.writeDatasetScanUserDefined("datasetEnhancerImpl", curEnhancer.getClass().getName(), curEnhancer.getConfigObject()));
            }
        }
        return enhancerElemList;
    }
    
    private Element writeDatasetScanUserDefined(final String userDefName, final String className, final Object configObj) {
        final Element userDefElem = new Element(userDefName, InvCatalogFactory10.defNS);
        userDefElem.setAttribute("className", className);
        if (configObj != null) {
            if (configObj instanceof Element) {
                userDefElem.addContent((Content)configObj);
            }
            else {
                userDefElem.addContent(new Comment("This class <" + className + "> not yet supported. This XML is missing configuration information (of type " + configObj.getClass().getName() + ")."));
            }
        }
        return userDefElem;
    }
    
    private void writeDatasetInfo(final InvDatasetImpl ds, final Element dsElem, final boolean doNestedDatasets, final boolean showNcML) {
        dsElem.setAttribute("name", ds.getName());
        if (ds.getCollectionType() != null && ds.getCollectionType() != CollectionType.NONE) {
            dsElem.setAttribute("collectionType", ds.getCollectionType().toString());
        }
        if (ds.isHarvest()) {
            dsElem.setAttribute("harvest", "true");
        }
        if (ds.getID() != null) {
            dsElem.setAttribute("ID", ds.getID());
        }
        if (ds.getUrlPath() != null) {
            dsElem.setAttribute("urlPath", ds.getUrlPath());
        }
        if (ds.getRestrictAccess() != null) {
            dsElem.setAttribute("restrictAccess", ds.getRestrictAccess());
        }
        for (final InvService service : ds.getServicesLocal()) {
            dsElem.addContent(this.writeService(service));
        }
        this.writeThreddsMetadata(dsElem, ds.getLocalMetadata());
        this.writeInheritedMetadata(dsElem, ds.getLocalMetadataInheritable());
        for (final InvAccessImpl a : ds.getAccessLocal()) {
            dsElem.addContent(this.writeAccess(a));
        }
        if (showNcML && ds.getNcmlElement() != null) {
            final Element ncml = (Element)ds.getNcmlElement().clone();
            ncml.detach();
            dsElem.addContent(ncml);
        }
        if (!doNestedDatasets) {
            return;
        }
        for (final InvDatasetImpl nested : ds.getDatasets()) {
            if (nested instanceof InvDatasetScan) {
                dsElem.addContent(this.writeDatasetScan((InvDatasetScan)nested));
            }
            else if (nested instanceof InvDatasetFmrc) {
                dsElem.addContent(this.writeDatasetFmrc((InvDatasetFmrc)nested));
            }
            else if (nested instanceof InvCatalogRef) {
                dsElem.addContent(this.writeCatalogRef((InvCatalogRef)nested));
            }
            else {
                dsElem.addContent(this.writeDataset(nested));
            }
        }
    }
    
    protected Element writeDate(final String name, final DateType date) {
        final Element dateElem = new Element(name, InvCatalogFactory10.defNS);
        dateElem.addContent(date.getText());
        if (date.getType() != null) {
            dateElem.setAttribute("type", date.getType());
        }
        if (date.getFormat() != null) {
            dateElem.setAttribute("format", date.getFormat());
        }
        return dateElem;
    }
    
    private Element writeDocumentation(final InvDocumentation doc, final String name) {
        final Element docElem = new Element(name, InvCatalogFactory10.defNS);
        if (doc.getType() != null) {
            docElem.setAttribute("type", doc.getType());
        }
        if (doc.hasXlink()) {
            docElem.setAttribute("href", doc.getXlinkHref(), InvCatalogFactory10.xlinkNS);
            if (!doc.getXlinkTitle().equals(doc.getURI().toString())) {
                docElem.setAttribute("title", doc.getXlinkTitle(), InvCatalogFactory10.xlinkNS);
            }
        }
        final String inline = doc.getInlineContent();
        if (inline != null) {
            docElem.addContent(inline);
        }
        return docElem;
    }
    
    public Element writeGeospatialCoverage(final ThreddsMetadata.GeospatialCoverage gc) {
        final Element elem = new Element("geospatialCoverage", InvCatalogFactory10.defNS);
        if (gc.getZPositive().equals("down")) {
            elem.setAttribute("zpositive", gc.getZPositive());
        }
        if (gc.getNorthSouthRange() != null) {
            this.writeGeospatialRange(elem, new Element("northsouth", InvCatalogFactory10.defNS), gc.getNorthSouthRange());
        }
        if (gc.getEastWestRange() != null) {
            this.writeGeospatialRange(elem, new Element("eastwest", InvCatalogFactory10.defNS), gc.getEastWestRange());
        }
        if (gc.getUpDownRange() != null) {
            this.writeGeospatialRange(elem, new Element("updown", InvCatalogFactory10.defNS), gc.getUpDownRange());
        }
        final List<ThreddsMetadata.Vocab> names = gc.getNames();
        final ThreddsMetadata.Vocab global = new ThreddsMetadata.Vocab("global", null);
        if (gc.isGlobal() && !names.contains(global)) {
            names.add(global);
        }
        else if (!gc.isGlobal() && names.contains(global)) {
            names.remove(global);
        }
        for (final ThreddsMetadata.Vocab name : names) {
            elem.addContent(this.writeControlledVocabulary(name, "name"));
        }
        return elem;
    }
    
    private void writeGeospatialRange(final Element parent, final Element elem, final ThreddsMetadata.Range r) {
        if (r == null) {
            return;
        }
        elem.addContent(new Element("start", InvCatalogFactory10.defNS).setText(Double.toString(r.getStart())));
        elem.addContent(new Element("size", InvCatalogFactory10.defNS).setText(Double.toString(r.getSize())));
        if (r.hasResolution()) {
            elem.addContent(new Element("resolution", InvCatalogFactory10.defNS).setText(Double.toString(r.getResolution())));
        }
        if (r.getUnits() != null) {
            elem.addContent(new Element("units", InvCatalogFactory10.defNS).setText(r.getUnits()));
        }
        parent.addContent(elem);
    }
    
    private Element writeMetadata(final InvMetadata mdata) {
        final Element mdataElem = new Element("metadata", InvCatalogFactory10.defNS);
        if (mdata.getMetadataType() != null) {
            mdataElem.setAttribute("metadataType", mdata.getMetadataType());
        }
        if (mdata.isInherited()) {
            mdataElem.setAttribute("inherited", "true");
        }
        final String ns = mdata.getNamespaceURI();
        if (ns != null && !ns.equals("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0")) {
            final Namespace mdataNS = Namespace.getNamespace(mdata.getNamespacePrefix(), ns);
            mdataElem.addNamespaceDeclaration(mdataNS);
        }
        if (mdata.hasXlink()) {
            mdataElem.setAttribute("href", mdata.getXlinkHref(), InvCatalogFactory10.xlinkNS);
            if (mdata.getXlinkTitle() != null) {
                mdataElem.setAttribute("title", mdata.getXlinkTitle(), InvCatalogFactory10.xlinkNS);
            }
        }
        else if (mdata.getThreddsMetadata() != null) {
            this.writeThreddsMetadata(mdataElem, mdata.getThreddsMetadata());
        }
        else {
            final MetadataConverterIF converter = mdata.getConverter();
            if (converter != null && mdata.getContentObject() != null) {
                if (mdata.getContentObject() instanceof Element) {
                    final Element mdataOrg = (Element)mdata.getContentObject();
                    final List<Element> children = (List<Element>)mdataOrg.getChildren();
                    for (final Element child : children) {
                        mdataElem.addContent((Content)child.clone());
                    }
                }
                else {
                    converter.addMetadataContent(mdataElem, mdata.getContentObject());
                    mdataElem.detach();
                }
            }
        }
        return mdataElem;
    }
    
    private Element writeProperty(final InvProperty prop) {
        final Element propElem = new Element("property", InvCatalogFactory10.defNS);
        propElem.setAttribute("name", prop.getName());
        propElem.setAttribute("value", prop.getValue());
        return propElem;
    }
    
    protected Element writeSource(final String elementName, final ThreddsMetadata.Source p) {
        final Element elem = new Element(elementName, InvCatalogFactory10.defNS);
        elem.addContent(this.writeControlledVocabulary(p.getNameVocab(), "name"));
        final Element contact = new Element("contact", InvCatalogFactory10.defNS);
        if (p.getUrl() != null) {
            contact.setAttribute("url", p.getUrl());
        }
        if (p.getEmail() != null) {
            contact.setAttribute("email", p.getEmail());
        }
        elem.addContent(contact);
        return elem;
    }
    
    private Element writeService(final InvService service) {
        final Element serviceElem = new Element("service", InvCatalogFactory10.defNS);
        serviceElem.setAttribute("name", service.getName());
        serviceElem.setAttribute("serviceType", service.getServiceType().toString());
        serviceElem.setAttribute("base", service.getBase());
        if (service.getSuffix() != null && service.getSuffix().length() > 0) {
            serviceElem.setAttribute("suffix", service.getSuffix());
        }
        for (final InvProperty p : service.getProperties()) {
            serviceElem.addContent(this.writeProperty(p));
        }
        for (final InvService nested : service.getServices()) {
            serviceElem.addContent(this.writeService(nested));
        }
        if (this.raw) {
            for (final InvProperty p2 : service.getDatasetRoots()) {
                serviceElem.addContent(this.writeDatasetRoot(p2));
            }
        }
        return serviceElem;
    }
    
    private Element writeDataSize(double size) {
        final Element sizeElem = new Element("dataSize", InvCatalogFactory10.defNS);
        if (InvCatalogFactory10.useBytesForDataSize) {
            sizeElem.setAttribute("units", "bytes");
            final long bytes = (long)size;
            sizeElem.setText(Long.toString(bytes));
            return sizeElem;
        }
        String unit;
        if (size > 1.0E15) {
            unit = "Pbytes";
            size *= 1.0E-15;
        }
        else if (size > 1.0E12) {
            unit = "Tbytes";
            size *= 1.0E-12;
        }
        else if (size > 1.0E9) {
            unit = "Gbytes";
            size *= 1.0E-9;
        }
        else if (size > 1000000.0) {
            unit = "Mbytes";
            size *= 1.0E-6;
        }
        else if (size > 1000.0) {
            unit = "Kbytes";
            size *= 0.001;
        }
        else {
            unit = "bytes";
        }
        sizeElem.setAttribute("units", unit);
        sizeElem.setText(ucar.unidata.util.Format.d(size, 4));
        return sizeElem;
    }
    
    protected void writeInheritedMetadata(final Element elem, final ThreddsMetadata tmi) {
        final Element mdataElem = new Element("metadata", InvCatalogFactory10.defNS);
        mdataElem.setAttribute("inherited", "true");
        this.writeThreddsMetadata(mdataElem, tmi);
        if (mdataElem.getChildren().size() > 0) {
            elem.addContent(mdataElem);
        }
    }
    
    protected void writeThreddsMetadata(final Element elem, final ThreddsMetadata tmg) {
        if (tmg.getServiceName() != null) {
            final Element serviceNameElem = new Element("serviceName", InvCatalogFactory10.defNS);
            serviceNameElem.setText(tmg.getServiceName());
            elem.addContent(serviceNameElem);
        }
        if (tmg.getAuthority() != null) {
            final Element authElem = new Element("authority", InvCatalogFactory10.defNS);
            authElem.setText(tmg.getAuthority());
            elem.addContent(authElem);
        }
        if (tmg.getDataType() != null && tmg.getDataType() != FeatureType.NONE && tmg.getDataType() != FeatureType.ANY) {
            final Element dataTypeElem = new Element("dataType", InvCatalogFactory10.defNS);
            dataTypeElem.setText(tmg.getDataType().toString());
            elem.addContent(dataTypeElem);
        }
        if (tmg.getDataFormatType() != null && tmg.getDataFormatType() != DataFormatType.NONE) {
            final Element dataFormatElem = new Element("dataFormat", InvCatalogFactory10.defNS);
            dataFormatElem.setText(tmg.getDataFormatType().toString());
            elem.addContent(dataFormatElem);
        }
        if (tmg.hasDataSize()) {
            elem.addContent(this.writeDataSize(tmg.getDataSize()));
        }
        final List<InvDocumentation> docList = tmg.getDocumentation();
        for (final InvDocumentation doc : docList) {
            elem.addContent(this.writeDocumentation(doc, "documentation"));
        }
        final List<ThreddsMetadata.Contributor> contribList = tmg.getContributors();
        for (final ThreddsMetadata.Contributor c : contribList) {
            elem.addContent(this.writeContributor(c));
        }
        final List<ThreddsMetadata.Source> creatorList = tmg.getCreators();
        for (final ThreddsMetadata.Source p : creatorList) {
            elem.addContent(this.writeSource("creator", p));
        }
        final List<ThreddsMetadata.Vocab> kewordList = tmg.getKeywords();
        for (final ThreddsMetadata.Vocab v : kewordList) {
            elem.addContent(this.writeControlledVocabulary(v, "keyword"));
        }
        final List<InvMetadata> mdList = tmg.getMetadata();
        for (final InvMetadata m : mdList) {
            elem.addContent(this.writeMetadata(m));
        }
        final List<ThreddsMetadata.Vocab> projList = tmg.getProjects();
        for (final ThreddsMetadata.Vocab v2 : projList) {
            elem.addContent(this.writeControlledVocabulary(v2, "project"));
        }
        final List<InvProperty> propertyList = tmg.getProperties();
        for (final InvProperty p2 : propertyList) {
            elem.addContent(this.writeProperty(p2));
        }
        final List<ThreddsMetadata.Source> pubList = tmg.getPublishers();
        for (final ThreddsMetadata.Source p3 : pubList) {
            elem.addContent(this.writeSource("publisher", p3));
        }
        final List<DateType> dateList = tmg.getDates();
        for (final DateType d : dateList) {
            elem.addContent(this.writeDate("date", d));
        }
        final ThreddsMetadata.GeospatialCoverage gc = tmg.getGeospatialCoverage();
        if (gc != null && !gc.isEmpty()) {
            elem.addContent(this.writeGeospatialCoverage(gc));
        }
        final DateRange tc = tmg.getTimeCoverage();
        if (tc != null) {
            elem.addContent(this.writeTimeCoverage(tc));
        }
        final List<ThreddsMetadata.Variables> varList = tmg.getVariables();
        for (final ThreddsMetadata.Variables v3 : varList) {
            elem.addContent(this.writeVariables(v3));
        }
    }
    
    protected Element writeTimeCoverage(final DateRange t) {
        final Element elem = new Element("timeCoverage", InvCatalogFactory10.defNS);
        final DateType start = t.getStart();
        final DateType end = t.getEnd();
        final TimeDuration duration = t.getDuration();
        final TimeDuration resolution = t.getResolution();
        if (t.useStart() && start != null && !start.isBlank()) {
            final Element startElem = new Element("start", InvCatalogFactory10.defNS);
            startElem.setText(start.toString());
            elem.addContent(startElem);
        }
        if (t.useEnd() && end != null && !end.isBlank()) {
            final Element telem = new Element("end", InvCatalogFactory10.defNS);
            telem.setText(end.toString());
            elem.addContent(telem);
        }
        if (t.useDuration() && duration != null && !duration.isBlank()) {
            final Element telem = new Element("duration", InvCatalogFactory10.defNS);
            telem.setText(duration.toString());
            elem.addContent(telem);
        }
        if (t.useResolution() && resolution != null && !resolution.isBlank()) {
            final Element telem = new Element("resolution", InvCatalogFactory10.defNS);
            telem.setText(t.getResolution().toString());
            elem.addContent(telem);
        }
        return elem;
    }
    
    protected Element writeVariable(final ThreddsMetadata.Variable v) {
        final Element elem = new Element("variable", InvCatalogFactory10.defNS);
        if (v.getName() != null) {
            elem.setAttribute("name", v.getName());
        }
        if (v.getDescription() != null) {
            final String desc = v.getDescription().trim();
            if (desc.length() > 0) {
                elem.setText(v.getDescription());
            }
        }
        if (v.getVocabularyName() != null) {
            elem.setAttribute("vocabulary_name", v.getVocabularyName());
        }
        if (v.getUnits() != null) {
            elem.setAttribute("units", v.getUnits());
        }
        final String id = v.getVocabularyId();
        if (id != null) {
            elem.setAttribute("vocabulary_id", id);
        }
        return elem;
    }
    
    protected Element writeVariables(final ThreddsMetadata.Variables vs) {
        final Element elem = new Element("variables", InvCatalogFactory10.defNS);
        if (vs.getVocabulary() != null) {
            elem.setAttribute("vocabulary", vs.getVocabulary());
        }
        if (vs.getVocabHref() != null) {
            elem.setAttribute("href", vs.getVocabHref(), InvCatalogFactory10.xlinkNS);
        }
        if (vs.getMapHref() != null) {
            final Element mapElem = new Element("variableMap", InvCatalogFactory10.defNS);
            mapElem.setAttribute("href", vs.getMapHref(), InvCatalogFactory10.xlinkNS);
            elem.addContent(mapElem);
        }
        else {
            final List<ThreddsMetadata.Variable> varList = vs.getVariableList();
            for (final ThreddsMetadata.Variable v : varList) {
                elem.addContent(this.writeVariable(v));
            }
        }
        return elem;
    }
    
    static {
        InvCatalogFactory10.logger = LoggerFactory.getLogger(InvCatalogFactory10.class);
        defNS = Namespace.getNamespace("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
        ncmlNS = Namespace.getNamespace("ncml", "http://www.unidata.ucar.edu/namespaces/netcdf/ncml-2.2");
        InvCatalogFactory10.useBytesForDataSize = false;
    }
}
