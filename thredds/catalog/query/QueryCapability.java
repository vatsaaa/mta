// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.util.ArrayList;

public class QueryCapability
{
    private String createFrom;
    private String name;
    private String version;
    private Query query;
    private ArrayList selectors;
    private ArrayList uniqueSelectors;
    private ArrayList userInterfaces;
    private Selector ss;
    private boolean fatalError;
    private StringBuffer errLog;
    private volatile int hashCode;
    
    QueryCapability() {
        this.selectors = new ArrayList();
        this.uniqueSelectors = new ArrayList();
        this.userInterfaces = new ArrayList();
        this.ss = null;
        this.fatalError = false;
        this.errLog = new StringBuffer();
        this.hashCode = 0;
    }
    
    public QueryCapability(final String urlString, final String name, final String version) {
        this.selectors = new ArrayList();
        this.uniqueSelectors = new ArrayList();
        this.userInterfaces = new ArrayList();
        this.ss = null;
        this.fatalError = false;
        this.errLog = new StringBuffer();
        this.hashCode = 0;
        this.createFrom = urlString;
        this.name = name;
        this.version = version;
    }
    
    public void appendErrorMessage(final String message, final boolean fatal) {
        this.errLog.append(message);
        this.errLog.append("\n");
        this.fatalError = (this.fatalError || fatal);
    }
    
    public String getErrorMessages() {
        return this.errLog.toString();
    }
    
    public boolean hasFatalError() {
        return this.fatalError;
    }
    
    public void addSelector(final Selector s) {
        this.selectors.add(s);
    }
    
    public void setQuery(final Query q) {
        this.query = q;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    public Query getQuery() {
        return this.query;
    }
    
    public ArrayList getSelectors() {
        return this.selectors;
    }
    
    public ArrayList getAllUniqueSelectors() {
        return this.uniqueSelectors;
    }
    
    public String getCreateFrom() {
        return this.createFrom;
    }
    
    public Selector getServiceSelector() {
        return this.ss;
    }
    
    public void setServiceSelector(final Selector ss) {
        this.ss = ss;
    }
    
    public void addUserInterface(final Object s) {
        this.userInterfaces.add(s);
    }
    
    public ArrayList getUserInterfaces() {
        return this.userInterfaces;
    }
    
    @Override
    public String toString() {
        return "name:<" + this.name + ">";
    }
    
    public void addUniqueSelector(final Selector s) {
        if (!this.uniqueSelectors.contains(s)) {
            this.uniqueSelectors.add(s);
        }
        this.selectors.add(s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof QueryCapability && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getQuery().hashCode();
            result = 37 * result + this.getSelectors().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
