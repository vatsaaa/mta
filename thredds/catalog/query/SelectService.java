// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.util.ArrayList;

public class SelectService extends Selector
{
    private ArrayList choices;
    private volatile int hashCode;
    
    public SelectService(final String id, final String title) {
        this.choices = new ArrayList();
        this.hashCode = 0;
        this.setId(id);
        this.setTitle(title);
    }
    
    public void addServiceChoice(final String service, final String title, final String dataFormat, final String returns, final String value) {
        this.choices.add(new ServiceChoice(service, title, dataFormat, returns, value));
    }
    
    public ArrayList getChoices() {
        return this.choices;
    }
    
    public int getSize() {
        return this.choices.size();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof SelectService && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.getTitle() != null) {
                result = 37 * result + this.getTitle().hashCode();
            }
            if (this.getId() != null) {
                result = 37 * result + this.getId().hashCode();
            }
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.isRequired()) {
                ++result;
            }
            if (this.isMultiple()) {
                ++result;
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    public class ServiceChoice implements Choice
    {
        private String service;
        private String title;
        private String dataFormat;
        private String returns;
        private String value;
        private volatile int hashCode;
        
        ServiceChoice(final String service, final String title, final String dataFormat, final String returns, final String value) {
            this.hashCode = 0;
            this.service = service;
            this.title = title;
            this.dataFormat = dataFormat;
            this.returns = returns;
            this.value = value;
        }
        
        public String getService() {
            return this.service;
        }
        
        public String getTitle() {
            return this.title;
        }
        
        public String getDataFormat() {
            return this.dataFormat;
        }
        
        public String getReturns() {
            return this.returns;
        }
        
        @Override
        public String toString() {
            return (this.title != null) ? this.title : this.service;
        }
        
        public String getValue2() {
            return this.value;
        }
        
        public String getValue() {
            return (this.value != null) ? this.value : this.service;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof ServiceChoice && o.hashCode() == this.hashCode());
        }
        
        @Override
        public int hashCode() {
            if (this.hashCode == 0) {
                int result = 17;
                if (this.getService() != null) {
                    result = 37 * result + this.getService().hashCode();
                }
                if (this.getTitle() != null) {
                    result = 37 * result + this.getTitle().hashCode();
                }
                if (this.getDataFormat() != null) {
                    result = 37 * result + this.getDataFormat().hashCode();
                }
                if (this.getReturns() != null) {
                    result = 37 * result + this.getReturns().hashCode();
                }
                if (this.getValue() != null) {
                    result = 37 * result + this.getValue().hashCode();
                }
                this.hashCode = result;
            }
            return this.hashCode;
        }
    }
}
