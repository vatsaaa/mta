// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

public class Station extends ListChoice
{
    private Location location;
    private String state;
    private String country;
    private volatile int hashCode;
    
    public Station(final Selector parent, final String name, final String value, final String description) {
        super(parent, name, value, description);
        this.state = null;
        this.country = null;
        this.hashCode = 0;
    }
    
    public Station(final Selector parent, final String name, final String value, final String state, final String country, final String description) {
        super(parent, name, value, description);
        this.state = null;
        this.country = null;
        this.hashCode = 0;
        this.state = state;
        this.country = country;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    public Location getLocation() {
        return this.location;
    }
    
    public double getLatitude() {
        return this.location.getLatitude();
    }
    
    public double getLongitude() {
        return this.location.getLongitude();
    }
    
    public double getElevation() {
        if (this.location.hasElevation()) {
            return this.location.getElevation();
        }
        return Double.NaN;
    }
    
    public String getStationName() {
        return this.getName();
    }
    
    public String getStationID() {
        return this.getValue();
    }
    
    public String getState() {
        return this.state;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Station && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getValue().hashCode();
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.getDescription() != null) {
                result = 37 * result + this.getDescription().hashCode();
            }
            result = 37 * result + this.getLocation().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
