// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import ucar.unidata.util.StringUtil;
import java.util.Iterator;
import thredds.catalog.InvDocumentation;
import java.util.List;
import java.util.ArrayList;

public abstract class Selector
{
    protected ArrayList children;
    protected List compound;
    protected boolean isUsed;
    protected String title;
    protected String id;
    protected String template;
    protected boolean required;
    protected boolean multiple;
    protected InvDocumentation desc;
    
    protected Selector() {
        this.children = new ArrayList();
    }
    
    protected Selector(final String title, final String id, final String template, final String required, final String multiple) {
        this.children = new ArrayList();
        this.title = title;
        this.id = id;
        this.template = template;
        this.required = (required == null || !required.equals("false"));
        this.multiple = (multiple != null && multiple.equals("true"));
    }
    
    public void setDescription(final InvDocumentation desc) {
        this.desc = desc;
    }
    
    public InvDocumentation getDescription() {
        return this.desc;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getTemplate() {
        return this.template;
    }
    
    public void setTemplate(final String template) {
        this.template = template;
    }
    
    public boolean isRequired() {
        return this.required;
    }
    
    public void setRequired(final String required) {
        this.required = (required == null || !required.equals("false"));
    }
    
    public boolean isMultiple() {
        return this.multiple;
    }
    
    public void setMultiple(final String multiple) {
        this.multiple = (multiple != null && multiple.equals("true"));
    }
    
    public String getSelectType() {
        return this.multiple ? "multiple" : "single";
    }
    
    public void setCompoundSelectors(final List compound) {
        this.compound = compound;
    }
    
    public boolean isUsed() {
        return this.isUsed;
    }
    
    public void setUsed(final boolean isUsed) {
        this.isUsed = isUsed;
        if (isUsed && this.compound != null) {
            for (final Selector s : this.compound) {
                if (s != this) {
                    s.setUsed(false);
                }
            }
        }
    }
    
    public void appendQuery(final StringBuffer sbuff, final ArrayList values) {
        if (this.template != null) {
            this.appendQueryFromTemplate(sbuff, values);
        }
        else {
            this.appendQueryFromParamValue(sbuff, values);
        }
    }
    
    private void appendQueryFromParamValue(final StringBuffer sbuff, final ArrayList choices) {
        for (int i = 1; i < choices.size(); i += 2) {
            sbuff.append(this.getId());
            sbuff.append("=");
            sbuff.append(choices.get(i).toString());
            sbuff.append("&");
        }
    }
    
    private void appendQueryFromTemplate(final StringBuffer sbuff, final ArrayList choices) {
        final StringBuffer templateBuff = new StringBuffer(this.template);
        for (int i = 0; i < choices.size(); i += 2) {
            StringUtil.substitute(templateBuff, choices.get(i).toString(), choices.get(i + 1).toString());
        }
        sbuff.append(templateBuff.toString());
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Selector && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (null != this.getId()) {
            return this.getId().hashCode();
        }
        return super.hashCode();
    }
}
