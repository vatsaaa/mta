// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

public class SelectGeoRegion extends Selector
{
    private Location lowerLeft;
    private Location upperRight;
    private volatile int hashCode;
    
    public SelectGeoRegion(final Location lowerLeft, final Location upperRight) {
        this.hashCode = 0;
        this.lowerLeft = lowerLeft;
        this.upperRight = upperRight;
    }
    
    public Location getLowerLeft() {
        return this.lowerLeft;
    }
    
    public Location getUpperRight() {
        return this.upperRight;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof SelectGeoRegion && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.getTitle() != null) {
                result = 37 * result + this.getTitle().hashCode();
            }
            if (this.getId() != null) {
                result = 37 * result + this.getId().hashCode();
            }
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.isRequired()) {
                ++result;
            }
            if (this.isMultiple()) {
                ++result;
            }
            if (this.getLowerLeft() != null) {
                result = 37 * result + this.getLowerLeft().hashCode();
            }
            if (this.getUpperRight() != null) {
                result = 37 * result + this.getUpperRight().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
