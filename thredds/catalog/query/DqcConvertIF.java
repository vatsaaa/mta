// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.io.OutputStream;
import java.io.IOException;
import java.net.URI;
import org.jdom.Document;

public interface DqcConvertIF
{
    QueryCapability parseXML(final DqcFactory p0, final Document p1, final URI p2) throws IOException;
    
    void writeXML(final QueryCapability p0, final OutputStream p1) throws IOException;
}
