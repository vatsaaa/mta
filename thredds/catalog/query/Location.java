// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

public class Location
{
    private double latitude;
    private double longitude;
    private double elevation;
    private String latitude_units;
    private String longitude_units;
    private String elevation_units;
    private boolean hasElevation;
    private volatile int hashCode;
    
    public Location(final String latitude, final String longitude, final String elevation, final String latitude_units, final String longitude_units, final String elevation_units) {
        this.hasElevation = false;
        this.hashCode = 0;
        try {
            this.latitude = Double.parseDouble(latitude);
            this.longitude = Double.parseDouble(longitude);
            if (elevation != null) {
                this.elevation = Double.parseDouble(elevation);
                this.hasElevation = true;
            }
        }
        catch (NumberFormatException ex) {}
        this.latitude_units = latitude_units;
        this.longitude_units = longitude_units;
        this.elevation_units = elevation_units;
    }
    
    public double getLatitude() {
        return this.latitude;
    }
    
    public double getLongitude() {
        return this.longitude;
    }
    
    public boolean hasElevation() {
        return this.hasElevation;
    }
    
    public double getElevation() {
        return this.elevation;
    }
    
    public String getLatitudeUnits() {
        return this.latitude_units;
    }
    
    public boolean isDefaultLatitudeUnits() {
        return this.latitude_units == null || this.latitude_units.equals("degrees_north");
    }
    
    public String getLongitudeUnits() {
        return this.longitude_units;
    }
    
    public boolean isDefaultLongitudeUnits() {
        return this.longitude_units == null || this.longitude_units.equals("degrees_east");
    }
    
    public String getElevationUnits() {
        return this.elevation_units;
    }
    
    public boolean isDefaultElevationUnits() {
        return this.elevation_units == null || this.elevation_units.equals("msl");
    }
    
    @Override
    public String toString() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("lat=");
        sbuff.append(this.latitude);
        sbuff.append(" lon=");
        sbuff.append(this.longitude);
        if (this.hasElevation) {
            sbuff.append(" elev=");
            sbuff.append(this.elevation);
            sbuff.append(this.elevation_units);
        }
        return sbuff.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Location && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + (int)(1000.0 * this.getLatitude());
            result = 37 * result + (int)(1000.0 * this.getLongitude());
            if (this.hasElevation()) {
                result = 37 * result + (int)(1000.0 * this.getElevation());
            }
            if (this.getLatitudeUnits() != null) {
                result = 37 * result + this.getLatitudeUnits().hashCode();
            }
            if (this.getLongitudeUnits() != null) {
                result = 37 * result + this.getLongitudeUnits().hashCode();
            }
            if (this.getElevationUnits() != null) {
                result = 37 * result + this.getElevationUnits().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
