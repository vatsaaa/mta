// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.net.URI;
import thredds.catalog.InvDocumentation;
import java.util.ArrayList;

public class ListChoice implements Choice
{
    private Selector parent;
    private String name;
    private String value;
    private ArrayList nestedSelectors;
    private InvDocumentation desc;
    private volatile int hashCode;
    
    public ListChoice(final Selector parent, final String name, final String value, final String description) {
        this.nestedSelectors = new ArrayList();
        this.hashCode = 0;
        this.parent = parent;
        this.name = name;
        this.value = value;
        if (description != null) {
            this.setDescription(new InvDocumentation(null, null, null, null, description));
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public Selector getParentSelector() {
        return this.parent;
    }
    
    public String getTemplate() {
        return this.parent.getTemplate();
    }
    
    public void addNestedSelector(final SelectList s) {
        this.nestedSelectors.add(s);
    }
    
    public ArrayList getNestedSelectors() {
        return this.nestedSelectors;
    }
    
    public boolean hasNestedSelectors() {
        return this.nestedSelectors.size() > 0;
    }
    
    public void setDescription(final InvDocumentation desc) {
        this.desc = desc;
    }
    
    public InvDocumentation getDescription() {
        return this.desc;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof ListChoice && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            result = 37 * result + this.getValue().hashCode();
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.getDescription() != null) {
                result = 37 * result + this.getDescription().hashCode();
            }
            if (this.hasNestedSelectors()) {
                result = 37 * result + this.getNestedSelectors().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
