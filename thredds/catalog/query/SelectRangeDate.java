// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

public class SelectRangeDate extends Selector
{
    private String start;
    private String end;
    private String duration;
    private String resolution;
    private String selectType;
    private volatile int hashCode;
    
    public SelectRangeDate(final String start, final String end, final String duration, final String resolution, final String selectType) {
        this.hashCode = 0;
        this.start = start;
        this.end = end;
        this.duration = duration;
        this.resolution = resolution;
        this.selectType = selectType;
    }
    
    public String getStart() {
        return this.start;
    }
    
    public String getEnd() {
        return this.end;
    }
    
    public String getDuration() {
        return this.duration;
    }
    
    public String getResolution() {
        return this.resolution;
    }
    
    @Override
    public String getSelectType() {
        return this.selectType;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof SelectRangeDate && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.getTitle() != null) {
                result = 37 * result + this.getTitle().hashCode();
            }
            if (this.getId() != null) {
                result = 37 * result + this.getId().hashCode();
            }
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.isRequired()) {
                ++result;
            }
            if (this.isMultiple()) {
                ++result;
            }
            if (this.getStart() != null) {
                result = 37 * result + this.getStart().hashCode();
            }
            if (this.getEnd() != null) {
                result = 37 * result + this.getEnd().hashCode();
            }
            if (this.getDuration() != null) {
                result = 37 * result + this.getDuration().hashCode();
            }
            if (this.getResolution() != null) {
                result = 37 * result + this.getResolution().hashCode();
            }
            if (this.getSelectType() != null) {
                result = 37 * result + this.getSelectType().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
