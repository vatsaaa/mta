// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import org.jdom.Element;
import java.net.URL;
import java.io.File;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import ucar.nc2.util.IO;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.JDOMException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import thredds.catalog.XMLEntityResolver;
import java.util.HashMap;
import org.jdom.input.SAXBuilder;
import ucar.nc2.util.DiskCache2;

public class DqcFactory
{
    public static boolean debugURL;
    public static boolean debugVersion;
    public static boolean showParsedXML;
    private static DiskCache2 diskCache;
    private static int buffer_size;
    private SAXBuilder builder;
    private DqcConvertIF defaultConverter;
    private HashMap versionToNamespaceHash;
    private HashMap namespaceToDqcConverterHash;
    private StringBuilder warnMessages;
    private StringBuilder errMessages;
    private StringBuilder fatalMessages;
    
    public static void setPersistenceCache(final DiskCache2 dc) {
        DqcFactory.diskCache = dc;
    }
    
    public DqcFactory(final boolean validate) {
        this.versionToNamespaceHash = new HashMap(10);
        this.namespaceToDqcConverterHash = new HashMap(10);
        final XMLEntityResolver jaxp = new XMLEntityResolver(validate);
        this.builder = jaxp.getSAXBuilder();
        this.warnMessages = jaxp.getWarningMessages();
        this.errMessages = jaxp.getErrorMessages();
        this.fatalMessages = jaxp.getFatalMessages();
        this.setDefaults();
    }
    
    private void setDefaults() {
        try {
            final Class fac2 = Class.forName("thredds.catalog.parser.jdom.DqcConvert2");
            final Object fac2o = fac2.newInstance();
            this.registerConverter("0.2", "http://www.unidata.ucar.edu/schemas/thredds/queryCapability", (DqcConvertIF)fac2o);
            final Class fac3 = Class.forName("thredds.catalog.parser.jdom.DqcConvert3");
            final Object fac3o = fac3.newInstance();
            this.registerConverter("0.3", "http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.3", (DqcConvertIF)fac3o);
            final Class fac4 = Class.forName("thredds.catalog.parser.jdom.DqcConvert4");
            final Object fac4o = fac4.newInstance();
            this.defaultConverter = (DqcConvertIF)fac4o;
            this.registerConverter("0.4", "http://www.unidata.ucar.edu/namespaces/thredds/queryCapability/v0.4", (DqcConvertIF)fac4o);
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException("DqcFactory: no implementing class found: " + e.getMessage());
        }
        catch (InstantiationException e2) {
            throw new RuntimeException("DqcFactory: instantition failed: " + e2.getMessage());
        }
        catch (IllegalAccessException e3) {
            throw new RuntimeException("DqcFactory: access failed: " + e3.getMessage());
        }
    }
    
    private void registerConverter(final String version, final String namespace, final DqcConvertIF converter) {
        this.namespaceToDqcConverterHash.put(namespace, converter);
        this.versionToNamespaceHash.put(version, namespace);
    }
    
    public void appendErr(final String err) {
        this.errMessages.append(err);
    }
    
    public void appendFatalErr(final String err) {
        this.fatalMessages.append(err);
    }
    
    public void appendWarning(final String err) {
        this.warnMessages.append(err);
    }
    
    public QueryCapability readXML(final String docAsString, final URI uri) throws IOException {
        this.warnMessages.setLength(0);
        this.errMessages.setLength(0);
        this.fatalMessages.setLength(0);
        Document doc = null;
        try {
            doc = this.builder.build(new StringReader(docAsString));
        }
        catch (JDOMException e) {
            this.fatalMessages.append(e.getMessage());
        }
        return this.readXML(doc, uri);
    }
    
    public QueryCapability readXML(final String uriString) throws IOException {
        URI uri;
        try {
            uri = new URI(uriString);
        }
        catch (URISyntaxException e) {
            throw new MalformedURLException(e.getMessage());
        }
        if (DqcFactory.diskCache != null) {
            final File file = DqcFactory.diskCache.getCacheFile(uriString);
            if (file != null) {
                HttpURLConnection conn = null;
                InputStream is = null;
                try {
                    final URL url = uri.toURL();
                    conn = (HttpURLConnection)url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setIfModifiedSince(file.lastModified());
                    final int code = conn.getResponseCode();
                    Label_0279: {
                        if (code == 200) {
                            is = conn.getInputStream();
                            if (is == null) {
                                break Label_0279;
                            }
                            final FileOutputStream fout = new FileOutputStream(file);
                            IO.copyB(is, fout, DqcFactory.buffer_size);
                            fout.close();
                            final InputStream fin = new BufferedInputStream(new FileInputStream(file), 50000);
                            try {
                                return this.readXML(fin, uri);
                            }
                            finally {
                                fin.close();
                            }
                        }
                        final FileInputStream fin2 = new FileInputStream(file);
                        try {
                            return this.readXML(fin2, uri);
                        }
                        finally {
                            fin2.close();
                        }
                    }
                }
                finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            }
            IO.readURLtoFileWithExceptions(uriString, file, DqcFactory.buffer_size);
            final InputStream fin3 = new BufferedInputStream(new FileInputStream(file), 50000);
            try {
                return this.readXML(fin3, uri);
            }
            finally {
                fin3.close();
            }
        }
        this.warnMessages.setLength(0);
        this.errMessages.setLength(0);
        this.fatalMessages.setLength(0);
        Document doc = null;
        try {
            doc = this.builder.build(uriString);
        }
        catch (JDOMException e2) {
            this.fatalMessages.append(e2.getMessage());
        }
        return this.readXML(doc, uri);
    }
    
    public QueryCapability readXML(final InputStream docIs, final URI uri) throws IOException {
        this.warnMessages.setLength(0);
        this.errMessages.setLength(0);
        this.fatalMessages.setLength(0);
        Document doc = null;
        try {
            doc = this.builder.build(docIs);
        }
        catch (JDOMException e) {
            this.fatalMessages.append(e.getMessage());
        }
        return this.readXML(doc, uri);
    }
    
    private QueryCapability readXML(final Document doc, final URI uri) throws IOException {
        if (doc == null) {
            final QueryCapability dqc = new QueryCapability();
            if (this.fatalMessages.length() > 0) {
                dqc.appendErrorMessage(this.fatalMessages.toString(), true);
            }
            if (this.errMessages.length() > 0) {
                dqc.appendErrorMessage(this.errMessages.toString(), false);
            }
            if (this.errMessages.length() > 0) {
                dqc.appendErrorMessage(this.warnMessages.toString(), false);
            }
            return dqc;
        }
        final Element root = doc.getRootElement();
        final String namespace = root.getNamespaceURI();
        DqcConvertIF fac = this.namespaceToDqcConverterHash.get(namespace);
        if (fac == null) {
            fac = this.defaultConverter;
            if (DqcFactory.debugVersion) {
                System.out.println("use default converter " + fac.getClass().getName() + "; no namespace " + namespace);
            }
        }
        else if (DqcFactory.debugVersion) {
            System.out.println("use converter " + fac.getClass().getName() + " based on namespace " + namespace);
        }
        final QueryCapability dqc2 = fac.parseXML(this, doc, uri);
        if (this.fatalMessages.length() > 0) {
            dqc2.appendErrorMessage(this.fatalMessages.toString(), true);
        }
        if (this.errMessages.length() > 0) {
            dqc2.appendErrorMessage(this.errMessages.toString(), false);
        }
        if (this.errMessages.length() > 0) {
            dqc2.appendErrorMessage(this.warnMessages.toString(), false);
        }
        return dqc2;
    }
    
    public String writeXML(final QueryCapability dqc) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream(10000);
        this.writeXML(dqc, os);
        return os.toString();
    }
    
    public void writeXML(final QueryCapability dqc, final OutputStream os) throws IOException {
        final String ns = this.versionToNamespaceHash.get(dqc.getVersion());
        DqcConvertIF fac = this.namespaceToDqcConverterHash.get(ns);
        if (fac == null) {
            fac = this.defaultConverter;
        }
        fac.writeXML(dqc, os);
    }
    
    public boolean writeXML(final QueryCapability dqc, final String filename) {
        try {
            final BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(filename));
            this.writeXML(dqc, os);
            os.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    private static void doOne(final DqcFactory fac, final String url) {
        System.out.println("***read " + url);
        try {
            final QueryCapability dqc = fac.readXML(url);
            System.out.println(" dqc hasFatalError= " + dqc.hasFatalError());
            System.out.println(" dqc messages= \n" + dqc.getErrorMessages());
            fac.writeXML(dqc, System.out);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) throws Exception {
        final DqcFactory fac = new DqcFactory(true);
        doOne(fac, "file:///C:/data/dqc/metarDQC.xml");
    }
    
    static {
        DqcFactory.debugURL = false;
        DqcFactory.debugVersion = false;
        DqcFactory.showParsedXML = false;
        DqcFactory.diskCache = null;
        DqcFactory.buffer_size = 64000;
    }
}
