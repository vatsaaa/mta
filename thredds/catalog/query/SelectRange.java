// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

public class SelectRange extends Selector
{
    private String min;
    private String max;
    private String units;
    private String resolution;
    private String selectType;
    private boolean modulo;
    private volatile int hashCode;
    
    public SelectRange(final String min, final String max, final String units, final String modulo, final String resolution, final String selectType) {
        this.hashCode = 0;
        this.min = min;
        this.max = max;
        this.units = units;
        this.modulo = (modulo != null && modulo.equals("true"));
        this.resolution = resolution;
        this.selectType = selectType;
    }
    
    public String getMin() {
        return this.min;
    }
    
    public String getMax() {
        return this.max;
    }
    
    public String getUnits() {
        return this.units;
    }
    
    public boolean isModulo() {
        return this.modulo;
    }
    
    public String getResolution() {
        return this.resolution;
    }
    
    @Override
    public String getSelectType() {
        return this.selectType;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof SelectRange && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.getTitle() != null) {
                result = 37 * result + this.getTitle().hashCode();
            }
            if (this.getId() != null) {
                result = 37 * result + this.getId().hashCode();
            }
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.isRequired()) {
                ++result;
            }
            if (this.isMultiple()) {
                ++result;
            }
            if (this.getMin() != null) {
                result = 37 * result + this.getMin().hashCode();
            }
            if (this.getMax() != null) {
                result = 37 * result + this.getMax().hashCode();
            }
            if (this.getUnits() != null) {
                result = 37 * result + this.getUnits().hashCode();
            }
            if (this.getResolution() != null) {
                result = 37 * result + this.getResolution().hashCode();
            }
            if (this.getSelectType() != null) {
                result = 37 * result + this.getSelectType().hashCode();
            }
            if (this.isModulo()) {
                ++result;
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
