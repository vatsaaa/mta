// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.net.URI;

public class Query
{
    private String base;
    private URI uriResolved;
    private String construct;
    private StringBuffer log;
    private volatile int hashCode;
    
    public Query(final String base, final URI uriResolved, final String construct) {
        this.log = new StringBuffer();
        this.hashCode = 0;
        this.base = base;
        this.uriResolved = uriResolved;
        this.construct = construct;
    }
    
    public String getBase() {
        return this.base;
    }
    
    public URI getUriResolved() {
        return this.uriResolved;
    }
    
    public String getConstruct() {
        return this.construct;
    }
    
    public String getReturns() {
        return "catalog";
    }
    
    @Override
    public String toString() {
        return "base=" + this.base + " construct=" + this.construct;
    }
    
    boolean validate(final StringBuffer out, final boolean show) {
        return true;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Query && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (null != this.getBase()) {
                result = 37 * result + this.getBase().hashCode();
            }
            if (null != this.getUriResolved()) {
                result = 37 * result + this.getUriResolved().hashCode();
            }
            if (null != this.getConstruct()) {
                result = 37 * result + this.getConstruct().hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
