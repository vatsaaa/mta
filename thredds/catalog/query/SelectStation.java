// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.util.ArrayList;

public class SelectStation extends Selector
{
    private ArrayList stations;
    private volatile int hashCode;
    
    public SelectStation() {
        this.stations = new ArrayList();
        this.hashCode = 0;
    }
    
    public SelectStation(final String label, final String id, final String template, final String selectType, final String required) {
        super(label, id, template, selectType, required);
        this.stations = new ArrayList();
        this.hashCode = 0;
    }
    
    public void addStation(final Station s) {
        this.stations.add(s);
    }
    
    public ArrayList getStations() {
        return this.stations;
    }
    
    public int getNumStations() {
        return this.stations.size();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof SelectStation && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.getTitle() != null) {
                result = 37 * result + this.getTitle().hashCode();
            }
            if (this.getId() != null) {
                result = 37 * result + this.getId().hashCode();
            }
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.isRequired()) {
                ++result;
            }
            if (this.isMultiple()) {
                ++result;
            }
            result = 37 * result + this.getStations().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
