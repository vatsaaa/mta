// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.query;

import java.util.ArrayList;

public class SelectList extends Selector
{
    private ArrayList choices;
    private SelectList firstNestedSelector;
    private volatile int hashCode;
    
    public SelectList() {
        this.choices = new ArrayList();
        this.firstNestedSelector = null;
        this.hashCode = 0;
    }
    
    public SelectList(final String label, final String id, final String template, final String required, final String multiple) {
        super(label, id, template, required, multiple);
        this.choices = new ArrayList();
        this.firstNestedSelector = null;
        this.hashCode = 0;
    }
    
    public void addChoice(final ListChoice c) {
        this.choices.add(c);
        if (this.firstNestedSelector == null && c.getNestedSelectors().size() > 0) {
            this.firstNestedSelector = c.getNestedSelectors().get(0);
        }
    }
    
    public boolean hasNestedSelectors() {
        return this.firstNestedSelector != null;
    }
    
    public SelectList getFirstNestedSelector() {
        return this.firstNestedSelector;
    }
    
    public ArrayList getChoices() {
        return this.choices;
    }
    
    public int getSize() {
        return this.choices.size();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof SelectList && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.getTitle() != null) {
                result = 37 * result + this.getTitle().hashCode();
            }
            if (this.getId() != null) {
                result = 37 * result + this.getId().hashCode();
            }
            if (this.getTemplate() != null) {
                result = 37 * result + this.getTemplate().hashCode();
            }
            if (this.isRequired()) {
                ++result;
            }
            if (this.isMultiple()) {
                ++result;
            }
            result = 37 * result + this.getChoices().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
