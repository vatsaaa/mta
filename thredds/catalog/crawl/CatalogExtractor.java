// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.crawl;

import ucar.nc2.dt.GridDatatype;
import ucar.nc2.Attribute;
import ucar.nc2.dataset.VerticalCT;
import ucar.nc2.units.TimeUnit;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.unidata.geoloc.LatLonPointImpl;
import java.util.List;
import ucar.unidata.geoloc.ProjectionImpl;
import java.util.Iterator;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.dt.GridCoordSystem;
import ucar.nc2.util.NamedObject;
import ucar.nc2.units.DateRange;
import ucar.unidata.util.Parameter;
import ucar.unidata.util.Format;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.util.IO;
import java.io.File;
import ucar.unidata.util.StringUtil;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.Formatter;
import java.io.IOException;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvAccess;
import thredds.catalog.InvDataset;
import ucar.nc2.util.CancelTask;
import java.io.PrintStream;
import ucar.nc2.thredds.ThreddsDataFactory;
import thredds.catalog.InvCatalogFactory;

public class CatalogExtractor implements CatalogCrawler.Listener
{
    private boolean verbose;
    private InvCatalogFactory catFactory;
    private ThreddsDataFactory tdataFactory;
    private int countDatasets;
    private int countNoAccess;
    private int countNoOpen;
    private PrintStream out;
    private String transferDir;
    private String copyDir;
    
    public CatalogExtractor(final boolean verbose) {
        this.verbose = true;
        this.catFactory = InvCatalogFactory.getDefaultFactory(true);
        this.tdataFactory = new ThreddsDataFactory();
        this.transferDir = "C:/data/bad/";
        this.copyDir = null;
        this.verbose = verbose;
    }
    
    public void copy(final String catUrl, final String copyToDir, final CancelTask task) throws IOException {
        this.copyDir = copyToDir;
        final InvCatalogImpl cat = this.catFactory.readXML(catUrl);
        final StringBuilder buff = new StringBuilder();
        if (!cat.check(buff, false)) {
            return;
        }
        this.countDatasets = 0;
        this.countNoAccess = 0;
        this.countNoOpen = 0;
        int countCatRefs = 0;
        final CatalogCrawler crawler = new CatalogCrawler(1, false, new CatalogCrawler.Listener() {
            public void getDataset(final InvDataset dd, final Object context) {
                final InvAccess access = CatalogExtractor.this.tdataFactory.chooseDatasetAccess(dd.getAccess());
                if (null != access) {
                    CatalogExtractor.this.transfer(access.getStandardUrlName(), CatalogExtractor.this.copyDir);
                }
            }
            
            public boolean getCatalogRef(final InvCatalogRef dd, final Object context) {
                return true;
            }
        });
        final long start = System.currentTimeMillis();
        try {
            countCatRefs = crawler.crawl(cat, task, this.out, null);
        }
        finally {
            final int took = (int)(System.currentTimeMillis() - start) / 1000;
            this.out.println("***Done " + catUrl + " took = " + took + " secs\n" + "   datasets=" + this.countDatasets + " no access=" + this.countNoAccess + " open failed=" + this.countNoOpen + " total catalogs=" + countCatRefs);
            if (this.verbose) {
                System.out.println("***Done " + catUrl + " took = " + took + " secs\n" + "   datasets=" + this.countDatasets + " no access=" + this.countNoAccess + " open failed=" + this.countNoOpen + " total catalogs=" + countCatRefs);
            }
        }
    }
    
    public void extractLoop(final PrintStream out, final String catUrl, final int type, final boolean skipDatasetScan, final CancelTask task) throws IOException {
        do {
            this.extract(out, catUrl, type, skipDatasetScan, task);
        } while (task == null || !task.isCancel());
    }
    
    public void extract(final PrintStream out, final String catUrl, final int type, final boolean skipDatasetScan, final CancelTask task) throws IOException {
        (this.out = out).println("***read " + catUrl);
        final InvCatalogImpl cat = this.catFactory.readXML(catUrl);
        final StringBuilder buff = new StringBuilder();
        final boolean isValid = cat.check(buff, false);
        if (!isValid) {
            System.out.println("***Catalog invalid= " + catUrl + " validation output=\n" + (Object)buff);
            out.println("***Catalog invalid= " + catUrl + " validation output=\n" + (Object)buff);
            return;
        }
        out.println("catalog <" + cat.getName() + "> is valid");
        out.println(" validation output=\n" + (Object)buff);
        this.countDatasets = 0;
        this.countNoAccess = 0;
        this.countNoOpen = 0;
        int countCatRefs = 0;
        final CatalogCrawler crawler = new CatalogCrawler(type, skipDatasetScan, this);
        final long start = System.currentTimeMillis();
        try {
            countCatRefs = crawler.crawl(cat, task, out, null);
        }
        finally {
            final int took = (int)(System.currentTimeMillis() - start) / 1000;
            out.println("***Done " + catUrl + " took = " + took + " secs\n" + "   datasets=" + this.countDatasets + " no access=" + this.countNoAccess + " open failed=" + this.countNoOpen + " total catalogs=" + countCatRefs);
            if (this.verbose) {
                System.out.println("***Done " + catUrl + " took = " + took + " secs\n" + "   datasets=" + this.countDatasets + " no access=" + this.countNoAccess + " open failed=" + this.countNoOpen + " total catalogs=" + countCatRefs);
            }
        }
    }
    
    public void getDataset(final InvDataset ds, final Object context) {
        ++this.countDatasets;
        this.openDataset(this.out, ds);
    }
    
    public boolean getCatalogRef(final InvCatalogRef dd, final Object context) {
        return true;
    }
    
    public boolean openDataset(final PrintStream out, final InvDataset ds) {
        final InvAccess access = this.tdataFactory.chooseDatasetAccess(ds.getAccess());
        if (access == null) {
            ++this.countNoAccess;
            out.println("  **FAILED to find access " + ds.getName());
            System.out.println("  **FAILED to find access " + ds.getName());
            return false;
        }
        NetcdfDataset ncd = null;
        final long start = System.currentTimeMillis();
        try {
            final Formatter log = new Formatter();
            ncd = this.tdataFactory.openDataset(access, true, null, log);
            if (ncd == null) {
                ++this.countNoOpen;
                out.println("  **FAILED to open " + access.getStandardUrlName() + " " + log);
                System.out.println("  **FAILED to open " + access.getStandardUrlName() + " " + log);
                this.transfer(access.getStandardUrlName(), this.transferDir);
                return false;
            }
            final int took = (int)(System.currentTimeMillis() - start);
            final boolean ok = true;
            out.println("  **Open " + ds.getDataType() + " " + ncd.getLocation() + " (" + ds.getName() + ") " + took + " msecs");
            if (this.verbose) {
                System.out.println("  **Open " + ds.getDataType() + " " + ncd.getLocation() + " (" + ds.getName() + ") " + took + " msecs; ok=" + ok);
            }
        }
        catch (Throwable e2) {
            ++this.countNoOpen;
            out.println("  **FAILED to open " + access.getStandardUrlName());
            System.out.println("  **FAILED to open " + access.getStandardUrlName());
            this.transfer(access.getStandardUrlName(), this.transferDir);
            return false;
        }
        finally {
            if (ncd != null) {
                try {
                    ncd.close();
                    out.println("   Close " + ncd.getLocation());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }
    
    private void transfer(final String url, final String copyToDir) {
        final String new_url = StringUtil.substitute(url, "dodsC", "fileServer");
        final int pos = url.lastIndexOf("/");
        final String filename = url.substring(pos + 1);
        final File file = new File(copyToDir, filename);
        IO.readURLtoFile(new_url, file);
        System.out.println("  **copied to " + file.getPath() + " size=" + file.length());
    }
    
    public boolean extractTypedDatasetInfo(final PrintStream out, final InvDataset ds) {
        boolean ok = true;
        final long start = System.currentTimeMillis();
        ThreddsDataFactory.Result result = null;
        try {
            result = this.tdataFactory.openFeatureDataset(ds, null);
            final int took = (int)(System.currentTimeMillis() - start);
            if (this.verbose) {
                System.out.println("  **Open " + result.featureType + " " + result.location + " (" + ds.getName() + ") " + took + " msecs");
            }
            out.println("  **Open " + result.featureType + " " + result.location + " (" + ds.getName() + ") " + took + " msecs");
            if (result.location == null) {
                ok = false;
            }
            else if (result.featureType == FeatureType.GRID) {
                this.extractGridDataset(out, (GridDataset)result.featureDataset);
            }
        }
        catch (Throwable e) {
            out.println("   **FAILED " + ds.getName());
            e.printStackTrace(out);
            e.printStackTrace();
            return false;
        }
        finally {
            if (result != null && result.featureDataset != null) {
                try {
                    result.featureDataset.close();
                    out.println("   Close " + result.featureType + " " + result.location);
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        return ok;
    }
    
    private void extractGridDataset(final PrintStream out, final GridDataset gridDs) {
        if (!this.verbose) {
            out.println("    ngrids = " + gridDs.getGrids().size());
            return;
        }
        out.println("Global Attributes");
        final NetcdfDataset ds = (NetcdfDataset)gridDs.getNetcdfFile();
        this.showAtts(out, ds.getGlobalAttributes());
        out.println();
        if (gridDs == null) {
            return;
        }
        GridCoordSystem gcsMax = null;
        LatLonRect llbbMax = null;
        LatLonRect llbb = null;
        DateRange dateRange = null;
        long nx = 0L;
        long ny = 0L;
        for (final GridDataset.Gridset gset : gridDs.getGridsets()) {
            final GridCoordSystem gcs = gset.getGeoCoordSystem();
            final CoordinateAxis1D xaxis = (CoordinateAxis1D)gcs.getXHorizAxis();
            final CoordinateAxis1D yaxis = (CoordinateAxis1D)gcs.getYHorizAxis();
            final long nx2 = xaxis.getSize();
            final long ny2 = yaxis.getSize();
            if (nx != nx2 || ny != ny2) {
                nx = nx2;
                ny = ny2;
                final double dx = xaxis.getIncrement();
                final double dy = yaxis.getIncrement();
                out.println("  horizontal = " + nx + " by " + ny + " points, resolution " + Format.d(dx, 4) + " " + Format.d(dy, 4) + " " + xaxis.getUnitsString());
            }
            final ProjectionImpl proj = gcs.getProjection();
            if (proj != null) {
                out.println(", " + proj.getClassName() + " projection;");
                final List<Parameter> params = proj.getProjectionParameters();
                for (final Parameter p : params) {
                    out.println("       " + p.getName() + " " + p.getStringValue());
                }
            }
            else {
                out.println();
            }
            final LatLonRect llbb2 = gcs.getLatLonBoundingBox();
            if (llbb == null || !llbb2.equals(llbb)) {
                llbb = llbb2;
                if (llbbMax == null) {
                    llbbMax = llbb;
                }
                else {
                    llbbMax.extend(llbb);
                }
                if (llbb.getWidth() >= 360.0) {
                    out.println("  BoundingBox == GLOBAL");
                }
                else {
                    final StringBuffer buff = new StringBuffer();
                    final LatLonPointImpl ll = llbb.getLowerLeftPoint();
                    final LatLonPointImpl ur = llbb.getUpperRightPoint();
                    buff.append(Double.toString(ll.getLongitude()));
                    buff.append(" ");
                    buff.append(Double.toString(ll.getLatitude()));
                    buff.append(" ");
                    buff.append(Double.toString(ur.getLongitude()));
                    buff.append(" ");
                    buff.append(Double.toString(ur.getLatitude()));
                    buff.append(" ");
                    out.println("  BoundingBox == " + llbb + " width= " + llbb.getWidth() + " " + ((llbb.getWidth() >= 360.0) ? "global" : ""));
                }
            }
            final CoordinateAxis1DTime taxis = gcs.getTimeAxis1D();
            final DateRange dateRange2 = gcs.getDateRange();
            if (taxis != null && (dateRange == null || !dateRange2.equals(dateRange))) {
                final long ntimes = taxis.getSize();
                try {
                    final TimeUnit tUnit = taxis.getTimeResolution();
                    dateRange = new DateRange(dateRange2, "1 hour");
                    out.println("  DateRange == start= " + dateRange.getStart() + " end= " + dateRange.getEnd() + " duration= " + dateRange.getDuration() + " ntimes = " + ntimes + " data resolution = " + tUnit);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            final CoordinateAxis1D vaxis = gcs.getVerticalAxis();
            if (vaxis != null) {
                final long nvert = vaxis.getSize();
                out.print("  Vertical axis= " + vaxis.getName() + " units=" + vaxis.getUnitsString() + " size= " + nvert);
                final VerticalCT vt = gcs.getVerticalCT();
                if (vt != null) {
                    out.print(" transform= " + vt.getVerticalTransformType());
                }
                final List<NamedObject> vertNames = vaxis.getNames();
                for (final NamedObject vertName : vertNames) {
                    out.print(" " + vertName);
                }
                out.println();
                if (gcsMax != null && gcsMax.getVerticalAxis().getSize() >= vaxis.getSize()) {
                    continue;
                }
                gcsMax = gcs;
            }
        }
    }
    
    private void showAtts(final PrintStream out, final List<Attribute> atts) {
        for (final Attribute att : atts) {
            out.println("  " + att);
        }
    }
    
    private void makeGrib1Vocabulary(final List<GridDatatype> grids, final PrintStream out) {
        out.println("\n<variables vocabulary='GRIB-1'>");
        for (final GridDatatype grid : grids) {
            final Attribute att = grid.findAttributeIgnoreCase("GRIB_param_number");
            final String stdName = (att != null) ? att.getNumericValue().toString() : null;
            out.print("  <variable name='");
            out.print(grid.getName());
            out.print("' vocabulary_name='");
            out.print((stdName != null) ? stdName : "dunno");
            out.print("' units='");
            out.print(grid.getUnitsString());
            out.println("'/>");
        }
        out.println("</variables>");
    }
}
