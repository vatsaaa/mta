// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.crawl;

import java.util.ArrayList;
import java.util.List;
import thredds.catalog.InvCatalogRef;
import java.util.Iterator;
import thredds.catalog.InvDataset;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.InvCatalogFactory;
import java.io.PrintStream;
import ucar.nc2.util.CancelTask;
import java.util.Random;

public class CatalogCrawler
{
    public static final int USE_ALL = 0;
    public static final int USE_ALL_DIRECT = 1;
    public static final int USE_FIRST_DIRECT = 2;
    public static final int USE_RANDOM_DIRECT = 3;
    public static final int USE_RANDOM_DIRECT_NOT_FIRST_OR_LAST = 4;
    private boolean skipDatasetScan;
    private int type;
    private Listener listen;
    private Random random;
    private int countCatrefs;
    
    public CatalogCrawler(final int type, final boolean skipDatasetScan, final Listener listen) {
        this.skipDatasetScan = false;
        this.type = 0;
        this.type = type;
        this.skipDatasetScan = skipDatasetScan;
        this.listen = listen;
        if (type == 3 || type == 4) {
            this.random = new Random(System.currentTimeMillis());
        }
    }
    
    public int crawl(final String catUrl, final CancelTask task, final PrintStream out, final Object context) {
        final InvCatalogFactory catFactory = InvCatalogFactory.getDefaultFactory(true);
        final InvCatalogImpl cat = catFactory.readXML(catUrl);
        final StringBuilder buff = new StringBuilder();
        final boolean isValid = cat.check(buff, false);
        if (out != null) {
            out.println("catalog <" + cat.getName() + "> " + (isValid ? "is" : "is not") + " valid");
            out.println(" validation output=\n" + (Object)buff);
        }
        if (isValid) {
            return this.crawl(cat, task, out, context);
        }
        return 0;
    }
    
    public int crawl(final InvCatalogImpl cat, final CancelTask task, final PrintStream out, final Object context) {
        if (out != null) {
            out.println("***CATALOG " + cat.getCreateFrom());
        }
        this.countCatrefs = 0;
        for (final InvDataset ds : cat.getDatasets()) {
            if (this.type == 0) {
                this.crawlDataset(ds, task, out, context);
            }
            else {
                this.crawlDirectDatasets(ds, task, out, context);
            }
            if (task != null && task.isCancel()) {
                break;
            }
        }
        return 1 + this.countCatrefs;
    }
    
    public void crawlDataset(final InvDataset ds, final CancelTask task, final PrintStream out, final Object context) {
        final boolean isCatRef = ds instanceof InvCatalogRef;
        final boolean isDataScan = ds.findProperty("DatasetScan") != null;
        final boolean skipScanChildren = this.skipDatasetScan && ds instanceof InvCatalogRef && isDataScan;
        if (isCatRef) {
            final InvCatalogRef catref = (InvCatalogRef)ds;
            if (out != null) {
                out.println(" **CATREF " + catref.getURI() + " (" + ds.getName() + ") ");
            }
            ++this.countCatrefs;
            if (!this.listen.getCatalogRef(catref, context)) {
                catref.release();
                return;
            }
        }
        if (!isCatRef || skipScanChildren || isDataScan) {
            this.listen.getDataset(ds, context);
        }
        if (!skipScanChildren) {
            final List<InvDataset> dlist = ds.getDatasets();
            if (isCatRef) {
                final InvCatalogRef catref2 = (InvCatalogRef)ds;
                if (!isDataScan) {
                    this.listen.getDataset(catref2.getProxyDataset(), context);
                }
            }
            for (final InvDataset dds : dlist) {
                this.crawlDataset(dds, task, out, context);
                if (task != null && task.isCancel()) {
                    break;
                }
            }
        }
        if (isCatRef) {
            final InvCatalogRef catref = (InvCatalogRef)ds;
            catref.release();
        }
    }
    
    public void crawlDirectDatasets(final InvDataset ds, final CancelTask task, final PrintStream out, final Object context) {
        final boolean isCatRef = ds instanceof InvCatalogRef;
        final boolean skipScanChildren = this.skipDatasetScan && ds instanceof InvCatalogRef && ds.findProperty("DatasetScan") != null;
        if (isCatRef) {
            final InvCatalogRef catref = (InvCatalogRef)ds;
            if (out != null) {
                out.println(" **CATREF " + catref.getURI() + " (" + ds.getName() + ") ");
            }
            ++this.countCatrefs;
            if (!this.listen.getCatalogRef(catref, context)) {
                catref.release();
                return;
            }
        }
        final List<InvDataset> dlist = ds.getDatasets();
        final List<InvDataset> leaves = new ArrayList<InvDataset>();
        for (final InvDataset dds : dlist) {
            if (dds.hasAccess()) {
                leaves.add(dds);
            }
        }
        if (leaves.size() > 0) {
            if (this.type == 2) {
                final InvDataset dds2 = leaves.get(0);
                this.listen.getDataset(dds2, context);
            }
            else if (this.type == 3) {
                this.listen.getDataset(this.chooseRandom(leaves), context);
            }
            else if (this.type == 4) {
                this.listen.getDataset(this.chooseRandomNotFirstOrLast(leaves), context);
            }
            else {
                for (final InvDataset dds : leaves) {
                    this.listen.getDataset(dds, context);
                    if (task != null && task.isCancel()) {
                        break;
                    }
                }
            }
        }
        if (!skipScanChildren) {
            for (final InvDataset dds : dlist) {
                if (dds.hasNestedDatasets()) {
                    this.crawlDirectDatasets(dds, task, out, context);
                }
                if (task != null && task.isCancel()) {
                    break;
                }
            }
        }
        if (ds instanceof InvCatalogRef) {
            final InvCatalogRef catref2 = (InvCatalogRef)ds;
            catref2.release();
        }
    }
    
    private InvDataset chooseRandom(final List datasets) {
        final int index = this.random.nextInt(datasets.size());
        return datasets.get(index);
    }
    
    private InvDataset chooseRandomNotFirstOrLast(final List datasets) {
        int index = this.random.nextInt(datasets.size());
        if (index == 0 && datasets.size() > 1) {
            ++index;
        }
        else if (index == datasets.size() - 1 && datasets.size() > 1) {
            --index;
        }
        return datasets.get(index);
    }
    
    public interface Listener
    {
        void getDataset(final InvDataset p0, final Object p1);
        
        boolean getCatalogRef(final InvCatalogRef p0, final Object p1);
    }
}
