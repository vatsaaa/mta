// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import ucar.nc2.constants.FeatureType;
import java.util.Iterator;

public abstract class DatasetFilter
{
    public abstract int accept(final InvDataset p0);
    
    public static class ByServiceType extends DatasetFilter
    {
        private ServiceType type;
        
        public ByServiceType(final ServiceType type) {
            this.type = type;
        }
        
        @Override
        public int accept(final InvDataset d) {
            for (final InvAccess a : d.getAccess()) {
                if (a.getService().getServiceType() == this.type) {
                    return 1;
                }
            }
            for (final InvAccess a : d.getAccess()) {
                if (a.getService().getServiceType() == ServiceType.QC) {
                    return 0;
                }
                if (a.getService().getServiceType() == ServiceType.RESOLVER) {
                    return 0;
                }
            }
            return -1;
        }
    }
    
    public static class ByDataType extends DatasetFilter
    {
        private FeatureType type;
        
        public ByDataType(final FeatureType type) {
            this.type = type;
        }
        
        @Override
        public int accept(final InvDataset d) {
            if (null == d.getDataType()) {
                return 0;
            }
            return (d.getDataType() == this.type) ? 1 : -1;
        }
    }
}
