// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;

public final class CollectionType
{
    private static ArrayList<CollectionType> members;
    public static final CollectionType NONE;
    public static final CollectionType TIMESERIES;
    public static final CollectionType STATIONS;
    public static final CollectionType FORECASTS;
    private String name;
    
    private CollectionType(final String s) {
        this.name = s;
        CollectionType.members.add(this);
    }
    
    private CollectionType(final String name, final boolean fake) {
        this.name = name;
    }
    
    public static Collection<CollectionType> getAllTypes() {
        return CollectionType.members;
    }
    
    public static CollectionType findType(final String name) {
        if (name == null) {
            return null;
        }
        for (final CollectionType m : CollectionType.members) {
            if (m.name.equalsIgnoreCase(name)) {
                return m;
            }
        }
        return null;
    }
    
    public static CollectionType getType(final String name) {
        if (name == null) {
            return null;
        }
        final CollectionType type = findType(name);
        return (type != null) ? type : new CollectionType(name, false);
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof CollectionType && o.hashCode() == this.hashCode());
    }
    
    static {
        CollectionType.members = new ArrayList<CollectionType>(20);
        NONE = new CollectionType("");
        TIMESERIES = new CollectionType("TimeSeries");
        STATIONS = new CollectionType("Stations");
        FORECASTS = new CollectionType("ForecastModelRuns");
    }
}
