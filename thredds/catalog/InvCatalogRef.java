// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import ucar.nc2.units.DateRange;
import ucar.nc2.units.DateType;
import ucar.nc2.constants.FeatureType;
import java.util.ArrayList;
import java.util.List;
import java.net.URISyntaxException;
import java.net.URI;

public class InvCatalogRef extends InvDatasetImpl
{
    private String href;
    private InvDatasetImpl proxy;
    private URI uri;
    private String errMessage;
    private boolean init;
    private boolean useProxy;
    private boolean debug;
    private boolean debugProxy;
    private boolean debugAsynch;
    
    public InvCatalogRef(final InvDatasetImpl parent, final String title, final String href) {
        super(parent, title);
        this.proxy = null;
        this.uri = null;
        this.errMessage = null;
        this.init = false;
        this.useProxy = false;
        this.debug = false;
        this.debugProxy = false;
        this.debugAsynch = false;
        this.href = href.trim();
    }
    
    public String getXlinkHref() {
        return this.href;
    }
    
    public void setXlinkHref(final String href) {
        this.href = href;
        this.uri = null;
    }
    
    public URI getURI() {
        if (this.uri != null) {
            return this.uri;
        }
        try {
            return this.getParentCatalog().resolveUri(this.href);
        }
        catch (URISyntaxException e) {
            this.errMessage = "URISyntaxException on url  " + this.href + " = " + e.getMessage();
            return null;
        }
    }
    
    @Override
    public List<InvDataset> getDatasets() {
        this.read();
        return this.useProxy ? this.proxy.getDatasets() : super.getDatasets();
    }
    
    public boolean isRead() {
        return this.init;
    }
    
    public InvDatasetImpl getProxyDataset() {
        this.read();
        return this.proxy;
    }
    
    public void release() {
        this.datasets = new ArrayList<InvDataset>();
        this.proxy = null;
        this.init = false;
    }
    
    @Override
    public boolean finish() {
        return super.finish();
    }
    
    private synchronized void read() {
        if (this.init) {
            return;
        }
        final URI uriResolved = this.getURI();
        if (uriResolved == null) {
            this.proxy = new InvDatasetImpl(null, "HREF ERROR");
            if (this.debug) {
                System.out.println(this.errMessage);
            }
            this.proxy.addProperty(new InvProperty("HREF ERROR", this.errMessage));
            this.datasets.add(this.proxy);
            this.init = true;
            return;
        }
        try {
            if (this.debug) {
                System.out.println(" InvCatalogRef read " + this.getFullName() + "  hrefResolved = " + uriResolved);
            }
            final InvCatalogFactory factory = ((InvCatalogImpl)this.getParentCatalog()).getCatalogFactory();
            final InvCatalogImpl cat = factory.readXML(uriResolved.toString());
            this.finishCatalog(cat);
        }
        catch (Exception e) {
            this.proxy = new InvDatasetImpl(null, "HREF ERROR");
            if (this.debug) {
                System.out.println("HREF ERROR =\n  " + this.href + " err= " + e.getMessage());
            }
            this.proxy.addProperty(new InvProperty("HREF ERROR", this.href));
            this.datasets.add(this.proxy);
            this.init = true;
        }
    }
    
    private void finishCatalog(final InvCatalogImpl cat) {
        if (cat.hasFatalError()) {
            this.proxy = new InvDatasetImpl(null, "ERROR OPENING");
            final StringBuilder out = new StringBuilder();
            cat.check(out);
            if (this.debug) {
                System.out.println("PARSE ERROR =\n  " + out.toString());
            }
            this.proxy.addProperty(new InvProperty("ERROR OPENING", out.toString()));
            this.proxy.finish();
        }
        else {
            final InvCatalogImpl parentCatalog = (InvCatalogImpl)this.getParentCatalog();
            final DatasetFilter filter = parentCatalog.getDatasetFilter();
            if (filter != null) {
                cat.filter(filter);
            }
            this.proxy = (InvDatasetImpl)cat.getDataset();
            if (this.proxy.getMark()) {
                this.proxy.setName(this.proxy.getName() + " (EMPTY)");
                this.proxy.addProperty(new InvProperty("isEmpty", "true"));
                this.proxy.finish();
            }
            final String name = this.getName().trim();
            final String proxyName = this.proxy.getName().trim();
            this.useProxy = (proxyName.equals(name) && !(this.proxy instanceof InvCatalogRef));
            if (this.debugProxy) {
                System.out.println("catRefname=" + name + "=topName=" + proxyName + "=" + this.useProxy);
            }
        }
        this.datasets.add(this.proxy);
        this.init = true;
    }
    
    public synchronized void readAsynch(final InvCatalogFactory factory, final CatalogSetCallback caller) {
        if (this.init) {
            caller.setCatalog((InvCatalogImpl)this.getParentCatalog());
            return;
        }
        String hrefResolved;
        try {
            final URI uri = this.getParentCatalog().resolveUri(this.href);
            hrefResolved = uri.toString();
        }
        catch (URISyntaxException e) {
            this.proxy = new InvDatasetImpl(null, "HREF ERROR");
            if (this.debug) {
                System.out.println("HREF ERROR =\n  " + this.href + " err= " + e.getMessage());
            }
            this.proxy.addProperty(new InvProperty("HREF ERROR", this.href));
            this.datasets.add(this.proxy);
            return;
        }
        try {
            if (this.debug) {
                System.out.println(" InvCatalogRef readXMLasynch " + this.getFullName() + "  hrefResolved = " + hrefResolved);
            }
            factory.readXMLasynch(hrefResolved, new Callback(caller));
        }
        catch (Exception e2) {
            this.proxy = new InvDatasetImpl(null, "HREF ERROR");
            if (this.debug) {
                System.out.println("HREF ERROR =\n  " + this.href + " err= " + e2.getMessage());
            }
            this.proxy.addProperty(new InvProperty("HREF ERROR", this.href));
            this.datasets.add(this.proxy);
        }
    }
    
    @Override
    boolean check(final StringBuilder out, final boolean show) {
        return this.isRead() ? this.proxy.check(out, show) : super.check(out, show);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InvCatalogRef)) {
            return false;
        }
        final InvCatalogRef invCatalogRef = (InvCatalogRef)o;
        Label_0054: {
            if (this.href != null) {
                if (this.href.equals(invCatalogRef.href)) {
                    break Label_0054;
                }
            }
            else if (invCatalogRef.href == null) {
                break Label_0054;
            }
            return false;
        }
        if (this.name != null) {
            if (this.name.equals(invCatalogRef.name)) {
                return true;
            }
        }
        else if (invCatalogRef.name == null) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = 17;
        result = 29 * result + ((this.href != null) ? this.href.hashCode() : 0);
        result = 29 * result + ((this.name != null) ? this.name.hashCode() : 0);
        return result;
    }
    
    @Override
    public InvDatasetImpl findDatasetByName(final String p0) {
        return this.useProxy ? this.proxy.findDatasetByName(p0) : super.findDatasetByName(p0);
    }
    
    @Override
    public String findProperty(final String p0) {
        return this.useProxy ? this.proxy.findProperty(p0) : super.findProperty(p0);
    }
    
    @Override
    public InvService findService(final String p0) {
        return this.useProxy ? this.proxy.findService(p0) : super.findService(p0);
    }
    
    @Override
    public InvAccess getAccess(final ServiceType p0) {
        return this.useProxy ? this.proxy.getAccess(p0) : super.getAccess(p0);
    }
    
    @Override
    public List<InvAccess> getAccess() {
        return this.useProxy ? this.proxy.getAccess() : super.getAccess();
    }
    
    @Override
    public String getAlias() {
        return this.useProxy ? this.proxy.getAlias() : super.getAlias();
    }
    
    @Override
    public String getAuthority() {
        return this.useProxy ? this.proxy.getAuthority() : super.getAuthority();
    }
    
    @Override
    public CollectionType getCollectionType() {
        return this.useProxy ? this.proxy.getCollectionType() : super.getCollectionType();
    }
    
    @Override
    public List<ThreddsMetadata.Contributor> getContributors() {
        return this.useProxy ? this.proxy.getContributors() : super.getContributors();
    }
    
    @Override
    public List<ThreddsMetadata.Source> getCreators() {
        return this.useProxy ? this.proxy.getCreators() : super.getCreators();
    }
    
    @Override
    public DataFormatType getDataFormatType() {
        return this.useProxy ? this.proxy.getDataFormatType() : super.getDataFormatType();
    }
    
    @Override
    public FeatureType getDataType() {
        return this.useProxy ? this.proxy.getDataType() : super.getDataType();
    }
    
    @Override
    public List<DateType> getDates() {
        return this.useProxy ? this.proxy.getDates() : super.getDates();
    }
    
    @Override
    public List<InvDocumentation> getDocumentation() {
        return this.useProxy ? this.proxy.getDocumentation() : super.getDocumentation();
    }
    
    @Override
    public String getDocumentation(final String p0) {
        return this.useProxy ? this.proxy.getDocumentation(p0) : super.getDocumentation(p0);
    }
    
    @Override
    public String getFullName() {
        return this.useProxy ? this.proxy.getFullName() : super.getFullName();
    }
    
    @Override
    public ThreddsMetadata.GeospatialCoverage getGeospatialCoverage() {
        return this.useProxy ? this.proxy.getGeospatialCoverage() : super.getGeospatialCoverage();
    }
    
    @Override
    public String getID() {
        return this.useProxy ? this.proxy.getID() : super.getID();
    }
    
    @Override
    public List<ThreddsMetadata.Vocab> getKeywords() {
        return this.useProxy ? this.proxy.getKeywords() : super.getKeywords();
    }
    
    @Override
    protected boolean getMark() {
        return this.useProxy ? this.proxy.getMark() : super.getMark();
    }
    
    @Override
    public List<InvMetadata> getMetadata(final MetadataType p0) {
        return this.useProxy ? this.proxy.getMetadata(p0) : super.getMetadata(p0);
    }
    
    @Override
    public List<InvMetadata> getMetadata() {
        return this.useProxy ? this.proxy.getMetadata() : super.getMetadata();
    }
    
    @Override
    public String getName() {
        return this.useProxy ? this.proxy.getName() : super.getName();
    }
    
    @Override
    public InvDataset getParent() {
        return this.useProxy ? this.proxy.getParent() : super.getParent();
    }
    
    @Override
    public List<ThreddsMetadata.Vocab> getProjects() {
        return this.useProxy ? this.proxy.getProjects() : super.getProjects();
    }
    
    @Override
    public List<InvProperty> getProperties() {
        return this.useProxy ? this.proxy.getProperties() : super.getProperties();
    }
    
    @Override
    public List<ThreddsMetadata.Source> getPublishers() {
        return this.useProxy ? this.proxy.getPublishers() : super.getPublishers();
    }
    
    @Override
    public InvService getServiceDefault() {
        return this.useProxy ? this.proxy.getServiceDefault() : super.getServiceDefault();
    }
    
    @Override
    public DateRange getTimeCoverage() {
        return this.useProxy ? this.proxy.getTimeCoverage() : super.getTimeCoverage();
    }
    
    @Override
    public String getUniqueID() {
        return this.useProxy ? this.proxy.getUniqueID() : super.getUniqueID();
    }
    
    @Override
    public String getUrlPath() {
        return this.useProxy ? this.proxy.getUrlPath() : super.getUrlPath();
    }
    
    @Override
    public Object getUserProperty(final Object p0) {
        return this.useProxy ? this.proxy.getUserProperty(p0) : super.getUserProperty(p0);
    }
    
    @Override
    public List<ThreddsMetadata.Variables> getVariables() {
        return this.useProxy ? this.proxy.getVariables() : super.getVariables();
    }
    
    @Override
    public boolean hasAccess() {
        return this.useProxy ? this.proxy.hasAccess() : super.hasAccess();
    }
    
    @Override
    public boolean hasNestedDatasets() {
        return !this.useProxy || this.proxy.hasNestedDatasets();
    }
    
    @Override
    public boolean isHarvest() {
        return this.useProxy ? this.proxy.isHarvest() : super.isHarvest();
    }
    
    private class Callback implements CatalogSetCallback
    {
        CatalogSetCallback caller;
        
        Callback(final CatalogSetCallback caller) {
            this.caller = caller;
        }
        
        public void setCatalog(final InvCatalogImpl cat) {
            if (InvCatalogRef.this.debugAsynch) {
                System.out.println(" setCatalog was called");
            }
            InvCatalogRef.this.finishCatalog(cat);
            this.caller.setCatalog(cat);
        }
        
        public void failed() {
            if (InvCatalogRef.this.debugAsynch) {
                System.out.println(" setCatalog failed");
            }
            this.caller.failed();
        }
    }
}
