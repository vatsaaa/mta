// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.util.List;

public final class DataFormatType
{
    private static List<DataFormatType> members;
    public static final DataFormatType NONE;
    public static final DataFormatType BUFR;
    public static final DataFormatType ESML;
    public static final DataFormatType GEMPAK;
    public static final DataFormatType GINI;
    public static final DataFormatType GRIB1;
    public static final DataFormatType GRIB2;
    public static final DataFormatType HDF4;
    public static final DataFormatType HDF5;
    public static final DataFormatType NETCDF;
    public static final DataFormatType NEXRAD2;
    public static final DataFormatType NCML;
    public static final DataFormatType NIDS;
    public static final DataFormatType MCIDAS_AREA;
    public static final DataFormatType GIF;
    public static final DataFormatType JPEG;
    public static final DataFormatType TIFF;
    public static final DataFormatType PLAIN;
    public static final DataFormatType TSV;
    public static final DataFormatType XML;
    public static final DataFormatType MPEG;
    public static final DataFormatType QUICKTIME;
    public static final DataFormatType REALTIME;
    public static final DataFormatType OTHER_UNKNOWN;
    private String name;
    
    private DataFormatType(final String s) {
        this.name = s;
        DataFormatType.members.add(this);
    }
    
    private DataFormatType(final String s, final boolean fake) {
        this.name = s;
    }
    
    public static Collection<DataFormatType> getAllTypes() {
        return DataFormatType.members;
    }
    
    public static DataFormatType findType(final String name) {
        if (name == null) {
            return null;
        }
        for (final DataFormatType m : DataFormatType.members) {
            if (m.name.equalsIgnoreCase(name)) {
                return m;
            }
        }
        return null;
    }
    
    public static DataFormatType getType(final String name) {
        if (name == null) {
            return null;
        }
        final DataFormatType t = findType(name);
        return (t != null) ? t : new DataFormatType(name, false);
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof DataFormatType && o.hashCode() == this.hashCode());
    }
    
    static {
        DataFormatType.members = new ArrayList<DataFormatType>(20);
        NONE = new DataFormatType("");
        BUFR = new DataFormatType("BUFR");
        ESML = new DataFormatType("ESML");
        GEMPAK = new DataFormatType("GEMPAK");
        GINI = new DataFormatType("GINI");
        GRIB1 = new DataFormatType("GRIB-1");
        GRIB2 = new DataFormatType("GRIB-2");
        HDF4 = new DataFormatType("HDF4");
        HDF5 = new DataFormatType("HDF5");
        NETCDF = new DataFormatType("NetCDF");
        NEXRAD2 = new DataFormatType("NEXRAD2");
        NCML = new DataFormatType("NcML");
        NIDS = new DataFormatType("NIDS");
        MCIDAS_AREA = new DataFormatType("McIDAS-AREA");
        GIF = new DataFormatType("image/gif");
        JPEG = new DataFormatType("image/jpeg");
        TIFF = new DataFormatType("image/tiff");
        PLAIN = new DataFormatType("text/plain");
        TSV = new DataFormatType("text/tab-separated-values");
        XML = new DataFormatType("text/xml");
        MPEG = new DataFormatType("video/mpeg");
        QUICKTIME = new DataFormatType("video/quicktime");
        REALTIME = new DataFormatType("video/realtime");
        OTHER_UNKNOWN = new DataFormatType("other/unknown");
    }
}
