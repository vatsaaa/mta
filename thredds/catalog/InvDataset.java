// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.util.Collection;
import java.net.URI;
import java.util.Iterator;
import java.util.ArrayList;
import ucar.nc2.units.DateRange;
import ucar.nc2.units.DateType;
import ucar.nc2.constants.FeatureType;
import java.util.List;

public abstract class InvDataset
{
    protected InvCatalog catalog;
    protected InvDataset parent;
    protected String name;
    protected String id;
    protected List<InvDataset> datasets;
    protected boolean harvest;
    protected CollectionType collectionType;
    protected List<InvAccess> access;
    protected List<InvService> services;
    protected String authorityName;
    protected FeatureType dataType;
    protected InvService defaultService;
    protected DataFormatType dataFormatType;
    protected String restrictAccess;
    protected List<InvDocumentation> docs;
    protected List<InvMetadata> metadata;
    protected List<InvProperty> properties;
    protected List<ThreddsMetadata.Source> creators;
    protected List<ThreddsMetadata.Contributor> contributors;
    protected List<DateType> dates;
    protected List<ThreddsMetadata.Vocab> keywords;
    protected List<ThreddsMetadata.Vocab> projects;
    protected List<ThreddsMetadata.Source> publishers;
    protected List<ThreddsMetadata.Variables> variables;
    public ThreddsMetadata.GeospatialCoverage gc;
    public DateRange tc;
    
    protected InvDataset(final InvDataset parent, final String name) {
        this.datasets = new ArrayList<InvDataset>();
        this.access = new ArrayList<InvAccess>();
        this.services = new ArrayList<InvService>();
        this.parent = parent;
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getFullName() {
        return (this.parent == null) ? this.name : ((this.parent.getFullName() == null || this.parent.getFullName().equals("")) ? this.name : (this.parent.getFullName() + "/" + this.name));
    }
    
    public CollectionType getCollectionType() {
        return this.collectionType;
    }
    
    public boolean isHarvest() {
        return this.harvest;
    }
    
    public String getID() {
        return this.id;
    }
    
    public String getUniqueID() {
        final String authority = this.getAuthority();
        if (authority != null && this.getID() != null) {
            return authority + ":" + this.getID();
        }
        if (this.getID() != null) {
            return this.getID();
        }
        return null;
    }
    
    public String getAuthority() {
        return this.authorityName;
    }
    
    public FeatureType getDataType() {
        return this.dataType;
    }
    
    public DataFormatType getDataFormatType() {
        return this.dataFormatType;
    }
    
    public boolean hasAccess() {
        return !this.access.isEmpty();
    }
    
    public List<InvAccess> getAccess() {
        return this.access;
    }
    
    public InvAccess getAccess(final ServiceType type) {
        for (final InvAccess a : this.getAccess()) {
            final InvService s = a.getService();
            if (s.getServiceType() == type) {
                return a;
            }
        }
        return null;
    }
    
    public InvAccess findAccess(final String accessURL) {
        for (final InvAccess a : this.getAccess()) {
            if (accessURL.equals(a.getStandardUrlName())) {
                return a;
            }
        }
        return null;
    }
    
    public String getSubsetUrl() {
        if (this.getID() == null) {
            return null;
        }
        return "catalog=" + this.getParentCatalog().baseURI.toString() + "&amp;dataset=" + this.getID();
    }
    
    public boolean hasNestedDatasets() {
        return !this.getDatasets().isEmpty();
    }
    
    public List<InvDataset> getDatasets() {
        return this.datasets;
    }
    
    public InvDatasetImpl findDatasetByName(final String name) {
        for (final InvDataset ds : this.getDatasets()) {
            if (ds.getName().equals(name)) {
                return (InvDatasetImpl)ds;
            }
        }
        return null;
    }
    
    public InvDataset getParent() {
        return this.parent;
    }
    
    public InvCatalog getParentCatalog() {
        if (this.catalog != null) {
            return this.catalog;
        }
        return (this.parent != null) ? this.parent.getParentCatalog() : null;
    }
    
    public String getCatalogUrl() {
        return this.getParentCatalog().getUriString() + "#" + this.getID();
    }
    
    public List<InvDocumentation> getDocumentation() {
        return this.docs;
    }
    
    public List<InvProperty> getProperties() {
        return this.properties;
    }
    
    public String findProperty(final String name) {
        InvProperty result = null;
        for (final InvProperty p : this.getProperties()) {
            if (p.getName().equals(name)) {
                result = p;
            }
        }
        return (result == null) ? null : result.getValue();
    }
    
    public List<InvMetadata> getMetadata() {
        return this.metadata;
    }
    
    public List<InvMetadata> getMetadata(final MetadataType want) {
        final List<InvMetadata> result = new ArrayList<InvMetadata>();
        for (final InvMetadata m : this.getMetadata()) {
            final MetadataType mtype = MetadataType.getType(m.getMetadataType());
            if (mtype == want) {
                result.add(m);
            }
        }
        return result;
    }
    
    public InvService findService(final String name) {
        if (name == null) {
            return null;
        }
        for (final InvService p : this.services) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        if (this.parent != null) {
            return this.parent.findService(name);
        }
        return (this.catalog == null) ? null : this.catalog.findService(name);
    }
    
    public InvService getServiceDefault() {
        return this.defaultService;
    }
    
    public String getRestrictAccess() {
        if (this.restrictAccess != null) {
            return this.restrictAccess;
        }
        if (this.parent != null) {
            return this.parent.getRestrictAccess();
        }
        return null;
    }
    
    public List<ThreddsMetadata.Source> getCreators() {
        return this.creators;
    }
    
    public List<ThreddsMetadata.Contributor> getContributors() {
        return this.contributors;
    }
    
    public List<DateType> getDates() {
        return this.dates;
    }
    
    public List<ThreddsMetadata.Vocab> getKeywords() {
        return this.keywords;
    }
    
    public List<ThreddsMetadata.Vocab> getProjects() {
        return this.projects;
    }
    
    public List<ThreddsMetadata.Source> getPublishers() {
        return this.publishers;
    }
    
    public String getHistory() {
        return this.getDocumentation("history");
    }
    
    public String getProcessing() {
        return this.getDocumentation("processing_level");
    }
    
    public String getRights() {
        return this.getDocumentation("rights");
    }
    
    public String getSummary() {
        return this.getDocumentation("summary");
    }
    
    public List<ThreddsMetadata.Variables> getVariables() {
        return this.variables;
    }
    
    public ThreddsMetadata.Variables getVariables(final String vocab) {
        final ThreddsMetadata.Variables result = new ThreddsMetadata.Variables(vocab, null, null, null, null);
        if (this.variables == null) {
            return result;
        }
        for (final ThreddsMetadata.Variables vs : this.variables) {
            if (vs.getVocabulary().equals(vocab)) {
                result.getVariableList().addAll(vs.getVariableList());
            }
        }
        return result;
    }
    
    public ThreddsMetadata.GeospatialCoverage getGeospatialCoverage() {
        return this.gc;
    }
    
    public DateRange getTimeCoverage() {
        return this.tc;
    }
    
    public String getDocumentation(final String type) {
        for (final InvDocumentation doc : this.getDocumentation()) {
            final String dtype = doc.getType();
            if (dtype != null && dtype.equalsIgnoreCase(type)) {
                return doc.getInlineContent();
            }
        }
        return null;
    }
}
