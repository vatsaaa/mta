// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.net.URISyntaxException;
import java.net.URI;

public class InvAccessImpl extends InvAccess
{
    private StringBuilder log;
    protected String serviceName;
    private String serviceTypeName;
    private String dataFormatName;
    private volatile int hashCode;
    
    public InvAccessImpl(final InvDataset dataset, final String urlPath, final InvService service) {
        this.log = new StringBuilder();
        this.hashCode = 0;
        this.dataset = dataset;
        this.urlPath = urlPath.trim();
        this.service = service;
    }
    
    protected InvAccessImpl(final InvDataset dataset, final String urlPath) {
        this.log = new StringBuilder();
        this.hashCode = 0;
        this.dataset = dataset;
        this.urlPath = urlPath.trim();
    }
    
    public InvAccessImpl(final InvDataset dataset, final String urlPath, final String serviceName, final String typeName, final String dataFormatName, final double dataSize) {
        this.log = new StringBuilder();
        this.hashCode = 0;
        this.dataset = dataset;
        this.urlPath = urlPath;
        this.serviceName = serviceName;
        this.serviceTypeName = typeName;
        this.dataFormatName = dataFormatName;
        this.dataSize = dataSize;
        if (typeName != null) {
            if (serviceName != null) {
                this.log.append("**InvAccess in (").append(dataset.getFullName()).append("):cannot declare both service (").append(serviceName).append(") and serviceType <").append(typeName).append(">\n");
            }
            else {
                this.service = new InvService("", typeName, "", "", null);
            }
        }
    }
    
    public boolean finish() {
        if (this.serviceName != null) {
            this.service = this.dataset.findService(this.serviceName);
            if (this.service == null) {
                this.log.append("**InvAccess in (").append(this.dataset.getFullName()).append("): has unknown service named (").append(this.serviceName).append(")\n");
            }
        }
        try {
            new URI(this.urlPath);
        }
        catch (URISyntaxException e) {
            this.log.append("**InvAccess in (").append(this.dataset.getFullName()).append("):\n   urlPath= ").append(this.urlPath).append(")\n  URISyntaxException=").append(e.getMessage());
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "service:(" + this.service.getName() + ") path:(" + this.urlPath + ")";
    }
    
    public String getServiceName() {
        return this.serviceName;
    }
    
    public String getServiceType() {
        return this.serviceTypeName;
    }
    
    public String getDataFormatName() {
        return this.dataFormatName;
    }
    
    public void setSize(final double dataSize) {
        this.dataSize = dataSize;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof InvAccessImpl && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.urlPath != null) {
                result = 37 * result + this.urlPath.hashCode();
            }
            if (this.service != null) {
                result = 37 * result + this.service.hashCode();
            }
            if (this.hasDataSize()) {
                result = 37 * result + (int)this.getDataSize();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    boolean check(final StringBuilder out, final boolean show) {
        boolean isValid = true;
        if (this.log.length() > 0) {
            isValid = false;
            out.append((CharSequence)this.log);
        }
        if (this.getService() == null) {
            out.append("**InvAccess in (").append(this.dataset.getFullName()).append("): with urlPath= (").append(this.urlPath).append(") has no valid service\n");
            isValid = false;
        }
        else if (this.getStandardUrlName() == null) {
            out.append("**InvAccess in (").append(this.dataset.getFullName()).append("): with urlPath= (").append(this.urlPath).append(") has invalid URL\n");
            isValid = false;
        }
        if (show) {
            System.out.println("   access " + this.urlPath + " valid = " + isValid);
        }
        return isValid;
    }
}
