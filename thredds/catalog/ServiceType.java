// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.util.List;

public final class ServiceType
{
    private static List<ServiceType> members;
    public static final ServiceType NONE;
    public static final ServiceType ADDE;
    public static final ServiceType DODS;
    public static final ServiceType OPENDAP;
    public static final ServiceType OPENDAPG;
    public static final ServiceType HTTPServer;
    public static final ServiceType FTP;
    public static final ServiceType GRIDFTP;
    public static final ServiceType FILE;
    public static final ServiceType LAS;
    public static final ServiceType WMS;
    public static final ServiceType WFS;
    public static final ServiceType WCS;
    public static final ServiceType WSDL;
    public static final ServiceType WebForm;
    public static final ServiceType CATALOG;
    public static final ServiceType QC;
    public static final ServiceType RESOLVER;
    public static final ServiceType COMPOUND;
    public static final ServiceType THREDDS;
    public static final ServiceType NetcdfSubset;
    public static final ServiceType CdmRemote;
    public static final ServiceType NETCDF;
    public static final ServiceType HTTP;
    public static final ServiceType NetcdfServer;
    private String name;
    
    private ServiceType(final String s) {
        this.name = s;
        ServiceType.members.add(this);
    }
    
    private ServiceType(final String name, final boolean fake) {
        this.name = name;
    }
    
    public static Collection<ServiceType> getAllTypes() {
        return ServiceType.members;
    }
    
    public static ServiceType findType(final String name) {
        if (name == null) {
            return null;
        }
        for (final ServiceType serviceType : ServiceType.members) {
            if (serviceType.name.equalsIgnoreCase(name)) {
                return serviceType;
            }
        }
        return null;
    }
    
    public static ServiceType getType(final String name) {
        if (name == null) {
            return null;
        }
        final ServiceType type = findType(name);
        return (type != null) ? type : new ServiceType(name, false);
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof ServiceType && o.hashCode() == this.hashCode());
    }
    
    static {
        ServiceType.members = new ArrayList<ServiceType>(20);
        NONE = new ServiceType("");
        ADDE = new ServiceType("ADDE");
        DODS = new ServiceType("DODS");
        OPENDAP = new ServiceType("OPENDAP");
        OPENDAPG = new ServiceType("OPENDAP-G");
        HTTPServer = new ServiceType("HTTPServer");
        FTP = new ServiceType("FTP");
        GRIDFTP = new ServiceType("GridFTP");
        FILE = new ServiceType("File");
        LAS = new ServiceType("LAS");
        WMS = new ServiceType("WMS");
        WFS = new ServiceType("WFS");
        WCS = new ServiceType("WCS");
        WSDL = new ServiceType("WSDL");
        WebForm = new ServiceType("WebForm");
        CATALOG = new ServiceType("Catalog");
        QC = new ServiceType("QueryCapability");
        RESOLVER = new ServiceType("Resolver");
        COMPOUND = new ServiceType("Compound");
        THREDDS = new ServiceType("THREDDS");
        NetcdfSubset = new ServiceType("NetcdfSubset");
        CdmRemote = new ServiceType("CdmRemote");
        NETCDF = new ServiceType("NetCDF");
        HTTP = new ServiceType("HTTP");
        NetcdfServer = new ServiceType("NetcdfServer");
    }
}
