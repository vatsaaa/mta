// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import thredds.catalog.util.DeepCopyUtils;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.net.URI;
import thredds.cataloggen.DatasetScanCatalogBuilder;
import thredds.cataloggen.CatalogBuilder;
import java.lang.reflect.InvocationTargetException;
import java.io.IOException;
import thredds.crawlabledataset.CrawlableDatasetFactory;
import thredds.cataloggen.ProxyDatasetHandler;
import thredds.cataloggen.datasetenhancer.RegExpAndDurationTimeCoverageEnhancer;
import java.util.ArrayList;
import thredds.cataloggen.inserter.SimpleLatestProxyDsHandler;
import java.util.HashMap;
import thredds.crawlabledataset.sorter.LexigraphicByNameSorter;
import thredds.crawlabledataset.filter.RegExpMatchOnNameFilter;
import thredds.cataloggen.CatalogRefExpander;
import thredds.cataloggen.DatasetEnhancer;
import java.util.List;
import java.util.Map;
import thredds.crawlabledataset.CrawlableDatasetSorter;
import thredds.crawlabledataset.CrawlableDatasetLabeler;
import thredds.crawlabledataset.CrawlableDatasetFilter;
import thredds.crawlabledataset.CrawlableDataset;
import org.slf4j.Logger;

public class InvDatasetScan extends InvCatalogRef
{
    private static Logger log;
    private static String context;
    private static String catalogServletName;
    private final String rootPath;
    private String scanLocation;
    private CrawlableDataset scanLocationCrDs;
    private final String crDsClassName;
    private final Object crDsConfigObj;
    private final CrawlableDatasetFilter filter;
    private CrawlableDatasetLabeler identifier;
    private CrawlableDatasetLabeler namer;
    private CrawlableDatasetSorter sorter;
    private Map proxyDatasetHandlers;
    private boolean addDatasetSize;
    private List<DatasetEnhancer> childEnhancerList;
    private CatalogRefExpander catalogRefExpander;
    private boolean isValid;
    private StringBuilder invalidMessage;
    
    public static void setContext(final String c) {
        InvDatasetScan.context = c;
    }
    
    public static void setCatalogServletName(final String catServletName) {
        InvDatasetScan.catalogServletName = catServletName;
    }
    
    private static String makeHref(final String path) {
        return InvDatasetScan.context + ((InvDatasetScan.catalogServletName == null) ? "" : InvDatasetScan.catalogServletName) + "/" + path + "/catalog.xml";
    }
    
    public InvDatasetScan(final InvDatasetImpl parent, final String name, final String path, final String scanLocation, final String id, final InvDatasetScan from) {
        this(parent, name, path, scanLocation, from.crDsClassName, from.crDsConfigObj, from.filter, from.identifier, from.namer, from.addDatasetSize, from.sorter, from.proxyDatasetHandlers, from.childEnhancerList, from.catalogRefExpander);
        this.setID(id);
    }
    
    public InvDatasetScan(final InvCatalogImpl catalog, final InvDatasetImpl parent, final String name, final String path, final String scanLocation, final String filter, final boolean addDatasetSize, final String addLatest, final boolean sortOrderIncreasing, final String datasetNameMatchPattern, final String startTimeSubstitutionPattern, final String duration) {
        super(parent, name, makeHref(path));
        InvDatasetScan.log.debug("InvDatasetScan(): parent=" + parent + ", name=" + name + " , path=" + path + " , scanLocation=" + scanLocation + " , filter=" + filter + " , addLatest=" + addLatest + " , sortOrderIncreasing=" + sortOrderIncreasing + " , datasetNameMatchPattern=" + datasetNameMatchPattern + " , startTimeSubstitutionPattern= " + startTimeSubstitutionPattern + ", duration=" + duration);
        this.rootPath = path;
        this.scanLocation = scanLocation;
        this.crDsClassName = null;
        this.crDsConfigObj = null;
        this.scanLocationCrDs = this.createScanLocationCrDs();
        this.isValid = true;
        if (this.scanLocationCrDs == null) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: could not create CrawlableDataset for scanLocation.");
        }
        else if (!this.scanLocationCrDs.exists()) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation does not exist.");
        }
        else if (!this.scanLocationCrDs.isCollection()) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation not a collection.");
        }
        if (filter != null) {
            this.filter = new RegExpMatchOnNameFilter(filter);
        }
        else {
            this.filter = null;
        }
        this.identifier = null;
        this.namer = null;
        this.addDatasetSize = addDatasetSize;
        this.sorter = new LexigraphicByNameSorter(sortOrderIncreasing);
        this.proxyDatasetHandlers = new HashMap();
        if (addLatest != null && addLatest.equalsIgnoreCase("true")) {
            final InvService service = catalog.findService("latest");
            if (service != null) {
                final ProxyDatasetHandler proxyDsHandler = new SimpleLatestProxyDsHandler("latest.xml", true, service, true);
                this.proxyDatasetHandlers.put("latest.xml", proxyDsHandler);
            }
        }
        if (datasetNameMatchPattern != null && startTimeSubstitutionPattern != null && duration != null) {
            (this.childEnhancerList = new ArrayList<DatasetEnhancer>()).add(RegExpAndDurationTimeCoverageEnhancer.getInstanceToMatchOnDatasetName(datasetNameMatchPattern, startTimeSubstitutionPattern, duration));
        }
    }
    
    public InvDatasetScan(final InvCatalogImpl catalog, final InvDatasetImpl parent, final String name, final String path, final String scanLocation, final CrawlableDatasetFilter filter, final boolean addDatasetSize, final String addLatest, final boolean sortOrderIncreasing, final String datasetNameMatchPattern, final String startTimeSubstitutionPattern, final String duration) {
        super(parent, name, makeHref(path));
        InvDatasetScan.log.debug("InvDatasetScan(): parent=" + parent + ", name=" + name + " , path=" + path + " , scanLocation=" + scanLocation + " , filter=" + filter + " , addLatest=" + addLatest + " , sortOrderIncreasing=" + sortOrderIncreasing + " , datasetNameMatchPattern=" + datasetNameMatchPattern + " , startTimeSubstitutionPattern= " + startTimeSubstitutionPattern + ", duration=" + duration);
        this.rootPath = path;
        this.scanLocation = scanLocation;
        this.filter = filter;
        this.crDsClassName = null;
        this.crDsConfigObj = null;
        this.scanLocationCrDs = this.createScanLocationCrDs();
        this.isValid = true;
        if (this.scanLocationCrDs == null) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: could not create CrawlableDataset for scanLocation.");
        }
        else if (!this.scanLocationCrDs.exists()) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation does not exist.");
        }
        else if (!this.scanLocationCrDs.isCollection()) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation not a collection.");
        }
        this.identifier = null;
        this.namer = null;
        this.addDatasetSize = addDatasetSize;
        this.sorter = new LexigraphicByNameSorter(sortOrderIncreasing);
        this.proxyDatasetHandlers = new HashMap();
        if (addLatest != null && addLatest.equalsIgnoreCase("true")) {
            final InvService service = catalog.findService("latest");
            if (service != null) {
                final ProxyDatasetHandler proxyDsHandler = new SimpleLatestProxyDsHandler("latest.xml", true, service, true);
                this.proxyDatasetHandlers.put("latest.xml", proxyDsHandler);
            }
        }
        if (datasetNameMatchPattern != null && startTimeSubstitutionPattern != null && duration != null) {
            (this.childEnhancerList = new ArrayList<DatasetEnhancer>()).add(RegExpAndDurationTimeCoverageEnhancer.getInstanceToMatchOnDatasetName(datasetNameMatchPattern, startTimeSubstitutionPattern, duration));
        }
    }
    
    public InvDatasetScan(final InvDatasetImpl parent, final String name, final String path, final String scanLocation, final String configClassName, final Object configObj, final CrawlableDatasetFilter filter, final CrawlableDatasetLabeler identifier, final CrawlableDatasetLabeler namer, final boolean addDatasetSize, final CrawlableDatasetSorter sorter, final Map proxyDatasetHandlers, final List childEnhancerList, final CatalogRefExpander catalogRefExpander) {
        super(parent, name, makeHref(path));
        this.rootPath = path;
        this.scanLocation = scanLocation;
        this.crDsClassName = configClassName;
        this.crDsConfigObj = configObj;
        this.scanLocationCrDs = this.createScanLocationCrDs();
        this.isValid = true;
        if (this.scanLocationCrDs == null) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: could not create CrawlableDataset for scanLocation.");
        }
        else if (!this.scanLocationCrDs.exists()) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation does not exist.");
        }
        else if (!this.scanLocationCrDs.isCollection()) {
            this.isValid = false;
            this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(path).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation not a collection.");
        }
        this.filter = filter;
        this.identifier = identifier;
        this.namer = namer;
        this.addDatasetSize = addDatasetSize;
        this.sorter = sorter;
        this.childEnhancerList = (List<DatasetEnhancer>)childEnhancerList;
        this.catalogRefExpander = catalogRefExpander;
        if (proxyDatasetHandlers == null) {
            this.proxyDatasetHandlers = new HashMap();
        }
        else {
            this.proxyDatasetHandlers = proxyDatasetHandlers;
        }
    }
    
    public String getPath() {
        return this.rootPath;
    }
    
    public String getScanLocation() {
        return this.scanLocation;
    }
    
    public void setScanLocation(final String scanLocation) {
        if (!scanLocation.equals(this.scanLocation)) {
            this.isValid = true;
            this.scanLocation = scanLocation;
            this.scanLocationCrDs = this.createScanLocationCrDs();
            if (this.scanLocationCrDs == null) {
                this.isValid = false;
                this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(this.rootPath).append("; scanLocation=").append(scanLocation).append(">: could not create CrawlableDataset for scanLocation.");
            }
            else if (!this.scanLocationCrDs.exists()) {
                this.isValid = false;
                this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(this.rootPath).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation does not exist.");
            }
            else if (!this.scanLocationCrDs.isCollection()) {
                this.isValid = false;
                this.invalidMessage = new StringBuilder("Invalid InvDatasetScan <path=").append(this.rootPath).append("; scanLocation=").append(scanLocation).append(">: CrawlableDataset for scanLocation not a collection.");
            }
        }
    }
    
    public String getCrDsClassName() {
        return this.crDsClassName;
    }
    
    public Object getCrDsConfigObj() {
        return this.crDsConfigObj;
    }
    
    public CrawlableDatasetFilter getFilter() {
        return this.filter;
    }
    
    public CrawlableDatasetLabeler getIdentifier() {
        return this.identifier;
    }
    
    public CrawlableDatasetLabeler getNamer() {
        return this.namer;
    }
    
    public CrawlableDatasetSorter getSorter() {
        return this.sorter;
    }
    
    public Map getProxyDatasetHandlers() {
        return this.proxyDatasetHandlers;
    }
    
    public boolean getAddDatasetSize() {
        return this.addDatasetSize;
    }
    
    public List getChildEnhancerList() {
        return this.childEnhancerList;
    }
    
    public CatalogRefExpander getCatalogRefExpander() {
        return this.catalogRefExpander;
    }
    
    public boolean isValid() {
        return this.isValid;
    }
    
    public String getInvalidMessage() {
        return this.invalidMessage.toString();
    }
    
    private CrawlableDataset createScanLocationCrDs() {
        CrawlableDataset scanLocationCrDs;
        try {
            scanLocationCrDs = CrawlableDatasetFactory.createCrawlableDataset(this.scanLocation, this.crDsClassName, this.crDsConfigObj);
        }
        catch (IllegalAccessException e) {
            InvDatasetScan.log.error("createScanLocationCrDs(): failed to create CrawlableDataset for collectionLevel <" + this.scanLocation + "> and class <" + this.crDsClassName + ">: " + e.getMessage());
            return null;
        }
        catch (NoSuchMethodException e2) {
            InvDatasetScan.log.error("createScanLocationCrDs(): failed to create CrawlableDataset for collectionLevel <" + this.scanLocation + "> and class <" + this.crDsClassName + ">: " + e2.getMessage());
            return null;
        }
        catch (IOException e3) {
            InvDatasetScan.log.error("createScanLocationCrDs(): failed to create CrawlableDataset for collectionLevel <" + this.scanLocation + "> and class <" + this.crDsClassName + ">: " + e3.getMessage());
            return null;
        }
        catch (InvocationTargetException e4) {
            InvDatasetScan.log.error("createScanLocationCrDs(): failed to create CrawlableDataset for collectionLevel <" + this.scanLocation + "> and class <" + this.crDsClassName + ">: " + e4.getMessage());
            return null;
        }
        catch (InstantiationException e5) {
            InvDatasetScan.log.error("createScanLocationCrDs(): failed to create CrawlableDataset for collectionLevel <" + this.scanLocation + "> and class <" + this.crDsClassName + ">: " + e5.getMessage());
            return null;
        }
        catch (ClassNotFoundException e6) {
            InvDatasetScan.log.error("createScanLocationCrDs(): failed to create CrawlableDataset for collectionLevel <" + this.scanLocation + "> and class <" + this.crDsClassName + ">: " + e6.getMessage());
            return null;
        }
        return scanLocationCrDs;
    }
    
    private CatalogBuilder buildCatalogBuilder() {
        final InvService service = this.getServiceDefault();
        DatasetScanCatalogBuilder dsScanCatBuilder;
        try {
            dsScanCatBuilder = new DatasetScanCatalogBuilder(this, this.scanLocationCrDs, service);
        }
        catch (IllegalArgumentException e) {
            InvDatasetScan.log.error("buildCatalogBuilder(): failed to create CatalogBuilder for this collection <" + this.scanLocationCrDs.getPath() + ">: " + e.getMessage());
            return null;
        }
        return dsScanCatBuilder;
    }
    
    public String translatePathToLocation(String dsPath) {
        if (dsPath == null) {
            return null;
        }
        if (dsPath.length() > 0 && dsPath.startsWith("/")) {
            dsPath = dsPath.substring(1);
        }
        if (!dsPath.startsWith(this.getPath())) {
            return null;
        }
        String dataDir = dsPath.substring(this.getPath().length());
        if (dataDir.startsWith("/")) {
            dataDir = dataDir.substring(1);
        }
        final CrawlableDataset curCrDs = this.scanLocationCrDs.getDescendant(dataDir);
        if (InvDatasetScan.log.isDebugEnabled()) {
            InvDatasetScan.log.debug("translatePathToLocation(): url dsPath= " + dsPath + " to dataset dsPath= " + curCrDs.getPath());
        }
        return curCrDs.getPath();
    }
    
    public CrawlableDataset requestCrawlableDataset(final String path) throws IOException {
        final String crDsPath = this.translatePathToLocation(path);
        if (crDsPath == null) {
            return null;
        }
        final CatalogBuilder catBuilder = this.buildCatalogBuilder();
        if (catBuilder == null) {
            return null;
        }
        return catBuilder.requestCrawlableDataset(crDsPath);
    }
    
    public InvCatalogImpl makeCatalogForDirectory(final String orgPath, final URI baseURI) {
        if (InvDatasetScan.log.isDebugEnabled()) {
            InvDatasetScan.log.debug("baseURI=" + baseURI);
            InvDatasetScan.log.debug("orgPath=" + orgPath);
            InvDatasetScan.log.debug("rootPath=" + this.rootPath);
            InvDatasetScan.log.debug("scanLocation=" + this.scanLocation);
        }
        String dsDirPath = this.translatePathToLocation(orgPath);
        if (dsDirPath == null) {
            final String tmpMsg = "makeCatalogForDirectory(): Requsting path <" + orgPath + "> must start with \"" + this.rootPath + "\".";
            InvDatasetScan.log.error(tmpMsg);
            return null;
        }
        String dsPath = dsDirPath.substring(this.scanLocationCrDs.getPath().length());
        if (dsPath.startsWith("/")) {
            dsPath = dsPath.substring(1);
        }
        final CrawlableDataset reqCrDs = this.scanLocationCrDs.getDescendant(dsPath);
        dsDirPath = reqCrDs.getParentDataset().getPath();
        final CatalogBuilder catBuilder = this.buildCatalogBuilder();
        if (catBuilder == null) {
            return null;
        }
        CrawlableDataset catalogCrDs;
        try {
            catalogCrDs = catBuilder.requestCrawlableDataset(dsDirPath);
        }
        catch (IOException e) {
            InvDatasetScan.log.error("makeCatalogForDirectory(): I/O error getting catalog level <" + dsDirPath + ">: " + e.getMessage(), e);
            return null;
        }
        if (catalogCrDs == null) {
            InvDatasetScan.log.warn("makeCatalogForDirectory(): requested catalog level <" + dsDirPath + "> not allowed (filtered out).");
            return null;
        }
        if (!catalogCrDs.isCollection()) {
            InvDatasetScan.log.warn("makeCatalogForDirectory(): requested catalog level <" + dsDirPath + "> is not a collection.");
            return null;
        }
        InvCatalogImpl catalog;
        try {
            catalog = catBuilder.generateCatalog(catalogCrDs);
        }
        catch (IOException e2) {
            InvDatasetScan.log.error("makeCatalogForDirectory(): catalog generation failed <" + catalogCrDs.getPath() + ">: " + e2.getMessage());
            return null;
        }
        if (catalog != null) {
            catalog.setBaseURI(baseURI);
        }
        return catalog;
    }
    
    public InvCatalogImpl makeProxyDsResolverCatalog(final String path, final URI baseURI) {
        if (path == null) {
            return null;
        }
        if (path.endsWith("/")) {
            return null;
        }
        String dsDirPath = this.translatePathToLocation(path);
        if (dsDirPath == null) {
            InvDatasetScan.log.error("makeProxyDsResolverCatalog(): Requsting path <" + path + "> must start with \"" + this.rootPath + "\".");
            return null;
        }
        final int pos = dsDirPath.lastIndexOf("/");
        if (pos == -1) {
            InvDatasetScan.log.error("makeProxyDsResolverCatalog(): Requsting path <" + path + "> must contain a slash (\"/\").");
            return null;
        }
        final String dsName = dsDirPath.substring(pos + 1);
        dsDirPath = dsDirPath.substring(0, pos);
        final ProxyDatasetHandler pdh = this.getProxyDatasetHandlers().get(dsName);
        if (pdh == null) {
            InvDatasetScan.log.error("makeProxyDsResolverCatalog(): No matching proxy dataset handler found <" + dsName + ">.");
            return null;
        }
        final CatalogBuilder catBuilder = this.buildCatalogBuilder();
        if (catBuilder == null) {
            return null;
        }
        CrawlableDataset catalogCrDs;
        try {
            catalogCrDs = catBuilder.requestCrawlableDataset(dsDirPath);
        }
        catch (IOException e) {
            InvDatasetScan.log.error("makeProxyDsResolverCatalog(): failed to create CrawlableDataset for catalogLevel <" + dsDirPath + "> and class <" + this.crDsClassName + ">: " + e.getMessage(), e);
            return null;
        }
        if (catalogCrDs == null) {
            InvDatasetScan.log.warn("makeProxyDsResolverCatalog(): requested catalog level <" + dsDirPath + "> not allowed (filtered out).");
            return null;
        }
        if (!catalogCrDs.isCollection()) {
            InvDatasetScan.log.warn("makeProxyDsResolverCatalog(): requested catalog level <" + dsDirPath + "> not a collection.");
            return null;
        }
        InvCatalogImpl catalog;
        try {
            catalog = catBuilder.generateProxyDsResolverCatalog(catalogCrDs, pdh);
        }
        catch (IOException e2) {
            InvDatasetScan.log.error("makeProxyDsResolverCatalog(): catalog generation failed <" + catalogCrDs.getPath() + ">: " + e2.getMessage());
            return null;
        }
        if (catalog != null) {
            catalog.setBaseURI(baseURI);
        }
        return catalog;
    }
    
    @Deprecated
    public InvCatalog makeLatestCatalogForDirectory(final String orgPath, final URI baseURI) {
        final InvCatalogImpl cat = this.makeCatalogForDirectory(orgPath, baseURI);
        if (cat == null) {
            return null;
        }
        final InvDatasetImpl topDs = cat.getDatasets().get(0);
        final Iterator it = topDs.getDatasets().iterator();
        while (it.hasNext()) {
            final InvDatasetImpl curDs = it.next();
            if (!curDs.hasAccess() || curDs.getUrlPath().endsWith("latest.xml")) {
                it.remove();
            }
        }
        if (topDs.getDatasets().isEmpty()) {
            return null;
        }
        final InvDatasetImpl latestDs = Collections.max((Collection<? extends InvDatasetImpl>)topDs.getDatasets(), (Comparator<? super InvDatasetImpl>)new Comparator() {
            public int compare(final Object obj1, final Object obj2) {
                final InvDataset ds1 = (InvDataset)obj1;
                final InvDataset ds2 = (InvDataset)obj2;
                return ds1.getName().compareTo(ds2.getName());
            }
        });
        final String baseName = topDs.getName();
        final String latestName = baseName.equals("") ? "Latest" : ("Latest " + baseName);
        latestDs.setName(latestName);
        final InvCatalog subsetCat = DeepCopyUtils.subsetCatalogOnDataset(cat, latestDs);
        return subsetCat;
    }
    
    @Override
    public boolean isRead() {
        return false;
    }
    
    @Override
    boolean check(final StringBuilder out, final boolean show) {
        boolean isValid = true;
        if (this.getPath() == null) {
            out.append("**Error: DatasetScan (").append(this.getFullName()).append("): must have path attribute\n");
            isValid = false;
        }
        if (this.getScanLocation() == null) {
            out.append("**Error: DatasetScan (").append(this.getFullName()).append("): must have dirLocation attribute\n");
            isValid = false;
        }
        if (this.getServiceDefault() == null) {
            out.append("**Error: DatasetScan (").append(this.getFullName()).append("): must have a default service\n");
            isValid = false;
        }
        return isValid && super.check(out, show);
    }
    
    static {
        InvDatasetScan.log = LoggerFactory.getLogger(InvDatasetScan.class);
        InvDatasetScan.context = "/thredds";
        InvDatasetScan.catalogServletName = "/catalog";
    }
}
