// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.dl;

import java.io.InputStream;
import java.util.StringTokenizer;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.HashMap;

public class Grib1toDIF implements VocabTranslator
{
    private static Grib1toDIF singleton;
    private HashMap hash;
    private int maxLines;
    
    public static Grib1toDIF getInstance() throws IOException {
        if (Grib1toDIF.singleton == null) {
            Grib1toDIF.singleton = new Grib1toDIF();
        }
        return Grib1toDIF.singleton;
    }
    
    private Grib1toDIF() throws IOException {
        this.hash = new HashMap();
        this.maxLines = 1000;
        final Class c = this.getClass();
        final String resourceName = "/resources/thredds/dl/GRIB-GCMD.csv";
        final InputStream ios = c.getResourceAsStream(resourceName);
        if (ios == null) {
            throw new IOException("Cant find resource= " + resourceName);
        }
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        for (int count = 0; count < this.maxLines; ++count) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            final StringTokenizer stoker = new StringTokenizer(line, ",");
            final String category = stoker.nextToken().trim();
            final String topic = stoker.nextToken().trim();
            final String term = stoker.nextToken().trim();
            final String var = stoker.nextToken().trim();
            final String gribNo = stoker.nextToken().trim();
            final String difParam = category + " > " + topic + " > " + term + " > " + var;
            this.hash.put(gribNo, difParam);
        }
        dataIS.close();
    }
    
    public String translate(final String fromId) {
        final int pos = fromId.lastIndexOf(",");
        final String paramNo = fromId.substring(pos + 1).trim();
        return this.hash.get(paramNo);
    }
}
