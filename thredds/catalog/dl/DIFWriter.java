// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.dl;

import org.slf4j.LoggerFactory;
import thredds.catalog.InvCatalogFactory;
import java.util.StringTokenizer;
import thredds.catalog.InvAccess;
import java.net.URI;
import ucar.nc2.units.TimeDuration;
import ucar.nc2.units.DateRange;
import ucar.nc2.units.DateType;
import java.util.Date;
import thredds.catalog.ServiceType;
import ucar.unidata.geoloc.LatLonPointImpl;
import org.jdom.Content;
import java.util.HashMap;
import java.util.Iterator;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import thredds.catalog.XMLEntityResolver;
import org.jdom.Document;
import org.jdom.Element;
import java.util.List;
import thredds.catalog.ThreddsMetadata;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import ucar.unidata.util.StringUtil;
import ucar.nc2.util.CancelTask;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvDataset;
import thredds.catalog.crawl.CatalogCrawler;
import java.io.File;
import thredds.catalog.InvCatalogImpl;
import org.jdom.Namespace;
import org.slf4j.Logger;

public class DIFWriter
{
    private static Logger log;
    private static final Namespace defNS;
    private static String schemaLocation;
    private String fileDir;
    private StringBuffer messBuffer;
    private boolean debug;
    
    public DIFWriter() {
        this.debug = false;
    }
    
    public void writeDatasetEntries(final InvCatalogImpl cat, final String fileDir, final StringBuffer mess) {
        this.fileDir = fileDir;
        this.messBuffer = mess;
        final File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final CatalogCrawler.Listener listener = new CatalogCrawler.Listener() {
            public void getDataset(final InvDataset ds, final Object context) {
                DIFWriter.this.doOneDataset(ds);
            }
            
            public boolean getCatalogRef(final InvCatalogRef dd, final Object context) {
                return true;
            }
        };
        final ByteArrayOutputStream bis = new ByteArrayOutputStream();
        final PrintStream ps = new PrintStream(bis);
        final CatalogCrawler crawler = new CatalogCrawler(0, true, listener);
        crawler.crawl(cat, null, ps, null);
        mess.append("\n*********************\n");
        mess.append(bis.toString());
    }
    
    public void doOneDataset(final InvDataset ds) {
        if (this.debug) {
            System.out.println("doDataset " + ds.getName());
        }
        if (this.isDatasetUseable(ds, this.messBuffer)) {
            final String id = StringUtil.replace(ds.getID(), "/", "-");
            final String fileOutName = this.fileDir + "/" + id + ".dif.xml";
            try {
                final OutputStream out = new BufferedOutputStream(new FileOutputStream(fileOutName));
                this.writeOneEntry(ds, out, this.messBuffer);
                out.close();
                this.messBuffer.append(" OK on Write\n");
            }
            catch (IOException ioe) {
                this.messBuffer.append("DIFWriter failed on write " + ioe.getMessage() + "\n");
                DIFWriter.log.error("DIFWriter failed on write " + ioe.getMessage(), ioe);
            }
        }
    }
    
    public void doOneDataset(final InvDataset ds, final String fileDir, final StringBuffer mess) {
        if (this.debug) {
            System.out.println("doDataset " + ds.getName());
        }
        if (this.isDatasetUseable(ds, mess)) {
            final String id = StringUtil.replace(ds.getID(), "/", "-");
            final String fileOutName = fileDir + "/" + id + ".dif.xml";
            try {
                final OutputStream out = new BufferedOutputStream(new FileOutputStream(fileOutName));
                this.writeOneEntry(ds, out, mess);
                out.close();
                mess.append(" OK on Write\n");
            }
            catch (IOException ioe) {
                mess.append("DIFWriter failed on write " + ioe.getMessage() + "\n");
                DIFWriter.log.error("DIFWriter failed on write " + ioe.getMessage(), ioe);
            }
        }
    }
    
    public boolean isDatasetUseable(final InvDataset ds, final StringBuffer sbuff) {
        boolean ok = true;
        sbuff.append("Dataset " + ds.getName() + " id = " + ds.getID() + ": ");
        if (!ds.isHarvest()) {
            ok = false;
            sbuff.append("Dataset " + ds.getName() + " id = " + ds.getID() + " has harvest = false\n");
        }
        if (ds.getName() == null) {
            ok = false;
            sbuff.append(" missing Name field\n");
        }
        if (ds.getUniqueID() == null) {
            ok = false;
            sbuff.append(" missing ID field\n");
        }
        ThreddsMetadata.Variables vs = ds.getVariables("DIF");
        if (vs == null || vs.getVariableList().size() == 0) {
            vs = ds.getVariables("GRIB-1");
        }
        if (vs == null || vs.getVariableList().size() == 0) {
            vs = ds.getVariables("GRIB-2");
        }
        if (vs == null || vs.getVariableList().size() == 0) {
            ok = false;
            sbuff.append(" missing Variables with DIF or GRIB compatible vocabulary\n");
        }
        final List list = ds.getPublishers();
        if (list == null || list.size() == 0) {
            ok = false;
            sbuff.append(" must have publisher element that defines the data center\n");
        }
        final String summary = ds.getDocumentation("summary");
        if (summary == null) {
            ok = false;
            sbuff.append(" must have documentation element of type summary\n");
        }
        sbuff.append(" useable= " + ok + "\n");
        return ok;
    }
    
    private void writeOneEntry(final InvDataset ds, final OutputStream out, final StringBuffer mess) throws IOException {
        final Element rootElem = new Element("DIF", DIFWriter.defNS);
        final Document doc = new Document(rootElem);
        this.writeDataset(ds, rootElem, mess);
        rootElem.addNamespaceDeclaration(DIFWriter.defNS);
        rootElem.addNamespaceDeclaration(XMLEntityResolver.xsiNS);
        rootElem.setAttribute("schemaLocation", DIFWriter.defNS.getURI() + " " + DIFWriter.schemaLocation, XMLEntityResolver.xsiNS);
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(doc, out);
    }
    
    private Iterator translateGribVocabulary(final ThreddsMetadata.Variables vs, final boolean isGrib1, final StringBuffer mess) {
        if (vs == null) {
            return null;
        }
        VocabTranslator vt;
        try {
            vt = (isGrib1 ? Grib1toDIF.getInstance() : Grib2toDIF.getInstance());
        }
        catch (IOException e) {
            DIFWriter.log.error("DIFWriter failed opening GribtoDIF VocabTranslator ", e);
            return null;
        }
        final HashMap hash = new HashMap();
        final List vlist = vs.getVariableList();
        for (int j = 0; j < vlist.size(); ++j) {
            final ThreddsMetadata.Variable v = vlist.get(j);
            final String fromVocabId = v.getVocabularyId();
            if (fromVocabId == null) {
                mess.append("** no id for " + v.getName() + "\n");
            }
            else {
                final String toVocabName = vt.translate(fromVocabId);
                if (toVocabName == null) {
                    mess.append("** no translation for " + fromVocabId + " == " + v.getVocabularyName() + "\n");
                }
                else if (hash.get(toVocabName) == null) {
                    final ThreddsMetadata.Variable transV = new ThreddsMetadata.Variable(v.getName(), v.getDescription(), toVocabName, v.getUnits(), fromVocabId);
                    hash.put(toVocabName, transV);
                }
            }
        }
        return hash.values().iterator();
    }
    
    private void writeDataset(final InvDataset ds, final Element rootElem, final StringBuffer mess) {
        final String entryId = StringUtil.allow(ds.getUniqueID(), "_-.", '-');
        rootElem.addContent(new Element("Entry_ID", DIFWriter.defNS).addContent(entryId));
        rootElem.addContent(new Element("Entry_Title", DIFWriter.defNS).addContent(ds.getFullName()));
        ThreddsMetadata.Variables vs = ds.getVariables("DIF");
        final boolean hasVocab = vs != null && vs.getVariableList().size() != 0;
        if (hasVocab) {
            final List vlist = vs.getVariableList();
            for (int j = 0; j < vlist.size(); ++j) {
                final ThreddsMetadata.Variable v = vlist.get(j);
                this.writeVariable(rootElem, v);
            }
        }
        else {
            vs = ds.getVariables("GRIB-1");
            if (vs != null && vs.getVariableList().size() != 0) {
                final Iterator iter = this.translateGribVocabulary(vs, true, mess);
                while (iter.hasNext()) {
                    final ThreddsMetadata.Variable v2 = iter.next();
                    this.writeVariable(rootElem, v2);
                }
            }
            else {
                vs = ds.getVariables("GRIB-2");
                if (vs != null && vs.getVariableList().size() != 0) {
                    final Iterator iter = this.translateGribVocabulary(vs, false, mess);
                    while (iter != null && iter.hasNext()) {
                        final ThreddsMetadata.Variable v2 = iter.next();
                        this.writeVariable(rootElem, v2);
                    }
                }
            }
        }
        List list = ds.getKeywords();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); ++i) {
                final ThreddsMetadata.Vocab k = list.get(i);
                rootElem.addContent(new Element("Keyword", DIFWriter.defNS).addContent(k.getText()));
            }
        }
        final DateRange tm = ds.getTimeCoverage();
        if (tm != null) {
            final DateType end = tm.getEnd();
            if (end.isPresent()) {
                final TimeDuration duration = tm.getDuration();
                final double ndays = -duration.getValueInSeconds() / 3600.0 / 24.0;
                final String reletiveTime = "RELATIVE_START_DATE: " + (int)ndays;
                rootElem.addContent(new Element("Keyword", DIFWriter.defNS).addContent(reletiveTime));
            }
        }
        final Element platform = new Element("Source_Name", DIFWriter.defNS);
        rootElem.addContent(platform);
        platform.addContent(new Element("Short_Name", DIFWriter.defNS).addContent("MODELS"));
        if (tm != null) {
            final Element tmElem = new Element("Temporal_Coverage", DIFWriter.defNS);
            rootElem.addContent(tmElem);
            tmElem.addContent(new Element("Start_Date", DIFWriter.defNS).addContent(tm.getStart().toDateString()));
            tmElem.addContent(new Element("Stop_Date", DIFWriter.defNS).addContent(tm.getEnd().toDateString()));
        }
        final ThreddsMetadata.GeospatialCoverage geo = ds.getGeospatialCoverage();
        if (geo != null) {
            final Element geoElem = new Element("Spatial_Coverage", DIFWriter.defNS);
            rootElem.addContent(geoElem);
            final double eastNormal = LatLonPointImpl.lonNormal(geo.getLonEast());
            final double westNormal = LatLonPointImpl.lonNormal(geo.getLonWest());
            geoElem.addContent(new Element("Southernmost_Latitude", DIFWriter.defNS).addContent(Double.toString(geo.getLatSouth())));
            geoElem.addContent(new Element("Northernmost_Latitude", DIFWriter.defNS).addContent(Double.toString(geo.getLatNorth())));
            geoElem.addContent(new Element("Westernmost_Longitude", DIFWriter.defNS).addContent(Double.toString(westNormal)));
            geoElem.addContent(new Element("Easternmost_Longitude", DIFWriter.defNS).addContent(Double.toString(eastNormal)));
        }
        final String rights = ds.getDocumentation("rights");
        if (rights != null) {
            rootElem.addContent(new Element("Use_Constraints", DIFWriter.defNS).addContent(rights));
        }
        list = ds.getPublishers();
        if (list.size() > 0) {
            for (int l = 0; l < list.size(); ++l) {
                final ThreddsMetadata.Source p = list.get(l);
                if (p.getNameVocab().getVocabulary().equalsIgnoreCase("DIF")) {
                    final Element dataCenter = new Element("Data_Center", DIFWriter.defNS);
                    rootElem.addContent(dataCenter);
                    this.writeDataCenter(p, dataCenter);
                    break;
                }
            }
        }
        final String summary = ds.getDocumentation("summary");
        if (summary != null) {
            final String summaryLines = StringUtil.breakTextAtWords(summary, "\n", 80);
            rootElem.addContent(new Element("Summary", DIFWriter.defNS).addContent(summaryLines));
        }
        URI uri;
        String href;
        if (ds instanceof InvCatalogRef) {
            final InvCatalogRef catref = (InvCatalogRef)ds;
            uri = catref.getURI();
            href = uri.toString();
            final int pos = href.lastIndexOf(46);
            href = href.substring(0, pos) + ".html";
        }
        else {
            final InvCatalogImpl cat = (InvCatalogImpl)ds.getParentCatalog();
            uri = cat.getBaseURI();
            final String catURL = uri.toString();
            final int pos2 = catURL.lastIndexOf(46);
            href = catURL.substring(0, pos2) + ".html";
            if (ds.hasAccess()) {
                href = href + "?dataset=" + ds.getID();
            }
        }
        rootElem.addContent(this.makeRelatedURL("GET DATA", "THREDDS CATALOG", uri.toString()));
        rootElem.addContent(this.makeRelatedURL("GET DATA", "THREDDS DIRECTORY", href));
        final InvAccess access;
        if (null != (access = ds.getAccess(ServiceType.OPENDAP))) {
            rootElem.addContent(this.makeRelatedURL("GET DATA", "OPENDAP DATA", access.getStandardUrlName()));
        }
        rootElem.addContent(new Element("Metadata_Name", DIFWriter.defNS).addContent("CEOS IDN DIF"));
        rootElem.addContent(new Element("Metadata_Version", DIFWriter.defNS).addContent("9.4"));
        final DateType today = new DateType(false, new Date());
        rootElem.addContent(new Element("DIF_Creation_Date", DIFWriter.defNS).addContent(today.toDateString()));
    }
    
    private Element makeRelatedURL(final String type, final String subtype, final String url) {
        final Element elem = new Element("Related_URL", DIFWriter.defNS);
        final Element uctElem = new Element("URL_Content_Type", DIFWriter.defNS);
        elem.addContent(uctElem);
        uctElem.addContent(new Element("Type", DIFWriter.defNS).addContent(type));
        uctElem.addContent(new Element("Subtype", DIFWriter.defNS).addContent(subtype));
        elem.addContent(new Element("URL", DIFWriter.defNS).addContent(url));
        return elem;
    }
    
    private void writeDataCenter(final ThreddsMetadata.Source p, final Element dataCenter) {
        final Element name = new Element("Data_Center_Name", DIFWriter.defNS);
        dataCenter.addContent(name);
        final StringTokenizer stoker = new StringTokenizer(p.getName(), ">");
        final int n = stoker.countTokens();
        if (n == 2) {
            name.addContent(new Element("Short_Name", DIFWriter.defNS).addContent(stoker.nextToken().trim()));
            name.addContent(new Element("Long_Name", DIFWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        else {
            name.addContent(new Element("Short_Name", DIFWriter.defNS).addContent(p.getName()));
        }
        if (p.getUrl() != null && p.getUrl().length() > 0) {
            dataCenter.addContent(new Element("Data_Center_URL", DIFWriter.defNS).addContent(p.getUrl()));
        }
        final Element person = new Element("Personnel", DIFWriter.defNS);
        dataCenter.addContent(person);
        person.addContent(new Element("Role", DIFWriter.defNS).addContent("DATA CENTER CONTACT"));
        person.addContent(new Element("Last_Name", DIFWriter.defNS).addContent("Any"));
        person.addContent(new Element("Email", DIFWriter.defNS).addContent(p.getEmail()));
    }
    
    private void writeVariable(final Element rootElem, final ThreddsMetadata.Variable v) {
        final String vname = v.getVocabularyName();
        final StringTokenizer stoker = new StringTokenizer(vname, ">");
        final int n = stoker.countTokens();
        if (n < 3) {
            return;
        }
        final Element param = new Element("Parameters", DIFWriter.defNS);
        rootElem.addContent(param);
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Category", DIFWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Topic", DIFWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Term", DIFWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Variable", DIFWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Detailed_Variable", DIFWriter.defNS).addContent(stoker.nextToken().trim()));
        }
    }
    
    private static void doCatalog(final InvCatalogFactory fac, final String url) {
        System.out.println("***read " + url);
        try {
            final InvCatalogImpl cat = fac.readXML(url);
            final StringBuilder buff = new StringBuilder();
            final boolean isValid = cat.check(buff, false);
            System.out.println("catalog <" + cat.getName() + "> " + (isValid ? "is" : "is not") + " valid");
            System.out.println(" validation output=\n" + (Object)buff);
            System.out.println(" catalog=\n" + fac.writeXML(cat));
            final DIFWriter w = new DIFWriter();
            final StringBuffer sbuff = new StringBuffer();
            w.writeDatasetEntries(cat, "C:/temp/dif2", sbuff);
            System.out.println(" messages=\n" + (Object)sbuff);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) throws Exception {
        final InvCatalogFactory catFactory = InvCatalogFactory.getDefaultFactory(true);
        doCatalog(catFactory, "http://motherlode.ucar.edu:9080/thredds/idd/models.xml");
    }
    
    static {
        DIFWriter.log = LoggerFactory.getLogger(DIFWriter.class);
        defNS = Namespace.getNamespace("http://gcmd.gsfc.nasa.gov/Aboutus/xml/dif/");
        DIFWriter.schemaLocation = "http://gcmd.gsfc.nasa.gov/Aboutus/xml/dif/dif_v9.4.xsd";
    }
}
