// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.dl;

import java.io.InputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

public class Grib2toDIF implements VocabTranslator
{
    private static Grib2toDIF singleton;
    private HashMap hash;
    private int maxLines;
    private boolean debug;
    
    public static Grib2toDIF getInstance() throws IOException {
        if (Grib2toDIF.singleton == null) {
            Grib2toDIF.singleton = new Grib2toDIF(null);
        }
        return Grib2toDIF.singleton;
    }
    
    public Grib2toDIF(final String dir) throws IOException {
        this.hash = new HashMap();
        this.maxLines = 1000;
        this.debug = false;
        final String resourceName = "/resources/thredds/dl/GRIB2-GCMD.csv";
        InputStream ios;
        if (null != dir) {
            ios = new FileInputStream(dir + resourceName);
        }
        else {
            final Class c = this.getClass();
            ios = c.getResourceAsStream(resourceName);
            if (ios == null) {
                throw new IOException("Cant find resource= " + resourceName);
            }
        }
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        String discipline = null;
        String category = null;
        String param = null;
        final int maxTokens = 8;
        final String[] tokens = new String[maxTokens];
        int count = 0;
        int countDiscipline = -1;
        int countCategory = 0;
        int countParam = 0;
        while (count < this.maxLines) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (this.debug) {
                System.out.println("line=  " + line);
            }
            int countTokens = 0;
            int place = 0;
            while (countTokens < maxTokens) {
                final int pos = line.indexOf(44, place);
                if (pos >= 0) {
                    tokens[countTokens++] = line.substring(place, pos);
                }
                else {
                    tokens[countTokens++] = line.substring(place);
                }
                place = pos + 1;
            }
            if (tokens[0].length() > 0) {
                discipline = tokens[0];
                ++countDiscipline;
                countCategory = -1;
                countParam = -1;
            }
            if (tokens[1].length() > 0) {
                category = tokens[1];
                ++countCategory;
                countParam = -1;
            }
            if (tokens[2].length() > 0) {
                param = tokens[2];
                ++countParam;
            }
            final String difParam = "Earth Science > " + tokens[7];
            if (!difParam.equalsIgnoreCase("n/a")) {
                final String gribId = "2," + countDiscipline + "," + countCategory + "," + countParam;
                this.hash.put(gribId, difParam);
                if (this.debug) {
                    System.out.println(" adding " + discipline + ":" + category + ":" + param + " = " + gribId + " = " + difParam);
                }
            }
            ++count;
        }
        dataIS.close();
    }
    
    public String translate(final String from) {
        return this.hash.get(from);
    }
    
    public static void main(final String[] args) throws IOException {
        new Grib2toDIF("C:/dev/thredds/resourcespath");
    }
}
