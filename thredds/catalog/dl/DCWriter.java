// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.dl;

import thredds.catalog.InvCatalogFactory;
import java.util.StringTokenizer;
import ucar.nc2.units.DateRange;
import ucar.nc2.units.DateType;
import java.util.Date;
import thredds.catalog.InvCatalogImpl;
import thredds.catalog.ThreddsMetadata;
import org.jdom.Content;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import thredds.catalog.XMLEntityResolver;
import org.jdom.Document;
import org.jdom.Element;
import java.io.IOException;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.List;
import thredds.catalog.InvDataset;
import java.io.File;
import thredds.catalog.InvCatalog;
import org.jdom.Namespace;

public class DCWriter
{
    private static final Namespace defNS;
    private static final String schemaLocation;
    private static final String schemaLocationLocal;
    private static String threddsServerURL;
    private InvCatalog cat;
    
    public DCWriter(final InvCatalog cat) {
        this.cat = cat;
    }
    
    public void writeItems(final String fileDir) {
        final File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final List datasets = this.cat.getDatasets();
        for (int i = 0; i < datasets.size(); ++i) {
            final InvDataset elem = datasets.get(i);
            this.doDataset(elem, fileDir);
        }
    }
    
    private void doDataset(final InvDataset ds, final String fileDir) {
        if (ds.isHarvest() && ds.getID() != null) {
            final String fileOutName = fileDir + "/" + ds.getID() + ".dc.xml";
            try {
                final OutputStream out = new BufferedOutputStream(new FileOutputStream(fileOutName));
                this.writeOneItem(ds, System.out);
                this.writeOneItem(ds, out);
                out.close();
                return;
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        final List datasets = ds.getDatasets();
        for (int i = 0; i < datasets.size(); ++i) {
            final InvDataset nested = datasets.get(i);
            this.doDataset(nested, fileDir);
        }
    }
    
    private void writeOneItem(final InvDataset ds, final OutputStream out) throws IOException {
        final Element rootElem = new Element("dc", DCWriter.defNS);
        final Document doc = new Document(rootElem);
        this.writeDataset(ds, rootElem);
        rootElem.addNamespaceDeclaration(XMLEntityResolver.xsiNS);
        rootElem.setAttribute("schemaLocation", DCWriter.defNS.getURI() + " " + DCWriter.schemaLocation, XMLEntityResolver.xsiNS);
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(doc, out);
    }
    
    public void writeDataset(final InvDataset ds, final Element rootElem) {
        rootElem.addContent(new Element("title", DCWriter.defNS).addContent(ds.getName()));
        rootElem.addContent(new Element("Entry_ID", DCWriter.defNS).addContent(ds.getUniqueID()));
        List list = ds.getKeywords();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); ++i) {
                final ThreddsMetadata.Vocab k = list.get(i);
                rootElem.addContent(new Element("Keyword", DCWriter.defNS).addContent(k.getText()));
            }
        }
        final DateRange tm = ds.getTimeCoverage();
        final Element tmElem = new Element("Temporal_Coverage", DCWriter.defNS);
        rootElem.addContent(tmElem);
        tmElem.addContent(new Element("Start_Date", DCWriter.defNS).addContent(tm.getStart().toDateString()));
        tmElem.addContent(new Element("End_Date", DCWriter.defNS).addContent(tm.getEnd().toDateString()));
        final ThreddsMetadata.GeospatialCoverage geo = ds.getGeospatialCoverage();
        final Element geoElem = new Element("Spatial_Coverage", DCWriter.defNS);
        rootElem.addContent(geoElem);
        geoElem.addContent(new Element("Southernmost_Latitude", DCWriter.defNS).addContent(Double.toString(geo.getLatSouth())));
        geoElem.addContent(new Element("Northernmost_Latitude", DCWriter.defNS).addContent(Double.toString(geo.getLatNorth())));
        geoElem.addContent(new Element("Westernmost_Latitude", DCWriter.defNS).addContent(Double.toString(geo.getLonWest())));
        geoElem.addContent(new Element("Easternmost_Latitude", DCWriter.defNS).addContent(Double.toString(geo.getLonEast())));
        rootElem.addContent(new Element("Use_Constraints", DCWriter.defNS).addContent(ds.getDocumentation("rights")));
        list = ds.getPublishers();
        if (list.size() > 0) {
            for (int j = 0; j < list.size(); ++j) {
                final ThreddsMetadata.Source p = list.get(j);
                final Element dataCenter = new Element("Data_Center", DCWriter.defNS);
                rootElem.addContent(dataCenter);
                this.writePublisher(p, dataCenter);
            }
        }
        rootElem.addContent(new Element("Summary", DCWriter.defNS).addContent(ds.getDocumentation("summary")));
        final Element primaryURLelem = new Element("Related_URL", DCWriter.defNS);
        rootElem.addContent(primaryURLelem);
        final String primaryURL = DCWriter.threddsServerURL + "?catalog=" + ((InvCatalogImpl)ds.getParentCatalog()).getBaseURI().toString() + "&dataset=" + ds.getID();
        primaryURLelem.addContent(new Element("URL_Content_Type", DCWriter.defNS).addContent("THREDDS access page"));
        primaryURLelem.addContent(new Element("URL", DCWriter.defNS).addContent(primaryURL));
        final DateType today = new DateType(false, new Date());
        rootElem.addContent(new Element("DIF_Creation_Date", DCWriter.defNS).addContent(today.toDateString()));
    }
    
    protected void writePublisher(final ThreddsMetadata.Source p, final Element dataCenter) {
        final Element name = new Element("Data_Center_Name", DCWriter.defNS);
        dataCenter.addContent(name);
        name.addContent(new Element("Short_Name", DCWriter.defNS).addContent(p.getName()));
        if (p.getUrl() != null && p.getUrl().length() > 0) {
            dataCenter.addContent(new Element("Data_Center_URL", DCWriter.defNS).addContent(p.getUrl()));
        }
        final Element person = new Element("Personnel", DCWriter.defNS);
        dataCenter.addContent(person);
        person.addContent(new Element("Role", DCWriter.defNS).addContent("DATA CENTER CONTACT"));
        person.addContent(new Element("Email", DCWriter.defNS).addContent(p.getEmail()));
    }
    
    private void writeVariable(final Element param, final ThreddsMetadata.Variable v) {
        final String vname = v.getVocabularyName();
        final StringTokenizer stoker = new StringTokenizer(vname, ">");
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Category", DCWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Topic", DCWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Term", DCWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Variable", DCWriter.defNS).addContent(stoker.nextToken().trim()));
        }
        if (stoker.hasMoreTokens()) {
            param.addContent(new Element("Detailed_Variable", DCWriter.defNS).addContent(stoker.nextToken().trim()));
        }
    }
    
    private static void doOne(final InvCatalogFactory fac, final String url) {
        System.out.println("***read " + url);
        try {
            final InvCatalogImpl cat = fac.readXML(url);
            final StringBuilder buff = new StringBuilder();
            final boolean isValid = cat.check(buff, false);
            System.out.println("catalog <" + cat.getName() + "> " + (isValid ? "is" : "is not") + " valid");
            System.out.println(" validation output=\n" + (Object)buff);
            final DCWriter w = new DCWriter(cat);
            w.writeItems("C:/temp/dif");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) throws Exception {
        final InvCatalogFactory catFactory = InvCatalogFactory.getDefaultFactory(true);
        doOne(catFactory, "file:///C:/dev/thredds/catalog/test/data/TestHarvest.xml");
    }
    
    static {
        defNS = Namespace.getNamespace("http://purl.org/dc/elements/1.1/");
        schemaLocation = DCWriter.defNS.getURI() + " http://www.unidata.ucar.edu/schemas/other/dc/dc.xsd";
        schemaLocationLocal = DCWriter.defNS.getURI() + " file://P:/schemas/other/dc/dc.xsd";
        DCWriter.threddsServerURL = "http://localhost:8080/thredds/subset.html";
    }
}
