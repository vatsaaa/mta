// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog.dl;

import thredds.catalog.InvCatalogFactory;
import ucar.nc2.units.TimeDuration;
import ucar.nc2.units.TimeUnit;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.units.DateRange;
import java.net.URI;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.units.DateType;
import java.util.Date;
import thredds.catalog.ThreddsMetadata;
import org.jdom.Content;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import thredds.catalog.XMLEntityResolver;
import org.jdom.Document;
import org.jdom.Element;
import java.util.List;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import ucar.unidata.util.StringUtil;
import ucar.nc2.util.CancelTask;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import thredds.catalog.InvCatalogRef;
import thredds.catalog.InvDataset;
import thredds.catalog.crawl.CatalogCrawler;
import java.io.File;
import thredds.catalog.InvCatalogImpl;
import org.jdom.Namespace;

public class ADNWriter
{
    private static final Namespace defNS;
    private static final String schemaLocation = "http://www.dlese.org/Metadata/adn-item/0.6.50/record.xsd";
    private String fileDir;
    private StringBuffer messBuffer;
    
    public void writeDatasetEntries(final InvCatalogImpl cat, final String fileDir, final StringBuffer mess) {
        this.fileDir = fileDir;
        this.messBuffer = mess;
        final File dir = new File(fileDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        final CatalogCrawler.Listener listener = new CatalogCrawler.Listener() {
            public void getDataset(final InvDataset ds, final Object context) {
                ADNWriter.this.doOneDataset(ds);
            }
            
            public boolean getCatalogRef(final InvCatalogRef dd, final Object context) {
                return true;
            }
        };
        final ByteArrayOutputStream bis = new ByteArrayOutputStream();
        final PrintStream ps = new PrintStream(bis);
        final CatalogCrawler crawler = new CatalogCrawler(0, true, listener);
        crawler.crawl(cat, null, ps, null);
        mess.append("\n*********************\n");
        mess.append(bis.toString());
    }
    
    private void doOneDataset(final InvDataset ds) {
        if (!ds.isHarvest()) {
            this.messBuffer.append(" Dataset " + ds.getName() + " id = " + ds.getID() + " has harvest = false\n");
        }
        else if (this.isDatasetUseable(ds, this.messBuffer)) {
            final String id = StringUtil.replace(ds.getID(), "/", "-");
            final String fileOutName = this.fileDir + "/" + id + ".adn.xml";
            try {
                final OutputStream out = new BufferedOutputStream(new FileOutputStream(fileOutName));
                this.writeOneItem(ds, out);
                out.close();
                this.messBuffer.append(" OK on Write\n");
            }
            catch (IOException ioe) {
                this.messBuffer.append("FAILED on Write " + ioe.getMessage() + "\n");
                ioe.printStackTrace();
            }
        }
    }
    
    public boolean isDatasetUseable(final InvDataset ds, final StringBuffer sbuff) {
        boolean ok = true;
        sbuff.append("Dataset " + ds.getName() + " id = " + ds.getID() + ": ");
        if (ds.getName() == null) {
            ok = false;
            sbuff.append(" missing Name field\n");
        }
        if (ds.getUniqueID() == null) {
            ok = false;
            sbuff.append(" missing ID field\n");
        }
        final List list = ds.getPublishers();
        if (list == null || list.size() == 0) {
            ok = false;
            sbuff.append(" must have publisher element that defines the data center\n");
        }
        final String summary = ds.getDocumentation("summary");
        if (summary == null) {
            ok = false;
            sbuff.append(" must have documentation element of type summary\n");
        }
        final String rights = ds.getDocumentation("rights");
        if (rights == null) {
            ok = false;
            sbuff.append(" must have documentation element of type rights\n");
        }
        sbuff.append(" useable= " + ok + "\n");
        return ok;
    }
    
    private void writeOneItem(final InvDataset ds, final OutputStream out) throws IOException {
        final Element rootElem = new Element("itemRecord", ADNWriter.defNS);
        final Document doc = new Document(rootElem);
        this.writeDataset(ds, rootElem);
        rootElem.addNamespaceDeclaration(XMLEntityResolver.xsiNS);
        rootElem.setAttribute("schemaLocation", ADNWriter.defNS.getURI() + " " + "http://www.dlese.org/Metadata/adn-item/0.6.50/record.xsd", XMLEntityResolver.xsiNS);
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(doc, out);
    }
    
    public void writeDataset(final InvDataset ds, final Element rootElem) {
        final Element generalElem = new Element("general", ADNWriter.defNS);
        rootElem.addContent(generalElem);
        generalElem.addContent(new Element("title", ADNWriter.defNS).addContent("Dataset " + ds.getFullName()));
        generalElem.addContent(new Element("description", ADNWriter.defNS).addContent(ds.getDocumentation("summary")));
        generalElem.addContent(new Element("language", ADNWriter.defNS).addContent("en"));
        final Element subjects = new Element("subjects", ADNWriter.defNS);
        generalElem.addContent(subjects);
        subjects.addContent(new Element("subject", ADNWriter.defNS).addContent("DLESE:Atmospheric science"));
        List list = ds.getKeywords();
        if (list.size() > 0) {
            final Element keywords = new Element("keywords", ADNWriter.defNS);
            generalElem.addContent(keywords);
            for (int i = 0; i < list.size(); ++i) {
                final ThreddsMetadata.Vocab k = list.get(i);
                keywords.addContent(new Element("keyword", ADNWriter.defNS).addContent(k.getText()));
            }
        }
        final Element lifeElem = new Element("lifecycle", ADNWriter.defNS);
        rootElem.addContent(lifeElem);
        if (ds.getPublishers().size() > 0 || ds.getCreators().size() > 0) {
            final Element contributors = new Element("contributors", ADNWriter.defNS);
            lifeElem.addContent(contributors);
            list = ds.getPublishers();
            for (int j = 0; j < list.size(); ++j) {
                final ThreddsMetadata.Source p = list.get(j);
                if (p.getNameVocab().getVocabulary().equalsIgnoreCase("ADN")) {
                    contributors.addContent(this.writeSource(p, "Publisher"));
                    break;
                }
            }
            list = ds.getCreators();
            for (int j = 0; j < list.size(); ++j) {
                final ThreddsMetadata.Source p = list.get(j);
                if (p.getNameVocab().getVocabulary().equalsIgnoreCase("ADN")) {
                    contributors.addContent(this.writeSource(p, "Author"));
                    break;
                }
            }
        }
        final Element metaElem = new Element("metaMetadata", ADNWriter.defNS);
        rootElem.addContent(metaElem);
        final Element entries = new Element("catalogEntries", ADNWriter.defNS);
        metaElem.addContent(entries);
        final Element entry = new Element("catalog", ADNWriter.defNS);
        entries.addContent(entry);
        final String id = StringUtil.allow(ds.getUniqueID(), ".", '-');
        entry.setAttribute("entry", id);
        entry.addContent("THREDDS-motherlode");
        final DateType today = new DateType(false, new Date());
        final Element dateInfo = new Element("dateInfo", ADNWriter.defNS);
        metaElem.addContent(dateInfo);
        dateInfo.setAttribute("created", today.toDateString());
        dateInfo.setAttribute("accessioned", today.toDateString());
        final Element status = new Element("statusOf", ADNWriter.defNS);
        metaElem.addContent(status);
        status.setAttribute("status", "Accessioned");
        metaElem.addContent(new Element("language", ADNWriter.defNS).addContent("en"));
        metaElem.addContent(new Element("scheme", ADNWriter.defNS).addContent("ADN (ADEPT/DLESE/NASA Alexandria Digital Earth Prototype/Digital Library for Earth System Education/National Aeronautics and Space Administration)"));
        metaElem.addContent(new Element("copyright", ADNWriter.defNS).addContent("Copyright (c) 2002 UCAR (University Corporation for Atmospheric Research)"));
        final Element terms = new Element("termsOfUse", ADNWriter.defNS);
        metaElem.addContent(terms);
        terms.addContent("Terms of use consistent with DLESE (Digital Library for Earth System Education) policy");
        terms.setAttribute("URI", "http://www.dlese.org/documents/policy/terms_use_full.html");
        final Element technical = new Element("technical", ADNWriter.defNS);
        rootElem.addContent(technical);
        final Element online = new Element("online", ADNWriter.defNS);
        technical.addContent(online);
        final Element primaryURLelem = new Element("primaryURL", ADNWriter.defNS);
        online.addContent(primaryURLelem);
        String href;
        if (ds instanceof InvCatalogRef) {
            final InvCatalogRef catref = (InvCatalogRef)ds;
            final URI uri = catref.getURI();
            href = uri.toString();
            final int pos = href.lastIndexOf(46);
            href = href.substring(0, pos) + ".html";
        }
        else {
            final InvCatalogImpl cat = (InvCatalogImpl)ds.getParentCatalog();
            final String catURL = cat.getBaseURI().toString();
            final int pos = catURL.lastIndexOf(46);
            final String catURLh = catURL.substring(0, pos) + ".html";
            href = catURLh + "?dataset=" + ds.getID();
        }
        primaryURLelem.addContent(href);
        final Element mediums = new Element("mediums", ADNWriter.defNS);
        online.addContent(mediums);
        mediums.addContent(new Element("medium", ADNWriter.defNS).addContent("text/html"));
        final Element reqs = new Element("requirements", ADNWriter.defNS);
        online.addContent(reqs);
        final Element req = new Element("requirement", ADNWriter.defNS);
        reqs.addContent(req);
        req.addContent(new Element("reqType", ADNWriter.defNS).addContent("DLESE:Other:More specific technical requirements"));
        final Element oreqs = new Element("otherRequirements", ADNWriter.defNS);
        online.addContent(oreqs);
        final Element oreq = new Element("otherRequirement", ADNWriter.defNS);
        oreqs.addContent(oreq);
        oreq.addContent(new Element("otherType", ADNWriter.defNS).addContent("Requires data viewer tool. See the documentation page in the resource."));
        final Element educational = new Element("educational", ADNWriter.defNS);
        rootElem.addContent(educational);
        final Element audiences = new Element("audiences", ADNWriter.defNS);
        educational.addContent(audiences);
        final Element audience = new Element("audience", ADNWriter.defNS);
        audiences.addContent(audience);
        audience.addContent(new Element("gradeRange", ADNWriter.defNS).addContent("DLESE:Graduate or professional"));
        final Element resourceTypes = new Element("resourceTypes", ADNWriter.defNS);
        educational.addContent(resourceTypes);
        String resourceType = "DLESE:Data:In situ dataset";
        if (ds.getDataType() == FeatureType.GRID) {
            resourceType = "DLESE:Data:Modeled dataset";
        }
        else if (ds.getDataType() == FeatureType.IMAGE) {
            resourceType = "DLESE:Data:Remotely sensed dataset";
        }
        resourceTypes.addContent(new Element("resourceType", ADNWriter.defNS).addContent(resourceType));
        final Element rights = new Element("rights", ADNWriter.defNS);
        rootElem.addContent(rights);
        rights.addContent(new Element("cost", ADNWriter.defNS).addContent("DLESE:No"));
        rights.addContent(new Element("description", ADNWriter.defNS).addContent(ds.getDocumentation("rights")));
        final ThreddsMetadata.GeospatialCoverage gc = ds.getGeospatialCoverage();
        if (null != gc) {
            rootElem.addContent(this.writeGeospatialCoverage(gc));
        }
        final DateRange dateRange = ds.getTimeCoverage();
        if (null != dateRange) {
            rootElem.addContent(this.writeTemporalCoverage(dateRange));
        }
    }
    
    protected boolean emailOK(final ThreddsMetadata.Source p) {
        final String email = p.getEmail();
        return email.indexOf(64) >= 0;
    }
    
    protected Element writeSource(final ThreddsMetadata.Source p, final String role) {
        final Element contributor = new Element("contributor", ADNWriter.defNS);
        contributor.setAttribute("role", role);
        final Element organization = new Element("organization", ADNWriter.defNS);
        contributor.addContent(organization);
        final String name = p.getNameVocab().getText();
        final int pos = name.indexOf("/");
        if (pos > 0) {
            organization.addContent(new Element("instName", ADNWriter.defNS).addContent(name.substring(0, pos)));
            organization.addContent(new Element("instDept", ADNWriter.defNS).addContent(name.substring(pos + 1)));
        }
        else {
            organization.addContent(new Element("instName", ADNWriter.defNS).addContent(name));
        }
        if (p.getUrl() != null && p.getUrl().length() > 0) {
            organization.addContent(new Element("instUrl", ADNWriter.defNS).addContent(p.getUrl()));
        }
        if (this.emailOK(p)) {
            organization.addContent(new Element("instEmail", ADNWriter.defNS).addContent(p.getEmail()));
        }
        return contributor;
    }
    
    protected Element writeGeospatialCoverage(final ThreddsMetadata.GeospatialCoverage gc) {
        final Element geos = new Element("geospatialCoverages", ADNWriter.defNS);
        final Element geo = new Element("geospatialCoverage", ADNWriter.defNS);
        geos.addContent(geo);
        final Element body = new Element("body", ADNWriter.defNS);
        geo.addContent(body);
        body.addContent(new Element("planet", ADNWriter.defNS).addContent("Earth"));
        geo.addContent(new Element("geodeticDatumGlobalOrHorz", ADNWriter.defNS).addContent("DLESE:WGS84"));
        geo.addContent(new Element("projection", ADNWriter.defNS).setAttribute("type", "DLESE:Unknown"));
        geo.addContent(new Element("coordinateSystem", ADNWriter.defNS).setAttribute("type", "DLESE:Geographic latitude and longitude"));
        final Element bb = new Element("boundBox", ADNWriter.defNS);
        geo.addContent(bb);
        final double west = LatLonPointImpl.lonNormal(gc.getLonWest());
        final double east = LatLonPointImpl.lonNormal(gc.getLonEast());
        bb.addContent(new Element("westCoord", ADNWriter.defNS).setText(ucar.unidata.util.Format.dfrac(west, 2)));
        bb.addContent(new Element("eastCoord", ADNWriter.defNS).setText(ucar.unidata.util.Format.dfrac(east, 2)));
        bb.addContent(new Element("northCoord", ADNWriter.defNS).setText(ucar.unidata.util.Format.dfrac(gc.getLatNorth(), 2)));
        bb.addContent(new Element("southCoord", ADNWriter.defNS).setText(ucar.unidata.util.Format.dfrac(gc.getLatSouth(), 2)));
        bb.addContent(new Element("bbSrcName", ADNWriter.defNS).setText("Calculated from dataset coordinate system by CDM/THREDDS"));
        return geos;
    }
    
    protected Element writeTemporalCoverage(final DateRange dateRange) {
        final Element tc = new Element("temporalCoverages", ADNWriter.defNS);
        final Element tp = new Element("timeAndPeriod", ADNWriter.defNS);
        final Element ti = new Element("timeInfo", ADNWriter.defNS);
        tc.addContent(tp.addContent(ti));
        final DateType startDate = dateRange.getStart();
        final DateType endDate = dateRange.getEnd();
        Element time;
        if (endDate.isPresent()) {
            final String units = "Days ago";
            final TimeDuration duration = dateRange.getDuration();
            double value = -0.0;
            try {
                final TimeUnit tdayUnit = new TimeUnit("days");
                value = duration.getValue(tdayUnit);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            time = new Element("timeRelative", ADNWriter.defNS);
            final Element begin = new Element("begin", ADNWriter.defNS);
            begin.setAttribute("units", units);
            begin.addContent(Double.toString(value));
            time.addContent(begin);
            final Element end = new Element("end", ADNWriter.defNS);
            end.setAttribute("units", units);
            end.addContent("0");
            time.addContent(end);
        }
        else {
            time = new Element("timeAD", ADNWriter.defNS);
            final Element begin2 = new Element("begin", ADNWriter.defNS);
            begin2.setAttribute("date", startDate.toDateString());
            time.addContent(begin2);
            final Element end2 = new Element("end", ADNWriter.defNS);
            end2.setAttribute("date", endDate.toDateString());
            time.addContent(end2);
        }
        ti.addContent(time);
        return tc;
    }
    
    private static void doOne(final InvCatalogFactory fac, final String url) {
        System.out.println("***read " + url);
        try {
            final InvCatalogImpl cat = fac.readXML(url);
            final StringBuilder buff = new StringBuilder();
            final boolean isValid = cat.check(buff, false);
            System.out.println("catalog <" + cat.getName() + "> " + (isValid ? "is" : "is not") + " valid");
            System.out.println(" validation output=\n" + (Object)buff);
            final ADNWriter w = new ADNWriter();
            final StringBuffer sbuff = new StringBuffer();
            w.writeDatasetEntries(cat, "C:/temp/adn3", sbuff);
            System.out.println(" messages=\n" + (Object)sbuff);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) throws Exception {
        final InvCatalogFactory catFactory = InvCatalogFactory.getDefaultFactory(true);
        doOne(catFactory, "http://motherlode.ucar.edu:9080/thredds/idd/models.xml");
    }
    
    static {
        defNS = Namespace.getNamespace("http://adn.dlese.org");
    }
}
