// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import ucar.nc2.units.DateType;
import java.util.List;
import java.util.Map;
import java.net.URI;

public abstract class InvCatalog
{
    protected String name;
    protected String version;
    protected URI baseURI;
    protected InvDatasetImpl topDataset;
    protected Map<String, InvDataset> dsHash;
    protected Map<String, InvService> serviceHash;
    protected List<InvService> services;
    protected List<InvProperty> properties;
    protected List<InvDataset> datasets;
    protected DateType expires;
    
    protected InvCatalog() {
        this.baseURI = null;
        this.dsHash = new HashMap<String, InvDataset>();
        this.serviceHash = new HashMap<String, InvService>();
        this.services = new ArrayList<InvService>();
        this.properties = new ArrayList<InvProperty>();
        this.datasets = new ArrayList<InvDataset>();
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    @Deprecated
    public InvDataset getDataset() {
        return this.topDataset;
    }
    
    public InvDataset findDatasetByID(final String id) {
        return this.dsHash.get(id);
    }
    
    public List<InvDataset> getDatasets() {
        return this.datasets;
    }
    
    public List<InvService> getServices() {
        return this.services;
    }
    
    public List<InvProperty> getProperties() {
        return this.properties;
    }
    
    public String findProperty(final String name) {
        InvProperty result = null;
        for (final InvProperty p : this.getProperties()) {
            if (p.getName().equals(name)) {
                result = p;
            }
        }
        return (result == null) ? null : result.getValue();
    }
    
    public DateType getExpires() {
        return this.expires;
    }
    
    public InvService findService(final String name) {
        if (name == null) {
            return null;
        }
        for (final InvService s : this.services) {
            if (name.equals(s.getName())) {
                return s;
            }
            for (final InvService nested : s.getServices()) {
                if (name.equals(nested.getName())) {
                    return nested;
                }
            }
        }
        return null;
    }
    
    public URI resolveUri(final String uriString) throws URISyntaxException {
        final URI want = new URI(uriString);
        if (this.baseURI == null || want.isAbsolute()) {
            return want;
        }
        final String scheme = this.baseURI.getScheme();
        if (scheme != null && scheme.equals("file")) {
            final String baseString = this.baseURI.toString();
            if (uriString.length() > 0 && uriString.charAt(0) == '#') {
                return new URI(baseString + uriString);
            }
            final int pos = baseString.lastIndexOf(47);
            if (pos > 0) {
                final String r = baseString.substring(0, pos + 1) + uriString;
                return new URI(r);
            }
        }
        return this.baseURI.resolve(want);
    }
    
    public String getUriString() {
        return this.baseURI.toString();
    }
    
    public abstract boolean check(final StringBuilder p0, final boolean p1);
    
    public boolean check(final StringBuilder out) {
        return this.check(out, false);
    }
    
    public abstract void subset(final InvDataset p0);
    
    public abstract void filter(final DatasetFilter p0);
}
