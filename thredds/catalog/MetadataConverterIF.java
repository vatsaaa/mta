// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.io.IOException;
import java.net.URI;
import org.jdom.Element;

public interface MetadataConverterIF
{
    Object readMetadataContent(final InvDataset p0, final Element p1);
    
    Object readMetadataContentFromURL(final InvDataset p0, final URI p1) throws IOException;
    
    void addMetadataContent(final Element p0, final Object p1);
    
    boolean validateMetadataContent(final Object p0, final StringBuilder p1);
}
