// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import ucar.nc2.units.DateRange;
import ucar.nc2.units.DateType;
import ucar.nc2.constants.FeatureType;
import java.util.List;

public class InvDatasetImplProxy extends InvDatasetImpl
{
    private InvDatasetImpl proxy;
    private String aliasName;
    
    public InvDatasetImplProxy(final String aliasName, final InvDatasetImpl proxy) {
        super(proxy.getParent(), proxy.getName());
        this.aliasName = aliasName;
        this.proxy = proxy;
    }
    
    public String getAliasName() {
        return this.aliasName;
    }
    
    @Override
    public void addAccess(final InvAccess p0) {
        this.proxy.addAccess(p0);
    }
    
    @Override
    public void addDataset(final InvDatasetImpl p0) {
        this.proxy.addDataset(p0);
    }
    
    @Override
    public void addDocumentation(final InvDocumentation p0) {
        this.proxy.addDocumentation(p0);
    }
    
    @Override
    public void addProperty(final InvProperty p0) {
        this.proxy.addProperty(p0);
    }
    
    @Override
    public void addService(final InvService p0) {
        this.proxy.addService(p0);
    }
    
    @Override
    boolean check(final StringBuilder p0, final boolean p1) {
        return this.proxy.check(p0, p1);
    }
    
    @Override
    public String dump() {
        return this.proxy.dump();
    }
    
    @Override
    String dump(final int p0) {
        return this.proxy.dump(p0);
    }
    
    @Override
    public boolean equals(final Object p0) {
        return this.proxy.equals(p0);
    }
    
    @Override
    public InvDatasetImpl findDatasetByName(final String p0) {
        return this.proxy.findDatasetByName(p0);
    }
    
    @Override
    public String findProperty(final String p0) {
        return this.proxy.findProperty(p0);
    }
    
    @Override
    public InvService findService(final String p0) {
        return this.proxy.findService(p0);
    }
    
    @Override
    public boolean finish() {
        return true;
    }
    
    @Override
    public InvAccess getAccess(final ServiceType p0) {
        return this.proxy.getAccess(p0);
    }
    
    @Override
    public List<InvAccess> getAccess() {
        return this.proxy.getAccess();
    }
    
    @Override
    public List<InvAccess> getAccessLocal() {
        return this.proxy.getAccessLocal();
    }
    
    @Override
    public String getAlias() {
        return this.proxy.getAlias();
    }
    
    @Override
    public String getAuthority() {
        return this.proxy.getAuthority();
    }
    
    @Override
    public CollectionType getCollectionType() {
        return this.proxy.getCollectionType();
    }
    
    @Override
    public List<ThreddsMetadata.Contributor> getContributors() {
        return this.proxy.getContributors();
    }
    
    @Override
    public List<ThreddsMetadata.Source> getCreators() {
        return this.proxy.getCreators();
    }
    
    @Override
    public DataFormatType getDataFormatType() {
        return this.proxy.getDataFormatType();
    }
    
    @Override
    public FeatureType getDataType() {
        return this.proxy.getDataType();
    }
    
    @Override
    public List<InvDataset> getDatasets() {
        return this.proxy.getDatasets();
    }
    
    @Override
    public List<DateType> getDates() {
        return this.proxy.getDates();
    }
    
    @Override
    public List<InvDocumentation> getDocumentation() {
        return this.proxy.getDocumentation();
    }
    
    @Override
    public String getDocumentation(final String p0) {
        return this.proxy.getDocumentation(p0);
    }
    
    @Override
    public String getFullName() {
        return this.proxy.getFullName();
    }
    
    @Override
    public ThreddsMetadata.GeospatialCoverage getGeospatialCoverage() {
        return this.proxy.getGeospatialCoverage();
    }
    
    @Override
    public String getID() {
        return this.proxy.getID();
    }
    
    @Override
    public List<ThreddsMetadata.Vocab> getKeywords() {
        return this.proxy.getKeywords();
    }
    
    @Override
    public ThreddsMetadata getLocalMetadata() {
        return this.proxy.getLocalMetadata();
    }
    
    @Override
    protected boolean getMark() {
        return this.proxy.getMark();
    }
    
    @Override
    public List<InvMetadata> getMetadata(final MetadataType p0) {
        return this.proxy.getMetadata(p0);
    }
    
    @Override
    public List<InvMetadata> getMetadata() {
        return this.proxy.getMetadata();
    }
    
    @Override
    public String getName() {
        return this.proxy.getName();
    }
    
    @Override
    public InvDataset getParent() {
        return this.proxy.getParent();
    }
    
    @Override
    public InvCatalog getParentCatalog() {
        return this.proxy.getParentCatalog();
    }
    
    @Override
    public List<ThreddsMetadata.Vocab> getProjects() {
        return this.proxy.getProjects();
    }
    
    @Override
    public List<InvProperty> getProperties() {
        return this.proxy.getProperties();
    }
    
    @Override
    public List<ThreddsMetadata.Source> getPublishers() {
        return this.proxy.getPublishers();
    }
    
    @Override
    public InvService getServiceDefault() {
        return this.proxy.getServiceDefault();
    }
    
    @Override
    public List<InvService> getServicesLocal() {
        return this.proxy.getServicesLocal();
    }
    
    @Override
    public DateRange getTimeCoverage() {
        return this.proxy.getTimeCoverage();
    }
    
    @Override
    public String getUniqueID() {
        return this.proxy.getUniqueID();
    }
    
    @Override
    public String getUrlPath() {
        return this.proxy.getUrlPath();
    }
    
    @Override
    public Object getUserProperty(final Object p0) {
        return this.proxy.getUserProperty(p0);
    }
    
    @Override
    public List<ThreddsMetadata.Variables> getVariables() {
        return this.proxy.getVariables();
    }
    
    @Override
    public boolean hasAccess() {
        return this.proxy.hasAccess();
    }
    
    @Override
    public boolean hasNestedDatasets() {
        return this.proxy.hasNestedDatasets();
    }
    
    @Override
    public int hashCode() {
        return this.proxy.hashCode();
    }
    
    @Override
    public boolean isHarvest() {
        return this.proxy.isHarvest();
    }
    
    @Override
    public boolean removeDataset(final InvDatasetImpl p0) {
        return this.proxy.removeDataset(p0);
    }
    
    @Override
    public void removeService(final InvService p0) {
        this.proxy.removeService(p0);
    }
    
    @Override
    public void setAlias(final String p0) {
        this.proxy.setAlias(p0);
    }
    
    @Override
    public void setAuthority(final String p0) {
        this.proxy.setAuthority(p0);
    }
    
    @Override
    public void setCatalog(final InvCatalog p0) {
        this.proxy.setCatalog(p0);
    }
    
    @Override
    public void setCollectionType(final CollectionType p0) {
        this.proxy.setCollectionType(p0);
    }
    
    @Override
    public void setContributors(final List<ThreddsMetadata.Contributor> p0) {
        this.proxy.setContributors(p0);
    }
    
    @Override
    public void setGeospatialCoverage(final ThreddsMetadata.GeospatialCoverage p0) {
        this.proxy.setGeospatialCoverage(p0);
    }
    
    @Override
    public void setHarvest(final boolean p0) {
        this.proxy.setHarvest(p0);
    }
    
    @Override
    public void setID(final String p0) {
        this.proxy.setID(p0);
    }
    
    @Override
    public void setKeywords(final List<ThreddsMetadata.Vocab> p0) {
        this.proxy.setKeywords(p0);
    }
    
    @Override
    public void setLocalMetadata(final ThreddsMetadata p0) {
        this.proxy.setLocalMetadata(p0);
    }
    
    @Override
    protected void setMark(final boolean p0) {
        this.proxy.setMark(p0);
    }
    
    @Override
    public void setName(final String p0) {
        this.proxy.setName(p0);
    }
    
    @Override
    public void setParent(final InvDatasetImpl p0) {
        this.proxy.setParent(p0);
    }
    
    @Override
    public void setProjects(final List<ThreddsMetadata.Vocab> p0) {
        this.proxy.setProjects(p0);
    }
    
    @Override
    public void setPublishers(final List<ThreddsMetadata.Source> p0) {
        this.proxy.setPublishers(p0);
    }
    
    @Override
    public void setServicesLocal(final List<InvService> p0) {
        this.proxy.setServicesLocal(p0);
    }
    
    @Override
    public void setTimeCoverage(final DateRange p0) {
        this.proxy.setTimeCoverage(p0);
    }
    
    @Override
    public void setUrlPath(final String p0) {
        this.proxy.setUrlPath(p0);
    }
    
    @Override
    public void setUserProperty(final Object p0, final Object p1) {
        this.proxy.setUserProperty(p0, p1);
    }
    
    @Override
    public String toString() {
        return this.proxy.toString();
    }
}
