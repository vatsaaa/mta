// 
// Decompiled by Procyon v0.5.36
// 

package thredds.catalog;

import java.io.IOException;
import java.io.OutputStream;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import ucar.nc2.units.DateType;
import java.net.URI;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import javax.swing.event.EventListenerList;
import java.util.List;

public class InvCatalogImpl extends InvCatalog
{
    private String createFrom;
    private List<DataRootConfig> roots;
    private StringBuilder log;
    private boolean hasError;
    private DatasetFilter filter;
    private boolean debugFilter;
    private EventListenerList listenerList;
    private InvCatalogImpl top;
    private InvCatalogFactory factory;
    private InvCatalogConvertIF converter;
    private volatile int hashCode;
    
    @Override
    @Deprecated
    public void subset(final InvDataset ds) {
        final InvDatasetImpl dataset = (InvDatasetImpl)ds;
        dataset.transferMetadata(dataset, true);
        this.topDataset = dataset;
        this.datasets.clear();
        this.datasets.add(this.topDataset);
        dataset.dataType = dataset.getDataType();
        dataset.setCatalog(this);
        dataset.parent = null;
        final List<InvService> services = new ArrayList<InvService>(dataset.getServicesLocal());
        this.findServices(services, dataset);
        dataset.setServicesLocal(services);
        this.finish();
    }
    
    void findServices(final List<InvService> result, final InvDataset ds) {
        if (ds instanceof InvCatalogRef) {
            return;
        }
        for (final InvAccess a : ds.getAccess()) {
            final InvService s = a.getService();
            final InvDataset d = a.getDataset();
            if (null == d.findService(s.getName()) && !result.contains(s)) {
                result.add(s);
            }
        }
        for (final InvDataset nested : ds.getDatasets()) {
            this.findServices(result, nested);
        }
    }
    
    @Override
    public void filter(final DatasetFilter filter) {
        this.mark(filter, this.topDataset);
        this.delete(this.topDataset);
        this.filter = filter;
    }
    
    protected DatasetFilter getDatasetFilter() {
        return this.filter;
    }
    
    private boolean mark(final DatasetFilter filter, final InvDatasetImpl ds) {
        if (ds instanceof InvCatalogRef) {
            final InvCatalogRef catRef = (InvCatalogRef)ds;
            if (!catRef.isRead()) {
                return false;
            }
        }
        boolean allMarked = true;
        for (final InvDataset nested : ds.getDatasets()) {
            allMarked &= this.mark(filter, (InvDatasetImpl)nested);
        }
        if (!allMarked) {
            return false;
        }
        if (filter.accept(ds) >= 0) {
            return false;
        }
        ds.setMark(true);
        if (this.debugFilter) {
            System.out.println(" mark " + ds.getName());
        }
        return true;
    }
    
    private void delete(final InvDatasetImpl ds) {
        if (ds instanceof InvCatalogRef) {
            final InvCatalogRef catRef = (InvCatalogRef)ds;
            if (!catRef.isRead()) {
                return;
            }
        }
        final Iterator iter = ds.getDatasets().iterator();
        while (iter.hasNext()) {
            final InvDatasetImpl nested = iter.next();
            if (nested.getMark()) {
                iter.remove();
                if (!this.debugFilter) {
                    continue;
                }
                System.out.println(" remove " + nested.getName());
            }
            else {
                this.delete(nested);
            }
        }
    }
    
    public InvCatalogImpl(final String name, final String version, final URI baseURI) {
        this(name, version, null, baseURI);
    }
    
    public InvCatalogImpl(final String name, final String version, final DateType expires, final URI baseURI) {
        this.roots = new ArrayList<DataRootConfig>();
        this.log = new StringBuilder();
        this.hasError = false;
        this.filter = null;
        this.debugFilter = false;
        this.listenerList = null;
        this.top = null;
        this.factory = null;
        this.converter = null;
        this.hashCode = 0;
        this.name = name;
        this.version = version;
        this.expires = expires;
        this.baseURI = baseURI;
    }
    
    public boolean finish() {
        if (this.datasets.size() == 1) {
            this.topDataset = this.datasets.get(0);
        }
        else {
            this.topDataset = new InvDatasetImpl(null, (this.name == null) ? "Top Dataset" : this.name);
            for (final InvDataset dataset : this.datasets) {
                this.topDataset.addDataset((InvDatasetImpl)dataset);
            }
            this.topDataset.setServicesLocal(this.services);
        }
        this.topDataset.setCatalog(this);
        this.dsHash = new HashMap<String, InvDataset>();
        this.addDatasetIds(this.topDataset);
        return this.topDataset.finish();
    }
    
    private void addDatasetIds(final InvDatasetImpl ds) {
        this.addDatasetByID(ds);
        if (ds instanceof InvCatalogRef) {
            return;
        }
        for (final InvDataset invDataset : ds.getDatasets()) {
            final InvDatasetImpl nested = (InvDatasetImpl)invDataset;
            this.addDatasetIds(nested);
        }
    }
    
    public void addDatasetByID(final InvDatasetImpl ds) {
        if (ds.getID() != null) {
            this.dsHash.put(ds.getID(), ds);
        }
    }
    
    public void removeDatasetByID(final InvDatasetImpl ds) {
        if (ds.getID() != null) {
            this.dsHash.remove(ds.getID());
        }
    }
    
    public void addDataset(final InvDatasetImpl ds) {
        if (ds != null) {
            this.datasets.add(ds);
        }
    }
    
    public boolean removeDataset(final InvDatasetImpl ds) {
        if (this.datasets.remove(ds)) {
            ds.setParent(null);
            this.removeDatasetByID(ds);
            return true;
        }
        return false;
    }
    
    public boolean replaceDataset(final InvDatasetImpl remove, final InvDatasetImpl add) {
        if (this.topDataset.equals(remove)) {
            (this.topDataset = add).setCatalog(this);
        }
        for (int i = 0; i < this.datasets.size(); ++i) {
            final InvDataset dataset = this.datasets.get(i);
            if (dataset.equals(remove)) {
                this.datasets.set(i, add);
                this.removeDatasetByID(remove);
                this.addDatasetByID(add);
                return true;
            }
        }
        return false;
    }
    
    public void addProperty(final InvProperty p) {
        this.properties.add(p);
    }
    
    public void addService(final InvService s) {
        if (s == null) {
            throw new IllegalArgumentException("Service to add was null.");
        }
        if (s.getName() == null) {
            return;
        }
        final Object obj = this.serviceHash.get(s.getName());
        if (obj == null) {
            this.serviceHash.put(s.getName(), s);
            this.services.add(s);
            return;
        }
        if (s.equals(obj)) {
            return;
        }
        this.log.append("Multiple Services with the same name\n");
    }
    
    @Deprecated
    public void setDataset(final InvDatasetImpl ds) {
        this.addDataset(this.topDataset = ds);
    }
    
    public String getCreateFrom() {
        return this.createFrom;
    }
    
    public void setCreateFrom(final String createFrom) {
        this.createFrom = createFrom;
    }
    
    public void setBaseURI(final URI baseURI) {
        this.baseURI = baseURI;
    }
    
    public URI getBaseURI() {
        return this.baseURI;
    }
    
    public void setExpires(final DateType expiresDate) {
        this.expires = expiresDate;
    }
    
    public boolean hasFatalError() {
        return this.hasError;
    }
    
    public void appendErrorMessage(final String message, final boolean isInvalid) {
        this.log.append(message);
        this.hasError |= isInvalid;
    }
    
    @Override
    public boolean check(final StringBuilder out, final boolean show) {
        final boolean isValid = !this.hasError;
        out.append("----Catalog Validation version 1.0.01\n");
        if (this.log.length() > 0) {
            out.append((CharSequence)this.log);
        }
        if (show) {
            System.out.println(" catalog valid = " + isValid);
        }
        for (final InvDataset ds : this.datasets) {
            final InvDatasetImpl dsi = (InvDatasetImpl)ds;
            dsi.check(out, show);
        }
        return isValid;
    }
    
    public String getLog() {
        return this.log.toString();
    }
    
    public String dump() {
        final StringBuilder buff = new StringBuilder(1000);
        buff.setLength(0);
        buff.append("Catalog <").append(this.getName()).append("> <").append(this.getVersion()).append("> <").append(this.getCreateFrom()).append(">\n");
        buff.append(this.topDataset.dump(2));
        return buff.toString();
    }
    
    public void addPropertyChangeListener(final PropertyChangeListener l) {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        this.listenerList.add(PropertyChangeListener.class, l);
    }
    
    public void removePropertyChangeListener(final PropertyChangeListener l) {
        this.listenerList.remove(PropertyChangeListener.class, l);
    }
    
    void firePropertyChangeEvent(final PropertyChangeEvent event) {
        if (this.listenerList == null) {
            return;
        }
        final Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == PropertyChangeListener.class) {
                ((PropertyChangeListener)listeners[i + 1]).propertyChange(event);
            }
        }
    }
    
    InvCatalogImpl getTopCatalog() {
        return (this.top == null) ? this : this.top;
    }
    
    void setTopCatalog(final InvCatalogImpl top) {
        this.top = top;
    }
    
    InvCatalogFactory getCatalogFactory() {
        return this.factory;
    }
    
    void setCatalogFactory(final InvCatalogFactory factory) {
        this.factory = factory;
    }
    
    InvCatalogConvertIF getCatalogConverter() {
        return this.converter;
    }
    
    void setCatalogConverter(final InvCatalogConvertIF converter) {
        this.converter = converter;
    }
    
    public void setCatalogConverterToVersion1() {
        this.setCatalogConverter(this.factory.getCatalogConverter("http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0"));
    }
    
    public List<DataRootConfig> getDatasetRoots() {
        return this.roots;
    }
    
    public void addDatasetRoot(final DataRootConfig root) {
        this.roots.add(root);
    }
    
    public void writeXML(final OutputStream os) throws IOException {
        final InvCatalogConvertIF fac = this.getCatalogConverter();
        fac.writeXML(this, os);
    }
    
    public void writeXML(final OutputStream os, final boolean raw) throws IOException {
        final InvCatalogConvertIF fac = this.getCatalogConverter();
        fac.writeXML(this, os, raw);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof InvCatalogImpl && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (null != this.getName()) {
                result = 37 * result + this.getName().hashCode();
            }
            result = 37 * result + this.getServices().hashCode();
            result = 37 * result + this.getDatasets().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
