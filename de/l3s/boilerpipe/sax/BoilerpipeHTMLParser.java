// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import de.l3s.boilerpipe.document.TextDocument;
import org.xml.sax.ContentHandler;
import org.apache.xerces.xni.parser.XMLParserConfiguration;
import org.cyberneko.html.HTMLConfiguration;
import org.apache.xerces.parsers.AbstractSAXParser;

public class BoilerpipeHTMLParser extends AbstractSAXParser
{
    private final BoilerpipeHTMLContentHandler contentHandler;
    
    public BoilerpipeHTMLParser() {
        this(new BoilerpipeHTMLContentHandler());
    }
    
    public BoilerpipeHTMLParser(final BoilerpipeHTMLContentHandler contentHandler) {
        super((XMLParserConfiguration)new HTMLConfiguration());
        this.setContentHandler((ContentHandler)(this.contentHandler = contentHandler));
    }
    
    public TextDocument toTextDocument() {
        return this.contentHandler.toTextDocument();
    }
}
