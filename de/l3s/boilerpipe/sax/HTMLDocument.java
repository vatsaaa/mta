// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.xml.sax.InputSource;
import java.nio.charset.Charset;

public class HTMLDocument implements InputSourceable
{
    private final Charset charset;
    private final byte[] data;
    
    public HTMLDocument(final byte[] data, final Charset charset) {
        this.data = data;
        this.charset = charset;
    }
    
    public Charset getCharset() {
        return this.charset;
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    public InputSource toInputSource() {
        final InputSource is = new InputSource(new ByteArrayInputStream(this.data));
        is.setEncoding(this.charset.name());
        return is;
    }
}
