// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import org.xml.sax.InputSource;

public interface InputSourceable
{
    InputSource toInputSource();
}
