// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

public class DefaultTagActionMap extends TagActionMap
{
    private static final long serialVersionUID = 1L;
    public static final TagActionMap INSTANCE;
    
    protected DefaultTagActionMap() {
        this.setTagAction("STYLE", CommonTagActions.TA_IGNORABLE_ELEMENT);
        this.setTagAction("SCRIPT", CommonTagActions.TA_IGNORABLE_ELEMENT);
        this.setTagAction("OPTION", CommonTagActions.TA_IGNORABLE_ELEMENT);
        this.setTagAction("OBJECT", CommonTagActions.TA_IGNORABLE_ELEMENT);
        this.setTagAction("EMBED", CommonTagActions.TA_IGNORABLE_ELEMENT);
        this.setTagAction("APPLET", CommonTagActions.TA_IGNORABLE_ELEMENT);
        this.setTagAction("LINK", CommonTagActions.TA_IGNORABLE_ELEMENT);
        this.setTagAction("A", CommonTagActions.TA_ANCHOR_TEXT);
        this.setTagAction("BODY", CommonTagActions.TA_BODY);
        this.setTagAction("STRIKE", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("U", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("B", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("I", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("EM", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("STRONG", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("SPAN", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("SUP", CommonTagActions.TA_INLINE_NO_WHITESPACE);
        this.setTagAction("ABBR", CommonTagActions.TA_INLINE_WHITESPACE);
        this.setTagAction("ACRONYM", CommonTagActions.TA_INLINE_WHITESPACE);
        this.setTagAction("FONT", CommonTagActions.TA_INLINE_NO_WHITESPACE);
    }
    
    static {
        INSTANCE = new DefaultTagActionMap();
    }
}
