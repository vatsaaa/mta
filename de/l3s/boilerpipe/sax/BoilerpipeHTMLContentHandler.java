// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import de.l3s.boilerpipe.document.TextDocument;
import java.util.Iterator;
import de.l3s.boilerpipe.util.UnicodeTokenizer;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import java.util.ArrayList;
import java.util.regex.Pattern;
import de.l3s.boilerpipe.labels.LabelAction;
import java.util.LinkedList;
import java.util.BitSet;
import de.l3s.boilerpipe.document.TextBlock;
import java.util.List;
import java.util.Map;
import org.xml.sax.ContentHandler;

public class BoilerpipeHTMLContentHandler implements ContentHandler
{
    private final Map<String, TagAction> tagActions;
    private String title;
    static final String ANCHOR_TEXT_START = "$\ue00a<";
    static final String ANCHOR_TEXT_END = ">\ue00a$";
    StringBuilder tokenBuffer;
    StringBuilder textBuffer;
    int inBody;
    int inAnchor;
    int inIgnorableElement;
    boolean sbLastWasWhitespace;
    private int textElementIdx;
    private final List<TextBlock> textBlocks;
    private String lastStartTag;
    private String lastEndTag;
    private Event lastEvent;
    private int offsetBlocks;
    private BitSet currentContainedTextElements;
    private boolean flush;
    boolean inAnchorText;
    LinkedList<LabelAction> labelStack;
    LinkedList<Integer> fontSizeStack;
    private static final Pattern PAT_VALID_WORD_CHARACTER;
    
    public void recycle() {
        this.tokenBuffer.setLength(0);
        this.textBuffer.setLength(0);
        this.inBody = 0;
        this.inAnchor = 0;
        this.inIgnorableElement = 0;
        this.sbLastWasWhitespace = false;
        this.textElementIdx = 0;
        this.textBlocks.clear();
        this.lastStartTag = null;
        this.lastEndTag = null;
        this.lastEvent = null;
        this.offsetBlocks = 0;
        this.currentContainedTextElements.clear();
        this.flush = false;
        this.inAnchorText = false;
    }
    
    public BoilerpipeHTMLContentHandler() {
        this(DefaultTagActionMap.INSTANCE);
    }
    
    public BoilerpipeHTMLContentHandler(final TagActionMap tagActions) {
        this.title = null;
        this.tokenBuffer = new StringBuilder();
        this.textBuffer = new StringBuilder();
        this.inBody = 0;
        this.inAnchor = 0;
        this.inIgnorableElement = 0;
        this.sbLastWasWhitespace = false;
        this.textElementIdx = 0;
        this.textBlocks = new ArrayList<TextBlock>();
        this.lastStartTag = null;
        this.lastEndTag = null;
        this.lastEvent = null;
        this.offsetBlocks = 0;
        this.currentContainedTextElements = new BitSet();
        this.flush = false;
        this.inAnchorText = false;
        this.labelStack = new LinkedList<LabelAction>();
        this.fontSizeStack = new LinkedList<Integer>();
        this.tagActions = tagActions;
    }
    
    public void endDocument() throws SAXException {
        this.flushBlock();
    }
    
    public void endPrefixMapping(final String prefix) throws SAXException {
    }
    
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
        if (!this.sbLastWasWhitespace) {
            this.textBuffer.append(' ');
            this.tokenBuffer.append(' ');
        }
        this.sbLastWasWhitespace = true;
    }
    
    public void processingInstruction(final String target, final String data) throws SAXException {
    }
    
    public void setDocumentLocator(final Locator locator) {
    }
    
    public void skippedEntity(final String name) throws SAXException {
    }
    
    public void startDocument() throws SAXException {
    }
    
    public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
    }
    
    public void startElement(final String uri, final String localName, final String qName, final Attributes atts) throws SAXException {
        final TagAction ta = this.tagActions.get(localName);
        if (ta != null) {
            this.flush |= ta.start(this, localName, qName, atts);
        }
        else {
            this.flush = true;
        }
        this.lastEvent = Event.START_TAG;
        this.lastStartTag = localName;
    }
    
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        final TagAction ta = this.tagActions.get(localName);
        if (ta != null) {
            this.flush |= ta.end(this, localName, qName);
        }
        else {
            this.flush = true;
        }
        this.lastEvent = Event.END_TAG;
        this.lastEndTag = localName;
    }
    
    public void characters(final char[] ch, int start, int length) throws SAXException {
        ++this.textElementIdx;
        if (this.flush) {
            this.flushBlock();
            this.flush = false;
        }
        if (this.inIgnorableElement != 0) {
            return;
        }
        boolean startWhitespace = false;
        boolean endWhitespace = false;
        if (length == 0) {
            return;
        }
        final int end = start + length;
        for (int i = start; i < end; ++i) {
            if (Character.isWhitespace(ch[i])) {
                ch[i] = ' ';
            }
        }
        while (start < end) {
            final char c = ch[start];
            if (c != ' ') {
                break;
            }
            startWhitespace = true;
            ++start;
            --length;
        }
        while (length > 0) {
            final char c = ch[start + length - 1];
            if (c != ' ') {
                break;
            }
            endWhitespace = true;
            --length;
        }
        if (length == 0) {
            if (startWhitespace || endWhitespace) {
                if (!this.sbLastWasWhitespace) {
                    this.textBuffer.append(' ');
                    this.tokenBuffer.append(' ');
                }
                this.sbLastWasWhitespace = true;
            }
            else {
                this.sbLastWasWhitespace = false;
            }
            this.lastEvent = Event.WHITESPACE;
            return;
        }
        if (startWhitespace && !this.sbLastWasWhitespace) {
            this.textBuffer.append(' ');
            this.tokenBuffer.append(' ');
        }
        this.textBuffer.append(ch, start, length);
        this.tokenBuffer.append(ch, start, length);
        if (endWhitespace) {
            this.textBuffer.append(' ');
            this.tokenBuffer.append(' ');
        }
        this.sbLastWasWhitespace = endWhitespace;
        this.lastEvent = Event.CHARACTERS;
        this.currentContainedTextElements.set(this.textElementIdx);
    }
    
    List<TextBlock> getTextBlocks() {
        return this.textBlocks;
    }
    
    void flushBlock() {
        if (this.inBody == 0) {
            if ("TITLE".equalsIgnoreCase(this.lastStartTag) && this.inBody == 0) {
                this.setTitle(this.tokenBuffer.toString().trim());
            }
            this.textBuffer.setLength(0);
            this.tokenBuffer.setLength(0);
            return;
        }
        final int length = this.tokenBuffer.length();
        switch (length) {
            case 0: {
                return;
            }
            case 1: {
                if (this.sbLastWasWhitespace) {
                    this.textBuffer.setLength(0);
                    this.tokenBuffer.setLength(0);
                    return;
                }
                break;
            }
        }
        final String[] tokens = UnicodeTokenizer.tokenize(this.tokenBuffer);
        int numWords = 0;
        int numLinkedWords = 0;
        int numWrappedLines = 0;
        int currentLineLength = -1;
        final int maxLineLength = 80;
        int numTokens = 0;
        int numWordsCurrentLine = 0;
        for (final String token : tokens) {
            if ("$\ue00a<".equals(token)) {
                this.inAnchorText = true;
            }
            else if (">\ue00a$".equals(token)) {
                this.inAnchorText = false;
            }
            else if (isWord(token)) {
                ++numTokens;
                ++numWords;
                ++numWordsCurrentLine;
                if (this.inAnchorText) {
                    ++numLinkedWords;
                }
                final int tokenLength = token.length();
                currentLineLength += tokenLength + 1;
                if (currentLineLength > 80) {
                    ++numWrappedLines;
                    currentLineLength = tokenLength;
                    numWordsCurrentLine = 1;
                }
            }
            else {
                ++numTokens;
            }
        }
        if (numTokens == 0) {
            return;
        }
        int numWordsInWrappedLines;
        if (numWrappedLines == 0) {
            numWordsInWrappedLines = numWords;
            numWrappedLines = 1;
        }
        else {
            numWordsInWrappedLines = numWords - numWordsCurrentLine;
        }
        final TextBlock tb = new TextBlock(this.textBuffer.toString().trim(), this.currentContainedTextElements, numWords, numLinkedWords, numWordsInWrappedLines, numWrappedLines, this.offsetBlocks);
        this.currentContainedTextElements = new BitSet();
        ++this.offsetBlocks;
        this.textBuffer.setLength(0);
        this.tokenBuffer.setLength(0);
        this.addTextBlock(tb);
    }
    
    protected void addTextBlock(final TextBlock tb) {
        for (final Integer l : this.fontSizeStack) {
            if (l != null) {
                tb.addLabel("font-" + l);
                break;
            }
        }
        for (final LabelAction labels : this.labelStack) {
            if (labels != null) {
                labels.addTo(tb);
            }
        }
        this.textBlocks.add(tb);
    }
    
    private static boolean isWord(final String token) {
        return BoilerpipeHTMLContentHandler.PAT_VALID_WORD_CHARACTER.matcher(token).find();
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String s) {
        if (s == null || s.length() == 0) {
            return;
        }
        this.title = s;
    }
    
    public TextDocument toTextDocument() {
        this.flushBlock();
        return new TextDocument(this.getTitle(), this.getTextBlocks());
    }
    
    public void addWhitespaceIfNecessary() {
        if (!this.sbLastWasWhitespace) {
            this.tokenBuffer.append(' ');
            this.textBuffer.append(' ');
            this.sbLastWasWhitespace = true;
        }
    }
    
    static {
        PAT_VALID_WORD_CHARACTER = Pattern.compile("[\\p{L}\\p{Nd}\\p{Nl}\\p{No}]");
    }
    
    private enum Event
    {
        START_TAG, 
        END_TAG, 
        CHARACTERS, 
        WHITESPACE;
    }
}
