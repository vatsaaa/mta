// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPInputStream;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.charset.Charset;
import java.net.URL;

public class HTMLFetcher
{
    private HTMLFetcher() {
    }
    
    public static HTMLDocument fetch(final URL url) throws IOException {
        final URLConnection conn = url.openConnection();
        final String charset = conn.getContentEncoding();
        Charset cs = Charset.forName("Cp1252");
        if (charset != null) {
            try {
                cs = Charset.forName(charset);
            }
            catch (UnsupportedCharsetException ex) {}
        }
        InputStream in = conn.getInputStream();
        final String encoding = conn.getContentEncoding();
        if (encoding != null) {
            if ("gzip".equalsIgnoreCase(encoding)) {
                in = new GZIPInputStream(in);
            }
            else {
                System.err.println("WARN: unsupported Content-Encoding: " + encoding);
            }
        }
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final byte[] buf = new byte[4096];
        int r;
        while ((r = in.read(buf)) != -1) {
            bos.write(buf, 0, r);
        }
        in.close();
        final byte[] data = bos.toByteArray();
        return new HTMLDocument(data, cs);
    }
}
