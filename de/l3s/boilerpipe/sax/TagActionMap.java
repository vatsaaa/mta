// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import java.util.HashMap;

public abstract class TagActionMap extends HashMap<String, TagAction>
{
    private static final long serialVersionUID = 1L;
    
    protected void setTagAction(final String tag, final TagAction action) {
        this.put(tag.toUpperCase(), action);
        this.put(tag.toLowerCase(), action);
        this.put(tag, action);
    }
    
    protected void addTagAction(final String tag, final TagAction action) {
        final TagAction previousAction = ((HashMap<K, TagAction>)this).get(tag);
        if (previousAction == null) {
            this.setTagAction(tag, action);
        }
        else {
            this.setTagAction(tag, new CommonTagActions.Chained(previousAction, action));
        }
    }
}
