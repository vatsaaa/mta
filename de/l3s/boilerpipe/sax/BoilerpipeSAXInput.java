// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import java.io.IOException;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.TextDocument;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import de.l3s.boilerpipe.BoilerpipeInput;

public final class BoilerpipeSAXInput implements BoilerpipeInput
{
    private final InputSource is;
    
    public BoilerpipeSAXInput(final InputSource is) throws SAXException {
        this.is = is;
    }
    
    public TextDocument getTextDocument() throws BoilerpipeProcessingException {
        return this.getTextDocument(new BoilerpipeHTMLParser());
    }
    
    public TextDocument getTextDocument(final BoilerpipeHTMLParser parser) throws BoilerpipeProcessingException {
        try {
            parser.parse(this.is);
        }
        catch (IOException e) {
            throw new BoilerpipeProcessingException(e);
        }
        catch (SAXException e2) {
            throw new BoilerpipeProcessingException(e2);
        }
        return parser.toTextDocument();
    }
}
