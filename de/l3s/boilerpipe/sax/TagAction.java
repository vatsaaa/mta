// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;

public interface TagAction
{
    boolean start(final BoilerpipeHTMLContentHandler p0, final String p1, final String p2, final Attributes p3) throws SAXException;
    
    boolean end(final BoilerpipeHTMLContentHandler p0, final String p1, final String p2) throws SAXException;
}
