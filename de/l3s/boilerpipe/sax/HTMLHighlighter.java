// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import org.apache.xerces.xni.parser.XMLParserConfiguration;
import org.cyberneko.html.HTMLConfiguration;
import java.util.BitSet;
import org.xml.sax.ContentHandler;
import org.apache.xerces.parsers.AbstractSAXParser;
import java.util.HashMap;
import org.xml.sax.SAXException;
import java.io.IOException;
import de.l3s.boilerpipe.BoilerpipeExtractor;
import java.net.URL;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.io.Reader;
import org.xml.sax.InputSource;
import java.io.StringReader;
import de.l3s.boilerpipe.document.TextDocument;
import java.util.Map;

public final class HTMLHighlighter
{
    private boolean outputHighlightOnly;
    private String extraStyleSheet;
    private String preHighlight;
    private String postHighlight;
    private static final TagAction TA_IGNORABLE_ELEMENT;
    private static final TagAction TA_HEAD;
    private static Map<String, TagAction> TAG_ACTIONS;
    
    public static HTMLHighlighter newHighlightingInstance() {
        return new HTMLHighlighter(false);
    }
    
    public static HTMLHighlighter newExtractingInstance() {
        return new HTMLHighlighter(true);
    }
    
    private HTMLHighlighter(final boolean extractHTML) {
        this.outputHighlightOnly = false;
        this.extraStyleSheet = "\n<style type=\"text/css\">\n.x-boilerpipe-mark1 { text-decoration:none; background-color: #ffff42 !important; color: black !important; display:inline !important; visibility:visible !important; }\n</style>\n";
        this.preHighlight = "<span class=\"x-boilerpipe-mark1\">";
        this.postHighlight = "</span>";
        if (extractHTML) {
            this.setOutputHighlightOnly(true);
            this.setExtraStyleSheet("");
            this.setPreHighlight("");
            this.setPostHighlight("");
        }
    }
    
    public String process(final TextDocument doc, final String origHTML) throws BoilerpipeProcessingException {
        return this.process(doc, new InputSource(new StringReader(origHTML)));
    }
    
    public String process(final TextDocument doc, final InputSource is) throws BoilerpipeProcessingException {
        final Implementation implementation = new Implementation();
        implementation.process(doc, is);
        return implementation.html.toString();
    }
    
    public String process(final URL url, final BoilerpipeExtractor extractor) throws IOException, BoilerpipeProcessingException, SAXException {
        final HTMLDocument htmlDoc = HTMLFetcher.fetch(url);
        final TextDocument doc = new BoilerpipeSAXInput(htmlDoc.toInputSource()).getTextDocument();
        extractor.process(doc);
        final InputSource is = htmlDoc.toInputSource();
        return this.process(doc, is);
    }
    
    public boolean isOutputHighlightOnly() {
        return this.outputHighlightOnly;
    }
    
    public void setOutputHighlightOnly(final boolean outputHighlightOnly) {
        this.outputHighlightOnly = outputHighlightOnly;
    }
    
    public String getExtraStyleSheet() {
        return this.extraStyleSheet;
    }
    
    public void setExtraStyleSheet(final String extraStyleSheet) {
        this.extraStyleSheet = extraStyleSheet;
    }
    
    public String getPreHighlight() {
        return this.preHighlight;
    }
    
    public void setPreHighlight(final String preHighlight) {
        this.preHighlight = preHighlight;
    }
    
    public String getPostHighlight() {
        return this.postHighlight;
    }
    
    public void setPostHighlight(final String postHighlight) {
        this.postHighlight = postHighlight;
    }
    
    private static String xmlEncode(final String in) {
        if (in == null) {
            return "";
        }
        final StringBuilder out = new StringBuilder(in.length());
        for (int i = 0; i < in.length(); ++i) {
            final char c = in.charAt(i);
            switch (c) {
                case '<': {
                    out.append("&lt;");
                    break;
                }
                case '>': {
                    out.append("&gt;");
                    break;
                }
                case '&': {
                    out.append("&amp;");
                    break;
                }
                case '\"': {
                    out.append("&quot;");
                    break;
                }
                default: {
                    out.append(c);
                    break;
                }
            }
        }
        return out.toString();
    }
    
    static {
        TA_IGNORABLE_ELEMENT = new TagAction() {
            @Override
            void beforeStart(final Implementation instance, final String localName) {
                instance.inIgnorableElement++;
            }
            
            @Override
            void afterEnd(final Implementation instance, final String localName) {
                instance.inIgnorableElement--;
            }
        };
        TA_HEAD = new TagAction() {
            @Override
            void beforeEnd(final Implementation instance, final String localName) {
                instance.html.append(instance.hl.extraStyleSheet);
            }
        };
        (HTMLHighlighter.TAG_ACTIONS = new HashMap<String, TagAction>()).put("STYLE", HTMLHighlighter.TA_IGNORABLE_ELEMENT);
        HTMLHighlighter.TAG_ACTIONS.put("SCRIPT", HTMLHighlighter.TA_IGNORABLE_ELEMENT);
        HTMLHighlighter.TAG_ACTIONS.put("OPTION", HTMLHighlighter.TA_IGNORABLE_ELEMENT);
        HTMLHighlighter.TAG_ACTIONS.put("OBJECT", HTMLHighlighter.TA_IGNORABLE_ELEMENT);
        HTMLHighlighter.TAG_ACTIONS.put("EMBED", HTMLHighlighter.TA_IGNORABLE_ELEMENT);
        HTMLHighlighter.TAG_ACTIONS.put("APPLET", HTMLHighlighter.TA_IGNORABLE_ELEMENT);
        HTMLHighlighter.TAG_ACTIONS.put("LINK", HTMLHighlighter.TA_IGNORABLE_ELEMENT);
        HTMLHighlighter.TAG_ACTIONS.put("HEAD", HTMLHighlighter.TA_HEAD);
    }
    
    private abstract static class TagAction
    {
        void beforeStart(final Implementation instance, final String localName) {
        }
        
        void afterStart(final Implementation instance, final String localName) {
        }
        
        void beforeEnd(final Implementation instance, final String localName) {
        }
        
        void afterEnd(final Implementation instance, final String localName) {
        }
    }
    
    private final class Implementation extends AbstractSAXParser implements ContentHandler
    {
        StringBuilder html;
        private int inIgnorableElement;
        private int characterElementIdx;
        private final BitSet contentBitSet;
        private final HTMLHighlighter hl;
        
        Implementation() {
            super((XMLParserConfiguration)new HTMLConfiguration());
            this.html = new StringBuilder();
            this.inIgnorableElement = 0;
            this.characterElementIdx = 0;
            this.contentBitSet = new BitSet();
            this.hl = HTMLHighlighter.this;
            this.setContentHandler((ContentHandler)this);
        }
        
        void process(final TextDocument doc, final InputSource is) throws BoilerpipeProcessingException {
            for (final TextBlock block : doc.getTextBlocks()) {
                if (block.isContent()) {
                    final BitSet bs = block.getContainedTextElements();
                    if (bs == null) {
                        continue;
                    }
                    this.contentBitSet.or(bs);
                }
            }
            try {
                this.parse(is);
            }
            catch (SAXException e) {
                throw new BoilerpipeProcessingException(e);
            }
            catch (IOException e2) {
                throw new BoilerpipeProcessingException(e2);
            }
        }
        
        public void endDocument() throws SAXException {
        }
        
        public void endPrefixMapping(final String prefix) throws SAXException {
        }
        
        public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
        }
        
        public void processingInstruction(final String target, final String data) throws SAXException {
        }
        
        public void setDocumentLocator(final Locator locator) {
        }
        
        public void skippedEntity(final String name) throws SAXException {
        }
        
        public void startDocument() throws SAXException {
        }
        
        public void startElement(final String uri, final String localName, final String qName, final Attributes atts) throws SAXException {
            final TagAction ta = HTMLHighlighter.TAG_ACTIONS.get(localName);
            if (ta != null) {
                ta.beforeStart(this, localName);
            }
            try {
                if (this.inIgnorableElement == 0) {
                    if (HTMLHighlighter.this.outputHighlightOnly) {
                        final boolean highlight = this.contentBitSet.get(this.characterElementIdx);
                        if (!highlight) {
                            return;
                        }
                    }
                    this.html.append('<');
                    this.html.append(qName);
                    for (int numAtts = atts.getLength(), i = 0; i < numAtts; ++i) {
                        final String attr = atts.getQName(i);
                        final String value = atts.getValue(i);
                        this.html.append(' ');
                        this.html.append(attr);
                        this.html.append("=\"");
                        this.html.append(xmlEncode(value));
                        this.html.append("\"");
                    }
                    this.html.append('>');
                }
            }
            finally {
                if (ta != null) {
                    ta.afterStart(this, localName);
                }
            }
        }
        
        public void endElement(final String uri, final String localName, final String qName) throws SAXException {
            final TagAction ta = HTMLHighlighter.TAG_ACTIONS.get(localName);
            if (ta != null) {
                ta.beforeEnd(this, localName);
            }
            try {
                if (this.inIgnorableElement == 0) {
                    if (HTMLHighlighter.this.outputHighlightOnly) {
                        final boolean highlight = this.contentBitSet.get(this.characterElementIdx);
                        if (!highlight) {
                            return;
                        }
                    }
                    this.html.append("</");
                    this.html.append(qName);
                    this.html.append('>');
                }
            }
            finally {
                if (ta != null) {
                    ta.afterEnd(this, localName);
                }
            }
        }
        
        public void characters(final char[] ch, final int start, final int length) throws SAXException {
            ++this.characterElementIdx;
            if (this.inIgnorableElement == 0) {
                final boolean highlight = this.contentBitSet.get(this.characterElementIdx);
                if (!highlight && HTMLHighlighter.this.outputHighlightOnly) {
                    return;
                }
                if (highlight) {
                    this.html.append(HTMLHighlighter.this.preHighlight);
                }
                this.html.append(xmlEncode(String.valueOf(ch, start, length)));
                if (highlight) {
                    this.html.append(HTMLHighlighter.this.postHighlight);
                }
            }
        }
        
        public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
        }
    }
}
