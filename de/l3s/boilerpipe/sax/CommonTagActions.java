// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.sax;

import de.l3s.boilerpipe.labels.LabelAction;
import java.util.Iterator;
import java.util.regex.Matcher;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import java.util.regex.Pattern;

public abstract class CommonTagActions
{
    public static final TagAction TA_IGNORABLE_ELEMENT;
    public static final TagAction TA_ANCHOR_TEXT;
    public static final TagAction TA_BODY;
    public static final TagAction TA_INLINE_WHITESPACE;
    @Deprecated
    public static final TagAction TA_INLINE;
    public static final TagAction TA_INLINE_NO_WHITESPACE;
    private static final Pattern PAT_FONT_SIZE;
    public static final TagAction TA_FONT;
    
    private CommonTagActions() {
    }
    
    static {
        TA_IGNORABLE_ELEMENT = new TagAction() {
            public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) {
                ++instance.inIgnorableElement;
                return true;
            }
            
            public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
                --instance.inIgnorableElement;
                return true;
            }
        };
        TA_ANCHOR_TEXT = new TagAction() {
            public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) throws SAXException {
                if (instance.inAnchor++ == 0) {
                    if (instance.inIgnorableElement == 0) {
                        instance.addWhitespaceIfNecessary();
                        instance.tokenBuffer.append("$\ue00a<");
                        instance.tokenBuffer.append(' ');
                        instance.sbLastWasWhitespace = true;
                    }
                    return false;
                }
                throw new SAXException("SAX input contains nested A elements -- You have probably hit a bug in your HTML parser (e.g., NekoHTML bug #2909310). Please clean the HTML externally and feed it to boilerpipe again");
            }
            
            public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
                final int inAnchor = instance.inAnchor - 1;
                instance.inAnchor = inAnchor;
                if (inAnchor == 0 && instance.inIgnorableElement == 0) {
                    instance.addWhitespaceIfNecessary();
                    instance.tokenBuffer.append(">\ue00a$");
                    instance.tokenBuffer.append(' ');
                    instance.sbLastWasWhitespace = true;
                }
                return false;
            }
        };
        TA_BODY = new TagAction() {
            public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) {
                instance.flushBlock();
                ++instance.inBody;
                return false;
            }
            
            public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
                instance.flushBlock();
                --instance.inBody;
                return false;
            }
        };
        TA_INLINE_WHITESPACE = new TagAction() {
            public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) {
                instance.addWhitespaceIfNecessary();
                return false;
            }
            
            public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
                instance.addWhitespaceIfNecessary();
                return false;
            }
        };
        TA_INLINE = CommonTagActions.TA_INLINE_WHITESPACE;
        TA_INLINE_NO_WHITESPACE = new TagAction() {
            public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) {
                return false;
            }
            
            public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
                return false;
            }
        };
        PAT_FONT_SIZE = Pattern.compile("([\\+\\-]?)([0-9])");
        TA_FONT = new TagAction() {
            public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) {
                final String sizeAttr = atts.getValue("size");
                if (sizeAttr != null) {
                    final Matcher m = CommonTagActions.PAT_FONT_SIZE.matcher(sizeAttr);
                    if (m.matches()) {
                        final String rel = m.group(1);
                        final int val = Integer.parseInt(m.group(2));
                        int size;
                        if (rel.length() == 0) {
                            size = val;
                        }
                        else {
                            int prevSize;
                            if (instance.fontSizeStack.isEmpty()) {
                                prevSize = 3;
                            }
                            else {
                                prevSize = 3;
                                for (final Integer s : instance.fontSizeStack) {
                                    if (s != null) {
                                        prevSize = s;
                                        break;
                                    }
                                }
                            }
                            if (rel.charAt(0) == '+') {
                                size = prevSize + val;
                            }
                            else {
                                size = prevSize - val;
                            }
                        }
                        instance.fontSizeStack.add(0, size);
                    }
                    else {
                        instance.fontSizeStack.add(0, null);
                    }
                }
                else {
                    instance.fontSizeStack.add(0, null);
                }
                return false;
            }
            
            public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
                instance.fontSizeStack.removeFirst();
                return false;
            }
        };
    }
    
    public static final class Chained implements TagAction
    {
        private final TagAction t1;
        private final TagAction t2;
        
        public Chained(final TagAction t1, final TagAction t2) {
            this.t1 = t1;
            this.t2 = t2;
        }
        
        public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) throws SAXException {
            return this.t1.start(instance, localName, qName, atts) | this.t2.start(instance, localName, qName, atts);
        }
        
        public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) throws SAXException {
            return this.t1.end(instance, localName, qName) | this.t2.end(instance, localName, qName);
        }
    }
    
    public static final class InlineTagLabelAction implements TagAction
    {
        private final LabelAction action;
        
        public InlineTagLabelAction(final LabelAction action) {
            this.action = action;
        }
        
        public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) {
            instance.addWhitespaceIfNecessary();
            instance.labelStack.add(this.action);
            return false;
        }
        
        public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
            instance.addWhitespaceIfNecessary();
            instance.labelStack.removeLast();
            return false;
        }
    }
    
    public static final class BlockTagLabelAction implements TagAction
    {
        private final LabelAction action;
        
        public BlockTagLabelAction(final LabelAction action) {
            this.action = action;
        }
        
        public boolean start(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName, final Attributes atts) {
            instance.labelStack.add(this.action);
            return true;
        }
        
        public boolean end(final BoilerpipeHTMLContentHandler instance, final String localName, final String qName) {
            instance.labelStack.removeLast();
            return true;
        }
    }
}
