// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.util;

import java.util.regex.Pattern;

public class UnicodeTokenizer
{
    private static final Pattern PAT_WORD_BOUNDARY;
    private static final Pattern PAT_NOT_WORD_BOUNDARY;
    
    public static String[] tokenize(final CharSequence text) {
        return UnicodeTokenizer.PAT_NOT_WORD_BOUNDARY.matcher(UnicodeTokenizer.PAT_WORD_BOUNDARY.matcher(text).replaceAll("\u2063")).replaceAll("$1").replaceAll("[ \u2063]+", " ").trim().split("[ ]+");
    }
    
    static {
        PAT_WORD_BOUNDARY = Pattern.compile("\\b");
        PAT_NOT_WORD_BOUNDARY = Pattern.compile("[\u2063]*([\\\"'\\.,\\!\\@\\-\\:\\;\\$\\?\\(\\)/])[\u2063]*");
    }
}
