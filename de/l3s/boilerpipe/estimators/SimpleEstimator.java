// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.estimators;

import de.l3s.boilerpipe.document.TextDocumentStatistics;

public final class SimpleEstimator
{
    public static final SimpleEstimator INSTANCE;
    
    private SimpleEstimator() {
    }
    
    public boolean isLowQuality(final TextDocumentStatistics dsBefore, final TextDocumentStatistics dsAfter) {
        return dsBefore.getNumWords() < 90 || dsAfter.getNumWords() < 70 || dsAfter.avgNumWords() < 25.0f;
    }
    
    static {
        INSTANCE = new SimpleEstimator();
    }
}
