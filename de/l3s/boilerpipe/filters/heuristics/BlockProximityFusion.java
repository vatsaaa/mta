// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.heuristics;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class BlockProximityFusion implements BoilerpipeFilter
{
    private final int maxBlocksDistance;
    public static final BlockProximityFusion MAX_DISTANCE_1;
    public static final BlockProximityFusion MAX_DISTANCE_1_CONTENT_ONLY;
    private final boolean contentOnly;
    
    public BlockProximityFusion(final int maxBlocksDistance, final boolean contentOnly) {
        this.maxBlocksDistance = maxBlocksDistance;
        this.contentOnly = contentOnly;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        final List<TextBlock> textBlocks = doc.getTextBlocks();
        if (textBlocks.size() < 2) {
            return false;
        }
        boolean changes = false;
        TextBlock prevBlock;
        int offset;
        if (this.contentOnly) {
            prevBlock = null;
            offset = 0;
            for (final TextBlock tb : textBlocks) {
                ++offset;
                if (tb.isContent()) {
                    prevBlock = tb;
                    break;
                }
            }
            if (prevBlock == null) {
                return false;
            }
        }
        else {
            prevBlock = textBlocks.get(0);
            offset = 1;
        }
        final Iterator<TextBlock> it = textBlocks.listIterator(offset);
        while (it.hasNext()) {
            final TextBlock block = it.next();
            if (!block.isContent()) {
                prevBlock = block;
            }
            else {
                final int diffBlocks = block.getOffsetBlocksStart() - prevBlock.getOffsetBlocksEnd() - 1;
                if (diffBlocks <= this.maxBlocksDistance) {
                    boolean ok = true;
                    if (this.contentOnly && (!prevBlock.isContent() || !block.isContent())) {
                        ok = false;
                    }
                    if (ok) {
                        prevBlock.mergeNext(block);
                        it.remove();
                        changes = true;
                    }
                    else {
                        prevBlock = block;
                    }
                }
                else {
                    prevBlock = block;
                }
            }
        }
        return changes;
    }
    
    static {
        MAX_DISTANCE_1 = new BlockProximityFusion(1, false);
        MAX_DISTANCE_1_CONTENT_ONLY = new BlockProximityFusion(1, true);
    }
}
