// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.heuristics;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class KeepLargestBlockFilter implements BoilerpipeFilter
{
    public static final KeepLargestBlockFilter INSTANCE;
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        final List<TextBlock> textBlocks = doc.getTextBlocks();
        if (textBlocks.size() < 2) {
            return false;
        }
        int maxNumWords = -1;
        TextBlock largestBlock = null;
        for (final TextBlock tb : textBlocks) {
            if (!tb.isContent()) {
                continue;
            }
            if (tb.getNumWords() <= maxNumWords) {
                continue;
            }
            largestBlock = tb;
            maxNumWords = tb.getNumWords();
        }
        for (final TextBlock tb : textBlocks) {
            if (tb == largestBlock) {
                tb.setIsContent(true);
            }
            else {
                tb.setIsContent(false);
                tb.addLabel("de.l3s.boilerpipe/MIGHT_BE_CONTENT");
            }
        }
        return true;
    }
    
    static {
        INSTANCE = new KeepLargestBlockFilter();
    }
}
