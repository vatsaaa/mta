// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.heuristics;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public class SimpleBlockFusionProcessor implements BoilerpipeFilter
{
    public static final SimpleBlockFusionProcessor INSTANCE;
    
    public static SimpleBlockFusionProcessor getInstance() {
        return SimpleBlockFusionProcessor.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        final List<TextBlock> textBlocks = doc.getTextBlocks();
        boolean changes = false;
        if (textBlocks.size() < 2) {
            return false;
        }
        TextBlock b1 = textBlocks.get(0);
        final Iterator<TextBlock> it = textBlocks.listIterator(1);
        while (it.hasNext()) {
            final TextBlock b2 = it.next();
            final boolean similar = b1.getTextDensity() == b2.getTextDensity();
            if (similar) {
                b1.mergeNext(b2);
                it.remove();
                changes = true;
            }
            else {
                b1 = b2;
            }
        }
        return changes;
    }
    
    static {
        INSTANCE = new SimpleBlockFusionProcessor();
    }
}
