// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.heuristics;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class ExpandTitleToContentFilter implements BoilerpipeFilter
{
    public static final ExpandTitleToContentFilter INSTANCE;
    
    public static ExpandTitleToContentFilter getInstance() {
        return ExpandTitleToContentFilter.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        int i = 0;
        int title = -1;
        int contentStart = -1;
        for (final TextBlock tb : doc.getTextBlocks()) {
            if (contentStart == -1 && tb.hasLabel("de.l3s.boilerpipe/TITLE")) {
                title = i;
                contentStart = -1;
            }
            if (contentStart == -1 && tb.isContent()) {
                contentStart = i;
            }
            ++i;
        }
        if (contentStart <= title || title == -1) {
            return false;
        }
        boolean changes = false;
        for (final TextBlock tb2 : doc.getTextBlocks().subList(title, contentStart)) {
            if (tb2.hasLabel("de.l3s.boilerpipe/MIGHT_BE_CONTENT")) {
                changes |= tb2.setIsContent(true);
            }
        }
        return changes;
    }
    
    static {
        INSTANCE = new ExpandTitleToContentFilter();
    }
}
