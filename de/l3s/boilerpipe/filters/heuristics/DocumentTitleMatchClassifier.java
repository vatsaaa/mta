// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.heuristics;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import java.util.HashSet;
import java.util.Set;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class DocumentTitleMatchClassifier implements BoilerpipeFilter
{
    private final Set<String> potentialTitles;
    
    public DocumentTitleMatchClassifier(String title) {
        if (title == null) {
            this.potentialTitles = null;
        }
        else {
            title = title.trim();
            if (title.length() == 0) {
                this.potentialTitles = null;
            }
            else {
                (this.potentialTitles = new HashSet<String>()).add(title);
                String p = this.getLongestPart(title, "[ ]*[\\|:][ ]*");
                if (p != null) {
                    this.potentialTitles.add(p);
                }
                p = this.getLongestPart(title, "[ ]*[\\|:\\(\\)][ ]*");
                if (p != null) {
                    this.potentialTitles.add(p);
                }
            }
        }
    }
    
    private String getLongestPart(final String title, final String pattern) {
        final String[] parts = title.split(pattern);
        if (parts.length == 1) {
            return null;
        }
        int longestNumWords = 0;
        String longestPart = "";
        for (int i = 0; i < parts.length; ++i) {
            final String p = parts[i];
            if (!p.contains(".com")) {
                final int numWords = p.split("[\b]+").length;
                if (numWords > longestNumWords || p.length() > longestPart.length()) {
                    longestNumWords = numWords;
                    longestPart = p;
                }
            }
        }
        if (longestPart.length() == 0) {
            return null;
        }
        return longestPart.trim();
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        if (this.potentialTitles == null) {
            return false;
        }
        boolean changes = false;
        for (final TextBlock tb : doc.getTextBlocks()) {
            final String text = tb.getText().trim();
            for (final String candidate : this.potentialTitles) {
                if (candidate.equals(text)) {
                    tb.addLabel("de.l3s.boilerpipe/TITLE");
                    changes = true;
                }
            }
        }
        return changes;
    }
}
