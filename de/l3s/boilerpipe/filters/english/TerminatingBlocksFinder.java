// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.english;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import java.util.regex.Pattern;
import de.l3s.boilerpipe.BoilerpipeFilter;

public class TerminatingBlocksFinder implements BoilerpipeFilter
{
    public static final TerminatingBlocksFinder INSTANCE;
    private static final Pattern N_COMMENTS;
    
    public static TerminatingBlocksFinder getInstance() {
        return TerminatingBlocksFinder.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        boolean changes = false;
        for (final TextBlock tb : doc.getTextBlocks()) {
            if (tb.getNumWords() < 20) {
                final String text = tb.getText().trim();
                if (!text.startsWith("Comments") && !TerminatingBlocksFinder.N_COMMENTS.matcher(text).find() && !text.contains("What you think...") && !text.contains("add your comment") && !text.contains("Add your comment") && !text.contains("Add Your Comment") && !text.contains("Add Comment") && !text.contains("Reader views") && !text.contains("Have your say") && !text.contains("Have Your Say") && !text.contains("Reader Comments") && !text.equals("Thanks for your comments - this feedback is now closed") && !text.startsWith("© Reuters") && !text.startsWith("Please rate this")) {
                    continue;
                }
                tb.addLabel("de.l3s.boilerpipe/INDICATES_END_OF_TEXT");
                changes = true;
            }
        }
        return changes;
    }
    
    static {
        INSTANCE = new TerminatingBlocksFinder();
        N_COMMENTS = Pattern.compile("(?msi)^[0-9]+ (Comments|users responded in)");
    }
}
