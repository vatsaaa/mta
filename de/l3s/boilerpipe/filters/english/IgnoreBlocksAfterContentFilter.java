// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.english;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class IgnoreBlocksAfterContentFilter extends HeuristicFilterBase implements BoilerpipeFilter
{
    public static final IgnoreBlocksAfterContentFilter DEFAULT_INSTANCE;
    private final int minNumWords;
    
    public static IgnoreBlocksAfterContentFilter getDefaultInstance() {
        return IgnoreBlocksAfterContentFilter.DEFAULT_INSTANCE;
    }
    
    public IgnoreBlocksAfterContentFilter(final int minNumWords) {
        this.minNumWords = minNumWords;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        boolean changes = false;
        int numWords = 0;
        boolean foundEndOfText = false;
        for (final TextBlock block : doc.getTextBlocks()) {
            final boolean endOfText = block.hasLabel("de.l3s.boilerpipe/INDICATES_END_OF_TEXT");
            if (block.isContent()) {
                numWords += HeuristicFilterBase.getNumFullTextWords(block);
            }
            if (endOfText && numWords >= this.minNumWords) {
                foundEndOfText = true;
            }
            if (foundEndOfText) {
                changes = true;
                block.setIsContent(false);
            }
        }
        return changes;
    }
    
    static {
        DEFAULT_INSTANCE = new IgnoreBlocksAfterContentFilter(60);
    }
}
