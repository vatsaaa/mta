// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.english;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.ListIterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public class DensityRulesClassifier implements BoilerpipeFilter
{
    public static final DensityRulesClassifier INSTANCE;
    
    public static DensityRulesClassifier getInstance() {
        return DensityRulesClassifier.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        final List<TextBlock> textBlocks = doc.getTextBlocks();
        boolean hasChanges = false;
        final ListIterator<TextBlock> it = textBlocks.listIterator();
        if (!it.hasNext()) {
            return false;
        }
        TextBlock prevBlock = TextBlock.EMPTY_START;
        TextBlock currentBlock = it.next();
        TextBlock nextBlock = it.hasNext() ? it.next() : TextBlock.EMPTY_START;
        hasChanges |= this.classify(prevBlock, currentBlock, nextBlock);
        if (nextBlock != TextBlock.EMPTY_START) {
            while (it.hasNext()) {
                prevBlock = currentBlock;
                currentBlock = nextBlock;
                nextBlock = it.next();
                hasChanges |= this.classify(prevBlock, currentBlock, nextBlock);
            }
            prevBlock = currentBlock;
            currentBlock = nextBlock;
            nextBlock = TextBlock.EMPTY_START;
            hasChanges |= this.classify(prevBlock, currentBlock, nextBlock);
        }
        return hasChanges;
    }
    
    protected boolean classify(final TextBlock prev, final TextBlock curr, final TextBlock next) {
        boolean isContent;
        if (curr.getLinkDensity() <= 0.333333) {
            if (prev.getLinkDensity() <= 0.555556) {
                if (curr.getTextDensity() <= 9.0f) {
                    isContent = (next.getTextDensity() > 10.0f || prev.getTextDensity() > 4.0f);
                }
                else {
                    isContent = (next.getTextDensity() != 0.0f);
                }
            }
            else {
                isContent = (next.getTextDensity() > 11.0f);
            }
        }
        else {
            isContent = false;
        }
        return curr.setIsContent(isContent);
    }
    
    static {
        INSTANCE = new DensityRulesClassifier();
    }
}
