// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.english;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class MinFulltextWordsFilter extends HeuristicFilterBase implements BoilerpipeFilter
{
    public static final MinFulltextWordsFilter DEFAULT_INSTANCE;
    private final int minWords;
    
    public static MinFulltextWordsFilter getDefaultInstance() {
        return MinFulltextWordsFilter.DEFAULT_INSTANCE;
    }
    
    public MinFulltextWordsFilter(final int minWords) {
        this.minWords = minWords;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        boolean changes = false;
        for (final TextBlock tb : doc.getTextBlocks()) {
            if (!tb.isContent()) {
                continue;
            }
            if (HeuristicFilterBase.getNumFullTextWords(tb) >= this.minWords) {
                continue;
            }
            tb.setIsContent(false);
            changes = true;
        }
        return changes;
    }
    
    static {
        DEFAULT_INSTANCE = new MinFulltextWordsFilter(30);
    }
}
