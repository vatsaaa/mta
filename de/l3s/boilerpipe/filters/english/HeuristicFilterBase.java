// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.english;

import de.l3s.boilerpipe.document.TextBlock;

abstract class HeuristicFilterBase
{
    protected static int getNumFullTextWords(final TextBlock tb) {
        return getNumFullTextWords(tb, 9.0f);
    }
    
    protected static int getNumFullTextWords(final TextBlock tb, final float minTextDensity) {
        if (tb.getTextDensity() >= minTextDensity) {
            return tb.getNumWords();
        }
        return 0;
    }
}
