// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.english;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class KeepLargestFulltextBlockFilter extends HeuristicFilterBase implements BoilerpipeFilter
{
    public static final KeepLargestFulltextBlockFilter INSTANCE;
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        final List<TextBlock> textBlocks = doc.getTextBlocks();
        if (textBlocks.size() < 2) {
            return false;
        }
        int max = -1;
        TextBlock largestBlock = null;
        int index = 0;
        for (final TextBlock tb : textBlocks) {
            if (!tb.isContent()) {
                continue;
            }
            final int numWords = HeuristicFilterBase.getNumFullTextWords(tb);
            if (numWords > max) {
                largestBlock = tb;
                max = numWords;
            }
            ++index;
        }
        if (largestBlock == null) {
            return false;
        }
        for (final TextBlock tb : textBlocks) {
            if (tb == largestBlock) {
                tb.setIsContent(true);
            }
            else {
                tb.setIsContent(false);
                tb.addLabel("de.l3s.boilerpipe/MIGHT_BE_CONTENT");
            }
        }
        return true;
    }
    
    static {
        INSTANCE = new KeepLargestFulltextBlockFilter();
    }
}
