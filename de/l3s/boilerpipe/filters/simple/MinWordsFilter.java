// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.simple;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class MinWordsFilter implements BoilerpipeFilter
{
    private final int minWords;
    
    public MinWordsFilter(final int minWords) {
        this.minWords = minWords;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        boolean changes = false;
        for (final TextBlock tb : doc.getTextBlocks()) {
            if (!tb.isContent()) {
                continue;
            }
            if (tb.getNumWords() >= this.minWords) {
                continue;
            }
            tb.setIsContent(false);
            changes = true;
        }
        return changes;
    }
}
