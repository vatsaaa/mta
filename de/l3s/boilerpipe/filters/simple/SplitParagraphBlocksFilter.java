// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.simple;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Set;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import de.l3s.boilerpipe.document.TextBlock;
import java.util.ArrayList;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class SplitParagraphBlocksFilter implements BoilerpipeFilter
{
    public static final SplitParagraphBlocksFilter INSTANCE;
    
    public static SplitParagraphBlocksFilter getInstance() {
        return SplitParagraphBlocksFilter.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        boolean changes = false;
        final List<TextBlock> blocks = doc.getTextBlocks();
        final List<TextBlock> blocksNew = new ArrayList<TextBlock>();
        for (final TextBlock tb : blocks) {
            final String text = tb.getText();
            final String[] paragraphs = text.split("[\n\r]+");
            if (paragraphs.length < 2) {
                blocksNew.add(tb);
            }
            else {
                final boolean isContent = tb.isContent();
                final Set<String> labels = tb.getLabels();
                for (final String p : paragraphs) {
                    final TextBlock tbP = new TextBlock(p);
                    tbP.setIsContent(isContent);
                    tbP.addLabels(labels);
                    blocksNew.add(tbP);
                    changes = true;
                }
            }
        }
        if (changes) {
            blocks.clear();
            blocks.addAll(blocksNew);
        }
        return changes;
    }
    
    static {
        INSTANCE = new SplitParagraphBlocksFilter();
    }
}
