// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.simple;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class InvertedFilter implements BoilerpipeFilter
{
    public static final InvertedFilter INSTANCE;
    
    private InvertedFilter() {
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        final List<TextBlock> tbs = doc.getTextBlocks();
        if (tbs.isEmpty()) {
            return false;
        }
        for (final TextBlock tb : tbs) {
            tb.setIsContent(!tb.isContent());
        }
        return true;
    }
    
    static {
        INSTANCE = new InvertedFilter();
    }
}
