// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.simple;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class LabelToContentFilter implements BoilerpipeFilter
{
    private String[] labels;
    
    public LabelToContentFilter(final String... label) {
        this.labels = label;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        boolean changes = false;
        for (final TextBlock tb : doc.getTextBlocks()) {
            if (!tb.isContent()) {
                for (final String label : this.labels) {
                    if (tb.hasLabel(label)) {
                        tb.setIsContent(true);
                        changes = true;
                        break;
                    }
                }
            }
        }
        return changes;
    }
}
