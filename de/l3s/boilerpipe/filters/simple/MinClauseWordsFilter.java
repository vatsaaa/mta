// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.simple;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.regex.Matcher;
import java.util.Iterator;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import java.util.regex.Pattern;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class MinClauseWordsFilter implements BoilerpipeFilter
{
    public static final MinClauseWordsFilter INSTANCE;
    private int minWords;
    private final boolean acceptClausesWithoutDelimiter;
    private final Pattern PAT_CLAUSE_DELIMITER;
    private final Pattern PAT_WHITESPACE;
    
    public MinClauseWordsFilter(final int minWords) {
        this(minWords, false);
    }
    
    public MinClauseWordsFilter(final int minWords, final boolean acceptClausesWithoutDelimiter) {
        this.PAT_CLAUSE_DELIMITER = Pattern.compile("[\\p{L}\\d][\\,\\.\\:\\;\\!\\?]+([ \\n\\r]+|$)");
        this.PAT_WHITESPACE = Pattern.compile("[ \\n\\r]+");
        this.minWords = minWords;
        this.acceptClausesWithoutDelimiter = acceptClausesWithoutDelimiter;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        boolean changes = false;
        for (final TextBlock tb : doc.getTextBlocks()) {
            if (!tb.isContent()) {
                continue;
            }
            final String text = tb.getText();
            final Matcher m = this.PAT_CLAUSE_DELIMITER.matcher(text);
            boolean found = m.find();
            int start = 0;
            boolean hasClause = false;
            while (found) {
                final int end = m.start() + 1;
                hasClause = this.isClause(text.subSequence(start, end));
                start = m.end();
                if (hasClause) {
                    break;
                }
                found = m.find();
            }
            final int end = text.length();
            if (this.acceptClausesWithoutDelimiter) {
                hasClause |= this.isClause(text.subSequence(start, end));
            }
            if (hasClause) {
                continue;
            }
            tb.setIsContent(false);
            changes = true;
        }
        return changes;
    }
    
    private boolean isClause(final CharSequence text) {
        final Matcher m = this.PAT_WHITESPACE.matcher(text);
        int n = 1;
        while (m.find()) {
            if (++n >= this.minWords) {
                return true;
            }
        }
        return n >= this.minWords;
    }
    
    static {
        INSTANCE = new MinClauseWordsFilter(5, false);
    }
}
