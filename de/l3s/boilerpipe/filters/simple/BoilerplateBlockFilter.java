// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.filters.simple;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import java.util.Iterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public final class BoilerplateBlockFilter implements BoilerpipeFilter
{
    public static final BoilerplateBlockFilter INSTANCE;
    
    public static BoilerplateBlockFilter getInstance() {
        return BoilerplateBlockFilter.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        final List<TextBlock> textBlocks = doc.getTextBlocks();
        boolean hasChanges = false;
        final Iterator<TextBlock> it = textBlocks.iterator();
        while (it.hasNext()) {
            final TextBlock tb = it.next();
            if (!tb.isContent()) {
                it.remove();
                hasChanges = true;
            }
        }
        return hasChanges;
    }
    
    static {
        INSTANCE = new BoilerplateBlockFilter();
    }
}
