// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe;

import de.l3s.boilerpipe.document.TextDocument;

public interface BoilerpipeInput
{
    TextDocument getTextDocument() throws BoilerpipeProcessingException;
}
