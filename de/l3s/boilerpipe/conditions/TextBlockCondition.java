// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.conditions;

import de.l3s.boilerpipe.document.TextBlock;

public interface TextBlockCondition
{
    boolean meetsCondition(final TextBlock p0);
}
