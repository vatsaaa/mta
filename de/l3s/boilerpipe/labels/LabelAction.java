// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.labels;

import de.l3s.boilerpipe.document.TextBlock;

public class LabelAction
{
    protected final String[] labels;
    
    public LabelAction(final String... labels) {
        this.labels = labels;
    }
    
    public void addTo(final TextBlock tb) {
        this.addLabelsTo(tb);
    }
    
    protected final void addLabelsTo(final TextBlock tb) {
        tb.addLabels(this.labels);
    }
}
