// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.labels;

import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.conditions.TextBlockCondition;

public final class ConditionalLabelAction extends LabelAction
{
    private final TextBlockCondition condition;
    
    public ConditionalLabelAction(final TextBlockCondition condition, final String... labels) {
        super(labels);
        this.condition = condition;
    }
    
    @Override
    public void addTo(final TextBlock tb) {
        if (this.condition.meetsCondition(tb)) {
            this.addLabelsTo(tb);
        }
    }
}
