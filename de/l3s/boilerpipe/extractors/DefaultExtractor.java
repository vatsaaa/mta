// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.filters.english.DensityRulesClassifier;
import de.l3s.boilerpipe.filters.heuristics.BlockProximityFusion;
import de.l3s.boilerpipe.filters.heuristics.SimpleBlockFusionProcessor;
import de.l3s.boilerpipe.document.TextDocument;

public class DefaultExtractor extends ExtractorBase
{
    public static final DefaultExtractor INSTANCE;
    
    public static DefaultExtractor getInstance() {
        return DefaultExtractor.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return SimpleBlockFusionProcessor.INSTANCE.process(doc) | BlockProximityFusion.MAX_DISTANCE_1.process(doc) | DensityRulesClassifier.INSTANCE.process(doc);
    }
    
    static {
        INSTANCE = new DefaultExtractor();
    }
}
