// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.filters.heuristics.ExpandTitleToContentFilter;
import de.l3s.boilerpipe.filters.english.KeepLargestFulltextBlockFilter;
import de.l3s.boilerpipe.filters.simple.BoilerplateBlockFilter;
import de.l3s.boilerpipe.filters.heuristics.BlockProximityFusion;
import de.l3s.boilerpipe.filters.english.IgnoreBlocksAfterContentFilter;
import de.l3s.boilerpipe.filters.english.NumWordsRulesClassifier;
import de.l3s.boilerpipe.filters.heuristics.DocumentTitleMatchClassifier;
import de.l3s.boilerpipe.filters.english.TerminatingBlocksFinder;
import de.l3s.boilerpipe.document.TextDocument;

public final class ArticleExtractor extends ExtractorBase
{
    public static final ArticleExtractor INSTANCE;
    
    public static ArticleExtractor getInstance() {
        return ArticleExtractor.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return TerminatingBlocksFinder.INSTANCE.process(doc) | new DocumentTitleMatchClassifier(doc.getTitle()).process(doc) | NumWordsRulesClassifier.INSTANCE.process(doc) | IgnoreBlocksAfterContentFilter.DEFAULT_INSTANCE.process(doc) | BlockProximityFusion.MAX_DISTANCE_1.process(doc) | BoilerplateBlockFilter.INSTANCE.process(doc) | BlockProximityFusion.MAX_DISTANCE_1_CONTENT_ONLY.process(doc) | KeepLargestFulltextBlockFilter.INSTANCE.process(doc) | ExpandTitleToContentFilter.INSTANCE.process(doc);
    }
    
    static {
        INSTANCE = new ArticleExtractor();
    }
}
