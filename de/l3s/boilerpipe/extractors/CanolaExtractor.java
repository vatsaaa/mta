// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import java.util.ListIterator;
import java.util.List;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.BoilerpipeFilter;

public class CanolaExtractor extends ExtractorBase
{
    public static final CanolaExtractor INSTANCE;
    public static final BoilerpipeFilter CLASSIFIER;
    
    public static CanolaExtractor getInstance() {
        return CanolaExtractor.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return CanolaExtractor.CLASSIFIER.process(doc);
    }
    
    static {
        INSTANCE = new CanolaExtractor();
        CLASSIFIER = new BoilerpipeFilter() {
            public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
                final List<TextBlock> textBlocks = doc.getTextBlocks();
                boolean hasChanges = false;
                final ListIterator<TextBlock> it = textBlocks.listIterator();
                if (!it.hasNext()) {
                    return false;
                }
                TextBlock prevBlock = TextBlock.EMPTY_START;
                TextBlock currentBlock = it.next();
                TextBlock nextBlock = it.hasNext() ? it.next() : TextBlock.EMPTY_START;
                hasChanges |= this.classify(prevBlock, currentBlock, nextBlock);
                if (nextBlock != TextBlock.EMPTY_START) {
                    while (it.hasNext()) {
                        prevBlock = currentBlock;
                        currentBlock = nextBlock;
                        nextBlock = it.next();
                        hasChanges |= this.classify(prevBlock, currentBlock, nextBlock);
                    }
                    prevBlock = currentBlock;
                    currentBlock = nextBlock;
                    nextBlock = TextBlock.EMPTY_START;
                    hasChanges |= this.classify(prevBlock, currentBlock, nextBlock);
                }
                return hasChanges;
            }
            
            protected boolean classify(final TextBlock prev, final TextBlock curr, final TextBlock next) {
                final boolean isContent = (curr.getLinkDensity() > 0.0f && next.getNumWords() > 11) || curr.getNumWords() > 19 || (next.getNumWords() > 6 && next.getLinkDensity() == 0.0f && prev.getLinkDensity() == 0.0f && (curr.getNumWords() > 6 || prev.getNumWords() > 7 || next.getNumWords() > 19));
                return curr.setIsContent(isContent);
            }
        };
    }
}
