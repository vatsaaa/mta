// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.filters.simple.MarkEverythingContentFilter;
import de.l3s.boilerpipe.filters.heuristics.SimpleBlockFusionProcessor;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.filters.simple.MinWordsFilter;

public final class KeepEverythingWithMinKWordsExtractor extends ExtractorBase
{
    private final MinWordsFilter filter;
    
    public KeepEverythingWithMinKWordsExtractor(final int kMin) {
        this.filter = new MinWordsFilter(kMin);
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return SimpleBlockFusionProcessor.INSTANCE.process(doc) | MarkEverythingContentFilter.INSTANCE.process(doc) | this.filter.process(doc);
    }
}
