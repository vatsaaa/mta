// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.filters.simple.MarkEverythingContentFilter;
import de.l3s.boilerpipe.document.TextDocument;

public final class KeepEverythingExtractor extends ExtractorBase
{
    public static final KeepEverythingExtractor INSTANCE;
    
    private KeepEverythingExtractor() {
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return MarkEverythingContentFilter.INSTANCE.process(doc);
    }
    
    static {
        INSTANCE = new KeepEverythingExtractor();
    }
}
