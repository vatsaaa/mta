// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.filters.english.KeepLargestFulltextBlockFilter;
import de.l3s.boilerpipe.filters.heuristics.BlockProximityFusion;
import de.l3s.boilerpipe.filters.english.NumWordsRulesClassifier;
import de.l3s.boilerpipe.document.TextDocument;

public final class LargestContentExtractor extends ExtractorBase
{
    public static final LargestContentExtractor INSTANCE;
    
    private LargestContentExtractor() {
    }
    
    public static LargestContentExtractor getInstance() {
        return LargestContentExtractor.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return NumWordsRulesClassifier.INSTANCE.process(doc) | BlockProximityFusion.MAX_DISTANCE_1.process(doc) | KeepLargestFulltextBlockFilter.INSTANCE.process(doc);
    }
    
    static {
        INSTANCE = new LargestContentExtractor();
    }
}
