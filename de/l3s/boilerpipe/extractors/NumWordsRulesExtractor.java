// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.filters.english.NumWordsRulesClassifier;
import de.l3s.boilerpipe.document.TextDocument;

public class NumWordsRulesExtractor extends ExtractorBase
{
    public static final NumWordsRulesExtractor INSTANCE;
    
    public static NumWordsRulesExtractor getInstance() {
        return NumWordsRulesExtractor.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return NumWordsRulesClassifier.INSTANCE.process(doc);
    }
    
    static {
        INSTANCE = new NumWordsRulesExtractor();
    }
}
