// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.document.TextDocument;
import java.io.IOException;
import de.l3s.boilerpipe.sax.HTMLFetcher;
import java.net.URL;
import org.xml.sax.SAXException;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.sax.BoilerpipeSAXInput;
import java.io.Reader;
import org.xml.sax.InputSource;
import java.io.StringReader;
import de.l3s.boilerpipe.BoilerpipeExtractor;

public abstract class ExtractorBase implements BoilerpipeExtractor
{
    public String getText(final String html) throws BoilerpipeProcessingException {
        try {
            return this.getText(new BoilerpipeSAXInput(new InputSource(new StringReader(html))).getTextDocument());
        }
        catch (SAXException e) {
            throw new BoilerpipeProcessingException(e);
        }
    }
    
    public String getText(final InputSource is) throws BoilerpipeProcessingException {
        try {
            return this.getText(new BoilerpipeSAXInput(is).getTextDocument());
        }
        catch (SAXException e) {
            throw new BoilerpipeProcessingException(e);
        }
    }
    
    public String getText(final URL url) throws BoilerpipeProcessingException {
        try {
            return this.getText(HTMLFetcher.fetch(url).toInputSource());
        }
        catch (IOException e) {
            throw new BoilerpipeProcessingException(e);
        }
    }
    
    public String getText(final Reader r) throws BoilerpipeProcessingException {
        return this.getText(new InputSource(r));
    }
    
    public String getText(final TextDocument doc) throws BoilerpipeProcessingException {
        this.process(doc);
        return doc.getContent();
    }
}
