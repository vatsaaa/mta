// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.filters.simple.MinClauseWordsFilter;
import de.l3s.boilerpipe.filters.simple.SplitParagraphBlocksFilter;
import de.l3s.boilerpipe.document.TextDocument;

public final class ArticleSentencesExtractor extends ExtractorBase
{
    public static final ArticleSentencesExtractor INSTANCE;
    
    public static ArticleSentencesExtractor getInstance() {
        return ArticleSentencesExtractor.INSTANCE;
    }
    
    public boolean process(final TextDocument doc) throws BoilerpipeProcessingException {
        return ArticleExtractor.INSTANCE.process(doc) | SplitParagraphBlocksFilter.INSTANCE.process(doc) | MinClauseWordsFilter.INSTANCE.process(doc);
    }
    
    static {
        INSTANCE = new ArticleSentencesExtractor();
    }
}
