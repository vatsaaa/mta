// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.extractors;

public final class CommonExtractors
{
    public static final ArticleExtractor ARTICLE_EXTRACTOR;
    public static final DefaultExtractor DEFAULT_EXTRACTOR;
    public static final CanolaExtractor CANOLA_EXTRACTOR;
    public static final KeepEverythingExtractor KEEP_EVERYTHING_EXTRACTOR;
    
    private CommonExtractors() {
    }
    
    static {
        ARTICLE_EXTRACTOR = ArticleExtractor.INSTANCE;
        DEFAULT_EXTRACTOR = DefaultExtractor.INSTANCE;
        CANOLA_EXTRACTOR = CanolaExtractor.INSTANCE;
        KEEP_EVERYTHING_EXTRACTOR = KeepEverythingExtractor.INSTANCE;
    }
}
