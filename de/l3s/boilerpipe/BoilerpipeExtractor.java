// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe;

import de.l3s.boilerpipe.document.TextDocument;
import java.io.Reader;
import org.xml.sax.InputSource;

public interface BoilerpipeExtractor extends BoilerpipeFilter
{
    String getText(final String p0) throws BoilerpipeProcessingException;
    
    String getText(final InputSource p0) throws BoilerpipeProcessingException;
    
    String getText(final Reader p0) throws BoilerpipeProcessingException;
    
    String getText(final TextDocument p0) throws BoilerpipeProcessingException;
}
