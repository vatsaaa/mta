// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe;

public class BoilerpipeProcessingException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public BoilerpipeProcessingException() {
    }
    
    public BoilerpipeProcessingException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public BoilerpipeProcessingException(final String message) {
        super(message);
    }
    
    public BoilerpipeProcessingException(final Throwable cause) {
        super(cause);
    }
}
