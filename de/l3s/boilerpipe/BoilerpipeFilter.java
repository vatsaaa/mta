// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe;

import de.l3s.boilerpipe.document.TextDocument;

public interface BoilerpipeFilter
{
    boolean process(final TextDocument p0) throws BoilerpipeProcessingException;
}
