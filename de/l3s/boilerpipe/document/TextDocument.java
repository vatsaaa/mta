// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.document;

import java.util.Iterator;
import java.util.List;

public class TextDocument
{
    final List<TextBlock> textBlocks;
    String title;
    
    public TextDocument(final List<TextBlock> textBlocks) {
        this(null, textBlocks);
    }
    
    public TextDocument(final String title, final List<TextBlock> textBlocks) {
        this.title = title;
        this.textBlocks = textBlocks;
    }
    
    public List<TextBlock> getTextBlocks() {
        return this.textBlocks;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public String getContent() {
        return this.getText(true, false);
    }
    
    public String getText(final boolean includeContent, final boolean includeNonContent) {
        final StringBuilder sb = new StringBuilder();
        for (final TextBlock block : this.getTextBlocks()) {
            if (block.isContent()) {
                if (!includeContent) {
                    continue;
                }
            }
            else if (!includeNonContent) {
                continue;
            }
            sb.append(block.getText());
            sb.append('\n');
        }
        return sb.toString();
    }
    
    public String debugString() {
        final StringBuilder sb = new StringBuilder();
        for (final TextBlock tb : this.getTextBlocks()) {
            sb.append(tb.toString());
            sb.append('\n');
        }
        return sb.toString();
    }
}
