// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.document;

import java.util.Iterator;

public final class TextDocumentStatistics
{
    private int numWords;
    private int numBlocks;
    
    public TextDocumentStatistics(final TextDocument doc, final boolean contentOnly) {
        this.numWords = 0;
        this.numBlocks = 0;
        for (final TextBlock tb : doc.getTextBlocks()) {
            if (contentOnly && !tb.isContent()) {
                continue;
            }
            this.numWords += tb.getNumWords();
            ++this.numBlocks;
        }
    }
    
    public float avgNumWords() {
        return this.numWords / (float)this.numBlocks;
    }
    
    public int getNumWords() {
        return this.numWords;
    }
}
