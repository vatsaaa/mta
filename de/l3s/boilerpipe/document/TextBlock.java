// 
// Decompiled by Procyon v0.5.36
// 

package de.l3s.boilerpipe.document;

import java.util.Collection;
import java.util.HashSet;
import java.util.BitSet;
import java.util.Set;

public class TextBlock implements Cloneable
{
    boolean isContent;
    private CharSequence text;
    Set<String> labels;
    int offsetBlocksStart;
    int offsetBlocksEnd;
    int numWords;
    int numWordsInAnchorText;
    int numWordsInWrappedLines;
    int numWrappedLines;
    float textDensity;
    float linkDensity;
    BitSet containedTextElements;
    private int numFullTextWords;
    private static final BitSet EMPTY_BITSET;
    public static final TextBlock EMPTY_START;
    public static final TextBlock EMPTY_END;
    
    public TextBlock(final String text) {
        this(text, null, 0, 0, 0, 0, 0);
    }
    
    public TextBlock(final String text, final BitSet containedTextElements, final int numWords, final int numWordsInAnchorText, final int numWordsInWrappedLines, final int numWrappedLines, final int offsetBlocks) {
        this.isContent = false;
        this.labels = null;
        this.numFullTextWords = 0;
        this.text = text;
        this.containedTextElements = containedTextElements;
        this.numWords = numWords;
        this.numWordsInAnchorText = numWordsInAnchorText;
        this.numWordsInWrappedLines = numWordsInWrappedLines;
        this.numWrappedLines = numWrappedLines;
        this.offsetBlocksStart = offsetBlocks;
        this.offsetBlocksEnd = offsetBlocks;
        this.initDensities();
    }
    
    public boolean isContent() {
        return this.isContent;
    }
    
    public boolean setIsContent(final boolean isContent) {
        if (isContent != this.isContent) {
            this.isContent = isContent;
            return true;
        }
        return false;
    }
    
    public String getText() {
        return this.text.toString();
    }
    
    public int getNumWords() {
        return this.numWords;
    }
    
    public int getNumWordsInAnchorText() {
        return this.numWordsInAnchorText;
    }
    
    public float getTextDensity() {
        return this.textDensity;
    }
    
    public float getLinkDensity() {
        return this.linkDensity;
    }
    
    public void mergeNext(final TextBlock other) {
        if (!(this.text instanceof StringBuilder)) {
            this.text = new StringBuilder(this.text);
        }
        final StringBuilder sb = (StringBuilder)this.text;
        sb.append('\n');
        sb.append(other.text);
        this.numWords += other.numWords;
        this.numWordsInAnchorText += other.numWordsInAnchorText;
        this.numWordsInWrappedLines += other.numWordsInWrappedLines;
        this.numWrappedLines += other.numWrappedLines;
        this.offsetBlocksStart = Math.min(this.offsetBlocksStart, other.offsetBlocksStart);
        this.offsetBlocksEnd = Math.max(this.offsetBlocksEnd, other.offsetBlocksEnd);
        this.initDensities();
        this.isContent |= other.isContent;
        if (this.containedTextElements == null) {
            this.containedTextElements = (BitSet)other.containedTextElements.clone();
        }
        else {
            this.containedTextElements.or(other.containedTextElements);
        }
        this.numFullTextWords += other.numFullTextWords;
        if (other.labels != null) {
            if (this.labels == null) {
                this.labels = new HashSet<String>(other.labels);
            }
            else {
                this.labels.addAll(other.labels);
            }
        }
    }
    
    private void initDensities() {
        if (this.numWordsInWrappedLines == 0) {
            this.numWordsInWrappedLines = this.numWords;
            this.numWrappedLines = 1;
        }
        this.textDensity = this.numWordsInWrappedLines / (float)this.numWrappedLines;
        this.linkDensity = ((this.numWords == 0) ? 0.0f : (this.numWordsInAnchorText / (float)this.numWords));
    }
    
    public int getOffsetBlocksStart() {
        return this.offsetBlocksStart;
    }
    
    public int getOffsetBlocksEnd() {
        return this.offsetBlocksEnd;
    }
    
    @Override
    public String toString() {
        return "[" + this.offsetBlocksStart + "-" + this.offsetBlocksEnd + "; nw=" + this.numWords + ";nwl=" + this.numWrappedLines + ";ld=" + this.linkDensity + "]\t" + (this.isContent ? "CONTENT" : "boilerplate") + "," + this.labels + "\t" + this.getText();
    }
    
    public void addLabel(final String label) {
        if (this.labels == null) {
            this.labels = new HashSet<String>(2);
        }
        this.labels.add(label);
    }
    
    public boolean hasLabel(final String label) {
        return this.labels != null && this.labels.contains(label);
    }
    
    public Set<String> getLabels() {
        return this.labels;
    }
    
    public void addLabels(final Set<String> l) {
        if (l == null) {
            return;
        }
        if (this.labels == null) {
            this.labels = new HashSet<String>(l);
        }
        else {
            this.labels.addAll(l);
        }
    }
    
    public void addLabels(final String... l) {
        if (l == null) {
            return;
        }
        if (this.labels == null) {
            this.labels = new HashSet<String>();
        }
        for (final String label : l) {
            this.labels.add(label);
        }
    }
    
    public BitSet getContainedTextElements() {
        return this.containedTextElements;
    }
    
    @Override
    protected Object clone() {
        TextBlock clone;
        try {
            clone = (TextBlock)super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        if (this.text != null && !(this.text instanceof String)) {
            clone.text = new StringBuilder(this.text);
        }
        if (this.labels != null && !this.labels.isEmpty()) {
            clone.labels = new HashSet<String>(this.labels);
        }
        if (this.containedTextElements != null) {
            clone.containedTextElements = (BitSet)this.containedTextElements.clone();
        }
        return clone;
    }
    
    static {
        EMPTY_BITSET = new BitSet();
        EMPTY_START = new TextBlock("", TextBlock.EMPTY_BITSET, 0, 0, 0, 0, -1);
        EMPTY_END = new TextBlock("", TextBlock.EMPTY_BITSET, 0, 0, 0, 0, Integer.MAX_VALUE);
    }
}
