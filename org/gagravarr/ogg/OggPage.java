// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.util.Iterator;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;

public class OggPage
{
    private int sid;
    private int seqNum;
    private long checksum;
    private long granulePosition;
    private boolean isBOS;
    private boolean isEOS;
    private boolean isContinue;
    private int numLVs;
    private byte[] lvs;
    private byte[] data;
    private ByteArrayOutputStream tmpData;
    
    protected OggPage(final int sid, final int seqNum) {
        this.numLVs = 0;
        this.lvs = new byte[255];
        this.sid = sid;
        this.seqNum = seqNum;
        this.tmpData = new ByteArrayOutputStream();
    }
    
    protected OggPage(final InputStream inp) throws IOException {
        this.numLVs = 0;
        this.lvs = new byte[255];
        final int version = inp.read();
        if (version != 0) {
            throw new IllegalArgumentException("Found Ogg page in format " + version + " but we only support version 0");
        }
        final int flags = inp.read();
        if ((flags & 0x1) == 0x1) {
            this.isContinue = true;
        }
        if ((flags & 0x2) == 0x2) {
            this.isBOS = true;
        }
        if ((flags & 0x4) == 0x4) {
            this.isEOS = true;
        }
        this.granulePosition = IOUtils.getInt(inp.read(), inp.read(), inp.read(), inp.read(), inp.read(), inp.read(), inp.read(), inp.read());
        this.sid = (int)IOUtils.getInt(inp.read(), inp.read(), inp.read(), inp.read());
        this.seqNum = (int)IOUtils.getInt(inp.read(), inp.read(), inp.read(), inp.read());
        this.checksum = IOUtils.getInt(inp.read(), inp.read(), inp.read(), inp.read());
        this.numLVs = inp.read();
        IOUtils.readFully(inp, this.lvs = new byte[this.numLVs]);
        IOUtils.readFully(inp, this.data = new byte[this.getDataSize()]);
    }
    
    protected int addPacket(final OggPacket packet, int offset) {
        if (packet.isBeginningOfStream()) {
            this.isBOS = true;
        }
        if (packet.isEndOfStream()) {
            this.isEOS = true;
        }
        final int size = packet.getData().length;
        for (int i = this.numLVs; i < 255; ++i) {
            final int remains = size - offset;
            int toAdd = 255;
            if (remains < 255) {
                toAdd = remains;
            }
            this.lvs[i] = IOUtils.fromInt(toAdd);
            this.tmpData.write(packet.getData(), offset, toAdd);
            ++this.numLVs;
            offset += toAdd;
            if (toAdd < 255) {
                break;
            }
        }
        return offset;
    }
    
    public boolean isChecksumValid() {
        if (this.checksum == 0L) {
            return true;
        }
        int crc = CRCUtils.getCRC(this.getHeader());
        if (this.data != null && this.data.length > 0) {
            crc = CRCUtils.getCRC(this.data, crc);
        }
        return this.checksum == crc;
    }
    
    protected long getChecksum() {
        return this.checksum;
    }
    
    protected boolean hasSpaceFor(final int bytes) {
        final int reqLVs = (int)Math.ceil(bytes / 255.0);
        return this.numLVs + reqLVs <= 255;
    }
    
    public int getPageSize() {
        int size = 27 + this.numLVs;
        size += this.getDataSize();
        return size;
    }
    
    public int getDataSize() {
        int size = 0;
        for (int i = 0; i < this.numLVs; ++i) {
            size += IOUtils.toInt(this.lvs[i]);
        }
        return size;
    }
    
    public int getSid() {
        return this.sid;
    }
    
    public int getSequenceNumber() {
        return this.seqNum;
    }
    
    public long getGranulePosition() {
        return this.granulePosition;
    }
    
    public byte[] getData() {
        if (this.tmpData != null && (this.data == null || this.tmpData.size() != this.data.length)) {
            this.data = this.tmpData.toByteArray();
        }
        return this.data;
    }
    
    protected void setGranulePosition(final long position) {
        this.granulePosition = position;
    }
    
    public boolean hasContinuation() {
        return this.numLVs != 0 && IOUtils.toInt(this.lvs[this.numLVs - 1]) == 255;
    }
    
    public boolean isContinuation() {
        return this.isContinue;
    }
    
    protected void setIsContinuation() {
        this.isContinue = true;
    }
    
    protected void setIsEOS() {
        this.isEOS = true;
    }
    
    protected int getNumLVs() {
        return this.numLVs;
    }
    
    public void writeHeader(final OutputStream out) throws IOException {
        final byte[] header = this.getHeader();
        this.getData();
        int crc = CRCUtils.getCRC(header);
        if (this.data != null && this.data.length > 0) {
            crc = CRCUtils.getCRC(this.data, crc);
        }
        IOUtils.putInt4(header, 22, crc);
        this.checksum = crc;
        out.write(header);
    }
    
    protected byte[] getHeader() {
        final byte[] header = new byte[27 + this.numLVs];
        header[0] = 79;
        header[2] = (header[1] = 103);
        header[3] = 83;
        header[4] = 0;
        byte flags = 0;
        if (this.isContinue) {
            ++flags;
        }
        if (this.isBOS) {
            flags += 2;
        }
        if (this.isEOS) {
            flags += 4;
        }
        header[5] = flags;
        IOUtils.putInt8(header, 6, this.granulePosition);
        IOUtils.putInt4(header, 14, this.sid);
        IOUtils.putInt4(header, 18, this.seqNum);
        header[26] = IOUtils.fromInt(this.numLVs);
        System.arraycopy(this.lvs, 0, header, 27, this.numLVs);
        return header;
    }
    
    @Override
    public String toString() {
        return "Ogg Page - " + this.getSid() + " @ " + this.getSequenceNumber() + " - " + this.numLVs + " LVs";
    }
    
    public OggPacketIterator getPacketIterator() {
        return new OggPacketIterator((OggPacketData)null);
    }
    
    public OggPacketIterator getPacketIterator(final OggPacketData previousPart) {
        return new OggPacketIterator(previousPart);
    }
    
    protected class OggPacketIterator implements Iterator<OggPacketData>
    {
        private OggPacketData prevPart;
        private int currentLV;
        private int currentOffset;
        
        private OggPacketIterator(final OggPacketData previousPart) {
            this.currentLV = 0;
            this.currentOffset = 0;
            this.prevPart = previousPart;
        }
        
        public boolean hasNext() {
            return this.currentLV < OggPage.this.numLVs || (this.currentLV == 0 && OggPage.this.numLVs == 0);
        }
        
        public OggPacketData next() {
            boolean continues = false;
            int packetLVs = 0;
            int packetSize = 0;
            for (int i = this.currentLV; i < OggPage.this.numLVs; ++i) {
                final int size = IOUtils.toInt(OggPage.this.lvs[i]);
                packetSize += size;
                ++packetLVs;
                if (size < 255) {
                    break;
                }
                if (i == OggPage.this.numLVs - 1 && size == 255) {
                    continues = true;
                }
            }
            byte[] pd = new byte[packetSize];
            for (int j = this.currentLV; j < this.currentLV + packetLVs; ++j) {
                final int size2 = IOUtils.toInt(OggPage.this.lvs[j]);
                final int offset = (j - this.currentLV) * 255;
                System.arraycopy(OggPage.this.data, this.currentOffset + offset, pd, offset, size2);
            }
            if (this.prevPart != null) {
                final int prevSize = this.prevPart.getData().length;
                final byte[] fpd = new byte[prevSize + pd.length];
                System.arraycopy(this.prevPart.getData(), 0, fpd, 0, prevSize);
                System.arraycopy(pd, 0, fpd, prevSize, pd.length);
                this.prevPart = null;
                pd = fpd;
            }
            OggPacketData packet;
            if (continues) {
                packet = new OggPacketData(pd);
            }
            else {
                boolean packetBOS = false;
                boolean packetEOS = false;
                if (OggPage.this.isBOS && this.currentLV == 0) {
                    packetBOS = true;
                }
                if (OggPage.this.isEOS && this.currentLV + packetLVs == OggPage.this.numLVs) {
                    packetEOS = true;
                }
                packet = new OggPacket(OggPage.this, pd, packetBOS, packetEOS);
            }
            this.currentLV += packetLVs;
            this.currentOffset += packetSize;
            if (this.currentLV == 0) {
                this.currentLV = 1;
            }
            return packet;
        }
        
        public void remove() {
            throw new IllegalStateException("Remove not supported");
        }
    }
}
