// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

public abstract class HighLevelOggStreamPacket
{
    private OggPacket oggPacket;
    private byte[] data;
    
    protected HighLevelOggStreamPacket(final OggPacket oggPacket) {
        this.oggPacket = oggPacket;
    }
    
    protected HighLevelOggStreamPacket() {
        this.oggPacket = null;
    }
    
    protected OggPacket getOggPacket() {
        return this.oggPacket;
    }
    
    public byte[] getData() {
        if (this.data != null) {
            return this.data;
        }
        if (this.oggPacket != null) {
            return this.oggPacket.getData();
        }
        return null;
    }
    
    public void setData(final byte[] data) {
        this.data = data;
    }
    
    public OggPacket write() {
        return this.oggPacket = new OggPacket(this.getData());
    }
}
