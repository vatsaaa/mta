// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestReadWrite extends TestCase
{
    private InputStream getTestFile() throws IOException {
        return this.getClass().getResourceAsStream("/testVORBIS.ogg");
    }
    
    private static long[] copy(final OggFile in, final OggFile out) throws IOException {
        final OggPacketReader r = in.getPacketReader();
        final ArrayList<Long> copyCRCs = new ArrayList<Long>();
        OggPacket p = null;
        OggPacket pp = null;
        OggPacketWriter w = null;
        while ((p = r.getNextPacket()) != null) {
            if (w == null) {
                w = out.getPacketWriter(p.getSid());
                copyCRCs.add(p._getParent().getChecksum());
            }
            if (pp != null && pp.getSequenceNumber() != p.getSequenceNumber()) {
                w.flush();
                copyCRCs.add(p._getParent().getChecksum());
            }
            final long oldGranule = p.getGranulePosition();
            w.bufferPacket(p);
            w.setGranulePosition(oldGranule);
            pp = p;
        }
        w.close();
        out.close();
        final long[] ret = new long[copyCRCs.size()];
        for (int i = 0; i < ret.length; ++i) {
            ret[i] = copyCRCs.get(i);
        }
        return ret;
    }
    
    public void testReadWrite() throws IOException {
        final OggFile in = new OggFile(this.getTestFile());
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final OggFile out = new OggFile(baos);
        copy(in, out);
        out.close();
    }
    
    public void testReadWriteContents() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final OggFile out = new OggFile(baos);
        copy(new OggFile(this.getTestFile()), out);
        final InputStream inp = this.getTestFile();
        final byte[] original = new byte[4241];
        IOUtils.readFully(inp, original);
        assertEquals(-1, inp.read());
        final byte[] readwrite = baos.toByteArray();
        assertEquals(original.length, readwrite.length);
        for (int i = 0; i < original.length; ++i) {
            assertEquals(original[i], readwrite[i]);
        }
    }
    
    public void testReadWriteRead() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final OggFile out = new OggFile(baos);
        final long[] crcs = copy(new OggFile(this.getTestFile()), out);
        final OggFile in = new OggFile(new ByteArrayInputStream(baos.toByteArray()));
        final OggPacketReader r = in.getPacketReader();
        OggPacket p;
        while ((p = r.getNextPacket()) != null) {
            final int page = p._getParent().getSequenceNumber();
            assertEquals(crcs[page], p._getParent().getChecksum());
        }
    }
    
    public void testPackets() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final OggFile out = new OggFile(baos);
        copy(new OggFile(this.getTestFile()), out);
        final OggFile ogg = new OggFile(new ByteArrayInputStream(baos.toByteArray()));
        final OggPacketReader r = ogg.getPacketReader();
        OggPacket p = r.getNextPacket();
        assertEquals(30, p.getData().length);
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(0, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(219, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(1, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(3484, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(1, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(35, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(28, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(30, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(52, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(51, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(69, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(59, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(40, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(38, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals((Object)null, (Object)p);
    }
}
