// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestReadBoundaries extends TestCase
{
    private InputStream getTestFile() throws IOException {
        return this.getClass().getResourceAsStream("/testBoundaries.ogg");
    }
    
    public static byte[] getBytes(final int len) {
        final byte[] b = new byte[len];
        for (int i = 0; i < len; ++i) {
            b[i] = IOUtils.fromInt(i % 256);
        }
        return b;
    }
    
    private static void assertEquals(final byte[] a, final byte[] b) {
        assertNotNull((Object)a);
        assertNotNull((Object)b);
        assertEquals(a.length, b.length);
        for (int i = 0; i < a.length; ++i) {
            assertEquals(a[i], b[i]);
        }
    }
    
    private void doTest(final OggPacketReader r) throws IOException {
        OggPacket p = r.getNextPacket();
        assertEquals(4660, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(0, p.getSequenceNumber());
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(6, p.getData().length);
        assertEquals(new byte[] { 0, 1, 2, 3, 4, 5 }, p.getData());
        p = r.getNextPacket();
        assertEquals(4660, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(1, p.getSequenceNumber());
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(6, p.getData().length);
        assertEquals(new byte[] { 0, 1, 2, 3, 4, 5 }, p.getData());
        p = r.getNextPacket();
        assertEquals(4660, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(1, p.getSequenceNumber());
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(6, p.getData().length);
        assertEquals(new byte[] { 10, 11, 12, 13, 14, 15 }, p.getData());
        p = r.getNextPacket();
        assertEquals(4660, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(1028, p.getData().length);
        assertEquals(getBytes(1028), p.getData());
    }
    
    public void testPages() throws IOException {
        final InputStream i = this.getTestFile();
        i.read();
        i.read();
        i.read();
        i.read();
        OggPage p = new OggPage(i);
        assertEquals(1, p.getNumLVs());
        assertEquals(false, p.isContinuation());
        assertEquals(false, p.hasContinuation());
        OggPage.OggPacketIterator it = p.getPacketIterator();
        assertEquals(true, it.hasNext());
        OggPacketData d = it.next();
        assertEquals(6, d.getData().length);
        assertEquals(false, it.hasNext());
        i.read();
        i.read();
        i.read();
        i.read();
        p = new OggPage(i);
        assertEquals(5, p.getNumLVs());
        assertEquals(false, p.isContinuation());
        assertEquals(true, p.hasContinuation());
        it = p.getPacketIterator();
        assertEquals(true, it.hasNext());
        d = it.next();
        assertEquals(6, d.getData().length);
        assertEquals(true, it.hasNext());
        d = it.next();
        assertEquals(6, d.getData().length);
        assertEquals(true, it.hasNext());
        d = it.next();
        assertEquals(765, d.getData().length);
        assertEquals(false, it.hasNext());
        i.read();
        i.read();
        i.read();
        i.read();
        p = new OggPage(i);
        assertEquals(2, p.getNumLVs());
        assertEquals(true, p.isContinuation());
        assertEquals(false, p.hasContinuation());
        it = p.getPacketIterator();
        assertEquals(true, it.hasNext());
        d = it.next();
        assertEquals(263, d.getData().length);
        assertEquals(false, it.hasNext());
    }
    
    public void testProcessRaw() throws IOException {
        final OggPacketReader r = new OggPacketReader(this.getTestFile());
        this.doTest(r);
    }
    
    public void testProcessOggFile() throws IOException {
        final OggFile ogg = new OggFile(this.getTestFile());
        final OggPacketReader r = ogg.getPacketReader();
        this.doTest(r);
    }
}
