// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.IOException;
import java.util.Iterator;
import java.util.ArrayList;

public class OggPacketWriter
{
    private boolean closed;
    private boolean doneFirstPacket;
    private OggFile file;
    private int sid;
    private int sequenceNumber;
    private long currentGranulePosition;
    private ArrayList<OggPage> buffer;
    
    protected OggPacketWriter(final OggFile parentFile, final int sid) {
        this.closed = false;
        this.doneFirstPacket = false;
        this.currentGranulePosition = 0L;
        this.buffer = new ArrayList<OggPage>();
        this.file = parentFile;
        this.sid = sid;
        this.sequenceNumber = 0;
    }
    
    public void setGranulePosition(final long position) {
        this.currentGranulePosition = position;
        for (final OggPage p : this.buffer) {
            p.setGranulePosition(position);
        }
    }
    
    public long getCurrentGranulePosition() {
        return this.currentGranulePosition;
    }
    
    public int getSid() {
        return this.sid;
    }
    
    private OggPage getCurrentPage(final boolean forceNew) {
        if (this.buffer.size() == 0 || forceNew) {
            final OggPage page = new OggPage(this.sid, this.sequenceNumber++);
            if (this.currentGranulePosition > 0L) {
                page.setGranulePosition(this.currentGranulePosition);
            }
            this.buffer.add(page);
            return page;
        }
        return this.buffer.get(this.buffer.size() - 1);
    }
    
    public void bufferPacket(final OggPacket packet) {
        if (this.closed) {
            throw new IllegalStateException("Can't buffer packets on a closed stream!");
        }
        if (!this.doneFirstPacket) {
            packet.setIsBOS();
            this.doneFirstPacket = true;
        }
        final int size = packet.getData().length;
        boolean emptyPacket = size == 0;
        OggPage page = this.getCurrentPage(false);
        for (int pos = 0; pos < size || emptyPacket; emptyPacket = false) {
            pos = page.addPacket(packet, pos);
            if (pos < size) {
                page = this.getCurrentPage(true);
                page.setIsContinuation();
            }
        }
        packet.setParent(page);
    }
    
    public void bufferPacket(final OggPacket packet, final boolean flush) throws IOException {
        this.bufferPacket(packet);
        if (flush) {
            this.flush();
        }
    }
    
    public int getSizePendingFlush() {
        int size = 0;
        for (final OggPage p : this.buffer) {
            size += p.getDataSize();
        }
        return size;
    }
    
    public void flush() throws IOException {
        if (this.closed) {
            throw new IllegalStateException("Can't flush packets on a closed stream!");
        }
        final OggPage[] pages = this.buffer.toArray(new OggPage[this.buffer.size()]);
        this.file.writePages(pages);
        this.buffer.clear();
    }
    
    public void close() throws IOException {
        if (this.buffer.size() > 0) {
            this.buffer.get(this.buffer.size() - 1).setIsEOS();
        }
        else {
            final OggPacket p = new OggPacket(new byte[0]);
            p.setIsEOS();
            this.bufferPacket(p);
        }
        this.flush();
        this.closed = true;
    }
}
