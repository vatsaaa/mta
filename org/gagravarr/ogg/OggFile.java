// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.io.OutputStream;
import java.io.InputStream;

public class OggFile
{
    private InputStream inp;
    private OutputStream out;
    private boolean writing;
    private Set<Integer> seenSIDs;
    
    public OggFile(final OutputStream output) {
        this.writing = true;
        this.seenSIDs = new HashSet<Integer>();
        this.out = output;
        this.writing = true;
    }
    
    public OggFile(final InputStream input) {
        this.writing = true;
        this.seenSIDs = new HashSet<Integer>();
        this.inp = input;
        this.writing = false;
    }
    
    public OggFile(final InputStream input, final OggStreamListener listener) throws IOException {
        this(input);
        final Map<Integer, OggStreamReader[]> readers = new HashMap<Integer, OggStreamReader[]>();
        final OggPacketReader reader = this.getPacketReader();
        OggPacket packet = null;
        while ((packet = reader.getNextPacket()) != null) {
            if (packet.isBeginningOfStream()) {
                final OggStreamReader[] streams = listener.processNewStream(packet.getSid(), packet.getData());
                if (streams != null && streams.length > 0) {
                    readers.put(packet.getSid(), streams);
                }
            }
            else {
                final OggStreamReader[] streams = readers.get(packet.getSid());
                if (streams != null) {
                    for (final OggStreamReader r : streams) {
                        r.processPacket(packet);
                    }
                }
            }
            if (packet.isEndOfStream()) {
                listener.processStreamEnd(packet.getSid());
            }
        }
    }
    
    public void close() throws IOException {
        if (this.inp != null) {
            this.inp.close();
        }
        if (this.out != null) {
            this.out.close();
        }
    }
    
    public OggPacketReader getPacketReader() {
        if (this.writing || this.inp == null) {
            throw new IllegalStateException("Can only read from a file opened with an InputStream");
        }
        return new OggPacketReader(this.inp);
    }
    
    public OggPacketWriter getPacketWriter() {
        return this.getPacketWriter(this.getUnusedSerialNumber());
    }
    
    public OggPacketWriter getPacketWriter(final int sid) {
        if (!this.writing) {
            throw new IllegalStateException("Can only write to a file opened with an OutputStream");
        }
        this.seenSIDs.add(sid);
        return new OggPacketWriter(this, sid);
    }
    
    protected synchronized void writePages(final OggPage[] pages) throws IOException {
        for (final OggPage page : pages) {
            page.writeHeader(this.out);
            this.out.write(page.getData());
        }
        this.out.flush();
    }
    
    protected int getUnusedSerialNumber() {
        int sid;
        do {
            sid = (int)(Math.random() * 32767.0);
        } while (this.seenSIDs.contains(sid));
        return sid;
    }
}
