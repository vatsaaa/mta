// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestReadSkipping extends TestCase
{
    private InputStream getTestFile() throws IOException {
        return this.getClass().getResourceAsStream("/testVORBIS.ogg");
    }
    
    public void testSkipToSequence() throws Exception {
        OggFile ogg = new OggFile(this.getTestFile());
        OggPacketReader r = ogg.getPacketReader();
        r.skipToSequenceNumber(-1, 0);
        assertEquals((Object)null, (Object)r.getNextPacket());
        ogg = new OggFile(this.getTestFile());
        r = ogg.getPacketReader();
        r.skipToSequenceNumber(74691676, 100);
        assertEquals((Object)null, (Object)r.getNextPacket());
        ogg = new OggFile(this.getTestFile());
        r = ogg.getPacketReader();
        r.skipToSequenceNumber(74691676, 0);
        OggPacket p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(0, p.getSequenceNumber());
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(1, p.getSequenceNumber());
        ogg = new OggFile(this.getTestFile());
        r = ogg.getPacketReader();
        r.skipToSequenceNumber(74691676, 2);
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(2, p.getSequenceNumber());
    }
    
    public void testSkipToGranuleExact() throws Exception {
        OggFile ogg = new OggFile(this.getTestFile());
        OggPacketReader r = ogg.getPacketReader();
        r.skipToGranulePosition(-1, 0L);
        assertEquals((Object)null, (Object)r.getNextPacket());
        ogg = new OggFile(this.getTestFile());
        r = ogg.getPacketReader();
        r.skipToGranulePosition(74691676, 974010L);
        assertEquals((Object)null, (Object)r.getNextPacket());
        ogg = new OggFile(this.getTestFile());
        r = ogg.getPacketReader();
        r.skipToGranulePosition(74691676, 0L);
        OggPacket p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(0, p.getSequenceNumber());
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(1, p.getSequenceNumber());
        ogg = new OggFile(this.getTestFile());
        r = ogg.getPacketReader();
        r.skipToGranulePosition(74691676, 960L);
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        ogg = new OggFile(this.getTestFile());
        r = ogg.getPacketReader();
        r.skipToGranulePosition(74691676, 801L);
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
    }
}
