// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

public class OggPacketData
{
    private byte[] data;
    
    protected OggPacketData(final byte[] data) {
        this.data = data;
    }
    
    public byte[] getData() {
        return this.data;
    }
}
