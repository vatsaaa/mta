// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.UnsupportedEncodingException;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;

public class IOUtils
{
    public static void readFully(final InputStream inp, final byte[] destination) throws IOException {
        readFully(inp, destination, 0, destination.length);
    }
    
    public static void readFully(final InputStream inp, final byte[] destination, final int offset, final int length) throws IOException {
        int r;
        for (int read = 0; read < length; read += r) {
            r = inp.read(destination, offset + read, length - read);
            if (r == -1) {
                throw new IOException("Asked to read " + length + " bytes from " + offset + " but hit EoF at " + read);
            }
        }
    }
    
    public static int toInt(final byte b) {
        if (b < 0) {
            return b & 0xFF;
        }
        return b;
    }
    
    public static byte fromInt(final int i) {
        if (i > 256) {
            throw new IllegalArgumentException("Number " + i + " too big");
        }
        if (i > 127) {
            return (byte)(i - 256);
        }
        return (byte)i;
    }
    
    public static int getInt2(final byte[] data) {
        return getInt2(data, 0);
    }
    
    public static int getInt2(final byte[] data, final int offset) {
        int i = offset;
        final int b0 = data[i++] & 0xFF;
        final int b2 = data[i++] & 0xFF;
        return getInt(b0, b2);
    }
    
    public static long getInt3(final byte[] data) {
        return getInt3(data, 0);
    }
    
    public static long getInt3(final byte[] data, final int offset) {
        int i = offset;
        final int b0 = data[i++] & 0xFF;
        final int b2 = data[i++] & 0xFF;
        final int b3 = data[i++] & 0xFF;
        return getInt(b0, b2, b3);
    }
    
    public static long getInt4(final byte[] data) {
        return getInt4(data, 0);
    }
    
    public static long getInt4(final byte[] data, final int offset) {
        int i = offset;
        final int b0 = data[i++] & 0xFF;
        final int b2 = data[i++] & 0xFF;
        final int b3 = data[i++] & 0xFF;
        final int b4 = data[i++] & 0xFF;
        return getInt(b0, b2, b3, b4);
    }
    
    public static long getInt8(final byte[] data) {
        return getInt8(data, 0);
    }
    
    public static long getInt8(final byte[] data, final int offset) {
        int i = offset;
        final int b0 = data[i++] & 0xFF;
        final int b2 = data[i++] & 0xFF;
        final int b3 = data[i++] & 0xFF;
        final int b4 = data[i++] & 0xFF;
        final int b5 = data[i++] & 0xFF;
        final int b6 = data[i++] & 0xFF;
        final int b7 = data[i++] & 0xFF;
        final int b8 = data[i++] & 0xFF;
        return getInt(b0, b2, b3, b4, b5, b6, b7, b8);
    }
    
    public static int getInt(final int i0, final int i1) {
        return (i1 << 8) + (i0 << 0);
    }
    
    public static long getInt(final int i0, final int i1, final int i2) {
        return (i2 << 16) + (i1 << 8) + (i0 << 0);
    }
    
    public static long getInt(final int i0, final int i1, final int i2, final int i3) {
        return (i3 << 24) + (i2 << 16) + (i1 << 8) + (i0 << 0);
    }
    
    public static long getInt(final int i0, final int i1, final int i2, final int i3, final int i4, final int i5, final int i6, final int i7) {
        return (i7 << 56) + (long)(i6 << 48) + (i5 << 40) + (i4 << 32) + (i3 << 24) + (i2 << 16) + (i1 << 8) + (i0 << 0);
    }
    
    public static int getInt2BE(final byte[] data) {
        return getInt2BE(data, 0);
    }
    
    public static int getInt2BE(final byte[] data, final int offset) {
        int i = offset;
        final int b0 = data[i++] & 0xFF;
        final int b2 = data[i++] & 0xFF;
        return getIntBE(b0, b2);
    }
    
    public static long getInt3BE(final byte[] data) {
        return getInt3BE(data, 0);
    }
    
    public static long getInt3BE(final byte[] data, final int offset) {
        int i = offset;
        final int b0 = data[i++] & 0xFF;
        final int b2 = data[i++] & 0xFF;
        final int b3 = data[i++] & 0xFF;
        return getIntBE(b0, b2, b3);
    }
    
    public static long getInt4BE(final byte[] data) {
        return getInt4BE(data, 0);
    }
    
    public static long getInt4BE(final byte[] data, final int offset) {
        int i = offset;
        final int b0 = data[i++] & 0xFF;
        final int b2 = data[i++] & 0xFF;
        final int b3 = data[i++] & 0xFF;
        final int b4 = data[i++] & 0xFF;
        return getIntBE(b0, b2, b3, b4);
    }
    
    public static int getIntBE(final int i0, final int i1) {
        return (i0 << 8) + (i1 << 0);
    }
    
    public static long getIntBE(final int i0, final int i1, final int i2) {
        return (i0 << 16) + (i1 << 8) + (i2 << 0);
    }
    
    public static long getIntBE(final int i0, final int i1, final int i2, final int i3) {
        return (i0 << 24) + (i1 << 16) + (i2 << 8) + (i3 << 0);
    }
    
    public static void writeInt2(final OutputStream out, final int v) throws IOException {
        final byte[] b2 = new byte[2];
        putInt2(b2, 0, v);
        out.write(b2);
    }
    
    public static void putInt2(final byte[] data, final int offset, final int v) {
        int i = offset;
        data[i++] = (byte)(v >>> 0 & 0xFF);
        data[i++] = (byte)(v >>> 8 & 0xFF);
    }
    
    public static void writeInt3(final OutputStream out, final long v) throws IOException {
        final byte[] b3 = new byte[3];
        putInt3(b3, 0, v);
        out.write(b3);
    }
    
    public static void putInt3(final byte[] data, final int offset, final long v) {
        int i = offset;
        data[i++] = (byte)(v >>> 0 & 0xFFL);
        data[i++] = (byte)(v >>> 8 & 0xFFL);
        data[i++] = (byte)(v >>> 16 & 0xFFL);
    }
    
    public static void writeInt4(final OutputStream out, final long v) throws IOException {
        final byte[] b4 = new byte[4];
        putInt4(b4, 0, v);
        out.write(b4);
    }
    
    public static void putInt4(final byte[] data, final int offset, final long v) {
        int i = offset;
        data[i++] = (byte)(v >>> 0 & 0xFFL);
        data[i++] = (byte)(v >>> 8 & 0xFFL);
        data[i++] = (byte)(v >>> 16 & 0xFFL);
        data[i++] = (byte)(v >>> 24 & 0xFFL);
    }
    
    public static void writeInt8(final OutputStream out, final long v) throws IOException {
        final byte[] b8 = new byte[8];
        putInt8(b8, 0, v);
        out.write(b8);
    }
    
    public static void putInt8(final byte[] data, final int offset, final long v) {
        int i = offset;
        data[i++] = (byte)(v >>> 0 & 0xFFL);
        data[i++] = (byte)(v >>> 8 & 0xFFL);
        data[i++] = (byte)(v >>> 16 & 0xFFL);
        data[i++] = (byte)(v >>> 24 & 0xFFL);
        data[i++] = (byte)(v >>> 32 & 0xFFL);
        data[i++] = (byte)(v >>> 40 & 0xFFL);
        data[i++] = (byte)(v >>> 48 & 0xFFL);
        data[i++] = (byte)(v >>> 56 & 0xFFL);
    }
    
    public static void writeInt2BE(final OutputStream out, final int v) throws IOException {
        final byte[] b2 = new byte[2];
        putInt2BE(b2, 0, v);
        out.write(b2);
    }
    
    public static void putInt2BE(final byte[] data, final int offset, final int v) {
        final int i = offset;
        data[i + 1] = (byte)(v >>> 0 & 0xFF);
        data[i + 0] = (byte)(v >>> 8 & 0xFF);
    }
    
    public static void writeInt3BE(final OutputStream out, final long v) throws IOException {
        final byte[] b3 = new byte[3];
        putInt3BE(b3, 0, v);
        out.write(b3);
    }
    
    public static void putInt3BE(final byte[] data, final int offset, final long v) {
        final int i = offset;
        data[i + 2] = (byte)(v >>> 0 & 0xFFL);
        data[i + 1] = (byte)(v >>> 8 & 0xFFL);
        data[i + 0] = (byte)(v >>> 16 & 0xFFL);
    }
    
    public static void writeInt4BE(final OutputStream out, final long v) throws IOException {
        final byte[] b4 = new byte[4];
        putInt4BE(b4, 0, v);
        out.write(b4);
    }
    
    public static void putInt4BE(final byte[] data, final int offset, final long v) {
        final int i = offset;
        data[i + 3] = (byte)(v >>> 0 & 0xFFL);
        data[i + 2] = (byte)(v >>> 8 & 0xFFL);
        data[i + 1] = (byte)(v >>> 16 & 0xFFL);
        data[i + 0] = (byte)(v >>> 24 & 0xFFL);
    }
    
    public static String getUTF8(final byte[] data, final int offset, final int length) {
        try {
            return new String(data, offset, length, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Broken JVM, UTF-8 not found", e);
        }
    }
    
    public static int putUTF8(final byte[] data, final int offset, final String str) {
        try {
            final byte[] s = str.getBytes("UTF-8");
            System.arraycopy(s, 0, data, offset, s.length);
            return s.length;
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Broken JVM, UTF-8 not found", e);
        }
    }
    
    public static void writeUTF8(final OutputStream out, final String str) throws IOException {
        try {
            final byte[] s = str.getBytes("UTF-8");
            out.write(s);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Broken JVM, UTF-8 not found", e);
        }
    }
    
    public static void writeUTF8WithLength(final OutputStream out, final String str) throws IOException {
        try {
            final byte[] s = str.getBytes("UTF-8");
            writeInt4(out, s.length);
            out.write(s);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Broken JVM, UTF-8 not found", e);
        }
    }
}
