// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import junit.framework.TestCase;

public class TestWriteBoundaries extends TestCase
{
    public static byte[] getBytes(final int len) {
        final byte[] b = new byte[len];
        for (int i = 0; i < len; ++i) {
            b[i] = IOUtils.fromInt(i % 256);
        }
        return b;
    }
    
    public void testMix() throws Exception {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OggFile ogg = new OggFile(baos);
        final OggPacketWriter w = ogg.getPacketWriter(1193046);
        OggPacket p = new OggPacket(new byte[] { 0, 1, 2, 3, 4, 5 });
        w.bufferPacket(p, false);
        p = new OggPacket(new byte[] { 10, 11, 12, 13, 14, 15 });
        w.bufferPacket(p, true);
        p = new OggPacket(new byte[] { 0, 1, 2, 3, 4, 5 });
        w.bufferPacket(p, false);
        p = new OggPacket(getBytes(131072));
        w.bufferPacket(p, false);
        p = new OggPacket(new byte[] { 0, 1, 2, 3, 4, 5 });
        w.bufferPacket(p, false);
        w.close();
        ogg = new OggFile(new ByteArrayInputStream(baos.toByteArray()));
        final OggPacketReader r = ogg.getPacketReader();
        p = r.getNextPacket();
        assertEquals(0, p.getSequenceNumber());
        assertEquals(6, p.getData().length);
        p = r.getNextPacket();
        assertEquals(0, p.getSequenceNumber());
        assertEquals(6, p.getData().length);
        p = r.getNextPacket();
        assertEquals(1, p.getSequenceNumber());
        assertEquals(6, p.getData().length);
        p = r.getNextPacket();
        assertEquals(3, p.getSequenceNumber());
        assertEquals(131072, p.getData().length);
        p = r.getNextPacket();
        assertEquals(3, p.getSequenceNumber());
        assertEquals(6, p.getData().length);
        p = r.getNextPacket();
        assertEquals((Object)null, (Object)p);
    }
    
    public void testWriteBigPackets() throws Exception {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OggFile ogg = new OggFile(baos);
        final OggPacketWriter w = ogg.getPacketWriter(1193046);
        OggPacket p = new OggPacket(new byte[] { 0, 1, 2, 3, 4, 5 });
        w.bufferPacket(p, true);
        p = new OggPacket(getBytes(131072));
        w.bufferPacket(p, true);
        p = new OggPacket(getBytes(144077));
        w.bufferPacket(p, false);
        p = new OggPacket(getBytes(161077));
        w.bufferPacket(p, false);
        p = new OggPacket(getBytes(231079));
        w.bufferPacket(p, false);
        p = new OggPacket(new byte[] { 0, 1, 2, 3, 4, 5 });
        w.bufferPacket(p, true);
        p = new OggPacket(new byte[] { 0, 1, 2, 3, 4, 5 });
        w.bufferPacket(p, false);
        w.close();
        ogg = new OggFile(new ByteArrayInputStream(baos.toByteArray()));
        final OggPacketReader r = ogg.getPacketReader();
        p = r.getNextPacket();
        assertEquals(0, p.getSequenceNumber());
        assertEquals(6, p.getData().length);
        p = r.getNextPacket();
        assertEquals(3, p.getSequenceNumber());
        assertEquals(131072, p.getData().length);
        p = r.getNextPacket();
        assertEquals(6, p.getSequenceNumber());
        assertEquals(144077, p.getData().length);
        p = r.getNextPacket();
        assertEquals(8, p.getSequenceNumber());
        assertEquals(161077, p.getData().length);
        p = r.getNextPacket();
        assertEquals(12, p.getSequenceNumber());
        assertEquals(231079, p.getData().length);
        p = r.getNextPacket();
        assertEquals(12, p.getSequenceNumber());
        assertEquals(6, p.getData().length);
        p = r.getNextPacket();
        assertEquals(13, p.getSequenceNumber());
        assertEquals(6, p.getData().length);
        p = r.getNextPacket();
        assertEquals((Object)null, (Object)p);
    }
}
