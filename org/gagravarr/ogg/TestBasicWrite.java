// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import junit.framework.TestCase;

public class TestBasicWrite extends TestCase
{
    public void testOpen() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final OggFile ogg = new OggFile(baos);
        try {
            ogg.getPacketReader();
            fail();
        }
        catch (IllegalStateException ex) {}
        final OggPacketWriter w = ogg.getPacketWriter(1234);
        w.close();
        ogg.close();
    }
    
    public void testEmptyPages() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final OggFile ogg = new OggFile(baos);
        OggPacketWriter w = ogg.getPacketWriter(1234);
        w.close();
        assertEquals(28, baos.size());
        final InputStream inp = new ByteArrayInputStream(baos.toByteArray());
        inp.read();
        inp.read();
        inp.read();
        inp.read();
        final OggPage page = new OggPage(inp);
        assertEquals(1, page.getNumLVs());
        OggFile opened = new OggFile(new ByteArrayInputStream(baos.toByteArray()));
        OggPacketReader r = opened.getPacketReader();
        OggPacket p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(1234, p.getSid());
        assertEquals(0, p.getSequenceNumber());
        assertEquals(0, p.getData().length);
        assertNull((Object)r.getNextPacket());
        w = ogg.getPacketWriter(54321);
        w.flush();
        p = new OggPacket(new byte[0]);
        w.bufferPacket(p);
        w.flush();
        p = new OggPacket(new byte[] { 22 });
        w.bufferPacket(p);
        w.close();
        opened = new OggFile(new ByteArrayInputStream(baos.toByteArray()));
        r = opened.getPacketReader();
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(1234, p.getSid());
        assertEquals(0, p.getSequenceNumber());
        assertEquals(0, p.getData().length);
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(54321, p.getSid());
        assertEquals(0, p.getSequenceNumber());
        assertEquals(0, p.getData().length);
        p = r.getNextPacket();
        assertNotNull((Object)p);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(54321, p.getSid());
        assertEquals(1, p.getSequenceNumber());
        assertEquals(1, p.getData().length);
        assertNull((Object)r.getNextPacket());
    }
    
    public void testInterleaved() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final OggFile ogg = new OggFile(baos);
        final OggPacketWriter w1 = ogg.getPacketWriter(1234);
        final OggPacketWriter w2 = ogg.getPacketWriter(4321);
        OggPacket p = new OggPacket(new byte[] { 1 });
        w1.bufferPacket(p, true);
        p = new OggPacket(new byte[] { 2 });
        w2.bufferPacket(p, true);
        p = new OggPacket(new byte[] { 1, 1 });
        w1.bufferPacket(p);
        p = new OggPacket(new byte[] { 1, 2 });
        w1.bufferPacket(p, false);
        p = new OggPacket(new byte[] { 2, 2 });
        w2.bufferPacket(p);
        w2.close();
        w1.close();
        final OggFile opened = new OggFile(new ByteArrayInputStream(baos.toByteArray()));
        final OggPacketReader r = opened.getPacketReader();
        p = r.getNextPacket();
        assertEquals(1234, p.getSid());
        assertEquals(0, p.getSequenceNumber());
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(1, p.getData().length);
        assertEquals(1, (int)p.getData()[0]);
        p = r.getNextPacket();
        assertEquals(4321, p.getSid());
        assertEquals(0, p.getSequenceNumber());
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(1, p.getData().length);
        assertEquals(2, (int)p.getData()[0]);
        p = r.getNextPacket();
        assertEquals(4321, p.getSid());
        assertEquals(1, p.getSequenceNumber());
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(2, p.getData().length);
        assertEquals(2, (int)p.getData()[0]);
        assertEquals(2, (int)p.getData()[1]);
        p = r.getNextPacket();
        assertEquals(1234, p.getSid());
        assertEquals(1, p.getSequenceNumber());
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(2, p.getData().length);
        assertEquals(1, (int)p.getData()[0]);
        assertEquals(1, (int)p.getData()[1]);
        p = r.getNextPacket();
        assertEquals(1234, p.getSid());
        assertEquals(1, p.getSequenceNumber());
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(2, p.getData().length);
        assertEquals(1, (int)p.getData()[0]);
        assertEquals(2, (int)p.getData()[1]);
        assertEquals((Object)null, (Object)r.getNextPacket());
    }
}
