// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestBasicRead extends TestCase
{
    private InputStream getTestFile() throws IOException {
        return this.getClass().getResourceAsStream("/testVORBIS.ogg");
    }
    
    public void testOpen() throws IOException {
        final OggFile ogg = new OggFile(this.getTestFile());
        final OggPacketReader r = ogg.getPacketReader();
        r.getNextPacket();
        try {
            ogg.getPacketWriter();
            fail();
        }
        catch (IllegalStateException ex) {}
    }
    
    public void testPages() throws IOException {
        final InputStream inp = this.getTestFile();
        inp.read();
        inp.read();
        inp.read();
        inp.read();
        OggPage page = new OggPage(inp);
        assertEquals(false, page.isContinuation());
        assertEquals(false, page.hasContinuation());
        assertEquals(0L, page.getGranulePosition());
        assertEquals(74691676, page.getSid());
        assertEquals(0, page.getSequenceNumber());
        assertEquals(true, page.isChecksumValid());
        assertEquals(30, page.getDataSize());
        assertEquals(58, page.getPageSize());
        assertEquals(1, page.getNumLVs());
        OggPage.OggPacketIterator i = page.getPacketIterator();
        assertEquals(true, i.hasNext());
        OggPacket p = (OggPacket)i.next();
        assertEquals(30, p.getData().length);
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(false, i.hasNext());
        inp.read();
        inp.read();
        inp.read();
        inp.read();
        page = new OggPage(inp);
        assertEquals(false, page.isContinuation());
        assertEquals(false, page.hasContinuation());
        assertEquals(0L, page.getGranulePosition());
        assertEquals(74691676, page.getSid());
        assertEquals(1, page.getSequenceNumber());
        assertEquals(true, page.isChecksumValid());
        assertEquals(15, page.getNumLVs());
        i = page.getPacketIterator();
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(219, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(3484, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(false, i.hasNext());
        inp.read();
        inp.read();
        inp.read();
        inp.read();
        page = new OggPage(inp);
        assertEquals(false, page.isContinuation());
        assertEquals(false, page.hasContinuation());
        assertEquals(960L, page.getGranulePosition());
        assertEquals(74691676, page.getSid());
        assertEquals(2, page.getSequenceNumber());
        assertEquals(true, page.isChecksumValid());
        assertEquals(9, page.getNumLVs());
        i = page.getPacketIterator();
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(35, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(28, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(30, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(52, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(51, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(69, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(59, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(40, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(true, i.hasNext());
        p = (OggPacket)i.next();
        assertEquals(38, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(false, i.hasNext());
        assertEquals(-1, inp.read());
    }
    
    public void testPackets() throws IOException {
        final OggFile ogg = new OggFile(this.getTestFile());
        final OggPacketReader r = ogg.getPacketReader();
        OggPacket p = r.getNextPacket();
        assertEquals(30, p.getData().length);
        assertEquals(true, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(0, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(219, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(1, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(3484, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(0L, p.getGranulePosition());
        assertEquals(1, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(35, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(28, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(30, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(52, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(51, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(69, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(59, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(40, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(false, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals(38, p.getData().length);
        assertEquals(false, p.isBeginningOfStream());
        assertEquals(true, p.isEndOfStream());
        assertEquals(74691676, p.getSid());
        assertEquals(960L, p.getGranulePosition());
        assertEquals(2, p.getSequenceNumber());
        p = r.getNextPacket();
        assertEquals((Object)null, (Object)p);
    }
    
    public void testCRC() throws IOException {
        final InputStream inp = this.getTestFile();
        inp.read();
        inp.read();
        inp.read();
        inp.read();
        OggPage page = new OggPage(inp);
        assertEquals(1776334944L, page.getChecksum());
        assertTrue(page.isChecksumValid());
        final byte[] header = page.getHeader();
        assertEquals(28, header.length);
        final OggPacket p = (OggPacket)page.getPacketIterator().next();
        page = new OggPage(74691676, 0);
        page.addPacket(p, 0);
        assertEquals(0L, page.getChecksum());
        assertTrue(page.isChecksumValid());
        final byte[] header2 = page.getHeader();
        assertEquals(28, header2.length);
        for (int i = 0; i < 28; ++i) {
            assertEquals(header[i], header2[i]);
        }
        int crc = CRCUtils.getCRC(header);
        crc = CRCUtils.getCRC(page.getData(), crc);
        assertEquals(1776334944, crc);
        crc = CRCUtils.getCRC(header);
        crc = CRCUtils.getCRC(page.getData(), crc);
        assertEquals(1776334944, crc);
        CRCUtils.getCRC(new byte[] { 1, 2, 3, 4 });
        page.writeHeader(new ByteArrayOutputStream());
        assertEquals(1776334944L, page.getChecksum());
        assertTrue(page.isChecksumValid());
    }
}
