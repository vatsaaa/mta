// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

public class CRCUtils
{
    protected static final Integer CRC_POLYNOMIAL;
    private static int[] CRC_TABLE;
    
    public static int getCRC(final byte[] data) {
        return getCRC(data, 0);
    }
    
    public static int getCRC(final byte[] data, final int previous) {
        int crc = previous;
        for (int i = 0; i < data.length; ++i) {
            final int a = crc << 8;
            final int b = CRCUtils.CRC_TABLE[(crc >>> 24 & 0xFF) ^ (data[i] & 0xFF)];
            crc = (a ^ b);
        }
        return crc;
    }
    
    static {
        CRC_POLYNOMIAL = 79764919;
        CRCUtils.CRC_TABLE = new int[256];
        for (int i = 0; i < 256; ++i) {
            int crc = i << 24;
            for (int j = 0; j < 8; ++j) {
                if ((crc & Integer.MIN_VALUE) != 0x0) {
                    crc = (crc << 1 ^ CRCUtils.CRC_POLYNOMIAL);
                }
                else {
                    crc <<= 1;
                }
            }
            CRCUtils.CRC_TABLE[i] = crc;
        }
    }
}
