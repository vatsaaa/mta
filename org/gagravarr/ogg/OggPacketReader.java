// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

import java.io.IOException;
import java.util.Iterator;
import java.io.InputStream;

public class OggPacketReader
{
    private InputStream inp;
    private Iterator<OggPacketData> it;
    private OggPacket nextPacket;
    
    public OggPacketReader(final InputStream inp) {
        this.inp = inp;
    }
    
    public OggPacket getNextPacket() throws IOException {
        if (this.nextPacket != null) {
            final OggPacket p = this.nextPacket;
            this.nextPacket = null;
            return p;
        }
        OggPacketData leftOver = null;
        if (this.it != null && this.it.hasNext()) {
            final OggPacketData packet = this.it.next();
            if (packet instanceof OggPacket) {
                return (OggPacket)packet;
            }
            leftOver = packet;
        }
        int searched = 0;
        int pos = -1;
        boolean found = false;
        while (searched < 65536 && !found) {
            final int r = this.inp.read();
            if (r == -1) {
                return null;
            }
            switch (pos) {
                case -1: {
                    if (r == 79) {
                        pos = 0;
                        break;
                    }
                    break;
                }
                case 0: {
                    if (r == 103) {
                        pos = 1;
                        break;
                    }
                    pos = -1;
                    break;
                }
                case 1: {
                    if (r == 103) {
                        pos = 2;
                        break;
                    }
                    pos = -1;
                    break;
                }
                case 2: {
                    if (r == 83) {
                        found = true;
                        break;
                    }
                    pos = -1;
                    break;
                }
            }
            if (found) {
                continue;
            }
            ++searched;
        }
        if (!found) {
            throw new IOException("Next ogg packet header not found after searching " + searched + " bytes");
        }
        searched -= 3;
        if (searched > 0) {
            System.err.println("Warning - had to skip " + searched + " bytes of junk data before finding the next packet header");
        }
        final OggPage page = new OggPage(this.inp);
        if (!page.isChecksumValid()) {
            System.err.println("Warning - invalid checksum on page " + page.getSequenceNumber() + " of stream " + Integer.toHexString(page.getSid()) + " (" + page.getSid() + ")");
        }
        this.it = page.getPacketIterator(leftOver);
        return this.getNextPacket();
    }
    
    public OggPacket getNextPacketWithSid(final int sid) throws IOException {
        OggPacket p = null;
        while ((p = this.getNextPacket()) != null) {
            if (p.getSid() == sid) {
                return p;
            }
        }
        return null;
    }
    
    public void unreadPacket(final OggPacket packet) {
        if (this.nextPacket != null) {
            throw new IllegalStateException("Can't un-read twice");
        }
        this.nextPacket = packet;
    }
    
    public void skipToSequenceNumber(final int sid, final int sequenceNumber) throws IOException {
        OggPacket p = null;
        while ((p = this.getNextPacket()) != null) {
            if (p.getSid() == sid && p.getSequenceNumber() >= sequenceNumber) {
                this.nextPacket = p;
                break;
            }
        }
    }
    
    public void skipToGranulePosition(final int sid, final long granulePosition) throws IOException {
        OggPacket p = null;
        while ((p = this.getNextPacket()) != null) {
            if (p.getSid() == sid && p.getGranulePosition() >= granulePosition) {
                this.nextPacket = p;
                break;
            }
        }
    }
}
