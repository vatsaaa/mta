// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

public interface OggStreamListener
{
    OggStreamReader[] processNewStream(final int p0, final byte[] p1);
    
    void processStreamEnd(final int p0);
}
