// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.ogg;

public class OggPacket extends OggPacketData
{
    private OggPage parent;
    private boolean bos;
    private boolean eos;
    
    protected OggPacket(final OggPage parent, final byte[] data, final boolean bos, final boolean eos) {
        super(data);
        this.parent = parent;
        this.bos = bos;
        this.eos = eos;
    }
    
    public OggPacket(final byte[] data) {
        super(data);
    }
    
    protected void setParent(final OggPage parent) {
        this.parent = parent;
    }
    
    protected void setIsBOS() {
        this.bos = true;
    }
    
    protected void setIsEOS() {
        this.eos = true;
    }
    
    protected OggPage _getParent() {
        return this.parent;
    }
    
    public int getSid() {
        return this.parent.getSid();
    }
    
    public long getGranulePosition() {
        return this.parent.getGranulePosition();
    }
    
    public int getSequenceNumber() {
        return this.parent.getSequenceNumber();
    }
    
    public boolean isBeginningOfStream() {
        return this.bos;
    }
    
    public boolean isEndOfStream() {
        return this.eos;
    }
}
