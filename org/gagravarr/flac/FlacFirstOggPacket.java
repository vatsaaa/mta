// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.gagravarr.ogg.IOUtils;
import org.gagravarr.ogg.OggPacket;
import org.gagravarr.ogg.HighLevelOggStreamPacket;

public class FlacFirstOggPacket extends HighLevelOggStreamPacket
{
    private int majorVersion;
    private int minorVersion;
    private int numberOfHeaderBlocks;
    private FlacInfo info;
    
    public FlacFirstOggPacket() {
        this(new FlacInfo());
    }
    
    public FlacFirstOggPacket(final FlacInfo info) {
        this.majorVersion = 1;
        this.minorVersion = 0;
        this.numberOfHeaderBlocks = 0;
        this.info = info;
    }
    
    public FlacFirstOggPacket(final OggPacket oggPacket) {
        super(oggPacket);
        final byte[] data = this.getData();
        this.majorVersion = IOUtils.toInt(data[5]);
        this.minorVersion = IOUtils.toInt(data[6]);
        this.numberOfHeaderBlocks = IOUtils.getInt2BE(data, 7);
        this.info = new FlacInfo(data, 17);
    }
    
    @Override
    public OggPacket write() {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write("FLAC".getBytes("ASCII"));
            baos.write(this.majorVersion);
            baos.write(this.minorVersion);
            IOUtils.writeInt2BE(baos, this.numberOfHeaderBlocks);
            baos.write("fLaC".getBytes("ASCII"));
            baos.write(this.info.getData());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        this.setData(baos.toByteArray());
        return super.write();
    }
    
    public int getMajorVersion() {
        return this.majorVersion;
    }
    
    public void setMajorVersion(final int majorVersion) {
        if (majorVersion > 255) {
            throw new IllegalArgumentException("Version numbers must be in the range 0-255");
        }
        this.majorVersion = majorVersion;
    }
    
    public FlacInfo getInfo() {
        return this.info;
    }
    
    public int getMinorVersion() {
        return this.minorVersion;
    }
    
    public void setMinorVersion(final int minorVersion) {
        if (minorVersion > 255) {
            throw new IllegalArgumentException("Version numbers must be in the range 0-255");
        }
        this.minorVersion = minorVersion;
    }
    
    public int getNumberOfHeaderBlocks() {
        return this.numberOfHeaderBlocks;
    }
    
    public void setNumberOfHeaderBlocks(final int numberOfHeaderBlocks) {
        this.numberOfHeaderBlocks = numberOfHeaderBlocks;
    }
    
    public static boolean isFlacStream(final OggPacket firstPacket) {
        return firstPacket.isBeginningOfStream() && isFlacSpecial(firstPacket);
    }
    
    private static boolean isFlacSpecial(final OggPacket packet) {
        final byte[] d = packet.getData();
        final byte type = d[0];
        return type == 127 && d[1] == 70 && d[2] == 76 && d[3] == 65 && d[4] == 67;
    }
}
