// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import java.util.ArrayList;
import org.gagravarr.ogg.IOUtils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;
import java.io.InputStream;

public class FlacNativeFile extends FlacFile
{
    private InputStream input;
    
    public FlacNativeFile(final File f) throws IOException, FileNotFoundException {
        this(new FileInputStream(f));
    }
    
    public FlacNativeFile(final InputStream inp) throws IOException {
        final byte[] header = new byte[4];
        IOUtils.readFully(inp, header);
        if (header[0] == 102 && header[1] == 76 && header[2] == 97 && header[3] == 67) {
            this.info = (FlacInfo)FlacMetadataBlock.create(inp);
            this.otherMetadata = new ArrayList<FlacMetadataBlock>();
            FlacMetadataBlock m;
            do {
                m = FlacMetadataBlock.create(inp);
                if (m instanceof FlacTags.FlacTagsAsMetadata) {
                    this.tags = ((FlacTags.FlacTagsAsMetadata)m).getTags();
                }
                else {
                    this.otherMetadata.add(m);
                }
            } while (!m.isLastMetadataBlock());
            return;
        }
        throw new IllegalArgumentException("Not a FLAC file");
    }
    
    @Override
    public FlacAudioFrame getNextAudioPacket() throws IOException {
        return new FlacAudioFrame(null);
    }
    
    @Override
    public void skipToGranule(final long granulePosition) throws IOException {
        throw new RuntimeException("Not supported");
    }
    
    @Override
    public void close() throws IOException {
        if (this.input != null) {
            this.input.close();
            this.input = null;
            return;
        }
        throw new RuntimeException("Not supported");
    }
}
