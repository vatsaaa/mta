// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import org.gagravarr.ogg.OggFile;
import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestFlacFileRead extends TestCase
{
    private InputStream getTestOggFile() throws IOException {
        return this.getClass().getResourceAsStream("/testFLAC.oga");
    }
    
    private InputStream getTestFlacFile() throws IOException {
        return this.getClass().getResourceAsStream("/testFLAC.flac");
    }
    
    public void testReadOgg() throws IOException {
        final OggFile ogg = new OggFile(this.getTestOggFile());
        final FlacOggFile flac = new FlacOggFile(ogg);
        final FlacFirstOggPacket first = flac.getFirstPacket();
        assertNotNull((Object)first);
        assertEquals(1, first.getMajorVersion());
        assertEquals(0, first.getMinorVersion());
        final FlacInfo info = flac.getInfo();
        assertNotNull((Object)info);
        assertEquals(4096, info.getMinimumBlockSize());
        assertEquals(4096, info.getMaximumBlockSize());
        assertEquals(2126, info.getMinimumFrameSize());
        assertEquals(2126, info.getMaximumFrameSize());
        assertEquals(44100, info.getSampleRate());
        assertEquals(16, info.getBitsPerSample());
        assertEquals(2, info.getNumChannels());
        assertEquals(960L, info.getNumberOfSamples());
        final FlacTags tags = flac.getTags();
        assertNotNull((Object)tags);
        assertEquals(7, tags.getAllComments().size());
        assertEquals("Test Album", tags.getAlbum());
        assertNotNull((Object)flac.getNextAudioPacket());
        final FlacAudioFrame ad = flac.getNextAudioPacket();
    }
    
    public void testReadFlacNative() throws IOException {
        final FlacNativeFile flac = new FlacNativeFile(this.getTestFlacFile());
        final FlacInfo info = flac.getInfo();
        assertNotNull((Object)info);
        assertEquals(4096, info.getMinimumBlockSize());
        assertEquals(4096, info.getMaximumBlockSize());
        assertEquals(2126, info.getMinimumFrameSize());
        assertEquals(2126, info.getMaximumFrameSize());
        assertEquals(44100, info.getSampleRate());
        assertEquals(16, info.getBitsPerSample());
        assertEquals(2, info.getNumChannels());
        assertEquals(960L, info.getNumberOfSamples());
        final FlacTags tags = flac.getTags();
        assertNotNull((Object)tags);
        assertEquals(7, tags.getAllComments().size());
        assertEquals("Test Album", tags.getAlbum());
        assertNotNull((Object)flac.getNextAudioPacket());
        final FlacAudioFrame ad = flac.getNextAudioPacket();
    }
}
