// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import java.io.IOException;
import java.io.OutputStream;

public class FlacUnhandledMetadataBlock extends FlacMetadataBlock
{
    private byte[] data;
    
    public FlacUnhandledMetadataBlock(final byte type, final byte[] data) {
        super(type);
        this.data = data;
    }
    
    @Override
    protected void write(final OutputStream out) throws IOException {
        out.write(this.data);
    }
}
