// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import java.io.IOException;
import java.io.OutputStream;
import org.gagravarr.ogg.IOUtils;

public class FlacInfo extends FlacMetadataBlock
{
    private int minimumBlockSize;
    private int maximumBlockSize;
    private int minimumFrameSize;
    private int maximumFrameSize;
    private int sampleRate;
    private int numChannels;
    private int bitsPerSample;
    private long numberOfSamples;
    private byte[] signature;
    
    public FlacInfo() {
        super((byte)0);
        this.signature = new byte[16];
    }
    
    public FlacInfo(final byte[] data, int offset) {
        super((byte)0);
        this.minimumBlockSize = IOUtils.getIntBE(IOUtils.toInt(data[offset++]), IOUtils.toInt(data[offset++]));
        this.maximumBlockSize = IOUtils.getIntBE(IOUtils.toInt(data[offset++]), IOUtils.toInt(data[offset++]));
        this.minimumFrameSize = (int)IOUtils.getIntBE(IOUtils.toInt(data[offset++]), IOUtils.toInt(data[offset++]), IOUtils.toInt(data[offset++]));
        this.maximumFrameSize = (int)IOUtils.getIntBE(IOUtils.toInt(data[offset++]), IOUtils.toInt(data[offset++]), IOUtils.toInt(data[offset++]));
        final int[] next = new int[8];
        for (int i = 0; i < 8; ++i) {
            next[i] = IOUtils.toInt(data[i + offset]);
        }
        offset += 8;
        this.sampleRate = (next[0] << 12) + (next[1] << 4) + ((next[2] & 0xF0) >> 4);
        this.numChannels = ((next[2] & 0xE) >> 1) + 1;
        this.bitsPerSample = ((next[2] & 0x1) << 4) + ((next[3] & 0xF0) >> 4) + 1;
        this.numberOfSamples = ((next[3] & 0xF) << 30) + (next[4] << 24) + (next[5] << 16) + (next[6] << 8) + next[7];
        System.arraycopy(data, offset, this.signature = new byte[16], 0, 16);
    }
    
    @Override
    protected void write(final OutputStream out) throws IOException {
        IOUtils.writeInt2BE(out, this.minimumBlockSize);
        IOUtils.writeInt2BE(out, this.maximumBlockSize);
        IOUtils.writeInt3BE(out, this.minimumFrameSize);
        IOUtils.writeInt3BE(out, this.maximumFrameSize);
        out.write(new byte[8]);
        out.write(this.signature);
    }
    
    public int getMinimumBlockSize() {
        return this.minimumBlockSize;
    }
    
    public void setMinimumBlockSize(final int minimumBlockSize) {
        this.minimumBlockSize = minimumBlockSize;
    }
    
    public int getMaximumBlockSize() {
        return this.maximumBlockSize;
    }
    
    public void setMaximumBlockSize(final int maximumBlockSize) {
        this.maximumBlockSize = maximumBlockSize;
    }
    
    public int getMinimumFrameSize() {
        return this.minimumFrameSize;
    }
    
    public void setMinimumFrameSize(final int minimumFrameSize) {
        this.minimumFrameSize = minimumFrameSize;
    }
    
    public int getMaximumFrameSize() {
        return this.maximumFrameSize;
    }
    
    public void setMaximumFrameSize(final int maximumFrameSize) {
        this.maximumFrameSize = maximumFrameSize;
    }
    
    public int getSampleRate() {
        return this.sampleRate;
    }
    
    public void setSampleRate(final int sampleRate) {
        this.sampleRate = sampleRate;
    }
    
    public int getNumChannels() {
        return this.numChannels;
    }
    
    public void setNumChannels(final int numChannels) {
        this.numChannels = numChannels;
    }
    
    public int getBitsPerSample() {
        return this.bitsPerSample;
    }
    
    public void setBitsPerSample(final int bitsPerSample) {
        this.bitsPerSample = bitsPerSample;
    }
    
    public long getNumberOfSamples() {
        return this.numberOfSamples;
    }
    
    public void setNumberOfSamples(final long numberOfSamples) {
        this.numberOfSamples = numberOfSamples;
    }
    
    public byte[] getSignature() {
        return this.signature;
    }
    
    public void setSignature(final byte[] signature) {
        this.signature = signature;
    }
}
