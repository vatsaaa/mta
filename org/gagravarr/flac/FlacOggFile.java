// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import java.util.Iterator;
import org.gagravarr.vorbis.VorbisPacket;
import java.io.OutputStream;
import org.gagravarr.ogg.OggPacket;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.util.List;
import org.gagravarr.ogg.OggPacketWriter;
import org.gagravarr.ogg.OggPacketReader;
import org.gagravarr.ogg.OggFile;

public class FlacOggFile extends FlacFile
{
    private OggFile ogg;
    private OggPacketReader r;
    private OggPacketWriter w;
    private int sid;
    private FlacFirstOggPacket firstPacket;
    private List<FlacAudioFrame> writtenAudio;
    
    public FlacOggFile(final File f) throws IOException, FileNotFoundException {
        this(new OggFile(new FileInputStream(f)));
    }
    
    public FlacOggFile(final OggFile ogg) throws IOException {
        this(ogg.getPacketReader());
        this.ogg = ogg;
    }
    
    public FlacOggFile(final OggPacketReader r) throws IOException {
        this.sid = -1;
        this.r = r;
        OggPacket p = null;
        while ((p = r.getNextPacket()) != null) {
            if (p.isBeginningOfStream() && p.getData().length > 10 && FlacFirstOggPacket.isFlacStream(p)) {
                this.sid = p.getSid();
                break;
            }
        }
        this.firstPacket = new FlacFirstOggPacket(p);
        this.info = this.firstPacket.getInfo();
        this.tags = new FlacTags(r.getNextPacketWithSid(this.sid));
        this.otherMetadata = new ArrayList<FlacMetadataBlock>();
        while ((p = r.getNextPacketWithSid(this.sid)) != null) {
            final FlacMetadataBlock block = FlacMetadataBlock.create(new ByteArrayInputStream(p.getData()));
            this.otherMetadata.add(block);
            if (block.isLastMetadataBlock()) {
                break;
            }
        }
    }
    
    public FlacOggFile(final OutputStream out) {
        this(out, new FlacInfo(), new FlacTags());
    }
    
    public FlacOggFile(final OutputStream out, final FlacInfo info, final FlacTags tags) {
        this(out, -1, info, tags);
    }
    
    public FlacOggFile(final OutputStream out, final int sid, final FlacInfo info, final FlacTags tags) {
        this.sid = -1;
        this.ogg = new OggFile(out);
        if (sid > 0) {
            this.w = this.ogg.getPacketWriter(sid);
            this.sid = sid;
        }
        else {
            this.w = this.ogg.getPacketWriter();
            this.sid = this.w.getSid();
        }
        this.writtenAudio = new ArrayList<FlacAudioFrame>();
        this.firstPacket = new FlacFirstOggPacket(info);
        this.info = info;
        this.tags = tags;
    }
    
    public FlacFirstOggPacket getFirstPacket() {
        return this.firstPacket;
    }
    
    @Override
    public FlacAudioFrame getNextAudioPacket() throws IOException {
        OggPacket p = null;
        final VorbisPacket vp = null;
        if ((p = this.r.getNextPacketWithSid(this.sid)) != null) {
            return new FlacAudioFrame(p.getData());
        }
        return null;
    }
    
    @Override
    public void skipToGranule(final long granulePosition) throws IOException {
        this.r.skipToGranulePosition(this.sid, granulePosition);
    }
    
    public int getSid() {
        return this.sid;
    }
    
    public void writeAudioData(final FlacAudioFrame data) {
        this.writtenAudio.add(data);
    }
    
    @Override
    public void close() throws IOException {
        if (this.r != null) {
            this.r = null;
            this.ogg.close();
            this.ogg = null;
        }
        if (this.w != null) {
            this.w.bufferPacket(this.firstPacket.write(), true);
            this.w.bufferPacket(this.tags.write(), false);
            final long lastGranule = 0L;
            for (final FlacAudioFrame fa : this.writtenAudio) {
                this.w.bufferPacket(new OggPacket(fa.getData()));
                if (this.w.getSizePendingFlush() > 16384) {
                    this.w.flush();
                }
            }
            this.w.close();
            this.w = null;
            this.ogg.close();
            this.ogg = null;
        }
    }
    
    public OggFile getOggFile() {
        return this.ogg;
    }
}
