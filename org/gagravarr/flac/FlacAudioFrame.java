// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

public class FlacAudioFrame extends FlacFrame
{
    private byte[] data;
    private long position;
    
    public FlacAudioFrame(final byte[] data) {
        this.data = data;
    }
    
    @Override
    public byte[] getData() {
        return this.data;
    }
}
