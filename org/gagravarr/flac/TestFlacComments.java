// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import org.gagravarr.ogg.OggPacketReader;
import org.gagravarr.ogg.OggFile;
import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestFlacComments extends TestCase
{
    private InputStream getTestOggFile() throws IOException {
        return this.getClass().getResourceAsStream("/testFLAC.oga");
    }
    
    private InputStream getTestFlacFile() throws IOException {
        return this.getClass().getResourceAsStream("/testFLAC.flac");
    }
    
    public void testReadOgg() throws IOException {
        OggFile ogg = new OggFile(this.getTestOggFile());
        final OggPacketReader r = ogg.getPacketReader();
        r.getNextPacket();
        final FlacTags tags = new FlacTags(r.getNextPacket());
        this.doTestComments(tags);
        ogg = new OggFile(this.getTestOggFile());
        final FlacOggFile flac = new FlacOggFile(ogg);
        this.doTestComments(flac.getTags());
    }
    
    public void testReadFlac() throws IOException {
        final FlacNativeFile flac = new FlacNativeFile(this.getTestFlacFile());
        this.doTestComments(flac.getTags());
    }
    
    private void doTestComments(final FlacTags tags) {
        assertEquals("reference libFLAC 1.2.1 20070917", tags.getVendor());
        assertEquals(7, tags.getAllComments().size());
        assertEquals("Test Title", tags.getTitle());
        assertEquals(1, tags.getComments("TiTlE").size());
        assertEquals("Test Title", (String)tags.getComments("tItLe").get(0));
        assertEquals("Test Artist", tags.getArtist());
        assertEquals(1, tags.getComments("ArTiSt").size());
        assertEquals("Test Artist", (String)tags.getComments("aRTiST").get(0));
        assertEquals("Test Genre", tags.getGenre());
        assertEquals(1, tags.getComments("GEnRE").size());
        assertEquals("Test Genre", (String)tags.getComments("genRE").get(0));
        assertEquals("Test Album", tags.getAlbum());
        assertEquals(1, tags.getComments("alBUm").size());
        assertEquals("Test Album", (String)tags.getComments("ALbUM").get(0));
        assertEquals(1, tags.getComments("DAte").size());
        assertEquals("2010-01-26", (String)tags.getComments("dAtE").get(0));
        assertEquals(2, tags.getComments("COmmENT").size());
        assertEquals("Test Comment", (String)tags.getComments("COMMent").get(0));
        assertEquals("Another Test Comment", (String)tags.getComments("COMMent").get(1));
    }
}
