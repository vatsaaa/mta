// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import org.gagravarr.ogg.OggFile;
import org.gagravarr.ogg.IOUtils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.util.List;

public abstract class FlacFile
{
    protected FlacInfo info;
    protected FlacTags tags;
    protected List<FlacMetadataBlock> otherMetadata;
    
    public static FlacFile open(final File f) throws IOException, FileNotFoundException {
        final InputStream inp = new FileInputStream(f);
        final FlacFile file = open(inp);
        inp.close();
        return file;
    }
    
    public static FlacFile open(final InputStream inp) throws IOException, FileNotFoundException {
        inp.mark(4);
        final byte[] header = new byte[4];
        IOUtils.readFully(inp, header);
        inp.reset();
        if (header[0] == 79 && header[1] == 103 && header[2] == 103 && header[3] == 83) {
            return new FlacOggFile(new OggFile(inp));
        }
        if (header[0] == 102 && header[1] == 76 && header[2] == 97 && header[3] == 67) {
            return new FlacNativeFile(inp);
        }
        throw new IllegalArgumentException("File type not recognised");
    }
    
    public static FlacFile open(final OggFile ogg) throws IOException {
        return new FlacOggFile(ogg);
    }
    
    public abstract FlacAudioFrame getNextAudioPacket() throws IOException;
    
    public abstract void skipToGranule(final long p0) throws IOException;
    
    public FlacInfo getInfo() {
        return this.info;
    }
    
    public FlacTags getTags() {
        return this.tags;
    }
    
    public abstract void close() throws IOException;
}
