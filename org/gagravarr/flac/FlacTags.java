// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import java.io.IOException;
import java.io.OutputStream;
import org.gagravarr.ogg.IOUtils;
import org.gagravarr.ogg.OggPacket;
import org.gagravarr.vorbis.VorbisComments;

public class FlacTags extends VorbisComments
{
    public FlacTags(final OggPacket packet) {
        super(packet);
        final byte type = this.getData()[0];
        if (type != 4) {
            throw new IllegalArgumentException("Invalid type " + type);
        }
    }
    
    public FlacTags() {
    }
    
    @Override
    protected int getHeaderSize() {
        return 4;
    }
    
    @Override
    protected void populateMetadataHeader(final byte[] b, final int type, final int dataLength) {
        b[0] = 4;
        IOUtils.putInt3BE(b, 1, dataLength);
    }
    
    protected static class FlacTagsAsMetadata extends FlacMetadataBlock
    {
        private FlacTags tags;
        
        protected FlacTagsAsMetadata(final byte[] data) {
            super((byte)4);
            final byte[] d = new byte[data.length + 4];
            d[0] = 4;
            System.arraycopy(data, 0, d, 4, data.length);
            this.tags = new FlacTags(new OggPacket(d));
        }
        
        @Override
        public byte[] getData() {
            return this.tags.getData();
        }
        
        @Override
        protected void write(final OutputStream out) throws IOException {
            throw new IllegalStateException("Must not call directly");
        }
        
        public FlacTags getTags() {
            return this.tags;
        }
    }
}
