// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.flac;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.gagravarr.ogg.IOUtils;
import java.io.InputStream;

public abstract class FlacMetadataBlock extends FlacFrame
{
    public static final byte STREAMINFO = 0;
    public static final byte PADDING = 1;
    public static final byte APPLICATION = 2;
    public static final byte SEEKTABLE = 3;
    public static final byte VORBIS_COMMENT = 4;
    public static final byte CUESHEET = 5;
    public static final byte PICTURE = 6;
    private byte type;
    
    public static FlacMetadataBlock create(final InputStream inp) throws IOException {
        final int typeI = inp.read();
        if (typeI == -1) {
            throw new IllegalArgumentException();
        }
        final byte type = IOUtils.fromInt(typeI);
        final byte[] l = new byte[3];
        IOUtils.readFully(inp, l);
        final int length = (int)IOUtils.getInt3BE(l);
        final byte[] data = new byte[length];
        IOUtils.readFully(inp, data);
        switch (type) {
            case 0: {
                return new FlacInfo(data, 0);
            }
            case 4: {
                return new FlacTags.FlacTagsAsMetadata(data);
            }
            default: {
                return new FlacUnhandledMetadataBlock(type, data);
            }
        }
    }
    
    protected FlacMetadataBlock(final byte type) {
        this.type = type;
    }
    
    public int getType() {
        return this.type & 0x7F;
    }
    
    public boolean isLastMetadataBlock() {
        return this.type < 0;
    }
    
    @Override
    public byte[] getData() {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            baos.write(this.type);
            baos.write(new byte[3]);
            this.write(baos);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        final byte[] data = baos.toByteArray();
        IOUtils.putInt3BE(data, 1, data.length);
        return data;
    }
    
    protected abstract void write(final OutputStream p0) throws IOException;
}
