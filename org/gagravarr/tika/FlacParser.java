// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.tika;

import org.gagravarr.flac.FlacInfo;
import org.xml.sax.SAXException;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import org.gagravarr.vorbis.VorbisComments;
import org.gagravarr.flac.FlacOggFile;
import org.apache.tika.sax.XHTMLContentHandler;
import org.gagravarr.flac.FlacFile;
import org.apache.tika.metadata.XMPDM;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collections;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class FlacParser extends AbstractParser
{
    private static MediaType TYPE;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return Collections.singleton(FlacParser.TYPE);
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, TikaException, SAXException {
        metadata.set("Content-Type", FlacParser.TYPE.toString());
        metadata.set(XMPDM.AUDIO_COMPRESSOR, "FLAC");
        final FlacFile flac = FlacFile.open(stream);
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        this.extractInfo(metadata, flac.getInfo());
        if (flac instanceof FlacOggFile) {
            final FlacOggFile ogg = (FlacOggFile)flac;
            metadata.add("version", "Flac " + ogg.getFirstPacket().getMajorVersion() + "." + ogg.getFirstPacket().getMinorVersion());
        }
        VorbisParser.extractComments(metadata, xhtml, flac.getTags());
        xhtml.endDocument();
    }
    
    protected void extractInfo(final Metadata metadata, final FlacInfo info) throws TikaException {
        metadata.set(XMPDM.AUDIO_SAMPLE_RATE, info.getSampleRate());
        VorbisParser.extractChannelInfo(metadata, info.getNumChannels());
    }
    
    static {
        FlacParser.TYPE = MediaType.audio("x-flac");
    }
}
