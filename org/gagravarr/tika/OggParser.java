// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.tika;

import java.util.Arrays;
import org.xml.sax.SAXException;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import org.gagravarr.ogg.OggPacket;
import org.gagravarr.ogg.OggPacketReader;
import org.gagravarr.flac.FlacFirstOggPacket;
import org.gagravarr.vorbis.VorbisPacket;
import java.util.ArrayList;
import org.gagravarr.ogg.OggFile;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.List;
import org.apache.tika.parser.AbstractParser;

public class OggParser extends AbstractParser
{
    private static List<MediaType> TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return new HashSet<MediaType>(OggParser.TYPES);
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, TikaException, SAXException {
        final OggFile ogg = new OggFile(stream);
        int streams = 0;
        int flacCount = 0;
        int vorbisCount = 0;
        final List<Integer> sids = new ArrayList<Integer>();
        final OggPacketReader r = ogg.getPacketReader();
        OggPacket p;
        while ((p = r.getNextPacket()) != null) {
            if (p.isBeginningOfStream()) {
                ++streams;
                sids.add(p.getSid());
                if (p.getData() == null || p.getData().length <= 10) {
                    continue;
                }
                if (VorbisPacket.isVorbisStream(p)) {
                    ++vorbisCount;
                }
                if (!FlacFirstOggPacket.isFlacStream(p)) {
                    continue;
                }
                ++flacCount;
            }
        }
        metadata.add("streams-total", Integer.toString(streams));
        metadata.add("streams-vorbis", Integer.toString(vorbisCount));
        metadata.add("streams-flac", Integer.toString(flacCount));
    }
    
    static {
        OggParser.TYPES = Arrays.asList(OggDetector.OGG_GENERAL, OggDetector.OGG_VIDEO);
    }
}
