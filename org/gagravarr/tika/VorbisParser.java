// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.tika;

import java.util.Iterator;
import java.util.Arrays;
import org.gagravarr.vorbis.VorbisComments;
import org.gagravarr.vorbis.VorbisInfo;
import org.xml.sax.SAXException;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.gagravarr.vorbis.VorbisFile;
import org.gagravarr.ogg.OggFile;
import org.apache.tika.metadata.XMPDM;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.List;
import org.apache.tika.parser.AbstractParser;

public class VorbisParser extends AbstractParser
{
    private static List<MediaType> TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return new HashSet<MediaType>(VorbisParser.TYPES);
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, TikaException, SAXException {
        metadata.set("Content-Type", OggDetector.OGG_VORBIS.toString());
        metadata.set(XMPDM.AUDIO_COMPRESSOR, "Vorbis");
        final OggFile ogg = new OggFile(stream);
        final VorbisFile vorbis = new VorbisFile(ogg);
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        this.extractInfo(metadata, vorbis.getInfo());
        extractComments(metadata, xhtml, vorbis.getComment());
        xhtml.endDocument();
    }
    
    protected void extractInfo(final Metadata metadata, final VorbisInfo info) throws TikaException {
        metadata.set(XMPDM.AUDIO_SAMPLE_RATE, (int)info.getRate());
        metadata.add("version", "Vorbis " + info.getVersion());
        extractChannelInfo(metadata, info.getChannels());
    }
    
    protected static void extractChannelInfo(final Metadata metadata, final int channelCount) {
        if (channelCount == 1) {
            metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "Mono");
        }
        else if (channelCount == 2) {
            metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "Stereo");
        }
        else if (channelCount == 5) {
            metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "5.1");
        }
        else if (channelCount == 7) {
            metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "7.1");
        }
    }
    
    protected static void extractComments(final Metadata metadata, final XHTMLContentHandler xhtml, final VorbisComments comments) throws TikaException, SAXException {
        metadata.set("title", comments.getTitle());
        metadata.set("Author", comments.getArtist());
        metadata.set(XMPDM.ARTIST, comments.getArtist());
        metadata.set(XMPDM.ALBUM, comments.getAlbum());
        metadata.set(XMPDM.GENRE, comments.getGenre());
        metadata.set(XMPDM.RELEASE_DATE, comments.getDate());
        metadata.add("vendor", comments.getVendor());
        for (final String comment : comments.getComments("comment")) {
            metadata.add(XMPDM.LOG_COMMENT.getName(), comment);
        }
        final List<String> done = Arrays.asList("title", "artist", "album", "genre", "date", "tracknumber", "vendor", "comment");
        for (final String key : comments.getAllComments().keySet()) {
            if (!done.contains(key)) {
                for (final String value : comments.getAllComments().get(key)) {
                    metadata.add(key, value);
                }
            }
        }
        xhtml.element("h1", comments.getTitle());
        xhtml.element("p", comments.getArtist());
        if (comments.getTrackNumber() != null) {
            xhtml.element("p", comments.getAlbum() + ", track " + comments.getTrackNumber());
            metadata.set(XMPDM.TRACK_NUMBER, comments.getTrackNumber());
        }
        else {
            xhtml.element("p", comments.getAlbum());
        }
        xhtml.element("p", comments.getDate());
        for (final String comment2 : comments.getComments("comment")) {
            xhtml.element("p", comment2);
        }
        xhtml.element("p", comments.getGenre());
    }
    
    static {
        VorbisParser.TYPES = Arrays.asList(OggDetector.OGG_AUDIO, OggDetector.OGG_VORBIS);
    }
}
