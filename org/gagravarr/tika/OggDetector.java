// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.tika;

import java.io.IOException;
import org.gagravarr.ogg.OggPacket;
import org.gagravarr.ogg.OggPacketReader;
import java.util.List;
import org.gagravarr.flac.FlacFirstOggPacket;
import org.gagravarr.vorbis.VorbisPacket;
import java.util.ArrayList;
import org.gagravarr.ogg.OggFile;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import org.apache.tika.mime.MediaType;
import org.apache.tika.detect.Detector;

public class OggDetector implements Detector
{
    public static final MediaType OGG_VIDEO;
    public static final MediaType OGG_GENERAL;
    public static final MediaType OGG_AUDIO;
    public static final MediaType OGG_VORBIS;
    public static final MediaType FLAC;
    
    public MediaType detect(final InputStream input, final Metadata metadata) throws IOException {
        if (input == null) {
            return MediaType.OCTET_STREAM;
        }
        input.mark(4);
        try {
            if (input.read() != 79 || input.read() != 103 || input.read() != 103 || input.read() != 83) {
                return MediaType.OCTET_STREAM;
            }
        }
        finally {
            input.reset();
        }
        final TikaInputStream tis = TikaInputStream.cast(input);
        if (tis != null) {
            tis.mark((int)tis.getLength() + 1);
            final OggFile ogg = new OggFile(tis);
            int streams = 0;
            int flacCount = 0;
            int vorbisCount = 0;
            final List<Integer> sids = new ArrayList<Integer>();
            final OggPacketReader r = ogg.getPacketReader();
            OggPacket p;
            while ((p = r.getNextPacket()) != null) {
                if (p.isBeginningOfStream()) {
                    ++streams;
                    sids.add(p.getSid());
                    if (p.getData() == null || p.getData().length <= 10) {
                        continue;
                    }
                    if (VorbisPacket.isVorbisStream(p)) {
                        ++vorbisCount;
                    }
                    if (!FlacFirstOggPacket.isFlacStream(p)) {
                        continue;
                    }
                    ++flacCount;
                }
            }
            tis.reset();
            if (vorbisCount == 1 && streams == 1) {
                return OggDetector.OGG_VORBIS;
            }
            if (flacCount == 1 && streams == 1) {
                return OggDetector.FLAC;
            }
            if (vorbisCount > 1 && vorbisCount == streams) {
                return OggDetector.OGG_VORBIS;
            }
            if (flacCount > 1 && flacCount == streams) {
                return OggDetector.FLAC;
            }
            if (streams <= 0) {
                return OggDetector.OGG_GENERAL;
            }
        }
        return OggDetector.OGG_GENERAL;
    }
    
    static {
        OGG_VIDEO = MediaType.video("ogg");
        OGG_GENERAL = MediaType.application("ogg");
        OGG_AUDIO = MediaType.audio("ogg");
        OGG_VORBIS = MediaType.audio("vorbis");
        FLAC = MediaType.audio("x-flac");
    }
}
