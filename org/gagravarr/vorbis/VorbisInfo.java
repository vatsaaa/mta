// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import org.gagravarr.ogg.IOUtils;
import org.gagravarr.ogg.OggPacket;

public class VorbisInfo extends VorbisPacket
{
    private int version;
    private int channels;
    private long rate;
    private int bitrateUpper;
    private int bitrateNominal;
    private int bitrateLower;
    private int blocksizes;
    
    public VorbisInfo() {
        this.version = 0;
    }
    
    public VorbisInfo(final OggPacket pkt) {
        super(pkt);
        final byte[] data = this.getData();
        this.version = (int)IOUtils.getInt4(data, 7);
        if (this.version != 0) {
            throw new IllegalArgumentException("Unsupported vorbis version " + this.version + " detected");
        }
        this.channels = data[11];
        this.rate = IOUtils.getInt4(data, 12);
        this.bitrateUpper = (int)IOUtils.getInt4(data, 16);
        this.bitrateNominal = (int)IOUtils.getInt4(data, 20);
        this.bitrateLower = (int)IOUtils.getInt4(data, 24);
        this.blocksizes = IOUtils.toInt(data[28]);
        final byte framingBit = data[29];
        if (framingBit == 0) {
            throw new IllegalArgumentException("Framing bit not set, invalid");
        }
    }
    
    @Override
    protected int getHeaderSize() {
        return 7;
    }
    
    @Override
    public OggPacket write() {
        final byte[] data = new byte[30];
        this.populateMetadataHeader(data, 1, data.length);
        IOUtils.putInt4(data, 7, this.version);
        data[11] = IOUtils.fromInt(this.channels);
        IOUtils.putInt4(data, 12, this.rate);
        IOUtils.putInt4(data, 16, this.bitrateUpper);
        IOUtils.putInt4(data, 20, this.bitrateNominal);
        IOUtils.putInt4(data, 24, this.bitrateLower);
        data[28] = IOUtils.fromInt(this.blocksizes);
        data[29] = 1;
        this.setData(data);
        return super.write();
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public int getChannels() {
        return this.channels;
    }
    
    public void setChannels(final int channels) {
        this.channels = channels;
    }
    
    public long getRate() {
        return this.rate;
    }
    
    public void setRate(final long rate) {
        this.rate = rate;
    }
    
    public int getBitrateUpper() {
        return this.bitrateUpper;
    }
    
    public void setBitrateUpper(final int bitrateUpper) {
        this.bitrateUpper = bitrateUpper;
    }
    
    public int getBitrateNominal() {
        return this.bitrateNominal;
    }
    
    public void setBitrateNominal(final int bitrateNominal) {
        this.bitrateNominal = bitrateNominal;
    }
    
    public int getBitrateLower() {
        return this.bitrateLower;
    }
    
    public void setBitrateLower(final int bitrateLower) {
        this.bitrateLower = bitrateLower;
    }
    
    public int getBlocksize0() {
        final int part = this.blocksizes & 0xF;
        return (int)Math.pow(2.0, part);
    }
    
    public void setBlocksize0(final int blocksize) {
        final int part = (int)(Math.log(blocksize) / Math.log(2.0));
        this.blocksizes = (this.blocksizes & 0xF0) + part;
    }
    
    public int getBlocksize1() {
        final int part = (this.blocksizes & 0xF0) >> 4;
        return (int)Math.pow(2.0, part);
    }
    
    public void setBlocksize1(final int blocksize) {
        final int part = (int)(Math.log(blocksize) / Math.log(2.0));
        this.blocksizes = (this.blocksizes & 0xF) + (part << 4);
    }
}
