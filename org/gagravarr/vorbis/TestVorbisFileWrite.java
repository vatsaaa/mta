// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.gagravarr.ogg.OggFile;
import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestVorbisFileWrite extends TestCase
{
    private InputStream getTestFile() throws IOException {
        return this.getClass().getResourceAsStream("/testVORBIS.ogg");
    }
    
    public void testReadWrite() throws IOException {
        final OggFile in = new OggFile(this.getTestFile());
        final VorbisFile vfIN = new VorbisFile(in);
        final int infoSize = vfIN.getInfo().getData().length;
        final int commentSize = vfIN.getComment().getData().length;
        final int setupSize = vfIN.getSetup().getData().length;
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final VorbisFile vfOUT = new VorbisFile(baos, vfIN.getInfo(), vfIN.getComment(), vfIN.getSetup());
        VorbisAudioData vad;
        while ((vad = vfIN.getNextAudioPacket()) != null) {
            vfOUT.writeAudioData(vad);
        }
        vfIN.close();
        vfOUT.close();
        assertEquals(infoSize, vfOUT.getInfo().getData().length);
        assertEquals(commentSize, vfOUT.getComment().getData().length);
        assertEquals(setupSize, vfOUT.getSetup().getData().length);
    }
    
    public void testReadWriteRead() throws IOException {
        final OggFile in = new OggFile(this.getTestFile());
        final VorbisFile vfOrig = new VorbisFile(in);
        final int infoSize = vfOrig.getInfo().getData().length;
        final int commentSize = vfOrig.getComment().getData().length;
        final int setupSize = vfOrig.getSetup().getData().length;
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final VorbisFile vfOUT = new VorbisFile(baos, vfOrig.getInfo(), vfOrig.getComment(), vfOrig.getSetup());
        VorbisAudioData vad;
        while ((vad = vfOrig.getNextAudioPacket()) != null) {
            vfOUT.writeAudioData(vad);
        }
        vfOrig.close();
        vfOUT.close();
        final VorbisFile vfIN = new VorbisFile(new OggFile(new ByteArrayInputStream(baos.toByteArray())));
        assertEquals(2, vfIN.getInfo().getChannels());
        assertEquals(44100L, vfIN.getInfo().getRate());
        assertEquals(0, vfIN.getInfo().getBitrateLower());
        assertEquals(0, vfIN.getInfo().getBitrateUpper());
        assertEquals(80000, vfIN.getInfo().getBitrateNominal());
        assertEquals("Test Title", vfIN.getComment().getTitle());
        assertEquals("Test Artist", vfIN.getComment().getArtist());
        assertNotNull((Object)vfIN.getNextAudioPacket());
        assertNotNull((Object)vfIN.getNextAudioPacket());
        assertNotNull((Object)vfIN.getNextAudioPacket());
        assertNotNull((Object)vfIN.getNextAudioPacket());
        final VorbisAudioData ad = vfIN.getNextAudioPacket();
        assertEquals(960L, ad.getGranulePosition());
        assertEquals(infoSize, vfOUT.getInfo().getData().length);
        assertEquals(commentSize, vfOUT.getComment().getData().length);
        assertEquals(setupSize, vfOUT.getSetup().getData().length);
        assertEquals(infoSize, vfIN.getInfo().getData().length);
        assertEquals(commentSize, vfIN.getComment().getData().length);
        assertEquals(setupSize, vfIN.getSetup().getData().length);
    }
}
