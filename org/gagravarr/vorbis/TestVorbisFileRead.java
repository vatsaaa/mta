// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import org.gagravarr.ogg.OggFile;
import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestVorbisFileRead extends TestCase
{
    private InputStream getTestFile() throws IOException {
        return this.getClass().getResourceAsStream("/testVORBIS.ogg");
    }
    
    public void testRead() throws IOException {
        final OggFile ogg = new OggFile(this.getTestFile());
        final VorbisFile vf = new VorbisFile(ogg);
        assertEquals(2, vf.getInfo().getChannels());
        assertEquals(44100L, vf.getInfo().getRate());
        assertEquals(0, vf.getInfo().getBitrateLower());
        assertEquals(0, vf.getInfo().getBitrateUpper());
        assertEquals(80000, vf.getInfo().getBitrateNominal());
        assertEquals("Test Title", vf.getComment().getTitle());
        assertEquals("Test Artist", vf.getComment().getArtist());
        assertEquals(3484, vf.getSetup().getData().length);
        assertNotNull((Object)vf.getNextAudioPacket());
        assertNotNull((Object)vf.getNextAudioPacket());
        assertNotNull((Object)vf.getNextAudioPacket());
        assertNotNull((Object)vf.getNextAudioPacket());
        final VorbisAudioData ad = vf.getNextAudioPacket();
        assertEquals(960L, ad.getGranulePosition());
    }
}
