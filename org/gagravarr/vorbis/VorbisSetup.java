// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import org.gagravarr.ogg.IOUtils;
import org.gagravarr.ogg.OggPacket;

public class VorbisSetup extends VorbisPacket
{
    public VorbisSetup(final OggPacket pkt) {
        super(pkt);
    }
    
    public VorbisSetup() {
    }
    
    @Override
    protected int getHeaderSize() {
        return 7;
    }
    
    public int getNumberOfCodebooks() {
        final byte[] data = this.getData();
        int number = -1;
        if (data != null && data.length >= 10) {
            number = IOUtils.toInt(data[8]);
        }
        return number + 1;
    }
}
