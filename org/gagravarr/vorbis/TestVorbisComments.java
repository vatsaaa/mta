// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import org.gagravarr.ogg.OggPacket;
import java.util.List;
import java.util.ArrayList;
import org.gagravarr.ogg.OggPacketReader;
import org.gagravarr.ogg.OggFile;
import java.io.IOException;
import java.io.InputStream;
import junit.framework.TestCase;

public class TestVorbisComments extends TestCase
{
    private InputStream getTestFile() throws IOException {
        return this.getClass().getResourceAsStream("/testVORBIS.ogg");
    }
    
    public void testRead() throws IOException {
        final OggFile ogg = new OggFile(this.getTestFile());
        final OggPacketReader r = ogg.getPacketReader();
        r.getNextPacket();
        final VorbisPacket p = VorbisPacket.create(r.getNextPacket());
        assertEquals((Object)VorbisComments.class, (Object)p.getClass());
        final VorbisComments c = (VorbisComments)p;
        assertEquals("Xiph.Org libVorbis I 20070622", c.getVendor());
        assertEquals(7, c.getAllComments().size());
        assertEquals("Test Title", c.getTitle());
        assertEquals(1, c.getComments("TiTlE").size());
        assertEquals("Test Title", (String)c.getComments("tItLe").get(0));
        assertEquals("Test Artist", c.getArtist());
        assertEquals(1, c.getComments("ArTiSt").size());
        assertEquals("Test Artist", (String)c.getComments("aRTiST").get(0));
        assertEquals("Test Genre", c.getGenre());
        assertEquals(1, c.getComments("GEnRE").size());
        assertEquals("Test Genre", (String)c.getComments("genRE").get(0));
        assertEquals("Test Album", c.getAlbum());
        assertEquals(1, c.getComments("alBUm").size());
        assertEquals("Test Album", (String)c.getComments("ALbUM").get(0));
        assertEquals(1, c.getComments("DAte").size());
        assertEquals("2010-01-26", (String)c.getComments("dAtE").get(0));
        assertEquals(2, c.getComments("COmmENT").size());
        assertEquals("Test Comment", (String)c.getComments("COMMent").get(0));
        assertEquals("Another Test Comment", (String)c.getComments("COMMent").get(1));
    }
    
    public void testModify() throws IOException {
        final VorbisComments c = new VorbisComments();
        assertEquals(0, c.getComments("a").size());
        assertEquals(0, c.getAllComments().size());
        assertEquals((String)null, c.getAlbum());
        c.addComment("a", "1");
        assertEquals(1, c.getComments("a").size());
        assertEquals("1", (String)c.getComments("a").get(0));
        c.addComment("A", "2");
        c.addComment("a", "3");
        assertEquals(3, c.getComments("A").size());
        assertEquals("1", (String)c.getComments("a").get(0));
        assertEquals("2", (String)c.getComments("a").get(1));
        assertEquals("3", (String)c.getComments("a").get(2));
        c.setComments("a", new ArrayList<String>());
        assertEquals(0, c.getComments("a").size());
        c.addComment("a", "F");
        assertEquals(1, c.getComments("a").size());
        assertEquals("F", (String)c.getComments("a").get(0));
        c.removeComments("A");
        assertEquals(0, c.getComments("a").size());
        assertEquals(0, c.getAllComments().size());
    }
    
    public void testWrite() throws IOException {
        final VorbisComments c = new VorbisComments();
        c.setVendor("A1");
        c.addComment("b", "c2a");
        c.addComment("B", "c2b");
        c.addComment("1", "c1");
        final OggPacket p = c.write();
        assertEquals(44, p.getData().length);
        assertEquals(3, (int)p.getData()[0]);
        assertEquals(118, (int)p.getData()[1]);
        assertEquals(111, (int)p.getData()[2]);
        assertEquals(114, (int)p.getData()[3]);
        assertEquals(98, (int)p.getData()[4]);
        assertEquals(105, (int)p.getData()[5]);
        assertEquals(115, (int)p.getData()[6]);
        assertEquals(2, (int)p.getData()[7]);
        assertEquals(0, (int)p.getData()[8]);
        assertEquals(0, (int)p.getData()[9]);
        assertEquals(0, (int)p.getData()[10]);
        assertEquals(65, (int)p.getData()[11]);
        assertEquals(49, (int)p.getData()[12]);
        assertEquals(3, (int)p.getData()[13]);
        assertEquals(0, (int)p.getData()[14]);
        assertEquals(0, (int)p.getData()[15]);
        assertEquals(0, (int)p.getData()[16]);
        assertEquals(4, (int)p.getData()[17]);
        assertEquals(0, (int)p.getData()[18]);
        assertEquals(0, (int)p.getData()[19]);
        assertEquals(0, (int)p.getData()[20]);
        assertEquals(49, (int)p.getData()[21]);
        assertEquals(61, (int)p.getData()[22]);
        assertEquals(99, (int)p.getData()[23]);
        assertEquals(49, (int)p.getData()[24]);
        assertEquals(5, (int)p.getData()[25]);
        assertEquals(0, (int)p.getData()[26]);
        assertEquals(0, (int)p.getData()[27]);
        assertEquals(0, (int)p.getData()[28]);
        assertEquals(98, (int)p.getData()[29]);
        assertEquals(61, (int)p.getData()[30]);
        assertEquals(99, (int)p.getData()[31]);
        assertEquals(50, (int)p.getData()[32]);
        assertEquals(97, (int)p.getData()[33]);
        assertEquals(5, (int)p.getData()[34]);
        assertEquals(0, (int)p.getData()[35]);
        assertEquals(0, (int)p.getData()[36]);
        assertEquals(0, (int)p.getData()[37]);
        assertEquals(98, (int)p.getData()[38]);
        assertEquals(61, (int)p.getData()[39]);
        assertEquals(99, (int)p.getData()[40]);
        assertEquals(50, (int)p.getData()[41]);
        assertEquals(98, (int)p.getData()[42]);
        assertEquals(1, (int)p.getData()[43]);
    }
}
