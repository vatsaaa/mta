// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import org.gagravarr.ogg.OggPacket;

public class VorbisAudioData extends VorbisPacket
{
    private long granulePosition;
    
    public VorbisAudioData(final OggPacket pkt) {
        super(pkt);
        this.granulePosition = pkt.getGranulePosition();
    }
    
    public VorbisAudioData(final byte[] data) {
        this.setData(data);
        this.granulePosition = -1L;
    }
    
    @Override
    protected int getHeaderSize() {
        return 0;
    }
    
    public long getGranulePosition() {
        return this.granulePosition;
    }
    
    public void setGranulePosition(final long granulePosition) {
        this.granulePosition = granulePosition;
    }
}
