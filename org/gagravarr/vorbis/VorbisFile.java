// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import java.util.Iterator;
import java.util.ArrayList;
import java.io.OutputStream;
import org.gagravarr.ogg.OggPacket;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.util.List;
import org.gagravarr.ogg.OggPacketWriter;
import org.gagravarr.ogg.OggPacketReader;
import org.gagravarr.ogg.OggFile;

public class VorbisFile
{
    private OggFile ogg;
    private OggPacketReader r;
    private OggPacketWriter w;
    private int sid;
    private VorbisInfo info;
    private VorbisComments comment;
    private VorbisSetup setup;
    private List<VorbisAudioData> writtenPackets;
    
    public VorbisFile(final File f) throws IOException, FileNotFoundException {
        this(new OggFile(new FileInputStream(f)));
    }
    
    public VorbisFile(final OggFile ogg) throws IOException {
        this(ogg.getPacketReader());
        this.ogg = ogg;
    }
    
    public VorbisFile(final OggPacketReader r) throws IOException {
        this.sid = -1;
        this.r = r;
        OggPacket p = null;
        while ((p = r.getNextPacket()) != null) {
            if (p.isBeginningOfStream() && p.getData().length > 10) {
                try {
                    VorbisPacket.create(p);
                    this.sid = p.getSid();
                }
                catch (IllegalArgumentException e) {
                    continue;
                }
                break;
            }
        }
        this.info = (VorbisInfo)VorbisPacket.create(p);
        this.comment = (VorbisComments)VorbisPacket.create(r.getNextPacketWithSid(this.sid));
        this.setup = (VorbisSetup)VorbisPacket.create(r.getNextPacketWithSid(this.sid));
    }
    
    public VorbisFile(final OutputStream out) {
        this(out, new VorbisInfo(), new VorbisComments(), new VorbisSetup());
    }
    
    public VorbisFile(final OutputStream out, final VorbisInfo info, final VorbisComments comments, final VorbisSetup setup) {
        this(out, -1, info, comments, setup);
    }
    
    public VorbisFile(final OutputStream out, final int sid, final VorbisInfo info, final VorbisComments comments, final VorbisSetup setup) {
        this.sid = -1;
        this.ogg = new OggFile(out);
        if (sid > 0) {
            this.w = this.ogg.getPacketWriter(sid);
            this.sid = sid;
        }
        else {
            this.w = this.ogg.getPacketWriter();
            this.sid = this.w.getSid();
        }
        this.writtenPackets = new ArrayList<VorbisAudioData>();
        this.info = info;
        this.comment = comments;
        this.setup = setup;
    }
    
    public VorbisAudioData getNextAudioPacket() throws IOException {
        OggPacket p = null;
        VorbisPacket vp = null;
        while ((p = this.r.getNextPacketWithSid(this.sid)) != null) {
            vp = VorbisPacket.create(p);
            if (vp instanceof VorbisAudioData) {
                return (VorbisAudioData)vp;
            }
            System.err.println("Skipping non audio packet " + vp + " mid audio stream");
        }
        return null;
    }
    
    public void skipToGranule(final long granulePosition) throws IOException {
        this.r.skipToGranulePosition(this.sid, granulePosition);
    }
    
    public int getSid() {
        return this.sid;
    }
    
    public VorbisInfo getInfo() {
        return this.info;
    }
    
    public VorbisComments getComment() {
        return this.comment;
    }
    
    public VorbisSetup getSetup() {
        return this.setup;
    }
    
    public void writeAudioData(final VorbisAudioData data) {
        this.writtenPackets.add(data);
    }
    
    public void close() throws IOException {
        if (this.r != null) {
            this.r = null;
            this.ogg.close();
            this.ogg = null;
        }
        if (this.w != null) {
            this.w.bufferPacket(this.info.write(), true);
            this.w.bufferPacket(this.comment.write(), false);
            this.w.bufferPacket(this.setup.write(), true);
            long lastGranule = 0L;
            for (final VorbisAudioData vd : this.writtenPackets) {
                if (vd.getGranulePosition() >= 0L && lastGranule != vd.getGranulePosition()) {
                    this.w.flush();
                    lastGranule = vd.getGranulePosition();
                    this.w.setGranulePosition(lastGranule);
                }
                this.w.bufferPacket(vd.write());
                if (this.w.getSizePendingFlush() > 16384) {
                    this.w.flush();
                }
            }
            this.w.close();
            this.w = null;
            this.ogg.close();
            this.ogg = null;
        }
    }
    
    public OggFile getOggFile() {
        return this.ogg;
    }
}
