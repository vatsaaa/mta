// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import org.gagravarr.ogg.IOUtils;
import org.gagravarr.ogg.OggPacket;
import org.gagravarr.ogg.HighLevelOggStreamPacket;

public abstract class VorbisPacket extends HighLevelOggStreamPacket
{
    public static final int TYPE_INFO = 1;
    public static final int TYPE_COMMENTS = 3;
    public static final int TYPE_SETUP = 5;
    protected static final int HEADER_LENGTH_METADATA = 7;
    protected static final int HEADER_LENGTH_AUDIO = 0;
    
    protected VorbisPacket(final OggPacket oggPacket) {
        super(oggPacket);
    }
    
    protected VorbisPacket() {
    }
    
    protected abstract int getHeaderSize();
    
    protected void populateMetadataHeader(final byte[] b, final int type, final int dataLength) {
        b[0] = IOUtils.fromInt(type);
        b[1] = 118;
        b[2] = 111;
        b[3] = 114;
        b[4] = 98;
        b[5] = 105;
        b[6] = 115;
    }
    
    public static boolean isVorbisStream(final OggPacket firstPacket) {
        return firstPacket.isBeginningOfStream() && isVorbisSpecial(firstPacket);
    }
    
    private static boolean isVorbisSpecial(final OggPacket packet) {
        final byte type = packet.getData()[0];
        if (type == 1 || type == 3 || type == 5) {
            final byte[] d = packet.getData();
            if (d[1] == 118 && d[2] == 111 && d[3] == 114 && d[4] == 98 && d[5] == 105 && d[6] == 115) {
                return true;
            }
        }
        return false;
    }
    
    public static VorbisPacket create(final OggPacket packet) {
        final byte type = packet.getData()[0];
        if (isVorbisSpecial(packet)) {
            switch (type) {
                case 1: {
                    return new VorbisInfo(packet);
                }
                case 3: {
                    return new VorbisComments(packet);
                }
                case 5: {
                    return new VorbisSetup(packet);
                }
            }
        }
        return new VorbisAudioData(packet);
    }
}
