// 
// Decompiled by Procyon v0.5.36
// 

package org.gagravarr.vorbis;

import java.util.Iterator;
import java.io.IOException;
import java.util.Arrays;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import org.gagravarr.ogg.IOUtils;
import java.util.HashMap;
import org.gagravarr.ogg.OggPacket;
import java.util.List;
import java.util.Map;

public class VorbisComments extends VorbisPacket
{
    public static final String KEY_ARTIST = "artist";
    public static final String KEY_ALBUM = "album";
    public static final String KEY_TITLE = "title";
    public static final String KEY_GENRE = "genre";
    public static final String KEY_TRACKNUMBER = "tracknumber";
    public static final String KEY_DATE = "date";
    private String vendor;
    private Map<String, List<String>> comments;
    
    public VorbisComments(final OggPacket pkt) {
        super(pkt);
        this.comments = new HashMap<String, List<String>>();
        final byte[] d = pkt.getData();
        final int dataBeginsAt = this.getHeaderSize();
        final int vlen = (int)IOUtils.getInt4(d, dataBeginsAt);
        this.vendor = IOUtils.getUTF8(d, dataBeginsAt + 4, vlen);
        int offset = dataBeginsAt + 4 + vlen;
        final int numComments = (int)IOUtils.getInt4(d, offset);
        offset += 4;
        for (int i = 0; i < numComments; ++i) {
            final int len = (int)IOUtils.getInt4(d, offset);
            offset += 4;
            final String c = IOUtils.getUTF8(d, offset, len);
            offset += len;
            final int equals = c.indexOf(61);
            if (equals == -1) {
                System.err.println("Warning - unable to parse comment '" + c + "'");
            }
            else {
                final String tag = normaliseTag(c.substring(0, equals));
                final String value = c.substring(equals + 1);
                this.addComment(tag, value);
            }
        }
        if (offset < d.length) {
            final byte framingBit = d[offset];
            if (framingBit == 0) {
                throw new IllegalArgumentException("Framing bit not set, invalid");
            }
        }
    }
    
    public VorbisComments() {
        this.comments = new HashMap<String, List<String>>();
        this.vendor = "Xiph.org Java Vorbis Tools 20100203";
    }
    
    @Override
    protected int getHeaderSize() {
        return 7;
    }
    
    public String getVendor() {
        return this.vendor;
    }
    
    public void setVendor(final String vendor) {
        this.vendor = vendor;
    }
    
    protected static String normaliseTag(final String tag) {
        final StringBuffer nt = new StringBuffer();
        for (final char c : tag.toLowerCase().toCharArray()) {
            if (c >= ' ' && c <= '}' && c != '=') {
                nt.append(c);
            }
        }
        return nt.toString();
    }
    
    protected String getSingleComment(final String normalisedTag) {
        final List<String> c = this.comments.get(normalisedTag);
        if (c != null && c.size() > 0) {
            return c.get(0);
        }
        return null;
    }
    
    public String getArtist() {
        return this.getSingleComment("artist");
    }
    
    public String getAlbum() {
        return this.getSingleComment("album");
    }
    
    public String getTitle() {
        return this.getSingleComment("title");
    }
    
    public String getGenre() {
        return this.getSingleComment("genre");
    }
    
    public String getTrackNumber() {
        return this.getSingleComment("tracknumber");
    }
    
    public int getTrackNumberNumeric() {
        final String number = this.getTrackNumber();
        if (number == null) {
            return -1;
        }
        try {
            return Integer.parseInt(number);
        }
        catch (NumberFormatException e) {
            return -1;
        }
    }
    
    public String getDate() {
        return this.getSingleComment("date");
    }
    
    public List<String> getComments(final String tag) {
        final List<String> c = this.comments.get(normaliseTag(tag));
        if (c == null) {
            return new ArrayList<String>();
        }
        return c;
    }
    
    public void removeComments(final String tag) {
        this.comments.remove(normaliseTag(tag));
    }
    
    public void removeAllComments() {
        this.comments.clear();
    }
    
    public void addComment(final String tag, final String comment) {
        final String nt = normaliseTag(tag);
        if (!this.comments.containsKey(nt)) {
            this.comments.put(nt, new ArrayList<String>());
        }
        this.comments.get(nt).add(comment);
    }
    
    public void setComments(final String tag, final List<String> comments) {
        final String nt = normaliseTag(tag);
        if (this.comments.containsKey(nt)) {
            this.comments.remove(nt);
        }
        this.comments.put(nt, comments);
    }
    
    public Map<String, List<String>> getAllComments() {
        return this.comments;
    }
    
    @Override
    public OggPacket write() {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            final byte[] header = new byte[this.getHeaderSize()];
            baos.write(header);
            IOUtils.writeUTF8WithLength(baos, this.vendor);
            int numComments = 0;
            for (final List<String> c : this.comments.values()) {
                numComments += c.size();
            }
            IOUtils.writeInt4(baos, numComments);
            final String[] tags = this.comments.keySet().toArray(new String[this.comments.size()]);
            Arrays.sort(tags);
            for (final String tag : tags) {
                for (final String value : this.comments.get(tag)) {
                    final String comment = tag + '=' + value;
                    IOUtils.writeUTF8WithLength(baos, comment);
                }
            }
            baos.write(1);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        final byte[] data = baos.toByteArray();
        this.populateMetadataHeader(data, 3, data.length);
        this.setData(data);
        return super.write();
    }
}
