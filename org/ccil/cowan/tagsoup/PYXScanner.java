// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.Reader;

public class PYXScanner implements Scanner
{
    public void resetDocumentLocator(final String publicid, final String systemid) {
    }
    
    public void scan(final Reader r, final ScanHandler h) throws IOException, SAXException {
        final BufferedReader br = new BufferedReader(r);
        char[] buff = null;
        boolean instag = false;
        String s;
        while ((s = br.readLine()) != null) {
            final int size = s.length();
            if (buff == null || buff.length < size) {
                buff = new char[size];
            }
            s.getChars(0, size, buff, 0);
            switch (buff[0]) {
                case '(': {
                    if (instag) {
                        h.stagc(buff, 0, 0);
                        instag = false;
                    }
                    h.gi(buff, 1, size - 1);
                    instag = true;
                    continue;
                }
                case ')': {
                    if (instag) {
                        h.stagc(buff, 0, 0);
                        instag = false;
                    }
                    h.etag(buff, 1, size - 1);
                    continue;
                }
                case '?': {
                    if (instag) {
                        h.stagc(buff, 0, 0);
                        instag = false;
                    }
                    h.pi(buff, 1, size - 1);
                    continue;
                }
                case 'A': {
                    final int sp = s.indexOf(32);
                    h.aname(buff, 1, sp - 1);
                    h.aval(buff, sp + 1, size - sp - 1);
                    continue;
                }
                case '-': {
                    if (instag) {
                        h.stagc(buff, 0, 0);
                        instag = false;
                    }
                    if (s.equals("-\\n")) {
                        buff[0] = '\n';
                        h.pcdata(buff, 0, 1);
                        continue;
                    }
                    h.pcdata(buff, 1, size - 1);
                    continue;
                }
                case 'E': {
                    if (instag) {
                        h.stagc(buff, 0, 0);
                        instag = false;
                    }
                    h.entity(buff, 1, size - 1);
                    continue;
                }
            }
        }
        h.eof(buff, 0, 0);
    }
    
    public void startCDATA() {
    }
    
    public static void main(final String[] argv) throws IOException, SAXException {
        final Scanner s = new PYXScanner();
        final Reader r = new InputStreamReader(System.in, "UTF-8");
        final Writer w = new BufferedWriter(new OutputStreamWriter(System.out, "UTF-8"));
        s.scan(r, new PYXWriter(w));
    }
}
