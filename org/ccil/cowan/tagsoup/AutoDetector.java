// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

import java.io.Reader;
import java.io.InputStream;

public interface AutoDetector
{
    Reader autoDetectingReader(final InputStream p0);
}
