// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

public class ElementType
{
    private String theName;
    private String theNamespace;
    private String theLocalName;
    private int theModel;
    private int theMemberOf;
    private int theFlags;
    private AttributesImpl theAtts;
    private ElementType theParent;
    private Schema theSchema;
    
    public ElementType(final String name, final int model, final int memberOf, final int flags, final Schema schema) {
        this.theName = name;
        this.theModel = model;
        this.theMemberOf = memberOf;
        this.theFlags = flags;
        this.theAtts = new AttributesImpl();
        this.theSchema = schema;
        this.theNamespace = this.namespace(name, false);
        this.theLocalName = this.localName(name);
    }
    
    public String namespace(final String name, final boolean attribute) {
        final int colon = name.indexOf(58);
        if (colon == -1) {
            return attribute ? "" : this.theSchema.getURI();
        }
        final String prefix = name.substring(0, colon);
        if (prefix.equals("xml")) {
            return "http://www.w3.org/XML/1998/namespace";
        }
        return ("urn:x-prefix:" + prefix).intern();
    }
    
    public String localName(final String name) {
        final int colon = name.indexOf(58);
        if (colon == -1) {
            return name;
        }
        return name.substring(colon + 1).intern();
    }
    
    public String name() {
        return this.theName;
    }
    
    public String namespace() {
        return this.theNamespace;
    }
    
    public String localName() {
        return this.theLocalName;
    }
    
    public int model() {
        return this.theModel;
    }
    
    public int memberOf() {
        return this.theMemberOf;
    }
    
    public int flags() {
        return this.theFlags;
    }
    
    public AttributesImpl atts() {
        return this.theAtts;
    }
    
    public ElementType parent() {
        return this.theParent;
    }
    
    public Schema schema() {
        return this.theSchema;
    }
    
    public boolean canContain(final ElementType other) {
        return (this.theModel & other.theMemberOf) != 0x0;
    }
    
    public void setAttribute(final AttributesImpl atts, String name, String type, String value) {
        if (name.equals("xmlns") || name.startsWith("xmlns:")) {
            return;
        }
        final String namespace = this.namespace(name, true);
        final String localName = this.localName(name);
        final int i = atts.getIndex(name);
        if (i == -1) {
            name = name.intern();
            if (type == null) {
                type = "CDATA";
            }
            if (!type.equals("CDATA")) {
                value = normalize(value);
            }
            atts.addAttribute(namespace, localName, name, type, value);
        }
        else {
            if (type == null) {
                type = atts.getType(i);
            }
            if (!type.equals("CDATA")) {
                value = normalize(value);
            }
            atts.setAttribute(i, namespace, localName, name, type, value);
        }
    }
    
    public static String normalize(String value) {
        if (value == null) {
            return value;
        }
        value = value.trim();
        if (value.indexOf("  ") == -1) {
            return value;
        }
        boolean space = false;
        final int len = value.length();
        final StringBuffer b = new StringBuffer(len);
        for (int i = 0; i < len; ++i) {
            final char v = value.charAt(i);
            if (v == ' ') {
                if (!space) {
                    b.append(v);
                }
                space = true;
            }
            else {
                b.append(v);
                space = false;
            }
        }
        return b.toString();
    }
    
    public void setAttribute(final String name, final String type, final String value) {
        this.setAttribute(this.theAtts, name, type, value);
    }
    
    public void setModel(final int model) {
        this.theModel = model;
    }
    
    public void setMemberOf(final int memberOf) {
        this.theMemberOf = memberOf;
    }
    
    public void setFlags(final int flags) {
        this.theFlags = flags;
    }
    
    public void setParent(final ElementType parent) {
        this.theParent = parent;
    }
}
