// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

import org.xml.sax.Attributes;

public class Element
{
    private ElementType theType;
    private AttributesImpl theAtts;
    private Element theNext;
    private boolean preclosed;
    
    public Element(final ElementType type, final boolean defaultAttributes) {
        this.theType = type;
        if (defaultAttributes) {
            this.theAtts = new AttributesImpl(type.atts());
        }
        else {
            this.theAtts = new AttributesImpl();
        }
        this.theNext = null;
        this.preclosed = false;
    }
    
    public ElementType type() {
        return this.theType;
    }
    
    public AttributesImpl atts() {
        return this.theAtts;
    }
    
    public Element next() {
        return this.theNext;
    }
    
    public void setNext(final Element next) {
        this.theNext = next;
    }
    
    public String name() {
        return this.theType.name();
    }
    
    public String namespace() {
        return this.theType.namespace();
    }
    
    public String localName() {
        return this.theType.localName();
    }
    
    public int model() {
        return this.theType.model();
    }
    
    public int memberOf() {
        return this.theType.memberOf();
    }
    
    public int flags() {
        return this.theType.flags();
    }
    
    public ElementType parent() {
        return this.theType.parent();
    }
    
    public boolean canContain(final Element other) {
        return this.theType.canContain(other.theType);
    }
    
    public void setAttribute(final String name, final String type, final String value) {
        this.theType.setAttribute(this.theAtts, name, type, value);
    }
    
    public void anonymize() {
        for (int i = this.theAtts.getLength() - 1; i >= 0; --i) {
            if (this.theAtts.getType(i).equals("ID") || this.theAtts.getQName(i).equals("name")) {
                this.theAtts.removeAttribute(i);
            }
        }
    }
    
    public void clean() {
        for (int i = this.theAtts.getLength() - 1; i >= 0; --i) {
            final String name = this.theAtts.getLocalName(i);
            if (this.theAtts.getValue(i) == null || name == null || name.length() == 0) {
                this.theAtts.removeAttribute(i);
            }
        }
    }
    
    public void preclose() {
        this.preclosed = true;
    }
    
    public boolean isPreclosed() {
        return this.preclosed;
    }
}
