// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

import java.util.HashMap;

public abstract class Schema
{
    public static final int M_ANY = -1;
    public static final int M_EMPTY = 0;
    public static final int M_PCDATA = 1073741824;
    public static final int M_ROOT = Integer.MIN_VALUE;
    public static final int F_RESTART = 1;
    public static final int F_CDATA = 2;
    public static final int F_NOFORCE = 4;
    private HashMap theEntities;
    private HashMap theElementTypes;
    private String theURI;
    private String thePrefix;
    private ElementType theRoot;
    
    public Schema() {
        this.theEntities = new HashMap();
        this.theElementTypes = new HashMap();
        this.theURI = "";
        this.thePrefix = "";
        this.theRoot = null;
    }
    
    public void elementType(final String name, final int model, final int memberOf, final int flags) {
        final ElementType e = new ElementType(name, model, memberOf, flags, this);
        this.theElementTypes.put(name.toLowerCase(), e);
        if (memberOf == Integer.MIN_VALUE) {
            this.theRoot = e;
        }
    }
    
    public ElementType rootElementType() {
        return this.theRoot;
    }
    
    public void attribute(final String elemName, final String attrName, final String type, final String value) {
        final ElementType e = this.getElementType(elemName);
        if (e == null) {
            throw new Error("Attribute " + attrName + " specified for unknown element type " + elemName);
        }
        e.setAttribute(attrName, type, value);
    }
    
    public void parent(final String name, final String parentName) {
        final ElementType child = this.getElementType(name);
        final ElementType parent = this.getElementType(parentName);
        if (child == null) {
            throw new Error("No child " + name + " for parent " + parentName);
        }
        if (parent == null) {
            throw new Error("No parent " + parentName + " for child " + name);
        }
        child.setParent(parent);
    }
    
    public void entity(final String name, final int value) {
        this.theEntities.put(name, new Integer(value));
    }
    
    public ElementType getElementType(final String name) {
        return this.theElementTypes.get(name.toLowerCase());
    }
    
    public int getEntity(final String name) {
        final Integer ch = this.theEntities.get(name);
        if (ch == null) {
            return 0;
        }
        return ch;
    }
    
    public String getURI() {
        return this.theURI;
    }
    
    public String getPrefix() {
        return this.thePrefix;
    }
    
    public void setURI(final String uri) {
        this.theURI = uri;
    }
    
    public void setPrefix(final String prefix) {
        this.thePrefix = prefix;
    }
}
