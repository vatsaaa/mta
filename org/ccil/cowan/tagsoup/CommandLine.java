// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

import org.xml.sax.ContentHandler;
import java.io.Writer;
import org.xml.sax.XMLReader;
import org.xml.sax.InputSource;
import org.xml.sax.ext.LexicalHandler;
import java.io.OutputStreamWriter;
import java.util.Enumeration;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Hashtable;

public class CommandLine
{
    static Hashtable options;
    private static Parser theParser;
    private static HTMLSchema theSchema;
    private static String theOutputEncoding;
    
    public static void main(final String[] argv) throws IOException, SAXException {
        final int optind = getopts(CommandLine.options, argv);
        if (hasOption(CommandLine.options, "--help")) {
            doHelp();
            return;
        }
        if (hasOption(CommandLine.options, "--version")) {
            System.err.println("TagSoup version 1.2.1");
            return;
        }
        if (argv.length == optind) {
            process("", System.out);
        }
        else if (hasOption(CommandLine.options, "--files")) {
            for (int i = optind; i < argv.length; ++i) {
                final String src = argv[i];
                final int j = src.lastIndexOf(46);
                String dst;
                if (j == -1) {
                    dst = src + ".xhtml";
                }
                else if (src.endsWith(".xhtml")) {
                    dst = src + "_";
                }
                else {
                    dst = src.substring(0, j) + ".xhtml";
                }
                System.err.println("src: " + src + " dst: " + dst);
                final OutputStream os = new FileOutputStream(dst);
                process(src, os);
            }
        }
        else {
            for (int i = optind; i < argv.length; ++i) {
                System.err.println("src: " + argv[i]);
                process(argv[i], System.out);
            }
        }
    }
    
    private static void doHelp() {
        System.err.print("usage: java -jar tagsoup-*.jar ");
        System.err.print(" [ ");
        boolean first = true;
        final Enumeration e = CommandLine.options.keys();
        while (e.hasMoreElements()) {
            if (!first) {
                System.err.print("| ");
            }
            first = false;
            final String key = e.nextElement();
            System.err.print(key);
            if (key.endsWith("=")) {
                System.err.print("?");
            }
            System.err.print(" ");
        }
        System.err.println("]*");
    }
    
    private static void process(final String src, final OutputStream os) throws IOException, SAXException {
        XMLReader r;
        if (hasOption(CommandLine.options, "--reuse")) {
            if (CommandLine.theParser == null) {
                CommandLine.theParser = new Parser();
            }
            r = CommandLine.theParser;
        }
        else {
            r = new Parser();
        }
        r.setProperty("http://www.ccil.org/~cowan/tagsoup/properties/schema", CommandLine.theSchema = new HTMLSchema());
        if (hasOption(CommandLine.options, "--nocdata")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/cdata-elements", false);
        }
        if (hasOption(CommandLine.options, "--nons") || hasOption(CommandLine.options, "--html")) {
            r.setFeature("http://xml.org/sax/features/namespaces", false);
        }
        if (hasOption(CommandLine.options, "--nobogons")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/ignore-bogons", true);
        }
        if (hasOption(CommandLine.options, "--any")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/bogons-empty", false);
        }
        else if (hasOption(CommandLine.options, "--emptybogons")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/bogons-empty", true);
        }
        if (hasOption(CommandLine.options, "--norootbogons")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/root-bogons", false);
        }
        if (hasOption(CommandLine.options, "--nodefaults")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/default-attributes", false);
        }
        if (hasOption(CommandLine.options, "--nocolons")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/translate-colons", true);
        }
        if (hasOption(CommandLine.options, "--norestart")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/restart-elements", false);
        }
        if (hasOption(CommandLine.options, "--ignorable")) {
            r.setFeature("http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace", true);
        }
        if (hasOption(CommandLine.options, "--pyxin")) {
            r.setProperty("http://www.ccil.org/~cowan/tagsoup/properties/scanner", new PYXScanner());
        }
        Writer w;
        if (CommandLine.theOutputEncoding == null) {
            w = new OutputStreamWriter(os);
        }
        else {
            w = new OutputStreamWriter(os, CommandLine.theOutputEncoding);
        }
        final ContentHandler h = chooseContentHandler(w);
        r.setContentHandler(h);
        if (hasOption(CommandLine.options, "--lexical") && h instanceof LexicalHandler) {
            r.setProperty("http://xml.org/sax/properties/lexical-handler", h);
        }
        final InputSource s = new InputSource();
        if (src != "") {
            s.setSystemId(src);
        }
        else {
            s.setByteStream(System.in);
        }
        if (hasOption(CommandLine.options, "--encoding=")) {
            final String encoding = CommandLine.options.get("--encoding=");
            if (encoding != null) {
                s.setEncoding(encoding);
            }
        }
        r.parse(s);
    }
    
    private static ContentHandler chooseContentHandler(final Writer w) {
        if (hasOption(CommandLine.options, "--pyx")) {
            return new PYXWriter(w);
        }
        final XMLWriter x = new XMLWriter(w);
        if (hasOption(CommandLine.options, "--html")) {
            x.setOutputProperty("method", "html");
            x.setOutputProperty("omit-xml-declaration", "yes");
        }
        if (hasOption(CommandLine.options, "--method=")) {
            final String method = CommandLine.options.get("--method=");
            if (method != null) {
                x.setOutputProperty("method", method);
            }
        }
        if (hasOption(CommandLine.options, "--doctype-public=")) {
            final String doctype_public = CommandLine.options.get("--doctype-public=");
            if (doctype_public != null) {
                x.setOutputProperty("doctype-public", doctype_public);
            }
        }
        if (hasOption(CommandLine.options, "--doctype-system=")) {
            final String doctype_system = CommandLine.options.get("--doctype-system=");
            if (doctype_system != null) {
                x.setOutputProperty("doctype-system", doctype_system);
            }
        }
        if (hasOption(CommandLine.options, "--output-encoding=")) {
            CommandLine.theOutputEncoding = CommandLine.options.get("--output-encoding=");
            if (CommandLine.theOutputEncoding != null) {
                x.setOutputProperty("encoding", CommandLine.theOutputEncoding);
            }
        }
        if (hasOption(CommandLine.options, "--omit-xml-declaration")) {
            x.setOutputProperty("omit-xml-declaration", "yes");
        }
        x.setPrefix(CommandLine.theSchema.getURI(), "");
        return x;
    }
    
    private static int getopts(final Hashtable options, final String[] argv) {
        int optind;
        for (optind = 0; optind < argv.length; ++optind) {
            String arg = argv[optind];
            String value = null;
            if (arg.charAt(0) != '-') {
                break;
            }
            final int eqsign = arg.indexOf(61);
            if (eqsign != -1) {
                value = arg.substring(eqsign + 1, arg.length());
                arg = arg.substring(0, eqsign + 1);
            }
            if (options.containsKey(arg)) {
                if (value == null) {
                    options.put(arg, Boolean.TRUE);
                }
                else {
                    options.put(arg, value);
                }
            }
            else {
                System.err.print("Unknown option ");
                System.err.println(arg);
                System.exit(1);
            }
        }
        return optind;
    }
    
    private static boolean hasOption(final Hashtable options, final String option) {
        return Boolean.getBoolean(option) || options.get(option) != Boolean.FALSE;
    }
    
    static {
        (CommandLine.options = new Hashtable()).put("--nocdata", Boolean.FALSE);
        CommandLine.options.put("--files", Boolean.FALSE);
        CommandLine.options.put("--reuse", Boolean.FALSE);
        CommandLine.options.put("--nons", Boolean.FALSE);
        CommandLine.options.put("--nobogons", Boolean.FALSE);
        CommandLine.options.put("--any", Boolean.FALSE);
        CommandLine.options.put("--emptybogons", Boolean.FALSE);
        CommandLine.options.put("--norootbogons", Boolean.FALSE);
        CommandLine.options.put("--pyxin", Boolean.FALSE);
        CommandLine.options.put("--lexical", Boolean.FALSE);
        CommandLine.options.put("--pyx", Boolean.FALSE);
        CommandLine.options.put("--html", Boolean.FALSE);
        CommandLine.options.put("--method=", Boolean.FALSE);
        CommandLine.options.put("--doctype-public=", Boolean.FALSE);
        CommandLine.options.put("--doctype-system=", Boolean.FALSE);
        CommandLine.options.put("--output-encoding=", Boolean.FALSE);
        CommandLine.options.put("--omit-xml-declaration", Boolean.FALSE);
        CommandLine.options.put("--encoding=", Boolean.FALSE);
        CommandLine.options.put("--help", Boolean.FALSE);
        CommandLine.options.put("--version", Boolean.FALSE);
        CommandLine.options.put("--nodefaults", Boolean.FALSE);
        CommandLine.options.put("--nocolons", Boolean.FALSE);
        CommandLine.options.put("--norestart", Boolean.FALSE);
        CommandLine.options.put("--ignorable", Boolean.FALSE);
        CommandLine.theParser = null;
        CommandLine.theSchema = null;
        CommandLine.theOutputEncoding = null;
    }
}
