// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import java.net.URLConnection;
import java.net.URL;
import java.io.UnsupportedEncodingException;
import java.io.InputStreamReader;
import java.io.InputStream;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.Reader;
import org.xml.sax.Locator;
import org.xml.sax.InputSource;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXNotRecognizedException;
import java.util.HashMap;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class Parser extends DefaultHandler implements ScanHandler, XMLReader, LexicalHandler
{
    private ContentHandler theContentHandler;
    private LexicalHandler theLexicalHandler;
    private DTDHandler theDTDHandler;
    private ErrorHandler theErrorHandler;
    private EntityResolver theEntityResolver;
    private Schema theSchema;
    private Scanner theScanner;
    private AutoDetector theAutoDetector;
    private static boolean DEFAULT_NAMESPACES;
    private static boolean DEFAULT_IGNORE_BOGONS;
    private static boolean DEFAULT_BOGONS_EMPTY;
    private static boolean DEFAULT_ROOT_BOGONS;
    private static boolean DEFAULT_DEFAULT_ATTRIBUTES;
    private static boolean DEFAULT_TRANSLATE_COLONS;
    private static boolean DEFAULT_RESTART_ELEMENTS;
    private static boolean DEFAULT_IGNORABLE_WHITESPACE;
    private static boolean DEFAULT_CDATA_ELEMENTS;
    private boolean namespaces;
    private boolean ignoreBogons;
    private boolean bogonsEmpty;
    private boolean rootBogons;
    private boolean defaultAttributes;
    private boolean translateColons;
    private boolean restartElements;
    private boolean ignorableWhitespace;
    private boolean CDATAElements;
    public static final String namespacesFeature = "http://xml.org/sax/features/namespaces";
    public static final String namespacePrefixesFeature = "http://xml.org/sax/features/namespace-prefixes";
    public static final String externalGeneralEntitiesFeature = "http://xml.org/sax/features/external-general-entities";
    public static final String externalParameterEntitiesFeature = "http://xml.org/sax/features/external-parameter-entities";
    public static final String isStandaloneFeature = "http://xml.org/sax/features/is-standalone";
    public static final String lexicalHandlerParameterEntitiesFeature = "http://xml.org/sax/features/lexical-handler/parameter-entities";
    public static final String resolveDTDURIsFeature = "http://xml.org/sax/features/resolve-dtd-uris";
    public static final String stringInterningFeature = "http://xml.org/sax/features/string-interning";
    public static final String useAttributes2Feature = "http://xml.org/sax/features/use-attributes2";
    public static final String useLocator2Feature = "http://xml.org/sax/features/use-locator2";
    public static final String useEntityResolver2Feature = "http://xml.org/sax/features/use-entity-resolver2";
    public static final String validationFeature = "http://xml.org/sax/features/validation";
    public static final String unicodeNormalizationCheckingFeature = "http://xml.org/sax/features/unicode-normalization-checking";
    public static final String xmlnsURIsFeature = "http://xml.org/sax/features/xmlns-uris";
    public static final String XML11Feature = "http://xml.org/sax/features/xml-1.1";
    public static final String ignoreBogonsFeature = "http://www.ccil.org/~cowan/tagsoup/features/ignore-bogons";
    public static final String bogonsEmptyFeature = "http://www.ccil.org/~cowan/tagsoup/features/bogons-empty";
    public static final String rootBogonsFeature = "http://www.ccil.org/~cowan/tagsoup/features/root-bogons";
    public static final String defaultAttributesFeature = "http://www.ccil.org/~cowan/tagsoup/features/default-attributes";
    public static final String translateColonsFeature = "http://www.ccil.org/~cowan/tagsoup/features/translate-colons";
    public static final String restartElementsFeature = "http://www.ccil.org/~cowan/tagsoup/features/restart-elements";
    public static final String ignorableWhitespaceFeature = "http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace";
    public static final String CDATAElementsFeature = "http://www.ccil.org/~cowan/tagsoup/features/cdata-elements";
    public static final String lexicalHandlerProperty = "http://xml.org/sax/properties/lexical-handler";
    public static final String scannerProperty = "http://www.ccil.org/~cowan/tagsoup/properties/scanner";
    public static final String schemaProperty = "http://www.ccil.org/~cowan/tagsoup/properties/schema";
    public static final String autoDetectorProperty = "http://www.ccil.org/~cowan/tagsoup/properties/auto-detector";
    private HashMap theFeatures;
    private Element theNewElement;
    private String theAttributeName;
    private boolean theDoctypeIsPresent;
    private String theDoctypePublicId;
    private String theDoctypeSystemId;
    private String theDoctypeName;
    private String thePITarget;
    private Element theStack;
    private Element theSaved;
    private Element thePCDATA;
    private int theEntity;
    private static char[] etagchars;
    private boolean virginStack;
    private static String legal;
    private char[] theCommentBuffer;
    
    public Parser() {
        this.theContentHandler = this;
        this.theLexicalHandler = this;
        this.theDTDHandler = this;
        this.theErrorHandler = this;
        this.theEntityResolver = this;
        this.namespaces = Parser.DEFAULT_NAMESPACES;
        this.ignoreBogons = Parser.DEFAULT_IGNORE_BOGONS;
        this.bogonsEmpty = Parser.DEFAULT_BOGONS_EMPTY;
        this.rootBogons = Parser.DEFAULT_ROOT_BOGONS;
        this.defaultAttributes = Parser.DEFAULT_DEFAULT_ATTRIBUTES;
        this.translateColons = Parser.DEFAULT_TRANSLATE_COLONS;
        this.restartElements = Parser.DEFAULT_RESTART_ELEMENTS;
        this.ignorableWhitespace = Parser.DEFAULT_IGNORABLE_WHITESPACE;
        this.CDATAElements = Parser.DEFAULT_CDATA_ELEMENTS;
        (this.theFeatures = new HashMap()).put("http://xml.org/sax/features/namespaces", truthValue(Parser.DEFAULT_NAMESPACES));
        this.theFeatures.put("http://xml.org/sax/features/namespace-prefixes", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/external-general-entities", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/external-parameter-entities", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/is-standalone", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/lexical-handler/parameter-entities", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/resolve-dtd-uris", Boolean.TRUE);
        this.theFeatures.put("http://xml.org/sax/features/string-interning", Boolean.TRUE);
        this.theFeatures.put("http://xml.org/sax/features/use-attributes2", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/use-locator2", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/use-entity-resolver2", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/validation", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/xmlns-uris", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/xmlns-uris", Boolean.FALSE);
        this.theFeatures.put("http://xml.org/sax/features/xml-1.1", Boolean.FALSE);
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/ignore-bogons", truthValue(Parser.DEFAULT_IGNORE_BOGONS));
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/bogons-empty", truthValue(Parser.DEFAULT_BOGONS_EMPTY));
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/root-bogons", truthValue(Parser.DEFAULT_ROOT_BOGONS));
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/default-attributes", truthValue(Parser.DEFAULT_DEFAULT_ATTRIBUTES));
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/translate-colons", truthValue(Parser.DEFAULT_TRANSLATE_COLONS));
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/restart-elements", truthValue(Parser.DEFAULT_RESTART_ELEMENTS));
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace", truthValue(Parser.DEFAULT_IGNORABLE_WHITESPACE));
        this.theFeatures.put("http://www.ccil.org/~cowan/tagsoup/features/cdata-elements", truthValue(Parser.DEFAULT_CDATA_ELEMENTS));
        this.theNewElement = null;
        this.theAttributeName = null;
        this.theDoctypeIsPresent = false;
        this.theDoctypePublicId = null;
        this.theDoctypeSystemId = null;
        this.theDoctypeName = null;
        this.thePITarget = null;
        this.theStack = null;
        this.theSaved = null;
        this.thePCDATA = null;
        this.theEntity = 0;
        this.virginStack = true;
        this.theCommentBuffer = new char[2000];
    }
    
    private static Boolean truthValue(final boolean b) {
        return b ? Boolean.TRUE : Boolean.FALSE;
    }
    
    public boolean getFeature(final String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        final Boolean b = this.theFeatures.get(name);
        if (b == null) {
            throw new SAXNotRecognizedException("Unknown feature " + name);
        }
        return b;
    }
    
    public void setFeature(final String name, final boolean value) throws SAXNotRecognizedException, SAXNotSupportedException {
        final Boolean b = this.theFeatures.get(name);
        if (b == null) {
            throw new SAXNotRecognizedException("Unknown feature " + name);
        }
        if (value) {
            this.theFeatures.put(name, Boolean.TRUE);
        }
        else {
            this.theFeatures.put(name, Boolean.FALSE);
        }
        if (name.equals("http://xml.org/sax/features/namespaces")) {
            this.namespaces = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/ignore-bogons")) {
            this.ignoreBogons = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/bogons-empty")) {
            this.bogonsEmpty = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/root-bogons")) {
            this.rootBogons = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/default-attributes")) {
            this.defaultAttributes = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/translate-colons")) {
            this.translateColons = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/restart-elements")) {
            this.restartElements = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace")) {
            this.ignorableWhitespace = value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/features/cdata-elements")) {
            this.CDATAElements = value;
        }
    }
    
    public Object getProperty(final String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        if (name.equals("http://xml.org/sax/properties/lexical-handler")) {
            return (this.theLexicalHandler == this) ? null : this.theLexicalHandler;
        }
        if (name.equals("http://www.ccil.org/~cowan/tagsoup/properties/scanner")) {
            return this.theScanner;
        }
        if (name.equals("http://www.ccil.org/~cowan/tagsoup/properties/schema")) {
            return this.theSchema;
        }
        if (name.equals("http://www.ccil.org/~cowan/tagsoup/properties/auto-detector")) {
            return this.theAutoDetector;
        }
        throw new SAXNotRecognizedException("Unknown property " + name);
    }
    
    public void setProperty(final String name, final Object value) throws SAXNotRecognizedException, SAXNotSupportedException {
        if (name.equals("http://xml.org/sax/properties/lexical-handler")) {
            if (value == null) {
                this.theLexicalHandler = this;
            }
            else {
                if (!(value instanceof LexicalHandler)) {
                    throw new SAXNotSupportedException("Your lexical handler is not a LexicalHandler");
                }
                this.theLexicalHandler = (LexicalHandler)value;
            }
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/properties/scanner")) {
            if (!(value instanceof Scanner)) {
                throw new SAXNotSupportedException("Your scanner is not a Scanner");
            }
            this.theScanner = (Scanner)value;
        }
        else if (name.equals("http://www.ccil.org/~cowan/tagsoup/properties/schema")) {
            if (!(value instanceof Schema)) {
                throw new SAXNotSupportedException("Your schema is not a Schema");
            }
            this.theSchema = (Schema)value;
        }
        else {
            if (!name.equals("http://www.ccil.org/~cowan/tagsoup/properties/auto-detector")) {
                throw new SAXNotRecognizedException("Unknown property " + name);
            }
            if (!(value instanceof AutoDetector)) {
                throw new SAXNotSupportedException("Your auto-detector is not an AutoDetector");
            }
            this.theAutoDetector = (AutoDetector)value;
        }
    }
    
    public void setEntityResolver(final EntityResolver resolver) {
        this.theEntityResolver = ((resolver == null) ? this : resolver);
    }
    
    public EntityResolver getEntityResolver() {
        return (this.theEntityResolver == this) ? null : this.theEntityResolver;
    }
    
    public void setDTDHandler(final DTDHandler handler) {
        this.theDTDHandler = ((handler == null) ? this : handler);
    }
    
    public DTDHandler getDTDHandler() {
        return (this.theDTDHandler == this) ? null : this.theDTDHandler;
    }
    
    public void setContentHandler(final ContentHandler handler) {
        this.theContentHandler = ((handler == null) ? this : handler);
    }
    
    public ContentHandler getContentHandler() {
        return (this.theContentHandler == this) ? null : this.theContentHandler;
    }
    
    public void setErrorHandler(final ErrorHandler handler) {
        this.theErrorHandler = ((handler == null) ? this : handler);
    }
    
    public ErrorHandler getErrorHandler() {
        return (this.theErrorHandler == this) ? null : this.theErrorHandler;
    }
    
    public void parse(final InputSource input) throws IOException, SAXException {
        this.setup();
        final Reader r = this.getReader(input);
        this.theContentHandler.startDocument();
        this.theScanner.resetDocumentLocator(input.getPublicId(), input.getSystemId());
        if (this.theScanner instanceof Locator) {
            this.theContentHandler.setDocumentLocator((Locator)this.theScanner);
        }
        if (!this.theSchema.getURI().equals("")) {
            this.theContentHandler.startPrefixMapping(this.theSchema.getPrefix(), this.theSchema.getURI());
        }
        this.theScanner.scan(r, this);
    }
    
    public void parse(final String systemid) throws IOException, SAXException {
        this.parse(new InputSource(systemid));
    }
    
    private void setup() {
        if (this.theSchema == null) {
            this.theSchema = new HTMLSchema();
        }
        if (this.theScanner == null) {
            this.theScanner = new HTMLScanner();
        }
        if (this.theAutoDetector == null) {
            this.theAutoDetector = new AutoDetector() {
                public Reader autoDetectingReader(final InputStream i) {
                    return new InputStreamReader(i);
                }
            };
        }
        this.theStack = new Element(this.theSchema.getElementType("<root>"), this.defaultAttributes);
        this.thePCDATA = new Element(this.theSchema.getElementType("<pcdata>"), this.defaultAttributes);
        this.theNewElement = null;
        this.theAttributeName = null;
        this.thePITarget = null;
        this.theSaved = null;
        this.theEntity = 0;
        this.virginStack = true;
        final String theDoctypeName = null;
        this.theDoctypeSystemId = theDoctypeName;
        this.theDoctypePublicId = theDoctypeName;
        this.theDoctypeName = theDoctypeName;
    }
    
    private Reader getReader(final InputSource s) throws SAXException, IOException {
        Reader r = s.getCharacterStream();
        InputStream i = s.getByteStream();
        final String encoding = s.getEncoding();
        final String publicid = s.getPublicId();
        final String systemid = s.getSystemId();
        if (r == null) {
            if (i == null) {
                i = this.getInputStream(publicid, systemid);
            }
            if (encoding == null) {
                r = this.theAutoDetector.autoDetectingReader(i);
            }
            else {
                try {
                    r = new InputStreamReader(i, encoding);
                }
                catch (UnsupportedEncodingException e) {
                    r = new InputStreamReader(i);
                }
            }
        }
        return r;
    }
    
    private InputStream getInputStream(final String publicid, final String systemid) throws IOException, SAXException {
        final URL basis = new URL("file", "", System.getProperty("user.dir") + "/.");
        final URL url = new URL(basis, systemid);
        final URLConnection c = url.openConnection();
        return c.getInputStream();
    }
    
    public void adup(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.theNewElement == null || this.theAttributeName == null) {
            return;
        }
        this.theNewElement.setAttribute(this.theAttributeName, null, this.theAttributeName);
        this.theAttributeName = null;
    }
    
    public void aname(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.theNewElement == null) {
            return;
        }
        this.theAttributeName = this.makeName(buff, offset, length).toLowerCase();
    }
    
    public void aval(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.theNewElement == null || this.theAttributeName == null) {
            return;
        }
        String value = new String(buff, offset, length);
        value = this.expandEntities(value);
        this.theNewElement.setAttribute(this.theAttributeName, null, value);
        this.theAttributeName = null;
    }
    
    private String expandEntities(final String src) {
        int refStart = -1;
        final int len = src.length();
        final char[] dst = new char[len];
        int dstlen = 0;
        for (int i = 0; i < len; ++i) {
            final char ch = src.charAt(i);
            dst[dstlen++] = ch;
            if (ch == '&' && refStart == -1) {
                refStart = dstlen;
            }
            else if (refStart != -1) {
                if (!Character.isLetter(ch) && !Character.isDigit(ch)) {
                    if (ch != '#') {
                        if (ch == ';') {
                            int ent = this.lookupEntity(dst, refStart, dstlen - refStart - 1);
                            if (ent > 65535) {
                                ent -= 65536;
                                dst[refStart - 1] = (char)((ent >> 10) + 55296);
                                dst[refStart] = (char)((ent & 0x3FF) + 56320);
                                dstlen = refStart + 1;
                            }
                            else if (ent != 0) {
                                dst[refStart - 1] = (char)ent;
                                dstlen = refStart;
                            }
                            refStart = -1;
                        }
                        else {
                            refStart = -1;
                        }
                    }
                }
            }
        }
        return new String(dst, 0, dstlen);
    }
    
    public void entity(final char[] buff, final int offset, final int length) throws SAXException {
        this.theEntity = this.lookupEntity(buff, offset, length);
    }
    
    private int lookupEntity(final char[] buff, final int offset, final int length) {
        final int result = 0;
        if (length < 1) {
            return result;
        }
        if (buff[offset] == '#') {
            Label_0068: {
                if (length > 1) {
                    if (buff[offset + 1] != 'x') {
                        if (buff[offset + 1] != 'X') {
                            break Label_0068;
                        }
                    }
                    try {
                        return Integer.parseInt(new String(buff, offset + 2, length - 2), 16);
                    }
                    catch (NumberFormatException e) {
                        return 0;
                    }
                }
                try {
                    return Integer.parseInt(new String(buff, offset + 1, length - 1), 10);
                }
                catch (NumberFormatException e) {
                    return 0;
                }
            }
        }
        return this.theSchema.getEntity(new String(buff, offset, length));
    }
    
    public void eof(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.virginStack) {
            this.rectify(this.thePCDATA);
        }
        while (this.theStack.next() != null) {
            this.pop();
        }
        if (!this.theSchema.getURI().equals("")) {
            this.theContentHandler.endPrefixMapping(this.theSchema.getPrefix());
        }
        this.theContentHandler.endDocument();
    }
    
    public void etag(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.etag_cdata(buff, offset, length)) {
            return;
        }
        this.etag_basic(buff, offset, length);
    }
    
    public boolean etag_cdata(final char[] buff, final int offset, final int length) throws SAXException {
        final String currentName = this.theStack.name();
        if (this.CDATAElements && (this.theStack.flags() & 0x2) != 0x0) {
            boolean realTag = length == currentName.length();
            if (realTag) {
                for (int i = 0; i < length; ++i) {
                    if (Character.toLowerCase(buff[offset + i]) != Character.toLowerCase(currentName.charAt(i))) {
                        realTag = false;
                        break;
                    }
                }
            }
            if (!realTag) {
                this.theContentHandler.characters(Parser.etagchars, 0, 2);
                this.theContentHandler.characters(buff, offset, length);
                this.theContentHandler.characters(Parser.etagchars, 2, 1);
                this.theScanner.startCDATA();
                return true;
            }
        }
        return false;
    }
    
    public void etag_basic(final char[] buff, final int offset, final int length) throws SAXException {
        this.theNewElement = null;
        String name;
        if (length != 0) {
            name = this.makeName(buff, offset, length);
            final ElementType type = this.theSchema.getElementType(name);
            if (type == null) {
                return;
            }
            name = type.name();
        }
        else {
            name = this.theStack.name();
        }
        boolean inNoforce = false;
        Element sp;
        for (sp = this.theStack; sp != null && !sp.name().equals(name); sp = sp.next()) {
            if ((sp.flags() & 0x4) != 0x0) {
                inNoforce = true;
            }
        }
        if (sp == null) {
            return;
        }
        if (sp.next() == null || sp.next().next() == null) {
            return;
        }
        if (inNoforce) {
            sp.preclose();
        }
        else {
            while (this.theStack != sp) {
                this.restartablyPop();
            }
            this.pop();
        }
        while (this.theStack.isPreclosed()) {
            this.pop();
        }
        this.restart(null);
    }
    
    private void restart(final Element e) throws SAXException {
        while (this.theSaved != null && this.theStack.canContain(this.theSaved) && (e == null || this.theSaved.canContain(e))) {
            final Element next = this.theSaved.next();
            this.push(this.theSaved);
            this.theSaved = next;
        }
    }
    
    private void pop() throws SAXException {
        if (this.theStack == null) {
            return;
        }
        final String name = this.theStack.name();
        String localName = this.theStack.localName();
        String namespace = this.theStack.namespace();
        final String prefix = this.prefixOf(name);
        if (!this.namespaces) {
            localName = (namespace = "");
        }
        this.theContentHandler.endElement(namespace, localName, name);
        if (this.foreign(prefix, namespace)) {
            this.theContentHandler.endPrefixMapping(prefix);
        }
        final Attributes atts = this.theStack.atts();
        for (int i = atts.getLength() - 1; i >= 0; --i) {
            final String attNamespace = atts.getURI(i);
            final String attPrefix = this.prefixOf(atts.getQName(i));
            if (this.foreign(attPrefix, attNamespace)) {
                this.theContentHandler.endPrefixMapping(attPrefix);
            }
        }
        this.theStack = this.theStack.next();
    }
    
    private void restartablyPop() throws SAXException {
        final Element popped = this.theStack;
        this.pop();
        if (this.restartElements && (popped.flags() & 0x1) != 0x0) {
            popped.anonymize();
            popped.setNext(this.theSaved);
            this.theSaved = popped;
        }
    }
    
    private void push(final Element e) throws SAXException {
        final String name = e.name();
        String localName = e.localName();
        String namespace = e.namespace();
        final String prefix = this.prefixOf(name);
        e.clean();
        if (!this.namespaces) {
            localName = (namespace = "");
        }
        if (this.virginStack && localName.equalsIgnoreCase(this.theDoctypeName)) {
            try {
                this.theEntityResolver.resolveEntity(this.theDoctypePublicId, this.theDoctypeSystemId);
            }
            catch (IOException ex) {}
        }
        if (this.foreign(prefix, namespace)) {
            this.theContentHandler.startPrefixMapping(prefix, namespace);
        }
        final Attributes atts = e.atts();
        for (int len = atts.getLength(), i = 0; i < len; ++i) {
            final String attNamespace = atts.getURI(i);
            final String attPrefix = this.prefixOf(atts.getQName(i));
            if (this.foreign(attPrefix, attNamespace)) {
                this.theContentHandler.startPrefixMapping(attPrefix, attNamespace);
            }
        }
        this.theContentHandler.startElement(namespace, localName, name, e.atts());
        e.setNext(this.theStack);
        this.theStack = e;
        this.virginStack = false;
        if (this.CDATAElements && (this.theStack.flags() & 0x2) != 0x0) {
            this.theScanner.startCDATA();
        }
    }
    
    private String prefixOf(final String name) {
        final int i = name.indexOf(58);
        String prefix = "";
        if (i != -1) {
            prefix = name.substring(0, i);
        }
        return prefix;
    }
    
    private boolean foreign(final String prefix, final String namespace) {
        final boolean foreign = !prefix.equals("") && !namespace.equals("") && !namespace.equals(this.theSchema.getURI());
        return foreign;
    }
    
    public void decl(final char[] buff, final int offset, final int length) throws SAXException {
        final String s = new String(buff, offset, length);
        String name = null;
        String systemid = null;
        String publicid = null;
        final String[] v = split(s);
        if (v.length > 0 && "DOCTYPE".equalsIgnoreCase(v[0])) {
            if (this.theDoctypeIsPresent) {
                return;
            }
            this.theDoctypeIsPresent = true;
            if (v.length > 1) {
                name = v[1];
                if (v.length > 3 && "SYSTEM".equals(v[2])) {
                    systemid = v[3];
                }
                else if (v.length > 3 && "PUBLIC".equals(v[2])) {
                    publicid = v[3];
                    if (v.length > 4) {
                        systemid = v[4];
                    }
                    else {
                        systemid = "";
                    }
                }
            }
        }
        publicid = trimquotes(publicid);
        systemid = trimquotes(systemid);
        if (name != null) {
            publicid = this.cleanPublicid(publicid);
            this.theLexicalHandler.startDTD(name, publicid, systemid);
            this.theLexicalHandler.endDTD();
            this.theDoctypeName = name;
            this.theDoctypePublicId = publicid;
            if (this.theScanner instanceof Locator) {
                this.theDoctypeSystemId = ((Locator)this.theScanner).getSystemId();
                try {
                    this.theDoctypeSystemId = new URL(new URL(this.theDoctypeSystemId), systemid).toString();
                }
                catch (Exception ex) {}
            }
        }
    }
    
    private static String trimquotes(String in) {
        if (in == null) {
            return in;
        }
        final int length = in.length();
        if (length == 0) {
            return in;
        }
        final char s = in.charAt(0);
        final char e = in.charAt(length - 1);
        if (s == e && (s == '\'' || s == '\"')) {
            in = in.substring(1, in.length() - 1);
        }
        return in;
    }
    
    private static String[] split(String val) throws IllegalArgumentException {
        val = val.trim();
        if (val.length() == 0) {
            return new String[0];
        }
        final ArrayList l = new ArrayList();
        int s = 0;
        int e = 0;
        boolean sq = false;
        boolean dq = false;
        char lastc = '\0';
        int len;
        for (len = val.length(), e = 0; e < len; ++e) {
            final char c = val.charAt(e);
            if (!dq && c == '\'' && lastc != '\\') {
                sq = !sq;
                if (s < 0) {
                    s = e;
                }
            }
            else if (!sq && c == '\"' && lastc != '\\') {
                dq = !dq;
                if (s < 0) {
                    s = e;
                }
            }
            else if (!sq && !dq) {
                if (Character.isWhitespace(c)) {
                    if (s >= 0) {
                        l.add(val.substring(s, e));
                    }
                    s = -1;
                }
                else if (s < 0 && c != ' ') {
                    s = e;
                }
            }
            lastc = c;
        }
        l.add(val.substring(s, e));
        return l.toArray(new String[0]);
    }
    
    private String cleanPublicid(final String src) {
        if (src == null) {
            return null;
        }
        final int len = src.length();
        final StringBuffer dst = new StringBuffer(len);
        boolean suppressSpace = true;
        for (int i = 0; i < len; ++i) {
            final char ch = src.charAt(i);
            if (Parser.legal.indexOf(ch) != -1) {
                dst.append(ch);
                suppressSpace = false;
            }
            else if (!suppressSpace) {
                dst.append(' ');
                suppressSpace = true;
            }
        }
        return dst.toString().trim();
    }
    
    public void gi(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.theNewElement != null) {
            return;
        }
        final String name = this.makeName(buff, offset, length);
        if (name == null) {
            return;
        }
        ElementType type = this.theSchema.getElementType(name);
        if (type == null) {
            if (this.ignoreBogons) {
                return;
            }
            final int bogonModel = this.bogonsEmpty ? 0 : -1;
            final int bogonMemberOf = this.rootBogons ? -1 : Integer.MAX_VALUE;
            this.theSchema.elementType(name, bogonModel, bogonMemberOf, 0);
            if (!this.rootBogons) {
                this.theSchema.parent(name, this.theSchema.rootElementType().name());
            }
            type = this.theSchema.getElementType(name);
        }
        this.theNewElement = new Element(type, this.defaultAttributes);
    }
    
    public void cdsect(final char[] buff, final int offset, final int length) throws SAXException {
        this.theLexicalHandler.startCDATA();
        this.pcdata(buff, offset, length);
        this.theLexicalHandler.endCDATA();
    }
    
    public void pcdata(final char[] buff, final int offset, final int length) throws SAXException {
        if (length == 0) {
            return;
        }
        boolean allWhite = true;
        for (int i = 0; i < length; ++i) {
            if (!Character.isWhitespace(buff[offset + i])) {
                allWhite = false;
            }
        }
        if (allWhite && !this.theStack.canContain(this.thePCDATA)) {
            if (this.ignorableWhitespace) {
                this.theContentHandler.ignorableWhitespace(buff, offset, length);
            }
        }
        else {
            this.rectify(this.thePCDATA);
            this.theContentHandler.characters(buff, offset, length);
        }
    }
    
    public void pitarget(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.theNewElement != null) {
            return;
        }
        this.thePITarget = this.makeName(buff, offset, length).replace(':', '_');
    }
    
    public void pi(final char[] buff, final int offset, int length) throws SAXException {
        if (this.theNewElement != null || this.thePITarget == null) {
            return;
        }
        if ("xml".equalsIgnoreCase(this.thePITarget)) {
            return;
        }
        if (length > 0 && buff[length - 1] == '?') {
            --length;
        }
        this.theContentHandler.processingInstruction(this.thePITarget, new String(buff, offset, length));
        this.thePITarget = null;
    }
    
    public void stagc(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.theNewElement == null) {
            return;
        }
        this.rectify(this.theNewElement);
        if (this.theStack.model() == 0) {
            this.etag_basic(buff, offset, length);
        }
    }
    
    public void stage(final char[] buff, final int offset, final int length) throws SAXException {
        if (this.theNewElement == null) {
            return;
        }
        this.rectify(this.theNewElement);
        this.etag_basic(buff, offset, length);
    }
    
    public void cmnt(final char[] buff, final int offset, final int length) throws SAXException {
        this.theLexicalHandler.comment(buff, offset, length);
    }
    
    private void rectify(Element e) throws SAXException {
        Element sp;
        while (true) {
            for (sp = this.theStack; sp != null && !sp.canContain(e); sp = sp.next()) {}
            if (sp != null) {
                break;
            }
            final ElementType parentType = e.parent();
            if (parentType == null) {
                break;
            }
            final Element parent = new Element(parentType, this.defaultAttributes);
            parent.setNext(e);
            e = parent;
        }
        if (sp == null) {
            return;
        }
        while (this.theStack != sp && this.theStack != null && this.theStack.next() != null) {
            if (this.theStack.next().next() == null) {
                break;
            }
            this.restartablyPop();
        }
        while (e != null) {
            final Element nexte = e.next();
            if (!e.name().equals("<pcdata>")) {
                this.push(e);
            }
            e = nexte;
            this.restart(e);
        }
        this.theNewElement = null;
    }
    
    public int getEntity() {
        return this.theEntity;
    }
    
    private String makeName(final char[] buff, int offset, int length) {
        final StringBuffer dst = new StringBuffer(length + 2);
        boolean seenColon = false;
        boolean start = true;
        while (length-- > 0) {
            final char ch = buff[offset];
            if (Character.isLetter(ch) || ch == '_') {
                start = false;
                dst.append(ch);
            }
            else if (Character.isDigit(ch) || ch == '-' || ch == '.') {
                if (start) {
                    dst.append('_');
                }
                start = false;
                dst.append(ch);
            }
            else if (ch == ':' && !seenColon) {
                seenColon = true;
                if (start) {
                    dst.append('_');
                }
                start = true;
                dst.append(this.translateColons ? '_' : ch);
            }
            ++offset;
        }
        final int dstLength = dst.length();
        if (dstLength == 0 || dst.charAt(dstLength - 1) == ':') {
            dst.append('_');
        }
        return dst.toString().intern();
    }
    
    public void comment(final char[] ch, final int start, final int length) throws SAXException {
    }
    
    public void endCDATA() throws SAXException {
    }
    
    public void endDTD() throws SAXException {
    }
    
    public void endEntity(final String name) throws SAXException {
    }
    
    public void startCDATA() throws SAXException {
    }
    
    public void startDTD(final String name, final String publicid, final String systemid) throws SAXException {
    }
    
    public void startEntity(final String name) throws SAXException {
    }
    
    static {
        Parser.DEFAULT_NAMESPACES = true;
        Parser.DEFAULT_IGNORE_BOGONS = false;
        Parser.DEFAULT_BOGONS_EMPTY = false;
        Parser.DEFAULT_ROOT_BOGONS = true;
        Parser.DEFAULT_DEFAULT_ATTRIBUTES = true;
        Parser.DEFAULT_TRANSLATE_COLONS = false;
        Parser.DEFAULT_RESTART_ELEMENTS = true;
        Parser.DEFAULT_IGNORABLE_WHITESPACE = false;
        Parser.DEFAULT_CDATA_ELEMENTS = true;
        Parser.etagchars = new char[] { '<', '/', '>' };
        Parser.legal = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-'()+,./:=?;!*#@$_%";
    }
}
