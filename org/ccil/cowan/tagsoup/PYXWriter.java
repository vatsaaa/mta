// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup;

import java.io.Writer;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import java.io.PrintWriter;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.ContentHandler;

public class PYXWriter implements ScanHandler, ContentHandler, LexicalHandler
{
    private PrintWriter theWriter;
    private static char[] dummy;
    private String attrName;
    
    public void adup(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.println(this.attrName);
        this.attrName = null;
    }
    
    public void aname(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.print('A');
        this.theWriter.write(buff, offset, length);
        this.theWriter.print(' ');
        this.attrName = new String(buff, offset, length);
    }
    
    public void aval(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.write(buff, offset, length);
        this.theWriter.println();
        this.attrName = null;
    }
    
    public void cmnt(final char[] buff, final int offset, final int length) throws SAXException {
    }
    
    public void entity(final char[] buff, final int offset, final int length) throws SAXException {
    }
    
    public int getEntity() {
        return 0;
    }
    
    public void eof(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.close();
    }
    
    public void etag(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.print(')');
        this.theWriter.write(buff, offset, length);
        this.theWriter.println();
    }
    
    public void decl(final char[] buff, final int offset, final int length) throws SAXException {
    }
    
    public void gi(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.print('(');
        this.theWriter.write(buff, offset, length);
        this.theWriter.println();
    }
    
    public void cdsect(final char[] buff, final int offset, final int length) throws SAXException {
        this.pcdata(buff, offset, length);
    }
    
    public void pcdata(final char[] buff, final int offset, int length) throws SAXException {
        if (length == 0) {
            return;
        }
        boolean inProgress = false;
        length += offset;
        for (int i = offset; i < length; ++i) {
            if (buff[i] == '\n') {
                if (inProgress) {
                    this.theWriter.println();
                }
                this.theWriter.println("-\\n");
                inProgress = false;
            }
            else {
                if (!inProgress) {
                    this.theWriter.print('-');
                }
                switch (buff[i]) {
                    case '\t': {
                        this.theWriter.print("\\t");
                        break;
                    }
                    case '\\': {
                        this.theWriter.print("\\\\");
                        break;
                    }
                    default: {
                        this.theWriter.print(buff[i]);
                        break;
                    }
                }
                inProgress = true;
            }
        }
        if (inProgress) {
            this.theWriter.println();
        }
    }
    
    public void pitarget(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.print('?');
        this.theWriter.write(buff, offset, length);
        this.theWriter.write(32);
    }
    
    public void pi(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.write(buff, offset, length);
        this.theWriter.println();
    }
    
    public void stagc(final char[] buff, final int offset, final int length) throws SAXException {
    }
    
    public void stage(final char[] buff, final int offset, final int length) throws SAXException {
        this.theWriter.println("!");
    }
    
    public void characters(final char[] buff, final int offset, final int length) throws SAXException {
        this.pcdata(buff, offset, length);
    }
    
    public void endDocument() throws SAXException {
        this.theWriter.close();
    }
    
    public void endElement(final String uri, final String localname, String qname) throws SAXException {
        if (qname.length() == 0) {
            qname = localname;
        }
        this.theWriter.print(')');
        this.theWriter.println(qname);
    }
    
    public void endPrefixMapping(final String prefix) throws SAXException {
    }
    
    public void ignorableWhitespace(final char[] buff, final int offset, final int length) throws SAXException {
        this.characters(buff, offset, length);
    }
    
    public void processingInstruction(final String target, final String data) throws SAXException {
        this.theWriter.print('?');
        this.theWriter.print(target);
        this.theWriter.print(' ');
        this.theWriter.println(data);
    }
    
    public void setDocumentLocator(final Locator locator) {
    }
    
    public void skippedEntity(final String name) throws SAXException {
    }
    
    public void startDocument() throws SAXException {
    }
    
    public void startElement(final String uri, final String localname, String qname, final Attributes atts) throws SAXException {
        if (qname.length() == 0) {
            qname = localname;
        }
        this.theWriter.print('(');
        this.theWriter.println(qname);
        for (int length = atts.getLength(), i = 0; i < length; ++i) {
            qname = atts.getQName(i);
            if (qname.length() == 0) {
                qname = atts.getLocalName(i);
            }
            this.theWriter.print('A');
            this.theWriter.print(qname);
            this.theWriter.print(' ');
            this.theWriter.println(atts.getValue(i));
        }
    }
    
    public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
    }
    
    public void comment(final char[] ch, final int start, final int length) throws SAXException {
        this.cmnt(ch, start, length);
    }
    
    public void endCDATA() throws SAXException {
    }
    
    public void endDTD() throws SAXException {
    }
    
    public void endEntity(final String name) throws SAXException {
    }
    
    public void startCDATA() throws SAXException {
    }
    
    public void startDTD(final String name, final String publicId, final String systemId) throws SAXException {
    }
    
    public void startEntity(final String name) throws SAXException {
    }
    
    public PYXWriter(final Writer w) {
        if (w instanceof PrintWriter) {
            this.theWriter = (PrintWriter)w;
        }
        else {
            this.theWriter = new PrintWriter(w);
        }
    }
    
    static {
        PYXWriter.dummy = new char[1];
    }
}
