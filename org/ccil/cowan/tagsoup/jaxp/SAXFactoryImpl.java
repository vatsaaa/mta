// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup.jaxp;

import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXNotRecognizedException;
import java.util.LinkedHashMap;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import java.util.HashMap;
import javax.xml.parsers.SAXParserFactory;

public class SAXFactoryImpl extends SAXParserFactory
{
    private SAXParserImpl prototypeParser;
    private HashMap features;
    
    public SAXFactoryImpl() {
        this.prototypeParser = null;
        this.features = null;
    }
    
    public SAXParser newSAXParser() throws ParserConfigurationException {
        try {
            return SAXParserImpl.newInstance(this.features);
        }
        catch (SAXException se) {
            throw new ParserConfigurationException(se.getMessage());
        }
    }
    
    public void setFeature(final String name, final boolean value) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException {
        this.getPrototype().setFeature(name, value);
        if (this.features == null) {
            this.features = new LinkedHashMap();
        }
        this.features.put(name, value ? Boolean.TRUE : Boolean.FALSE);
    }
    
    public boolean getFeature(final String name) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException {
        return this.getPrototype().getFeature(name);
    }
    
    private SAXParserImpl getPrototype() {
        if (this.prototypeParser == null) {
            this.prototypeParser = new SAXParserImpl();
        }
        return this.prototypeParser;
    }
}
