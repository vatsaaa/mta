// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup.jaxp;

import org.xml.sax.AttributeList;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXNotSupportedException;
import java.util.Locale;
import org.xml.sax.ErrorHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.DTDHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.DocumentHandler;
import java.io.IOException;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.Parser;

public class SAX1ParserAdapter implements Parser
{
    final XMLReader xmlReader;
    
    public SAX1ParserAdapter(final XMLReader xr) {
        this.xmlReader = xr;
    }
    
    public void parse(final InputSource source) throws SAXException {
        try {
            this.xmlReader.parse(source);
        }
        catch (IOException ioe) {
            throw new SAXException(ioe);
        }
    }
    
    public void parse(final String systemId) throws SAXException {
        try {
            this.xmlReader.parse(systemId);
        }
        catch (IOException ioe) {
            throw new SAXException(ioe);
        }
    }
    
    public void setDocumentHandler(final DocumentHandler h) {
        this.xmlReader.setContentHandler(new DocHandlerWrapper(h));
    }
    
    public void setDTDHandler(final DTDHandler h) {
        this.xmlReader.setDTDHandler(h);
    }
    
    public void setEntityResolver(final EntityResolver r) {
        this.xmlReader.setEntityResolver(r);
    }
    
    public void setErrorHandler(final ErrorHandler h) {
        this.xmlReader.setErrorHandler(h);
    }
    
    public void setLocale(final Locale locale) throws SAXException {
        throw new SAXNotSupportedException("TagSoup does not implement setLocale() method");
    }
    
    static final class DocHandlerWrapper implements ContentHandler
    {
        final DocumentHandler docHandler;
        final AttributesWrapper mAttrWrapper;
        
        DocHandlerWrapper(final DocumentHandler h) {
            this.mAttrWrapper = new AttributesWrapper();
            this.docHandler = h;
        }
        
        public void characters(final char[] ch, final int start, final int length) throws SAXException {
            this.docHandler.characters(ch, start, length);
        }
        
        public void endDocument() throws SAXException {
            this.docHandler.endDocument();
        }
        
        public void endElement(final String uri, final String localName, String qName) throws SAXException {
            if (qName == null) {
                qName = localName;
            }
            this.docHandler.endElement(qName);
        }
        
        public void endPrefixMapping(final String prefix) {
        }
        
        public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
            this.docHandler.ignorableWhitespace(ch, start, length);
        }
        
        public void processingInstruction(final String target, final String data) throws SAXException {
            this.docHandler.processingInstruction(target, data);
        }
        
        public void setDocumentLocator(final Locator locator) {
            this.docHandler.setDocumentLocator(locator);
        }
        
        public void skippedEntity(final String name) {
        }
        
        public void startDocument() throws SAXException {
            this.docHandler.startDocument();
        }
        
        public void startElement(final String uri, final String localName, String qName, final Attributes attrs) throws SAXException {
            if (qName == null) {
                qName = localName;
            }
            this.mAttrWrapper.setAttributes(attrs);
            this.docHandler.startElement(qName, this.mAttrWrapper);
        }
        
        public void startPrefixMapping(final String prefix, final String uri) {
        }
    }
    
    static final class AttributesWrapper implements AttributeList
    {
        Attributes attrs;
        
        public AttributesWrapper() {
        }
        
        public void setAttributes(final Attributes a) {
            this.attrs = a;
        }
        
        public int getLength() {
            return this.attrs.getLength();
        }
        
        public String getName(final int i) {
            final String n = this.attrs.getQName(i);
            return (n == null) ? this.attrs.getLocalName(i) : n;
        }
        
        public String getType(final int i) {
            return this.attrs.getType(i);
        }
        
        public String getType(final String name) {
            return this.attrs.getType(name);
        }
        
        public String getValue(final int i) {
            return this.attrs.getValue(i);
        }
        
        public String getValue(final String name) {
            return this.attrs.getValue(name);
        }
    }
}
