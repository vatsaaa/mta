// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup.jaxp;

import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.XMLReader;
import org.xml.sax.SAXException;
import java.util.Iterator;
import java.util.Map;
import org.ccil.cowan.tagsoup.Parser;
import javax.xml.parsers.SAXParser;

public class SAXParserImpl extends SAXParser
{
    final Parser parser;
    
    protected SAXParserImpl() {
        this.parser = new Parser();
    }
    
    public static SAXParserImpl newInstance(final Map features) throws SAXException {
        final SAXParserImpl parser = new SAXParserImpl();
        if (features != null) {
            for (final Map.Entry entry : features.entrySet()) {
                parser.setFeature(entry.getKey(), entry.getValue());
            }
        }
        return parser;
    }
    
    public org.xml.sax.Parser getParser() throws SAXException {
        return new SAX1ParserAdapter(this.parser);
    }
    
    public XMLReader getXMLReader() {
        return this.parser;
    }
    
    public boolean isNamespaceAware() {
        try {
            return this.parser.getFeature("http://xml.org/sax/features/namespaces");
        }
        catch (SAXException sex) {
            throw new RuntimeException(sex.getMessage());
        }
    }
    
    public boolean isValidating() {
        try {
            return this.parser.getFeature("http://xml.org/sax/features/validation");
        }
        catch (SAXException sex) {
            throw new RuntimeException(sex.getMessage());
        }
    }
    
    public void setProperty(final String name, final Object value) throws SAXNotRecognizedException, SAXNotSupportedException {
        this.parser.setProperty(name, value);
    }
    
    public Object getProperty(final String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        return this.parser.getProperty(name);
    }
    
    public void setFeature(final String name, final boolean value) throws SAXNotRecognizedException, SAXNotSupportedException {
        this.parser.setFeature(name, value);
    }
    
    public boolean getFeature(final String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        return this.parser.getFeature(name);
    }
}
