// 
// Decompiled by Procyon v0.5.36
// 

package org.ccil.cowan.tagsoup.jaxp;

import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class JAXPTest
{
    public static void main(final String[] args) throws Exception {
        new JAXPTest().test(args);
    }
    
    private void test(final String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Usage: java " + this.getClass() + " [input-file]");
            System.exit(1);
        }
        final File f = new File(args[0]);
        System.setProperty("javax.xml.parsers.SAXParserFactory", "org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl");
        final SAXParserFactory spf = SAXParserFactory.newInstance();
        System.out.println("Ok, SAX factory JAXP creates is: " + spf);
        System.out.println("Let's parse...");
        spf.newSAXParser().parse(f, new DefaultHandler());
        System.out.println("Done. And then DOM build:");
        final Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(f);
        System.out.println("Succesfully built DOM tree from '" + f + "', -> " + doc);
    }
}
