// 
// Decompiled by Procyon v0.5.36
// 

package org.slf4j.impl;

import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.ILoggerFactory;

public class Log4jLoggerFactory implements ILoggerFactory
{
    Map loggerMap;
    
    public Log4jLoggerFactory() {
        this.loggerMap = new HashMap();
    }
    
    public Logger getLogger(final String name) {
        Logger slf4jLogger = null;
        synchronized (this) {
            slf4jLogger = this.loggerMap.get(name);
            if (slf4jLogger == null) {
                org.apache.log4j.Logger log4jLogger;
                if (name.equalsIgnoreCase("ROOT")) {
                    log4jLogger = LogManager.getRootLogger();
                }
                else {
                    log4jLogger = LogManager.getLogger(name);
                }
                slf4jLogger = new Log4jLoggerAdapter(log4jLogger);
                this.loggerMap.put(name, slf4jLogger);
            }
        }
        return slf4jLogger;
    }
}
