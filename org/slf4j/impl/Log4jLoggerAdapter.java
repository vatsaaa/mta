// 
// Decompiled by Procyon v0.5.36
// 

package org.slf4j.impl;

import org.slf4j.Marker;
import org.slf4j.helpers.MessageFormatter;
import org.apache.log4j.Priority;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.io.Serializable;
import org.slf4j.spi.LocationAwareLogger;
import org.slf4j.helpers.MarkerIgnoringBase;

public final class Log4jLoggerAdapter extends MarkerIgnoringBase implements LocationAwareLogger, Serializable
{
    private static final long serialVersionUID = 6182834493563598289L;
    final transient org.apache.log4j.Logger logger;
    static final String FQCN;
    final boolean traceCapable;
    
    Log4jLoggerAdapter(final org.apache.log4j.Logger logger) {
        this.logger = logger;
        this.name = logger.getName();
        this.traceCapable = this.isTraceCapable();
    }
    
    private boolean isTraceCapable() {
        try {
            this.logger.isTraceEnabled();
            return true;
        }
        catch (NoSuchMethodError e) {
            return false;
        }
    }
    
    public boolean isTraceEnabled() {
        if (this.traceCapable) {
            return this.logger.isTraceEnabled();
        }
        return this.logger.isDebugEnabled();
    }
    
    public void trace(final String msg) {
        this.logger.log(Log4jLoggerAdapter.FQCN, this.traceCapable ? Level.TRACE : Level.DEBUG, msg, null);
    }
    
    public void trace(final String format, final Object arg) {
        if (this.isTraceEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            this.logger.log(Log4jLoggerAdapter.FQCN, this.traceCapable ? Level.TRACE : Level.DEBUG, msgStr, null);
        }
    }
    
    public void trace(final String format, final Object arg1, final Object arg2) {
        if (this.isTraceEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            this.logger.log(Log4jLoggerAdapter.FQCN, this.traceCapable ? Level.TRACE : Level.DEBUG, msgStr, null);
        }
    }
    
    public void trace(final String format, final Object[] argArray) {
        if (this.isTraceEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            this.logger.log(Log4jLoggerAdapter.FQCN, this.traceCapable ? Level.TRACE : Level.DEBUG, msgStr, null);
        }
    }
    
    public void trace(final String msg, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, this.traceCapable ? Level.TRACE : Level.DEBUG, msg, t);
    }
    
    public boolean isDebugEnabled() {
        return this.logger.isDebugEnabled();
    }
    
    public void debug(final String msg) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, msg, null);
    }
    
    public void debug(final String format, final Object arg) {
        if (this.logger.isDebugEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, msgStr, null);
        }
    }
    
    public void debug(final String format, final Object arg1, final Object arg2) {
        if (this.logger.isDebugEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, msgStr, null);
        }
    }
    
    public void debug(final String format, final Object[] argArray) {
        if (this.logger.isDebugEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, msgStr, null);
        }
    }
    
    public void debug(final String msg, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, msg, t);
    }
    
    public boolean isInfoEnabled() {
        return this.logger.isInfoEnabled();
    }
    
    public void info(final String msg) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, msg, null);
    }
    
    public void info(final String format, final Object arg) {
        if (this.logger.isInfoEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, msgStr, null);
        }
    }
    
    public void info(final String format, final Object arg1, final Object arg2) {
        if (this.logger.isInfoEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, msgStr, null);
        }
    }
    
    public void info(final String format, final Object[] argArray) {
        if (this.logger.isInfoEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, msgStr, null);
        }
    }
    
    public void info(final String msg, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, msg, t);
    }
    
    public boolean isWarnEnabled() {
        return this.logger.isEnabledFor(Level.WARN);
    }
    
    public void warn(final String msg) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, msg, null);
    }
    
    public void warn(final String format, final Object arg) {
        if (this.logger.isEnabledFor(Level.WARN)) {
            final String msgStr = MessageFormatter.format(format, arg);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, msgStr, null);
        }
    }
    
    public void warn(final String format, final Object arg1, final Object arg2) {
        if (this.logger.isEnabledFor(Level.WARN)) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, msgStr, null);
        }
    }
    
    public void warn(final String format, final Object[] argArray) {
        if (this.logger.isEnabledFor(Level.WARN)) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, msgStr, null);
        }
    }
    
    public void warn(final String msg, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, msg, t);
    }
    
    public boolean isErrorEnabled() {
        return this.logger.isEnabledFor(Level.ERROR);
    }
    
    public void error(final String msg) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, msg, null);
    }
    
    public void error(final String format, final Object arg) {
        if (this.logger.isEnabledFor(Level.ERROR)) {
            final String msgStr = MessageFormatter.format(format, arg);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, msgStr, null);
        }
    }
    
    public void error(final String format, final Object arg1, final Object arg2) {
        if (this.logger.isEnabledFor(Level.ERROR)) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, msgStr, null);
        }
    }
    
    public void error(final String format, final Object[] argArray) {
        if (this.logger.isEnabledFor(Level.ERROR)) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, msgStr, null);
        }
    }
    
    public void error(final String msg, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, msg, t);
    }
    
    public void log(final Marker marker, final String callerFQCN, final int level, final String msg, final Throwable t) {
        Level log4jLevel = null;
        switch (level) {
            case 0: {
                log4jLevel = (this.traceCapable ? Level.TRACE : Level.DEBUG);
                break;
            }
            case 10: {
                log4jLevel = Level.DEBUG;
                break;
            }
            case 20: {
                log4jLevel = Level.INFO;
                break;
            }
            case 30: {
                log4jLevel = Level.WARN;
                break;
            }
            case 40: {
                log4jLevel = Level.ERROR;
                break;
            }
            default: {
                throw new IllegalStateException("Level number " + level + " is not recognized.");
            }
        }
        this.logger.log(callerFQCN, log4jLevel, msg, t);
    }
    
    static {
        FQCN = Log4jLoggerAdapter.class.getName();
    }
}
