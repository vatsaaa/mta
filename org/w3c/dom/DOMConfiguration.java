// 
// Decompiled by Procyon v0.5.36
// 

package org.w3c.dom;

public interface DOMConfiguration
{
    boolean canSetParameter(final String p0, final Object p1);
    
    Object getParameter(final String p0);
    
    DOMStringList getParameterNames();
    
    void setParameter(final String p0, final Object p1);
}
