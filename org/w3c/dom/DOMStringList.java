// 
// Decompiled by Procyon v0.5.36
// 

package org.w3c.dom;

public interface DOMStringList
{
    boolean contains(final String p0);
    
    int getLength();
    
    String item(final int p0);
}
