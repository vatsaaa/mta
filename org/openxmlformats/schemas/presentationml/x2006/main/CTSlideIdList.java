// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSlideIdList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSlideIdList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctslideidlist70a5type");
    
    List<CTSlideIdListEntry> getSldIdList();
    
    @Deprecated
    CTSlideIdListEntry[] getSldIdArray();
    
    CTSlideIdListEntry getSldIdArray(final int p0);
    
    int sizeOfSldIdArray();
    
    void setSldIdArray(final CTSlideIdListEntry[] p0);
    
    void setSldIdArray(final int p0, final CTSlideIdListEntry p1);
    
    CTSlideIdListEntry insertNewSldId(final int p0);
    
    CTSlideIdListEntry addNewSldId();
    
    void removeSldId(final int p0);
    
    public static final class Factory
    {
        public static CTSlideIdList newInstance() {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().newInstance(CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList newInstance(final XmlOptions xmlOptions) {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().newInstance(CTSlideIdList.type, xmlOptions);
        }
        
        public static CTSlideIdList parse(final String s) throws XmlException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(s, CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(s, CTSlideIdList.type, xmlOptions);
        }
        
        public static CTSlideIdList parse(final File file) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(file, CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(file, CTSlideIdList.type, xmlOptions);
        }
        
        public static CTSlideIdList parse(final URL url) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(url, CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(url, CTSlideIdList.type, xmlOptions);
        }
        
        public static CTSlideIdList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideIdList.type, xmlOptions);
        }
        
        public static CTSlideIdList parse(final Reader reader) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(reader, CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(reader, CTSlideIdList.type, xmlOptions);
        }
        
        public static CTSlideIdList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideIdList.type, xmlOptions);
        }
        
        public static CTSlideIdList parse(final Node node) throws XmlException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(node, CTSlideIdList.type, null);
        }
        
        public static CTSlideIdList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(node, CTSlideIdList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSlideIdList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideIdList.type, null);
        }
        
        @Deprecated
        public static CTSlideIdList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSlideIdList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideIdList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideIdList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideIdList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
