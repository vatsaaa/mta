// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface NotesMasterDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(NotesMasterDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("notesmaster8840doctype");
    
    CTNotesMaster getNotesMaster();
    
    void setNotesMaster(final CTNotesMaster p0);
    
    CTNotesMaster addNewNotesMaster();
    
    public static final class Factory
    {
        public static NotesMasterDocument newInstance() {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().newInstance(NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument newInstance(final XmlOptions xmlOptions) {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().newInstance(NotesMasterDocument.type, xmlOptions);
        }
        
        public static NotesMasterDocument parse(final String s) throws XmlException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(s, NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(s, NotesMasterDocument.type, xmlOptions);
        }
        
        public static NotesMasterDocument parse(final File file) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(file, NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(file, NotesMasterDocument.type, xmlOptions);
        }
        
        public static NotesMasterDocument parse(final URL url) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(url, NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(url, NotesMasterDocument.type, xmlOptions);
        }
        
        public static NotesMasterDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(inputStream, NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(inputStream, NotesMasterDocument.type, xmlOptions);
        }
        
        public static NotesMasterDocument parse(final Reader reader) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(reader, NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(reader, NotesMasterDocument.type, xmlOptions);
        }
        
        public static NotesMasterDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, NotesMasterDocument.type, xmlOptions);
        }
        
        public static NotesMasterDocument parse(final Node node) throws XmlException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(node, NotesMasterDocument.type, null);
        }
        
        public static NotesMasterDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(node, NotesMasterDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static NotesMasterDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, NotesMasterDocument.type, null);
        }
        
        @Deprecated
        public static NotesMasterDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (NotesMasterDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, NotesMasterDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, NotesMasterDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, NotesMasterDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
