// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface SldLayoutDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(SldLayoutDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sldlayout638edoctype");
    
    CTSlideLayout getSldLayout();
    
    void setSldLayout(final CTSlideLayout p0);
    
    CTSlideLayout addNewSldLayout();
    
    public static final class Factory
    {
        public static SldLayoutDocument newInstance() {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().newInstance(SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument newInstance(final XmlOptions xmlOptions) {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().newInstance(SldLayoutDocument.type, xmlOptions);
        }
        
        public static SldLayoutDocument parse(final String s) throws XmlException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(s, SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(s, SldLayoutDocument.type, xmlOptions);
        }
        
        public static SldLayoutDocument parse(final File file) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(file, SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(file, SldLayoutDocument.type, xmlOptions);
        }
        
        public static SldLayoutDocument parse(final URL url) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(url, SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(url, SldLayoutDocument.type, xmlOptions);
        }
        
        public static SldLayoutDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(inputStream, SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(inputStream, SldLayoutDocument.type, xmlOptions);
        }
        
        public static SldLayoutDocument parse(final Reader reader) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(reader, SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(reader, SldLayoutDocument.type, xmlOptions);
        }
        
        public static SldLayoutDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, SldLayoutDocument.type, xmlOptions);
        }
        
        public static SldLayoutDocument parse(final Node node) throws XmlException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(node, SldLayoutDocument.type, null);
        }
        
        public static SldLayoutDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(node, SldLayoutDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static SldLayoutDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, SldLayoutDocument.type, null);
        }
        
        @Deprecated
        public static SldLayoutDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (SldLayoutDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, SldLayoutDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, SldLayoutDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, SldLayoutDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
