// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STSlideId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STSlideId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stslideida0b3type");
    
    public static final class Factory
    {
        public static STSlideId newValue(final Object o) {
            return (STSlideId)STSlideId.type.newValue(o);
        }
        
        public static STSlideId newInstance() {
            return (STSlideId)XmlBeans.getContextTypeLoader().newInstance(STSlideId.type, null);
        }
        
        public static STSlideId newInstance(final XmlOptions xmlOptions) {
            return (STSlideId)XmlBeans.getContextTypeLoader().newInstance(STSlideId.type, xmlOptions);
        }
        
        public static STSlideId parse(final String s) throws XmlException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(s, STSlideId.type, null);
        }
        
        public static STSlideId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(s, STSlideId.type, xmlOptions);
        }
        
        public static STSlideId parse(final File file) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(file, STSlideId.type, null);
        }
        
        public static STSlideId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(file, STSlideId.type, xmlOptions);
        }
        
        public static STSlideId parse(final URL url) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(url, STSlideId.type, null);
        }
        
        public static STSlideId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(url, STSlideId.type, xmlOptions);
        }
        
        public static STSlideId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(inputStream, STSlideId.type, null);
        }
        
        public static STSlideId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(inputStream, STSlideId.type, xmlOptions);
        }
        
        public static STSlideId parse(final Reader reader) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(reader, STSlideId.type, null);
        }
        
        public static STSlideId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(reader, STSlideId.type, xmlOptions);
        }
        
        public static STSlideId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSlideId.type, null);
        }
        
        public static STSlideId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSlideId.type, xmlOptions);
        }
        
        public static STSlideId parse(final Node node) throws XmlException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(node, STSlideId.type, null);
        }
        
        public static STSlideId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(node, STSlideId.type, xmlOptions);
        }
        
        @Deprecated
        public static STSlideId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSlideId.type, null);
        }
        
        @Deprecated
        public static STSlideId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STSlideId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSlideId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSlideId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSlideId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
