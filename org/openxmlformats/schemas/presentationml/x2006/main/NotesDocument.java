// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface NotesDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(NotesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("notes4a02doctype");
    
    CTNotesSlide getNotes();
    
    void setNotes(final CTNotesSlide p0);
    
    CTNotesSlide addNewNotes();
    
    public static final class Factory
    {
        public static NotesDocument newInstance() {
            return (NotesDocument)XmlBeans.getContextTypeLoader().newInstance(NotesDocument.type, null);
        }
        
        public static NotesDocument newInstance(final XmlOptions xmlOptions) {
            return (NotesDocument)XmlBeans.getContextTypeLoader().newInstance(NotesDocument.type, xmlOptions);
        }
        
        public static NotesDocument parse(final String s) throws XmlException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(s, NotesDocument.type, null);
        }
        
        public static NotesDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(s, NotesDocument.type, xmlOptions);
        }
        
        public static NotesDocument parse(final File file) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(file, NotesDocument.type, null);
        }
        
        public static NotesDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(file, NotesDocument.type, xmlOptions);
        }
        
        public static NotesDocument parse(final URL url) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(url, NotesDocument.type, null);
        }
        
        public static NotesDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(url, NotesDocument.type, xmlOptions);
        }
        
        public static NotesDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, NotesDocument.type, null);
        }
        
        public static NotesDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, NotesDocument.type, xmlOptions);
        }
        
        public static NotesDocument parse(final Reader reader) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(reader, NotesDocument.type, null);
        }
        
        public static NotesDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(reader, NotesDocument.type, xmlOptions);
        }
        
        public static NotesDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, NotesDocument.type, null);
        }
        
        public static NotesDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, NotesDocument.type, xmlOptions);
        }
        
        public static NotesDocument parse(final Node node) throws XmlException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(node, NotesDocument.type, null);
        }
        
        public static NotesDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(node, NotesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static NotesDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, NotesDocument.type, null);
        }
        
        @Deprecated
        public static NotesDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (NotesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, NotesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, NotesDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, NotesDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
