// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTCommentAuthorList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTCommentAuthorList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcommentauthorlisteb07type");
    
    List<CTCommentAuthor> getCmAuthorList();
    
    @Deprecated
    CTCommentAuthor[] getCmAuthorArray();
    
    CTCommentAuthor getCmAuthorArray(final int p0);
    
    int sizeOfCmAuthorArray();
    
    void setCmAuthorArray(final CTCommentAuthor[] p0);
    
    void setCmAuthorArray(final int p0, final CTCommentAuthor p1);
    
    CTCommentAuthor insertNewCmAuthor(final int p0);
    
    CTCommentAuthor addNewCmAuthor();
    
    void removeCmAuthor(final int p0);
    
    public static final class Factory
    {
        public static CTCommentAuthorList newInstance() {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().newInstance(CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList newInstance(final XmlOptions xmlOptions) {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().newInstance(CTCommentAuthorList.type, xmlOptions);
        }
        
        public static CTCommentAuthorList parse(final String s) throws XmlException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(s, CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(s, CTCommentAuthorList.type, xmlOptions);
        }
        
        public static CTCommentAuthorList parse(final File file) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(file, CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(file, CTCommentAuthorList.type, xmlOptions);
        }
        
        public static CTCommentAuthorList parse(final URL url) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(url, CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(url, CTCommentAuthorList.type, xmlOptions);
        }
        
        public static CTCommentAuthorList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(inputStream, CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(inputStream, CTCommentAuthorList.type, xmlOptions);
        }
        
        public static CTCommentAuthorList parse(final Reader reader) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(reader, CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(reader, CTCommentAuthorList.type, xmlOptions);
        }
        
        public static CTCommentAuthorList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCommentAuthorList.type, xmlOptions);
        }
        
        public static CTCommentAuthorList parse(final Node node) throws XmlException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(node, CTCommentAuthorList.type, null);
        }
        
        public static CTCommentAuthorList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(node, CTCommentAuthorList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTCommentAuthorList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCommentAuthorList.type, null);
        }
        
        @Deprecated
        public static CTCommentAuthorList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTCommentAuthorList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCommentAuthorList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCommentAuthorList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCommentAuthorList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
