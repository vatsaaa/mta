// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CmAuthorLstDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CmAuthorLstDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cmauthorlst86abdoctype");
    
    CTCommentAuthorList getCmAuthorLst();
    
    void setCmAuthorLst(final CTCommentAuthorList p0);
    
    CTCommentAuthorList addNewCmAuthorLst();
    
    public static final class Factory
    {
        public static CmAuthorLstDocument newInstance() {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().newInstance(CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument newInstance(final XmlOptions xmlOptions) {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().newInstance(CmAuthorLstDocument.type, xmlOptions);
        }
        
        public static CmAuthorLstDocument parse(final String s) throws XmlException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(s, CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(s, CmAuthorLstDocument.type, xmlOptions);
        }
        
        public static CmAuthorLstDocument parse(final File file) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(file, CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(file, CmAuthorLstDocument.type, xmlOptions);
        }
        
        public static CmAuthorLstDocument parse(final URL url) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(url, CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(url, CmAuthorLstDocument.type, xmlOptions);
        }
        
        public static CmAuthorLstDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CmAuthorLstDocument.type, xmlOptions);
        }
        
        public static CmAuthorLstDocument parse(final Reader reader) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(reader, CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(reader, CmAuthorLstDocument.type, xmlOptions);
        }
        
        public static CmAuthorLstDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CmAuthorLstDocument.type, xmlOptions);
        }
        
        public static CmAuthorLstDocument parse(final Node node) throws XmlException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(node, CmAuthorLstDocument.type, null);
        }
        
        public static CmAuthorLstDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(node, CmAuthorLstDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static CmAuthorLstDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CmAuthorLstDocument.type, null);
        }
        
        @Deprecated
        public static CmAuthorLstDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CmAuthorLstDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CmAuthorLstDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CmAuthorLstDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CmAuthorLstDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
