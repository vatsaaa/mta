// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTCustomerDataList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTCustomerDataList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcustomerdatalist8b7ftype");
    
    List<CTCustomerData> getCustDataList();
    
    @Deprecated
    CTCustomerData[] getCustDataArray();
    
    CTCustomerData getCustDataArray(final int p0);
    
    int sizeOfCustDataArray();
    
    void setCustDataArray(final CTCustomerData[] p0);
    
    void setCustDataArray(final int p0, final CTCustomerData p1);
    
    CTCustomerData insertNewCustData(final int p0);
    
    CTCustomerData addNewCustData();
    
    void removeCustData(final int p0);
    
    CTTagsData getTags();
    
    boolean isSetTags();
    
    void setTags(final CTTagsData p0);
    
    CTTagsData addNewTags();
    
    void unsetTags();
    
    public static final class Factory
    {
        public static CTCustomerDataList newInstance() {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().newInstance(CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList newInstance(final XmlOptions xmlOptions) {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().newInstance(CTCustomerDataList.type, xmlOptions);
        }
        
        public static CTCustomerDataList parse(final String s) throws XmlException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(s, CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(s, CTCustomerDataList.type, xmlOptions);
        }
        
        public static CTCustomerDataList parse(final File file) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(file, CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(file, CTCustomerDataList.type, xmlOptions);
        }
        
        public static CTCustomerDataList parse(final URL url) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(url, CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(url, CTCustomerDataList.type, xmlOptions);
        }
        
        public static CTCustomerDataList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(inputStream, CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(inputStream, CTCustomerDataList.type, xmlOptions);
        }
        
        public static CTCustomerDataList parse(final Reader reader) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(reader, CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(reader, CTCustomerDataList.type, xmlOptions);
        }
        
        public static CTCustomerDataList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCustomerDataList.type, xmlOptions);
        }
        
        public static CTCustomerDataList parse(final Node node) throws XmlException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(node, CTCustomerDataList.type, null);
        }
        
        public static CTCustomerDataList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(node, CTCustomerDataList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTCustomerDataList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCustomerDataList.type, null);
        }
        
        @Deprecated
        public static CTCustomerDataList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTCustomerDataList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCustomerDataList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCustomerDataList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCustomerDataList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
