// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNotesMasterIdList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNotesMasterIdList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnotesmasteridlist2853type");
    
    CTNotesMasterIdListEntry getNotesMasterId();
    
    boolean isSetNotesMasterId();
    
    void setNotesMasterId(final CTNotesMasterIdListEntry p0);
    
    CTNotesMasterIdListEntry addNewNotesMasterId();
    
    void unsetNotesMasterId();
    
    public static final class Factory
    {
        public static CTNotesMasterIdList newInstance() {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().newInstance(CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList newInstance(final XmlOptions xmlOptions) {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().newInstance(CTNotesMasterIdList.type, xmlOptions);
        }
        
        public static CTNotesMasterIdList parse(final String s) throws XmlException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(s, CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(s, CTNotesMasterIdList.type, xmlOptions);
        }
        
        public static CTNotesMasterIdList parse(final File file) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(file, CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(file, CTNotesMasterIdList.type, xmlOptions);
        }
        
        public static CTNotesMasterIdList parse(final URL url) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(url, CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(url, CTNotesMasterIdList.type, xmlOptions);
        }
        
        public static CTNotesMasterIdList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(inputStream, CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(inputStream, CTNotesMasterIdList.type, xmlOptions);
        }
        
        public static CTNotesMasterIdList parse(final Reader reader) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(reader, CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(reader, CTNotesMasterIdList.type, xmlOptions);
        }
        
        public static CTNotesMasterIdList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNotesMasterIdList.type, xmlOptions);
        }
        
        public static CTNotesMasterIdList parse(final Node node) throws XmlException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(node, CTNotesMasterIdList.type, null);
        }
        
        public static CTNotesMasterIdList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(node, CTNotesMasterIdList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNotesMasterIdList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNotesMasterIdList.type, null);
        }
        
        @Deprecated
        public static CTNotesMasterIdList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNotesMasterIdList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNotesMasterIdList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNotesMasterIdList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNotesMasterIdList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
