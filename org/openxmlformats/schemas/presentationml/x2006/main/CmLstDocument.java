// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CmLstDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CmLstDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cmlst3880doctype");
    
    CTCommentList getCmLst();
    
    void setCmLst(final CTCommentList p0);
    
    CTCommentList addNewCmLst();
    
    public static final class Factory
    {
        public static CmLstDocument newInstance() {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().newInstance(CmLstDocument.type, null);
        }
        
        public static CmLstDocument newInstance(final XmlOptions xmlOptions) {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().newInstance(CmLstDocument.type, xmlOptions);
        }
        
        public static CmLstDocument parse(final String s) throws XmlException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(s, CmLstDocument.type, null);
        }
        
        public static CmLstDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(s, CmLstDocument.type, xmlOptions);
        }
        
        public static CmLstDocument parse(final File file) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(file, CmLstDocument.type, null);
        }
        
        public static CmLstDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(file, CmLstDocument.type, xmlOptions);
        }
        
        public static CmLstDocument parse(final URL url) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(url, CmLstDocument.type, null);
        }
        
        public static CmLstDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(url, CmLstDocument.type, xmlOptions);
        }
        
        public static CmLstDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CmLstDocument.type, null);
        }
        
        public static CmLstDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CmLstDocument.type, xmlOptions);
        }
        
        public static CmLstDocument parse(final Reader reader) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(reader, CmLstDocument.type, null);
        }
        
        public static CmLstDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(reader, CmLstDocument.type, xmlOptions);
        }
        
        public static CmLstDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CmLstDocument.type, null);
        }
        
        public static CmLstDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CmLstDocument.type, xmlOptions);
        }
        
        public static CmLstDocument parse(final Node node) throws XmlException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(node, CmLstDocument.type, null);
        }
        
        public static CmLstDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(node, CmLstDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static CmLstDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CmLstDocument.type, null);
        }
        
        @Deprecated
        public static CmLstDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CmLstDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CmLstDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CmLstDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CmLstDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
