// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STName extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STName.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stnameadaatype");
    
    public static final class Factory
    {
        public static STName newValue(final Object o) {
            return (STName)STName.type.newValue(o);
        }
        
        public static STName newInstance() {
            return (STName)XmlBeans.getContextTypeLoader().newInstance(STName.type, null);
        }
        
        public static STName newInstance(final XmlOptions xmlOptions) {
            return (STName)XmlBeans.getContextTypeLoader().newInstance(STName.type, xmlOptions);
        }
        
        public static STName parse(final String s) throws XmlException {
            return (STName)XmlBeans.getContextTypeLoader().parse(s, STName.type, null);
        }
        
        public static STName parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STName)XmlBeans.getContextTypeLoader().parse(s, STName.type, xmlOptions);
        }
        
        public static STName parse(final File file) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(file, STName.type, null);
        }
        
        public static STName parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(file, STName.type, xmlOptions);
        }
        
        public static STName parse(final URL url) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(url, STName.type, null);
        }
        
        public static STName parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(url, STName.type, xmlOptions);
        }
        
        public static STName parse(final InputStream inputStream) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(inputStream, STName.type, null);
        }
        
        public static STName parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(inputStream, STName.type, xmlOptions);
        }
        
        public static STName parse(final Reader reader) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(reader, STName.type, null);
        }
        
        public static STName parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STName)XmlBeans.getContextTypeLoader().parse(reader, STName.type, xmlOptions);
        }
        
        public static STName parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STName)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STName.type, null);
        }
        
        public static STName parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STName)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STName.type, xmlOptions);
        }
        
        public static STName parse(final Node node) throws XmlException {
            return (STName)XmlBeans.getContextTypeLoader().parse(node, STName.type, null);
        }
        
        public static STName parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STName)XmlBeans.getContextTypeLoader().parse(node, STName.type, xmlOptions);
        }
        
        @Deprecated
        public static STName parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STName)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STName.type, null);
        }
        
        @Deprecated
        public static STName parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STName)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STName.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STName.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STName.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
