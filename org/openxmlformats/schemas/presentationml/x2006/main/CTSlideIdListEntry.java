// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSlideIdListEntry extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSlideIdListEntry.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctslideidlistentry427dtype");
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    long getId();
    
    STSlideId xgetId();
    
    void setId(final long p0);
    
    void xsetId(final STSlideId p0);
    
    String getId2();
    
    STRelationshipId xgetId2();
    
    void setId2(final String p0);
    
    void xsetId2(final STRelationshipId p0);
    
    public static final class Factory
    {
        public static CTSlideIdListEntry newInstance() {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().newInstance(CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry newInstance(final XmlOptions xmlOptions) {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().newInstance(CTSlideIdListEntry.type, xmlOptions);
        }
        
        public static CTSlideIdListEntry parse(final String s) throws XmlException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(s, CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(s, CTSlideIdListEntry.type, xmlOptions);
        }
        
        public static CTSlideIdListEntry parse(final File file) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(file, CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(file, CTSlideIdListEntry.type, xmlOptions);
        }
        
        public static CTSlideIdListEntry parse(final URL url) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(url, CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(url, CTSlideIdListEntry.type, xmlOptions);
        }
        
        public static CTSlideIdListEntry parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideIdListEntry.type, xmlOptions);
        }
        
        public static CTSlideIdListEntry parse(final Reader reader) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(reader, CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(reader, CTSlideIdListEntry.type, xmlOptions);
        }
        
        public static CTSlideIdListEntry parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideIdListEntry.type, xmlOptions);
        }
        
        public static CTSlideIdListEntry parse(final Node node) throws XmlException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(node, CTSlideIdListEntry.type, null);
        }
        
        public static CTSlideIdListEntry parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(node, CTSlideIdListEntry.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSlideIdListEntry parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideIdListEntry.type, null);
        }
        
        @Deprecated
        public static CTSlideIdListEntry parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSlideIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideIdListEntry.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideIdListEntry.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideIdListEntry.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
