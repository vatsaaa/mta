// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSlideSize extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSlideSize.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctslidesizeb0fdtype");
    
    int getCx();
    
    STSlideSizeCoordinate xgetCx();
    
    void setCx(final int p0);
    
    void xsetCx(final STSlideSizeCoordinate p0);
    
    int getCy();
    
    STSlideSizeCoordinate xgetCy();
    
    void setCy(final int p0);
    
    void xsetCy(final STSlideSizeCoordinate p0);
    
    STSlideSizeType.Enum getType();
    
    STSlideSizeType xgetType();
    
    boolean isSetType();
    
    void setType(final STSlideSizeType.Enum p0);
    
    void xsetType(final STSlideSizeType p0);
    
    void unsetType();
    
    public static final class Factory
    {
        public static CTSlideSize newInstance() {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().newInstance(CTSlideSize.type, null);
        }
        
        public static CTSlideSize newInstance(final XmlOptions xmlOptions) {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().newInstance(CTSlideSize.type, xmlOptions);
        }
        
        public static CTSlideSize parse(final String s) throws XmlException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(s, CTSlideSize.type, null);
        }
        
        public static CTSlideSize parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(s, CTSlideSize.type, xmlOptions);
        }
        
        public static CTSlideSize parse(final File file) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(file, CTSlideSize.type, null);
        }
        
        public static CTSlideSize parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(file, CTSlideSize.type, xmlOptions);
        }
        
        public static CTSlideSize parse(final URL url) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(url, CTSlideSize.type, null);
        }
        
        public static CTSlideSize parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(url, CTSlideSize.type, xmlOptions);
        }
        
        public static CTSlideSize parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideSize.type, null);
        }
        
        public static CTSlideSize parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideSize.type, xmlOptions);
        }
        
        public static CTSlideSize parse(final Reader reader) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(reader, CTSlideSize.type, null);
        }
        
        public static CTSlideSize parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(reader, CTSlideSize.type, xmlOptions);
        }
        
        public static CTSlideSize parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideSize.type, null);
        }
        
        public static CTSlideSize parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideSize.type, xmlOptions);
        }
        
        public static CTSlideSize parse(final Node node) throws XmlException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(node, CTSlideSize.type, null);
        }
        
        public static CTSlideSize parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(node, CTSlideSize.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSlideSize parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideSize.type, null);
        }
        
        @Deprecated
        public static CTSlideSize parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSlideSize)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideSize.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideSize.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideSize.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
