// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNotesMasterIdListEntry extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNotesMasterIdListEntry.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnotesmasteridlistentry278ftype");
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    String getId();
    
    STRelationshipId xgetId();
    
    void setId(final String p0);
    
    void xsetId(final STRelationshipId p0);
    
    public static final class Factory
    {
        public static CTNotesMasterIdListEntry newInstance() {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().newInstance(CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry newInstance(final XmlOptions xmlOptions) {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().newInstance(CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        public static CTNotesMasterIdListEntry parse(final String s) throws XmlException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(s, CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(s, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        public static CTNotesMasterIdListEntry parse(final File file) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(file, CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(file, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        public static CTNotesMasterIdListEntry parse(final URL url) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(url, CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(url, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        public static CTNotesMasterIdListEntry parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(inputStream, CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(inputStream, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        public static CTNotesMasterIdListEntry parse(final Reader reader) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(reader, CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(reader, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        public static CTNotesMasterIdListEntry parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        public static CTNotesMasterIdListEntry parse(final Node node) throws XmlException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(node, CTNotesMasterIdListEntry.type, null);
        }
        
        public static CTNotesMasterIdListEntry parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(node, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNotesMasterIdListEntry parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNotesMasterIdListEntry.type, null);
        }
        
        @Deprecated
        public static CTNotesMasterIdListEntry parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNotesMasterIdListEntry)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNotesMasterIdListEntry.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNotesMasterIdListEntry.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
