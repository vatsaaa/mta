// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTConnector extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTConnector.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctconnector3522type");
    
    CTConnectorNonVisual getNvCxnSpPr();
    
    void setNvCxnSpPr(final CTConnectorNonVisual p0);
    
    CTConnectorNonVisual addNewNvCxnSpPr();
    
    CTShapeProperties getSpPr();
    
    void setSpPr(final CTShapeProperties p0);
    
    CTShapeProperties addNewSpPr();
    
    CTShapeStyle getStyle();
    
    boolean isSetStyle();
    
    void setStyle(final CTShapeStyle p0);
    
    CTShapeStyle addNewStyle();
    
    void unsetStyle();
    
    CTExtensionListModify getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionListModify p0);
    
    CTExtensionListModify addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTConnector newInstance() {
            return (CTConnector)XmlBeans.getContextTypeLoader().newInstance(CTConnector.type, null);
        }
        
        public static CTConnector newInstance(final XmlOptions xmlOptions) {
            return (CTConnector)XmlBeans.getContextTypeLoader().newInstance(CTConnector.type, xmlOptions);
        }
        
        public static CTConnector parse(final String s) throws XmlException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(s, CTConnector.type, null);
        }
        
        public static CTConnector parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(s, CTConnector.type, xmlOptions);
        }
        
        public static CTConnector parse(final File file) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(file, CTConnector.type, null);
        }
        
        public static CTConnector parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(file, CTConnector.type, xmlOptions);
        }
        
        public static CTConnector parse(final URL url) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(url, CTConnector.type, null);
        }
        
        public static CTConnector parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(url, CTConnector.type, xmlOptions);
        }
        
        public static CTConnector parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(inputStream, CTConnector.type, null);
        }
        
        public static CTConnector parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(inputStream, CTConnector.type, xmlOptions);
        }
        
        public static CTConnector parse(final Reader reader) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(reader, CTConnector.type, null);
        }
        
        public static CTConnector parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(reader, CTConnector.type, xmlOptions);
        }
        
        public static CTConnector parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTConnector.type, null);
        }
        
        public static CTConnector parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTConnector.type, xmlOptions);
        }
        
        public static CTConnector parse(final Node node) throws XmlException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(node, CTConnector.type, null);
        }
        
        public static CTConnector parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(node, CTConnector.type, xmlOptions);
        }
        
        @Deprecated
        public static CTConnector parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTConnector.type, null);
        }
        
        @Deprecated
        public static CTConnector parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTConnector)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTConnector.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTConnector.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTConnector.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
