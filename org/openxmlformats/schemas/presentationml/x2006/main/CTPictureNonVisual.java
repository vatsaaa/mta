// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualPictureProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPictureNonVisual extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPictureNonVisual.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpicturenonvisualb236type");
    
    CTNonVisualDrawingProps getCNvPr();
    
    void setCNvPr(final CTNonVisualDrawingProps p0);
    
    CTNonVisualDrawingProps addNewCNvPr();
    
    CTNonVisualPictureProperties getCNvPicPr();
    
    void setCNvPicPr(final CTNonVisualPictureProperties p0);
    
    CTNonVisualPictureProperties addNewCNvPicPr();
    
    CTApplicationNonVisualDrawingProps getNvPr();
    
    void setNvPr(final CTApplicationNonVisualDrawingProps p0);
    
    CTApplicationNonVisualDrawingProps addNewNvPr();
    
    public static final class Factory
    {
        public static CTPictureNonVisual newInstance() {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().newInstance(CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual newInstance(final XmlOptions xmlOptions) {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().newInstance(CTPictureNonVisual.type, xmlOptions);
        }
        
        public static CTPictureNonVisual parse(final String s) throws XmlException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(s, CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(s, CTPictureNonVisual.type, xmlOptions);
        }
        
        public static CTPictureNonVisual parse(final File file) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(file, CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(file, CTPictureNonVisual.type, xmlOptions);
        }
        
        public static CTPictureNonVisual parse(final URL url) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(url, CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(url, CTPictureNonVisual.type, xmlOptions);
        }
        
        public static CTPictureNonVisual parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(inputStream, CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(inputStream, CTPictureNonVisual.type, xmlOptions);
        }
        
        public static CTPictureNonVisual parse(final Reader reader) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(reader, CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(reader, CTPictureNonVisual.type, xmlOptions);
        }
        
        public static CTPictureNonVisual parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPictureNonVisual.type, xmlOptions);
        }
        
        public static CTPictureNonVisual parse(final Node node) throws XmlException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(node, CTPictureNonVisual.type, null);
        }
        
        public static CTPictureNonVisual parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(node, CTPictureNonVisual.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPictureNonVisual parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPictureNonVisual.type, null);
        }
        
        @Deprecated
        public static CTPictureNonVisual parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPictureNonVisual)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPictureNonVisual.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPictureNonVisual.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPictureNonVisual.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
