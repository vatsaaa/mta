// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGraphicalObjectFrame extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGraphicalObjectFrame.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgraphicalobjectframebfeatype");
    
    CTGraphicalObjectFrameNonVisual getNvGraphicFramePr();
    
    void setNvGraphicFramePr(final CTGraphicalObjectFrameNonVisual p0);
    
    CTGraphicalObjectFrameNonVisual addNewNvGraphicFramePr();
    
    CTTransform2D getXfrm();
    
    void setXfrm(final CTTransform2D p0);
    
    CTTransform2D addNewXfrm();
    
    CTGraphicalObject getGraphic();
    
    void setGraphic(final CTGraphicalObject p0);
    
    CTGraphicalObject addNewGraphic();
    
    CTExtensionListModify getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionListModify p0);
    
    CTExtensionListModify addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTGraphicalObjectFrame newInstance() {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().newInstance(CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame newInstance(final XmlOptions xmlOptions) {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().newInstance(CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        public static CTGraphicalObjectFrame parse(final String s) throws XmlException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(s, CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(s, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        public static CTGraphicalObjectFrame parse(final File file) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(file, CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(file, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        public static CTGraphicalObjectFrame parse(final URL url) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(url, CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(url, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        public static CTGraphicalObjectFrame parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(inputStream, CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(inputStream, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        public static CTGraphicalObjectFrame parse(final Reader reader) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(reader, CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(reader, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        public static CTGraphicalObjectFrame parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        public static CTGraphicalObjectFrame parse(final Node node) throws XmlException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(node, CTGraphicalObjectFrame.type, null);
        }
        
        public static CTGraphicalObjectFrame parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(node, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGraphicalObjectFrame parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGraphicalObjectFrame.type, null);
        }
        
        @Deprecated
        public static CTGraphicalObjectFrame parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGraphicalObjectFrame)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGraphicalObjectFrame.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGraphicalObjectFrame.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
