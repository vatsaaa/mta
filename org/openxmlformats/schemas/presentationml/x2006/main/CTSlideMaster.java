// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorMapping;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSlideMaster extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSlideMaster.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctslidemasterd8fctype");
    
    CTCommonSlideData getCSld();
    
    void setCSld(final CTCommonSlideData p0);
    
    CTCommonSlideData addNewCSld();
    
    CTColorMapping getClrMap();
    
    void setClrMap(final CTColorMapping p0);
    
    CTColorMapping addNewClrMap();
    
    CTSlideLayoutIdList getSldLayoutIdLst();
    
    boolean isSetSldLayoutIdLst();
    
    void setSldLayoutIdLst(final CTSlideLayoutIdList p0);
    
    CTSlideLayoutIdList addNewSldLayoutIdLst();
    
    void unsetSldLayoutIdLst();
    
    CTSlideTransition getTransition();
    
    boolean isSetTransition();
    
    void setTransition(final CTSlideTransition p0);
    
    CTSlideTransition addNewTransition();
    
    void unsetTransition();
    
    CTSlideTiming getTiming();
    
    boolean isSetTiming();
    
    void setTiming(final CTSlideTiming p0);
    
    CTSlideTiming addNewTiming();
    
    void unsetTiming();
    
    CTHeaderFooter getHf();
    
    boolean isSetHf();
    
    void setHf(final CTHeaderFooter p0);
    
    CTHeaderFooter addNewHf();
    
    void unsetHf();
    
    CTSlideMasterTextStyles getTxStyles();
    
    boolean isSetTxStyles();
    
    void setTxStyles(final CTSlideMasterTextStyles p0);
    
    CTSlideMasterTextStyles addNewTxStyles();
    
    void unsetTxStyles();
    
    CTExtensionListModify getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionListModify p0);
    
    CTExtensionListModify addNewExtLst();
    
    void unsetExtLst();
    
    boolean getPreserve();
    
    XmlBoolean xgetPreserve();
    
    boolean isSetPreserve();
    
    void setPreserve(final boolean p0);
    
    void xsetPreserve(final XmlBoolean p0);
    
    void unsetPreserve();
    
    public static final class Factory
    {
        public static CTSlideMaster newInstance() {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().newInstance(CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster newInstance(final XmlOptions xmlOptions) {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().newInstance(CTSlideMaster.type, xmlOptions);
        }
        
        public static CTSlideMaster parse(final String s) throws XmlException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(s, CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(s, CTSlideMaster.type, xmlOptions);
        }
        
        public static CTSlideMaster parse(final File file) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(file, CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(file, CTSlideMaster.type, xmlOptions);
        }
        
        public static CTSlideMaster parse(final URL url) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(url, CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(url, CTSlideMaster.type, xmlOptions);
        }
        
        public static CTSlideMaster parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(inputStream, CTSlideMaster.type, xmlOptions);
        }
        
        public static CTSlideMaster parse(final Reader reader) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(reader, CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(reader, CTSlideMaster.type, xmlOptions);
        }
        
        public static CTSlideMaster parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSlideMaster.type, xmlOptions);
        }
        
        public static CTSlideMaster parse(final Node node) throws XmlException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(node, CTSlideMaster.type, null);
        }
        
        public static CTSlideMaster parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(node, CTSlideMaster.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSlideMaster parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideMaster.type, null);
        }
        
        @Deprecated
        public static CTSlideMaster parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSlideMaster)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSlideMaster.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideMaster.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSlideMaster.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
