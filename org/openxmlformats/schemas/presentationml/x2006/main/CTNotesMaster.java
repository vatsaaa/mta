// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextListStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorMapping;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNotesMaster extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNotesMaster.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnotesmaster69ectype");
    
    CTCommonSlideData getCSld();
    
    void setCSld(final CTCommonSlideData p0);
    
    CTCommonSlideData addNewCSld();
    
    CTColorMapping getClrMap();
    
    void setClrMap(final CTColorMapping p0);
    
    CTColorMapping addNewClrMap();
    
    CTHeaderFooter getHf();
    
    boolean isSetHf();
    
    void setHf(final CTHeaderFooter p0);
    
    CTHeaderFooter addNewHf();
    
    void unsetHf();
    
    CTTextListStyle getNotesStyle();
    
    boolean isSetNotesStyle();
    
    void setNotesStyle(final CTTextListStyle p0);
    
    CTTextListStyle addNewNotesStyle();
    
    void unsetNotesStyle();
    
    CTExtensionListModify getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionListModify p0);
    
    CTExtensionListModify addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTNotesMaster newInstance() {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().newInstance(CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster newInstance(final XmlOptions xmlOptions) {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().newInstance(CTNotesMaster.type, xmlOptions);
        }
        
        public static CTNotesMaster parse(final String s) throws XmlException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(s, CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(s, CTNotesMaster.type, xmlOptions);
        }
        
        public static CTNotesMaster parse(final File file) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(file, CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(file, CTNotesMaster.type, xmlOptions);
        }
        
        public static CTNotesMaster parse(final URL url) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(url, CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(url, CTNotesMaster.type, xmlOptions);
        }
        
        public static CTNotesMaster parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(inputStream, CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(inputStream, CTNotesMaster.type, xmlOptions);
        }
        
        public static CTNotesMaster parse(final Reader reader) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(reader, CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(reader, CTNotesMaster.type, xmlOptions);
        }
        
        public static CTNotesMaster parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNotesMaster.type, xmlOptions);
        }
        
        public static CTNotesMaster parse(final Node node) throws XmlException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(node, CTNotesMaster.type, null);
        }
        
        public static CTNotesMaster parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(node, CTNotesMaster.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNotesMaster parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNotesMaster.type, null);
        }
        
        @Deprecated
        public static CTNotesMaster parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNotesMaster)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNotesMaster.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNotesMaster.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNotesMaster.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
