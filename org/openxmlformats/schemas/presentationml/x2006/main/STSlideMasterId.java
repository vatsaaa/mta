// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STSlideMasterId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STSlideMasterId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stslidemasteridfe71type");
    
    public static final class Factory
    {
        public static STSlideMasterId newValue(final Object o) {
            return (STSlideMasterId)STSlideMasterId.type.newValue(o);
        }
        
        public static STSlideMasterId newInstance() {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().newInstance(STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId newInstance(final XmlOptions xmlOptions) {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().newInstance(STSlideMasterId.type, xmlOptions);
        }
        
        public static STSlideMasterId parse(final String s) throws XmlException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(s, STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(s, STSlideMasterId.type, xmlOptions);
        }
        
        public static STSlideMasterId parse(final File file) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(file, STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(file, STSlideMasterId.type, xmlOptions);
        }
        
        public static STSlideMasterId parse(final URL url) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(url, STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(url, STSlideMasterId.type, xmlOptions);
        }
        
        public static STSlideMasterId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(inputStream, STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(inputStream, STSlideMasterId.type, xmlOptions);
        }
        
        public static STSlideMasterId parse(final Reader reader) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(reader, STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(reader, STSlideMasterId.type, xmlOptions);
        }
        
        public static STSlideMasterId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSlideMasterId.type, xmlOptions);
        }
        
        public static STSlideMasterId parse(final Node node) throws XmlException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(node, STSlideMasterId.type, null);
        }
        
        public static STSlideMasterId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(node, STSlideMasterId.type, xmlOptions);
        }
        
        @Deprecated
        public static STSlideMasterId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSlideMasterId.type, null);
        }
        
        @Deprecated
        public static STSlideMasterId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STSlideMasterId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSlideMasterId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSlideMasterId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSlideMasterId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
