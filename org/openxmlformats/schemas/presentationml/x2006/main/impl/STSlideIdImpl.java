// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.presentationml.x2006.main.STSlideId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STSlideIdImpl extends JavaLongHolderEx implements STSlideId
{
    public STSlideIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSlideIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
