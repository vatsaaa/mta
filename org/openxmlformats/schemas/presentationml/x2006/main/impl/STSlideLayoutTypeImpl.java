// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.presentationml.x2006.main.STSlideLayoutType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STSlideLayoutTypeImpl extends JavaStringEnumerationHolderEx implements STSlideLayoutType
{
    public STSlideLayoutTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSlideLayoutTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
