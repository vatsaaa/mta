// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommentAuthorList;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.presentationml.x2006.main.CmAuthorLstDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CmAuthorLstDocumentImpl extends XmlComplexContentImpl implements CmAuthorLstDocument
{
    private static final QName CMAUTHORLST$0;
    
    public CmAuthorLstDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTCommentAuthorList getCmAuthorLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCommentAuthorList list = (CTCommentAuthorList)this.get_store().find_element_user(CmAuthorLstDocumentImpl.CMAUTHORLST$0, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public void setCmAuthorLst(final CTCommentAuthorList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCommentAuthorList list2 = (CTCommentAuthorList)this.get_store().find_element_user(CmAuthorLstDocumentImpl.CMAUTHORLST$0, 0);
            if (list2 == null) {
                list2 = (CTCommentAuthorList)this.get_store().add_element_user(CmAuthorLstDocumentImpl.CMAUTHORLST$0);
            }
            list2.set(list);
        }
    }
    
    public CTCommentAuthorList addNewCmAuthorLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCommentAuthorList)this.get_store().add_element_user(CmAuthorLstDocumentImpl.CMAUTHORLST$0);
        }
    }
    
    static {
        CMAUTHORLST$0 = new QName("http://schemas.openxmlformats.org/presentationml/2006/main", "cmAuthorLst");
    }
}
