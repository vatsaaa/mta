// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommentList;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.presentationml.x2006.main.CmLstDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CmLstDocumentImpl extends XmlComplexContentImpl implements CmLstDocument
{
    private static final QName CMLST$0;
    
    public CmLstDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTCommentList getCmLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCommentList list = (CTCommentList)this.get_store().find_element_user(CmLstDocumentImpl.CMLST$0, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public void setCmLst(final CTCommentList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCommentList list2 = (CTCommentList)this.get_store().find_element_user(CmLstDocumentImpl.CMLST$0, 0);
            if (list2 == null) {
                list2 = (CTCommentList)this.get_store().add_element_user(CmLstDocumentImpl.CMLST$0);
            }
            list2.set(list);
        }
    }
    
    public CTCommentList addNewCmLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCommentList)this.get_store().add_element_user(CmLstDocumentImpl.CMLST$0);
        }
    }
    
    static {
        CMLST$0 = new QName("http://schemas.openxmlformats.org/presentationml/2006/main", "cmLst");
    }
}
