// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlide;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.presentationml.x2006.main.SldDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class SldDocumentImpl extends XmlComplexContentImpl implements SldDocument
{
    private static final QName SLD$0;
    
    public SldDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTSlide getSld() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSlide ctSlide = (CTSlide)this.get_store().find_element_user(SldDocumentImpl.SLD$0, 0);
            if (ctSlide == null) {
                return null;
            }
            return ctSlide;
        }
    }
    
    public void setSld(final CTSlide ctSlide) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSlide ctSlide2 = (CTSlide)this.get_store().find_element_user(SldDocumentImpl.SLD$0, 0);
            if (ctSlide2 == null) {
                ctSlide2 = (CTSlide)this.get_store().add_element_user(SldDocumentImpl.SLD$0);
            }
            ctSlide2.set(ctSlide);
        }
    }
    
    public CTSlide addNewSld() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSlide)this.get_store().add_element_user(SldDocumentImpl.SLD$0);
        }
    }
    
    static {
        SLD$0 = new QName("http://schemas.openxmlformats.org/presentationml/2006/main", "sld");
    }
}
