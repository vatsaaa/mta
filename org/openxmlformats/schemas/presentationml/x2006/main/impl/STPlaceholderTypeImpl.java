// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.presentationml.x2006.main.STPlaceholderType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STPlaceholderTypeImpl extends JavaStringEnumerationHolderEx implements STPlaceholderType
{
    public STPlaceholderTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPlaceholderTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
