// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.presentationml.x2006.main.STName;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STNameImpl extends JavaStringHolderEx implements STName
{
    public STNameImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STNameImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
