// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.presentationml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.presentationml.x2006.main.STSlideMasterId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STSlideMasterIdImpl extends JavaLongHolderEx implements STSlideMasterId
{
    public STSlideMasterIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSlideMasterIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
