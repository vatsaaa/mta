// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.picture.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.picture.CTPicture;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.picture.PicDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class PicDocumentImpl extends XmlComplexContentImpl implements PicDocument
{
    private static final QName PIC$0;
    
    public PicDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTPicture getPic() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPicture ctPicture = (CTPicture)this.get_store().find_element_user(PicDocumentImpl.PIC$0, 0);
            if (ctPicture == null) {
                return null;
            }
            return ctPicture;
        }
    }
    
    public void setPic(final CTPicture ctPicture) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPicture ctPicture2 = (CTPicture)this.get_store().find_element_user(PicDocumentImpl.PIC$0, 0);
            if (ctPicture2 == null) {
                ctPicture2 = (CTPicture)this.get_store().add_element_user(PicDocumentImpl.PIC$0);
            }
            ctPicture2.set(ctPicture);
        }
    }
    
    public CTPicture addNewPic() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPicture)this.get_store().add_element_user(PicDocumentImpl.PIC$0);
        }
    }
    
    static {
        PIC$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/picture", "pic");
    }
}
