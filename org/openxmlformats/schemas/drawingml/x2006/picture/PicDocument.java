// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.picture;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface PicDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(PicDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("pic8010doctype");
    
    CTPicture getPic();
    
    void setPic(final CTPicture p0);
    
    CTPicture addNewPic();
    
    public static final class Factory
    {
        public static PicDocument newInstance() {
            return (PicDocument)XmlBeans.getContextTypeLoader().newInstance(PicDocument.type, null);
        }
        
        public static PicDocument newInstance(final XmlOptions xmlOptions) {
            return (PicDocument)XmlBeans.getContextTypeLoader().newInstance(PicDocument.type, xmlOptions);
        }
        
        public static PicDocument parse(final String s) throws XmlException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(s, PicDocument.type, null);
        }
        
        public static PicDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(s, PicDocument.type, xmlOptions);
        }
        
        public static PicDocument parse(final File file) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(file, PicDocument.type, null);
        }
        
        public static PicDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(file, PicDocument.type, xmlOptions);
        }
        
        public static PicDocument parse(final URL url) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(url, PicDocument.type, null);
        }
        
        public static PicDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(url, PicDocument.type, xmlOptions);
        }
        
        public static PicDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(inputStream, PicDocument.type, null);
        }
        
        public static PicDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(inputStream, PicDocument.type, xmlOptions);
        }
        
        public static PicDocument parse(final Reader reader) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(reader, PicDocument.type, null);
        }
        
        public static PicDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(reader, PicDocument.type, xmlOptions);
        }
        
        public static PicDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, PicDocument.type, null);
        }
        
        public static PicDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, PicDocument.type, xmlOptions);
        }
        
        public static PicDocument parse(final Node node) throws XmlException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(node, PicDocument.type, null);
        }
        
        public static PicDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(node, PicDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static PicDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, PicDocument.type, null);
        }
        
        @Deprecated
        public static PicDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (PicDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, PicDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, PicDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, PicDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
