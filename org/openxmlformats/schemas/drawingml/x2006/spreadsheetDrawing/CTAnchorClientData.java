// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTAnchorClientData extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTAnchorClientData.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctanchorclientdata02betype");
    
    boolean getFLocksWithSheet();
    
    XmlBoolean xgetFLocksWithSheet();
    
    boolean isSetFLocksWithSheet();
    
    void setFLocksWithSheet(final boolean p0);
    
    void xsetFLocksWithSheet(final XmlBoolean p0);
    
    void unsetFLocksWithSheet();
    
    boolean getFPrintsWithSheet();
    
    XmlBoolean xgetFPrintsWithSheet();
    
    boolean isSetFPrintsWithSheet();
    
    void setFPrintsWithSheet(final boolean p0);
    
    void xsetFPrintsWithSheet(final XmlBoolean p0);
    
    void unsetFPrintsWithSheet();
    
    public static final class Factory
    {
        public static CTAnchorClientData newInstance() {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().newInstance(CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData newInstance(final XmlOptions xmlOptions) {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().newInstance(CTAnchorClientData.type, xmlOptions);
        }
        
        public static CTAnchorClientData parse(final String s) throws XmlException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(s, CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(s, CTAnchorClientData.type, xmlOptions);
        }
        
        public static CTAnchorClientData parse(final File file) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(file, CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(file, CTAnchorClientData.type, xmlOptions);
        }
        
        public static CTAnchorClientData parse(final URL url) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(url, CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(url, CTAnchorClientData.type, xmlOptions);
        }
        
        public static CTAnchorClientData parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(inputStream, CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(inputStream, CTAnchorClientData.type, xmlOptions);
        }
        
        public static CTAnchorClientData parse(final Reader reader) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(reader, CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(reader, CTAnchorClientData.type, xmlOptions);
        }
        
        public static CTAnchorClientData parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAnchorClientData.type, xmlOptions);
        }
        
        public static CTAnchorClientData parse(final Node node) throws XmlException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(node, CTAnchorClientData.type, null);
        }
        
        public static CTAnchorClientData parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(node, CTAnchorClientData.type, xmlOptions);
        }
        
        @Deprecated
        public static CTAnchorClientData parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAnchorClientData.type, null);
        }
        
        @Deprecated
        public static CTAnchorClientData parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTAnchorClientData)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAnchorClientData.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAnchorClientData.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAnchorClientData.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
