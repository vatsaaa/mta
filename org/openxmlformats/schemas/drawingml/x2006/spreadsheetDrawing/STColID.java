// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STColID extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STColID.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stcolidb7f5type");
    
    public static final class Factory
    {
        public static STColID newValue(final Object o) {
            return (STColID)STColID.type.newValue(o);
        }
        
        public static STColID newInstance() {
            return (STColID)XmlBeans.getContextTypeLoader().newInstance(STColID.type, null);
        }
        
        public static STColID newInstance(final XmlOptions xmlOptions) {
            return (STColID)XmlBeans.getContextTypeLoader().newInstance(STColID.type, xmlOptions);
        }
        
        public static STColID parse(final String s) throws XmlException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(s, STColID.type, null);
        }
        
        public static STColID parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(s, STColID.type, xmlOptions);
        }
        
        public static STColID parse(final File file) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(file, STColID.type, null);
        }
        
        public static STColID parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(file, STColID.type, xmlOptions);
        }
        
        public static STColID parse(final URL url) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(url, STColID.type, null);
        }
        
        public static STColID parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(url, STColID.type, xmlOptions);
        }
        
        public static STColID parse(final InputStream inputStream) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(inputStream, STColID.type, null);
        }
        
        public static STColID parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(inputStream, STColID.type, xmlOptions);
        }
        
        public static STColID parse(final Reader reader) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(reader, STColID.type, null);
        }
        
        public static STColID parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(reader, STColID.type, xmlOptions);
        }
        
        public static STColID parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STColID.type, null);
        }
        
        public static STColID parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STColID.type, xmlOptions);
        }
        
        public static STColID parse(final Node node) throws XmlException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(node, STColID.type, null);
        }
        
        public static STColID parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(node, STColID.type, xmlOptions);
        }
        
        @Deprecated
        public static STColID parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STColID.type, null);
        }
        
        @Deprecated
        public static STColID parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STColID)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STColID.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STColID.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STColID.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
