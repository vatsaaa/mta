// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.XmlString;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTShape extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTShape.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctshapee40btype");
    
    CTShapeNonVisual getNvSpPr();
    
    void setNvSpPr(final CTShapeNonVisual p0);
    
    CTShapeNonVisual addNewNvSpPr();
    
    CTShapeProperties getSpPr();
    
    void setSpPr(final CTShapeProperties p0);
    
    CTShapeProperties addNewSpPr();
    
    CTShapeStyle getStyle();
    
    boolean isSetStyle();
    
    void setStyle(final CTShapeStyle p0);
    
    CTShapeStyle addNewStyle();
    
    void unsetStyle();
    
    CTTextBody getTxBody();
    
    boolean isSetTxBody();
    
    void setTxBody(final CTTextBody p0);
    
    CTTextBody addNewTxBody();
    
    void unsetTxBody();
    
    String getMacro();
    
    XmlString xgetMacro();
    
    boolean isSetMacro();
    
    void setMacro(final String p0);
    
    void xsetMacro(final XmlString p0);
    
    void unsetMacro();
    
    String getTextlink();
    
    XmlString xgetTextlink();
    
    boolean isSetTextlink();
    
    void setTextlink(final String p0);
    
    void xsetTextlink(final XmlString p0);
    
    void unsetTextlink();
    
    boolean getFLocksText();
    
    XmlBoolean xgetFLocksText();
    
    boolean isSetFLocksText();
    
    void setFLocksText(final boolean p0);
    
    void xsetFLocksText(final XmlBoolean p0);
    
    void unsetFLocksText();
    
    boolean getFPublished();
    
    XmlBoolean xgetFPublished();
    
    boolean isSetFPublished();
    
    void setFPublished(final boolean p0);
    
    void xsetFPublished(final XmlBoolean p0);
    
    void unsetFPublished();
    
    public static final class Factory
    {
        public static CTShape newInstance() {
            return (CTShape)XmlBeans.getContextTypeLoader().newInstance(CTShape.type, null);
        }
        
        public static CTShape newInstance(final XmlOptions xmlOptions) {
            return (CTShape)XmlBeans.getContextTypeLoader().newInstance(CTShape.type, xmlOptions);
        }
        
        public static CTShape parse(final String s) throws XmlException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(s, CTShape.type, null);
        }
        
        public static CTShape parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(s, CTShape.type, xmlOptions);
        }
        
        public static CTShape parse(final File file) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(file, CTShape.type, null);
        }
        
        public static CTShape parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(file, CTShape.type, xmlOptions);
        }
        
        public static CTShape parse(final URL url) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(url, CTShape.type, null);
        }
        
        public static CTShape parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(url, CTShape.type, xmlOptions);
        }
        
        public static CTShape parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(inputStream, CTShape.type, null);
        }
        
        public static CTShape parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(inputStream, CTShape.type, xmlOptions);
        }
        
        public static CTShape parse(final Reader reader) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(reader, CTShape.type, null);
        }
        
        public static CTShape parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(reader, CTShape.type, xmlOptions);
        }
        
        public static CTShape parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShape.type, null);
        }
        
        public static CTShape parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShape.type, xmlOptions);
        }
        
        public static CTShape parse(final Node node) throws XmlException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(node, CTShape.type, null);
        }
        
        public static CTShape parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(node, CTShape.type, xmlOptions);
        }
        
        @Deprecated
        public static CTShape parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShape.type, null);
        }
        
        @Deprecated
        public static CTShape parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTShape)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShape.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShape.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShape.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
