// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGroupShapeProperties;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGroupShape extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGroupShape.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgroupshape6c36type");
    
    CTGroupShapeNonVisual getNvGrpSpPr();
    
    void setNvGrpSpPr(final CTGroupShapeNonVisual p0);
    
    CTGroupShapeNonVisual addNewNvGrpSpPr();
    
    CTGroupShapeProperties getGrpSpPr();
    
    void setGrpSpPr(final CTGroupShapeProperties p0);
    
    CTGroupShapeProperties addNewGrpSpPr();
    
    List<CTShape> getSpList();
    
    @Deprecated
    CTShape[] getSpArray();
    
    CTShape getSpArray(final int p0);
    
    int sizeOfSpArray();
    
    void setSpArray(final CTShape[] p0);
    
    void setSpArray(final int p0, final CTShape p1);
    
    CTShape insertNewSp(final int p0);
    
    CTShape addNewSp();
    
    void removeSp(final int p0);
    
    List<CTGroupShape> getGrpSpList();
    
    @Deprecated
    CTGroupShape[] getGrpSpArray();
    
    CTGroupShape getGrpSpArray(final int p0);
    
    int sizeOfGrpSpArray();
    
    void setGrpSpArray(final CTGroupShape[] p0);
    
    void setGrpSpArray(final int p0, final CTGroupShape p1);
    
    CTGroupShape insertNewGrpSp(final int p0);
    
    CTGroupShape addNewGrpSp();
    
    void removeGrpSp(final int p0);
    
    List<CTGraphicalObjectFrame> getGraphicFrameList();
    
    @Deprecated
    CTGraphicalObjectFrame[] getGraphicFrameArray();
    
    CTGraphicalObjectFrame getGraphicFrameArray(final int p0);
    
    int sizeOfGraphicFrameArray();
    
    void setGraphicFrameArray(final CTGraphicalObjectFrame[] p0);
    
    void setGraphicFrameArray(final int p0, final CTGraphicalObjectFrame p1);
    
    CTGraphicalObjectFrame insertNewGraphicFrame(final int p0);
    
    CTGraphicalObjectFrame addNewGraphicFrame();
    
    void removeGraphicFrame(final int p0);
    
    List<CTConnector> getCxnSpList();
    
    @Deprecated
    CTConnector[] getCxnSpArray();
    
    CTConnector getCxnSpArray(final int p0);
    
    int sizeOfCxnSpArray();
    
    void setCxnSpArray(final CTConnector[] p0);
    
    void setCxnSpArray(final int p0, final CTConnector p1);
    
    CTConnector insertNewCxnSp(final int p0);
    
    CTConnector addNewCxnSp();
    
    void removeCxnSp(final int p0);
    
    List<CTPicture> getPicList();
    
    @Deprecated
    CTPicture[] getPicArray();
    
    CTPicture getPicArray(final int p0);
    
    int sizeOfPicArray();
    
    void setPicArray(final CTPicture[] p0);
    
    void setPicArray(final int p0, final CTPicture p1);
    
    CTPicture insertNewPic(final int p0);
    
    CTPicture addNewPic();
    
    void removePic(final int p0);
    
    public static final class Factory
    {
        public static CTGroupShape newInstance() {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().newInstance(CTGroupShape.type, null);
        }
        
        public static CTGroupShape newInstance(final XmlOptions xmlOptions) {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().newInstance(CTGroupShape.type, xmlOptions);
        }
        
        public static CTGroupShape parse(final String s) throws XmlException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(s, CTGroupShape.type, null);
        }
        
        public static CTGroupShape parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(s, CTGroupShape.type, xmlOptions);
        }
        
        public static CTGroupShape parse(final File file) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(file, CTGroupShape.type, null);
        }
        
        public static CTGroupShape parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(file, CTGroupShape.type, xmlOptions);
        }
        
        public static CTGroupShape parse(final URL url) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(url, CTGroupShape.type, null);
        }
        
        public static CTGroupShape parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(url, CTGroupShape.type, xmlOptions);
        }
        
        public static CTGroupShape parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(inputStream, CTGroupShape.type, null);
        }
        
        public static CTGroupShape parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(inputStream, CTGroupShape.type, xmlOptions);
        }
        
        public static CTGroupShape parse(final Reader reader) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(reader, CTGroupShape.type, null);
        }
        
        public static CTGroupShape parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(reader, CTGroupShape.type, xmlOptions);
        }
        
        public static CTGroupShape parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGroupShape.type, null);
        }
        
        public static CTGroupShape parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGroupShape.type, xmlOptions);
        }
        
        public static CTGroupShape parse(final Node node) throws XmlException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(node, CTGroupShape.type, null);
        }
        
        public static CTGroupShape parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(node, CTGroupShape.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGroupShape parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGroupShape.type, null);
        }
        
        @Deprecated
        public static CTGroupShape parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGroupShape)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGroupShape.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGroupShape.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGroupShape.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
