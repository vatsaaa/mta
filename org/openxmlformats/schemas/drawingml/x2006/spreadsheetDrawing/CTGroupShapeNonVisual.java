// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualGroupDrawingShapeProps;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGroupShapeNonVisual extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGroupShapeNonVisual.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgroupshapenonvisual5a55type");
    
    CTNonVisualDrawingProps getCNvPr();
    
    void setCNvPr(final CTNonVisualDrawingProps p0);
    
    CTNonVisualDrawingProps addNewCNvPr();
    
    CTNonVisualGroupDrawingShapeProps getCNvGrpSpPr();
    
    void setCNvGrpSpPr(final CTNonVisualGroupDrawingShapeProps p0);
    
    CTNonVisualGroupDrawingShapeProps addNewCNvGrpSpPr();
    
    public static final class Factory
    {
        public static CTGroupShapeNonVisual newInstance() {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().newInstance(CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual newInstance(final XmlOptions xmlOptions) {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().newInstance(CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        public static CTGroupShapeNonVisual parse(final String s) throws XmlException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(s, CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(s, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        public static CTGroupShapeNonVisual parse(final File file) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(file, CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(file, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        public static CTGroupShapeNonVisual parse(final URL url) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(url, CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(url, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        public static CTGroupShapeNonVisual parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(inputStream, CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(inputStream, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        public static CTGroupShapeNonVisual parse(final Reader reader) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(reader, CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(reader, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        public static CTGroupShapeNonVisual parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        public static CTGroupShapeNonVisual parse(final Node node) throws XmlException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(node, CTGroupShapeNonVisual.type, null);
        }
        
        public static CTGroupShapeNonVisual parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(node, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGroupShapeNonVisual parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGroupShapeNonVisual.type, null);
        }
        
        @Deprecated
        public static CTGroupShapeNonVisual parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGroupShapeNonVisual)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGroupShapeNonVisual.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGroupShapeNonVisual.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
