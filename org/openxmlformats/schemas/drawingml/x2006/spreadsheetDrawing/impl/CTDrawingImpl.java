// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.impl;

import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTAbsoluteAnchor;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTOneCellAnchor;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTTwoCellAnchor;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTDrawing;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTDrawingImpl extends XmlComplexContentImpl implements CTDrawing
{
    private static final QName TWOCELLANCHOR$0;
    private static final QName ONECELLANCHOR$2;
    private static final QName ABSOLUTEANCHOR$4;
    
    public CTDrawingImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTTwoCellAnchor> getTwoCellAnchorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTwoCellAnchor>)new CTDrawingImpl.TwoCellAnchorList(this);
        }
    }
    
    public CTTwoCellAnchor[] getTwoCellAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTDrawingImpl.TWOCELLANCHOR$0, list);
            final CTTwoCellAnchor[] array = new CTTwoCellAnchor[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTwoCellAnchor getTwoCellAnchorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTwoCellAnchor ctTwoCellAnchor = (CTTwoCellAnchor)this.get_store().find_element_user(CTDrawingImpl.TWOCELLANCHOR$0, n);
            if (ctTwoCellAnchor == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTwoCellAnchor;
        }
    }
    
    public int sizeOfTwoCellAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTDrawingImpl.TWOCELLANCHOR$0);
        }
    }
    
    public void setTwoCellAnchorArray(final CTTwoCellAnchor[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTDrawingImpl.TWOCELLANCHOR$0);
        }
    }
    
    public void setTwoCellAnchorArray(final int n, final CTTwoCellAnchor ctTwoCellAnchor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTwoCellAnchor ctTwoCellAnchor2 = (CTTwoCellAnchor)this.get_store().find_element_user(CTDrawingImpl.TWOCELLANCHOR$0, n);
            if (ctTwoCellAnchor2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTwoCellAnchor2.set(ctTwoCellAnchor);
        }
    }
    
    public CTTwoCellAnchor insertNewTwoCellAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTwoCellAnchor)this.get_store().insert_element_user(CTDrawingImpl.TWOCELLANCHOR$0, n);
        }
    }
    
    public CTTwoCellAnchor addNewTwoCellAnchor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTwoCellAnchor)this.get_store().add_element_user(CTDrawingImpl.TWOCELLANCHOR$0);
        }
    }
    
    public void removeTwoCellAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTDrawingImpl.TWOCELLANCHOR$0, n);
        }
    }
    
    public List<CTOneCellAnchor> getOneCellAnchorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOneCellAnchor>)new CTDrawingImpl.OneCellAnchorList(this);
        }
    }
    
    public CTOneCellAnchor[] getOneCellAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTDrawingImpl.ONECELLANCHOR$2, list);
            final CTOneCellAnchor[] array = new CTOneCellAnchor[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOneCellAnchor getOneCellAnchorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOneCellAnchor ctOneCellAnchor = (CTOneCellAnchor)this.get_store().find_element_user(CTDrawingImpl.ONECELLANCHOR$2, n);
            if (ctOneCellAnchor == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOneCellAnchor;
        }
    }
    
    public int sizeOfOneCellAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTDrawingImpl.ONECELLANCHOR$2);
        }
    }
    
    public void setOneCellAnchorArray(final CTOneCellAnchor[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTDrawingImpl.ONECELLANCHOR$2);
        }
    }
    
    public void setOneCellAnchorArray(final int n, final CTOneCellAnchor ctOneCellAnchor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOneCellAnchor ctOneCellAnchor2 = (CTOneCellAnchor)this.get_store().find_element_user(CTDrawingImpl.ONECELLANCHOR$2, n);
            if (ctOneCellAnchor2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOneCellAnchor2.set(ctOneCellAnchor);
        }
    }
    
    public CTOneCellAnchor insertNewOneCellAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOneCellAnchor)this.get_store().insert_element_user(CTDrawingImpl.ONECELLANCHOR$2, n);
        }
    }
    
    public CTOneCellAnchor addNewOneCellAnchor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOneCellAnchor)this.get_store().add_element_user(CTDrawingImpl.ONECELLANCHOR$2);
        }
    }
    
    public void removeOneCellAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTDrawingImpl.ONECELLANCHOR$2, n);
        }
    }
    
    public List<CTAbsoluteAnchor> getAbsoluteAnchorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAbsoluteAnchor>)new CTDrawingImpl.AbsoluteAnchorList(this);
        }
    }
    
    public CTAbsoluteAnchor[] getAbsoluteAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTDrawingImpl.ABSOLUTEANCHOR$4, list);
            final CTAbsoluteAnchor[] array = new CTAbsoluteAnchor[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAbsoluteAnchor getAbsoluteAnchorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAbsoluteAnchor ctAbsoluteAnchor = (CTAbsoluteAnchor)this.get_store().find_element_user(CTDrawingImpl.ABSOLUTEANCHOR$4, n);
            if (ctAbsoluteAnchor == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAbsoluteAnchor;
        }
    }
    
    public int sizeOfAbsoluteAnchorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTDrawingImpl.ABSOLUTEANCHOR$4);
        }
    }
    
    public void setAbsoluteAnchorArray(final CTAbsoluteAnchor[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTDrawingImpl.ABSOLUTEANCHOR$4);
        }
    }
    
    public void setAbsoluteAnchorArray(final int n, final CTAbsoluteAnchor ctAbsoluteAnchor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAbsoluteAnchor ctAbsoluteAnchor2 = (CTAbsoluteAnchor)this.get_store().find_element_user(CTDrawingImpl.ABSOLUTEANCHOR$4, n);
            if (ctAbsoluteAnchor2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAbsoluteAnchor2.set((XmlObject)ctAbsoluteAnchor);
        }
    }
    
    public CTAbsoluteAnchor insertNewAbsoluteAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAbsoluteAnchor)this.get_store().insert_element_user(CTDrawingImpl.ABSOLUTEANCHOR$4, n);
        }
    }
    
    public CTAbsoluteAnchor addNewAbsoluteAnchor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAbsoluteAnchor)this.get_store().add_element_user(CTDrawingImpl.ABSOLUTEANCHOR$4);
        }
    }
    
    public void removeAbsoluteAnchor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTDrawingImpl.ABSOLUTEANCHOR$4, n);
        }
    }
    
    static {
        TWOCELLANCHOR$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "twoCellAnchor");
        ONECELLANCHOR$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "oneCellAnchor");
        ABSOLUTEANCHOR$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "absoluteAnchor");
    }
}
