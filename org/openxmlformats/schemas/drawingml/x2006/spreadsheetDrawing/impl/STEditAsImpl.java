// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.STEditAs;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STEditAsImpl extends JavaStringEnumerationHolderEx implements STEditAs
{
    public STEditAsImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STEditAsImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
