// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.STColID;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STColIDImpl extends JavaIntHolderEx implements STColID
{
    public STColIDImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STColIDImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
