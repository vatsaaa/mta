// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.STRowID;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STRowIDImpl extends JavaIntHolderEx implements STRowID
{
    public STRowIDImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STRowIDImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
