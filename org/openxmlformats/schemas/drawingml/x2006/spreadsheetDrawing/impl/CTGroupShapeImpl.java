// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.impl;

import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTPicture;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTConnector;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTGraphicalObjectFrame;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTShape;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGroupShapeProperties;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTGroupShapeNonVisual;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTGroupShape;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTGroupShapeImpl extends XmlComplexContentImpl implements CTGroupShape
{
    private static final QName NVGRPSPPR$0;
    private static final QName GRPSPPR$2;
    private static final QName SP$4;
    private static final QName GRPSP$6;
    private static final QName GRAPHICFRAME$8;
    private static final QName CXNSP$10;
    private static final QName PIC$12;
    
    public CTGroupShapeImpl(final SchemaType type) {
        super(type);
    }
    
    public CTGroupShapeNonVisual getNvGrpSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupShapeNonVisual ctGroupShapeNonVisual = (CTGroupShapeNonVisual)this.get_store().find_element_user(CTGroupShapeImpl.NVGRPSPPR$0, 0);
            if (ctGroupShapeNonVisual == null) {
                return null;
            }
            return ctGroupShapeNonVisual;
        }
    }
    
    public void setNvGrpSpPr(final CTGroupShapeNonVisual ctGroupShapeNonVisual) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTGroupShapeNonVisual ctGroupShapeNonVisual2 = (CTGroupShapeNonVisual)this.get_store().find_element_user(CTGroupShapeImpl.NVGRPSPPR$0, 0);
            if (ctGroupShapeNonVisual2 == null) {
                ctGroupShapeNonVisual2 = (CTGroupShapeNonVisual)this.get_store().add_element_user(CTGroupShapeImpl.NVGRPSPPR$0);
            }
            ctGroupShapeNonVisual2.set(ctGroupShapeNonVisual);
        }
    }
    
    public CTGroupShapeNonVisual addNewNvGrpSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupShapeNonVisual)this.get_store().add_element_user(CTGroupShapeImpl.NVGRPSPPR$0);
        }
    }
    
    public CTGroupShapeProperties getGrpSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupShapeProperties ctGroupShapeProperties = (CTGroupShapeProperties)this.get_store().find_element_user(CTGroupShapeImpl.GRPSPPR$2, 0);
            if (ctGroupShapeProperties == null) {
                return null;
            }
            return ctGroupShapeProperties;
        }
    }
    
    public void setGrpSpPr(final CTGroupShapeProperties ctGroupShapeProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTGroupShapeProperties ctGroupShapeProperties2 = (CTGroupShapeProperties)this.get_store().find_element_user(CTGroupShapeImpl.GRPSPPR$2, 0);
            if (ctGroupShapeProperties2 == null) {
                ctGroupShapeProperties2 = (CTGroupShapeProperties)this.get_store().add_element_user(CTGroupShapeImpl.GRPSPPR$2);
            }
            ctGroupShapeProperties2.set(ctGroupShapeProperties);
        }
    }
    
    public CTGroupShapeProperties addNewGrpSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupShapeProperties)this.get_store().add_element_user(CTGroupShapeImpl.GRPSPPR$2);
        }
    }
    
    public List<CTShape> getSpList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTShape>)new CTGroupShapeImpl.SpList(this);
        }
    }
    
    public CTShape[] getSpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupShapeImpl.SP$4, list);
            final CTShape[] array = new CTShape[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTShape getSpArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShape ctShape = (CTShape)this.get_store().find_element_user(CTGroupShapeImpl.SP$4, n);
            if (ctShape == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctShape;
        }
    }
    
    public int sizeOfSpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupShapeImpl.SP$4);
        }
    }
    
    public void setSpArray(final CTShape[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupShapeImpl.SP$4);
        }
    }
    
    public void setSpArray(final int n, final CTShape ctShape) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShape ctShape2 = (CTShape)this.get_store().find_element_user(CTGroupShapeImpl.SP$4, n);
            if (ctShape2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctShape2.set(ctShape);
        }
    }
    
    public CTShape insertNewSp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShape)this.get_store().insert_element_user(CTGroupShapeImpl.SP$4, n);
        }
    }
    
    public CTShape addNewSp() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShape)this.get_store().add_element_user(CTGroupShapeImpl.SP$4);
        }
    }
    
    public void removeSp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupShapeImpl.SP$4, n);
        }
    }
    
    public List<CTGroupShape> getGrpSpList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGroupShape>)new CTGroupShapeImpl.GrpSpList(this);
        }
    }
    
    public CTGroupShape[] getGrpSpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupShapeImpl.GRPSP$6, list);
            final CTGroupShape[] array = new CTGroupShape[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGroupShape getGrpSpArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupShape ctGroupShape = (CTGroupShape)this.get_store().find_element_user(CTGroupShapeImpl.GRPSP$6, n);
            if (ctGroupShape == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGroupShape;
        }
    }
    
    public int sizeOfGrpSpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupShapeImpl.GRPSP$6);
        }
    }
    
    public void setGrpSpArray(final CTGroupShape[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupShapeImpl.GRPSP$6);
        }
    }
    
    public void setGrpSpArray(final int n, final CTGroupShape ctGroupShape) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupShape ctGroupShape2 = (CTGroupShape)this.get_store().find_element_user(CTGroupShapeImpl.GRPSP$6, n);
            if (ctGroupShape2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGroupShape2.set(ctGroupShape);
        }
    }
    
    public CTGroupShape insertNewGrpSp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupShape)this.get_store().insert_element_user(CTGroupShapeImpl.GRPSP$6, n);
        }
    }
    
    public CTGroupShape addNewGrpSp() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupShape)this.get_store().add_element_user(CTGroupShapeImpl.GRPSP$6);
        }
    }
    
    public void removeGrpSp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupShapeImpl.GRPSP$6, n);
        }
    }
    
    public List<CTGraphicalObjectFrame> getGraphicFrameList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGraphicalObjectFrame>)new CTGroupShapeImpl.GraphicFrameList(this);
        }
    }
    
    public CTGraphicalObjectFrame[] getGraphicFrameArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupShapeImpl.GRAPHICFRAME$8, list);
            final CTGraphicalObjectFrame[] array = new CTGraphicalObjectFrame[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGraphicalObjectFrame getGraphicFrameArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGraphicalObjectFrame ctGraphicalObjectFrame = (CTGraphicalObjectFrame)this.get_store().find_element_user(CTGroupShapeImpl.GRAPHICFRAME$8, n);
            if (ctGraphicalObjectFrame == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGraphicalObjectFrame;
        }
    }
    
    public int sizeOfGraphicFrameArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupShapeImpl.GRAPHICFRAME$8);
        }
    }
    
    public void setGraphicFrameArray(final CTGraphicalObjectFrame[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupShapeImpl.GRAPHICFRAME$8);
        }
    }
    
    public void setGraphicFrameArray(final int n, final CTGraphicalObjectFrame ctGraphicalObjectFrame) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGraphicalObjectFrame ctGraphicalObjectFrame2 = (CTGraphicalObjectFrame)this.get_store().find_element_user(CTGroupShapeImpl.GRAPHICFRAME$8, n);
            if (ctGraphicalObjectFrame2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGraphicalObjectFrame2.set(ctGraphicalObjectFrame);
        }
    }
    
    public CTGraphicalObjectFrame insertNewGraphicFrame(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGraphicalObjectFrame)this.get_store().insert_element_user(CTGroupShapeImpl.GRAPHICFRAME$8, n);
        }
    }
    
    public CTGraphicalObjectFrame addNewGraphicFrame() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGraphicalObjectFrame)this.get_store().add_element_user(CTGroupShapeImpl.GRAPHICFRAME$8);
        }
    }
    
    public void removeGraphicFrame(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupShapeImpl.GRAPHICFRAME$8, n);
        }
    }
    
    public List<CTConnector> getCxnSpList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTConnector>)new CTGroupShapeImpl.CxnSpList(this);
        }
    }
    
    public CTConnector[] getCxnSpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupShapeImpl.CXNSP$10, list);
            final CTConnector[] array = new CTConnector[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTConnector getCxnSpArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTConnector ctConnector = (CTConnector)this.get_store().find_element_user(CTGroupShapeImpl.CXNSP$10, n);
            if (ctConnector == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctConnector;
        }
    }
    
    public int sizeOfCxnSpArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupShapeImpl.CXNSP$10);
        }
    }
    
    public void setCxnSpArray(final CTConnector[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupShapeImpl.CXNSP$10);
        }
    }
    
    public void setCxnSpArray(final int n, final CTConnector ctConnector) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTConnector ctConnector2 = (CTConnector)this.get_store().find_element_user(CTGroupShapeImpl.CXNSP$10, n);
            if (ctConnector2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctConnector2.set(ctConnector);
        }
    }
    
    public CTConnector insertNewCxnSp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTConnector)this.get_store().insert_element_user(CTGroupShapeImpl.CXNSP$10, n);
        }
    }
    
    public CTConnector addNewCxnSp() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTConnector)this.get_store().add_element_user(CTGroupShapeImpl.CXNSP$10);
        }
    }
    
    public void removeCxnSp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupShapeImpl.CXNSP$10, n);
        }
    }
    
    public List<CTPicture> getPicList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPicture>)new CTGroupShapeImpl.PicList(this);
        }
    }
    
    public CTPicture[] getPicArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGroupShapeImpl.PIC$12, list);
            final CTPicture[] array = new CTPicture[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPicture getPicArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPicture ctPicture = (CTPicture)this.get_store().find_element_user(CTGroupShapeImpl.PIC$12, n);
            if (ctPicture == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPicture;
        }
    }
    
    public int sizeOfPicArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGroupShapeImpl.PIC$12);
        }
    }
    
    public void setPicArray(final CTPicture[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGroupShapeImpl.PIC$12);
        }
    }
    
    public void setPicArray(final int n, final CTPicture ctPicture) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPicture ctPicture2 = (CTPicture)this.get_store().find_element_user(CTGroupShapeImpl.PIC$12, n);
            if (ctPicture2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPicture2.set(ctPicture);
        }
    }
    
    public CTPicture insertNewPic(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPicture)this.get_store().insert_element_user(CTGroupShapeImpl.PIC$12, n);
        }
    }
    
    public CTPicture addNewPic() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPicture)this.get_store().add_element_user(CTGroupShapeImpl.PIC$12);
        }
    }
    
    public void removePic(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGroupShapeImpl.PIC$12, n);
        }
    }
    
    static {
        NVGRPSPPR$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "nvGrpSpPr");
        GRPSPPR$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "grpSpPr");
        SP$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "sp");
        GRPSP$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "grpSp");
        GRAPHICFRAME$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "graphicFrame");
        CXNSP$10 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "cxnSp");
        PIC$12 = new QName("http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing", "pic");
    }
}
