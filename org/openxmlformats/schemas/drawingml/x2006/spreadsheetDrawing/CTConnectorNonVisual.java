// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualConnectorProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTConnectorNonVisual extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTConnectorNonVisual.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctconnectornonvisual1a74type");
    
    CTNonVisualDrawingProps getCNvPr();
    
    void setCNvPr(final CTNonVisualDrawingProps p0);
    
    CTNonVisualDrawingProps addNewCNvPr();
    
    CTNonVisualConnectorProperties getCNvCxnSpPr();
    
    void setCNvCxnSpPr(final CTNonVisualConnectorProperties p0);
    
    CTNonVisualConnectorProperties addNewCNvCxnSpPr();
    
    public static final class Factory
    {
        public static CTConnectorNonVisual newInstance() {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().newInstance(CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual newInstance(final XmlOptions xmlOptions) {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().newInstance(CTConnectorNonVisual.type, xmlOptions);
        }
        
        public static CTConnectorNonVisual parse(final String s) throws XmlException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(s, CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(s, CTConnectorNonVisual.type, xmlOptions);
        }
        
        public static CTConnectorNonVisual parse(final File file) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(file, CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(file, CTConnectorNonVisual.type, xmlOptions);
        }
        
        public static CTConnectorNonVisual parse(final URL url) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(url, CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(url, CTConnectorNonVisual.type, xmlOptions);
        }
        
        public static CTConnectorNonVisual parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(inputStream, CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(inputStream, CTConnectorNonVisual.type, xmlOptions);
        }
        
        public static CTConnectorNonVisual parse(final Reader reader) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(reader, CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(reader, CTConnectorNonVisual.type, xmlOptions);
        }
        
        public static CTConnectorNonVisual parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTConnectorNonVisual.type, xmlOptions);
        }
        
        public static CTConnectorNonVisual parse(final Node node) throws XmlException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(node, CTConnectorNonVisual.type, null);
        }
        
        public static CTConnectorNonVisual parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(node, CTConnectorNonVisual.type, xmlOptions);
        }
        
        @Deprecated
        public static CTConnectorNonVisual parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTConnectorNonVisual.type, null);
        }
        
        @Deprecated
        public static CTConnectorNonVisual parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTConnectorNonVisual)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTConnectorNonVisual.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTConnectorNonVisual.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTConnectorNonVisual.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
