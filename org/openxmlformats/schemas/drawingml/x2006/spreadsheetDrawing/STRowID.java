// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STRowID extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STRowID.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("strowidf4cftype");
    
    public static final class Factory
    {
        public static STRowID newValue(final Object o) {
            return (STRowID)STRowID.type.newValue(o);
        }
        
        public static STRowID newInstance() {
            return (STRowID)XmlBeans.getContextTypeLoader().newInstance(STRowID.type, null);
        }
        
        public static STRowID newInstance(final XmlOptions xmlOptions) {
            return (STRowID)XmlBeans.getContextTypeLoader().newInstance(STRowID.type, xmlOptions);
        }
        
        public static STRowID parse(final String s) throws XmlException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(s, STRowID.type, null);
        }
        
        public static STRowID parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(s, STRowID.type, xmlOptions);
        }
        
        public static STRowID parse(final File file) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(file, STRowID.type, null);
        }
        
        public static STRowID parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(file, STRowID.type, xmlOptions);
        }
        
        public static STRowID parse(final URL url) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(url, STRowID.type, null);
        }
        
        public static STRowID parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(url, STRowID.type, xmlOptions);
        }
        
        public static STRowID parse(final InputStream inputStream) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(inputStream, STRowID.type, null);
        }
        
        public static STRowID parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(inputStream, STRowID.type, xmlOptions);
        }
        
        public static STRowID parse(final Reader reader) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(reader, STRowID.type, null);
        }
        
        public static STRowID parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(reader, STRowID.type, xmlOptions);
        }
        
        public static STRowID parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STRowID.type, null);
        }
        
        public static STRowID parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STRowID.type, xmlOptions);
        }
        
        public static STRowID parse(final Node node) throws XmlException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(node, STRowID.type, null);
        }
        
        public static STRowID parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(node, STRowID.type, xmlOptions);
        }
        
        @Deprecated
        public static STRowID parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STRowID.type, null);
        }
        
        @Deprecated
        public static STRowID parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STRowID)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STRowID.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STRowID.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STRowID.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
