// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.drawingml.x2006.main.STCoordinate;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTMarker extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTMarker.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctmarkeree8etype");
    
    int getCol();
    
    STColID xgetCol();
    
    void setCol(final int p0);
    
    void xsetCol(final STColID p0);
    
    long getColOff();
    
    STCoordinate xgetColOff();
    
    void setColOff(final long p0);
    
    void xsetColOff(final STCoordinate p0);
    
    int getRow();
    
    STRowID xgetRow();
    
    void setRow(final int p0);
    
    void xsetRow(final STRowID p0);
    
    long getRowOff();
    
    STCoordinate xgetRowOff();
    
    void setRowOff(final long p0);
    
    void xsetRowOff(final STCoordinate p0);
    
    public static final class Factory
    {
        public static CTMarker newInstance() {
            return (CTMarker)XmlBeans.getContextTypeLoader().newInstance(CTMarker.type, null);
        }
        
        public static CTMarker newInstance(final XmlOptions xmlOptions) {
            return (CTMarker)XmlBeans.getContextTypeLoader().newInstance(CTMarker.type, xmlOptions);
        }
        
        public static CTMarker parse(final String s) throws XmlException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(s, CTMarker.type, null);
        }
        
        public static CTMarker parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(s, CTMarker.type, xmlOptions);
        }
        
        public static CTMarker parse(final File file) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(file, CTMarker.type, null);
        }
        
        public static CTMarker parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(file, CTMarker.type, xmlOptions);
        }
        
        public static CTMarker parse(final URL url) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(url, CTMarker.type, null);
        }
        
        public static CTMarker parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(url, CTMarker.type, xmlOptions);
        }
        
        public static CTMarker parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(inputStream, CTMarker.type, null);
        }
        
        public static CTMarker parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(inputStream, CTMarker.type, xmlOptions);
        }
        
        public static CTMarker parse(final Reader reader) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(reader, CTMarker.type, null);
        }
        
        public static CTMarker parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(reader, CTMarker.type, xmlOptions);
        }
        
        public static CTMarker parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMarker.type, null);
        }
        
        public static CTMarker parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMarker.type, xmlOptions);
        }
        
        public static CTMarker parse(final Node node) throws XmlException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(node, CTMarker.type, null);
        }
        
        public static CTMarker parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(node, CTMarker.type, xmlOptions);
        }
        
        @Deprecated
        public static CTMarker parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMarker.type, null);
        }
        
        @Deprecated
        public static CTMarker parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTMarker)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMarker.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMarker.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMarker.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
