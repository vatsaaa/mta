// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTBlipFillProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTBlipFillProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctblipfillproperties0382type");
    
    CTBlip getBlip();
    
    boolean isSetBlip();
    
    void setBlip(final CTBlip p0);
    
    CTBlip addNewBlip();
    
    void unsetBlip();
    
    CTRelativeRect getSrcRect();
    
    boolean isSetSrcRect();
    
    void setSrcRect(final CTRelativeRect p0);
    
    CTRelativeRect addNewSrcRect();
    
    void unsetSrcRect();
    
    CTTileInfoProperties getTile();
    
    boolean isSetTile();
    
    void setTile(final CTTileInfoProperties p0);
    
    CTTileInfoProperties addNewTile();
    
    void unsetTile();
    
    CTStretchInfoProperties getStretch();
    
    boolean isSetStretch();
    
    void setStretch(final CTStretchInfoProperties p0);
    
    CTStretchInfoProperties addNewStretch();
    
    void unsetStretch();
    
    long getDpi();
    
    XmlUnsignedInt xgetDpi();
    
    boolean isSetDpi();
    
    void setDpi(final long p0);
    
    void xsetDpi(final XmlUnsignedInt p0);
    
    void unsetDpi();
    
    boolean getRotWithShape();
    
    XmlBoolean xgetRotWithShape();
    
    boolean isSetRotWithShape();
    
    void setRotWithShape(final boolean p0);
    
    void xsetRotWithShape(final XmlBoolean p0);
    
    void unsetRotWithShape();
    
    public static final class Factory
    {
        public static CTBlipFillProperties newInstance() {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().newInstance(CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties newInstance(final XmlOptions xmlOptions) {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().newInstance(CTBlipFillProperties.type, xmlOptions);
        }
        
        public static CTBlipFillProperties parse(final String s) throws XmlException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(s, CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(s, CTBlipFillProperties.type, xmlOptions);
        }
        
        public static CTBlipFillProperties parse(final File file) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(file, CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(file, CTBlipFillProperties.type, xmlOptions);
        }
        
        public static CTBlipFillProperties parse(final URL url) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(url, CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(url, CTBlipFillProperties.type, xmlOptions);
        }
        
        public static CTBlipFillProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTBlipFillProperties.type, xmlOptions);
        }
        
        public static CTBlipFillProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(reader, CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(reader, CTBlipFillProperties.type, xmlOptions);
        }
        
        public static CTBlipFillProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBlipFillProperties.type, xmlOptions);
        }
        
        public static CTBlipFillProperties parse(final Node node) throws XmlException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(node, CTBlipFillProperties.type, null);
        }
        
        public static CTBlipFillProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(node, CTBlipFillProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTBlipFillProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBlipFillProperties.type, null);
        }
        
        @Deprecated
        public static CTBlipFillProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTBlipFillProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBlipFillProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBlipFillProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBlipFillProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
