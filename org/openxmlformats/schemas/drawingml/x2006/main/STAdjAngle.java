// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlAnySimpleType;

public interface STAdjAngle extends XmlAnySimpleType
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STAdjAngle.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stadjanglef017type");
    
    Object getObjectValue();
    
    void setObjectValue(final Object p0);
    
    @Deprecated
    Object objectValue();
    
    @Deprecated
    void objectSet(final Object p0);
    
    SchemaType instanceType();
    
    public static final class Factory
    {
        public static STAdjAngle newValue(final Object o) {
            return (STAdjAngle)STAdjAngle.type.newValue(o);
        }
        
        public static STAdjAngle newInstance() {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().newInstance(STAdjAngle.type, null);
        }
        
        public static STAdjAngle newInstance(final XmlOptions xmlOptions) {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().newInstance(STAdjAngle.type, xmlOptions);
        }
        
        public static STAdjAngle parse(final String s) throws XmlException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(s, STAdjAngle.type, null);
        }
        
        public static STAdjAngle parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(s, STAdjAngle.type, xmlOptions);
        }
        
        public static STAdjAngle parse(final File file) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(file, STAdjAngle.type, null);
        }
        
        public static STAdjAngle parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(file, STAdjAngle.type, xmlOptions);
        }
        
        public static STAdjAngle parse(final URL url) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(url, STAdjAngle.type, null);
        }
        
        public static STAdjAngle parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(url, STAdjAngle.type, xmlOptions);
        }
        
        public static STAdjAngle parse(final InputStream inputStream) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(inputStream, STAdjAngle.type, null);
        }
        
        public static STAdjAngle parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(inputStream, STAdjAngle.type, xmlOptions);
        }
        
        public static STAdjAngle parse(final Reader reader) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(reader, STAdjAngle.type, null);
        }
        
        public static STAdjAngle parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(reader, STAdjAngle.type, xmlOptions);
        }
        
        public static STAdjAngle parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAdjAngle.type, null);
        }
        
        public static STAdjAngle parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAdjAngle.type, xmlOptions);
        }
        
        public static STAdjAngle parse(final Node node) throws XmlException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(node, STAdjAngle.type, null);
        }
        
        public static STAdjAngle parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(node, STAdjAngle.type, xmlOptions);
        }
        
        @Deprecated
        public static STAdjAngle parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAdjAngle.type, null);
        }
        
        @Deprecated
        public static STAdjAngle parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STAdjAngle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAdjAngle.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAdjAngle.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAdjAngle.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
