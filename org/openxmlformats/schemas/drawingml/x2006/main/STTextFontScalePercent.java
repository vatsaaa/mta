// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STTextFontScalePercent extends STPercentage
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextFontScalePercent.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextfontscalepercente6c2type");
    
    public static final class Factory
    {
        public static STTextFontScalePercent newValue(final Object o) {
            return (STTextFontScalePercent)STTextFontScalePercent.type.newValue(o);
        }
        
        public static STTextFontScalePercent newInstance() {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().newInstance(STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent newInstance(final XmlOptions xmlOptions) {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().newInstance(STTextFontScalePercent.type, xmlOptions);
        }
        
        public static STTextFontScalePercent parse(final String s) throws XmlException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(s, STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(s, STTextFontScalePercent.type, xmlOptions);
        }
        
        public static STTextFontScalePercent parse(final File file) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(file, STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(file, STTextFontScalePercent.type, xmlOptions);
        }
        
        public static STTextFontScalePercent parse(final URL url) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(url, STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(url, STTextFontScalePercent.type, xmlOptions);
        }
        
        public static STTextFontScalePercent parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextFontScalePercent.type, xmlOptions);
        }
        
        public static STTextFontScalePercent parse(final Reader reader) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(reader, STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(reader, STTextFontScalePercent.type, xmlOptions);
        }
        
        public static STTextFontScalePercent parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextFontScalePercent.type, xmlOptions);
        }
        
        public static STTextFontScalePercent parse(final Node node) throws XmlException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(node, STTextFontScalePercent.type, null);
        }
        
        public static STTextFontScalePercent parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(node, STTextFontScalePercent.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextFontScalePercent parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextFontScalePercent.type, null);
        }
        
        @Deprecated
        public static STTextFontScalePercent parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextFontScalePercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextFontScalePercent.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextFontScalePercent.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextFontScalePercent.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
