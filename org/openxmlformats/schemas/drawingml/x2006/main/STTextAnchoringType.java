// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlToken;

public interface STTextAnchoringType extends XmlToken
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextAnchoringType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextanchoringtyped99btype");
    public static final Enum T = Enum.forString("t");
    public static final Enum CTR = Enum.forString("ctr");
    public static final Enum B = Enum.forString("b");
    public static final Enum JUST = Enum.forString("just");
    public static final Enum DIST = Enum.forString("dist");
    public static final int INT_T = 1;
    public static final int INT_CTR = 2;
    public static final int INT_B = 3;
    public static final int INT_JUST = 4;
    public static final int INT_DIST = 5;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STTextAnchoringType newValue(final Object o) {
            return (STTextAnchoringType)STTextAnchoringType.type.newValue(o);
        }
        
        public static STTextAnchoringType newInstance() {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().newInstance(STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType newInstance(final XmlOptions xmlOptions) {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().newInstance(STTextAnchoringType.type, xmlOptions);
        }
        
        public static STTextAnchoringType parse(final String s) throws XmlException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(s, STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(s, STTextAnchoringType.type, xmlOptions);
        }
        
        public static STTextAnchoringType parse(final File file) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(file, STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(file, STTextAnchoringType.type, xmlOptions);
        }
        
        public static STTextAnchoringType parse(final URL url) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(url, STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(url, STTextAnchoringType.type, xmlOptions);
        }
        
        public static STTextAnchoringType parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(inputStream, STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(inputStream, STTextAnchoringType.type, xmlOptions);
        }
        
        public static STTextAnchoringType parse(final Reader reader) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(reader, STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(reader, STTextAnchoringType.type, xmlOptions);
        }
        
        public static STTextAnchoringType parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextAnchoringType.type, xmlOptions);
        }
        
        public static STTextAnchoringType parse(final Node node) throws XmlException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(node, STTextAnchoringType.type, null);
        }
        
        public static STTextAnchoringType parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(node, STTextAnchoringType.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextAnchoringType parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextAnchoringType.type, null);
        }
        
        @Deprecated
        public static STTextAnchoringType parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextAnchoringType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextAnchoringType.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextAnchoringType.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextAnchoringType.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_T = 1;
        static final int INT_CTR = 2;
        static final int INT_B = 3;
        static final int INT_JUST = 4;
        static final int INT_DIST = 5;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("t", 1), new Enum("ctr", 2), new Enum("b", 3), new Enum("just", 4), new Enum("dist", 5) });
        }
    }
}
