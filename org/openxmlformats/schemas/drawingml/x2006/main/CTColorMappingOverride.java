// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTColorMappingOverride extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTColorMappingOverride.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcolormappingoverridea2b2type");
    
    CTEmptyElement getMasterClrMapping();
    
    boolean isSetMasterClrMapping();
    
    void setMasterClrMapping(final CTEmptyElement p0);
    
    CTEmptyElement addNewMasterClrMapping();
    
    void unsetMasterClrMapping();
    
    CTColorMapping getOverrideClrMapping();
    
    boolean isSetOverrideClrMapping();
    
    void setOverrideClrMapping(final CTColorMapping p0);
    
    CTColorMapping addNewOverrideClrMapping();
    
    void unsetOverrideClrMapping();
    
    public static final class Factory
    {
        public static CTColorMappingOverride newInstance() {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().newInstance(CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride newInstance(final XmlOptions xmlOptions) {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().newInstance(CTColorMappingOverride.type, xmlOptions);
        }
        
        public static CTColorMappingOverride parse(final String s) throws XmlException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(s, CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(s, CTColorMappingOverride.type, xmlOptions);
        }
        
        public static CTColorMappingOverride parse(final File file) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(file, CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(file, CTColorMappingOverride.type, xmlOptions);
        }
        
        public static CTColorMappingOverride parse(final URL url) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(url, CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(url, CTColorMappingOverride.type, xmlOptions);
        }
        
        public static CTColorMappingOverride parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(inputStream, CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(inputStream, CTColorMappingOverride.type, xmlOptions);
        }
        
        public static CTColorMappingOverride parse(final Reader reader) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(reader, CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(reader, CTColorMappingOverride.type, xmlOptions);
        }
        
        public static CTColorMappingOverride parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTColorMappingOverride.type, xmlOptions);
        }
        
        public static CTColorMappingOverride parse(final Node node) throws XmlException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(node, CTColorMappingOverride.type, null);
        }
        
        public static CTColorMappingOverride parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(node, CTColorMappingOverride.type, xmlOptions);
        }
        
        @Deprecated
        public static CTColorMappingOverride parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTColorMappingOverride.type, null);
        }
        
        @Deprecated
        public static CTColorMappingOverride parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTColorMappingOverride)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTColorMappingOverride.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTColorMappingOverride.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTColorMappingOverride.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
