// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlToken;

public interface STFontCollectionIndex extends XmlToken
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STFontCollectionIndex.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stfontcollectionindex6766type");
    public static final Enum MAJOR = Enum.forString("major");
    public static final Enum MINOR = Enum.forString("minor");
    public static final Enum NONE = Enum.forString("none");
    public static final int INT_MAJOR = 1;
    public static final int INT_MINOR = 2;
    public static final int INT_NONE = 3;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STFontCollectionIndex newValue(final Object o) {
            return (STFontCollectionIndex)STFontCollectionIndex.type.newValue(o);
        }
        
        public static STFontCollectionIndex newInstance() {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().newInstance(STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex newInstance(final XmlOptions xmlOptions) {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().newInstance(STFontCollectionIndex.type, xmlOptions);
        }
        
        public static STFontCollectionIndex parse(final String s) throws XmlException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(s, STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(s, STFontCollectionIndex.type, xmlOptions);
        }
        
        public static STFontCollectionIndex parse(final File file) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(file, STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(file, STFontCollectionIndex.type, xmlOptions);
        }
        
        public static STFontCollectionIndex parse(final URL url) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(url, STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(url, STFontCollectionIndex.type, xmlOptions);
        }
        
        public static STFontCollectionIndex parse(final InputStream inputStream) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(inputStream, STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(inputStream, STFontCollectionIndex.type, xmlOptions);
        }
        
        public static STFontCollectionIndex parse(final Reader reader) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(reader, STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(reader, STFontCollectionIndex.type, xmlOptions);
        }
        
        public static STFontCollectionIndex parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFontCollectionIndex.type, xmlOptions);
        }
        
        public static STFontCollectionIndex parse(final Node node) throws XmlException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(node, STFontCollectionIndex.type, null);
        }
        
        public static STFontCollectionIndex parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(node, STFontCollectionIndex.type, xmlOptions);
        }
        
        @Deprecated
        public static STFontCollectionIndex parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFontCollectionIndex.type, null);
        }
        
        @Deprecated
        public static STFontCollectionIndex parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STFontCollectionIndex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFontCollectionIndex.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFontCollectionIndex.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFontCollectionIndex.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_MAJOR = 1;
        static final int INT_MINOR = 2;
        static final int INT_NONE = 3;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("major", 1), new Enum("minor", 2), new Enum("none", 3) });
        }
    }
}
