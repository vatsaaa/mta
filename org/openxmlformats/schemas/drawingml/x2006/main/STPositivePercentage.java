// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STPositivePercentage extends STPercentage
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPositivePercentage.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpositivepercentagedb9etype");
    
    public static final class Factory
    {
        public static STPositivePercentage newValue(final Object o) {
            return (STPositivePercentage)STPositivePercentage.type.newValue(o);
        }
        
        public static STPositivePercentage newInstance() {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().newInstance(STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage newInstance(final XmlOptions xmlOptions) {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().newInstance(STPositivePercentage.type, xmlOptions);
        }
        
        public static STPositivePercentage parse(final String s) throws XmlException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(s, STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(s, STPositivePercentage.type, xmlOptions);
        }
        
        public static STPositivePercentage parse(final File file) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(file, STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(file, STPositivePercentage.type, xmlOptions);
        }
        
        public static STPositivePercentage parse(final URL url) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(url, STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(url, STPositivePercentage.type, xmlOptions);
        }
        
        public static STPositivePercentage parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STPositivePercentage.type, xmlOptions);
        }
        
        public static STPositivePercentage parse(final Reader reader) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(reader, STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(reader, STPositivePercentage.type, xmlOptions);
        }
        
        public static STPositivePercentage parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositivePercentage.type, xmlOptions);
        }
        
        public static STPositivePercentage parse(final Node node) throws XmlException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(node, STPositivePercentage.type, null);
        }
        
        public static STPositivePercentage parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(node, STPositivePercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static STPositivePercentage parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositivePercentage.type, null);
        }
        
        @Deprecated
        public static STPositivePercentage parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPositivePercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositivePercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositivePercentage.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositivePercentage.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
