// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNonVisualConnectorProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNonVisualConnectorProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnonvisualconnectorproperties6f8etype");
    
    CTConnectorLocking getCxnSpLocks();
    
    boolean isSetCxnSpLocks();
    
    void setCxnSpLocks(final CTConnectorLocking p0);
    
    CTConnectorLocking addNewCxnSpLocks();
    
    void unsetCxnSpLocks();
    
    CTConnection getStCxn();
    
    boolean isSetStCxn();
    
    void setStCxn(final CTConnection p0);
    
    CTConnection addNewStCxn();
    
    void unsetStCxn();
    
    CTConnection getEndCxn();
    
    boolean isSetEndCxn();
    
    void setEndCxn(final CTConnection p0);
    
    CTConnection addNewEndCxn();
    
    void unsetEndCxn();
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTNonVisualConnectorProperties newInstance() {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().newInstance(CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties newInstance(final XmlOptions xmlOptions) {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().newInstance(CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        public static CTNonVisualConnectorProperties parse(final String s) throws XmlException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(s, CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(s, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        public static CTNonVisualConnectorProperties parse(final File file) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(file, CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(file, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        public static CTNonVisualConnectorProperties parse(final URL url) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(url, CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(url, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        public static CTNonVisualConnectorProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        public static CTNonVisualConnectorProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        public static CTNonVisualConnectorProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        public static CTNonVisualConnectorProperties parse(final Node node) throws XmlException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(node, CTNonVisualConnectorProperties.type, null);
        }
        
        public static CTNonVisualConnectorProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(node, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNonVisualConnectorProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNonVisualConnectorProperties.type, null);
        }
        
        @Deprecated
        public static CTNonVisualConnectorProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNonVisualConnectorProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNonVisualConnectorProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNonVisualConnectorProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
