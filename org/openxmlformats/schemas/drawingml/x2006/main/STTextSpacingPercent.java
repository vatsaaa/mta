// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STTextSpacingPercent extends STPercentage
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextSpacingPercent.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextspacingpercentde3atype");
    
    public static final class Factory
    {
        public static STTextSpacingPercent newValue(final Object o) {
            return (STTextSpacingPercent)STTextSpacingPercent.type.newValue(o);
        }
        
        public static STTextSpacingPercent newInstance() {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().newInstance(STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent newInstance(final XmlOptions xmlOptions) {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().newInstance(STTextSpacingPercent.type, xmlOptions);
        }
        
        public static STTextSpacingPercent parse(final String s) throws XmlException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(s, STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(s, STTextSpacingPercent.type, xmlOptions);
        }
        
        public static STTextSpacingPercent parse(final File file) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(file, STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(file, STTextSpacingPercent.type, xmlOptions);
        }
        
        public static STTextSpacingPercent parse(final URL url) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(url, STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(url, STTextSpacingPercent.type, xmlOptions);
        }
        
        public static STTextSpacingPercent parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextSpacingPercent.type, xmlOptions);
        }
        
        public static STTextSpacingPercent parse(final Reader reader) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(reader, STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(reader, STTextSpacingPercent.type, xmlOptions);
        }
        
        public static STTextSpacingPercent parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextSpacingPercent.type, xmlOptions);
        }
        
        public static STTextSpacingPercent parse(final Node node) throws XmlException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(node, STTextSpacingPercent.type, null);
        }
        
        public static STTextSpacingPercent parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(node, STTextSpacingPercent.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextSpacingPercent parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextSpacingPercent.type, null);
        }
        
        @Deprecated
        public static STTextSpacingPercent parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextSpacingPercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextSpacingPercent.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextSpacingPercent.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextSpacingPercent.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
