// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTEmptyElement extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTEmptyElement.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctemptyelement05catype");
    
    public static final class Factory
    {
        public static CTEmptyElement newInstance() {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().newInstance(CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement newInstance(final XmlOptions xmlOptions) {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().newInstance(CTEmptyElement.type, xmlOptions);
        }
        
        public static CTEmptyElement parse(final String s) throws XmlException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(s, CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(s, CTEmptyElement.type, xmlOptions);
        }
        
        public static CTEmptyElement parse(final File file) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(file, CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(file, CTEmptyElement.type, xmlOptions);
        }
        
        public static CTEmptyElement parse(final URL url) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(url, CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(url, CTEmptyElement.type, xmlOptions);
        }
        
        public static CTEmptyElement parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(inputStream, CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(inputStream, CTEmptyElement.type, xmlOptions);
        }
        
        public static CTEmptyElement parse(final Reader reader) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(reader, CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(reader, CTEmptyElement.type, xmlOptions);
        }
        
        public static CTEmptyElement parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTEmptyElement.type, xmlOptions);
        }
        
        public static CTEmptyElement parse(final Node node) throws XmlException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(node, CTEmptyElement.type, null);
        }
        
        public static CTEmptyElement parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(node, CTEmptyElement.type, xmlOptions);
        }
        
        @Deprecated
        public static CTEmptyElement parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTEmptyElement.type, null);
        }
        
        @Deprecated
        public static CTEmptyElement parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTEmptyElement)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTEmptyElement.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTEmptyElement.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTEmptyElement.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
