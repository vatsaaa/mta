// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STTextIndentLevelType extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextIndentLevelType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextindentleveltypeaf86type");
    
    public static final class Factory
    {
        public static STTextIndentLevelType newValue(final Object o) {
            return (STTextIndentLevelType)STTextIndentLevelType.type.newValue(o);
        }
        
        public static STTextIndentLevelType newInstance() {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().newInstance(STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType newInstance(final XmlOptions xmlOptions) {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().newInstance(STTextIndentLevelType.type, xmlOptions);
        }
        
        public static STTextIndentLevelType parse(final String s) throws XmlException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(s, STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(s, STTextIndentLevelType.type, xmlOptions);
        }
        
        public static STTextIndentLevelType parse(final File file) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(file, STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(file, STTextIndentLevelType.type, xmlOptions);
        }
        
        public static STTextIndentLevelType parse(final URL url) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(url, STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(url, STTextIndentLevelType.type, xmlOptions);
        }
        
        public static STTextIndentLevelType parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(inputStream, STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(inputStream, STTextIndentLevelType.type, xmlOptions);
        }
        
        public static STTextIndentLevelType parse(final Reader reader) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(reader, STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(reader, STTextIndentLevelType.type, xmlOptions);
        }
        
        public static STTextIndentLevelType parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextIndentLevelType.type, xmlOptions);
        }
        
        public static STTextIndentLevelType parse(final Node node) throws XmlException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(node, STTextIndentLevelType.type, null);
        }
        
        public static STTextIndentLevelType parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(node, STTextIndentLevelType.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextIndentLevelType parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextIndentLevelType.type, null);
        }
        
        @Deprecated
        public static STTextIndentLevelType parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextIndentLevelType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextIndentLevelType.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextIndentLevelType.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextIndentLevelType.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
