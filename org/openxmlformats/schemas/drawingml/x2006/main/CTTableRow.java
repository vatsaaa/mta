// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTableRow extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTableRow.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttablerow4ac7type");
    
    List<CTTableCell> getTcList();
    
    @Deprecated
    CTTableCell[] getTcArray();
    
    CTTableCell getTcArray(final int p0);
    
    int sizeOfTcArray();
    
    void setTcArray(final CTTableCell[] p0);
    
    void setTcArray(final int p0, final CTTableCell p1);
    
    CTTableCell insertNewTc(final int p0);
    
    CTTableCell addNewTc();
    
    void removeTc(final int p0);
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    long getH();
    
    STCoordinate xgetH();
    
    void setH(final long p0);
    
    void xsetH(final STCoordinate p0);
    
    public static final class Factory
    {
        public static CTTableRow newInstance() {
            return (CTTableRow)XmlBeans.getContextTypeLoader().newInstance(CTTableRow.type, null);
        }
        
        public static CTTableRow newInstance(final XmlOptions xmlOptions) {
            return (CTTableRow)XmlBeans.getContextTypeLoader().newInstance(CTTableRow.type, xmlOptions);
        }
        
        public static CTTableRow parse(final String s) throws XmlException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(s, CTTableRow.type, null);
        }
        
        public static CTTableRow parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(s, CTTableRow.type, xmlOptions);
        }
        
        public static CTTableRow parse(final File file) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(file, CTTableRow.type, null);
        }
        
        public static CTTableRow parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(file, CTTableRow.type, xmlOptions);
        }
        
        public static CTTableRow parse(final URL url) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(url, CTTableRow.type, null);
        }
        
        public static CTTableRow parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(url, CTTableRow.type, xmlOptions);
        }
        
        public static CTTableRow parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableRow.type, null);
        }
        
        public static CTTableRow parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableRow.type, xmlOptions);
        }
        
        public static CTTableRow parse(final Reader reader) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(reader, CTTableRow.type, null);
        }
        
        public static CTTableRow parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(reader, CTTableRow.type, xmlOptions);
        }
        
        public static CTTableRow parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableRow.type, null);
        }
        
        public static CTTableRow parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableRow.type, xmlOptions);
        }
        
        public static CTTableRow parse(final Node node) throws XmlException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(node, CTTableRow.type, null);
        }
        
        public static CTTableRow parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(node, CTTableRow.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTableRow parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableRow.type, null);
        }
        
        @Deprecated
        public static CTTableRow parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTableRow)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableRow.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableRow.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableRow.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
