// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTOfficeStyleSheet extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTOfficeStyleSheet.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctofficestylesheetce25type");
    
    CTBaseStyles getThemeElements();
    
    void setThemeElements(final CTBaseStyles p0);
    
    CTBaseStyles addNewThemeElements();
    
    CTObjectStyleDefaults getObjectDefaults();
    
    boolean isSetObjectDefaults();
    
    void setObjectDefaults(final CTObjectStyleDefaults p0);
    
    CTObjectStyleDefaults addNewObjectDefaults();
    
    void unsetObjectDefaults();
    
    CTColorSchemeList getExtraClrSchemeLst();
    
    boolean isSetExtraClrSchemeLst();
    
    void setExtraClrSchemeLst(final CTColorSchemeList p0);
    
    CTColorSchemeList addNewExtraClrSchemeLst();
    
    void unsetExtraClrSchemeLst();
    
    CTCustomColorList getCustClrLst();
    
    boolean isSetCustClrLst();
    
    void setCustClrLst(final CTCustomColorList p0);
    
    CTCustomColorList addNewCustClrLst();
    
    void unsetCustClrLst();
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    String getName();
    
    XmlString xgetName();
    
    boolean isSetName();
    
    void setName(final String p0);
    
    void xsetName(final XmlString p0);
    
    void unsetName();
    
    public static final class Factory
    {
        public static CTOfficeStyleSheet newInstance() {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().newInstance(CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet newInstance(final XmlOptions xmlOptions) {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().newInstance(CTOfficeStyleSheet.type, xmlOptions);
        }
        
        public static CTOfficeStyleSheet parse(final String s) throws XmlException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(s, CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(s, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        public static CTOfficeStyleSheet parse(final File file) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(file, CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(file, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        public static CTOfficeStyleSheet parse(final URL url) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(url, CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(url, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        public static CTOfficeStyleSheet parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(inputStream, CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(inputStream, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        public static CTOfficeStyleSheet parse(final Reader reader) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(reader, CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(reader, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        public static CTOfficeStyleSheet parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        public static CTOfficeStyleSheet parse(final Node node) throws XmlException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(node, CTOfficeStyleSheet.type, null);
        }
        
        public static CTOfficeStyleSheet parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(node, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        @Deprecated
        public static CTOfficeStyleSheet parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTOfficeStyleSheet.type, null);
        }
        
        @Deprecated
        public static CTOfficeStyleSheet parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTOfficeStyleSheet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTOfficeStyleSheet.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTOfficeStyleSheet.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
