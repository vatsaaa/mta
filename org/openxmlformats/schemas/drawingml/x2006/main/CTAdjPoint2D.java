// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTAdjPoint2D extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTAdjPoint2D.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctadjpoint2d1656type");
    
    Object getX();
    
    STAdjCoordinate xgetX();
    
    void setX(final Object p0);
    
    void xsetX(final STAdjCoordinate p0);
    
    Object getY();
    
    STAdjCoordinate xgetY();
    
    void setY(final Object p0);
    
    void xsetY(final STAdjCoordinate p0);
    
    public static final class Factory
    {
        public static CTAdjPoint2D newInstance() {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().newInstance(CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D newInstance(final XmlOptions xmlOptions) {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().newInstance(CTAdjPoint2D.type, xmlOptions);
        }
        
        public static CTAdjPoint2D parse(final String s) throws XmlException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(s, CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(s, CTAdjPoint2D.type, xmlOptions);
        }
        
        public static CTAdjPoint2D parse(final File file) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(file, CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(file, CTAdjPoint2D.type, xmlOptions);
        }
        
        public static CTAdjPoint2D parse(final URL url) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(url, CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(url, CTAdjPoint2D.type, xmlOptions);
        }
        
        public static CTAdjPoint2D parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(inputStream, CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(inputStream, CTAdjPoint2D.type, xmlOptions);
        }
        
        public static CTAdjPoint2D parse(final Reader reader) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(reader, CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(reader, CTAdjPoint2D.type, xmlOptions);
        }
        
        public static CTAdjPoint2D parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAdjPoint2D.type, xmlOptions);
        }
        
        public static CTAdjPoint2D parse(final Node node) throws XmlException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(node, CTAdjPoint2D.type, null);
        }
        
        public static CTAdjPoint2D parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(node, CTAdjPoint2D.type, xmlOptions);
        }
        
        @Deprecated
        public static CTAdjPoint2D parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAdjPoint2D.type, null);
        }
        
        @Deprecated
        public static CTAdjPoint2D parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTAdjPoint2D)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAdjPoint2D.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAdjPoint2D.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAdjPoint2D.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
