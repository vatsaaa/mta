// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTLinearShadeProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTLinearShadeProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctlinearshadeproperties7f0ctype");
    
    int getAng();
    
    STPositiveFixedAngle xgetAng();
    
    boolean isSetAng();
    
    void setAng(final int p0);
    
    void xsetAng(final STPositiveFixedAngle p0);
    
    void unsetAng();
    
    boolean getScaled();
    
    XmlBoolean xgetScaled();
    
    boolean isSetScaled();
    
    void setScaled(final boolean p0);
    
    void xsetScaled(final XmlBoolean p0);
    
    void unsetScaled();
    
    public static final class Factory
    {
        public static CTLinearShadeProperties newInstance() {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().newInstance(CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties newInstance(final XmlOptions xmlOptions) {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().newInstance(CTLinearShadeProperties.type, xmlOptions);
        }
        
        public static CTLinearShadeProperties parse(final String s) throws XmlException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(s, CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(s, CTLinearShadeProperties.type, xmlOptions);
        }
        
        public static CTLinearShadeProperties parse(final File file) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(file, CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(file, CTLinearShadeProperties.type, xmlOptions);
        }
        
        public static CTLinearShadeProperties parse(final URL url) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(url, CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(url, CTLinearShadeProperties.type, xmlOptions);
        }
        
        public static CTLinearShadeProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTLinearShadeProperties.type, xmlOptions);
        }
        
        public static CTLinearShadeProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(reader, CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(reader, CTLinearShadeProperties.type, xmlOptions);
        }
        
        public static CTLinearShadeProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLinearShadeProperties.type, xmlOptions);
        }
        
        public static CTLinearShadeProperties parse(final Node node) throws XmlException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(node, CTLinearShadeProperties.type, null);
        }
        
        public static CTLinearShadeProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(node, CTLinearShadeProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTLinearShadeProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLinearShadeProperties.type, null);
        }
        
        @Deprecated
        public static CTLinearShadeProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTLinearShadeProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLinearShadeProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLinearShadeProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLinearShadeProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
