// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGradientStopList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGradientStopList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgradientstoplist7eabtype");
    
    List<CTGradientStop> getGsList();
    
    @Deprecated
    CTGradientStop[] getGsArray();
    
    CTGradientStop getGsArray(final int p0);
    
    int sizeOfGsArray();
    
    void setGsArray(final CTGradientStop[] p0);
    
    void setGsArray(final int p0, final CTGradientStop p1);
    
    CTGradientStop insertNewGs(final int p0);
    
    CTGradientStop addNewGs();
    
    void removeGs(final int p0);
    
    public static final class Factory
    {
        public static CTGradientStopList newInstance() {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().newInstance(CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList newInstance(final XmlOptions xmlOptions) {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().newInstance(CTGradientStopList.type, xmlOptions);
        }
        
        public static CTGradientStopList parse(final String s) throws XmlException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(s, CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(s, CTGradientStopList.type, xmlOptions);
        }
        
        public static CTGradientStopList parse(final File file) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(file, CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(file, CTGradientStopList.type, xmlOptions);
        }
        
        public static CTGradientStopList parse(final URL url) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(url, CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(url, CTGradientStopList.type, xmlOptions);
        }
        
        public static CTGradientStopList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(inputStream, CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(inputStream, CTGradientStopList.type, xmlOptions);
        }
        
        public static CTGradientStopList parse(final Reader reader) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(reader, CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(reader, CTGradientStopList.type, xmlOptions);
        }
        
        public static CTGradientStopList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGradientStopList.type, xmlOptions);
        }
        
        public static CTGradientStopList parse(final Node node) throws XmlException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(node, CTGradientStopList.type, null);
        }
        
        public static CTGradientStopList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(node, CTGradientStopList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGradientStopList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGradientStopList.type, null);
        }
        
        @Deprecated
        public static CTGradientStopList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGradientStopList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGradientStopList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGradientStopList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGradientStopList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
