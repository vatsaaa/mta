// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGraphicalObject extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGraphicalObject.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgraphicalobject1ce3type");
    
    CTGraphicalObjectData getGraphicData();
    
    void setGraphicData(final CTGraphicalObjectData p0);
    
    CTGraphicalObjectData addNewGraphicData();
    
    public static final class Factory
    {
        public static CTGraphicalObject newInstance() {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().newInstance(CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject newInstance(final XmlOptions xmlOptions) {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().newInstance(CTGraphicalObject.type, xmlOptions);
        }
        
        public static CTGraphicalObject parse(final String s) throws XmlException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(s, CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(s, CTGraphicalObject.type, xmlOptions);
        }
        
        public static CTGraphicalObject parse(final File file) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(file, CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(file, CTGraphicalObject.type, xmlOptions);
        }
        
        public static CTGraphicalObject parse(final URL url) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(url, CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(url, CTGraphicalObject.type, xmlOptions);
        }
        
        public static CTGraphicalObject parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(inputStream, CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(inputStream, CTGraphicalObject.type, xmlOptions);
        }
        
        public static CTGraphicalObject parse(final Reader reader) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(reader, CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(reader, CTGraphicalObject.type, xmlOptions);
        }
        
        public static CTGraphicalObject parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGraphicalObject.type, xmlOptions);
        }
        
        public static CTGraphicalObject parse(final Node node) throws XmlException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(node, CTGraphicalObject.type, null);
        }
        
        public static CTGraphicalObject parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(node, CTGraphicalObject.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGraphicalObject parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGraphicalObject.type, null);
        }
        
        @Deprecated
        public static CTGraphicalObject parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGraphicalObject)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGraphicalObject.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGraphicalObject.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGraphicalObject.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
