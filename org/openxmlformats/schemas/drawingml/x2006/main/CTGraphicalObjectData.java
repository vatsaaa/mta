// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlToken;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGraphicalObjectData extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGraphicalObjectData.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgraphicalobjectdata66adtype");
    
    String getUri();
    
    XmlToken xgetUri();
    
    boolean isSetUri();
    
    void setUri(final String p0);
    
    void xsetUri(final XmlToken p0);
    
    void unsetUri();
    
    public static final class Factory
    {
        public static CTGraphicalObjectData newInstance() {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().newInstance(CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData newInstance(final XmlOptions xmlOptions) {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().newInstance(CTGraphicalObjectData.type, xmlOptions);
        }
        
        public static CTGraphicalObjectData parse(final String s) throws XmlException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(s, CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(s, CTGraphicalObjectData.type, xmlOptions);
        }
        
        public static CTGraphicalObjectData parse(final File file) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(file, CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(file, CTGraphicalObjectData.type, xmlOptions);
        }
        
        public static CTGraphicalObjectData parse(final URL url) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(url, CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(url, CTGraphicalObjectData.type, xmlOptions);
        }
        
        public static CTGraphicalObjectData parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(inputStream, CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(inputStream, CTGraphicalObjectData.type, xmlOptions);
        }
        
        public static CTGraphicalObjectData parse(final Reader reader) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(reader, CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(reader, CTGraphicalObjectData.type, xmlOptions);
        }
        
        public static CTGraphicalObjectData parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGraphicalObjectData.type, xmlOptions);
        }
        
        public static CTGraphicalObjectData parse(final Node node) throws XmlException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(node, CTGraphicalObjectData.type, null);
        }
        
        public static CTGraphicalObjectData parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(node, CTGraphicalObjectData.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGraphicalObjectData parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGraphicalObjectData.type, null);
        }
        
        @Deprecated
        public static CTGraphicalObjectData parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGraphicalObjectData)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGraphicalObjectData.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGraphicalObjectData.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGraphicalObjectData.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
