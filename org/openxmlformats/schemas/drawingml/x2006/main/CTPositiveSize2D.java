// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPositiveSize2D extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPositiveSize2D.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpositivesize2d0147type");
    
    long getCx();
    
    STPositiveCoordinate xgetCx();
    
    void setCx(final long p0);
    
    void xsetCx(final STPositiveCoordinate p0);
    
    long getCy();
    
    STPositiveCoordinate xgetCy();
    
    void setCy(final long p0);
    
    void xsetCy(final STPositiveCoordinate p0);
    
    public static final class Factory
    {
        public static CTPositiveSize2D newInstance() {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().newInstance(CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D newInstance(final XmlOptions xmlOptions) {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().newInstance(CTPositiveSize2D.type, xmlOptions);
        }
        
        public static CTPositiveSize2D parse(final String s) throws XmlException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(s, CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(s, CTPositiveSize2D.type, xmlOptions);
        }
        
        public static CTPositiveSize2D parse(final File file) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(file, CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(file, CTPositiveSize2D.type, xmlOptions);
        }
        
        public static CTPositiveSize2D parse(final URL url) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(url, CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(url, CTPositiveSize2D.type, xmlOptions);
        }
        
        public static CTPositiveSize2D parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(inputStream, CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(inputStream, CTPositiveSize2D.type, xmlOptions);
        }
        
        public static CTPositiveSize2D parse(final Reader reader) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(reader, CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(reader, CTPositiveSize2D.type, xmlOptions);
        }
        
        public static CTPositiveSize2D parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPositiveSize2D.type, xmlOptions);
        }
        
        public static CTPositiveSize2D parse(final Node node) throws XmlException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(node, CTPositiveSize2D.type, null);
        }
        
        public static CTPositiveSize2D parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(node, CTPositiveSize2D.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPositiveSize2D parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPositiveSize2D.type, null);
        }
        
        @Deprecated
        public static CTPositiveSize2D parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPositiveSize2D)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPositiveSize2D.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPositiveSize2D.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPositiveSize2D.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
