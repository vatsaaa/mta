// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGeomRect extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGeomRect.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgeomrect53dbtype");
    
    Object getL();
    
    STAdjCoordinate xgetL();
    
    void setL(final Object p0);
    
    void xsetL(final STAdjCoordinate p0);
    
    Object getT();
    
    STAdjCoordinate xgetT();
    
    void setT(final Object p0);
    
    void xsetT(final STAdjCoordinate p0);
    
    Object getR();
    
    STAdjCoordinate xgetR();
    
    void setR(final Object p0);
    
    void xsetR(final STAdjCoordinate p0);
    
    Object getB();
    
    STAdjCoordinate xgetB();
    
    void setB(final Object p0);
    
    void xsetB(final STAdjCoordinate p0);
    
    public static final class Factory
    {
        public static CTGeomRect newInstance() {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().newInstance(CTGeomRect.type, null);
        }
        
        public static CTGeomRect newInstance(final XmlOptions xmlOptions) {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().newInstance(CTGeomRect.type, xmlOptions);
        }
        
        public static CTGeomRect parse(final String s) throws XmlException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(s, CTGeomRect.type, null);
        }
        
        public static CTGeomRect parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(s, CTGeomRect.type, xmlOptions);
        }
        
        public static CTGeomRect parse(final File file) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(file, CTGeomRect.type, null);
        }
        
        public static CTGeomRect parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(file, CTGeomRect.type, xmlOptions);
        }
        
        public static CTGeomRect parse(final URL url) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(url, CTGeomRect.type, null);
        }
        
        public static CTGeomRect parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(url, CTGeomRect.type, xmlOptions);
        }
        
        public static CTGeomRect parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(inputStream, CTGeomRect.type, null);
        }
        
        public static CTGeomRect parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(inputStream, CTGeomRect.type, xmlOptions);
        }
        
        public static CTGeomRect parse(final Reader reader) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(reader, CTGeomRect.type, null);
        }
        
        public static CTGeomRect parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(reader, CTGeomRect.type, xmlOptions);
        }
        
        public static CTGeomRect parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGeomRect.type, null);
        }
        
        public static CTGeomRect parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGeomRect.type, xmlOptions);
        }
        
        public static CTGeomRect parse(final Node node) throws XmlException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(node, CTGeomRect.type, null);
        }
        
        public static CTGeomRect parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(node, CTGeomRect.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGeomRect parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGeomRect.type, null);
        }
        
        @Deprecated
        public static CTGeomRect parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGeomRect)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGeomRect.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGeomRect.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGeomRect.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
