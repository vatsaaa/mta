// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlLong;

public interface STCoordinate extends XmlLong
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STCoordinate.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stcoordinatefae3type");
    
    public static final class Factory
    {
        public static STCoordinate newValue(final Object o) {
            return (STCoordinate)STCoordinate.type.newValue(o);
        }
        
        public static STCoordinate newInstance() {
            return (STCoordinate)XmlBeans.getContextTypeLoader().newInstance(STCoordinate.type, null);
        }
        
        public static STCoordinate newInstance(final XmlOptions xmlOptions) {
            return (STCoordinate)XmlBeans.getContextTypeLoader().newInstance(STCoordinate.type, xmlOptions);
        }
        
        public static STCoordinate parse(final String s) throws XmlException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(s, STCoordinate.type, null);
        }
        
        public static STCoordinate parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(s, STCoordinate.type, xmlOptions);
        }
        
        public static STCoordinate parse(final File file) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(file, STCoordinate.type, null);
        }
        
        public static STCoordinate parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(file, STCoordinate.type, xmlOptions);
        }
        
        public static STCoordinate parse(final URL url) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(url, STCoordinate.type, null);
        }
        
        public static STCoordinate parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(url, STCoordinate.type, xmlOptions);
        }
        
        public static STCoordinate parse(final InputStream inputStream) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(inputStream, STCoordinate.type, null);
        }
        
        public static STCoordinate parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(inputStream, STCoordinate.type, xmlOptions);
        }
        
        public static STCoordinate parse(final Reader reader) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(reader, STCoordinate.type, null);
        }
        
        public static STCoordinate parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(reader, STCoordinate.type, xmlOptions);
        }
        
        public static STCoordinate parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STCoordinate.type, null);
        }
        
        public static STCoordinate parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STCoordinate.type, xmlOptions);
        }
        
        public static STCoordinate parse(final Node node) throws XmlException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(node, STCoordinate.type, null);
        }
        
        public static STCoordinate parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(node, STCoordinate.type, xmlOptions);
        }
        
        @Deprecated
        public static STCoordinate parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STCoordinate.type, null);
        }
        
        @Deprecated
        public static STCoordinate parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STCoordinate)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STCoordinate.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STCoordinate.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STCoordinate.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
