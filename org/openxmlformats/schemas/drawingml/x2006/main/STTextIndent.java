// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STTextIndent extends STCoordinate32
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextIndent.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextindent16e4type");
    
    public static final class Factory
    {
        public static STTextIndent newValue(final Object o) {
            return (STTextIndent)STTextIndent.type.newValue(o);
        }
        
        public static STTextIndent newInstance() {
            return (STTextIndent)XmlBeans.getContextTypeLoader().newInstance(STTextIndent.type, null);
        }
        
        public static STTextIndent newInstance(final XmlOptions xmlOptions) {
            return (STTextIndent)XmlBeans.getContextTypeLoader().newInstance(STTextIndent.type, xmlOptions);
        }
        
        public static STTextIndent parse(final String s) throws XmlException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(s, STTextIndent.type, null);
        }
        
        public static STTextIndent parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(s, STTextIndent.type, xmlOptions);
        }
        
        public static STTextIndent parse(final File file) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(file, STTextIndent.type, null);
        }
        
        public static STTextIndent parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(file, STTextIndent.type, xmlOptions);
        }
        
        public static STTextIndent parse(final URL url) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(url, STTextIndent.type, null);
        }
        
        public static STTextIndent parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(url, STTextIndent.type, xmlOptions);
        }
        
        public static STTextIndent parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextIndent.type, null);
        }
        
        public static STTextIndent parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextIndent.type, xmlOptions);
        }
        
        public static STTextIndent parse(final Reader reader) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(reader, STTextIndent.type, null);
        }
        
        public static STTextIndent parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(reader, STTextIndent.type, xmlOptions);
        }
        
        public static STTextIndent parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextIndent.type, null);
        }
        
        public static STTextIndent parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextIndent.type, xmlOptions);
        }
        
        public static STTextIndent parse(final Node node) throws XmlException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(node, STTextIndent.type, null);
        }
        
        public static STTextIndent parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(node, STTextIndent.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextIndent parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextIndent.type, null);
        }
        
        @Deprecated
        public static STTextIndent parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextIndent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextIndent.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextIndent.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextIndent.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
