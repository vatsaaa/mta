// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTableProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTableProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttableproperties3512type");
    
    CTNoFillProperties getNoFill();
    
    boolean isSetNoFill();
    
    void setNoFill(final CTNoFillProperties p0);
    
    CTNoFillProperties addNewNoFill();
    
    void unsetNoFill();
    
    CTSolidColorFillProperties getSolidFill();
    
    boolean isSetSolidFill();
    
    void setSolidFill(final CTSolidColorFillProperties p0);
    
    CTSolidColorFillProperties addNewSolidFill();
    
    void unsetSolidFill();
    
    CTGradientFillProperties getGradFill();
    
    boolean isSetGradFill();
    
    void setGradFill(final CTGradientFillProperties p0);
    
    CTGradientFillProperties addNewGradFill();
    
    void unsetGradFill();
    
    CTBlipFillProperties getBlipFill();
    
    boolean isSetBlipFill();
    
    void setBlipFill(final CTBlipFillProperties p0);
    
    CTBlipFillProperties addNewBlipFill();
    
    void unsetBlipFill();
    
    CTPatternFillProperties getPattFill();
    
    boolean isSetPattFill();
    
    void setPattFill(final CTPatternFillProperties p0);
    
    CTPatternFillProperties addNewPattFill();
    
    void unsetPattFill();
    
    CTGroupFillProperties getGrpFill();
    
    boolean isSetGrpFill();
    
    void setGrpFill(final CTGroupFillProperties p0);
    
    CTGroupFillProperties addNewGrpFill();
    
    void unsetGrpFill();
    
    CTEffectList getEffectLst();
    
    boolean isSetEffectLst();
    
    void setEffectLst(final CTEffectList p0);
    
    CTEffectList addNewEffectLst();
    
    void unsetEffectLst();
    
    CTEffectContainer getEffectDag();
    
    boolean isSetEffectDag();
    
    void setEffectDag(final CTEffectContainer p0);
    
    CTEffectContainer addNewEffectDag();
    
    void unsetEffectDag();
    
    CTTableStyle getTableStyle();
    
    boolean isSetTableStyle();
    
    void setTableStyle(final CTTableStyle p0);
    
    CTTableStyle addNewTableStyle();
    
    void unsetTableStyle();
    
    String getTableStyleId();
    
    STGuid xgetTableStyleId();
    
    boolean isSetTableStyleId();
    
    void setTableStyleId(final String p0);
    
    void xsetTableStyleId(final STGuid p0);
    
    void unsetTableStyleId();
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    boolean getRtl();
    
    XmlBoolean xgetRtl();
    
    boolean isSetRtl();
    
    void setRtl(final boolean p0);
    
    void xsetRtl(final XmlBoolean p0);
    
    void unsetRtl();
    
    boolean getFirstRow();
    
    XmlBoolean xgetFirstRow();
    
    boolean isSetFirstRow();
    
    void setFirstRow(final boolean p0);
    
    void xsetFirstRow(final XmlBoolean p0);
    
    void unsetFirstRow();
    
    boolean getFirstCol();
    
    XmlBoolean xgetFirstCol();
    
    boolean isSetFirstCol();
    
    void setFirstCol(final boolean p0);
    
    void xsetFirstCol(final XmlBoolean p0);
    
    void unsetFirstCol();
    
    boolean getLastRow();
    
    XmlBoolean xgetLastRow();
    
    boolean isSetLastRow();
    
    void setLastRow(final boolean p0);
    
    void xsetLastRow(final XmlBoolean p0);
    
    void unsetLastRow();
    
    boolean getLastCol();
    
    XmlBoolean xgetLastCol();
    
    boolean isSetLastCol();
    
    void setLastCol(final boolean p0);
    
    void xsetLastCol(final XmlBoolean p0);
    
    void unsetLastCol();
    
    boolean getBandRow();
    
    XmlBoolean xgetBandRow();
    
    boolean isSetBandRow();
    
    void setBandRow(final boolean p0);
    
    void xsetBandRow(final XmlBoolean p0);
    
    void unsetBandRow();
    
    boolean getBandCol();
    
    XmlBoolean xgetBandCol();
    
    boolean isSetBandCol();
    
    void setBandCol(final boolean p0);
    
    void xsetBandCol(final XmlBoolean p0);
    
    void unsetBandCol();
    
    public static final class Factory
    {
        public static CTTableProperties newInstance() {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().newInstance(CTTableProperties.type, null);
        }
        
        public static CTTableProperties newInstance(final XmlOptions xmlOptions) {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().newInstance(CTTableProperties.type, xmlOptions);
        }
        
        public static CTTableProperties parse(final String s) throws XmlException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(s, CTTableProperties.type, null);
        }
        
        public static CTTableProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(s, CTTableProperties.type, xmlOptions);
        }
        
        public static CTTableProperties parse(final File file) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(file, CTTableProperties.type, null);
        }
        
        public static CTTableProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(file, CTTableProperties.type, xmlOptions);
        }
        
        public static CTTableProperties parse(final URL url) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(url, CTTableProperties.type, null);
        }
        
        public static CTTableProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(url, CTTableProperties.type, xmlOptions);
        }
        
        public static CTTableProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableProperties.type, null);
        }
        
        public static CTTableProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableProperties.type, xmlOptions);
        }
        
        public static CTTableProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(reader, CTTableProperties.type, null);
        }
        
        public static CTTableProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(reader, CTTableProperties.type, xmlOptions);
        }
        
        public static CTTableProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableProperties.type, null);
        }
        
        public static CTTableProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableProperties.type, xmlOptions);
        }
        
        public static CTTableProperties parse(final Node node) throws XmlException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(node, CTTableProperties.type, null);
        }
        
        public static CTTableProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(node, CTTableProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTableProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableProperties.type, null);
        }
        
        @Deprecated
        public static CTTableProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTableProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
