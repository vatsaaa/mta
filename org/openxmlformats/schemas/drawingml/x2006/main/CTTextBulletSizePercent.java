// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextBulletSizePercent extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextBulletSizePercent.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextbulletsizepercent9b26type");
    
    int getVal();
    
    STTextBulletSizePercent xgetVal();
    
    boolean isSetVal();
    
    void setVal(final int p0);
    
    void xsetVal(final STTextBulletSizePercent p0);
    
    void unsetVal();
    
    public static final class Factory
    {
        public static CTTextBulletSizePercent newInstance() {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().newInstance(CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent newInstance(final XmlOptions xmlOptions) {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().newInstance(CTTextBulletSizePercent.type, xmlOptions);
        }
        
        public static CTTextBulletSizePercent parse(final String s) throws XmlException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(s, CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(s, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        public static CTTextBulletSizePercent parse(final File file) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(file, CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(file, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        public static CTTextBulletSizePercent parse(final URL url) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(url, CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(url, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        public static CTTextBulletSizePercent parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        public static CTTextBulletSizePercent parse(final Reader reader) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(reader, CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(reader, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        public static CTTextBulletSizePercent parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        public static CTTextBulletSizePercent parse(final Node node) throws XmlException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(node, CTTextBulletSizePercent.type, null);
        }
        
        public static CTTextBulletSizePercent parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(node, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextBulletSizePercent parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextBulletSizePercent.type, null);
        }
        
        @Deprecated
        public static CTTextBulletSizePercent parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextBulletSizePercent.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextBulletSizePercent.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
