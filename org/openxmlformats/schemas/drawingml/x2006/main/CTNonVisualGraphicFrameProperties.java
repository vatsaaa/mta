// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNonVisualGraphicFrameProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNonVisualGraphicFrameProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnonvisualgraphicframeproperties43b6type");
    
    CTGraphicalObjectFrameLocking getGraphicFrameLocks();
    
    boolean isSetGraphicFrameLocks();
    
    void setGraphicFrameLocks(final CTGraphicalObjectFrameLocking p0);
    
    CTGraphicalObjectFrameLocking addNewGraphicFrameLocks();
    
    void unsetGraphicFrameLocks();
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTNonVisualGraphicFrameProperties newInstance() {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().newInstance(CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties newInstance(final XmlOptions xmlOptions) {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().newInstance(CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final String s) throws XmlException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(s, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(s, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final File file) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(file, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(file, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final URL url) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(url, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(url, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final Node node) throws XmlException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(node, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        public static CTNonVisualGraphicFrameProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(node, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNonVisualGraphicFrameProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        @Deprecated
        public static CTNonVisualGraphicFrameProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNonVisualGraphicFrameProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNonVisualGraphicFrameProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNonVisualGraphicFrameProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
