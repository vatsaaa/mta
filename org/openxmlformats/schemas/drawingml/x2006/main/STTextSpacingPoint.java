// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STTextSpacingPoint extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextSpacingPoint.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextspacingpointdd05type");
    
    public static final class Factory
    {
        public static STTextSpacingPoint newValue(final Object o) {
            return (STTextSpacingPoint)STTextSpacingPoint.type.newValue(o);
        }
        
        public static STTextSpacingPoint newInstance() {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().newInstance(STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint newInstance(final XmlOptions xmlOptions) {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().newInstance(STTextSpacingPoint.type, xmlOptions);
        }
        
        public static STTextSpacingPoint parse(final String s) throws XmlException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(s, STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(s, STTextSpacingPoint.type, xmlOptions);
        }
        
        public static STTextSpacingPoint parse(final File file) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(file, STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(file, STTextSpacingPoint.type, xmlOptions);
        }
        
        public static STTextSpacingPoint parse(final URL url) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(url, STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(url, STTextSpacingPoint.type, xmlOptions);
        }
        
        public static STTextSpacingPoint parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(inputStream, STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(inputStream, STTextSpacingPoint.type, xmlOptions);
        }
        
        public static STTextSpacingPoint parse(final Reader reader) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(reader, STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(reader, STTextSpacingPoint.type, xmlOptions);
        }
        
        public static STTextSpacingPoint parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextSpacingPoint.type, xmlOptions);
        }
        
        public static STTextSpacingPoint parse(final Node node) throws XmlException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(node, STTextSpacingPoint.type, null);
        }
        
        public static STTextSpacingPoint parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(node, STTextSpacingPoint.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextSpacingPoint parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextSpacingPoint.type, null);
        }
        
        @Deprecated
        public static STTextSpacingPoint parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextSpacingPoint)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextSpacingPoint.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextSpacingPoint.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextSpacingPoint.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
