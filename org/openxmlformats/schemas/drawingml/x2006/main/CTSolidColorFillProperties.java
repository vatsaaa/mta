// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSolidColorFillProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSolidColorFillProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsolidcolorfillproperties9cc9type");
    
    CTScRgbColor getScrgbClr();
    
    boolean isSetScrgbClr();
    
    void setScrgbClr(final CTScRgbColor p0);
    
    CTScRgbColor addNewScrgbClr();
    
    void unsetScrgbClr();
    
    CTSRgbColor getSrgbClr();
    
    boolean isSetSrgbClr();
    
    void setSrgbClr(final CTSRgbColor p0);
    
    CTSRgbColor addNewSrgbClr();
    
    void unsetSrgbClr();
    
    CTHslColor getHslClr();
    
    boolean isSetHslClr();
    
    void setHslClr(final CTHslColor p0);
    
    CTHslColor addNewHslClr();
    
    void unsetHslClr();
    
    CTSystemColor getSysClr();
    
    boolean isSetSysClr();
    
    void setSysClr(final CTSystemColor p0);
    
    CTSystemColor addNewSysClr();
    
    void unsetSysClr();
    
    CTSchemeColor getSchemeClr();
    
    boolean isSetSchemeClr();
    
    void setSchemeClr(final CTSchemeColor p0);
    
    CTSchemeColor addNewSchemeClr();
    
    void unsetSchemeClr();
    
    CTPresetColor getPrstClr();
    
    boolean isSetPrstClr();
    
    void setPrstClr(final CTPresetColor p0);
    
    CTPresetColor addNewPrstClr();
    
    void unsetPrstClr();
    
    public static final class Factory
    {
        public static CTSolidColorFillProperties newInstance() {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().newInstance(CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties newInstance(final XmlOptions xmlOptions) {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().newInstance(CTSolidColorFillProperties.type, xmlOptions);
        }
        
        public static CTSolidColorFillProperties parse(final String s) throws XmlException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(s, CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(s, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        public static CTSolidColorFillProperties parse(final File file) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(file, CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(file, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        public static CTSolidColorFillProperties parse(final URL url) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(url, CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(url, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        public static CTSolidColorFillProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        public static CTSolidColorFillProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(reader, CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(reader, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        public static CTSolidColorFillProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        public static CTSolidColorFillProperties parse(final Node node) throws XmlException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(node, CTSolidColorFillProperties.type, null);
        }
        
        public static CTSolidColorFillProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(node, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSolidColorFillProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSolidColorFillProperties.type, null);
        }
        
        @Deprecated
        public static CTSolidColorFillProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSolidColorFillProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSolidColorFillProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSolidColorFillProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
