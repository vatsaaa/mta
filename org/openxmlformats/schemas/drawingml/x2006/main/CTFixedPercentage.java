// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTFixedPercentage extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTFixedPercentage.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfixedpercentagea2dftype");
    
    int getVal();
    
    STFixedPercentage xgetVal();
    
    void setVal(final int p0);
    
    void xsetVal(final STFixedPercentage p0);
    
    public static final class Factory
    {
        public static CTFixedPercentage newInstance() {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage newInstance(final XmlOptions xmlOptions) {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(CTFixedPercentage.type, xmlOptions);
        }
        
        public static CTFixedPercentage parse(final String s) throws XmlException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, CTFixedPercentage.type, xmlOptions);
        }
        
        public static CTFixedPercentage parse(final File file) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, CTFixedPercentage.type, xmlOptions);
        }
        
        public static CTFixedPercentage parse(final URL url) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, CTFixedPercentage.type, xmlOptions);
        }
        
        public static CTFixedPercentage parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, CTFixedPercentage.type, xmlOptions);
        }
        
        public static CTFixedPercentage parse(final Reader reader) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, CTFixedPercentage.type, xmlOptions);
        }
        
        public static CTFixedPercentage parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFixedPercentage.type, xmlOptions);
        }
        
        public static CTFixedPercentage parse(final Node node) throws XmlException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, CTFixedPercentage.type, null);
        }
        
        public static CTFixedPercentage parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, CTFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static CTFixedPercentage parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFixedPercentage.type, null);
        }
        
        @Deprecated
        public static CTFixedPercentage parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFixedPercentage.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFixedPercentage.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
