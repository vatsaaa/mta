// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTableGrid extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTableGrid.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttablegrid69a5type");
    
    List<CTTableCol> getGridColList();
    
    @Deprecated
    CTTableCol[] getGridColArray();
    
    CTTableCol getGridColArray(final int p0);
    
    int sizeOfGridColArray();
    
    void setGridColArray(final CTTableCol[] p0);
    
    void setGridColArray(final int p0, final CTTableCol p1);
    
    CTTableCol insertNewGridCol(final int p0);
    
    CTTableCol addNewGridCol();
    
    void removeGridCol(final int p0);
    
    public static final class Factory
    {
        public static CTTableGrid newInstance() {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().newInstance(CTTableGrid.type, null);
        }
        
        public static CTTableGrid newInstance(final XmlOptions xmlOptions) {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().newInstance(CTTableGrid.type, xmlOptions);
        }
        
        public static CTTableGrid parse(final String s) throws XmlException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(s, CTTableGrid.type, null);
        }
        
        public static CTTableGrid parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(s, CTTableGrid.type, xmlOptions);
        }
        
        public static CTTableGrid parse(final File file) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(file, CTTableGrid.type, null);
        }
        
        public static CTTableGrid parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(file, CTTableGrid.type, xmlOptions);
        }
        
        public static CTTableGrid parse(final URL url) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(url, CTTableGrid.type, null);
        }
        
        public static CTTableGrid parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(url, CTTableGrid.type, xmlOptions);
        }
        
        public static CTTableGrid parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableGrid.type, null);
        }
        
        public static CTTableGrid parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableGrid.type, xmlOptions);
        }
        
        public static CTTableGrid parse(final Reader reader) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(reader, CTTableGrid.type, null);
        }
        
        public static CTTableGrid parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(reader, CTTableGrid.type, xmlOptions);
        }
        
        public static CTTableGrid parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableGrid.type, null);
        }
        
        public static CTTableGrid parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableGrid.type, xmlOptions);
        }
        
        public static CTTableGrid parse(final Node node) throws XmlException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(node, CTTableGrid.type, null);
        }
        
        public static CTTableGrid parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(node, CTTableGrid.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTableGrid parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableGrid.type, null);
        }
        
        @Deprecated
        public static CTTableGrid parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTableGrid)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableGrid.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableGrid.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableGrid.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
