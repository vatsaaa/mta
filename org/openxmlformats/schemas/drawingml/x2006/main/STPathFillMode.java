// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlToken;

public interface STPathFillMode extends XmlToken
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPathFillMode.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpathfillmode3cf6type");
    public static final Enum NONE = Enum.forString("none");
    public static final Enum NORM = Enum.forString("norm");
    public static final Enum LIGHTEN = Enum.forString("lighten");
    public static final Enum LIGHTEN_LESS = Enum.forString("lightenLess");
    public static final Enum DARKEN = Enum.forString("darken");
    public static final Enum DARKEN_LESS = Enum.forString("darkenLess");
    public static final int INT_NONE = 1;
    public static final int INT_NORM = 2;
    public static final int INT_LIGHTEN = 3;
    public static final int INT_LIGHTEN_LESS = 4;
    public static final int INT_DARKEN = 5;
    public static final int INT_DARKEN_LESS = 6;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STPathFillMode newValue(final Object o) {
            return (STPathFillMode)STPathFillMode.type.newValue(o);
        }
        
        public static STPathFillMode newInstance() {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().newInstance(STPathFillMode.type, null);
        }
        
        public static STPathFillMode newInstance(final XmlOptions xmlOptions) {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().newInstance(STPathFillMode.type, xmlOptions);
        }
        
        public static STPathFillMode parse(final String s) throws XmlException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(s, STPathFillMode.type, null);
        }
        
        public static STPathFillMode parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(s, STPathFillMode.type, xmlOptions);
        }
        
        public static STPathFillMode parse(final File file) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(file, STPathFillMode.type, null);
        }
        
        public static STPathFillMode parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(file, STPathFillMode.type, xmlOptions);
        }
        
        public static STPathFillMode parse(final URL url) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(url, STPathFillMode.type, null);
        }
        
        public static STPathFillMode parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(url, STPathFillMode.type, xmlOptions);
        }
        
        public static STPathFillMode parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(inputStream, STPathFillMode.type, null);
        }
        
        public static STPathFillMode parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(inputStream, STPathFillMode.type, xmlOptions);
        }
        
        public static STPathFillMode parse(final Reader reader) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(reader, STPathFillMode.type, null);
        }
        
        public static STPathFillMode parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(reader, STPathFillMode.type, xmlOptions);
        }
        
        public static STPathFillMode parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPathFillMode.type, null);
        }
        
        public static STPathFillMode parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPathFillMode.type, xmlOptions);
        }
        
        public static STPathFillMode parse(final Node node) throws XmlException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(node, STPathFillMode.type, null);
        }
        
        public static STPathFillMode parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(node, STPathFillMode.type, xmlOptions);
        }
        
        @Deprecated
        public static STPathFillMode parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPathFillMode.type, null);
        }
        
        @Deprecated
        public static STPathFillMode parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPathFillMode)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPathFillMode.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPathFillMode.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPathFillMode.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_NONE = 1;
        static final int INT_NORM = 2;
        static final int INT_LIGHTEN = 3;
        static final int INT_LIGHTEN_LESS = 4;
        static final int INT_DARKEN = 5;
        static final int INT_DARKEN_LESS = 6;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("none", 1), new Enum("norm", 2), new Enum("lighten", 3), new Enum("lightenLess", 4), new Enum("darken", 5), new Enum("darkenLess", 6) });
        }
    }
}
