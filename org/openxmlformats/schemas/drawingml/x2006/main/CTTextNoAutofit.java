// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextNoAutofit extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextNoAutofit.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextnoautofit1045type");
    
    public static final class Factory
    {
        public static CTTextNoAutofit newInstance() {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().newInstance(CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit newInstance(final XmlOptions xmlOptions) {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().newInstance(CTTextNoAutofit.type, xmlOptions);
        }
        
        public static CTTextNoAutofit parse(final String s) throws XmlException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(s, CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(s, CTTextNoAutofit.type, xmlOptions);
        }
        
        public static CTTextNoAutofit parse(final File file) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(file, CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(file, CTTextNoAutofit.type, xmlOptions);
        }
        
        public static CTTextNoAutofit parse(final URL url) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(url, CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(url, CTTextNoAutofit.type, xmlOptions);
        }
        
        public static CTTextNoAutofit parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextNoAutofit.type, xmlOptions);
        }
        
        public static CTTextNoAutofit parse(final Reader reader) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(reader, CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(reader, CTTextNoAutofit.type, xmlOptions);
        }
        
        public static CTTextNoAutofit parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextNoAutofit.type, xmlOptions);
        }
        
        public static CTTextNoAutofit parse(final Node node) throws XmlException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(node, CTTextNoAutofit.type, null);
        }
        
        public static CTTextNoAutofit parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(node, CTTextNoAutofit.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextNoAutofit parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextNoAutofit.type, null);
        }
        
        @Deprecated
        public static CTTextNoAutofit parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextNoAutofit)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextNoAutofit.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextNoAutofit.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextNoAutofit.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
