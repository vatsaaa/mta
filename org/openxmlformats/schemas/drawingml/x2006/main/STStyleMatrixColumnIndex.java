// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STStyleMatrixColumnIndex extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STStyleMatrixColumnIndex.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ststylematrixcolumnindex1755type");
    
    public static final class Factory
    {
        public static STStyleMatrixColumnIndex newValue(final Object o) {
            return (STStyleMatrixColumnIndex)STStyleMatrixColumnIndex.type.newValue(o);
        }
        
        public static STStyleMatrixColumnIndex newInstance() {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().newInstance(STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex newInstance(final XmlOptions xmlOptions) {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().newInstance(STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        public static STStyleMatrixColumnIndex parse(final String s) throws XmlException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(s, STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(s, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        public static STStyleMatrixColumnIndex parse(final File file) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(file, STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(file, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        public static STStyleMatrixColumnIndex parse(final URL url) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(url, STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(url, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        public static STStyleMatrixColumnIndex parse(final InputStream inputStream) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(inputStream, STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(inputStream, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        public static STStyleMatrixColumnIndex parse(final Reader reader) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(reader, STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(reader, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        public static STStyleMatrixColumnIndex parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        public static STStyleMatrixColumnIndex parse(final Node node) throws XmlException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(node, STStyleMatrixColumnIndex.type, null);
        }
        
        public static STStyleMatrixColumnIndex parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(node, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        @Deprecated
        public static STStyleMatrixColumnIndex parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STStyleMatrixColumnIndex.type, null);
        }
        
        @Deprecated
        public static STStyleMatrixColumnIndex parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STStyleMatrixColumnIndex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STStyleMatrixColumnIndex.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STStyleMatrixColumnIndex.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
