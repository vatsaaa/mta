// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextBulletSizePoint extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextBulletSizePoint.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextbulletsizepointe4f1type");
    
    int getVal();
    
    STTextFontSize xgetVal();
    
    boolean isSetVal();
    
    void setVal(final int p0);
    
    void xsetVal(final STTextFontSize p0);
    
    void unsetVal();
    
    public static final class Factory
    {
        public static CTTextBulletSizePoint newInstance() {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().newInstance(CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint newInstance(final XmlOptions xmlOptions) {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().newInstance(CTTextBulletSizePoint.type, xmlOptions);
        }
        
        public static CTTextBulletSizePoint parse(final String s) throws XmlException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(s, CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(s, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        public static CTTextBulletSizePoint parse(final File file) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(file, CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(file, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        public static CTTextBulletSizePoint parse(final URL url) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(url, CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(url, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        public static CTTextBulletSizePoint parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        public static CTTextBulletSizePoint parse(final Reader reader) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(reader, CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(reader, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        public static CTTextBulletSizePoint parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        public static CTTextBulletSizePoint parse(final Node node) throws XmlException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(node, CTTextBulletSizePoint.type, null);
        }
        
        public static CTTextBulletSizePoint parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(node, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextBulletSizePoint parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextBulletSizePoint.type, null);
        }
        
        @Deprecated
        public static CTTextBulletSizePoint parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextBulletSizePoint)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextBulletSizePoint.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextBulletSizePoint.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
