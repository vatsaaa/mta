// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlToken;

public interface STLineCap extends XmlToken
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLineCap.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlinecapcddftype");
    public static final Enum RND = Enum.forString("rnd");
    public static final Enum SQ = Enum.forString("sq");
    public static final Enum FLAT = Enum.forString("flat");
    public static final int INT_RND = 1;
    public static final int INT_SQ = 2;
    public static final int INT_FLAT = 3;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STLineCap newValue(final Object o) {
            return (STLineCap)STLineCap.type.newValue(o);
        }
        
        public static STLineCap newInstance() {
            return (STLineCap)XmlBeans.getContextTypeLoader().newInstance(STLineCap.type, null);
        }
        
        public static STLineCap newInstance(final XmlOptions xmlOptions) {
            return (STLineCap)XmlBeans.getContextTypeLoader().newInstance(STLineCap.type, xmlOptions);
        }
        
        public static STLineCap parse(final String s) throws XmlException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(s, STLineCap.type, null);
        }
        
        public static STLineCap parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(s, STLineCap.type, xmlOptions);
        }
        
        public static STLineCap parse(final File file) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(file, STLineCap.type, null);
        }
        
        public static STLineCap parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(file, STLineCap.type, xmlOptions);
        }
        
        public static STLineCap parse(final URL url) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(url, STLineCap.type, null);
        }
        
        public static STLineCap parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(url, STLineCap.type, xmlOptions);
        }
        
        public static STLineCap parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(inputStream, STLineCap.type, null);
        }
        
        public static STLineCap parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(inputStream, STLineCap.type, xmlOptions);
        }
        
        public static STLineCap parse(final Reader reader) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(reader, STLineCap.type, null);
        }
        
        public static STLineCap parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(reader, STLineCap.type, xmlOptions);
        }
        
        public static STLineCap parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineCap.type, null);
        }
        
        public static STLineCap parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineCap.type, xmlOptions);
        }
        
        public static STLineCap parse(final Node node) throws XmlException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(node, STLineCap.type, null);
        }
        
        public static STLineCap parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(node, STLineCap.type, xmlOptions);
        }
        
        @Deprecated
        public static STLineCap parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineCap.type, null);
        }
        
        @Deprecated
        public static STLineCap parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLineCap)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineCap.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineCap.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineCap.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_RND = 1;
        static final int INT_SQ = 2;
        static final int INT_FLAT = 3;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("rnd", 1), new Enum("sq", 2), new Enum("flat", 3) });
        }
    }
}
