// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNoFillProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNoFillProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnofillpropertiesbf92type");
    
    public static final class Factory
    {
        public static CTNoFillProperties newInstance() {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().newInstance(CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties newInstance(final XmlOptions xmlOptions) {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().newInstance(CTNoFillProperties.type, xmlOptions);
        }
        
        public static CTNoFillProperties parse(final String s) throws XmlException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(s, CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(s, CTNoFillProperties.type, xmlOptions);
        }
        
        public static CTNoFillProperties parse(final File file) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(file, CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(file, CTNoFillProperties.type, xmlOptions);
        }
        
        public static CTNoFillProperties parse(final URL url) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(url, CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(url, CTNoFillProperties.type, xmlOptions);
        }
        
        public static CTNoFillProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNoFillProperties.type, xmlOptions);
        }
        
        public static CTNoFillProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNoFillProperties.type, xmlOptions);
        }
        
        public static CTNoFillProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNoFillProperties.type, xmlOptions);
        }
        
        public static CTNoFillProperties parse(final Node node) throws XmlException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(node, CTNoFillProperties.type, null);
        }
        
        public static CTNoFillProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(node, CTNoFillProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNoFillProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNoFillProperties.type, null);
        }
        
        @Deprecated
        public static CTNoFillProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNoFillProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNoFillProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNoFillProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNoFillProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
