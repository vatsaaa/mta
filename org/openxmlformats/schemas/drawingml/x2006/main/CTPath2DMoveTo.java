// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPath2DMoveTo extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPath2DMoveTo.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpath2dmovetoa01etype");
    
    CTAdjPoint2D getPt();
    
    void setPt(final CTAdjPoint2D p0);
    
    CTAdjPoint2D addNewPt();
    
    public static final class Factory
    {
        public static CTPath2DMoveTo newInstance() {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().newInstance(CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo newInstance(final XmlOptions xmlOptions) {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().newInstance(CTPath2DMoveTo.type, xmlOptions);
        }
        
        public static CTPath2DMoveTo parse(final String s) throws XmlException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(s, CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(s, CTPath2DMoveTo.type, xmlOptions);
        }
        
        public static CTPath2DMoveTo parse(final File file) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(file, CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(file, CTPath2DMoveTo.type, xmlOptions);
        }
        
        public static CTPath2DMoveTo parse(final URL url) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(url, CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(url, CTPath2DMoveTo.type, xmlOptions);
        }
        
        public static CTPath2DMoveTo parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(inputStream, CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(inputStream, CTPath2DMoveTo.type, xmlOptions);
        }
        
        public static CTPath2DMoveTo parse(final Reader reader) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(reader, CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(reader, CTPath2DMoveTo.type, xmlOptions);
        }
        
        public static CTPath2DMoveTo parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPath2DMoveTo.type, xmlOptions);
        }
        
        public static CTPath2DMoveTo parse(final Node node) throws XmlException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(node, CTPath2DMoveTo.type, null);
        }
        
        public static CTPath2DMoveTo parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(node, CTPath2DMoveTo.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPath2DMoveTo parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPath2DMoveTo.type, null);
        }
        
        @Deprecated
        public static CTPath2DMoveTo parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPath2DMoveTo)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPath2DMoveTo.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPath2DMoveTo.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPath2DMoveTo.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
