// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTRegularTextRun extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTRegularTextRun.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctregulartextrun7e3dtype");
    
    CTTextCharacterProperties getRPr();
    
    boolean isSetRPr();
    
    void setRPr(final CTTextCharacterProperties p0);
    
    CTTextCharacterProperties addNewRPr();
    
    void unsetRPr();
    
    String getT();
    
    XmlString xgetT();
    
    void setT(final String p0);
    
    void xsetT(final XmlString p0);
    
    public static final class Factory
    {
        public static CTRegularTextRun newInstance() {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().newInstance(CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun newInstance(final XmlOptions xmlOptions) {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().newInstance(CTRegularTextRun.type, xmlOptions);
        }
        
        public static CTRegularTextRun parse(final String s) throws XmlException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(s, CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(s, CTRegularTextRun.type, xmlOptions);
        }
        
        public static CTRegularTextRun parse(final File file) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(file, CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(file, CTRegularTextRun.type, xmlOptions);
        }
        
        public static CTRegularTextRun parse(final URL url) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(url, CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(url, CTRegularTextRun.type, xmlOptions);
        }
        
        public static CTRegularTextRun parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(inputStream, CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(inputStream, CTRegularTextRun.type, xmlOptions);
        }
        
        public static CTRegularTextRun parse(final Reader reader) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(reader, CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(reader, CTRegularTextRun.type, xmlOptions);
        }
        
        public static CTRegularTextRun parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTRegularTextRun.type, xmlOptions);
        }
        
        public static CTRegularTextRun parse(final Node node) throws XmlException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(node, CTRegularTextRun.type, null);
        }
        
        public static CTRegularTextRun parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(node, CTRegularTextRun.type, xmlOptions);
        }
        
        @Deprecated
        public static CTRegularTextRun parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTRegularTextRun.type, null);
        }
        
        @Deprecated
        public static CTRegularTextRun parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTRegularTextRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTRegularTextRun.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTRegularTextRun.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTRegularTextRun.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
