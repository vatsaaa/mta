// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTGeomGuideList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTGeomGuideList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctgeomguidelist364ftype");
    
    List<CTGeomGuide> getGdList();
    
    @Deprecated
    CTGeomGuide[] getGdArray();
    
    CTGeomGuide getGdArray(final int p0);
    
    int sizeOfGdArray();
    
    void setGdArray(final CTGeomGuide[] p0);
    
    void setGdArray(final int p0, final CTGeomGuide p1);
    
    CTGeomGuide insertNewGd(final int p0);
    
    CTGeomGuide addNewGd();
    
    void removeGd(final int p0);
    
    public static final class Factory
    {
        public static CTGeomGuideList newInstance() {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().newInstance(CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList newInstance(final XmlOptions xmlOptions) {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().newInstance(CTGeomGuideList.type, xmlOptions);
        }
        
        public static CTGeomGuideList parse(final String s) throws XmlException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(s, CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(s, CTGeomGuideList.type, xmlOptions);
        }
        
        public static CTGeomGuideList parse(final File file) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(file, CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(file, CTGeomGuideList.type, xmlOptions);
        }
        
        public static CTGeomGuideList parse(final URL url) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(url, CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(url, CTGeomGuideList.type, xmlOptions);
        }
        
        public static CTGeomGuideList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(inputStream, CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(inputStream, CTGeomGuideList.type, xmlOptions);
        }
        
        public static CTGeomGuideList parse(final Reader reader) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(reader, CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(reader, CTGeomGuideList.type, xmlOptions);
        }
        
        public static CTGeomGuideList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTGeomGuideList.type, xmlOptions);
        }
        
        public static CTGeomGuideList parse(final Node node) throws XmlException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(node, CTGeomGuideList.type, null);
        }
        
        public static CTGeomGuideList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(node, CTGeomGuideList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTGeomGuideList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGeomGuideList.type, null);
        }
        
        @Deprecated
        public static CTGeomGuideList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTGeomGuideList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTGeomGuideList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGeomGuideList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTGeomGuideList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
