// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STPositiveFixedPercentage extends STPercentage
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPositiveFixedPercentage.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpositivefixedpercentagee756type");
    
    public static final class Factory
    {
        public static STPositiveFixedPercentage newValue(final Object o) {
            return (STPositiveFixedPercentage)STPositiveFixedPercentage.type.newValue(o);
        }
        
        public static STPositiveFixedPercentage newInstance() {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage newInstance(final XmlOptions xmlOptions) {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(STPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static STPositiveFixedPercentage parse(final String s) throws XmlException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static STPositiveFixedPercentage parse(final File file) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static STPositiveFixedPercentage parse(final URL url) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static STPositiveFixedPercentage parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static STPositiveFixedPercentage parse(final Reader reader) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static STPositiveFixedPercentage parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static STPositiveFixedPercentage parse(final Node node) throws XmlException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, STPositiveFixedPercentage.type, null);
        }
        
        public static STPositiveFixedPercentage parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static STPositiveFixedPercentage parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveFixedPercentage.type, null);
        }
        
        @Deprecated
        public static STPositiveFixedPercentage parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveFixedPercentage.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveFixedPercentage.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
