// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPercentage extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPercentage.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpercentage4e75type");
    
    int getVal();
    
    STPercentage xgetVal();
    
    void setVal(final int p0);
    
    void xsetVal(final STPercentage p0);
    
    public static final class Factory
    {
        public static CTPercentage newInstance() {
            return (CTPercentage)XmlBeans.getContextTypeLoader().newInstance(CTPercentage.type, null);
        }
        
        public static CTPercentage newInstance(final XmlOptions xmlOptions) {
            return (CTPercentage)XmlBeans.getContextTypeLoader().newInstance(CTPercentage.type, xmlOptions);
        }
        
        public static CTPercentage parse(final String s) throws XmlException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(s, CTPercentage.type, null);
        }
        
        public static CTPercentage parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(s, CTPercentage.type, xmlOptions);
        }
        
        public static CTPercentage parse(final File file) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(file, CTPercentage.type, null);
        }
        
        public static CTPercentage parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(file, CTPercentage.type, xmlOptions);
        }
        
        public static CTPercentage parse(final URL url) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(url, CTPercentage.type, null);
        }
        
        public static CTPercentage parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(url, CTPercentage.type, xmlOptions);
        }
        
        public static CTPercentage parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, CTPercentage.type, null);
        }
        
        public static CTPercentage parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, CTPercentage.type, xmlOptions);
        }
        
        public static CTPercentage parse(final Reader reader) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(reader, CTPercentage.type, null);
        }
        
        public static CTPercentage parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(reader, CTPercentage.type, xmlOptions);
        }
        
        public static CTPercentage parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPercentage.type, null);
        }
        
        public static CTPercentage parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPercentage.type, xmlOptions);
        }
        
        public static CTPercentage parse(final Node node) throws XmlException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(node, CTPercentage.type, null);
        }
        
        public static CTPercentage parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(node, CTPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPercentage parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPercentage.type, null);
        }
        
        @Deprecated
        public static CTPercentage parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPercentage.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPercentage.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
