// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STTextPoint extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextPoint.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextpoint4284type");
    
    public static final class Factory
    {
        public static STTextPoint newValue(final Object o) {
            return (STTextPoint)STTextPoint.type.newValue(o);
        }
        
        public static STTextPoint newInstance() {
            return (STTextPoint)XmlBeans.getContextTypeLoader().newInstance(STTextPoint.type, null);
        }
        
        public static STTextPoint newInstance(final XmlOptions xmlOptions) {
            return (STTextPoint)XmlBeans.getContextTypeLoader().newInstance(STTextPoint.type, xmlOptions);
        }
        
        public static STTextPoint parse(final String s) throws XmlException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(s, STTextPoint.type, null);
        }
        
        public static STTextPoint parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(s, STTextPoint.type, xmlOptions);
        }
        
        public static STTextPoint parse(final File file) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(file, STTextPoint.type, null);
        }
        
        public static STTextPoint parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(file, STTextPoint.type, xmlOptions);
        }
        
        public static STTextPoint parse(final URL url) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(url, STTextPoint.type, null);
        }
        
        public static STTextPoint parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(url, STTextPoint.type, xmlOptions);
        }
        
        public static STTextPoint parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(inputStream, STTextPoint.type, null);
        }
        
        public static STTextPoint parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(inputStream, STTextPoint.type, xmlOptions);
        }
        
        public static STTextPoint parse(final Reader reader) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(reader, STTextPoint.type, null);
        }
        
        public static STTextPoint parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(reader, STTextPoint.type, xmlOptions);
        }
        
        public static STTextPoint parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextPoint.type, null);
        }
        
        public static STTextPoint parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextPoint.type, xmlOptions);
        }
        
        public static STTextPoint parse(final Node node) throws XmlException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(node, STTextPoint.type, null);
        }
        
        public static STTextPoint parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(node, STTextPoint.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextPoint parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextPoint.type, null);
        }
        
        @Deprecated
        public static STTextPoint parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextPoint)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextPoint.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextPoint.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextPoint.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
