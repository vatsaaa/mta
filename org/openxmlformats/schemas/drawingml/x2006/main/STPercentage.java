// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STPercentage extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPercentage.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpercentage0a85type");
    
    public static final class Factory
    {
        public static STPercentage newValue(final Object o) {
            return (STPercentage)STPercentage.type.newValue(o);
        }
        
        public static STPercentage newInstance() {
            return (STPercentage)XmlBeans.getContextTypeLoader().newInstance(STPercentage.type, null);
        }
        
        public static STPercentage newInstance(final XmlOptions xmlOptions) {
            return (STPercentage)XmlBeans.getContextTypeLoader().newInstance(STPercentage.type, xmlOptions);
        }
        
        public static STPercentage parse(final String s) throws XmlException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(s, STPercentage.type, null);
        }
        
        public static STPercentage parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(s, STPercentage.type, xmlOptions);
        }
        
        public static STPercentage parse(final File file) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(file, STPercentage.type, null);
        }
        
        public static STPercentage parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(file, STPercentage.type, xmlOptions);
        }
        
        public static STPercentage parse(final URL url) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(url, STPercentage.type, null);
        }
        
        public static STPercentage parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(url, STPercentage.type, xmlOptions);
        }
        
        public static STPercentage parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STPercentage.type, null);
        }
        
        public static STPercentage parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STPercentage.type, xmlOptions);
        }
        
        public static STPercentage parse(final Reader reader) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(reader, STPercentage.type, null);
        }
        
        public static STPercentage parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(reader, STPercentage.type, xmlOptions);
        }
        
        public static STPercentage parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPercentage.type, null);
        }
        
        public static STPercentage parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPercentage.type, xmlOptions);
        }
        
        public static STPercentage parse(final Node node) throws XmlException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(node, STPercentage.type, null);
        }
        
        public static STPercentage parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(node, STPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static STPercentage parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPercentage.type, null);
        }
        
        @Deprecated
        public static STPercentage parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPercentage.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPercentage.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
