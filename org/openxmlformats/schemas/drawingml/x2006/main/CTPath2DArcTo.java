// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPath2DArcTo extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPath2DArcTo.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpath2darctodaa7type");
    
    Object getWR();
    
    STAdjCoordinate xgetWR();
    
    void setWR(final Object p0);
    
    void xsetWR(final STAdjCoordinate p0);
    
    Object getHR();
    
    STAdjCoordinate xgetHR();
    
    void setHR(final Object p0);
    
    void xsetHR(final STAdjCoordinate p0);
    
    Object getStAng();
    
    STAdjAngle xgetStAng();
    
    void setStAng(final Object p0);
    
    void xsetStAng(final STAdjAngle p0);
    
    Object getSwAng();
    
    STAdjAngle xgetSwAng();
    
    void setSwAng(final Object p0);
    
    void xsetSwAng(final STAdjAngle p0);
    
    public static final class Factory
    {
        public static CTPath2DArcTo newInstance() {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().newInstance(CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo newInstance(final XmlOptions xmlOptions) {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().newInstance(CTPath2DArcTo.type, xmlOptions);
        }
        
        public static CTPath2DArcTo parse(final String s) throws XmlException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(s, CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(s, CTPath2DArcTo.type, xmlOptions);
        }
        
        public static CTPath2DArcTo parse(final File file) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(file, CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(file, CTPath2DArcTo.type, xmlOptions);
        }
        
        public static CTPath2DArcTo parse(final URL url) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(url, CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(url, CTPath2DArcTo.type, xmlOptions);
        }
        
        public static CTPath2DArcTo parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(inputStream, CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(inputStream, CTPath2DArcTo.type, xmlOptions);
        }
        
        public static CTPath2DArcTo parse(final Reader reader) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(reader, CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(reader, CTPath2DArcTo.type, xmlOptions);
        }
        
        public static CTPath2DArcTo parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPath2DArcTo.type, xmlOptions);
        }
        
        public static CTPath2DArcTo parse(final Node node) throws XmlException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(node, CTPath2DArcTo.type, null);
        }
        
        public static CTPath2DArcTo parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(node, CTPath2DArcTo.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPath2DArcTo parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPath2DArcTo.type, null);
        }
        
        @Deprecated
        public static CTPath2DArcTo parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPath2DArcTo)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPath2DArcTo.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPath2DArcTo.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPath2DArcTo.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
