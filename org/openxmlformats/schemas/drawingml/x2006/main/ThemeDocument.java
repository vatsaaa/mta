// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface ThemeDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(ThemeDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("themefd26doctype");
    
    CTOfficeStyleSheet getTheme();
    
    void setTheme(final CTOfficeStyleSheet p0);
    
    CTOfficeStyleSheet addNewTheme();
    
    public static final class Factory
    {
        public static ThemeDocument newInstance() {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().newInstance(ThemeDocument.type, null);
        }
        
        public static ThemeDocument newInstance(final XmlOptions xmlOptions) {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().newInstance(ThemeDocument.type, xmlOptions);
        }
        
        public static ThemeDocument parse(final String s) throws XmlException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(s, ThemeDocument.type, null);
        }
        
        public static ThemeDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(s, ThemeDocument.type, xmlOptions);
        }
        
        public static ThemeDocument parse(final File file) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(file, ThemeDocument.type, null);
        }
        
        public static ThemeDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(file, ThemeDocument.type, xmlOptions);
        }
        
        public static ThemeDocument parse(final URL url) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(url, ThemeDocument.type, null);
        }
        
        public static ThemeDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(url, ThemeDocument.type, xmlOptions);
        }
        
        public static ThemeDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(inputStream, ThemeDocument.type, null);
        }
        
        public static ThemeDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(inputStream, ThemeDocument.type, xmlOptions);
        }
        
        public static ThemeDocument parse(final Reader reader) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(reader, ThemeDocument.type, null);
        }
        
        public static ThemeDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(reader, ThemeDocument.type, xmlOptions);
        }
        
        public static ThemeDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, ThemeDocument.type, null);
        }
        
        public static ThemeDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, ThemeDocument.type, xmlOptions);
        }
        
        public static ThemeDocument parse(final Node node) throws XmlException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(node, ThemeDocument.type, null);
        }
        
        public static ThemeDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(node, ThemeDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static ThemeDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, ThemeDocument.type, null);
        }
        
        @Deprecated
        public static ThemeDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (ThemeDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, ThemeDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, ThemeDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, ThemeDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
