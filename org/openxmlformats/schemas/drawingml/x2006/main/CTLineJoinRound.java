// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTLineJoinRound extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTLineJoinRound.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctlinejoinround7be1type");
    
    public static final class Factory
    {
        public static CTLineJoinRound newInstance() {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().newInstance(CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound newInstance(final XmlOptions xmlOptions) {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().newInstance(CTLineJoinRound.type, xmlOptions);
        }
        
        public static CTLineJoinRound parse(final String s) throws XmlException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(s, CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(s, CTLineJoinRound.type, xmlOptions);
        }
        
        public static CTLineJoinRound parse(final File file) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(file, CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(file, CTLineJoinRound.type, xmlOptions);
        }
        
        public static CTLineJoinRound parse(final URL url) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(url, CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(url, CTLineJoinRound.type, xmlOptions);
        }
        
        public static CTLineJoinRound parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(inputStream, CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(inputStream, CTLineJoinRound.type, xmlOptions);
        }
        
        public static CTLineJoinRound parse(final Reader reader) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(reader, CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(reader, CTLineJoinRound.type, xmlOptions);
        }
        
        public static CTLineJoinRound parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLineJoinRound.type, xmlOptions);
        }
        
        public static CTLineJoinRound parse(final Node node) throws XmlException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(node, CTLineJoinRound.type, null);
        }
        
        public static CTLineJoinRound parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(node, CTLineJoinRound.type, xmlOptions);
        }
        
        @Deprecated
        public static CTLineJoinRound parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLineJoinRound.type, null);
        }
        
        @Deprecated
        public static CTLineJoinRound parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTLineJoinRound)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLineJoinRound.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLineJoinRound.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLineJoinRound.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
