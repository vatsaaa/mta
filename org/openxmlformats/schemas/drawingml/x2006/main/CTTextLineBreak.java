// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextLineBreak extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextLineBreak.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextlinebreak932ftype");
    
    CTTextCharacterProperties getRPr();
    
    boolean isSetRPr();
    
    void setRPr(final CTTextCharacterProperties p0);
    
    CTTextCharacterProperties addNewRPr();
    
    void unsetRPr();
    
    public static final class Factory
    {
        public static CTTextLineBreak newInstance() {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().newInstance(CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak newInstance(final XmlOptions xmlOptions) {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().newInstance(CTTextLineBreak.type, xmlOptions);
        }
        
        public static CTTextLineBreak parse(final String s) throws XmlException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(s, CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(s, CTTextLineBreak.type, xmlOptions);
        }
        
        public static CTTextLineBreak parse(final File file) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(file, CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(file, CTTextLineBreak.type, xmlOptions);
        }
        
        public static CTTextLineBreak parse(final URL url) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(url, CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(url, CTTextLineBreak.type, xmlOptions);
        }
        
        public static CTTextLineBreak parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextLineBreak.type, xmlOptions);
        }
        
        public static CTTextLineBreak parse(final Reader reader) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(reader, CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(reader, CTTextLineBreak.type, xmlOptions);
        }
        
        public static CTTextLineBreak parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextLineBreak.type, xmlOptions);
        }
        
        public static CTTextLineBreak parse(final Node node) throws XmlException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(node, CTTextLineBreak.type, null);
        }
        
        public static CTTextLineBreak parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(node, CTTextLineBreak.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextLineBreak parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextLineBreak.type, null);
        }
        
        @Deprecated
        public static CTTextLineBreak parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextLineBreak)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextLineBreak.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextLineBreak.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextLineBreak.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
