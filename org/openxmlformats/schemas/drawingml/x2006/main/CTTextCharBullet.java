// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextCharBullet extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextCharBullet.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextcharbullet3c20type");
    
    String getChar();
    
    XmlString xgetChar();
    
    void setChar(final String p0);
    
    void xsetChar(final XmlString p0);
    
    public static final class Factory
    {
        public static CTTextCharBullet newInstance() {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().newInstance(CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet newInstance(final XmlOptions xmlOptions) {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().newInstance(CTTextCharBullet.type, xmlOptions);
        }
        
        public static CTTextCharBullet parse(final String s) throws XmlException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(s, CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(s, CTTextCharBullet.type, xmlOptions);
        }
        
        public static CTTextCharBullet parse(final File file) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(file, CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(file, CTTextCharBullet.type, xmlOptions);
        }
        
        public static CTTextCharBullet parse(final URL url) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(url, CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(url, CTTextCharBullet.type, xmlOptions);
        }
        
        public static CTTextCharBullet parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextCharBullet.type, xmlOptions);
        }
        
        public static CTTextCharBullet parse(final Reader reader) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(reader, CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(reader, CTTextCharBullet.type, xmlOptions);
        }
        
        public static CTTextCharBullet parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextCharBullet.type, xmlOptions);
        }
        
        public static CTTextCharBullet parse(final Node node) throws XmlException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(node, CTTextCharBullet.type, null);
        }
        
        public static CTTextCharBullet parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(node, CTTextCharBullet.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextCharBullet parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextCharBullet.type, null);
        }
        
        @Deprecated
        public static CTTextCharBullet parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextCharBullet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextCharBullet.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextCharBullet.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextCharBullet.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
