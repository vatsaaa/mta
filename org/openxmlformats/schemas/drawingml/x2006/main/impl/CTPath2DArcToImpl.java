// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.openxmlformats.schemas.drawingml.x2006.main.STAdjAngle;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.STAdjCoordinate;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DArcTo;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTPath2DArcToImpl extends XmlComplexContentImpl implements CTPath2DArcTo
{
    private static final QName WR$0;
    private static final QName HR$2;
    private static final QName STANG$4;
    private static final QName SWANG$6;
    
    public CTPath2DArcToImpl(final SchemaType type) {
        super(type);
    }
    
    public Object getWR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.WR$0);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getObjectValue();
        }
    }
    
    public STAdjCoordinate xgetWR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STAdjCoordinate)this.get_store().find_attribute_user(CTPath2DArcToImpl.WR$0);
        }
    }
    
    public void setWR(final Object objectValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.WR$0);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTPath2DArcToImpl.WR$0);
            }
            simpleValue.setObjectValue(objectValue);
        }
    }
    
    public void xsetWR(final STAdjCoordinate stAdjCoordinate) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STAdjCoordinate stAdjCoordinate2 = (STAdjCoordinate)this.get_store().find_attribute_user(CTPath2DArcToImpl.WR$0);
            if (stAdjCoordinate2 == null) {
                stAdjCoordinate2 = (STAdjCoordinate)this.get_store().add_attribute_user(CTPath2DArcToImpl.WR$0);
            }
            stAdjCoordinate2.set(stAdjCoordinate);
        }
    }
    
    public Object getHR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.HR$2);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getObjectValue();
        }
    }
    
    public STAdjCoordinate xgetHR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STAdjCoordinate)this.get_store().find_attribute_user(CTPath2DArcToImpl.HR$2);
        }
    }
    
    public void setHR(final Object objectValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.HR$2);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTPath2DArcToImpl.HR$2);
            }
            simpleValue.setObjectValue(objectValue);
        }
    }
    
    public void xsetHR(final STAdjCoordinate stAdjCoordinate) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STAdjCoordinate stAdjCoordinate2 = (STAdjCoordinate)this.get_store().find_attribute_user(CTPath2DArcToImpl.HR$2);
            if (stAdjCoordinate2 == null) {
                stAdjCoordinate2 = (STAdjCoordinate)this.get_store().add_attribute_user(CTPath2DArcToImpl.HR$2);
            }
            stAdjCoordinate2.set(stAdjCoordinate);
        }
    }
    
    public Object getStAng() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.STANG$4);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getObjectValue();
        }
    }
    
    public STAdjAngle xgetStAng() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STAdjAngle)this.get_store().find_attribute_user(CTPath2DArcToImpl.STANG$4);
        }
    }
    
    public void setStAng(final Object objectValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.STANG$4);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTPath2DArcToImpl.STANG$4);
            }
            simpleValue.setObjectValue(objectValue);
        }
    }
    
    public void xsetStAng(final STAdjAngle stAdjAngle) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STAdjAngle stAdjAngle2 = (STAdjAngle)this.get_store().find_attribute_user(CTPath2DArcToImpl.STANG$4);
            if (stAdjAngle2 == null) {
                stAdjAngle2 = (STAdjAngle)this.get_store().add_attribute_user(CTPath2DArcToImpl.STANG$4);
            }
            stAdjAngle2.set(stAdjAngle);
        }
    }
    
    public Object getSwAng() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.SWANG$6);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getObjectValue();
        }
    }
    
    public STAdjAngle xgetSwAng() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STAdjAngle)this.get_store().find_attribute_user(CTPath2DArcToImpl.SWANG$6);
        }
    }
    
    public void setSwAng(final Object objectValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTPath2DArcToImpl.SWANG$6);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTPath2DArcToImpl.SWANG$6);
            }
            simpleValue.setObjectValue(objectValue);
        }
    }
    
    public void xsetSwAng(final STAdjAngle stAdjAngle) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STAdjAngle stAdjAngle2 = (STAdjAngle)this.get_store().find_attribute_user(CTPath2DArcToImpl.SWANG$6);
            if (stAdjAngle2 == null) {
                stAdjAngle2 = (STAdjAngle)this.get_store().add_attribute_user(CTPath2DArcToImpl.SWANG$6);
            }
            stAdjAngle2.set(stAdjAngle);
        }
    }
    
    static {
        WR$0 = new QName("", "wR");
        HR$2 = new QName("", "hR");
        STANG$4 = new QName("", "stAng");
        SWANG$6 = new QName("", "swAng");
    }
}
