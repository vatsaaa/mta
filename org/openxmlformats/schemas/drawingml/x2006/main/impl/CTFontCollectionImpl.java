// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.openxmlformats.schemas.drawingml.x2006.main.CTOfficeArtExtensionList;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSupplementalFont;
import java.util.List;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextFont;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTFontCollection;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTFontCollectionImpl extends XmlComplexContentImpl implements CTFontCollection
{
    private static final QName LATIN$0;
    private static final QName EA$2;
    private static final QName CS$4;
    private static final QName FONT$6;
    private static final QName EXTLST$8;
    
    public CTFontCollectionImpl(final SchemaType type) {
        super(type);
    }
    
    public CTTextFont getLatin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTextFont ctTextFont = (CTTextFont)this.get_store().find_element_user(CTFontCollectionImpl.LATIN$0, 0);
            if (ctTextFont == null) {
                return null;
            }
            return ctTextFont;
        }
    }
    
    public void setLatin(final CTTextFont ctTextFont) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTextFont ctTextFont2 = (CTTextFont)this.get_store().find_element_user(CTFontCollectionImpl.LATIN$0, 0);
            if (ctTextFont2 == null) {
                ctTextFont2 = (CTTextFont)this.get_store().add_element_user(CTFontCollectionImpl.LATIN$0);
            }
            ctTextFont2.set(ctTextFont);
        }
    }
    
    public CTTextFont addNewLatin() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTextFont)this.get_store().add_element_user(CTFontCollectionImpl.LATIN$0);
        }
    }
    
    public CTTextFont getEa() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTextFont ctTextFont = (CTTextFont)this.get_store().find_element_user(CTFontCollectionImpl.EA$2, 0);
            if (ctTextFont == null) {
                return null;
            }
            return ctTextFont;
        }
    }
    
    public void setEa(final CTTextFont ctTextFont) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTextFont ctTextFont2 = (CTTextFont)this.get_store().find_element_user(CTFontCollectionImpl.EA$2, 0);
            if (ctTextFont2 == null) {
                ctTextFont2 = (CTTextFont)this.get_store().add_element_user(CTFontCollectionImpl.EA$2);
            }
            ctTextFont2.set(ctTextFont);
        }
    }
    
    public CTTextFont addNewEa() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTextFont)this.get_store().add_element_user(CTFontCollectionImpl.EA$2);
        }
    }
    
    public CTTextFont getCs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTextFont ctTextFont = (CTTextFont)this.get_store().find_element_user(CTFontCollectionImpl.CS$4, 0);
            if (ctTextFont == null) {
                return null;
            }
            return ctTextFont;
        }
    }
    
    public void setCs(final CTTextFont ctTextFont) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTextFont ctTextFont2 = (CTTextFont)this.get_store().find_element_user(CTFontCollectionImpl.CS$4, 0);
            if (ctTextFont2 == null) {
                ctTextFont2 = (CTTextFont)this.get_store().add_element_user(CTFontCollectionImpl.CS$4);
            }
            ctTextFont2.set(ctTextFont);
        }
    }
    
    public CTTextFont addNewCs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTextFont)this.get_store().add_element_user(CTFontCollectionImpl.CS$4);
        }
    }
    
    public List<CTSupplementalFont> getFontList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSupplementalFont>)new CTFontCollectionImpl.FontList(this);
        }
    }
    
    public CTSupplementalFont[] getFontArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontCollectionImpl.FONT$6, list);
            final CTSupplementalFont[] array = new CTSupplementalFont[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSupplementalFont getFontArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSupplementalFont ctSupplementalFont = (CTSupplementalFont)this.get_store().find_element_user(CTFontCollectionImpl.FONT$6, n);
            if (ctSupplementalFont == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSupplementalFont;
        }
    }
    
    public int sizeOfFontArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontCollectionImpl.FONT$6);
        }
    }
    
    public void setFontArray(final CTSupplementalFont[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTFontCollectionImpl.FONT$6);
        }
    }
    
    public void setFontArray(final int n, final CTSupplementalFont ctSupplementalFont) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSupplementalFont ctSupplementalFont2 = (CTSupplementalFont)this.get_store().find_element_user(CTFontCollectionImpl.FONT$6, n);
            if (ctSupplementalFont2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSupplementalFont2.set((XmlObject)ctSupplementalFont);
        }
    }
    
    public CTSupplementalFont insertNewFont(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSupplementalFont)this.get_store().insert_element_user(CTFontCollectionImpl.FONT$6, n);
        }
    }
    
    public CTSupplementalFont addNewFont() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSupplementalFont)this.get_store().add_element_user(CTFontCollectionImpl.FONT$6);
        }
    }
    
    public void removeFont(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontCollectionImpl.FONT$6, n);
        }
    }
    
    public CTOfficeArtExtensionList getExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOfficeArtExtensionList list = (CTOfficeArtExtensionList)this.get_store().find_element_user(CTFontCollectionImpl.EXTLST$8, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public boolean isSetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontCollectionImpl.EXTLST$8) != 0;
        }
    }
    
    public void setExtLst(final CTOfficeArtExtensionList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTOfficeArtExtensionList list2 = (CTOfficeArtExtensionList)this.get_store().find_element_user(CTFontCollectionImpl.EXTLST$8, 0);
            if (list2 == null) {
                list2 = (CTOfficeArtExtensionList)this.get_store().add_element_user(CTFontCollectionImpl.EXTLST$8);
            }
            list2.set((XmlObject)list);
        }
    }
    
    public CTOfficeArtExtensionList addNewExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOfficeArtExtensionList)this.get_store().add_element_user(CTFontCollectionImpl.EXTLST$8);
        }
    }
    
    public void unsetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontCollectionImpl.EXTLST$8, 0);
        }
    }
    
    static {
        LATIN$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "latin");
        EA$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "ea");
        CS$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "cs");
        FONT$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "font");
        EXTLST$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "extLst");
    }
}
