// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STGeomGuideName;
import org.openxmlformats.schemas.drawingml.x2006.main.STAngle;
import org.openxmlformats.schemas.drawingml.x2006.main.STAdjAngle;
import org.apache.xmlbeans.impl.values.XmlUnionImpl;

public class STAdjAngleImpl extends XmlUnionImpl implements STAdjAngle, STAngle, STGeomGuideName
{
    public STAdjAngleImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STAdjAngleImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
