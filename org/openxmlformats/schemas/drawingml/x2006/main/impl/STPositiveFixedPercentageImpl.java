// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPositiveFixedPercentage;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STPositiveFixedPercentageImpl extends JavaIntHolderEx implements STPositiveFixedPercentage
{
    public STPositiveFixedPercentageImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPositiveFixedPercentageImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
