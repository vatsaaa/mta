// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPositivePercentage;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STPositivePercentageImpl extends JavaIntHolderEx implements STPositivePercentage
{
    public STPositivePercentageImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPositivePercentageImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
