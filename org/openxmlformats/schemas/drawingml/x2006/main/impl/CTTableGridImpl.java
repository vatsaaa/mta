// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableCol;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableGrid;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTTableGridImpl extends XmlComplexContentImpl implements CTTableGrid
{
    private static final QName GRIDCOL$0;
    
    public CTTableGridImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTTableCol> getGridColList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTableCol>)new CTTableGridImpl.GridColList(this);
        }
    }
    
    public CTTableCol[] getGridColArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTableGridImpl.GRIDCOL$0, list);
            final CTTableCol[] array = new CTTableCol[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTableCol getGridColArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTableCol ctTableCol = (CTTableCol)this.get_store().find_element_user(CTTableGridImpl.GRIDCOL$0, n);
            if (ctTableCol == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTableCol;
        }
    }
    
    public int sizeOfGridColArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTableGridImpl.GRIDCOL$0);
        }
    }
    
    public void setGridColArray(final CTTableCol[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTableGridImpl.GRIDCOL$0);
        }
    }
    
    public void setGridColArray(final int n, final CTTableCol ctTableCol) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTableCol ctTableCol2 = (CTTableCol)this.get_store().find_element_user(CTTableGridImpl.GRIDCOL$0, n);
            if (ctTableCol2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTableCol2.set(ctTableCol);
        }
    }
    
    public CTTableCol insertNewGridCol(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTableCol)this.get_store().insert_element_user(CTTableGridImpl.GRIDCOL$0, n);
        }
    }
    
    public CTTableCol addNewGridCol() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTableCol)this.get_store().add_element_user(CTTableGridImpl.GRIDCOL$0);
        }
    }
    
    public void removeGridCol(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTableGridImpl.GRIDCOL$0, n);
        }
    }
    
    static {
        GRIDCOL$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "gridCol");
    }
}
