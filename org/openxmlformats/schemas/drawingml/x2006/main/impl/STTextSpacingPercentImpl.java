// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextSpacingPercent;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STTextSpacingPercentImpl extends JavaIntHolderEx implements STTextSpacingPercent
{
    public STTextSpacingPercentImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextSpacingPercentImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
