// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextFontSize;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STTextFontSizeImpl extends JavaIntHolderEx implements STTextFontSize
{
    public STTextFontSizeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextFontSizeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
