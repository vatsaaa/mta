// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STCoordinate;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STCoordinateImpl extends JavaLongHolderEx implements STCoordinate
{
    public STCoordinateImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCoordinateImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
