// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineProperties;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineStyleList;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTLineStyleListImpl extends XmlComplexContentImpl implements CTLineStyleList
{
    private static final QName LN$0;
    
    public CTLineStyleListImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTLineProperties> getLnList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLineProperties>)new CTLineStyleListImpl.LnList(this);
        }
    }
    
    public CTLineProperties[] getLnArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTLineStyleListImpl.LN$0, list);
            final CTLineProperties[] array = new CTLineProperties[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLineProperties getLnArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLineProperties ctLineProperties = (CTLineProperties)this.get_store().find_element_user(CTLineStyleListImpl.LN$0, n);
            if (ctLineProperties == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLineProperties;
        }
    }
    
    public int sizeOfLnArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTLineStyleListImpl.LN$0);
        }
    }
    
    public void setLnArray(final CTLineProperties[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTLineStyleListImpl.LN$0);
        }
    }
    
    public void setLnArray(final int n, final CTLineProperties ctLineProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLineProperties ctLineProperties2 = (CTLineProperties)this.get_store().find_element_user(CTLineStyleListImpl.LN$0, n);
            if (ctLineProperties2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLineProperties2.set(ctLineProperties);
        }
    }
    
    public CTLineProperties insertNewLn(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLineProperties)this.get_store().insert_element_user(CTLineStyleListImpl.LN$0, n);
        }
    }
    
    public CTLineProperties addNewLn() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLineProperties)this.get_store().add_element_user(CTLineStyleListImpl.LN$0);
        }
    }
    
    public void removeLn(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTLineStyleListImpl.LN$0, n);
        }
    }
    
    static {
        LN$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "ln");
    }
}
