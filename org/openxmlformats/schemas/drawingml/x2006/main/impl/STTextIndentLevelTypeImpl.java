// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextIndentLevelType;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STTextIndentLevelTypeImpl extends JavaIntHolderEx implements STTextIndentLevelType
{
    public STTextIndentLevelTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextIndentLevelTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
