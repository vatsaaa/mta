// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STGeomGuideName;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STGeomGuideNameImpl extends JavaStringHolderEx implements STGeomGuideName
{
    public STGeomGuideNameImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STGeomGuideNameImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
