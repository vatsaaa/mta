// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STCoordinate32;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STCoordinate32Impl extends JavaIntHolderEx implements STCoordinate32
{
    public STCoordinate32Impl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCoordinate32Impl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
