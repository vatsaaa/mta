// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineWidth;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STLineWidthImpl extends JavaIntHolderEx implements STLineWidth
{
    public STLineWidthImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLineWidthImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
