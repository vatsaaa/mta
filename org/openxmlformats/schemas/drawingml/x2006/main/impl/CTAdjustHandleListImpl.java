// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.openxmlformats.schemas.drawingml.x2006.main.CTPolarAdjustHandle;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTXYAdjustHandle;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAdjustHandleList;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTAdjustHandleListImpl extends XmlComplexContentImpl implements CTAdjustHandleList
{
    private static final QName AHXY$0;
    private static final QName AHPOLAR$2;
    
    public CTAdjustHandleListImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTXYAdjustHandle> getAhXYList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTXYAdjustHandle>)new CTAdjustHandleListImpl.AhXYList(this);
        }
    }
    
    public CTXYAdjustHandle[] getAhXYArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTAdjustHandleListImpl.AHXY$0, list);
            final CTXYAdjustHandle[] array = new CTXYAdjustHandle[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTXYAdjustHandle getAhXYArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTXYAdjustHandle ctxyAdjustHandle = (CTXYAdjustHandle)this.get_store().find_element_user(CTAdjustHandleListImpl.AHXY$0, n);
            if (ctxyAdjustHandle == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctxyAdjustHandle;
        }
    }
    
    public int sizeOfAhXYArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTAdjustHandleListImpl.AHXY$0);
        }
    }
    
    public void setAhXYArray(final CTXYAdjustHandle[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTAdjustHandleListImpl.AHXY$0);
        }
    }
    
    public void setAhXYArray(final int n, final CTXYAdjustHandle ctxyAdjustHandle) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTXYAdjustHandle ctxyAdjustHandle2 = (CTXYAdjustHandle)this.get_store().find_element_user(CTAdjustHandleListImpl.AHXY$0, n);
            if (ctxyAdjustHandle2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctxyAdjustHandle2.set((XmlObject)ctxyAdjustHandle);
        }
    }
    
    public CTXYAdjustHandle insertNewAhXY(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTXYAdjustHandle)this.get_store().insert_element_user(CTAdjustHandleListImpl.AHXY$0, n);
        }
    }
    
    public CTXYAdjustHandle addNewAhXY() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTXYAdjustHandle)this.get_store().add_element_user(CTAdjustHandleListImpl.AHXY$0);
        }
    }
    
    public void removeAhXY(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTAdjustHandleListImpl.AHXY$0, n);
        }
    }
    
    public List<CTPolarAdjustHandle> getAhPolarList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPolarAdjustHandle>)new CTAdjustHandleListImpl.AhPolarList(this);
        }
    }
    
    public CTPolarAdjustHandle[] getAhPolarArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTAdjustHandleListImpl.AHPOLAR$2, list);
            final CTPolarAdjustHandle[] array = new CTPolarAdjustHandle[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPolarAdjustHandle getAhPolarArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPolarAdjustHandle ctPolarAdjustHandle = (CTPolarAdjustHandle)this.get_store().find_element_user(CTAdjustHandleListImpl.AHPOLAR$2, n);
            if (ctPolarAdjustHandle == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPolarAdjustHandle;
        }
    }
    
    public int sizeOfAhPolarArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTAdjustHandleListImpl.AHPOLAR$2);
        }
    }
    
    public void setAhPolarArray(final CTPolarAdjustHandle[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTAdjustHandleListImpl.AHPOLAR$2);
        }
    }
    
    public void setAhPolarArray(final int n, final CTPolarAdjustHandle ctPolarAdjustHandle) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPolarAdjustHandle ctPolarAdjustHandle2 = (CTPolarAdjustHandle)this.get_store().find_element_user(CTAdjustHandleListImpl.AHPOLAR$2, n);
            if (ctPolarAdjustHandle2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPolarAdjustHandle2.set((XmlObject)ctPolarAdjustHandle);
        }
    }
    
    public CTPolarAdjustHandle insertNewAhPolar(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPolarAdjustHandle)this.get_store().insert_element_user(CTAdjustHandleListImpl.AHPOLAR$2, n);
        }
    }
    
    public CTPolarAdjustHandle addNewAhPolar() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPolarAdjustHandle)this.get_store().add_element_user(CTAdjustHandleListImpl.AHPOLAR$2);
        }
    }
    
    public void removeAhPolar(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTAdjustHandleListImpl.AHPOLAR$2, n);
        }
    }
    
    static {
        AHXY$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "ahXY");
        AHPOLAR$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "ahPolar");
    }
}
