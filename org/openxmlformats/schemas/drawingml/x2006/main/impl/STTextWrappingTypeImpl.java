// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextWrappingType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTextWrappingTypeImpl extends JavaStringEnumerationHolderEx implements STTextWrappingType
{
    public STTextWrappingTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextWrappingTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
