// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPenAlignment;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STPenAlignmentImpl extends JavaStringEnumerationHolderEx implements STPenAlignment
{
    public STPenAlignmentImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPenAlignmentImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
