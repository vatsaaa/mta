// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextBulletSizePercent;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STTextBulletSizePercentImpl extends JavaIntHolderEx implements STTextBulletSizePercent
{
    public STTextBulletSizePercentImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextBulletSizePercentImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
