// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.openxmlformats.schemas.drawingml.x2006.main.CTFontReference;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTStyleMatrixReference;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeStyle;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTShapeStyleImpl extends XmlComplexContentImpl implements CTShapeStyle
{
    private static final QName LNREF$0;
    private static final QName FILLREF$2;
    private static final QName EFFECTREF$4;
    private static final QName FONTREF$6;
    
    public CTShapeStyleImpl(final SchemaType type) {
        super(type);
    }
    
    public CTStyleMatrixReference getLnRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStyleMatrixReference ctStyleMatrixReference = (CTStyleMatrixReference)this.get_store().find_element_user(CTShapeStyleImpl.LNREF$0, 0);
            if (ctStyleMatrixReference == null) {
                return null;
            }
            return ctStyleMatrixReference;
        }
    }
    
    public void setLnRef(final CTStyleMatrixReference ctStyleMatrixReference) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTStyleMatrixReference ctStyleMatrixReference2 = (CTStyleMatrixReference)this.get_store().find_element_user(CTShapeStyleImpl.LNREF$0, 0);
            if (ctStyleMatrixReference2 == null) {
                ctStyleMatrixReference2 = (CTStyleMatrixReference)this.get_store().add_element_user(CTShapeStyleImpl.LNREF$0);
            }
            ctStyleMatrixReference2.set(ctStyleMatrixReference);
        }
    }
    
    public CTStyleMatrixReference addNewLnRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStyleMatrixReference)this.get_store().add_element_user(CTShapeStyleImpl.LNREF$0);
        }
    }
    
    public CTStyleMatrixReference getFillRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStyleMatrixReference ctStyleMatrixReference = (CTStyleMatrixReference)this.get_store().find_element_user(CTShapeStyleImpl.FILLREF$2, 0);
            if (ctStyleMatrixReference == null) {
                return null;
            }
            return ctStyleMatrixReference;
        }
    }
    
    public void setFillRef(final CTStyleMatrixReference ctStyleMatrixReference) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTStyleMatrixReference ctStyleMatrixReference2 = (CTStyleMatrixReference)this.get_store().find_element_user(CTShapeStyleImpl.FILLREF$2, 0);
            if (ctStyleMatrixReference2 == null) {
                ctStyleMatrixReference2 = (CTStyleMatrixReference)this.get_store().add_element_user(CTShapeStyleImpl.FILLREF$2);
            }
            ctStyleMatrixReference2.set(ctStyleMatrixReference);
        }
    }
    
    public CTStyleMatrixReference addNewFillRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStyleMatrixReference)this.get_store().add_element_user(CTShapeStyleImpl.FILLREF$2);
        }
    }
    
    public CTStyleMatrixReference getEffectRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStyleMatrixReference ctStyleMatrixReference = (CTStyleMatrixReference)this.get_store().find_element_user(CTShapeStyleImpl.EFFECTREF$4, 0);
            if (ctStyleMatrixReference == null) {
                return null;
            }
            return ctStyleMatrixReference;
        }
    }
    
    public void setEffectRef(final CTStyleMatrixReference ctStyleMatrixReference) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTStyleMatrixReference ctStyleMatrixReference2 = (CTStyleMatrixReference)this.get_store().find_element_user(CTShapeStyleImpl.EFFECTREF$4, 0);
            if (ctStyleMatrixReference2 == null) {
                ctStyleMatrixReference2 = (CTStyleMatrixReference)this.get_store().add_element_user(CTShapeStyleImpl.EFFECTREF$4);
            }
            ctStyleMatrixReference2.set(ctStyleMatrixReference);
        }
    }
    
    public CTStyleMatrixReference addNewEffectRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStyleMatrixReference)this.get_store().add_element_user(CTShapeStyleImpl.EFFECTREF$4);
        }
    }
    
    public CTFontReference getFontRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontReference ctFontReference = (CTFontReference)this.get_store().find_element_user(CTShapeStyleImpl.FONTREF$6, 0);
            if (ctFontReference == null) {
                return null;
            }
            return ctFontReference;
        }
    }
    
    public void setFontRef(final CTFontReference ctFontReference) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTFontReference ctFontReference2 = (CTFontReference)this.get_store().find_element_user(CTShapeStyleImpl.FONTREF$6, 0);
            if (ctFontReference2 == null) {
                ctFontReference2 = (CTFontReference)this.get_store().add_element_user(CTShapeStyleImpl.FONTREF$6);
            }
            ctFontReference2.set(ctFontReference);
        }
    }
    
    public CTFontReference addNewFontRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontReference)this.get_store().add_element_user(CTShapeStyleImpl.FONTREF$6);
        }
    }
    
    static {
        LNREF$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "lnRef");
        FILLREF$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "fillRef");
        EFFECTREF$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "effectRef");
        FONTREF$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "fontRef");
    }
}
