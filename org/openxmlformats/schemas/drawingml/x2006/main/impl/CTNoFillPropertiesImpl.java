// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNoFillProperties;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTNoFillPropertiesImpl extends XmlComplexContentImpl implements CTNoFillProperties
{
    public CTNoFillPropertiesImpl(final SchemaType type) {
        super(type);
    }
}
