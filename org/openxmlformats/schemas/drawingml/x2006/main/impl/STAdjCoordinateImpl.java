// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STGeomGuideName;
import org.openxmlformats.schemas.drawingml.x2006.main.STCoordinate;
import org.openxmlformats.schemas.drawingml.x2006.main.STAdjCoordinate;
import org.apache.xmlbeans.impl.values.XmlUnionImpl;

public class STAdjCoordinateImpl extends XmlUnionImpl implements STAdjCoordinate, STCoordinate, STGeomGuideName
{
    public STAdjCoordinateImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STAdjCoordinateImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
