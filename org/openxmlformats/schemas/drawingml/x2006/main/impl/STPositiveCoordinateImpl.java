// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPositiveCoordinate;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STPositiveCoordinateImpl extends JavaLongHolderEx implements STPositiveCoordinate
{
    public STPositiveCoordinateImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPositiveCoordinateImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
