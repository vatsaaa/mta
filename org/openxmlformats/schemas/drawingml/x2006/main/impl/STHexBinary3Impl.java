// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STHexBinary3;
import org.apache.xmlbeans.impl.values.JavaHexBinaryHolderEx;

public class STHexBinary3Impl extends JavaHexBinaryHolderEx implements STHexBinary3
{
    public STHexBinary3Impl(final SchemaType type) {
        super(type, false);
    }
    
    protected STHexBinary3Impl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
