// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPercentage;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STPercentageImpl extends JavaIntHolderEx implements STPercentage
{
    public STPercentageImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPercentageImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
