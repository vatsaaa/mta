// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STSchemeColorVal;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STSchemeColorValImpl extends JavaStringEnumerationHolderEx implements STSchemeColorVal
{
    public STSchemeColorValImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSchemeColorValImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
