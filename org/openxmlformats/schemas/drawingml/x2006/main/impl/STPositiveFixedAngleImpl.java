// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPositiveFixedAngle;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STPositiveFixedAngleImpl extends JavaIntHolderEx implements STPositiveFixedAngle
{
    public STPositiveFixedAngleImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPositiveFixedAngleImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
