// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextPoint;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STTextPointImpl extends JavaIntHolderEx implements STTextPoint
{
    public STTextPointImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextPointImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
