// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextMargin;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STTextMarginImpl extends JavaIntHolderEx implements STTextMargin
{
    public STTextMarginImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextMarginImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
