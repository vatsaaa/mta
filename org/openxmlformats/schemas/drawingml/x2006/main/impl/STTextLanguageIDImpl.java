// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextLanguageID;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STTextLanguageIDImpl extends JavaStringHolderEx implements STTextLanguageID
{
    public STTextLanguageIDImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextLanguageIDImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
