// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STLineEndTypeImpl extends JavaStringEnumerationHolderEx implements STLineEndType
{
    public STLineEndTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLineEndTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
