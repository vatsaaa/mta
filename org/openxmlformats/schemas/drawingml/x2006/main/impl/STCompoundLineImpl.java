// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STCompoundLine;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STCompoundLineImpl extends JavaStringEnumerationHolderEx implements STCompoundLine
{
    public STCompoundLineImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCompoundLineImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
