// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGradientStop;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGradientStopList;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTGradientStopListImpl extends XmlComplexContentImpl implements CTGradientStopList
{
    private static final QName GS$0;
    
    public CTGradientStopListImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTGradientStop> getGsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGradientStop>)new CTGradientStopListImpl.GsList(this);
        }
    }
    
    public CTGradientStop[] getGsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTGradientStopListImpl.GS$0, list);
            final CTGradientStop[] array = new CTGradientStop[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGradientStop getGsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGradientStop ctGradientStop = (CTGradientStop)this.get_store().find_element_user(CTGradientStopListImpl.GS$0, n);
            if (ctGradientStop == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGradientStop;
        }
    }
    
    public int sizeOfGsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGradientStopListImpl.GS$0);
        }
    }
    
    public void setGsArray(final CTGradientStop[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTGradientStopListImpl.GS$0);
        }
    }
    
    public void setGsArray(final int n, final CTGradientStop ctGradientStop) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGradientStop ctGradientStop2 = (CTGradientStop)this.get_store().find_element_user(CTGradientStopListImpl.GS$0, n);
            if (ctGradientStop2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGradientStop2.set(ctGradientStop);
        }
    }
    
    public CTGradientStop insertNewGs(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGradientStop)this.get_store().insert_element_user(CTGradientStopListImpl.GS$0, n);
        }
    }
    
    public CTGradientStop addNewGs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGradientStop)this.get_store().add_element_user(CTGradientStopListImpl.GS$0);
        }
    }
    
    public void removeGs(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGradientStopListImpl.GS$0, n);
        }
    }
    
    static {
        GS$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "gs");
    }
}
