// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndLength;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STLineEndLengthImpl extends JavaStringEnumerationHolderEx implements STLineEndLength
{
    public STLineEndLengthImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLineEndLengthImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
