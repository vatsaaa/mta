// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextAnchoringType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTextAnchoringTypeImpl extends JavaStringEnumerationHolderEx implements STTextAnchoringType
{
    public STTextAnchoringTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextAnchoringTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
