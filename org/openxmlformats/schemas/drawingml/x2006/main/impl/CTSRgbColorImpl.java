// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.openxmlformats.schemas.drawingml.x2006.main.STHexBinary3;
import org.apache.xmlbeans.SimpleValue;
import org.openxmlformats.schemas.drawingml.x2006.main.CTInverseGammaTransform;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGammaTransform;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPercentage;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAngle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveFixedAngle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositivePercentage;
import org.openxmlformats.schemas.drawingml.x2006.main.CTFixedPercentage;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGrayscaleTransform;
import org.openxmlformats.schemas.drawingml.x2006.main.CTInverseTransform;
import org.openxmlformats.schemas.drawingml.x2006.main.CTComplementTransform;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveFixedPercentage;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTSRgbColorImpl extends XmlComplexContentImpl implements CTSRgbColor
{
    private static final QName TINT$0;
    private static final QName SHADE$2;
    private static final QName COMP$4;
    private static final QName INV$6;
    private static final QName GRAY$8;
    private static final QName ALPHA$10;
    private static final QName ALPHAOFF$12;
    private static final QName ALPHAMOD$14;
    private static final QName HUE$16;
    private static final QName HUEOFF$18;
    private static final QName HUEMOD$20;
    private static final QName SAT$22;
    private static final QName SATOFF$24;
    private static final QName SATMOD$26;
    private static final QName LUM$28;
    private static final QName LUMOFF$30;
    private static final QName LUMMOD$32;
    private static final QName RED$34;
    private static final QName REDOFF$36;
    private static final QName REDMOD$38;
    private static final QName GREEN$40;
    private static final QName GREENOFF$42;
    private static final QName GREENMOD$44;
    private static final QName BLUE$46;
    private static final QName BLUEOFF$48;
    private static final QName BLUEMOD$50;
    private static final QName GAMMA$52;
    private static final QName INVGAMMA$54;
    private static final QName VAL$56;
    
    public CTSRgbColorImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTPositiveFixedPercentage> getTintList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPositiveFixedPercentage>)new CTSRgbColorImpl.TintList(this);
        }
    }
    
    public CTPositiveFixedPercentage[] getTintArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.TINT$0, list);
            final CTPositiveFixedPercentage[] array = new CTPositiveFixedPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPositiveFixedPercentage getTintArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedPercentage ctPositiveFixedPercentage = (CTPositiveFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.TINT$0, n);
            if (ctPositiveFixedPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPositiveFixedPercentage;
        }
    }
    
    public int sizeOfTintArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.TINT$0);
        }
    }
    
    public void setTintArray(final CTPositiveFixedPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.TINT$0);
        }
    }
    
    public void setTintArray(final int n, final CTPositiveFixedPercentage ctPositiveFixedPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedPercentage ctPositiveFixedPercentage2 = (CTPositiveFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.TINT$0, n);
            if (ctPositiveFixedPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPositiveFixedPercentage2.set(ctPositiveFixedPercentage);
        }
    }
    
    public CTPositiveFixedPercentage insertNewTint(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.TINT$0, n);
        }
    }
    
    public CTPositiveFixedPercentage addNewTint() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedPercentage)this.get_store().add_element_user(CTSRgbColorImpl.TINT$0);
        }
    }
    
    public void removeTint(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.TINT$0, n);
        }
    }
    
    public List<CTPositiveFixedPercentage> getShadeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPositiveFixedPercentage>)new CTSRgbColorImpl.ShadeList(this);
        }
    }
    
    public CTPositiveFixedPercentage[] getShadeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.SHADE$2, list);
            final CTPositiveFixedPercentage[] array = new CTPositiveFixedPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPositiveFixedPercentage getShadeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedPercentage ctPositiveFixedPercentage = (CTPositiveFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SHADE$2, n);
            if (ctPositiveFixedPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPositiveFixedPercentage;
        }
    }
    
    public int sizeOfShadeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.SHADE$2);
        }
    }
    
    public void setShadeArray(final CTPositiveFixedPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.SHADE$2);
        }
    }
    
    public void setShadeArray(final int n, final CTPositiveFixedPercentage ctPositiveFixedPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedPercentage ctPositiveFixedPercentage2 = (CTPositiveFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SHADE$2, n);
            if (ctPositiveFixedPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPositiveFixedPercentage2.set(ctPositiveFixedPercentage);
        }
    }
    
    public CTPositiveFixedPercentage insertNewShade(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.SHADE$2, n);
        }
    }
    
    public CTPositiveFixedPercentage addNewShade() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedPercentage)this.get_store().add_element_user(CTSRgbColorImpl.SHADE$2);
        }
    }
    
    public void removeShade(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.SHADE$2, n);
        }
    }
    
    public List<CTComplementTransform> getCompList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTComplementTransform>)new CTSRgbColorImpl.CompList(this);
        }
    }
    
    public CTComplementTransform[] getCompArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.COMP$4, list);
            final CTComplementTransform[] array = new CTComplementTransform[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTComplementTransform getCompArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTComplementTransform ctComplementTransform = (CTComplementTransform)this.get_store().find_element_user(CTSRgbColorImpl.COMP$4, n);
            if (ctComplementTransform == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctComplementTransform;
        }
    }
    
    public int sizeOfCompArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.COMP$4);
        }
    }
    
    public void setCompArray(final CTComplementTransform[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSRgbColorImpl.COMP$4);
        }
    }
    
    public void setCompArray(final int n, final CTComplementTransform ctComplementTransform) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTComplementTransform ctComplementTransform2 = (CTComplementTransform)this.get_store().find_element_user(CTSRgbColorImpl.COMP$4, n);
            if (ctComplementTransform2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctComplementTransform2.set((XmlObject)ctComplementTransform);
        }
    }
    
    public CTComplementTransform insertNewComp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTComplementTransform)this.get_store().insert_element_user(CTSRgbColorImpl.COMP$4, n);
        }
    }
    
    public CTComplementTransform addNewComp() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTComplementTransform)this.get_store().add_element_user(CTSRgbColorImpl.COMP$4);
        }
    }
    
    public void removeComp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.COMP$4, n);
        }
    }
    
    public List<CTInverseTransform> getInvList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTInverseTransform>)new CTSRgbColorImpl.InvList(this);
        }
    }
    
    public CTInverseTransform[] getInvArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.INV$6, list);
            final CTInverseTransform[] array = new CTInverseTransform[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTInverseTransform getInvArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTInverseTransform ctInverseTransform = (CTInverseTransform)this.get_store().find_element_user(CTSRgbColorImpl.INV$6, n);
            if (ctInverseTransform == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctInverseTransform;
        }
    }
    
    public int sizeOfInvArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.INV$6);
        }
    }
    
    public void setInvArray(final CTInverseTransform[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSRgbColorImpl.INV$6);
        }
    }
    
    public void setInvArray(final int n, final CTInverseTransform ctInverseTransform) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTInverseTransform ctInverseTransform2 = (CTInverseTransform)this.get_store().find_element_user(CTSRgbColorImpl.INV$6, n);
            if (ctInverseTransform2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctInverseTransform2.set((XmlObject)ctInverseTransform);
        }
    }
    
    public CTInverseTransform insertNewInv(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTInverseTransform)this.get_store().insert_element_user(CTSRgbColorImpl.INV$6, n);
        }
    }
    
    public CTInverseTransform addNewInv() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTInverseTransform)this.get_store().add_element_user(CTSRgbColorImpl.INV$6);
        }
    }
    
    public void removeInv(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.INV$6, n);
        }
    }
    
    public List<CTGrayscaleTransform> getGrayList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGrayscaleTransform>)new CTSRgbColorImpl.GrayList(this);
        }
    }
    
    public CTGrayscaleTransform[] getGrayArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.GRAY$8, list);
            final CTGrayscaleTransform[] array = new CTGrayscaleTransform[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGrayscaleTransform getGrayArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGrayscaleTransform ctGrayscaleTransform = (CTGrayscaleTransform)this.get_store().find_element_user(CTSRgbColorImpl.GRAY$8, n);
            if (ctGrayscaleTransform == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGrayscaleTransform;
        }
    }
    
    public int sizeOfGrayArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.GRAY$8);
        }
    }
    
    public void setGrayArray(final CTGrayscaleTransform[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSRgbColorImpl.GRAY$8);
        }
    }
    
    public void setGrayArray(final int n, final CTGrayscaleTransform ctGrayscaleTransform) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGrayscaleTransform ctGrayscaleTransform2 = (CTGrayscaleTransform)this.get_store().find_element_user(CTSRgbColorImpl.GRAY$8, n);
            if (ctGrayscaleTransform2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGrayscaleTransform2.set((XmlObject)ctGrayscaleTransform);
        }
    }
    
    public CTGrayscaleTransform insertNewGray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGrayscaleTransform)this.get_store().insert_element_user(CTSRgbColorImpl.GRAY$8, n);
        }
    }
    
    public CTGrayscaleTransform addNewGray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGrayscaleTransform)this.get_store().add_element_user(CTSRgbColorImpl.GRAY$8);
        }
    }
    
    public void removeGray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.GRAY$8, n);
        }
    }
    
    public List<CTPositiveFixedPercentage> getAlphaList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPositiveFixedPercentage>)new CTSRgbColorImpl.AlphaList(this);
        }
    }
    
    public CTPositiveFixedPercentage[] getAlphaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.ALPHA$10, list);
            final CTPositiveFixedPercentage[] array = new CTPositiveFixedPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPositiveFixedPercentage getAlphaArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedPercentage ctPositiveFixedPercentage = (CTPositiveFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.ALPHA$10, n);
            if (ctPositiveFixedPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPositiveFixedPercentage;
        }
    }
    
    public int sizeOfAlphaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.ALPHA$10);
        }
    }
    
    public void setAlphaArray(final CTPositiveFixedPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.ALPHA$10);
        }
    }
    
    public void setAlphaArray(final int n, final CTPositiveFixedPercentage ctPositiveFixedPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedPercentage ctPositiveFixedPercentage2 = (CTPositiveFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.ALPHA$10, n);
            if (ctPositiveFixedPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPositiveFixedPercentage2.set(ctPositiveFixedPercentage);
        }
    }
    
    public CTPositiveFixedPercentage insertNewAlpha(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.ALPHA$10, n);
        }
    }
    
    public CTPositiveFixedPercentage addNewAlpha() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedPercentage)this.get_store().add_element_user(CTSRgbColorImpl.ALPHA$10);
        }
    }
    
    public void removeAlpha(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.ALPHA$10, n);
        }
    }
    
    public List<CTFixedPercentage> getAlphaOffList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFixedPercentage>)new CTSRgbColorImpl.AlphaOffList(this);
        }
    }
    
    public CTFixedPercentage[] getAlphaOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.ALPHAOFF$12, list);
            final CTFixedPercentage[] array = new CTFixedPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFixedPercentage getAlphaOffArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFixedPercentage ctFixedPercentage = (CTFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.ALPHAOFF$12, n);
            if (ctFixedPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFixedPercentage;
        }
    }
    
    public int sizeOfAlphaOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.ALPHAOFF$12);
        }
    }
    
    public void setAlphaOffArray(final CTFixedPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.ALPHAOFF$12);
        }
    }
    
    public void setAlphaOffArray(final int n, final CTFixedPercentage ctFixedPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFixedPercentage ctFixedPercentage2 = (CTFixedPercentage)this.get_store().find_element_user(CTSRgbColorImpl.ALPHAOFF$12, n);
            if (ctFixedPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFixedPercentage2.set(ctFixedPercentage);
        }
    }
    
    public CTFixedPercentage insertNewAlphaOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFixedPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.ALPHAOFF$12, n);
        }
    }
    
    public CTFixedPercentage addNewAlphaOff() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFixedPercentage)this.get_store().add_element_user(CTSRgbColorImpl.ALPHAOFF$12);
        }
    }
    
    public void removeAlphaOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.ALPHAOFF$12, n);
        }
    }
    
    public List<CTPositivePercentage> getAlphaModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPositivePercentage>)new CTSRgbColorImpl.AlphaModList(this);
        }
    }
    
    public CTPositivePercentage[] getAlphaModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.ALPHAMOD$14, list);
            final CTPositivePercentage[] array = new CTPositivePercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPositivePercentage getAlphaModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositivePercentage ctPositivePercentage = (CTPositivePercentage)this.get_store().find_element_user(CTSRgbColorImpl.ALPHAMOD$14, n);
            if (ctPositivePercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPositivePercentage;
        }
    }
    
    public int sizeOfAlphaModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.ALPHAMOD$14);
        }
    }
    
    public void setAlphaModArray(final CTPositivePercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.ALPHAMOD$14);
        }
    }
    
    public void setAlphaModArray(final int n, final CTPositivePercentage ctPositivePercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositivePercentage ctPositivePercentage2 = (CTPositivePercentage)this.get_store().find_element_user(CTSRgbColorImpl.ALPHAMOD$14, n);
            if (ctPositivePercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPositivePercentage2.set(ctPositivePercentage);
        }
    }
    
    public CTPositivePercentage insertNewAlphaMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositivePercentage)this.get_store().insert_element_user(CTSRgbColorImpl.ALPHAMOD$14, n);
        }
    }
    
    public CTPositivePercentage addNewAlphaMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositivePercentage)this.get_store().add_element_user(CTSRgbColorImpl.ALPHAMOD$14);
        }
    }
    
    public void removeAlphaMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.ALPHAMOD$14, n);
        }
    }
    
    public List<CTPositiveFixedAngle> getHueList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPositiveFixedAngle>)new CTSRgbColorImpl.HueList(this);
        }
    }
    
    public CTPositiveFixedAngle[] getHueArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.HUE$16, list);
            final CTPositiveFixedAngle[] array = new CTPositiveFixedAngle[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPositiveFixedAngle getHueArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedAngle ctPositiveFixedAngle = (CTPositiveFixedAngle)this.get_store().find_element_user(CTSRgbColorImpl.HUE$16, n);
            if (ctPositiveFixedAngle == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPositiveFixedAngle;
        }
    }
    
    public int sizeOfHueArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.HUE$16);
        }
    }
    
    public void setHueArray(final CTPositiveFixedAngle[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSRgbColorImpl.HUE$16);
        }
    }
    
    public void setHueArray(final int n, final CTPositiveFixedAngle ctPositiveFixedAngle) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositiveFixedAngle ctPositiveFixedAngle2 = (CTPositiveFixedAngle)this.get_store().find_element_user(CTSRgbColorImpl.HUE$16, n);
            if (ctPositiveFixedAngle2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPositiveFixedAngle2.set((XmlObject)ctPositiveFixedAngle);
        }
    }
    
    public CTPositiveFixedAngle insertNewHue(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedAngle)this.get_store().insert_element_user(CTSRgbColorImpl.HUE$16, n);
        }
    }
    
    public CTPositiveFixedAngle addNewHue() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositiveFixedAngle)this.get_store().add_element_user(CTSRgbColorImpl.HUE$16);
        }
    }
    
    public void removeHue(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.HUE$16, n);
        }
    }
    
    public List<CTAngle> getHueOffList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAngle>)new CTSRgbColorImpl.HueOffList(this);
        }
    }
    
    public CTAngle[] getHueOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.HUEOFF$18, list);
            final CTAngle[] array = new CTAngle[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAngle getHueOffArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAngle ctAngle = (CTAngle)this.get_store().find_element_user(CTSRgbColorImpl.HUEOFF$18, n);
            if (ctAngle == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAngle;
        }
    }
    
    public int sizeOfHueOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.HUEOFF$18);
        }
    }
    
    public void setHueOffArray(final CTAngle[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSRgbColorImpl.HUEOFF$18);
        }
    }
    
    public void setHueOffArray(final int n, final CTAngle ctAngle) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAngle ctAngle2 = (CTAngle)this.get_store().find_element_user(CTSRgbColorImpl.HUEOFF$18, n);
            if (ctAngle2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAngle2.set((XmlObject)ctAngle);
        }
    }
    
    public CTAngle insertNewHueOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAngle)this.get_store().insert_element_user(CTSRgbColorImpl.HUEOFF$18, n);
        }
    }
    
    public CTAngle addNewHueOff() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAngle)this.get_store().add_element_user(CTSRgbColorImpl.HUEOFF$18);
        }
    }
    
    public void removeHueOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.HUEOFF$18, n);
        }
    }
    
    public List<CTPositivePercentage> getHueModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPositivePercentage>)new CTSRgbColorImpl.HueModList(this);
        }
    }
    
    public CTPositivePercentage[] getHueModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.HUEMOD$20, list);
            final CTPositivePercentage[] array = new CTPositivePercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPositivePercentage getHueModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositivePercentage ctPositivePercentage = (CTPositivePercentage)this.get_store().find_element_user(CTSRgbColorImpl.HUEMOD$20, n);
            if (ctPositivePercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPositivePercentage;
        }
    }
    
    public int sizeOfHueModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.HUEMOD$20);
        }
    }
    
    public void setHueModArray(final CTPositivePercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.HUEMOD$20);
        }
    }
    
    public void setHueModArray(final int n, final CTPositivePercentage ctPositivePercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPositivePercentage ctPositivePercentage2 = (CTPositivePercentage)this.get_store().find_element_user(CTSRgbColorImpl.HUEMOD$20, n);
            if (ctPositivePercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPositivePercentage2.set(ctPositivePercentage);
        }
    }
    
    public CTPositivePercentage insertNewHueMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositivePercentage)this.get_store().insert_element_user(CTSRgbColorImpl.HUEMOD$20, n);
        }
    }
    
    public CTPositivePercentage addNewHueMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPositivePercentage)this.get_store().add_element_user(CTSRgbColorImpl.HUEMOD$20);
        }
    }
    
    public void removeHueMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.HUEMOD$20, n);
        }
    }
    
    public List<CTPercentage> getSatList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.SatList(this);
        }
    }
    
    public CTPercentage[] getSatArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.SAT$22, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getSatArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SAT$22, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfSatArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.SAT$22);
        }
    }
    
    public void setSatArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.SAT$22);
        }
    }
    
    public void setSatArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SAT$22, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewSat(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.SAT$22, n);
        }
    }
    
    public CTPercentage addNewSat() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.SAT$22);
        }
    }
    
    public void removeSat(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.SAT$22, n);
        }
    }
    
    public List<CTPercentage> getSatOffList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.SatOffList(this);
        }
    }
    
    public CTPercentage[] getSatOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.SATOFF$24, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getSatOffArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SATOFF$24, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfSatOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.SATOFF$24);
        }
    }
    
    public void setSatOffArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.SATOFF$24);
        }
    }
    
    public void setSatOffArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SATOFF$24, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewSatOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.SATOFF$24, n);
        }
    }
    
    public CTPercentage addNewSatOff() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.SATOFF$24);
        }
    }
    
    public void removeSatOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.SATOFF$24, n);
        }
    }
    
    public List<CTPercentage> getSatModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.SatModList(this);
        }
    }
    
    public CTPercentage[] getSatModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.SATMOD$26, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getSatModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SATMOD$26, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfSatModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.SATMOD$26);
        }
    }
    
    public void setSatModArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.SATMOD$26);
        }
    }
    
    public void setSatModArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.SATMOD$26, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewSatMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.SATMOD$26, n);
        }
    }
    
    public CTPercentage addNewSatMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.SATMOD$26);
        }
    }
    
    public void removeSatMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.SATMOD$26, n);
        }
    }
    
    public List<CTPercentage> getLumList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.LumList(this);
        }
    }
    
    public CTPercentage[] getLumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.LUM$28, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getLumArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.LUM$28, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfLumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.LUM$28);
        }
    }
    
    public void setLumArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.LUM$28);
        }
    }
    
    public void setLumArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.LUM$28, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewLum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.LUM$28, n);
        }
    }
    
    public CTPercentage addNewLum() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.LUM$28);
        }
    }
    
    public void removeLum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.LUM$28, n);
        }
    }
    
    public List<CTPercentage> getLumOffList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.LumOffList(this);
        }
    }
    
    public CTPercentage[] getLumOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.LUMOFF$30, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getLumOffArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.LUMOFF$30, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfLumOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.LUMOFF$30);
        }
    }
    
    public void setLumOffArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.LUMOFF$30);
        }
    }
    
    public void setLumOffArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.LUMOFF$30, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewLumOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.LUMOFF$30, n);
        }
    }
    
    public CTPercentage addNewLumOff() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.LUMOFF$30);
        }
    }
    
    public void removeLumOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.LUMOFF$30, n);
        }
    }
    
    public List<CTPercentage> getLumModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.LumModList(this);
        }
    }
    
    public CTPercentage[] getLumModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.LUMMOD$32, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getLumModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.LUMMOD$32, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfLumModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.LUMMOD$32);
        }
    }
    
    public void setLumModArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.LUMMOD$32);
        }
    }
    
    public void setLumModArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.LUMMOD$32, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewLumMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.LUMMOD$32, n);
        }
    }
    
    public CTPercentage addNewLumMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.LUMMOD$32);
        }
    }
    
    public void removeLumMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.LUMMOD$32, n);
        }
    }
    
    public List<CTPercentage> getRedList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.RedList(this);
        }
    }
    
    public CTPercentage[] getRedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.RED$34, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getRedArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.RED$34, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfRedArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.RED$34);
        }
    }
    
    public void setRedArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.RED$34);
        }
    }
    
    public void setRedArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.RED$34, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewRed(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.RED$34, n);
        }
    }
    
    public CTPercentage addNewRed() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.RED$34);
        }
    }
    
    public void removeRed(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.RED$34, n);
        }
    }
    
    public List<CTPercentage> getRedOffList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.RedOffList(this);
        }
    }
    
    public CTPercentage[] getRedOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.REDOFF$36, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getRedOffArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.REDOFF$36, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfRedOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.REDOFF$36);
        }
    }
    
    public void setRedOffArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.REDOFF$36);
        }
    }
    
    public void setRedOffArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.REDOFF$36, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewRedOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.REDOFF$36, n);
        }
    }
    
    public CTPercentage addNewRedOff() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.REDOFF$36);
        }
    }
    
    public void removeRedOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.REDOFF$36, n);
        }
    }
    
    public List<CTPercentage> getRedModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.RedModList(this);
        }
    }
    
    public CTPercentage[] getRedModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.REDMOD$38, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getRedModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.REDMOD$38, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfRedModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.REDMOD$38);
        }
    }
    
    public void setRedModArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.REDMOD$38);
        }
    }
    
    public void setRedModArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.REDMOD$38, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewRedMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.REDMOD$38, n);
        }
    }
    
    public CTPercentage addNewRedMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.REDMOD$38);
        }
    }
    
    public void removeRedMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.REDMOD$38, n);
        }
    }
    
    public List<CTPercentage> getGreenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.GreenList(this);
        }
    }
    
    public CTPercentage[] getGreenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.GREEN$40, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getGreenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.GREEN$40, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfGreenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.GREEN$40);
        }
    }
    
    public void setGreenArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.GREEN$40);
        }
    }
    
    public void setGreenArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.GREEN$40, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewGreen(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.GREEN$40, n);
        }
    }
    
    public CTPercentage addNewGreen() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.GREEN$40);
        }
    }
    
    public void removeGreen(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.GREEN$40, n);
        }
    }
    
    public List<CTPercentage> getGreenOffList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.GreenOffList(this);
        }
    }
    
    public CTPercentage[] getGreenOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.GREENOFF$42, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getGreenOffArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.GREENOFF$42, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfGreenOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.GREENOFF$42);
        }
    }
    
    public void setGreenOffArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.GREENOFF$42);
        }
    }
    
    public void setGreenOffArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.GREENOFF$42, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewGreenOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.GREENOFF$42, n);
        }
    }
    
    public CTPercentage addNewGreenOff() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.GREENOFF$42);
        }
    }
    
    public void removeGreenOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.GREENOFF$42, n);
        }
    }
    
    public List<CTPercentage> getGreenModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.GreenModList(this);
        }
    }
    
    public CTPercentage[] getGreenModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.GREENMOD$44, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getGreenModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.GREENMOD$44, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfGreenModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.GREENMOD$44);
        }
    }
    
    public void setGreenModArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.GREENMOD$44);
        }
    }
    
    public void setGreenModArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.GREENMOD$44, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewGreenMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.GREENMOD$44, n);
        }
    }
    
    public CTPercentage addNewGreenMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.GREENMOD$44);
        }
    }
    
    public void removeGreenMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.GREENMOD$44, n);
        }
    }
    
    public List<CTPercentage> getBlueList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.BlueList(this);
        }
    }
    
    public CTPercentage[] getBlueArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.BLUE$46, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getBlueArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.BLUE$46, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfBlueArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.BLUE$46);
        }
    }
    
    public void setBlueArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.BLUE$46);
        }
    }
    
    public void setBlueArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.BLUE$46, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewBlue(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.BLUE$46, n);
        }
    }
    
    public CTPercentage addNewBlue() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.BLUE$46);
        }
    }
    
    public void removeBlue(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.BLUE$46, n);
        }
    }
    
    public List<CTPercentage> getBlueOffList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.BlueOffList(this);
        }
    }
    
    public CTPercentage[] getBlueOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.BLUEOFF$48, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getBlueOffArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.BLUEOFF$48, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfBlueOffArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.BLUEOFF$48);
        }
    }
    
    public void setBlueOffArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.BLUEOFF$48);
        }
    }
    
    public void setBlueOffArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.BLUEOFF$48, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewBlueOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.BLUEOFF$48, n);
        }
    }
    
    public CTPercentage addNewBlueOff() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.BLUEOFF$48);
        }
    }
    
    public void removeBlueOff(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.BLUEOFF$48, n);
        }
    }
    
    public List<CTPercentage> getBlueModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPercentage>)new CTSRgbColorImpl.BlueModList(this);
        }
    }
    
    public CTPercentage[] getBlueModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.BLUEMOD$50, list);
            final CTPercentage[] array = new CTPercentage[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPercentage getBlueModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.BLUEMOD$50, n);
            if (ctPercentage == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPercentage;
        }
    }
    
    public int sizeOfBlueModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.BLUEMOD$50);
        }
    }
    
    public void setBlueModArray(final CTPercentage[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSRgbColorImpl.BLUEMOD$50);
        }
    }
    
    public void setBlueModArray(final int n, final CTPercentage ctPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPercentage ctPercentage2 = (CTPercentage)this.get_store().find_element_user(CTSRgbColorImpl.BLUEMOD$50, n);
            if (ctPercentage2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPercentage2.set(ctPercentage);
        }
    }
    
    public CTPercentage insertNewBlueMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().insert_element_user(CTSRgbColorImpl.BLUEMOD$50, n);
        }
    }
    
    public CTPercentage addNewBlueMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPercentage)this.get_store().add_element_user(CTSRgbColorImpl.BLUEMOD$50);
        }
    }
    
    public void removeBlueMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.BLUEMOD$50, n);
        }
    }
    
    public List<CTGammaTransform> getGammaList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGammaTransform>)new CTSRgbColorImpl.GammaList(this);
        }
    }
    
    public CTGammaTransform[] getGammaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.GAMMA$52, list);
            final CTGammaTransform[] array = new CTGammaTransform[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGammaTransform getGammaArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGammaTransform ctGammaTransform = (CTGammaTransform)this.get_store().find_element_user(CTSRgbColorImpl.GAMMA$52, n);
            if (ctGammaTransform == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGammaTransform;
        }
    }
    
    public int sizeOfGammaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.GAMMA$52);
        }
    }
    
    public void setGammaArray(final CTGammaTransform[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSRgbColorImpl.GAMMA$52);
        }
    }
    
    public void setGammaArray(final int n, final CTGammaTransform ctGammaTransform) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGammaTransform ctGammaTransform2 = (CTGammaTransform)this.get_store().find_element_user(CTSRgbColorImpl.GAMMA$52, n);
            if (ctGammaTransform2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGammaTransform2.set((XmlObject)ctGammaTransform);
        }
    }
    
    public CTGammaTransform insertNewGamma(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGammaTransform)this.get_store().insert_element_user(CTSRgbColorImpl.GAMMA$52, n);
        }
    }
    
    public CTGammaTransform addNewGamma() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGammaTransform)this.get_store().add_element_user(CTSRgbColorImpl.GAMMA$52);
        }
    }
    
    public void removeGamma(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.GAMMA$52, n);
        }
    }
    
    public List<CTInverseGammaTransform> getInvGammaList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTInverseGammaTransform>)new CTSRgbColorImpl.InvGammaList(this);
        }
    }
    
    public CTInverseGammaTransform[] getInvGammaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSRgbColorImpl.INVGAMMA$54, list);
            final CTInverseGammaTransform[] array = new CTInverseGammaTransform[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTInverseGammaTransform getInvGammaArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTInverseGammaTransform ctInverseGammaTransform = (CTInverseGammaTransform)this.get_store().find_element_user(CTSRgbColorImpl.INVGAMMA$54, n);
            if (ctInverseGammaTransform == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctInverseGammaTransform;
        }
    }
    
    public int sizeOfInvGammaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSRgbColorImpl.INVGAMMA$54);
        }
    }
    
    public void setInvGammaArray(final CTInverseGammaTransform[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSRgbColorImpl.INVGAMMA$54);
        }
    }
    
    public void setInvGammaArray(final int n, final CTInverseGammaTransform ctInverseGammaTransform) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTInverseGammaTransform ctInverseGammaTransform2 = (CTInverseGammaTransform)this.get_store().find_element_user(CTSRgbColorImpl.INVGAMMA$54, n);
            if (ctInverseGammaTransform2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctInverseGammaTransform2.set((XmlObject)ctInverseGammaTransform);
        }
    }
    
    public CTInverseGammaTransform insertNewInvGamma(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTInverseGammaTransform)this.get_store().insert_element_user(CTSRgbColorImpl.INVGAMMA$54, n);
        }
    }
    
    public CTInverseGammaTransform addNewInvGamma() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTInverseGammaTransform)this.get_store().add_element_user(CTSRgbColorImpl.INVGAMMA$54);
        }
    }
    
    public void removeInvGamma(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSRgbColorImpl.INVGAMMA$54, n);
        }
    }
    
    public byte[] getVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTSRgbColorImpl.VAL$56);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getByteArrayValue();
        }
    }
    
    public STHexBinary3 xgetVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STHexBinary3)this.get_store().find_attribute_user(CTSRgbColorImpl.VAL$56);
        }
    }
    
    public void setVal(final byte[] byteArrayValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTSRgbColorImpl.VAL$56);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTSRgbColorImpl.VAL$56);
            }
            simpleValue.setByteArrayValue(byteArrayValue);
        }
    }
    
    public void xsetVal(final STHexBinary3 stHexBinary3) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STHexBinary3 stHexBinary4 = (STHexBinary3)this.get_store().find_attribute_user(CTSRgbColorImpl.VAL$56);
            if (stHexBinary4 == null) {
                stHexBinary4 = (STHexBinary3)this.get_store().add_attribute_user(CTSRgbColorImpl.VAL$56);
            }
            stHexBinary4.set(stHexBinary3);
        }
    }
    
    static {
        TINT$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "tint");
        SHADE$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "shade");
        COMP$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "comp");
        INV$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "inv");
        GRAY$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "gray");
        ALPHA$10 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alpha");
        ALPHAOFF$12 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaOff");
        ALPHAMOD$14 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaMod");
        HUE$16 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "hue");
        HUEOFF$18 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "hueOff");
        HUEMOD$20 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "hueMod");
        SAT$22 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "sat");
        SATOFF$24 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "satOff");
        SATMOD$26 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "satMod");
        LUM$28 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "lum");
        LUMOFF$30 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "lumOff");
        LUMMOD$32 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "lumMod");
        RED$34 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "red");
        REDOFF$36 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "redOff");
        REDMOD$38 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "redMod");
        GREEN$40 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "green");
        GREENOFF$42 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "greenOff");
        GREENMOD$44 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "greenMod");
        BLUE$46 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "blue");
        BLUEOFF$48 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "blueOff");
        BLUEMOD$50 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "blueMod");
        GAMMA$52 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "gamma");
        INVGAMMA$54 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "invGamma");
        VAL$56 = new QName("", "val");
    }
}
