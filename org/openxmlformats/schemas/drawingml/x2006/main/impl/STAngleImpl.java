// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STAngle;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STAngleImpl extends JavaIntHolderEx implements STAngle
{
    public STAngleImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STAngleImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
