// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineJoinRound;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTLineJoinRoundImpl extends XmlComplexContentImpl implements CTLineJoinRound
{
    public CTLineJoinRoundImpl(final SchemaType type) {
        super(type);
    }
}
