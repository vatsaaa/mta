// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextVerticalType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTextVerticalTypeImpl extends JavaStringEnumerationHolderEx implements STTextVerticalType
{
    public STTextVerticalTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextVerticalTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
