// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.openxmlformats.schemas.drawingml.x2006.main.STPositiveFixedPercentage;
import org.apache.xmlbeans.SimpleValue;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSystemColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTHslColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTScRgbColor;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGradientStop;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTGradientStopImpl extends XmlComplexContentImpl implements CTGradientStop
{
    private static final QName SCRGBCLR$0;
    private static final QName SRGBCLR$2;
    private static final QName HSLCLR$4;
    private static final QName SYSCLR$6;
    private static final QName SCHEMECLR$8;
    private static final QName PRSTCLR$10;
    private static final QName POS$12;
    
    public CTGradientStopImpl(final SchemaType type) {
        super(type);
    }
    
    public CTScRgbColor getScrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTScRgbColor ctScRgbColor = (CTScRgbColor)this.get_store().find_element_user(CTGradientStopImpl.SCRGBCLR$0, 0);
            if (ctScRgbColor == null) {
                return null;
            }
            return ctScRgbColor;
        }
    }
    
    public boolean isSetScrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGradientStopImpl.SCRGBCLR$0) != 0;
        }
    }
    
    public void setScrgbClr(final CTScRgbColor ctScRgbColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTScRgbColor ctScRgbColor2 = (CTScRgbColor)this.get_store().find_element_user(CTGradientStopImpl.SCRGBCLR$0, 0);
            if (ctScRgbColor2 == null) {
                ctScRgbColor2 = (CTScRgbColor)this.get_store().add_element_user(CTGradientStopImpl.SCRGBCLR$0);
            }
            ctScRgbColor2.set(ctScRgbColor);
        }
    }
    
    public CTScRgbColor addNewScrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTScRgbColor)this.get_store().add_element_user(CTGradientStopImpl.SCRGBCLR$0);
        }
    }
    
    public void unsetScrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGradientStopImpl.SCRGBCLR$0, 0);
        }
    }
    
    public CTSRgbColor getSrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSRgbColor ctsRgbColor = (CTSRgbColor)this.get_store().find_element_user(CTGradientStopImpl.SRGBCLR$2, 0);
            if (ctsRgbColor == null) {
                return null;
            }
            return ctsRgbColor;
        }
    }
    
    public boolean isSetSrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGradientStopImpl.SRGBCLR$2) != 0;
        }
    }
    
    public void setSrgbClr(final CTSRgbColor ctsRgbColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSRgbColor ctsRgbColor2 = (CTSRgbColor)this.get_store().find_element_user(CTGradientStopImpl.SRGBCLR$2, 0);
            if (ctsRgbColor2 == null) {
                ctsRgbColor2 = (CTSRgbColor)this.get_store().add_element_user(CTGradientStopImpl.SRGBCLR$2);
            }
            ctsRgbColor2.set(ctsRgbColor);
        }
    }
    
    public CTSRgbColor addNewSrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSRgbColor)this.get_store().add_element_user(CTGradientStopImpl.SRGBCLR$2);
        }
    }
    
    public void unsetSrgbClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGradientStopImpl.SRGBCLR$2, 0);
        }
    }
    
    public CTHslColor getHslClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHslColor ctHslColor = (CTHslColor)this.get_store().find_element_user(CTGradientStopImpl.HSLCLR$4, 0);
            if (ctHslColor == null) {
                return null;
            }
            return ctHslColor;
        }
    }
    
    public boolean isSetHslClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGradientStopImpl.HSLCLR$4) != 0;
        }
    }
    
    public void setHslClr(final CTHslColor ctHslColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTHslColor ctHslColor2 = (CTHslColor)this.get_store().find_element_user(CTGradientStopImpl.HSLCLR$4, 0);
            if (ctHslColor2 == null) {
                ctHslColor2 = (CTHslColor)this.get_store().add_element_user(CTGradientStopImpl.HSLCLR$4);
            }
            ctHslColor2.set(ctHslColor);
        }
    }
    
    public CTHslColor addNewHslClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHslColor)this.get_store().add_element_user(CTGradientStopImpl.HSLCLR$4);
        }
    }
    
    public void unsetHslClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGradientStopImpl.HSLCLR$4, 0);
        }
    }
    
    public CTSystemColor getSysClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSystemColor ctSystemColor = (CTSystemColor)this.get_store().find_element_user(CTGradientStopImpl.SYSCLR$6, 0);
            if (ctSystemColor == null) {
                return null;
            }
            return ctSystemColor;
        }
    }
    
    public boolean isSetSysClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGradientStopImpl.SYSCLR$6) != 0;
        }
    }
    
    public void setSysClr(final CTSystemColor ctSystemColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSystemColor ctSystemColor2 = (CTSystemColor)this.get_store().find_element_user(CTGradientStopImpl.SYSCLR$6, 0);
            if (ctSystemColor2 == null) {
                ctSystemColor2 = (CTSystemColor)this.get_store().add_element_user(CTGradientStopImpl.SYSCLR$6);
            }
            ctSystemColor2.set(ctSystemColor);
        }
    }
    
    public CTSystemColor addNewSysClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSystemColor)this.get_store().add_element_user(CTGradientStopImpl.SYSCLR$6);
        }
    }
    
    public void unsetSysClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGradientStopImpl.SYSCLR$6, 0);
        }
    }
    
    public CTSchemeColor getSchemeClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSchemeColor ctSchemeColor = (CTSchemeColor)this.get_store().find_element_user(CTGradientStopImpl.SCHEMECLR$8, 0);
            if (ctSchemeColor == null) {
                return null;
            }
            return ctSchemeColor;
        }
    }
    
    public boolean isSetSchemeClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGradientStopImpl.SCHEMECLR$8) != 0;
        }
    }
    
    public void setSchemeClr(final CTSchemeColor ctSchemeColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSchemeColor ctSchemeColor2 = (CTSchemeColor)this.get_store().find_element_user(CTGradientStopImpl.SCHEMECLR$8, 0);
            if (ctSchemeColor2 == null) {
                ctSchemeColor2 = (CTSchemeColor)this.get_store().add_element_user(CTGradientStopImpl.SCHEMECLR$8);
            }
            ctSchemeColor2.set(ctSchemeColor);
        }
    }
    
    public CTSchemeColor addNewSchemeClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSchemeColor)this.get_store().add_element_user(CTGradientStopImpl.SCHEMECLR$8);
        }
    }
    
    public void unsetSchemeClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGradientStopImpl.SCHEMECLR$8, 0);
        }
    }
    
    public CTPresetColor getPrstClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPresetColor ctPresetColor = (CTPresetColor)this.get_store().find_element_user(CTGradientStopImpl.PRSTCLR$10, 0);
            if (ctPresetColor == null) {
                return null;
            }
            return ctPresetColor;
        }
    }
    
    public boolean isSetPrstClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTGradientStopImpl.PRSTCLR$10) != 0;
        }
    }
    
    public void setPrstClr(final CTPresetColor ctPresetColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPresetColor ctPresetColor2 = (CTPresetColor)this.get_store().find_element_user(CTGradientStopImpl.PRSTCLR$10, 0);
            if (ctPresetColor2 == null) {
                ctPresetColor2 = (CTPresetColor)this.get_store().add_element_user(CTGradientStopImpl.PRSTCLR$10);
            }
            ctPresetColor2.set(ctPresetColor);
        }
    }
    
    public CTPresetColor addNewPrstClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPresetColor)this.get_store().add_element_user(CTGradientStopImpl.PRSTCLR$10);
        }
    }
    
    public void unsetPrstClr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTGradientStopImpl.PRSTCLR$10, 0);
        }
    }
    
    public int getPos() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGradientStopImpl.POS$12);
            if (simpleValue == null) {
                return 0;
            }
            return simpleValue.getIntValue();
        }
    }
    
    public STPositiveFixedPercentage xgetPos() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STPositiveFixedPercentage)this.get_store().find_attribute_user(CTGradientStopImpl.POS$12);
        }
    }
    
    public void setPos(final int intValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTGradientStopImpl.POS$12);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTGradientStopImpl.POS$12);
            }
            simpleValue.setIntValue(intValue);
        }
    }
    
    public void xsetPos(final STPositiveFixedPercentage stPositiveFixedPercentage) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STPositiveFixedPercentage stPositiveFixedPercentage2 = (STPositiveFixedPercentage)this.get_store().find_attribute_user(CTGradientStopImpl.POS$12);
            if (stPositiveFixedPercentage2 == null) {
                stPositiveFixedPercentage2 = (STPositiveFixedPercentage)this.get_store().add_attribute_user(CTGradientStopImpl.POS$12);
            }
            stPositiveFixedPercentage2.set(stPositiveFixedPercentage);
        }
    }
    
    static {
        SCRGBCLR$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "scrgbClr");
        SRGBCLR$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "srgbClr");
        HSLCLR$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "hslClr");
        SYSCLR$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "sysClr");
        SCHEMECLR$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "schemeClr");
        PRSTCLR$10 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "prstClr");
        POS$12 = new QName("", "pos");
    }
}
