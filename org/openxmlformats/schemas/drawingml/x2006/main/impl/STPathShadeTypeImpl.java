// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPathShadeType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STPathShadeTypeImpl extends JavaStringEnumerationHolderEx implements STPathShadeType
{
    public STPathShadeTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPathShadeTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
