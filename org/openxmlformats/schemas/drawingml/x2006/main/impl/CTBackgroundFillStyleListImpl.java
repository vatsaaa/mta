// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.openxmlformats.schemas.drawingml.x2006.main.CTGroupFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPatternFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlipFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGradientFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSolidColorFillProperties;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNoFillProperties;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBackgroundFillStyleList;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTBackgroundFillStyleListImpl extends XmlComplexContentImpl implements CTBackgroundFillStyleList
{
    private static final QName NOFILL$0;
    private static final QName SOLIDFILL$2;
    private static final QName GRADFILL$4;
    private static final QName BLIPFILL$6;
    private static final QName PATTFILL$8;
    private static final QName GRPFILL$10;
    
    public CTBackgroundFillStyleListImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTNoFillProperties> getNoFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTNoFillProperties>)new CTBackgroundFillStyleListImpl.NoFillList(this);
        }
    }
    
    public CTNoFillProperties[] getNoFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBackgroundFillStyleListImpl.NOFILL$0, list);
            final CTNoFillProperties[] array = new CTNoFillProperties[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTNoFillProperties getNoFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNoFillProperties ctNoFillProperties = (CTNoFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.NOFILL$0, n);
            if (ctNoFillProperties == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctNoFillProperties;
        }
    }
    
    public int sizeOfNoFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBackgroundFillStyleListImpl.NOFILL$0);
        }
    }
    
    public void setNoFillArray(final CTNoFillProperties[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTBackgroundFillStyleListImpl.NOFILL$0);
        }
    }
    
    public void setNoFillArray(final int n, final CTNoFillProperties ctNoFillProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNoFillProperties ctNoFillProperties2 = (CTNoFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.NOFILL$0, n);
            if (ctNoFillProperties2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctNoFillProperties2.set(ctNoFillProperties);
        }
    }
    
    public CTNoFillProperties insertNewNoFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNoFillProperties)this.get_store().insert_element_user(CTBackgroundFillStyleListImpl.NOFILL$0, n);
        }
    }
    
    public CTNoFillProperties addNewNoFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNoFillProperties)this.get_store().add_element_user(CTBackgroundFillStyleListImpl.NOFILL$0);
        }
    }
    
    public void removeNoFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBackgroundFillStyleListImpl.NOFILL$0, n);
        }
    }
    
    public List<CTSolidColorFillProperties> getSolidFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSolidColorFillProperties>)new CTBackgroundFillStyleListImpl.SolidFillList(this);
        }
    }
    
    public CTSolidColorFillProperties[] getSolidFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBackgroundFillStyleListImpl.SOLIDFILL$2, list);
            final CTSolidColorFillProperties[] array = new CTSolidColorFillProperties[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSolidColorFillProperties getSolidFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSolidColorFillProperties ctSolidColorFillProperties = (CTSolidColorFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.SOLIDFILL$2, n);
            if (ctSolidColorFillProperties == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSolidColorFillProperties;
        }
    }
    
    public int sizeOfSolidFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBackgroundFillStyleListImpl.SOLIDFILL$2);
        }
    }
    
    public void setSolidFillArray(final CTSolidColorFillProperties[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTBackgroundFillStyleListImpl.SOLIDFILL$2);
        }
    }
    
    public void setSolidFillArray(final int n, final CTSolidColorFillProperties ctSolidColorFillProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSolidColorFillProperties ctSolidColorFillProperties2 = (CTSolidColorFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.SOLIDFILL$2, n);
            if (ctSolidColorFillProperties2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSolidColorFillProperties2.set(ctSolidColorFillProperties);
        }
    }
    
    public CTSolidColorFillProperties insertNewSolidFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSolidColorFillProperties)this.get_store().insert_element_user(CTBackgroundFillStyleListImpl.SOLIDFILL$2, n);
        }
    }
    
    public CTSolidColorFillProperties addNewSolidFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSolidColorFillProperties)this.get_store().add_element_user(CTBackgroundFillStyleListImpl.SOLIDFILL$2);
        }
    }
    
    public void removeSolidFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBackgroundFillStyleListImpl.SOLIDFILL$2, n);
        }
    }
    
    public List<CTGradientFillProperties> getGradFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGradientFillProperties>)new CTBackgroundFillStyleListImpl.GradFillList(this);
        }
    }
    
    public CTGradientFillProperties[] getGradFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBackgroundFillStyleListImpl.GRADFILL$4, list);
            final CTGradientFillProperties[] array = new CTGradientFillProperties[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGradientFillProperties getGradFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGradientFillProperties ctGradientFillProperties = (CTGradientFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.GRADFILL$4, n);
            if (ctGradientFillProperties == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGradientFillProperties;
        }
    }
    
    public int sizeOfGradFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBackgroundFillStyleListImpl.GRADFILL$4);
        }
    }
    
    public void setGradFillArray(final CTGradientFillProperties[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTBackgroundFillStyleListImpl.GRADFILL$4);
        }
    }
    
    public void setGradFillArray(final int n, final CTGradientFillProperties ctGradientFillProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGradientFillProperties ctGradientFillProperties2 = (CTGradientFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.GRADFILL$4, n);
            if (ctGradientFillProperties2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGradientFillProperties2.set(ctGradientFillProperties);
        }
    }
    
    public CTGradientFillProperties insertNewGradFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGradientFillProperties)this.get_store().insert_element_user(CTBackgroundFillStyleListImpl.GRADFILL$4, n);
        }
    }
    
    public CTGradientFillProperties addNewGradFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGradientFillProperties)this.get_store().add_element_user(CTBackgroundFillStyleListImpl.GRADFILL$4);
        }
    }
    
    public void removeGradFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBackgroundFillStyleListImpl.GRADFILL$4, n);
        }
    }
    
    public List<CTBlipFillProperties> getBlipFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBlipFillProperties>)new CTBackgroundFillStyleListImpl.BlipFillList(this);
        }
    }
    
    public CTBlipFillProperties[] getBlipFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBackgroundFillStyleListImpl.BLIPFILL$6, list);
            final CTBlipFillProperties[] array = new CTBlipFillProperties[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBlipFillProperties getBlipFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBlipFillProperties ctBlipFillProperties = (CTBlipFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.BLIPFILL$6, n);
            if (ctBlipFillProperties == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBlipFillProperties;
        }
    }
    
    public int sizeOfBlipFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBackgroundFillStyleListImpl.BLIPFILL$6);
        }
    }
    
    public void setBlipFillArray(final CTBlipFillProperties[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTBackgroundFillStyleListImpl.BLIPFILL$6);
        }
    }
    
    public void setBlipFillArray(final int n, final CTBlipFillProperties ctBlipFillProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBlipFillProperties ctBlipFillProperties2 = (CTBlipFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.BLIPFILL$6, n);
            if (ctBlipFillProperties2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBlipFillProperties2.set(ctBlipFillProperties);
        }
    }
    
    public CTBlipFillProperties insertNewBlipFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBlipFillProperties)this.get_store().insert_element_user(CTBackgroundFillStyleListImpl.BLIPFILL$6, n);
        }
    }
    
    public CTBlipFillProperties addNewBlipFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBlipFillProperties)this.get_store().add_element_user(CTBackgroundFillStyleListImpl.BLIPFILL$6);
        }
    }
    
    public void removeBlipFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBackgroundFillStyleListImpl.BLIPFILL$6, n);
        }
    }
    
    public List<CTPatternFillProperties> getPattFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPatternFillProperties>)new CTBackgroundFillStyleListImpl.PattFillList(this);
        }
    }
    
    public CTPatternFillProperties[] getPattFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBackgroundFillStyleListImpl.PATTFILL$8, list);
            final CTPatternFillProperties[] array = new CTPatternFillProperties[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPatternFillProperties getPattFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPatternFillProperties ctPatternFillProperties = (CTPatternFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.PATTFILL$8, n);
            if (ctPatternFillProperties == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPatternFillProperties;
        }
    }
    
    public int sizeOfPattFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBackgroundFillStyleListImpl.PATTFILL$8);
        }
    }
    
    public void setPattFillArray(final CTPatternFillProperties[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBackgroundFillStyleListImpl.PATTFILL$8);
        }
    }
    
    public void setPattFillArray(final int n, final CTPatternFillProperties ctPatternFillProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPatternFillProperties ctPatternFillProperties2 = (CTPatternFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.PATTFILL$8, n);
            if (ctPatternFillProperties2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPatternFillProperties2.set((XmlObject)ctPatternFillProperties);
        }
    }
    
    public CTPatternFillProperties insertNewPattFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPatternFillProperties)this.get_store().insert_element_user(CTBackgroundFillStyleListImpl.PATTFILL$8, n);
        }
    }
    
    public CTPatternFillProperties addNewPattFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPatternFillProperties)this.get_store().add_element_user(CTBackgroundFillStyleListImpl.PATTFILL$8);
        }
    }
    
    public void removePattFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBackgroundFillStyleListImpl.PATTFILL$8, n);
        }
    }
    
    public List<CTGroupFillProperties> getGrpFillList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGroupFillProperties>)new CTBackgroundFillStyleListImpl.GrpFillList(this);
        }
    }
    
    public CTGroupFillProperties[] getGrpFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBackgroundFillStyleListImpl.GRPFILL$10, list);
            final CTGroupFillProperties[] array = new CTGroupFillProperties[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGroupFillProperties getGrpFillArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupFillProperties ctGroupFillProperties = (CTGroupFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.GRPFILL$10, n);
            if (ctGroupFillProperties == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGroupFillProperties;
        }
    }
    
    public int sizeOfGrpFillArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBackgroundFillStyleListImpl.GRPFILL$10);
        }
    }
    
    public void setGrpFillArray(final CTGroupFillProperties[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBackgroundFillStyleListImpl.GRPFILL$10);
        }
    }
    
    public void setGrpFillArray(final int n, final CTGroupFillProperties ctGroupFillProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupFillProperties ctGroupFillProperties2 = (CTGroupFillProperties)this.get_store().find_element_user(CTBackgroundFillStyleListImpl.GRPFILL$10, n);
            if (ctGroupFillProperties2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGroupFillProperties2.set((XmlObject)ctGroupFillProperties);
        }
    }
    
    public CTGroupFillProperties insertNewGrpFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupFillProperties)this.get_store().insert_element_user(CTBackgroundFillStyleListImpl.GRPFILL$10, n);
        }
    }
    
    public CTGroupFillProperties addNewGrpFill() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupFillProperties)this.get_store().add_element_user(CTBackgroundFillStyleListImpl.GRPFILL$10);
        }
    }
    
    public void removeGrpFill(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBackgroundFillStyleListImpl.GRPFILL$10, n);
        }
    }
    
    static {
        NOFILL$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "noFill");
        SOLIDFILL$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "solidFill");
        GRADFILL$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "gradFill");
        BLIPFILL$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "blipFill");
        PATTFILL$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "pattFill");
        GRPFILL$10 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "grpFill");
    }
}
