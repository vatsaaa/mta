// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.StringEnumAbstractBase;
import org.openxmlformats.schemas.drawingml.x2006.main.STBlipCompression;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.SimpleValue;
import org.openxmlformats.schemas.drawingml.x2006.main.CTOfficeArtExtensionList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTintEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLuminanceEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTHSLEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGrayscaleEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTFillOverlayEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTDuotoneEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorReplaceEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorChangeEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlurEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBiLevelEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAlphaReplaceEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAlphaModulateFixedEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAlphaModulateEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAlphaInverseEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAlphaFloorEffect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAlphaCeilingEffect;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAlphaBiLevelEffect;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlip;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTBlipImpl extends XmlComplexContentImpl implements CTBlip
{
    private static final QName ALPHABILEVEL$0;
    private static final QName ALPHACEILING$2;
    private static final QName ALPHAFLOOR$4;
    private static final QName ALPHAINV$6;
    private static final QName ALPHAMOD$8;
    private static final QName ALPHAMODFIX$10;
    private static final QName ALPHAREPL$12;
    private static final QName BILEVEL$14;
    private static final QName BLUR$16;
    private static final QName CLRCHANGE$18;
    private static final QName CLRREPL$20;
    private static final QName DUOTONE$22;
    private static final QName FILLOVERLAY$24;
    private static final QName GRAYSCL$26;
    private static final QName HSL$28;
    private static final QName LUM$30;
    private static final QName TINT$32;
    private static final QName EXTLST$34;
    private static final QName EMBED$36;
    private static final QName LINK$38;
    private static final QName CSTATE$40;
    
    public CTBlipImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTAlphaBiLevelEffect> getAlphaBiLevelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAlphaBiLevelEffect>)new CTBlipImpl.AlphaBiLevelList(this);
        }
    }
    
    public CTAlphaBiLevelEffect[] getAlphaBiLevelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.ALPHABILEVEL$0, list);
            final CTAlphaBiLevelEffect[] array = new CTAlphaBiLevelEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAlphaBiLevelEffect getAlphaBiLevelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaBiLevelEffect ctAlphaBiLevelEffect = (CTAlphaBiLevelEffect)this.get_store().find_element_user(CTBlipImpl.ALPHABILEVEL$0, n);
            if (ctAlphaBiLevelEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAlphaBiLevelEffect;
        }
    }
    
    public int sizeOfAlphaBiLevelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.ALPHABILEVEL$0);
        }
    }
    
    public void setAlphaBiLevelArray(final CTAlphaBiLevelEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.ALPHABILEVEL$0);
        }
    }
    
    public void setAlphaBiLevelArray(final int n, final CTAlphaBiLevelEffect ctAlphaBiLevelEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaBiLevelEffect ctAlphaBiLevelEffect2 = (CTAlphaBiLevelEffect)this.get_store().find_element_user(CTBlipImpl.ALPHABILEVEL$0, n);
            if (ctAlphaBiLevelEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAlphaBiLevelEffect2.set((XmlObject)ctAlphaBiLevelEffect);
        }
    }
    
    public CTAlphaBiLevelEffect insertNewAlphaBiLevel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaBiLevelEffect)this.get_store().insert_element_user(CTBlipImpl.ALPHABILEVEL$0, n);
        }
    }
    
    public CTAlphaBiLevelEffect addNewAlphaBiLevel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaBiLevelEffect)this.get_store().add_element_user(CTBlipImpl.ALPHABILEVEL$0);
        }
    }
    
    public void removeAlphaBiLevel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.ALPHABILEVEL$0, n);
        }
    }
    
    public List<CTAlphaCeilingEffect> getAlphaCeilingList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAlphaCeilingEffect>)new CTBlipImpl.AlphaCeilingList(this);
        }
    }
    
    public CTAlphaCeilingEffect[] getAlphaCeilingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.ALPHACEILING$2, list);
            final CTAlphaCeilingEffect[] array = new CTAlphaCeilingEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAlphaCeilingEffect getAlphaCeilingArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaCeilingEffect ctAlphaCeilingEffect = (CTAlphaCeilingEffect)this.get_store().find_element_user(CTBlipImpl.ALPHACEILING$2, n);
            if (ctAlphaCeilingEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAlphaCeilingEffect;
        }
    }
    
    public int sizeOfAlphaCeilingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.ALPHACEILING$2);
        }
    }
    
    public void setAlphaCeilingArray(final CTAlphaCeilingEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.ALPHACEILING$2);
        }
    }
    
    public void setAlphaCeilingArray(final int n, final CTAlphaCeilingEffect ctAlphaCeilingEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaCeilingEffect ctAlphaCeilingEffect2 = (CTAlphaCeilingEffect)this.get_store().find_element_user(CTBlipImpl.ALPHACEILING$2, n);
            if (ctAlphaCeilingEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAlphaCeilingEffect2.set((XmlObject)ctAlphaCeilingEffect);
        }
    }
    
    public CTAlphaCeilingEffect insertNewAlphaCeiling(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaCeilingEffect)this.get_store().insert_element_user(CTBlipImpl.ALPHACEILING$2, n);
        }
    }
    
    public CTAlphaCeilingEffect addNewAlphaCeiling() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaCeilingEffect)this.get_store().add_element_user(CTBlipImpl.ALPHACEILING$2);
        }
    }
    
    public void removeAlphaCeiling(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.ALPHACEILING$2, n);
        }
    }
    
    public List<CTAlphaFloorEffect> getAlphaFloorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAlphaFloorEffect>)new CTBlipImpl.AlphaFloorList(this);
        }
    }
    
    public CTAlphaFloorEffect[] getAlphaFloorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.ALPHAFLOOR$4, list);
            final CTAlphaFloorEffect[] array = new CTAlphaFloorEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAlphaFloorEffect getAlphaFloorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaFloorEffect ctAlphaFloorEffect = (CTAlphaFloorEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAFLOOR$4, n);
            if (ctAlphaFloorEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAlphaFloorEffect;
        }
    }
    
    public int sizeOfAlphaFloorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.ALPHAFLOOR$4);
        }
    }
    
    public void setAlphaFloorArray(final CTAlphaFloorEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.ALPHAFLOOR$4);
        }
    }
    
    public void setAlphaFloorArray(final int n, final CTAlphaFloorEffect ctAlphaFloorEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaFloorEffect ctAlphaFloorEffect2 = (CTAlphaFloorEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAFLOOR$4, n);
            if (ctAlphaFloorEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAlphaFloorEffect2.set((XmlObject)ctAlphaFloorEffect);
        }
    }
    
    public CTAlphaFloorEffect insertNewAlphaFloor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaFloorEffect)this.get_store().insert_element_user(CTBlipImpl.ALPHAFLOOR$4, n);
        }
    }
    
    public CTAlphaFloorEffect addNewAlphaFloor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaFloorEffect)this.get_store().add_element_user(CTBlipImpl.ALPHAFLOOR$4);
        }
    }
    
    public void removeAlphaFloor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.ALPHAFLOOR$4, n);
        }
    }
    
    public List<CTAlphaInverseEffect> getAlphaInvList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAlphaInverseEffect>)new CTBlipImpl.AlphaInvList(this);
        }
    }
    
    public CTAlphaInverseEffect[] getAlphaInvArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.ALPHAINV$6, list);
            final CTAlphaInverseEffect[] array = new CTAlphaInverseEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAlphaInverseEffect getAlphaInvArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaInverseEffect ctAlphaInverseEffect = (CTAlphaInverseEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAINV$6, n);
            if (ctAlphaInverseEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAlphaInverseEffect;
        }
    }
    
    public int sizeOfAlphaInvArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.ALPHAINV$6);
        }
    }
    
    public void setAlphaInvArray(final CTAlphaInverseEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.ALPHAINV$6);
        }
    }
    
    public void setAlphaInvArray(final int n, final CTAlphaInverseEffect ctAlphaInverseEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaInverseEffect ctAlphaInverseEffect2 = (CTAlphaInverseEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAINV$6, n);
            if (ctAlphaInverseEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAlphaInverseEffect2.set((XmlObject)ctAlphaInverseEffect);
        }
    }
    
    public CTAlphaInverseEffect insertNewAlphaInv(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaInverseEffect)this.get_store().insert_element_user(CTBlipImpl.ALPHAINV$6, n);
        }
    }
    
    public CTAlphaInverseEffect addNewAlphaInv() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaInverseEffect)this.get_store().add_element_user(CTBlipImpl.ALPHAINV$6);
        }
    }
    
    public void removeAlphaInv(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.ALPHAINV$6, n);
        }
    }
    
    public List<CTAlphaModulateEffect> getAlphaModList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAlphaModulateEffect>)new CTBlipImpl.AlphaModList(this);
        }
    }
    
    public CTAlphaModulateEffect[] getAlphaModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.ALPHAMOD$8, list);
            final CTAlphaModulateEffect[] array = new CTAlphaModulateEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAlphaModulateEffect getAlphaModArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaModulateEffect ctAlphaModulateEffect = (CTAlphaModulateEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAMOD$8, n);
            if (ctAlphaModulateEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAlphaModulateEffect;
        }
    }
    
    public int sizeOfAlphaModArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.ALPHAMOD$8);
        }
    }
    
    public void setAlphaModArray(final CTAlphaModulateEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.ALPHAMOD$8);
        }
    }
    
    public void setAlphaModArray(final int n, final CTAlphaModulateEffect ctAlphaModulateEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaModulateEffect ctAlphaModulateEffect2 = (CTAlphaModulateEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAMOD$8, n);
            if (ctAlphaModulateEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAlphaModulateEffect2.set((XmlObject)ctAlphaModulateEffect);
        }
    }
    
    public CTAlphaModulateEffect insertNewAlphaMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaModulateEffect)this.get_store().insert_element_user(CTBlipImpl.ALPHAMOD$8, n);
        }
    }
    
    public CTAlphaModulateEffect addNewAlphaMod() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaModulateEffect)this.get_store().add_element_user(CTBlipImpl.ALPHAMOD$8);
        }
    }
    
    public void removeAlphaMod(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.ALPHAMOD$8, n);
        }
    }
    
    public List<CTAlphaModulateFixedEffect> getAlphaModFixList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAlphaModulateFixedEffect>)new CTBlipImpl.AlphaModFixList(this);
        }
    }
    
    public CTAlphaModulateFixedEffect[] getAlphaModFixArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.ALPHAMODFIX$10, list);
            final CTAlphaModulateFixedEffect[] array = new CTAlphaModulateFixedEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAlphaModulateFixedEffect getAlphaModFixArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaModulateFixedEffect ctAlphaModulateFixedEffect = (CTAlphaModulateFixedEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAMODFIX$10, n);
            if (ctAlphaModulateFixedEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAlphaModulateFixedEffect;
        }
    }
    
    public int sizeOfAlphaModFixArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.ALPHAMODFIX$10);
        }
    }
    
    public void setAlphaModFixArray(final CTAlphaModulateFixedEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTBlipImpl.ALPHAMODFIX$10);
        }
    }
    
    public void setAlphaModFixArray(final int n, final CTAlphaModulateFixedEffect ctAlphaModulateFixedEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaModulateFixedEffect ctAlphaModulateFixedEffect2 = (CTAlphaModulateFixedEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAMODFIX$10, n);
            if (ctAlphaModulateFixedEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAlphaModulateFixedEffect2.set(ctAlphaModulateFixedEffect);
        }
    }
    
    public CTAlphaModulateFixedEffect insertNewAlphaModFix(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaModulateFixedEffect)this.get_store().insert_element_user(CTBlipImpl.ALPHAMODFIX$10, n);
        }
    }
    
    public CTAlphaModulateFixedEffect addNewAlphaModFix() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaModulateFixedEffect)this.get_store().add_element_user(CTBlipImpl.ALPHAMODFIX$10);
        }
    }
    
    public void removeAlphaModFix(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.ALPHAMODFIX$10, n);
        }
    }
    
    public List<CTAlphaReplaceEffect> getAlphaReplList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAlphaReplaceEffect>)new CTBlipImpl.AlphaReplList(this);
        }
    }
    
    public CTAlphaReplaceEffect[] getAlphaReplArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.ALPHAREPL$12, list);
            final CTAlphaReplaceEffect[] array = new CTAlphaReplaceEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAlphaReplaceEffect getAlphaReplArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaReplaceEffect ctAlphaReplaceEffect = (CTAlphaReplaceEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAREPL$12, n);
            if (ctAlphaReplaceEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAlphaReplaceEffect;
        }
    }
    
    public int sizeOfAlphaReplArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.ALPHAREPL$12);
        }
    }
    
    public void setAlphaReplArray(final CTAlphaReplaceEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.ALPHAREPL$12);
        }
    }
    
    public void setAlphaReplArray(final int n, final CTAlphaReplaceEffect ctAlphaReplaceEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAlphaReplaceEffect ctAlphaReplaceEffect2 = (CTAlphaReplaceEffect)this.get_store().find_element_user(CTBlipImpl.ALPHAREPL$12, n);
            if (ctAlphaReplaceEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAlphaReplaceEffect2.set((XmlObject)ctAlphaReplaceEffect);
        }
    }
    
    public CTAlphaReplaceEffect insertNewAlphaRepl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaReplaceEffect)this.get_store().insert_element_user(CTBlipImpl.ALPHAREPL$12, n);
        }
    }
    
    public CTAlphaReplaceEffect addNewAlphaRepl() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAlphaReplaceEffect)this.get_store().add_element_user(CTBlipImpl.ALPHAREPL$12);
        }
    }
    
    public void removeAlphaRepl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.ALPHAREPL$12, n);
        }
    }
    
    public List<CTBiLevelEffect> getBiLevelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBiLevelEffect>)new CTBlipImpl.BiLevelList(this);
        }
    }
    
    public CTBiLevelEffect[] getBiLevelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.BILEVEL$14, list);
            final CTBiLevelEffect[] array = new CTBiLevelEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBiLevelEffect getBiLevelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBiLevelEffect ctBiLevelEffect = (CTBiLevelEffect)this.get_store().find_element_user(CTBlipImpl.BILEVEL$14, n);
            if (ctBiLevelEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBiLevelEffect;
        }
    }
    
    public int sizeOfBiLevelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.BILEVEL$14);
        }
    }
    
    public void setBiLevelArray(final CTBiLevelEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.BILEVEL$14);
        }
    }
    
    public void setBiLevelArray(final int n, final CTBiLevelEffect ctBiLevelEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBiLevelEffect ctBiLevelEffect2 = (CTBiLevelEffect)this.get_store().find_element_user(CTBlipImpl.BILEVEL$14, n);
            if (ctBiLevelEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBiLevelEffect2.set((XmlObject)ctBiLevelEffect);
        }
    }
    
    public CTBiLevelEffect insertNewBiLevel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBiLevelEffect)this.get_store().insert_element_user(CTBlipImpl.BILEVEL$14, n);
        }
    }
    
    public CTBiLevelEffect addNewBiLevel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBiLevelEffect)this.get_store().add_element_user(CTBlipImpl.BILEVEL$14);
        }
    }
    
    public void removeBiLevel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.BILEVEL$14, n);
        }
    }
    
    public List<CTBlurEffect> getBlurList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBlurEffect>)new CTBlipImpl.BlurList(this);
        }
    }
    
    public CTBlurEffect[] getBlurArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.BLUR$16, list);
            final CTBlurEffect[] array = new CTBlurEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBlurEffect getBlurArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBlurEffect ctBlurEffect = (CTBlurEffect)this.get_store().find_element_user(CTBlipImpl.BLUR$16, n);
            if (ctBlurEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBlurEffect;
        }
    }
    
    public int sizeOfBlurArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.BLUR$16);
        }
    }
    
    public void setBlurArray(final CTBlurEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.BLUR$16);
        }
    }
    
    public void setBlurArray(final int n, final CTBlurEffect ctBlurEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBlurEffect ctBlurEffect2 = (CTBlurEffect)this.get_store().find_element_user(CTBlipImpl.BLUR$16, n);
            if (ctBlurEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBlurEffect2.set((XmlObject)ctBlurEffect);
        }
    }
    
    public CTBlurEffect insertNewBlur(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBlurEffect)this.get_store().insert_element_user(CTBlipImpl.BLUR$16, n);
        }
    }
    
    public CTBlurEffect addNewBlur() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBlurEffect)this.get_store().add_element_user(CTBlipImpl.BLUR$16);
        }
    }
    
    public void removeBlur(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.BLUR$16, n);
        }
    }
    
    public List<CTColorChangeEffect> getClrChangeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTColorChangeEffect>)new CTBlipImpl.ClrChangeList(this);
        }
    }
    
    public CTColorChangeEffect[] getClrChangeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.CLRCHANGE$18, list);
            final CTColorChangeEffect[] array = new CTColorChangeEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTColorChangeEffect getClrChangeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColorChangeEffect ctColorChangeEffect = (CTColorChangeEffect)this.get_store().find_element_user(CTBlipImpl.CLRCHANGE$18, n);
            if (ctColorChangeEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctColorChangeEffect;
        }
    }
    
    public int sizeOfClrChangeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.CLRCHANGE$18);
        }
    }
    
    public void setClrChangeArray(final CTColorChangeEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.CLRCHANGE$18);
        }
    }
    
    public void setClrChangeArray(final int n, final CTColorChangeEffect ctColorChangeEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColorChangeEffect ctColorChangeEffect2 = (CTColorChangeEffect)this.get_store().find_element_user(CTBlipImpl.CLRCHANGE$18, n);
            if (ctColorChangeEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctColorChangeEffect2.set((XmlObject)ctColorChangeEffect);
        }
    }
    
    public CTColorChangeEffect insertNewClrChange(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColorChangeEffect)this.get_store().insert_element_user(CTBlipImpl.CLRCHANGE$18, n);
        }
    }
    
    public CTColorChangeEffect addNewClrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColorChangeEffect)this.get_store().add_element_user(CTBlipImpl.CLRCHANGE$18);
        }
    }
    
    public void removeClrChange(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.CLRCHANGE$18, n);
        }
    }
    
    public List<CTColorReplaceEffect> getClrReplList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTColorReplaceEffect>)new CTBlipImpl.ClrReplList(this);
        }
    }
    
    public CTColorReplaceEffect[] getClrReplArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.CLRREPL$20, list);
            final CTColorReplaceEffect[] array = new CTColorReplaceEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTColorReplaceEffect getClrReplArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColorReplaceEffect ctColorReplaceEffect = (CTColorReplaceEffect)this.get_store().find_element_user(CTBlipImpl.CLRREPL$20, n);
            if (ctColorReplaceEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctColorReplaceEffect;
        }
    }
    
    public int sizeOfClrReplArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.CLRREPL$20);
        }
    }
    
    public void setClrReplArray(final CTColorReplaceEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.CLRREPL$20);
        }
    }
    
    public void setClrReplArray(final int n, final CTColorReplaceEffect ctColorReplaceEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColorReplaceEffect ctColorReplaceEffect2 = (CTColorReplaceEffect)this.get_store().find_element_user(CTBlipImpl.CLRREPL$20, n);
            if (ctColorReplaceEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctColorReplaceEffect2.set((XmlObject)ctColorReplaceEffect);
        }
    }
    
    public CTColorReplaceEffect insertNewClrRepl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColorReplaceEffect)this.get_store().insert_element_user(CTBlipImpl.CLRREPL$20, n);
        }
    }
    
    public CTColorReplaceEffect addNewClrRepl() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColorReplaceEffect)this.get_store().add_element_user(CTBlipImpl.CLRREPL$20);
        }
    }
    
    public void removeClrRepl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.CLRREPL$20, n);
        }
    }
    
    public List<CTDuotoneEffect> getDuotoneList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDuotoneEffect>)new CTBlipImpl.DuotoneList(this);
        }
    }
    
    public CTDuotoneEffect[] getDuotoneArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.DUOTONE$22, list);
            final CTDuotoneEffect[] array = new CTDuotoneEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDuotoneEffect getDuotoneArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDuotoneEffect ctDuotoneEffect = (CTDuotoneEffect)this.get_store().find_element_user(CTBlipImpl.DUOTONE$22, n);
            if (ctDuotoneEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDuotoneEffect;
        }
    }
    
    public int sizeOfDuotoneArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.DUOTONE$22);
        }
    }
    
    public void setDuotoneArray(final CTDuotoneEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.DUOTONE$22);
        }
    }
    
    public void setDuotoneArray(final int n, final CTDuotoneEffect ctDuotoneEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDuotoneEffect ctDuotoneEffect2 = (CTDuotoneEffect)this.get_store().find_element_user(CTBlipImpl.DUOTONE$22, n);
            if (ctDuotoneEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDuotoneEffect2.set((XmlObject)ctDuotoneEffect);
        }
    }
    
    public CTDuotoneEffect insertNewDuotone(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDuotoneEffect)this.get_store().insert_element_user(CTBlipImpl.DUOTONE$22, n);
        }
    }
    
    public CTDuotoneEffect addNewDuotone() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDuotoneEffect)this.get_store().add_element_user(CTBlipImpl.DUOTONE$22);
        }
    }
    
    public void removeDuotone(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.DUOTONE$22, n);
        }
    }
    
    public List<CTFillOverlayEffect> getFillOverlayList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFillOverlayEffect>)new CTBlipImpl.FillOverlayList(this);
        }
    }
    
    public CTFillOverlayEffect[] getFillOverlayArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.FILLOVERLAY$24, list);
            final CTFillOverlayEffect[] array = new CTFillOverlayEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFillOverlayEffect getFillOverlayArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFillOverlayEffect ctFillOverlayEffect = (CTFillOverlayEffect)this.get_store().find_element_user(CTBlipImpl.FILLOVERLAY$24, n);
            if (ctFillOverlayEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFillOverlayEffect;
        }
    }
    
    public int sizeOfFillOverlayArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.FILLOVERLAY$24);
        }
    }
    
    public void setFillOverlayArray(final CTFillOverlayEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.FILLOVERLAY$24);
        }
    }
    
    public void setFillOverlayArray(final int n, final CTFillOverlayEffect ctFillOverlayEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFillOverlayEffect ctFillOverlayEffect2 = (CTFillOverlayEffect)this.get_store().find_element_user(CTBlipImpl.FILLOVERLAY$24, n);
            if (ctFillOverlayEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFillOverlayEffect2.set((XmlObject)ctFillOverlayEffect);
        }
    }
    
    public CTFillOverlayEffect insertNewFillOverlay(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFillOverlayEffect)this.get_store().insert_element_user(CTBlipImpl.FILLOVERLAY$24, n);
        }
    }
    
    public CTFillOverlayEffect addNewFillOverlay() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFillOverlayEffect)this.get_store().add_element_user(CTBlipImpl.FILLOVERLAY$24);
        }
    }
    
    public void removeFillOverlay(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.FILLOVERLAY$24, n);
        }
    }
    
    public List<CTGrayscaleEffect> getGraysclList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGrayscaleEffect>)new CTBlipImpl.GraysclList(this);
        }
    }
    
    public CTGrayscaleEffect[] getGraysclArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.GRAYSCL$26, list);
            final CTGrayscaleEffect[] array = new CTGrayscaleEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGrayscaleEffect getGraysclArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGrayscaleEffect ctGrayscaleEffect = (CTGrayscaleEffect)this.get_store().find_element_user(CTBlipImpl.GRAYSCL$26, n);
            if (ctGrayscaleEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGrayscaleEffect;
        }
    }
    
    public int sizeOfGraysclArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.GRAYSCL$26);
        }
    }
    
    public void setGraysclArray(final CTGrayscaleEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.GRAYSCL$26);
        }
    }
    
    public void setGraysclArray(final int n, final CTGrayscaleEffect ctGrayscaleEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGrayscaleEffect ctGrayscaleEffect2 = (CTGrayscaleEffect)this.get_store().find_element_user(CTBlipImpl.GRAYSCL$26, n);
            if (ctGrayscaleEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGrayscaleEffect2.set((XmlObject)ctGrayscaleEffect);
        }
    }
    
    public CTGrayscaleEffect insertNewGrayscl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGrayscaleEffect)this.get_store().insert_element_user(CTBlipImpl.GRAYSCL$26, n);
        }
    }
    
    public CTGrayscaleEffect addNewGrayscl() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGrayscaleEffect)this.get_store().add_element_user(CTBlipImpl.GRAYSCL$26);
        }
    }
    
    public void removeGrayscl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.GRAYSCL$26, n);
        }
    }
    
    public List<CTHSLEffect> getHslList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTHSLEffect>)new CTBlipImpl.HslList(this);
        }
    }
    
    public CTHSLEffect[] getHslArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.HSL$28, list);
            final CTHSLEffect[] array = new CTHSLEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTHSLEffect getHslArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHSLEffect cthslEffect = (CTHSLEffect)this.get_store().find_element_user(CTBlipImpl.HSL$28, n);
            if (cthslEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return cthslEffect;
        }
    }
    
    public int sizeOfHslArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.HSL$28);
        }
    }
    
    public void setHslArray(final CTHSLEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.HSL$28);
        }
    }
    
    public void setHslArray(final int n, final CTHSLEffect cthslEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHSLEffect cthslEffect2 = (CTHSLEffect)this.get_store().find_element_user(CTBlipImpl.HSL$28, n);
            if (cthslEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            cthslEffect2.set((XmlObject)cthslEffect);
        }
    }
    
    public CTHSLEffect insertNewHsl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHSLEffect)this.get_store().insert_element_user(CTBlipImpl.HSL$28, n);
        }
    }
    
    public CTHSLEffect addNewHsl() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHSLEffect)this.get_store().add_element_user(CTBlipImpl.HSL$28);
        }
    }
    
    public void removeHsl(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.HSL$28, n);
        }
    }
    
    public List<CTLuminanceEffect> getLumList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLuminanceEffect>)new CTBlipImpl.LumList(this);
        }
    }
    
    public CTLuminanceEffect[] getLumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.LUM$30, list);
            final CTLuminanceEffect[] array = new CTLuminanceEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLuminanceEffect getLumArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLuminanceEffect ctLuminanceEffect = (CTLuminanceEffect)this.get_store().find_element_user(CTBlipImpl.LUM$30, n);
            if (ctLuminanceEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLuminanceEffect;
        }
    }
    
    public int sizeOfLumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.LUM$30);
        }
    }
    
    public void setLumArray(final CTLuminanceEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.LUM$30);
        }
    }
    
    public void setLumArray(final int n, final CTLuminanceEffect ctLuminanceEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLuminanceEffect ctLuminanceEffect2 = (CTLuminanceEffect)this.get_store().find_element_user(CTBlipImpl.LUM$30, n);
            if (ctLuminanceEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLuminanceEffect2.set((XmlObject)ctLuminanceEffect);
        }
    }
    
    public CTLuminanceEffect insertNewLum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLuminanceEffect)this.get_store().insert_element_user(CTBlipImpl.LUM$30, n);
        }
    }
    
    public CTLuminanceEffect addNewLum() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLuminanceEffect)this.get_store().add_element_user(CTBlipImpl.LUM$30);
        }
    }
    
    public void removeLum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.LUM$30, n);
        }
    }
    
    public List<CTTintEffect> getTintList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTintEffect>)new CTBlipImpl.TintList(this);
        }
    }
    
    public CTTintEffect[] getTintArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBlipImpl.TINT$32, list);
            final CTTintEffect[] array = new CTTintEffect[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTintEffect getTintArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTintEffect ctTintEffect = (CTTintEffect)this.get_store().find_element_user(CTBlipImpl.TINT$32, n);
            if (ctTintEffect == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTintEffect;
        }
    }
    
    public int sizeOfTintArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.TINT$32);
        }
    }
    
    public void setTintArray(final CTTintEffect[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTBlipImpl.TINT$32);
        }
    }
    
    public void setTintArray(final int n, final CTTintEffect ctTintEffect) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTintEffect ctTintEffect2 = (CTTintEffect)this.get_store().find_element_user(CTBlipImpl.TINT$32, n);
            if (ctTintEffect2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTintEffect2.set((XmlObject)ctTintEffect);
        }
    }
    
    public CTTintEffect insertNewTint(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTintEffect)this.get_store().insert_element_user(CTBlipImpl.TINT$32, n);
        }
    }
    
    public CTTintEffect addNewTint() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTintEffect)this.get_store().add_element_user(CTBlipImpl.TINT$32);
        }
    }
    
    public void removeTint(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.TINT$32, n);
        }
    }
    
    public CTOfficeArtExtensionList getExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOfficeArtExtensionList list = (CTOfficeArtExtensionList)this.get_store().find_element_user(CTBlipImpl.EXTLST$34, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public boolean isSetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBlipImpl.EXTLST$34) != 0;
        }
    }
    
    public void setExtLst(final CTOfficeArtExtensionList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTOfficeArtExtensionList list2 = (CTOfficeArtExtensionList)this.get_store().find_element_user(CTBlipImpl.EXTLST$34, 0);
            if (list2 == null) {
                list2 = (CTOfficeArtExtensionList)this.get_store().add_element_user(CTBlipImpl.EXTLST$34);
            }
            list2.set((XmlObject)list);
        }
    }
    
    public CTOfficeArtExtensionList addNewExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOfficeArtExtensionList)this.get_store().add_element_user(CTBlipImpl.EXTLST$34);
        }
    }
    
    public void unsetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBlipImpl.EXTLST$34, 0);
        }
    }
    
    public String getEmbed() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBlipImpl.EMBED$36);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_default_attribute_value(CTBlipImpl.EMBED$36);
            }
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public STRelationshipId xgetEmbed() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STRelationshipId stRelationshipId = (STRelationshipId)this.get_store().find_attribute_user(CTBlipImpl.EMBED$36);
            if (stRelationshipId == null) {
                stRelationshipId = (STRelationshipId)this.get_default_attribute_value(CTBlipImpl.EMBED$36);
            }
            return stRelationshipId;
        }
    }
    
    public boolean isSetEmbed() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTBlipImpl.EMBED$36) != null;
        }
    }
    
    public void setEmbed(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBlipImpl.EMBED$36);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTBlipImpl.EMBED$36);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetEmbed(final STRelationshipId stRelationshipId) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STRelationshipId stRelationshipId2 = (STRelationshipId)this.get_store().find_attribute_user(CTBlipImpl.EMBED$36);
            if (stRelationshipId2 == null) {
                stRelationshipId2 = (STRelationshipId)this.get_store().add_attribute_user(CTBlipImpl.EMBED$36);
            }
            stRelationshipId2.set(stRelationshipId);
        }
    }
    
    public void unsetEmbed() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTBlipImpl.EMBED$36);
        }
    }
    
    public String getLink() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBlipImpl.LINK$38);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_default_attribute_value(CTBlipImpl.LINK$38);
            }
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getStringValue();
        }
    }
    
    public STRelationshipId xgetLink() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STRelationshipId stRelationshipId = (STRelationshipId)this.get_store().find_attribute_user(CTBlipImpl.LINK$38);
            if (stRelationshipId == null) {
                stRelationshipId = (STRelationshipId)this.get_default_attribute_value(CTBlipImpl.LINK$38);
            }
            return stRelationshipId;
        }
    }
    
    public boolean isSetLink() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTBlipImpl.LINK$38) != null;
        }
    }
    
    public void setLink(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBlipImpl.LINK$38);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTBlipImpl.LINK$38);
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetLink(final STRelationshipId stRelationshipId) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STRelationshipId stRelationshipId2 = (STRelationshipId)this.get_store().find_attribute_user(CTBlipImpl.LINK$38);
            if (stRelationshipId2 == null) {
                stRelationshipId2 = (STRelationshipId)this.get_store().add_attribute_user(CTBlipImpl.LINK$38);
            }
            stRelationshipId2.set(stRelationshipId);
        }
    }
    
    public void unsetLink() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTBlipImpl.LINK$38);
        }
    }
    
    public STBlipCompression.Enum getCstate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBlipImpl.CSTATE$40);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_default_attribute_value(CTBlipImpl.CSTATE$40);
            }
            if (simpleValue == null) {
                return null;
            }
            return (STBlipCompression.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STBlipCompression xgetCstate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STBlipCompression stBlipCompression = (STBlipCompression)this.get_store().find_attribute_user(CTBlipImpl.CSTATE$40);
            if (stBlipCompression == null) {
                stBlipCompression = (STBlipCompression)this.get_default_attribute_value(CTBlipImpl.CSTATE$40);
            }
            return stBlipCompression;
        }
    }
    
    public boolean isSetCstate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTBlipImpl.CSTATE$40) != null;
        }
    }
    
    public void setCstate(final STBlipCompression.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBlipImpl.CSTATE$40);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTBlipImpl.CSTATE$40);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetCstate(final STBlipCompression stBlipCompression) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STBlipCompression stBlipCompression2 = (STBlipCompression)this.get_store().find_attribute_user(CTBlipImpl.CSTATE$40);
            if (stBlipCompression2 == null) {
                stBlipCompression2 = (STBlipCompression)this.get_store().add_attribute_user(CTBlipImpl.CSTATE$40);
            }
            stBlipCompression2.set((XmlObject)stBlipCompression);
        }
    }
    
    public void unsetCstate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTBlipImpl.CSTATE$40);
        }
    }
    
    static {
        ALPHABILEVEL$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaBiLevel");
        ALPHACEILING$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaCeiling");
        ALPHAFLOOR$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaFloor");
        ALPHAINV$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaInv");
        ALPHAMOD$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaMod");
        ALPHAMODFIX$10 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaModFix");
        ALPHAREPL$12 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "alphaRepl");
        BILEVEL$14 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "biLevel");
        BLUR$16 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "blur");
        CLRCHANGE$18 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "clrChange");
        CLRREPL$20 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "clrRepl");
        DUOTONE$22 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "duotone");
        FILLOVERLAY$24 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "fillOverlay");
        GRAYSCL$26 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "grayscl");
        HSL$28 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "hsl");
        LUM$30 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "lum");
        TINT$32 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "tint");
        EXTLST$34 = new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "extLst");
        EMBED$36 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "embed");
        LINK$38 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "link");
        CSTATE$40 = new QName("", "cstate");
    }
}
