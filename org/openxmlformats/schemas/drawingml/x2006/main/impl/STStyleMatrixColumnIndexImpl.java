// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STStyleMatrixColumnIndex;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STStyleMatrixColumnIndexImpl extends JavaLongHolderEx implements STStyleMatrixColumnIndex
{
    public STStyleMatrixColumnIndexImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STStyleMatrixColumnIndexImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
