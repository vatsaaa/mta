// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextSpacingPoint;
import org.apache.xmlbeans.impl.values.JavaIntHolderEx;

public class STTextSpacingPointImpl extends JavaIntHolderEx implements STTextSpacingPoint
{
    public STTextSpacingPointImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextSpacingPointImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
