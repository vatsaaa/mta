// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPresetLineDashVal;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STPresetLineDashValImpl extends JavaStringEnumerationHolderEx implements STPresetLineDashVal
{
    public STPresetLineDashValImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPresetLineDashValImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
