// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STColorSchemeIndex;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STColorSchemeIndexImpl extends JavaStringEnumerationHolderEx implements STColorSchemeIndex
{
    public STColorSchemeIndexImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STColorSchemeIndexImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
