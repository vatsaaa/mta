// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STDrawingElementId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STDrawingElementIdImpl extends JavaLongHolderEx implements STDrawingElementId
{
    public STDrawingElementIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STDrawingElementIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
