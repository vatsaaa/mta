// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.main.STGeomGuideFormula;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STGeomGuideFormulaImpl extends JavaStringHolderEx implements STGeomGuideFormula
{
    public STGeomGuideFormulaImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STGeomGuideFormulaImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
