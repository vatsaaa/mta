// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPositiveFixedPercentage extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPositiveFixedPercentage.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpositivefixedpercentage8966type");
    
    int getVal();
    
    STPositiveFixedPercentage xgetVal();
    
    void setVal(final int p0);
    
    void xsetVal(final STPositiveFixedPercentage p0);
    
    public static final class Factory
    {
        public static CTPositiveFixedPercentage newInstance() {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage newInstance(final XmlOptions xmlOptions) {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static CTPositiveFixedPercentage parse(final String s) throws XmlException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static CTPositiveFixedPercentage parse(final File file) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static CTPositiveFixedPercentage parse(final URL url) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static CTPositiveFixedPercentage parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static CTPositiveFixedPercentage parse(final Reader reader) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static CTPositiveFixedPercentage parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        public static CTPositiveFixedPercentage parse(final Node node) throws XmlException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, CTPositiveFixedPercentage.type, null);
        }
        
        public static CTPositiveFixedPercentage parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPositiveFixedPercentage parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPositiveFixedPercentage.type, null);
        }
        
        @Deprecated
        public static CTPositiveFixedPercentage parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPositiveFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPositiveFixedPercentage.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPositiveFixedPercentage.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
