// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STLineWidth extends STCoordinate32
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLineWidth.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlinewidth8313type");
    
    public static final class Factory
    {
        public static STLineWidth newValue(final Object o) {
            return (STLineWidth)STLineWidth.type.newValue(o);
        }
        
        public static STLineWidth newInstance() {
            return (STLineWidth)XmlBeans.getContextTypeLoader().newInstance(STLineWidth.type, null);
        }
        
        public static STLineWidth newInstance(final XmlOptions xmlOptions) {
            return (STLineWidth)XmlBeans.getContextTypeLoader().newInstance(STLineWidth.type, xmlOptions);
        }
        
        public static STLineWidth parse(final String s) throws XmlException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(s, STLineWidth.type, null);
        }
        
        public static STLineWidth parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(s, STLineWidth.type, xmlOptions);
        }
        
        public static STLineWidth parse(final File file) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(file, STLineWidth.type, null);
        }
        
        public static STLineWidth parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(file, STLineWidth.type, xmlOptions);
        }
        
        public static STLineWidth parse(final URL url) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(url, STLineWidth.type, null);
        }
        
        public static STLineWidth parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(url, STLineWidth.type, xmlOptions);
        }
        
        public static STLineWidth parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(inputStream, STLineWidth.type, null);
        }
        
        public static STLineWidth parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(inputStream, STLineWidth.type, xmlOptions);
        }
        
        public static STLineWidth parse(final Reader reader) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(reader, STLineWidth.type, null);
        }
        
        public static STLineWidth parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(reader, STLineWidth.type, xmlOptions);
        }
        
        public static STLineWidth parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineWidth.type, null);
        }
        
        public static STLineWidth parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineWidth.type, xmlOptions);
        }
        
        public static STLineWidth parse(final Node node) throws XmlException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(node, STLineWidth.type, null);
        }
        
        public static STLineWidth parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(node, STLineWidth.type, xmlOptions);
        }
        
        @Deprecated
        public static STLineWidth parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineWidth.type, null);
        }
        
        @Deprecated
        public static STLineWidth parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLineWidth)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineWidth.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineWidth.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineWidth.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
