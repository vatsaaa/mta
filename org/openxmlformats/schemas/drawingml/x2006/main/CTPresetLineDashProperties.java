// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPresetLineDashProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPresetLineDashProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpresetlinedashproperties4553type");
    
    STPresetLineDashVal.Enum getVal();
    
    STPresetLineDashVal xgetVal();
    
    boolean isSetVal();
    
    void setVal(final STPresetLineDashVal.Enum p0);
    
    void xsetVal(final STPresetLineDashVal p0);
    
    void unsetVal();
    
    public static final class Factory
    {
        public static CTPresetLineDashProperties newInstance() {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().newInstance(CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties newInstance(final XmlOptions xmlOptions) {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().newInstance(CTPresetLineDashProperties.type, xmlOptions);
        }
        
        public static CTPresetLineDashProperties parse(final String s) throws XmlException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(s, CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(s, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        public static CTPresetLineDashProperties parse(final File file) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(file, CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(file, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        public static CTPresetLineDashProperties parse(final URL url) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(url, CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(url, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        public static CTPresetLineDashProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        public static CTPresetLineDashProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(reader, CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(reader, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        public static CTPresetLineDashProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        public static CTPresetLineDashProperties parse(final Node node) throws XmlException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(node, CTPresetLineDashProperties.type, null);
        }
        
        public static CTPresetLineDashProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(node, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPresetLineDashProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPresetLineDashProperties.type, null);
        }
        
        @Deprecated
        public static CTPresetLineDashProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPresetLineDashProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPresetLineDashProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPresetLineDashProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
