// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STTextFontSize extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextFontSize.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextfontsizeb3a8type");
    
    public static final class Factory
    {
        public static STTextFontSize newValue(final Object o) {
            return (STTextFontSize)STTextFontSize.type.newValue(o);
        }
        
        public static STTextFontSize newInstance() {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().newInstance(STTextFontSize.type, null);
        }
        
        public static STTextFontSize newInstance(final XmlOptions xmlOptions) {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().newInstance(STTextFontSize.type, xmlOptions);
        }
        
        public static STTextFontSize parse(final String s) throws XmlException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(s, STTextFontSize.type, null);
        }
        
        public static STTextFontSize parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(s, STTextFontSize.type, xmlOptions);
        }
        
        public static STTextFontSize parse(final File file) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(file, STTextFontSize.type, null);
        }
        
        public static STTextFontSize parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(file, STTextFontSize.type, xmlOptions);
        }
        
        public static STTextFontSize parse(final URL url) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(url, STTextFontSize.type, null);
        }
        
        public static STTextFontSize parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(url, STTextFontSize.type, xmlOptions);
        }
        
        public static STTextFontSize parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(inputStream, STTextFontSize.type, null);
        }
        
        public static STTextFontSize parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(inputStream, STTextFontSize.type, xmlOptions);
        }
        
        public static STTextFontSize parse(final Reader reader) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(reader, STTextFontSize.type, null);
        }
        
        public static STTextFontSize parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(reader, STTextFontSize.type, xmlOptions);
        }
        
        public static STTextFontSize parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextFontSize.type, null);
        }
        
        public static STTextFontSize parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextFontSize.type, xmlOptions);
        }
        
        public static STTextFontSize parse(final Node node) throws XmlException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(node, STTextFontSize.type, null);
        }
        
        public static STTextFontSize parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(node, STTextFontSize.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextFontSize parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextFontSize.type, null);
        }
        
        @Deprecated
        public static STTextFontSize parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextFontSize)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextFontSize.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextFontSize.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextFontSize.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
