// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTransform2D extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTransform2D.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttransform2d5deftype");
    
    CTPoint2D getOff();
    
    boolean isSetOff();
    
    void setOff(final CTPoint2D p0);
    
    CTPoint2D addNewOff();
    
    void unsetOff();
    
    CTPositiveSize2D getExt();
    
    boolean isSetExt();
    
    void setExt(final CTPositiveSize2D p0);
    
    CTPositiveSize2D addNewExt();
    
    void unsetExt();
    
    int getRot();
    
    STAngle xgetRot();
    
    boolean isSetRot();
    
    void setRot(final int p0);
    
    void xsetRot(final STAngle p0);
    
    void unsetRot();
    
    boolean getFlipH();
    
    XmlBoolean xgetFlipH();
    
    boolean isSetFlipH();
    
    void setFlipH(final boolean p0);
    
    void xsetFlipH(final XmlBoolean p0);
    
    void unsetFlipH();
    
    boolean getFlipV();
    
    XmlBoolean xgetFlipV();
    
    boolean isSetFlipV();
    
    void setFlipV(final boolean p0);
    
    void xsetFlipV(final XmlBoolean p0);
    
    void unsetFlipV();
    
    public static final class Factory
    {
        public static CTTransform2D newInstance() {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().newInstance(CTTransform2D.type, null);
        }
        
        public static CTTransform2D newInstance(final XmlOptions xmlOptions) {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().newInstance(CTTransform2D.type, xmlOptions);
        }
        
        public static CTTransform2D parse(final String s) throws XmlException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(s, CTTransform2D.type, null);
        }
        
        public static CTTransform2D parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(s, CTTransform2D.type, xmlOptions);
        }
        
        public static CTTransform2D parse(final File file) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(file, CTTransform2D.type, null);
        }
        
        public static CTTransform2D parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(file, CTTransform2D.type, xmlOptions);
        }
        
        public static CTTransform2D parse(final URL url) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(url, CTTransform2D.type, null);
        }
        
        public static CTTransform2D parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(url, CTTransform2D.type, xmlOptions);
        }
        
        public static CTTransform2D parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(inputStream, CTTransform2D.type, null);
        }
        
        public static CTTransform2D parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(inputStream, CTTransform2D.type, xmlOptions);
        }
        
        public static CTTransform2D parse(final Reader reader) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(reader, CTTransform2D.type, null);
        }
        
        public static CTTransform2D parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(reader, CTTransform2D.type, xmlOptions);
        }
        
        public static CTTransform2D parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTransform2D.type, null);
        }
        
        public static CTTransform2D parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTransform2D.type, xmlOptions);
        }
        
        public static CTTransform2D parse(final Node node) throws XmlException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(node, CTTransform2D.type, null);
        }
        
        public static CTTransform2D parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(node, CTTransform2D.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTransform2D parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTransform2D.type, null);
        }
        
        @Deprecated
        public static CTTransform2D parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTransform2D)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTransform2D.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTransform2D.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTransform2D.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
