// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STPositiveCoordinate32 extends STCoordinate32
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPositiveCoordinate32.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpositivecoordinate321b9btype");
    
    public static final class Factory
    {
        public static STPositiveCoordinate32 newValue(final Object o) {
            return (STPositiveCoordinate32)STPositiveCoordinate32.type.newValue(o);
        }
        
        public static STPositiveCoordinate32 newInstance() {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().newInstance(STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 newInstance(final XmlOptions xmlOptions) {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().newInstance(STPositiveCoordinate32.type, xmlOptions);
        }
        
        public static STPositiveCoordinate32 parse(final String s) throws XmlException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(s, STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(s, STPositiveCoordinate32.type, xmlOptions);
        }
        
        public static STPositiveCoordinate32 parse(final File file) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(file, STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(file, STPositiveCoordinate32.type, xmlOptions);
        }
        
        public static STPositiveCoordinate32 parse(final URL url) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(url, STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(url, STPositiveCoordinate32.type, xmlOptions);
        }
        
        public static STPositiveCoordinate32 parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveCoordinate32.type, xmlOptions);
        }
        
        public static STPositiveCoordinate32 parse(final Reader reader) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(reader, STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(reader, STPositiveCoordinate32.type, xmlOptions);
        }
        
        public static STPositiveCoordinate32 parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveCoordinate32.type, xmlOptions);
        }
        
        public static STPositiveCoordinate32 parse(final Node node) throws XmlException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(node, STPositiveCoordinate32.type, null);
        }
        
        public static STPositiveCoordinate32 parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(node, STPositiveCoordinate32.type, xmlOptions);
        }
        
        @Deprecated
        public static STPositiveCoordinate32 parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveCoordinate32.type, null);
        }
        
        @Deprecated
        public static STPositiveCoordinate32 parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPositiveCoordinate32)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveCoordinate32.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveCoordinate32.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveCoordinate32.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
