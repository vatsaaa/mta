// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlToken;

public interface STPathShadeType extends XmlToken
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPathShadeType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpathshadetype93c3type");
    public static final Enum SHAPE = Enum.forString("shape");
    public static final Enum CIRCLE = Enum.forString("circle");
    public static final Enum RECT = Enum.forString("rect");
    public static final int INT_SHAPE = 1;
    public static final int INT_CIRCLE = 2;
    public static final int INT_RECT = 3;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STPathShadeType newValue(final Object o) {
            return (STPathShadeType)STPathShadeType.type.newValue(o);
        }
        
        public static STPathShadeType newInstance() {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().newInstance(STPathShadeType.type, null);
        }
        
        public static STPathShadeType newInstance(final XmlOptions xmlOptions) {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().newInstance(STPathShadeType.type, xmlOptions);
        }
        
        public static STPathShadeType parse(final String s) throws XmlException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(s, STPathShadeType.type, null);
        }
        
        public static STPathShadeType parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(s, STPathShadeType.type, xmlOptions);
        }
        
        public static STPathShadeType parse(final File file) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(file, STPathShadeType.type, null);
        }
        
        public static STPathShadeType parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(file, STPathShadeType.type, xmlOptions);
        }
        
        public static STPathShadeType parse(final URL url) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(url, STPathShadeType.type, null);
        }
        
        public static STPathShadeType parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(url, STPathShadeType.type, xmlOptions);
        }
        
        public static STPathShadeType parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(inputStream, STPathShadeType.type, null);
        }
        
        public static STPathShadeType parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(inputStream, STPathShadeType.type, xmlOptions);
        }
        
        public static STPathShadeType parse(final Reader reader) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(reader, STPathShadeType.type, null);
        }
        
        public static STPathShadeType parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(reader, STPathShadeType.type, xmlOptions);
        }
        
        public static STPathShadeType parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPathShadeType.type, null);
        }
        
        public static STPathShadeType parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPathShadeType.type, xmlOptions);
        }
        
        public static STPathShadeType parse(final Node node) throws XmlException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(node, STPathShadeType.type, null);
        }
        
        public static STPathShadeType parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(node, STPathShadeType.type, xmlOptions);
        }
        
        @Deprecated
        public static STPathShadeType parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPathShadeType.type, null);
        }
        
        @Deprecated
        public static STPathShadeType parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPathShadeType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPathShadeType.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPathShadeType.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPathShadeType.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_SHAPE = 1;
        static final int INT_CIRCLE = 2;
        static final int INT_RECT = 3;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("shape", 1), new Enum("circle", 2), new Enum("rect", 3) });
        }
    }
}
