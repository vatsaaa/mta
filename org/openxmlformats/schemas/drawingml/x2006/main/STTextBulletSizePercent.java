// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STTextBulletSizePercent extends STPercentage
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextBulletSizePercent.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextbulletsizepercentb516type");
    
    public static final class Factory
    {
        public static STTextBulletSizePercent newValue(final Object o) {
            return (STTextBulletSizePercent)STTextBulletSizePercent.type.newValue(o);
        }
        
        public static STTextBulletSizePercent newInstance() {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().newInstance(STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent newInstance(final XmlOptions xmlOptions) {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().newInstance(STTextBulletSizePercent.type, xmlOptions);
        }
        
        public static STTextBulletSizePercent parse(final String s) throws XmlException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(s, STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(s, STTextBulletSizePercent.type, xmlOptions);
        }
        
        public static STTextBulletSizePercent parse(final File file) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(file, STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(file, STTextBulletSizePercent.type, xmlOptions);
        }
        
        public static STTextBulletSizePercent parse(final URL url) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(url, STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(url, STTextBulletSizePercent.type, xmlOptions);
        }
        
        public static STTextBulletSizePercent parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(inputStream, STTextBulletSizePercent.type, xmlOptions);
        }
        
        public static STTextBulletSizePercent parse(final Reader reader) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(reader, STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(reader, STTextBulletSizePercent.type, xmlOptions);
        }
        
        public static STTextBulletSizePercent parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextBulletSizePercent.type, xmlOptions);
        }
        
        public static STTextBulletSizePercent parse(final Node node) throws XmlException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(node, STTextBulletSizePercent.type, null);
        }
        
        public static STTextBulletSizePercent parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(node, STTextBulletSizePercent.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextBulletSizePercent parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextBulletSizePercent.type, null);
        }
        
        @Deprecated
        public static STTextBulletSizePercent parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextBulletSizePercent)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextBulletSizePercent.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextBulletSizePercent.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextBulletSizePercent.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
