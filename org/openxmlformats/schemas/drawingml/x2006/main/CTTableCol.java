// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTableCol extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTableCol.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttablecol19edtype");
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    long getW();
    
    STCoordinate xgetW();
    
    void setW(final long p0);
    
    void xsetW(final STCoordinate p0);
    
    public static final class Factory
    {
        public static CTTableCol newInstance() {
            return (CTTableCol)XmlBeans.getContextTypeLoader().newInstance(CTTableCol.type, null);
        }
        
        public static CTTableCol newInstance(final XmlOptions xmlOptions) {
            return (CTTableCol)XmlBeans.getContextTypeLoader().newInstance(CTTableCol.type, xmlOptions);
        }
        
        public static CTTableCol parse(final String s) throws XmlException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(s, CTTableCol.type, null);
        }
        
        public static CTTableCol parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(s, CTTableCol.type, xmlOptions);
        }
        
        public static CTTableCol parse(final File file) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(file, CTTableCol.type, null);
        }
        
        public static CTTableCol parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(file, CTTableCol.type, xmlOptions);
        }
        
        public static CTTableCol parse(final URL url) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(url, CTTableCol.type, null);
        }
        
        public static CTTableCol parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(url, CTTableCol.type, xmlOptions);
        }
        
        public static CTTableCol parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableCol.type, null);
        }
        
        public static CTTableCol parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableCol.type, xmlOptions);
        }
        
        public static CTTableCol parse(final Reader reader) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(reader, CTTableCol.type, null);
        }
        
        public static CTTableCol parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(reader, CTTableCol.type, xmlOptions);
        }
        
        public static CTTableCol parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableCol.type, null);
        }
        
        public static CTTableCol parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableCol.type, xmlOptions);
        }
        
        public static CTTableCol parse(final Node node) throws XmlException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(node, CTTableCol.type, null);
        }
        
        public static CTTableCol parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(node, CTTableCol.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTableCol parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableCol.type, null);
        }
        
        @Deprecated
        public static CTTableCol parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTableCol)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableCol.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableCol.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableCol.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
