// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlAnySimpleType;

public interface STAdjCoordinate extends XmlAnySimpleType
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STAdjCoordinate.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stadjcoordinated920type");
    
    Object getObjectValue();
    
    void setObjectValue(final Object p0);
    
    @Deprecated
    Object objectValue();
    
    @Deprecated
    void objectSet(final Object p0);
    
    SchemaType instanceType();
    
    public static final class Factory
    {
        public static STAdjCoordinate newValue(final Object o) {
            return (STAdjCoordinate)STAdjCoordinate.type.newValue(o);
        }
        
        public static STAdjCoordinate newInstance() {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().newInstance(STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate newInstance(final XmlOptions xmlOptions) {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().newInstance(STAdjCoordinate.type, xmlOptions);
        }
        
        public static STAdjCoordinate parse(final String s) throws XmlException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(s, STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(s, STAdjCoordinate.type, xmlOptions);
        }
        
        public static STAdjCoordinate parse(final File file) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(file, STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(file, STAdjCoordinate.type, xmlOptions);
        }
        
        public static STAdjCoordinate parse(final URL url) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(url, STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(url, STAdjCoordinate.type, xmlOptions);
        }
        
        public static STAdjCoordinate parse(final InputStream inputStream) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(inputStream, STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(inputStream, STAdjCoordinate.type, xmlOptions);
        }
        
        public static STAdjCoordinate parse(final Reader reader) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(reader, STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(reader, STAdjCoordinate.type, xmlOptions);
        }
        
        public static STAdjCoordinate parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAdjCoordinate.type, xmlOptions);
        }
        
        public static STAdjCoordinate parse(final Node node) throws XmlException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(node, STAdjCoordinate.type, null);
        }
        
        public static STAdjCoordinate parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(node, STAdjCoordinate.type, xmlOptions);
        }
        
        @Deprecated
        public static STAdjCoordinate parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAdjCoordinate.type, null);
        }
        
        @Deprecated
        public static STAdjCoordinate parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STAdjCoordinate)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAdjCoordinate.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAdjCoordinate.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAdjCoordinate.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
