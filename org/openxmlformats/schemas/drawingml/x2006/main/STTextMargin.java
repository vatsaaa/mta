// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STTextMargin extends STCoordinate32
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextMargin.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextmargin9666type");
    
    public static final class Factory
    {
        public static STTextMargin newValue(final Object o) {
            return (STTextMargin)STTextMargin.type.newValue(o);
        }
        
        public static STTextMargin newInstance() {
            return (STTextMargin)XmlBeans.getContextTypeLoader().newInstance(STTextMargin.type, null);
        }
        
        public static STTextMargin newInstance(final XmlOptions xmlOptions) {
            return (STTextMargin)XmlBeans.getContextTypeLoader().newInstance(STTextMargin.type, xmlOptions);
        }
        
        public static STTextMargin parse(final String s) throws XmlException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(s, STTextMargin.type, null);
        }
        
        public static STTextMargin parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(s, STTextMargin.type, xmlOptions);
        }
        
        public static STTextMargin parse(final File file) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(file, STTextMargin.type, null);
        }
        
        public static STTextMargin parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(file, STTextMargin.type, xmlOptions);
        }
        
        public static STTextMargin parse(final URL url) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(url, STTextMargin.type, null);
        }
        
        public static STTextMargin parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(url, STTextMargin.type, xmlOptions);
        }
        
        public static STTextMargin parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(inputStream, STTextMargin.type, null);
        }
        
        public static STTextMargin parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(inputStream, STTextMargin.type, xmlOptions);
        }
        
        public static STTextMargin parse(final Reader reader) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(reader, STTextMargin.type, null);
        }
        
        public static STTextMargin parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(reader, STTextMargin.type, xmlOptions);
        }
        
        public static STTextMargin parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextMargin.type, null);
        }
        
        public static STTextMargin parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextMargin.type, xmlOptions);
        }
        
        public static STTextMargin parse(final Node node) throws XmlException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(node, STTextMargin.type, null);
        }
        
        public static STTextMargin parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(node, STTextMargin.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextMargin parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextMargin.type, null);
        }
        
        @Deprecated
        public static STTextMargin parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextMargin)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextMargin.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextMargin.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextMargin.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
