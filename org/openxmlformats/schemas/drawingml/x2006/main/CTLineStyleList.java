// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTLineStyleList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTLineStyleList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctlinestylelist510ctype");
    
    List<CTLineProperties> getLnList();
    
    @Deprecated
    CTLineProperties[] getLnArray();
    
    CTLineProperties getLnArray(final int p0);
    
    int sizeOfLnArray();
    
    void setLnArray(final CTLineProperties[] p0);
    
    void setLnArray(final int p0, final CTLineProperties p1);
    
    CTLineProperties insertNewLn(final int p0);
    
    CTLineProperties addNewLn();
    
    void removeLn(final int p0);
    
    public static final class Factory
    {
        public static CTLineStyleList newInstance() {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().newInstance(CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList newInstance(final XmlOptions xmlOptions) {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().newInstance(CTLineStyleList.type, xmlOptions);
        }
        
        public static CTLineStyleList parse(final String s) throws XmlException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(s, CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(s, CTLineStyleList.type, xmlOptions);
        }
        
        public static CTLineStyleList parse(final File file) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(file, CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(file, CTLineStyleList.type, xmlOptions);
        }
        
        public static CTLineStyleList parse(final URL url) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(url, CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(url, CTLineStyleList.type, xmlOptions);
        }
        
        public static CTLineStyleList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(inputStream, CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(inputStream, CTLineStyleList.type, xmlOptions);
        }
        
        public static CTLineStyleList parse(final Reader reader) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(reader, CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(reader, CTLineStyleList.type, xmlOptions);
        }
        
        public static CTLineStyleList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLineStyleList.type, xmlOptions);
        }
        
        public static CTLineStyleList parse(final Node node) throws XmlException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(node, CTLineStyleList.type, null);
        }
        
        public static CTLineStyleList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(node, CTLineStyleList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTLineStyleList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLineStyleList.type, null);
        }
        
        @Deprecated
        public static CTLineStyleList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTLineStyleList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLineStyleList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLineStyleList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLineStyleList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
