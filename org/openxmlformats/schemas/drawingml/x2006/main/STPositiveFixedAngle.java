// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STPositiveFixedAngle extends STAngle
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPositiveFixedAngle.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpositivefixedangle2503type");
    
    public static final class Factory
    {
        public static STPositiveFixedAngle newValue(final Object o) {
            return (STPositiveFixedAngle)STPositiveFixedAngle.type.newValue(o);
        }
        
        public static STPositiveFixedAngle newInstance() {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().newInstance(STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle newInstance(final XmlOptions xmlOptions) {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().newInstance(STPositiveFixedAngle.type, xmlOptions);
        }
        
        public static STPositiveFixedAngle parse(final String s) throws XmlException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(s, STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(s, STPositiveFixedAngle.type, xmlOptions);
        }
        
        public static STPositiveFixedAngle parse(final File file) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(file, STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(file, STPositiveFixedAngle.type, xmlOptions);
        }
        
        public static STPositiveFixedAngle parse(final URL url) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(url, STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(url, STPositiveFixedAngle.type, xmlOptions);
        }
        
        public static STPositiveFixedAngle parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveFixedAngle.type, xmlOptions);
        }
        
        public static STPositiveFixedAngle parse(final Reader reader) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(reader, STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(reader, STPositiveFixedAngle.type, xmlOptions);
        }
        
        public static STPositiveFixedAngle parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveFixedAngle.type, xmlOptions);
        }
        
        public static STPositiveFixedAngle parse(final Node node) throws XmlException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(node, STPositiveFixedAngle.type, null);
        }
        
        public static STPositiveFixedAngle parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(node, STPositiveFixedAngle.type, xmlOptions);
        }
        
        @Deprecated
        public static STPositiveFixedAngle parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveFixedAngle.type, null);
        }
        
        @Deprecated
        public static STPositiveFixedAngle parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPositiveFixedAngle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveFixedAngle.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveFixedAngle.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveFixedAngle.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
