// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STTextLanguageID extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextLanguageID.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttextlanguageid806btype");
    
    public static final class Factory
    {
        public static STTextLanguageID newValue(final Object o) {
            return (STTextLanguageID)STTextLanguageID.type.newValue(o);
        }
        
        public static STTextLanguageID newInstance() {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().newInstance(STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID newInstance(final XmlOptions xmlOptions) {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().newInstance(STTextLanguageID.type, xmlOptions);
        }
        
        public static STTextLanguageID parse(final String s) throws XmlException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(s, STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(s, STTextLanguageID.type, xmlOptions);
        }
        
        public static STTextLanguageID parse(final File file) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(file, STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(file, STTextLanguageID.type, xmlOptions);
        }
        
        public static STTextLanguageID parse(final URL url) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(url, STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(url, STTextLanguageID.type, xmlOptions);
        }
        
        public static STTextLanguageID parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(inputStream, STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(inputStream, STTextLanguageID.type, xmlOptions);
        }
        
        public static STTextLanguageID parse(final Reader reader) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(reader, STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(reader, STTextLanguageID.type, xmlOptions);
        }
        
        public static STTextLanguageID parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextLanguageID.type, xmlOptions);
        }
        
        public static STTextLanguageID parse(final Node node) throws XmlException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(node, STTextLanguageID.type, null);
        }
        
        public static STTextLanguageID parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(node, STTextLanguageID.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextLanguageID parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextLanguageID.type, null);
        }
        
        @Deprecated
        public static STTextLanguageID parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextLanguageID)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextLanguageID.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextLanguageID.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextLanguageID.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
