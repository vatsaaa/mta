// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTColorMapping extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTColorMapping.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcolormapping5bc6type");
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    STColorSchemeIndex.Enum getBg1();
    
    STColorSchemeIndex xgetBg1();
    
    void setBg1(final STColorSchemeIndex.Enum p0);
    
    void xsetBg1(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getTx1();
    
    STColorSchemeIndex xgetTx1();
    
    void setTx1(final STColorSchemeIndex.Enum p0);
    
    void xsetTx1(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getBg2();
    
    STColorSchemeIndex xgetBg2();
    
    void setBg2(final STColorSchemeIndex.Enum p0);
    
    void xsetBg2(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getTx2();
    
    STColorSchemeIndex xgetTx2();
    
    void setTx2(final STColorSchemeIndex.Enum p0);
    
    void xsetTx2(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getAccent1();
    
    STColorSchemeIndex xgetAccent1();
    
    void setAccent1(final STColorSchemeIndex.Enum p0);
    
    void xsetAccent1(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getAccent2();
    
    STColorSchemeIndex xgetAccent2();
    
    void setAccent2(final STColorSchemeIndex.Enum p0);
    
    void xsetAccent2(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getAccent3();
    
    STColorSchemeIndex xgetAccent3();
    
    void setAccent3(final STColorSchemeIndex.Enum p0);
    
    void xsetAccent3(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getAccent4();
    
    STColorSchemeIndex xgetAccent4();
    
    void setAccent4(final STColorSchemeIndex.Enum p0);
    
    void xsetAccent4(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getAccent5();
    
    STColorSchemeIndex xgetAccent5();
    
    void setAccent5(final STColorSchemeIndex.Enum p0);
    
    void xsetAccent5(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getAccent6();
    
    STColorSchemeIndex xgetAccent6();
    
    void setAccent6(final STColorSchemeIndex.Enum p0);
    
    void xsetAccent6(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getHlink();
    
    STColorSchemeIndex xgetHlink();
    
    void setHlink(final STColorSchemeIndex.Enum p0);
    
    void xsetHlink(final STColorSchemeIndex p0);
    
    STColorSchemeIndex.Enum getFolHlink();
    
    STColorSchemeIndex xgetFolHlink();
    
    void setFolHlink(final STColorSchemeIndex.Enum p0);
    
    void xsetFolHlink(final STColorSchemeIndex p0);
    
    public static final class Factory
    {
        public static CTColorMapping newInstance() {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().newInstance(CTColorMapping.type, null);
        }
        
        public static CTColorMapping newInstance(final XmlOptions xmlOptions) {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().newInstance(CTColorMapping.type, xmlOptions);
        }
        
        public static CTColorMapping parse(final String s) throws XmlException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(s, CTColorMapping.type, null);
        }
        
        public static CTColorMapping parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(s, CTColorMapping.type, xmlOptions);
        }
        
        public static CTColorMapping parse(final File file) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(file, CTColorMapping.type, null);
        }
        
        public static CTColorMapping parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(file, CTColorMapping.type, xmlOptions);
        }
        
        public static CTColorMapping parse(final URL url) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(url, CTColorMapping.type, null);
        }
        
        public static CTColorMapping parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(url, CTColorMapping.type, xmlOptions);
        }
        
        public static CTColorMapping parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(inputStream, CTColorMapping.type, null);
        }
        
        public static CTColorMapping parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(inputStream, CTColorMapping.type, xmlOptions);
        }
        
        public static CTColorMapping parse(final Reader reader) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(reader, CTColorMapping.type, null);
        }
        
        public static CTColorMapping parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(reader, CTColorMapping.type, xmlOptions);
        }
        
        public static CTColorMapping parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTColorMapping.type, null);
        }
        
        public static CTColorMapping parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTColorMapping.type, xmlOptions);
        }
        
        public static CTColorMapping parse(final Node node) throws XmlException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(node, CTColorMapping.type, null);
        }
        
        public static CTColorMapping parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(node, CTColorMapping.type, xmlOptions);
        }
        
        @Deprecated
        public static CTColorMapping parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTColorMapping.type, null);
        }
        
        @Deprecated
        public static CTColorMapping parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTColorMapping)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTColorMapping.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTColorMapping.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTColorMapping.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
