// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextShapeAutofit extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextShapeAutofit.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextshapeautofita009type");
    
    public static final class Factory
    {
        public static CTTextShapeAutofit newInstance() {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().newInstance(CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit newInstance(final XmlOptions xmlOptions) {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().newInstance(CTTextShapeAutofit.type, xmlOptions);
        }
        
        public static CTTextShapeAutofit parse(final String s) throws XmlException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(s, CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(s, CTTextShapeAutofit.type, xmlOptions);
        }
        
        public static CTTextShapeAutofit parse(final File file) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(file, CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(file, CTTextShapeAutofit.type, xmlOptions);
        }
        
        public static CTTextShapeAutofit parse(final URL url) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(url, CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(url, CTTextShapeAutofit.type, xmlOptions);
        }
        
        public static CTTextShapeAutofit parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextShapeAutofit.type, xmlOptions);
        }
        
        public static CTTextShapeAutofit parse(final Reader reader) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(reader, CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(reader, CTTextShapeAutofit.type, xmlOptions);
        }
        
        public static CTTextShapeAutofit parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextShapeAutofit.type, xmlOptions);
        }
        
        public static CTTextShapeAutofit parse(final Node node) throws XmlException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(node, CTTextShapeAutofit.type, null);
        }
        
        public static CTTextShapeAutofit parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(node, CTTextShapeAutofit.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextShapeAutofit parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextShapeAutofit.type, null);
        }
        
        @Deprecated
        public static CTTextShapeAutofit parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextShapeAutofit)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextShapeAutofit.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextShapeAutofit.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextShapeAutofit.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
