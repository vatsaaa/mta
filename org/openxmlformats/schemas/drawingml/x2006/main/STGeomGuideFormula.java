// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STGeomGuideFormula extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STGeomGuideFormula.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stgeomguideformula4b51type");
    
    public static final class Factory
    {
        public static STGeomGuideFormula newValue(final Object o) {
            return (STGeomGuideFormula)STGeomGuideFormula.type.newValue(o);
        }
        
        public static STGeomGuideFormula newInstance() {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().newInstance(STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula newInstance(final XmlOptions xmlOptions) {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().newInstance(STGeomGuideFormula.type, xmlOptions);
        }
        
        public static STGeomGuideFormula parse(final String s) throws XmlException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(s, STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(s, STGeomGuideFormula.type, xmlOptions);
        }
        
        public static STGeomGuideFormula parse(final File file) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(file, STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(file, STGeomGuideFormula.type, xmlOptions);
        }
        
        public static STGeomGuideFormula parse(final URL url) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(url, STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(url, STGeomGuideFormula.type, xmlOptions);
        }
        
        public static STGeomGuideFormula parse(final InputStream inputStream) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(inputStream, STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(inputStream, STGeomGuideFormula.type, xmlOptions);
        }
        
        public static STGeomGuideFormula parse(final Reader reader) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(reader, STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(reader, STGeomGuideFormula.type, xmlOptions);
        }
        
        public static STGeomGuideFormula parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STGeomGuideFormula.type, xmlOptions);
        }
        
        public static STGeomGuideFormula parse(final Node node) throws XmlException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(node, STGeomGuideFormula.type, null);
        }
        
        public static STGeomGuideFormula parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(node, STGeomGuideFormula.type, xmlOptions);
        }
        
        @Deprecated
        public static STGeomGuideFormula parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STGeomGuideFormula.type, null);
        }
        
        @Deprecated
        public static STGeomGuideFormula parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STGeomGuideFormula)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STGeomGuideFormula.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STGeomGuideFormula.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STGeomGuideFormula.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
