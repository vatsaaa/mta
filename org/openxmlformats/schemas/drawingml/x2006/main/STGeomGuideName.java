// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlToken;

public interface STGeomGuideName extends XmlToken
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STGeomGuideName.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stgeomguidename366ctype");
    
    public static final class Factory
    {
        public static STGeomGuideName newValue(final Object o) {
            return (STGeomGuideName)STGeomGuideName.type.newValue(o);
        }
        
        public static STGeomGuideName newInstance() {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().newInstance(STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName newInstance(final XmlOptions xmlOptions) {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().newInstance(STGeomGuideName.type, xmlOptions);
        }
        
        public static STGeomGuideName parse(final String s) throws XmlException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(s, STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(s, STGeomGuideName.type, xmlOptions);
        }
        
        public static STGeomGuideName parse(final File file) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(file, STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(file, STGeomGuideName.type, xmlOptions);
        }
        
        public static STGeomGuideName parse(final URL url) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(url, STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(url, STGeomGuideName.type, xmlOptions);
        }
        
        public static STGeomGuideName parse(final InputStream inputStream) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(inputStream, STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(inputStream, STGeomGuideName.type, xmlOptions);
        }
        
        public static STGeomGuideName parse(final Reader reader) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(reader, STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(reader, STGeomGuideName.type, xmlOptions);
        }
        
        public static STGeomGuideName parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STGeomGuideName.type, xmlOptions);
        }
        
        public static STGeomGuideName parse(final Node node) throws XmlException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(node, STGeomGuideName.type, null);
        }
        
        public static STGeomGuideName parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(node, STGeomGuideName.type, xmlOptions);
        }
        
        @Deprecated
        public static STGeomGuideName parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STGeomGuideName.type, null);
        }
        
        @Deprecated
        public static STGeomGuideName parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STGeomGuideName)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STGeomGuideName.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STGeomGuideName.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STGeomGuideName.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
