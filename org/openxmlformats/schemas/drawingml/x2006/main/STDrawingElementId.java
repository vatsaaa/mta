// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STDrawingElementId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STDrawingElementId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stdrawingelementid75a4type");
    
    public static final class Factory
    {
        public static STDrawingElementId newValue(final Object o) {
            return (STDrawingElementId)STDrawingElementId.type.newValue(o);
        }
        
        public static STDrawingElementId newInstance() {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().newInstance(STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId newInstance(final XmlOptions xmlOptions) {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().newInstance(STDrawingElementId.type, xmlOptions);
        }
        
        public static STDrawingElementId parse(final String s) throws XmlException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(s, STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(s, STDrawingElementId.type, xmlOptions);
        }
        
        public static STDrawingElementId parse(final File file) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(file, STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(file, STDrawingElementId.type, xmlOptions);
        }
        
        public static STDrawingElementId parse(final URL url) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(url, STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(url, STDrawingElementId.type, xmlOptions);
        }
        
        public static STDrawingElementId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(inputStream, STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(inputStream, STDrawingElementId.type, xmlOptions);
        }
        
        public static STDrawingElementId parse(final Reader reader) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(reader, STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(reader, STDrawingElementId.type, xmlOptions);
        }
        
        public static STDrawingElementId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STDrawingElementId.type, xmlOptions);
        }
        
        public static STDrawingElementId parse(final Node node) throws XmlException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(node, STDrawingElementId.type, null);
        }
        
        public static STDrawingElementId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(node, STDrawingElementId.type, xmlOptions);
        }
        
        @Deprecated
        public static STDrawingElementId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STDrawingElementId.type, null);
        }
        
        @Deprecated
        public static STDrawingElementId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STDrawingElementId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STDrawingElementId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STDrawingElementId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STDrawingElementId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
