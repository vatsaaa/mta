// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlToken;

public interface STLineEndWidth extends XmlToken
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLineEndWidth.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlineendwidth16aatype");
    public static final Enum SM = Enum.forString("sm");
    public static final Enum MED = Enum.forString("med");
    public static final Enum LG = Enum.forString("lg");
    public static final int INT_SM = 1;
    public static final int INT_MED = 2;
    public static final int INT_LG = 3;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STLineEndWidth newValue(final Object o) {
            return (STLineEndWidth)STLineEndWidth.type.newValue(o);
        }
        
        public static STLineEndWidth newInstance() {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().newInstance(STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth newInstance(final XmlOptions xmlOptions) {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().newInstance(STLineEndWidth.type, xmlOptions);
        }
        
        public static STLineEndWidth parse(final String s) throws XmlException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(s, STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(s, STLineEndWidth.type, xmlOptions);
        }
        
        public static STLineEndWidth parse(final File file) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(file, STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(file, STLineEndWidth.type, xmlOptions);
        }
        
        public static STLineEndWidth parse(final URL url) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(url, STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(url, STLineEndWidth.type, xmlOptions);
        }
        
        public static STLineEndWidth parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(inputStream, STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(inputStream, STLineEndWidth.type, xmlOptions);
        }
        
        public static STLineEndWidth parse(final Reader reader) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(reader, STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(reader, STLineEndWidth.type, xmlOptions);
        }
        
        public static STLineEndWidth parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineEndWidth.type, xmlOptions);
        }
        
        public static STLineEndWidth parse(final Node node) throws XmlException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(node, STLineEndWidth.type, null);
        }
        
        public static STLineEndWidth parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(node, STLineEndWidth.type, xmlOptions);
        }
        
        @Deprecated
        public static STLineEndWidth parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineEndWidth.type, null);
        }
        
        @Deprecated
        public static STLineEndWidth parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLineEndWidth)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineEndWidth.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineEndWidth.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineEndWidth.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_SM = 1;
        static final int INT_MED = 2;
        static final int INT_LG = 3;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("sm", 1), new Enum("med", 2), new Enum("lg", 3) });
        }
    }
}
