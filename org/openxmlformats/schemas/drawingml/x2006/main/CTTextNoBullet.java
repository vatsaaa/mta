// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextNoBullet extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextNoBullet.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextnobulleta08btype");
    
    public static final class Factory
    {
        public static CTTextNoBullet newInstance() {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().newInstance(CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet newInstance(final XmlOptions xmlOptions) {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().newInstance(CTTextNoBullet.type, xmlOptions);
        }
        
        public static CTTextNoBullet parse(final String s) throws XmlException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(s, CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(s, CTTextNoBullet.type, xmlOptions);
        }
        
        public static CTTextNoBullet parse(final File file) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(file, CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(file, CTTextNoBullet.type, xmlOptions);
        }
        
        public static CTTextNoBullet parse(final URL url) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(url, CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(url, CTTextNoBullet.type, xmlOptions);
        }
        
        public static CTTextNoBullet parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextNoBullet.type, xmlOptions);
        }
        
        public static CTTextNoBullet parse(final Reader reader) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(reader, CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(reader, CTTextNoBullet.type, xmlOptions);
        }
        
        public static CTTextNoBullet parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextNoBullet.type, xmlOptions);
        }
        
        public static CTTextNoBullet parse(final Node node) throws XmlException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(node, CTTextNoBullet.type, null);
        }
        
        public static CTTextNoBullet parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(node, CTTextNoBullet.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextNoBullet parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextNoBullet.type, null);
        }
        
        @Deprecated
        public static CTTextNoBullet parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextNoBullet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextNoBullet.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextNoBullet.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextNoBullet.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
