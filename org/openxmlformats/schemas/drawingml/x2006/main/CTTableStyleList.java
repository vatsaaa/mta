// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTableStyleList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTableStyleList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttablestylelist4bdctype");
    
    List<CTTableStyle> getTblStyleList();
    
    @Deprecated
    CTTableStyle[] getTblStyleArray();
    
    CTTableStyle getTblStyleArray(final int p0);
    
    int sizeOfTblStyleArray();
    
    void setTblStyleArray(final CTTableStyle[] p0);
    
    void setTblStyleArray(final int p0, final CTTableStyle p1);
    
    CTTableStyle insertNewTblStyle(final int p0);
    
    CTTableStyle addNewTblStyle();
    
    void removeTblStyle(final int p0);
    
    String getDef();
    
    STGuid xgetDef();
    
    void setDef(final String p0);
    
    void xsetDef(final STGuid p0);
    
    public static final class Factory
    {
        public static CTTableStyleList newInstance() {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().newInstance(CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList newInstance(final XmlOptions xmlOptions) {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().newInstance(CTTableStyleList.type, xmlOptions);
        }
        
        public static CTTableStyleList parse(final String s) throws XmlException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(s, CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(s, CTTableStyleList.type, xmlOptions);
        }
        
        public static CTTableStyleList parse(final File file) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(file, CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(file, CTTableStyleList.type, xmlOptions);
        }
        
        public static CTTableStyleList parse(final URL url) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(url, CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(url, CTTableStyleList.type, xmlOptions);
        }
        
        public static CTTableStyleList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableStyleList.type, xmlOptions);
        }
        
        public static CTTableStyleList parse(final Reader reader) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(reader, CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(reader, CTTableStyleList.type, xmlOptions);
        }
        
        public static CTTableStyleList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableStyleList.type, xmlOptions);
        }
        
        public static CTTableStyleList parse(final Node node) throws XmlException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(node, CTTableStyleList.type, null);
        }
        
        public static CTTableStyleList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(node, CTTableStyleList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTableStyleList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableStyleList.type, null);
        }
        
        @Deprecated
        public static CTTableStyleList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTableStyleList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableStyleList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableStyleList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableStyleList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
