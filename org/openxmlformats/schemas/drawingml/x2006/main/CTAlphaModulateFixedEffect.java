// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTAlphaModulateFixedEffect extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTAlphaModulateFixedEffect.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctalphamodulatefixedeffect9769type");
    
    int getAmt();
    
    STPositivePercentage xgetAmt();
    
    boolean isSetAmt();
    
    void setAmt(final int p0);
    
    void xsetAmt(final STPositivePercentage p0);
    
    void unsetAmt();
    
    public static final class Factory
    {
        public static CTAlphaModulateFixedEffect newInstance() {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().newInstance(CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect newInstance(final XmlOptions xmlOptions) {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().newInstance(CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        public static CTAlphaModulateFixedEffect parse(final String s) throws XmlException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(s, CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(s, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        public static CTAlphaModulateFixedEffect parse(final File file) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(file, CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(file, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        public static CTAlphaModulateFixedEffect parse(final URL url) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(url, CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(url, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        public static CTAlphaModulateFixedEffect parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(inputStream, CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(inputStream, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        public static CTAlphaModulateFixedEffect parse(final Reader reader) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(reader, CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(reader, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        public static CTAlphaModulateFixedEffect parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        public static CTAlphaModulateFixedEffect parse(final Node node) throws XmlException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(node, CTAlphaModulateFixedEffect.type, null);
        }
        
        public static CTAlphaModulateFixedEffect parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(node, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        @Deprecated
        public static CTAlphaModulateFixedEffect parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAlphaModulateFixedEffect.type, null);
        }
        
        @Deprecated
        public static CTAlphaModulateFixedEffect parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTAlphaModulateFixedEffect)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAlphaModulateFixedEffect.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAlphaModulateFixedEffect.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
