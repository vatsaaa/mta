// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STTextTypeface extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTextTypeface.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttexttypefacea80ftype");
    
    public static final class Factory
    {
        public static STTextTypeface newValue(final Object o) {
            return (STTextTypeface)STTextTypeface.type.newValue(o);
        }
        
        public static STTextTypeface newInstance() {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().newInstance(STTextTypeface.type, null);
        }
        
        public static STTextTypeface newInstance(final XmlOptions xmlOptions) {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().newInstance(STTextTypeface.type, xmlOptions);
        }
        
        public static STTextTypeface parse(final String s) throws XmlException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(s, STTextTypeface.type, null);
        }
        
        public static STTextTypeface parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(s, STTextTypeface.type, xmlOptions);
        }
        
        public static STTextTypeface parse(final File file) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(file, STTextTypeface.type, null);
        }
        
        public static STTextTypeface parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(file, STTextTypeface.type, xmlOptions);
        }
        
        public static STTextTypeface parse(final URL url) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(url, STTextTypeface.type, null);
        }
        
        public static STTextTypeface parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(url, STTextTypeface.type, xmlOptions);
        }
        
        public static STTextTypeface parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(inputStream, STTextTypeface.type, null);
        }
        
        public static STTextTypeface parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(inputStream, STTextTypeface.type, xmlOptions);
        }
        
        public static STTextTypeface parse(final Reader reader) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(reader, STTextTypeface.type, null);
        }
        
        public static STTextTypeface parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(reader, STTextTypeface.type, xmlOptions);
        }
        
        public static STTextTypeface parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextTypeface.type, null);
        }
        
        public static STTextTypeface parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTextTypeface.type, xmlOptions);
        }
        
        public static STTextTypeface parse(final Node node) throws XmlException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(node, STTextTypeface.type, null);
        }
        
        public static STTextTypeface parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(node, STTextTypeface.type, xmlOptions);
        }
        
        @Deprecated
        public static STTextTypeface parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextTypeface.type, null);
        }
        
        @Deprecated
        public static STTextTypeface parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTextTypeface)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTextTypeface.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextTypeface.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTextTypeface.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
