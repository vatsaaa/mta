// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTShapeStyle extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTShapeStyle.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctshapestyle81ebtype");
    
    CTStyleMatrixReference getLnRef();
    
    void setLnRef(final CTStyleMatrixReference p0);
    
    CTStyleMatrixReference addNewLnRef();
    
    CTStyleMatrixReference getFillRef();
    
    void setFillRef(final CTStyleMatrixReference p0);
    
    CTStyleMatrixReference addNewFillRef();
    
    CTStyleMatrixReference getEffectRef();
    
    void setEffectRef(final CTStyleMatrixReference p0);
    
    CTStyleMatrixReference addNewEffectRef();
    
    CTFontReference getFontRef();
    
    void setFontRef(final CTFontReference p0);
    
    CTFontReference addNewFontRef();
    
    public static final class Factory
    {
        public static CTShapeStyle newInstance() {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().newInstance(CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle newInstance(final XmlOptions xmlOptions) {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().newInstance(CTShapeStyle.type, xmlOptions);
        }
        
        public static CTShapeStyle parse(final String s) throws XmlException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(s, CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(s, CTShapeStyle.type, xmlOptions);
        }
        
        public static CTShapeStyle parse(final File file) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(file, CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(file, CTShapeStyle.type, xmlOptions);
        }
        
        public static CTShapeStyle parse(final URL url) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(url, CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(url, CTShapeStyle.type, xmlOptions);
        }
        
        public static CTShapeStyle parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(inputStream, CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(inputStream, CTShapeStyle.type, xmlOptions);
        }
        
        public static CTShapeStyle parse(final Reader reader) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(reader, CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(reader, CTShapeStyle.type, xmlOptions);
        }
        
        public static CTShapeStyle parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTShapeStyle.type, xmlOptions);
        }
        
        public static CTShapeStyle parse(final Node node) throws XmlException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(node, CTShapeStyle.type, null);
        }
        
        public static CTShapeStyle parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(node, CTShapeStyle.type, xmlOptions);
        }
        
        @Deprecated
        public static CTShapeStyle parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShapeStyle.type, null);
        }
        
        @Deprecated
        public static CTShapeStyle parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTShapeStyle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTShapeStyle.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShapeStyle.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTShapeStyle.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
