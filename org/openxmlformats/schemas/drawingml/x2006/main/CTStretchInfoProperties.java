// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTStretchInfoProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTStretchInfoProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctstretchinfopropertiesde57type");
    
    CTRelativeRect getFillRect();
    
    boolean isSetFillRect();
    
    void setFillRect(final CTRelativeRect p0);
    
    CTRelativeRect addNewFillRect();
    
    void unsetFillRect();
    
    public static final class Factory
    {
        public static CTStretchInfoProperties newInstance() {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().newInstance(CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties newInstance(final XmlOptions xmlOptions) {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().newInstance(CTStretchInfoProperties.type, xmlOptions);
        }
        
        public static CTStretchInfoProperties parse(final String s) throws XmlException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(s, CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(s, CTStretchInfoProperties.type, xmlOptions);
        }
        
        public static CTStretchInfoProperties parse(final File file) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(file, CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(file, CTStretchInfoProperties.type, xmlOptions);
        }
        
        public static CTStretchInfoProperties parse(final URL url) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(url, CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(url, CTStretchInfoProperties.type, xmlOptions);
        }
        
        public static CTStretchInfoProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTStretchInfoProperties.type, xmlOptions);
        }
        
        public static CTStretchInfoProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(reader, CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(reader, CTStretchInfoProperties.type, xmlOptions);
        }
        
        public static CTStretchInfoProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStretchInfoProperties.type, xmlOptions);
        }
        
        public static CTStretchInfoProperties parse(final Node node) throws XmlException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(node, CTStretchInfoProperties.type, null);
        }
        
        public static CTStretchInfoProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(node, CTStretchInfoProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTStretchInfoProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStretchInfoProperties.type, null);
        }
        
        @Deprecated
        public static CTStretchInfoProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTStretchInfoProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStretchInfoProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStretchInfoProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStretchInfoProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
