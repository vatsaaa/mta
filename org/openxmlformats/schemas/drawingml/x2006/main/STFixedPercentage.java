// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STFixedPercentage extends STPercentage
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STFixedPercentage.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stfixedpercentagef0cftype");
    
    public static final class Factory
    {
        public static STFixedPercentage newValue(final Object o) {
            return (STFixedPercentage)STFixedPercentage.type.newValue(o);
        }
        
        public static STFixedPercentage newInstance() {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage newInstance(final XmlOptions xmlOptions) {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().newInstance(STFixedPercentage.type, xmlOptions);
        }
        
        public static STFixedPercentage parse(final String s) throws XmlException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(s, STFixedPercentage.type, xmlOptions);
        }
        
        public static STFixedPercentage parse(final File file) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(file, STFixedPercentage.type, xmlOptions);
        }
        
        public static STFixedPercentage parse(final URL url) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(url, STFixedPercentage.type, xmlOptions);
        }
        
        public static STFixedPercentage parse(final InputStream inputStream) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(inputStream, STFixedPercentage.type, xmlOptions);
        }
        
        public static STFixedPercentage parse(final Reader reader) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(reader, STFixedPercentage.type, xmlOptions);
        }
        
        public static STFixedPercentage parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFixedPercentage.type, xmlOptions);
        }
        
        public static STFixedPercentage parse(final Node node) throws XmlException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, STFixedPercentage.type, null);
        }
        
        public static STFixedPercentage parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(node, STFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static STFixedPercentage parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFixedPercentage.type, null);
        }
        
        @Deprecated
        public static STFixedPercentage parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STFixedPercentage)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFixedPercentage.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFixedPercentage.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFixedPercentage.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
