// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlLong;

public interface STPositiveCoordinate extends XmlLong
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPositiveCoordinate.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpositivecoordinatecbfctype");
    
    public static final class Factory
    {
        public static STPositiveCoordinate newValue(final Object o) {
            return (STPositiveCoordinate)STPositiveCoordinate.type.newValue(o);
        }
        
        public static STPositiveCoordinate newInstance() {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().newInstance(STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate newInstance(final XmlOptions xmlOptions) {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().newInstance(STPositiveCoordinate.type, xmlOptions);
        }
        
        public static STPositiveCoordinate parse(final String s) throws XmlException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(s, STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(s, STPositiveCoordinate.type, xmlOptions);
        }
        
        public static STPositiveCoordinate parse(final File file) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(file, STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(file, STPositiveCoordinate.type, xmlOptions);
        }
        
        public static STPositiveCoordinate parse(final URL url) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(url, STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(url, STPositiveCoordinate.type, xmlOptions);
        }
        
        public static STPositiveCoordinate parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(inputStream, STPositiveCoordinate.type, xmlOptions);
        }
        
        public static STPositiveCoordinate parse(final Reader reader) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(reader, STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(reader, STPositiveCoordinate.type, xmlOptions);
        }
        
        public static STPositiveCoordinate parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPositiveCoordinate.type, xmlOptions);
        }
        
        public static STPositiveCoordinate parse(final Node node) throws XmlException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(node, STPositiveCoordinate.type, null);
        }
        
        public static STPositiveCoordinate parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(node, STPositiveCoordinate.type, xmlOptions);
        }
        
        @Deprecated
        public static STPositiveCoordinate parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveCoordinate.type, null);
        }
        
        @Deprecated
        public static STPositiveCoordinate parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPositiveCoordinate)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPositiveCoordinate.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveCoordinate.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPositiveCoordinate.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
