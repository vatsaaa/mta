// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInt;

public interface STAngle extends XmlInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STAngle.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stangle8074type");
    
    public static final class Factory
    {
        public static STAngle newValue(final Object o) {
            return (STAngle)STAngle.type.newValue(o);
        }
        
        public static STAngle newInstance() {
            return (STAngle)XmlBeans.getContextTypeLoader().newInstance(STAngle.type, null);
        }
        
        public static STAngle newInstance(final XmlOptions xmlOptions) {
            return (STAngle)XmlBeans.getContextTypeLoader().newInstance(STAngle.type, xmlOptions);
        }
        
        public static STAngle parse(final String s) throws XmlException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(s, STAngle.type, null);
        }
        
        public static STAngle parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(s, STAngle.type, xmlOptions);
        }
        
        public static STAngle parse(final File file) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(file, STAngle.type, null);
        }
        
        public static STAngle parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(file, STAngle.type, xmlOptions);
        }
        
        public static STAngle parse(final URL url) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(url, STAngle.type, null);
        }
        
        public static STAngle parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(url, STAngle.type, xmlOptions);
        }
        
        public static STAngle parse(final InputStream inputStream) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(inputStream, STAngle.type, null);
        }
        
        public static STAngle parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(inputStream, STAngle.type, xmlOptions);
        }
        
        public static STAngle parse(final Reader reader) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(reader, STAngle.type, null);
        }
        
        public static STAngle parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(reader, STAngle.type, xmlOptions);
        }
        
        public static STAngle parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAngle.type, null);
        }
        
        public static STAngle parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAngle.type, xmlOptions);
        }
        
        public static STAngle parse(final Node node) throws XmlException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(node, STAngle.type, null);
        }
        
        public static STAngle parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(node, STAngle.type, xmlOptions);
        }
        
        @Deprecated
        public static STAngle parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAngle.type, null);
        }
        
        @Deprecated
        public static STAngle parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STAngle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAngle.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAngle.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAngle.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
