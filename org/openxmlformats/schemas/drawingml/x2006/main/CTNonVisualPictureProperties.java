// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNonVisualPictureProperties extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNonVisualPictureProperties.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnonvisualpicturepropertiesee3ftype");
    
    CTPictureLocking getPicLocks();
    
    boolean isSetPicLocks();
    
    void setPicLocks(final CTPictureLocking p0);
    
    CTPictureLocking addNewPicLocks();
    
    void unsetPicLocks();
    
    CTOfficeArtExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTOfficeArtExtensionList p0);
    
    CTOfficeArtExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    boolean getPreferRelativeResize();
    
    XmlBoolean xgetPreferRelativeResize();
    
    boolean isSetPreferRelativeResize();
    
    void setPreferRelativeResize(final boolean p0);
    
    void xsetPreferRelativeResize(final XmlBoolean p0);
    
    void unsetPreferRelativeResize();
    
    public static final class Factory
    {
        public static CTNonVisualPictureProperties newInstance() {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().newInstance(CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties newInstance(final XmlOptions xmlOptions) {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().newInstance(CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        public static CTNonVisualPictureProperties parse(final String s) throws XmlException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(s, CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(s, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        public static CTNonVisualPictureProperties parse(final File file) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(file, CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(file, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        public static CTNonVisualPictureProperties parse(final URL url) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(url, CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(url, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        public static CTNonVisualPictureProperties parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(inputStream, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        public static CTNonVisualPictureProperties parse(final Reader reader) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(reader, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        public static CTNonVisualPictureProperties parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        public static CTNonVisualPictureProperties parse(final Node node) throws XmlException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(node, CTNonVisualPictureProperties.type, null);
        }
        
        public static CTNonVisualPictureProperties parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(node, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNonVisualPictureProperties parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNonVisualPictureProperties.type, null);
        }
        
        @Deprecated
        public static CTNonVisualPictureProperties parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNonVisualPictureProperties)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNonVisualPictureProperties.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNonVisualPictureProperties.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
