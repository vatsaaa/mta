// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.STWrapDistance;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STWrapDistanceImpl extends JavaLongHolderEx implements STWrapDistance
{
    public STWrapDistanceImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STWrapDistanceImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
