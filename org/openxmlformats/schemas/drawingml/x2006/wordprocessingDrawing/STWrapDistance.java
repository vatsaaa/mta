// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STWrapDistance extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STWrapDistance.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stwrapdistanceea50type");
    
    public static final class Factory
    {
        public static STWrapDistance newValue(final Object o) {
            return (STWrapDistance)STWrapDistance.type.newValue(o);
        }
        
        public static STWrapDistance newInstance() {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().newInstance(STWrapDistance.type, null);
        }
        
        public static STWrapDistance newInstance(final XmlOptions xmlOptions) {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().newInstance(STWrapDistance.type, xmlOptions);
        }
        
        public static STWrapDistance parse(final String s) throws XmlException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(s, STWrapDistance.type, null);
        }
        
        public static STWrapDistance parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(s, STWrapDistance.type, xmlOptions);
        }
        
        public static STWrapDistance parse(final File file) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(file, STWrapDistance.type, null);
        }
        
        public static STWrapDistance parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(file, STWrapDistance.type, xmlOptions);
        }
        
        public static STWrapDistance parse(final URL url) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(url, STWrapDistance.type, null);
        }
        
        public static STWrapDistance parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(url, STWrapDistance.type, xmlOptions);
        }
        
        public static STWrapDistance parse(final InputStream inputStream) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(inputStream, STWrapDistance.type, null);
        }
        
        public static STWrapDistance parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(inputStream, STWrapDistance.type, xmlOptions);
        }
        
        public static STWrapDistance parse(final Reader reader) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(reader, STWrapDistance.type, null);
        }
        
        public static STWrapDistance parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(reader, STWrapDistance.type, xmlOptions);
        }
        
        public static STWrapDistance parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STWrapDistance.type, null);
        }
        
        public static STWrapDistance parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STWrapDistance.type, xmlOptions);
        }
        
        public static STWrapDistance parse(final Node node) throws XmlException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(node, STWrapDistance.type, null);
        }
        
        public static STWrapDistance parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(node, STWrapDistance.type, xmlOptions);
        }
        
        @Deprecated
        public static STWrapDistance parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STWrapDistance.type, null);
        }
        
        @Deprecated
        public static STWrapDistance parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STWrapDistance)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STWrapDistance.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STWrapDistance.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STWrapDistance.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
