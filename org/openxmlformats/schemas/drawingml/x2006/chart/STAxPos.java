// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STAxPos extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STAxPos.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("staxpos4379type");
    public static final Enum B = Enum.forString("b");
    public static final Enum L = Enum.forString("l");
    public static final Enum R = Enum.forString("r");
    public static final Enum T = Enum.forString("t");
    public static final int INT_B = 1;
    public static final int INT_L = 2;
    public static final int INT_R = 3;
    public static final int INT_T = 4;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STAxPos newValue(final Object o) {
            return (STAxPos)STAxPos.type.newValue(o);
        }
        
        public static STAxPos newInstance() {
            return (STAxPos)XmlBeans.getContextTypeLoader().newInstance(STAxPos.type, null);
        }
        
        public static STAxPos newInstance(final XmlOptions xmlOptions) {
            return (STAxPos)XmlBeans.getContextTypeLoader().newInstance(STAxPos.type, xmlOptions);
        }
        
        public static STAxPos parse(final String s) throws XmlException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(s, STAxPos.type, null);
        }
        
        public static STAxPos parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(s, STAxPos.type, xmlOptions);
        }
        
        public static STAxPos parse(final File file) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(file, STAxPos.type, null);
        }
        
        public static STAxPos parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(file, STAxPos.type, xmlOptions);
        }
        
        public static STAxPos parse(final URL url) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(url, STAxPos.type, null);
        }
        
        public static STAxPos parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(url, STAxPos.type, xmlOptions);
        }
        
        public static STAxPos parse(final InputStream inputStream) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(inputStream, STAxPos.type, null);
        }
        
        public static STAxPos parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(inputStream, STAxPos.type, xmlOptions);
        }
        
        public static STAxPos parse(final Reader reader) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(reader, STAxPos.type, null);
        }
        
        public static STAxPos parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(reader, STAxPos.type, xmlOptions);
        }
        
        public static STAxPos parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAxPos.type, null);
        }
        
        public static STAxPos parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STAxPos.type, xmlOptions);
        }
        
        public static STAxPos parse(final Node node) throws XmlException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(node, STAxPos.type, null);
        }
        
        public static STAxPos parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(node, STAxPos.type, xmlOptions);
        }
        
        @Deprecated
        public static STAxPos parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAxPos.type, null);
        }
        
        @Deprecated
        public static STAxPos parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STAxPos)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STAxPos.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAxPos.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STAxPos.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_B = 1;
        static final int INT_L = 2;
        static final int INT_R = 3;
        static final int INT_T = 4;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("b", 1), new Enum("l", 2), new Enum("r", 3), new Enum("t", 4) });
        }
    }
}
