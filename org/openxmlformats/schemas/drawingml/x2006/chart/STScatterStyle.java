// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STScatterStyle extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STScatterStyle.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stscatterstyle9eb9type");
    public static final Enum NONE = Enum.forString("none");
    public static final Enum LINE = Enum.forString("line");
    public static final Enum LINE_MARKER = Enum.forString("lineMarker");
    public static final Enum MARKER = Enum.forString("marker");
    public static final Enum SMOOTH = Enum.forString("smooth");
    public static final Enum SMOOTH_MARKER = Enum.forString("smoothMarker");
    public static final int INT_NONE = 1;
    public static final int INT_LINE = 2;
    public static final int INT_LINE_MARKER = 3;
    public static final int INT_MARKER = 4;
    public static final int INT_SMOOTH = 5;
    public static final int INT_SMOOTH_MARKER = 6;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STScatterStyle newValue(final Object o) {
            return (STScatterStyle)STScatterStyle.type.newValue(o);
        }
        
        public static STScatterStyle newInstance() {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().newInstance(STScatterStyle.type, null);
        }
        
        public static STScatterStyle newInstance(final XmlOptions xmlOptions) {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().newInstance(STScatterStyle.type, xmlOptions);
        }
        
        public static STScatterStyle parse(final String s) throws XmlException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(s, STScatterStyle.type, null);
        }
        
        public static STScatterStyle parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(s, STScatterStyle.type, xmlOptions);
        }
        
        public static STScatterStyle parse(final File file) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(file, STScatterStyle.type, null);
        }
        
        public static STScatterStyle parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(file, STScatterStyle.type, xmlOptions);
        }
        
        public static STScatterStyle parse(final URL url) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(url, STScatterStyle.type, null);
        }
        
        public static STScatterStyle parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(url, STScatterStyle.type, xmlOptions);
        }
        
        public static STScatterStyle parse(final InputStream inputStream) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(inputStream, STScatterStyle.type, null);
        }
        
        public static STScatterStyle parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(inputStream, STScatterStyle.type, xmlOptions);
        }
        
        public static STScatterStyle parse(final Reader reader) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(reader, STScatterStyle.type, null);
        }
        
        public static STScatterStyle parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(reader, STScatterStyle.type, xmlOptions);
        }
        
        public static STScatterStyle parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STScatterStyle.type, null);
        }
        
        public static STScatterStyle parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STScatterStyle.type, xmlOptions);
        }
        
        public static STScatterStyle parse(final Node node) throws XmlException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(node, STScatterStyle.type, null);
        }
        
        public static STScatterStyle parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(node, STScatterStyle.type, xmlOptions);
        }
        
        @Deprecated
        public static STScatterStyle parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STScatterStyle.type, null);
        }
        
        @Deprecated
        public static STScatterStyle parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STScatterStyle)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STScatterStyle.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STScatterStyle.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STScatterStyle.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_NONE = 1;
        static final int INT_LINE = 2;
        static final int INT_LINE_MARKER = 3;
        static final int INT_MARKER = 4;
        static final int INT_SMOOTH = 5;
        static final int INT_SMOOTH_MARKER = 6;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("none", 1), new Enum("line", 2), new Enum("lineMarker", 3), new Enum("marker", 4), new Enum("smooth", 5), new Enum("smoothMarker", 6) });
        }
    }
}
