// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STXstring extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STXstring.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stxstringb8cdtype");
    
    public static final class Factory
    {
        public static STXstring newValue(final Object o) {
            return (STXstring)STXstring.type.newValue(o);
        }
        
        public static STXstring newInstance() {
            return (STXstring)XmlBeans.getContextTypeLoader().newInstance(STXstring.type, null);
        }
        
        public static STXstring newInstance(final XmlOptions xmlOptions) {
            return (STXstring)XmlBeans.getContextTypeLoader().newInstance(STXstring.type, xmlOptions);
        }
        
        public static STXstring parse(final String s) throws XmlException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(s, STXstring.type, null);
        }
        
        public static STXstring parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(s, STXstring.type, xmlOptions);
        }
        
        public static STXstring parse(final File file) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(file, STXstring.type, null);
        }
        
        public static STXstring parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(file, STXstring.type, xmlOptions);
        }
        
        public static STXstring parse(final URL url) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(url, STXstring.type, null);
        }
        
        public static STXstring parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(url, STXstring.type, xmlOptions);
        }
        
        public static STXstring parse(final InputStream inputStream) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(inputStream, STXstring.type, null);
        }
        
        public static STXstring parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(inputStream, STXstring.type, xmlOptions);
        }
        
        public static STXstring parse(final Reader reader) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(reader, STXstring.type, null);
        }
        
        public static STXstring parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(reader, STXstring.type, xmlOptions);
        }
        
        public static STXstring parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STXstring.type, null);
        }
        
        public static STXstring parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STXstring.type, xmlOptions);
        }
        
        public static STXstring parse(final Node node) throws XmlException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(node, STXstring.type, null);
        }
        
        public static STXstring parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(node, STXstring.type, xmlOptions);
        }
        
        @Deprecated
        public static STXstring parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STXstring.type, null);
        }
        
        @Deprecated
        public static STXstring parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STXstring)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STXstring.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STXstring.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STXstring.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
