// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTLayout extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTLayout.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctlayout3192type");
    
    CTManualLayout getManualLayout();
    
    boolean isSetManualLayout();
    
    void setManualLayout(final CTManualLayout p0);
    
    CTManualLayout addNewManualLayout();
    
    void unsetManualLayout();
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTLayout newInstance() {
            return (CTLayout)XmlBeans.getContextTypeLoader().newInstance(CTLayout.type, null);
        }
        
        public static CTLayout newInstance(final XmlOptions xmlOptions) {
            return (CTLayout)XmlBeans.getContextTypeLoader().newInstance(CTLayout.type, xmlOptions);
        }
        
        public static CTLayout parse(final String s) throws XmlException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(s, CTLayout.type, null);
        }
        
        public static CTLayout parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(s, CTLayout.type, xmlOptions);
        }
        
        public static CTLayout parse(final File file) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(file, CTLayout.type, null);
        }
        
        public static CTLayout parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(file, CTLayout.type, xmlOptions);
        }
        
        public static CTLayout parse(final URL url) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(url, CTLayout.type, null);
        }
        
        public static CTLayout parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(url, CTLayout.type, xmlOptions);
        }
        
        public static CTLayout parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(inputStream, CTLayout.type, null);
        }
        
        public static CTLayout parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(inputStream, CTLayout.type, xmlOptions);
        }
        
        public static CTLayout parse(final Reader reader) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(reader, CTLayout.type, null);
        }
        
        public static CTLayout parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(reader, CTLayout.type, xmlOptions);
        }
        
        public static CTLayout parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLayout.type, null);
        }
        
        public static CTLayout parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLayout.type, xmlOptions);
        }
        
        public static CTLayout parse(final Node node) throws XmlException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(node, CTLayout.type, null);
        }
        
        public static CTLayout parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(node, CTLayout.type, xmlOptions);
        }
        
        @Deprecated
        public static CTLayout parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLayout.type, null);
        }
        
        @Deprecated
        public static CTLayout parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTLayout)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLayout.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLayout.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLayout.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
