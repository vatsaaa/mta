// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTAxDataSource extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTAxDataSource.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctaxdatasource1440type");
    
    CTMultiLvlStrRef getMultiLvlStrRef();
    
    boolean isSetMultiLvlStrRef();
    
    void setMultiLvlStrRef(final CTMultiLvlStrRef p0);
    
    CTMultiLvlStrRef addNewMultiLvlStrRef();
    
    void unsetMultiLvlStrRef();
    
    CTNumRef getNumRef();
    
    boolean isSetNumRef();
    
    void setNumRef(final CTNumRef p0);
    
    CTNumRef addNewNumRef();
    
    void unsetNumRef();
    
    CTNumData getNumLit();
    
    boolean isSetNumLit();
    
    void setNumLit(final CTNumData p0);
    
    CTNumData addNewNumLit();
    
    void unsetNumLit();
    
    CTStrRef getStrRef();
    
    boolean isSetStrRef();
    
    void setStrRef(final CTStrRef p0);
    
    CTStrRef addNewStrRef();
    
    void unsetStrRef();
    
    CTStrData getStrLit();
    
    boolean isSetStrLit();
    
    void setStrLit(final CTStrData p0);
    
    CTStrData addNewStrLit();
    
    void unsetStrLit();
    
    public static final class Factory
    {
        public static CTAxDataSource newInstance() {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().newInstance(CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource newInstance(final XmlOptions xmlOptions) {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().newInstance(CTAxDataSource.type, xmlOptions);
        }
        
        public static CTAxDataSource parse(final String s) throws XmlException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(s, CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(s, CTAxDataSource.type, xmlOptions);
        }
        
        public static CTAxDataSource parse(final File file) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(file, CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(file, CTAxDataSource.type, xmlOptions);
        }
        
        public static CTAxDataSource parse(final URL url) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(url, CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(url, CTAxDataSource.type, xmlOptions);
        }
        
        public static CTAxDataSource parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(inputStream, CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(inputStream, CTAxDataSource.type, xmlOptions);
        }
        
        public static CTAxDataSource parse(final Reader reader) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(reader, CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(reader, CTAxDataSource.type, xmlOptions);
        }
        
        public static CTAxDataSource parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAxDataSource.type, xmlOptions);
        }
        
        public static CTAxDataSource parse(final Node node) throws XmlException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(node, CTAxDataSource.type, null);
        }
        
        public static CTAxDataSource parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(node, CTAxDataSource.type, xmlOptions);
        }
        
        @Deprecated
        public static CTAxDataSource parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAxDataSource.type, null);
        }
        
        @Deprecated
        public static CTAxDataSource parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTAxDataSource)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAxDataSource.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAxDataSource.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAxDataSource.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
