// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STOrientation extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STOrientation.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("storientationc326type");
    public static final Enum MAX_MIN = Enum.forString("maxMin");
    public static final Enum MIN_MAX = Enum.forString("minMax");
    public static final int INT_MAX_MIN = 1;
    public static final int INT_MIN_MAX = 2;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STOrientation newValue(final Object o) {
            return (STOrientation)STOrientation.type.newValue(o);
        }
        
        public static STOrientation newInstance() {
            return (STOrientation)XmlBeans.getContextTypeLoader().newInstance(STOrientation.type, null);
        }
        
        public static STOrientation newInstance(final XmlOptions xmlOptions) {
            return (STOrientation)XmlBeans.getContextTypeLoader().newInstance(STOrientation.type, xmlOptions);
        }
        
        public static STOrientation parse(final String s) throws XmlException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(s, STOrientation.type, null);
        }
        
        public static STOrientation parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(s, STOrientation.type, xmlOptions);
        }
        
        public static STOrientation parse(final File file) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(file, STOrientation.type, null);
        }
        
        public static STOrientation parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(file, STOrientation.type, xmlOptions);
        }
        
        public static STOrientation parse(final URL url) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(url, STOrientation.type, null);
        }
        
        public static STOrientation parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(url, STOrientation.type, xmlOptions);
        }
        
        public static STOrientation parse(final InputStream inputStream) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(inputStream, STOrientation.type, null);
        }
        
        public static STOrientation parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(inputStream, STOrientation.type, xmlOptions);
        }
        
        public static STOrientation parse(final Reader reader) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(reader, STOrientation.type, null);
        }
        
        public static STOrientation parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(reader, STOrientation.type, xmlOptions);
        }
        
        public static STOrientation parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STOrientation.type, null);
        }
        
        public static STOrientation parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STOrientation.type, xmlOptions);
        }
        
        public static STOrientation parse(final Node node) throws XmlException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(node, STOrientation.type, null);
        }
        
        public static STOrientation parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(node, STOrientation.type, xmlOptions);
        }
        
        @Deprecated
        public static STOrientation parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STOrientation.type, null);
        }
        
        @Deprecated
        public static STOrientation parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STOrientation)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STOrientation.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STOrientation.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STOrientation.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_MAX_MIN = 1;
        static final int INT_MIN_MAX = 2;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("maxMin", 1), new Enum("minMax", 2) });
        }
    }
}
