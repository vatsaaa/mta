// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlDouble;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPageMargins extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPageMargins.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpagemarginsb730type");
    
    double getL();
    
    XmlDouble xgetL();
    
    void setL(final double p0);
    
    void xsetL(final XmlDouble p0);
    
    double getR();
    
    XmlDouble xgetR();
    
    void setR(final double p0);
    
    void xsetR(final XmlDouble p0);
    
    double getT();
    
    XmlDouble xgetT();
    
    void setT(final double p0);
    
    void xsetT(final XmlDouble p0);
    
    double getB();
    
    XmlDouble xgetB();
    
    void setB(final double p0);
    
    void xsetB(final XmlDouble p0);
    
    double getHeader();
    
    XmlDouble xgetHeader();
    
    void setHeader(final double p0);
    
    void xsetHeader(final XmlDouble p0);
    
    double getFooter();
    
    XmlDouble xgetFooter();
    
    void setFooter(final double p0);
    
    void xsetFooter(final XmlDouble p0);
    
    public static final class Factory
    {
        public static CTPageMargins newInstance() {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().newInstance(CTPageMargins.type, null);
        }
        
        public static CTPageMargins newInstance(final XmlOptions xmlOptions) {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().newInstance(CTPageMargins.type, xmlOptions);
        }
        
        public static CTPageMargins parse(final String s) throws XmlException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(s, CTPageMargins.type, null);
        }
        
        public static CTPageMargins parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(s, CTPageMargins.type, xmlOptions);
        }
        
        public static CTPageMargins parse(final File file) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(file, CTPageMargins.type, null);
        }
        
        public static CTPageMargins parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(file, CTPageMargins.type, xmlOptions);
        }
        
        public static CTPageMargins parse(final URL url) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(url, CTPageMargins.type, null);
        }
        
        public static CTPageMargins parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(url, CTPageMargins.type, xmlOptions);
        }
        
        public static CTPageMargins parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(inputStream, CTPageMargins.type, null);
        }
        
        public static CTPageMargins parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(inputStream, CTPageMargins.type, xmlOptions);
        }
        
        public static CTPageMargins parse(final Reader reader) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(reader, CTPageMargins.type, null);
        }
        
        public static CTPageMargins parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(reader, CTPageMargins.type, xmlOptions);
        }
        
        public static CTPageMargins parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPageMargins.type, null);
        }
        
        public static CTPageMargins parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPageMargins.type, xmlOptions);
        }
        
        public static CTPageMargins parse(final Node node) throws XmlException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(node, CTPageMargins.type, null);
        }
        
        public static CTPageMargins parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(node, CTPageMargins.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPageMargins parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPageMargins.type, null);
        }
        
        @Deprecated
        public static CTPageMargins parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPageMargins)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPageMargins.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPageMargins.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPageMargins.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
