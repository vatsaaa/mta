// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNumDataSource extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNumDataSource.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnumdatasourcef0bbtype");
    
    CTNumRef getNumRef();
    
    boolean isSetNumRef();
    
    void setNumRef(final CTNumRef p0);
    
    CTNumRef addNewNumRef();
    
    void unsetNumRef();
    
    CTNumData getNumLit();
    
    boolean isSetNumLit();
    
    void setNumLit(final CTNumData p0);
    
    CTNumData addNewNumLit();
    
    void unsetNumLit();
    
    public static final class Factory
    {
        public static CTNumDataSource newInstance() {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().newInstance(CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource newInstance(final XmlOptions xmlOptions) {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().newInstance(CTNumDataSource.type, xmlOptions);
        }
        
        public static CTNumDataSource parse(final String s) throws XmlException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(s, CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(s, CTNumDataSource.type, xmlOptions);
        }
        
        public static CTNumDataSource parse(final File file) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(file, CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(file, CTNumDataSource.type, xmlOptions);
        }
        
        public static CTNumDataSource parse(final URL url) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(url, CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(url, CTNumDataSource.type, xmlOptions);
        }
        
        public static CTNumDataSource parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumDataSource.type, xmlOptions);
        }
        
        public static CTNumDataSource parse(final Reader reader) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(reader, CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(reader, CTNumDataSource.type, xmlOptions);
        }
        
        public static CTNumDataSource parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumDataSource.type, xmlOptions);
        }
        
        public static CTNumDataSource parse(final Node node) throws XmlException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(node, CTNumDataSource.type, null);
        }
        
        public static CTNumDataSource parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(node, CTNumDataSource.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNumDataSource parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumDataSource.type, null);
        }
        
        @Deprecated
        public static CTNumDataSource parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNumDataSource)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumDataSource.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumDataSource.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumDataSource.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
