// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTLayoutMode extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTLayoutMode.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctlayoutmode53eftype");
    
    STLayoutMode.Enum getVal();
    
    STLayoutMode xgetVal();
    
    boolean isSetVal();
    
    void setVal(final STLayoutMode.Enum p0);
    
    void xsetVal(final STLayoutMode p0);
    
    void unsetVal();
    
    public static final class Factory
    {
        public static CTLayoutMode newInstance() {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().newInstance(CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode newInstance(final XmlOptions xmlOptions) {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().newInstance(CTLayoutMode.type, xmlOptions);
        }
        
        public static CTLayoutMode parse(final String s) throws XmlException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(s, CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(s, CTLayoutMode.type, xmlOptions);
        }
        
        public static CTLayoutMode parse(final File file) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(file, CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(file, CTLayoutMode.type, xmlOptions);
        }
        
        public static CTLayoutMode parse(final URL url) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(url, CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(url, CTLayoutMode.type, xmlOptions);
        }
        
        public static CTLayoutMode parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(inputStream, CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(inputStream, CTLayoutMode.type, xmlOptions);
        }
        
        public static CTLayoutMode parse(final Reader reader) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(reader, CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(reader, CTLayoutMode.type, xmlOptions);
        }
        
        public static CTLayoutMode parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLayoutMode.type, xmlOptions);
        }
        
        public static CTLayoutMode parse(final Node node) throws XmlException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(node, CTLayoutMode.type, null);
        }
        
        public static CTLayoutMode parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(node, CTLayoutMode.type, xmlOptions);
        }
        
        @Deprecated
        public static CTLayoutMode parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLayoutMode.type, null);
        }
        
        @Deprecated
        public static CTLayoutMode parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLayoutMode.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLayoutMode.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLayoutMode.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
