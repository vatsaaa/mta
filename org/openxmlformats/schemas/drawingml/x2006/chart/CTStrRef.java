// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTStrRef extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTStrRef.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctstrref5d1atype");
    
    String getF();
    
    XmlString xgetF();
    
    void setF(final String p0);
    
    void xsetF(final XmlString p0);
    
    CTStrData getStrCache();
    
    boolean isSetStrCache();
    
    void setStrCache(final CTStrData p0);
    
    CTStrData addNewStrCache();
    
    void unsetStrCache();
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTStrRef newInstance() {
            return (CTStrRef)XmlBeans.getContextTypeLoader().newInstance(CTStrRef.type, null);
        }
        
        public static CTStrRef newInstance(final XmlOptions xmlOptions) {
            return (CTStrRef)XmlBeans.getContextTypeLoader().newInstance(CTStrRef.type, xmlOptions);
        }
        
        public static CTStrRef parse(final String s) throws XmlException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(s, CTStrRef.type, null);
        }
        
        public static CTStrRef parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(s, CTStrRef.type, xmlOptions);
        }
        
        public static CTStrRef parse(final File file) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(file, CTStrRef.type, null);
        }
        
        public static CTStrRef parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(file, CTStrRef.type, xmlOptions);
        }
        
        public static CTStrRef parse(final URL url) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(url, CTStrRef.type, null);
        }
        
        public static CTStrRef parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(url, CTStrRef.type, xmlOptions);
        }
        
        public static CTStrRef parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(inputStream, CTStrRef.type, null);
        }
        
        public static CTStrRef parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(inputStream, CTStrRef.type, xmlOptions);
        }
        
        public static CTStrRef parse(final Reader reader) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(reader, CTStrRef.type, null);
        }
        
        public static CTStrRef parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(reader, CTStrRef.type, xmlOptions);
        }
        
        public static CTStrRef parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStrRef.type, null);
        }
        
        public static CTStrRef parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStrRef.type, xmlOptions);
        }
        
        public static CTStrRef parse(final Node node) throws XmlException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(node, CTStrRef.type, null);
        }
        
        public static CTStrRef parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(node, CTStrRef.type, xmlOptions);
        }
        
        @Deprecated
        public static CTStrRef parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStrRef.type, null);
        }
        
        @Deprecated
        public static CTStrRef parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTStrRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStrRef.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStrRef.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStrRef.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
