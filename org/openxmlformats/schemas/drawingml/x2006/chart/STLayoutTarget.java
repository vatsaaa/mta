// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STLayoutTarget extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLayoutTarget.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlayouttarget19f1type");
    public static final Enum INNER = Enum.forString("inner");
    public static final Enum OUTER = Enum.forString("outer");
    public static final int INT_INNER = 1;
    public static final int INT_OUTER = 2;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STLayoutTarget newValue(final Object o) {
            return (STLayoutTarget)STLayoutTarget.type.newValue(o);
        }
        
        public static STLayoutTarget newInstance() {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().newInstance(STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget newInstance(final XmlOptions xmlOptions) {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().newInstance(STLayoutTarget.type, xmlOptions);
        }
        
        public static STLayoutTarget parse(final String s) throws XmlException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(s, STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(s, STLayoutTarget.type, xmlOptions);
        }
        
        public static STLayoutTarget parse(final File file) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(file, STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(file, STLayoutTarget.type, xmlOptions);
        }
        
        public static STLayoutTarget parse(final URL url) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(url, STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(url, STLayoutTarget.type, xmlOptions);
        }
        
        public static STLayoutTarget parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(inputStream, STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(inputStream, STLayoutTarget.type, xmlOptions);
        }
        
        public static STLayoutTarget parse(final Reader reader) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(reader, STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(reader, STLayoutTarget.type, xmlOptions);
        }
        
        public static STLayoutTarget parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLayoutTarget.type, xmlOptions);
        }
        
        public static STLayoutTarget parse(final Node node) throws XmlException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(node, STLayoutTarget.type, null);
        }
        
        public static STLayoutTarget parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(node, STLayoutTarget.type, xmlOptions);
        }
        
        @Deprecated
        public static STLayoutTarget parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLayoutTarget.type, null);
        }
        
        @Deprecated
        public static STLayoutTarget parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLayoutTarget)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLayoutTarget.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLayoutTarget.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLayoutTarget.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_INNER = 1;
        static final int INT_OUTER = 2;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("inner", 1), new Enum("outer", 2) });
        }
    }
}
