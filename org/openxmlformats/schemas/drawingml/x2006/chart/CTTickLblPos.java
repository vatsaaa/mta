// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTickLblPos extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTickLblPos.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctticklblposff61type");
    
    STTickLblPos.Enum getVal();
    
    STTickLblPos xgetVal();
    
    boolean isSetVal();
    
    void setVal(final STTickLblPos.Enum p0);
    
    void xsetVal(final STTickLblPos p0);
    
    void unsetVal();
    
    public static final class Factory
    {
        public static CTTickLblPos newInstance() {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().newInstance(CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos newInstance(final XmlOptions xmlOptions) {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().newInstance(CTTickLblPos.type, xmlOptions);
        }
        
        public static CTTickLblPos parse(final String s) throws XmlException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(s, CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(s, CTTickLblPos.type, xmlOptions);
        }
        
        public static CTTickLblPos parse(final File file) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(file, CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(file, CTTickLblPos.type, xmlOptions);
        }
        
        public static CTTickLblPos parse(final URL url) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(url, CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(url, CTTickLblPos.type, xmlOptions);
        }
        
        public static CTTickLblPos parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(inputStream, CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(inputStream, CTTickLblPos.type, xmlOptions);
        }
        
        public static CTTickLblPos parse(final Reader reader) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(reader, CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(reader, CTTickLblPos.type, xmlOptions);
        }
        
        public static CTTickLblPos parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTickLblPos.type, xmlOptions);
        }
        
        public static CTTickLblPos parse(final Node node) throws XmlException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(node, CTTickLblPos.type, null);
        }
        
        public static CTTickLblPos parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(node, CTTickLblPos.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTickLblPos parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTickLblPos.type, null);
        }
        
        @Deprecated
        public static CTTickLblPos parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTickLblPos)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTickLblPos.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTickLblPos.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTickLblPos.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
