// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTUnsignedInt extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTUnsignedInt.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctunsignedinte8ectype");
    
    long getVal();
    
    XmlUnsignedInt xgetVal();
    
    void setVal(final long p0);
    
    void xsetVal(final XmlUnsignedInt p0);
    
    public static final class Factory
    {
        public static CTUnsignedInt newInstance() {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().newInstance(CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt newInstance(final XmlOptions xmlOptions) {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().newInstance(CTUnsignedInt.type, xmlOptions);
        }
        
        public static CTUnsignedInt parse(final String s) throws XmlException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(s, CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(s, CTUnsignedInt.type, xmlOptions);
        }
        
        public static CTUnsignedInt parse(final File file) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(file, CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(file, CTUnsignedInt.type, xmlOptions);
        }
        
        public static CTUnsignedInt parse(final URL url) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(url, CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(url, CTUnsignedInt.type, xmlOptions);
        }
        
        public static CTUnsignedInt parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(inputStream, CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(inputStream, CTUnsignedInt.type, xmlOptions);
        }
        
        public static CTUnsignedInt parse(final Reader reader) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(reader, CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(reader, CTUnsignedInt.type, xmlOptions);
        }
        
        public static CTUnsignedInt parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTUnsignedInt.type, xmlOptions);
        }
        
        public static CTUnsignedInt parse(final Node node) throws XmlException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(node, CTUnsignedInt.type, null);
        }
        
        public static CTUnsignedInt parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(node, CTUnsignedInt.type, xmlOptions);
        }
        
        @Deprecated
        public static CTUnsignedInt parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTUnsignedInt.type, null);
        }
        
        @Deprecated
        public static CTUnsignedInt parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTUnsignedInt)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTUnsignedInt.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTUnsignedInt.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTUnsignedInt.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
