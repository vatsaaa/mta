// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTStrData extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTStrData.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctstrdatad58btype");
    
    CTUnsignedInt getPtCount();
    
    boolean isSetPtCount();
    
    void setPtCount(final CTUnsignedInt p0);
    
    CTUnsignedInt addNewPtCount();
    
    void unsetPtCount();
    
    List<CTStrVal> getPtList();
    
    @Deprecated
    CTStrVal[] getPtArray();
    
    CTStrVal getPtArray(final int p0);
    
    int sizeOfPtArray();
    
    void setPtArray(final CTStrVal[] p0);
    
    void setPtArray(final int p0, final CTStrVal p1);
    
    CTStrVal insertNewPt(final int p0);
    
    CTStrVal addNewPt();
    
    void removePt(final int p0);
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTStrData newInstance() {
            return (CTStrData)XmlBeans.getContextTypeLoader().newInstance(CTStrData.type, null);
        }
        
        public static CTStrData newInstance(final XmlOptions xmlOptions) {
            return (CTStrData)XmlBeans.getContextTypeLoader().newInstance(CTStrData.type, xmlOptions);
        }
        
        public static CTStrData parse(final String s) throws XmlException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(s, CTStrData.type, null);
        }
        
        public static CTStrData parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(s, CTStrData.type, xmlOptions);
        }
        
        public static CTStrData parse(final File file) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(file, CTStrData.type, null);
        }
        
        public static CTStrData parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(file, CTStrData.type, xmlOptions);
        }
        
        public static CTStrData parse(final URL url) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(url, CTStrData.type, null);
        }
        
        public static CTStrData parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(url, CTStrData.type, xmlOptions);
        }
        
        public static CTStrData parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(inputStream, CTStrData.type, null);
        }
        
        public static CTStrData parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(inputStream, CTStrData.type, xmlOptions);
        }
        
        public static CTStrData parse(final Reader reader) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(reader, CTStrData.type, null);
        }
        
        public static CTStrData parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(reader, CTStrData.type, xmlOptions);
        }
        
        public static CTStrData parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStrData.type, null);
        }
        
        public static CTStrData parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStrData.type, xmlOptions);
        }
        
        public static CTStrData parse(final Node node) throws XmlException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(node, CTStrData.type, null);
        }
        
        public static CTStrData parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(node, CTStrData.type, xmlOptions);
        }
        
        @Deprecated
        public static CTStrData parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStrData.type, null);
        }
        
        @Deprecated
        public static CTStrData parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTStrData)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStrData.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStrData.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStrData.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
