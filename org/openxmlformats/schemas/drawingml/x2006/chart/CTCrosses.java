// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTCrosses extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTCrosses.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcrossesbcb8type");
    
    STCrosses.Enum getVal();
    
    STCrosses xgetVal();
    
    void setVal(final STCrosses.Enum p0);
    
    void xsetVal(final STCrosses p0);
    
    public static final class Factory
    {
        public static CTCrosses newInstance() {
            return (CTCrosses)XmlBeans.getContextTypeLoader().newInstance(CTCrosses.type, null);
        }
        
        public static CTCrosses newInstance(final XmlOptions xmlOptions) {
            return (CTCrosses)XmlBeans.getContextTypeLoader().newInstance(CTCrosses.type, xmlOptions);
        }
        
        public static CTCrosses parse(final String s) throws XmlException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(s, CTCrosses.type, null);
        }
        
        public static CTCrosses parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(s, CTCrosses.type, xmlOptions);
        }
        
        public static CTCrosses parse(final File file) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(file, CTCrosses.type, null);
        }
        
        public static CTCrosses parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(file, CTCrosses.type, xmlOptions);
        }
        
        public static CTCrosses parse(final URL url) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(url, CTCrosses.type, null);
        }
        
        public static CTCrosses parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(url, CTCrosses.type, xmlOptions);
        }
        
        public static CTCrosses parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(inputStream, CTCrosses.type, null);
        }
        
        public static CTCrosses parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(inputStream, CTCrosses.type, xmlOptions);
        }
        
        public static CTCrosses parse(final Reader reader) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(reader, CTCrosses.type, null);
        }
        
        public static CTCrosses parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(reader, CTCrosses.type, xmlOptions);
        }
        
        public static CTCrosses parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCrosses.type, null);
        }
        
        public static CTCrosses parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCrosses.type, xmlOptions);
        }
        
        public static CTCrosses parse(final Node node) throws XmlException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(node, CTCrosses.type, null);
        }
        
        public static CTCrosses parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(node, CTCrosses.type, xmlOptions);
        }
        
        @Deprecated
        public static CTCrosses parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCrosses.type, null);
        }
        
        @Deprecated
        public static CTCrosses parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTCrosses)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCrosses.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCrosses.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCrosses.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
