// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STCrosses;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STCrossesImpl extends JavaStringEnumerationHolderEx implements STCrosses
{
    public STCrossesImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCrossesImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
