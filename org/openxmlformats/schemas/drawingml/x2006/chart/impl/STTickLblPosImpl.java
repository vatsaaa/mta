// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STTickLblPos;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTickLblPosImpl extends JavaStringEnumerationHolderEx implements STTickLblPos
{
    public STTickLblPosImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTickLblPosImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
