// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.openxmlformats.schemas.drawingml.x2006.chart.CTExtensionList;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBoolean;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumDataSource;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTAxDataSource;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTErrBars;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTTrendline;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTDLbls;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTDPt;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTMarker;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTSerTx;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTUnsignedInt;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTScatterSer;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTScatterSerImpl extends XmlComplexContentImpl implements CTScatterSer
{
    private static final QName IDX$0;
    private static final QName ORDER$2;
    private static final QName TX$4;
    private static final QName SPPR$6;
    private static final QName MARKER$8;
    private static final QName DPT$10;
    private static final QName DLBLS$12;
    private static final QName TRENDLINE$14;
    private static final QName ERRBARS$16;
    private static final QName XVAL$18;
    private static final QName YVAL$20;
    private static final QName SMOOTH$22;
    private static final QName EXTLST$24;
    
    public CTScatterSerImpl(final SchemaType type) {
        super(type);
    }
    
    public CTUnsignedInt getIdx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTUnsignedInt ctUnsignedInt = (CTUnsignedInt)this.get_store().find_element_user(CTScatterSerImpl.IDX$0, 0);
            if (ctUnsignedInt == null) {
                return null;
            }
            return ctUnsignedInt;
        }
    }
    
    public void setIdx(final CTUnsignedInt ctUnsignedInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTUnsignedInt ctUnsignedInt2 = (CTUnsignedInt)this.get_store().find_element_user(CTScatterSerImpl.IDX$0, 0);
            if (ctUnsignedInt2 == null) {
                ctUnsignedInt2 = (CTUnsignedInt)this.get_store().add_element_user(CTScatterSerImpl.IDX$0);
            }
            ctUnsignedInt2.set(ctUnsignedInt);
        }
    }
    
    public CTUnsignedInt addNewIdx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTUnsignedInt)this.get_store().add_element_user(CTScatterSerImpl.IDX$0);
        }
    }
    
    public CTUnsignedInt getOrder() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTUnsignedInt ctUnsignedInt = (CTUnsignedInt)this.get_store().find_element_user(CTScatterSerImpl.ORDER$2, 0);
            if (ctUnsignedInt == null) {
                return null;
            }
            return ctUnsignedInt;
        }
    }
    
    public void setOrder(final CTUnsignedInt ctUnsignedInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTUnsignedInt ctUnsignedInt2 = (CTUnsignedInt)this.get_store().find_element_user(CTScatterSerImpl.ORDER$2, 0);
            if (ctUnsignedInt2 == null) {
                ctUnsignedInt2 = (CTUnsignedInt)this.get_store().add_element_user(CTScatterSerImpl.ORDER$2);
            }
            ctUnsignedInt2.set(ctUnsignedInt);
        }
    }
    
    public CTUnsignedInt addNewOrder() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTUnsignedInt)this.get_store().add_element_user(CTScatterSerImpl.ORDER$2);
        }
    }
    
    public CTSerTx getTx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSerTx ctSerTx = (CTSerTx)this.get_store().find_element_user(CTScatterSerImpl.TX$4, 0);
            if (ctSerTx == null) {
                return null;
            }
            return ctSerTx;
        }
    }
    
    public boolean isSetTx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.TX$4) != 0;
        }
    }
    
    public void setTx(final CTSerTx ctSerTx) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSerTx ctSerTx2 = (CTSerTx)this.get_store().find_element_user(CTScatterSerImpl.TX$4, 0);
            if (ctSerTx2 == null) {
                ctSerTx2 = (CTSerTx)this.get_store().add_element_user(CTScatterSerImpl.TX$4);
            }
            ctSerTx2.set((XmlObject)ctSerTx);
        }
    }
    
    public CTSerTx addNewTx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSerTx)this.get_store().add_element_user(CTScatterSerImpl.TX$4);
        }
    }
    
    public void unsetTx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.TX$4, 0);
        }
    }
    
    public CTShapeProperties getSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShapeProperties ctShapeProperties = (CTShapeProperties)this.get_store().find_element_user(CTScatterSerImpl.SPPR$6, 0);
            if (ctShapeProperties == null) {
                return null;
            }
            return ctShapeProperties;
        }
    }
    
    public boolean isSetSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.SPPR$6) != 0;
        }
    }
    
    public void setSpPr(final CTShapeProperties ctShapeProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTShapeProperties ctShapeProperties2 = (CTShapeProperties)this.get_store().find_element_user(CTScatterSerImpl.SPPR$6, 0);
            if (ctShapeProperties2 == null) {
                ctShapeProperties2 = (CTShapeProperties)this.get_store().add_element_user(CTScatterSerImpl.SPPR$6);
            }
            ctShapeProperties2.set(ctShapeProperties);
        }
    }
    
    public CTShapeProperties addNewSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShapeProperties)this.get_store().add_element_user(CTScatterSerImpl.SPPR$6);
        }
    }
    
    public void unsetSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.SPPR$6, 0);
        }
    }
    
    public CTMarker getMarker() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarker ctMarker = (CTMarker)this.get_store().find_element_user(CTScatterSerImpl.MARKER$8, 0);
            if (ctMarker == null) {
                return null;
            }
            return ctMarker;
        }
    }
    
    public boolean isSetMarker() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.MARKER$8) != 0;
        }
    }
    
    public void setMarker(final CTMarker ctMarker) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTMarker ctMarker2 = (CTMarker)this.get_store().find_element_user(CTScatterSerImpl.MARKER$8, 0);
            if (ctMarker2 == null) {
                ctMarker2 = (CTMarker)this.get_store().add_element_user(CTScatterSerImpl.MARKER$8);
            }
            ctMarker2.set((XmlObject)ctMarker);
        }
    }
    
    public CTMarker addNewMarker() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarker)this.get_store().add_element_user(CTScatterSerImpl.MARKER$8);
        }
    }
    
    public void unsetMarker() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.MARKER$8, 0);
        }
    }
    
    public List<CTDPt> getDPtList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDPt>)new CTScatterSerImpl.DPtList(this);
        }
    }
    
    public CTDPt[] getDPtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTScatterSerImpl.DPT$10, list);
            final CTDPt[] array = new CTDPt[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDPt getDPtArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDPt ctdPt = (CTDPt)this.get_store().find_element_user(CTScatterSerImpl.DPT$10, n);
            if (ctdPt == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctdPt;
        }
    }
    
    public int sizeOfDPtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.DPT$10);
        }
    }
    
    public void setDPtArray(final CTDPt[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTScatterSerImpl.DPT$10);
        }
    }
    
    public void setDPtArray(final int n, final CTDPt ctdPt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDPt ctdPt2 = (CTDPt)this.get_store().find_element_user(CTScatterSerImpl.DPT$10, n);
            if (ctdPt2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctdPt2.set((XmlObject)ctdPt);
        }
    }
    
    public CTDPt insertNewDPt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDPt)this.get_store().insert_element_user(CTScatterSerImpl.DPT$10, n);
        }
    }
    
    public CTDPt addNewDPt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDPt)this.get_store().add_element_user(CTScatterSerImpl.DPT$10);
        }
    }
    
    public void removeDPt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.DPT$10, n);
        }
    }
    
    public CTDLbls getDLbls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDLbls ctdLbls = (CTDLbls)this.get_store().find_element_user(CTScatterSerImpl.DLBLS$12, 0);
            if (ctdLbls == null) {
                return null;
            }
            return ctdLbls;
        }
    }
    
    public boolean isSetDLbls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.DLBLS$12) != 0;
        }
    }
    
    public void setDLbls(final CTDLbls ctdLbls) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTDLbls ctdLbls2 = (CTDLbls)this.get_store().find_element_user(CTScatterSerImpl.DLBLS$12, 0);
            if (ctdLbls2 == null) {
                ctdLbls2 = (CTDLbls)this.get_store().add_element_user(CTScatterSerImpl.DLBLS$12);
            }
            ctdLbls2.set((XmlObject)ctdLbls);
        }
    }
    
    public CTDLbls addNewDLbls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDLbls)this.get_store().add_element_user(CTScatterSerImpl.DLBLS$12);
        }
    }
    
    public void unsetDLbls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.DLBLS$12, 0);
        }
    }
    
    public List<CTTrendline> getTrendlineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrendline>)new CTScatterSerImpl.TrendlineList(this);
        }
    }
    
    public CTTrendline[] getTrendlineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTScatterSerImpl.TRENDLINE$14, list);
            final CTTrendline[] array = new CTTrendline[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrendline getTrendlineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrendline ctTrendline = (CTTrendline)this.get_store().find_element_user(CTScatterSerImpl.TRENDLINE$14, n);
            if (ctTrendline == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrendline;
        }
    }
    
    public int sizeOfTrendlineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.TRENDLINE$14);
        }
    }
    
    public void setTrendlineArray(final CTTrendline[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTScatterSerImpl.TRENDLINE$14);
        }
    }
    
    public void setTrendlineArray(final int n, final CTTrendline ctTrendline) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrendline ctTrendline2 = (CTTrendline)this.get_store().find_element_user(CTScatterSerImpl.TRENDLINE$14, n);
            if (ctTrendline2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrendline2.set((XmlObject)ctTrendline);
        }
    }
    
    public CTTrendline insertNewTrendline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrendline)this.get_store().insert_element_user(CTScatterSerImpl.TRENDLINE$14, n);
        }
    }
    
    public CTTrendline addNewTrendline() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrendline)this.get_store().add_element_user(CTScatterSerImpl.TRENDLINE$14);
        }
    }
    
    public void removeTrendline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.TRENDLINE$14, n);
        }
    }
    
    public List<CTErrBars> getErrBarsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTErrBars>)new CTScatterSerImpl.ErrBarsList(this);
        }
    }
    
    public CTErrBars[] getErrBarsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTScatterSerImpl.ERRBARS$16, list);
            final CTErrBars[] array = new CTErrBars[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTErrBars getErrBarsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTErrBars ctErrBars = (CTErrBars)this.get_store().find_element_user(CTScatterSerImpl.ERRBARS$16, n);
            if (ctErrBars == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctErrBars;
        }
    }
    
    public int sizeOfErrBarsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.ERRBARS$16);
        }
    }
    
    public void setErrBarsArray(final CTErrBars[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTScatterSerImpl.ERRBARS$16);
        }
    }
    
    public void setErrBarsArray(final int n, final CTErrBars ctErrBars) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTErrBars ctErrBars2 = (CTErrBars)this.get_store().find_element_user(CTScatterSerImpl.ERRBARS$16, n);
            if (ctErrBars2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctErrBars2.set((XmlObject)ctErrBars);
        }
    }
    
    public CTErrBars insertNewErrBars(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTErrBars)this.get_store().insert_element_user(CTScatterSerImpl.ERRBARS$16, n);
        }
    }
    
    public CTErrBars addNewErrBars() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTErrBars)this.get_store().add_element_user(CTScatterSerImpl.ERRBARS$16);
        }
    }
    
    public void removeErrBars(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.ERRBARS$16, n);
        }
    }
    
    public CTAxDataSource getXVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAxDataSource ctAxDataSource = (CTAxDataSource)this.get_store().find_element_user(CTScatterSerImpl.XVAL$18, 0);
            if (ctAxDataSource == null) {
                return null;
            }
            return ctAxDataSource;
        }
    }
    
    public boolean isSetXVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.XVAL$18) != 0;
        }
    }
    
    public void setXVal(final CTAxDataSource ctAxDataSource) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTAxDataSource ctAxDataSource2 = (CTAxDataSource)this.get_store().find_element_user(CTScatterSerImpl.XVAL$18, 0);
            if (ctAxDataSource2 == null) {
                ctAxDataSource2 = (CTAxDataSource)this.get_store().add_element_user(CTScatterSerImpl.XVAL$18);
            }
            ctAxDataSource2.set(ctAxDataSource);
        }
    }
    
    public CTAxDataSource addNewXVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAxDataSource)this.get_store().add_element_user(CTScatterSerImpl.XVAL$18);
        }
    }
    
    public void unsetXVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.XVAL$18, 0);
        }
    }
    
    public CTNumDataSource getYVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNumDataSource ctNumDataSource = (CTNumDataSource)this.get_store().find_element_user(CTScatterSerImpl.YVAL$20, 0);
            if (ctNumDataSource == null) {
                return null;
            }
            return ctNumDataSource;
        }
    }
    
    public boolean isSetYVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.YVAL$20) != 0;
        }
    }
    
    public void setYVal(final CTNumDataSource ctNumDataSource) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTNumDataSource ctNumDataSource2 = (CTNumDataSource)this.get_store().find_element_user(CTScatterSerImpl.YVAL$20, 0);
            if (ctNumDataSource2 == null) {
                ctNumDataSource2 = (CTNumDataSource)this.get_store().add_element_user(CTScatterSerImpl.YVAL$20);
            }
            ctNumDataSource2.set(ctNumDataSource);
        }
    }
    
    public CTNumDataSource addNewYVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNumDataSource)this.get_store().add_element_user(CTScatterSerImpl.YVAL$20);
        }
    }
    
    public void unsetYVal() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.YVAL$20, 0);
        }
    }
    
    public CTBoolean getSmooth() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBoolean ctBoolean = (CTBoolean)this.get_store().find_element_user(CTScatterSerImpl.SMOOTH$22, 0);
            if (ctBoolean == null) {
                return null;
            }
            return ctBoolean;
        }
    }
    
    public boolean isSetSmooth() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.SMOOTH$22) != 0;
        }
    }
    
    public void setSmooth(final CTBoolean ctBoolean) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTBoolean ctBoolean2 = (CTBoolean)this.get_store().find_element_user(CTScatterSerImpl.SMOOTH$22, 0);
            if (ctBoolean2 == null) {
                ctBoolean2 = (CTBoolean)this.get_store().add_element_user(CTScatterSerImpl.SMOOTH$22);
            }
            ctBoolean2.set(ctBoolean);
        }
    }
    
    public CTBoolean addNewSmooth() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBoolean)this.get_store().add_element_user(CTScatterSerImpl.SMOOTH$22);
        }
    }
    
    public void unsetSmooth() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.SMOOTH$22, 0);
        }
    }
    
    public CTExtensionList getExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTExtensionList list = (CTExtensionList)this.get_store().find_element_user(CTScatterSerImpl.EXTLST$24, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public boolean isSetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTScatterSerImpl.EXTLST$24) != 0;
        }
    }
    
    public void setExtLst(final CTExtensionList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTExtensionList list2 = (CTExtensionList)this.get_store().find_element_user(CTScatterSerImpl.EXTLST$24, 0);
            if (list2 == null) {
                list2 = (CTExtensionList)this.get_store().add_element_user(CTScatterSerImpl.EXTLST$24);
            }
            list2.set((XmlObject)list);
        }
    }
    
    public CTExtensionList addNewExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTExtensionList)this.get_store().add_element_user(CTScatterSerImpl.EXTLST$24);
        }
    }
    
    public void unsetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTScatterSerImpl.EXTLST$24, 0);
        }
    }
    
    static {
        IDX$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "idx");
        ORDER$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "order");
        TX$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "tx");
        SPPR$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "spPr");
        MARKER$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "marker");
        DPT$10 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "dPt");
        DLBLS$12 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "dLbls");
        TRENDLINE$14 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "trendline");
        ERRBARS$16 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "errBars");
        XVAL$18 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "xVal");
        YVAL$20 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "yVal");
        SMOOTH$22 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "smooth");
        EXTLST$24 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "extLst");
    }
}
