// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STLogBase;
import org.apache.xmlbeans.impl.values.JavaDoubleHolderEx;

public class STLogBaseImpl extends JavaDoubleHolderEx implements STLogBase
{
    public STLogBaseImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLogBaseImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
