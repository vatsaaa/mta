// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STAxPos;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STAxPosImpl extends JavaStringEnumerationHolderEx implements STAxPos
{
    public STAxPosImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STAxPosImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
