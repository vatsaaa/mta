// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STOrientation;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STOrientationImpl extends JavaStringEnumerationHolderEx implements STOrientation
{
    public STOrientationImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STOrientationImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
