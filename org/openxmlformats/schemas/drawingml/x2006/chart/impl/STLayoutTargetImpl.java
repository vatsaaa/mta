// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STLayoutTarget;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STLayoutTargetImpl extends JavaStringEnumerationHolderEx implements STLayoutTarget
{
    public STLayoutTargetImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLayoutTargetImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
