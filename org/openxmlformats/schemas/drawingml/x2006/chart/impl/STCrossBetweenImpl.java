// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STCrossBetween;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STCrossBetweenImpl extends JavaStringEnumerationHolderEx implements STCrossBetween
{
    public STCrossBetweenImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCrossBetweenImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
