// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STLegendPos;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STLegendPosImpl extends JavaStringEnumerationHolderEx implements STLegendPos
{
    public STLegendPosImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLegendPosImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
