// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STLayoutMode;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STLayoutModeImpl extends JavaStringEnumerationHolderEx implements STLayoutMode
{
    public STLayoutModeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLayoutModeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
