// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STScatterStyle;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STScatterStyleImpl extends JavaStringEnumerationHolderEx implements STScatterStyle
{
    public STScatterStyleImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STScatterStyleImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
