// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.openxmlformats.schemas.drawingml.x2006.chart.CTExtensionList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTDTable;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTSerAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTDateAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTCatAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTValAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBubbleChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTSurface3DChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTSurfaceChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTOfPieChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBar3DChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTDoughnutChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPie3DChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPieChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTScatterChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTRadarChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTStockChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLine3DChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLineChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTArea3DChart;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTAreaChart;
import java.util.List;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLayout;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPlotArea;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTPlotAreaImpl extends XmlComplexContentImpl implements CTPlotArea
{
    private static final QName LAYOUT$0;
    private static final QName AREACHART$2;
    private static final QName AREA3DCHART$4;
    private static final QName LINECHART$6;
    private static final QName LINE3DCHART$8;
    private static final QName STOCKCHART$10;
    private static final QName RADARCHART$12;
    private static final QName SCATTERCHART$14;
    private static final QName PIECHART$16;
    private static final QName PIE3DCHART$18;
    private static final QName DOUGHNUTCHART$20;
    private static final QName BARCHART$22;
    private static final QName BAR3DCHART$24;
    private static final QName OFPIECHART$26;
    private static final QName SURFACECHART$28;
    private static final QName SURFACE3DCHART$30;
    private static final QName BUBBLECHART$32;
    private static final QName VALAX$34;
    private static final QName CATAX$36;
    private static final QName DATEAX$38;
    private static final QName SERAX$40;
    private static final QName DTABLE$42;
    private static final QName SPPR$44;
    private static final QName EXTLST$46;
    
    public CTPlotAreaImpl(final SchemaType type) {
        super(type);
    }
    
    public CTLayout getLayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLayout ctLayout = (CTLayout)this.get_store().find_element_user(CTPlotAreaImpl.LAYOUT$0, 0);
            if (ctLayout == null) {
                return null;
            }
            return ctLayout;
        }
    }
    
    public boolean isSetLayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.LAYOUT$0) != 0;
        }
    }
    
    public void setLayout(final CTLayout ctLayout) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTLayout ctLayout2 = (CTLayout)this.get_store().find_element_user(CTPlotAreaImpl.LAYOUT$0, 0);
            if (ctLayout2 == null) {
                ctLayout2 = (CTLayout)this.get_store().add_element_user(CTPlotAreaImpl.LAYOUT$0);
            }
            ctLayout2.set(ctLayout);
        }
    }
    
    public CTLayout addNewLayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLayout)this.get_store().add_element_user(CTPlotAreaImpl.LAYOUT$0);
        }
    }
    
    public void unsetLayout() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.LAYOUT$0, 0);
        }
    }
    
    public List<CTAreaChart> getAreaChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAreaChart>)new CTPlotAreaImpl.AreaChartList(this);
        }
    }
    
    public CTAreaChart[] getAreaChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.AREACHART$2, list);
            final CTAreaChart[] array = new CTAreaChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAreaChart getAreaChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAreaChart ctAreaChart = (CTAreaChart)this.get_store().find_element_user(CTPlotAreaImpl.AREACHART$2, n);
            if (ctAreaChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAreaChart;
        }
    }
    
    public int sizeOfAreaChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.AREACHART$2);
        }
    }
    
    public void setAreaChartArray(final CTAreaChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.AREACHART$2);
        }
    }
    
    public void setAreaChartArray(final int n, final CTAreaChart ctAreaChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAreaChart ctAreaChart2 = (CTAreaChart)this.get_store().find_element_user(CTPlotAreaImpl.AREACHART$2, n);
            if (ctAreaChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAreaChart2.set((XmlObject)ctAreaChart);
        }
    }
    
    public CTAreaChart insertNewAreaChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAreaChart)this.get_store().insert_element_user(CTPlotAreaImpl.AREACHART$2, n);
        }
    }
    
    public CTAreaChart addNewAreaChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAreaChart)this.get_store().add_element_user(CTPlotAreaImpl.AREACHART$2);
        }
    }
    
    public void removeAreaChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.AREACHART$2, n);
        }
    }
    
    public List<CTArea3DChart> getArea3DChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTArea3DChart>)new CTPlotAreaImpl.Area3DChartList(this);
        }
    }
    
    public CTArea3DChart[] getArea3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.AREA3DCHART$4, list);
            final CTArea3DChart[] array = new CTArea3DChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTArea3DChart getArea3DChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTArea3DChart ctArea3DChart = (CTArea3DChart)this.get_store().find_element_user(CTPlotAreaImpl.AREA3DCHART$4, n);
            if (ctArea3DChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctArea3DChart;
        }
    }
    
    public int sizeOfArea3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.AREA3DCHART$4);
        }
    }
    
    public void setArea3DChartArray(final CTArea3DChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.AREA3DCHART$4);
        }
    }
    
    public void setArea3DChartArray(final int n, final CTArea3DChart ctArea3DChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTArea3DChart ctArea3DChart2 = (CTArea3DChart)this.get_store().find_element_user(CTPlotAreaImpl.AREA3DCHART$4, n);
            if (ctArea3DChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctArea3DChart2.set((XmlObject)ctArea3DChart);
        }
    }
    
    public CTArea3DChart insertNewArea3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTArea3DChart)this.get_store().insert_element_user(CTPlotAreaImpl.AREA3DCHART$4, n);
        }
    }
    
    public CTArea3DChart addNewArea3DChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTArea3DChart)this.get_store().add_element_user(CTPlotAreaImpl.AREA3DCHART$4);
        }
    }
    
    public void removeArea3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.AREA3DCHART$4, n);
        }
    }
    
    public List<CTLineChart> getLineChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLineChart>)new CTPlotAreaImpl.LineChartList(this);
        }
    }
    
    public CTLineChart[] getLineChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.LINECHART$6, list);
            final CTLineChart[] array = new CTLineChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLineChart getLineChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLineChart ctLineChart = (CTLineChart)this.get_store().find_element_user(CTPlotAreaImpl.LINECHART$6, n);
            if (ctLineChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLineChart;
        }
    }
    
    public int sizeOfLineChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.LINECHART$6);
        }
    }
    
    public void setLineChartArray(final CTLineChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.LINECHART$6);
        }
    }
    
    public void setLineChartArray(final int n, final CTLineChart ctLineChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLineChart ctLineChart2 = (CTLineChart)this.get_store().find_element_user(CTPlotAreaImpl.LINECHART$6, n);
            if (ctLineChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLineChart2.set((XmlObject)ctLineChart);
        }
    }
    
    public CTLineChart insertNewLineChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLineChart)this.get_store().insert_element_user(CTPlotAreaImpl.LINECHART$6, n);
        }
    }
    
    public CTLineChart addNewLineChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLineChart)this.get_store().add_element_user(CTPlotAreaImpl.LINECHART$6);
        }
    }
    
    public void removeLineChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.LINECHART$6, n);
        }
    }
    
    public List<CTLine3DChart> getLine3DChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLine3DChart>)new CTPlotAreaImpl.Line3DChartList(this);
        }
    }
    
    public CTLine3DChart[] getLine3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.LINE3DCHART$8, list);
            final CTLine3DChart[] array = new CTLine3DChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLine3DChart getLine3DChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLine3DChart ctLine3DChart = (CTLine3DChart)this.get_store().find_element_user(CTPlotAreaImpl.LINE3DCHART$8, n);
            if (ctLine3DChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLine3DChart;
        }
    }
    
    public int sizeOfLine3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.LINE3DCHART$8);
        }
    }
    
    public void setLine3DChartArray(final CTLine3DChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.LINE3DCHART$8);
        }
    }
    
    public void setLine3DChartArray(final int n, final CTLine3DChart ctLine3DChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLine3DChart ctLine3DChart2 = (CTLine3DChart)this.get_store().find_element_user(CTPlotAreaImpl.LINE3DCHART$8, n);
            if (ctLine3DChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLine3DChart2.set((XmlObject)ctLine3DChart);
        }
    }
    
    public CTLine3DChart insertNewLine3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLine3DChart)this.get_store().insert_element_user(CTPlotAreaImpl.LINE3DCHART$8, n);
        }
    }
    
    public CTLine3DChart addNewLine3DChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLine3DChart)this.get_store().add_element_user(CTPlotAreaImpl.LINE3DCHART$8);
        }
    }
    
    public void removeLine3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.LINE3DCHART$8, n);
        }
    }
    
    public List<CTStockChart> getStockChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTStockChart>)new CTPlotAreaImpl.StockChartList(this);
        }
    }
    
    public CTStockChart[] getStockChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.STOCKCHART$10, list);
            final CTStockChart[] array = new CTStockChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTStockChart getStockChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStockChart ctStockChart = (CTStockChart)this.get_store().find_element_user(CTPlotAreaImpl.STOCKCHART$10, n);
            if (ctStockChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctStockChart;
        }
    }
    
    public int sizeOfStockChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.STOCKCHART$10);
        }
    }
    
    public void setStockChartArray(final CTStockChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.STOCKCHART$10);
        }
    }
    
    public void setStockChartArray(final int n, final CTStockChart ctStockChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStockChart ctStockChart2 = (CTStockChart)this.get_store().find_element_user(CTPlotAreaImpl.STOCKCHART$10, n);
            if (ctStockChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctStockChart2.set((XmlObject)ctStockChart);
        }
    }
    
    public CTStockChart insertNewStockChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStockChart)this.get_store().insert_element_user(CTPlotAreaImpl.STOCKCHART$10, n);
        }
    }
    
    public CTStockChart addNewStockChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStockChart)this.get_store().add_element_user(CTPlotAreaImpl.STOCKCHART$10);
        }
    }
    
    public void removeStockChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.STOCKCHART$10, n);
        }
    }
    
    public List<CTRadarChart> getRadarChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRadarChart>)new CTPlotAreaImpl.RadarChartList(this);
        }
    }
    
    public CTRadarChart[] getRadarChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.RADARCHART$12, list);
            final CTRadarChart[] array = new CTRadarChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRadarChart getRadarChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRadarChart ctRadarChart = (CTRadarChart)this.get_store().find_element_user(CTPlotAreaImpl.RADARCHART$12, n);
            if (ctRadarChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRadarChart;
        }
    }
    
    public int sizeOfRadarChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.RADARCHART$12);
        }
    }
    
    public void setRadarChartArray(final CTRadarChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.RADARCHART$12);
        }
    }
    
    public void setRadarChartArray(final int n, final CTRadarChart ctRadarChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRadarChart ctRadarChart2 = (CTRadarChart)this.get_store().find_element_user(CTPlotAreaImpl.RADARCHART$12, n);
            if (ctRadarChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRadarChart2.set((XmlObject)ctRadarChart);
        }
    }
    
    public CTRadarChart insertNewRadarChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRadarChart)this.get_store().insert_element_user(CTPlotAreaImpl.RADARCHART$12, n);
        }
    }
    
    public CTRadarChart addNewRadarChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRadarChart)this.get_store().add_element_user(CTPlotAreaImpl.RADARCHART$12);
        }
    }
    
    public void removeRadarChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.RADARCHART$12, n);
        }
    }
    
    public List<CTScatterChart> getScatterChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTScatterChart>)new CTPlotAreaImpl.ScatterChartList(this);
        }
    }
    
    public CTScatterChart[] getScatterChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.SCATTERCHART$14, list);
            final CTScatterChart[] array = new CTScatterChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTScatterChart getScatterChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTScatterChart ctScatterChart = (CTScatterChart)this.get_store().find_element_user(CTPlotAreaImpl.SCATTERCHART$14, n);
            if (ctScatterChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctScatterChart;
        }
    }
    
    public int sizeOfScatterChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.SCATTERCHART$14);
        }
    }
    
    public void setScatterChartArray(final CTScatterChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTPlotAreaImpl.SCATTERCHART$14);
        }
    }
    
    public void setScatterChartArray(final int n, final CTScatterChart ctScatterChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTScatterChart ctScatterChart2 = (CTScatterChart)this.get_store().find_element_user(CTPlotAreaImpl.SCATTERCHART$14, n);
            if (ctScatterChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctScatterChart2.set(ctScatterChart);
        }
    }
    
    public CTScatterChart insertNewScatterChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTScatterChart)this.get_store().insert_element_user(CTPlotAreaImpl.SCATTERCHART$14, n);
        }
    }
    
    public CTScatterChart addNewScatterChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTScatterChart)this.get_store().add_element_user(CTPlotAreaImpl.SCATTERCHART$14);
        }
    }
    
    public void removeScatterChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.SCATTERCHART$14, n);
        }
    }
    
    public List<CTPieChart> getPieChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPieChart>)new CTPlotAreaImpl.PieChartList(this);
        }
    }
    
    public CTPieChart[] getPieChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.PIECHART$16, list);
            final CTPieChart[] array = new CTPieChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPieChart getPieChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPieChart ctPieChart = (CTPieChart)this.get_store().find_element_user(CTPlotAreaImpl.PIECHART$16, n);
            if (ctPieChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPieChart;
        }
    }
    
    public int sizeOfPieChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.PIECHART$16);
        }
    }
    
    public void setPieChartArray(final CTPieChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.PIECHART$16);
        }
    }
    
    public void setPieChartArray(final int n, final CTPieChart ctPieChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPieChart ctPieChart2 = (CTPieChart)this.get_store().find_element_user(CTPlotAreaImpl.PIECHART$16, n);
            if (ctPieChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPieChart2.set((XmlObject)ctPieChart);
        }
    }
    
    public CTPieChart insertNewPieChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPieChart)this.get_store().insert_element_user(CTPlotAreaImpl.PIECHART$16, n);
        }
    }
    
    public CTPieChart addNewPieChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPieChart)this.get_store().add_element_user(CTPlotAreaImpl.PIECHART$16);
        }
    }
    
    public void removePieChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.PIECHART$16, n);
        }
    }
    
    public List<CTPie3DChart> getPie3DChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPie3DChart>)new CTPlotAreaImpl.Pie3DChartList(this);
        }
    }
    
    public CTPie3DChart[] getPie3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.PIE3DCHART$18, list);
            final CTPie3DChart[] array = new CTPie3DChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPie3DChart getPie3DChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPie3DChart ctPie3DChart = (CTPie3DChart)this.get_store().find_element_user(CTPlotAreaImpl.PIE3DCHART$18, n);
            if (ctPie3DChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPie3DChart;
        }
    }
    
    public int sizeOfPie3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.PIE3DCHART$18);
        }
    }
    
    public void setPie3DChartArray(final CTPie3DChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.PIE3DCHART$18);
        }
    }
    
    public void setPie3DChartArray(final int n, final CTPie3DChart ctPie3DChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPie3DChart ctPie3DChart2 = (CTPie3DChart)this.get_store().find_element_user(CTPlotAreaImpl.PIE3DCHART$18, n);
            if (ctPie3DChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPie3DChart2.set((XmlObject)ctPie3DChart);
        }
    }
    
    public CTPie3DChart insertNewPie3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPie3DChart)this.get_store().insert_element_user(CTPlotAreaImpl.PIE3DCHART$18, n);
        }
    }
    
    public CTPie3DChart addNewPie3DChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPie3DChart)this.get_store().add_element_user(CTPlotAreaImpl.PIE3DCHART$18);
        }
    }
    
    public void removePie3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.PIE3DCHART$18, n);
        }
    }
    
    public List<CTDoughnutChart> getDoughnutChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDoughnutChart>)new CTPlotAreaImpl.DoughnutChartList(this);
        }
    }
    
    public CTDoughnutChart[] getDoughnutChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.DOUGHNUTCHART$20, list);
            final CTDoughnutChart[] array = new CTDoughnutChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDoughnutChart getDoughnutChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDoughnutChart ctDoughnutChart = (CTDoughnutChart)this.get_store().find_element_user(CTPlotAreaImpl.DOUGHNUTCHART$20, n);
            if (ctDoughnutChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDoughnutChart;
        }
    }
    
    public int sizeOfDoughnutChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.DOUGHNUTCHART$20);
        }
    }
    
    public void setDoughnutChartArray(final CTDoughnutChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.DOUGHNUTCHART$20);
        }
    }
    
    public void setDoughnutChartArray(final int n, final CTDoughnutChart ctDoughnutChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDoughnutChart ctDoughnutChart2 = (CTDoughnutChart)this.get_store().find_element_user(CTPlotAreaImpl.DOUGHNUTCHART$20, n);
            if (ctDoughnutChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDoughnutChart2.set((XmlObject)ctDoughnutChart);
        }
    }
    
    public CTDoughnutChart insertNewDoughnutChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDoughnutChart)this.get_store().insert_element_user(CTPlotAreaImpl.DOUGHNUTCHART$20, n);
        }
    }
    
    public CTDoughnutChart addNewDoughnutChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDoughnutChart)this.get_store().add_element_user(CTPlotAreaImpl.DOUGHNUTCHART$20);
        }
    }
    
    public void removeDoughnutChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.DOUGHNUTCHART$20, n);
        }
    }
    
    public List<CTBarChart> getBarChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBarChart>)new CTPlotAreaImpl.BarChartList(this);
        }
    }
    
    public CTBarChart[] getBarChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.BARCHART$22, list);
            final CTBarChart[] array = new CTBarChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBarChart getBarChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBarChart ctBarChart = (CTBarChart)this.get_store().find_element_user(CTPlotAreaImpl.BARCHART$22, n);
            if (ctBarChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBarChart;
        }
    }
    
    public int sizeOfBarChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.BARCHART$22);
        }
    }
    
    public void setBarChartArray(final CTBarChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.BARCHART$22);
        }
    }
    
    public void setBarChartArray(final int n, final CTBarChart ctBarChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBarChart ctBarChart2 = (CTBarChart)this.get_store().find_element_user(CTPlotAreaImpl.BARCHART$22, n);
            if (ctBarChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBarChart2.set((XmlObject)ctBarChart);
        }
    }
    
    public CTBarChart insertNewBarChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBarChart)this.get_store().insert_element_user(CTPlotAreaImpl.BARCHART$22, n);
        }
    }
    
    public CTBarChart addNewBarChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBarChart)this.get_store().add_element_user(CTPlotAreaImpl.BARCHART$22);
        }
    }
    
    public void removeBarChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.BARCHART$22, n);
        }
    }
    
    public List<CTBar3DChart> getBar3DChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBar3DChart>)new CTPlotAreaImpl.Bar3DChartList(this);
        }
    }
    
    public CTBar3DChart[] getBar3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.BAR3DCHART$24, list);
            final CTBar3DChart[] array = new CTBar3DChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBar3DChart getBar3DChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBar3DChart ctBar3DChart = (CTBar3DChart)this.get_store().find_element_user(CTPlotAreaImpl.BAR3DCHART$24, n);
            if (ctBar3DChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBar3DChart;
        }
    }
    
    public int sizeOfBar3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.BAR3DCHART$24);
        }
    }
    
    public void setBar3DChartArray(final CTBar3DChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.BAR3DCHART$24);
        }
    }
    
    public void setBar3DChartArray(final int n, final CTBar3DChart ctBar3DChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBar3DChart ctBar3DChart2 = (CTBar3DChart)this.get_store().find_element_user(CTPlotAreaImpl.BAR3DCHART$24, n);
            if (ctBar3DChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBar3DChart2.set((XmlObject)ctBar3DChart);
        }
    }
    
    public CTBar3DChart insertNewBar3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBar3DChart)this.get_store().insert_element_user(CTPlotAreaImpl.BAR3DCHART$24, n);
        }
    }
    
    public CTBar3DChart addNewBar3DChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBar3DChart)this.get_store().add_element_user(CTPlotAreaImpl.BAR3DCHART$24);
        }
    }
    
    public void removeBar3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.BAR3DCHART$24, n);
        }
    }
    
    public List<CTOfPieChart> getOfPieChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOfPieChart>)new CTPlotAreaImpl.OfPieChartList(this);
        }
    }
    
    public CTOfPieChart[] getOfPieChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.OFPIECHART$26, list);
            final CTOfPieChart[] array = new CTOfPieChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOfPieChart getOfPieChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOfPieChart ctOfPieChart = (CTOfPieChart)this.get_store().find_element_user(CTPlotAreaImpl.OFPIECHART$26, n);
            if (ctOfPieChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOfPieChart;
        }
    }
    
    public int sizeOfOfPieChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.OFPIECHART$26);
        }
    }
    
    public void setOfPieChartArray(final CTOfPieChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.OFPIECHART$26);
        }
    }
    
    public void setOfPieChartArray(final int n, final CTOfPieChart ctOfPieChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOfPieChart ctOfPieChart2 = (CTOfPieChart)this.get_store().find_element_user(CTPlotAreaImpl.OFPIECHART$26, n);
            if (ctOfPieChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOfPieChart2.set((XmlObject)ctOfPieChart);
        }
    }
    
    public CTOfPieChart insertNewOfPieChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOfPieChart)this.get_store().insert_element_user(CTPlotAreaImpl.OFPIECHART$26, n);
        }
    }
    
    public CTOfPieChart addNewOfPieChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOfPieChart)this.get_store().add_element_user(CTPlotAreaImpl.OFPIECHART$26);
        }
    }
    
    public void removeOfPieChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.OFPIECHART$26, n);
        }
    }
    
    public List<CTSurfaceChart> getSurfaceChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSurfaceChart>)new CTPlotAreaImpl.SurfaceChartList(this);
        }
    }
    
    public CTSurfaceChart[] getSurfaceChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.SURFACECHART$28, list);
            final CTSurfaceChart[] array = new CTSurfaceChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSurfaceChart getSurfaceChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSurfaceChart ctSurfaceChart = (CTSurfaceChart)this.get_store().find_element_user(CTPlotAreaImpl.SURFACECHART$28, n);
            if (ctSurfaceChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSurfaceChart;
        }
    }
    
    public int sizeOfSurfaceChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.SURFACECHART$28);
        }
    }
    
    public void setSurfaceChartArray(final CTSurfaceChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.SURFACECHART$28);
        }
    }
    
    public void setSurfaceChartArray(final int n, final CTSurfaceChart ctSurfaceChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSurfaceChart ctSurfaceChart2 = (CTSurfaceChart)this.get_store().find_element_user(CTPlotAreaImpl.SURFACECHART$28, n);
            if (ctSurfaceChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSurfaceChart2.set((XmlObject)ctSurfaceChart);
        }
    }
    
    public CTSurfaceChart insertNewSurfaceChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSurfaceChart)this.get_store().insert_element_user(CTPlotAreaImpl.SURFACECHART$28, n);
        }
    }
    
    public CTSurfaceChart addNewSurfaceChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSurfaceChart)this.get_store().add_element_user(CTPlotAreaImpl.SURFACECHART$28);
        }
    }
    
    public void removeSurfaceChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.SURFACECHART$28, n);
        }
    }
    
    public List<CTSurface3DChart> getSurface3DChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSurface3DChart>)new CTPlotAreaImpl.Surface3DChartList(this);
        }
    }
    
    public CTSurface3DChart[] getSurface3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.SURFACE3DCHART$30, list);
            final CTSurface3DChart[] array = new CTSurface3DChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSurface3DChart getSurface3DChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSurface3DChart ctSurface3DChart = (CTSurface3DChart)this.get_store().find_element_user(CTPlotAreaImpl.SURFACE3DCHART$30, n);
            if (ctSurface3DChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSurface3DChart;
        }
    }
    
    public int sizeOfSurface3DChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.SURFACE3DCHART$30);
        }
    }
    
    public void setSurface3DChartArray(final CTSurface3DChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.SURFACE3DCHART$30);
        }
    }
    
    public void setSurface3DChartArray(final int n, final CTSurface3DChart ctSurface3DChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSurface3DChart ctSurface3DChart2 = (CTSurface3DChart)this.get_store().find_element_user(CTPlotAreaImpl.SURFACE3DCHART$30, n);
            if (ctSurface3DChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSurface3DChart2.set((XmlObject)ctSurface3DChart);
        }
    }
    
    public CTSurface3DChart insertNewSurface3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSurface3DChart)this.get_store().insert_element_user(CTPlotAreaImpl.SURFACE3DCHART$30, n);
        }
    }
    
    public CTSurface3DChart addNewSurface3DChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSurface3DChart)this.get_store().add_element_user(CTPlotAreaImpl.SURFACE3DCHART$30);
        }
    }
    
    public void removeSurface3DChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.SURFACE3DCHART$30, n);
        }
    }
    
    public List<CTBubbleChart> getBubbleChartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBubbleChart>)new CTPlotAreaImpl.BubbleChartList(this);
        }
    }
    
    public CTBubbleChart[] getBubbleChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.BUBBLECHART$32, list);
            final CTBubbleChart[] array = new CTBubbleChart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBubbleChart getBubbleChartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBubbleChart ctBubbleChart = (CTBubbleChart)this.get_store().find_element_user(CTPlotAreaImpl.BUBBLECHART$32, n);
            if (ctBubbleChart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBubbleChart;
        }
    }
    
    public int sizeOfBubbleChartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.BUBBLECHART$32);
        }
    }
    
    public void setBubbleChartArray(final CTBubbleChart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.BUBBLECHART$32);
        }
    }
    
    public void setBubbleChartArray(final int n, final CTBubbleChart ctBubbleChart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBubbleChart ctBubbleChart2 = (CTBubbleChart)this.get_store().find_element_user(CTPlotAreaImpl.BUBBLECHART$32, n);
            if (ctBubbleChart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBubbleChart2.set((XmlObject)ctBubbleChart);
        }
    }
    
    public CTBubbleChart insertNewBubbleChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBubbleChart)this.get_store().insert_element_user(CTPlotAreaImpl.BUBBLECHART$32, n);
        }
    }
    
    public CTBubbleChart addNewBubbleChart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBubbleChart)this.get_store().add_element_user(CTPlotAreaImpl.BUBBLECHART$32);
        }
    }
    
    public void removeBubbleChart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.BUBBLECHART$32, n);
        }
    }
    
    public List<CTValAx> getValAxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTValAx>)new CTPlotAreaImpl.ValAxList(this);
        }
    }
    
    public CTValAx[] getValAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.VALAX$34, list);
            final CTValAx[] array = new CTValAx[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTValAx getValAxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTValAx ctValAx = (CTValAx)this.get_store().find_element_user(CTPlotAreaImpl.VALAX$34, n);
            if (ctValAx == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctValAx;
        }
    }
    
    public int sizeOfValAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.VALAX$34);
        }
    }
    
    public void setValAxArray(final CTValAx[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTPlotAreaImpl.VALAX$34);
        }
    }
    
    public void setValAxArray(final int n, final CTValAx ctValAx) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTValAx ctValAx2 = (CTValAx)this.get_store().find_element_user(CTPlotAreaImpl.VALAX$34, n);
            if (ctValAx2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctValAx2.set(ctValAx);
        }
    }
    
    public CTValAx insertNewValAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTValAx)this.get_store().insert_element_user(CTPlotAreaImpl.VALAX$34, n);
        }
    }
    
    public CTValAx addNewValAx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTValAx)this.get_store().add_element_user(CTPlotAreaImpl.VALAX$34);
        }
    }
    
    public void removeValAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.VALAX$34, n);
        }
    }
    
    public List<CTCatAx> getCatAxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCatAx>)new CTPlotAreaImpl.CatAxList(this);
        }
    }
    
    public CTCatAx[] getCatAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.CATAX$36, list);
            final CTCatAx[] array = new CTCatAx[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCatAx getCatAxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCatAx ctCatAx = (CTCatAx)this.get_store().find_element_user(CTPlotAreaImpl.CATAX$36, n);
            if (ctCatAx == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCatAx;
        }
    }
    
    public int sizeOfCatAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.CATAX$36);
        }
    }
    
    public void setCatAxArray(final CTCatAx[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.CATAX$36);
        }
    }
    
    public void setCatAxArray(final int n, final CTCatAx ctCatAx) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCatAx ctCatAx2 = (CTCatAx)this.get_store().find_element_user(CTPlotAreaImpl.CATAX$36, n);
            if (ctCatAx2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCatAx2.set((XmlObject)ctCatAx);
        }
    }
    
    public CTCatAx insertNewCatAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCatAx)this.get_store().insert_element_user(CTPlotAreaImpl.CATAX$36, n);
        }
    }
    
    public CTCatAx addNewCatAx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCatAx)this.get_store().add_element_user(CTPlotAreaImpl.CATAX$36);
        }
    }
    
    public void removeCatAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.CATAX$36, n);
        }
    }
    
    public List<CTDateAx> getDateAxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDateAx>)new CTPlotAreaImpl.DateAxList(this);
        }
    }
    
    public CTDateAx[] getDateAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.DATEAX$38, list);
            final CTDateAx[] array = new CTDateAx[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDateAx getDateAxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDateAx ctDateAx = (CTDateAx)this.get_store().find_element_user(CTPlotAreaImpl.DATEAX$38, n);
            if (ctDateAx == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDateAx;
        }
    }
    
    public int sizeOfDateAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.DATEAX$38);
        }
    }
    
    public void setDateAxArray(final CTDateAx[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.DATEAX$38);
        }
    }
    
    public void setDateAxArray(final int n, final CTDateAx ctDateAx) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDateAx ctDateAx2 = (CTDateAx)this.get_store().find_element_user(CTPlotAreaImpl.DATEAX$38, n);
            if (ctDateAx2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDateAx2.set((XmlObject)ctDateAx);
        }
    }
    
    public CTDateAx insertNewDateAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDateAx)this.get_store().insert_element_user(CTPlotAreaImpl.DATEAX$38, n);
        }
    }
    
    public CTDateAx addNewDateAx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDateAx)this.get_store().add_element_user(CTPlotAreaImpl.DATEAX$38);
        }
    }
    
    public void removeDateAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.DATEAX$38, n);
        }
    }
    
    public List<CTSerAx> getSerAxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSerAx>)new CTPlotAreaImpl.SerAxList(this);
        }
    }
    
    public CTSerAx[] getSerAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTPlotAreaImpl.SERAX$40, list);
            final CTSerAx[] array = new CTSerAx[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSerAx getSerAxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSerAx ctSerAx = (CTSerAx)this.get_store().find_element_user(CTPlotAreaImpl.SERAX$40, n);
            if (ctSerAx == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSerAx;
        }
    }
    
    public int sizeOfSerAxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.SERAX$40);
        }
    }
    
    public void setSerAxArray(final CTSerAx[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTPlotAreaImpl.SERAX$40);
        }
    }
    
    public void setSerAxArray(final int n, final CTSerAx ctSerAx) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSerAx ctSerAx2 = (CTSerAx)this.get_store().find_element_user(CTPlotAreaImpl.SERAX$40, n);
            if (ctSerAx2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSerAx2.set((XmlObject)ctSerAx);
        }
    }
    
    public CTSerAx insertNewSerAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSerAx)this.get_store().insert_element_user(CTPlotAreaImpl.SERAX$40, n);
        }
    }
    
    public CTSerAx addNewSerAx() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSerAx)this.get_store().add_element_user(CTPlotAreaImpl.SERAX$40);
        }
    }
    
    public void removeSerAx(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.SERAX$40, n);
        }
    }
    
    public CTDTable getDTable() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDTable ctdTable = (CTDTable)this.get_store().find_element_user(CTPlotAreaImpl.DTABLE$42, 0);
            if (ctdTable == null) {
                return null;
            }
            return ctdTable;
        }
    }
    
    public boolean isSetDTable() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.DTABLE$42) != 0;
        }
    }
    
    public void setDTable(final CTDTable ctdTable) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTDTable ctdTable2 = (CTDTable)this.get_store().find_element_user(CTPlotAreaImpl.DTABLE$42, 0);
            if (ctdTable2 == null) {
                ctdTable2 = (CTDTable)this.get_store().add_element_user(CTPlotAreaImpl.DTABLE$42);
            }
            ctdTable2.set((XmlObject)ctdTable);
        }
    }
    
    public CTDTable addNewDTable() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDTable)this.get_store().add_element_user(CTPlotAreaImpl.DTABLE$42);
        }
    }
    
    public void unsetDTable() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.DTABLE$42, 0);
        }
    }
    
    public CTShapeProperties getSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTShapeProperties ctShapeProperties = (CTShapeProperties)this.get_store().find_element_user(CTPlotAreaImpl.SPPR$44, 0);
            if (ctShapeProperties == null) {
                return null;
            }
            return ctShapeProperties;
        }
    }
    
    public boolean isSetSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.SPPR$44) != 0;
        }
    }
    
    public void setSpPr(final CTShapeProperties ctShapeProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTShapeProperties ctShapeProperties2 = (CTShapeProperties)this.get_store().find_element_user(CTPlotAreaImpl.SPPR$44, 0);
            if (ctShapeProperties2 == null) {
                ctShapeProperties2 = (CTShapeProperties)this.get_store().add_element_user(CTPlotAreaImpl.SPPR$44);
            }
            ctShapeProperties2.set(ctShapeProperties);
        }
    }
    
    public CTShapeProperties addNewSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTShapeProperties)this.get_store().add_element_user(CTPlotAreaImpl.SPPR$44);
        }
    }
    
    public void unsetSpPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.SPPR$44, 0);
        }
    }
    
    public CTExtensionList getExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTExtensionList list = (CTExtensionList)this.get_store().find_element_user(CTPlotAreaImpl.EXTLST$46, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public boolean isSetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPlotAreaImpl.EXTLST$46) != 0;
        }
    }
    
    public void setExtLst(final CTExtensionList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTExtensionList list2 = (CTExtensionList)this.get_store().find_element_user(CTPlotAreaImpl.EXTLST$46, 0);
            if (list2 == null) {
                list2 = (CTExtensionList)this.get_store().add_element_user(CTPlotAreaImpl.EXTLST$46);
            }
            list2.set((XmlObject)list);
        }
    }
    
    public CTExtensionList addNewExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTExtensionList)this.get_store().add_element_user(CTPlotAreaImpl.EXTLST$46);
        }
    }
    
    public void unsetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPlotAreaImpl.EXTLST$46, 0);
        }
    }
    
    static {
        LAYOUT$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "layout");
        AREACHART$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "areaChart");
        AREA3DCHART$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "area3DChart");
        LINECHART$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "lineChart");
        LINE3DCHART$8 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "line3DChart");
        STOCKCHART$10 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "stockChart");
        RADARCHART$12 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "radarChart");
        SCATTERCHART$14 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "scatterChart");
        PIECHART$16 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "pieChart");
        PIE3DCHART$18 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "pie3DChart");
        DOUGHNUTCHART$20 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "doughnutChart");
        BARCHART$22 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "barChart");
        BAR3DCHART$24 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "bar3DChart");
        OFPIECHART$26 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "ofPieChart");
        SURFACECHART$28 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "surfaceChart");
        SURFACE3DCHART$30 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "surface3DChart");
        BUBBLECHART$32 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "bubbleChart");
        VALAX$34 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "valAx");
        CATAX$36 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "catAx");
        DATEAX$38 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "dateAx");
        SERAX$40 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "serAx");
        DTABLE$42 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "dTable");
        SPPR$44 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "spPr");
        EXTLST$46 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "extLst");
    }
}
