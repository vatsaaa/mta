// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.openxmlformats.schemas.drawingml.x2006.chart.CTRelId;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPageSetup;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPageMargins;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTHeaderFooter;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPrintSettings;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTPrintSettingsImpl extends XmlComplexContentImpl implements CTPrintSettings
{
    private static final QName HEADERFOOTER$0;
    private static final QName PAGEMARGINS$2;
    private static final QName PAGESETUP$4;
    private static final QName LEGACYDRAWINGHF$6;
    
    public CTPrintSettingsImpl(final SchemaType type) {
        super(type);
    }
    
    public CTHeaderFooter getHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHeaderFooter ctHeaderFooter = (CTHeaderFooter)this.get_store().find_element_user(CTPrintSettingsImpl.HEADERFOOTER$0, 0);
            if (ctHeaderFooter == null) {
                return null;
            }
            return ctHeaderFooter;
        }
    }
    
    public boolean isSetHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPrintSettingsImpl.HEADERFOOTER$0) != 0;
        }
    }
    
    public void setHeaderFooter(final CTHeaderFooter ctHeaderFooter) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTHeaderFooter ctHeaderFooter2 = (CTHeaderFooter)this.get_store().find_element_user(CTPrintSettingsImpl.HEADERFOOTER$0, 0);
            if (ctHeaderFooter2 == null) {
                ctHeaderFooter2 = (CTHeaderFooter)this.get_store().add_element_user(CTPrintSettingsImpl.HEADERFOOTER$0);
            }
            ctHeaderFooter2.set(ctHeaderFooter);
        }
    }
    
    public CTHeaderFooter addNewHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHeaderFooter)this.get_store().add_element_user(CTPrintSettingsImpl.HEADERFOOTER$0);
        }
    }
    
    public void unsetHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPrintSettingsImpl.HEADERFOOTER$0, 0);
        }
    }
    
    public CTPageMargins getPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPageMargins ctPageMargins = (CTPageMargins)this.get_store().find_element_user(CTPrintSettingsImpl.PAGEMARGINS$2, 0);
            if (ctPageMargins == null) {
                return null;
            }
            return ctPageMargins;
        }
    }
    
    public boolean isSetPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPrintSettingsImpl.PAGEMARGINS$2) != 0;
        }
    }
    
    public void setPageMargins(final CTPageMargins ctPageMargins) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPageMargins ctPageMargins2 = (CTPageMargins)this.get_store().find_element_user(CTPrintSettingsImpl.PAGEMARGINS$2, 0);
            if (ctPageMargins2 == null) {
                ctPageMargins2 = (CTPageMargins)this.get_store().add_element_user(CTPrintSettingsImpl.PAGEMARGINS$2);
            }
            ctPageMargins2.set(ctPageMargins);
        }
    }
    
    public CTPageMargins addNewPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPageMargins)this.get_store().add_element_user(CTPrintSettingsImpl.PAGEMARGINS$2);
        }
    }
    
    public void unsetPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPrintSettingsImpl.PAGEMARGINS$2, 0);
        }
    }
    
    public CTPageSetup getPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPageSetup ctPageSetup = (CTPageSetup)this.get_store().find_element_user(CTPrintSettingsImpl.PAGESETUP$4, 0);
            if (ctPageSetup == null) {
                return null;
            }
            return ctPageSetup;
        }
    }
    
    public boolean isSetPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPrintSettingsImpl.PAGESETUP$4) != 0;
        }
    }
    
    public void setPageSetup(final CTPageSetup ctPageSetup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPageSetup ctPageSetup2 = (CTPageSetup)this.get_store().find_element_user(CTPrintSettingsImpl.PAGESETUP$4, 0);
            if (ctPageSetup2 == null) {
                ctPageSetup2 = (CTPageSetup)this.get_store().add_element_user(CTPrintSettingsImpl.PAGESETUP$4);
            }
            ctPageSetup2.set(ctPageSetup);
        }
    }
    
    public CTPageSetup addNewPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPageSetup)this.get_store().add_element_user(CTPrintSettingsImpl.PAGESETUP$4);
        }
    }
    
    public void unsetPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPrintSettingsImpl.PAGESETUP$4, 0);
        }
    }
    
    public CTRelId getLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRelId ctRelId = (CTRelId)this.get_store().find_element_user(CTPrintSettingsImpl.LEGACYDRAWINGHF$6, 0);
            if (ctRelId == null) {
                return null;
            }
            return ctRelId;
        }
    }
    
    public boolean isSetLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPrintSettingsImpl.LEGACYDRAWINGHF$6) != 0;
        }
    }
    
    public void setLegacyDrawingHF(final CTRelId ctRelId) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTRelId ctRelId2 = (CTRelId)this.get_store().find_element_user(CTPrintSettingsImpl.LEGACYDRAWINGHF$6, 0);
            if (ctRelId2 == null) {
                ctRelId2 = (CTRelId)this.get_store().add_element_user(CTPrintSettingsImpl.LEGACYDRAWINGHF$6);
            }
            ctRelId2.set((XmlObject)ctRelId);
        }
    }
    
    public CTRelId addNewLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRelId)this.get_store().add_element_user(CTPrintSettingsImpl.LEGACYDRAWINGHF$6);
        }
    }
    
    public void unsetLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPrintSettingsImpl.LEGACYDRAWINGHF$6, 0);
        }
    }
    
    static {
        HEADERFOOTER$0 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "headerFooter");
        PAGEMARGINS$2 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "pageMargins");
        PAGESETUP$4 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "pageSetup");
        LEGACYDRAWINGHF$6 = new QName("http://schemas.openxmlformats.org/drawingml/2006/chart", "legacyDrawingHF");
    }
}
