// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.drawingml.x2006.chart.STXstring;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STXstringImpl extends JavaStringHolderEx implements STXstring
{
    public STXstringImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STXstringImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
