// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlDouble;

public interface STLogBase extends XmlDouble
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLogBase.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlogbase11a1type");
    
    public static final class Factory
    {
        public static STLogBase newValue(final Object o) {
            return (STLogBase)STLogBase.type.newValue(o);
        }
        
        public static STLogBase newInstance() {
            return (STLogBase)XmlBeans.getContextTypeLoader().newInstance(STLogBase.type, null);
        }
        
        public static STLogBase newInstance(final XmlOptions xmlOptions) {
            return (STLogBase)XmlBeans.getContextTypeLoader().newInstance(STLogBase.type, xmlOptions);
        }
        
        public static STLogBase parse(final String s) throws XmlException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(s, STLogBase.type, null);
        }
        
        public static STLogBase parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(s, STLogBase.type, xmlOptions);
        }
        
        public static STLogBase parse(final File file) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(file, STLogBase.type, null);
        }
        
        public static STLogBase parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(file, STLogBase.type, xmlOptions);
        }
        
        public static STLogBase parse(final URL url) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(url, STLogBase.type, null);
        }
        
        public static STLogBase parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(url, STLogBase.type, xmlOptions);
        }
        
        public static STLogBase parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(inputStream, STLogBase.type, null);
        }
        
        public static STLogBase parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(inputStream, STLogBase.type, xmlOptions);
        }
        
        public static STLogBase parse(final Reader reader) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(reader, STLogBase.type, null);
        }
        
        public static STLogBase parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(reader, STLogBase.type, xmlOptions);
        }
        
        public static STLogBase parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLogBase.type, null);
        }
        
        public static STLogBase parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLogBase.type, xmlOptions);
        }
        
        public static STLogBase parse(final Node node) throws XmlException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(node, STLogBase.type, null);
        }
        
        public static STLogBase parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(node, STLogBase.type, xmlOptions);
        }
        
        @Deprecated
        public static STLogBase parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLogBase.type, null);
        }
        
        @Deprecated
        public static STLogBase parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLogBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLogBase.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLogBase.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLogBase.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
