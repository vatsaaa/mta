// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlDouble;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTDouble extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTDouble.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctdoublec10btype");
    
    double getVal();
    
    XmlDouble xgetVal();
    
    void setVal(final double p0);
    
    void xsetVal(final XmlDouble p0);
    
    public static final class Factory
    {
        public static CTDouble newInstance() {
            return (CTDouble)XmlBeans.getContextTypeLoader().newInstance(CTDouble.type, null);
        }
        
        public static CTDouble newInstance(final XmlOptions xmlOptions) {
            return (CTDouble)XmlBeans.getContextTypeLoader().newInstance(CTDouble.type, xmlOptions);
        }
        
        public static CTDouble parse(final String s) throws XmlException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(s, CTDouble.type, null);
        }
        
        public static CTDouble parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(s, CTDouble.type, xmlOptions);
        }
        
        public static CTDouble parse(final File file) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(file, CTDouble.type, null);
        }
        
        public static CTDouble parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(file, CTDouble.type, xmlOptions);
        }
        
        public static CTDouble parse(final URL url) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(url, CTDouble.type, null);
        }
        
        public static CTDouble parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(url, CTDouble.type, xmlOptions);
        }
        
        public static CTDouble parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(inputStream, CTDouble.type, null);
        }
        
        public static CTDouble parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(inputStream, CTDouble.type, xmlOptions);
        }
        
        public static CTDouble parse(final Reader reader) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(reader, CTDouble.type, null);
        }
        
        public static CTDouble parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(reader, CTDouble.type, xmlOptions);
        }
        
        public static CTDouble parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDouble.type, null);
        }
        
        public static CTDouble parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDouble.type, xmlOptions);
        }
        
        public static CTDouble parse(final Node node) throws XmlException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(node, CTDouble.type, null);
        }
        
        public static CTDouble parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(node, CTDouble.type, xmlOptions);
        }
        
        @Deprecated
        public static CTDouble parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDouble.type, null);
        }
        
        @Deprecated
        public static CTDouble parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTDouble)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDouble.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDouble.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDouble.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
