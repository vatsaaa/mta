// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface ChartSpaceDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(ChartSpaceDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("chartspace36e0doctype");
    
    CTChartSpace getChartSpace();
    
    void setChartSpace(final CTChartSpace p0);
    
    CTChartSpace addNewChartSpace();
    
    public static final class Factory
    {
        public static ChartSpaceDocument newInstance() {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().newInstance(ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument newInstance(final XmlOptions xmlOptions) {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().newInstance(ChartSpaceDocument.type, xmlOptions);
        }
        
        public static ChartSpaceDocument parse(final String s) throws XmlException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(s, ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(s, ChartSpaceDocument.type, xmlOptions);
        }
        
        public static ChartSpaceDocument parse(final File file) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(file, ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(file, ChartSpaceDocument.type, xmlOptions);
        }
        
        public static ChartSpaceDocument parse(final URL url) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(url, ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(url, ChartSpaceDocument.type, xmlOptions);
        }
        
        public static ChartSpaceDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(inputStream, ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(inputStream, ChartSpaceDocument.type, xmlOptions);
        }
        
        public static ChartSpaceDocument parse(final Reader reader) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(reader, ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(reader, ChartSpaceDocument.type, xmlOptions);
        }
        
        public static ChartSpaceDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, ChartSpaceDocument.type, xmlOptions);
        }
        
        public static ChartSpaceDocument parse(final Node node) throws XmlException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(node, ChartSpaceDocument.type, null);
        }
        
        public static ChartSpaceDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(node, ChartSpaceDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static ChartSpaceDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, ChartSpaceDocument.type, null);
        }
        
        @Deprecated
        public static ChartSpaceDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (ChartSpaceDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, ChartSpaceDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, ChartSpaceDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, ChartSpaceDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
