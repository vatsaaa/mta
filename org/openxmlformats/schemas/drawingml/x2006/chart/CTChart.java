// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTChart extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTChart.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctchartc108type");
    
    CTTitle getTitle();
    
    boolean isSetTitle();
    
    void setTitle(final CTTitle p0);
    
    CTTitle addNewTitle();
    
    void unsetTitle();
    
    CTBoolean getAutoTitleDeleted();
    
    boolean isSetAutoTitleDeleted();
    
    void setAutoTitleDeleted(final CTBoolean p0);
    
    CTBoolean addNewAutoTitleDeleted();
    
    void unsetAutoTitleDeleted();
    
    CTPivotFmts getPivotFmts();
    
    boolean isSetPivotFmts();
    
    void setPivotFmts(final CTPivotFmts p0);
    
    CTPivotFmts addNewPivotFmts();
    
    void unsetPivotFmts();
    
    CTView3D getView3D();
    
    boolean isSetView3D();
    
    void setView3D(final CTView3D p0);
    
    CTView3D addNewView3D();
    
    void unsetView3D();
    
    CTSurface getFloor();
    
    boolean isSetFloor();
    
    void setFloor(final CTSurface p0);
    
    CTSurface addNewFloor();
    
    void unsetFloor();
    
    CTSurface getSideWall();
    
    boolean isSetSideWall();
    
    void setSideWall(final CTSurface p0);
    
    CTSurface addNewSideWall();
    
    void unsetSideWall();
    
    CTSurface getBackWall();
    
    boolean isSetBackWall();
    
    void setBackWall(final CTSurface p0);
    
    CTSurface addNewBackWall();
    
    void unsetBackWall();
    
    CTPlotArea getPlotArea();
    
    void setPlotArea(final CTPlotArea p0);
    
    CTPlotArea addNewPlotArea();
    
    CTLegend getLegend();
    
    boolean isSetLegend();
    
    void setLegend(final CTLegend p0);
    
    CTLegend addNewLegend();
    
    void unsetLegend();
    
    CTBoolean getPlotVisOnly();
    
    boolean isSetPlotVisOnly();
    
    void setPlotVisOnly(final CTBoolean p0);
    
    CTBoolean addNewPlotVisOnly();
    
    void unsetPlotVisOnly();
    
    CTDispBlanksAs getDispBlanksAs();
    
    boolean isSetDispBlanksAs();
    
    void setDispBlanksAs(final CTDispBlanksAs p0);
    
    CTDispBlanksAs addNewDispBlanksAs();
    
    void unsetDispBlanksAs();
    
    CTBoolean getShowDLblsOverMax();
    
    boolean isSetShowDLblsOverMax();
    
    void setShowDLblsOverMax(final CTBoolean p0);
    
    CTBoolean addNewShowDLblsOverMax();
    
    void unsetShowDLblsOverMax();
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTChart newInstance() {
            return (CTChart)XmlBeans.getContextTypeLoader().newInstance(CTChart.type, null);
        }
        
        public static CTChart newInstance(final XmlOptions xmlOptions) {
            return (CTChart)XmlBeans.getContextTypeLoader().newInstance(CTChart.type, xmlOptions);
        }
        
        public static CTChart parse(final String s) throws XmlException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(s, CTChart.type, null);
        }
        
        public static CTChart parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(s, CTChart.type, xmlOptions);
        }
        
        public static CTChart parse(final File file) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(file, CTChart.type, null);
        }
        
        public static CTChart parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(file, CTChart.type, xmlOptions);
        }
        
        public static CTChart parse(final URL url) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(url, CTChart.type, null);
        }
        
        public static CTChart parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(url, CTChart.type, xmlOptions);
        }
        
        public static CTChart parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(inputStream, CTChart.type, null);
        }
        
        public static CTChart parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(inputStream, CTChart.type, xmlOptions);
        }
        
        public static CTChart parse(final Reader reader) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(reader, CTChart.type, null);
        }
        
        public static CTChart parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(reader, CTChart.type, xmlOptions);
        }
        
        public static CTChart parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTChart.type, null);
        }
        
        public static CTChart parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTChart.type, xmlOptions);
        }
        
        public static CTChart parse(final Node node) throws XmlException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(node, CTChart.type, null);
        }
        
        public static CTChart parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(node, CTChart.type, xmlOptions);
        }
        
        @Deprecated
        public static CTChart parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTChart.type, null);
        }
        
        @Deprecated
        public static CTChart parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTChart)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTChart.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTChart.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTChart.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
