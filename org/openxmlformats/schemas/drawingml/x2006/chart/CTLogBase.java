// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTLogBase extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTLogBase.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctlogbase9191type");
    
    double getVal();
    
    STLogBase xgetVal();
    
    void setVal(final double p0);
    
    void xsetVal(final STLogBase p0);
    
    public static final class Factory
    {
        public static CTLogBase newInstance() {
            return (CTLogBase)XmlBeans.getContextTypeLoader().newInstance(CTLogBase.type, null);
        }
        
        public static CTLogBase newInstance(final XmlOptions xmlOptions) {
            return (CTLogBase)XmlBeans.getContextTypeLoader().newInstance(CTLogBase.type, xmlOptions);
        }
        
        public static CTLogBase parse(final String s) throws XmlException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(s, CTLogBase.type, null);
        }
        
        public static CTLogBase parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(s, CTLogBase.type, xmlOptions);
        }
        
        public static CTLogBase parse(final File file) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(file, CTLogBase.type, null);
        }
        
        public static CTLogBase parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(file, CTLogBase.type, xmlOptions);
        }
        
        public static CTLogBase parse(final URL url) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(url, CTLogBase.type, null);
        }
        
        public static CTLogBase parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(url, CTLogBase.type, xmlOptions);
        }
        
        public static CTLogBase parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTLogBase.type, null);
        }
        
        public static CTLogBase parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTLogBase.type, xmlOptions);
        }
        
        public static CTLogBase parse(final Reader reader) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(reader, CTLogBase.type, null);
        }
        
        public static CTLogBase parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(reader, CTLogBase.type, xmlOptions);
        }
        
        public static CTLogBase parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLogBase.type, null);
        }
        
        public static CTLogBase parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLogBase.type, xmlOptions);
        }
        
        public static CTLogBase parse(final Node node) throws XmlException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(node, CTLogBase.type, null);
        }
        
        public static CTLogBase parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(node, CTLogBase.type, xmlOptions);
        }
        
        @Deprecated
        public static CTLogBase parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLogBase.type, null);
        }
        
        @Deprecated
        public static CTLogBase parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTLogBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLogBase.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLogBase.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLogBase.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
