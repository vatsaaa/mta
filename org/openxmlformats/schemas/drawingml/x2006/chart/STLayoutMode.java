// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STLayoutMode extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLayoutMode.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlayoutmode19dftype");
    public static final Enum EDGE = Enum.forString("edge");
    public static final Enum FACTOR = Enum.forString("factor");
    public static final int INT_EDGE = 1;
    public static final int INT_FACTOR = 2;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STLayoutMode newValue(final Object o) {
            return (STLayoutMode)STLayoutMode.type.newValue(o);
        }
        
        public static STLayoutMode newInstance() {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().newInstance(STLayoutMode.type, null);
        }
        
        public static STLayoutMode newInstance(final XmlOptions xmlOptions) {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().newInstance(STLayoutMode.type, xmlOptions);
        }
        
        public static STLayoutMode parse(final String s) throws XmlException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(s, STLayoutMode.type, null);
        }
        
        public static STLayoutMode parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(s, STLayoutMode.type, xmlOptions);
        }
        
        public static STLayoutMode parse(final File file) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(file, STLayoutMode.type, null);
        }
        
        public static STLayoutMode parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(file, STLayoutMode.type, xmlOptions);
        }
        
        public static STLayoutMode parse(final URL url) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(url, STLayoutMode.type, null);
        }
        
        public static STLayoutMode parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(url, STLayoutMode.type, xmlOptions);
        }
        
        public static STLayoutMode parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(inputStream, STLayoutMode.type, null);
        }
        
        public static STLayoutMode parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(inputStream, STLayoutMode.type, xmlOptions);
        }
        
        public static STLayoutMode parse(final Reader reader) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(reader, STLayoutMode.type, null);
        }
        
        public static STLayoutMode parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(reader, STLayoutMode.type, xmlOptions);
        }
        
        public static STLayoutMode parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLayoutMode.type, null);
        }
        
        public static STLayoutMode parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLayoutMode.type, xmlOptions);
        }
        
        public static STLayoutMode parse(final Node node) throws XmlException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(node, STLayoutMode.type, null);
        }
        
        public static STLayoutMode parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(node, STLayoutMode.type, xmlOptions);
        }
        
        @Deprecated
        public static STLayoutMode parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLayoutMode.type, null);
        }
        
        @Deprecated
        public static STLayoutMode parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLayoutMode)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLayoutMode.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLayoutMode.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLayoutMode.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_EDGE = 1;
        static final int INT_FACTOR = 2;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("edge", 1), new Enum("factor", 2) });
        }
    }
}
