// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.drawingml.x2006.chart;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STCrossBetween extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STCrossBetween.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stcrossbetweenf504type");
    public static final Enum BETWEEN = Enum.forString("between");
    public static final Enum MID_CAT = Enum.forString("midCat");
    public static final int INT_BETWEEN = 1;
    public static final int INT_MID_CAT = 2;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STCrossBetween newValue(final Object o) {
            return (STCrossBetween)STCrossBetween.type.newValue(o);
        }
        
        public static STCrossBetween newInstance() {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().newInstance(STCrossBetween.type, null);
        }
        
        public static STCrossBetween newInstance(final XmlOptions xmlOptions) {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().newInstance(STCrossBetween.type, xmlOptions);
        }
        
        public static STCrossBetween parse(final String s) throws XmlException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(s, STCrossBetween.type, null);
        }
        
        public static STCrossBetween parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(s, STCrossBetween.type, xmlOptions);
        }
        
        public static STCrossBetween parse(final File file) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(file, STCrossBetween.type, null);
        }
        
        public static STCrossBetween parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(file, STCrossBetween.type, xmlOptions);
        }
        
        public static STCrossBetween parse(final URL url) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(url, STCrossBetween.type, null);
        }
        
        public static STCrossBetween parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(url, STCrossBetween.type, xmlOptions);
        }
        
        public static STCrossBetween parse(final InputStream inputStream) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(inputStream, STCrossBetween.type, null);
        }
        
        public static STCrossBetween parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(inputStream, STCrossBetween.type, xmlOptions);
        }
        
        public static STCrossBetween parse(final Reader reader) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(reader, STCrossBetween.type, null);
        }
        
        public static STCrossBetween parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(reader, STCrossBetween.type, xmlOptions);
        }
        
        public static STCrossBetween parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STCrossBetween.type, null);
        }
        
        public static STCrossBetween parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STCrossBetween.type, xmlOptions);
        }
        
        public static STCrossBetween parse(final Node node) throws XmlException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(node, STCrossBetween.type, null);
        }
        
        public static STCrossBetween parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(node, STCrossBetween.type, xmlOptions);
        }
        
        @Deprecated
        public static STCrossBetween parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STCrossBetween.type, null);
        }
        
        @Deprecated
        public static STCrossBetween parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STCrossBetween)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STCrossBetween.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STCrossBetween.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STCrossBetween.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_BETWEEN = 1;
        static final int INT_MID_CAT = 2;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("between", 1), new Enum("midCat", 2) });
        }
    }
}
