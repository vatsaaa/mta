// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface ChartsheetDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(ChartsheetDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("chartsheet99dedoctype");
    
    CTChartsheet getChartsheet();
    
    void setChartsheet(final CTChartsheet p0);
    
    CTChartsheet addNewChartsheet();
    
    public static final class Factory
    {
        public static ChartsheetDocument newInstance() {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().newInstance(ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument newInstance(final XmlOptions xmlOptions) {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().newInstance(ChartsheetDocument.type, xmlOptions);
        }
        
        public static ChartsheetDocument parse(final String s) throws XmlException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(s, ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(s, ChartsheetDocument.type, xmlOptions);
        }
        
        public static ChartsheetDocument parse(final File file) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(file, ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(file, ChartsheetDocument.type, xmlOptions);
        }
        
        public static ChartsheetDocument parse(final URL url) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(url, ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(url, ChartsheetDocument.type, xmlOptions);
        }
        
        public static ChartsheetDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(inputStream, ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(inputStream, ChartsheetDocument.type, xmlOptions);
        }
        
        public static ChartsheetDocument parse(final Reader reader) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(reader, ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(reader, ChartsheetDocument.type, xmlOptions);
        }
        
        public static ChartsheetDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, ChartsheetDocument.type, xmlOptions);
        }
        
        public static ChartsheetDocument parse(final Node node) throws XmlException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(node, ChartsheetDocument.type, null);
        }
        
        public static ChartsheetDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(node, ChartsheetDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static ChartsheetDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, ChartsheetDocument.type, null);
        }
        
        @Deprecated
        public static ChartsheetDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (ChartsheetDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, ChartsheetDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, ChartsheetDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, ChartsheetDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
