// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STBorderId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STBorderId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stborderid1a80type");
    
    public static final class Factory
    {
        public static STBorderId newValue(final Object o) {
            return (STBorderId)STBorderId.type.newValue(o);
        }
        
        public static STBorderId newInstance() {
            return (STBorderId)XmlBeans.getContextTypeLoader().newInstance(STBorderId.type, null);
        }
        
        public static STBorderId newInstance(final XmlOptions xmlOptions) {
            return (STBorderId)XmlBeans.getContextTypeLoader().newInstance(STBorderId.type, xmlOptions);
        }
        
        public static STBorderId parse(final String s) throws XmlException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(s, STBorderId.type, null);
        }
        
        public static STBorderId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(s, STBorderId.type, xmlOptions);
        }
        
        public static STBorderId parse(final File file) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(file, STBorderId.type, null);
        }
        
        public static STBorderId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(file, STBorderId.type, xmlOptions);
        }
        
        public static STBorderId parse(final URL url) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(url, STBorderId.type, null);
        }
        
        public static STBorderId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(url, STBorderId.type, xmlOptions);
        }
        
        public static STBorderId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(inputStream, STBorderId.type, null);
        }
        
        public static STBorderId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(inputStream, STBorderId.type, xmlOptions);
        }
        
        public static STBorderId parse(final Reader reader) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(reader, STBorderId.type, null);
        }
        
        public static STBorderId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(reader, STBorderId.type, xmlOptions);
        }
        
        public static STBorderId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STBorderId.type, null);
        }
        
        public static STBorderId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STBorderId.type, xmlOptions);
        }
        
        public static STBorderId parse(final Node node) throws XmlException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(node, STBorderId.type, null);
        }
        
        public static STBorderId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(node, STBorderId.type, xmlOptions);
        }
        
        @Deprecated
        public static STBorderId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STBorderId.type, null);
        }
        
        @Deprecated
        public static STBorderId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STBorderId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STBorderId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STBorderId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STBorderId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
