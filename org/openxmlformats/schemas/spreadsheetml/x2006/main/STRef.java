// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STRef extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STRef.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stref90a2type");
    
    public static final class Factory
    {
        public static STRef newValue(final Object o) {
            return (STRef)STRef.type.newValue(o);
        }
        
        public static STRef newInstance() {
            return (STRef)XmlBeans.getContextTypeLoader().newInstance(STRef.type, null);
        }
        
        public static STRef newInstance(final XmlOptions xmlOptions) {
            return (STRef)XmlBeans.getContextTypeLoader().newInstance(STRef.type, xmlOptions);
        }
        
        public static STRef parse(final String s) throws XmlException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(s, STRef.type, null);
        }
        
        public static STRef parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(s, STRef.type, xmlOptions);
        }
        
        public static STRef parse(final File file) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(file, STRef.type, null);
        }
        
        public static STRef parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(file, STRef.type, xmlOptions);
        }
        
        public static STRef parse(final URL url) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(url, STRef.type, null);
        }
        
        public static STRef parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(url, STRef.type, xmlOptions);
        }
        
        public static STRef parse(final InputStream inputStream) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(inputStream, STRef.type, null);
        }
        
        public static STRef parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(inputStream, STRef.type, xmlOptions);
        }
        
        public static STRef parse(final Reader reader) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(reader, STRef.type, null);
        }
        
        public static STRef parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(reader, STRef.type, xmlOptions);
        }
        
        public static STRef parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STRef.type, null);
        }
        
        public static STRef parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STRef.type, xmlOptions);
        }
        
        public static STRef parse(final Node node) throws XmlException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(node, STRef.type, null);
        }
        
        public static STRef parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(node, STRef.type, xmlOptions);
        }
        
        @Deprecated
        public static STRef parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STRef.type, null);
        }
        
        @Deprecated
        public static STRef parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STRef.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STRef.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STRef.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
