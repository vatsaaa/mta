// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface StyleSheetDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(StyleSheetDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stylesheet5d8bdoctype");
    
    CTStylesheet getStyleSheet();
    
    void setStyleSheet(final CTStylesheet p0);
    
    CTStylesheet addNewStyleSheet();
    
    public static final class Factory
    {
        public static StyleSheetDocument newInstance() {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().newInstance(StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument newInstance(final XmlOptions xmlOptions) {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().newInstance(StyleSheetDocument.type, xmlOptions);
        }
        
        public static StyleSheetDocument parse(final String s) throws XmlException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(s, StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(s, StyleSheetDocument.type, xmlOptions);
        }
        
        public static StyleSheetDocument parse(final File file) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(file, StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(file, StyleSheetDocument.type, xmlOptions);
        }
        
        public static StyleSheetDocument parse(final URL url) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(url, StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(url, StyleSheetDocument.type, xmlOptions);
        }
        
        public static StyleSheetDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(inputStream, StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(inputStream, StyleSheetDocument.type, xmlOptions);
        }
        
        public static StyleSheetDocument parse(final Reader reader) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(reader, StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(reader, StyleSheetDocument.type, xmlOptions);
        }
        
        public static StyleSheetDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, StyleSheetDocument.type, xmlOptions);
        }
        
        public static StyleSheetDocument parse(final Node node) throws XmlException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(node, StyleSheetDocument.type, null);
        }
        
        public static StyleSheetDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(node, StyleSheetDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static StyleSheetDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, StyleSheetDocument.type, null);
        }
        
        @Deprecated
        public static StyleSheetDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (StyleSheetDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, StyleSheetDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, StyleSheetDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, StyleSheetDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
