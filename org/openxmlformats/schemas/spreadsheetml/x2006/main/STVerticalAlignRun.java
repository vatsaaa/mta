// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STVerticalAlignRun extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STVerticalAlignRun.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stverticalalignrun4db5type");
    public static final Enum BASELINE = Enum.forString("baseline");
    public static final Enum SUPERSCRIPT = Enum.forString("superscript");
    public static final Enum SUBSCRIPT = Enum.forString("subscript");
    public static final int INT_BASELINE = 1;
    public static final int INT_SUPERSCRIPT = 2;
    public static final int INT_SUBSCRIPT = 3;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STVerticalAlignRun newValue(final Object o) {
            return (STVerticalAlignRun)STVerticalAlignRun.type.newValue(o);
        }
        
        public static STVerticalAlignRun newInstance() {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().newInstance(STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun newInstance(final XmlOptions xmlOptions) {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().newInstance(STVerticalAlignRun.type, xmlOptions);
        }
        
        public static STVerticalAlignRun parse(final String s) throws XmlException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(s, STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(s, STVerticalAlignRun.type, xmlOptions);
        }
        
        public static STVerticalAlignRun parse(final File file) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(file, STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(file, STVerticalAlignRun.type, xmlOptions);
        }
        
        public static STVerticalAlignRun parse(final URL url) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(url, STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(url, STVerticalAlignRun.type, xmlOptions);
        }
        
        public static STVerticalAlignRun parse(final InputStream inputStream) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(inputStream, STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(inputStream, STVerticalAlignRun.type, xmlOptions);
        }
        
        public static STVerticalAlignRun parse(final Reader reader) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(reader, STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(reader, STVerticalAlignRun.type, xmlOptions);
        }
        
        public static STVerticalAlignRun parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STVerticalAlignRun.type, xmlOptions);
        }
        
        public static STVerticalAlignRun parse(final Node node) throws XmlException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(node, STVerticalAlignRun.type, null);
        }
        
        public static STVerticalAlignRun parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(node, STVerticalAlignRun.type, xmlOptions);
        }
        
        @Deprecated
        public static STVerticalAlignRun parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STVerticalAlignRun.type, null);
        }
        
        @Deprecated
        public static STVerticalAlignRun parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STVerticalAlignRun.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STVerticalAlignRun.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STVerticalAlignRun.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_BASELINE = 1;
        static final int INT_SUPERSCRIPT = 2;
        static final int INT_SUBSCRIPT = 3;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("baseline", 1), new Enum("superscript", 2), new Enum("subscript", 3) });
        }
    }
}
