// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTUnderlineProperty extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTUnderlineProperty.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctunderlineproperty8e20type");
    
    STUnderlineValues.Enum getVal();
    
    STUnderlineValues xgetVal();
    
    boolean isSetVal();
    
    void setVal(final STUnderlineValues.Enum p0);
    
    void xsetVal(final STUnderlineValues p0);
    
    void unsetVal();
    
    public static final class Factory
    {
        public static CTUnderlineProperty newInstance() {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().newInstance(CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty newInstance(final XmlOptions xmlOptions) {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().newInstance(CTUnderlineProperty.type, xmlOptions);
        }
        
        public static CTUnderlineProperty parse(final String s) throws XmlException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(s, CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(s, CTUnderlineProperty.type, xmlOptions);
        }
        
        public static CTUnderlineProperty parse(final File file) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(file, CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(file, CTUnderlineProperty.type, xmlOptions);
        }
        
        public static CTUnderlineProperty parse(final URL url) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(url, CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(url, CTUnderlineProperty.type, xmlOptions);
        }
        
        public static CTUnderlineProperty parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(inputStream, CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(inputStream, CTUnderlineProperty.type, xmlOptions);
        }
        
        public static CTUnderlineProperty parse(final Reader reader) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(reader, CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(reader, CTUnderlineProperty.type, xmlOptions);
        }
        
        public static CTUnderlineProperty parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTUnderlineProperty.type, xmlOptions);
        }
        
        public static CTUnderlineProperty parse(final Node node) throws XmlException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(node, CTUnderlineProperty.type, null);
        }
        
        public static CTUnderlineProperty parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(node, CTUnderlineProperty.type, xmlOptions);
        }
        
        @Deprecated
        public static CTUnderlineProperty parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTUnderlineProperty.type, null);
        }
        
        @Deprecated
        public static CTUnderlineProperty parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTUnderlineProperty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTUnderlineProperty.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTUnderlineProperty.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTUnderlineProperty.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
