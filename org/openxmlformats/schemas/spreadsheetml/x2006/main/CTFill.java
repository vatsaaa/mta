// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTFill extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTFill.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfill550ctype");
    
    CTPatternFill getPatternFill();
    
    boolean isSetPatternFill();
    
    void setPatternFill(final CTPatternFill p0);
    
    CTPatternFill addNewPatternFill();
    
    void unsetPatternFill();
    
    CTGradientFill getGradientFill();
    
    boolean isSetGradientFill();
    
    void setGradientFill(final CTGradientFill p0);
    
    CTGradientFill addNewGradientFill();
    
    void unsetGradientFill();
    
    public static final class Factory
    {
        public static CTFill newInstance() {
            return (CTFill)XmlBeans.getContextTypeLoader().newInstance(CTFill.type, null);
        }
        
        public static CTFill newInstance(final XmlOptions xmlOptions) {
            return (CTFill)XmlBeans.getContextTypeLoader().newInstance(CTFill.type, xmlOptions);
        }
        
        public static CTFill parse(final String s) throws XmlException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(s, CTFill.type, null);
        }
        
        public static CTFill parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(s, CTFill.type, xmlOptions);
        }
        
        public static CTFill parse(final File file) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(file, CTFill.type, null);
        }
        
        public static CTFill parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(file, CTFill.type, xmlOptions);
        }
        
        public static CTFill parse(final URL url) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(url, CTFill.type, null);
        }
        
        public static CTFill parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(url, CTFill.type, xmlOptions);
        }
        
        public static CTFill parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(inputStream, CTFill.type, null);
        }
        
        public static CTFill parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(inputStream, CTFill.type, xmlOptions);
        }
        
        public static CTFill parse(final Reader reader) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(reader, CTFill.type, null);
        }
        
        public static CTFill parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(reader, CTFill.type, xmlOptions);
        }
        
        public static CTFill parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFill.type, null);
        }
        
        public static CTFill parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFill.type, xmlOptions);
        }
        
        public static CTFill parse(final Node node) throws XmlException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(node, CTFill.type, null);
        }
        
        public static CTFill parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(node, CTFill.type, xmlOptions);
        }
        
        @Deprecated
        public static CTFill parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFill.type, null);
        }
        
        @Deprecated
        public static CTFill parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTFill)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFill.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFill.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFill.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
