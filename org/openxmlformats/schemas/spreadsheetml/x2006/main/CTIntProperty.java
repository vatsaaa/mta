// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlInt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTIntProperty extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTIntProperty.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctintproperty32c3type");
    
    int getVal();
    
    XmlInt xgetVal();
    
    void setVal(final int p0);
    
    void xsetVal(final XmlInt p0);
    
    public static final class Factory
    {
        public static CTIntProperty newInstance() {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().newInstance(CTIntProperty.type, null);
        }
        
        public static CTIntProperty newInstance(final XmlOptions xmlOptions) {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().newInstance(CTIntProperty.type, xmlOptions);
        }
        
        public static CTIntProperty parse(final String s) throws XmlException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(s, CTIntProperty.type, null);
        }
        
        public static CTIntProperty parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(s, CTIntProperty.type, xmlOptions);
        }
        
        public static CTIntProperty parse(final File file) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(file, CTIntProperty.type, null);
        }
        
        public static CTIntProperty parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(file, CTIntProperty.type, xmlOptions);
        }
        
        public static CTIntProperty parse(final URL url) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(url, CTIntProperty.type, null);
        }
        
        public static CTIntProperty parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(url, CTIntProperty.type, xmlOptions);
        }
        
        public static CTIntProperty parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(inputStream, CTIntProperty.type, null);
        }
        
        public static CTIntProperty parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(inputStream, CTIntProperty.type, xmlOptions);
        }
        
        public static CTIntProperty parse(final Reader reader) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(reader, CTIntProperty.type, null);
        }
        
        public static CTIntProperty parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(reader, CTIntProperty.type, xmlOptions);
        }
        
        public static CTIntProperty parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTIntProperty.type, null);
        }
        
        public static CTIntProperty parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTIntProperty.type, xmlOptions);
        }
        
        public static CTIntProperty parse(final Node node) throws XmlException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(node, CTIntProperty.type, null);
        }
        
        public static CTIntProperty parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(node, CTIntProperty.type, xmlOptions);
        }
        
        @Deprecated
        public static CTIntProperty parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTIntProperty.type, null);
        }
        
        @Deprecated
        public static CTIntProperty parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTIntProperty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTIntProperty.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTIntProperty.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTIntProperty.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
