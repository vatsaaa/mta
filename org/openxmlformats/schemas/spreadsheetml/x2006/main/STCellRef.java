// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STCellRef extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STCellRef.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stcellrefe4e0type");
    
    public static final class Factory
    {
        public static STCellRef newValue(final Object o) {
            return (STCellRef)STCellRef.type.newValue(o);
        }
        
        public static STCellRef newInstance() {
            return (STCellRef)XmlBeans.getContextTypeLoader().newInstance(STCellRef.type, null);
        }
        
        public static STCellRef newInstance(final XmlOptions xmlOptions) {
            return (STCellRef)XmlBeans.getContextTypeLoader().newInstance(STCellRef.type, xmlOptions);
        }
        
        public static STCellRef parse(final String s) throws XmlException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(s, STCellRef.type, null);
        }
        
        public static STCellRef parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(s, STCellRef.type, xmlOptions);
        }
        
        public static STCellRef parse(final File file) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(file, STCellRef.type, null);
        }
        
        public static STCellRef parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(file, STCellRef.type, xmlOptions);
        }
        
        public static STCellRef parse(final URL url) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(url, STCellRef.type, null);
        }
        
        public static STCellRef parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(url, STCellRef.type, xmlOptions);
        }
        
        public static STCellRef parse(final InputStream inputStream) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(inputStream, STCellRef.type, null);
        }
        
        public static STCellRef parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(inputStream, STCellRef.type, xmlOptions);
        }
        
        public static STCellRef parse(final Reader reader) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(reader, STCellRef.type, null);
        }
        
        public static STCellRef parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(reader, STCellRef.type, xmlOptions);
        }
        
        public static STCellRef parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STCellRef.type, null);
        }
        
        public static STCellRef parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STCellRef.type, xmlOptions);
        }
        
        public static STCellRef parse(final Node node) throws XmlException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(node, STCellRef.type, null);
        }
        
        public static STCellRef parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(node, STCellRef.type, xmlOptions);
        }
        
        @Deprecated
        public static STCellRef parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STCellRef.type, null);
        }
        
        @Deprecated
        public static STCellRef parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STCellRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STCellRef.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STCellRef.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STCellRef.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
