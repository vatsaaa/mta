// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlDouble;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTColor extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTColor.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcolord2c2type");
    
    boolean getAuto();
    
    XmlBoolean xgetAuto();
    
    boolean isSetAuto();
    
    void setAuto(final boolean p0);
    
    void xsetAuto(final XmlBoolean p0);
    
    void unsetAuto();
    
    long getIndexed();
    
    XmlUnsignedInt xgetIndexed();
    
    boolean isSetIndexed();
    
    void setIndexed(final long p0);
    
    void xsetIndexed(final XmlUnsignedInt p0);
    
    void unsetIndexed();
    
    byte[] getRgb();
    
    STUnsignedIntHex xgetRgb();
    
    boolean isSetRgb();
    
    void setRgb(final byte[] p0);
    
    void xsetRgb(final STUnsignedIntHex p0);
    
    void unsetRgb();
    
    long getTheme();
    
    XmlUnsignedInt xgetTheme();
    
    boolean isSetTheme();
    
    void setTheme(final long p0);
    
    void xsetTheme(final XmlUnsignedInt p0);
    
    void unsetTheme();
    
    double getTint();
    
    XmlDouble xgetTint();
    
    boolean isSetTint();
    
    void setTint(final double p0);
    
    void xsetTint(final XmlDouble p0);
    
    void unsetTint();
    
    public static final class Factory
    {
        public static CTColor newInstance() {
            return (CTColor)XmlBeans.getContextTypeLoader().newInstance(CTColor.type, null);
        }
        
        public static CTColor newInstance(final XmlOptions xmlOptions) {
            return (CTColor)XmlBeans.getContextTypeLoader().newInstance(CTColor.type, xmlOptions);
        }
        
        public static CTColor parse(final String s) throws XmlException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(s, CTColor.type, null);
        }
        
        public static CTColor parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(s, CTColor.type, xmlOptions);
        }
        
        public static CTColor parse(final File file) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(file, CTColor.type, null);
        }
        
        public static CTColor parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(file, CTColor.type, xmlOptions);
        }
        
        public static CTColor parse(final URL url) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(url, CTColor.type, null);
        }
        
        public static CTColor parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(url, CTColor.type, xmlOptions);
        }
        
        public static CTColor parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(inputStream, CTColor.type, null);
        }
        
        public static CTColor parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(inputStream, CTColor.type, xmlOptions);
        }
        
        public static CTColor parse(final Reader reader) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(reader, CTColor.type, null);
        }
        
        public static CTColor parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(reader, CTColor.type, xmlOptions);
        }
        
        public static CTColor parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTColor.type, null);
        }
        
        public static CTColor parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTColor.type, xmlOptions);
        }
        
        public static CTColor parse(final Node node) throws XmlException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(node, CTColor.type, null);
        }
        
        public static CTColor parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(node, CTColor.type, xmlOptions);
        }
        
        @Deprecated
        public static CTColor parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTColor.type, null);
        }
        
        @Deprecated
        public static CTColor parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTColor)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTColor.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTColor.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTColor.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
