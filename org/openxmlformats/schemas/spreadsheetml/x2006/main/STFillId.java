// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STFillId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STFillId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stfillida097type");
    
    public static final class Factory
    {
        public static STFillId newValue(final Object o) {
            return (STFillId)STFillId.type.newValue(o);
        }
        
        public static STFillId newInstance() {
            return (STFillId)XmlBeans.getContextTypeLoader().newInstance(STFillId.type, null);
        }
        
        public static STFillId newInstance(final XmlOptions xmlOptions) {
            return (STFillId)XmlBeans.getContextTypeLoader().newInstance(STFillId.type, xmlOptions);
        }
        
        public static STFillId parse(final String s) throws XmlException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(s, STFillId.type, null);
        }
        
        public static STFillId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(s, STFillId.type, xmlOptions);
        }
        
        public static STFillId parse(final File file) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(file, STFillId.type, null);
        }
        
        public static STFillId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(file, STFillId.type, xmlOptions);
        }
        
        public static STFillId parse(final URL url) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(url, STFillId.type, null);
        }
        
        public static STFillId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(url, STFillId.type, xmlOptions);
        }
        
        public static STFillId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(inputStream, STFillId.type, null);
        }
        
        public static STFillId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(inputStream, STFillId.type, xmlOptions);
        }
        
        public static STFillId parse(final Reader reader) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(reader, STFillId.type, null);
        }
        
        public static STFillId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(reader, STFillId.type, xmlOptions);
        }
        
        public static STFillId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFillId.type, null);
        }
        
        public static STFillId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFillId.type, xmlOptions);
        }
        
        public static STFillId parse(final Node node) throws XmlException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(node, STFillId.type, null);
        }
        
        public static STFillId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(node, STFillId.type, xmlOptions);
        }
        
        @Deprecated
        public static STFillId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFillId.type, null);
        }
        
        @Deprecated
        public static STFillId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STFillId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFillId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFillId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFillId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
