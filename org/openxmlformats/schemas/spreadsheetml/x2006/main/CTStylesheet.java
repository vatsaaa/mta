// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTStylesheet extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTStylesheet.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctstylesheet4257type");
    
    CTNumFmts getNumFmts();
    
    boolean isSetNumFmts();
    
    void setNumFmts(final CTNumFmts p0);
    
    CTNumFmts addNewNumFmts();
    
    void unsetNumFmts();
    
    CTFonts getFonts();
    
    boolean isSetFonts();
    
    void setFonts(final CTFonts p0);
    
    CTFonts addNewFonts();
    
    void unsetFonts();
    
    CTFills getFills();
    
    boolean isSetFills();
    
    void setFills(final CTFills p0);
    
    CTFills addNewFills();
    
    void unsetFills();
    
    CTBorders getBorders();
    
    boolean isSetBorders();
    
    void setBorders(final CTBorders p0);
    
    CTBorders addNewBorders();
    
    void unsetBorders();
    
    CTCellStyleXfs getCellStyleXfs();
    
    boolean isSetCellStyleXfs();
    
    void setCellStyleXfs(final CTCellStyleXfs p0);
    
    CTCellStyleXfs addNewCellStyleXfs();
    
    void unsetCellStyleXfs();
    
    CTCellXfs getCellXfs();
    
    boolean isSetCellXfs();
    
    void setCellXfs(final CTCellXfs p0);
    
    CTCellXfs addNewCellXfs();
    
    void unsetCellXfs();
    
    CTCellStyles getCellStyles();
    
    boolean isSetCellStyles();
    
    void setCellStyles(final CTCellStyles p0);
    
    CTCellStyles addNewCellStyles();
    
    void unsetCellStyles();
    
    CTDxfs getDxfs();
    
    boolean isSetDxfs();
    
    void setDxfs(final CTDxfs p0);
    
    CTDxfs addNewDxfs();
    
    void unsetDxfs();
    
    CTTableStyles getTableStyles();
    
    boolean isSetTableStyles();
    
    void setTableStyles(final CTTableStyles p0);
    
    CTTableStyles addNewTableStyles();
    
    void unsetTableStyles();
    
    CTColors getColors();
    
    boolean isSetColors();
    
    void setColors(final CTColors p0);
    
    CTColors addNewColors();
    
    void unsetColors();
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTStylesheet newInstance() {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().newInstance(CTStylesheet.type, null);
        }
        
        public static CTStylesheet newInstance(final XmlOptions xmlOptions) {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().newInstance(CTStylesheet.type, xmlOptions);
        }
        
        public static CTStylesheet parse(final String s) throws XmlException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(s, CTStylesheet.type, null);
        }
        
        public static CTStylesheet parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(s, CTStylesheet.type, xmlOptions);
        }
        
        public static CTStylesheet parse(final File file) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(file, CTStylesheet.type, null);
        }
        
        public static CTStylesheet parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(file, CTStylesheet.type, xmlOptions);
        }
        
        public static CTStylesheet parse(final URL url) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(url, CTStylesheet.type, null);
        }
        
        public static CTStylesheet parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(url, CTStylesheet.type, xmlOptions);
        }
        
        public static CTStylesheet parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(inputStream, CTStylesheet.type, null);
        }
        
        public static CTStylesheet parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(inputStream, CTStylesheet.type, xmlOptions);
        }
        
        public static CTStylesheet parse(final Reader reader) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(reader, CTStylesheet.type, null);
        }
        
        public static CTStylesheet parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(reader, CTStylesheet.type, xmlOptions);
        }
        
        public static CTStylesheet parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStylesheet.type, null);
        }
        
        public static CTStylesheet parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStylesheet.type, xmlOptions);
        }
        
        public static CTStylesheet parse(final Node node) throws XmlException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(node, CTStylesheet.type, null);
        }
        
        public static CTStylesheet parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(node, CTStylesheet.type, xmlOptions);
        }
        
        @Deprecated
        public static CTStylesheet parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStylesheet.type, null);
        }
        
        @Deprecated
        public static CTStylesheet parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTStylesheet)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStylesheet.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStylesheet.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStylesheet.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
