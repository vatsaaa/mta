// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTFonts extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTFonts.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfonts6623type");
    
    List<CTFont> getFontList();
    
    @Deprecated
    CTFont[] getFontArray();
    
    CTFont getFontArray(final int p0);
    
    int sizeOfFontArray();
    
    void setFontArray(final CTFont[] p0);
    
    void setFontArray(final int p0, final CTFont p1);
    
    CTFont insertNewFont(final int p0);
    
    CTFont addNewFont();
    
    void removeFont(final int p0);
    
    long getCount();
    
    XmlUnsignedInt xgetCount();
    
    boolean isSetCount();
    
    void setCount(final long p0);
    
    void xsetCount(final XmlUnsignedInt p0);
    
    void unsetCount();
    
    public static final class Factory
    {
        public static CTFonts newInstance() {
            return (CTFonts)XmlBeans.getContextTypeLoader().newInstance(CTFonts.type, null);
        }
        
        public static CTFonts newInstance(final XmlOptions xmlOptions) {
            return (CTFonts)XmlBeans.getContextTypeLoader().newInstance(CTFonts.type, xmlOptions);
        }
        
        public static CTFonts parse(final String s) throws XmlException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(s, CTFonts.type, null);
        }
        
        public static CTFonts parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(s, CTFonts.type, xmlOptions);
        }
        
        public static CTFonts parse(final File file) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(file, CTFonts.type, null);
        }
        
        public static CTFonts parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(file, CTFonts.type, xmlOptions);
        }
        
        public static CTFonts parse(final URL url) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(url, CTFonts.type, null);
        }
        
        public static CTFonts parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(url, CTFonts.type, xmlOptions);
        }
        
        public static CTFonts parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(inputStream, CTFonts.type, null);
        }
        
        public static CTFonts parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(inputStream, CTFonts.type, xmlOptions);
        }
        
        public static CTFonts parse(final Reader reader) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(reader, CTFonts.type, null);
        }
        
        public static CTFonts parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(reader, CTFonts.type, xmlOptions);
        }
        
        public static CTFonts parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFonts.type, null);
        }
        
        public static CTFonts parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFonts.type, xmlOptions);
        }
        
        public static CTFonts parse(final Node node) throws XmlException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(node, CTFonts.type, null);
        }
        
        public static CTFonts parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(node, CTFonts.type, xmlOptions);
        }
        
        @Deprecated
        public static CTFonts parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFonts.type, null);
        }
        
        @Deprecated
        public static CTFonts parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTFonts)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFonts.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFonts.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFonts.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
