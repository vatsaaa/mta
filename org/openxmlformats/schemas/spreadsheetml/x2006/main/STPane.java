// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STPane extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPane.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpane2ac1type");
    public static final Enum BOTTOM_RIGHT = Enum.forString("bottomRight");
    public static final Enum TOP_RIGHT = Enum.forString("topRight");
    public static final Enum BOTTOM_LEFT = Enum.forString("bottomLeft");
    public static final Enum TOP_LEFT = Enum.forString("topLeft");
    public static final int INT_BOTTOM_RIGHT = 1;
    public static final int INT_TOP_RIGHT = 2;
    public static final int INT_BOTTOM_LEFT = 3;
    public static final int INT_TOP_LEFT = 4;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STPane newValue(final Object o) {
            return (STPane)STPane.type.newValue(o);
        }
        
        public static STPane newInstance() {
            return (STPane)XmlBeans.getContextTypeLoader().newInstance(STPane.type, null);
        }
        
        public static STPane newInstance(final XmlOptions xmlOptions) {
            return (STPane)XmlBeans.getContextTypeLoader().newInstance(STPane.type, xmlOptions);
        }
        
        public static STPane parse(final String s) throws XmlException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(s, STPane.type, null);
        }
        
        public static STPane parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(s, STPane.type, xmlOptions);
        }
        
        public static STPane parse(final File file) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(file, STPane.type, null);
        }
        
        public static STPane parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(file, STPane.type, xmlOptions);
        }
        
        public static STPane parse(final URL url) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(url, STPane.type, null);
        }
        
        public static STPane parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(url, STPane.type, xmlOptions);
        }
        
        public static STPane parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(inputStream, STPane.type, null);
        }
        
        public static STPane parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(inputStream, STPane.type, xmlOptions);
        }
        
        public static STPane parse(final Reader reader) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(reader, STPane.type, null);
        }
        
        public static STPane parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(reader, STPane.type, xmlOptions);
        }
        
        public static STPane parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPane.type, null);
        }
        
        public static STPane parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPane.type, xmlOptions);
        }
        
        public static STPane parse(final Node node) throws XmlException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(node, STPane.type, null);
        }
        
        public static STPane parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(node, STPane.type, xmlOptions);
        }
        
        @Deprecated
        public static STPane parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPane.type, null);
        }
        
        @Deprecated
        public static STPane parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPane)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPane.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPane.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPane.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_BOTTOM_RIGHT = 1;
        static final int INT_TOP_RIGHT = 2;
        static final int INT_BOTTOM_LEFT = 3;
        static final int INT_TOP_LEFT = 4;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("bottomRight", 1), new Enum("topRight", 2), new Enum("bottomLeft", 3), new Enum("topLeft", 4) });
        }
    }
}
