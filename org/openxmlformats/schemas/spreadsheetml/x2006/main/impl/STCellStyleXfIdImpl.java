// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCellStyleXfId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STCellStyleXfIdImpl extends JavaLongHolderEx implements STCellStyleXfId
{
    public STCellStyleXfIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCellStyleXfIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
