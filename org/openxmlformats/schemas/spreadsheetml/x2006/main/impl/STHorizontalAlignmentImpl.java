// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STHorizontalAlignment;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STHorizontalAlignmentImpl extends JavaStringEnumerationHolderEx implements STHorizontalAlignment
{
    public STHorizontalAlignmentImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STHorizontalAlignmentImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
