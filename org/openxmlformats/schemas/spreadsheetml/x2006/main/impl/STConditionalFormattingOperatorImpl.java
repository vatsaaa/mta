// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STConditionalFormattingOperator;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STConditionalFormattingOperatorImpl extends JavaStringEnumerationHolderEx implements STConditionalFormattingOperator
{
    public STConditionalFormattingOperatorImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STConditionalFormattingOperatorImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
