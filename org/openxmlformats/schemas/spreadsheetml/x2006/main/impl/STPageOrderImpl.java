// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPageOrder;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STPageOrderImpl extends JavaStringEnumerationHolderEx implements STPageOrder
{
    public STPageOrderImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPageOrderImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
