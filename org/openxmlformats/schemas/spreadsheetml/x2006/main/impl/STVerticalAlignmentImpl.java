// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STVerticalAlignment;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STVerticalAlignmentImpl extends JavaStringEnumerationHolderEx implements STVerticalAlignment
{
    public STVerticalAlignmentImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STVerticalAlignmentImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
