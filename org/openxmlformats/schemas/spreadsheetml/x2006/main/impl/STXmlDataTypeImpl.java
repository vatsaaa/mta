// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STXmlDataType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STXmlDataTypeImpl extends JavaStringEnumerationHolderEx implements STXmlDataType
{
    public STXmlDataTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STXmlDataTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
