// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STSheetState;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STSheetStateImpl extends JavaStringEnumerationHolderEx implements STSheetState
{
    public STSheetStateImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSheetStateImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
