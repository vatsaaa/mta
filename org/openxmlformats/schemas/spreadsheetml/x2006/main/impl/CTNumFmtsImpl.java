// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTNumFmt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTNumFmts;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTNumFmtsImpl extends XmlComplexContentImpl implements CTNumFmts
{
    private static final QName NUMFMT$0;
    private static final QName COUNT$2;
    
    public CTNumFmtsImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTNumFmt> getNumFmtList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTNumFmt>)new CTNumFmtsImpl.NumFmtList(this);
        }
    }
    
    public CTNumFmt[] getNumFmtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTNumFmtsImpl.NUMFMT$0, list);
            final CTNumFmt[] array = new CTNumFmt[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTNumFmt getNumFmtArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNumFmt ctNumFmt = (CTNumFmt)this.get_store().find_element_user(CTNumFmtsImpl.NUMFMT$0, n);
            if (ctNumFmt == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctNumFmt;
        }
    }
    
    public int sizeOfNumFmtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTNumFmtsImpl.NUMFMT$0);
        }
    }
    
    public void setNumFmtArray(final CTNumFmt[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTNumFmtsImpl.NUMFMT$0);
        }
    }
    
    public void setNumFmtArray(final int n, final CTNumFmt ctNumFmt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNumFmt ctNumFmt2 = (CTNumFmt)this.get_store().find_element_user(CTNumFmtsImpl.NUMFMT$0, n);
            if (ctNumFmt2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctNumFmt2.set(ctNumFmt);
        }
    }
    
    public CTNumFmt insertNewNumFmt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNumFmt)this.get_store().insert_element_user(CTNumFmtsImpl.NUMFMT$0, n);
        }
    }
    
    public CTNumFmt addNewNumFmt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNumFmt)this.get_store().add_element_user(CTNumFmtsImpl.NUMFMT$0);
        }
    }
    
    public void removeNumFmt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTNumFmtsImpl.NUMFMT$0, n);
        }
    }
    
    public long getCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTNumFmtsImpl.COUNT$2);
            if (simpleValue == null) {
                return 0L;
            }
            return simpleValue.getLongValue();
        }
    }
    
    public XmlUnsignedInt xgetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedInt)this.get_store().find_attribute_user(CTNumFmtsImpl.COUNT$2);
        }
    }
    
    public boolean isSetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTNumFmtsImpl.COUNT$2) != null;
        }
    }
    
    public void setCount(final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTNumFmtsImpl.COUNT$2);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTNumFmtsImpl.COUNT$2);
            }
            simpleValue.setLongValue(longValue);
        }
    }
    
    public void xsetCount(final XmlUnsignedInt xmlUnsignedInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlUnsignedInt xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().find_attribute_user(CTNumFmtsImpl.COUNT$2);
            if (xmlUnsignedInt2 == null) {
                xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().add_attribute_user(CTNumFmtsImpl.COUNT$2);
            }
            xmlUnsignedInt2.set(xmlUnsignedInt);
        }
    }
    
    public void unsetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTNumFmtsImpl.COUNT$2);
        }
    }
    
    static {
        NUMFMT$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "numFmt");
        COUNT$2 = new QName("", "count");
    }
}
