// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTComments;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CommentsDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CommentsDocumentImpl extends XmlComplexContentImpl implements CommentsDocument
{
    private static final QName COMMENTS$0;
    
    public CommentsDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTComments getComments() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTComments ctComments = (CTComments)this.get_store().find_element_user(CommentsDocumentImpl.COMMENTS$0, 0);
            if (ctComments == null) {
                return null;
            }
            return ctComments;
        }
    }
    
    public void setComments(final CTComments ctComments) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTComments ctComments2 = (CTComments)this.get_store().find_element_user(CommentsDocumentImpl.COMMENTS$0, 0);
            if (ctComments2 == null) {
                ctComments2 = (CTComments)this.get_store().add_element_user(CommentsDocumentImpl.COMMENTS$0);
            }
            ctComments2.set(ctComments);
        }
    }
    
    public CTComments addNewComments() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTComments)this.get_store().add_element_user(CommentsDocumentImpl.COMMENTS$0);
        }
    }
    
    static {
        COMMENTS$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "comments");
    }
}
