// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.TableDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class TableDocumentImpl extends XmlComplexContentImpl implements TableDocument
{
    private static final QName TABLE$0;
    
    public TableDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTTable getTable() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTable ctTable = (CTTable)this.get_store().find_element_user(TableDocumentImpl.TABLE$0, 0);
            if (ctTable == null) {
                return null;
            }
            return ctTable;
        }
    }
    
    public void setTable(final CTTable ctTable) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTable ctTable2 = (CTTable)this.get_store().find_element_user(TableDocumentImpl.TABLE$0, 0);
            if (ctTable2 == null) {
                ctTable2 = (CTTable)this.get_store().add_element_user(TableDocumentImpl.TABLE$0);
            }
            ctTable2.set(ctTable);
        }
    }
    
    public CTTable addNewTable() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTable)this.get_store().add_element_user(TableDocumentImpl.TABLE$0);
        }
    }
    
    static {
        TABLE$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "table");
    }
}
