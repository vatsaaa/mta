// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFontScheme;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTVerticalAlignFontProperty;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTUnderlineProperty;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFontSize;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBooleanProperty;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTIntProperty;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFontName;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRPrElt;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTRPrEltImpl extends XmlComplexContentImpl implements CTRPrElt
{
    private static final QName RFONT$0;
    private static final QName CHARSET$2;
    private static final QName FAMILY$4;
    private static final QName B$6;
    private static final QName I$8;
    private static final QName STRIKE$10;
    private static final QName OUTLINE$12;
    private static final QName SHADOW$14;
    private static final QName CONDENSE$16;
    private static final QName EXTEND$18;
    private static final QName COLOR$20;
    private static final QName SZ$22;
    private static final QName U$24;
    private static final QName VERTALIGN$26;
    private static final QName SCHEME$28;
    
    public CTRPrEltImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTFontName> getRFontList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFontName>)new CTRPrEltImpl.RFontList(this);
        }
    }
    
    public CTFontName[] getRFontArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.RFONT$0, list);
            final CTFontName[] array = new CTFontName[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFontName getRFontArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontName ctFontName = (CTFontName)this.get_store().find_element_user(CTRPrEltImpl.RFONT$0, n);
            if (ctFontName == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFontName;
        }
    }
    
    public int sizeOfRFontArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.RFONT$0);
        }
    }
    
    public void setRFontArray(final CTFontName[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.RFONT$0);
        }
    }
    
    public void setRFontArray(final int n, final CTFontName ctFontName) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontName ctFontName2 = (CTFontName)this.get_store().find_element_user(CTRPrEltImpl.RFONT$0, n);
            if (ctFontName2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFontName2.set(ctFontName);
        }
    }
    
    public CTFontName insertNewRFont(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontName)this.get_store().insert_element_user(CTRPrEltImpl.RFONT$0, n);
        }
    }
    
    public CTFontName addNewRFont() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontName)this.get_store().add_element_user(CTRPrEltImpl.RFONT$0);
        }
    }
    
    public void removeRFont(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.RFONT$0, n);
        }
    }
    
    public List<CTIntProperty> getCharsetList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTIntProperty>)new CTRPrEltImpl.CharsetList(this);
        }
    }
    
    public CTIntProperty[] getCharsetArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.CHARSET$2, list);
            final CTIntProperty[] array = new CTIntProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTIntProperty getCharsetArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty = (CTIntProperty)this.get_store().find_element_user(CTRPrEltImpl.CHARSET$2, n);
            if (ctIntProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctIntProperty;
        }
    }
    
    public int sizeOfCharsetArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.CHARSET$2);
        }
    }
    
    public void setCharsetArray(final CTIntProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.CHARSET$2);
        }
    }
    
    public void setCharsetArray(final int n, final CTIntProperty ctIntProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty2 = (CTIntProperty)this.get_store().find_element_user(CTRPrEltImpl.CHARSET$2, n);
            if (ctIntProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctIntProperty2.set(ctIntProperty);
        }
    }
    
    public CTIntProperty insertNewCharset(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().insert_element_user(CTRPrEltImpl.CHARSET$2, n);
        }
    }
    
    public CTIntProperty addNewCharset() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().add_element_user(CTRPrEltImpl.CHARSET$2);
        }
    }
    
    public void removeCharset(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.CHARSET$2, n);
        }
    }
    
    public List<CTIntProperty> getFamilyList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTIntProperty>)new CTRPrEltImpl.FamilyList(this);
        }
    }
    
    public CTIntProperty[] getFamilyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.FAMILY$4, list);
            final CTIntProperty[] array = new CTIntProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTIntProperty getFamilyArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty = (CTIntProperty)this.get_store().find_element_user(CTRPrEltImpl.FAMILY$4, n);
            if (ctIntProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctIntProperty;
        }
    }
    
    public int sizeOfFamilyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.FAMILY$4);
        }
    }
    
    public void setFamilyArray(final CTIntProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.FAMILY$4);
        }
    }
    
    public void setFamilyArray(final int n, final CTIntProperty ctIntProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty2 = (CTIntProperty)this.get_store().find_element_user(CTRPrEltImpl.FAMILY$4, n);
            if (ctIntProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctIntProperty2.set(ctIntProperty);
        }
    }
    
    public CTIntProperty insertNewFamily(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().insert_element_user(CTRPrEltImpl.FAMILY$4, n);
        }
    }
    
    public CTIntProperty addNewFamily() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().add_element_user(CTRPrEltImpl.FAMILY$4);
        }
    }
    
    public void removeFamily(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.FAMILY$4, n);
        }
    }
    
    public List<CTBooleanProperty> getBList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTRPrEltImpl.BList(this);
        }
    }
    
    public CTBooleanProperty[] getBArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.B$6, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getBArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.B$6, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfBArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.B$6);
        }
    }
    
    public void setBArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.B$6);
        }
    }
    
    public void setBArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.B$6, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewB(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTRPrEltImpl.B$6, n);
        }
    }
    
    public CTBooleanProperty addNewB() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTRPrEltImpl.B$6);
        }
    }
    
    public void removeB(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.B$6, n);
        }
    }
    
    public List<CTBooleanProperty> getIList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTRPrEltImpl.IList(this);
        }
    }
    
    public CTBooleanProperty[] getIArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.I$8, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getIArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.I$8, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfIArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.I$8);
        }
    }
    
    public void setIArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.I$8);
        }
    }
    
    public void setIArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.I$8, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewI(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTRPrEltImpl.I$8, n);
        }
    }
    
    public CTBooleanProperty addNewI() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTRPrEltImpl.I$8);
        }
    }
    
    public void removeI(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.I$8, n);
        }
    }
    
    public List<CTBooleanProperty> getStrikeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTRPrEltImpl.StrikeList(this);
        }
    }
    
    public CTBooleanProperty[] getStrikeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.STRIKE$10, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getStrikeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.STRIKE$10, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfStrikeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.STRIKE$10);
        }
    }
    
    public void setStrikeArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.STRIKE$10);
        }
    }
    
    public void setStrikeArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.STRIKE$10, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewStrike(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTRPrEltImpl.STRIKE$10, n);
        }
    }
    
    public CTBooleanProperty addNewStrike() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTRPrEltImpl.STRIKE$10);
        }
    }
    
    public void removeStrike(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.STRIKE$10, n);
        }
    }
    
    public List<CTBooleanProperty> getOutlineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTRPrEltImpl.OutlineList(this);
        }
    }
    
    public CTBooleanProperty[] getOutlineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.OUTLINE$12, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getOutlineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.OUTLINE$12, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfOutlineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.OUTLINE$12);
        }
    }
    
    public void setOutlineArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.OUTLINE$12);
        }
    }
    
    public void setOutlineArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.OUTLINE$12, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewOutline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTRPrEltImpl.OUTLINE$12, n);
        }
    }
    
    public CTBooleanProperty addNewOutline() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTRPrEltImpl.OUTLINE$12);
        }
    }
    
    public void removeOutline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.OUTLINE$12, n);
        }
    }
    
    public List<CTBooleanProperty> getShadowList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTRPrEltImpl.ShadowList(this);
        }
    }
    
    public CTBooleanProperty[] getShadowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.SHADOW$14, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getShadowArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.SHADOW$14, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfShadowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.SHADOW$14);
        }
    }
    
    public void setShadowArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.SHADOW$14);
        }
    }
    
    public void setShadowArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.SHADOW$14, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewShadow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTRPrEltImpl.SHADOW$14, n);
        }
    }
    
    public CTBooleanProperty addNewShadow() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTRPrEltImpl.SHADOW$14);
        }
    }
    
    public void removeShadow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.SHADOW$14, n);
        }
    }
    
    public List<CTBooleanProperty> getCondenseList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTRPrEltImpl.CondenseList(this);
        }
    }
    
    public CTBooleanProperty[] getCondenseArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.CONDENSE$16, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getCondenseArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.CONDENSE$16, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfCondenseArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.CONDENSE$16);
        }
    }
    
    public void setCondenseArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.CONDENSE$16);
        }
    }
    
    public void setCondenseArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.CONDENSE$16, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewCondense(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTRPrEltImpl.CONDENSE$16, n);
        }
    }
    
    public CTBooleanProperty addNewCondense() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTRPrEltImpl.CONDENSE$16);
        }
    }
    
    public void removeCondense(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.CONDENSE$16, n);
        }
    }
    
    public List<CTBooleanProperty> getExtendList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTRPrEltImpl.ExtendList(this);
        }
    }
    
    public CTBooleanProperty[] getExtendArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.EXTEND$18, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getExtendArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.EXTEND$18, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfExtendArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.EXTEND$18);
        }
    }
    
    public void setExtendArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.EXTEND$18);
        }
    }
    
    public void setExtendArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTRPrEltImpl.EXTEND$18, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewExtend(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTRPrEltImpl.EXTEND$18, n);
        }
    }
    
    public CTBooleanProperty addNewExtend() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTRPrEltImpl.EXTEND$18);
        }
    }
    
    public void removeExtend(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.EXTEND$18, n);
        }
    }
    
    public List<CTColor> getColorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTColor>)new CTRPrEltImpl.ColorList(this);
        }
    }
    
    public CTColor[] getColorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.COLOR$20, list);
            final CTColor[] array = new CTColor[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTColor getColorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColor ctColor = (CTColor)this.get_store().find_element_user(CTRPrEltImpl.COLOR$20, n);
            if (ctColor == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctColor;
        }
    }
    
    public int sizeOfColorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.COLOR$20);
        }
    }
    
    public void setColorArray(final CTColor[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.COLOR$20);
        }
    }
    
    public void setColorArray(final int n, final CTColor ctColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColor ctColor2 = (CTColor)this.get_store().find_element_user(CTRPrEltImpl.COLOR$20, n);
            if (ctColor2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctColor2.set(ctColor);
        }
    }
    
    public CTColor insertNewColor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColor)this.get_store().insert_element_user(CTRPrEltImpl.COLOR$20, n);
        }
    }
    
    public CTColor addNewColor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColor)this.get_store().add_element_user(CTRPrEltImpl.COLOR$20);
        }
    }
    
    public void removeColor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.COLOR$20, n);
        }
    }
    
    public List<CTFontSize> getSzList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFontSize>)new CTRPrEltImpl.SzList(this);
        }
    }
    
    public CTFontSize[] getSzArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.SZ$22, list);
            final CTFontSize[] array = new CTFontSize[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFontSize getSzArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontSize ctFontSize = (CTFontSize)this.get_store().find_element_user(CTRPrEltImpl.SZ$22, n);
            if (ctFontSize == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFontSize;
        }
    }
    
    public int sizeOfSzArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.SZ$22);
        }
    }
    
    public void setSzArray(final CTFontSize[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.SZ$22);
        }
    }
    
    public void setSzArray(final int n, final CTFontSize ctFontSize) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontSize ctFontSize2 = (CTFontSize)this.get_store().find_element_user(CTRPrEltImpl.SZ$22, n);
            if (ctFontSize2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFontSize2.set(ctFontSize);
        }
    }
    
    public CTFontSize insertNewSz(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontSize)this.get_store().insert_element_user(CTRPrEltImpl.SZ$22, n);
        }
    }
    
    public CTFontSize addNewSz() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontSize)this.get_store().add_element_user(CTRPrEltImpl.SZ$22);
        }
    }
    
    public void removeSz(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.SZ$22, n);
        }
    }
    
    public List<CTUnderlineProperty> getUList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTUnderlineProperty>)new CTRPrEltImpl.UList(this);
        }
    }
    
    public CTUnderlineProperty[] getUArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.U$24, list);
            final CTUnderlineProperty[] array = new CTUnderlineProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTUnderlineProperty getUArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTUnderlineProperty ctUnderlineProperty = (CTUnderlineProperty)this.get_store().find_element_user(CTRPrEltImpl.U$24, n);
            if (ctUnderlineProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctUnderlineProperty;
        }
    }
    
    public int sizeOfUArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.U$24);
        }
    }
    
    public void setUArray(final CTUnderlineProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.U$24);
        }
    }
    
    public void setUArray(final int n, final CTUnderlineProperty ctUnderlineProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTUnderlineProperty ctUnderlineProperty2 = (CTUnderlineProperty)this.get_store().find_element_user(CTRPrEltImpl.U$24, n);
            if (ctUnderlineProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctUnderlineProperty2.set(ctUnderlineProperty);
        }
    }
    
    public CTUnderlineProperty insertNewU(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTUnderlineProperty)this.get_store().insert_element_user(CTRPrEltImpl.U$24, n);
        }
    }
    
    public CTUnderlineProperty addNewU() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTUnderlineProperty)this.get_store().add_element_user(CTRPrEltImpl.U$24);
        }
    }
    
    public void removeU(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.U$24, n);
        }
    }
    
    public List<CTVerticalAlignFontProperty> getVertAlignList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTVerticalAlignFontProperty>)new CTRPrEltImpl.VertAlignList(this);
        }
    }
    
    public CTVerticalAlignFontProperty[] getVertAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.VERTALIGN$26, list);
            final CTVerticalAlignFontProperty[] array = new CTVerticalAlignFontProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTVerticalAlignFontProperty getVertAlignArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTVerticalAlignFontProperty ctVerticalAlignFontProperty = (CTVerticalAlignFontProperty)this.get_store().find_element_user(CTRPrEltImpl.VERTALIGN$26, n);
            if (ctVerticalAlignFontProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctVerticalAlignFontProperty;
        }
    }
    
    public int sizeOfVertAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.VERTALIGN$26);
        }
    }
    
    public void setVertAlignArray(final CTVerticalAlignFontProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.VERTALIGN$26);
        }
    }
    
    public void setVertAlignArray(final int n, final CTVerticalAlignFontProperty ctVerticalAlignFontProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTVerticalAlignFontProperty ctVerticalAlignFontProperty2 = (CTVerticalAlignFontProperty)this.get_store().find_element_user(CTRPrEltImpl.VERTALIGN$26, n);
            if (ctVerticalAlignFontProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctVerticalAlignFontProperty2.set(ctVerticalAlignFontProperty);
        }
    }
    
    public CTVerticalAlignFontProperty insertNewVertAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTVerticalAlignFontProperty)this.get_store().insert_element_user(CTRPrEltImpl.VERTALIGN$26, n);
        }
    }
    
    public CTVerticalAlignFontProperty addNewVertAlign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTVerticalAlignFontProperty)this.get_store().add_element_user(CTRPrEltImpl.VERTALIGN$26);
        }
    }
    
    public void removeVertAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.VERTALIGN$26, n);
        }
    }
    
    public List<CTFontScheme> getSchemeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFontScheme>)new CTRPrEltImpl.SchemeList(this);
        }
    }
    
    public CTFontScheme[] getSchemeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRPrEltImpl.SCHEME$28, list);
            final CTFontScheme[] array = new CTFontScheme[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFontScheme getSchemeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontScheme ctFontScheme = (CTFontScheme)this.get_store().find_element_user(CTRPrEltImpl.SCHEME$28, n);
            if (ctFontScheme == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFontScheme;
        }
    }
    
    public int sizeOfSchemeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRPrEltImpl.SCHEME$28);
        }
    }
    
    public void setSchemeArray(final CTFontScheme[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRPrEltImpl.SCHEME$28);
        }
    }
    
    public void setSchemeArray(final int n, final CTFontScheme ctFontScheme) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontScheme ctFontScheme2 = (CTFontScheme)this.get_store().find_element_user(CTRPrEltImpl.SCHEME$28, n);
            if (ctFontScheme2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFontScheme2.set(ctFontScheme);
        }
    }
    
    public CTFontScheme insertNewScheme(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontScheme)this.get_store().insert_element_user(CTRPrEltImpl.SCHEME$28, n);
        }
    }
    
    public CTFontScheme addNewScheme() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontScheme)this.get_store().add_element_user(CTRPrEltImpl.SCHEME$28);
        }
    }
    
    public void removeScheme(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRPrEltImpl.SCHEME$28, n);
        }
    }
    
    static {
        RFONT$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "rFont");
        CHARSET$2 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "charset");
        FAMILY$4 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "family");
        B$6 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "b");
        I$8 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "i");
        STRIKE$10 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "strike");
        OUTLINE$12 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "outline");
        SHADOW$14 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "shadow");
        CONDENSE$16 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "condense");
        EXTEND$18 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "extend");
        COLOR$20 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "color");
        SZ$22 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sz");
        U$24 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "u");
        VERTALIGN$26 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "vertAlign");
        SCHEME$28 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "scheme");
    }
}
