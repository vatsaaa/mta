// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STNumFmtId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STNumFmtIdImpl extends JavaLongHolderEx implements STNumFmtId
{
    public STNumFmtIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STNumFmtIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
