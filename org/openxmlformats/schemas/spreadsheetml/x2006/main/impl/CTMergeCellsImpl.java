// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTMergeCell;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTMergeCells;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTMergeCellsImpl extends XmlComplexContentImpl implements CTMergeCells
{
    private static final QName MERGECELL$0;
    private static final QName COUNT$2;
    
    public CTMergeCellsImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTMergeCell> getMergeCellList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMergeCell>)new CTMergeCellsImpl.MergeCellList(this);
        }
    }
    
    public CTMergeCell[] getMergeCellArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTMergeCellsImpl.MERGECELL$0, list);
            final CTMergeCell[] array = new CTMergeCell[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMergeCell getMergeCellArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMergeCell ctMergeCell = (CTMergeCell)this.get_store().find_element_user(CTMergeCellsImpl.MERGECELL$0, n);
            if (ctMergeCell == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMergeCell;
        }
    }
    
    public int sizeOfMergeCellArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTMergeCellsImpl.MERGECELL$0);
        }
    }
    
    public void setMergeCellArray(final CTMergeCell[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTMergeCellsImpl.MERGECELL$0);
        }
    }
    
    public void setMergeCellArray(final int n, final CTMergeCell ctMergeCell) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMergeCell ctMergeCell2 = (CTMergeCell)this.get_store().find_element_user(CTMergeCellsImpl.MERGECELL$0, n);
            if (ctMergeCell2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMergeCell2.set(ctMergeCell);
        }
    }
    
    public CTMergeCell insertNewMergeCell(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMergeCell)this.get_store().insert_element_user(CTMergeCellsImpl.MERGECELL$0, n);
        }
    }
    
    public CTMergeCell addNewMergeCell() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMergeCell)this.get_store().add_element_user(CTMergeCellsImpl.MERGECELL$0);
        }
    }
    
    public void removeMergeCell(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTMergeCellsImpl.MERGECELL$0, n);
        }
    }
    
    public long getCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTMergeCellsImpl.COUNT$2);
            if (simpleValue == null) {
                return 0L;
            }
            return simpleValue.getLongValue();
        }
    }
    
    public XmlUnsignedInt xgetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedInt)this.get_store().find_attribute_user(CTMergeCellsImpl.COUNT$2);
        }
    }
    
    public boolean isSetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTMergeCellsImpl.COUNT$2) != null;
        }
    }
    
    public void setCount(final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTMergeCellsImpl.COUNT$2);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTMergeCellsImpl.COUNT$2);
            }
            simpleValue.setLongValue(longValue);
        }
    }
    
    public void xsetCount(final XmlUnsignedInt xmlUnsignedInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlUnsignedInt xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().find_attribute_user(CTMergeCellsImpl.COUNT$2);
            if (xmlUnsignedInt2 == null) {
                xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().add_attribute_user(CTMergeCellsImpl.COUNT$2);
            }
            xmlUnsignedInt2.set(xmlUnsignedInt);
        }
    }
    
    public void unsetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTMergeCellsImpl.COUNT$2);
        }
    }
    
    static {
        MERGECELL$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "mergeCell");
        COUNT$2 = new QName("", "count");
    }
}
