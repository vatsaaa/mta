// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STSqref;
import org.apache.xmlbeans.impl.values.XmlListImpl;

public class STSqrefImpl extends XmlListImpl implements STSqref
{
    public STSqrefImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSqrefImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
