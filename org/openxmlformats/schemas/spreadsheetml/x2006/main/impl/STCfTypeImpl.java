// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCfType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STCfTypeImpl extends JavaStringEnumerationHolderEx implements STCfType
{
    public STCfTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCfTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
