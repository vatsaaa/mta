// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STFillId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STFillIdImpl extends JavaLongHolderEx implements STFillId
{
    public STFillIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STFillIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
