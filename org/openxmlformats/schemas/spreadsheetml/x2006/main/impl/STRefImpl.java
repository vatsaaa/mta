// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STRef;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STRefImpl extends JavaStringHolderEx implements STRef
{
    public STRefImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STRefImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
