// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBookView;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBookViews;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTBookViewsImpl extends XmlComplexContentImpl implements CTBookViews
{
    private static final QName WORKBOOKVIEW$0;
    
    public CTBookViewsImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTBookView> getWorkbookViewList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBookView>)new CTBookViewsImpl.WorkbookViewList(this);
        }
    }
    
    public CTBookView[] getWorkbookViewArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBookViewsImpl.WORKBOOKVIEW$0, list);
            final CTBookView[] array = new CTBookView[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBookView getWorkbookViewArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBookView ctBookView = (CTBookView)this.get_store().find_element_user(CTBookViewsImpl.WORKBOOKVIEW$0, n);
            if (ctBookView == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBookView;
        }
    }
    
    public int sizeOfWorkbookViewArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBookViewsImpl.WORKBOOKVIEW$0);
        }
    }
    
    public void setWorkbookViewArray(final CTBookView[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTBookViewsImpl.WORKBOOKVIEW$0);
        }
    }
    
    public void setWorkbookViewArray(final int n, final CTBookView ctBookView) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBookView ctBookView2 = (CTBookView)this.get_store().find_element_user(CTBookViewsImpl.WORKBOOKVIEW$0, n);
            if (ctBookView2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBookView2.set(ctBookView);
        }
    }
    
    public CTBookView insertNewWorkbookView(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBookView)this.get_store().insert_element_user(CTBookViewsImpl.WORKBOOKVIEW$0, n);
        }
    }
    
    public CTBookView addNewWorkbookView() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBookView)this.get_store().add_element_user(CTBookViewsImpl.WORKBOOKVIEW$0);
        }
    }
    
    public void removeWorkbookView(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBookViewsImpl.WORKBOOKVIEW$0, n);
        }
    }
    
    static {
        WORKBOOKVIEW$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "workbookView");
    }
}
