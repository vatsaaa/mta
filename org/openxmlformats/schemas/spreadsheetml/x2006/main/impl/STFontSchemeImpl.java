// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STFontScheme;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STFontSchemeImpl extends JavaStringEnumerationHolderEx implements STFontScheme
{
    public STFontSchemeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STFontSchemeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
