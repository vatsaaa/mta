// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCellComments;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STCellCommentsImpl extends JavaStringEnumerationHolderEx implements STCellComments
{
    public STCellCommentsImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCellCommentsImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
