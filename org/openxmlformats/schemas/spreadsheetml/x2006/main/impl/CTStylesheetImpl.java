// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTExtensionList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColors;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableStyles;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDxfs;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellStyles;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellXfs;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellStyleXfs;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorders;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFills;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFonts;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTNumFmts;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTStylesheet;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTStylesheetImpl extends XmlComplexContentImpl implements CTStylesheet
{
    private static final QName NUMFMTS$0;
    private static final QName FONTS$2;
    private static final QName FILLS$4;
    private static final QName BORDERS$6;
    private static final QName CELLSTYLEXFS$8;
    private static final QName CELLXFS$10;
    private static final QName CELLSTYLES$12;
    private static final QName DXFS$14;
    private static final QName TABLESTYLES$16;
    private static final QName COLORS$18;
    private static final QName EXTLST$20;
    
    public CTStylesheetImpl(final SchemaType type) {
        super(type);
    }
    
    public CTNumFmts getNumFmts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNumFmts ctNumFmts = (CTNumFmts)this.get_store().find_element_user(CTStylesheetImpl.NUMFMTS$0, 0);
            if (ctNumFmts == null) {
                return null;
            }
            return ctNumFmts;
        }
    }
    
    public boolean isSetNumFmts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.NUMFMTS$0) != 0;
        }
    }
    
    public void setNumFmts(final CTNumFmts ctNumFmts) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTNumFmts ctNumFmts2 = (CTNumFmts)this.get_store().find_element_user(CTStylesheetImpl.NUMFMTS$0, 0);
            if (ctNumFmts2 == null) {
                ctNumFmts2 = (CTNumFmts)this.get_store().add_element_user(CTStylesheetImpl.NUMFMTS$0);
            }
            ctNumFmts2.set(ctNumFmts);
        }
    }
    
    public CTNumFmts addNewNumFmts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNumFmts)this.get_store().add_element_user(CTStylesheetImpl.NUMFMTS$0);
        }
    }
    
    public void unsetNumFmts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.NUMFMTS$0, 0);
        }
    }
    
    public CTFonts getFonts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFonts ctFonts = (CTFonts)this.get_store().find_element_user(CTStylesheetImpl.FONTS$2, 0);
            if (ctFonts == null) {
                return null;
            }
            return ctFonts;
        }
    }
    
    public boolean isSetFonts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.FONTS$2) != 0;
        }
    }
    
    public void setFonts(final CTFonts ctFonts) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTFonts ctFonts2 = (CTFonts)this.get_store().find_element_user(CTStylesheetImpl.FONTS$2, 0);
            if (ctFonts2 == null) {
                ctFonts2 = (CTFonts)this.get_store().add_element_user(CTStylesheetImpl.FONTS$2);
            }
            ctFonts2.set(ctFonts);
        }
    }
    
    public CTFonts addNewFonts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFonts)this.get_store().add_element_user(CTStylesheetImpl.FONTS$2);
        }
    }
    
    public void unsetFonts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.FONTS$2, 0);
        }
    }
    
    public CTFills getFills() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFills ctFills = (CTFills)this.get_store().find_element_user(CTStylesheetImpl.FILLS$4, 0);
            if (ctFills == null) {
                return null;
            }
            return ctFills;
        }
    }
    
    public boolean isSetFills() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.FILLS$4) != 0;
        }
    }
    
    public void setFills(final CTFills ctFills) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTFills ctFills2 = (CTFills)this.get_store().find_element_user(CTStylesheetImpl.FILLS$4, 0);
            if (ctFills2 == null) {
                ctFills2 = (CTFills)this.get_store().add_element_user(CTStylesheetImpl.FILLS$4);
            }
            ctFills2.set(ctFills);
        }
    }
    
    public CTFills addNewFills() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFills)this.get_store().add_element_user(CTStylesheetImpl.FILLS$4);
        }
    }
    
    public void unsetFills() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.FILLS$4, 0);
        }
    }
    
    public CTBorders getBorders() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorders ctBorders = (CTBorders)this.get_store().find_element_user(CTStylesheetImpl.BORDERS$6, 0);
            if (ctBorders == null) {
                return null;
            }
            return ctBorders;
        }
    }
    
    public boolean isSetBorders() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.BORDERS$6) != 0;
        }
    }
    
    public void setBorders(final CTBorders ctBorders) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTBorders ctBorders2 = (CTBorders)this.get_store().find_element_user(CTStylesheetImpl.BORDERS$6, 0);
            if (ctBorders2 == null) {
                ctBorders2 = (CTBorders)this.get_store().add_element_user(CTStylesheetImpl.BORDERS$6);
            }
            ctBorders2.set(ctBorders);
        }
    }
    
    public CTBorders addNewBorders() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorders)this.get_store().add_element_user(CTStylesheetImpl.BORDERS$6);
        }
    }
    
    public void unsetBorders() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.BORDERS$6, 0);
        }
    }
    
    public CTCellStyleXfs getCellStyleXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCellStyleXfs ctCellStyleXfs = (CTCellStyleXfs)this.get_store().find_element_user(CTStylesheetImpl.CELLSTYLEXFS$8, 0);
            if (ctCellStyleXfs == null) {
                return null;
            }
            return ctCellStyleXfs;
        }
    }
    
    public boolean isSetCellStyleXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.CELLSTYLEXFS$8) != 0;
        }
    }
    
    public void setCellStyleXfs(final CTCellStyleXfs ctCellStyleXfs) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCellStyleXfs ctCellStyleXfs2 = (CTCellStyleXfs)this.get_store().find_element_user(CTStylesheetImpl.CELLSTYLEXFS$8, 0);
            if (ctCellStyleXfs2 == null) {
                ctCellStyleXfs2 = (CTCellStyleXfs)this.get_store().add_element_user(CTStylesheetImpl.CELLSTYLEXFS$8);
            }
            ctCellStyleXfs2.set(ctCellStyleXfs);
        }
    }
    
    public CTCellStyleXfs addNewCellStyleXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCellStyleXfs)this.get_store().add_element_user(CTStylesheetImpl.CELLSTYLEXFS$8);
        }
    }
    
    public void unsetCellStyleXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.CELLSTYLEXFS$8, 0);
        }
    }
    
    public CTCellXfs getCellXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCellXfs ctCellXfs = (CTCellXfs)this.get_store().find_element_user(CTStylesheetImpl.CELLXFS$10, 0);
            if (ctCellXfs == null) {
                return null;
            }
            return ctCellXfs;
        }
    }
    
    public boolean isSetCellXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.CELLXFS$10) != 0;
        }
    }
    
    public void setCellXfs(final CTCellXfs ctCellXfs) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCellXfs ctCellXfs2 = (CTCellXfs)this.get_store().find_element_user(CTStylesheetImpl.CELLXFS$10, 0);
            if (ctCellXfs2 == null) {
                ctCellXfs2 = (CTCellXfs)this.get_store().add_element_user(CTStylesheetImpl.CELLXFS$10);
            }
            ctCellXfs2.set(ctCellXfs);
        }
    }
    
    public CTCellXfs addNewCellXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCellXfs)this.get_store().add_element_user(CTStylesheetImpl.CELLXFS$10);
        }
    }
    
    public void unsetCellXfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.CELLXFS$10, 0);
        }
    }
    
    public CTCellStyles getCellStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCellStyles ctCellStyles = (CTCellStyles)this.get_store().find_element_user(CTStylesheetImpl.CELLSTYLES$12, 0);
            if (ctCellStyles == null) {
                return null;
            }
            return ctCellStyles;
        }
    }
    
    public boolean isSetCellStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.CELLSTYLES$12) != 0;
        }
    }
    
    public void setCellStyles(final CTCellStyles ctCellStyles) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCellStyles ctCellStyles2 = (CTCellStyles)this.get_store().find_element_user(CTStylesheetImpl.CELLSTYLES$12, 0);
            if (ctCellStyles2 == null) {
                ctCellStyles2 = (CTCellStyles)this.get_store().add_element_user(CTStylesheetImpl.CELLSTYLES$12);
            }
            ctCellStyles2.set((XmlObject)ctCellStyles);
        }
    }
    
    public CTCellStyles addNewCellStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCellStyles)this.get_store().add_element_user(CTStylesheetImpl.CELLSTYLES$12);
        }
    }
    
    public void unsetCellStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.CELLSTYLES$12, 0);
        }
    }
    
    public CTDxfs getDxfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDxfs ctDxfs = (CTDxfs)this.get_store().find_element_user(CTStylesheetImpl.DXFS$14, 0);
            if (ctDxfs == null) {
                return null;
            }
            return ctDxfs;
        }
    }
    
    public boolean isSetDxfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.DXFS$14) != 0;
        }
    }
    
    public void setDxfs(final CTDxfs ctDxfs) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTDxfs ctDxfs2 = (CTDxfs)this.get_store().find_element_user(CTStylesheetImpl.DXFS$14, 0);
            if (ctDxfs2 == null) {
                ctDxfs2 = (CTDxfs)this.get_store().add_element_user(CTStylesheetImpl.DXFS$14);
            }
            ctDxfs2.set(ctDxfs);
        }
    }
    
    public CTDxfs addNewDxfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDxfs)this.get_store().add_element_user(CTStylesheetImpl.DXFS$14);
        }
    }
    
    public void unsetDxfs() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.DXFS$14, 0);
        }
    }
    
    public CTTableStyles getTableStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTableStyles ctTableStyles = (CTTableStyles)this.get_store().find_element_user(CTStylesheetImpl.TABLESTYLES$16, 0);
            if (ctTableStyles == null) {
                return null;
            }
            return ctTableStyles;
        }
    }
    
    public boolean isSetTableStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.TABLESTYLES$16) != 0;
        }
    }
    
    public void setTableStyles(final CTTableStyles ctTableStyles) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTableStyles ctTableStyles2 = (CTTableStyles)this.get_store().find_element_user(CTStylesheetImpl.TABLESTYLES$16, 0);
            if (ctTableStyles2 == null) {
                ctTableStyles2 = (CTTableStyles)this.get_store().add_element_user(CTStylesheetImpl.TABLESTYLES$16);
            }
            ctTableStyles2.set((XmlObject)ctTableStyles);
        }
    }
    
    public CTTableStyles addNewTableStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTableStyles)this.get_store().add_element_user(CTStylesheetImpl.TABLESTYLES$16);
        }
    }
    
    public void unsetTableStyles() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.TABLESTYLES$16, 0);
        }
    }
    
    public CTColors getColors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColors ctColors = (CTColors)this.get_store().find_element_user(CTStylesheetImpl.COLORS$18, 0);
            if (ctColors == null) {
                return null;
            }
            return ctColors;
        }
    }
    
    public boolean isSetColors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.COLORS$18) != 0;
        }
    }
    
    public void setColors(final CTColors ctColors) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTColors ctColors2 = (CTColors)this.get_store().find_element_user(CTStylesheetImpl.COLORS$18, 0);
            if (ctColors2 == null) {
                ctColors2 = (CTColors)this.get_store().add_element_user(CTStylesheetImpl.COLORS$18);
            }
            ctColors2.set((XmlObject)ctColors);
        }
    }
    
    public CTColors addNewColors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColors)this.get_store().add_element_user(CTStylesheetImpl.COLORS$18);
        }
    }
    
    public void unsetColors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.COLORS$18, 0);
        }
    }
    
    public CTExtensionList getExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTExtensionList list = (CTExtensionList)this.get_store().find_element_user(CTStylesheetImpl.EXTLST$20, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public boolean isSetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTStylesheetImpl.EXTLST$20) != 0;
        }
    }
    
    public void setExtLst(final CTExtensionList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTExtensionList list2 = (CTExtensionList)this.get_store().find_element_user(CTStylesheetImpl.EXTLST$20, 0);
            if (list2 == null) {
                list2 = (CTExtensionList)this.get_store().add_element_user(CTStylesheetImpl.EXTLST$20);
            }
            list2.set((XmlObject)list);
        }
    }
    
    public CTExtensionList addNewExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTExtensionList)this.get_store().add_element_user(CTStylesheetImpl.EXTLST$20);
        }
    }
    
    public void unsetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTStylesheetImpl.EXTLST$20, 0);
        }
    }
    
    static {
        NUMFMTS$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "numFmts");
        FONTS$2 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "fonts");
        FILLS$4 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "fills");
        BORDERS$6 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "borders");
        CELLSTYLEXFS$8 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "cellStyleXfs");
        CELLXFS$10 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "cellXfs");
        CELLSTYLES$12 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "cellStyles");
        DXFS$14 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "dxfs");
        TABLESTYLES$16 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "tableStyles");
        COLORS$18 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "colors");
        EXTLST$20 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "extLst");
    }
}
