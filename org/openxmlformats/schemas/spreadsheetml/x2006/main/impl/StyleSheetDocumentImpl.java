// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTStylesheet;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.StyleSheetDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class StyleSheetDocumentImpl extends XmlComplexContentImpl implements StyleSheetDocument
{
    private static final QName STYLESHEET$0;
    
    public StyleSheetDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTStylesheet getStyleSheet() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTStylesheet ctStylesheet = (CTStylesheet)this.get_store().find_element_user(StyleSheetDocumentImpl.STYLESHEET$0, 0);
            if (ctStylesheet == null) {
                return null;
            }
            return ctStylesheet;
        }
    }
    
    public void setStyleSheet(final CTStylesheet ctStylesheet) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTStylesheet ctStylesheet2 = (CTStylesheet)this.get_store().find_element_user(StyleSheetDocumentImpl.STYLESHEET$0, 0);
            if (ctStylesheet2 == null) {
                ctStylesheet2 = (CTStylesheet)this.get_store().add_element_user(StyleSheetDocumentImpl.STYLESHEET$0);
            }
            ctStylesheet2.set(ctStylesheet);
        }
    }
    
    public CTStylesheet addNewStyleSheet() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTStylesheet)this.get_store().add_element_user(StyleSheetDocumentImpl.STYLESHEET$0);
        }
    }
    
    static {
        STYLESHEET$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "styleSheet");
    }
}
