// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTComment;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCommentList;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTCommentListImpl extends XmlComplexContentImpl implements CTCommentList
{
    private static final QName COMMENT$0;
    
    public CTCommentListImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTComment> getCommentList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTComment>)new CTCommentListImpl.CommentList(this);
        }
    }
    
    public CTComment[] getCommentArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTCommentListImpl.COMMENT$0, list);
            final CTComment[] array = new CTComment[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTComment getCommentArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTComment ctComment = (CTComment)this.get_store().find_element_user(CTCommentListImpl.COMMENT$0, n);
            if (ctComment == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctComment;
        }
    }
    
    public int sizeOfCommentArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTCommentListImpl.COMMENT$0);
        }
    }
    
    public void setCommentArray(final CTComment[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTCommentListImpl.COMMENT$0);
        }
    }
    
    public void setCommentArray(final int n, final CTComment ctComment) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTComment ctComment2 = (CTComment)this.get_store().find_element_user(CTCommentListImpl.COMMENT$0, n);
            if (ctComment2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctComment2.set(ctComment);
        }
    }
    
    public CTComment insertNewComment(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTComment)this.get_store().insert_element_user(CTCommentListImpl.COMMENT$0, n);
        }
    }
    
    public CTComment addNewComment() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTComment)this.get_store().add_element_user(CTCommentListImpl.COMMENT$0);
        }
    }
    
    public void removeComment(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTCommentListImpl.COMMENT$0, n);
        }
    }
    
    static {
        COMMENT$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "comment");
    }
}
