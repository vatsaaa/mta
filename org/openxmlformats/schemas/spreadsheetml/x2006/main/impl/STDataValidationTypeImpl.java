// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STDataValidationType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STDataValidationTypeImpl extends JavaStringEnumerationHolderEx implements STDataValidationType
{
    public STDataValidationTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STDataValidationTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
