// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STUnsignedShortHex;
import org.apache.xmlbeans.impl.values.JavaHexBinaryHolderEx;

public class STUnsignedShortHexImpl extends JavaHexBinaryHolderEx implements STUnsignedShortHex
{
    public STUnsignedShortHexImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STUnsignedShortHexImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
