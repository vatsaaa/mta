// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPane;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STPaneImpl extends JavaStringEnumerationHolderEx implements STPane
{
    public STPaneImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPaneImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
