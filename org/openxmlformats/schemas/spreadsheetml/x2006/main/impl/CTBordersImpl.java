// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorder;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorders;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTBordersImpl extends XmlComplexContentImpl implements CTBorders
{
    private static final QName BORDER$0;
    private static final QName COUNT$2;
    
    public CTBordersImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTBorder> getBorderList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBorder>)new CTBordersImpl.BorderList(this);
        }
    }
    
    public CTBorder[] getBorderArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTBordersImpl.BORDER$0, list);
            final CTBorder[] array = new CTBorder[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBorder getBorderArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder = (CTBorder)this.get_store().find_element_user(CTBordersImpl.BORDER$0, n);
            if (ctBorder == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBorder;
        }
    }
    
    public int sizeOfBorderArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTBordersImpl.BORDER$0);
        }
    }
    
    public void setBorderArray(final CTBorder[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTBordersImpl.BORDER$0);
        }
    }
    
    public void setBorderArray(final int n, final CTBorder ctBorder) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorder ctBorder2 = (CTBorder)this.get_store().find_element_user(CTBordersImpl.BORDER$0, n);
            if (ctBorder2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBorder2.set(ctBorder);
        }
    }
    
    public CTBorder insertNewBorder(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().insert_element_user(CTBordersImpl.BORDER$0, n);
        }
    }
    
    public CTBorder addNewBorder() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorder)this.get_store().add_element_user(CTBordersImpl.BORDER$0);
        }
    }
    
    public void removeBorder(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTBordersImpl.BORDER$0, n);
        }
    }
    
    public long getCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBordersImpl.COUNT$2);
            if (simpleValue == null) {
                return 0L;
            }
            return simpleValue.getLongValue();
        }
    }
    
    public XmlUnsignedInt xgetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedInt)this.get_store().find_attribute_user(CTBordersImpl.COUNT$2);
        }
    }
    
    public boolean isSetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTBordersImpl.COUNT$2) != null;
        }
    }
    
    public void setCount(final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTBordersImpl.COUNT$2);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTBordersImpl.COUNT$2);
            }
            simpleValue.setLongValue(longValue);
        }
    }
    
    public void xsetCount(final XmlUnsignedInt xmlUnsignedInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlUnsignedInt xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().find_attribute_user(CTBordersImpl.COUNT$2);
            if (xmlUnsignedInt2 == null) {
                xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().add_attribute_user(CTBordersImpl.COUNT$2);
            }
            xmlUnsignedInt2.set(xmlUnsignedInt);
        }
    }
    
    public void unsetCount() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTBordersImpl.COUNT$2);
        }
    }
    
    static {
        BORDER$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "border");
        COUNT$2 = new QName("", "count");
    }
}
