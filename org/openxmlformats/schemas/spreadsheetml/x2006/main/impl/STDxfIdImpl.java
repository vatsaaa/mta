// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STDxfId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STDxfIdImpl extends JavaLongHolderEx implements STDxfId
{
    public STDxfIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STDxfIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
