// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTHyperlink;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTHyperlinks;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTHyperlinksImpl extends XmlComplexContentImpl implements CTHyperlinks
{
    private static final QName HYPERLINK$0;
    
    public CTHyperlinksImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTHyperlink> getHyperlinkList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTHyperlink>)new CTHyperlinksImpl.HyperlinkList(this);
        }
    }
    
    public CTHyperlink[] getHyperlinkArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTHyperlinksImpl.HYPERLINK$0, list);
            final CTHyperlink[] array = new CTHyperlink[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTHyperlink getHyperlinkArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHyperlink ctHyperlink = (CTHyperlink)this.get_store().find_element_user(CTHyperlinksImpl.HYPERLINK$0, n);
            if (ctHyperlink == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctHyperlink;
        }
    }
    
    public int sizeOfHyperlinkArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTHyperlinksImpl.HYPERLINK$0);
        }
    }
    
    public void setHyperlinkArray(final CTHyperlink[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTHyperlinksImpl.HYPERLINK$0);
        }
    }
    
    public void setHyperlinkArray(final int n, final CTHyperlink ctHyperlink) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHyperlink ctHyperlink2 = (CTHyperlink)this.get_store().find_element_user(CTHyperlinksImpl.HYPERLINK$0, n);
            if (ctHyperlink2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctHyperlink2.set(ctHyperlink);
        }
    }
    
    public CTHyperlink insertNewHyperlink(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHyperlink)this.get_store().insert_element_user(CTHyperlinksImpl.HYPERLINK$0, n);
        }
    }
    
    public CTHyperlink addNewHyperlink() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHyperlink)this.get_store().add_element_user(CTHyperlinksImpl.HYPERLINK$0);
        }
    }
    
    public void removeHyperlink(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTHyperlinksImpl.HYPERLINK$0, n);
        }
    }
    
    static {
        HYPERLINK$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "hyperlink");
    }
}
