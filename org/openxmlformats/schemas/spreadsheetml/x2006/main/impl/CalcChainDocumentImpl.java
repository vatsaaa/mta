// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCalcChain;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CalcChainDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CalcChainDocumentImpl extends XmlComplexContentImpl implements CalcChainDocument
{
    private static final QName CALCCHAIN$0;
    
    public CalcChainDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTCalcChain getCalcChain() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCalcChain ctCalcChain = (CTCalcChain)this.get_store().find_element_user(CalcChainDocumentImpl.CALCCHAIN$0, 0);
            if (ctCalcChain == null) {
                return null;
            }
            return ctCalcChain;
        }
    }
    
    public void setCalcChain(final CTCalcChain ctCalcChain) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCalcChain ctCalcChain2 = (CTCalcChain)this.get_store().find_element_user(CalcChainDocumentImpl.CALCCHAIN$0, 0);
            if (ctCalcChain2 == null) {
                ctCalcChain2 = (CTCalcChain)this.get_store().add_element_user(CalcChainDocumentImpl.CALCCHAIN$0);
            }
            ctCalcChain2.set(ctCalcChain);
        }
    }
    
    public CTCalcChain addNewCalcChain() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCalcChain)this.get_store().add_element_user(CalcChainDocumentImpl.CALCCHAIN$0);
        }
    }
    
    static {
        CALCCHAIN$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "calcChain");
    }
}
