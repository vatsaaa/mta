// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCellRef;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STCellRefImpl extends JavaStringHolderEx implements STCellRef
{
    public STCellRefImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCellRefImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
