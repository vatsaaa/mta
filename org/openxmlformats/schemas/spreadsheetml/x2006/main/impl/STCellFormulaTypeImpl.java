// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCellFormulaType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STCellFormulaTypeImpl extends JavaStringEnumerationHolderEx implements STCellFormulaType
{
    public STCellFormulaTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STCellFormulaTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
