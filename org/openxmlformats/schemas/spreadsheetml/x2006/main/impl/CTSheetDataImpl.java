// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRow;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetData;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTSheetDataImpl extends XmlComplexContentImpl implements CTSheetData
{
    private static final QName ROW$0;
    
    public CTSheetDataImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTRow> getRowList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRow>)new CTSheetDataImpl.RowList(this);
        }
    }
    
    public CTRow[] getRowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSheetDataImpl.ROW$0, list);
            final CTRow[] array = new CTRow[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRow getRowArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRow ctRow = (CTRow)this.get_store().find_element_user(CTSheetDataImpl.ROW$0, n);
            if (ctRow == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRow;
        }
    }
    
    public int sizeOfRowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSheetDataImpl.ROW$0);
        }
    }
    
    public void setRowArray(final CTRow[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSheetDataImpl.ROW$0);
        }
    }
    
    public void setRowArray(final int n, final CTRow ctRow) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRow ctRow2 = (CTRow)this.get_store().find_element_user(CTSheetDataImpl.ROW$0, n);
            if (ctRow2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRow2.set(ctRow);
        }
    }
    
    public CTRow insertNewRow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRow)this.get_store().insert_element_user(CTSheetDataImpl.ROW$0, n);
        }
    }
    
    public CTRow addNewRow() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRow)this.get_store().add_element_user(CTSheetDataImpl.ROW$0);
        }
    }
    
    public void removeRow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSheetDataImpl.ROW$0, n);
        }
    }
    
    static {
        ROW$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "row");
    }
}
