// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STUnderlineValues;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STUnderlineValuesImpl extends JavaStringEnumerationHolderEx implements STUnderlineValues
{
    public STUnderlineValuesImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STUnderlineValuesImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
