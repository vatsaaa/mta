// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STUnsignedIntHex;
import org.apache.xmlbeans.impl.values.JavaHexBinaryHolderEx;

public class STUnsignedIntHexImpl extends JavaHexBinaryHolderEx implements STUnsignedIntHex
{
    public STUnsignedIntHexImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STUnsignedIntHexImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
