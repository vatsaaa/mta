// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFontScheme;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTVerticalAlignFontProperty;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTUnderlineProperty;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFontSize;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import java.util.AbstractList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBooleanProperty;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTIntProperty;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFontName;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFont;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTFontImpl extends XmlComplexContentImpl implements CTFont
{
    private static final QName NAME$0;
    private static final QName CHARSET$2;
    private static final QName FAMILY$4;
    private static final QName B$6;
    private static final QName I$8;
    private static final QName STRIKE$10;
    private static final QName OUTLINE$12;
    private static final QName SHADOW$14;
    private static final QName CONDENSE$16;
    private static final QName EXTEND$18;
    private static final QName COLOR$20;
    private static final QName SZ$22;
    private static final QName U$24;
    private static final QName VERTALIGN$26;
    private static final QName SCHEME$28;
    
    public CTFontImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTFontName> getNameList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFontName>)new CTFontImpl.NameList(this);
        }
    }
    
    public CTFontName[] getNameArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.NAME$0, list);
            final CTFontName[] array = new CTFontName[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFontName getNameArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontName ctFontName = (CTFontName)this.get_store().find_element_user(CTFontImpl.NAME$0, n);
            if (ctFontName == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFontName;
        }
    }
    
    public int sizeOfNameArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.NAME$0);
        }
    }
    
    public void setNameArray(final CTFontName[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.NAME$0);
        }
    }
    
    public void setNameArray(final int n, final CTFontName ctFontName) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontName ctFontName2 = (CTFontName)this.get_store().find_element_user(CTFontImpl.NAME$0, n);
            if (ctFontName2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFontName2.set(ctFontName);
        }
    }
    
    public CTFontName insertNewName(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontName)this.get_store().insert_element_user(CTFontImpl.NAME$0, n);
        }
    }
    
    public CTFontName addNewName() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontName)this.get_store().add_element_user(CTFontImpl.NAME$0);
        }
    }
    
    public void removeName(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.NAME$0, n);
        }
    }
    
    public List<CTIntProperty> getCharsetList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTIntProperty>)new CTFontImpl.CharsetList(this);
        }
    }
    
    public CTIntProperty[] getCharsetArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.CHARSET$2, list);
            final CTIntProperty[] array = new CTIntProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTIntProperty getCharsetArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty = (CTIntProperty)this.get_store().find_element_user(CTFontImpl.CHARSET$2, n);
            if (ctIntProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctIntProperty;
        }
    }
    
    public int sizeOfCharsetArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.CHARSET$2);
        }
    }
    
    public void setCharsetArray(final CTIntProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.CHARSET$2);
        }
    }
    
    public void setCharsetArray(final int n, final CTIntProperty ctIntProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty2 = (CTIntProperty)this.get_store().find_element_user(CTFontImpl.CHARSET$2, n);
            if (ctIntProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctIntProperty2.set(ctIntProperty);
        }
    }
    
    public CTIntProperty insertNewCharset(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().insert_element_user(CTFontImpl.CHARSET$2, n);
        }
    }
    
    public CTIntProperty addNewCharset() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().add_element_user(CTFontImpl.CHARSET$2);
        }
    }
    
    public void removeCharset(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.CHARSET$2, n);
        }
    }
    
    public List<CTIntProperty> getFamilyList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTIntProperty>)new CTFontImpl.FamilyList(this);
        }
    }
    
    public CTIntProperty[] getFamilyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.FAMILY$4, list);
            final CTIntProperty[] array = new CTIntProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTIntProperty getFamilyArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty = (CTIntProperty)this.get_store().find_element_user(CTFontImpl.FAMILY$4, n);
            if (ctIntProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctIntProperty;
        }
    }
    
    public int sizeOfFamilyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.FAMILY$4);
        }
    }
    
    public void setFamilyArray(final CTIntProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.FAMILY$4);
        }
    }
    
    public void setFamilyArray(final int n, final CTIntProperty ctIntProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIntProperty ctIntProperty2 = (CTIntProperty)this.get_store().find_element_user(CTFontImpl.FAMILY$4, n);
            if (ctIntProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctIntProperty2.set(ctIntProperty);
        }
    }
    
    public CTIntProperty insertNewFamily(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().insert_element_user(CTFontImpl.FAMILY$4, n);
        }
    }
    
    public CTIntProperty addNewFamily() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIntProperty)this.get_store().add_element_user(CTFontImpl.FAMILY$4);
        }
    }
    
    public void removeFamily(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.FAMILY$4, n);
        }
    }
    
    public List<CTBooleanProperty> getBList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class BList extends AbstractList<CTBooleanProperty>
            {
                @Override
                public CTBooleanProperty get(final int n) {
                    return CTFontImpl.this.getBArray(n);
                }
                
                @Override
                public CTBooleanProperty set(final int n, final CTBooleanProperty ctBooleanProperty) {
                    final CTBooleanProperty bArray = CTFontImpl.this.getBArray(n);
                    CTFontImpl.this.setBArray(n, ctBooleanProperty);
                    return bArray;
                }
                
                @Override
                public void add(final int n, final CTBooleanProperty ctBooleanProperty) {
                    CTFontImpl.this.insertNewB(n).set(ctBooleanProperty);
                }
                
                @Override
                public CTBooleanProperty remove(final int n) {
                    final CTBooleanProperty bArray = CTFontImpl.this.getBArray(n);
                    CTFontImpl.this.removeB(n);
                    return bArray;
                }
                
                @Override
                public int size() {
                    return CTFontImpl.this.sizeOfBArray();
                }
            }
            return new BList();
        }
    }
    
    public CTBooleanProperty[] getBArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.B$6, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getBArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.B$6, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfBArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.B$6);
        }
    }
    
    public void setBArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.B$6);
        }
    }
    
    public void setBArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.B$6, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewB(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTFontImpl.B$6, n);
        }
    }
    
    public CTBooleanProperty addNewB() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTFontImpl.B$6);
        }
    }
    
    public void removeB(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.B$6, n);
        }
    }
    
    public List<CTBooleanProperty> getIList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class IList extends AbstractList<CTBooleanProperty>
            {
                @Override
                public CTBooleanProperty get(final int n) {
                    return CTFontImpl.this.getIArray(n);
                }
                
                @Override
                public CTBooleanProperty set(final int n, final CTBooleanProperty ctBooleanProperty) {
                    final CTBooleanProperty iArray = CTFontImpl.this.getIArray(n);
                    CTFontImpl.this.setIArray(n, ctBooleanProperty);
                    return iArray;
                }
                
                @Override
                public void add(final int n, final CTBooleanProperty ctBooleanProperty) {
                    CTFontImpl.this.insertNewI(n).set(ctBooleanProperty);
                }
                
                @Override
                public CTBooleanProperty remove(final int n) {
                    final CTBooleanProperty iArray = CTFontImpl.this.getIArray(n);
                    CTFontImpl.this.removeI(n);
                    return iArray;
                }
                
                @Override
                public int size() {
                    return CTFontImpl.this.sizeOfIArray();
                }
            }
            return new IList();
        }
    }
    
    public CTBooleanProperty[] getIArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.I$8, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getIArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.I$8, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfIArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.I$8);
        }
    }
    
    public void setIArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.I$8);
        }
    }
    
    public void setIArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.I$8, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewI(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTFontImpl.I$8, n);
        }
    }
    
    public CTBooleanProperty addNewI() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTFontImpl.I$8);
        }
    }
    
    public void removeI(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.I$8, n);
        }
    }
    
    public List<CTBooleanProperty> getStrikeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class StrikeList extends AbstractList<CTBooleanProperty>
            {
                @Override
                public CTBooleanProperty get(final int n) {
                    return CTFontImpl.this.getStrikeArray(n);
                }
                
                @Override
                public CTBooleanProperty set(final int n, final CTBooleanProperty ctBooleanProperty) {
                    final CTBooleanProperty strikeArray = CTFontImpl.this.getStrikeArray(n);
                    CTFontImpl.this.setStrikeArray(n, ctBooleanProperty);
                    return strikeArray;
                }
                
                @Override
                public void add(final int n, final CTBooleanProperty ctBooleanProperty) {
                    CTFontImpl.this.insertNewStrike(n).set(ctBooleanProperty);
                }
                
                @Override
                public CTBooleanProperty remove(final int n) {
                    final CTBooleanProperty strikeArray = CTFontImpl.this.getStrikeArray(n);
                    CTFontImpl.this.removeStrike(n);
                    return strikeArray;
                }
                
                @Override
                public int size() {
                    return CTFontImpl.this.sizeOfStrikeArray();
                }
            }
            return new StrikeList();
        }
    }
    
    public CTBooleanProperty[] getStrikeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.STRIKE$10, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getStrikeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.STRIKE$10, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfStrikeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.STRIKE$10);
        }
    }
    
    public void setStrikeArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.STRIKE$10);
        }
    }
    
    public void setStrikeArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.STRIKE$10, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewStrike(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTFontImpl.STRIKE$10, n);
        }
    }
    
    public CTBooleanProperty addNewStrike() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTFontImpl.STRIKE$10);
        }
    }
    
    public void removeStrike(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.STRIKE$10, n);
        }
    }
    
    public List<CTBooleanProperty> getOutlineList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTFontImpl.OutlineList(this);
        }
    }
    
    public CTBooleanProperty[] getOutlineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.OUTLINE$12, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getOutlineArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.OUTLINE$12, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfOutlineArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.OUTLINE$12);
        }
    }
    
    public void setOutlineArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.OUTLINE$12);
        }
    }
    
    public void setOutlineArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.OUTLINE$12, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewOutline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTFontImpl.OUTLINE$12, n);
        }
    }
    
    public CTBooleanProperty addNewOutline() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTFontImpl.OUTLINE$12);
        }
    }
    
    public void removeOutline(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.OUTLINE$12, n);
        }
    }
    
    public List<CTBooleanProperty> getShadowList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTFontImpl.ShadowList(this);
        }
    }
    
    public CTBooleanProperty[] getShadowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.SHADOW$14, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getShadowArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.SHADOW$14, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfShadowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.SHADOW$14);
        }
    }
    
    public void setShadowArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.SHADOW$14);
        }
    }
    
    public void setShadowArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.SHADOW$14, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewShadow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTFontImpl.SHADOW$14, n);
        }
    }
    
    public CTBooleanProperty addNewShadow() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTFontImpl.SHADOW$14);
        }
    }
    
    public void removeShadow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.SHADOW$14, n);
        }
    }
    
    public List<CTBooleanProperty> getCondenseList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTFontImpl.CondenseList(this);
        }
    }
    
    public CTBooleanProperty[] getCondenseArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.CONDENSE$16, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getCondenseArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.CONDENSE$16, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfCondenseArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.CONDENSE$16);
        }
    }
    
    public void setCondenseArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.CONDENSE$16);
        }
    }
    
    public void setCondenseArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.CONDENSE$16, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewCondense(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTFontImpl.CONDENSE$16, n);
        }
    }
    
    public CTBooleanProperty addNewCondense() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTFontImpl.CONDENSE$16);
        }
    }
    
    public void removeCondense(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.CONDENSE$16, n);
        }
    }
    
    public List<CTBooleanProperty> getExtendList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBooleanProperty>)new CTFontImpl.ExtendList(this);
        }
    }
    
    public CTBooleanProperty[] getExtendArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.EXTEND$18, list);
            final CTBooleanProperty[] array = new CTBooleanProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBooleanProperty getExtendArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.EXTEND$18, n);
            if (ctBooleanProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBooleanProperty;
        }
    }
    
    public int sizeOfExtendArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.EXTEND$18);
        }
    }
    
    public void setExtendArray(final CTBooleanProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.EXTEND$18);
        }
    }
    
    public void setExtendArray(final int n, final CTBooleanProperty ctBooleanProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBooleanProperty ctBooleanProperty2 = (CTBooleanProperty)this.get_store().find_element_user(CTFontImpl.EXTEND$18, n);
            if (ctBooleanProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBooleanProperty2.set(ctBooleanProperty);
        }
    }
    
    public CTBooleanProperty insertNewExtend(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().insert_element_user(CTFontImpl.EXTEND$18, n);
        }
    }
    
    public CTBooleanProperty addNewExtend() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBooleanProperty)this.get_store().add_element_user(CTFontImpl.EXTEND$18);
        }
    }
    
    public void removeExtend(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.EXTEND$18, n);
        }
    }
    
    public List<CTColor> getColorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTColor>)new CTFontImpl.ColorList(this);
        }
    }
    
    public CTColor[] getColorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.COLOR$20, list);
            final CTColor[] array = new CTColor[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTColor getColorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColor ctColor = (CTColor)this.get_store().find_element_user(CTFontImpl.COLOR$20, n);
            if (ctColor == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctColor;
        }
    }
    
    public int sizeOfColorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.COLOR$20);
        }
    }
    
    public void setColorArray(final CTColor[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.COLOR$20);
        }
    }
    
    public void setColorArray(final int n, final CTColor ctColor) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTColor ctColor2 = (CTColor)this.get_store().find_element_user(CTFontImpl.COLOR$20, n);
            if (ctColor2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctColor2.set(ctColor);
        }
    }
    
    public CTColor insertNewColor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColor)this.get_store().insert_element_user(CTFontImpl.COLOR$20, n);
        }
    }
    
    public CTColor addNewColor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTColor)this.get_store().add_element_user(CTFontImpl.COLOR$20);
        }
    }
    
    public void removeColor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.COLOR$20, n);
        }
    }
    
    public List<CTFontSize> getSzList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFontSize>)new CTFontImpl.SzList(this);
        }
    }
    
    public CTFontSize[] getSzArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.SZ$22, list);
            final CTFontSize[] array = new CTFontSize[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFontSize getSzArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontSize ctFontSize = (CTFontSize)this.get_store().find_element_user(CTFontImpl.SZ$22, n);
            if (ctFontSize == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFontSize;
        }
    }
    
    public int sizeOfSzArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.SZ$22);
        }
    }
    
    public void setSzArray(final CTFontSize[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.SZ$22);
        }
    }
    
    public void setSzArray(final int n, final CTFontSize ctFontSize) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontSize ctFontSize2 = (CTFontSize)this.get_store().find_element_user(CTFontImpl.SZ$22, n);
            if (ctFontSize2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFontSize2.set(ctFontSize);
        }
    }
    
    public CTFontSize insertNewSz(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontSize)this.get_store().insert_element_user(CTFontImpl.SZ$22, n);
        }
    }
    
    public CTFontSize addNewSz() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontSize)this.get_store().add_element_user(CTFontImpl.SZ$22);
        }
    }
    
    public void removeSz(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.SZ$22, n);
        }
    }
    
    public List<CTUnderlineProperty> getUList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class UList extends AbstractList<CTUnderlineProperty>
            {
                @Override
                public CTUnderlineProperty get(final int n) {
                    return CTFontImpl.this.getUArray(n);
                }
                
                @Override
                public CTUnderlineProperty set(final int n, final CTUnderlineProperty ctUnderlineProperty) {
                    final CTUnderlineProperty uArray = CTFontImpl.this.getUArray(n);
                    CTFontImpl.this.setUArray(n, ctUnderlineProperty);
                    return uArray;
                }
                
                @Override
                public void add(final int n, final CTUnderlineProperty ctUnderlineProperty) {
                    CTFontImpl.this.insertNewU(n).set(ctUnderlineProperty);
                }
                
                @Override
                public CTUnderlineProperty remove(final int n) {
                    final CTUnderlineProperty uArray = CTFontImpl.this.getUArray(n);
                    CTFontImpl.this.removeU(n);
                    return uArray;
                }
                
                @Override
                public int size() {
                    return CTFontImpl.this.sizeOfUArray();
                }
            }
            return new UList();
        }
    }
    
    public CTUnderlineProperty[] getUArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.U$24, list);
            final CTUnderlineProperty[] array = new CTUnderlineProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTUnderlineProperty getUArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTUnderlineProperty ctUnderlineProperty = (CTUnderlineProperty)this.get_store().find_element_user(CTFontImpl.U$24, n);
            if (ctUnderlineProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctUnderlineProperty;
        }
    }
    
    public int sizeOfUArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.U$24);
        }
    }
    
    public void setUArray(final CTUnderlineProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.U$24);
        }
    }
    
    public void setUArray(final int n, final CTUnderlineProperty ctUnderlineProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTUnderlineProperty ctUnderlineProperty2 = (CTUnderlineProperty)this.get_store().find_element_user(CTFontImpl.U$24, n);
            if (ctUnderlineProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctUnderlineProperty2.set(ctUnderlineProperty);
        }
    }
    
    public CTUnderlineProperty insertNewU(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTUnderlineProperty)this.get_store().insert_element_user(CTFontImpl.U$24, n);
        }
    }
    
    public CTUnderlineProperty addNewU() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTUnderlineProperty)this.get_store().add_element_user(CTFontImpl.U$24);
        }
    }
    
    public void removeU(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.U$24, n);
        }
    }
    
    public List<CTVerticalAlignFontProperty> getVertAlignList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTVerticalAlignFontProperty>)new CTFontImpl.VertAlignList(this);
        }
    }
    
    public CTVerticalAlignFontProperty[] getVertAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.VERTALIGN$26, list);
            final CTVerticalAlignFontProperty[] array = new CTVerticalAlignFontProperty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTVerticalAlignFontProperty getVertAlignArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTVerticalAlignFontProperty ctVerticalAlignFontProperty = (CTVerticalAlignFontProperty)this.get_store().find_element_user(CTFontImpl.VERTALIGN$26, n);
            if (ctVerticalAlignFontProperty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctVerticalAlignFontProperty;
        }
    }
    
    public int sizeOfVertAlignArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.VERTALIGN$26);
        }
    }
    
    public void setVertAlignArray(final CTVerticalAlignFontProperty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.VERTALIGN$26);
        }
    }
    
    public void setVertAlignArray(final int n, final CTVerticalAlignFontProperty ctVerticalAlignFontProperty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTVerticalAlignFontProperty ctVerticalAlignFontProperty2 = (CTVerticalAlignFontProperty)this.get_store().find_element_user(CTFontImpl.VERTALIGN$26, n);
            if (ctVerticalAlignFontProperty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctVerticalAlignFontProperty2.set(ctVerticalAlignFontProperty);
        }
    }
    
    public CTVerticalAlignFontProperty insertNewVertAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTVerticalAlignFontProperty)this.get_store().insert_element_user(CTFontImpl.VERTALIGN$26, n);
        }
    }
    
    public CTVerticalAlignFontProperty addNewVertAlign() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTVerticalAlignFontProperty)this.get_store().add_element_user(CTFontImpl.VERTALIGN$26);
        }
    }
    
    public void removeVertAlign(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.VERTALIGN$26, n);
        }
    }
    
    public List<CTFontScheme> getSchemeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFontScheme>)new CTFontImpl.SchemeList(this);
        }
    }
    
    public CTFontScheme[] getSchemeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTFontImpl.SCHEME$28, list);
            final CTFontScheme[] array = new CTFontScheme[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFontScheme getSchemeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontScheme ctFontScheme = (CTFontScheme)this.get_store().find_element_user(CTFontImpl.SCHEME$28, n);
            if (ctFontScheme == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFontScheme;
        }
    }
    
    public int sizeOfSchemeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTFontImpl.SCHEME$28);
        }
    }
    
    public void setSchemeArray(final CTFontScheme[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTFontImpl.SCHEME$28);
        }
    }
    
    public void setSchemeArray(final int n, final CTFontScheme ctFontScheme) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFontScheme ctFontScheme2 = (CTFontScheme)this.get_store().find_element_user(CTFontImpl.SCHEME$28, n);
            if (ctFontScheme2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFontScheme2.set(ctFontScheme);
        }
    }
    
    public CTFontScheme insertNewScheme(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontScheme)this.get_store().insert_element_user(CTFontImpl.SCHEME$28, n);
        }
    }
    
    public CTFontScheme addNewScheme() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFontScheme)this.get_store().add_element_user(CTFontImpl.SCHEME$28);
        }
    }
    
    public void removeScheme(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTFontImpl.SCHEME$28, n);
        }
    }
    
    static {
        NAME$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "name");
        CHARSET$2 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "charset");
        FAMILY$4 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "family");
        B$6 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "b");
        I$8 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "i");
        STRIKE$10 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "strike");
        OUTLINE$12 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "outline");
        SHADOW$14 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "shadow");
        CONDENSE$16 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "condense");
        EXTEND$18 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "extend");
        COLOR$20 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "color");
        SZ$22 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sz");
        U$24 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "u");
        VERTALIGN$26 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "vertAlign");
        SCHEME$28 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "scheme");
    }
}
