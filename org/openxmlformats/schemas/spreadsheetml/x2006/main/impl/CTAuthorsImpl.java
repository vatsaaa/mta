// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STXstring;
import org.apache.xmlbeans.SimpleValue;
import java.util.ArrayList;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTAuthors;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTAuthorsImpl extends XmlComplexContentImpl implements CTAuthors
{
    private static final QName AUTHOR$0;
    
    public CTAuthorsImpl(final SchemaType type) {
        super(type);
    }
    
    public List<String> getAuthorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTAuthorsImpl.AuthorList(this);
        }
    }
    
    public String[] getAuthorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTAuthorsImpl.AUTHOR$0, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getAuthorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTAuthorsImpl.AUTHOR$0, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<STXstring> xgetAuthorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STXstring>)new CTAuthorsImpl.AuthorList(this);
        }
    }
    
    public STXstring[] xgetAuthorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTAuthorsImpl.AUTHOR$0, list);
            final STXstring[] array = new STXstring[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STXstring xgetAuthorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STXstring stXstring = (STXstring)this.get_store().find_element_user(CTAuthorsImpl.AUTHOR$0, n);
            if (stXstring == null) {
                throw new IndexOutOfBoundsException();
            }
            return stXstring;
        }
    }
    
    public int sizeOfAuthorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTAuthorsImpl.AUTHOR$0);
        }
    }
    
    public void setAuthorArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTAuthorsImpl.AUTHOR$0);
        }
    }
    
    public void setAuthorArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTAuthorsImpl.AUTHOR$0, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetAuthorArray(final STXstring[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTAuthorsImpl.AUTHOR$0);
        }
    }
    
    public void xsetAuthorArray(final int n, final STXstring stXstring) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STXstring stXstring2 = (STXstring)this.get_store().find_element_user(CTAuthorsImpl.AUTHOR$0, n);
            if (stXstring2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stXstring2.set(stXstring);
        }
    }
    
    public void insertAuthor(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTAuthorsImpl.AUTHOR$0, n)).setStringValue(stringValue);
        }
    }
    
    public void addAuthor(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTAuthorsImpl.AUTHOR$0)).setStringValue(stringValue);
        }
    }
    
    public STXstring insertNewAuthor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STXstring)this.get_store().insert_element_user(CTAuthorsImpl.AUTHOR$0, n);
        }
    }
    
    public STXstring addNewAuthor() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STXstring)this.get_store().add_element_user(CTAuthorsImpl.AUTHOR$0);
        }
    }
    
    public void removeAuthor(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTAuthorsImpl.AUTHOR$0, n);
        }
    }
    
    static {
        AUTHOR$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "author");
    }
}
