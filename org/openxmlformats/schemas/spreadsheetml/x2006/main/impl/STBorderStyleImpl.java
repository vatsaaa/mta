// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STBorderStyle;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STBorderStyleImpl extends JavaStringEnumerationHolderEx implements STBorderStyle
{
    public STBorderStyleImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STBorderStyleImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
