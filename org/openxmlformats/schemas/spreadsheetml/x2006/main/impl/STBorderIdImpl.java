// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STBorderId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STBorderIdImpl extends JavaLongHolderEx implements STBorderId
{
    public STBorderIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STBorderIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
