// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTExtensionList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableParts;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWebPublishItems;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTControls;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTOleObjects;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetBackgroundPicture;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTLegacyDrawing;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDrawing;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSmartTags;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTIgnoredErrors;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellWatches;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCustomProperties;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPageBreak;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTHeaderFooter;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPageSetup;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPageMargins;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPrintOptions;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTHyperlinks;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDataValidations;
import java.util.AbstractList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTConditionalFormatting;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPhoneticPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTMergeCells;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCustomSheetViews;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDataConsolidate;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSortState;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTAutoFilter;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTScenarios;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTProtectedRanges;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetProtection;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetCalcPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetData;
import java.util.ArrayList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCols;
import java.util.List;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetFormatPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetViews;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetDimension;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetPr;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTWorksheetImpl extends XmlComplexContentImpl implements CTWorksheet
{
    private static final QName SHEETPR$0;
    private static final QName DIMENSION$2;
    private static final QName SHEETVIEWS$4;
    private static final QName SHEETFORMATPR$6;
    private static final QName COLS$8;
    private static final QName SHEETDATA$10;
    private static final QName SHEETCALCPR$12;
    private static final QName SHEETPROTECTION$14;
    private static final QName PROTECTEDRANGES$16;
    private static final QName SCENARIOS$18;
    private static final QName AUTOFILTER$20;
    private static final QName SORTSTATE$22;
    private static final QName DATACONSOLIDATE$24;
    private static final QName CUSTOMSHEETVIEWS$26;
    private static final QName MERGECELLS$28;
    private static final QName PHONETICPR$30;
    private static final QName CONDITIONALFORMATTING$32;
    private static final QName DATAVALIDATIONS$34;
    private static final QName HYPERLINKS$36;
    private static final QName PRINTOPTIONS$38;
    private static final QName PAGEMARGINS$40;
    private static final QName PAGESETUP$42;
    private static final QName HEADERFOOTER$44;
    private static final QName ROWBREAKS$46;
    private static final QName COLBREAKS$48;
    private static final QName CUSTOMPROPERTIES$50;
    private static final QName CELLWATCHES$52;
    private static final QName IGNOREDERRORS$54;
    private static final QName SMARTTAGS$56;
    private static final QName DRAWING$58;
    private static final QName LEGACYDRAWING$60;
    private static final QName LEGACYDRAWINGHF$62;
    private static final QName PICTURE$64;
    private static final QName OLEOBJECTS$66;
    private static final QName CONTROLS$68;
    private static final QName WEBPUBLISHITEMS$70;
    private static final QName TABLEPARTS$72;
    private static final QName EXTLST$74;
    
    public CTWorksheetImpl(final SchemaType type) {
        super(type);
    }
    
    public CTSheetPr getSheetPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetPr ctSheetPr = (CTSheetPr)this.get_store().find_element_user(CTWorksheetImpl.SHEETPR$0, 0);
            if (ctSheetPr == null) {
                return null;
            }
            return ctSheetPr;
        }
    }
    
    public boolean isSetSheetPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SHEETPR$0) != 0;
        }
    }
    
    public void setSheetPr(final CTSheetPr ctSheetPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetPr ctSheetPr2 = (CTSheetPr)this.get_store().find_element_user(CTWorksheetImpl.SHEETPR$0, 0);
            if (ctSheetPr2 == null) {
                ctSheetPr2 = (CTSheetPr)this.get_store().add_element_user(CTWorksheetImpl.SHEETPR$0);
            }
            ctSheetPr2.set(ctSheetPr);
        }
    }
    
    public CTSheetPr addNewSheetPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetPr)this.get_store().add_element_user(CTWorksheetImpl.SHEETPR$0);
        }
    }
    
    public void unsetSheetPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SHEETPR$0, 0);
        }
    }
    
    public CTSheetDimension getDimension() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetDimension ctSheetDimension = (CTSheetDimension)this.get_store().find_element_user(CTWorksheetImpl.DIMENSION$2, 0);
            if (ctSheetDimension == null) {
                return null;
            }
            return ctSheetDimension;
        }
    }
    
    public boolean isSetDimension() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.DIMENSION$2) != 0;
        }
    }
    
    public void setDimension(final CTSheetDimension ctSheetDimension) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetDimension ctSheetDimension2 = (CTSheetDimension)this.get_store().find_element_user(CTWorksheetImpl.DIMENSION$2, 0);
            if (ctSheetDimension2 == null) {
                ctSheetDimension2 = (CTSheetDimension)this.get_store().add_element_user(CTWorksheetImpl.DIMENSION$2);
            }
            ctSheetDimension2.set(ctSheetDimension);
        }
    }
    
    public CTSheetDimension addNewDimension() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetDimension)this.get_store().add_element_user(CTWorksheetImpl.DIMENSION$2);
        }
    }
    
    public void unsetDimension() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.DIMENSION$2, 0);
        }
    }
    
    public CTSheetViews getSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetViews ctSheetViews = (CTSheetViews)this.get_store().find_element_user(CTWorksheetImpl.SHEETVIEWS$4, 0);
            if (ctSheetViews == null) {
                return null;
            }
            return ctSheetViews;
        }
    }
    
    public boolean isSetSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SHEETVIEWS$4) != 0;
        }
    }
    
    public void setSheetViews(final CTSheetViews ctSheetViews) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetViews ctSheetViews2 = (CTSheetViews)this.get_store().find_element_user(CTWorksheetImpl.SHEETVIEWS$4, 0);
            if (ctSheetViews2 == null) {
                ctSheetViews2 = (CTSheetViews)this.get_store().add_element_user(CTWorksheetImpl.SHEETVIEWS$4);
            }
            ctSheetViews2.set(ctSheetViews);
        }
    }
    
    public CTSheetViews addNewSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetViews)this.get_store().add_element_user(CTWorksheetImpl.SHEETVIEWS$4);
        }
    }
    
    public void unsetSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SHEETVIEWS$4, 0);
        }
    }
    
    public CTSheetFormatPr getSheetFormatPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetFormatPr ctSheetFormatPr = (CTSheetFormatPr)this.get_store().find_element_user(CTWorksheetImpl.SHEETFORMATPR$6, 0);
            if (ctSheetFormatPr == null) {
                return null;
            }
            return ctSheetFormatPr;
        }
    }
    
    public boolean isSetSheetFormatPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SHEETFORMATPR$6) != 0;
        }
    }
    
    public void setSheetFormatPr(final CTSheetFormatPr ctSheetFormatPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetFormatPr ctSheetFormatPr2 = (CTSheetFormatPr)this.get_store().find_element_user(CTWorksheetImpl.SHEETFORMATPR$6, 0);
            if (ctSheetFormatPr2 == null) {
                ctSheetFormatPr2 = (CTSheetFormatPr)this.get_store().add_element_user(CTWorksheetImpl.SHEETFORMATPR$6);
            }
            ctSheetFormatPr2.set(ctSheetFormatPr);
        }
    }
    
    public CTSheetFormatPr addNewSheetFormatPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetFormatPr)this.get_store().add_element_user(CTWorksheetImpl.SHEETFORMATPR$6);
        }
    }
    
    public void unsetSheetFormatPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SHEETFORMATPR$6, 0);
        }
    }
    
    public List<CTCols> getColsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCols>)new CTWorksheetImpl.ColsList(this);
        }
    }
    
    public CTCols[] getColsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTWorksheetImpl.COLS$8, list);
            final CTCols[] array = new CTCols[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCols getColsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCols ctCols = (CTCols)this.get_store().find_element_user(CTWorksheetImpl.COLS$8, n);
            if (ctCols == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCols;
        }
    }
    
    public int sizeOfColsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.COLS$8);
        }
    }
    
    public void setColsArray(final CTCols[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTWorksheetImpl.COLS$8);
        }
    }
    
    public void setColsArray(final int n, final CTCols ctCols) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCols ctCols2 = (CTCols)this.get_store().find_element_user(CTWorksheetImpl.COLS$8, n);
            if (ctCols2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCols2.set(ctCols);
        }
    }
    
    public CTCols insertNewCols(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCols)this.get_store().insert_element_user(CTWorksheetImpl.COLS$8, n);
        }
    }
    
    public CTCols addNewCols() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCols)this.get_store().add_element_user(CTWorksheetImpl.COLS$8);
        }
    }
    
    public void removeCols(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.COLS$8, n);
        }
    }
    
    public CTSheetData getSheetData() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetData ctSheetData = (CTSheetData)this.get_store().find_element_user(CTWorksheetImpl.SHEETDATA$10, 0);
            if (ctSheetData == null) {
                return null;
            }
            return ctSheetData;
        }
    }
    
    public void setSheetData(final CTSheetData ctSheetData) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetData ctSheetData2 = (CTSheetData)this.get_store().find_element_user(CTWorksheetImpl.SHEETDATA$10, 0);
            if (ctSheetData2 == null) {
                ctSheetData2 = (CTSheetData)this.get_store().add_element_user(CTWorksheetImpl.SHEETDATA$10);
            }
            ctSheetData2.set(ctSheetData);
        }
    }
    
    public CTSheetData addNewSheetData() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetData)this.get_store().add_element_user(CTWorksheetImpl.SHEETDATA$10);
        }
    }
    
    public CTSheetCalcPr getSheetCalcPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetCalcPr ctSheetCalcPr = (CTSheetCalcPr)this.get_store().find_element_user(CTWorksheetImpl.SHEETCALCPR$12, 0);
            if (ctSheetCalcPr == null) {
                return null;
            }
            return ctSheetCalcPr;
        }
    }
    
    public boolean isSetSheetCalcPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SHEETCALCPR$12) != 0;
        }
    }
    
    public void setSheetCalcPr(final CTSheetCalcPr ctSheetCalcPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetCalcPr ctSheetCalcPr2 = (CTSheetCalcPr)this.get_store().find_element_user(CTWorksheetImpl.SHEETCALCPR$12, 0);
            if (ctSheetCalcPr2 == null) {
                ctSheetCalcPr2 = (CTSheetCalcPr)this.get_store().add_element_user(CTWorksheetImpl.SHEETCALCPR$12);
            }
            ctSheetCalcPr2.set(ctSheetCalcPr);
        }
    }
    
    public CTSheetCalcPr addNewSheetCalcPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetCalcPr)this.get_store().add_element_user(CTWorksheetImpl.SHEETCALCPR$12);
        }
    }
    
    public void unsetSheetCalcPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SHEETCALCPR$12, 0);
        }
    }
    
    public CTSheetProtection getSheetProtection() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetProtection ctSheetProtection = (CTSheetProtection)this.get_store().find_element_user(CTWorksheetImpl.SHEETPROTECTION$14, 0);
            if (ctSheetProtection == null) {
                return null;
            }
            return ctSheetProtection;
        }
    }
    
    public boolean isSetSheetProtection() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SHEETPROTECTION$14) != 0;
        }
    }
    
    public void setSheetProtection(final CTSheetProtection ctSheetProtection) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetProtection ctSheetProtection2 = (CTSheetProtection)this.get_store().find_element_user(CTWorksheetImpl.SHEETPROTECTION$14, 0);
            if (ctSheetProtection2 == null) {
                ctSheetProtection2 = (CTSheetProtection)this.get_store().add_element_user(CTWorksheetImpl.SHEETPROTECTION$14);
            }
            ctSheetProtection2.set(ctSheetProtection);
        }
    }
    
    public CTSheetProtection addNewSheetProtection() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetProtection)this.get_store().add_element_user(CTWorksheetImpl.SHEETPROTECTION$14);
        }
    }
    
    public void unsetSheetProtection() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SHEETPROTECTION$14, 0);
        }
    }
    
    public CTProtectedRanges getProtectedRanges() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTProtectedRanges ctProtectedRanges = (CTProtectedRanges)this.get_store().find_element_user(CTWorksheetImpl.PROTECTEDRANGES$16, 0);
            if (ctProtectedRanges == null) {
                return null;
            }
            return ctProtectedRanges;
        }
    }
    
    public boolean isSetProtectedRanges() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.PROTECTEDRANGES$16) != 0;
        }
    }
    
    public void setProtectedRanges(final CTProtectedRanges ctProtectedRanges) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTProtectedRanges ctProtectedRanges2 = (CTProtectedRanges)this.get_store().find_element_user(CTWorksheetImpl.PROTECTEDRANGES$16, 0);
            if (ctProtectedRanges2 == null) {
                ctProtectedRanges2 = (CTProtectedRanges)this.get_store().add_element_user(CTWorksheetImpl.PROTECTEDRANGES$16);
            }
            ctProtectedRanges2.set((XmlObject)ctProtectedRanges);
        }
    }
    
    public CTProtectedRanges addNewProtectedRanges() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTProtectedRanges)this.get_store().add_element_user(CTWorksheetImpl.PROTECTEDRANGES$16);
        }
    }
    
    public void unsetProtectedRanges() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.PROTECTEDRANGES$16, 0);
        }
    }
    
    public CTScenarios getScenarios() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTScenarios ctScenarios = (CTScenarios)this.get_store().find_element_user(CTWorksheetImpl.SCENARIOS$18, 0);
            if (ctScenarios == null) {
                return null;
            }
            return ctScenarios;
        }
    }
    
    public boolean isSetScenarios() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SCENARIOS$18) != 0;
        }
    }
    
    public void setScenarios(final CTScenarios ctScenarios) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTScenarios ctScenarios2 = (CTScenarios)this.get_store().find_element_user(CTWorksheetImpl.SCENARIOS$18, 0);
            if (ctScenarios2 == null) {
                ctScenarios2 = (CTScenarios)this.get_store().add_element_user(CTWorksheetImpl.SCENARIOS$18);
            }
            ctScenarios2.set((XmlObject)ctScenarios);
        }
    }
    
    public CTScenarios addNewScenarios() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTScenarios)this.get_store().add_element_user(CTWorksheetImpl.SCENARIOS$18);
        }
    }
    
    public void unsetScenarios() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SCENARIOS$18, 0);
        }
    }
    
    public CTAutoFilter getAutoFilter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAutoFilter ctAutoFilter = (CTAutoFilter)this.get_store().find_element_user(CTWorksheetImpl.AUTOFILTER$20, 0);
            if (ctAutoFilter == null) {
                return null;
            }
            return ctAutoFilter;
        }
    }
    
    public boolean isSetAutoFilter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.AUTOFILTER$20) != 0;
        }
    }
    
    public void setAutoFilter(final CTAutoFilter ctAutoFilter) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTAutoFilter ctAutoFilter2 = (CTAutoFilter)this.get_store().find_element_user(CTWorksheetImpl.AUTOFILTER$20, 0);
            if (ctAutoFilter2 == null) {
                ctAutoFilter2 = (CTAutoFilter)this.get_store().add_element_user(CTWorksheetImpl.AUTOFILTER$20);
            }
            ctAutoFilter2.set(ctAutoFilter);
        }
    }
    
    public CTAutoFilter addNewAutoFilter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAutoFilter)this.get_store().add_element_user(CTWorksheetImpl.AUTOFILTER$20);
        }
    }
    
    public void unsetAutoFilter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.AUTOFILTER$20, 0);
        }
    }
    
    public CTSortState getSortState() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSortState ctSortState = (CTSortState)this.get_store().find_element_user(CTWorksheetImpl.SORTSTATE$22, 0);
            if (ctSortState == null) {
                return null;
            }
            return ctSortState;
        }
    }
    
    public boolean isSetSortState() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SORTSTATE$22) != 0;
        }
    }
    
    public void setSortState(final CTSortState ctSortState) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSortState ctSortState2 = (CTSortState)this.get_store().find_element_user(CTWorksheetImpl.SORTSTATE$22, 0);
            if (ctSortState2 == null) {
                ctSortState2 = (CTSortState)this.get_store().add_element_user(CTWorksheetImpl.SORTSTATE$22);
            }
            ctSortState2.set((XmlObject)ctSortState);
        }
    }
    
    public CTSortState addNewSortState() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSortState)this.get_store().add_element_user(CTWorksheetImpl.SORTSTATE$22);
        }
    }
    
    public void unsetSortState() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SORTSTATE$22, 0);
        }
    }
    
    public CTDataConsolidate getDataConsolidate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDataConsolidate ctDataConsolidate = (CTDataConsolidate)this.get_store().find_element_user(CTWorksheetImpl.DATACONSOLIDATE$24, 0);
            if (ctDataConsolidate == null) {
                return null;
            }
            return ctDataConsolidate;
        }
    }
    
    public boolean isSetDataConsolidate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.DATACONSOLIDATE$24) != 0;
        }
    }
    
    public void setDataConsolidate(final CTDataConsolidate ctDataConsolidate) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTDataConsolidate ctDataConsolidate2 = (CTDataConsolidate)this.get_store().find_element_user(CTWorksheetImpl.DATACONSOLIDATE$24, 0);
            if (ctDataConsolidate2 == null) {
                ctDataConsolidate2 = (CTDataConsolidate)this.get_store().add_element_user(CTWorksheetImpl.DATACONSOLIDATE$24);
            }
            ctDataConsolidate2.set((XmlObject)ctDataConsolidate);
        }
    }
    
    public CTDataConsolidate addNewDataConsolidate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDataConsolidate)this.get_store().add_element_user(CTWorksheetImpl.DATACONSOLIDATE$24);
        }
    }
    
    public void unsetDataConsolidate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.DATACONSOLIDATE$24, 0);
        }
    }
    
    public CTCustomSheetViews getCustomSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCustomSheetViews ctCustomSheetViews = (CTCustomSheetViews)this.get_store().find_element_user(CTWorksheetImpl.CUSTOMSHEETVIEWS$26, 0);
            if (ctCustomSheetViews == null) {
                return null;
            }
            return ctCustomSheetViews;
        }
    }
    
    public boolean isSetCustomSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.CUSTOMSHEETVIEWS$26) != 0;
        }
    }
    
    public void setCustomSheetViews(final CTCustomSheetViews ctCustomSheetViews) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCustomSheetViews ctCustomSheetViews2 = (CTCustomSheetViews)this.get_store().find_element_user(CTWorksheetImpl.CUSTOMSHEETVIEWS$26, 0);
            if (ctCustomSheetViews2 == null) {
                ctCustomSheetViews2 = (CTCustomSheetViews)this.get_store().add_element_user(CTWorksheetImpl.CUSTOMSHEETVIEWS$26);
            }
            ctCustomSheetViews2.set((XmlObject)ctCustomSheetViews);
        }
    }
    
    public CTCustomSheetViews addNewCustomSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCustomSheetViews)this.get_store().add_element_user(CTWorksheetImpl.CUSTOMSHEETVIEWS$26);
        }
    }
    
    public void unsetCustomSheetViews() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.CUSTOMSHEETVIEWS$26, 0);
        }
    }
    
    public CTMergeCells getMergeCells() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMergeCells ctMergeCells = (CTMergeCells)this.get_store().find_element_user(CTWorksheetImpl.MERGECELLS$28, 0);
            if (ctMergeCells == null) {
                return null;
            }
            return ctMergeCells;
        }
    }
    
    public boolean isSetMergeCells() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.MERGECELLS$28) != 0;
        }
    }
    
    public void setMergeCells(final CTMergeCells ctMergeCells) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTMergeCells ctMergeCells2 = (CTMergeCells)this.get_store().find_element_user(CTWorksheetImpl.MERGECELLS$28, 0);
            if (ctMergeCells2 == null) {
                ctMergeCells2 = (CTMergeCells)this.get_store().add_element_user(CTWorksheetImpl.MERGECELLS$28);
            }
            ctMergeCells2.set(ctMergeCells);
        }
    }
    
    public CTMergeCells addNewMergeCells() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMergeCells)this.get_store().add_element_user(CTWorksheetImpl.MERGECELLS$28);
        }
    }
    
    public void unsetMergeCells() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.MERGECELLS$28, 0);
        }
    }
    
    public CTPhoneticPr getPhoneticPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPhoneticPr ctPhoneticPr = (CTPhoneticPr)this.get_store().find_element_user(CTWorksheetImpl.PHONETICPR$30, 0);
            if (ctPhoneticPr == null) {
                return null;
            }
            return ctPhoneticPr;
        }
    }
    
    public boolean isSetPhoneticPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.PHONETICPR$30) != 0;
        }
    }
    
    public void setPhoneticPr(final CTPhoneticPr ctPhoneticPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPhoneticPr ctPhoneticPr2 = (CTPhoneticPr)this.get_store().find_element_user(CTWorksheetImpl.PHONETICPR$30, 0);
            if (ctPhoneticPr2 == null) {
                ctPhoneticPr2 = (CTPhoneticPr)this.get_store().add_element_user(CTWorksheetImpl.PHONETICPR$30);
            }
            ctPhoneticPr2.set(ctPhoneticPr);
        }
    }
    
    public CTPhoneticPr addNewPhoneticPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPhoneticPr)this.get_store().add_element_user(CTWorksheetImpl.PHONETICPR$30);
        }
    }
    
    public void unsetPhoneticPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.PHONETICPR$30, 0);
        }
    }
    
    public List<CTConditionalFormatting> getConditionalFormattingList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class ConditionalFormattingList extends AbstractList<CTConditionalFormatting>
            {
                @Override
                public CTConditionalFormatting get(final int n) {
                    return CTWorksheetImpl.this.getConditionalFormattingArray(n);
                }
                
                @Override
                public CTConditionalFormatting set(final int n, final CTConditionalFormatting ctConditionalFormatting) {
                    final CTConditionalFormatting conditionalFormattingArray = CTWorksheetImpl.this.getConditionalFormattingArray(n);
                    CTWorksheetImpl.this.setConditionalFormattingArray(n, ctConditionalFormatting);
                    return conditionalFormattingArray;
                }
                
                @Override
                public void add(final int n, final CTConditionalFormatting ctConditionalFormatting) {
                    CTWorksheetImpl.this.insertNewConditionalFormatting(n).set(ctConditionalFormatting);
                }
                
                @Override
                public CTConditionalFormatting remove(final int n) {
                    final CTConditionalFormatting conditionalFormattingArray = CTWorksheetImpl.this.getConditionalFormattingArray(n);
                    CTWorksheetImpl.this.removeConditionalFormatting(n);
                    return conditionalFormattingArray;
                }
                
                @Override
                public int size() {
                    return CTWorksheetImpl.this.sizeOfConditionalFormattingArray();
                }
            }
            return new ConditionalFormattingList();
        }
    }
    
    public CTConditionalFormatting[] getConditionalFormattingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTWorksheetImpl.CONDITIONALFORMATTING$32, list);
            final CTConditionalFormatting[] array = new CTConditionalFormatting[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTConditionalFormatting getConditionalFormattingArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTConditionalFormatting ctConditionalFormatting = (CTConditionalFormatting)this.get_store().find_element_user(CTWorksheetImpl.CONDITIONALFORMATTING$32, n);
            if (ctConditionalFormatting == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctConditionalFormatting;
        }
    }
    
    public int sizeOfConditionalFormattingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.CONDITIONALFORMATTING$32);
        }
    }
    
    public void setConditionalFormattingArray(final CTConditionalFormatting[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTWorksheetImpl.CONDITIONALFORMATTING$32);
        }
    }
    
    public void setConditionalFormattingArray(final int n, final CTConditionalFormatting ctConditionalFormatting) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTConditionalFormatting ctConditionalFormatting2 = (CTConditionalFormatting)this.get_store().find_element_user(CTWorksheetImpl.CONDITIONALFORMATTING$32, n);
            if (ctConditionalFormatting2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctConditionalFormatting2.set(ctConditionalFormatting);
        }
    }
    
    public CTConditionalFormatting insertNewConditionalFormatting(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTConditionalFormatting)this.get_store().insert_element_user(CTWorksheetImpl.CONDITIONALFORMATTING$32, n);
        }
    }
    
    public CTConditionalFormatting addNewConditionalFormatting() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTConditionalFormatting)this.get_store().add_element_user(CTWorksheetImpl.CONDITIONALFORMATTING$32);
        }
    }
    
    public void removeConditionalFormatting(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.CONDITIONALFORMATTING$32, n);
        }
    }
    
    public CTDataValidations getDataValidations() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDataValidations ctDataValidations = (CTDataValidations)this.get_store().find_element_user(CTWorksheetImpl.DATAVALIDATIONS$34, 0);
            if (ctDataValidations == null) {
                return null;
            }
            return ctDataValidations;
        }
    }
    
    public boolean isSetDataValidations() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.DATAVALIDATIONS$34) != 0;
        }
    }
    
    public void setDataValidations(final CTDataValidations ctDataValidations) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTDataValidations ctDataValidations2 = (CTDataValidations)this.get_store().find_element_user(CTWorksheetImpl.DATAVALIDATIONS$34, 0);
            if (ctDataValidations2 == null) {
                ctDataValidations2 = (CTDataValidations)this.get_store().add_element_user(CTWorksheetImpl.DATAVALIDATIONS$34);
            }
            ctDataValidations2.set(ctDataValidations);
        }
    }
    
    public CTDataValidations addNewDataValidations() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDataValidations)this.get_store().add_element_user(CTWorksheetImpl.DATAVALIDATIONS$34);
        }
    }
    
    public void unsetDataValidations() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.DATAVALIDATIONS$34, 0);
        }
    }
    
    public CTHyperlinks getHyperlinks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHyperlinks ctHyperlinks = (CTHyperlinks)this.get_store().find_element_user(CTWorksheetImpl.HYPERLINKS$36, 0);
            if (ctHyperlinks == null) {
                return null;
            }
            return ctHyperlinks;
        }
    }
    
    public boolean isSetHyperlinks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.HYPERLINKS$36) != 0;
        }
    }
    
    public void setHyperlinks(final CTHyperlinks ctHyperlinks) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTHyperlinks ctHyperlinks2 = (CTHyperlinks)this.get_store().find_element_user(CTWorksheetImpl.HYPERLINKS$36, 0);
            if (ctHyperlinks2 == null) {
                ctHyperlinks2 = (CTHyperlinks)this.get_store().add_element_user(CTWorksheetImpl.HYPERLINKS$36);
            }
            ctHyperlinks2.set(ctHyperlinks);
        }
    }
    
    public CTHyperlinks addNewHyperlinks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHyperlinks)this.get_store().add_element_user(CTWorksheetImpl.HYPERLINKS$36);
        }
    }
    
    public void unsetHyperlinks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.HYPERLINKS$36, 0);
        }
    }
    
    public CTPrintOptions getPrintOptions() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPrintOptions ctPrintOptions = (CTPrintOptions)this.get_store().find_element_user(CTWorksheetImpl.PRINTOPTIONS$38, 0);
            if (ctPrintOptions == null) {
                return null;
            }
            return ctPrintOptions;
        }
    }
    
    public boolean isSetPrintOptions() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.PRINTOPTIONS$38) != 0;
        }
    }
    
    public void setPrintOptions(final CTPrintOptions ctPrintOptions) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPrintOptions ctPrintOptions2 = (CTPrintOptions)this.get_store().find_element_user(CTWorksheetImpl.PRINTOPTIONS$38, 0);
            if (ctPrintOptions2 == null) {
                ctPrintOptions2 = (CTPrintOptions)this.get_store().add_element_user(CTWorksheetImpl.PRINTOPTIONS$38);
            }
            ctPrintOptions2.set(ctPrintOptions);
        }
    }
    
    public CTPrintOptions addNewPrintOptions() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPrintOptions)this.get_store().add_element_user(CTWorksheetImpl.PRINTOPTIONS$38);
        }
    }
    
    public void unsetPrintOptions() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.PRINTOPTIONS$38, 0);
        }
    }
    
    public CTPageMargins getPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPageMargins ctPageMargins = (CTPageMargins)this.get_store().find_element_user(CTWorksheetImpl.PAGEMARGINS$40, 0);
            if (ctPageMargins == null) {
                return null;
            }
            return ctPageMargins;
        }
    }
    
    public boolean isSetPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.PAGEMARGINS$40) != 0;
        }
    }
    
    public void setPageMargins(final CTPageMargins ctPageMargins) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPageMargins ctPageMargins2 = (CTPageMargins)this.get_store().find_element_user(CTWorksheetImpl.PAGEMARGINS$40, 0);
            if (ctPageMargins2 == null) {
                ctPageMargins2 = (CTPageMargins)this.get_store().add_element_user(CTWorksheetImpl.PAGEMARGINS$40);
            }
            ctPageMargins2.set(ctPageMargins);
        }
    }
    
    public CTPageMargins addNewPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPageMargins)this.get_store().add_element_user(CTWorksheetImpl.PAGEMARGINS$40);
        }
    }
    
    public void unsetPageMargins() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.PAGEMARGINS$40, 0);
        }
    }
    
    public CTPageSetup getPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPageSetup ctPageSetup = (CTPageSetup)this.get_store().find_element_user(CTWorksheetImpl.PAGESETUP$42, 0);
            if (ctPageSetup == null) {
                return null;
            }
            return ctPageSetup;
        }
    }
    
    public boolean isSetPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.PAGESETUP$42) != 0;
        }
    }
    
    public void setPageSetup(final CTPageSetup ctPageSetup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPageSetup ctPageSetup2 = (CTPageSetup)this.get_store().find_element_user(CTWorksheetImpl.PAGESETUP$42, 0);
            if (ctPageSetup2 == null) {
                ctPageSetup2 = (CTPageSetup)this.get_store().add_element_user(CTWorksheetImpl.PAGESETUP$42);
            }
            ctPageSetup2.set(ctPageSetup);
        }
    }
    
    public CTPageSetup addNewPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPageSetup)this.get_store().add_element_user(CTWorksheetImpl.PAGESETUP$42);
        }
    }
    
    public void unsetPageSetup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.PAGESETUP$42, 0);
        }
    }
    
    public CTHeaderFooter getHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHeaderFooter ctHeaderFooter = (CTHeaderFooter)this.get_store().find_element_user(CTWorksheetImpl.HEADERFOOTER$44, 0);
            if (ctHeaderFooter == null) {
                return null;
            }
            return ctHeaderFooter;
        }
    }
    
    public boolean isSetHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.HEADERFOOTER$44) != 0;
        }
    }
    
    public void setHeaderFooter(final CTHeaderFooter ctHeaderFooter) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTHeaderFooter ctHeaderFooter2 = (CTHeaderFooter)this.get_store().find_element_user(CTWorksheetImpl.HEADERFOOTER$44, 0);
            if (ctHeaderFooter2 == null) {
                ctHeaderFooter2 = (CTHeaderFooter)this.get_store().add_element_user(CTWorksheetImpl.HEADERFOOTER$44);
            }
            ctHeaderFooter2.set(ctHeaderFooter);
        }
    }
    
    public CTHeaderFooter addNewHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHeaderFooter)this.get_store().add_element_user(CTWorksheetImpl.HEADERFOOTER$44);
        }
    }
    
    public void unsetHeaderFooter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.HEADERFOOTER$44, 0);
        }
    }
    
    public CTPageBreak getRowBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPageBreak ctPageBreak = (CTPageBreak)this.get_store().find_element_user(CTWorksheetImpl.ROWBREAKS$46, 0);
            if (ctPageBreak == null) {
                return null;
            }
            return ctPageBreak;
        }
    }
    
    public boolean isSetRowBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.ROWBREAKS$46) != 0;
        }
    }
    
    public void setRowBreaks(final CTPageBreak ctPageBreak) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPageBreak ctPageBreak2 = (CTPageBreak)this.get_store().find_element_user(CTWorksheetImpl.ROWBREAKS$46, 0);
            if (ctPageBreak2 == null) {
                ctPageBreak2 = (CTPageBreak)this.get_store().add_element_user(CTWorksheetImpl.ROWBREAKS$46);
            }
            ctPageBreak2.set(ctPageBreak);
        }
    }
    
    public CTPageBreak addNewRowBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPageBreak)this.get_store().add_element_user(CTWorksheetImpl.ROWBREAKS$46);
        }
    }
    
    public void unsetRowBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.ROWBREAKS$46, 0);
        }
    }
    
    public CTPageBreak getColBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPageBreak ctPageBreak = (CTPageBreak)this.get_store().find_element_user(CTWorksheetImpl.COLBREAKS$48, 0);
            if (ctPageBreak == null) {
                return null;
            }
            return ctPageBreak;
        }
    }
    
    public boolean isSetColBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.COLBREAKS$48) != 0;
        }
    }
    
    public void setColBreaks(final CTPageBreak ctPageBreak) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPageBreak ctPageBreak2 = (CTPageBreak)this.get_store().find_element_user(CTWorksheetImpl.COLBREAKS$48, 0);
            if (ctPageBreak2 == null) {
                ctPageBreak2 = (CTPageBreak)this.get_store().add_element_user(CTWorksheetImpl.COLBREAKS$48);
            }
            ctPageBreak2.set(ctPageBreak);
        }
    }
    
    public CTPageBreak addNewColBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPageBreak)this.get_store().add_element_user(CTWorksheetImpl.COLBREAKS$48);
        }
    }
    
    public void unsetColBreaks() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.COLBREAKS$48, 0);
        }
    }
    
    public CTCustomProperties getCustomProperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCustomProperties ctCustomProperties = (CTCustomProperties)this.get_store().find_element_user(CTWorksheetImpl.CUSTOMPROPERTIES$50, 0);
            if (ctCustomProperties == null) {
                return null;
            }
            return ctCustomProperties;
        }
    }
    
    public boolean isSetCustomProperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.CUSTOMPROPERTIES$50) != 0;
        }
    }
    
    public void setCustomProperties(final CTCustomProperties ctCustomProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCustomProperties ctCustomProperties2 = (CTCustomProperties)this.get_store().find_element_user(CTWorksheetImpl.CUSTOMPROPERTIES$50, 0);
            if (ctCustomProperties2 == null) {
                ctCustomProperties2 = (CTCustomProperties)this.get_store().add_element_user(CTWorksheetImpl.CUSTOMPROPERTIES$50);
            }
            ctCustomProperties2.set((XmlObject)ctCustomProperties);
        }
    }
    
    public CTCustomProperties addNewCustomProperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCustomProperties)this.get_store().add_element_user(CTWorksheetImpl.CUSTOMPROPERTIES$50);
        }
    }
    
    public void unsetCustomProperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.CUSTOMPROPERTIES$50, 0);
        }
    }
    
    public CTCellWatches getCellWatches() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCellWatches ctCellWatches = (CTCellWatches)this.get_store().find_element_user(CTWorksheetImpl.CELLWATCHES$52, 0);
            if (ctCellWatches == null) {
                return null;
            }
            return ctCellWatches;
        }
    }
    
    public boolean isSetCellWatches() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.CELLWATCHES$52) != 0;
        }
    }
    
    public void setCellWatches(final CTCellWatches ctCellWatches) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTCellWatches ctCellWatches2 = (CTCellWatches)this.get_store().find_element_user(CTWorksheetImpl.CELLWATCHES$52, 0);
            if (ctCellWatches2 == null) {
                ctCellWatches2 = (CTCellWatches)this.get_store().add_element_user(CTWorksheetImpl.CELLWATCHES$52);
            }
            ctCellWatches2.set((XmlObject)ctCellWatches);
        }
    }
    
    public CTCellWatches addNewCellWatches() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCellWatches)this.get_store().add_element_user(CTWorksheetImpl.CELLWATCHES$52);
        }
    }
    
    public void unsetCellWatches() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.CELLWATCHES$52, 0);
        }
    }
    
    public CTIgnoredErrors getIgnoredErrors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTIgnoredErrors ctIgnoredErrors = (CTIgnoredErrors)this.get_store().find_element_user(CTWorksheetImpl.IGNOREDERRORS$54, 0);
            if (ctIgnoredErrors == null) {
                return null;
            }
            return ctIgnoredErrors;
        }
    }
    
    public boolean isSetIgnoredErrors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.IGNOREDERRORS$54) != 0;
        }
    }
    
    public void setIgnoredErrors(final CTIgnoredErrors ctIgnoredErrors) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTIgnoredErrors ctIgnoredErrors2 = (CTIgnoredErrors)this.get_store().find_element_user(CTWorksheetImpl.IGNOREDERRORS$54, 0);
            if (ctIgnoredErrors2 == null) {
                ctIgnoredErrors2 = (CTIgnoredErrors)this.get_store().add_element_user(CTWorksheetImpl.IGNOREDERRORS$54);
            }
            ctIgnoredErrors2.set((XmlObject)ctIgnoredErrors);
        }
    }
    
    public CTIgnoredErrors addNewIgnoredErrors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTIgnoredErrors)this.get_store().add_element_user(CTWorksheetImpl.IGNOREDERRORS$54);
        }
    }
    
    public void unsetIgnoredErrors() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.IGNOREDERRORS$54, 0);
        }
    }
    
    public CTSmartTags getSmartTags() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSmartTags ctSmartTags = (CTSmartTags)this.get_store().find_element_user(CTWorksheetImpl.SMARTTAGS$56, 0);
            if (ctSmartTags == null) {
                return null;
            }
            return ctSmartTags;
        }
    }
    
    public boolean isSetSmartTags() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.SMARTTAGS$56) != 0;
        }
    }
    
    public void setSmartTags(final CTSmartTags ctSmartTags) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSmartTags ctSmartTags2 = (CTSmartTags)this.get_store().find_element_user(CTWorksheetImpl.SMARTTAGS$56, 0);
            if (ctSmartTags2 == null) {
                ctSmartTags2 = (CTSmartTags)this.get_store().add_element_user(CTWorksheetImpl.SMARTTAGS$56);
            }
            ctSmartTags2.set((XmlObject)ctSmartTags);
        }
    }
    
    public CTSmartTags addNewSmartTags() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSmartTags)this.get_store().add_element_user(CTWorksheetImpl.SMARTTAGS$56);
        }
    }
    
    public void unsetSmartTags() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.SMARTTAGS$56, 0);
        }
    }
    
    public CTDrawing getDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDrawing ctDrawing = (CTDrawing)this.get_store().find_element_user(CTWorksheetImpl.DRAWING$58, 0);
            if (ctDrawing == null) {
                return null;
            }
            return ctDrawing;
        }
    }
    
    public boolean isSetDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.DRAWING$58) != 0;
        }
    }
    
    public void setDrawing(final CTDrawing ctDrawing) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTDrawing ctDrawing2 = (CTDrawing)this.get_store().find_element_user(CTWorksheetImpl.DRAWING$58, 0);
            if (ctDrawing2 == null) {
                ctDrawing2 = (CTDrawing)this.get_store().add_element_user(CTWorksheetImpl.DRAWING$58);
            }
            ctDrawing2.set(ctDrawing);
        }
    }
    
    public CTDrawing addNewDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDrawing)this.get_store().add_element_user(CTWorksheetImpl.DRAWING$58);
        }
    }
    
    public void unsetDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.DRAWING$58, 0);
        }
    }
    
    public CTLegacyDrawing getLegacyDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLegacyDrawing ctLegacyDrawing = (CTLegacyDrawing)this.get_store().find_element_user(CTWorksheetImpl.LEGACYDRAWING$60, 0);
            if (ctLegacyDrawing == null) {
                return null;
            }
            return ctLegacyDrawing;
        }
    }
    
    public boolean isSetLegacyDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.LEGACYDRAWING$60) != 0;
        }
    }
    
    public void setLegacyDrawing(final CTLegacyDrawing ctLegacyDrawing) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTLegacyDrawing ctLegacyDrawing2 = (CTLegacyDrawing)this.get_store().find_element_user(CTWorksheetImpl.LEGACYDRAWING$60, 0);
            if (ctLegacyDrawing2 == null) {
                ctLegacyDrawing2 = (CTLegacyDrawing)this.get_store().add_element_user(CTWorksheetImpl.LEGACYDRAWING$60);
            }
            ctLegacyDrawing2.set(ctLegacyDrawing);
        }
    }
    
    public CTLegacyDrawing addNewLegacyDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLegacyDrawing)this.get_store().add_element_user(CTWorksheetImpl.LEGACYDRAWING$60);
        }
    }
    
    public void unsetLegacyDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.LEGACYDRAWING$60, 0);
        }
    }
    
    public CTLegacyDrawing getLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLegacyDrawing ctLegacyDrawing = (CTLegacyDrawing)this.get_store().find_element_user(CTWorksheetImpl.LEGACYDRAWINGHF$62, 0);
            if (ctLegacyDrawing == null) {
                return null;
            }
            return ctLegacyDrawing;
        }
    }
    
    public boolean isSetLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.LEGACYDRAWINGHF$62) != 0;
        }
    }
    
    public void setLegacyDrawingHF(final CTLegacyDrawing ctLegacyDrawing) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTLegacyDrawing ctLegacyDrawing2 = (CTLegacyDrawing)this.get_store().find_element_user(CTWorksheetImpl.LEGACYDRAWINGHF$62, 0);
            if (ctLegacyDrawing2 == null) {
                ctLegacyDrawing2 = (CTLegacyDrawing)this.get_store().add_element_user(CTWorksheetImpl.LEGACYDRAWINGHF$62);
            }
            ctLegacyDrawing2.set(ctLegacyDrawing);
        }
    }
    
    public CTLegacyDrawing addNewLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLegacyDrawing)this.get_store().add_element_user(CTWorksheetImpl.LEGACYDRAWINGHF$62);
        }
    }
    
    public void unsetLegacyDrawingHF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.LEGACYDRAWINGHF$62, 0);
        }
    }
    
    public CTSheetBackgroundPicture getPicture() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSheetBackgroundPicture ctSheetBackgroundPicture = (CTSheetBackgroundPicture)this.get_store().find_element_user(CTWorksheetImpl.PICTURE$64, 0);
            if (ctSheetBackgroundPicture == null) {
                return null;
            }
            return ctSheetBackgroundPicture;
        }
    }
    
    public boolean isSetPicture() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.PICTURE$64) != 0;
        }
    }
    
    public void setPicture(final CTSheetBackgroundPicture ctSheetBackgroundPicture) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSheetBackgroundPicture ctSheetBackgroundPicture2 = (CTSheetBackgroundPicture)this.get_store().find_element_user(CTWorksheetImpl.PICTURE$64, 0);
            if (ctSheetBackgroundPicture2 == null) {
                ctSheetBackgroundPicture2 = (CTSheetBackgroundPicture)this.get_store().add_element_user(CTWorksheetImpl.PICTURE$64);
            }
            ctSheetBackgroundPicture2.set((XmlObject)ctSheetBackgroundPicture);
        }
    }
    
    public CTSheetBackgroundPicture addNewPicture() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSheetBackgroundPicture)this.get_store().add_element_user(CTWorksheetImpl.PICTURE$64);
        }
    }
    
    public void unsetPicture() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.PICTURE$64, 0);
        }
    }
    
    public CTOleObjects getOleObjects() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOleObjects ctOleObjects = (CTOleObjects)this.get_store().find_element_user(CTWorksheetImpl.OLEOBJECTS$66, 0);
            if (ctOleObjects == null) {
                return null;
            }
            return ctOleObjects;
        }
    }
    
    public boolean isSetOleObjects() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.OLEOBJECTS$66) != 0;
        }
    }
    
    public void setOleObjects(final CTOleObjects ctOleObjects) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTOleObjects ctOleObjects2 = (CTOleObjects)this.get_store().find_element_user(CTWorksheetImpl.OLEOBJECTS$66, 0);
            if (ctOleObjects2 == null) {
                ctOleObjects2 = (CTOleObjects)this.get_store().add_element_user(CTWorksheetImpl.OLEOBJECTS$66);
            }
            ctOleObjects2.set((XmlObject)ctOleObjects);
        }
    }
    
    public CTOleObjects addNewOleObjects() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOleObjects)this.get_store().add_element_user(CTWorksheetImpl.OLEOBJECTS$66);
        }
    }
    
    public void unsetOleObjects() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.OLEOBJECTS$66, 0);
        }
    }
    
    public CTControls getControls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTControls ctControls = (CTControls)this.get_store().find_element_user(CTWorksheetImpl.CONTROLS$68, 0);
            if (ctControls == null) {
                return null;
            }
            return ctControls;
        }
    }
    
    public boolean isSetControls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.CONTROLS$68) != 0;
        }
    }
    
    public void setControls(final CTControls ctControls) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTControls ctControls2 = (CTControls)this.get_store().find_element_user(CTWorksheetImpl.CONTROLS$68, 0);
            if (ctControls2 == null) {
                ctControls2 = (CTControls)this.get_store().add_element_user(CTWorksheetImpl.CONTROLS$68);
            }
            ctControls2.set((XmlObject)ctControls);
        }
    }
    
    public CTControls addNewControls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTControls)this.get_store().add_element_user(CTWorksheetImpl.CONTROLS$68);
        }
    }
    
    public void unsetControls() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.CONTROLS$68, 0);
        }
    }
    
    public CTWebPublishItems getWebPublishItems() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTWebPublishItems ctWebPublishItems = (CTWebPublishItems)this.get_store().find_element_user(CTWorksheetImpl.WEBPUBLISHITEMS$70, 0);
            if (ctWebPublishItems == null) {
                return null;
            }
            return ctWebPublishItems;
        }
    }
    
    public boolean isSetWebPublishItems() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.WEBPUBLISHITEMS$70) != 0;
        }
    }
    
    public void setWebPublishItems(final CTWebPublishItems ctWebPublishItems) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTWebPublishItems ctWebPublishItems2 = (CTWebPublishItems)this.get_store().find_element_user(CTWorksheetImpl.WEBPUBLISHITEMS$70, 0);
            if (ctWebPublishItems2 == null) {
                ctWebPublishItems2 = (CTWebPublishItems)this.get_store().add_element_user(CTWorksheetImpl.WEBPUBLISHITEMS$70);
            }
            ctWebPublishItems2.set((XmlObject)ctWebPublishItems);
        }
    }
    
    public CTWebPublishItems addNewWebPublishItems() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTWebPublishItems)this.get_store().add_element_user(CTWorksheetImpl.WEBPUBLISHITEMS$70);
        }
    }
    
    public void unsetWebPublishItems() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.WEBPUBLISHITEMS$70, 0);
        }
    }
    
    public CTTableParts getTableParts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTableParts ctTableParts = (CTTableParts)this.get_store().find_element_user(CTWorksheetImpl.TABLEPARTS$72, 0);
            if (ctTableParts == null) {
                return null;
            }
            return ctTableParts;
        }
    }
    
    public boolean isSetTableParts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.TABLEPARTS$72) != 0;
        }
    }
    
    public void setTableParts(final CTTableParts ctTableParts) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTableParts ctTableParts2 = (CTTableParts)this.get_store().find_element_user(CTWorksheetImpl.TABLEPARTS$72, 0);
            if (ctTableParts2 == null) {
                ctTableParts2 = (CTTableParts)this.get_store().add_element_user(CTWorksheetImpl.TABLEPARTS$72);
            }
            ctTableParts2.set(ctTableParts);
        }
    }
    
    public CTTableParts addNewTableParts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTableParts)this.get_store().add_element_user(CTWorksheetImpl.TABLEPARTS$72);
        }
    }
    
    public void unsetTableParts() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.TABLEPARTS$72, 0);
        }
    }
    
    public CTExtensionList getExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTExtensionList list = (CTExtensionList)this.get_store().find_element_user(CTWorksheetImpl.EXTLST$74, 0);
            if (list == null) {
                return null;
            }
            return list;
        }
    }
    
    public boolean isSetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTWorksheetImpl.EXTLST$74) != 0;
        }
    }
    
    public void setExtLst(final CTExtensionList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTExtensionList list2 = (CTExtensionList)this.get_store().find_element_user(CTWorksheetImpl.EXTLST$74, 0);
            if (list2 == null) {
                list2 = (CTExtensionList)this.get_store().add_element_user(CTWorksheetImpl.EXTLST$74);
            }
            list2.set((XmlObject)list);
        }
    }
    
    public CTExtensionList addNewExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTExtensionList)this.get_store().add_element_user(CTWorksheetImpl.EXTLST$74);
        }
    }
    
    public void unsetExtLst() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTWorksheetImpl.EXTLST$74, 0);
        }
    }
    
    static {
        SHEETPR$0 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sheetPr");
        DIMENSION$2 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "dimension");
        SHEETVIEWS$4 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sheetViews");
        SHEETFORMATPR$6 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sheetFormatPr");
        COLS$8 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "cols");
        SHEETDATA$10 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sheetData");
        SHEETCALCPR$12 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sheetCalcPr");
        SHEETPROTECTION$14 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sheetProtection");
        PROTECTEDRANGES$16 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "protectedRanges");
        SCENARIOS$18 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "scenarios");
        AUTOFILTER$20 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "autoFilter");
        SORTSTATE$22 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "sortState");
        DATACONSOLIDATE$24 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "dataConsolidate");
        CUSTOMSHEETVIEWS$26 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "customSheetViews");
        MERGECELLS$28 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "mergeCells");
        PHONETICPR$30 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "phoneticPr");
        CONDITIONALFORMATTING$32 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "conditionalFormatting");
        DATAVALIDATIONS$34 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "dataValidations");
        HYPERLINKS$36 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "hyperlinks");
        PRINTOPTIONS$38 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "printOptions");
        PAGEMARGINS$40 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "pageMargins");
        PAGESETUP$42 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "pageSetup");
        HEADERFOOTER$44 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "headerFooter");
        ROWBREAKS$46 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "rowBreaks");
        COLBREAKS$48 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "colBreaks");
        CUSTOMPROPERTIES$50 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "customProperties");
        CELLWATCHES$52 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "cellWatches");
        IGNOREDERRORS$54 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "ignoredErrors");
        SMARTTAGS$56 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "smartTags");
        DRAWING$58 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "drawing");
        LEGACYDRAWING$60 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "legacyDrawing");
        LEGACYDRAWINGHF$62 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "legacyDrawingHF");
        PICTURE$64 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "picture");
        OLEOBJECTS$66 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "oleObjects");
        CONTROLS$68 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "controls");
        WEBPUBLISHITEMS$70 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "webPublishItems");
        TABLEPARTS$72 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "tableParts");
        EXTLST$74 = new QName("http://schemas.openxmlformats.org/spreadsheetml/2006/main", "extLst");
    }
}
