// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STFontId;
import org.apache.xmlbeans.impl.values.JavaLongHolderEx;

public class STFontIdImpl extends JavaLongHolderEx implements STFontId
{
    public STFontIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STFontIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
