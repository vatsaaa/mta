// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STFontId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STFontId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stfontid9d63type");
    
    public static final class Factory
    {
        public static STFontId newValue(final Object o) {
            return (STFontId)STFontId.type.newValue(o);
        }
        
        public static STFontId newInstance() {
            return (STFontId)XmlBeans.getContextTypeLoader().newInstance(STFontId.type, null);
        }
        
        public static STFontId newInstance(final XmlOptions xmlOptions) {
            return (STFontId)XmlBeans.getContextTypeLoader().newInstance(STFontId.type, xmlOptions);
        }
        
        public static STFontId parse(final String s) throws XmlException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(s, STFontId.type, null);
        }
        
        public static STFontId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(s, STFontId.type, xmlOptions);
        }
        
        public static STFontId parse(final File file) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(file, STFontId.type, null);
        }
        
        public static STFontId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(file, STFontId.type, xmlOptions);
        }
        
        public static STFontId parse(final URL url) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(url, STFontId.type, null);
        }
        
        public static STFontId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(url, STFontId.type, xmlOptions);
        }
        
        public static STFontId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(inputStream, STFontId.type, null);
        }
        
        public static STFontId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(inputStream, STFontId.type, xmlOptions);
        }
        
        public static STFontId parse(final Reader reader) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(reader, STFontId.type, null);
        }
        
        public static STFontId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(reader, STFontId.type, xmlOptions);
        }
        
        public static STFontId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFontId.type, null);
        }
        
        public static STFontId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFontId.type, xmlOptions);
        }
        
        public static STFontId parse(final Node node) throws XmlException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(node, STFontId.type, null);
        }
        
        public static STFontId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(node, STFontId.type, xmlOptions);
        }
        
        @Deprecated
        public static STFontId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFontId.type, null);
        }
        
        @Deprecated
        public static STFontId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STFontId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFontId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFontId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFontId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
