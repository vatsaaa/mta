// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTXmlColumnPr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTXmlColumnPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctxmlcolumnprc14etype");
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    long getMapId();
    
    XmlUnsignedInt xgetMapId();
    
    void setMapId(final long p0);
    
    void xsetMapId(final XmlUnsignedInt p0);
    
    String getXpath();
    
    STXstring xgetXpath();
    
    void setXpath(final String p0);
    
    void xsetXpath(final STXstring p0);
    
    boolean getDenormalized();
    
    XmlBoolean xgetDenormalized();
    
    boolean isSetDenormalized();
    
    void setDenormalized(final boolean p0);
    
    void xsetDenormalized(final XmlBoolean p0);
    
    void unsetDenormalized();
    
    STXmlDataType.Enum getXmlDataType();
    
    STXmlDataType xgetXmlDataType();
    
    void setXmlDataType(final STXmlDataType.Enum p0);
    
    void xsetXmlDataType(final STXmlDataType p0);
    
    public static final class Factory
    {
        public static CTXmlColumnPr newInstance() {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().newInstance(CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr newInstance(final XmlOptions xmlOptions) {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().newInstance(CTXmlColumnPr.type, xmlOptions);
        }
        
        public static CTXmlColumnPr parse(final String s) throws XmlException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(s, CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(s, CTXmlColumnPr.type, xmlOptions);
        }
        
        public static CTXmlColumnPr parse(final File file) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(file, CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(file, CTXmlColumnPr.type, xmlOptions);
        }
        
        public static CTXmlColumnPr parse(final URL url) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(url, CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(url, CTXmlColumnPr.type, xmlOptions);
        }
        
        public static CTXmlColumnPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTXmlColumnPr.type, xmlOptions);
        }
        
        public static CTXmlColumnPr parse(final Reader reader) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(reader, CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(reader, CTXmlColumnPr.type, xmlOptions);
        }
        
        public static CTXmlColumnPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTXmlColumnPr.type, xmlOptions);
        }
        
        public static CTXmlColumnPr parse(final Node node) throws XmlException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(node, CTXmlColumnPr.type, null);
        }
        
        public static CTXmlColumnPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(node, CTXmlColumnPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTXmlColumnPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTXmlColumnPr.type, null);
        }
        
        @Deprecated
        public static CTXmlColumnPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTXmlColumnPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTXmlColumnPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTXmlColumnPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTXmlColumnPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
