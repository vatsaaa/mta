// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface MapInfoDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(MapInfoDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("mapinfo5715doctype");
    
    CTMapInfo getMapInfo();
    
    void setMapInfo(final CTMapInfo p0);
    
    CTMapInfo addNewMapInfo();
    
    public static final class Factory
    {
        public static MapInfoDocument newInstance() {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().newInstance(MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument newInstance(final XmlOptions xmlOptions) {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().newInstance(MapInfoDocument.type, xmlOptions);
        }
        
        public static MapInfoDocument parse(final String s) throws XmlException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(s, MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(s, MapInfoDocument.type, xmlOptions);
        }
        
        public static MapInfoDocument parse(final File file) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(file, MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(file, MapInfoDocument.type, xmlOptions);
        }
        
        public static MapInfoDocument parse(final URL url) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(url, MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(url, MapInfoDocument.type, xmlOptions);
        }
        
        public static MapInfoDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(inputStream, MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(inputStream, MapInfoDocument.type, xmlOptions);
        }
        
        public static MapInfoDocument parse(final Reader reader) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(reader, MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(reader, MapInfoDocument.type, xmlOptions);
        }
        
        public static MapInfoDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, MapInfoDocument.type, xmlOptions);
        }
        
        public static MapInfoDocument parse(final Node node) throws XmlException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(node, MapInfoDocument.type, null);
        }
        
        public static MapInfoDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(node, MapInfoDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static MapInfoDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, MapInfoDocument.type, null);
        }
        
        @Deprecated
        public static MapInfoDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (MapInfoDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, MapInfoDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, MapInfoDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, MapInfoDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
