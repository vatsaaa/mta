// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlAnySimpleType;

public interface STSqref extends XmlAnySimpleType
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STSqref.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stsqrefb044type");
    
    List getListValue();
    
    List xgetListValue();
    
    void setListValue(final List p0);
    
    @Deprecated
    List listValue();
    
    @Deprecated
    List xlistValue();
    
    @Deprecated
    void set(final List p0);
    
    public static final class Factory
    {
        public static STSqref newValue(final Object o) {
            return (STSqref)STSqref.type.newValue(o);
        }
        
        public static STSqref newInstance() {
            return (STSqref)XmlBeans.getContextTypeLoader().newInstance(STSqref.type, null);
        }
        
        public static STSqref newInstance(final XmlOptions xmlOptions) {
            return (STSqref)XmlBeans.getContextTypeLoader().newInstance(STSqref.type, xmlOptions);
        }
        
        public static STSqref parse(final String s) throws XmlException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(s, STSqref.type, null);
        }
        
        public static STSqref parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(s, STSqref.type, xmlOptions);
        }
        
        public static STSqref parse(final File file) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(file, STSqref.type, null);
        }
        
        public static STSqref parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(file, STSqref.type, xmlOptions);
        }
        
        public static STSqref parse(final URL url) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(url, STSqref.type, null);
        }
        
        public static STSqref parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(url, STSqref.type, xmlOptions);
        }
        
        public static STSqref parse(final InputStream inputStream) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(inputStream, STSqref.type, null);
        }
        
        public static STSqref parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(inputStream, STSqref.type, xmlOptions);
        }
        
        public static STSqref parse(final Reader reader) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(reader, STSqref.type, null);
        }
        
        public static STSqref parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(reader, STSqref.type, xmlOptions);
        }
        
        public static STSqref parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSqref.type, null);
        }
        
        public static STSqref parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSqref.type, xmlOptions);
        }
        
        public static STSqref parse(final Node node) throws XmlException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(node, STSqref.type, null);
        }
        
        public static STSqref parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(node, STSqref.type, xmlOptions);
        }
        
        @Deprecated
        public static STSqref parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSqref.type, null);
        }
        
        @Deprecated
        public static STSqref parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STSqref)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSqref.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSqref.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSqref.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
