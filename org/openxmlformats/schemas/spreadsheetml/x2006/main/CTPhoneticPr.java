// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPhoneticPr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPhoneticPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctphoneticpr898btype");
    
    long getFontId();
    
    STFontId xgetFontId();
    
    void setFontId(final long p0);
    
    void xsetFontId(final STFontId p0);
    
    STPhoneticType.Enum getType();
    
    STPhoneticType xgetType();
    
    boolean isSetType();
    
    void setType(final STPhoneticType.Enum p0);
    
    void xsetType(final STPhoneticType p0);
    
    void unsetType();
    
    STPhoneticAlignment.Enum getAlignment();
    
    STPhoneticAlignment xgetAlignment();
    
    boolean isSetAlignment();
    
    void setAlignment(final STPhoneticAlignment.Enum p0);
    
    void xsetAlignment(final STPhoneticAlignment p0);
    
    void unsetAlignment();
    
    public static final class Factory
    {
        public static CTPhoneticPr newInstance() {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().newInstance(CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr newInstance(final XmlOptions xmlOptions) {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().newInstance(CTPhoneticPr.type, xmlOptions);
        }
        
        public static CTPhoneticPr parse(final String s) throws XmlException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(s, CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(s, CTPhoneticPr.type, xmlOptions);
        }
        
        public static CTPhoneticPr parse(final File file) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(file, CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(file, CTPhoneticPr.type, xmlOptions);
        }
        
        public static CTPhoneticPr parse(final URL url) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(url, CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(url, CTPhoneticPr.type, xmlOptions);
        }
        
        public static CTPhoneticPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTPhoneticPr.type, xmlOptions);
        }
        
        public static CTPhoneticPr parse(final Reader reader) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(reader, CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(reader, CTPhoneticPr.type, xmlOptions);
        }
        
        public static CTPhoneticPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPhoneticPr.type, xmlOptions);
        }
        
        public static CTPhoneticPr parse(final Node node) throws XmlException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(node, CTPhoneticPr.type, null);
        }
        
        public static CTPhoneticPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(node, CTPhoneticPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPhoneticPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPhoneticPr.type, null);
        }
        
        @Deprecated
        public static CTPhoneticPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPhoneticPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPhoneticPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPhoneticPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPhoneticPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
