// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface TableDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(TableDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("table0b99doctype");
    
    CTTable getTable();
    
    void setTable(final CTTable p0);
    
    CTTable addNewTable();
    
    public static final class Factory
    {
        public static TableDocument newInstance() {
            return (TableDocument)XmlBeans.getContextTypeLoader().newInstance(TableDocument.type, null);
        }
        
        public static TableDocument newInstance(final XmlOptions xmlOptions) {
            return (TableDocument)XmlBeans.getContextTypeLoader().newInstance(TableDocument.type, xmlOptions);
        }
        
        public static TableDocument parse(final String s) throws XmlException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(s, TableDocument.type, null);
        }
        
        public static TableDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(s, TableDocument.type, xmlOptions);
        }
        
        public static TableDocument parse(final File file) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(file, TableDocument.type, null);
        }
        
        public static TableDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(file, TableDocument.type, xmlOptions);
        }
        
        public static TableDocument parse(final URL url) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(url, TableDocument.type, null);
        }
        
        public static TableDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(url, TableDocument.type, xmlOptions);
        }
        
        public static TableDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(inputStream, TableDocument.type, null);
        }
        
        public static TableDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(inputStream, TableDocument.type, xmlOptions);
        }
        
        public static TableDocument parse(final Reader reader) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(reader, TableDocument.type, null);
        }
        
        public static TableDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(reader, TableDocument.type, xmlOptions);
        }
        
        public static TableDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, TableDocument.type, null);
        }
        
        public static TableDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, TableDocument.type, xmlOptions);
        }
        
        public static TableDocument parse(final Node node) throws XmlException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(node, TableDocument.type, null);
        }
        
        public static TableDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(node, TableDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static TableDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, TableDocument.type, null);
        }
        
        @Deprecated
        public static TableDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (TableDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, TableDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, TableDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, TableDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
