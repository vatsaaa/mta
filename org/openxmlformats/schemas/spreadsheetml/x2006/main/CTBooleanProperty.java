// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTBooleanProperty extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTBooleanProperty.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctbooleanproperty1f3ctype");
    
    boolean getVal();
    
    XmlBoolean xgetVal();
    
    boolean isSetVal();
    
    void setVal(final boolean p0);
    
    void xsetVal(final XmlBoolean p0);
    
    void unsetVal();
    
    public static final class Factory
    {
        public static CTBooleanProperty newInstance() {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().newInstance(CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty newInstance(final XmlOptions xmlOptions) {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().newInstance(CTBooleanProperty.type, xmlOptions);
        }
        
        public static CTBooleanProperty parse(final String s) throws XmlException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(s, CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(s, CTBooleanProperty.type, xmlOptions);
        }
        
        public static CTBooleanProperty parse(final File file) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(file, CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(file, CTBooleanProperty.type, xmlOptions);
        }
        
        public static CTBooleanProperty parse(final URL url) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(url, CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(url, CTBooleanProperty.type, xmlOptions);
        }
        
        public static CTBooleanProperty parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(inputStream, CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(inputStream, CTBooleanProperty.type, xmlOptions);
        }
        
        public static CTBooleanProperty parse(final Reader reader) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(reader, CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(reader, CTBooleanProperty.type, xmlOptions);
        }
        
        public static CTBooleanProperty parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBooleanProperty.type, xmlOptions);
        }
        
        public static CTBooleanProperty parse(final Node node) throws XmlException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(node, CTBooleanProperty.type, null);
        }
        
        public static CTBooleanProperty parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(node, CTBooleanProperty.type, xmlOptions);
        }
        
        @Deprecated
        public static CTBooleanProperty parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBooleanProperty.type, null);
        }
        
        @Deprecated
        public static CTBooleanProperty parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTBooleanProperty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBooleanProperty.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBooleanProperty.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBooleanProperty.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
