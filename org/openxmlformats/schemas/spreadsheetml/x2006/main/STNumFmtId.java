// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STNumFmtId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STNumFmtId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stnumfmtid76fbtype");
    
    public static final class Factory
    {
        public static STNumFmtId newValue(final Object o) {
            return (STNumFmtId)STNumFmtId.type.newValue(o);
        }
        
        public static STNumFmtId newInstance() {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().newInstance(STNumFmtId.type, null);
        }
        
        public static STNumFmtId newInstance(final XmlOptions xmlOptions) {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().newInstance(STNumFmtId.type, xmlOptions);
        }
        
        public static STNumFmtId parse(final String s) throws XmlException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(s, STNumFmtId.type, null);
        }
        
        public static STNumFmtId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(s, STNumFmtId.type, xmlOptions);
        }
        
        public static STNumFmtId parse(final File file) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(file, STNumFmtId.type, null);
        }
        
        public static STNumFmtId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(file, STNumFmtId.type, xmlOptions);
        }
        
        public static STNumFmtId parse(final URL url) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(url, STNumFmtId.type, null);
        }
        
        public static STNumFmtId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(url, STNumFmtId.type, xmlOptions);
        }
        
        public static STNumFmtId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(inputStream, STNumFmtId.type, null);
        }
        
        public static STNumFmtId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(inputStream, STNumFmtId.type, xmlOptions);
        }
        
        public static STNumFmtId parse(final Reader reader) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(reader, STNumFmtId.type, null);
        }
        
        public static STNumFmtId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(reader, STNumFmtId.type, xmlOptions);
        }
        
        public static STNumFmtId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STNumFmtId.type, null);
        }
        
        public static STNumFmtId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STNumFmtId.type, xmlOptions);
        }
        
        public static STNumFmtId parse(final Node node) throws XmlException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(node, STNumFmtId.type, null);
        }
        
        public static STNumFmtId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(node, STNumFmtId.type, xmlOptions);
        }
        
        @Deprecated
        public static STNumFmtId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STNumFmtId.type, null);
        }
        
        @Deprecated
        public static STNumFmtId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STNumFmtId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STNumFmtId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STNumFmtId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STNumFmtId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
