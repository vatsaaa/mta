// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedInt;

public interface STDxfId extends XmlUnsignedInt
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STDxfId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stdxfid9fdctype");
    
    public static final class Factory
    {
        public static STDxfId newValue(final Object o) {
            return (STDxfId)STDxfId.type.newValue(o);
        }
        
        public static STDxfId newInstance() {
            return (STDxfId)XmlBeans.getContextTypeLoader().newInstance(STDxfId.type, null);
        }
        
        public static STDxfId newInstance(final XmlOptions xmlOptions) {
            return (STDxfId)XmlBeans.getContextTypeLoader().newInstance(STDxfId.type, xmlOptions);
        }
        
        public static STDxfId parse(final String s) throws XmlException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(s, STDxfId.type, null);
        }
        
        public static STDxfId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(s, STDxfId.type, xmlOptions);
        }
        
        public static STDxfId parse(final File file) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(file, STDxfId.type, null);
        }
        
        public static STDxfId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(file, STDxfId.type, xmlOptions);
        }
        
        public static STDxfId parse(final URL url) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(url, STDxfId.type, null);
        }
        
        public static STDxfId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(url, STDxfId.type, xmlOptions);
        }
        
        public static STDxfId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(inputStream, STDxfId.type, null);
        }
        
        public static STDxfId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(inputStream, STDxfId.type, xmlOptions);
        }
        
        public static STDxfId parse(final Reader reader) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(reader, STDxfId.type, null);
        }
        
        public static STDxfId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(reader, STDxfId.type, xmlOptions);
        }
        
        public static STDxfId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STDxfId.type, null);
        }
        
        public static STDxfId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STDxfId.type, xmlOptions);
        }
        
        public static STDxfId parse(final Node node) throws XmlException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(node, STDxfId.type, null);
        }
        
        public static STDxfId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(node, STDxfId.type, xmlOptions);
        }
        
        @Deprecated
        public static STDxfId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STDxfId.type, null);
        }
        
        @Deprecated
        public static STDxfId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STDxfId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STDxfId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STDxfId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STDxfId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
