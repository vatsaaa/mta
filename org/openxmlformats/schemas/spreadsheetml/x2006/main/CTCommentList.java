// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTCommentList extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTCommentList.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcommentlist7a3ctype");
    
    List<CTComment> getCommentList();
    
    @Deprecated
    CTComment[] getCommentArray();
    
    CTComment getCommentArray(final int p0);
    
    int sizeOfCommentArray();
    
    void setCommentArray(final CTComment[] p0);
    
    void setCommentArray(final int p0, final CTComment p1);
    
    CTComment insertNewComment(final int p0);
    
    CTComment addNewComment();
    
    void removeComment(final int p0);
    
    public static final class Factory
    {
        public static CTCommentList newInstance() {
            return (CTCommentList)XmlBeans.getContextTypeLoader().newInstance(CTCommentList.type, null);
        }
        
        public static CTCommentList newInstance(final XmlOptions xmlOptions) {
            return (CTCommentList)XmlBeans.getContextTypeLoader().newInstance(CTCommentList.type, xmlOptions);
        }
        
        public static CTCommentList parse(final String s) throws XmlException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(s, CTCommentList.type, null);
        }
        
        public static CTCommentList parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(s, CTCommentList.type, xmlOptions);
        }
        
        public static CTCommentList parse(final File file) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(file, CTCommentList.type, null);
        }
        
        public static CTCommentList parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(file, CTCommentList.type, xmlOptions);
        }
        
        public static CTCommentList parse(final URL url) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(url, CTCommentList.type, null);
        }
        
        public static CTCommentList parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(url, CTCommentList.type, xmlOptions);
        }
        
        public static CTCommentList parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(inputStream, CTCommentList.type, null);
        }
        
        public static CTCommentList parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(inputStream, CTCommentList.type, xmlOptions);
        }
        
        public static CTCommentList parse(final Reader reader) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(reader, CTCommentList.type, null);
        }
        
        public static CTCommentList parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(reader, CTCommentList.type, xmlOptions);
        }
        
        public static CTCommentList parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCommentList.type, null);
        }
        
        public static CTCommentList parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTCommentList.type, xmlOptions);
        }
        
        public static CTCommentList parse(final Node node) throws XmlException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(node, CTCommentList.type, null);
        }
        
        public static CTCommentList parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(node, CTCommentList.type, xmlOptions);
        }
        
        @Deprecated
        public static CTCommentList parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCommentList.type, null);
        }
        
        @Deprecated
        public static CTCommentList parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTCommentList)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTCommentList.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCommentList.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTCommentList.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
