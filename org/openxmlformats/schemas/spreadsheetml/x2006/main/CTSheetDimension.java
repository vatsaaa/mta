// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSheetDimension extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSheetDimension.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsheetdimensiond310type");
    
    String getRef();
    
    STRef xgetRef();
    
    void setRef(final String p0);
    
    void xsetRef(final STRef p0);
    
    public static final class Factory
    {
        public static CTSheetDimension newInstance() {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().newInstance(CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension newInstance(final XmlOptions xmlOptions) {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().newInstance(CTSheetDimension.type, xmlOptions);
        }
        
        public static CTSheetDimension parse(final String s) throws XmlException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(s, CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(s, CTSheetDimension.type, xmlOptions);
        }
        
        public static CTSheetDimension parse(final File file) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(file, CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(file, CTSheetDimension.type, xmlOptions);
        }
        
        public static CTSheetDimension parse(final URL url) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(url, CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(url, CTSheetDimension.type, xmlOptions);
        }
        
        public static CTSheetDimension parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(inputStream, CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(inputStream, CTSheetDimension.type, xmlOptions);
        }
        
        public static CTSheetDimension parse(final Reader reader) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(reader, CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(reader, CTSheetDimension.type, xmlOptions);
        }
        
        public static CTSheetDimension parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSheetDimension.type, xmlOptions);
        }
        
        public static CTSheetDimension parse(final Node node) throws XmlException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(node, CTSheetDimension.type, null);
        }
        
        public static CTSheetDimension parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(node, CTSheetDimension.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSheetDimension parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSheetDimension.type, null);
        }
        
        @Deprecated
        public static CTSheetDimension parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSheetDimension)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSheetDimension.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSheetDimension.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSheetDimension.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
