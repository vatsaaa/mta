// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSst extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSst.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsst44f3type");
    
    List<CTRst> getSiList();
    
    @Deprecated
    CTRst[] getSiArray();
    
    CTRst getSiArray(final int p0);
    
    int sizeOfSiArray();
    
    void setSiArray(final CTRst[] p0);
    
    void setSiArray(final int p0, final CTRst p1);
    
    CTRst insertNewSi(final int p0);
    
    CTRst addNewSi();
    
    void removeSi(final int p0);
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    long getCount();
    
    XmlUnsignedInt xgetCount();
    
    boolean isSetCount();
    
    void setCount(final long p0);
    
    void xsetCount(final XmlUnsignedInt p0);
    
    void unsetCount();
    
    long getUniqueCount();
    
    XmlUnsignedInt xgetUniqueCount();
    
    boolean isSetUniqueCount();
    
    void setUniqueCount(final long p0);
    
    void xsetUniqueCount(final XmlUnsignedInt p0);
    
    void unsetUniqueCount();
    
    public static final class Factory
    {
        public static CTSst newInstance() {
            return (CTSst)XmlBeans.getContextTypeLoader().newInstance(CTSst.type, null);
        }
        
        public static CTSst newInstance(final XmlOptions xmlOptions) {
            return (CTSst)XmlBeans.getContextTypeLoader().newInstance(CTSst.type, xmlOptions);
        }
        
        public static CTSst parse(final String s) throws XmlException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(s, CTSst.type, null);
        }
        
        public static CTSst parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(s, CTSst.type, xmlOptions);
        }
        
        public static CTSst parse(final File file) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(file, CTSst.type, null);
        }
        
        public static CTSst parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(file, CTSst.type, xmlOptions);
        }
        
        public static CTSst parse(final URL url) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(url, CTSst.type, null);
        }
        
        public static CTSst parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(url, CTSst.type, xmlOptions);
        }
        
        public static CTSst parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(inputStream, CTSst.type, null);
        }
        
        public static CTSst parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(inputStream, CTSst.type, xmlOptions);
        }
        
        public static CTSst parse(final Reader reader) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(reader, CTSst.type, null);
        }
        
        public static CTSst parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(reader, CTSst.type, xmlOptions);
        }
        
        public static CTSst parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSst.type, null);
        }
        
        public static CTSst parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSst.type, xmlOptions);
        }
        
        public static CTSst parse(final Node node) throws XmlException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(node, CTSst.type, null);
        }
        
        public static CTSst parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(node, CTSst.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSst parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSst.type, null);
        }
        
        @Deprecated
        public static CTSst parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSst)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSst.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSst.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSst.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
