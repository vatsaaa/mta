// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTableParts extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTableParts.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttablepartsf6bbtype");
    
    List<CTTablePart> getTablePartList();
    
    @Deprecated
    CTTablePart[] getTablePartArray();
    
    CTTablePart getTablePartArray(final int p0);
    
    int sizeOfTablePartArray();
    
    void setTablePartArray(final CTTablePart[] p0);
    
    void setTablePartArray(final int p0, final CTTablePart p1);
    
    CTTablePart insertNewTablePart(final int p0);
    
    CTTablePart addNewTablePart();
    
    void removeTablePart(final int p0);
    
    long getCount();
    
    XmlUnsignedInt xgetCount();
    
    boolean isSetCount();
    
    void setCount(final long p0);
    
    void xsetCount(final XmlUnsignedInt p0);
    
    void unsetCount();
    
    public static final class Factory
    {
        public static CTTableParts newInstance() {
            return (CTTableParts)XmlBeans.getContextTypeLoader().newInstance(CTTableParts.type, null);
        }
        
        public static CTTableParts newInstance(final XmlOptions xmlOptions) {
            return (CTTableParts)XmlBeans.getContextTypeLoader().newInstance(CTTableParts.type, xmlOptions);
        }
        
        public static CTTableParts parse(final String s) throws XmlException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(s, CTTableParts.type, null);
        }
        
        public static CTTableParts parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(s, CTTableParts.type, xmlOptions);
        }
        
        public static CTTableParts parse(final File file) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(file, CTTableParts.type, null);
        }
        
        public static CTTableParts parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(file, CTTableParts.type, xmlOptions);
        }
        
        public static CTTableParts parse(final URL url) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(url, CTTableParts.type, null);
        }
        
        public static CTTableParts parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(url, CTTableParts.type, xmlOptions);
        }
        
        public static CTTableParts parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableParts.type, null);
        }
        
        public static CTTableParts parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableParts.type, xmlOptions);
        }
        
        public static CTTableParts parse(final Reader reader) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(reader, CTTableParts.type, null);
        }
        
        public static CTTableParts parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(reader, CTTableParts.type, xmlOptions);
        }
        
        public static CTTableParts parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableParts.type, null);
        }
        
        public static CTTableParts parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableParts.type, xmlOptions);
        }
        
        public static CTTableParts parse(final Node node) throws XmlException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(node, CTTableParts.type, null);
        }
        
        public static CTTableParts parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(node, CTTableParts.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTableParts parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableParts.type, null);
        }
        
        @Deprecated
        public static CTTableParts parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTableParts)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableParts.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableParts.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableParts.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
