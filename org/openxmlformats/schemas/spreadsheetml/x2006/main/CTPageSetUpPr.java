// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPageSetUpPr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPageSetUpPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpagesetuppr24cftype");
    
    boolean getAutoPageBreaks();
    
    XmlBoolean xgetAutoPageBreaks();
    
    boolean isSetAutoPageBreaks();
    
    void setAutoPageBreaks(final boolean p0);
    
    void xsetAutoPageBreaks(final XmlBoolean p0);
    
    void unsetAutoPageBreaks();
    
    boolean getFitToPage();
    
    XmlBoolean xgetFitToPage();
    
    boolean isSetFitToPage();
    
    void setFitToPage(final boolean p0);
    
    void xsetFitToPage(final XmlBoolean p0);
    
    void unsetFitToPage();
    
    public static final class Factory
    {
        public static CTPageSetUpPr newInstance() {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().newInstance(CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr newInstance(final XmlOptions xmlOptions) {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().newInstance(CTPageSetUpPr.type, xmlOptions);
        }
        
        public static CTPageSetUpPr parse(final String s) throws XmlException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(s, CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(s, CTPageSetUpPr.type, xmlOptions);
        }
        
        public static CTPageSetUpPr parse(final File file) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(file, CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(file, CTPageSetUpPr.type, xmlOptions);
        }
        
        public static CTPageSetUpPr parse(final URL url) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(url, CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(url, CTPageSetUpPr.type, xmlOptions);
        }
        
        public static CTPageSetUpPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTPageSetUpPr.type, xmlOptions);
        }
        
        public static CTPageSetUpPr parse(final Reader reader) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(reader, CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(reader, CTPageSetUpPr.type, xmlOptions);
        }
        
        public static CTPageSetUpPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPageSetUpPr.type, xmlOptions);
        }
        
        public static CTPageSetUpPr parse(final Node node) throws XmlException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(node, CTPageSetUpPr.type, null);
        }
        
        public static CTPageSetUpPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(node, CTPageSetUpPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPageSetUpPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPageSetUpPr.type, null);
        }
        
        @Deprecated
        public static CTPageSetUpPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPageSetUpPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPageSetUpPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPageSetUpPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPageSetUpPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
