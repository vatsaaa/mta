// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlDouble;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTFontSize extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTFontSize.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfontsizeb3b9type");
    
    double getVal();
    
    XmlDouble xgetVal();
    
    void setVal(final double p0);
    
    void xsetVal(final XmlDouble p0);
    
    public static final class Factory
    {
        public static CTFontSize newInstance() {
            return (CTFontSize)XmlBeans.getContextTypeLoader().newInstance(CTFontSize.type, null);
        }
        
        public static CTFontSize newInstance(final XmlOptions xmlOptions) {
            return (CTFontSize)XmlBeans.getContextTypeLoader().newInstance(CTFontSize.type, xmlOptions);
        }
        
        public static CTFontSize parse(final String s) throws XmlException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(s, CTFontSize.type, null);
        }
        
        public static CTFontSize parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(s, CTFontSize.type, xmlOptions);
        }
        
        public static CTFontSize parse(final File file) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(file, CTFontSize.type, null);
        }
        
        public static CTFontSize parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(file, CTFontSize.type, xmlOptions);
        }
        
        public static CTFontSize parse(final URL url) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(url, CTFontSize.type, null);
        }
        
        public static CTFontSize parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(url, CTFontSize.type, xmlOptions);
        }
        
        public static CTFontSize parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(inputStream, CTFontSize.type, null);
        }
        
        public static CTFontSize parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(inputStream, CTFontSize.type, xmlOptions);
        }
        
        public static CTFontSize parse(final Reader reader) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(reader, CTFontSize.type, null);
        }
        
        public static CTFontSize parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(reader, CTFontSize.type, xmlOptions);
        }
        
        public static CTFontSize parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFontSize.type, null);
        }
        
        public static CTFontSize parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFontSize.type, xmlOptions);
        }
        
        public static CTFontSize parse(final Node node) throws XmlException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(node, CTFontSize.type, null);
        }
        
        public static CTFontSize parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(node, CTFontSize.type, xmlOptions);
        }
        
        @Deprecated
        public static CTFontSize parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFontSize.type, null);
        }
        
        @Deprecated
        public static CTFontSize parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTFontSize)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFontSize.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFontSize.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFontSize.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
