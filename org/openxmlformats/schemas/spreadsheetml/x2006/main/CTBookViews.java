// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTBookViews extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTBookViews.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctbookviewsb864type");
    
    List<CTBookView> getWorkbookViewList();
    
    @Deprecated
    CTBookView[] getWorkbookViewArray();
    
    CTBookView getWorkbookViewArray(final int p0);
    
    int sizeOfWorkbookViewArray();
    
    void setWorkbookViewArray(final CTBookView[] p0);
    
    void setWorkbookViewArray(final int p0, final CTBookView p1);
    
    CTBookView insertNewWorkbookView(final int p0);
    
    CTBookView addNewWorkbookView();
    
    void removeWorkbookView(final int p0);
    
    public static final class Factory
    {
        public static CTBookViews newInstance() {
            return (CTBookViews)XmlBeans.getContextTypeLoader().newInstance(CTBookViews.type, null);
        }
        
        public static CTBookViews newInstance(final XmlOptions xmlOptions) {
            return (CTBookViews)XmlBeans.getContextTypeLoader().newInstance(CTBookViews.type, xmlOptions);
        }
        
        public static CTBookViews parse(final String s) throws XmlException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(s, CTBookViews.type, null);
        }
        
        public static CTBookViews parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(s, CTBookViews.type, xmlOptions);
        }
        
        public static CTBookViews parse(final File file) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(file, CTBookViews.type, null);
        }
        
        public static CTBookViews parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(file, CTBookViews.type, xmlOptions);
        }
        
        public static CTBookViews parse(final URL url) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(url, CTBookViews.type, null);
        }
        
        public static CTBookViews parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(url, CTBookViews.type, xmlOptions);
        }
        
        public static CTBookViews parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(inputStream, CTBookViews.type, null);
        }
        
        public static CTBookViews parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(inputStream, CTBookViews.type, xmlOptions);
        }
        
        public static CTBookViews parse(final Reader reader) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(reader, CTBookViews.type, null);
        }
        
        public static CTBookViews parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(reader, CTBookViews.type, xmlOptions);
        }
        
        public static CTBookViews parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBookViews.type, null);
        }
        
        public static CTBookViews parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBookViews.type, xmlOptions);
        }
        
        public static CTBookViews parse(final Node node) throws XmlException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(node, CTBookViews.type, null);
        }
        
        public static CTBookViews parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(node, CTBookViews.type, xmlOptions);
        }
        
        @Deprecated
        public static CTBookViews parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBookViews.type, null);
        }
        
        @Deprecated
        public static CTBookViews parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTBookViews)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBookViews.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBookViews.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBookViews.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
