// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTFills extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTFills.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfills2c6ftype");
    
    List<CTFill> getFillList();
    
    @Deprecated
    CTFill[] getFillArray();
    
    CTFill getFillArray(final int p0);
    
    int sizeOfFillArray();
    
    void setFillArray(final CTFill[] p0);
    
    void setFillArray(final int p0, final CTFill p1);
    
    CTFill insertNewFill(final int p0);
    
    CTFill addNewFill();
    
    void removeFill(final int p0);
    
    long getCount();
    
    XmlUnsignedInt xgetCount();
    
    boolean isSetCount();
    
    void setCount(final long p0);
    
    void xsetCount(final XmlUnsignedInt p0);
    
    void unsetCount();
    
    public static final class Factory
    {
        public static CTFills newInstance() {
            return (CTFills)XmlBeans.getContextTypeLoader().newInstance(CTFills.type, null);
        }
        
        public static CTFills newInstance(final XmlOptions xmlOptions) {
            return (CTFills)XmlBeans.getContextTypeLoader().newInstance(CTFills.type, xmlOptions);
        }
        
        public static CTFills parse(final String s) throws XmlException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(s, CTFills.type, null);
        }
        
        public static CTFills parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(s, CTFills.type, xmlOptions);
        }
        
        public static CTFills parse(final File file) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(file, CTFills.type, null);
        }
        
        public static CTFills parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(file, CTFills.type, xmlOptions);
        }
        
        public static CTFills parse(final URL url) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(url, CTFills.type, null);
        }
        
        public static CTFills parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(url, CTFills.type, xmlOptions);
        }
        
        public static CTFills parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(inputStream, CTFills.type, null);
        }
        
        public static CTFills parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(inputStream, CTFills.type, xmlOptions);
        }
        
        public static CTFills parse(final Reader reader) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(reader, CTFills.type, null);
        }
        
        public static CTFills parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(reader, CTFills.type, xmlOptions);
        }
        
        public static CTFills parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFills.type, null);
        }
        
        public static CTFills parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFills.type, xmlOptions);
        }
        
        public static CTFills parse(final Node node) throws XmlException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(node, CTFills.type, null);
        }
        
        public static CTFills parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(node, CTFills.type, xmlOptions);
        }
        
        @Deprecated
        public static CTFills parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFills.type, null);
        }
        
        @Deprecated
        public static CTFills parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTFills)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFills.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFills.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFills.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
