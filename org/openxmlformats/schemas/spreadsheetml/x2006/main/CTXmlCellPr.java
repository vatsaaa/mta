// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTXmlCellPr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTXmlCellPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctxmlcellprf1datype");
    
    CTXmlPr getXmlPr();
    
    void setXmlPr(final CTXmlPr p0);
    
    CTXmlPr addNewXmlPr();
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    long getId();
    
    XmlUnsignedInt xgetId();
    
    void setId(final long p0);
    
    void xsetId(final XmlUnsignedInt p0);
    
    String getUniqueName();
    
    STXstring xgetUniqueName();
    
    boolean isSetUniqueName();
    
    void setUniqueName(final String p0);
    
    void xsetUniqueName(final STXstring p0);
    
    void unsetUniqueName();
    
    public static final class Factory
    {
        public static CTXmlCellPr newInstance() {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().newInstance(CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr newInstance(final XmlOptions xmlOptions) {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().newInstance(CTXmlCellPr.type, xmlOptions);
        }
        
        public static CTXmlCellPr parse(final String s) throws XmlException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(s, CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(s, CTXmlCellPr.type, xmlOptions);
        }
        
        public static CTXmlCellPr parse(final File file) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(file, CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(file, CTXmlCellPr.type, xmlOptions);
        }
        
        public static CTXmlCellPr parse(final URL url) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(url, CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(url, CTXmlCellPr.type, xmlOptions);
        }
        
        public static CTXmlCellPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTXmlCellPr.type, xmlOptions);
        }
        
        public static CTXmlCellPr parse(final Reader reader) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(reader, CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(reader, CTXmlCellPr.type, xmlOptions);
        }
        
        public static CTXmlCellPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTXmlCellPr.type, xmlOptions);
        }
        
        public static CTXmlCellPr parse(final Node node) throws XmlException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(node, CTXmlCellPr.type, null);
        }
        
        public static CTXmlCellPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(node, CTXmlCellPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTXmlCellPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTXmlCellPr.type, null);
        }
        
        @Deprecated
        public static CTXmlCellPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTXmlCellPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTXmlCellPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTXmlCellPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTXmlCellPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
