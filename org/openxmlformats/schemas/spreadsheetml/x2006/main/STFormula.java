// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STFormula extends STXstring
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STFormula.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stformula7e35type");
    
    public static final class Factory
    {
        public static STFormula newValue(final Object o) {
            return (STFormula)STFormula.type.newValue(o);
        }
        
        public static STFormula newInstance() {
            return (STFormula)XmlBeans.getContextTypeLoader().newInstance(STFormula.type, null);
        }
        
        public static STFormula newInstance(final XmlOptions xmlOptions) {
            return (STFormula)XmlBeans.getContextTypeLoader().newInstance(STFormula.type, xmlOptions);
        }
        
        public static STFormula parse(final String s) throws XmlException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(s, STFormula.type, null);
        }
        
        public static STFormula parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(s, STFormula.type, xmlOptions);
        }
        
        public static STFormula parse(final File file) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(file, STFormula.type, null);
        }
        
        public static STFormula parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(file, STFormula.type, xmlOptions);
        }
        
        public static STFormula parse(final URL url) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(url, STFormula.type, null);
        }
        
        public static STFormula parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(url, STFormula.type, xmlOptions);
        }
        
        public static STFormula parse(final InputStream inputStream) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(inputStream, STFormula.type, null);
        }
        
        public static STFormula parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(inputStream, STFormula.type, xmlOptions);
        }
        
        public static STFormula parse(final Reader reader) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(reader, STFormula.type, null);
        }
        
        public static STFormula parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(reader, STFormula.type, xmlOptions);
        }
        
        public static STFormula parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFormula.type, null);
        }
        
        public static STFormula parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFormula.type, xmlOptions);
        }
        
        public static STFormula parse(final Node node) throws XmlException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(node, STFormula.type, null);
        }
        
        public static STFormula parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(node, STFormula.type, xmlOptions);
        }
        
        @Deprecated
        public static STFormula parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFormula.type, null);
        }
        
        @Deprecated
        public static STFormula parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STFormula)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFormula.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFormula.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFormula.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
