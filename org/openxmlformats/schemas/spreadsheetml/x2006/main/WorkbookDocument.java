// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface WorkbookDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(WorkbookDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("workbookec17doctype");
    
    CTWorkbook getWorkbook();
    
    void setWorkbook(final CTWorkbook p0);
    
    CTWorkbook addNewWorkbook();
    
    public static final class Factory
    {
        public static WorkbookDocument newInstance() {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().newInstance(WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument newInstance(final XmlOptions xmlOptions) {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().newInstance(WorkbookDocument.type, xmlOptions);
        }
        
        public static WorkbookDocument parse(final String s) throws XmlException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(s, WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(s, WorkbookDocument.type, xmlOptions);
        }
        
        public static WorkbookDocument parse(final File file) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(file, WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(file, WorkbookDocument.type, xmlOptions);
        }
        
        public static WorkbookDocument parse(final URL url) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(url, WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(url, WorkbookDocument.type, xmlOptions);
        }
        
        public static WorkbookDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(inputStream, WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(inputStream, WorkbookDocument.type, xmlOptions);
        }
        
        public static WorkbookDocument parse(final Reader reader) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(reader, WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(reader, WorkbookDocument.type, xmlOptions);
        }
        
        public static WorkbookDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, WorkbookDocument.type, xmlOptions);
        }
        
        public static WorkbookDocument parse(final Node node) throws XmlException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(node, WorkbookDocument.type, null);
        }
        
        public static WorkbookDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(node, WorkbookDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static WorkbookDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, WorkbookDocument.type, null);
        }
        
        @Deprecated
        public static WorkbookDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (WorkbookDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, WorkbookDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, WorkbookDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, WorkbookDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
