// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTOutlinePr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTOutlinePr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctoutlineprc483type");
    
    boolean getApplyStyles();
    
    XmlBoolean xgetApplyStyles();
    
    boolean isSetApplyStyles();
    
    void setApplyStyles(final boolean p0);
    
    void xsetApplyStyles(final XmlBoolean p0);
    
    void unsetApplyStyles();
    
    boolean getSummaryBelow();
    
    XmlBoolean xgetSummaryBelow();
    
    boolean isSetSummaryBelow();
    
    void setSummaryBelow(final boolean p0);
    
    void xsetSummaryBelow(final XmlBoolean p0);
    
    void unsetSummaryBelow();
    
    boolean getSummaryRight();
    
    XmlBoolean xgetSummaryRight();
    
    boolean isSetSummaryRight();
    
    void setSummaryRight(final boolean p0);
    
    void xsetSummaryRight(final XmlBoolean p0);
    
    void unsetSummaryRight();
    
    boolean getShowOutlineSymbols();
    
    XmlBoolean xgetShowOutlineSymbols();
    
    boolean isSetShowOutlineSymbols();
    
    void setShowOutlineSymbols(final boolean p0);
    
    void xsetShowOutlineSymbols(final XmlBoolean p0);
    
    void unsetShowOutlineSymbols();
    
    public static final class Factory
    {
        public static CTOutlinePr newInstance() {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().newInstance(CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr newInstance(final XmlOptions xmlOptions) {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().newInstance(CTOutlinePr.type, xmlOptions);
        }
        
        public static CTOutlinePr parse(final String s) throws XmlException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(s, CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(s, CTOutlinePr.type, xmlOptions);
        }
        
        public static CTOutlinePr parse(final File file) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(file, CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(file, CTOutlinePr.type, xmlOptions);
        }
        
        public static CTOutlinePr parse(final URL url) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(url, CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(url, CTOutlinePr.type, xmlOptions);
        }
        
        public static CTOutlinePr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(inputStream, CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(inputStream, CTOutlinePr.type, xmlOptions);
        }
        
        public static CTOutlinePr parse(final Reader reader) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(reader, CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(reader, CTOutlinePr.type, xmlOptions);
        }
        
        public static CTOutlinePr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTOutlinePr.type, xmlOptions);
        }
        
        public static CTOutlinePr parse(final Node node) throws XmlException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(node, CTOutlinePr.type, null);
        }
        
        public static CTOutlinePr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(node, CTOutlinePr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTOutlinePr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTOutlinePr.type, null);
        }
        
        @Deprecated
        public static CTOutlinePr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTOutlinePr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTOutlinePr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTOutlinePr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTOutlinePr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
