// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface WorksheetDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(WorksheetDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("worksheetf539doctype");
    
    CTWorksheet getWorksheet();
    
    void setWorksheet(final CTWorksheet p0);
    
    CTWorksheet addNewWorksheet();
    
    public static final class Factory
    {
        public static WorksheetDocument newInstance() {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().newInstance(WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument newInstance(final XmlOptions xmlOptions) {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().newInstance(WorksheetDocument.type, xmlOptions);
        }
        
        public static WorksheetDocument parse(final String s) throws XmlException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(s, WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(s, WorksheetDocument.type, xmlOptions);
        }
        
        public static WorksheetDocument parse(final File file) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(file, WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(file, WorksheetDocument.type, xmlOptions);
        }
        
        public static WorksheetDocument parse(final URL url) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(url, WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(url, WorksheetDocument.type, xmlOptions);
        }
        
        public static WorksheetDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(inputStream, WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(inputStream, WorksheetDocument.type, xmlOptions);
        }
        
        public static WorksheetDocument parse(final Reader reader) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(reader, WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(reader, WorksheetDocument.type, xmlOptions);
        }
        
        public static WorksheetDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, WorksheetDocument.type, xmlOptions);
        }
        
        public static WorksheetDocument parse(final Node node) throws XmlException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(node, WorksheetDocument.type, null);
        }
        
        public static WorksheetDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(node, WorksheetDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static WorksheetDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, WorksheetDocument.type, null);
        }
        
        @Deprecated
        public static WorksheetDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (WorksheetDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, WorksheetDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, WorksheetDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, WorksheetDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
