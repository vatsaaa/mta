// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTLegacyDrawing extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTLegacyDrawing.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctlegacydrawing49f4type");
    
    String getId();
    
    STRelationshipId xgetId();
    
    void setId(final String p0);
    
    void xsetId(final STRelationshipId p0);
    
    public static final class Factory
    {
        public static CTLegacyDrawing newInstance() {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().newInstance(CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing newInstance(final XmlOptions xmlOptions) {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().newInstance(CTLegacyDrawing.type, xmlOptions);
        }
        
        public static CTLegacyDrawing parse(final String s) throws XmlException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(s, CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(s, CTLegacyDrawing.type, xmlOptions);
        }
        
        public static CTLegacyDrawing parse(final File file) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(file, CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(file, CTLegacyDrawing.type, xmlOptions);
        }
        
        public static CTLegacyDrawing parse(final URL url) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(url, CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(url, CTLegacyDrawing.type, xmlOptions);
        }
        
        public static CTLegacyDrawing parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(inputStream, CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(inputStream, CTLegacyDrawing.type, xmlOptions);
        }
        
        public static CTLegacyDrawing parse(final Reader reader) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(reader, CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(reader, CTLegacyDrawing.type, xmlOptions);
        }
        
        public static CTLegacyDrawing parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTLegacyDrawing.type, xmlOptions);
        }
        
        public static CTLegacyDrawing parse(final Node node) throws XmlException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(node, CTLegacyDrawing.type, null);
        }
        
        public static CTLegacyDrawing parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(node, CTLegacyDrawing.type, xmlOptions);
        }
        
        @Deprecated
        public static CTLegacyDrawing parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLegacyDrawing.type, null);
        }
        
        @Deprecated
        public static CTLegacyDrawing parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTLegacyDrawing)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTLegacyDrawing.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLegacyDrawing.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTLegacyDrawing.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
