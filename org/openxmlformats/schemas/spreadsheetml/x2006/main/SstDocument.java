// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface SstDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(SstDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sstf81fdoctype");
    
    CTSst getSst();
    
    void setSst(final CTSst p0);
    
    CTSst addNewSst();
    
    public static final class Factory
    {
        public static SstDocument newInstance() {
            return (SstDocument)XmlBeans.getContextTypeLoader().newInstance(SstDocument.type, null);
        }
        
        public static SstDocument newInstance(final XmlOptions xmlOptions) {
            return (SstDocument)XmlBeans.getContextTypeLoader().newInstance(SstDocument.type, xmlOptions);
        }
        
        public static SstDocument parse(final String s) throws XmlException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(s, SstDocument.type, null);
        }
        
        public static SstDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(s, SstDocument.type, xmlOptions);
        }
        
        public static SstDocument parse(final File file) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(file, SstDocument.type, null);
        }
        
        public static SstDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(file, SstDocument.type, xmlOptions);
        }
        
        public static SstDocument parse(final URL url) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(url, SstDocument.type, null);
        }
        
        public static SstDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(url, SstDocument.type, xmlOptions);
        }
        
        public static SstDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(inputStream, SstDocument.type, null);
        }
        
        public static SstDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(inputStream, SstDocument.type, xmlOptions);
        }
        
        public static SstDocument parse(final Reader reader) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(reader, SstDocument.type, null);
        }
        
        public static SstDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(reader, SstDocument.type, xmlOptions);
        }
        
        public static SstDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, SstDocument.type, null);
        }
        
        public static SstDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, SstDocument.type, xmlOptions);
        }
        
        public static SstDocument parse(final Node node) throws XmlException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(node, SstDocument.type, null);
        }
        
        public static SstDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(node, SstDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static SstDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, SstDocument.type, null);
        }
        
        @Deprecated
        public static SstDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (SstDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, SstDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, SstDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, SstDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
