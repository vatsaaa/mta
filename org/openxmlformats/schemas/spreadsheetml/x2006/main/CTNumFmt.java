// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNumFmt extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNumFmt.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnumfmt3870type");
    
    long getNumFmtId();
    
    STNumFmtId xgetNumFmtId();
    
    void setNumFmtId(final long p0);
    
    void xsetNumFmtId(final STNumFmtId p0);
    
    String getFormatCode();
    
    STXstring xgetFormatCode();
    
    void setFormatCode(final String p0);
    
    void xsetFormatCode(final STXstring p0);
    
    public static final class Factory
    {
        public static CTNumFmt newInstance() {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().newInstance(CTNumFmt.type, null);
        }
        
        public static CTNumFmt newInstance(final XmlOptions xmlOptions) {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().newInstance(CTNumFmt.type, xmlOptions);
        }
        
        public static CTNumFmt parse(final String s) throws XmlException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(s, CTNumFmt.type, null);
        }
        
        public static CTNumFmt parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(s, CTNumFmt.type, xmlOptions);
        }
        
        public static CTNumFmt parse(final File file) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(file, CTNumFmt.type, null);
        }
        
        public static CTNumFmt parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(file, CTNumFmt.type, xmlOptions);
        }
        
        public static CTNumFmt parse(final URL url) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(url, CTNumFmt.type, null);
        }
        
        public static CTNumFmt parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(url, CTNumFmt.type, xmlOptions);
        }
        
        public static CTNumFmt parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumFmt.type, null);
        }
        
        public static CTNumFmt parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumFmt.type, xmlOptions);
        }
        
        public static CTNumFmt parse(final Reader reader) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(reader, CTNumFmt.type, null);
        }
        
        public static CTNumFmt parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(reader, CTNumFmt.type, xmlOptions);
        }
        
        public static CTNumFmt parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumFmt.type, null);
        }
        
        public static CTNumFmt parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumFmt.type, xmlOptions);
        }
        
        public static CTNumFmt parse(final Node node) throws XmlException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(node, CTNumFmt.type, null);
        }
        
        public static CTNumFmt parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(node, CTNumFmt.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNumFmt parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumFmt.type, null);
        }
        
        @Deprecated
        public static CTNumFmt parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNumFmt)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumFmt.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumFmt.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumFmt.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
