// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CalcChainDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CalcChainDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("calcchainfc37doctype");
    
    CTCalcChain getCalcChain();
    
    void setCalcChain(final CTCalcChain p0);
    
    CTCalcChain addNewCalcChain();
    
    public static final class Factory
    {
        public static CalcChainDocument newInstance() {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().newInstance(CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument newInstance(final XmlOptions xmlOptions) {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().newInstance(CalcChainDocument.type, xmlOptions);
        }
        
        public static CalcChainDocument parse(final String s) throws XmlException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(s, CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(s, CalcChainDocument.type, xmlOptions);
        }
        
        public static CalcChainDocument parse(final File file) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(file, CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(file, CalcChainDocument.type, xmlOptions);
        }
        
        public static CalcChainDocument parse(final URL url) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(url, CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(url, CalcChainDocument.type, xmlOptions);
        }
        
        public static CalcChainDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CalcChainDocument.type, xmlOptions);
        }
        
        public static CalcChainDocument parse(final Reader reader) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(reader, CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(reader, CalcChainDocument.type, xmlOptions);
        }
        
        public static CalcChainDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CalcChainDocument.type, xmlOptions);
        }
        
        public static CalcChainDocument parse(final Node node) throws XmlException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(node, CalcChainDocument.type, null);
        }
        
        public static CalcChainDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(node, CalcChainDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static CalcChainDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CalcChainDocument.type, null);
        }
        
        @Deprecated
        public static CalcChainDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CalcChainDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CalcChainDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CalcChainDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CalcChainDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
