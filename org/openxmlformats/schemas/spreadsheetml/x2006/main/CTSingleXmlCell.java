// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSingleXmlCell extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSingleXmlCell.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsinglexmlcell7790type");
    
    CTXmlCellPr getXmlCellPr();
    
    void setXmlCellPr(final CTXmlCellPr p0);
    
    CTXmlCellPr addNewXmlCellPr();
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    long getId();
    
    XmlUnsignedInt xgetId();
    
    void setId(final long p0);
    
    void xsetId(final XmlUnsignedInt p0);
    
    String getR();
    
    STCellRef xgetR();
    
    void setR(final String p0);
    
    void xsetR(final STCellRef p0);
    
    long getConnectionId();
    
    XmlUnsignedInt xgetConnectionId();
    
    void setConnectionId(final long p0);
    
    void xsetConnectionId(final XmlUnsignedInt p0);
    
    public static final class Factory
    {
        public static CTSingleXmlCell newInstance() {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().newInstance(CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell newInstance(final XmlOptions xmlOptions) {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().newInstance(CTSingleXmlCell.type, xmlOptions);
        }
        
        public static CTSingleXmlCell parse(final String s) throws XmlException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(s, CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(s, CTSingleXmlCell.type, xmlOptions);
        }
        
        public static CTSingleXmlCell parse(final File file) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(file, CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(file, CTSingleXmlCell.type, xmlOptions);
        }
        
        public static CTSingleXmlCell parse(final URL url) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(url, CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(url, CTSingleXmlCell.type, xmlOptions);
        }
        
        public static CTSingleXmlCell parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(inputStream, CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(inputStream, CTSingleXmlCell.type, xmlOptions);
        }
        
        public static CTSingleXmlCell parse(final Reader reader) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(reader, CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(reader, CTSingleXmlCell.type, xmlOptions);
        }
        
        public static CTSingleXmlCell parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSingleXmlCell.type, xmlOptions);
        }
        
        public static CTSingleXmlCell parse(final Node node) throws XmlException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(node, CTSingleXmlCell.type, null);
        }
        
        public static CTSingleXmlCell parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(node, CTSingleXmlCell.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSingleXmlCell parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSingleXmlCell.type, null);
        }
        
        @Deprecated
        public static CTSingleXmlCell parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSingleXmlCell)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSingleXmlCell.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSingleXmlCell.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSingleXmlCell.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
