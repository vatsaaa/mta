// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlHexBinary;

public interface STUnsignedShortHex extends XmlHexBinary
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STUnsignedShortHex.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stunsignedshorthex0bedtype");
    
    public static final class Factory
    {
        public static STUnsignedShortHex newValue(final Object o) {
            return (STUnsignedShortHex)STUnsignedShortHex.type.newValue(o);
        }
        
        public static STUnsignedShortHex newInstance() {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().newInstance(STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex newInstance(final XmlOptions xmlOptions) {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().newInstance(STUnsignedShortHex.type, xmlOptions);
        }
        
        public static STUnsignedShortHex parse(final String s) throws XmlException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(s, STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(s, STUnsignedShortHex.type, xmlOptions);
        }
        
        public static STUnsignedShortHex parse(final File file) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(file, STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(file, STUnsignedShortHex.type, xmlOptions);
        }
        
        public static STUnsignedShortHex parse(final URL url) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(url, STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(url, STUnsignedShortHex.type, xmlOptions);
        }
        
        public static STUnsignedShortHex parse(final InputStream inputStream) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(inputStream, STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(inputStream, STUnsignedShortHex.type, xmlOptions);
        }
        
        public static STUnsignedShortHex parse(final Reader reader) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(reader, STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(reader, STUnsignedShortHex.type, xmlOptions);
        }
        
        public static STUnsignedShortHex parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STUnsignedShortHex.type, xmlOptions);
        }
        
        public static STUnsignedShortHex parse(final Node node) throws XmlException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(node, STUnsignedShortHex.type, null);
        }
        
        public static STUnsignedShortHex parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(node, STUnsignedShortHex.type, xmlOptions);
        }
        
        @Deprecated
        public static STUnsignedShortHex parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STUnsignedShortHex.type, null);
        }
        
        @Deprecated
        public static STUnsignedShortHex parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STUnsignedShortHex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STUnsignedShortHex.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STUnsignedShortHex.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STUnsignedShortHex.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
