// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTMergeCells extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTMergeCells.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctmergecells1242type");
    
    List<CTMergeCell> getMergeCellList();
    
    @Deprecated
    CTMergeCell[] getMergeCellArray();
    
    CTMergeCell getMergeCellArray(final int p0);
    
    int sizeOfMergeCellArray();
    
    void setMergeCellArray(final CTMergeCell[] p0);
    
    void setMergeCellArray(final int p0, final CTMergeCell p1);
    
    CTMergeCell insertNewMergeCell(final int p0);
    
    CTMergeCell addNewMergeCell();
    
    void removeMergeCell(final int p0);
    
    long getCount();
    
    XmlUnsignedInt xgetCount();
    
    boolean isSetCount();
    
    void setCount(final long p0);
    
    void xsetCount(final XmlUnsignedInt p0);
    
    void unsetCount();
    
    public static final class Factory
    {
        public static CTMergeCells newInstance() {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().newInstance(CTMergeCells.type, null);
        }
        
        public static CTMergeCells newInstance(final XmlOptions xmlOptions) {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().newInstance(CTMergeCells.type, xmlOptions);
        }
        
        public static CTMergeCells parse(final String s) throws XmlException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(s, CTMergeCells.type, null);
        }
        
        public static CTMergeCells parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(s, CTMergeCells.type, xmlOptions);
        }
        
        public static CTMergeCells parse(final File file) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(file, CTMergeCells.type, null);
        }
        
        public static CTMergeCells parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(file, CTMergeCells.type, xmlOptions);
        }
        
        public static CTMergeCells parse(final URL url) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(url, CTMergeCells.type, null);
        }
        
        public static CTMergeCells parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(url, CTMergeCells.type, xmlOptions);
        }
        
        public static CTMergeCells parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(inputStream, CTMergeCells.type, null);
        }
        
        public static CTMergeCells parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(inputStream, CTMergeCells.type, xmlOptions);
        }
        
        public static CTMergeCells parse(final Reader reader) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(reader, CTMergeCells.type, null);
        }
        
        public static CTMergeCells parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(reader, CTMergeCells.type, xmlOptions);
        }
        
        public static CTMergeCells parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMergeCells.type, null);
        }
        
        public static CTMergeCells parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMergeCells.type, xmlOptions);
        }
        
        public static CTMergeCells parse(final Node node) throws XmlException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(node, CTMergeCells.type, null);
        }
        
        public static CTMergeCells parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(node, CTMergeCells.type, xmlOptions);
        }
        
        @Deprecated
        public static CTMergeCells parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMergeCells.type, null);
        }
        
        @Deprecated
        public static CTMergeCells parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTMergeCells)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMergeCells.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMergeCells.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMergeCells.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
