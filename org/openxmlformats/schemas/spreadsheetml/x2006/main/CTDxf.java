// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTDxf extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTDxf.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctdxfa3b1type");
    
    CTFont getFont();
    
    boolean isSetFont();
    
    void setFont(final CTFont p0);
    
    CTFont addNewFont();
    
    void unsetFont();
    
    CTNumFmt getNumFmt();
    
    boolean isSetNumFmt();
    
    void setNumFmt(final CTNumFmt p0);
    
    CTNumFmt addNewNumFmt();
    
    void unsetNumFmt();
    
    CTFill getFill();
    
    boolean isSetFill();
    
    void setFill(final CTFill p0);
    
    CTFill addNewFill();
    
    void unsetFill();
    
    CTCellAlignment getAlignment();
    
    boolean isSetAlignment();
    
    void setAlignment(final CTCellAlignment p0);
    
    CTCellAlignment addNewAlignment();
    
    void unsetAlignment();
    
    CTBorder getBorder();
    
    boolean isSetBorder();
    
    void setBorder(final CTBorder p0);
    
    CTBorder addNewBorder();
    
    void unsetBorder();
    
    CTCellProtection getProtection();
    
    boolean isSetProtection();
    
    void setProtection(final CTCellProtection p0);
    
    CTCellProtection addNewProtection();
    
    void unsetProtection();
    
    CTExtensionList getExtLst();
    
    boolean isSetExtLst();
    
    void setExtLst(final CTExtensionList p0);
    
    CTExtensionList addNewExtLst();
    
    void unsetExtLst();
    
    public static final class Factory
    {
        public static CTDxf newInstance() {
            return (CTDxf)XmlBeans.getContextTypeLoader().newInstance(CTDxf.type, null);
        }
        
        public static CTDxf newInstance(final XmlOptions xmlOptions) {
            return (CTDxf)XmlBeans.getContextTypeLoader().newInstance(CTDxf.type, xmlOptions);
        }
        
        public static CTDxf parse(final String s) throws XmlException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(s, CTDxf.type, null);
        }
        
        public static CTDxf parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(s, CTDxf.type, xmlOptions);
        }
        
        public static CTDxf parse(final File file) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(file, CTDxf.type, null);
        }
        
        public static CTDxf parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(file, CTDxf.type, xmlOptions);
        }
        
        public static CTDxf parse(final URL url) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(url, CTDxf.type, null);
        }
        
        public static CTDxf parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(url, CTDxf.type, xmlOptions);
        }
        
        public static CTDxf parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(inputStream, CTDxf.type, null);
        }
        
        public static CTDxf parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(inputStream, CTDxf.type, xmlOptions);
        }
        
        public static CTDxf parse(final Reader reader) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(reader, CTDxf.type, null);
        }
        
        public static CTDxf parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(reader, CTDxf.type, xmlOptions);
        }
        
        public static CTDxf parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDxf.type, null);
        }
        
        public static CTDxf parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDxf.type, xmlOptions);
        }
        
        public static CTDxf parse(final Node node) throws XmlException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(node, CTDxf.type, null);
        }
        
        public static CTDxf parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(node, CTDxf.type, xmlOptions);
        }
        
        @Deprecated
        public static CTDxf parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDxf.type, null);
        }
        
        @Deprecated
        public static CTDxf parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTDxf)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDxf.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDxf.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDxf.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
