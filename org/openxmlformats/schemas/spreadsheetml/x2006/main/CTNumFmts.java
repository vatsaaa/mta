// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNumFmts extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNumFmts.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnumfmtsb58btype");
    
    List<CTNumFmt> getNumFmtList();
    
    @Deprecated
    CTNumFmt[] getNumFmtArray();
    
    CTNumFmt getNumFmtArray(final int p0);
    
    int sizeOfNumFmtArray();
    
    void setNumFmtArray(final CTNumFmt[] p0);
    
    void setNumFmtArray(final int p0, final CTNumFmt p1);
    
    CTNumFmt insertNewNumFmt(final int p0);
    
    CTNumFmt addNewNumFmt();
    
    void removeNumFmt(final int p0);
    
    long getCount();
    
    XmlUnsignedInt xgetCount();
    
    boolean isSetCount();
    
    void setCount(final long p0);
    
    void xsetCount(final XmlUnsignedInt p0);
    
    void unsetCount();
    
    public static final class Factory
    {
        public static CTNumFmts newInstance() {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().newInstance(CTNumFmts.type, null);
        }
        
        public static CTNumFmts newInstance(final XmlOptions xmlOptions) {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().newInstance(CTNumFmts.type, xmlOptions);
        }
        
        public static CTNumFmts parse(final String s) throws XmlException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(s, CTNumFmts.type, null);
        }
        
        public static CTNumFmts parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(s, CTNumFmts.type, xmlOptions);
        }
        
        public static CTNumFmts parse(final File file) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(file, CTNumFmts.type, null);
        }
        
        public static CTNumFmts parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(file, CTNumFmts.type, xmlOptions);
        }
        
        public static CTNumFmts parse(final URL url) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(url, CTNumFmts.type, null);
        }
        
        public static CTNumFmts parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(url, CTNumFmts.type, xmlOptions);
        }
        
        public static CTNumFmts parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumFmts.type, null);
        }
        
        public static CTNumFmts parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumFmts.type, xmlOptions);
        }
        
        public static CTNumFmts parse(final Reader reader) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(reader, CTNumFmts.type, null);
        }
        
        public static CTNumFmts parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(reader, CTNumFmts.type, xmlOptions);
        }
        
        public static CTNumFmts parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumFmts.type, null);
        }
        
        public static CTNumFmts parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumFmts.type, xmlOptions);
        }
        
        public static CTNumFmts parse(final Node node) throws XmlException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(node, CTNumFmts.type, null);
        }
        
        public static CTNumFmts parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(node, CTNumFmts.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNumFmts parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumFmts.type, null);
        }
        
        @Deprecated
        public static CTNumFmts parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNumFmts)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumFmts.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumFmts.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumFmts.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
