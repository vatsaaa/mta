// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlUnsignedInt;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTableColumns extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTableColumns.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttablecolumnsebb8type");
    
    List<CTTableColumn> getTableColumnList();
    
    @Deprecated
    CTTableColumn[] getTableColumnArray();
    
    CTTableColumn getTableColumnArray(final int p0);
    
    int sizeOfTableColumnArray();
    
    void setTableColumnArray(final CTTableColumn[] p0);
    
    void setTableColumnArray(final int p0, final CTTableColumn p1);
    
    CTTableColumn insertNewTableColumn(final int p0);
    
    CTTableColumn addNewTableColumn();
    
    void removeTableColumn(final int p0);
    
    long getCount();
    
    XmlUnsignedInt xgetCount();
    
    boolean isSetCount();
    
    void setCount(final long p0);
    
    void xsetCount(final XmlUnsignedInt p0);
    
    void unsetCount();
    
    public static final class Factory
    {
        public static CTTableColumns newInstance() {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().newInstance(CTTableColumns.type, null);
        }
        
        public static CTTableColumns newInstance(final XmlOptions xmlOptions) {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().newInstance(CTTableColumns.type, xmlOptions);
        }
        
        public static CTTableColumns parse(final String s) throws XmlException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(s, CTTableColumns.type, null);
        }
        
        public static CTTableColumns parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(s, CTTableColumns.type, xmlOptions);
        }
        
        public static CTTableColumns parse(final File file) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(file, CTTableColumns.type, null);
        }
        
        public static CTTableColumns parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(file, CTTableColumns.type, xmlOptions);
        }
        
        public static CTTableColumns parse(final URL url) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(url, CTTableColumns.type, null);
        }
        
        public static CTTableColumns parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(url, CTTableColumns.type, xmlOptions);
        }
        
        public static CTTableColumns parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableColumns.type, null);
        }
        
        public static CTTableColumns parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(inputStream, CTTableColumns.type, xmlOptions);
        }
        
        public static CTTableColumns parse(final Reader reader) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(reader, CTTableColumns.type, null);
        }
        
        public static CTTableColumns parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(reader, CTTableColumns.type, xmlOptions);
        }
        
        public static CTTableColumns parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableColumns.type, null);
        }
        
        public static CTTableColumns parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTableColumns.type, xmlOptions);
        }
        
        public static CTTableColumns parse(final Node node) throws XmlException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(node, CTTableColumns.type, null);
        }
        
        public static CTTableColumns parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(node, CTTableColumns.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTableColumns parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableColumns.type, null);
        }
        
        @Deprecated
        public static CTTableColumns parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTableColumns)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTableColumns.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableColumns.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTableColumns.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
