// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface SingleXmlCellsDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(SingleXmlCellsDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("singlexmlcells33bfdoctype");
    
    CTSingleXmlCells getSingleXmlCells();
    
    void setSingleXmlCells(final CTSingleXmlCells p0);
    
    CTSingleXmlCells addNewSingleXmlCells();
    
    public static final class Factory
    {
        public static SingleXmlCellsDocument newInstance() {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().newInstance(SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument newInstance(final XmlOptions xmlOptions) {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().newInstance(SingleXmlCellsDocument.type, xmlOptions);
        }
        
        public static SingleXmlCellsDocument parse(final String s) throws XmlException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(s, SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(s, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        public static SingleXmlCellsDocument parse(final File file) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(file, SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(file, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        public static SingleXmlCellsDocument parse(final URL url) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(url, SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(url, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        public static SingleXmlCellsDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(inputStream, SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(inputStream, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        public static SingleXmlCellsDocument parse(final Reader reader) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(reader, SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(reader, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        public static SingleXmlCellsDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        public static SingleXmlCellsDocument parse(final Node node) throws XmlException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(node, SingleXmlCellsDocument.type, null);
        }
        
        public static SingleXmlCellsDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(node, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static SingleXmlCellsDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, SingleXmlCellsDocument.type, null);
        }
        
        @Deprecated
        public static SingleXmlCellsDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (SingleXmlCellsDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, SingleXmlCellsDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, SingleXmlCellsDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
