// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTHyperlink extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTHyperlink.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cthyperlink0c85type");
    
    String getRef();
    
    STRef xgetRef();
    
    void setRef(final String p0);
    
    void xsetRef(final STRef p0);
    
    String getId();
    
    STRelationshipId xgetId();
    
    boolean isSetId();
    
    void setId(final String p0);
    
    void xsetId(final STRelationshipId p0);
    
    void unsetId();
    
    String getLocation();
    
    STXstring xgetLocation();
    
    boolean isSetLocation();
    
    void setLocation(final String p0);
    
    void xsetLocation(final STXstring p0);
    
    void unsetLocation();
    
    String getTooltip();
    
    STXstring xgetTooltip();
    
    boolean isSetTooltip();
    
    void setTooltip(final String p0);
    
    void xsetTooltip(final STXstring p0);
    
    void unsetTooltip();
    
    String getDisplay();
    
    STXstring xgetDisplay();
    
    boolean isSetDisplay();
    
    void setDisplay(final String p0);
    
    void xsetDisplay(final STXstring p0);
    
    void unsetDisplay();
    
    public static final class Factory
    {
        public static CTHyperlink newInstance() {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().newInstance(CTHyperlink.type, null);
        }
        
        public static CTHyperlink newInstance(final XmlOptions xmlOptions) {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().newInstance(CTHyperlink.type, xmlOptions);
        }
        
        public static CTHyperlink parse(final String s) throws XmlException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(s, CTHyperlink.type, null);
        }
        
        public static CTHyperlink parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(s, CTHyperlink.type, xmlOptions);
        }
        
        public static CTHyperlink parse(final File file) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(file, CTHyperlink.type, null);
        }
        
        public static CTHyperlink parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(file, CTHyperlink.type, xmlOptions);
        }
        
        public static CTHyperlink parse(final URL url) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(url, CTHyperlink.type, null);
        }
        
        public static CTHyperlink parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(url, CTHyperlink.type, xmlOptions);
        }
        
        public static CTHyperlink parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(inputStream, CTHyperlink.type, null);
        }
        
        public static CTHyperlink parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(inputStream, CTHyperlink.type, xmlOptions);
        }
        
        public static CTHyperlink parse(final Reader reader) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(reader, CTHyperlink.type, null);
        }
        
        public static CTHyperlink parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(reader, CTHyperlink.type, xmlOptions);
        }
        
        public static CTHyperlink parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTHyperlink.type, null);
        }
        
        public static CTHyperlink parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTHyperlink.type, xmlOptions);
        }
        
        public static CTHyperlink parse(final Node node) throws XmlException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(node, CTHyperlink.type, null);
        }
        
        public static CTHyperlink parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(node, CTHyperlink.type, xmlOptions);
        }
        
        @Deprecated
        public static CTHyperlink parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTHyperlink.type, null);
        }
        
        @Deprecated
        public static CTHyperlink parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTHyperlink)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTHyperlink.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTHyperlink.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTHyperlink.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
