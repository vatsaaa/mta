// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTMap extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTMap.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctmap023btype");
    
    CTDataBinding getDataBinding();
    
    boolean isSetDataBinding();
    
    void setDataBinding(final CTDataBinding p0);
    
    CTDataBinding addNewDataBinding();
    
    void unsetDataBinding();
    
    long getID();
    
    XmlUnsignedInt xgetID();
    
    void setID(final long p0);
    
    void xsetID(final XmlUnsignedInt p0);
    
    String getName();
    
    XmlString xgetName();
    
    void setName(final String p0);
    
    void xsetName(final XmlString p0);
    
    String getRootElement();
    
    XmlString xgetRootElement();
    
    void setRootElement(final String p0);
    
    void xsetRootElement(final XmlString p0);
    
    String getSchemaID();
    
    XmlString xgetSchemaID();
    
    void setSchemaID(final String p0);
    
    void xsetSchemaID(final XmlString p0);
    
    boolean getShowImportExportValidationErrors();
    
    XmlBoolean xgetShowImportExportValidationErrors();
    
    void setShowImportExportValidationErrors(final boolean p0);
    
    void xsetShowImportExportValidationErrors(final XmlBoolean p0);
    
    boolean getAutoFit();
    
    XmlBoolean xgetAutoFit();
    
    void setAutoFit(final boolean p0);
    
    void xsetAutoFit(final XmlBoolean p0);
    
    boolean getAppend();
    
    XmlBoolean xgetAppend();
    
    void setAppend(final boolean p0);
    
    void xsetAppend(final XmlBoolean p0);
    
    boolean getPreserveSortAFLayout();
    
    XmlBoolean xgetPreserveSortAFLayout();
    
    void setPreserveSortAFLayout(final boolean p0);
    
    void xsetPreserveSortAFLayout(final XmlBoolean p0);
    
    boolean getPreserveFormat();
    
    XmlBoolean xgetPreserveFormat();
    
    void setPreserveFormat(final boolean p0);
    
    void xsetPreserveFormat(final XmlBoolean p0);
    
    public static final class Factory
    {
        public static CTMap newInstance() {
            return (CTMap)XmlBeans.getContextTypeLoader().newInstance(CTMap.type, null);
        }
        
        public static CTMap newInstance(final XmlOptions xmlOptions) {
            return (CTMap)XmlBeans.getContextTypeLoader().newInstance(CTMap.type, xmlOptions);
        }
        
        public static CTMap parse(final String s) throws XmlException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(s, CTMap.type, null);
        }
        
        public static CTMap parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(s, CTMap.type, xmlOptions);
        }
        
        public static CTMap parse(final File file) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(file, CTMap.type, null);
        }
        
        public static CTMap parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(file, CTMap.type, xmlOptions);
        }
        
        public static CTMap parse(final URL url) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(url, CTMap.type, null);
        }
        
        public static CTMap parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(url, CTMap.type, xmlOptions);
        }
        
        public static CTMap parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(inputStream, CTMap.type, null);
        }
        
        public static CTMap parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(inputStream, CTMap.type, xmlOptions);
        }
        
        public static CTMap parse(final Reader reader) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(reader, CTMap.type, null);
        }
        
        public static CTMap parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(reader, CTMap.type, xmlOptions);
        }
        
        public static CTMap parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMap.type, null);
        }
        
        public static CTMap parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMap.type, xmlOptions);
        }
        
        public static CTMap parse(final Node node) throws XmlException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(node, CTMap.type, null);
        }
        
        public static CTMap parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(node, CTMap.type, xmlOptions);
        }
        
        @Deprecated
        public static CTMap parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMap.type, null);
        }
        
        @Deprecated
        public static CTMap parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTMap)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMap.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMap.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMap.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
