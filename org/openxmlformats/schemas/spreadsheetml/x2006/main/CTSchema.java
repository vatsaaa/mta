// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSchema extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSchema.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctschema0e6atype");
    
    String getID();
    
    XmlString xgetID();
    
    void setID(final String p0);
    
    void xsetID(final XmlString p0);
    
    String getSchemaRef();
    
    XmlString xgetSchemaRef();
    
    boolean isSetSchemaRef();
    
    void setSchemaRef(final String p0);
    
    void xsetSchemaRef(final XmlString p0);
    
    void unsetSchemaRef();
    
    String getNamespace();
    
    XmlString xgetNamespace();
    
    boolean isSetNamespace();
    
    void setNamespace(final String p0);
    
    void xsetNamespace(final XmlString p0);
    
    void unsetNamespace();
    
    public static final class Factory
    {
        public static CTSchema newInstance() {
            return (CTSchema)XmlBeans.getContextTypeLoader().newInstance(CTSchema.type, null);
        }
        
        public static CTSchema newInstance(final XmlOptions xmlOptions) {
            return (CTSchema)XmlBeans.getContextTypeLoader().newInstance(CTSchema.type, xmlOptions);
        }
        
        public static CTSchema parse(final String s) throws XmlException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(s, CTSchema.type, null);
        }
        
        public static CTSchema parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(s, CTSchema.type, xmlOptions);
        }
        
        public static CTSchema parse(final File file) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(file, CTSchema.type, null);
        }
        
        public static CTSchema parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(file, CTSchema.type, xmlOptions);
        }
        
        public static CTSchema parse(final URL url) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(url, CTSchema.type, null);
        }
        
        public static CTSchema parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(url, CTSchema.type, xmlOptions);
        }
        
        public static CTSchema parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(inputStream, CTSchema.type, null);
        }
        
        public static CTSchema parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(inputStream, CTSchema.type, xmlOptions);
        }
        
        public static CTSchema parse(final Reader reader) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(reader, CTSchema.type, null);
        }
        
        public static CTSchema parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(reader, CTSchema.type, xmlOptions);
        }
        
        public static CTSchema parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSchema.type, null);
        }
        
        public static CTSchema parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSchema.type, xmlOptions);
        }
        
        public static CTSchema parse(final Node node) throws XmlException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(node, CTSchema.type, null);
        }
        
        public static CTSchema parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(node, CTSchema.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSchema parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSchema.type, null);
        }
        
        @Deprecated
        public static CTSchema parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSchema)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSchema.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSchema.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSchema.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
