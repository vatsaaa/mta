// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTRst extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTRst.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctrsta472type");
    
    String getT();
    
    STXstring xgetT();
    
    boolean isSetT();
    
    void setT(final String p0);
    
    void xsetT(final STXstring p0);
    
    void unsetT();
    
    List<CTRElt> getRList();
    
    @Deprecated
    CTRElt[] getRArray();
    
    CTRElt getRArray(final int p0);
    
    int sizeOfRArray();
    
    void setRArray(final CTRElt[] p0);
    
    void setRArray(final int p0, final CTRElt p1);
    
    CTRElt insertNewR(final int p0);
    
    CTRElt addNewR();
    
    void removeR(final int p0);
    
    List<CTPhoneticRun> getRPhList();
    
    @Deprecated
    CTPhoneticRun[] getRPhArray();
    
    CTPhoneticRun getRPhArray(final int p0);
    
    int sizeOfRPhArray();
    
    void setRPhArray(final CTPhoneticRun[] p0);
    
    void setRPhArray(final int p0, final CTPhoneticRun p1);
    
    CTPhoneticRun insertNewRPh(final int p0);
    
    CTPhoneticRun addNewRPh();
    
    void removeRPh(final int p0);
    
    CTPhoneticPr getPhoneticPr();
    
    boolean isSetPhoneticPr();
    
    void setPhoneticPr(final CTPhoneticPr p0);
    
    CTPhoneticPr addNewPhoneticPr();
    
    void unsetPhoneticPr();
    
    public static final class Factory
    {
        public static CTRst newInstance() {
            return (CTRst)XmlBeans.getContextTypeLoader().newInstance(CTRst.type, null);
        }
        
        public static CTRst newInstance(final XmlOptions xmlOptions) {
            return (CTRst)XmlBeans.getContextTypeLoader().newInstance(CTRst.type, xmlOptions);
        }
        
        public static CTRst parse(final String s) throws XmlException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(s, CTRst.type, null);
        }
        
        public static CTRst parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(s, CTRst.type, xmlOptions);
        }
        
        public static CTRst parse(final File file) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(file, CTRst.type, null);
        }
        
        public static CTRst parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(file, CTRst.type, xmlOptions);
        }
        
        public static CTRst parse(final URL url) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(url, CTRst.type, null);
        }
        
        public static CTRst parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(url, CTRst.type, xmlOptions);
        }
        
        public static CTRst parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(inputStream, CTRst.type, null);
        }
        
        public static CTRst parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(inputStream, CTRst.type, xmlOptions);
        }
        
        public static CTRst parse(final Reader reader) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(reader, CTRst.type, null);
        }
        
        public static CTRst parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(reader, CTRst.type, xmlOptions);
        }
        
        public static CTRst parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTRst.type, null);
        }
        
        public static CTRst parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTRst.type, xmlOptions);
        }
        
        public static CTRst parse(final Node node) throws XmlException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(node, CTRst.type, null);
        }
        
        public static CTRst parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(node, CTRst.type, xmlOptions);
        }
        
        @Deprecated
        public static CTRst parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTRst.type, null);
        }
        
        @Deprecated
        public static CTRst parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTRst)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTRst.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTRst.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTRst.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
