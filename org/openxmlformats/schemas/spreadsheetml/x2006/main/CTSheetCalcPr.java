// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSheetCalcPr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSheetCalcPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsheetcalcprc6d5type");
    
    boolean getFullCalcOnLoad();
    
    XmlBoolean xgetFullCalcOnLoad();
    
    boolean isSetFullCalcOnLoad();
    
    void setFullCalcOnLoad(final boolean p0);
    
    void xsetFullCalcOnLoad(final XmlBoolean p0);
    
    void unsetFullCalcOnLoad();
    
    public static final class Factory
    {
        public static CTSheetCalcPr newInstance() {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().newInstance(CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr newInstance(final XmlOptions xmlOptions) {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().newInstance(CTSheetCalcPr.type, xmlOptions);
        }
        
        public static CTSheetCalcPr parse(final String s) throws XmlException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(s, CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(s, CTSheetCalcPr.type, xmlOptions);
        }
        
        public static CTSheetCalcPr parse(final File file) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(file, CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(file, CTSheetCalcPr.type, xmlOptions);
        }
        
        public static CTSheetCalcPr parse(final URL url) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(url, CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(url, CTSheetCalcPr.type, xmlOptions);
        }
        
        public static CTSheetCalcPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTSheetCalcPr.type, xmlOptions);
        }
        
        public static CTSheetCalcPr parse(final Reader reader) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(reader, CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(reader, CTSheetCalcPr.type, xmlOptions);
        }
        
        public static CTSheetCalcPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSheetCalcPr.type, xmlOptions);
        }
        
        public static CTSheetCalcPr parse(final Node node) throws XmlException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(node, CTSheetCalcPr.type, null);
        }
        
        public static CTSheetCalcPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(node, CTSheetCalcPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSheetCalcPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSheetCalcPr.type, null);
        }
        
        @Deprecated
        public static CTSheetCalcPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSheetCalcPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSheetCalcPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSheetCalcPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSheetCalcPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
