// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSheetPr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSheetPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsheetpr3ae0type");
    
    CTColor getTabColor();
    
    boolean isSetTabColor();
    
    void setTabColor(final CTColor p0);
    
    CTColor addNewTabColor();
    
    void unsetTabColor();
    
    CTOutlinePr getOutlinePr();
    
    boolean isSetOutlinePr();
    
    void setOutlinePr(final CTOutlinePr p0);
    
    CTOutlinePr addNewOutlinePr();
    
    void unsetOutlinePr();
    
    CTPageSetUpPr getPageSetUpPr();
    
    boolean isSetPageSetUpPr();
    
    void setPageSetUpPr(final CTPageSetUpPr p0);
    
    CTPageSetUpPr addNewPageSetUpPr();
    
    void unsetPageSetUpPr();
    
    boolean getSyncHorizontal();
    
    XmlBoolean xgetSyncHorizontal();
    
    boolean isSetSyncHorizontal();
    
    void setSyncHorizontal(final boolean p0);
    
    void xsetSyncHorizontal(final XmlBoolean p0);
    
    void unsetSyncHorizontal();
    
    boolean getSyncVertical();
    
    XmlBoolean xgetSyncVertical();
    
    boolean isSetSyncVertical();
    
    void setSyncVertical(final boolean p0);
    
    void xsetSyncVertical(final XmlBoolean p0);
    
    void unsetSyncVertical();
    
    String getSyncRef();
    
    STRef xgetSyncRef();
    
    boolean isSetSyncRef();
    
    void setSyncRef(final String p0);
    
    void xsetSyncRef(final STRef p0);
    
    void unsetSyncRef();
    
    boolean getTransitionEvaluation();
    
    XmlBoolean xgetTransitionEvaluation();
    
    boolean isSetTransitionEvaluation();
    
    void setTransitionEvaluation(final boolean p0);
    
    void xsetTransitionEvaluation(final XmlBoolean p0);
    
    void unsetTransitionEvaluation();
    
    boolean getTransitionEntry();
    
    XmlBoolean xgetTransitionEntry();
    
    boolean isSetTransitionEntry();
    
    void setTransitionEntry(final boolean p0);
    
    void xsetTransitionEntry(final XmlBoolean p0);
    
    void unsetTransitionEntry();
    
    boolean getPublished();
    
    XmlBoolean xgetPublished();
    
    boolean isSetPublished();
    
    void setPublished(final boolean p0);
    
    void xsetPublished(final XmlBoolean p0);
    
    void unsetPublished();
    
    String getCodeName();
    
    XmlString xgetCodeName();
    
    boolean isSetCodeName();
    
    void setCodeName(final String p0);
    
    void xsetCodeName(final XmlString p0);
    
    void unsetCodeName();
    
    boolean getFilterMode();
    
    XmlBoolean xgetFilterMode();
    
    boolean isSetFilterMode();
    
    void setFilterMode(final boolean p0);
    
    void xsetFilterMode(final XmlBoolean p0);
    
    void unsetFilterMode();
    
    boolean getEnableFormatConditionsCalculation();
    
    XmlBoolean xgetEnableFormatConditionsCalculation();
    
    boolean isSetEnableFormatConditionsCalculation();
    
    void setEnableFormatConditionsCalculation(final boolean p0);
    
    void xsetEnableFormatConditionsCalculation(final XmlBoolean p0);
    
    void unsetEnableFormatConditionsCalculation();
    
    public static final class Factory
    {
        public static CTSheetPr newInstance() {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().newInstance(CTSheetPr.type, null);
        }
        
        public static CTSheetPr newInstance(final XmlOptions xmlOptions) {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().newInstance(CTSheetPr.type, xmlOptions);
        }
        
        public static CTSheetPr parse(final String s) throws XmlException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(s, CTSheetPr.type, null);
        }
        
        public static CTSheetPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(s, CTSheetPr.type, xmlOptions);
        }
        
        public static CTSheetPr parse(final File file) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(file, CTSheetPr.type, null);
        }
        
        public static CTSheetPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(file, CTSheetPr.type, xmlOptions);
        }
        
        public static CTSheetPr parse(final URL url) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(url, CTSheetPr.type, null);
        }
        
        public static CTSheetPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(url, CTSheetPr.type, xmlOptions);
        }
        
        public static CTSheetPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTSheetPr.type, null);
        }
        
        public static CTSheetPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTSheetPr.type, xmlOptions);
        }
        
        public static CTSheetPr parse(final Reader reader) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(reader, CTSheetPr.type, null);
        }
        
        public static CTSheetPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(reader, CTSheetPr.type, xmlOptions);
        }
        
        public static CTSheetPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSheetPr.type, null);
        }
        
        public static CTSheetPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSheetPr.type, xmlOptions);
        }
        
        public static CTSheetPr parse(final Node node) throws XmlException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(node, CTSheetPr.type, null);
        }
        
        public static CTSheetPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(node, CTSheetPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSheetPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSheetPr.type, null);
        }
        
        @Deprecated
        public static CTSheetPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSheetPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSheetPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSheetPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSheetPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
