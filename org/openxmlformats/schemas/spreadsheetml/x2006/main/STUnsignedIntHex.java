// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlHexBinary;

public interface STUnsignedIntHex extends XmlHexBinary
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STUnsignedIntHex.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stunsignedinthex27datype");
    
    public static final class Factory
    {
        public static STUnsignedIntHex newValue(final Object o) {
            return (STUnsignedIntHex)STUnsignedIntHex.type.newValue(o);
        }
        
        public static STUnsignedIntHex newInstance() {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().newInstance(STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex newInstance(final XmlOptions xmlOptions) {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().newInstance(STUnsignedIntHex.type, xmlOptions);
        }
        
        public static STUnsignedIntHex parse(final String s) throws XmlException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(s, STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(s, STUnsignedIntHex.type, xmlOptions);
        }
        
        public static STUnsignedIntHex parse(final File file) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(file, STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(file, STUnsignedIntHex.type, xmlOptions);
        }
        
        public static STUnsignedIntHex parse(final URL url) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(url, STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(url, STUnsignedIntHex.type, xmlOptions);
        }
        
        public static STUnsignedIntHex parse(final InputStream inputStream) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(inputStream, STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(inputStream, STUnsignedIntHex.type, xmlOptions);
        }
        
        public static STUnsignedIntHex parse(final Reader reader) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(reader, STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(reader, STUnsignedIntHex.type, xmlOptions);
        }
        
        public static STUnsignedIntHex parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STUnsignedIntHex.type, xmlOptions);
        }
        
        public static STUnsignedIntHex parse(final Node node) throws XmlException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(node, STUnsignedIntHex.type, null);
        }
        
        public static STUnsignedIntHex parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(node, STUnsignedIntHex.type, xmlOptions);
        }
        
        @Deprecated
        public static STUnsignedIntHex parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STUnsignedIntHex.type, null);
        }
        
        @Deprecated
        public static STUnsignedIntHex parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STUnsignedIntHex)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STUnsignedIntHex.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STUnsignedIntHex.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STUnsignedIntHex.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
