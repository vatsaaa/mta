// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.spreadsheetml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTablePart extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTablePart.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttablepart1140type");
    
    String getId();
    
    STRelationshipId xgetId();
    
    void setId(final String p0);
    
    void xsetId(final STRelationshipId p0);
    
    public static final class Factory
    {
        public static CTTablePart newInstance() {
            return (CTTablePart)XmlBeans.getContextTypeLoader().newInstance(CTTablePart.type, null);
        }
        
        public static CTTablePart newInstance(final XmlOptions xmlOptions) {
            return (CTTablePart)XmlBeans.getContextTypeLoader().newInstance(CTTablePart.type, xmlOptions);
        }
        
        public static CTTablePart parse(final String s) throws XmlException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(s, CTTablePart.type, null);
        }
        
        public static CTTablePart parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(s, CTTablePart.type, xmlOptions);
        }
        
        public static CTTablePart parse(final File file) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(file, CTTablePart.type, null);
        }
        
        public static CTTablePart parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(file, CTTablePart.type, xmlOptions);
        }
        
        public static CTTablePart parse(final URL url) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(url, CTTablePart.type, null);
        }
        
        public static CTTablePart parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(url, CTTablePart.type, xmlOptions);
        }
        
        public static CTTablePart parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(inputStream, CTTablePart.type, null);
        }
        
        public static CTTablePart parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(inputStream, CTTablePart.type, xmlOptions);
        }
        
        public static CTTablePart parse(final Reader reader) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(reader, CTTablePart.type, null);
        }
        
        public static CTTablePart parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(reader, CTTablePart.type, xmlOptions);
        }
        
        public static CTTablePart parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTablePart.type, null);
        }
        
        public static CTTablePart parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTablePart.type, xmlOptions);
        }
        
        public static CTTablePart parse(final Node node) throws XmlException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(node, CTTablePart.type, null);
        }
        
        public static CTTablePart parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(node, CTTablePart.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTablePart parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTablePart.type, null);
        }
        
        @Deprecated
        public static CTTablePart parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTablePart)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTablePart.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTablePart.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTablePart.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
