// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBrClear;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STBrClearImpl extends JavaStringEnumerationHolderEx implements STBrClear
{
    public STBrClearImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STBrClearImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
