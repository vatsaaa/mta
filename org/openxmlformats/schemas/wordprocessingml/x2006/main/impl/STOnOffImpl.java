// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STOnOffImpl extends JavaStringEnumerationHolderEx implements STOnOff
{
    public STOnOffImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STOnOffImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
