// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTEmpty;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTEmptyImpl extends XmlComplexContentImpl implements CTEmpty
{
    public CTEmptyImpl(final SchemaType type) {
        super(type);
    }
}
