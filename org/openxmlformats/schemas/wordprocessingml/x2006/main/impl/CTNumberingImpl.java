// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDecimalNumber;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTNum;
import java.util.AbstractList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTNumPicBullet;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTNumbering;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTNumberingImpl extends XmlComplexContentImpl implements CTNumbering
{
    private static final QName NUMPICBULLET$0;
    private static final QName ABSTRACTNUM$2;
    private static final QName NUM$4;
    private static final QName NUMIDMACATCLEANUP$6;
    
    public CTNumberingImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTNumPicBullet> getNumPicBulletList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTNumPicBullet>)new CTNumberingImpl.NumPicBulletList(this);
        }
    }
    
    public CTNumPicBullet[] getNumPicBulletArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTNumberingImpl.NUMPICBULLET$0, list);
            final CTNumPicBullet[] array = new CTNumPicBullet[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTNumPicBullet getNumPicBulletArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNumPicBullet ctNumPicBullet = (CTNumPicBullet)this.get_store().find_element_user(CTNumberingImpl.NUMPICBULLET$0, n);
            if (ctNumPicBullet == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctNumPicBullet;
        }
    }
    
    public int sizeOfNumPicBulletArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTNumberingImpl.NUMPICBULLET$0);
        }
    }
    
    public void setNumPicBulletArray(final CTNumPicBullet[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTNumberingImpl.NUMPICBULLET$0);
        }
    }
    
    public void setNumPicBulletArray(final int n, final CTNumPicBullet ctNumPicBullet) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNumPicBullet ctNumPicBullet2 = (CTNumPicBullet)this.get_store().find_element_user(CTNumberingImpl.NUMPICBULLET$0, n);
            if (ctNumPicBullet2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctNumPicBullet2.set((XmlObject)ctNumPicBullet);
        }
    }
    
    public CTNumPicBullet insertNewNumPicBullet(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNumPicBullet)this.get_store().insert_element_user(CTNumberingImpl.NUMPICBULLET$0, n);
        }
    }
    
    public CTNumPicBullet addNewNumPicBullet() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNumPicBullet)this.get_store().add_element_user(CTNumberingImpl.NUMPICBULLET$0);
        }
    }
    
    public void removeNumPicBullet(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTNumberingImpl.NUMPICBULLET$0, n);
        }
    }
    
    public List<CTAbstractNum> getAbstractNumList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class AbstractNumList extends AbstractList<CTAbstractNum>
            {
                @Override
                public CTAbstractNum get(final int n) {
                    return CTNumberingImpl.this.getAbstractNumArray(n);
                }
                
                @Override
                public CTAbstractNum set(final int n, final CTAbstractNum ctAbstractNum) {
                    final CTAbstractNum abstractNumArray = CTNumberingImpl.this.getAbstractNumArray(n);
                    CTNumberingImpl.this.setAbstractNumArray(n, ctAbstractNum);
                    return abstractNumArray;
                }
                
                @Override
                public void add(final int n, final CTAbstractNum ctAbstractNum) {
                    CTNumberingImpl.this.insertNewAbstractNum(n).set(ctAbstractNum);
                }
                
                @Override
                public CTAbstractNum remove(final int n) {
                    final CTAbstractNum abstractNumArray = CTNumberingImpl.this.getAbstractNumArray(n);
                    CTNumberingImpl.this.removeAbstractNum(n);
                    return abstractNumArray;
                }
                
                @Override
                public int size() {
                    return CTNumberingImpl.this.sizeOfAbstractNumArray();
                }
            }
            return new AbstractNumList();
        }
    }
    
    public CTAbstractNum[] getAbstractNumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTNumberingImpl.ABSTRACTNUM$2, list);
            final CTAbstractNum[] array = new CTAbstractNum[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAbstractNum getAbstractNumArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAbstractNum ctAbstractNum = (CTAbstractNum)this.get_store().find_element_user(CTNumberingImpl.ABSTRACTNUM$2, n);
            if (ctAbstractNum == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAbstractNum;
        }
    }
    
    public int sizeOfAbstractNumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTNumberingImpl.ABSTRACTNUM$2);
        }
    }
    
    public void setAbstractNumArray(final CTAbstractNum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTNumberingImpl.ABSTRACTNUM$2);
        }
    }
    
    public void setAbstractNumArray(final int n, final CTAbstractNum ctAbstractNum) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAbstractNum ctAbstractNum2 = (CTAbstractNum)this.get_store().find_element_user(CTNumberingImpl.ABSTRACTNUM$2, n);
            if (ctAbstractNum2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAbstractNum2.set(ctAbstractNum);
        }
    }
    
    public CTAbstractNum insertNewAbstractNum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAbstractNum)this.get_store().insert_element_user(CTNumberingImpl.ABSTRACTNUM$2, n);
        }
    }
    
    public CTAbstractNum addNewAbstractNum() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAbstractNum)this.get_store().add_element_user(CTNumberingImpl.ABSTRACTNUM$2);
        }
    }
    
    public void removeAbstractNum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTNumberingImpl.ABSTRACTNUM$2, n);
        }
    }
    
    public List<CTNum> getNumList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class NumList extends AbstractList<CTNum>
            {
                @Override
                public CTNum get(final int n) {
                    return CTNumberingImpl.this.getNumArray(n);
                }
                
                @Override
                public CTNum set(final int n, final CTNum ctNum) {
                    final CTNum numArray = CTNumberingImpl.this.getNumArray(n);
                    CTNumberingImpl.this.setNumArray(n, ctNum);
                    return numArray;
                }
                
                @Override
                public void add(final int n, final CTNum ctNum) {
                    CTNumberingImpl.this.insertNewNum(n).set(ctNum);
                }
                
                @Override
                public CTNum remove(final int n) {
                    final CTNum numArray = CTNumberingImpl.this.getNumArray(n);
                    CTNumberingImpl.this.removeNum(n);
                    return numArray;
                }
                
                @Override
                public int size() {
                    return CTNumberingImpl.this.sizeOfNumArray();
                }
            }
            return new NumList();
        }
    }
    
    public CTNum[] getNumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTNumberingImpl.NUM$4, list);
            final CTNum[] array = new CTNum[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTNum getNumArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNum ctNum = (CTNum)this.get_store().find_element_user(CTNumberingImpl.NUM$4, n);
            if (ctNum == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctNum;
        }
    }
    
    public int sizeOfNumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTNumberingImpl.NUM$4);
        }
    }
    
    public void setNumArray(final CTNum[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTNumberingImpl.NUM$4);
        }
    }
    
    public void setNumArray(final int n, final CTNum ctNum) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNum ctNum2 = (CTNum)this.get_store().find_element_user(CTNumberingImpl.NUM$4, n);
            if (ctNum2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctNum2.set(ctNum);
        }
    }
    
    public CTNum insertNewNum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNum)this.get_store().insert_element_user(CTNumberingImpl.NUM$4, n);
        }
    }
    
    public CTNum addNewNum() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNum)this.get_store().add_element_user(CTNumberingImpl.NUM$4);
        }
    }
    
    public void removeNum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTNumberingImpl.NUM$4, n);
        }
    }
    
    public CTDecimalNumber getNumIdMacAtCleanup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber = (CTDecimalNumber)this.get_store().find_element_user(CTNumberingImpl.NUMIDMACATCLEANUP$6, 0);
            if (ctDecimalNumber == null) {
                return null;
            }
            return ctDecimalNumber;
        }
    }
    
    public boolean isSetNumIdMacAtCleanup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTNumberingImpl.NUMIDMACATCLEANUP$6) != 0;
        }
    }
    
    public void setNumIdMacAtCleanup(final CTDecimalNumber ctDecimalNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTDecimalNumber ctDecimalNumber2 = (CTDecimalNumber)this.get_store().find_element_user(CTNumberingImpl.NUMIDMACATCLEANUP$6, 0);
            if (ctDecimalNumber2 == null) {
                ctDecimalNumber2 = (CTDecimalNumber)this.get_store().add_element_user(CTNumberingImpl.NUMIDMACATCLEANUP$6);
            }
            ctDecimalNumber2.set(ctDecimalNumber);
        }
    }
    
    public CTDecimalNumber addNewNumIdMacAtCleanup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().add_element_user(CTNumberingImpl.NUMIDMACATCLEANUP$6);
        }
    }
    
    public void unsetNumIdMacAtCleanup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTNumberingImpl.NUMIDMACATCLEANUP$6, 0);
        }
    }
    
    static {
        NUMPICBULLET$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "numPicBullet");
        ABSTRACTNUM$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "abstractNum");
        NUM$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "num");
        NUMIDMACATCLEANUP$6 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "numIdMacAtCleanup");
    }
}
