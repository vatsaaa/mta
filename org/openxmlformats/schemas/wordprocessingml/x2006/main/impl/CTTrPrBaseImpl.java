// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import java.util.AbstractList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDecimalNumber;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTCnf;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPrBase;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTTrPrBaseImpl extends XmlComplexContentImpl implements CTTrPrBase
{
    private static final QName CNFSTYLE$0;
    private static final QName DIVID$2;
    private static final QName GRIDBEFORE$4;
    private static final QName GRIDAFTER$6;
    private static final QName WBEFORE$8;
    private static final QName WAFTER$10;
    private static final QName CANTSPLIT$12;
    private static final QName TRHEIGHT$14;
    private static final QName TBLHEADER$16;
    private static final QName TBLCELLSPACING$18;
    private static final QName JC$20;
    private static final QName HIDDEN$22;
    
    public CTTrPrBaseImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTCnf> getCnfStyleList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCnf>)new CTTrPrBaseImpl.CnfStyleList(this);
        }
    }
    
    public CTCnf[] getCnfStyleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.CNFSTYLE$0, list);
            final CTCnf[] array = new CTCnf[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCnf getCnfStyleArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCnf ctCnf = (CTCnf)this.get_store().find_element_user(CTTrPrBaseImpl.CNFSTYLE$0, n);
            if (ctCnf == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCnf;
        }
    }
    
    public int sizeOfCnfStyleArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.CNFSTYLE$0);
        }
    }
    
    public void setCnfStyleArray(final CTCnf[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTrPrBaseImpl.CNFSTYLE$0);
        }
    }
    
    public void setCnfStyleArray(final int n, final CTCnf ctCnf) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCnf ctCnf2 = (CTCnf)this.get_store().find_element_user(CTTrPrBaseImpl.CNFSTYLE$0, n);
            if (ctCnf2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCnf2.set((XmlObject)ctCnf);
        }
    }
    
    public CTCnf insertNewCnfStyle(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCnf)this.get_store().insert_element_user(CTTrPrBaseImpl.CNFSTYLE$0, n);
        }
    }
    
    public CTCnf addNewCnfStyle() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCnf)this.get_store().add_element_user(CTTrPrBaseImpl.CNFSTYLE$0);
        }
    }
    
    public void removeCnfStyle(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.CNFSTYLE$0, n);
        }
    }
    
    public List<CTDecimalNumber> getDivIdList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDecimalNumber>)new CTTrPrBaseImpl.DivIdList(this);
        }
    }
    
    public CTDecimalNumber[] getDivIdArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.DIVID$2, list);
            final CTDecimalNumber[] array = new CTDecimalNumber[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDecimalNumber getDivIdArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber = (CTDecimalNumber)this.get_store().find_element_user(CTTrPrBaseImpl.DIVID$2, n);
            if (ctDecimalNumber == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDecimalNumber;
        }
    }
    
    public int sizeOfDivIdArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.DIVID$2);
        }
    }
    
    public void setDivIdArray(final CTDecimalNumber[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.DIVID$2);
        }
    }
    
    public void setDivIdArray(final int n, final CTDecimalNumber ctDecimalNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber2 = (CTDecimalNumber)this.get_store().find_element_user(CTTrPrBaseImpl.DIVID$2, n);
            if (ctDecimalNumber2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDecimalNumber2.set(ctDecimalNumber);
        }
    }
    
    public CTDecimalNumber insertNewDivId(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().insert_element_user(CTTrPrBaseImpl.DIVID$2, n);
        }
    }
    
    public CTDecimalNumber addNewDivId() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().add_element_user(CTTrPrBaseImpl.DIVID$2);
        }
    }
    
    public void removeDivId(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.DIVID$2, n);
        }
    }
    
    public List<CTDecimalNumber> getGridBeforeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDecimalNumber>)new CTTrPrBaseImpl.GridBeforeList(this);
        }
    }
    
    public CTDecimalNumber[] getGridBeforeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.GRIDBEFORE$4, list);
            final CTDecimalNumber[] array = new CTDecimalNumber[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDecimalNumber getGridBeforeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber = (CTDecimalNumber)this.get_store().find_element_user(CTTrPrBaseImpl.GRIDBEFORE$4, n);
            if (ctDecimalNumber == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDecimalNumber;
        }
    }
    
    public int sizeOfGridBeforeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.GRIDBEFORE$4);
        }
    }
    
    public void setGridBeforeArray(final CTDecimalNumber[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.GRIDBEFORE$4);
        }
    }
    
    public void setGridBeforeArray(final int n, final CTDecimalNumber ctDecimalNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber2 = (CTDecimalNumber)this.get_store().find_element_user(CTTrPrBaseImpl.GRIDBEFORE$4, n);
            if (ctDecimalNumber2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDecimalNumber2.set(ctDecimalNumber);
        }
    }
    
    public CTDecimalNumber insertNewGridBefore(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().insert_element_user(CTTrPrBaseImpl.GRIDBEFORE$4, n);
        }
    }
    
    public CTDecimalNumber addNewGridBefore() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().add_element_user(CTTrPrBaseImpl.GRIDBEFORE$4);
        }
    }
    
    public void removeGridBefore(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.GRIDBEFORE$4, n);
        }
    }
    
    public List<CTDecimalNumber> getGridAfterList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDecimalNumber>)new CTTrPrBaseImpl.GridAfterList(this);
        }
    }
    
    public CTDecimalNumber[] getGridAfterArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.GRIDAFTER$6, list);
            final CTDecimalNumber[] array = new CTDecimalNumber[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDecimalNumber getGridAfterArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber = (CTDecimalNumber)this.get_store().find_element_user(CTTrPrBaseImpl.GRIDAFTER$6, n);
            if (ctDecimalNumber == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDecimalNumber;
        }
    }
    
    public int sizeOfGridAfterArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.GRIDAFTER$6);
        }
    }
    
    public void setGridAfterArray(final CTDecimalNumber[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.GRIDAFTER$6);
        }
    }
    
    public void setGridAfterArray(final int n, final CTDecimalNumber ctDecimalNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber2 = (CTDecimalNumber)this.get_store().find_element_user(CTTrPrBaseImpl.GRIDAFTER$6, n);
            if (ctDecimalNumber2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDecimalNumber2.set(ctDecimalNumber);
        }
    }
    
    public CTDecimalNumber insertNewGridAfter(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().insert_element_user(CTTrPrBaseImpl.GRIDAFTER$6, n);
        }
    }
    
    public CTDecimalNumber addNewGridAfter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().add_element_user(CTTrPrBaseImpl.GRIDAFTER$6);
        }
    }
    
    public void removeGridAfter(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.GRIDAFTER$6, n);
        }
    }
    
    public List<CTTblWidth> getWBeforeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTblWidth>)new CTTrPrBaseImpl.WBeforeList(this);
        }
    }
    
    public CTTblWidth[] getWBeforeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.WBEFORE$8, list);
            final CTTblWidth[] array = new CTTblWidth[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTblWidth getWBeforeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblWidth ctTblWidth = (CTTblWidth)this.get_store().find_element_user(CTTrPrBaseImpl.WBEFORE$8, n);
            if (ctTblWidth == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTblWidth;
        }
    }
    
    public int sizeOfWBeforeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.WBEFORE$8);
        }
    }
    
    public void setWBeforeArray(final CTTblWidth[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.WBEFORE$8);
        }
    }
    
    public void setWBeforeArray(final int n, final CTTblWidth ctTblWidth) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblWidth ctTblWidth2 = (CTTblWidth)this.get_store().find_element_user(CTTrPrBaseImpl.WBEFORE$8, n);
            if (ctTblWidth2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTblWidth2.set(ctTblWidth);
        }
    }
    
    public CTTblWidth insertNewWBefore(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblWidth)this.get_store().insert_element_user(CTTrPrBaseImpl.WBEFORE$8, n);
        }
    }
    
    public CTTblWidth addNewWBefore() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblWidth)this.get_store().add_element_user(CTTrPrBaseImpl.WBEFORE$8);
        }
    }
    
    public void removeWBefore(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.WBEFORE$8, n);
        }
    }
    
    public List<CTTblWidth> getWAfterList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTblWidth>)new CTTrPrBaseImpl.WAfterList(this);
        }
    }
    
    public CTTblWidth[] getWAfterArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.WAFTER$10, list);
            final CTTblWidth[] array = new CTTblWidth[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTblWidth getWAfterArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblWidth ctTblWidth = (CTTblWidth)this.get_store().find_element_user(CTTrPrBaseImpl.WAFTER$10, n);
            if (ctTblWidth == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTblWidth;
        }
    }
    
    public int sizeOfWAfterArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.WAFTER$10);
        }
    }
    
    public void setWAfterArray(final CTTblWidth[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.WAFTER$10);
        }
    }
    
    public void setWAfterArray(final int n, final CTTblWidth ctTblWidth) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblWidth ctTblWidth2 = (CTTblWidth)this.get_store().find_element_user(CTTrPrBaseImpl.WAFTER$10, n);
            if (ctTblWidth2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTblWidth2.set(ctTblWidth);
        }
    }
    
    public CTTblWidth insertNewWAfter(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblWidth)this.get_store().insert_element_user(CTTrPrBaseImpl.WAFTER$10, n);
        }
    }
    
    public CTTblWidth addNewWAfter() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblWidth)this.get_store().add_element_user(CTTrPrBaseImpl.WAFTER$10);
        }
    }
    
    public void removeWAfter(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.WAFTER$10, n);
        }
    }
    
    public List<CTOnOff> getCantSplitList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class CantSplitList extends AbstractList<CTOnOff>
            {
                @Override
                public CTOnOff get(final int n) {
                    return CTTrPrBaseImpl.this.getCantSplitArray(n);
                }
                
                @Override
                public CTOnOff set(final int n, final CTOnOff ctOnOff) {
                    final CTOnOff cantSplitArray = CTTrPrBaseImpl.this.getCantSplitArray(n);
                    CTTrPrBaseImpl.this.setCantSplitArray(n, ctOnOff);
                    return cantSplitArray;
                }
                
                @Override
                public void add(final int n, final CTOnOff ctOnOff) {
                    CTTrPrBaseImpl.this.insertNewCantSplit(n).set(ctOnOff);
                }
                
                @Override
                public CTOnOff remove(final int n) {
                    final CTOnOff cantSplitArray = CTTrPrBaseImpl.this.getCantSplitArray(n);
                    CTTrPrBaseImpl.this.removeCantSplit(n);
                    return cantSplitArray;
                }
                
                @Override
                public int size() {
                    return CTTrPrBaseImpl.this.sizeOfCantSplitArray();
                }
            }
            return new CantSplitList();
        }
    }
    
    public CTOnOff[] getCantSplitArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.CANTSPLIT$12, list);
            final CTOnOff[] array = new CTOnOff[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOnOff getCantSplitArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff = (CTOnOff)this.get_store().find_element_user(CTTrPrBaseImpl.CANTSPLIT$12, n);
            if (ctOnOff == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOnOff;
        }
    }
    
    public int sizeOfCantSplitArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.CANTSPLIT$12);
        }
    }
    
    public void setCantSplitArray(final CTOnOff[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.CANTSPLIT$12);
        }
    }
    
    public void setCantSplitArray(final int n, final CTOnOff ctOnOff) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff2 = (CTOnOff)this.get_store().find_element_user(CTTrPrBaseImpl.CANTSPLIT$12, n);
            if (ctOnOff2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOnOff2.set(ctOnOff);
        }
    }
    
    public CTOnOff insertNewCantSplit(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().insert_element_user(CTTrPrBaseImpl.CANTSPLIT$12, n);
        }
    }
    
    public CTOnOff addNewCantSplit() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().add_element_user(CTTrPrBaseImpl.CANTSPLIT$12);
        }
    }
    
    public void removeCantSplit(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.CANTSPLIT$12, n);
        }
    }
    
    public List<CTHeight> getTrHeightList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTHeight>)new CTTrPrBaseImpl.TrHeightList(this);
        }
    }
    
    public CTHeight[] getTrHeightArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.TRHEIGHT$14, list);
            final CTHeight[] array = new CTHeight[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTHeight getTrHeightArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHeight ctHeight = (CTHeight)this.get_store().find_element_user(CTTrPrBaseImpl.TRHEIGHT$14, n);
            if (ctHeight == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctHeight;
        }
    }
    
    public int sizeOfTrHeightArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.TRHEIGHT$14);
        }
    }
    
    public void setTrHeightArray(final CTHeight[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.TRHEIGHT$14);
        }
    }
    
    public void setTrHeightArray(final int n, final CTHeight ctHeight) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTHeight ctHeight2 = (CTHeight)this.get_store().find_element_user(CTTrPrBaseImpl.TRHEIGHT$14, n);
            if (ctHeight2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctHeight2.set(ctHeight);
        }
    }
    
    public CTHeight insertNewTrHeight(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHeight)this.get_store().insert_element_user(CTTrPrBaseImpl.TRHEIGHT$14, n);
        }
    }
    
    public CTHeight addNewTrHeight() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTHeight)this.get_store().add_element_user(CTTrPrBaseImpl.TRHEIGHT$14);
        }
    }
    
    public void removeTrHeight(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.TRHEIGHT$14, n);
        }
    }
    
    public List<CTOnOff> getTblHeaderList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class TblHeaderList extends AbstractList<CTOnOff>
            {
                @Override
                public CTOnOff get(final int n) {
                    return CTTrPrBaseImpl.this.getTblHeaderArray(n);
                }
                
                @Override
                public CTOnOff set(final int n, final CTOnOff ctOnOff) {
                    final CTOnOff tblHeaderArray = CTTrPrBaseImpl.this.getTblHeaderArray(n);
                    CTTrPrBaseImpl.this.setTblHeaderArray(n, ctOnOff);
                    return tblHeaderArray;
                }
                
                @Override
                public void add(final int n, final CTOnOff ctOnOff) {
                    CTTrPrBaseImpl.this.insertNewTblHeader(n).set(ctOnOff);
                }
                
                @Override
                public CTOnOff remove(final int n) {
                    final CTOnOff tblHeaderArray = CTTrPrBaseImpl.this.getTblHeaderArray(n);
                    CTTrPrBaseImpl.this.removeTblHeader(n);
                    return tblHeaderArray;
                }
                
                @Override
                public int size() {
                    return CTTrPrBaseImpl.this.sizeOfTblHeaderArray();
                }
            }
            return new TblHeaderList();
        }
    }
    
    public CTOnOff[] getTblHeaderArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.TBLHEADER$16, list);
            final CTOnOff[] array = new CTOnOff[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOnOff getTblHeaderArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff = (CTOnOff)this.get_store().find_element_user(CTTrPrBaseImpl.TBLHEADER$16, n);
            if (ctOnOff == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOnOff;
        }
    }
    
    public int sizeOfTblHeaderArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.TBLHEADER$16);
        }
    }
    
    public void setTblHeaderArray(final CTOnOff[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.TBLHEADER$16);
        }
    }
    
    public void setTblHeaderArray(final int n, final CTOnOff ctOnOff) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff2 = (CTOnOff)this.get_store().find_element_user(CTTrPrBaseImpl.TBLHEADER$16, n);
            if (ctOnOff2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOnOff2.set(ctOnOff);
        }
    }
    
    public CTOnOff insertNewTblHeader(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().insert_element_user(CTTrPrBaseImpl.TBLHEADER$16, n);
        }
    }
    
    public CTOnOff addNewTblHeader() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().add_element_user(CTTrPrBaseImpl.TBLHEADER$16);
        }
    }
    
    public void removeTblHeader(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.TBLHEADER$16, n);
        }
    }
    
    public List<CTTblWidth> getTblCellSpacingList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTblWidth>)new CTTrPrBaseImpl.TblCellSpacingList(this);
        }
    }
    
    public CTTblWidth[] getTblCellSpacingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.TBLCELLSPACING$18, list);
            final CTTblWidth[] array = new CTTblWidth[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTblWidth getTblCellSpacingArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblWidth ctTblWidth = (CTTblWidth)this.get_store().find_element_user(CTTrPrBaseImpl.TBLCELLSPACING$18, n);
            if (ctTblWidth == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTblWidth;
        }
    }
    
    public int sizeOfTblCellSpacingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.TBLCELLSPACING$18);
        }
    }
    
    public void setTblCellSpacingArray(final CTTblWidth[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.TBLCELLSPACING$18);
        }
    }
    
    public void setTblCellSpacingArray(final int n, final CTTblWidth ctTblWidth) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblWidth ctTblWidth2 = (CTTblWidth)this.get_store().find_element_user(CTTrPrBaseImpl.TBLCELLSPACING$18, n);
            if (ctTblWidth2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTblWidth2.set(ctTblWidth);
        }
    }
    
    public CTTblWidth insertNewTblCellSpacing(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblWidth)this.get_store().insert_element_user(CTTrPrBaseImpl.TBLCELLSPACING$18, n);
        }
    }
    
    public CTTblWidth addNewTblCellSpacing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblWidth)this.get_store().add_element_user(CTTrPrBaseImpl.TBLCELLSPACING$18);
        }
    }
    
    public void removeTblCellSpacing(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.TBLCELLSPACING$18, n);
        }
    }
    
    public List<CTJc> getJcList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTJc>)new CTTrPrBaseImpl.JcList(this);
        }
    }
    
    public CTJc[] getJcArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.JC$20, list);
            final CTJc[] array = new CTJc[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTJc getJcArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTJc ctJc = (CTJc)this.get_store().find_element_user(CTTrPrBaseImpl.JC$20, n);
            if (ctJc == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctJc;
        }
    }
    
    public int sizeOfJcArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.JC$20);
        }
    }
    
    public void setJcArray(final CTJc[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.JC$20);
        }
    }
    
    public void setJcArray(final int n, final CTJc ctJc) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTJc ctJc2 = (CTJc)this.get_store().find_element_user(CTTrPrBaseImpl.JC$20, n);
            if (ctJc2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctJc2.set(ctJc);
        }
    }
    
    public CTJc insertNewJc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTJc)this.get_store().insert_element_user(CTTrPrBaseImpl.JC$20, n);
        }
    }
    
    public CTJc addNewJc() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTJc)this.get_store().add_element_user(CTTrPrBaseImpl.JC$20);
        }
    }
    
    public void removeJc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.JC$20, n);
        }
    }
    
    public List<CTOnOff> getHiddenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOnOff>)new CTTrPrBaseImpl.HiddenList(this);
        }
    }
    
    public CTOnOff[] getHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTrPrBaseImpl.HIDDEN$22, list);
            final CTOnOff[] array = new CTOnOff[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOnOff getHiddenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff = (CTOnOff)this.get_store().find_element_user(CTTrPrBaseImpl.HIDDEN$22, n);
            if (ctOnOff == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOnOff;
        }
    }
    
    public int sizeOfHiddenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrBaseImpl.HIDDEN$22);
        }
    }
    
    public void setHiddenArray(final CTOnOff[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTrPrBaseImpl.HIDDEN$22);
        }
    }
    
    public void setHiddenArray(final int n, final CTOnOff ctOnOff) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff2 = (CTOnOff)this.get_store().find_element_user(CTTrPrBaseImpl.HIDDEN$22, n);
            if (ctOnOff2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOnOff2.set(ctOnOff);
        }
    }
    
    public CTOnOff insertNewHidden(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().insert_element_user(CTTrPrBaseImpl.HIDDEN$22, n);
        }
    }
    
    public CTOnOff addNewHidden() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().add_element_user(CTTrPrBaseImpl.HIDDEN$22);
        }
    }
    
    public void removeHidden(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrBaseImpl.HIDDEN$22, n);
        }
    }
    
    static {
        CNFSTYLE$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "cnfStyle");
        DIVID$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "divId");
        GRIDBEFORE$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "gridBefore");
        GRIDAFTER$6 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "gridAfter");
        WBEFORE$8 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "wBefore");
        WAFTER$10 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "wAfter");
        CANTSPLIT$12 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "cantSplit");
        TRHEIGHT$14 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "trHeight");
        TBLHEADER$16 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tblHeader");
        TBLCELLSPACING$18 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tblCellSpacing");
        JC$20 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "jc");
        HIDDEN$22 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "hidden");
    }
}
