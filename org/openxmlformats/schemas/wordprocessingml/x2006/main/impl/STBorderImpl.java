// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STBorderImpl extends JavaStringEnumerationHolderEx implements STBorder
{
    public STBorderImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STBorderImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
