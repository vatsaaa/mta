// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPrChange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTParaRPr;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;

public class CTPPrImpl extends CTPPrBaseImpl implements CTPPr
{
    private static final QName RPR$0;
    private static final QName SECTPR$2;
    private static final QName PPRCHANGE$4;
    
    public CTPPrImpl(final SchemaType schemaType) {
        super(schemaType);
    }
    
    public CTParaRPr getRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTParaRPr ctParaRPr = (CTParaRPr)this.get_store().find_element_user(CTPPrImpl.RPR$0, 0);
            if (ctParaRPr == null) {
                return null;
            }
            return ctParaRPr;
        }
    }
    
    public boolean isSetRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPPrImpl.RPR$0) != 0;
        }
    }
    
    public void setRPr(final CTParaRPr ctParaRPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTParaRPr ctParaRPr2 = (CTParaRPr)this.get_store().find_element_user(CTPPrImpl.RPR$0, 0);
            if (ctParaRPr2 == null) {
                ctParaRPr2 = (CTParaRPr)this.get_store().add_element_user(CTPPrImpl.RPR$0);
            }
            ctParaRPr2.set(ctParaRPr);
        }
    }
    
    public CTParaRPr addNewRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTParaRPr)this.get_store().add_element_user(CTPPrImpl.RPR$0);
        }
    }
    
    public void unsetRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPPrImpl.RPR$0, 0);
        }
    }
    
    public CTSectPr getSectPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSectPr ctSectPr = (CTSectPr)this.get_store().find_element_user(CTPPrImpl.SECTPR$2, 0);
            if (ctSectPr == null) {
                return null;
            }
            return ctSectPr;
        }
    }
    
    public boolean isSetSectPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPPrImpl.SECTPR$2) != 0;
        }
    }
    
    public void setSectPr(final CTSectPr ctSectPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTSectPr ctSectPr2 = (CTSectPr)this.get_store().find_element_user(CTPPrImpl.SECTPR$2, 0);
            if (ctSectPr2 == null) {
                ctSectPr2 = (CTSectPr)this.get_store().add_element_user(CTPPrImpl.SECTPR$2);
            }
            ctSectPr2.set(ctSectPr);
        }
    }
    
    public CTSectPr addNewSectPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSectPr)this.get_store().add_element_user(CTPPrImpl.SECTPR$2);
        }
    }
    
    public void unsetSectPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPPrImpl.SECTPR$2, 0);
        }
    }
    
    public CTPPrChange getPPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPPrChange ctpPrChange = (CTPPrChange)this.get_store().find_element_user(CTPPrImpl.PPRCHANGE$4, 0);
            if (ctpPrChange == null) {
                return null;
            }
            return ctpPrChange;
        }
    }
    
    public boolean isSetPPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTPPrImpl.PPRCHANGE$4) != 0;
        }
    }
    
    public void setPPrChange(final CTPPrChange ctpPrChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTPPrChange ctpPrChange2 = (CTPPrChange)this.get_store().find_element_user(CTPPrImpl.PPRCHANGE$4, 0);
            if (ctpPrChange2 == null) {
                ctpPrChange2 = (CTPPrChange)this.get_store().add_element_user(CTPPrImpl.PPRCHANGE$4);
            }
            ctpPrChange2.set((XmlObject)ctpPrChange);
        }
    }
    
    public CTPPrChange addNewPPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPPrChange)this.get_store().add_element_user(CTPPrImpl.PPRCHANGE$4);
        }
    }
    
    public void unsetPPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTPPrImpl.PPRCHANGE$4, 0);
        }
    }
    
    static {
        RPR$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rPr");
        SECTPR$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "sectPr");
        PPRCHANGE$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "pPrChange");
    }
}
