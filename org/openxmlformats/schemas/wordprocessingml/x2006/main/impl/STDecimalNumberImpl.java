// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STDecimalNumber;
import org.apache.xmlbeans.impl.values.JavaIntegerHolderEx;

public class STDecimalNumberImpl extends JavaIntegerHolderEx implements STDecimalNumber
{
    public STDecimalNumberImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STDecimalNumberImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
