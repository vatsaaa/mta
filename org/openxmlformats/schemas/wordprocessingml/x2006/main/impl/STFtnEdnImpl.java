// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STFtnEdn;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STFtnEdnImpl extends JavaStringEnumerationHolderEx implements STFtnEdn
{
    public STFtnEdnImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STFtnEdnImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
