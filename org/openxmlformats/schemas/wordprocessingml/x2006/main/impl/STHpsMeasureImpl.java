// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHpsMeasure;
import org.apache.xmlbeans.impl.values.JavaIntegerHolderEx;

public class STHpsMeasureImpl extends JavaIntegerHolderEx implements STHpsMeasure
{
    public STHpsMeasureImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STHpsMeasureImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
