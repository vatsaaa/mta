// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STSignedTwipsMeasure;
import org.apache.xmlbeans.impl.values.JavaIntegerHolderEx;

public class STSignedTwipsMeasureImpl extends JavaIntegerHolderEx implements STSignedTwipsMeasure
{
    public STSignedTwipsMeasureImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSignedTwipsMeasureImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
