// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTextAlignment;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTextAlignmentImpl extends JavaStringEnumerationHolderEx implements STTextAlignment
{
    public STTextAlignmentImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTextAlignmentImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
