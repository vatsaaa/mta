// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTabStop;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTabs;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTTabsImpl extends XmlComplexContentImpl implements CTTabs
{
    private static final QName TAB$0;
    
    public CTTabsImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTTabStop> getTabList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTabStop>)new CTTabsImpl.TabList(this);
        }
    }
    
    public CTTabStop[] getTabArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTabsImpl.TAB$0, list);
            final CTTabStop[] array = new CTTabStop[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTabStop getTabArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTabStop ctTabStop = (CTTabStop)this.get_store().find_element_user(CTTabsImpl.TAB$0, n);
            if (ctTabStop == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTabStop;
        }
    }
    
    public int sizeOfTabArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTabsImpl.TAB$0);
        }
    }
    
    public void setTabArray(final CTTabStop[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTabsImpl.TAB$0);
        }
    }
    
    public void setTabArray(final int n, final CTTabStop ctTabStop) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTabStop ctTabStop2 = (CTTabStop)this.get_store().find_element_user(CTTabsImpl.TAB$0, n);
            if (ctTabStop2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTabStop2.set(ctTabStop);
        }
    }
    
    public CTTabStop insertNewTab(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTabStop)this.get_store().insert_element_user(CTTabsImpl.TAB$0, n);
        }
    }
    
    public CTTabStop addNewTab() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTabStop)this.get_store().add_element_user(CTTabsImpl.TAB$0);
        }
    }
    
    public void removeTab(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTabsImpl.TAB$0, n);
        }
    }
    
    static {
        TAB$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tab");
    }
}
