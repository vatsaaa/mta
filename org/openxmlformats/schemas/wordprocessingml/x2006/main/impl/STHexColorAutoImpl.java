// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHexColorAuto;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STHexColorAutoImpl extends JavaStringEnumerationHolderEx implements STHexColorAuto
{
    public STHexColorAutoImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STHexColorAutoImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
