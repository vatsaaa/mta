// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTheme;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STThemeImpl extends JavaStringEnumerationHolderEx implements STTheme
{
    public STThemeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STThemeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
