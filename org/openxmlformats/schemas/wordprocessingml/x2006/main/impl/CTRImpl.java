// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.STLongHexNumber;
import org.apache.xmlbeans.SimpleValue;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPTab;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDrawing;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFtnEdnRef;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRuby;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFldChar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPicture;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSym;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTEmpty;
import java.util.AbstractList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBr;
import java.util.List;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTRImpl extends XmlComplexContentImpl implements CTR
{
    private static final QName RPR$0;
    private static final QName BR$2;
    private static final QName T$4;
    private static final QName DELTEXT$6;
    private static final QName INSTRTEXT$8;
    private static final QName DELINSTRTEXT$10;
    private static final QName NOBREAKHYPHEN$12;
    private static final QName SOFTHYPHEN$14;
    private static final QName DAYSHORT$16;
    private static final QName MONTHSHORT$18;
    private static final QName YEARSHORT$20;
    private static final QName DAYLONG$22;
    private static final QName MONTHLONG$24;
    private static final QName YEARLONG$26;
    private static final QName ANNOTATIONREF$28;
    private static final QName FOOTNOTEREF$30;
    private static final QName ENDNOTEREF$32;
    private static final QName SEPARATOR$34;
    private static final QName CONTINUATIONSEPARATOR$36;
    private static final QName SYM$38;
    private static final QName PGNUM$40;
    private static final QName CR$42;
    private static final QName TAB$44;
    private static final QName OBJECT$46;
    private static final QName PICT$48;
    private static final QName FLDCHAR$50;
    private static final QName RUBY$52;
    private static final QName FOOTNOTEREFERENCE$54;
    private static final QName ENDNOTEREFERENCE$56;
    private static final QName COMMENTREFERENCE$58;
    private static final QName DRAWING$60;
    private static final QName PTAB$62;
    private static final QName LASTRENDEREDPAGEBREAK$64;
    private static final QName RSIDRPR$66;
    private static final QName RSIDDEL$68;
    private static final QName RSIDR$70;
    
    public CTRImpl(final SchemaType type) {
        super(type);
    }
    
    public CTRPr getRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRPr ctrPr = (CTRPr)this.get_store().find_element_user(CTRImpl.RPR$0, 0);
            if (ctrPr == null) {
                return null;
            }
            return ctrPr;
        }
    }
    
    public boolean isSetRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.RPR$0) != 0;
        }
    }
    
    public void setRPr(final CTRPr ctrPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTRPr ctrPr2 = (CTRPr)this.get_store().find_element_user(CTRImpl.RPR$0, 0);
            if (ctrPr2 == null) {
                ctrPr2 = (CTRPr)this.get_store().add_element_user(CTRImpl.RPR$0);
            }
            ctrPr2.set(ctrPr);
        }
    }
    
    public CTRPr addNewRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRPr)this.get_store().add_element_user(CTRImpl.RPR$0);
        }
    }
    
    public void unsetRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.RPR$0, 0);
        }
    }
    
    public List<CTBr> getBrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBr>)new CTRImpl.BrList(this);
        }
    }
    
    public CTBr[] getBrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.BR$2, list);
            final CTBr[] array = new CTBr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBr getBrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBr ctBr = (CTBr)this.get_store().find_element_user(CTRImpl.BR$2, n);
            if (ctBr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBr;
        }
    }
    
    public int sizeOfBrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.BR$2);
        }
    }
    
    public void setBrArray(final CTBr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.BR$2);
        }
    }
    
    public void setBrArray(final int n, final CTBr ctBr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBr ctBr2 = (CTBr)this.get_store().find_element_user(CTRImpl.BR$2, n);
            if (ctBr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBr2.set(ctBr);
        }
    }
    
    public CTBr insertNewBr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBr)this.get_store().insert_element_user(CTRImpl.BR$2, n);
        }
    }
    
    public CTBr addNewBr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBr)this.get_store().add_element_user(CTRImpl.BR$2);
        }
    }
    
    public void removeBr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.BR$2, n);
        }
    }
    
    public List<CTText> getTList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class TList extends AbstractList<CTText>
            {
                @Override
                public CTText get(final int n) {
                    return CTRImpl.this.getTArray(n);
                }
                
                @Override
                public CTText set(final int n, final CTText ctText) {
                    final CTText tArray = CTRImpl.this.getTArray(n);
                    CTRImpl.this.setTArray(n, ctText);
                    return tArray;
                }
                
                @Override
                public void add(final int n, final CTText ctText) {
                    CTRImpl.this.insertNewT(n).set(ctText);
                }
                
                @Override
                public CTText remove(final int n) {
                    final CTText tArray = CTRImpl.this.getTArray(n);
                    CTRImpl.this.removeT(n);
                    return tArray;
                }
                
                @Override
                public int size() {
                    return CTRImpl.this.sizeOfTArray();
                }
            }
            return new TList();
        }
    }
    
    public CTText[] getTArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.T$4, list);
            final CTText[] array = new CTText[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTText getTArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText = (CTText)this.get_store().find_element_user(CTRImpl.T$4, n);
            if (ctText == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctText;
        }
    }
    
    public int sizeOfTArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.T$4);
        }
    }
    
    public void setTArray(final CTText[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.T$4);
        }
    }
    
    public void setTArray(final int n, final CTText ctText) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText2 = (CTText)this.get_store().find_element_user(CTRImpl.T$4, n);
            if (ctText2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctText2.set(ctText);
        }
    }
    
    public CTText insertNewT(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().insert_element_user(CTRImpl.T$4, n);
        }
    }
    
    public CTText addNewT() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().add_element_user(CTRImpl.T$4);
        }
    }
    
    public void removeT(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.T$4, n);
        }
    }
    
    public List<CTText> getDelTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTText>)new CTRImpl.DelTextList(this);
        }
    }
    
    public CTText[] getDelTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.DELTEXT$6, list);
            final CTText[] array = new CTText[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTText getDelTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText = (CTText)this.get_store().find_element_user(CTRImpl.DELTEXT$6, n);
            if (ctText == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctText;
        }
    }
    
    public int sizeOfDelTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.DELTEXT$6);
        }
    }
    
    public void setDelTextArray(final CTText[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.DELTEXT$6);
        }
    }
    
    public void setDelTextArray(final int n, final CTText ctText) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText2 = (CTText)this.get_store().find_element_user(CTRImpl.DELTEXT$6, n);
            if (ctText2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctText2.set(ctText);
        }
    }
    
    public CTText insertNewDelText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().insert_element_user(CTRImpl.DELTEXT$6, n);
        }
    }
    
    public CTText addNewDelText() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().add_element_user(CTRImpl.DELTEXT$6);
        }
    }
    
    public void removeDelText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.DELTEXT$6, n);
        }
    }
    
    public List<CTText> getInstrTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTText>)new CTRImpl.InstrTextList(this);
        }
    }
    
    public CTText[] getInstrTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.INSTRTEXT$8, list);
            final CTText[] array = new CTText[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTText getInstrTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText = (CTText)this.get_store().find_element_user(CTRImpl.INSTRTEXT$8, n);
            if (ctText == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctText;
        }
    }
    
    public int sizeOfInstrTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.INSTRTEXT$8);
        }
    }
    
    public void setInstrTextArray(final CTText[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.INSTRTEXT$8);
        }
    }
    
    public void setInstrTextArray(final int n, final CTText ctText) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText2 = (CTText)this.get_store().find_element_user(CTRImpl.INSTRTEXT$8, n);
            if (ctText2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctText2.set(ctText);
        }
    }
    
    public CTText insertNewInstrText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().insert_element_user(CTRImpl.INSTRTEXT$8, n);
        }
    }
    
    public CTText addNewInstrText() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().add_element_user(CTRImpl.INSTRTEXT$8);
        }
    }
    
    public void removeInstrText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.INSTRTEXT$8, n);
        }
    }
    
    public List<CTText> getDelInstrTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTText>)new CTRImpl.DelInstrTextList(this);
        }
    }
    
    public CTText[] getDelInstrTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.DELINSTRTEXT$10, list);
            final CTText[] array = new CTText[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTText getDelInstrTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText = (CTText)this.get_store().find_element_user(CTRImpl.DELINSTRTEXT$10, n);
            if (ctText == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctText;
        }
    }
    
    public int sizeOfDelInstrTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.DELINSTRTEXT$10);
        }
    }
    
    public void setDelInstrTextArray(final CTText[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.DELINSTRTEXT$10);
        }
    }
    
    public void setDelInstrTextArray(final int n, final CTText ctText) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTText ctText2 = (CTText)this.get_store().find_element_user(CTRImpl.DELINSTRTEXT$10, n);
            if (ctText2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctText2.set(ctText);
        }
    }
    
    public CTText insertNewDelInstrText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().insert_element_user(CTRImpl.DELINSTRTEXT$10, n);
        }
    }
    
    public CTText addNewDelInstrText() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTText)this.get_store().add_element_user(CTRImpl.DELINSTRTEXT$10);
        }
    }
    
    public void removeDelInstrText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.DELINSTRTEXT$10, n);
        }
    }
    
    public List<CTEmpty> getNoBreakHyphenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.NoBreakHyphenList(this);
        }
    }
    
    public CTEmpty[] getNoBreakHyphenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.NOBREAKHYPHEN$12, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getNoBreakHyphenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.NOBREAKHYPHEN$12, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfNoBreakHyphenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.NOBREAKHYPHEN$12);
        }
    }
    
    public void setNoBreakHyphenArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.NOBREAKHYPHEN$12);
        }
    }
    
    public void setNoBreakHyphenArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.NOBREAKHYPHEN$12, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewNoBreakHyphen(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.NOBREAKHYPHEN$12, n);
        }
    }
    
    public CTEmpty addNewNoBreakHyphen() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.NOBREAKHYPHEN$12);
        }
    }
    
    public void removeNoBreakHyphen(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.NOBREAKHYPHEN$12, n);
        }
    }
    
    public List<CTEmpty> getSoftHyphenList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.SoftHyphenList(this);
        }
    }
    
    public CTEmpty[] getSoftHyphenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.SOFTHYPHEN$14, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getSoftHyphenArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.SOFTHYPHEN$14, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfSoftHyphenArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.SOFTHYPHEN$14);
        }
    }
    
    public void setSoftHyphenArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.SOFTHYPHEN$14);
        }
    }
    
    public void setSoftHyphenArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.SOFTHYPHEN$14, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewSoftHyphen(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.SOFTHYPHEN$14, n);
        }
    }
    
    public CTEmpty addNewSoftHyphen() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.SOFTHYPHEN$14);
        }
    }
    
    public void removeSoftHyphen(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.SOFTHYPHEN$14, n);
        }
    }
    
    public List<CTEmpty> getDayShortList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.DayShortList(this);
        }
    }
    
    public CTEmpty[] getDayShortArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.DAYSHORT$16, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getDayShortArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.DAYSHORT$16, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfDayShortArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.DAYSHORT$16);
        }
    }
    
    public void setDayShortArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.DAYSHORT$16);
        }
    }
    
    public void setDayShortArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.DAYSHORT$16, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewDayShort(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.DAYSHORT$16, n);
        }
    }
    
    public CTEmpty addNewDayShort() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.DAYSHORT$16);
        }
    }
    
    public void removeDayShort(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.DAYSHORT$16, n);
        }
    }
    
    public List<CTEmpty> getMonthShortList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.MonthShortList(this);
        }
    }
    
    public CTEmpty[] getMonthShortArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.MONTHSHORT$18, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getMonthShortArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.MONTHSHORT$18, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfMonthShortArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.MONTHSHORT$18);
        }
    }
    
    public void setMonthShortArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.MONTHSHORT$18);
        }
    }
    
    public void setMonthShortArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.MONTHSHORT$18, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewMonthShort(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.MONTHSHORT$18, n);
        }
    }
    
    public CTEmpty addNewMonthShort() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.MONTHSHORT$18);
        }
    }
    
    public void removeMonthShort(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.MONTHSHORT$18, n);
        }
    }
    
    public List<CTEmpty> getYearShortList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.YearShortList(this);
        }
    }
    
    public CTEmpty[] getYearShortArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.YEARSHORT$20, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getYearShortArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.YEARSHORT$20, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfYearShortArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.YEARSHORT$20);
        }
    }
    
    public void setYearShortArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.YEARSHORT$20);
        }
    }
    
    public void setYearShortArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.YEARSHORT$20, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewYearShort(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.YEARSHORT$20, n);
        }
    }
    
    public CTEmpty addNewYearShort() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.YEARSHORT$20);
        }
    }
    
    public void removeYearShort(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.YEARSHORT$20, n);
        }
    }
    
    public List<CTEmpty> getDayLongList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.DayLongList(this);
        }
    }
    
    public CTEmpty[] getDayLongArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.DAYLONG$22, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getDayLongArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.DAYLONG$22, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfDayLongArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.DAYLONG$22);
        }
    }
    
    public void setDayLongArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.DAYLONG$22);
        }
    }
    
    public void setDayLongArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.DAYLONG$22, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewDayLong(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.DAYLONG$22, n);
        }
    }
    
    public CTEmpty addNewDayLong() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.DAYLONG$22);
        }
    }
    
    public void removeDayLong(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.DAYLONG$22, n);
        }
    }
    
    public List<CTEmpty> getMonthLongList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.MonthLongList(this);
        }
    }
    
    public CTEmpty[] getMonthLongArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.MONTHLONG$24, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getMonthLongArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.MONTHLONG$24, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfMonthLongArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.MONTHLONG$24);
        }
    }
    
    public void setMonthLongArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.MONTHLONG$24);
        }
    }
    
    public void setMonthLongArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.MONTHLONG$24, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewMonthLong(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.MONTHLONG$24, n);
        }
    }
    
    public CTEmpty addNewMonthLong() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.MONTHLONG$24);
        }
    }
    
    public void removeMonthLong(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.MONTHLONG$24, n);
        }
    }
    
    public List<CTEmpty> getYearLongList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.YearLongList(this);
        }
    }
    
    public CTEmpty[] getYearLongArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.YEARLONG$26, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getYearLongArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.YEARLONG$26, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfYearLongArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.YEARLONG$26);
        }
    }
    
    public void setYearLongArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.YEARLONG$26);
        }
    }
    
    public void setYearLongArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.YEARLONG$26, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewYearLong(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.YEARLONG$26, n);
        }
    }
    
    public CTEmpty addNewYearLong() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.YEARLONG$26);
        }
    }
    
    public void removeYearLong(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.YEARLONG$26, n);
        }
    }
    
    public List<CTEmpty> getAnnotationRefList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.AnnotationRefList(this);
        }
    }
    
    public CTEmpty[] getAnnotationRefArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.ANNOTATIONREF$28, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getAnnotationRefArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.ANNOTATIONREF$28, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfAnnotationRefArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.ANNOTATIONREF$28);
        }
    }
    
    public void setAnnotationRefArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.ANNOTATIONREF$28);
        }
    }
    
    public void setAnnotationRefArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.ANNOTATIONREF$28, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewAnnotationRef(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.ANNOTATIONREF$28, n);
        }
    }
    
    public CTEmpty addNewAnnotationRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.ANNOTATIONREF$28);
        }
    }
    
    public void removeAnnotationRef(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.ANNOTATIONREF$28, n);
        }
    }
    
    public List<CTEmpty> getFootnoteRefList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.FootnoteRefList(this);
        }
    }
    
    public CTEmpty[] getFootnoteRefArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.FOOTNOTEREF$30, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getFootnoteRefArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.FOOTNOTEREF$30, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfFootnoteRefArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.FOOTNOTEREF$30);
        }
    }
    
    public void setFootnoteRefArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.FOOTNOTEREF$30);
        }
    }
    
    public void setFootnoteRefArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.FOOTNOTEREF$30, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewFootnoteRef(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.FOOTNOTEREF$30, n);
        }
    }
    
    public CTEmpty addNewFootnoteRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.FOOTNOTEREF$30);
        }
    }
    
    public void removeFootnoteRef(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.FOOTNOTEREF$30, n);
        }
    }
    
    public List<CTEmpty> getEndnoteRefList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.EndnoteRefList(this);
        }
    }
    
    public CTEmpty[] getEndnoteRefArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.ENDNOTEREF$32, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getEndnoteRefArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.ENDNOTEREF$32, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfEndnoteRefArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.ENDNOTEREF$32);
        }
    }
    
    public void setEndnoteRefArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.ENDNOTEREF$32);
        }
    }
    
    public void setEndnoteRefArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.ENDNOTEREF$32, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewEndnoteRef(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.ENDNOTEREF$32, n);
        }
    }
    
    public CTEmpty addNewEndnoteRef() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.ENDNOTEREF$32);
        }
    }
    
    public void removeEndnoteRef(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.ENDNOTEREF$32, n);
        }
    }
    
    public List<CTEmpty> getSeparatorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.SeparatorList(this);
        }
    }
    
    public CTEmpty[] getSeparatorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.SEPARATOR$34, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getSeparatorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.SEPARATOR$34, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfSeparatorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.SEPARATOR$34);
        }
    }
    
    public void setSeparatorArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.SEPARATOR$34);
        }
    }
    
    public void setSeparatorArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.SEPARATOR$34, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewSeparator(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.SEPARATOR$34, n);
        }
    }
    
    public CTEmpty addNewSeparator() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.SEPARATOR$34);
        }
    }
    
    public void removeSeparator(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.SEPARATOR$34, n);
        }
    }
    
    public List<CTEmpty> getContinuationSeparatorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.ContinuationSeparatorList(this);
        }
    }
    
    public CTEmpty[] getContinuationSeparatorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.CONTINUATIONSEPARATOR$36, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getContinuationSeparatorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.CONTINUATIONSEPARATOR$36, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfContinuationSeparatorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.CONTINUATIONSEPARATOR$36);
        }
    }
    
    public void setContinuationSeparatorArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.CONTINUATIONSEPARATOR$36);
        }
    }
    
    public void setContinuationSeparatorArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.CONTINUATIONSEPARATOR$36, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewContinuationSeparator(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.CONTINUATIONSEPARATOR$36, n);
        }
    }
    
    public CTEmpty addNewContinuationSeparator() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.CONTINUATIONSEPARATOR$36);
        }
    }
    
    public void removeContinuationSeparator(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.CONTINUATIONSEPARATOR$36, n);
        }
    }
    
    public List<CTSym> getSymList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSym>)new CTRImpl.SymList(this);
        }
    }
    
    public CTSym[] getSymArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.SYM$38, list);
            final CTSym[] array = new CTSym[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSym getSymArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSym ctSym = (CTSym)this.get_store().find_element_user(CTRImpl.SYM$38, n);
            if (ctSym == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSym;
        }
    }
    
    public int sizeOfSymArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.SYM$38);
        }
    }
    
    public void setSymArray(final CTSym[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.SYM$38);
        }
    }
    
    public void setSymArray(final int n, final CTSym ctSym) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSym ctSym2 = (CTSym)this.get_store().find_element_user(CTRImpl.SYM$38, n);
            if (ctSym2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSym2.set(ctSym);
        }
    }
    
    public CTSym insertNewSym(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSym)this.get_store().insert_element_user(CTRImpl.SYM$38, n);
        }
    }
    
    public CTSym addNewSym() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSym)this.get_store().add_element_user(CTRImpl.SYM$38);
        }
    }
    
    public void removeSym(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.SYM$38, n);
        }
    }
    
    public List<CTEmpty> getPgNumList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.PgNumList(this);
        }
    }
    
    public CTEmpty[] getPgNumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.PGNUM$40, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getPgNumArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.PGNUM$40, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfPgNumArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.PGNUM$40);
        }
    }
    
    public void setPgNumArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.PGNUM$40);
        }
    }
    
    public void setPgNumArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.PGNUM$40, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewPgNum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.PGNUM$40, n);
        }
    }
    
    public CTEmpty addNewPgNum() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.PGNUM$40);
        }
    }
    
    public void removePgNum(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.PGNUM$40, n);
        }
    }
    
    public List<CTEmpty> getCrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class CrList extends AbstractList<CTEmpty>
            {
                @Override
                public CTEmpty get(final int n) {
                    return CTRImpl.this.getCrArray(n);
                }
                
                @Override
                public CTEmpty set(final int n, final CTEmpty ctEmpty) {
                    final CTEmpty crArray = CTRImpl.this.getCrArray(n);
                    CTRImpl.this.setCrArray(n, ctEmpty);
                    return crArray;
                }
                
                @Override
                public void add(final int n, final CTEmpty ctEmpty) {
                    CTRImpl.this.insertNewCr(n).set(ctEmpty);
                }
                
                @Override
                public CTEmpty remove(final int n) {
                    final CTEmpty crArray = CTRImpl.this.getCrArray(n);
                    CTRImpl.this.removeCr(n);
                    return crArray;
                }
                
                @Override
                public int size() {
                    return CTRImpl.this.sizeOfCrArray();
                }
            }
            return new CrList();
        }
    }
    
    public CTEmpty[] getCrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.CR$42, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getCrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.CR$42, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfCrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.CR$42);
        }
    }
    
    public void setCrArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.CR$42);
        }
    }
    
    public void setCrArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.CR$42, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewCr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.CR$42, n);
        }
    }
    
    public CTEmpty addNewCr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.CR$42);
        }
    }
    
    public void removeCr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.CR$42, n);
        }
    }
    
    public List<CTEmpty> getTabList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.TabList(this);
        }
    }
    
    public CTEmpty[] getTabArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.TAB$44, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getTabArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.TAB$44, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfTabArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.TAB$44);
        }
    }
    
    public void setTabArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.TAB$44);
        }
    }
    
    public void setTabArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.TAB$44, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewTab(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.TAB$44, n);
        }
    }
    
    public CTEmpty addNewTab() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.TAB$44);
        }
    }
    
    public void removeTab(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.TAB$44, n);
        }
    }
    
    public List<CTObject> getObjectList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTObject>)new CTRImpl.ObjectList(this);
        }
    }
    
    public CTObject[] getObjectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.OBJECT$46, list);
            final CTObject[] array = new CTObject[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTObject getObjectArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTObject ctObject = (CTObject)this.get_store().find_element_user(CTRImpl.OBJECT$46, n);
            if (ctObject == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctObject;
        }
    }
    
    public int sizeOfObjectArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.OBJECT$46);
        }
    }
    
    public void setObjectArray(final CTObject[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.OBJECT$46);
        }
    }
    
    public void setObjectArray(final int n, final CTObject ctObject) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTObject ctObject2 = (CTObject)this.get_store().find_element_user(CTRImpl.OBJECT$46, n);
            if (ctObject2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctObject2.set(ctObject);
        }
    }
    
    public CTObject insertNewObject(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTObject)this.get_store().insert_element_user(CTRImpl.OBJECT$46, n);
        }
    }
    
    public CTObject addNewObject() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTObject)this.get_store().add_element_user(CTRImpl.OBJECT$46);
        }
    }
    
    public void removeObject(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.OBJECT$46, n);
        }
    }
    
    public List<CTPicture> getPictList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class PictList extends AbstractList<CTPicture>
            {
                @Override
                public CTPicture get(final int n) {
                    return CTRImpl.this.getPictArray(n);
                }
                
                @Override
                public CTPicture set(final int n, final CTPicture ctPicture) {
                    final CTPicture pictArray = CTRImpl.this.getPictArray(n);
                    CTRImpl.this.setPictArray(n, ctPicture);
                    return pictArray;
                }
                
                @Override
                public void add(final int n, final CTPicture ctPicture) {
                    CTRImpl.this.insertNewPict(n).set(ctPicture);
                }
                
                @Override
                public CTPicture remove(final int n) {
                    final CTPicture pictArray = CTRImpl.this.getPictArray(n);
                    CTRImpl.this.removePict(n);
                    return pictArray;
                }
                
                @Override
                public int size() {
                    return CTRImpl.this.sizeOfPictArray();
                }
            }
            return new PictList();
        }
    }
    
    public CTPicture[] getPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.PICT$48, list);
            final CTPicture[] array = new CTPicture[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPicture getPictArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPicture ctPicture = (CTPicture)this.get_store().find_element_user(CTRImpl.PICT$48, n);
            if (ctPicture == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPicture;
        }
    }
    
    public int sizeOfPictArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.PICT$48);
        }
    }
    
    public void setPictArray(final CTPicture[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.PICT$48);
        }
    }
    
    public void setPictArray(final int n, final CTPicture ctPicture) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPicture ctPicture2 = (CTPicture)this.get_store().find_element_user(CTRImpl.PICT$48, n);
            if (ctPicture2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPicture2.set(ctPicture);
        }
    }
    
    public CTPicture insertNewPict(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPicture)this.get_store().insert_element_user(CTRImpl.PICT$48, n);
        }
    }
    
    public CTPicture addNewPict() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPicture)this.get_store().add_element_user(CTRImpl.PICT$48);
        }
    }
    
    public void removePict(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.PICT$48, n);
        }
    }
    
    public List<CTFldChar> getFldCharList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFldChar>)new CTRImpl.FldCharList(this);
        }
    }
    
    public CTFldChar[] getFldCharArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.FLDCHAR$50, list);
            final CTFldChar[] array = new CTFldChar[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFldChar getFldCharArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFldChar ctFldChar = (CTFldChar)this.get_store().find_element_user(CTRImpl.FLDCHAR$50, n);
            if (ctFldChar == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFldChar;
        }
    }
    
    public int sizeOfFldCharArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.FLDCHAR$50);
        }
    }
    
    public void setFldCharArray(final CTFldChar[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.FLDCHAR$50);
        }
    }
    
    public void setFldCharArray(final int n, final CTFldChar ctFldChar) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFldChar ctFldChar2 = (CTFldChar)this.get_store().find_element_user(CTRImpl.FLDCHAR$50, n);
            if (ctFldChar2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFldChar2.set(ctFldChar);
        }
    }
    
    public CTFldChar insertNewFldChar(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFldChar)this.get_store().insert_element_user(CTRImpl.FLDCHAR$50, n);
        }
    }
    
    public CTFldChar addNewFldChar() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFldChar)this.get_store().add_element_user(CTRImpl.FLDCHAR$50);
        }
    }
    
    public void removeFldChar(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.FLDCHAR$50, n);
        }
    }
    
    public List<CTRuby> getRubyList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRuby>)new CTRImpl.RubyList(this);
        }
    }
    
    public CTRuby[] getRubyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.RUBY$52, list);
            final CTRuby[] array = new CTRuby[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRuby getRubyArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRuby ctRuby = (CTRuby)this.get_store().find_element_user(CTRImpl.RUBY$52, n);
            if (ctRuby == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRuby;
        }
    }
    
    public int sizeOfRubyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.RUBY$52);
        }
    }
    
    public void setRubyArray(final CTRuby[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRImpl.RUBY$52);
        }
    }
    
    public void setRubyArray(final int n, final CTRuby ctRuby) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRuby ctRuby2 = (CTRuby)this.get_store().find_element_user(CTRImpl.RUBY$52, n);
            if (ctRuby2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRuby2.set((XmlObject)ctRuby);
        }
    }
    
    public CTRuby insertNewRuby(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRuby)this.get_store().insert_element_user(CTRImpl.RUBY$52, n);
        }
    }
    
    public CTRuby addNewRuby() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRuby)this.get_store().add_element_user(CTRImpl.RUBY$52);
        }
    }
    
    public void removeRuby(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.RUBY$52, n);
        }
    }
    
    public List<CTFtnEdnRef> getFootnoteReferenceList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFtnEdnRef>)new CTRImpl.FootnoteReferenceList(this);
        }
    }
    
    public CTFtnEdnRef[] getFootnoteReferenceArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.FOOTNOTEREFERENCE$54, list);
            final CTFtnEdnRef[] array = new CTFtnEdnRef[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFtnEdnRef getFootnoteReferenceArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFtnEdnRef ctFtnEdnRef = (CTFtnEdnRef)this.get_store().find_element_user(CTRImpl.FOOTNOTEREFERENCE$54, n);
            if (ctFtnEdnRef == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFtnEdnRef;
        }
    }
    
    public int sizeOfFootnoteReferenceArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.FOOTNOTEREFERENCE$54);
        }
    }
    
    public void setFootnoteReferenceArray(final CTFtnEdnRef[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.FOOTNOTEREFERENCE$54);
        }
    }
    
    public void setFootnoteReferenceArray(final int n, final CTFtnEdnRef ctFtnEdnRef) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFtnEdnRef ctFtnEdnRef2 = (CTFtnEdnRef)this.get_store().find_element_user(CTRImpl.FOOTNOTEREFERENCE$54, n);
            if (ctFtnEdnRef2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFtnEdnRef2.set(ctFtnEdnRef);
        }
    }
    
    public CTFtnEdnRef insertNewFootnoteReference(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFtnEdnRef)this.get_store().insert_element_user(CTRImpl.FOOTNOTEREFERENCE$54, n);
        }
    }
    
    public CTFtnEdnRef addNewFootnoteReference() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFtnEdnRef)this.get_store().add_element_user(CTRImpl.FOOTNOTEREFERENCE$54);
        }
    }
    
    public void removeFootnoteReference(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.FOOTNOTEREFERENCE$54, n);
        }
    }
    
    public List<CTFtnEdnRef> getEndnoteReferenceList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFtnEdnRef>)new CTRImpl.EndnoteReferenceList(this);
        }
    }
    
    public CTFtnEdnRef[] getEndnoteReferenceArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.ENDNOTEREFERENCE$56, list);
            final CTFtnEdnRef[] array = new CTFtnEdnRef[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFtnEdnRef getEndnoteReferenceArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFtnEdnRef ctFtnEdnRef = (CTFtnEdnRef)this.get_store().find_element_user(CTRImpl.ENDNOTEREFERENCE$56, n);
            if (ctFtnEdnRef == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFtnEdnRef;
        }
    }
    
    public int sizeOfEndnoteReferenceArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.ENDNOTEREFERENCE$56);
        }
    }
    
    public void setEndnoteReferenceArray(final CTFtnEdnRef[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.ENDNOTEREFERENCE$56);
        }
    }
    
    public void setEndnoteReferenceArray(final int n, final CTFtnEdnRef ctFtnEdnRef) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFtnEdnRef ctFtnEdnRef2 = (CTFtnEdnRef)this.get_store().find_element_user(CTRImpl.ENDNOTEREFERENCE$56, n);
            if (ctFtnEdnRef2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFtnEdnRef2.set(ctFtnEdnRef);
        }
    }
    
    public CTFtnEdnRef insertNewEndnoteReference(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFtnEdnRef)this.get_store().insert_element_user(CTRImpl.ENDNOTEREFERENCE$56, n);
        }
    }
    
    public CTFtnEdnRef addNewEndnoteReference() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFtnEdnRef)this.get_store().add_element_user(CTRImpl.ENDNOTEREFERENCE$56);
        }
    }
    
    public void removeEndnoteReference(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.ENDNOTEREFERENCE$56, n);
        }
    }
    
    public List<CTMarkup> getCommentReferenceList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTRImpl.CommentReferenceList(this);
        }
    }
    
    public CTMarkup[] getCommentReferenceArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.COMMENTREFERENCE$58, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCommentReferenceArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTRImpl.COMMENTREFERENCE$58, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCommentReferenceArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.COMMENTREFERENCE$58);
        }
    }
    
    public void setCommentReferenceArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.COMMENTREFERENCE$58);
        }
    }
    
    public void setCommentReferenceArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTRImpl.COMMENTREFERENCE$58, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCommentReference(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTRImpl.COMMENTREFERENCE$58, n);
        }
    }
    
    public CTMarkup addNewCommentReference() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTRImpl.COMMENTREFERENCE$58);
        }
    }
    
    public void removeCommentReference(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.COMMENTREFERENCE$58, n);
        }
    }
    
    public List<CTDrawing> getDrawingList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class DrawingList extends AbstractList<CTDrawing>
            {
                @Override
                public CTDrawing get(final int n) {
                    return CTRImpl.this.getDrawingArray(n);
                }
                
                @Override
                public CTDrawing set(final int n, final CTDrawing ctDrawing) {
                    final CTDrawing drawingArray = CTRImpl.this.getDrawingArray(n);
                    CTRImpl.this.setDrawingArray(n, ctDrawing);
                    return drawingArray;
                }
                
                @Override
                public void add(final int n, final CTDrawing ctDrawing) {
                    CTRImpl.this.insertNewDrawing(n).set(ctDrawing);
                }
                
                @Override
                public CTDrawing remove(final int n) {
                    final CTDrawing drawingArray = CTRImpl.this.getDrawingArray(n);
                    CTRImpl.this.removeDrawing(n);
                    return drawingArray;
                }
                
                @Override
                public int size() {
                    return CTRImpl.this.sizeOfDrawingArray();
                }
            }
            return new DrawingList();
        }
    }
    
    public CTDrawing[] getDrawingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.DRAWING$60, list);
            final CTDrawing[] array = new CTDrawing[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDrawing getDrawingArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDrawing ctDrawing = (CTDrawing)this.get_store().find_element_user(CTRImpl.DRAWING$60, n);
            if (ctDrawing == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDrawing;
        }
    }
    
    public int sizeOfDrawingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.DRAWING$60);
        }
    }
    
    public void setDrawingArray(final CTDrawing[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.DRAWING$60);
        }
    }
    
    public void setDrawingArray(final int n, final CTDrawing ctDrawing) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDrawing ctDrawing2 = (CTDrawing)this.get_store().find_element_user(CTRImpl.DRAWING$60, n);
            if (ctDrawing2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDrawing2.set(ctDrawing);
        }
    }
    
    public CTDrawing insertNewDrawing(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDrawing)this.get_store().insert_element_user(CTRImpl.DRAWING$60, n);
        }
    }
    
    public CTDrawing addNewDrawing() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDrawing)this.get_store().add_element_user(CTRImpl.DRAWING$60);
        }
    }
    
    public void removeDrawing(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.DRAWING$60, n);
        }
    }
    
    public List<CTPTab> getPtabList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPTab>)new CTRImpl.PtabList(this);
        }
    }
    
    public CTPTab[] getPtabArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.PTAB$62, list);
            final CTPTab[] array = new CTPTab[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPTab getPtabArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPTab ctpTab = (CTPTab)this.get_store().find_element_user(CTRImpl.PTAB$62, n);
            if (ctpTab == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctpTab;
        }
    }
    
    public int sizeOfPtabArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.PTAB$62);
        }
    }
    
    public void setPtabArray(final CTPTab[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.PTAB$62);
        }
    }
    
    public void setPtabArray(final int n, final CTPTab ctpTab) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPTab ctpTab2 = (CTPTab)this.get_store().find_element_user(CTRImpl.PTAB$62, n);
            if (ctpTab2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctpTab2.set(ctpTab);
        }
    }
    
    public CTPTab insertNewPtab(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPTab)this.get_store().insert_element_user(CTRImpl.PTAB$62, n);
        }
    }
    
    public CTPTab addNewPtab() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPTab)this.get_store().add_element_user(CTRImpl.PTAB$62);
        }
    }
    
    public void removePtab(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.PTAB$62, n);
        }
    }
    
    public List<CTEmpty> getLastRenderedPageBreakList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTRImpl.LastRenderedPageBreakList(this);
        }
    }
    
    public CTEmpty[] getLastRenderedPageBreakArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRImpl.LASTRENDEREDPAGEBREAK$64, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getLastRenderedPageBreakArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTRImpl.LASTRENDEREDPAGEBREAK$64, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfLastRenderedPageBreakArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRImpl.LASTRENDEREDPAGEBREAK$64);
        }
    }
    
    public void setLastRenderedPageBreakArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRImpl.LASTRENDEREDPAGEBREAK$64);
        }
    }
    
    public void setLastRenderedPageBreakArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTRImpl.LASTRENDEREDPAGEBREAK$64, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewLastRenderedPageBreak(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTRImpl.LASTRENDEREDPAGEBREAK$64, n);
        }
    }
    
    public CTEmpty addNewLastRenderedPageBreak() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTRImpl.LASTRENDEREDPAGEBREAK$64);
        }
    }
    
    public void removeLastRenderedPageBreak(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRImpl.LASTRENDEREDPAGEBREAK$64, n);
        }
    }
    
    public byte[] getRsidRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTRImpl.RSIDRPR$66);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getByteArrayValue();
        }
    }
    
    public STLongHexNumber xgetRsidRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STLongHexNumber)this.get_store().find_attribute_user(CTRImpl.RSIDRPR$66);
        }
    }
    
    public boolean isSetRsidRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTRImpl.RSIDRPR$66) != null;
        }
    }
    
    public void setRsidRPr(final byte[] byteArrayValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTRImpl.RSIDRPR$66);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTRImpl.RSIDRPR$66);
            }
            simpleValue.setByteArrayValue(byteArrayValue);
        }
    }
    
    public void xsetRsidRPr(final STLongHexNumber stLongHexNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STLongHexNumber stLongHexNumber2 = (STLongHexNumber)this.get_store().find_attribute_user(CTRImpl.RSIDRPR$66);
            if (stLongHexNumber2 == null) {
                stLongHexNumber2 = (STLongHexNumber)this.get_store().add_attribute_user(CTRImpl.RSIDRPR$66);
            }
            stLongHexNumber2.set(stLongHexNumber);
        }
    }
    
    public void unsetRsidRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTRImpl.RSIDRPR$66);
        }
    }
    
    public byte[] getRsidDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTRImpl.RSIDDEL$68);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getByteArrayValue();
        }
    }
    
    public STLongHexNumber xgetRsidDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STLongHexNumber)this.get_store().find_attribute_user(CTRImpl.RSIDDEL$68);
        }
    }
    
    public boolean isSetRsidDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTRImpl.RSIDDEL$68) != null;
        }
    }
    
    public void setRsidDel(final byte[] byteArrayValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTRImpl.RSIDDEL$68);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTRImpl.RSIDDEL$68);
            }
            simpleValue.setByteArrayValue(byteArrayValue);
        }
    }
    
    public void xsetRsidDel(final STLongHexNumber stLongHexNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STLongHexNumber stLongHexNumber2 = (STLongHexNumber)this.get_store().find_attribute_user(CTRImpl.RSIDDEL$68);
            if (stLongHexNumber2 == null) {
                stLongHexNumber2 = (STLongHexNumber)this.get_store().add_attribute_user(CTRImpl.RSIDDEL$68);
            }
            stLongHexNumber2.set(stLongHexNumber);
        }
    }
    
    public void unsetRsidDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTRImpl.RSIDDEL$68);
        }
    }
    
    public byte[] getRsidR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTRImpl.RSIDR$70);
            if (simpleValue == null) {
                return null;
            }
            return simpleValue.getByteArrayValue();
        }
    }
    
    public STLongHexNumber xgetRsidR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STLongHexNumber)this.get_store().find_attribute_user(CTRImpl.RSIDR$70);
        }
    }
    
    public boolean isSetRsidR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(CTRImpl.RSIDR$70) != null;
        }
    }
    
    public void setRsidR(final byte[] byteArrayValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTRImpl.RSIDR$70);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTRImpl.RSIDR$70);
            }
            simpleValue.setByteArrayValue(byteArrayValue);
        }
    }
    
    public void xsetRsidR(final STLongHexNumber stLongHexNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STLongHexNumber stLongHexNumber2 = (STLongHexNumber)this.get_store().find_attribute_user(CTRImpl.RSIDR$70);
            if (stLongHexNumber2 == null) {
                stLongHexNumber2 = (STLongHexNumber)this.get_store().add_attribute_user(CTRImpl.RSIDR$70);
            }
            stLongHexNumber2.set(stLongHexNumber);
        }
    }
    
    public void unsetRsidR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(CTRImpl.RSIDR$70);
        }
    }
    
    static {
        RPR$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rPr");
        BR$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "br");
        T$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "t");
        DELTEXT$6 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "delText");
        INSTRTEXT$8 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "instrText");
        DELINSTRTEXT$10 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "delInstrText");
        NOBREAKHYPHEN$12 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "noBreakHyphen");
        SOFTHYPHEN$14 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "softHyphen");
        DAYSHORT$16 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "dayShort");
        MONTHSHORT$18 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "monthShort");
        YEARSHORT$20 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "yearShort");
        DAYLONG$22 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "dayLong");
        MONTHLONG$24 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "monthLong");
        YEARLONG$26 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "yearLong");
        ANNOTATIONREF$28 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "annotationRef");
        FOOTNOTEREF$30 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "footnoteRef");
        ENDNOTEREF$32 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "endnoteRef");
        SEPARATOR$34 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "separator");
        CONTINUATIONSEPARATOR$36 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "continuationSeparator");
        SYM$38 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "sym");
        PGNUM$40 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "pgNum");
        CR$42 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "cr");
        TAB$44 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tab");
        OBJECT$46 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "object");
        PICT$48 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "pict");
        FLDCHAR$50 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "fldChar");
        RUBY$52 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "ruby");
        FOOTNOTEREFERENCE$54 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "footnoteReference");
        ENDNOTEREFERENCE$56 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "endnoteReference");
        COMMENTREFERENCE$58 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "commentReference");
        DRAWING$60 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "drawing");
        PTAB$62 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "ptab");
        LASTRENDEREDPAGEBREAK$64 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "lastRenderedPageBreak");
        RSIDRPR$66 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rsidRPr");
        RSIDDEL$68 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rsidDel");
        RSIDR$70 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rsidR");
    }
}
