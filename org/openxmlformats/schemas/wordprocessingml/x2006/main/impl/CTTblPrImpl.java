// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPrChange;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;

public class CTTblPrImpl extends CTTblPrBaseImpl implements CTTblPr
{
    private static final QName TBLPRCHANGE$0;
    
    public CTTblPrImpl(final SchemaType schemaType) {
        super(schemaType);
    }
    
    public CTTblPrChange getTblPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblPrChange ctTblPrChange = (CTTblPrChange)this.get_store().find_element_user(CTTblPrImpl.TBLPRCHANGE$0, 0);
            if (ctTblPrChange == null) {
                return null;
            }
            return ctTblPrChange;
        }
    }
    
    public boolean isSetTblPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblPrImpl.TBLPRCHANGE$0) != 0;
        }
    }
    
    public void setTblPrChange(final CTTblPrChange ctTblPrChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTblPrChange ctTblPrChange2 = (CTTblPrChange)this.get_store().find_element_user(CTTblPrImpl.TBLPRCHANGE$0, 0);
            if (ctTblPrChange2 == null) {
                ctTblPrChange2 = (CTTblPrChange)this.get_store().add_element_user(CTTblPrImpl.TBLPRCHANGE$0);
            }
            ctTblPrChange2.set((XmlObject)ctTblPrChange);
        }
    }
    
    public CTTblPrChange addNewTblPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblPrChange)this.get_store().add_element_user(CTTblPrImpl.TBLPRCHANGE$0);
        }
    }
    
    public void unsetTblPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblPrImpl.TBLPRCHANGE$0, 0);
        }
    }
    
    static {
        TBLPRCHANGE$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tblPrChange");
    }
}
