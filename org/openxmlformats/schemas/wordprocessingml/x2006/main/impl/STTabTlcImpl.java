// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTabTlc;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTabTlcImpl extends JavaStringEnumerationHolderEx implements STTabTlc
{
    public STTabTlcImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTabTlcImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
