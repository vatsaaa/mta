// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTwipsMeasure;
import org.apache.xmlbeans.impl.values.JavaIntegerHolderEx;

public class STTwipsMeasureImpl extends JavaIntegerHolderEx implements STTwipsMeasure
{
    public STTwipsMeasureImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTwipsMeasureImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
