// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STString;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STStringImpl extends JavaStringHolderEx implements STString
{
    public STStringImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STStringImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
