// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMath;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMathPara;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRunTrackChange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPerm;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPermStart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTProofErr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTCustomXmlRow;
import java.util.AbstractList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblGrid;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrackChange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMoveBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkupRange;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTTblImpl extends XmlComplexContentImpl implements CTTbl
{
    private static final QName BOOKMARKSTART$0;
    private static final QName BOOKMARKEND$2;
    private static final QName MOVEFROMRANGESTART$4;
    private static final QName MOVEFROMRANGEEND$6;
    private static final QName MOVETORANGESTART$8;
    private static final QName MOVETORANGEEND$10;
    private static final QName COMMENTRANGESTART$12;
    private static final QName COMMENTRANGEEND$14;
    private static final QName CUSTOMXMLINSRANGESTART$16;
    private static final QName CUSTOMXMLINSRANGEEND$18;
    private static final QName CUSTOMXMLDELRANGESTART$20;
    private static final QName CUSTOMXMLDELRANGEEND$22;
    private static final QName CUSTOMXMLMOVEFROMRANGESTART$24;
    private static final QName CUSTOMXMLMOVEFROMRANGEEND$26;
    private static final QName CUSTOMXMLMOVETORANGESTART$28;
    private static final QName CUSTOMXMLMOVETORANGEEND$30;
    private static final QName TBLPR$32;
    private static final QName TBLGRID$34;
    private static final QName TR$36;
    private static final QName CUSTOMXML$38;
    private static final QName SDT$40;
    private static final QName PROOFERR$42;
    private static final QName PERMSTART$44;
    private static final QName PERMEND$46;
    private static final QName INS$48;
    private static final QName DEL$50;
    private static final QName MOVEFROM$52;
    private static final QName MOVETO$54;
    private static final QName OMATHPARA$56;
    private static final QName OMATH$58;
    
    public CTTblImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTBookmark> getBookmarkStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBookmark>)new CTTblImpl.BookmarkStartList(this);
        }
    }
    
    public CTBookmark[] getBookmarkStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.BOOKMARKSTART$0, list);
            final CTBookmark[] array = new CTBookmark[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBookmark getBookmarkStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBookmark ctBookmark = (CTBookmark)this.get_store().find_element_user(CTTblImpl.BOOKMARKSTART$0, n);
            if (ctBookmark == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBookmark;
        }
    }
    
    public int sizeOfBookmarkStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.BOOKMARKSTART$0);
        }
    }
    
    public void setBookmarkStartArray(final CTBookmark[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.BOOKMARKSTART$0);
        }
    }
    
    public void setBookmarkStartArray(final int n, final CTBookmark ctBookmark) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBookmark ctBookmark2 = (CTBookmark)this.get_store().find_element_user(CTTblImpl.BOOKMARKSTART$0, n);
            if (ctBookmark2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBookmark2.set(ctBookmark);
        }
    }
    
    public CTBookmark insertNewBookmarkStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBookmark)this.get_store().insert_element_user(CTTblImpl.BOOKMARKSTART$0, n);
        }
    }
    
    public CTBookmark addNewBookmarkStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBookmark)this.get_store().add_element_user(CTTblImpl.BOOKMARKSTART$0);
        }
    }
    
    public void removeBookmarkStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.BOOKMARKSTART$0, n);
        }
    }
    
    public List<CTMarkupRange> getBookmarkEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTTblImpl.BookmarkEndList(this);
        }
    }
    
    public CTMarkupRange[] getBookmarkEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.BOOKMARKEND$2, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getBookmarkEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.BOOKMARKEND$2, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfBookmarkEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.BOOKMARKEND$2);
        }
    }
    
    public void setBookmarkEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.BOOKMARKEND$2);
        }
    }
    
    public void setBookmarkEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.BOOKMARKEND$2, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewBookmarkEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTTblImpl.BOOKMARKEND$2, n);
        }
    }
    
    public CTMarkupRange addNewBookmarkEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTTblImpl.BOOKMARKEND$2);
        }
    }
    
    public void removeBookmarkEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.BOOKMARKEND$2, n);
        }
    }
    
    public List<CTMoveBookmark> getMoveFromRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMoveBookmark>)new CTTblImpl.MoveFromRangeStartList(this);
        }
    }
    
    public CTMoveBookmark[] getMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.MOVEFROMRANGESTART$4, list);
            final CTMoveBookmark[] array = new CTMoveBookmark[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMoveBookmark getMoveFromRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark = (CTMoveBookmark)this.get_store().find_element_user(CTTblImpl.MOVEFROMRANGESTART$4, n);
            if (ctMoveBookmark == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMoveBookmark;
        }
    }
    
    public int sizeOfMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.MOVEFROMRANGESTART$4);
        }
    }
    
    public void setMoveFromRangeStartArray(final CTMoveBookmark[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.MOVEFROMRANGESTART$4);
        }
    }
    
    public void setMoveFromRangeStartArray(final int n, final CTMoveBookmark ctMoveBookmark) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark2 = (CTMoveBookmark)this.get_store().find_element_user(CTTblImpl.MOVEFROMRANGESTART$4, n);
            if (ctMoveBookmark2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMoveBookmark2.set((XmlObject)ctMoveBookmark);
        }
    }
    
    public CTMoveBookmark insertNewMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().insert_element_user(CTTblImpl.MOVEFROMRANGESTART$4, n);
        }
    }
    
    public CTMoveBookmark addNewMoveFromRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().add_element_user(CTTblImpl.MOVEFROMRANGESTART$4);
        }
    }
    
    public void removeMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.MOVEFROMRANGESTART$4, n);
        }
    }
    
    public List<CTMarkupRange> getMoveFromRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTTblImpl.MoveFromRangeEndList(this);
        }
    }
    
    public CTMarkupRange[] getMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.MOVEFROMRANGEEND$6, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getMoveFromRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.MOVEFROMRANGEEND$6, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.MOVEFROMRANGEEND$6);
        }
    }
    
    public void setMoveFromRangeEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.MOVEFROMRANGEEND$6);
        }
    }
    
    public void setMoveFromRangeEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.MOVEFROMRANGEEND$6, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTTblImpl.MOVEFROMRANGEEND$6, n);
        }
    }
    
    public CTMarkupRange addNewMoveFromRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTTblImpl.MOVEFROMRANGEEND$6);
        }
    }
    
    public void removeMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.MOVEFROMRANGEEND$6, n);
        }
    }
    
    public List<CTMoveBookmark> getMoveToRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMoveBookmark>)new CTTblImpl.MoveToRangeStartList(this);
        }
    }
    
    public CTMoveBookmark[] getMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.MOVETORANGESTART$8, list);
            final CTMoveBookmark[] array = new CTMoveBookmark[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMoveBookmark getMoveToRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark = (CTMoveBookmark)this.get_store().find_element_user(CTTblImpl.MOVETORANGESTART$8, n);
            if (ctMoveBookmark == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMoveBookmark;
        }
    }
    
    public int sizeOfMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.MOVETORANGESTART$8);
        }
    }
    
    public void setMoveToRangeStartArray(final CTMoveBookmark[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.MOVETORANGESTART$8);
        }
    }
    
    public void setMoveToRangeStartArray(final int n, final CTMoveBookmark ctMoveBookmark) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark2 = (CTMoveBookmark)this.get_store().find_element_user(CTTblImpl.MOVETORANGESTART$8, n);
            if (ctMoveBookmark2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMoveBookmark2.set((XmlObject)ctMoveBookmark);
        }
    }
    
    public CTMoveBookmark insertNewMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().insert_element_user(CTTblImpl.MOVETORANGESTART$8, n);
        }
    }
    
    public CTMoveBookmark addNewMoveToRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().add_element_user(CTTblImpl.MOVETORANGESTART$8);
        }
    }
    
    public void removeMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.MOVETORANGESTART$8, n);
        }
    }
    
    public List<CTMarkupRange> getMoveToRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTTblImpl.MoveToRangeEndList(this);
        }
    }
    
    public CTMarkupRange[] getMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.MOVETORANGEEND$10, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getMoveToRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.MOVETORANGEEND$10, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.MOVETORANGEEND$10);
        }
    }
    
    public void setMoveToRangeEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.MOVETORANGEEND$10);
        }
    }
    
    public void setMoveToRangeEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.MOVETORANGEEND$10, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTTblImpl.MOVETORANGEEND$10, n);
        }
    }
    
    public CTMarkupRange addNewMoveToRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTTblImpl.MOVETORANGEEND$10);
        }
    }
    
    public void removeMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.MOVETORANGEEND$10, n);
        }
    }
    
    public List<CTMarkupRange> getCommentRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTTblImpl.CommentRangeStartList(this);
        }
    }
    
    public CTMarkupRange[] getCommentRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.COMMENTRANGESTART$12, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getCommentRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.COMMENTRANGESTART$12, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfCommentRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.COMMENTRANGESTART$12);
        }
    }
    
    public void setCommentRangeStartArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.COMMENTRANGESTART$12);
        }
    }
    
    public void setCommentRangeStartArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.COMMENTRANGESTART$12, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewCommentRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTTblImpl.COMMENTRANGESTART$12, n);
        }
    }
    
    public CTMarkupRange addNewCommentRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTTblImpl.COMMENTRANGESTART$12);
        }
    }
    
    public void removeCommentRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.COMMENTRANGESTART$12, n);
        }
    }
    
    public List<CTMarkupRange> getCommentRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTTblImpl.CommentRangeEndList(this);
        }
    }
    
    public CTMarkupRange[] getCommentRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.COMMENTRANGEEND$14, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getCommentRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.COMMENTRANGEEND$14, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfCommentRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.COMMENTRANGEEND$14);
        }
    }
    
    public void setCommentRangeEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.COMMENTRANGEEND$14);
        }
    }
    
    public void setCommentRangeEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTTblImpl.COMMENTRANGEEND$14, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewCommentRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTTblImpl.COMMENTRANGEEND$14, n);
        }
    }
    
    public CTMarkupRange addNewCommentRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTTblImpl.COMMENTRANGEEND$14);
        }
    }
    
    public void removeCommentRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.COMMENTRANGEEND$14, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlInsRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTTblImpl.CustomXmlInsRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlInsRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLINSRANGESTART$16, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlInsRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLINSRANGESTART$16, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlInsRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLINSRANGESTART$16);
        }
    }
    
    public void setCustomXmlInsRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLINSRANGESTART$16);
        }
    }
    
    public void setCustomXmlInsRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLINSRANGESTART$16, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlInsRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLINSRANGESTART$16, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlInsRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLINSRANGESTART$16);
        }
    }
    
    public void removeCustomXmlInsRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLINSRANGESTART$16, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlInsRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTTblImpl.CustomXmlInsRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlInsRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLINSRANGEEND$18, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlInsRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLINSRANGEEND$18, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlInsRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLINSRANGEEND$18);
        }
    }
    
    public void setCustomXmlInsRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLINSRANGEEND$18);
        }
    }
    
    public void setCustomXmlInsRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLINSRANGEEND$18, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlInsRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLINSRANGEEND$18, n);
        }
    }
    
    public CTMarkup addNewCustomXmlInsRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLINSRANGEEND$18);
        }
    }
    
    public void removeCustomXmlInsRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLINSRANGEEND$18, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlDelRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTTblImpl.CustomXmlDelRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlDelRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLDELRANGESTART$20, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlDelRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLDELRANGESTART$20, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlDelRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLDELRANGESTART$20);
        }
    }
    
    public void setCustomXmlDelRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLDELRANGESTART$20);
        }
    }
    
    public void setCustomXmlDelRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLDELRANGESTART$20, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlDelRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLDELRANGESTART$20, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlDelRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLDELRANGESTART$20);
        }
    }
    
    public void removeCustomXmlDelRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLDELRANGESTART$20, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlDelRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTTblImpl.CustomXmlDelRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlDelRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLDELRANGEEND$22, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlDelRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLDELRANGEEND$22, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlDelRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLDELRANGEEND$22);
        }
    }
    
    public void setCustomXmlDelRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLDELRANGEEND$22);
        }
    }
    
    public void setCustomXmlDelRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLDELRANGEEND$22, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlDelRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLDELRANGEEND$22, n);
        }
    }
    
    public CTMarkup addNewCustomXmlDelRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLDELRANGEEND$22);
        }
    }
    
    public void removeCustomXmlDelRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLDELRANGEEND$22, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlMoveFromRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTTblImpl.CustomXmlMoveFromRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlMoveFromRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24);
        }
    }
    
    public void setCustomXmlMoveFromRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24);
        }
    }
    
    public void setCustomXmlMoveFromRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlMoveFromRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24);
        }
    }
    
    public void removeCustomXmlMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLMOVEFROMRANGESTART$24, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlMoveFromRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTTblImpl.CustomXmlMoveFromRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlMoveFromRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26);
        }
    }
    
    public void setCustomXmlMoveFromRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26);
        }
    }
    
    public void setCustomXmlMoveFromRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26, n);
        }
    }
    
    public CTMarkup addNewCustomXmlMoveFromRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26);
        }
    }
    
    public void removeCustomXmlMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLMOVEFROMRANGEEND$26, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlMoveToRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTTblImpl.CustomXmlMoveToRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLMOVETORANGESTART$28, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlMoveToRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVETORANGESTART$28, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLMOVETORANGESTART$28);
        }
    }
    
    public void setCustomXmlMoveToRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLMOVETORANGESTART$28);
        }
    }
    
    public void setCustomXmlMoveToRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVETORANGESTART$28, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLMOVETORANGESTART$28, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlMoveToRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLMOVETORANGESTART$28);
        }
    }
    
    public void removeCustomXmlMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLMOVETORANGESTART$28, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlMoveToRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTTblImpl.CustomXmlMoveToRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXMLMOVETORANGEEND$30, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlMoveToRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVETORANGEEND$30, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXMLMOVETORANGEEND$30);
        }
    }
    
    public void setCustomXmlMoveToRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.CUSTOMXMLMOVETORANGEEND$30);
        }
    }
    
    public void setCustomXmlMoveToRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTTblImpl.CUSTOMXMLMOVETORANGEEND$30, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTTblImpl.CUSTOMXMLMOVETORANGEEND$30, n);
        }
    }
    
    public CTMarkup addNewCustomXmlMoveToRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTTblImpl.CUSTOMXMLMOVETORANGEEND$30);
        }
    }
    
    public void removeCustomXmlMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXMLMOVETORANGEEND$30, n);
        }
    }
    
    public CTTblPr getTblPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblPr ctTblPr = (CTTblPr)this.get_store().find_element_user(CTTblImpl.TBLPR$32, 0);
            if (ctTblPr == null) {
                return null;
            }
            return ctTblPr;
        }
    }
    
    public void setTblPr(final CTTblPr ctTblPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTblPr ctTblPr2 = (CTTblPr)this.get_store().find_element_user(CTTblImpl.TBLPR$32, 0);
            if (ctTblPr2 == null) {
                ctTblPr2 = (CTTblPr)this.get_store().add_element_user(CTTblImpl.TBLPR$32);
            }
            ctTblPr2.set(ctTblPr);
        }
    }
    
    public CTTblPr addNewTblPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblPr)this.get_store().add_element_user(CTTblImpl.TBLPR$32);
        }
    }
    
    public CTTblGrid getTblGrid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTblGrid ctTblGrid = (CTTblGrid)this.get_store().find_element_user(CTTblImpl.TBLGRID$34, 0);
            if (ctTblGrid == null) {
                return null;
            }
            return ctTblGrid;
        }
    }
    
    public void setTblGrid(final CTTblGrid ctTblGrid) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTblGrid ctTblGrid2 = (CTTblGrid)this.get_store().find_element_user(CTTblImpl.TBLGRID$34, 0);
            if (ctTblGrid2 == null) {
                ctTblGrid2 = (CTTblGrid)this.get_store().add_element_user(CTTblImpl.TBLGRID$34);
            }
            ctTblGrid2.set((XmlObject)ctTblGrid);
        }
    }
    
    public CTTblGrid addNewTblGrid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTblGrid)this.get_store().add_element_user(CTTblImpl.TBLGRID$34);
        }
    }
    
    public List<CTRow> getTrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class TrList extends AbstractList<CTRow>
            {
                @Override
                public CTRow get(final int n) {
                    return CTTblImpl.this.getTrArray(n);
                }
                
                @Override
                public CTRow set(final int n, final CTRow ctRow) {
                    final CTRow trArray = CTTblImpl.this.getTrArray(n);
                    CTTblImpl.this.setTrArray(n, ctRow);
                    return trArray;
                }
                
                @Override
                public void add(final int n, final CTRow ctRow) {
                    CTTblImpl.this.insertNewTr(n).set(ctRow);
                }
                
                @Override
                public CTRow remove(final int n) {
                    final CTRow trArray = CTTblImpl.this.getTrArray(n);
                    CTTblImpl.this.removeTr(n);
                    return trArray;
                }
                
                @Override
                public int size() {
                    return CTTblImpl.this.sizeOfTrArray();
                }
            }
            return new TrList();
        }
    }
    
    public CTRow[] getTrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.TR$36, list);
            final CTRow[] array = new CTRow[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRow getTrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRow ctRow = (CTRow)this.get_store().find_element_user(CTTblImpl.TR$36, n);
            if (ctRow == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRow;
        }
    }
    
    public int sizeOfTrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.TR$36);
        }
    }
    
    public void setTrArray(final CTRow[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.TR$36);
        }
    }
    
    public void setTrArray(final int n, final CTRow ctRow) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRow ctRow2 = (CTRow)this.get_store().find_element_user(CTTblImpl.TR$36, n);
            if (ctRow2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRow2.set(ctRow);
        }
    }
    
    public CTRow insertNewTr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRow)this.get_store().insert_element_user(CTTblImpl.TR$36, n);
        }
    }
    
    public CTRow addNewTr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRow)this.get_store().add_element_user(CTTblImpl.TR$36);
        }
    }
    
    public void removeTr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.TR$36, n);
        }
    }
    
    public List<CTCustomXmlRow> getCustomXmlList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCustomXmlRow>)new CTTblImpl.CustomXmlList(this);
        }
    }
    
    public CTCustomXmlRow[] getCustomXmlArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.CUSTOMXML$38, list);
            final CTCustomXmlRow[] array = new CTCustomXmlRow[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCustomXmlRow getCustomXmlArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCustomXmlRow ctCustomXmlRow = (CTCustomXmlRow)this.get_store().find_element_user(CTTblImpl.CUSTOMXML$38, n);
            if (ctCustomXmlRow == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCustomXmlRow;
        }
    }
    
    public int sizeOfCustomXmlArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.CUSTOMXML$38);
        }
    }
    
    public void setCustomXmlArray(final CTCustomXmlRow[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.CUSTOMXML$38);
        }
    }
    
    public void setCustomXmlArray(final int n, final CTCustomXmlRow ctCustomXmlRow) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCustomXmlRow ctCustomXmlRow2 = (CTCustomXmlRow)this.get_store().find_element_user(CTTblImpl.CUSTOMXML$38, n);
            if (ctCustomXmlRow2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCustomXmlRow2.set((XmlObject)ctCustomXmlRow);
        }
    }
    
    public CTCustomXmlRow insertNewCustomXml(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCustomXmlRow)this.get_store().insert_element_user(CTTblImpl.CUSTOMXML$38, n);
        }
    }
    
    public CTCustomXmlRow addNewCustomXml() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCustomXmlRow)this.get_store().add_element_user(CTTblImpl.CUSTOMXML$38);
        }
    }
    
    public void removeCustomXml(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.CUSTOMXML$38, n);
        }
    }
    
    public List<CTSdtRow> getSdtList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtRow>)new CTTblImpl.SdtList(this);
        }
    }
    
    public CTSdtRow[] getSdtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.SDT$40, list);
            final CTSdtRow[] array = new CTSdtRow[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtRow getSdtArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtRow ctSdtRow = (CTSdtRow)this.get_store().find_element_user(CTTblImpl.SDT$40, n);
            if (ctSdtRow == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSdtRow;
        }
    }
    
    public int sizeOfSdtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.SDT$40);
        }
    }
    
    public void setSdtArray(final CTSdtRow[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.SDT$40);
        }
    }
    
    public void setSdtArray(final int n, final CTSdtRow ctSdtRow) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtRow ctSdtRow2 = (CTSdtRow)this.get_store().find_element_user(CTTblImpl.SDT$40, n);
            if (ctSdtRow2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSdtRow2.set((XmlObject)ctSdtRow);
        }
    }
    
    public CTSdtRow insertNewSdt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtRow)this.get_store().insert_element_user(CTTblImpl.SDT$40, n);
        }
    }
    
    public CTSdtRow addNewSdt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtRow)this.get_store().add_element_user(CTTblImpl.SDT$40);
        }
    }
    
    public void removeSdt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.SDT$40, n);
        }
    }
    
    public List<CTProofErr> getProofErrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTProofErr>)new CTTblImpl.ProofErrList(this);
        }
    }
    
    public CTProofErr[] getProofErrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.PROOFERR$42, list);
            final CTProofErr[] array = new CTProofErr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTProofErr getProofErrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTProofErr ctProofErr = (CTProofErr)this.get_store().find_element_user(CTTblImpl.PROOFERR$42, n);
            if (ctProofErr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctProofErr;
        }
    }
    
    public int sizeOfProofErrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.PROOFERR$42);
        }
    }
    
    public void setProofErrArray(final CTProofErr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.PROOFERR$42);
        }
    }
    
    public void setProofErrArray(final int n, final CTProofErr ctProofErr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTProofErr ctProofErr2 = (CTProofErr)this.get_store().find_element_user(CTTblImpl.PROOFERR$42, n);
            if (ctProofErr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctProofErr2.set(ctProofErr);
        }
    }
    
    public CTProofErr insertNewProofErr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTProofErr)this.get_store().insert_element_user(CTTblImpl.PROOFERR$42, n);
        }
    }
    
    public CTProofErr addNewProofErr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTProofErr)this.get_store().add_element_user(CTTblImpl.PROOFERR$42);
        }
    }
    
    public void removeProofErr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.PROOFERR$42, n);
        }
    }
    
    public List<CTPermStart> getPermStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPermStart>)new CTTblImpl.PermStartList(this);
        }
    }
    
    public CTPermStart[] getPermStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.PERMSTART$44, list);
            final CTPermStart[] array = new CTPermStart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPermStart getPermStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPermStart ctPermStart = (CTPermStart)this.get_store().find_element_user(CTTblImpl.PERMSTART$44, n);
            if (ctPermStart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPermStart;
        }
    }
    
    public int sizeOfPermStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.PERMSTART$44);
        }
    }
    
    public void setPermStartArray(final CTPermStart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.PERMSTART$44);
        }
    }
    
    public void setPermStartArray(final int n, final CTPermStart ctPermStart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPermStart ctPermStart2 = (CTPermStart)this.get_store().find_element_user(CTTblImpl.PERMSTART$44, n);
            if (ctPermStart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPermStart2.set((XmlObject)ctPermStart);
        }
    }
    
    public CTPermStart insertNewPermStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPermStart)this.get_store().insert_element_user(CTTblImpl.PERMSTART$44, n);
        }
    }
    
    public CTPermStart addNewPermStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPermStart)this.get_store().add_element_user(CTTblImpl.PERMSTART$44);
        }
    }
    
    public void removePermStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.PERMSTART$44, n);
        }
    }
    
    public List<CTPerm> getPermEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPerm>)new CTTblImpl.PermEndList(this);
        }
    }
    
    public CTPerm[] getPermEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.PERMEND$46, list);
            final CTPerm[] array = new CTPerm[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPerm getPermEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPerm ctPerm = (CTPerm)this.get_store().find_element_user(CTTblImpl.PERMEND$46, n);
            if (ctPerm == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPerm;
        }
    }
    
    public int sizeOfPermEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.PERMEND$46);
        }
    }
    
    public void setPermEndArray(final CTPerm[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.PERMEND$46);
        }
    }
    
    public void setPermEndArray(final int n, final CTPerm ctPerm) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPerm ctPerm2 = (CTPerm)this.get_store().find_element_user(CTTblImpl.PERMEND$46, n);
            if (ctPerm2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPerm2.set((XmlObject)ctPerm);
        }
    }
    
    public CTPerm insertNewPermEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPerm)this.get_store().insert_element_user(CTTblImpl.PERMEND$46, n);
        }
    }
    
    public CTPerm addNewPermEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPerm)this.get_store().add_element_user(CTTblImpl.PERMEND$46);
        }
    }
    
    public void removePermEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.PERMEND$46, n);
        }
    }
    
    public List<CTRunTrackChange> getInsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTTblImpl.InsList(this);
        }
    }
    
    public CTRunTrackChange[] getInsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.INS$48, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getInsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.INS$48, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfInsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.INS$48);
        }
    }
    
    public void setInsArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.INS$48);
        }
    }
    
    public void setInsArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.INS$48, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewIns(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTTblImpl.INS$48, n);
        }
    }
    
    public CTRunTrackChange addNewIns() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTTblImpl.INS$48);
        }
    }
    
    public void removeIns(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.INS$48, n);
        }
    }
    
    public List<CTRunTrackChange> getDelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTTblImpl.DelList(this);
        }
    }
    
    public CTRunTrackChange[] getDelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.DEL$50, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getDelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.DEL$50, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfDelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.DEL$50);
        }
    }
    
    public void setDelArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.DEL$50);
        }
    }
    
    public void setDelArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.DEL$50, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewDel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTTblImpl.DEL$50, n);
        }
    }
    
    public CTRunTrackChange addNewDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTTblImpl.DEL$50);
        }
    }
    
    public void removeDel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.DEL$50, n);
        }
    }
    
    public List<CTRunTrackChange> getMoveFromList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTTblImpl.MoveFromList(this);
        }
    }
    
    public CTRunTrackChange[] getMoveFromArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.MOVEFROM$52, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getMoveFromArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.MOVEFROM$52, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfMoveFromArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.MOVEFROM$52);
        }
    }
    
    public void setMoveFromArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.MOVEFROM$52);
        }
    }
    
    public void setMoveFromArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.MOVEFROM$52, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewMoveFrom(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTTblImpl.MOVEFROM$52, n);
        }
    }
    
    public CTRunTrackChange addNewMoveFrom() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTTblImpl.MOVEFROM$52);
        }
    }
    
    public void removeMoveFrom(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.MOVEFROM$52, n);
        }
    }
    
    public List<CTRunTrackChange> getMoveToList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTTblImpl.MoveToList(this);
        }
    }
    
    public CTRunTrackChange[] getMoveToArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.MOVETO$54, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getMoveToArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.MOVETO$54, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfMoveToArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.MOVETO$54);
        }
    }
    
    public void setMoveToArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTTblImpl.MOVETO$54);
        }
    }
    
    public void setMoveToArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTTblImpl.MOVETO$54, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewMoveTo(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTTblImpl.MOVETO$54, n);
        }
    }
    
    public CTRunTrackChange addNewMoveTo() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTTblImpl.MOVETO$54);
        }
    }
    
    public void removeMoveTo(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.MOVETO$54, n);
        }
    }
    
    public List<CTOMathPara> getOMathParaList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOMathPara>)new CTTblImpl.OMathParaList(this);
        }
    }
    
    public CTOMathPara[] getOMathParaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.OMATHPARA$56, list);
            final CTOMathPara[] array = new CTOMathPara[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOMathPara getOMathParaArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMathPara ctoMathPara = (CTOMathPara)this.get_store().find_element_user(CTTblImpl.OMATHPARA$56, n);
            if (ctoMathPara == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctoMathPara;
        }
    }
    
    public int sizeOfOMathParaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.OMATHPARA$56);
        }
    }
    
    public void setOMathParaArray(final CTOMathPara[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.OMATHPARA$56);
        }
    }
    
    public void setOMathParaArray(final int n, final CTOMathPara ctoMathPara) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMathPara ctoMathPara2 = (CTOMathPara)this.get_store().find_element_user(CTTblImpl.OMATHPARA$56, n);
            if (ctoMathPara2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctoMathPara2.set((XmlObject)ctoMathPara);
        }
    }
    
    public CTOMathPara insertNewOMathPara(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMathPara)this.get_store().insert_element_user(CTTblImpl.OMATHPARA$56, n);
        }
    }
    
    public CTOMathPara addNewOMathPara() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMathPara)this.get_store().add_element_user(CTTblImpl.OMATHPARA$56);
        }
    }
    
    public void removeOMathPara(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.OMATHPARA$56, n);
        }
    }
    
    public List<CTOMath> getOMathList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOMath>)new CTTblImpl.OMathList(this);
        }
    }
    
    public CTOMath[] getOMathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTTblImpl.OMATH$58, list);
            final CTOMath[] array = new CTOMath[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOMath getOMathArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMath ctoMath = (CTOMath)this.get_store().find_element_user(CTTblImpl.OMATH$58, n);
            if (ctoMath == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctoMath;
        }
    }
    
    public int sizeOfOMathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTblImpl.OMATH$58);
        }
    }
    
    public void setOMathArray(final CTOMath[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTTblImpl.OMATH$58);
        }
    }
    
    public void setOMathArray(final int n, final CTOMath ctoMath) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMath ctoMath2 = (CTOMath)this.get_store().find_element_user(CTTblImpl.OMATH$58, n);
            if (ctoMath2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctoMath2.set((XmlObject)ctoMath);
        }
    }
    
    public CTOMath insertNewOMath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMath)this.get_store().insert_element_user(CTTblImpl.OMATH$58, n);
        }
    }
    
    public CTOMath addNewOMath() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMath)this.get_store().add_element_user(CTTblImpl.OMATH$58);
        }
    }
    
    public void removeOMath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTblImpl.OMATH$58, n);
        }
    }
    
    static {
        BOOKMARKSTART$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookmarkStart");
        BOOKMARKEND$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookmarkEnd");
        MOVEFROMRANGESTART$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveFromRangeStart");
        MOVEFROMRANGEEND$6 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveFromRangeEnd");
        MOVETORANGESTART$8 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveToRangeStart");
        MOVETORANGEEND$10 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveToRangeEnd");
        COMMENTRANGESTART$12 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "commentRangeStart");
        COMMENTRANGEEND$14 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "commentRangeEnd");
        CUSTOMXMLINSRANGESTART$16 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlInsRangeStart");
        CUSTOMXMLINSRANGEEND$18 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlInsRangeEnd");
        CUSTOMXMLDELRANGESTART$20 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlDelRangeStart");
        CUSTOMXMLDELRANGEEND$22 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlDelRangeEnd");
        CUSTOMXMLMOVEFROMRANGESTART$24 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveFromRangeStart");
        CUSTOMXMLMOVEFROMRANGEEND$26 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveFromRangeEnd");
        CUSTOMXMLMOVETORANGESTART$28 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveToRangeStart");
        CUSTOMXMLMOVETORANGEEND$30 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveToRangeEnd");
        TBLPR$32 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tblPr");
        TBLGRID$34 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tblGrid");
        TR$36 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tr");
        CUSTOMXML$38 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXml");
        SDT$40 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "sdt");
        PROOFERR$42 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "proofErr");
        PERMSTART$44 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "permStart");
        PERMEND$46 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "permEnd");
        INS$48 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "ins");
        DEL$50 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "del");
        MOVEFROM$52 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveFrom");
        MOVETO$54 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveTo");
        OMATHPARA$56 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "oMathPara");
        OMATH$58 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "oMath");
    }
}
