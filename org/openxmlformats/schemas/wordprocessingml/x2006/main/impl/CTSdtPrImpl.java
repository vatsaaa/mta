// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtText;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtDropDownList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtDocPart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtDate;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtComboBox;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTEmpty;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDecimalNumber;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDataBinding;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPlaceholder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLock;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTString;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtPr;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTSdtPrImpl extends XmlComplexContentImpl implements CTSdtPr
{
    private static final QName RPR$0;
    private static final QName ALIAS$2;
    private static final QName LOCK$4;
    private static final QName PLACEHOLDER$6;
    private static final QName SHOWINGPLCHDR$8;
    private static final QName DATABINDING$10;
    private static final QName TEMPORARY$12;
    private static final QName ID$14;
    private static final QName TAG$16;
    private static final QName EQUATION$18;
    private static final QName COMBOBOX$20;
    private static final QName DATE$22;
    private static final QName DOCPARTOBJ$24;
    private static final QName DOCPARTLIST$26;
    private static final QName DROPDOWNLIST$28;
    private static final QName PICTURE$30;
    private static final QName RICHTEXT$32;
    private static final QName TEXT$34;
    private static final QName CITATION$36;
    private static final QName GROUP$38;
    private static final QName BIBLIOGRAPHY$40;
    
    public CTSdtPrImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTRPr> getRPrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRPr>)new CTSdtPrImpl.RPrList(this);
        }
    }
    
    public CTRPr[] getRPrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.RPR$0, list);
            final CTRPr[] array = new CTRPr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRPr getRPrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRPr ctrPr = (CTRPr)this.get_store().find_element_user(CTSdtPrImpl.RPR$0, n);
            if (ctrPr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctrPr;
        }
    }
    
    public int sizeOfRPrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.RPR$0);
        }
    }
    
    public void setRPrArray(final CTRPr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.RPR$0);
        }
    }
    
    public void setRPrArray(final int n, final CTRPr ctrPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRPr ctrPr2 = (CTRPr)this.get_store().find_element_user(CTSdtPrImpl.RPR$0, n);
            if (ctrPr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctrPr2.set(ctrPr);
        }
    }
    
    public CTRPr insertNewRPr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRPr)this.get_store().insert_element_user(CTSdtPrImpl.RPR$0, n);
        }
    }
    
    public CTRPr addNewRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRPr)this.get_store().add_element_user(CTSdtPrImpl.RPR$0);
        }
    }
    
    public void removeRPr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.RPR$0, n);
        }
    }
    
    public List<CTString> getAliasList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTString>)new CTSdtPrImpl.AliasList(this);
        }
    }
    
    public CTString[] getAliasArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.ALIAS$2, list);
            final CTString[] array = new CTString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTString getAliasArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTString ctString = (CTString)this.get_store().find_element_user(CTSdtPrImpl.ALIAS$2, n);
            if (ctString == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctString;
        }
    }
    
    public int sizeOfAliasArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.ALIAS$2);
        }
    }
    
    public void setAliasArray(final CTString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.ALIAS$2);
        }
    }
    
    public void setAliasArray(final int n, final CTString ctString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTString ctString2 = (CTString)this.get_store().find_element_user(CTSdtPrImpl.ALIAS$2, n);
            if (ctString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctString2.set(ctString);
        }
    }
    
    public CTString insertNewAlias(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTString)this.get_store().insert_element_user(CTSdtPrImpl.ALIAS$2, n);
        }
    }
    
    public CTString addNewAlias() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTString)this.get_store().add_element_user(CTSdtPrImpl.ALIAS$2);
        }
    }
    
    public void removeAlias(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.ALIAS$2, n);
        }
    }
    
    public List<CTLock> getLockList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLock>)new CTSdtPrImpl.LockList(this);
        }
    }
    
    public CTLock[] getLockArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.LOCK$4, list);
            final CTLock[] array = new CTLock[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLock getLockArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLock ctLock = (CTLock)this.get_store().find_element_user(CTSdtPrImpl.LOCK$4, n);
            if (ctLock == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLock;
        }
    }
    
    public int sizeOfLockArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.LOCK$4);
        }
    }
    
    public void setLockArray(final CTLock[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSdtPrImpl.LOCK$4);
        }
    }
    
    public void setLockArray(final int n, final CTLock ctLock) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLock ctLock2 = (CTLock)this.get_store().find_element_user(CTSdtPrImpl.LOCK$4, n);
            if (ctLock2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLock2.set((XmlObject)ctLock);
        }
    }
    
    public CTLock insertNewLock(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLock)this.get_store().insert_element_user(CTSdtPrImpl.LOCK$4, n);
        }
    }
    
    public CTLock addNewLock() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLock)this.get_store().add_element_user(CTSdtPrImpl.LOCK$4);
        }
    }
    
    public void removeLock(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.LOCK$4, n);
        }
    }
    
    public List<CTPlaceholder> getPlaceholderList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPlaceholder>)new CTSdtPrImpl.PlaceholderList(this);
        }
    }
    
    public CTPlaceholder[] getPlaceholderArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.PLACEHOLDER$6, list);
            final CTPlaceholder[] array = new CTPlaceholder[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPlaceholder getPlaceholderArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPlaceholder ctPlaceholder = (CTPlaceholder)this.get_store().find_element_user(CTSdtPrImpl.PLACEHOLDER$6, n);
            if (ctPlaceholder == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPlaceholder;
        }
    }
    
    public int sizeOfPlaceholderArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.PLACEHOLDER$6);
        }
    }
    
    public void setPlaceholderArray(final CTPlaceholder[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSdtPrImpl.PLACEHOLDER$6);
        }
    }
    
    public void setPlaceholderArray(final int n, final CTPlaceholder ctPlaceholder) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPlaceholder ctPlaceholder2 = (CTPlaceholder)this.get_store().find_element_user(CTSdtPrImpl.PLACEHOLDER$6, n);
            if (ctPlaceholder2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPlaceholder2.set((XmlObject)ctPlaceholder);
        }
    }
    
    public CTPlaceholder insertNewPlaceholder(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPlaceholder)this.get_store().insert_element_user(CTSdtPrImpl.PLACEHOLDER$6, n);
        }
    }
    
    public CTPlaceholder addNewPlaceholder() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPlaceholder)this.get_store().add_element_user(CTSdtPrImpl.PLACEHOLDER$6);
        }
    }
    
    public void removePlaceholder(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.PLACEHOLDER$6, n);
        }
    }
    
    public List<CTOnOff> getShowingPlcHdrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOnOff>)new CTSdtPrImpl.ShowingPlcHdrList(this);
        }
    }
    
    public CTOnOff[] getShowingPlcHdrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.SHOWINGPLCHDR$8, list);
            final CTOnOff[] array = new CTOnOff[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOnOff getShowingPlcHdrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff = (CTOnOff)this.get_store().find_element_user(CTSdtPrImpl.SHOWINGPLCHDR$8, n);
            if (ctOnOff == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOnOff;
        }
    }
    
    public int sizeOfShowingPlcHdrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.SHOWINGPLCHDR$8);
        }
    }
    
    public void setShowingPlcHdrArray(final CTOnOff[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.SHOWINGPLCHDR$8);
        }
    }
    
    public void setShowingPlcHdrArray(final int n, final CTOnOff ctOnOff) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff2 = (CTOnOff)this.get_store().find_element_user(CTSdtPrImpl.SHOWINGPLCHDR$8, n);
            if (ctOnOff2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOnOff2.set(ctOnOff);
        }
    }
    
    public CTOnOff insertNewShowingPlcHdr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().insert_element_user(CTSdtPrImpl.SHOWINGPLCHDR$8, n);
        }
    }
    
    public CTOnOff addNewShowingPlcHdr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().add_element_user(CTSdtPrImpl.SHOWINGPLCHDR$8);
        }
    }
    
    public void removeShowingPlcHdr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.SHOWINGPLCHDR$8, n);
        }
    }
    
    public List<CTDataBinding> getDataBindingList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDataBinding>)new CTSdtPrImpl.DataBindingList(this);
        }
    }
    
    public CTDataBinding[] getDataBindingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.DATABINDING$10, list);
            final CTDataBinding[] array = new CTDataBinding[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDataBinding getDataBindingArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDataBinding ctDataBinding = (CTDataBinding)this.get_store().find_element_user(CTSdtPrImpl.DATABINDING$10, n);
            if (ctDataBinding == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDataBinding;
        }
    }
    
    public int sizeOfDataBindingArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.DATABINDING$10);
        }
    }
    
    public void setDataBindingArray(final CTDataBinding[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSdtPrImpl.DATABINDING$10);
        }
    }
    
    public void setDataBindingArray(final int n, final CTDataBinding ctDataBinding) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDataBinding ctDataBinding2 = (CTDataBinding)this.get_store().find_element_user(CTSdtPrImpl.DATABINDING$10, n);
            if (ctDataBinding2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDataBinding2.set((XmlObject)ctDataBinding);
        }
    }
    
    public CTDataBinding insertNewDataBinding(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDataBinding)this.get_store().insert_element_user(CTSdtPrImpl.DATABINDING$10, n);
        }
    }
    
    public CTDataBinding addNewDataBinding() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDataBinding)this.get_store().add_element_user(CTSdtPrImpl.DATABINDING$10);
        }
    }
    
    public void removeDataBinding(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.DATABINDING$10, n);
        }
    }
    
    public List<CTOnOff> getTemporaryList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOnOff>)new CTSdtPrImpl.TemporaryList(this);
        }
    }
    
    public CTOnOff[] getTemporaryArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.TEMPORARY$12, list);
            final CTOnOff[] array = new CTOnOff[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOnOff getTemporaryArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff = (CTOnOff)this.get_store().find_element_user(CTSdtPrImpl.TEMPORARY$12, n);
            if (ctOnOff == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctOnOff;
        }
    }
    
    public int sizeOfTemporaryArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.TEMPORARY$12);
        }
    }
    
    public void setTemporaryArray(final CTOnOff[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.TEMPORARY$12);
        }
    }
    
    public void setTemporaryArray(final int n, final CTOnOff ctOnOff) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOnOff ctOnOff2 = (CTOnOff)this.get_store().find_element_user(CTSdtPrImpl.TEMPORARY$12, n);
            if (ctOnOff2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctOnOff2.set(ctOnOff);
        }
    }
    
    public CTOnOff insertNewTemporary(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().insert_element_user(CTSdtPrImpl.TEMPORARY$12, n);
        }
    }
    
    public CTOnOff addNewTemporary() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOnOff)this.get_store().add_element_user(CTSdtPrImpl.TEMPORARY$12);
        }
    }
    
    public void removeTemporary(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.TEMPORARY$12, n);
        }
    }
    
    public List<CTDecimalNumber> getIdList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTDecimalNumber>)new CTSdtPrImpl.IdList(this);
        }
    }
    
    public CTDecimalNumber[] getIdArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.ID$14, list);
            final CTDecimalNumber[] array = new CTDecimalNumber[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTDecimalNumber getIdArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber = (CTDecimalNumber)this.get_store().find_element_user(CTSdtPrImpl.ID$14, n);
            if (ctDecimalNumber == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctDecimalNumber;
        }
    }
    
    public int sizeOfIdArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.ID$14);
        }
    }
    
    public void setIdArray(final CTDecimalNumber[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.ID$14);
        }
    }
    
    public void setIdArray(final int n, final CTDecimalNumber ctDecimalNumber) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTDecimalNumber ctDecimalNumber2 = (CTDecimalNumber)this.get_store().find_element_user(CTSdtPrImpl.ID$14, n);
            if (ctDecimalNumber2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctDecimalNumber2.set(ctDecimalNumber);
        }
    }
    
    public CTDecimalNumber insertNewId(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().insert_element_user(CTSdtPrImpl.ID$14, n);
        }
    }
    
    public CTDecimalNumber addNewId() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTDecimalNumber)this.get_store().add_element_user(CTSdtPrImpl.ID$14);
        }
    }
    
    public void removeId(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.ID$14, n);
        }
    }
    
    public List<CTString> getTagList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTString>)new CTSdtPrImpl.TagList(this);
        }
    }
    
    public CTString[] getTagArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.TAG$16, list);
            final CTString[] array = new CTString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTString getTagArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTString ctString = (CTString)this.get_store().find_element_user(CTSdtPrImpl.TAG$16, n);
            if (ctString == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctString;
        }
    }
    
    public int sizeOfTagArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.TAG$16);
        }
    }
    
    public void setTagArray(final CTString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.TAG$16);
        }
    }
    
    public void setTagArray(final int n, final CTString ctString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTString ctString2 = (CTString)this.get_store().find_element_user(CTSdtPrImpl.TAG$16, n);
            if (ctString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctString2.set(ctString);
        }
    }
    
    public CTString insertNewTag(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTString)this.get_store().insert_element_user(CTSdtPrImpl.TAG$16, n);
        }
    }
    
    public CTString addNewTag() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTString)this.get_store().add_element_user(CTSdtPrImpl.TAG$16);
        }
    }
    
    public void removeTag(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.TAG$16, n);
        }
    }
    
    public List<CTEmpty> getEquationList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTSdtPrImpl.EquationList(this);
        }
    }
    
    public CTEmpty[] getEquationArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.EQUATION$18, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getEquationArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.EQUATION$18, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfEquationArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.EQUATION$18);
        }
    }
    
    public void setEquationArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.EQUATION$18);
        }
    }
    
    public void setEquationArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.EQUATION$18, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewEquation(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTSdtPrImpl.EQUATION$18, n);
        }
    }
    
    public CTEmpty addNewEquation() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTSdtPrImpl.EQUATION$18);
        }
    }
    
    public void removeEquation(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.EQUATION$18, n);
        }
    }
    
    public List<CTSdtComboBox> getComboBoxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtComboBox>)new CTSdtPrImpl.ComboBoxList(this);
        }
    }
    
    public CTSdtComboBox[] getComboBoxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.COMBOBOX$20, list);
            final CTSdtComboBox[] array = new CTSdtComboBox[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtComboBox getComboBoxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtComboBox ctSdtComboBox = (CTSdtComboBox)this.get_store().find_element_user(CTSdtPrImpl.COMBOBOX$20, n);
            if (ctSdtComboBox == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSdtComboBox;
        }
    }
    
    public int sizeOfComboBoxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.COMBOBOX$20);
        }
    }
    
    public void setComboBoxArray(final CTSdtComboBox[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSdtPrImpl.COMBOBOX$20);
        }
    }
    
    public void setComboBoxArray(final int n, final CTSdtComboBox ctSdtComboBox) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtComboBox ctSdtComboBox2 = (CTSdtComboBox)this.get_store().find_element_user(CTSdtPrImpl.COMBOBOX$20, n);
            if (ctSdtComboBox2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSdtComboBox2.set((XmlObject)ctSdtComboBox);
        }
    }
    
    public CTSdtComboBox insertNewComboBox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtComboBox)this.get_store().insert_element_user(CTSdtPrImpl.COMBOBOX$20, n);
        }
    }
    
    public CTSdtComboBox addNewComboBox() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtComboBox)this.get_store().add_element_user(CTSdtPrImpl.COMBOBOX$20);
        }
    }
    
    public void removeComboBox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.COMBOBOX$20, n);
        }
    }
    
    public List<CTSdtDate> getDateList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtDate>)new CTSdtPrImpl.DateList(this);
        }
    }
    
    public CTSdtDate[] getDateArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.DATE$22, list);
            final CTSdtDate[] array = new CTSdtDate[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtDate getDateArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDate ctSdtDate = (CTSdtDate)this.get_store().find_element_user(CTSdtPrImpl.DATE$22, n);
            if (ctSdtDate == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSdtDate;
        }
    }
    
    public int sizeOfDateArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.DATE$22);
        }
    }
    
    public void setDateArray(final CTSdtDate[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSdtPrImpl.DATE$22);
        }
    }
    
    public void setDateArray(final int n, final CTSdtDate ctSdtDate) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDate ctSdtDate2 = (CTSdtDate)this.get_store().find_element_user(CTSdtPrImpl.DATE$22, n);
            if (ctSdtDate2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSdtDate2.set((XmlObject)ctSdtDate);
        }
    }
    
    public CTSdtDate insertNewDate(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDate)this.get_store().insert_element_user(CTSdtPrImpl.DATE$22, n);
        }
    }
    
    public CTSdtDate addNewDate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDate)this.get_store().add_element_user(CTSdtPrImpl.DATE$22);
        }
    }
    
    public void removeDate(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.DATE$22, n);
        }
    }
    
    public List<CTSdtDocPart> getDocPartObjList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtDocPart>)new CTSdtPrImpl.DocPartObjList(this);
        }
    }
    
    public CTSdtDocPart[] getDocPartObjArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.DOCPARTOBJ$24, list);
            final CTSdtDocPart[] array = new CTSdtDocPart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtDocPart getDocPartObjArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDocPart ctSdtDocPart = (CTSdtDocPart)this.get_store().find_element_user(CTSdtPrImpl.DOCPARTOBJ$24, n);
            if (ctSdtDocPart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSdtDocPart;
        }
    }
    
    public int sizeOfDocPartObjArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.DOCPARTOBJ$24);
        }
    }
    
    public void setDocPartObjArray(final CTSdtDocPart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.DOCPARTOBJ$24);
        }
    }
    
    public void setDocPartObjArray(final int n, final CTSdtDocPart ctSdtDocPart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDocPart ctSdtDocPart2 = (CTSdtDocPart)this.get_store().find_element_user(CTSdtPrImpl.DOCPARTOBJ$24, n);
            if (ctSdtDocPart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSdtDocPart2.set(ctSdtDocPart);
        }
    }
    
    public CTSdtDocPart insertNewDocPartObj(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDocPart)this.get_store().insert_element_user(CTSdtPrImpl.DOCPARTOBJ$24, n);
        }
    }
    
    public CTSdtDocPart addNewDocPartObj() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDocPart)this.get_store().add_element_user(CTSdtPrImpl.DOCPARTOBJ$24);
        }
    }
    
    public void removeDocPartObj(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.DOCPARTOBJ$24, n);
        }
    }
    
    public List<CTSdtDocPart> getDocPartListList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtDocPart>)new CTSdtPrImpl.DocPartListList(this);
        }
    }
    
    public CTSdtDocPart[] getDocPartListArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.DOCPARTLIST$26, list);
            final CTSdtDocPart[] array = new CTSdtDocPart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtDocPart getDocPartListArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDocPart ctSdtDocPart = (CTSdtDocPart)this.get_store().find_element_user(CTSdtPrImpl.DOCPARTLIST$26, n);
            if (ctSdtDocPart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSdtDocPart;
        }
    }
    
    public int sizeOfDocPartListArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.DOCPARTLIST$26);
        }
    }
    
    public void setDocPartListArray(final CTSdtDocPart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.DOCPARTLIST$26);
        }
    }
    
    public void setDocPartListArray(final int n, final CTSdtDocPart ctSdtDocPart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDocPart ctSdtDocPart2 = (CTSdtDocPart)this.get_store().find_element_user(CTSdtPrImpl.DOCPARTLIST$26, n);
            if (ctSdtDocPart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSdtDocPart2.set(ctSdtDocPart);
        }
    }
    
    public CTSdtDocPart insertNewDocPartList(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDocPart)this.get_store().insert_element_user(CTSdtPrImpl.DOCPARTLIST$26, n);
        }
    }
    
    public CTSdtDocPart addNewDocPartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDocPart)this.get_store().add_element_user(CTSdtPrImpl.DOCPARTLIST$26);
        }
    }
    
    public void removeDocPartList(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.DOCPARTLIST$26, n);
        }
    }
    
    public List<CTSdtDropDownList> getDropDownListList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtDropDownList>)new CTSdtPrImpl.DropDownListList(this);
        }
    }
    
    public CTSdtDropDownList[] getDropDownListArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.DROPDOWNLIST$28, list);
            final CTSdtDropDownList[] array = new CTSdtDropDownList[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtDropDownList getDropDownListArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDropDownList list = (CTSdtDropDownList)this.get_store().find_element_user(CTSdtPrImpl.DROPDOWNLIST$28, n);
            if (list == null) {
                throw new IndexOutOfBoundsException();
            }
            return list;
        }
    }
    
    public int sizeOfDropDownListArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.DROPDOWNLIST$28);
        }
    }
    
    public void setDropDownListArray(final CTSdtDropDownList[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSdtPrImpl.DROPDOWNLIST$28);
        }
    }
    
    public void setDropDownListArray(final int n, final CTSdtDropDownList list) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtDropDownList list2 = (CTSdtDropDownList)this.get_store().find_element_user(CTSdtPrImpl.DROPDOWNLIST$28, n);
            if (list2 == null) {
                throw new IndexOutOfBoundsException();
            }
            list2.set((XmlObject)list);
        }
    }
    
    public CTSdtDropDownList insertNewDropDownList(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDropDownList)this.get_store().insert_element_user(CTSdtPrImpl.DROPDOWNLIST$28, n);
        }
    }
    
    public CTSdtDropDownList addNewDropDownList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtDropDownList)this.get_store().add_element_user(CTSdtPrImpl.DROPDOWNLIST$28);
        }
    }
    
    public void removeDropDownList(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.DROPDOWNLIST$28, n);
        }
    }
    
    public List<CTEmpty> getPictureList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTSdtPrImpl.PictureList(this);
        }
    }
    
    public CTEmpty[] getPictureArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.PICTURE$30, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getPictureArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.PICTURE$30, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfPictureArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.PICTURE$30);
        }
    }
    
    public void setPictureArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.PICTURE$30);
        }
    }
    
    public void setPictureArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.PICTURE$30, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewPicture(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTSdtPrImpl.PICTURE$30, n);
        }
    }
    
    public CTEmpty addNewPicture() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTSdtPrImpl.PICTURE$30);
        }
    }
    
    public void removePicture(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.PICTURE$30, n);
        }
    }
    
    public List<CTEmpty> getRichTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTSdtPrImpl.RichTextList(this);
        }
    }
    
    public CTEmpty[] getRichTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.RICHTEXT$32, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getRichTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.RICHTEXT$32, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfRichTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.RICHTEXT$32);
        }
    }
    
    public void setRichTextArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.RICHTEXT$32);
        }
    }
    
    public void setRichTextArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.RICHTEXT$32, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewRichText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTSdtPrImpl.RICHTEXT$32, n);
        }
    }
    
    public CTEmpty addNewRichText() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTSdtPrImpl.RICHTEXT$32);
        }
    }
    
    public void removeRichText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.RICHTEXT$32, n);
        }
    }
    
    public List<CTSdtText> getTextList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtText>)new CTSdtPrImpl.TextList(this);
        }
    }
    
    public CTSdtText[] getTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.TEXT$34, list);
            final CTSdtText[] array = new CTSdtText[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtText getTextArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtText ctSdtText = (CTSdtText)this.get_store().find_element_user(CTSdtPrImpl.TEXT$34, n);
            if (ctSdtText == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSdtText;
        }
    }
    
    public int sizeOfTextArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.TEXT$34);
        }
    }
    
    public void setTextArray(final CTSdtText[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSdtPrImpl.TEXT$34);
        }
    }
    
    public void setTextArray(final int n, final CTSdtText ctSdtText) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtText ctSdtText2 = (CTSdtText)this.get_store().find_element_user(CTSdtPrImpl.TEXT$34, n);
            if (ctSdtText2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSdtText2.set((XmlObject)ctSdtText);
        }
    }
    
    public CTSdtText insertNewText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtText)this.get_store().insert_element_user(CTSdtPrImpl.TEXT$34, n);
        }
    }
    
    public CTSdtText addNewText() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtText)this.get_store().add_element_user(CTSdtPrImpl.TEXT$34);
        }
    }
    
    public void removeText(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.TEXT$34, n);
        }
    }
    
    public List<CTEmpty> getCitationList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTSdtPrImpl.CitationList(this);
        }
    }
    
    public CTEmpty[] getCitationArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.CITATION$36, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getCitationArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.CITATION$36, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfCitationArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.CITATION$36);
        }
    }
    
    public void setCitationArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.CITATION$36);
        }
    }
    
    public void setCitationArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.CITATION$36, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewCitation(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTSdtPrImpl.CITATION$36, n);
        }
    }
    
    public CTEmpty addNewCitation() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTSdtPrImpl.CITATION$36);
        }
    }
    
    public void removeCitation(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.CITATION$36, n);
        }
    }
    
    public List<CTEmpty> getGroupList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTSdtPrImpl.GroupList(this);
        }
    }
    
    public CTEmpty[] getGroupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.GROUP$38, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getGroupArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.GROUP$38, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfGroupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.GROUP$38);
        }
    }
    
    public void setGroupArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.GROUP$38);
        }
    }
    
    public void setGroupArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.GROUP$38, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewGroup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTSdtPrImpl.GROUP$38, n);
        }
    }
    
    public CTEmpty addNewGroup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTSdtPrImpl.GROUP$38);
        }
    }
    
    public void removeGroup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.GROUP$38, n);
        }
    }
    
    public List<CTEmpty> getBibliographyList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEmpty>)new CTSdtPrImpl.BibliographyList(this);
        }
    }
    
    public CTEmpty[] getBibliographyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtPrImpl.BIBLIOGRAPHY$40, list);
            final CTEmpty[] array = new CTEmpty[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEmpty getBibliographyArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.BIBLIOGRAPHY$40, n);
            if (ctEmpty == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEmpty;
        }
    }
    
    public int sizeOfBibliographyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtPrImpl.BIBLIOGRAPHY$40);
        }
    }
    
    public void setBibliographyArray(final CTEmpty[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtPrImpl.BIBLIOGRAPHY$40);
        }
    }
    
    public void setBibliographyArray(final int n, final CTEmpty ctEmpty) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEmpty ctEmpty2 = (CTEmpty)this.get_store().find_element_user(CTSdtPrImpl.BIBLIOGRAPHY$40, n);
            if (ctEmpty2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEmpty2.set(ctEmpty);
        }
    }
    
    public CTEmpty insertNewBibliography(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().insert_element_user(CTSdtPrImpl.BIBLIOGRAPHY$40, n);
        }
    }
    
    public CTEmpty addNewBibliography() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEmpty)this.get_store().add_element_user(CTSdtPrImpl.BIBLIOGRAPHY$40);
        }
    }
    
    public void removeBibliography(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtPrImpl.BIBLIOGRAPHY$40, n);
        }
    }
    
    static {
        RPR$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rPr");
        ALIAS$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "alias");
        LOCK$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "lock");
        PLACEHOLDER$6 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "placeholder");
        SHOWINGPLCHDR$8 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "showingPlcHdr");
        DATABINDING$10 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "dataBinding");
        TEMPORARY$12 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "temporary");
        ID$14 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "id");
        TAG$16 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tag");
        EQUATION$18 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "equation");
        COMBOBOX$20 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "comboBox");
        DATE$22 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "date");
        DOCPARTOBJ$24 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "docPartObj");
        DOCPARTLIST$26 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "docPartList");
        DROPDOWNLIST$28 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "dropDownList");
        PICTURE$30 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "picture");
        RICHTEXT$32 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "richText");
        TEXT$34 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "text");
        CITATION$36 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "citation");
        GROUP$38 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "group");
        BIBLIOGRAPHY$40 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bibliography");
    }
}
