// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtEndPr;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTSdtEndPrImpl extends XmlComplexContentImpl implements CTSdtEndPr
{
    private static final QName RPR$0;
    
    public CTSdtEndPrImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTRPr> getRPrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRPr>)new CTSdtEndPrImpl.RPrList(this);
        }
    }
    
    public CTRPr[] getRPrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSdtEndPrImpl.RPR$0, list);
            final CTRPr[] array = new CTRPr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRPr getRPrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRPr ctrPr = (CTRPr)this.get_store().find_element_user(CTSdtEndPrImpl.RPR$0, n);
            if (ctrPr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctrPr;
        }
    }
    
    public int sizeOfRPrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSdtEndPrImpl.RPR$0);
        }
    }
    
    public void setRPrArray(final CTRPr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTSdtEndPrImpl.RPR$0);
        }
    }
    
    public void setRPrArray(final int n, final CTRPr ctrPr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRPr ctrPr2 = (CTRPr)this.get_store().find_element_user(CTSdtEndPrImpl.RPR$0, n);
            if (ctrPr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctrPr2.set(ctrPr);
        }
    }
    
    public CTRPr insertNewRPr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRPr)this.get_store().insert_element_user(CTSdtEndPrImpl.RPR$0, n);
        }
    }
    
    public CTRPr addNewRPr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRPr)this.get_store().add_element_user(CTSdtEndPrImpl.RPR$0);
        }
    }
    
    public void removeRPr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSdtEndPrImpl.RPR$0, n);
        }
    }
    
    static {
        RPR$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "rPr");
    }
}
