// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STFldCharType;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STFldCharTypeImpl extends JavaStringEnumerationHolderEx implements STFldCharType
{
    public STFldCharTypeImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STFldCharTypeImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
