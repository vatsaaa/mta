// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STLongHexNumber;
import org.apache.xmlbeans.impl.values.JavaHexBinaryHolderEx;

public class STLongHexNumberImpl extends JavaHexBinaryHolderEx implements STLongHexNumber
{
    public STLongHexNumberImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STLongHexNumberImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
