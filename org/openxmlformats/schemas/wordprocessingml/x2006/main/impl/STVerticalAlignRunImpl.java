// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalAlignRun;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STVerticalAlignRunImpl extends JavaStringEnumerationHolderEx implements STVerticalAlignRun
{
    public STVerticalAlignRunImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STVerticalAlignRunImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
