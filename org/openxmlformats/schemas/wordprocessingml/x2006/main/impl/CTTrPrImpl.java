// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPrChange;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrackChange;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;

public class CTTrPrImpl extends CTTrPrBaseImpl implements CTTrPr
{
    private static final QName INS$0;
    private static final QName DEL$2;
    private static final QName TRPRCHANGE$4;
    
    public CTTrPrImpl(final SchemaType schemaType) {
        super(schemaType);
    }
    
    public CTTrackChange getIns() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTTrPrImpl.INS$0, 0);
            if (ctTrackChange == null) {
                return null;
            }
            return ctTrackChange;
        }
    }
    
    public boolean isSetIns() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrImpl.INS$0) != 0;
        }
    }
    
    public void setIns(final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTTrPrImpl.INS$0, 0);
            if (ctTrackChange2 == null) {
                ctTrackChange2 = (CTTrackChange)this.get_store().add_element_user(CTTrPrImpl.INS$0);
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange addNewIns() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTTrPrImpl.INS$0);
        }
    }
    
    public void unsetIns() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrImpl.INS$0, 0);
        }
    }
    
    public CTTrackChange getDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTTrPrImpl.DEL$2, 0);
            if (ctTrackChange == null) {
                return null;
            }
            return ctTrackChange;
        }
    }
    
    public boolean isSetDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrImpl.DEL$2) != 0;
        }
    }
    
    public void setDel(final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTTrPrImpl.DEL$2, 0);
            if (ctTrackChange2 == null) {
                ctTrackChange2 = (CTTrackChange)this.get_store().add_element_user(CTTrPrImpl.DEL$2);
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange addNewDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTTrPrImpl.DEL$2);
        }
    }
    
    public void unsetDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrImpl.DEL$2, 0);
        }
    }
    
    public CTTrPrChange getTrPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrPrChange ctTrPrChange = (CTTrPrChange)this.get_store().find_element_user(CTTrPrImpl.TRPRCHANGE$4, 0);
            if (ctTrPrChange == null) {
                return null;
            }
            return ctTrPrChange;
        }
    }
    
    public boolean isSetTrPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTrPrImpl.TRPRCHANGE$4) != 0;
        }
    }
    
    public void setTrPrChange(final CTTrPrChange ctTrPrChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTrPrChange ctTrPrChange2 = (CTTrPrChange)this.get_store().find_element_user(CTTrPrImpl.TRPRCHANGE$4, 0);
            if (ctTrPrChange2 == null) {
                ctTrPrChange2 = (CTTrPrChange)this.get_store().add_element_user(CTTrPrImpl.TRPRCHANGE$4);
            }
            ctTrPrChange2.set((XmlObject)ctTrPrChange);
        }
    }
    
    public CTTrPrChange addNewTrPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrPrChange)this.get_store().add_element_user(CTTrPrImpl.TRPRCHANGE$4);
        }
    }
    
    public void unsetTrPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTrPrImpl.TRPRCHANGE$4, 0);
        }
    }
    
    static {
        INS$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "ins");
        DEL$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "del");
        TRPRCHANGE$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "trPrChange");
    }
}
