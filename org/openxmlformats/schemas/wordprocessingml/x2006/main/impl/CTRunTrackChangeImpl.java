// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.openxmlformats.schemas.officeDocument.x2006.math.CTSSup;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTSSubSup;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTSSub;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTSPre;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTRad;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTPhant;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTNary;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTM;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTLimUpp;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTLimLow;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTGroupChr;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTFunc;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTF;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTEqArr;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTD;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTBorderBox;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTBox;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTBar;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTAcc;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMath;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMathPara;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrackChange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMoveBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkupRange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPerm;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPermStart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTProofErr;
import java.util.AbstractList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSmartTagRun;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTCustomXmlRun;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRunTrackChange;

public class CTRunTrackChangeImpl extends CTTrackChangeImpl implements CTRunTrackChange
{
    private static final QName CUSTOMXML$0;
    private static final QName SMARTTAG$2;
    private static final QName SDT$4;
    private static final QName R$6;
    private static final QName PROOFERR$8;
    private static final QName PERMSTART$10;
    private static final QName PERMEND$12;
    private static final QName BOOKMARKSTART$14;
    private static final QName BOOKMARKEND$16;
    private static final QName MOVEFROMRANGESTART$18;
    private static final QName MOVEFROMRANGEEND$20;
    private static final QName MOVETORANGESTART$22;
    private static final QName MOVETORANGEEND$24;
    private static final QName COMMENTRANGESTART$26;
    private static final QName COMMENTRANGEEND$28;
    private static final QName CUSTOMXMLINSRANGESTART$30;
    private static final QName CUSTOMXMLINSRANGEEND$32;
    private static final QName CUSTOMXMLDELRANGESTART$34;
    private static final QName CUSTOMXMLDELRANGEEND$36;
    private static final QName CUSTOMXMLMOVEFROMRANGESTART$38;
    private static final QName CUSTOMXMLMOVEFROMRANGEEND$40;
    private static final QName CUSTOMXMLMOVETORANGESTART$42;
    private static final QName CUSTOMXMLMOVETORANGEEND$44;
    private static final QName INS$46;
    private static final QName DEL$48;
    private static final QName MOVEFROM$50;
    private static final QName MOVETO$52;
    private static final QName OMATHPARA$54;
    private static final QName OMATH$56;
    private static final QName ACC$58;
    private static final QName BAR$60;
    private static final QName BOX$62;
    private static final QName BORDERBOX$64;
    private static final QName D$66;
    private static final QName EQARR$68;
    private static final QName F$70;
    private static final QName FUNC$72;
    private static final QName GROUPCHR$74;
    private static final QName LIMLOW$76;
    private static final QName LIMUPP$78;
    private static final QName M$80;
    private static final QName NARY$82;
    private static final QName PHANT$84;
    private static final QName RAD$86;
    private static final QName SPRE$88;
    private static final QName SSUB$90;
    private static final QName SSUBSUP$92;
    private static final QName SSUP$94;
    private static final QName R2$96;
    
    public CTRunTrackChangeImpl(final SchemaType schemaType) {
        super(schemaType);
    }
    
    public List<CTCustomXmlRun> getCustomXmlList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCustomXmlRun>)new CTRunTrackChangeImpl.CustomXmlList(this);
        }
    }
    
    public CTCustomXmlRun[] getCustomXmlArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXML$0, list);
            final CTCustomXmlRun[] array = new CTCustomXmlRun[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCustomXmlRun getCustomXmlArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCustomXmlRun ctCustomXmlRun = (CTCustomXmlRun)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXML$0, n);
            if (ctCustomXmlRun == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCustomXmlRun;
        }
    }
    
    public int sizeOfCustomXmlArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXML$0);
        }
    }
    
    public void setCustomXmlArray(final CTCustomXmlRun[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.CUSTOMXML$0);
        }
    }
    
    public void setCustomXmlArray(final int n, final CTCustomXmlRun ctCustomXmlRun) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCustomXmlRun ctCustomXmlRun2 = (CTCustomXmlRun)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXML$0, n);
            if (ctCustomXmlRun2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCustomXmlRun2.set((XmlObject)ctCustomXmlRun);
        }
    }
    
    public CTCustomXmlRun insertNewCustomXml(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCustomXmlRun)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXML$0, n);
        }
    }
    
    public CTCustomXmlRun addNewCustomXml() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCustomXmlRun)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXML$0);
        }
    }
    
    public void removeCustomXml(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXML$0, n);
        }
    }
    
    public List<CTSmartTagRun> getSmartTagList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSmartTagRun>)new CTRunTrackChangeImpl.SmartTagList(this);
        }
    }
    
    public CTSmartTagRun[] getSmartTagArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.SMARTTAG$2, list);
            final CTSmartTagRun[] array = new CTSmartTagRun[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSmartTagRun getSmartTagArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSmartTagRun ctSmartTagRun = (CTSmartTagRun)this.get_store().find_element_user(CTRunTrackChangeImpl.SMARTTAG$2, n);
            if (ctSmartTagRun == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSmartTagRun;
        }
    }
    
    public int sizeOfSmartTagArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.SMARTTAG$2);
        }
    }
    
    public void setSmartTagArray(final CTSmartTagRun[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.SMARTTAG$2);
        }
    }
    
    public void setSmartTagArray(final int n, final CTSmartTagRun ctSmartTagRun) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSmartTagRun ctSmartTagRun2 = (CTSmartTagRun)this.get_store().find_element_user(CTRunTrackChangeImpl.SMARTTAG$2, n);
            if (ctSmartTagRun2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSmartTagRun2.set(ctSmartTagRun);
        }
    }
    
    public CTSmartTagRun insertNewSmartTag(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSmartTagRun)this.get_store().insert_element_user(CTRunTrackChangeImpl.SMARTTAG$2, n);
        }
    }
    
    public CTSmartTagRun addNewSmartTag() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSmartTagRun)this.get_store().add_element_user(CTRunTrackChangeImpl.SMARTTAG$2);
        }
    }
    
    public void removeSmartTag(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.SMARTTAG$2, n);
        }
    }
    
    public List<CTSdtRun> getSdtList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSdtRun>)new CTRunTrackChangeImpl.SdtList(this);
        }
    }
    
    public CTSdtRun[] getSdtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.SDT$4, list);
            final CTSdtRun[] array = new CTSdtRun[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSdtRun getSdtArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtRun ctSdtRun = (CTSdtRun)this.get_store().find_element_user(CTRunTrackChangeImpl.SDT$4, n);
            if (ctSdtRun == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctSdtRun;
        }
    }
    
    public int sizeOfSdtArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.SDT$4);
        }
    }
    
    public void setSdtArray(final CTSdtRun[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.SDT$4);
        }
    }
    
    public void setSdtArray(final int n, final CTSdtRun ctSdtRun) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSdtRun ctSdtRun2 = (CTSdtRun)this.get_store().find_element_user(CTRunTrackChangeImpl.SDT$4, n);
            if (ctSdtRun2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctSdtRun2.set(ctSdtRun);
        }
    }
    
    public CTSdtRun insertNewSdt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtRun)this.get_store().insert_element_user(CTRunTrackChangeImpl.SDT$4, n);
        }
    }
    
    public CTSdtRun addNewSdt() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSdtRun)this.get_store().add_element_user(CTRunTrackChangeImpl.SDT$4);
        }
    }
    
    public void removeSdt(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.SDT$4, n);
        }
    }
    
    public List<CTR> getRList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final class RList extends AbstractList<CTR>
            {
                @Override
                public CTR get(final int n) {
                    return CTRunTrackChangeImpl.this.getRArray(n);
                }
                
                @Override
                public CTR set(final int n, final CTR ctr) {
                    final CTR rArray = CTRunTrackChangeImpl.this.getRArray(n);
                    CTRunTrackChangeImpl.this.setRArray(n, ctr);
                    return rArray;
                }
                
                @Override
                public void add(final int n, final CTR ctr) {
                    CTRunTrackChangeImpl.this.insertNewR(n).set(ctr);
                }
                
                @Override
                public CTR remove(final int n) {
                    final CTR rArray = CTRunTrackChangeImpl.this.getRArray(n);
                    CTRunTrackChangeImpl.this.removeR(n);
                    return rArray;
                }
                
                @Override
                public int size() {
                    return CTRunTrackChangeImpl.this.sizeOfRArray();
                }
            }
            return new RList();
        }
    }
    
    public CTR[] getRArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.R$6, list);
            final CTR[] array = new CTR[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTR getRArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTR ctr = (CTR)this.get_store().find_element_user(CTRunTrackChangeImpl.R$6, n);
            if (ctr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctr;
        }
    }
    
    public int sizeOfRArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.R$6);
        }
    }
    
    public void setRArray(final CTR[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.R$6);
        }
    }
    
    public void setRArray(final int n, final CTR ctr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTR ctr2 = (CTR)this.get_store().find_element_user(CTRunTrackChangeImpl.R$6, n);
            if (ctr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctr2.set(ctr);
        }
    }
    
    public CTR insertNewR(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTR)this.get_store().insert_element_user(CTRunTrackChangeImpl.R$6, n);
        }
    }
    
    public CTR addNewR() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTR)this.get_store().add_element_user(CTRunTrackChangeImpl.R$6);
        }
    }
    
    public void removeR(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.R$6, n);
        }
    }
    
    public List<CTProofErr> getProofErrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTProofErr>)new CTRunTrackChangeImpl.ProofErrList(this);
        }
    }
    
    public CTProofErr[] getProofErrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.PROOFERR$8, list);
            final CTProofErr[] array = new CTProofErr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTProofErr getProofErrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTProofErr ctProofErr = (CTProofErr)this.get_store().find_element_user(CTRunTrackChangeImpl.PROOFERR$8, n);
            if (ctProofErr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctProofErr;
        }
    }
    
    public int sizeOfProofErrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.PROOFERR$8);
        }
    }
    
    public void setProofErrArray(final CTProofErr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.PROOFERR$8);
        }
    }
    
    public void setProofErrArray(final int n, final CTProofErr ctProofErr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTProofErr ctProofErr2 = (CTProofErr)this.get_store().find_element_user(CTRunTrackChangeImpl.PROOFERR$8, n);
            if (ctProofErr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctProofErr2.set(ctProofErr);
        }
    }
    
    public CTProofErr insertNewProofErr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTProofErr)this.get_store().insert_element_user(CTRunTrackChangeImpl.PROOFERR$8, n);
        }
    }
    
    public CTProofErr addNewProofErr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTProofErr)this.get_store().add_element_user(CTRunTrackChangeImpl.PROOFERR$8);
        }
    }
    
    public void removeProofErr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.PROOFERR$8, n);
        }
    }
    
    public List<CTPermStart> getPermStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPermStart>)new CTRunTrackChangeImpl.PermStartList(this);
        }
    }
    
    public CTPermStart[] getPermStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.PERMSTART$10, list);
            final CTPermStart[] array = new CTPermStart[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPermStart getPermStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPermStart ctPermStart = (CTPermStart)this.get_store().find_element_user(CTRunTrackChangeImpl.PERMSTART$10, n);
            if (ctPermStart == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPermStart;
        }
    }
    
    public int sizeOfPermStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.PERMSTART$10);
        }
    }
    
    public void setPermStartArray(final CTPermStart[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.PERMSTART$10);
        }
    }
    
    public void setPermStartArray(final int n, final CTPermStart ctPermStart) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPermStart ctPermStart2 = (CTPermStart)this.get_store().find_element_user(CTRunTrackChangeImpl.PERMSTART$10, n);
            if (ctPermStart2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPermStart2.set((XmlObject)ctPermStart);
        }
    }
    
    public CTPermStart insertNewPermStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPermStart)this.get_store().insert_element_user(CTRunTrackChangeImpl.PERMSTART$10, n);
        }
    }
    
    public CTPermStart addNewPermStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPermStart)this.get_store().add_element_user(CTRunTrackChangeImpl.PERMSTART$10);
        }
    }
    
    public void removePermStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.PERMSTART$10, n);
        }
    }
    
    public List<CTPerm> getPermEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPerm>)new CTRunTrackChangeImpl.PermEndList(this);
        }
    }
    
    public CTPerm[] getPermEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.PERMEND$12, list);
            final CTPerm[] array = new CTPerm[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPerm getPermEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPerm ctPerm = (CTPerm)this.get_store().find_element_user(CTRunTrackChangeImpl.PERMEND$12, n);
            if (ctPerm == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPerm;
        }
    }
    
    public int sizeOfPermEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.PERMEND$12);
        }
    }
    
    public void setPermEndArray(final CTPerm[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.PERMEND$12);
        }
    }
    
    public void setPermEndArray(final int n, final CTPerm ctPerm) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPerm ctPerm2 = (CTPerm)this.get_store().find_element_user(CTRunTrackChangeImpl.PERMEND$12, n);
            if (ctPerm2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPerm2.set((XmlObject)ctPerm);
        }
    }
    
    public CTPerm insertNewPermEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPerm)this.get_store().insert_element_user(CTRunTrackChangeImpl.PERMEND$12, n);
        }
    }
    
    public CTPerm addNewPermEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPerm)this.get_store().add_element_user(CTRunTrackChangeImpl.PERMEND$12);
        }
    }
    
    public void removePermEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.PERMEND$12, n);
        }
    }
    
    public List<CTBookmark> getBookmarkStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBookmark>)new CTRunTrackChangeImpl.BookmarkStartList(this);
        }
    }
    
    public CTBookmark[] getBookmarkStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.BOOKMARKSTART$14, list);
            final CTBookmark[] array = new CTBookmark[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBookmark getBookmarkStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBookmark ctBookmark = (CTBookmark)this.get_store().find_element_user(CTRunTrackChangeImpl.BOOKMARKSTART$14, n);
            if (ctBookmark == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBookmark;
        }
    }
    
    public int sizeOfBookmarkStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.BOOKMARKSTART$14);
        }
    }
    
    public void setBookmarkStartArray(final CTBookmark[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.BOOKMARKSTART$14);
        }
    }
    
    public void setBookmarkStartArray(final int n, final CTBookmark ctBookmark) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBookmark ctBookmark2 = (CTBookmark)this.get_store().find_element_user(CTRunTrackChangeImpl.BOOKMARKSTART$14, n);
            if (ctBookmark2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBookmark2.set(ctBookmark);
        }
    }
    
    public CTBookmark insertNewBookmarkStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBookmark)this.get_store().insert_element_user(CTRunTrackChangeImpl.BOOKMARKSTART$14, n);
        }
    }
    
    public CTBookmark addNewBookmarkStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBookmark)this.get_store().add_element_user(CTRunTrackChangeImpl.BOOKMARKSTART$14);
        }
    }
    
    public void removeBookmarkStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.BOOKMARKSTART$14, n);
        }
    }
    
    public List<CTMarkupRange> getBookmarkEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTRunTrackChangeImpl.BookmarkEndList(this);
        }
    }
    
    public CTMarkupRange[] getBookmarkEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.BOOKMARKEND$16, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getBookmarkEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.BOOKMARKEND$16, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfBookmarkEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.BOOKMARKEND$16);
        }
    }
    
    public void setBookmarkEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.BOOKMARKEND$16);
        }
    }
    
    public void setBookmarkEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.BOOKMARKEND$16, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewBookmarkEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTRunTrackChangeImpl.BOOKMARKEND$16, n);
        }
    }
    
    public CTMarkupRange addNewBookmarkEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTRunTrackChangeImpl.BOOKMARKEND$16);
        }
    }
    
    public void removeBookmarkEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.BOOKMARKEND$16, n);
        }
    }
    
    public List<CTMoveBookmark> getMoveFromRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMoveBookmark>)new CTRunTrackChangeImpl.MoveFromRangeStartList(this);
        }
    }
    
    public CTMoveBookmark[] getMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.MOVEFROMRANGESTART$18, list);
            final CTMoveBookmark[] array = new CTMoveBookmark[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMoveBookmark getMoveFromRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark = (CTMoveBookmark)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVEFROMRANGESTART$18, n);
            if (ctMoveBookmark == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMoveBookmark;
        }
    }
    
    public int sizeOfMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.MOVEFROMRANGESTART$18);
        }
    }
    
    public void setMoveFromRangeStartArray(final CTMoveBookmark[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.MOVEFROMRANGESTART$18);
        }
    }
    
    public void setMoveFromRangeStartArray(final int n, final CTMoveBookmark ctMoveBookmark) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark2 = (CTMoveBookmark)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVEFROMRANGESTART$18, n);
            if (ctMoveBookmark2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMoveBookmark2.set((XmlObject)ctMoveBookmark);
        }
    }
    
    public CTMoveBookmark insertNewMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().insert_element_user(CTRunTrackChangeImpl.MOVEFROMRANGESTART$18, n);
        }
    }
    
    public CTMoveBookmark addNewMoveFromRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().add_element_user(CTRunTrackChangeImpl.MOVEFROMRANGESTART$18);
        }
    }
    
    public void removeMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.MOVEFROMRANGESTART$18, n);
        }
    }
    
    public List<CTMarkupRange> getMoveFromRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTRunTrackChangeImpl.MoveFromRangeEndList(this);
        }
    }
    
    public CTMarkupRange[] getMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.MOVEFROMRANGEEND$20, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getMoveFromRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVEFROMRANGEEND$20, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.MOVEFROMRANGEEND$20);
        }
    }
    
    public void setMoveFromRangeEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.MOVEFROMRANGEEND$20);
        }
    }
    
    public void setMoveFromRangeEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVEFROMRANGEEND$20, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTRunTrackChangeImpl.MOVEFROMRANGEEND$20, n);
        }
    }
    
    public CTMarkupRange addNewMoveFromRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTRunTrackChangeImpl.MOVEFROMRANGEEND$20);
        }
    }
    
    public void removeMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.MOVEFROMRANGEEND$20, n);
        }
    }
    
    public List<CTMoveBookmark> getMoveToRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMoveBookmark>)new CTRunTrackChangeImpl.MoveToRangeStartList(this);
        }
    }
    
    public CTMoveBookmark[] getMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.MOVETORANGESTART$22, list);
            final CTMoveBookmark[] array = new CTMoveBookmark[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMoveBookmark getMoveToRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark = (CTMoveBookmark)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVETORANGESTART$22, n);
            if (ctMoveBookmark == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMoveBookmark;
        }
    }
    
    public int sizeOfMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.MOVETORANGESTART$22);
        }
    }
    
    public void setMoveToRangeStartArray(final CTMoveBookmark[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.MOVETORANGESTART$22);
        }
    }
    
    public void setMoveToRangeStartArray(final int n, final CTMoveBookmark ctMoveBookmark) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMoveBookmark ctMoveBookmark2 = (CTMoveBookmark)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVETORANGESTART$22, n);
            if (ctMoveBookmark2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMoveBookmark2.set((XmlObject)ctMoveBookmark);
        }
    }
    
    public CTMoveBookmark insertNewMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().insert_element_user(CTRunTrackChangeImpl.MOVETORANGESTART$22, n);
        }
    }
    
    public CTMoveBookmark addNewMoveToRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMoveBookmark)this.get_store().add_element_user(CTRunTrackChangeImpl.MOVETORANGESTART$22);
        }
    }
    
    public void removeMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.MOVETORANGESTART$22, n);
        }
    }
    
    public List<CTMarkupRange> getMoveToRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTRunTrackChangeImpl.MoveToRangeEndList(this);
        }
    }
    
    public CTMarkupRange[] getMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.MOVETORANGEEND$24, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getMoveToRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVETORANGEEND$24, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.MOVETORANGEEND$24);
        }
    }
    
    public void setMoveToRangeEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.MOVETORANGEEND$24);
        }
    }
    
    public void setMoveToRangeEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVETORANGEEND$24, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTRunTrackChangeImpl.MOVETORANGEEND$24, n);
        }
    }
    
    public CTMarkupRange addNewMoveToRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTRunTrackChangeImpl.MOVETORANGEEND$24);
        }
    }
    
    public void removeMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.MOVETORANGEEND$24, n);
        }
    }
    
    public List<CTMarkupRange> getCommentRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTRunTrackChangeImpl.CommentRangeStartList(this);
        }
    }
    
    public CTMarkupRange[] getCommentRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.COMMENTRANGESTART$26, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getCommentRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.COMMENTRANGESTART$26, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfCommentRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.COMMENTRANGESTART$26);
        }
    }
    
    public void setCommentRangeStartArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.COMMENTRANGESTART$26);
        }
    }
    
    public void setCommentRangeStartArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.COMMENTRANGESTART$26, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewCommentRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTRunTrackChangeImpl.COMMENTRANGESTART$26, n);
        }
    }
    
    public CTMarkupRange addNewCommentRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTRunTrackChangeImpl.COMMENTRANGESTART$26);
        }
    }
    
    public void removeCommentRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.COMMENTRANGESTART$26, n);
        }
    }
    
    public List<CTMarkupRange> getCommentRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkupRange>)new CTRunTrackChangeImpl.CommentRangeEndList(this);
        }
    }
    
    public CTMarkupRange[] getCommentRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.COMMENTRANGEEND$28, list);
            final CTMarkupRange[] array = new CTMarkupRange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkupRange getCommentRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.COMMENTRANGEEND$28, n);
            if (ctMarkupRange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkupRange;
        }
    }
    
    public int sizeOfCommentRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.COMMENTRANGEEND$28);
        }
    }
    
    public void setCommentRangeEndArray(final CTMarkupRange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.COMMENTRANGEEND$28);
        }
    }
    
    public void setCommentRangeEndArray(final int n, final CTMarkupRange ctMarkupRange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkupRange ctMarkupRange2 = (CTMarkupRange)this.get_store().find_element_user(CTRunTrackChangeImpl.COMMENTRANGEEND$28, n);
            if (ctMarkupRange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkupRange2.set(ctMarkupRange);
        }
    }
    
    public CTMarkupRange insertNewCommentRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().insert_element_user(CTRunTrackChangeImpl.COMMENTRANGEEND$28, n);
        }
    }
    
    public CTMarkupRange addNewCommentRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkupRange)this.get_store().add_element_user(CTRunTrackChangeImpl.COMMENTRANGEEND$28);
        }
    }
    
    public void removeCommentRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.COMMENTRANGEEND$28, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlInsRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTRunTrackChangeImpl.CustomXmlInsRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlInsRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlInsRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlInsRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30);
        }
    }
    
    public void setCustomXmlInsRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30);
        }
    }
    
    public void setCustomXmlInsRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlInsRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlInsRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30);
        }
    }
    
    public void removeCustomXmlInsRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLINSRANGESTART$30, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlInsRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTRunTrackChangeImpl.CustomXmlInsRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlInsRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlInsRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlInsRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32);
        }
    }
    
    public void setCustomXmlInsRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32);
        }
    }
    
    public void setCustomXmlInsRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlInsRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32, n);
        }
    }
    
    public CTMarkup addNewCustomXmlInsRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32);
        }
    }
    
    public void removeCustomXmlInsRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLINSRANGEEND$32, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlDelRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTRunTrackChangeImpl.CustomXmlDelRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlDelRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlDelRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlDelRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34);
        }
    }
    
    public void setCustomXmlDelRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34);
        }
    }
    
    public void setCustomXmlDelRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlDelRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlDelRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34);
        }
    }
    
    public void removeCustomXmlDelRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLDELRANGESTART$34, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlDelRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTRunTrackChangeImpl.CustomXmlDelRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlDelRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlDelRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlDelRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36);
        }
    }
    
    public void setCustomXmlDelRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36);
        }
    }
    
    public void setCustomXmlDelRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlDelRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36, n);
        }
    }
    
    public CTMarkup addNewCustomXmlDelRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36);
        }
    }
    
    public void removeCustomXmlDelRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLDELRANGEEND$36, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlMoveFromRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTRunTrackChangeImpl.CustomXmlMoveFromRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlMoveFromRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlMoveFromRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38);
        }
    }
    
    public void setCustomXmlMoveFromRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38);
        }
    }
    
    public void setCustomXmlMoveFromRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlMoveFromRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38);
        }
    }
    
    public void removeCustomXmlMoveFromRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGESTART$38, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlMoveFromRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTRunTrackChangeImpl.CustomXmlMoveFromRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlMoveFromRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlMoveFromRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40);
        }
    }
    
    public void setCustomXmlMoveFromRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40);
        }
    }
    
    public void setCustomXmlMoveFromRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40, n);
        }
    }
    
    public CTMarkup addNewCustomXmlMoveFromRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40);
        }
    }
    
    public void removeCustomXmlMoveFromRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLMOVEFROMRANGEEND$40, n);
        }
    }
    
    public List<CTTrackChange> getCustomXmlMoveToRangeStartList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTTrackChange>)new CTRunTrackChangeImpl.CustomXmlMoveToRangeStartList(this);
        }
    }
    
    public CTTrackChange[] getCustomXmlMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42, list);
            final CTTrackChange[] array = new CTTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTTrackChange getCustomXmlMoveToRangeStartArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42, n);
            if (ctTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctTrackChange;
        }
    }
    
    public int sizeOfCustomXmlMoveToRangeStartArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42);
        }
    }
    
    public void setCustomXmlMoveToRangeStartArray(final CTTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42);
        }
    }
    
    public void setCustomXmlMoveToRangeStartArray(final int n, final CTTrackChange ctTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTrackChange ctTrackChange2 = (CTTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42, n);
            if (ctTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctTrackChange2.set(ctTrackChange);
        }
    }
    
    public CTTrackChange insertNewCustomXmlMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42, n);
        }
    }
    
    public CTTrackChange addNewCustomXmlMoveToRangeStart() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42);
        }
    }
    
    public void removeCustomXmlMoveToRangeStart(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGESTART$42, n);
        }
    }
    
    public List<CTMarkup> getCustomXmlMoveToRangeEndList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTMarkup>)new CTRunTrackChangeImpl.CustomXmlMoveToRangeEndList(this);
        }
    }
    
    public CTMarkup[] getCustomXmlMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44, list);
            final CTMarkup[] array = new CTMarkup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTMarkup getCustomXmlMoveToRangeEndArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44, n);
            if (ctMarkup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctMarkup;
        }
    }
    
    public int sizeOfCustomXmlMoveToRangeEndArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44);
        }
    }
    
    public void setCustomXmlMoveToRangeEndArray(final CTMarkup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44);
        }
    }
    
    public void setCustomXmlMoveToRangeEndArray(final int n, final CTMarkup ctMarkup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTMarkup ctMarkup2 = (CTMarkup)this.get_store().find_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44, n);
            if (ctMarkup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctMarkup2.set(ctMarkup);
        }
    }
    
    public CTMarkup insertNewCustomXmlMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().insert_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44, n);
        }
    }
    
    public CTMarkup addNewCustomXmlMoveToRangeEnd() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTMarkup)this.get_store().add_element_user(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44);
        }
    }
    
    public void removeCustomXmlMoveToRangeEnd(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.CUSTOMXMLMOVETORANGEEND$44, n);
        }
    }
    
    public List<CTRunTrackChange> getInsList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTRunTrackChangeImpl.InsList(this);
        }
    }
    
    public CTRunTrackChange[] getInsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.INS$46, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getInsArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.INS$46, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfInsArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.INS$46);
        }
    }
    
    public void setInsArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.INS$46);
        }
    }
    
    public void setInsArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.INS$46, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewIns(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.INS$46, n);
        }
    }
    
    public CTRunTrackChange addNewIns() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.INS$46);
        }
    }
    
    public void removeIns(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.INS$46, n);
        }
    }
    
    public List<CTRunTrackChange> getDelList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTRunTrackChangeImpl.DelList(this);
        }
    }
    
    public CTRunTrackChange[] getDelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.DEL$48, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getDelArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.DEL$48, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfDelArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.DEL$48);
        }
    }
    
    public void setDelArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.DEL$48);
        }
    }
    
    public void setDelArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.DEL$48, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewDel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.DEL$48, n);
        }
    }
    
    public CTRunTrackChange addNewDel() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.DEL$48);
        }
    }
    
    public void removeDel(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.DEL$48, n);
        }
    }
    
    public List<CTRunTrackChange> getMoveFromList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTRunTrackChangeImpl.MoveFromList(this);
        }
    }
    
    public CTRunTrackChange[] getMoveFromArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.MOVEFROM$50, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getMoveFromArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVEFROM$50, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfMoveFromArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.MOVEFROM$50);
        }
    }
    
    public void setMoveFromArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.MOVEFROM$50);
        }
    }
    
    public void setMoveFromArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVEFROM$50, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewMoveFrom(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.MOVEFROM$50, n);
        }
    }
    
    public CTRunTrackChange addNewMoveFrom() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.MOVEFROM$50);
        }
    }
    
    public void removeMoveFrom(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.MOVEFROM$50, n);
        }
    }
    
    public List<CTRunTrackChange> getMoveToList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRunTrackChange>)new CTRunTrackChangeImpl.MoveToList(this);
        }
    }
    
    public CTRunTrackChange[] getMoveToArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.MOVETO$52, list);
            final CTRunTrackChange[] array = new CTRunTrackChange[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRunTrackChange getMoveToArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVETO$52, n);
            if (ctRunTrackChange == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRunTrackChange;
        }
    }
    
    public int sizeOfMoveToArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.MOVETO$52);
        }
    }
    
    public void setMoveToArray(final CTRunTrackChange[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTRunTrackChangeImpl.MOVETO$52);
        }
    }
    
    public void setMoveToArray(final int n, final CTRunTrackChange ctRunTrackChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRunTrackChange ctRunTrackChange2 = (CTRunTrackChange)this.get_store().find_element_user(CTRunTrackChangeImpl.MOVETO$52, n);
            if (ctRunTrackChange2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRunTrackChange2.set(ctRunTrackChange);
        }
    }
    
    public CTRunTrackChange insertNewMoveTo(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().insert_element_user(CTRunTrackChangeImpl.MOVETO$52, n);
        }
    }
    
    public CTRunTrackChange addNewMoveTo() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRunTrackChange)this.get_store().add_element_user(CTRunTrackChangeImpl.MOVETO$52);
        }
    }
    
    public void removeMoveTo(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.MOVETO$52, n);
        }
    }
    
    public List<CTOMathPara> getOMathParaList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOMathPara>)new CTRunTrackChangeImpl.OMathParaList(this);
        }
    }
    
    public CTOMathPara[] getOMathParaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.OMATHPARA$54, list);
            final CTOMathPara[] array = new CTOMathPara[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOMathPara getOMathParaArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMathPara ctoMathPara = (CTOMathPara)this.get_store().find_element_user(CTRunTrackChangeImpl.OMATHPARA$54, n);
            if (ctoMathPara == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctoMathPara;
        }
    }
    
    public int sizeOfOMathParaArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.OMATHPARA$54);
        }
    }
    
    public void setOMathParaArray(final CTOMathPara[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.OMATHPARA$54);
        }
    }
    
    public void setOMathParaArray(final int n, final CTOMathPara ctoMathPara) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMathPara ctoMathPara2 = (CTOMathPara)this.get_store().find_element_user(CTRunTrackChangeImpl.OMATHPARA$54, n);
            if (ctoMathPara2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctoMathPara2.set((XmlObject)ctoMathPara);
        }
    }
    
    public CTOMathPara insertNewOMathPara(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMathPara)this.get_store().insert_element_user(CTRunTrackChangeImpl.OMATHPARA$54, n);
        }
    }
    
    public CTOMathPara addNewOMathPara() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMathPara)this.get_store().add_element_user(CTRunTrackChangeImpl.OMATHPARA$54);
        }
    }
    
    public void removeOMathPara(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.OMATHPARA$54, n);
        }
    }
    
    public List<CTOMath> getOMathList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTOMath>)new CTRunTrackChangeImpl.OMathList(this);
        }
    }
    
    public CTOMath[] getOMathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.OMATH$56, list);
            final CTOMath[] array = new CTOMath[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTOMath getOMathArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMath ctoMath = (CTOMath)this.get_store().find_element_user(CTRunTrackChangeImpl.OMATH$56, n);
            if (ctoMath == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctoMath;
        }
    }
    
    public int sizeOfOMathArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.OMATH$56);
        }
    }
    
    public void setOMathArray(final CTOMath[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.OMATH$56);
        }
    }
    
    public void setOMathArray(final int n, final CTOMath ctoMath) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTOMath ctoMath2 = (CTOMath)this.get_store().find_element_user(CTRunTrackChangeImpl.OMATH$56, n);
            if (ctoMath2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctoMath2.set((XmlObject)ctoMath);
        }
    }
    
    public CTOMath insertNewOMath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMath)this.get_store().insert_element_user(CTRunTrackChangeImpl.OMATH$56, n);
        }
    }
    
    public CTOMath addNewOMath() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTOMath)this.get_store().add_element_user(CTRunTrackChangeImpl.OMATH$56);
        }
    }
    
    public void removeOMath(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.OMATH$56, n);
        }
    }
    
    public List<CTAcc> getAccList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAcc>)new CTRunTrackChangeImpl.AccList(this);
        }
    }
    
    public CTAcc[] getAccArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.ACC$58, list);
            final CTAcc[] array = new CTAcc[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAcc getAccArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAcc ctAcc = (CTAcc)this.get_store().find_element_user(CTRunTrackChangeImpl.ACC$58, n);
            if (ctAcc == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAcc;
        }
    }
    
    public int sizeOfAccArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.ACC$58);
        }
    }
    
    public void setAccArray(final CTAcc[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.ACC$58);
        }
    }
    
    public void setAccArray(final int n, final CTAcc ctAcc) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAcc ctAcc2 = (CTAcc)this.get_store().find_element_user(CTRunTrackChangeImpl.ACC$58, n);
            if (ctAcc2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAcc2.set((XmlObject)ctAcc);
        }
    }
    
    public CTAcc insertNewAcc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAcc)this.get_store().insert_element_user(CTRunTrackChangeImpl.ACC$58, n);
        }
    }
    
    public CTAcc addNewAcc() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAcc)this.get_store().add_element_user(CTRunTrackChangeImpl.ACC$58);
        }
    }
    
    public void removeAcc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.ACC$58, n);
        }
    }
    
    public List<CTBar> getBarList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBar>)new CTRunTrackChangeImpl.BarList(this);
        }
    }
    
    public CTBar[] getBarArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.BAR$60, list);
            final CTBar[] array = new CTBar[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBar getBarArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBar ctBar = (CTBar)this.get_store().find_element_user(CTRunTrackChangeImpl.BAR$60, n);
            if (ctBar == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBar;
        }
    }
    
    public int sizeOfBarArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.BAR$60);
        }
    }
    
    public void setBarArray(final CTBar[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.BAR$60);
        }
    }
    
    public void setBarArray(final int n, final CTBar ctBar) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBar ctBar2 = (CTBar)this.get_store().find_element_user(CTRunTrackChangeImpl.BAR$60, n);
            if (ctBar2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBar2.set((XmlObject)ctBar);
        }
    }
    
    public CTBar insertNewBar(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBar)this.get_store().insert_element_user(CTRunTrackChangeImpl.BAR$60, n);
        }
    }
    
    public CTBar addNewBar() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBar)this.get_store().add_element_user(CTRunTrackChangeImpl.BAR$60);
        }
    }
    
    public void removeBar(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.BAR$60, n);
        }
    }
    
    public List<CTBox> getBoxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBox>)new CTRunTrackChangeImpl.BoxList(this);
        }
    }
    
    public CTBox[] getBoxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.BOX$62, list);
            final CTBox[] array = new CTBox[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBox getBoxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBox ctBox = (CTBox)this.get_store().find_element_user(CTRunTrackChangeImpl.BOX$62, n);
            if (ctBox == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBox;
        }
    }
    
    public int sizeOfBoxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.BOX$62);
        }
    }
    
    public void setBoxArray(final CTBox[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.BOX$62);
        }
    }
    
    public void setBoxArray(final int n, final CTBox ctBox) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBox ctBox2 = (CTBox)this.get_store().find_element_user(CTRunTrackChangeImpl.BOX$62, n);
            if (ctBox2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBox2.set((XmlObject)ctBox);
        }
    }
    
    public CTBox insertNewBox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBox)this.get_store().insert_element_user(CTRunTrackChangeImpl.BOX$62, n);
        }
    }
    
    public CTBox addNewBox() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBox)this.get_store().add_element_user(CTRunTrackChangeImpl.BOX$62);
        }
    }
    
    public void removeBox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.BOX$62, n);
        }
    }
    
    public List<CTBorderBox> getBorderBoxList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTBorderBox>)new CTRunTrackChangeImpl.BorderBoxList(this);
        }
    }
    
    public CTBorderBox[] getBorderBoxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.BORDERBOX$64, list);
            final CTBorderBox[] array = new CTBorderBox[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTBorderBox getBorderBoxArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorderBox ctBorderBox = (CTBorderBox)this.get_store().find_element_user(CTRunTrackChangeImpl.BORDERBOX$64, n);
            if (ctBorderBox == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctBorderBox;
        }
    }
    
    public int sizeOfBorderBoxArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.BORDERBOX$64);
        }
    }
    
    public void setBorderBoxArray(final CTBorderBox[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.BORDERBOX$64);
        }
    }
    
    public void setBorderBoxArray(final int n, final CTBorderBox ctBorderBox) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTBorderBox ctBorderBox2 = (CTBorderBox)this.get_store().find_element_user(CTRunTrackChangeImpl.BORDERBOX$64, n);
            if (ctBorderBox2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctBorderBox2.set((XmlObject)ctBorderBox);
        }
    }
    
    public CTBorderBox insertNewBorderBox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorderBox)this.get_store().insert_element_user(CTRunTrackChangeImpl.BORDERBOX$64, n);
        }
    }
    
    public CTBorderBox addNewBorderBox() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTBorderBox)this.get_store().add_element_user(CTRunTrackChangeImpl.BORDERBOX$64);
        }
    }
    
    public void removeBorderBox(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.BORDERBOX$64, n);
        }
    }
    
    public List<CTD> getDList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTD>)new CTRunTrackChangeImpl.DList(this);
        }
    }
    
    public CTD[] getDArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.D$66, list);
            final CTD[] array = new CTD[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTD getDArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTD ctd = (CTD)this.get_store().find_element_user(CTRunTrackChangeImpl.D$66, n);
            if (ctd == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctd;
        }
    }
    
    public int sizeOfDArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.D$66);
        }
    }
    
    public void setDArray(final CTD[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.D$66);
        }
    }
    
    public void setDArray(final int n, final CTD ctd) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTD ctd2 = (CTD)this.get_store().find_element_user(CTRunTrackChangeImpl.D$66, n);
            if (ctd2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctd2.set((XmlObject)ctd);
        }
    }
    
    public CTD insertNewD(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTD)this.get_store().insert_element_user(CTRunTrackChangeImpl.D$66, n);
        }
    }
    
    public CTD addNewD() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTD)this.get_store().add_element_user(CTRunTrackChangeImpl.D$66);
        }
    }
    
    public void removeD(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.D$66, n);
        }
    }
    
    public List<CTEqArr> getEqArrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTEqArr>)new CTRunTrackChangeImpl.EqArrList(this);
        }
    }
    
    public CTEqArr[] getEqArrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.EQARR$68, list);
            final CTEqArr[] array = new CTEqArr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTEqArr getEqArrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEqArr ctEqArr = (CTEqArr)this.get_store().find_element_user(CTRunTrackChangeImpl.EQARR$68, n);
            if (ctEqArr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctEqArr;
        }
    }
    
    public int sizeOfEqArrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.EQARR$68);
        }
    }
    
    public void setEqArrArray(final CTEqArr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.EQARR$68);
        }
    }
    
    public void setEqArrArray(final int n, final CTEqArr ctEqArr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTEqArr ctEqArr2 = (CTEqArr)this.get_store().find_element_user(CTRunTrackChangeImpl.EQARR$68, n);
            if (ctEqArr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctEqArr2.set((XmlObject)ctEqArr);
        }
    }
    
    public CTEqArr insertNewEqArr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEqArr)this.get_store().insert_element_user(CTRunTrackChangeImpl.EQARR$68, n);
        }
    }
    
    public CTEqArr addNewEqArr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTEqArr)this.get_store().add_element_user(CTRunTrackChangeImpl.EQARR$68);
        }
    }
    
    public void removeEqArr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.EQARR$68, n);
        }
    }
    
    public List<CTF> getFList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTF>)new CTRunTrackChangeImpl.FList(this);
        }
    }
    
    public CTF[] getFArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.F$70, list);
            final CTF[] array = new CTF[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTF getFArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTF ctf = (CTF)this.get_store().find_element_user(CTRunTrackChangeImpl.F$70, n);
            if (ctf == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctf;
        }
    }
    
    public int sizeOfFArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.F$70);
        }
    }
    
    public void setFArray(final CTF[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.F$70);
        }
    }
    
    public void setFArray(final int n, final CTF ctf) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTF ctf2 = (CTF)this.get_store().find_element_user(CTRunTrackChangeImpl.F$70, n);
            if (ctf2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctf2.set((XmlObject)ctf);
        }
    }
    
    public CTF insertNewF(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTF)this.get_store().insert_element_user(CTRunTrackChangeImpl.F$70, n);
        }
    }
    
    public CTF addNewF() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTF)this.get_store().add_element_user(CTRunTrackChangeImpl.F$70);
        }
    }
    
    public void removeF(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.F$70, n);
        }
    }
    
    public List<CTFunc> getFuncList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTFunc>)new CTRunTrackChangeImpl.FuncList(this);
        }
    }
    
    public CTFunc[] getFuncArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.FUNC$72, list);
            final CTFunc[] array = new CTFunc[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTFunc getFuncArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFunc ctFunc = (CTFunc)this.get_store().find_element_user(CTRunTrackChangeImpl.FUNC$72, n);
            if (ctFunc == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctFunc;
        }
    }
    
    public int sizeOfFuncArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.FUNC$72);
        }
    }
    
    public void setFuncArray(final CTFunc[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.FUNC$72);
        }
    }
    
    public void setFuncArray(final int n, final CTFunc ctFunc) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTFunc ctFunc2 = (CTFunc)this.get_store().find_element_user(CTRunTrackChangeImpl.FUNC$72, n);
            if (ctFunc2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctFunc2.set((XmlObject)ctFunc);
        }
    }
    
    public CTFunc insertNewFunc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFunc)this.get_store().insert_element_user(CTRunTrackChangeImpl.FUNC$72, n);
        }
    }
    
    public CTFunc addNewFunc() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTFunc)this.get_store().add_element_user(CTRunTrackChangeImpl.FUNC$72);
        }
    }
    
    public void removeFunc(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.FUNC$72, n);
        }
    }
    
    public List<CTGroupChr> getGroupChrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTGroupChr>)new CTRunTrackChangeImpl.GroupChrList(this);
        }
    }
    
    public CTGroupChr[] getGroupChrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.GROUPCHR$74, list);
            final CTGroupChr[] array = new CTGroupChr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTGroupChr getGroupChrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupChr ctGroupChr = (CTGroupChr)this.get_store().find_element_user(CTRunTrackChangeImpl.GROUPCHR$74, n);
            if (ctGroupChr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctGroupChr;
        }
    }
    
    public int sizeOfGroupChrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.GROUPCHR$74);
        }
    }
    
    public void setGroupChrArray(final CTGroupChr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.GROUPCHR$74);
        }
    }
    
    public void setGroupChrArray(final int n, final CTGroupChr ctGroupChr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTGroupChr ctGroupChr2 = (CTGroupChr)this.get_store().find_element_user(CTRunTrackChangeImpl.GROUPCHR$74, n);
            if (ctGroupChr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctGroupChr2.set((XmlObject)ctGroupChr);
        }
    }
    
    public CTGroupChr insertNewGroupChr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupChr)this.get_store().insert_element_user(CTRunTrackChangeImpl.GROUPCHR$74, n);
        }
    }
    
    public CTGroupChr addNewGroupChr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTGroupChr)this.get_store().add_element_user(CTRunTrackChangeImpl.GROUPCHR$74);
        }
    }
    
    public void removeGroupChr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.GROUPCHR$74, n);
        }
    }
    
    public List<CTLimLow> getLimLowList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLimLow>)new CTRunTrackChangeImpl.LimLowList(this);
        }
    }
    
    public CTLimLow[] getLimLowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.LIMLOW$76, list);
            final CTLimLow[] array = new CTLimLow[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLimLow getLimLowArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLimLow ctLimLow = (CTLimLow)this.get_store().find_element_user(CTRunTrackChangeImpl.LIMLOW$76, n);
            if (ctLimLow == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLimLow;
        }
    }
    
    public int sizeOfLimLowArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.LIMLOW$76);
        }
    }
    
    public void setLimLowArray(final CTLimLow[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.LIMLOW$76);
        }
    }
    
    public void setLimLowArray(final int n, final CTLimLow ctLimLow) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLimLow ctLimLow2 = (CTLimLow)this.get_store().find_element_user(CTRunTrackChangeImpl.LIMLOW$76, n);
            if (ctLimLow2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLimLow2.set((XmlObject)ctLimLow);
        }
    }
    
    public CTLimLow insertNewLimLow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLimLow)this.get_store().insert_element_user(CTRunTrackChangeImpl.LIMLOW$76, n);
        }
    }
    
    public CTLimLow addNewLimLow() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLimLow)this.get_store().add_element_user(CTRunTrackChangeImpl.LIMLOW$76);
        }
    }
    
    public void removeLimLow(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.LIMLOW$76, n);
        }
    }
    
    public List<CTLimUpp> getLimUppList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTLimUpp>)new CTRunTrackChangeImpl.LimUppList(this);
        }
    }
    
    public CTLimUpp[] getLimUppArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.LIMUPP$78, list);
            final CTLimUpp[] array = new CTLimUpp[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTLimUpp getLimUppArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLimUpp ctLimUpp = (CTLimUpp)this.get_store().find_element_user(CTRunTrackChangeImpl.LIMUPP$78, n);
            if (ctLimUpp == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctLimUpp;
        }
    }
    
    public int sizeOfLimUppArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.LIMUPP$78);
        }
    }
    
    public void setLimUppArray(final CTLimUpp[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.LIMUPP$78);
        }
    }
    
    public void setLimUppArray(final int n, final CTLimUpp ctLimUpp) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTLimUpp ctLimUpp2 = (CTLimUpp)this.get_store().find_element_user(CTRunTrackChangeImpl.LIMUPP$78, n);
            if (ctLimUpp2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctLimUpp2.set((XmlObject)ctLimUpp);
        }
    }
    
    public CTLimUpp insertNewLimUpp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLimUpp)this.get_store().insert_element_user(CTRunTrackChangeImpl.LIMUPP$78, n);
        }
    }
    
    public CTLimUpp addNewLimUpp() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTLimUpp)this.get_store().add_element_user(CTRunTrackChangeImpl.LIMUPP$78);
        }
    }
    
    public void removeLimUpp(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.LIMUPP$78, n);
        }
    }
    
    public List<CTM> getMList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTM>)new CTRunTrackChangeImpl.MList(this);
        }
    }
    
    public CTM[] getMArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.M$80, list);
            final CTM[] array = new CTM[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTM getMArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTM ctm = (CTM)this.get_store().find_element_user(CTRunTrackChangeImpl.M$80, n);
            if (ctm == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctm;
        }
    }
    
    public int sizeOfMArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.M$80);
        }
    }
    
    public void setMArray(final CTM[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.M$80);
        }
    }
    
    public void setMArray(final int n, final CTM ctm) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTM ctm2 = (CTM)this.get_store().find_element_user(CTRunTrackChangeImpl.M$80, n);
            if (ctm2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctm2.set((XmlObject)ctm);
        }
    }
    
    public CTM insertNewM(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTM)this.get_store().insert_element_user(CTRunTrackChangeImpl.M$80, n);
        }
    }
    
    public CTM addNewM() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTM)this.get_store().add_element_user(CTRunTrackChangeImpl.M$80);
        }
    }
    
    public void removeM(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.M$80, n);
        }
    }
    
    public List<CTNary> getNaryList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTNary>)new CTRunTrackChangeImpl.NaryList(this);
        }
    }
    
    public CTNary[] getNaryArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.NARY$82, list);
            final CTNary[] array = new CTNary[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTNary getNaryArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNary ctNary = (CTNary)this.get_store().find_element_user(CTRunTrackChangeImpl.NARY$82, n);
            if (ctNary == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctNary;
        }
    }
    
    public int sizeOfNaryArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.NARY$82);
        }
    }
    
    public void setNaryArray(final CTNary[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.NARY$82);
        }
    }
    
    public void setNaryArray(final int n, final CTNary ctNary) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTNary ctNary2 = (CTNary)this.get_store().find_element_user(CTRunTrackChangeImpl.NARY$82, n);
            if (ctNary2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctNary2.set((XmlObject)ctNary);
        }
    }
    
    public CTNary insertNewNary(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNary)this.get_store().insert_element_user(CTRunTrackChangeImpl.NARY$82, n);
        }
    }
    
    public CTNary addNewNary() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTNary)this.get_store().add_element_user(CTRunTrackChangeImpl.NARY$82);
        }
    }
    
    public void removeNary(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.NARY$82, n);
        }
    }
    
    public List<CTPhant> getPhantList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTPhant>)new CTRunTrackChangeImpl.PhantList(this);
        }
    }
    
    public CTPhant[] getPhantArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.PHANT$84, list);
            final CTPhant[] array = new CTPhant[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTPhant getPhantArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPhant ctPhant = (CTPhant)this.get_store().find_element_user(CTRunTrackChangeImpl.PHANT$84, n);
            if (ctPhant == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctPhant;
        }
    }
    
    public int sizeOfPhantArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.PHANT$84);
        }
    }
    
    public void setPhantArray(final CTPhant[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.PHANT$84);
        }
    }
    
    public void setPhantArray(final int n, final CTPhant ctPhant) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTPhant ctPhant2 = (CTPhant)this.get_store().find_element_user(CTRunTrackChangeImpl.PHANT$84, n);
            if (ctPhant2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctPhant2.set((XmlObject)ctPhant);
        }
    }
    
    public CTPhant insertNewPhant(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPhant)this.get_store().insert_element_user(CTRunTrackChangeImpl.PHANT$84, n);
        }
    }
    
    public CTPhant addNewPhant() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTPhant)this.get_store().add_element_user(CTRunTrackChangeImpl.PHANT$84);
        }
    }
    
    public void removePhant(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.PHANT$84, n);
        }
    }
    
    public List<CTRad> getRadList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTRad>)new CTRunTrackChangeImpl.RadList(this);
        }
    }
    
    public CTRad[] getRadArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.RAD$86, list);
            final CTRad[] array = new CTRad[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTRad getRadArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRad ctRad = (CTRad)this.get_store().find_element_user(CTRunTrackChangeImpl.RAD$86, n);
            if (ctRad == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctRad;
        }
    }
    
    public int sizeOfRadArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.RAD$86);
        }
    }
    
    public void setRadArray(final CTRad[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.RAD$86);
        }
    }
    
    public void setRadArray(final int n, final CTRad ctRad) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTRad ctRad2 = (CTRad)this.get_store().find_element_user(CTRunTrackChangeImpl.RAD$86, n);
            if (ctRad2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctRad2.set((XmlObject)ctRad);
        }
    }
    
    public CTRad insertNewRad(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRad)this.get_store().insert_element_user(CTRunTrackChangeImpl.RAD$86, n);
        }
    }
    
    public CTRad addNewRad() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTRad)this.get_store().add_element_user(CTRunTrackChangeImpl.RAD$86);
        }
    }
    
    public void removeRad(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.RAD$86, n);
        }
    }
    
    public List<CTSPre> getSPreList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSPre>)new CTRunTrackChangeImpl.SPreList(this);
        }
    }
    
    public CTSPre[] getSPreArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.SPRE$88, list);
            final CTSPre[] array = new CTSPre[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSPre getSPreArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSPre ctsPre = (CTSPre)this.get_store().find_element_user(CTRunTrackChangeImpl.SPRE$88, n);
            if (ctsPre == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctsPre;
        }
    }
    
    public int sizeOfSPreArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.SPRE$88);
        }
    }
    
    public void setSPreArray(final CTSPre[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.SPRE$88);
        }
    }
    
    public void setSPreArray(final int n, final CTSPre ctsPre) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSPre ctsPre2 = (CTSPre)this.get_store().find_element_user(CTRunTrackChangeImpl.SPRE$88, n);
            if (ctsPre2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctsPre2.set((XmlObject)ctsPre);
        }
    }
    
    public CTSPre insertNewSPre(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSPre)this.get_store().insert_element_user(CTRunTrackChangeImpl.SPRE$88, n);
        }
    }
    
    public CTSPre addNewSPre() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSPre)this.get_store().add_element_user(CTRunTrackChangeImpl.SPRE$88);
        }
    }
    
    public void removeSPre(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.SPRE$88, n);
        }
    }
    
    public List<CTSSub> getSSubList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSSub>)new CTRunTrackChangeImpl.SSubList(this);
        }
    }
    
    public CTSSub[] getSSubArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.SSUB$90, list);
            final CTSSub[] array = new CTSSub[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSSub getSSubArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSSub ctsSub = (CTSSub)this.get_store().find_element_user(CTRunTrackChangeImpl.SSUB$90, n);
            if (ctsSub == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctsSub;
        }
    }
    
    public int sizeOfSSubArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.SSUB$90);
        }
    }
    
    public void setSSubArray(final CTSSub[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.SSUB$90);
        }
    }
    
    public void setSSubArray(final int n, final CTSSub ctsSub) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSSub ctsSub2 = (CTSSub)this.get_store().find_element_user(CTRunTrackChangeImpl.SSUB$90, n);
            if (ctsSub2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctsSub2.set((XmlObject)ctsSub);
        }
    }
    
    public CTSSub insertNewSSub(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSSub)this.get_store().insert_element_user(CTRunTrackChangeImpl.SSUB$90, n);
        }
    }
    
    public CTSSub addNewSSub() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSSub)this.get_store().add_element_user(CTRunTrackChangeImpl.SSUB$90);
        }
    }
    
    public void removeSSub(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.SSUB$90, n);
        }
    }
    
    public List<CTSSubSup> getSSubSupList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSSubSup>)new CTRunTrackChangeImpl.SSubSupList(this);
        }
    }
    
    public CTSSubSup[] getSSubSupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.SSUBSUP$92, list);
            final CTSSubSup[] array = new CTSSubSup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSSubSup getSSubSupArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSSubSup ctsSubSup = (CTSSubSup)this.get_store().find_element_user(CTRunTrackChangeImpl.SSUBSUP$92, n);
            if (ctsSubSup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctsSubSup;
        }
    }
    
    public int sizeOfSSubSupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.SSUBSUP$92);
        }
    }
    
    public void setSSubSupArray(final CTSSubSup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.SSUBSUP$92);
        }
    }
    
    public void setSSubSupArray(final int n, final CTSSubSup ctsSubSup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSSubSup ctsSubSup2 = (CTSSubSup)this.get_store().find_element_user(CTRunTrackChangeImpl.SSUBSUP$92, n);
            if (ctsSubSup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctsSubSup2.set((XmlObject)ctsSubSup);
        }
    }
    
    public CTSSubSup insertNewSSubSup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSSubSup)this.get_store().insert_element_user(CTRunTrackChangeImpl.SSUBSUP$92, n);
        }
    }
    
    public CTSSubSup addNewSSubSup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSSubSup)this.get_store().add_element_user(CTRunTrackChangeImpl.SSUBSUP$92);
        }
    }
    
    public void removeSSubSup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.SSUBSUP$92, n);
        }
    }
    
    public List<CTSSup> getSSupList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTSSup>)new CTRunTrackChangeImpl.SSupList(this);
        }
    }
    
    public CTSSup[] getSSupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.SSUP$94, list);
            final CTSSup[] array = new CTSSup[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTSSup getSSupArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSSup ctsSup = (CTSSup)this.get_store().find_element_user(CTRunTrackChangeImpl.SSUP$94, n);
            if (ctsSup == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctsSup;
        }
    }
    
    public int sizeOfSSupArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.SSUP$94);
        }
    }
    
    public void setSSupArray(final CTSSup[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.SSUP$94);
        }
    }
    
    public void setSSupArray(final int n, final CTSSup ctsSup) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTSSup ctsSup2 = (CTSSup)this.get_store().find_element_user(CTRunTrackChangeImpl.SSUP$94, n);
            if (ctsSup2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctsSup2.set((XmlObject)ctsSup);
        }
    }
    
    public CTSSup insertNewSSup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSSup)this.get_store().insert_element_user(CTRunTrackChangeImpl.SSUP$94, n);
        }
    }
    
    public CTSSup addNewSSup() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTSSup)this.get_store().add_element_user(CTRunTrackChangeImpl.SSUP$94);
        }
    }
    
    public void removeSSup(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.SSUP$94, n);
        }
    }
    
    public List<org.openxmlformats.schemas.officeDocument.x2006.math.CTR> getR2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<org.openxmlformats.schemas.officeDocument.x2006.math.CTR>)new CTRunTrackChangeImpl.R2List(this);
        }
    }
    
    public org.openxmlformats.schemas.officeDocument.x2006.math.CTR[] getR2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTRunTrackChangeImpl.R2$96, list);
            final org.openxmlformats.schemas.officeDocument.x2006.math.CTR[] array = new org.openxmlformats.schemas.officeDocument.x2006.math.CTR[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public org.openxmlformats.schemas.officeDocument.x2006.math.CTR getR2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final org.openxmlformats.schemas.officeDocument.x2006.math.CTR ctr = (org.openxmlformats.schemas.officeDocument.x2006.math.CTR)this.get_store().find_element_user(CTRunTrackChangeImpl.R2$96, n);
            if (ctr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctr;
        }
    }
    
    public int sizeOfR2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTRunTrackChangeImpl.R2$96);
        }
    }
    
    public void setR2Array(final org.openxmlformats.schemas.officeDocument.x2006.math.CTR[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTRunTrackChangeImpl.R2$96);
        }
    }
    
    public void setR2Array(final int n, final org.openxmlformats.schemas.officeDocument.x2006.math.CTR ctr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final org.openxmlformats.schemas.officeDocument.x2006.math.CTR ctr2 = (org.openxmlformats.schemas.officeDocument.x2006.math.CTR)this.get_store().find_element_user(CTRunTrackChangeImpl.R2$96, n);
            if (ctr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctr2.set((XmlObject)ctr);
        }
    }
    
    public org.openxmlformats.schemas.officeDocument.x2006.math.CTR insertNewR2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (org.openxmlformats.schemas.officeDocument.x2006.math.CTR)this.get_store().insert_element_user(CTRunTrackChangeImpl.R2$96, n);
        }
    }
    
    public org.openxmlformats.schemas.officeDocument.x2006.math.CTR addNewR2() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (org.openxmlformats.schemas.officeDocument.x2006.math.CTR)this.get_store().add_element_user(CTRunTrackChangeImpl.R2$96);
        }
    }
    
    public void removeR2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTRunTrackChangeImpl.R2$96, n);
        }
    }
    
    static {
        CUSTOMXML$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXml");
        SMARTTAG$2 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "smartTag");
        SDT$4 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "sdt");
        R$6 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "r");
        PROOFERR$8 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "proofErr");
        PERMSTART$10 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "permStart");
        PERMEND$12 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "permEnd");
        BOOKMARKSTART$14 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookmarkStart");
        BOOKMARKEND$16 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "bookmarkEnd");
        MOVEFROMRANGESTART$18 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveFromRangeStart");
        MOVEFROMRANGEEND$20 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveFromRangeEnd");
        MOVETORANGESTART$22 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveToRangeStart");
        MOVETORANGEEND$24 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveToRangeEnd");
        COMMENTRANGESTART$26 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "commentRangeStart");
        COMMENTRANGEEND$28 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "commentRangeEnd");
        CUSTOMXMLINSRANGESTART$30 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlInsRangeStart");
        CUSTOMXMLINSRANGEEND$32 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlInsRangeEnd");
        CUSTOMXMLDELRANGESTART$34 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlDelRangeStart");
        CUSTOMXMLDELRANGEEND$36 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlDelRangeEnd");
        CUSTOMXMLMOVEFROMRANGESTART$38 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveFromRangeStart");
        CUSTOMXMLMOVEFROMRANGEEND$40 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveFromRangeEnd");
        CUSTOMXMLMOVETORANGESTART$42 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveToRangeStart");
        CUSTOMXMLMOVETORANGEEND$44 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "customXmlMoveToRangeEnd");
        INS$46 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "ins");
        DEL$48 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "del");
        MOVEFROM$50 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveFrom");
        MOVETO$52 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "moveTo");
        OMATHPARA$54 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "oMathPara");
        OMATH$56 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "oMath");
        ACC$58 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "acc");
        BAR$60 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "bar");
        BOX$62 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "box");
        BORDERBOX$64 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "borderBox");
        D$66 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "d");
        EQARR$68 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "eqArr");
        F$70 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "f");
        FUNC$72 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "func");
        GROUPCHR$74 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "groupChr");
        LIMLOW$76 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "limLow");
        LIMUPP$78 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "limUpp");
        M$80 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "m");
        NARY$82 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "nary");
        PHANT$84 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "phant");
        RAD$86 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "rad");
        SPRE$88 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "sPre");
        SSUB$90 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "sSub");
        SSUBSUP$92 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "sSubSup");
        SSUP$94 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "sSup");
        R2$96 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/math", "r");
    }
}
