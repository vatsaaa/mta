// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAttr;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSmartTagPr;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTSmartTagPrImpl extends XmlComplexContentImpl implements CTSmartTagPr
{
    private static final QName ATTR$0;
    
    public CTSmartTagPrImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTAttr> getAttrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTAttr>)new CTSmartTagPrImpl.AttrList(this);
        }
    }
    
    public CTAttr[] getAttrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTSmartTagPrImpl.ATTR$0, list);
            final CTAttr[] array = new CTAttr[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTAttr getAttrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAttr ctAttr = (CTAttr)this.get_store().find_element_user(CTSmartTagPrImpl.ATTR$0, n);
            if (ctAttr == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctAttr;
        }
    }
    
    public int sizeOfAttrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTSmartTagPrImpl.ATTR$0);
        }
    }
    
    public void setAttrArray(final CTAttr[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTSmartTagPrImpl.ATTR$0);
        }
    }
    
    public void setAttrArray(final int n, final CTAttr ctAttr) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTAttr ctAttr2 = (CTAttr)this.get_store().find_element_user(CTSmartTagPrImpl.ATTR$0, n);
            if (ctAttr2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctAttr2.set((XmlObject)ctAttr);
        }
    }
    
    public CTAttr insertNewAttr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAttr)this.get_store().insert_element_user(CTSmartTagPrImpl.ATTR$0, n);
        }
    }
    
    public CTAttr addNewAttr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTAttr)this.get_store().add_element_user(CTSmartTagPrImpl.ATTR$0);
        }
    }
    
    public void removeAttr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTSmartTagPrImpl.ATTR$0, n);
        }
    }
    
    static {
        ATTR$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "attr");
    }
}
