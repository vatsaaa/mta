// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHdrFtr;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STHdrFtrImpl extends JavaStringEnumerationHolderEx implements STHdrFtr
{
    public STHdrFtrImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STHdrFtrImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
