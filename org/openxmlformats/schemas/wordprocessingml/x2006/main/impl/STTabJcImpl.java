// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTabJc;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STTabJcImpl extends JavaStringEnumerationHolderEx implements STTabJc
{
    public STTabJcImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STTabJcImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
