// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STEighthPointMeasure;
import org.apache.xmlbeans.impl.values.JavaIntegerHolderEx;

public class STEighthPointMeasureImpl extends JavaIntegerHolderEx implements STEighthPointMeasure
{
    public STEighthPointMeasureImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STEighthPointMeasureImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
