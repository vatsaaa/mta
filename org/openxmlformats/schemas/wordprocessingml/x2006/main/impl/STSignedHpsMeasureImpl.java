// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STSignedHpsMeasure;
import org.apache.xmlbeans.impl.values.JavaIntegerHolderEx;

public class STSignedHpsMeasureImpl extends JavaIntegerHolderEx implements STSignedHpsMeasure
{
    public STSignedHpsMeasureImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STSignedHpsMeasureImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
