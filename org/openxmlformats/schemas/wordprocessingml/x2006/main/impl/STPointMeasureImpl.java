// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STPointMeasure;
import org.apache.xmlbeans.impl.values.JavaIntegerHolderEx;

public class STPointMeasureImpl extends JavaIntegerHolderEx implements STPointMeasure
{
    public STPointMeasureImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STPointMeasureImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
