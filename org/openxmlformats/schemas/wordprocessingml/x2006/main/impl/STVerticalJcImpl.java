// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STVerticalJcImpl extends JavaStringEnumerationHolderEx implements STVerticalJc
{
    public STVerticalJcImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STVerticalJcImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
