// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPrChange;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;

public class CTTcPrImpl extends CTTcPrInnerImpl implements CTTcPr
{
    private static final QName TCPRCHANGE$0;
    
    public CTTcPrImpl(final SchemaType schemaType) {
        super(schemaType);
    }
    
    public CTTcPrChange getTcPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTTcPrChange ctTcPrChange = (CTTcPrChange)this.get_store().find_element_user(CTTcPrImpl.TCPRCHANGE$0, 0);
            if (ctTcPrChange == null) {
                return null;
            }
            return ctTcPrChange;
        }
    }
    
    public boolean isSetTcPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTTcPrImpl.TCPRCHANGE$0) != 0;
        }
    }
    
    public void setTcPrChange(final CTTcPrChange ctTcPrChange) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTTcPrChange ctTcPrChange2 = (CTTcPrChange)this.get_store().find_element_user(CTTcPrImpl.TCPRCHANGE$0, 0);
            if (ctTcPrChange2 == null) {
                ctTcPrChange2 = (CTTcPrChange)this.get_store().add_element_user(CTTcPrImpl.TCPRCHANGE$0);
            }
            ctTcPrChange2.set((XmlObject)ctTcPrChange);
        }
    }
    
    public CTTcPrChange addNewTcPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTTcPrChange)this.get_store().add_element_user(CTTcPrImpl.TCPRCHANGE$0);
        }
    }
    
    public void unsetTcPrChange() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTTcPrImpl.TCPRCHANGE$0, 0);
        }
    }
    
    static {
        TCPRCHANGE$0 = new QName("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "tcPrChange");
    }
}
