// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPictureBase;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTPictureBaseImpl extends XmlComplexContentImpl implements CTPictureBase
{
    public CTPictureBaseImpl(final SchemaType type) {
        super(type);
    }
}
