// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STUnderline;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STUnderlineImpl extends JavaStringEnumerationHolderEx implements STUnderline
{
    public STUnderlineImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STUnderlineImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
