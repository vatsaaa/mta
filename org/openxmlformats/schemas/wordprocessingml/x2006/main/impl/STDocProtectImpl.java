// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STDocProtect;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class STDocProtectImpl extends JavaStringEnumerationHolderEx implements STDocProtect
{
    public STDocProtectImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STDocProtectImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
