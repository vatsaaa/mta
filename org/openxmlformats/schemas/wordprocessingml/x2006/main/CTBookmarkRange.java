// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.math.BigInteger;
import org.apache.xmlbeans.SchemaType;

public interface CTBookmarkRange extends CTMarkupRange
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTBookmarkRange.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctbookmarkranged88btype");
    
    BigInteger getColFirst();
    
    STDecimalNumber xgetColFirst();
    
    boolean isSetColFirst();
    
    void setColFirst(final BigInteger p0);
    
    void xsetColFirst(final STDecimalNumber p0);
    
    void unsetColFirst();
    
    BigInteger getColLast();
    
    STDecimalNumber xgetColLast();
    
    boolean isSetColLast();
    
    void setColLast(final BigInteger p0);
    
    void xsetColLast(final STDecimalNumber p0);
    
    void unsetColLast();
    
    public static final class Factory
    {
        public static CTBookmarkRange newInstance() {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().newInstance(CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange newInstance(final XmlOptions xmlOptions) {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().newInstance(CTBookmarkRange.type, xmlOptions);
        }
        
        public static CTBookmarkRange parse(final String s) throws XmlException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(s, CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(s, CTBookmarkRange.type, xmlOptions);
        }
        
        public static CTBookmarkRange parse(final File file) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(file, CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(file, CTBookmarkRange.type, xmlOptions);
        }
        
        public static CTBookmarkRange parse(final URL url) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(url, CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(url, CTBookmarkRange.type, xmlOptions);
        }
        
        public static CTBookmarkRange parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(inputStream, CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(inputStream, CTBookmarkRange.type, xmlOptions);
        }
        
        public static CTBookmarkRange parse(final Reader reader) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(reader, CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(reader, CTBookmarkRange.type, xmlOptions);
        }
        
        public static CTBookmarkRange parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTBookmarkRange.type, xmlOptions);
        }
        
        public static CTBookmarkRange parse(final Node node) throws XmlException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(node, CTBookmarkRange.type, null);
        }
        
        public static CTBookmarkRange parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(node, CTBookmarkRange.type, xmlOptions);
        }
        
        @Deprecated
        public static CTBookmarkRange parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBookmarkRange.type, null);
        }
        
        @Deprecated
        public static CTBookmarkRange parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTBookmarkRange)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTBookmarkRange.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBookmarkRange.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTBookmarkRange.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
