// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface CTHdrFtrRef extends CTRel
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTHdrFtrRef.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cthdrftrref224dtype");
    
    STHdrFtr.Enum getType();
    
    STHdrFtr xgetType();
    
    void setType(final STHdrFtr.Enum p0);
    
    void xsetType(final STHdrFtr p0);
    
    public static final class Factory
    {
        public static CTHdrFtrRef newInstance() {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().newInstance(CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef newInstance(final XmlOptions xmlOptions) {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().newInstance(CTHdrFtrRef.type, xmlOptions);
        }
        
        public static CTHdrFtrRef parse(final String s) throws XmlException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(s, CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(s, CTHdrFtrRef.type, xmlOptions);
        }
        
        public static CTHdrFtrRef parse(final File file) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(file, CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(file, CTHdrFtrRef.type, xmlOptions);
        }
        
        public static CTHdrFtrRef parse(final URL url) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(url, CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(url, CTHdrFtrRef.type, xmlOptions);
        }
        
        public static CTHdrFtrRef parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(inputStream, CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(inputStream, CTHdrFtrRef.type, xmlOptions);
        }
        
        public static CTHdrFtrRef parse(final Reader reader) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(reader, CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(reader, CTHdrFtrRef.type, xmlOptions);
        }
        
        public static CTHdrFtrRef parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTHdrFtrRef.type, xmlOptions);
        }
        
        public static CTHdrFtrRef parse(final Node node) throws XmlException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(node, CTHdrFtrRef.type, null);
        }
        
        public static CTHdrFtrRef parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(node, CTHdrFtrRef.type, xmlOptions);
        }
        
        @Deprecated
        public static CTHdrFtrRef parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTHdrFtrRef.type, null);
        }
        
        @Deprecated
        public static CTHdrFtrRef parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTHdrFtrRef)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTHdrFtrRef.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTHdrFtrRef.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTHdrFtrRef.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
