// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STString extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STString.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ststringa627type");
    
    public static final class Factory
    {
        public static STString newValue(final Object o) {
            return (STString)STString.type.newValue(o);
        }
        
        public static STString newInstance() {
            return (STString)XmlBeans.getContextTypeLoader().newInstance(STString.type, null);
        }
        
        public static STString newInstance(final XmlOptions xmlOptions) {
            return (STString)XmlBeans.getContextTypeLoader().newInstance(STString.type, xmlOptions);
        }
        
        public static STString parse(final String s) throws XmlException {
            return (STString)XmlBeans.getContextTypeLoader().parse(s, STString.type, null);
        }
        
        public static STString parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STString)XmlBeans.getContextTypeLoader().parse(s, STString.type, xmlOptions);
        }
        
        public static STString parse(final File file) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(file, STString.type, null);
        }
        
        public static STString parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(file, STString.type, xmlOptions);
        }
        
        public static STString parse(final URL url) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(url, STString.type, null);
        }
        
        public static STString parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(url, STString.type, xmlOptions);
        }
        
        public static STString parse(final InputStream inputStream) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(inputStream, STString.type, null);
        }
        
        public static STString parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(inputStream, STString.type, xmlOptions);
        }
        
        public static STString parse(final Reader reader) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(reader, STString.type, null);
        }
        
        public static STString parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STString)XmlBeans.getContextTypeLoader().parse(reader, STString.type, xmlOptions);
        }
        
        public static STString parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STString)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STString.type, null);
        }
        
        public static STString parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STString)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STString.type, xmlOptions);
        }
        
        public static STString parse(final Node node) throws XmlException {
            return (STString)XmlBeans.getContextTypeLoader().parse(node, STString.type, null);
        }
        
        public static STString parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STString)XmlBeans.getContextTypeLoader().parse(node, STString.type, xmlOptions);
        }
        
        @Deprecated
        public static STString parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STString)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STString.type, null);
        }
        
        @Deprecated
        public static STString parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STString)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STString.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STString.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STString.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
