// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STFldCharType extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STFldCharType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stfldchartype1eb4type");
    public static final Enum BEGIN = Enum.forString("begin");
    public static final Enum SEPARATE = Enum.forString("separate");
    public static final Enum END = Enum.forString("end");
    public static final int INT_BEGIN = 1;
    public static final int INT_SEPARATE = 2;
    public static final int INT_END = 3;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STFldCharType newValue(final Object o) {
            return (STFldCharType)STFldCharType.type.newValue(o);
        }
        
        public static STFldCharType newInstance() {
            return (STFldCharType)XmlBeans.getContextTypeLoader().newInstance(STFldCharType.type, null);
        }
        
        public static STFldCharType newInstance(final XmlOptions xmlOptions) {
            return (STFldCharType)XmlBeans.getContextTypeLoader().newInstance(STFldCharType.type, xmlOptions);
        }
        
        public static STFldCharType parse(final String s) throws XmlException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(s, STFldCharType.type, null);
        }
        
        public static STFldCharType parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(s, STFldCharType.type, xmlOptions);
        }
        
        public static STFldCharType parse(final File file) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(file, STFldCharType.type, null);
        }
        
        public static STFldCharType parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(file, STFldCharType.type, xmlOptions);
        }
        
        public static STFldCharType parse(final URL url) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(url, STFldCharType.type, null);
        }
        
        public static STFldCharType parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(url, STFldCharType.type, xmlOptions);
        }
        
        public static STFldCharType parse(final InputStream inputStream) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(inputStream, STFldCharType.type, null);
        }
        
        public static STFldCharType parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(inputStream, STFldCharType.type, xmlOptions);
        }
        
        public static STFldCharType parse(final Reader reader) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(reader, STFldCharType.type, null);
        }
        
        public static STFldCharType parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(reader, STFldCharType.type, xmlOptions);
        }
        
        public static STFldCharType parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFldCharType.type, null);
        }
        
        public static STFldCharType parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STFldCharType.type, xmlOptions);
        }
        
        public static STFldCharType parse(final Node node) throws XmlException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(node, STFldCharType.type, null);
        }
        
        public static STFldCharType parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(node, STFldCharType.type, xmlOptions);
        }
        
        @Deprecated
        public static STFldCharType parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFldCharType.type, null);
        }
        
        @Deprecated
        public static STFldCharType parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STFldCharType)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STFldCharType.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFldCharType.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STFldCharType.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_BEGIN = 1;
        static final int INT_SEPARATE = 2;
        static final int INT_END = 3;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("begin", 1), new Enum("separate", 2), new Enum("end", 3) });
        }
    }
}
