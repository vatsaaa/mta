// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.math.BigInteger;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTMarkup extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTMarkup.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctmarkup2d80type");
    
    BigInteger getId();
    
    STDecimalNumber xgetId();
    
    void setId(final BigInteger p0);
    
    void xsetId(final STDecimalNumber p0);
    
    public static final class Factory
    {
        public static CTMarkup newInstance() {
            return (CTMarkup)XmlBeans.getContextTypeLoader().newInstance(CTMarkup.type, null);
        }
        
        public static CTMarkup newInstance(final XmlOptions xmlOptions) {
            return (CTMarkup)XmlBeans.getContextTypeLoader().newInstance(CTMarkup.type, xmlOptions);
        }
        
        public static CTMarkup parse(final String s) throws XmlException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(s, CTMarkup.type, null);
        }
        
        public static CTMarkup parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(s, CTMarkup.type, xmlOptions);
        }
        
        public static CTMarkup parse(final File file) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(file, CTMarkup.type, null);
        }
        
        public static CTMarkup parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(file, CTMarkup.type, xmlOptions);
        }
        
        public static CTMarkup parse(final URL url) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(url, CTMarkup.type, null);
        }
        
        public static CTMarkup parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(url, CTMarkup.type, xmlOptions);
        }
        
        public static CTMarkup parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(inputStream, CTMarkup.type, null);
        }
        
        public static CTMarkup parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(inputStream, CTMarkup.type, xmlOptions);
        }
        
        public static CTMarkup parse(final Reader reader) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(reader, CTMarkup.type, null);
        }
        
        public static CTMarkup parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(reader, CTMarkup.type, xmlOptions);
        }
        
        public static CTMarkup parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMarkup.type, null);
        }
        
        public static CTMarkup parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMarkup.type, xmlOptions);
        }
        
        public static CTMarkup parse(final Node node) throws XmlException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(node, CTMarkup.type, null);
        }
        
        public static CTMarkup parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(node, CTMarkup.type, xmlOptions);
        }
        
        @Deprecated
        public static CTMarkup parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMarkup.type, null);
        }
        
        @Deprecated
        public static CTMarkup parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTMarkup)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMarkup.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMarkup.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMarkup.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
