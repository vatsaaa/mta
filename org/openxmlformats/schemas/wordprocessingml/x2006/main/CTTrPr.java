// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface CTTrPr extends CTTrPrBase
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTrPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttrpr2848type");
    
    CTTrackChange getIns();
    
    boolean isSetIns();
    
    void setIns(final CTTrackChange p0);
    
    CTTrackChange addNewIns();
    
    void unsetIns();
    
    CTTrackChange getDel();
    
    boolean isSetDel();
    
    void setDel(final CTTrackChange p0);
    
    CTTrackChange addNewDel();
    
    void unsetDel();
    
    CTTrPrChange getTrPrChange();
    
    boolean isSetTrPrChange();
    
    void setTrPrChange(final CTTrPrChange p0);
    
    CTTrPrChange addNewTrPrChange();
    
    void unsetTrPrChange();
    
    public static final class Factory
    {
        public static CTTrPr newInstance() {
            return (CTTrPr)XmlBeans.getContextTypeLoader().newInstance(CTTrPr.type, null);
        }
        
        public static CTTrPr newInstance(final XmlOptions xmlOptions) {
            return (CTTrPr)XmlBeans.getContextTypeLoader().newInstance(CTTrPr.type, xmlOptions);
        }
        
        public static CTTrPr parse(final String s) throws XmlException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(s, CTTrPr.type, null);
        }
        
        public static CTTrPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(s, CTTrPr.type, xmlOptions);
        }
        
        public static CTTrPr parse(final File file) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(file, CTTrPr.type, null);
        }
        
        public static CTTrPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(file, CTTrPr.type, xmlOptions);
        }
        
        public static CTTrPr parse(final URL url) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(url, CTTrPr.type, null);
        }
        
        public static CTTrPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(url, CTTrPr.type, xmlOptions);
        }
        
        public static CTTrPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTTrPr.type, null);
        }
        
        public static CTTrPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTTrPr.type, xmlOptions);
        }
        
        public static CTTrPr parse(final Reader reader) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(reader, CTTrPr.type, null);
        }
        
        public static CTTrPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(reader, CTTrPr.type, xmlOptions);
        }
        
        public static CTTrPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTrPr.type, null);
        }
        
        public static CTTrPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTrPr.type, xmlOptions);
        }
        
        public static CTTrPr parse(final Node node) throws XmlException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(node, CTTrPr.type, null);
        }
        
        public static CTTrPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(node, CTTrPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTrPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTrPr.type, null);
        }
        
        @Deprecated
        public static CTTrPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTrPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTrPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTrPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTrPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
