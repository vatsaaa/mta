// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STPointMeasure extends STUnsignedDecimalNumber
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STPointMeasure.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stpointmeasurea96atype");
    
    public static final class Factory
    {
        public static STPointMeasure newValue(final Object o) {
            return (STPointMeasure)STPointMeasure.type.newValue(o);
        }
        
        public static STPointMeasure newInstance() {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().newInstance(STPointMeasure.type, null);
        }
        
        public static STPointMeasure newInstance(final XmlOptions xmlOptions) {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().newInstance(STPointMeasure.type, xmlOptions);
        }
        
        public static STPointMeasure parse(final String s) throws XmlException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(s, STPointMeasure.type, null);
        }
        
        public static STPointMeasure parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(s, STPointMeasure.type, xmlOptions);
        }
        
        public static STPointMeasure parse(final File file) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(file, STPointMeasure.type, null);
        }
        
        public static STPointMeasure parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(file, STPointMeasure.type, xmlOptions);
        }
        
        public static STPointMeasure parse(final URL url) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(url, STPointMeasure.type, null);
        }
        
        public static STPointMeasure parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(url, STPointMeasure.type, xmlOptions);
        }
        
        public static STPointMeasure parse(final InputStream inputStream) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STPointMeasure.type, null);
        }
        
        public static STPointMeasure parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STPointMeasure.type, xmlOptions);
        }
        
        public static STPointMeasure parse(final Reader reader) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(reader, STPointMeasure.type, null);
        }
        
        public static STPointMeasure parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(reader, STPointMeasure.type, xmlOptions);
        }
        
        public static STPointMeasure parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPointMeasure.type, null);
        }
        
        public static STPointMeasure parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STPointMeasure.type, xmlOptions);
        }
        
        public static STPointMeasure parse(final Node node) throws XmlException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(node, STPointMeasure.type, null);
        }
        
        public static STPointMeasure parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(node, STPointMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static STPointMeasure parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPointMeasure.type, null);
        }
        
        @Deprecated
        public static STPointMeasure parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STPointMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPointMeasure.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STPointMeasure.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
