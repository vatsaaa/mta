// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTNumbering extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTNumbering.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctnumberingfdf9type");
    
    List<CTNumPicBullet> getNumPicBulletList();
    
    @Deprecated
    CTNumPicBullet[] getNumPicBulletArray();
    
    CTNumPicBullet getNumPicBulletArray(final int p0);
    
    int sizeOfNumPicBulletArray();
    
    void setNumPicBulletArray(final CTNumPicBullet[] p0);
    
    void setNumPicBulletArray(final int p0, final CTNumPicBullet p1);
    
    CTNumPicBullet insertNewNumPicBullet(final int p0);
    
    CTNumPicBullet addNewNumPicBullet();
    
    void removeNumPicBullet(final int p0);
    
    List<CTAbstractNum> getAbstractNumList();
    
    @Deprecated
    CTAbstractNum[] getAbstractNumArray();
    
    CTAbstractNum getAbstractNumArray(final int p0);
    
    int sizeOfAbstractNumArray();
    
    void setAbstractNumArray(final CTAbstractNum[] p0);
    
    void setAbstractNumArray(final int p0, final CTAbstractNum p1);
    
    CTAbstractNum insertNewAbstractNum(final int p0);
    
    CTAbstractNum addNewAbstractNum();
    
    void removeAbstractNum(final int p0);
    
    List<CTNum> getNumList();
    
    @Deprecated
    CTNum[] getNumArray();
    
    CTNum getNumArray(final int p0);
    
    int sizeOfNumArray();
    
    void setNumArray(final CTNum[] p0);
    
    void setNumArray(final int p0, final CTNum p1);
    
    CTNum insertNewNum(final int p0);
    
    CTNum addNewNum();
    
    void removeNum(final int p0);
    
    CTDecimalNumber getNumIdMacAtCleanup();
    
    boolean isSetNumIdMacAtCleanup();
    
    void setNumIdMacAtCleanup(final CTDecimalNumber p0);
    
    CTDecimalNumber addNewNumIdMacAtCleanup();
    
    void unsetNumIdMacAtCleanup();
    
    public static final class Factory
    {
        public static CTNumbering newInstance() {
            return (CTNumbering)XmlBeans.getContextTypeLoader().newInstance(CTNumbering.type, null);
        }
        
        public static CTNumbering newInstance(final XmlOptions xmlOptions) {
            return (CTNumbering)XmlBeans.getContextTypeLoader().newInstance(CTNumbering.type, xmlOptions);
        }
        
        public static CTNumbering parse(final String s) throws XmlException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(s, CTNumbering.type, null);
        }
        
        public static CTNumbering parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(s, CTNumbering.type, xmlOptions);
        }
        
        public static CTNumbering parse(final File file) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(file, CTNumbering.type, null);
        }
        
        public static CTNumbering parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(file, CTNumbering.type, xmlOptions);
        }
        
        public static CTNumbering parse(final URL url) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(url, CTNumbering.type, null);
        }
        
        public static CTNumbering parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(url, CTNumbering.type, xmlOptions);
        }
        
        public static CTNumbering parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumbering.type, null);
        }
        
        public static CTNumbering parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(inputStream, CTNumbering.type, xmlOptions);
        }
        
        public static CTNumbering parse(final Reader reader) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(reader, CTNumbering.type, null);
        }
        
        public static CTNumbering parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(reader, CTNumbering.type, xmlOptions);
        }
        
        public static CTNumbering parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumbering.type, null);
        }
        
        public static CTNumbering parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTNumbering.type, xmlOptions);
        }
        
        public static CTNumbering parse(final Node node) throws XmlException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(node, CTNumbering.type, null);
        }
        
        public static CTNumbering parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(node, CTNumbering.type, xmlOptions);
        }
        
        @Deprecated
        public static CTNumbering parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumbering.type, null);
        }
        
        @Deprecated
        public static CTNumbering parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTNumbering)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTNumbering.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumbering.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTNumbering.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
