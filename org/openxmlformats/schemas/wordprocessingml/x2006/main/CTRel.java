// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTRel extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTRel.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctrel4519type");
    
    String getId();
    
    STRelationshipId xgetId();
    
    void setId(final String p0);
    
    void xsetId(final STRelationshipId p0);
    
    public static final class Factory
    {
        public static CTRel newInstance() {
            return (CTRel)XmlBeans.getContextTypeLoader().newInstance(CTRel.type, null);
        }
        
        public static CTRel newInstance(final XmlOptions xmlOptions) {
            return (CTRel)XmlBeans.getContextTypeLoader().newInstance(CTRel.type, xmlOptions);
        }
        
        public static CTRel parse(final String s) throws XmlException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(s, CTRel.type, null);
        }
        
        public static CTRel parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(s, CTRel.type, xmlOptions);
        }
        
        public static CTRel parse(final File file) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(file, CTRel.type, null);
        }
        
        public static CTRel parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(file, CTRel.type, xmlOptions);
        }
        
        public static CTRel parse(final URL url) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(url, CTRel.type, null);
        }
        
        public static CTRel parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(url, CTRel.type, xmlOptions);
        }
        
        public static CTRel parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(inputStream, CTRel.type, null);
        }
        
        public static CTRel parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(inputStream, CTRel.type, xmlOptions);
        }
        
        public static CTRel parse(final Reader reader) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(reader, CTRel.type, null);
        }
        
        public static CTRel parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(reader, CTRel.type, xmlOptions);
        }
        
        public static CTRel parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTRel.type, null);
        }
        
        public static CTRel parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTRel.type, xmlOptions);
        }
        
        public static CTRel parse(final Node node) throws XmlException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(node, CTRel.type, null);
        }
        
        public static CTRel parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(node, CTRel.type, xmlOptions);
        }
        
        @Deprecated
        public static CTRel parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTRel.type, null);
        }
        
        @Deprecated
        public static CTRel parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTRel)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTRel.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTRel.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTRel.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
