// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPictureBase extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPictureBase.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpicturebase5f83type");
    
    public static final class Factory
    {
        public static CTPictureBase newInstance() {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().newInstance(CTPictureBase.type, null);
        }
        
        public static CTPictureBase newInstance(final XmlOptions xmlOptions) {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().newInstance(CTPictureBase.type, xmlOptions);
        }
        
        public static CTPictureBase parse(final String s) throws XmlException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(s, CTPictureBase.type, null);
        }
        
        public static CTPictureBase parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(s, CTPictureBase.type, xmlOptions);
        }
        
        public static CTPictureBase parse(final File file) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(file, CTPictureBase.type, null);
        }
        
        public static CTPictureBase parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(file, CTPictureBase.type, xmlOptions);
        }
        
        public static CTPictureBase parse(final URL url) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(url, CTPictureBase.type, null);
        }
        
        public static CTPictureBase parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(url, CTPictureBase.type, xmlOptions);
        }
        
        public static CTPictureBase parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTPictureBase.type, null);
        }
        
        public static CTPictureBase parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTPictureBase.type, xmlOptions);
        }
        
        public static CTPictureBase parse(final Reader reader) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(reader, CTPictureBase.type, null);
        }
        
        public static CTPictureBase parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(reader, CTPictureBase.type, xmlOptions);
        }
        
        public static CTPictureBase parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPictureBase.type, null);
        }
        
        public static CTPictureBase parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPictureBase.type, xmlOptions);
        }
        
        public static CTPictureBase parse(final Node node) throws XmlException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(node, CTPictureBase.type, null);
        }
        
        public static CTPictureBase parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(node, CTPictureBase.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPictureBase parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPictureBase.type, null);
        }
        
        @Deprecated
        public static CTPictureBase parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPictureBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPictureBase.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPictureBase.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPictureBase.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
