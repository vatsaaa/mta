// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface EndnotesDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(EndnotesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("endnotes960edoctype");
    
    CTEndnotes getEndnotes();
    
    void setEndnotes(final CTEndnotes p0);
    
    CTEndnotes addNewEndnotes();
    
    public static final class Factory
    {
        public static EndnotesDocument newInstance() {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().newInstance(EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument newInstance(final XmlOptions xmlOptions) {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().newInstance(EndnotesDocument.type, xmlOptions);
        }
        
        public static EndnotesDocument parse(final String s) throws XmlException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(s, EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(s, EndnotesDocument.type, xmlOptions);
        }
        
        public static EndnotesDocument parse(final File file) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(file, EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(file, EndnotesDocument.type, xmlOptions);
        }
        
        public static EndnotesDocument parse(final URL url) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(url, EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(url, EndnotesDocument.type, xmlOptions);
        }
        
        public static EndnotesDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, EndnotesDocument.type, xmlOptions);
        }
        
        public static EndnotesDocument parse(final Reader reader) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(reader, EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(reader, EndnotesDocument.type, xmlOptions);
        }
        
        public static EndnotesDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, EndnotesDocument.type, xmlOptions);
        }
        
        public static EndnotesDocument parse(final Node node) throws XmlException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(node, EndnotesDocument.type, null);
        }
        
        public static EndnotesDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(node, EndnotesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static EndnotesDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, EndnotesDocument.type, null);
        }
        
        @Deprecated
        public static EndnotesDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (EndnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, EndnotesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, EndnotesDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, EndnotesDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
