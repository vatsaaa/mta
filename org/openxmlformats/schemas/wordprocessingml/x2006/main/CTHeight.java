// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.math.BigInteger;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTHeight extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTHeight.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctheighta2e1type");
    
    BigInteger getVal();
    
    STTwipsMeasure xgetVal();
    
    boolean isSetVal();
    
    void setVal(final BigInteger p0);
    
    void xsetVal(final STTwipsMeasure p0);
    
    void unsetVal();
    
    STHeightRule.Enum getHRule();
    
    STHeightRule xgetHRule();
    
    boolean isSetHRule();
    
    void setHRule(final STHeightRule.Enum p0);
    
    void xsetHRule(final STHeightRule p0);
    
    void unsetHRule();
    
    public static final class Factory
    {
        public static CTHeight newInstance() {
            return (CTHeight)XmlBeans.getContextTypeLoader().newInstance(CTHeight.type, null);
        }
        
        public static CTHeight newInstance(final XmlOptions xmlOptions) {
            return (CTHeight)XmlBeans.getContextTypeLoader().newInstance(CTHeight.type, xmlOptions);
        }
        
        public static CTHeight parse(final String s) throws XmlException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(s, CTHeight.type, null);
        }
        
        public static CTHeight parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(s, CTHeight.type, xmlOptions);
        }
        
        public static CTHeight parse(final File file) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(file, CTHeight.type, null);
        }
        
        public static CTHeight parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(file, CTHeight.type, xmlOptions);
        }
        
        public static CTHeight parse(final URL url) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(url, CTHeight.type, null);
        }
        
        public static CTHeight parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(url, CTHeight.type, xmlOptions);
        }
        
        public static CTHeight parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(inputStream, CTHeight.type, null);
        }
        
        public static CTHeight parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(inputStream, CTHeight.type, xmlOptions);
        }
        
        public static CTHeight parse(final Reader reader) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(reader, CTHeight.type, null);
        }
        
        public static CTHeight parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(reader, CTHeight.type, xmlOptions);
        }
        
        public static CTHeight parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTHeight.type, null);
        }
        
        public static CTHeight parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTHeight.type, xmlOptions);
        }
        
        public static CTHeight parse(final Node node) throws XmlException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(node, CTHeight.type, null);
        }
        
        public static CTHeight parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(node, CTHeight.type, xmlOptions);
        }
        
        @Deprecated
        public static CTHeight parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTHeight.type, null);
        }
        
        @Deprecated
        public static CTHeight parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTHeight)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTHeight.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTHeight.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTHeight.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
