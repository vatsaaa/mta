// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface StylesDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(StylesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("styles2732doctype");
    
    CTStyles getStyles();
    
    void setStyles(final CTStyles p0);
    
    CTStyles addNewStyles();
    
    public static final class Factory
    {
        public static StylesDocument newInstance() {
            return (StylesDocument)XmlBeans.getContextTypeLoader().newInstance(StylesDocument.type, null);
        }
        
        public static StylesDocument newInstance(final XmlOptions xmlOptions) {
            return (StylesDocument)XmlBeans.getContextTypeLoader().newInstance(StylesDocument.type, xmlOptions);
        }
        
        public static StylesDocument parse(final String s) throws XmlException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(s, StylesDocument.type, null);
        }
        
        public static StylesDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(s, StylesDocument.type, xmlOptions);
        }
        
        public static StylesDocument parse(final File file) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(file, StylesDocument.type, null);
        }
        
        public static StylesDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(file, StylesDocument.type, xmlOptions);
        }
        
        public static StylesDocument parse(final URL url) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(url, StylesDocument.type, null);
        }
        
        public static StylesDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(url, StylesDocument.type, xmlOptions);
        }
        
        public static StylesDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, StylesDocument.type, null);
        }
        
        public static StylesDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, StylesDocument.type, xmlOptions);
        }
        
        public static StylesDocument parse(final Reader reader) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(reader, StylesDocument.type, null);
        }
        
        public static StylesDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(reader, StylesDocument.type, xmlOptions);
        }
        
        public static StylesDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, StylesDocument.type, null);
        }
        
        public static StylesDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, StylesDocument.type, xmlOptions);
        }
        
        public static StylesDocument parse(final Node node) throws XmlException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(node, StylesDocument.type, null);
        }
        
        public static StylesDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(node, StylesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static StylesDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, StylesDocument.type, null);
        }
        
        @Deprecated
        public static StylesDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (StylesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, StylesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, StylesDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, StylesDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
