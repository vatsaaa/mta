// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlHexBinary;

public interface STHexColorRGB extends XmlHexBinary
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STHexColorRGB.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sthexcolorrgbd59dtype");
    
    public static final class Factory
    {
        public static STHexColorRGB newValue(final Object o) {
            return (STHexColorRGB)STHexColorRGB.type.newValue(o);
        }
        
        public static STHexColorRGB newInstance() {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().newInstance(STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB newInstance(final XmlOptions xmlOptions) {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().newInstance(STHexColorRGB.type, xmlOptions);
        }
        
        public static STHexColorRGB parse(final String s) throws XmlException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(s, STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(s, STHexColorRGB.type, xmlOptions);
        }
        
        public static STHexColorRGB parse(final File file) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(file, STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(file, STHexColorRGB.type, xmlOptions);
        }
        
        public static STHexColorRGB parse(final URL url) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(url, STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(url, STHexColorRGB.type, xmlOptions);
        }
        
        public static STHexColorRGB parse(final InputStream inputStream) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(inputStream, STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(inputStream, STHexColorRGB.type, xmlOptions);
        }
        
        public static STHexColorRGB parse(final Reader reader) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(reader, STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(reader, STHexColorRGB.type, xmlOptions);
        }
        
        public static STHexColorRGB parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STHexColorRGB.type, xmlOptions);
        }
        
        public static STHexColorRGB parse(final Node node) throws XmlException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(node, STHexColorRGB.type, null);
        }
        
        public static STHexColorRGB parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(node, STHexColorRGB.type, xmlOptions);
        }
        
        @Deprecated
        public static STHexColorRGB parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STHexColorRGB.type, null);
        }
        
        @Deprecated
        public static STHexColorRGB parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STHexColorRGB)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STHexColorRGB.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STHexColorRGB.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STHexColorRGB.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
