// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.Calendar;
import org.apache.xmlbeans.SchemaType;

public interface CTTrackChange extends CTMarkup
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTrackChange.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttrackchangec317type");
    
    String getAuthor();
    
    STString xgetAuthor();
    
    void setAuthor(final String p0);
    
    void xsetAuthor(final STString p0);
    
    Calendar getDate();
    
    STDateTime xgetDate();
    
    boolean isSetDate();
    
    void setDate(final Calendar p0);
    
    void xsetDate(final STDateTime p0);
    
    void unsetDate();
    
    public static final class Factory
    {
        public static CTTrackChange newInstance() {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().newInstance(CTTrackChange.type, null);
        }
        
        public static CTTrackChange newInstance(final XmlOptions xmlOptions) {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().newInstance(CTTrackChange.type, xmlOptions);
        }
        
        public static CTTrackChange parse(final String s) throws XmlException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(s, CTTrackChange.type, null);
        }
        
        public static CTTrackChange parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(s, CTTrackChange.type, xmlOptions);
        }
        
        public static CTTrackChange parse(final File file) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(file, CTTrackChange.type, null);
        }
        
        public static CTTrackChange parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(file, CTTrackChange.type, xmlOptions);
        }
        
        public static CTTrackChange parse(final URL url) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(url, CTTrackChange.type, null);
        }
        
        public static CTTrackChange parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(url, CTTrackChange.type, xmlOptions);
        }
        
        public static CTTrackChange parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(inputStream, CTTrackChange.type, null);
        }
        
        public static CTTrackChange parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(inputStream, CTTrackChange.type, xmlOptions);
        }
        
        public static CTTrackChange parse(final Reader reader) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(reader, CTTrackChange.type, null);
        }
        
        public static CTTrackChange parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(reader, CTTrackChange.type, xmlOptions);
        }
        
        public static CTTrackChange parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTrackChange.type, null);
        }
        
        public static CTTrackChange parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTrackChange.type, xmlOptions);
        }
        
        public static CTTrackChange parse(final Node node) throws XmlException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(node, CTTrackChange.type, null);
        }
        
        public static CTTrackChange parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(node, CTTrackChange.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTrackChange parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTrackChange.type, null);
        }
        
        @Deprecated
        public static CTTrackChange parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTrackChange)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTrackChange.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTrackChange.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTrackChange.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
