// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTDocumentBase extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTDocumentBase.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctdocumentbasedf5ctype");
    
    CTBackground getBackground();
    
    boolean isSetBackground();
    
    void setBackground(final CTBackground p0);
    
    CTBackground addNewBackground();
    
    void unsetBackground();
    
    public static final class Factory
    {
        public static CTDocumentBase newInstance() {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().newInstance(CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase newInstance(final XmlOptions xmlOptions) {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().newInstance(CTDocumentBase.type, xmlOptions);
        }
        
        public static CTDocumentBase parse(final String s) throws XmlException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(s, CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(s, CTDocumentBase.type, xmlOptions);
        }
        
        public static CTDocumentBase parse(final File file) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(file, CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(file, CTDocumentBase.type, xmlOptions);
        }
        
        public static CTDocumentBase parse(final URL url) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(url, CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(url, CTDocumentBase.type, xmlOptions);
        }
        
        public static CTDocumentBase parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTDocumentBase.type, xmlOptions);
        }
        
        public static CTDocumentBase parse(final Reader reader) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(reader, CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(reader, CTDocumentBase.type, xmlOptions);
        }
        
        public static CTDocumentBase parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDocumentBase.type, xmlOptions);
        }
        
        public static CTDocumentBase parse(final Node node) throws XmlException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(node, CTDocumentBase.type, null);
        }
        
        public static CTDocumentBase parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(node, CTDocumentBase.type, xmlOptions);
        }
        
        @Deprecated
        public static CTDocumentBase parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDocumentBase.type, null);
        }
        
        @Deprecated
        public static CTDocumentBase parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTDocumentBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDocumentBase.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDocumentBase.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDocumentBase.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
