// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface CTPPr extends CTPPrBase
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctppr01c0type");
    
    CTParaRPr getRPr();
    
    boolean isSetRPr();
    
    void setRPr(final CTParaRPr p0);
    
    CTParaRPr addNewRPr();
    
    void unsetRPr();
    
    CTSectPr getSectPr();
    
    boolean isSetSectPr();
    
    void setSectPr(final CTSectPr p0);
    
    CTSectPr addNewSectPr();
    
    void unsetSectPr();
    
    CTPPrChange getPPrChange();
    
    boolean isSetPPrChange();
    
    void setPPrChange(final CTPPrChange p0);
    
    CTPPrChange addNewPPrChange();
    
    void unsetPPrChange();
    
    public static final class Factory
    {
        public static CTPPr newInstance() {
            return (CTPPr)XmlBeans.getContextTypeLoader().newInstance(CTPPr.type, null);
        }
        
        public static CTPPr newInstance(final XmlOptions xmlOptions) {
            return (CTPPr)XmlBeans.getContextTypeLoader().newInstance(CTPPr.type, xmlOptions);
        }
        
        public static CTPPr parse(final String s) throws XmlException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(s, CTPPr.type, null);
        }
        
        public static CTPPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(s, CTPPr.type, xmlOptions);
        }
        
        public static CTPPr parse(final File file) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(file, CTPPr.type, null);
        }
        
        public static CTPPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(file, CTPPr.type, xmlOptions);
        }
        
        public static CTPPr parse(final URL url) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(url, CTPPr.type, null);
        }
        
        public static CTPPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(url, CTPPr.type, xmlOptions);
        }
        
        public static CTPPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTPPr.type, null);
        }
        
        public static CTPPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTPPr.type, xmlOptions);
        }
        
        public static CTPPr parse(final Reader reader) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(reader, CTPPr.type, null);
        }
        
        public static CTPPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(reader, CTPPr.type, xmlOptions);
        }
        
        public static CTPPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPPr.type, null);
        }
        
        public static CTPPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPPr.type, xmlOptions);
        }
        
        public static CTPPr parse(final Node node) throws XmlException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(node, CTPPr.type, null);
        }
        
        public static CTPPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(node, CTPPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPPr.type, null);
        }
        
        @Deprecated
        public static CTPPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
