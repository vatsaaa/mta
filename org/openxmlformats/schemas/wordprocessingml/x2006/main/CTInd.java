// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.math.BigInteger;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTInd extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTInd.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctind4b93type");
    
    BigInteger getLeft();
    
    STSignedTwipsMeasure xgetLeft();
    
    boolean isSetLeft();
    
    void setLeft(final BigInteger p0);
    
    void xsetLeft(final STSignedTwipsMeasure p0);
    
    void unsetLeft();
    
    BigInteger getLeftChars();
    
    STDecimalNumber xgetLeftChars();
    
    boolean isSetLeftChars();
    
    void setLeftChars(final BigInteger p0);
    
    void xsetLeftChars(final STDecimalNumber p0);
    
    void unsetLeftChars();
    
    BigInteger getRight();
    
    STSignedTwipsMeasure xgetRight();
    
    boolean isSetRight();
    
    void setRight(final BigInteger p0);
    
    void xsetRight(final STSignedTwipsMeasure p0);
    
    void unsetRight();
    
    BigInteger getRightChars();
    
    STDecimalNumber xgetRightChars();
    
    boolean isSetRightChars();
    
    void setRightChars(final BigInteger p0);
    
    void xsetRightChars(final STDecimalNumber p0);
    
    void unsetRightChars();
    
    BigInteger getHanging();
    
    STTwipsMeasure xgetHanging();
    
    boolean isSetHanging();
    
    void setHanging(final BigInteger p0);
    
    void xsetHanging(final STTwipsMeasure p0);
    
    void unsetHanging();
    
    BigInteger getHangingChars();
    
    STDecimalNumber xgetHangingChars();
    
    boolean isSetHangingChars();
    
    void setHangingChars(final BigInteger p0);
    
    void xsetHangingChars(final STDecimalNumber p0);
    
    void unsetHangingChars();
    
    BigInteger getFirstLine();
    
    STTwipsMeasure xgetFirstLine();
    
    boolean isSetFirstLine();
    
    void setFirstLine(final BigInteger p0);
    
    void xsetFirstLine(final STTwipsMeasure p0);
    
    void unsetFirstLine();
    
    BigInteger getFirstLineChars();
    
    STDecimalNumber xgetFirstLineChars();
    
    boolean isSetFirstLineChars();
    
    void setFirstLineChars(final BigInteger p0);
    
    void xsetFirstLineChars(final STDecimalNumber p0);
    
    void unsetFirstLineChars();
    
    public static final class Factory
    {
        public static CTInd newInstance() {
            return (CTInd)XmlBeans.getContextTypeLoader().newInstance(CTInd.type, null);
        }
        
        public static CTInd newInstance(final XmlOptions xmlOptions) {
            return (CTInd)XmlBeans.getContextTypeLoader().newInstance(CTInd.type, xmlOptions);
        }
        
        public static CTInd parse(final String s) throws XmlException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(s, CTInd.type, null);
        }
        
        public static CTInd parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(s, CTInd.type, xmlOptions);
        }
        
        public static CTInd parse(final File file) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(file, CTInd.type, null);
        }
        
        public static CTInd parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(file, CTInd.type, xmlOptions);
        }
        
        public static CTInd parse(final URL url) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(url, CTInd.type, null);
        }
        
        public static CTInd parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(url, CTInd.type, xmlOptions);
        }
        
        public static CTInd parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(inputStream, CTInd.type, null);
        }
        
        public static CTInd parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(inputStream, CTInd.type, xmlOptions);
        }
        
        public static CTInd parse(final Reader reader) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(reader, CTInd.type, null);
        }
        
        public static CTInd parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(reader, CTInd.type, xmlOptions);
        }
        
        public static CTInd parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTInd.type, null);
        }
        
        public static CTInd parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTInd.type, xmlOptions);
        }
        
        public static CTInd parse(final Node node) throws XmlException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(node, CTInd.type, null);
        }
        
        public static CTInd parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(node, CTInd.type, xmlOptions);
        }
        
        @Deprecated
        public static CTInd parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTInd.type, null);
        }
        
        @Deprecated
        public static CTInd parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTInd)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTInd.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTInd.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTInd.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
