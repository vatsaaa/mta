// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTJc extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTJc.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctjc158ftype");
    
    STJc.Enum getVal();
    
    STJc xgetVal();
    
    void setVal(final STJc.Enum p0);
    
    void xsetVal(final STJc p0);
    
    public static final class Factory
    {
        public static CTJc newInstance() {
            return (CTJc)XmlBeans.getContextTypeLoader().newInstance(CTJc.type, null);
        }
        
        public static CTJc newInstance(final XmlOptions xmlOptions) {
            return (CTJc)XmlBeans.getContextTypeLoader().newInstance(CTJc.type, xmlOptions);
        }
        
        public static CTJc parse(final String s) throws XmlException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(s, CTJc.type, null);
        }
        
        public static CTJc parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(s, CTJc.type, xmlOptions);
        }
        
        public static CTJc parse(final File file) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(file, CTJc.type, null);
        }
        
        public static CTJc parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(file, CTJc.type, xmlOptions);
        }
        
        public static CTJc parse(final URL url) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(url, CTJc.type, null);
        }
        
        public static CTJc parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(url, CTJc.type, xmlOptions);
        }
        
        public static CTJc parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(inputStream, CTJc.type, null);
        }
        
        public static CTJc parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(inputStream, CTJc.type, xmlOptions);
        }
        
        public static CTJc parse(final Reader reader) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(reader, CTJc.type, null);
        }
        
        public static CTJc parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(reader, CTJc.type, xmlOptions);
        }
        
        public static CTJc parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTJc.type, null);
        }
        
        public static CTJc parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTJc.type, xmlOptions);
        }
        
        public static CTJc parse(final Node node) throws XmlException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(node, CTJc.type, null);
        }
        
        public static CTJc parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(node, CTJc.type, xmlOptions);
        }
        
        @Deprecated
        public static CTJc parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTJc.type, null);
        }
        
        @Deprecated
        public static CTJc parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTJc)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTJc.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTJc.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTJc.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
