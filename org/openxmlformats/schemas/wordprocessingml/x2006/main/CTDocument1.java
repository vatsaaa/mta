// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface CTDocument1 extends CTDocumentBase
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTDocument1.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctdocument64adtype");
    
    CTBody getBody();
    
    boolean isSetBody();
    
    void setBody(final CTBody p0);
    
    CTBody addNewBody();
    
    void unsetBody();
    
    public static final class Factory
    {
        public static CTDocument1 newInstance() {
            return (CTDocument1)XmlBeans.getContextTypeLoader().newInstance(CTDocument1.type, null);
        }
        
        public static CTDocument1 newInstance(final XmlOptions xmlOptions) {
            return (CTDocument1)XmlBeans.getContextTypeLoader().newInstance(CTDocument1.type, xmlOptions);
        }
        
        public static CTDocument1 parse(final String s) throws XmlException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(s, CTDocument1.type, null);
        }
        
        public static CTDocument1 parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(s, CTDocument1.type, xmlOptions);
        }
        
        public static CTDocument1 parse(final File file) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(file, CTDocument1.type, null);
        }
        
        public static CTDocument1 parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(file, CTDocument1.type, xmlOptions);
        }
        
        public static CTDocument1 parse(final URL url) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(url, CTDocument1.type, null);
        }
        
        public static CTDocument1 parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(url, CTDocument1.type, xmlOptions);
        }
        
        public static CTDocument1 parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(inputStream, CTDocument1.type, null);
        }
        
        public static CTDocument1 parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(inputStream, CTDocument1.type, xmlOptions);
        }
        
        public static CTDocument1 parse(final Reader reader) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(reader, CTDocument1.type, null);
        }
        
        public static CTDocument1 parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(reader, CTDocument1.type, xmlOptions);
        }
        
        public static CTDocument1 parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDocument1.type, null);
        }
        
        public static CTDocument1 parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDocument1.type, xmlOptions);
        }
        
        public static CTDocument1 parse(final Node node) throws XmlException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(node, CTDocument1.type, null);
        }
        
        public static CTDocument1 parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(node, CTDocument1.type, xmlOptions);
        }
        
        @Deprecated
        public static CTDocument1 parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDocument1.type, null);
        }
        
        @Deprecated
        public static CTDocument1 parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTDocument1)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDocument1.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDocument1.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDocument1.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
