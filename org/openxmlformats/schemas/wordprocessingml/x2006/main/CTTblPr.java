// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface CTTblPr extends CTTblPrBase
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTblPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttblpr5b72type");
    
    CTTblPrChange getTblPrChange();
    
    boolean isSetTblPrChange();
    
    void setTblPrChange(final CTTblPrChange p0);
    
    CTTblPrChange addNewTblPrChange();
    
    void unsetTblPrChange();
    
    public static final class Factory
    {
        public static CTTblPr newInstance() {
            return (CTTblPr)XmlBeans.getContextTypeLoader().newInstance(CTTblPr.type, null);
        }
        
        public static CTTblPr newInstance(final XmlOptions xmlOptions) {
            return (CTTblPr)XmlBeans.getContextTypeLoader().newInstance(CTTblPr.type, xmlOptions);
        }
        
        public static CTTblPr parse(final String s) throws XmlException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(s, CTTblPr.type, null);
        }
        
        public static CTTblPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(s, CTTblPr.type, xmlOptions);
        }
        
        public static CTTblPr parse(final File file) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(file, CTTblPr.type, null);
        }
        
        public static CTTblPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(file, CTTblPr.type, xmlOptions);
        }
        
        public static CTTblPr parse(final URL url) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(url, CTTblPr.type, null);
        }
        
        public static CTTblPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(url, CTTblPr.type, xmlOptions);
        }
        
        public static CTTblPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTTblPr.type, null);
        }
        
        public static CTTblPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTTblPr.type, xmlOptions);
        }
        
        public static CTTblPr parse(final Reader reader) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(reader, CTTblPr.type, null);
        }
        
        public static CTTblPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(reader, CTTblPr.type, xmlOptions);
        }
        
        public static CTTblPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTblPr.type, null);
        }
        
        public static CTTblPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTblPr.type, xmlOptions);
        }
        
        public static CTTblPr parse(final Node node) throws XmlException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(node, CTTblPr.type, null);
        }
        
        public static CTTblPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(node, CTTblPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTblPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTblPr.type, null);
        }
        
        @Deprecated
        public static CTTblPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTblPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTblPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTblPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTblPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
