// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface CTPicture extends CTPictureBase
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPicture.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctpicture1054type");
    
    CTRel getMovie();
    
    boolean isSetMovie();
    
    void setMovie(final CTRel p0);
    
    CTRel addNewMovie();
    
    void unsetMovie();
    
    CTControl getControl();
    
    boolean isSetControl();
    
    void setControl(final CTControl p0);
    
    CTControl addNewControl();
    
    void unsetControl();
    
    public static final class Factory
    {
        public static CTPicture newInstance() {
            return (CTPicture)XmlBeans.getContextTypeLoader().newInstance(CTPicture.type, null);
        }
        
        public static CTPicture newInstance(final XmlOptions xmlOptions) {
            return (CTPicture)XmlBeans.getContextTypeLoader().newInstance(CTPicture.type, xmlOptions);
        }
        
        public static CTPicture parse(final String s) throws XmlException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(s, CTPicture.type, null);
        }
        
        public static CTPicture parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(s, CTPicture.type, xmlOptions);
        }
        
        public static CTPicture parse(final File file) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(file, CTPicture.type, null);
        }
        
        public static CTPicture parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(file, CTPicture.type, xmlOptions);
        }
        
        public static CTPicture parse(final URL url) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(url, CTPicture.type, null);
        }
        
        public static CTPicture parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(url, CTPicture.type, xmlOptions);
        }
        
        public static CTPicture parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(inputStream, CTPicture.type, null);
        }
        
        public static CTPicture parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(inputStream, CTPicture.type, xmlOptions);
        }
        
        public static CTPicture parse(final Reader reader) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(reader, CTPicture.type, null);
        }
        
        public static CTPicture parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(reader, CTPicture.type, xmlOptions);
        }
        
        public static CTPicture parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPicture.type, null);
        }
        
        public static CTPicture parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPicture.type, xmlOptions);
        }
        
        public static CTPicture parse(final Node node) throws XmlException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(node, CTPicture.type, null);
        }
        
        public static CTPicture parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(node, CTPicture.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPicture parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPicture.type, null);
        }
        
        @Deprecated
        public static CTPicture parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPicture)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPicture.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPicture.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPicture.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
