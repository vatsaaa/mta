// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface CTMarkupRange extends CTMarkup
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTMarkupRange.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctmarkuprangeba3dtype");
    
    STDisplacedByCustomXml.Enum getDisplacedByCustomXml();
    
    STDisplacedByCustomXml xgetDisplacedByCustomXml();
    
    boolean isSetDisplacedByCustomXml();
    
    void setDisplacedByCustomXml(final STDisplacedByCustomXml.Enum p0);
    
    void xsetDisplacedByCustomXml(final STDisplacedByCustomXml p0);
    
    void unsetDisplacedByCustomXml();
    
    public static final class Factory
    {
        public static CTMarkupRange newInstance() {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().newInstance(CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange newInstance(final XmlOptions xmlOptions) {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().newInstance(CTMarkupRange.type, xmlOptions);
        }
        
        public static CTMarkupRange parse(final String s) throws XmlException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(s, CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(s, CTMarkupRange.type, xmlOptions);
        }
        
        public static CTMarkupRange parse(final File file) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(file, CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(file, CTMarkupRange.type, xmlOptions);
        }
        
        public static CTMarkupRange parse(final URL url) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(url, CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(url, CTMarkupRange.type, xmlOptions);
        }
        
        public static CTMarkupRange parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(inputStream, CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(inputStream, CTMarkupRange.type, xmlOptions);
        }
        
        public static CTMarkupRange parse(final Reader reader) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(reader, CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(reader, CTMarkupRange.type, xmlOptions);
        }
        
        public static CTMarkupRange parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTMarkupRange.type, xmlOptions);
        }
        
        public static CTMarkupRange parse(final Node node) throws XmlException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(node, CTMarkupRange.type, null);
        }
        
        public static CTMarkupRange parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(node, CTMarkupRange.type, xmlOptions);
        }
        
        @Deprecated
        public static CTMarkupRange parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMarkupRange.type, null);
        }
        
        @Deprecated
        public static CTMarkupRange parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTMarkupRange)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTMarkupRange.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMarkupRange.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTMarkupRange.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
