// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSdtRun extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSdtRun.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsdtrun5c60type");
    
    CTSdtPr getSdtPr();
    
    boolean isSetSdtPr();
    
    void setSdtPr(final CTSdtPr p0);
    
    CTSdtPr addNewSdtPr();
    
    void unsetSdtPr();
    
    CTSdtEndPr getSdtEndPr();
    
    boolean isSetSdtEndPr();
    
    void setSdtEndPr(final CTSdtEndPr p0);
    
    CTSdtEndPr addNewSdtEndPr();
    
    void unsetSdtEndPr();
    
    CTSdtContentRun getSdtContent();
    
    boolean isSetSdtContent();
    
    void setSdtContent(final CTSdtContentRun p0);
    
    CTSdtContentRun addNewSdtContent();
    
    void unsetSdtContent();
    
    public static final class Factory
    {
        public static CTSdtRun newInstance() {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().newInstance(CTSdtRun.type, null);
        }
        
        public static CTSdtRun newInstance(final XmlOptions xmlOptions) {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().newInstance(CTSdtRun.type, xmlOptions);
        }
        
        public static CTSdtRun parse(final String s) throws XmlException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(s, CTSdtRun.type, null);
        }
        
        public static CTSdtRun parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(s, CTSdtRun.type, xmlOptions);
        }
        
        public static CTSdtRun parse(final File file) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(file, CTSdtRun.type, null);
        }
        
        public static CTSdtRun parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(file, CTSdtRun.type, xmlOptions);
        }
        
        public static CTSdtRun parse(final URL url) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(url, CTSdtRun.type, null);
        }
        
        public static CTSdtRun parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(url, CTSdtRun.type, xmlOptions);
        }
        
        public static CTSdtRun parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(inputStream, CTSdtRun.type, null);
        }
        
        public static CTSdtRun parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(inputStream, CTSdtRun.type, xmlOptions);
        }
        
        public static CTSdtRun parse(final Reader reader) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(reader, CTSdtRun.type, null);
        }
        
        public static CTSdtRun parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(reader, CTSdtRun.type, xmlOptions);
        }
        
        public static CTSdtRun parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSdtRun.type, null);
        }
        
        public static CTSdtRun parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSdtRun.type, xmlOptions);
        }
        
        public static CTSdtRun parse(final Node node) throws XmlException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(node, CTSdtRun.type, null);
        }
        
        public static CTSdtRun parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(node, CTSdtRun.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSdtRun parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSdtRun.type, null);
        }
        
        @Deprecated
        public static CTSdtRun parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSdtRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSdtRun.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSdtRun.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSdtRun.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
