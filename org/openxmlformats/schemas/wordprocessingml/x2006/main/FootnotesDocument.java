// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface FootnotesDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(FootnotesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("footnotes8773doctype");
    
    CTFootnotes getFootnotes();
    
    void setFootnotes(final CTFootnotes p0);
    
    CTFootnotes addNewFootnotes();
    
    public static final class Factory
    {
        public static FootnotesDocument newInstance() {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().newInstance(FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument newInstance(final XmlOptions xmlOptions) {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().newInstance(FootnotesDocument.type, xmlOptions);
        }
        
        public static FootnotesDocument parse(final String s) throws XmlException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(s, FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(s, FootnotesDocument.type, xmlOptions);
        }
        
        public static FootnotesDocument parse(final File file) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(file, FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(file, FootnotesDocument.type, xmlOptions);
        }
        
        public static FootnotesDocument parse(final URL url) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(url, FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(url, FootnotesDocument.type, xmlOptions);
        }
        
        public static FootnotesDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, FootnotesDocument.type, xmlOptions);
        }
        
        public static FootnotesDocument parse(final Reader reader) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(reader, FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(reader, FootnotesDocument.type, xmlOptions);
        }
        
        public static FootnotesDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, FootnotesDocument.type, xmlOptions);
        }
        
        public static FootnotesDocument parse(final Node node) throws XmlException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(node, FootnotesDocument.type, null);
        }
        
        public static FootnotesDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(node, FootnotesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static FootnotesDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, FootnotesDocument.type, null);
        }
        
        @Deprecated
        public static FootnotesDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (FootnotesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, FootnotesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, FootnotesDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, FootnotesDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
