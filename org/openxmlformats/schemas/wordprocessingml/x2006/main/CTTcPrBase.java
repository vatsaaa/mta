// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTcPrBase extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTcPrBase.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttcprbase93e6type");
    
    CTCnf getCnfStyle();
    
    boolean isSetCnfStyle();
    
    void setCnfStyle(final CTCnf p0);
    
    CTCnf addNewCnfStyle();
    
    void unsetCnfStyle();
    
    CTTblWidth getTcW();
    
    boolean isSetTcW();
    
    void setTcW(final CTTblWidth p0);
    
    CTTblWidth addNewTcW();
    
    void unsetTcW();
    
    CTDecimalNumber getGridSpan();
    
    boolean isSetGridSpan();
    
    void setGridSpan(final CTDecimalNumber p0);
    
    CTDecimalNumber addNewGridSpan();
    
    void unsetGridSpan();
    
    CTHMerge getHMerge();
    
    boolean isSetHMerge();
    
    void setHMerge(final CTHMerge p0);
    
    CTHMerge addNewHMerge();
    
    void unsetHMerge();
    
    CTVMerge getVMerge();
    
    boolean isSetVMerge();
    
    void setVMerge(final CTVMerge p0);
    
    CTVMerge addNewVMerge();
    
    void unsetVMerge();
    
    CTTcBorders getTcBorders();
    
    boolean isSetTcBorders();
    
    void setTcBorders(final CTTcBorders p0);
    
    CTTcBorders addNewTcBorders();
    
    void unsetTcBorders();
    
    CTShd getShd();
    
    boolean isSetShd();
    
    void setShd(final CTShd p0);
    
    CTShd addNewShd();
    
    void unsetShd();
    
    CTOnOff getNoWrap();
    
    boolean isSetNoWrap();
    
    void setNoWrap(final CTOnOff p0);
    
    CTOnOff addNewNoWrap();
    
    void unsetNoWrap();
    
    CTTcMar getTcMar();
    
    boolean isSetTcMar();
    
    void setTcMar(final CTTcMar p0);
    
    CTTcMar addNewTcMar();
    
    void unsetTcMar();
    
    CTTextDirection getTextDirection();
    
    boolean isSetTextDirection();
    
    void setTextDirection(final CTTextDirection p0);
    
    CTTextDirection addNewTextDirection();
    
    void unsetTextDirection();
    
    CTOnOff getTcFitText();
    
    boolean isSetTcFitText();
    
    void setTcFitText(final CTOnOff p0);
    
    CTOnOff addNewTcFitText();
    
    void unsetTcFitText();
    
    CTVerticalJc getVAlign();
    
    boolean isSetVAlign();
    
    void setVAlign(final CTVerticalJc p0);
    
    CTVerticalJc addNewVAlign();
    
    void unsetVAlign();
    
    CTOnOff getHideMark();
    
    boolean isSetHideMark();
    
    void setHideMark(final CTOnOff p0);
    
    CTOnOff addNewHideMark();
    
    void unsetHideMark();
    
    public static final class Factory
    {
        public static CTTcPrBase newInstance() {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().newInstance(CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase newInstance(final XmlOptions xmlOptions) {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().newInstance(CTTcPrBase.type, xmlOptions);
        }
        
        public static CTTcPrBase parse(final String s) throws XmlException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(s, CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(s, CTTcPrBase.type, xmlOptions);
        }
        
        public static CTTcPrBase parse(final File file) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(file, CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(file, CTTcPrBase.type, xmlOptions);
        }
        
        public static CTTcPrBase parse(final URL url) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(url, CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(url, CTTcPrBase.type, xmlOptions);
        }
        
        public static CTTcPrBase parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(inputStream, CTTcPrBase.type, xmlOptions);
        }
        
        public static CTTcPrBase parse(final Reader reader) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(reader, CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(reader, CTTcPrBase.type, xmlOptions);
        }
        
        public static CTTcPrBase parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTcPrBase.type, xmlOptions);
        }
        
        public static CTTcPrBase parse(final Node node) throws XmlException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(node, CTTcPrBase.type, null);
        }
        
        public static CTTcPrBase parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(node, CTTcPrBase.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTcPrBase parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTcPrBase.type, null);
        }
        
        @Deprecated
        public static CTTcPrBase parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTcPrBase)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTcPrBase.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTcPrBase.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTcPrBase.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
