// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlHexBinary;

public interface STLongHexNumber extends XmlHexBinary
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLongHexNumber.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlonghexnumberd6batype");
    
    public static final class Factory
    {
        public static STLongHexNumber newValue(final Object o) {
            return (STLongHexNumber)STLongHexNumber.type.newValue(o);
        }
        
        public static STLongHexNumber newInstance() {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().newInstance(STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber newInstance(final XmlOptions xmlOptions) {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().newInstance(STLongHexNumber.type, xmlOptions);
        }
        
        public static STLongHexNumber parse(final String s) throws XmlException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(s, STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(s, STLongHexNumber.type, xmlOptions);
        }
        
        public static STLongHexNumber parse(final File file) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(file, STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(file, STLongHexNumber.type, xmlOptions);
        }
        
        public static STLongHexNumber parse(final URL url) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(url, STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(url, STLongHexNumber.type, xmlOptions);
        }
        
        public static STLongHexNumber parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(inputStream, STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(inputStream, STLongHexNumber.type, xmlOptions);
        }
        
        public static STLongHexNumber parse(final Reader reader) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(reader, STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(reader, STLongHexNumber.type, xmlOptions);
        }
        
        public static STLongHexNumber parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLongHexNumber.type, xmlOptions);
        }
        
        public static STLongHexNumber parse(final Node node) throws XmlException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(node, STLongHexNumber.type, null);
        }
        
        public static STLongHexNumber parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(node, STLongHexNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static STLongHexNumber parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLongHexNumber.type, null);
        }
        
        @Deprecated
        public static STLongHexNumber parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLongHexNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLongHexNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLongHexNumber.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLongHexNumber.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
