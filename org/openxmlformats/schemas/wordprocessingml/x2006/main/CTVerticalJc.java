// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTVerticalJc extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTVerticalJc.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctverticaljca439type");
    
    STVerticalJc.Enum getVal();
    
    STVerticalJc xgetVal();
    
    void setVal(final STVerticalJc.Enum p0);
    
    void xsetVal(final STVerticalJc p0);
    
    public static final class Factory
    {
        public static CTVerticalJc newInstance() {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().newInstance(CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc newInstance(final XmlOptions xmlOptions) {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().newInstance(CTVerticalJc.type, xmlOptions);
        }
        
        public static CTVerticalJc parse(final String s) throws XmlException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(s, CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(s, CTVerticalJc.type, xmlOptions);
        }
        
        public static CTVerticalJc parse(final File file) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(file, CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(file, CTVerticalJc.type, xmlOptions);
        }
        
        public static CTVerticalJc parse(final URL url) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(url, CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(url, CTVerticalJc.type, xmlOptions);
        }
        
        public static CTVerticalJc parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(inputStream, CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(inputStream, CTVerticalJc.type, xmlOptions);
        }
        
        public static CTVerticalJc parse(final Reader reader) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(reader, CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(reader, CTVerticalJc.type, xmlOptions);
        }
        
        public static CTVerticalJc parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVerticalJc.type, xmlOptions);
        }
        
        public static CTVerticalJc parse(final Node node) throws XmlException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(node, CTVerticalJc.type, null);
        }
        
        public static CTVerticalJc parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(node, CTVerticalJc.type, xmlOptions);
        }
        
        @Deprecated
        public static CTVerticalJc parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVerticalJc.type, null);
        }
        
        @Deprecated
        public static CTVerticalJc parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTVerticalJc)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVerticalJc.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVerticalJc.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVerticalJc.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
