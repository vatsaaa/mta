// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STLineSpacingRule extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STLineSpacingRule.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stlinespacingrule6237type");
    public static final Enum AUTO = Enum.forString("auto");
    public static final Enum EXACT = Enum.forString("exact");
    public static final Enum AT_LEAST = Enum.forString("atLeast");
    public static final int INT_AUTO = 1;
    public static final int INT_EXACT = 2;
    public static final int INT_AT_LEAST = 3;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STLineSpacingRule newValue(final Object o) {
            return (STLineSpacingRule)STLineSpacingRule.type.newValue(o);
        }
        
        public static STLineSpacingRule newInstance() {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().newInstance(STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule newInstance(final XmlOptions xmlOptions) {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().newInstance(STLineSpacingRule.type, xmlOptions);
        }
        
        public static STLineSpacingRule parse(final String s) throws XmlException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(s, STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(s, STLineSpacingRule.type, xmlOptions);
        }
        
        public static STLineSpacingRule parse(final File file) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(file, STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(file, STLineSpacingRule.type, xmlOptions);
        }
        
        public static STLineSpacingRule parse(final URL url) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(url, STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(url, STLineSpacingRule.type, xmlOptions);
        }
        
        public static STLineSpacingRule parse(final InputStream inputStream) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(inputStream, STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(inputStream, STLineSpacingRule.type, xmlOptions);
        }
        
        public static STLineSpacingRule parse(final Reader reader) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(reader, STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(reader, STLineSpacingRule.type, xmlOptions);
        }
        
        public static STLineSpacingRule parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STLineSpacingRule.type, xmlOptions);
        }
        
        public static STLineSpacingRule parse(final Node node) throws XmlException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(node, STLineSpacingRule.type, null);
        }
        
        public static STLineSpacingRule parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(node, STLineSpacingRule.type, xmlOptions);
        }
        
        @Deprecated
        public static STLineSpacingRule parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineSpacingRule.type, null);
        }
        
        @Deprecated
        public static STLineSpacingRule parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STLineSpacingRule)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STLineSpacingRule.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineSpacingRule.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STLineSpacingRule.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_AUTO = 1;
        static final int INT_EXACT = 2;
        static final int INT_AT_LEAST = 3;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("auto", 1), new Enum("exact", 2), new Enum("atLeast", 3) });
        }
    }
}
