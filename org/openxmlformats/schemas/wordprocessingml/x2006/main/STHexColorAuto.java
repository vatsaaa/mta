// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STHexColorAuto extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STHexColorAuto.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sthexcolorauto3ce1type");
    public static final Enum AUTO = Enum.forString("auto");
    public static final int INT_AUTO = 1;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STHexColorAuto newValue(final Object o) {
            return (STHexColorAuto)STHexColorAuto.type.newValue(o);
        }
        
        public static STHexColorAuto newInstance() {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().newInstance(STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto newInstance(final XmlOptions xmlOptions) {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().newInstance(STHexColorAuto.type, xmlOptions);
        }
        
        public static STHexColorAuto parse(final String s) throws XmlException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(s, STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(s, STHexColorAuto.type, xmlOptions);
        }
        
        public static STHexColorAuto parse(final File file) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(file, STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(file, STHexColorAuto.type, xmlOptions);
        }
        
        public static STHexColorAuto parse(final URL url) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(url, STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(url, STHexColorAuto.type, xmlOptions);
        }
        
        public static STHexColorAuto parse(final InputStream inputStream) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(inputStream, STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(inputStream, STHexColorAuto.type, xmlOptions);
        }
        
        public static STHexColorAuto parse(final Reader reader) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(reader, STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(reader, STHexColorAuto.type, xmlOptions);
        }
        
        public static STHexColorAuto parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STHexColorAuto.type, xmlOptions);
        }
        
        public static STHexColorAuto parse(final Node node) throws XmlException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(node, STHexColorAuto.type, null);
        }
        
        public static STHexColorAuto parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(node, STHexColorAuto.type, xmlOptions);
        }
        
        @Deprecated
        public static STHexColorAuto parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STHexColorAuto.type, null);
        }
        
        @Deprecated
        public static STHexColorAuto parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STHexColorAuto)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STHexColorAuto.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STHexColorAuto.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STHexColorAuto.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_AUTO = 1;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("auto", 1) });
        }
    }
}
