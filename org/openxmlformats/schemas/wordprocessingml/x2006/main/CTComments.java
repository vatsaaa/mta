// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTComments extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTComments.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctcomments7674type");
    
    List<CTComment> getCommentList();
    
    @Deprecated
    CTComment[] getCommentArray();
    
    CTComment getCommentArray(final int p0);
    
    int sizeOfCommentArray();
    
    void setCommentArray(final CTComment[] p0);
    
    void setCommentArray(final int p0, final CTComment p1);
    
    CTComment insertNewComment(final int p0);
    
    CTComment addNewComment();
    
    void removeComment(final int p0);
    
    public static final class Factory
    {
        public static CTComments newInstance() {
            return (CTComments)XmlBeans.getContextTypeLoader().newInstance(CTComments.type, null);
        }
        
        public static CTComments newInstance(final XmlOptions xmlOptions) {
            return (CTComments)XmlBeans.getContextTypeLoader().newInstance(CTComments.type, xmlOptions);
        }
        
        public static CTComments parse(final String s) throws XmlException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(s, CTComments.type, null);
        }
        
        public static CTComments parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(s, CTComments.type, xmlOptions);
        }
        
        public static CTComments parse(final File file) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(file, CTComments.type, null);
        }
        
        public static CTComments parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(file, CTComments.type, xmlOptions);
        }
        
        public static CTComments parse(final URL url) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(url, CTComments.type, null);
        }
        
        public static CTComments parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(url, CTComments.type, xmlOptions);
        }
        
        public static CTComments parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(inputStream, CTComments.type, null);
        }
        
        public static CTComments parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(inputStream, CTComments.type, xmlOptions);
        }
        
        public static CTComments parse(final Reader reader) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(reader, CTComments.type, null);
        }
        
        public static CTComments parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(reader, CTComments.type, xmlOptions);
        }
        
        public static CTComments parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTComments.type, null);
        }
        
        public static CTComments parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTComments.type, xmlOptions);
        }
        
        public static CTComments parse(final Node node) throws XmlException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(node, CTComments.type, null);
        }
        
        public static CTComments parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(node, CTComments.type, xmlOptions);
        }
        
        @Deprecated
        public static CTComments parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTComments.type, null);
        }
        
        @Deprecated
        public static CTComments parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTComments)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTComments.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTComments.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTComments.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
