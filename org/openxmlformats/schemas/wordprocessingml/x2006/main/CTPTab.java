// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTPTab extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTPTab.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctptaba283type");
    
    STPTabAlignment.Enum getAlignment();
    
    STPTabAlignment xgetAlignment();
    
    void setAlignment(final STPTabAlignment.Enum p0);
    
    void xsetAlignment(final STPTabAlignment p0);
    
    STPTabRelativeTo.Enum getRelativeTo();
    
    STPTabRelativeTo xgetRelativeTo();
    
    void setRelativeTo(final STPTabRelativeTo.Enum p0);
    
    void xsetRelativeTo(final STPTabRelativeTo p0);
    
    STPTabLeader.Enum getLeader();
    
    STPTabLeader xgetLeader();
    
    void setLeader(final STPTabLeader.Enum p0);
    
    void xsetLeader(final STPTabLeader p0);
    
    public static final class Factory
    {
        public static CTPTab newInstance() {
            return (CTPTab)XmlBeans.getContextTypeLoader().newInstance(CTPTab.type, null);
        }
        
        public static CTPTab newInstance(final XmlOptions xmlOptions) {
            return (CTPTab)XmlBeans.getContextTypeLoader().newInstance(CTPTab.type, xmlOptions);
        }
        
        public static CTPTab parse(final String s) throws XmlException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(s, CTPTab.type, null);
        }
        
        public static CTPTab parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(s, CTPTab.type, xmlOptions);
        }
        
        public static CTPTab parse(final File file) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(file, CTPTab.type, null);
        }
        
        public static CTPTab parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(file, CTPTab.type, xmlOptions);
        }
        
        public static CTPTab parse(final URL url) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(url, CTPTab.type, null);
        }
        
        public static CTPTab parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(url, CTPTab.type, xmlOptions);
        }
        
        public static CTPTab parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(inputStream, CTPTab.type, null);
        }
        
        public static CTPTab parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(inputStream, CTPTab.type, xmlOptions);
        }
        
        public static CTPTab parse(final Reader reader) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(reader, CTPTab.type, null);
        }
        
        public static CTPTab parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(reader, CTPTab.type, xmlOptions);
        }
        
        public static CTPTab parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPTab.type, null);
        }
        
        public static CTPTab parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTPTab.type, xmlOptions);
        }
        
        public static CTPTab parse(final Node node) throws XmlException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(node, CTPTab.type, null);
        }
        
        public static CTPTab parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(node, CTPTab.type, xmlOptions);
        }
        
        @Deprecated
        public static CTPTab parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPTab.type, null);
        }
        
        @Deprecated
        public static CTPTab parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTPTab)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTPTab.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPTab.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTPTab.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
