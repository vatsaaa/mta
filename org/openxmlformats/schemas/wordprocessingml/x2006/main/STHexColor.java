// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlAnySimpleType;

public interface STHexColor extends XmlAnySimpleType
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STHexColor.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sthexcolor55d0type");
    
    Object getObjectValue();
    
    void setObjectValue(final Object p0);
    
    @Deprecated
    Object objectValue();
    
    @Deprecated
    void objectSet(final Object p0);
    
    SchemaType instanceType();
    
    public static final class Factory
    {
        public static STHexColor newValue(final Object o) {
            return (STHexColor)STHexColor.type.newValue(o);
        }
        
        public static STHexColor newInstance() {
            return (STHexColor)XmlBeans.getContextTypeLoader().newInstance(STHexColor.type, null);
        }
        
        public static STHexColor newInstance(final XmlOptions xmlOptions) {
            return (STHexColor)XmlBeans.getContextTypeLoader().newInstance(STHexColor.type, xmlOptions);
        }
        
        public static STHexColor parse(final String s) throws XmlException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(s, STHexColor.type, null);
        }
        
        public static STHexColor parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(s, STHexColor.type, xmlOptions);
        }
        
        public static STHexColor parse(final File file) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(file, STHexColor.type, null);
        }
        
        public static STHexColor parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(file, STHexColor.type, xmlOptions);
        }
        
        public static STHexColor parse(final URL url) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(url, STHexColor.type, null);
        }
        
        public static STHexColor parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(url, STHexColor.type, xmlOptions);
        }
        
        public static STHexColor parse(final InputStream inputStream) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(inputStream, STHexColor.type, null);
        }
        
        public static STHexColor parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(inputStream, STHexColor.type, xmlOptions);
        }
        
        public static STHexColor parse(final Reader reader) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(reader, STHexColor.type, null);
        }
        
        public static STHexColor parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(reader, STHexColor.type, xmlOptions);
        }
        
        public static STHexColor parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STHexColor.type, null);
        }
        
        public static STHexColor parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STHexColor.type, xmlOptions);
        }
        
        public static STHexColor parse(final Node node) throws XmlException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(node, STHexColor.type, null);
        }
        
        public static STHexColor parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(node, STHexColor.type, xmlOptions);
        }
        
        @Deprecated
        public static STHexColor parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STHexColor.type, null);
        }
        
        @Deprecated
        public static STHexColor parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STHexColor)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STHexColor.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STHexColor.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STHexColor.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
