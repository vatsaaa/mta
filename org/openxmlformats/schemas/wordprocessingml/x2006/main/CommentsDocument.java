// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CommentsDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CommentsDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("comments3da0doctype");
    
    CTComments getComments();
    
    void setComments(final CTComments p0);
    
    CTComments addNewComments();
    
    public static final class Factory
    {
        public static CommentsDocument newInstance() {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().newInstance(CommentsDocument.type, null);
        }
        
        public static CommentsDocument newInstance(final XmlOptions xmlOptions) {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().newInstance(CommentsDocument.type, xmlOptions);
        }
        
        public static CommentsDocument parse(final String s) throws XmlException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(s, CommentsDocument.type, null);
        }
        
        public static CommentsDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(s, CommentsDocument.type, xmlOptions);
        }
        
        public static CommentsDocument parse(final File file) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(file, CommentsDocument.type, null);
        }
        
        public static CommentsDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(file, CommentsDocument.type, xmlOptions);
        }
        
        public static CommentsDocument parse(final URL url) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(url, CommentsDocument.type, null);
        }
        
        public static CommentsDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(url, CommentsDocument.type, xmlOptions);
        }
        
        public static CommentsDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CommentsDocument.type, null);
        }
        
        public static CommentsDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(inputStream, CommentsDocument.type, xmlOptions);
        }
        
        public static CommentsDocument parse(final Reader reader) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(reader, CommentsDocument.type, null);
        }
        
        public static CommentsDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(reader, CommentsDocument.type, xmlOptions);
        }
        
        public static CommentsDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CommentsDocument.type, null);
        }
        
        public static CommentsDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CommentsDocument.type, xmlOptions);
        }
        
        public static CommentsDocument parse(final Node node) throws XmlException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(node, CommentsDocument.type, null);
        }
        
        public static CommentsDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(node, CommentsDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static CommentsDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CommentsDocument.type, null);
        }
        
        @Deprecated
        public static CommentsDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CommentsDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CommentsDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CommentsDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CommentsDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
