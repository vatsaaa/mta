// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTextAlignment extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTextAlignment.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttextalignment495ctype");
    
    STTextAlignment.Enum getVal();
    
    STTextAlignment xgetVal();
    
    void setVal(final STTextAlignment.Enum p0);
    
    void xsetVal(final STTextAlignment p0);
    
    public static final class Factory
    {
        public static CTTextAlignment newInstance() {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().newInstance(CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment newInstance(final XmlOptions xmlOptions) {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().newInstance(CTTextAlignment.type, xmlOptions);
        }
        
        public static CTTextAlignment parse(final String s) throws XmlException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(s, CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(s, CTTextAlignment.type, xmlOptions);
        }
        
        public static CTTextAlignment parse(final File file) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(file, CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(file, CTTextAlignment.type, xmlOptions);
        }
        
        public static CTTextAlignment parse(final URL url) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(url, CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(url, CTTextAlignment.type, xmlOptions);
        }
        
        public static CTTextAlignment parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(inputStream, CTTextAlignment.type, xmlOptions);
        }
        
        public static CTTextAlignment parse(final Reader reader) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(reader, CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(reader, CTTextAlignment.type, xmlOptions);
        }
        
        public static CTTextAlignment parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTextAlignment.type, xmlOptions);
        }
        
        public static CTTextAlignment parse(final Node node) throws XmlException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(node, CTTextAlignment.type, null);
        }
        
        public static CTTextAlignment parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(node, CTTextAlignment.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTextAlignment parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextAlignment.type, null);
        }
        
        @Deprecated
        public static CTTextAlignment parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTextAlignment)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTextAlignment.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextAlignment.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTextAlignment.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
