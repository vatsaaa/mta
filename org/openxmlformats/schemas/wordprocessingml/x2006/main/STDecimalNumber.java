// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInteger;

public interface STDecimalNumber extends XmlInteger
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STDecimalNumber.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stdecimalnumber8d28type");
    
    public static final class Factory
    {
        public static STDecimalNumber newValue(final Object o) {
            return (STDecimalNumber)STDecimalNumber.type.newValue(o);
        }
        
        public static STDecimalNumber newInstance() {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().newInstance(STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber newInstance(final XmlOptions xmlOptions) {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().newInstance(STDecimalNumber.type, xmlOptions);
        }
        
        public static STDecimalNumber parse(final String s) throws XmlException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(s, STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(s, STDecimalNumber.type, xmlOptions);
        }
        
        public static STDecimalNumber parse(final File file) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(file, STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(file, STDecimalNumber.type, xmlOptions);
        }
        
        public static STDecimalNumber parse(final URL url) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(url, STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(url, STDecimalNumber.type, xmlOptions);
        }
        
        public static STDecimalNumber parse(final InputStream inputStream) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(inputStream, STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(inputStream, STDecimalNumber.type, xmlOptions);
        }
        
        public static STDecimalNumber parse(final Reader reader) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(reader, STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(reader, STDecimalNumber.type, xmlOptions);
        }
        
        public static STDecimalNumber parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STDecimalNumber.type, xmlOptions);
        }
        
        public static STDecimalNumber parse(final Node node) throws XmlException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(node, STDecimalNumber.type, null);
        }
        
        public static STDecimalNumber parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(node, STDecimalNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static STDecimalNumber parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STDecimalNumber.type, null);
        }
        
        @Deprecated
        public static STDecimalNumber parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STDecimalNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STDecimalNumber.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STDecimalNumber.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
