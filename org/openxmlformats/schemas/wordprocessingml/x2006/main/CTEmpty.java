// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTEmpty extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTEmpty.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctempty3fa5type");
    
    public static final class Factory
    {
        public static CTEmpty newInstance() {
            return (CTEmpty)XmlBeans.getContextTypeLoader().newInstance(CTEmpty.type, null);
        }
        
        public static CTEmpty newInstance(final XmlOptions xmlOptions) {
            return (CTEmpty)XmlBeans.getContextTypeLoader().newInstance(CTEmpty.type, xmlOptions);
        }
        
        public static CTEmpty parse(final String s) throws XmlException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(s, CTEmpty.type, null);
        }
        
        public static CTEmpty parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(s, CTEmpty.type, xmlOptions);
        }
        
        public static CTEmpty parse(final File file) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(file, CTEmpty.type, null);
        }
        
        public static CTEmpty parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(file, CTEmpty.type, xmlOptions);
        }
        
        public static CTEmpty parse(final URL url) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(url, CTEmpty.type, null);
        }
        
        public static CTEmpty parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(url, CTEmpty.type, xmlOptions);
        }
        
        public static CTEmpty parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(inputStream, CTEmpty.type, null);
        }
        
        public static CTEmpty parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(inputStream, CTEmpty.type, xmlOptions);
        }
        
        public static CTEmpty parse(final Reader reader) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(reader, CTEmpty.type, null);
        }
        
        public static CTEmpty parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(reader, CTEmpty.type, xmlOptions);
        }
        
        public static CTEmpty parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTEmpty.type, null);
        }
        
        public static CTEmpty parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTEmpty.type, xmlOptions);
        }
        
        public static CTEmpty parse(final Node node) throws XmlException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(node, CTEmpty.type, null);
        }
        
        public static CTEmpty parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(node, CTEmpty.type, xmlOptions);
        }
        
        @Deprecated
        public static CTEmpty parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTEmpty.type, null);
        }
        
        @Deprecated
        public static CTEmpty parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTEmpty)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTEmpty.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTEmpty.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTEmpty.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
