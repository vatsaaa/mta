// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.math.BigInteger;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTAbstractNum extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTAbstractNum.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctabstractnum588etype");
    
    CTLongHexNumber getNsid();
    
    boolean isSetNsid();
    
    void setNsid(final CTLongHexNumber p0);
    
    CTLongHexNumber addNewNsid();
    
    void unsetNsid();
    
    CTMultiLevelType getMultiLevelType();
    
    boolean isSetMultiLevelType();
    
    void setMultiLevelType(final CTMultiLevelType p0);
    
    CTMultiLevelType addNewMultiLevelType();
    
    void unsetMultiLevelType();
    
    CTLongHexNumber getTmpl();
    
    boolean isSetTmpl();
    
    void setTmpl(final CTLongHexNumber p0);
    
    CTLongHexNumber addNewTmpl();
    
    void unsetTmpl();
    
    CTString getName();
    
    boolean isSetName();
    
    void setName(final CTString p0);
    
    CTString addNewName();
    
    void unsetName();
    
    CTString getStyleLink();
    
    boolean isSetStyleLink();
    
    void setStyleLink(final CTString p0);
    
    CTString addNewStyleLink();
    
    void unsetStyleLink();
    
    CTString getNumStyleLink();
    
    boolean isSetNumStyleLink();
    
    void setNumStyleLink(final CTString p0);
    
    CTString addNewNumStyleLink();
    
    void unsetNumStyleLink();
    
    List<CTLvl> getLvlList();
    
    @Deprecated
    CTLvl[] getLvlArray();
    
    CTLvl getLvlArray(final int p0);
    
    int sizeOfLvlArray();
    
    void setLvlArray(final CTLvl[] p0);
    
    void setLvlArray(final int p0, final CTLvl p1);
    
    CTLvl insertNewLvl(final int p0);
    
    CTLvl addNewLvl();
    
    void removeLvl(final int p0);
    
    BigInteger getAbstractNumId();
    
    STDecimalNumber xgetAbstractNumId();
    
    void setAbstractNumId(final BigInteger p0);
    
    void xsetAbstractNumId(final STDecimalNumber p0);
    
    public static final class Factory
    {
        public static CTAbstractNum newInstance() {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().newInstance(CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum newInstance(final XmlOptions xmlOptions) {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().newInstance(CTAbstractNum.type, xmlOptions);
        }
        
        public static CTAbstractNum parse(final String s) throws XmlException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(s, CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(s, CTAbstractNum.type, xmlOptions);
        }
        
        public static CTAbstractNum parse(final File file) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(file, CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(file, CTAbstractNum.type, xmlOptions);
        }
        
        public static CTAbstractNum parse(final URL url) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(url, CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(url, CTAbstractNum.type, xmlOptions);
        }
        
        public static CTAbstractNum parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(inputStream, CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(inputStream, CTAbstractNum.type, xmlOptions);
        }
        
        public static CTAbstractNum parse(final Reader reader) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(reader, CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(reader, CTAbstractNum.type, xmlOptions);
        }
        
        public static CTAbstractNum parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTAbstractNum.type, xmlOptions);
        }
        
        public static CTAbstractNum parse(final Node node) throws XmlException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(node, CTAbstractNum.type, null);
        }
        
        public static CTAbstractNum parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(node, CTAbstractNum.type, xmlOptions);
        }
        
        @Deprecated
        public static CTAbstractNum parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAbstractNum.type, null);
        }
        
        @Deprecated
        public static CTAbstractNum parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTAbstractNum)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTAbstractNum.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAbstractNum.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTAbstractNum.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
