// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSym extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSym.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsym0dabtype");
    
    String getFont();
    
    STString xgetFont();
    
    boolean isSetFont();
    
    void setFont(final String p0);
    
    void xsetFont(final STString p0);
    
    void unsetFont();
    
    byte[] getChar();
    
    STShortHexNumber xgetChar();
    
    boolean isSetChar();
    
    void setChar(final byte[] p0);
    
    void xsetChar(final STShortHexNumber p0);
    
    void unsetChar();
    
    public static final class Factory
    {
        public static CTSym newInstance() {
            return (CTSym)XmlBeans.getContextTypeLoader().newInstance(CTSym.type, null);
        }
        
        public static CTSym newInstance(final XmlOptions xmlOptions) {
            return (CTSym)XmlBeans.getContextTypeLoader().newInstance(CTSym.type, xmlOptions);
        }
        
        public static CTSym parse(final String s) throws XmlException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(s, CTSym.type, null);
        }
        
        public static CTSym parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(s, CTSym.type, xmlOptions);
        }
        
        public static CTSym parse(final File file) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(file, CTSym.type, null);
        }
        
        public static CTSym parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(file, CTSym.type, xmlOptions);
        }
        
        public static CTSym parse(final URL url) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(url, CTSym.type, null);
        }
        
        public static CTSym parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(url, CTSym.type, xmlOptions);
        }
        
        public static CTSym parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(inputStream, CTSym.type, null);
        }
        
        public static CTSym parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(inputStream, CTSym.type, xmlOptions);
        }
        
        public static CTSym parse(final Reader reader) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(reader, CTSym.type, null);
        }
        
        public static CTSym parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(reader, CTSym.type, xmlOptions);
        }
        
        public static CTSym parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSym.type, null);
        }
        
        public static CTSym parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSym.type, xmlOptions);
        }
        
        public static CTSym parse(final Node node) throws XmlException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(node, CTSym.type, null);
        }
        
        public static CTSym parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(node, CTSym.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSym parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSym.type, null);
        }
        
        @Deprecated
        public static CTSym parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSym)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSym.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSym.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSym.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
