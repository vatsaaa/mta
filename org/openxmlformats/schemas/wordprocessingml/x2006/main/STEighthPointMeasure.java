// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STEighthPointMeasure extends STUnsignedDecimalNumber
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STEighthPointMeasure.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("steighthpointmeasure3371type");
    
    public static final class Factory
    {
        public static STEighthPointMeasure newValue(final Object o) {
            return (STEighthPointMeasure)STEighthPointMeasure.type.newValue(o);
        }
        
        public static STEighthPointMeasure newInstance() {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().newInstance(STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure newInstance(final XmlOptions xmlOptions) {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().newInstance(STEighthPointMeasure.type, xmlOptions);
        }
        
        public static STEighthPointMeasure parse(final String s) throws XmlException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(s, STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(s, STEighthPointMeasure.type, xmlOptions);
        }
        
        public static STEighthPointMeasure parse(final File file) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(file, STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(file, STEighthPointMeasure.type, xmlOptions);
        }
        
        public static STEighthPointMeasure parse(final URL url) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(url, STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(url, STEighthPointMeasure.type, xmlOptions);
        }
        
        public static STEighthPointMeasure parse(final InputStream inputStream) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STEighthPointMeasure.type, xmlOptions);
        }
        
        public static STEighthPointMeasure parse(final Reader reader) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(reader, STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(reader, STEighthPointMeasure.type, xmlOptions);
        }
        
        public static STEighthPointMeasure parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STEighthPointMeasure.type, xmlOptions);
        }
        
        public static STEighthPointMeasure parse(final Node node) throws XmlException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(node, STEighthPointMeasure.type, null);
        }
        
        public static STEighthPointMeasure parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(node, STEighthPointMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static STEighthPointMeasure parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STEighthPointMeasure.type, null);
        }
        
        @Deprecated
        public static STEighthPointMeasure parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STEighthPointMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STEighthPointMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STEighthPointMeasure.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STEighthPointMeasure.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
