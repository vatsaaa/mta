// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTabs extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTabs.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttabsa2aatype");
    
    List<CTTabStop> getTabList();
    
    @Deprecated
    CTTabStop[] getTabArray();
    
    CTTabStop getTabArray(final int p0);
    
    int sizeOfTabArray();
    
    void setTabArray(final CTTabStop[] p0);
    
    void setTabArray(final int p0, final CTTabStop p1);
    
    CTTabStop insertNewTab(final int p0);
    
    CTTabStop addNewTab();
    
    void removeTab(final int p0);
    
    public static final class Factory
    {
        public static CTTabs newInstance() {
            return (CTTabs)XmlBeans.getContextTypeLoader().newInstance(CTTabs.type, null);
        }
        
        public static CTTabs newInstance(final XmlOptions xmlOptions) {
            return (CTTabs)XmlBeans.getContextTypeLoader().newInstance(CTTabs.type, xmlOptions);
        }
        
        public static CTTabs parse(final String s) throws XmlException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(s, CTTabs.type, null);
        }
        
        public static CTTabs parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(s, CTTabs.type, xmlOptions);
        }
        
        public static CTTabs parse(final File file) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(file, CTTabs.type, null);
        }
        
        public static CTTabs parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(file, CTTabs.type, xmlOptions);
        }
        
        public static CTTabs parse(final URL url) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(url, CTTabs.type, null);
        }
        
        public static CTTabs parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(url, CTTabs.type, xmlOptions);
        }
        
        public static CTTabs parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(inputStream, CTTabs.type, null);
        }
        
        public static CTTabs parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(inputStream, CTTabs.type, xmlOptions);
        }
        
        public static CTTabs parse(final Reader reader) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(reader, CTTabs.type, null);
        }
        
        public static CTTabs parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(reader, CTTabs.type, xmlOptions);
        }
        
        public static CTTabs parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTabs.type, null);
        }
        
        public static CTTabs parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTabs.type, xmlOptions);
        }
        
        public static CTTabs parse(final Node node) throws XmlException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(node, CTTabs.type, null);
        }
        
        public static CTTabs parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(node, CTTabs.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTabs parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTabs.type, null);
        }
        
        @Deprecated
        public static CTTabs parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTabs)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTabs.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTabs.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTabs.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
