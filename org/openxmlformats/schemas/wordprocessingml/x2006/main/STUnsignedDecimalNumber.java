// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlUnsignedLong;

public interface STUnsignedDecimalNumber extends XmlUnsignedLong
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STUnsignedDecimalNumber.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stunsigneddecimalnumber74fdtype");
    
    public static final class Factory
    {
        public static STUnsignedDecimalNumber newValue(final Object o) {
            return (STUnsignedDecimalNumber)STUnsignedDecimalNumber.type.newValue(o);
        }
        
        public static STUnsignedDecimalNumber newInstance() {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().newInstance(STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber newInstance(final XmlOptions xmlOptions) {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().newInstance(STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        public static STUnsignedDecimalNumber parse(final String s) throws XmlException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(s, STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(s, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        public static STUnsignedDecimalNumber parse(final File file) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(file, STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(file, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        public static STUnsignedDecimalNumber parse(final URL url) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(url, STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(url, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        public static STUnsignedDecimalNumber parse(final InputStream inputStream) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(inputStream, STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(inputStream, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        public static STUnsignedDecimalNumber parse(final Reader reader) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(reader, STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(reader, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        public static STUnsignedDecimalNumber parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        public static STUnsignedDecimalNumber parse(final Node node) throws XmlException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(node, STUnsignedDecimalNumber.type, null);
        }
        
        public static STUnsignedDecimalNumber parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(node, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static STUnsignedDecimalNumber parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STUnsignedDecimalNumber.type, null);
        }
        
        @Deprecated
        public static STUnsignedDecimalNumber parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STUnsignedDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STUnsignedDecimalNumber.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STUnsignedDecimalNumber.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
