// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInteger;

public interface STSignedTwipsMeasure extends XmlInteger
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STSignedTwipsMeasure.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stsignedtwipsmeasureb227type");
    
    public static final class Factory
    {
        public static STSignedTwipsMeasure newValue(final Object o) {
            return (STSignedTwipsMeasure)STSignedTwipsMeasure.type.newValue(o);
        }
        
        public static STSignedTwipsMeasure newInstance() {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().newInstance(STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure newInstance(final XmlOptions xmlOptions) {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().newInstance(STSignedTwipsMeasure.type, xmlOptions);
        }
        
        public static STSignedTwipsMeasure parse(final String s) throws XmlException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(s, STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(s, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        public static STSignedTwipsMeasure parse(final File file) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(file, STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(file, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        public static STSignedTwipsMeasure parse(final URL url) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(url, STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(url, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        public static STSignedTwipsMeasure parse(final InputStream inputStream) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        public static STSignedTwipsMeasure parse(final Reader reader) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(reader, STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(reader, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        public static STSignedTwipsMeasure parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        public static STSignedTwipsMeasure parse(final Node node) throws XmlException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(node, STSignedTwipsMeasure.type, null);
        }
        
        public static STSignedTwipsMeasure parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(node, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static STSignedTwipsMeasure parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSignedTwipsMeasure.type, null);
        }
        
        @Deprecated
        public static STSignedTwipsMeasure parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STSignedTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSignedTwipsMeasure.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSignedTwipsMeasure.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
