// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTTblBorders extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTTblBorders.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("cttblborders459ftype");
    
    CTBorder getTop();
    
    boolean isSetTop();
    
    void setTop(final CTBorder p0);
    
    CTBorder addNewTop();
    
    void unsetTop();
    
    CTBorder getLeft();
    
    boolean isSetLeft();
    
    void setLeft(final CTBorder p0);
    
    CTBorder addNewLeft();
    
    void unsetLeft();
    
    CTBorder getBottom();
    
    boolean isSetBottom();
    
    void setBottom(final CTBorder p0);
    
    CTBorder addNewBottom();
    
    void unsetBottom();
    
    CTBorder getRight();
    
    boolean isSetRight();
    
    void setRight(final CTBorder p0);
    
    CTBorder addNewRight();
    
    void unsetRight();
    
    CTBorder getInsideH();
    
    boolean isSetInsideH();
    
    void setInsideH(final CTBorder p0);
    
    CTBorder addNewInsideH();
    
    void unsetInsideH();
    
    CTBorder getInsideV();
    
    boolean isSetInsideV();
    
    void setInsideV(final CTBorder p0);
    
    CTBorder addNewInsideV();
    
    void unsetInsideV();
    
    public static final class Factory
    {
        public static CTTblBorders newInstance() {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().newInstance(CTTblBorders.type, null);
        }
        
        public static CTTblBorders newInstance(final XmlOptions xmlOptions) {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().newInstance(CTTblBorders.type, xmlOptions);
        }
        
        public static CTTblBorders parse(final String s) throws XmlException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(s, CTTblBorders.type, null);
        }
        
        public static CTTblBorders parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(s, CTTblBorders.type, xmlOptions);
        }
        
        public static CTTblBorders parse(final File file) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(file, CTTblBorders.type, null);
        }
        
        public static CTTblBorders parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(file, CTTblBorders.type, xmlOptions);
        }
        
        public static CTTblBorders parse(final URL url) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(url, CTTblBorders.type, null);
        }
        
        public static CTTblBorders parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(url, CTTblBorders.type, xmlOptions);
        }
        
        public static CTTblBorders parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(inputStream, CTTblBorders.type, null);
        }
        
        public static CTTblBorders parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(inputStream, CTTblBorders.type, xmlOptions);
        }
        
        public static CTTblBorders parse(final Reader reader) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(reader, CTTblBorders.type, null);
        }
        
        public static CTTblBorders parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(reader, CTTblBorders.type, xmlOptions);
        }
        
        public static CTTblBorders parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTblBorders.type, null);
        }
        
        public static CTTblBorders parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTTblBorders.type, xmlOptions);
        }
        
        public static CTTblBorders parse(final Node node) throws XmlException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(node, CTTblBorders.type, null);
        }
        
        public static CTTblBorders parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(node, CTTblBorders.type, xmlOptions);
        }
        
        @Deprecated
        public static CTTblBorders parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTblBorders.type, null);
        }
        
        @Deprecated
        public static CTTblBorders parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTTblBorders)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTTblBorders.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTblBorders.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTTblBorders.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
