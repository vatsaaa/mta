// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTFootnotes extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTFootnotes.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfootnotes691ftype");
    
    List<CTFtnEdn> getFootnoteList();
    
    @Deprecated
    CTFtnEdn[] getFootnoteArray();
    
    CTFtnEdn getFootnoteArray(final int p0);
    
    int sizeOfFootnoteArray();
    
    void setFootnoteArray(final CTFtnEdn[] p0);
    
    void setFootnoteArray(final int p0, final CTFtnEdn p1);
    
    CTFtnEdn insertNewFootnote(final int p0);
    
    CTFtnEdn addNewFootnote();
    
    void removeFootnote(final int p0);
    
    public static final class Factory
    {
        public static CTFootnotes newInstance() {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().newInstance(CTFootnotes.type, null);
        }
        
        public static CTFootnotes newInstance(final XmlOptions xmlOptions) {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().newInstance(CTFootnotes.type, xmlOptions);
        }
        
        public static CTFootnotes parse(final String s) throws XmlException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(s, CTFootnotes.type, null);
        }
        
        public static CTFootnotes parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(s, CTFootnotes.type, xmlOptions);
        }
        
        public static CTFootnotes parse(final File file) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(file, CTFootnotes.type, null);
        }
        
        public static CTFootnotes parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(file, CTFootnotes.type, xmlOptions);
        }
        
        public static CTFootnotes parse(final URL url) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(url, CTFootnotes.type, null);
        }
        
        public static CTFootnotes parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(url, CTFootnotes.type, xmlOptions);
        }
        
        public static CTFootnotes parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(inputStream, CTFootnotes.type, null);
        }
        
        public static CTFootnotes parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(inputStream, CTFootnotes.type, xmlOptions);
        }
        
        public static CTFootnotes parse(final Reader reader) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(reader, CTFootnotes.type, null);
        }
        
        public static CTFootnotes parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(reader, CTFootnotes.type, xmlOptions);
        }
        
        public static CTFootnotes parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFootnotes.type, null);
        }
        
        public static CTFootnotes parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFootnotes.type, xmlOptions);
        }
        
        public static CTFootnotes parse(final Node node) throws XmlException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(node, CTFootnotes.type, null);
        }
        
        public static CTFootnotes parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(node, CTFootnotes.type, xmlOptions);
        }
        
        @Deprecated
        public static CTFootnotes parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFootnotes.type, null);
        }
        
        @Deprecated
        public static CTFootnotes parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTFootnotes)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFootnotes.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFootnotes.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFootnotes.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
