// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.math.BigInteger;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTDecimalNumber extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTDecimalNumber.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctdecimalnumbera518type");
    
    BigInteger getVal();
    
    STDecimalNumber xgetVal();
    
    void setVal(final BigInteger p0);
    
    void xsetVal(final STDecimalNumber p0);
    
    public static final class Factory
    {
        public static CTDecimalNumber newInstance() {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().newInstance(CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber newInstance(final XmlOptions xmlOptions) {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().newInstance(CTDecimalNumber.type, xmlOptions);
        }
        
        public static CTDecimalNumber parse(final String s) throws XmlException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(s, CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(s, CTDecimalNumber.type, xmlOptions);
        }
        
        public static CTDecimalNumber parse(final File file) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(file, CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(file, CTDecimalNumber.type, xmlOptions);
        }
        
        public static CTDecimalNumber parse(final URL url) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(url, CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(url, CTDecimalNumber.type, xmlOptions);
        }
        
        public static CTDecimalNumber parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(inputStream, CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(inputStream, CTDecimalNumber.type, xmlOptions);
        }
        
        public static CTDecimalNumber parse(final Reader reader) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(reader, CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(reader, CTDecimalNumber.type, xmlOptions);
        }
        
        public static CTDecimalNumber parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTDecimalNumber.type, xmlOptions);
        }
        
        public static CTDecimalNumber parse(final Node node) throws XmlException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(node, CTDecimalNumber.type, null);
        }
        
        public static CTDecimalNumber parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(node, CTDecimalNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static CTDecimalNumber parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDecimalNumber.type, null);
        }
        
        @Deprecated
        public static CTDecimalNumber parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTDecimalNumber)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTDecimalNumber.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDecimalNumber.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTDecimalNumber.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
