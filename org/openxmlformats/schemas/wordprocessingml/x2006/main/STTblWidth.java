// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.StringEnumAbstractBase;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STTblWidth extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTblWidth.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttblwidth3a30type");
    public static final Enum NIL = Enum.forString("nil");
    public static final Enum PCT = Enum.forString("pct");
    public static final Enum DXA = Enum.forString("dxa");
    public static final Enum AUTO = Enum.forString("auto");
    public static final int INT_NIL = 1;
    public static final int INT_PCT = 2;
    public static final int INT_DXA = 3;
    public static final int INT_AUTO = 4;
    
    StringEnumAbstractBase enumValue();
    
    void set(final StringEnumAbstractBase p0);
    
    public static final class Factory
    {
        public static STTblWidth newValue(final Object o) {
            return (STTblWidth)STTblWidth.type.newValue(o);
        }
        
        public static STTblWidth newInstance() {
            return (STTblWidth)XmlBeans.getContextTypeLoader().newInstance(STTblWidth.type, null);
        }
        
        public static STTblWidth newInstance(final XmlOptions xmlOptions) {
            return (STTblWidth)XmlBeans.getContextTypeLoader().newInstance(STTblWidth.type, xmlOptions);
        }
        
        public static STTblWidth parse(final String s) throws XmlException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(s, STTblWidth.type, null);
        }
        
        public static STTblWidth parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(s, STTblWidth.type, xmlOptions);
        }
        
        public static STTblWidth parse(final File file) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(file, STTblWidth.type, null);
        }
        
        public static STTblWidth parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(file, STTblWidth.type, xmlOptions);
        }
        
        public static STTblWidth parse(final URL url) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(url, STTblWidth.type, null);
        }
        
        public static STTblWidth parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(url, STTblWidth.type, xmlOptions);
        }
        
        public static STTblWidth parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(inputStream, STTblWidth.type, null);
        }
        
        public static STTblWidth parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(inputStream, STTblWidth.type, xmlOptions);
        }
        
        public static STTblWidth parse(final Reader reader) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(reader, STTblWidth.type, null);
        }
        
        public static STTblWidth parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(reader, STTblWidth.type, xmlOptions);
        }
        
        public static STTblWidth parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTblWidth.type, null);
        }
        
        public static STTblWidth parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTblWidth.type, xmlOptions);
        }
        
        public static STTblWidth parse(final Node node) throws XmlException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(node, STTblWidth.type, null);
        }
        
        public static STTblWidth parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(node, STTblWidth.type, xmlOptions);
        }
        
        @Deprecated
        public static STTblWidth parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTblWidth.type, null);
        }
        
        @Deprecated
        public static STTblWidth parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTblWidth)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTblWidth.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTblWidth.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTblWidth.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
    
    public static final class Enum extends StringEnumAbstractBase
    {
        static final int INT_NIL = 1;
        static final int INT_PCT = 2;
        static final int INT_DXA = 3;
        static final int INT_AUTO = 4;
        public static final Table table;
        private static final long serialVersionUID = 1L;
        
        public static Enum forString(final String s) {
            return (Enum)Enum.table.forString(s);
        }
        
        public static Enum forInt(final int i) {
            return (Enum)Enum.table.forInt(i);
        }
        
        private Enum(final String s, final int i) {
            super(s, i);
        }
        
        private Object readResolve() {
            return forInt(this.intValue());
        }
        
        static {
            table = new Table(new Enum[] { new Enum("nil", 1), new Enum("pct", 2), new Enum("dxa", 3), new Enum("auto", 4) });
        }
    }
}
