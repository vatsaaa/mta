// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInteger;

public interface STSignedHpsMeasure extends XmlInteger
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STSignedHpsMeasure.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stsignedhpsmeasure8e89type");
    
    public static final class Factory
    {
        public static STSignedHpsMeasure newValue(final Object o) {
            return (STSignedHpsMeasure)STSignedHpsMeasure.type.newValue(o);
        }
        
        public static STSignedHpsMeasure newInstance() {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().newInstance(STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure newInstance(final XmlOptions xmlOptions) {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().newInstance(STSignedHpsMeasure.type, xmlOptions);
        }
        
        public static STSignedHpsMeasure parse(final String s) throws XmlException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(s, STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(s, STSignedHpsMeasure.type, xmlOptions);
        }
        
        public static STSignedHpsMeasure parse(final File file) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(file, STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(file, STSignedHpsMeasure.type, xmlOptions);
        }
        
        public static STSignedHpsMeasure parse(final URL url) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(url, STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(url, STSignedHpsMeasure.type, xmlOptions);
        }
        
        public static STSignedHpsMeasure parse(final InputStream inputStream) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STSignedHpsMeasure.type, xmlOptions);
        }
        
        public static STSignedHpsMeasure parse(final Reader reader) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(reader, STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(reader, STSignedHpsMeasure.type, xmlOptions);
        }
        
        public static STSignedHpsMeasure parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STSignedHpsMeasure.type, xmlOptions);
        }
        
        public static STSignedHpsMeasure parse(final Node node) throws XmlException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(node, STSignedHpsMeasure.type, null);
        }
        
        public static STSignedHpsMeasure parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(node, STSignedHpsMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static STSignedHpsMeasure parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSignedHpsMeasure.type, null);
        }
        
        @Deprecated
        public static STSignedHpsMeasure parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STSignedHpsMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STSignedHpsMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSignedHpsMeasure.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STSignedHpsMeasure.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
