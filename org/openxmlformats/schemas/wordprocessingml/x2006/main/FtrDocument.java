// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface FtrDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(FtrDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ftre182doctype");
    
    CTHdrFtr getFtr();
    
    void setFtr(final CTHdrFtr p0);
    
    CTHdrFtr addNewFtr();
    
    public static final class Factory
    {
        public static FtrDocument newInstance() {
            return (FtrDocument)XmlBeans.getContextTypeLoader().newInstance(FtrDocument.type, null);
        }
        
        public static FtrDocument newInstance(final XmlOptions xmlOptions) {
            return (FtrDocument)XmlBeans.getContextTypeLoader().newInstance(FtrDocument.type, xmlOptions);
        }
        
        public static FtrDocument parse(final String s) throws XmlException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(s, FtrDocument.type, null);
        }
        
        public static FtrDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(s, FtrDocument.type, xmlOptions);
        }
        
        public static FtrDocument parse(final File file) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(file, FtrDocument.type, null);
        }
        
        public static FtrDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(file, FtrDocument.type, xmlOptions);
        }
        
        public static FtrDocument parse(final URL url) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(url, FtrDocument.type, null);
        }
        
        public static FtrDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(url, FtrDocument.type, xmlOptions);
        }
        
        public static FtrDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(inputStream, FtrDocument.type, null);
        }
        
        public static FtrDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(inputStream, FtrDocument.type, xmlOptions);
        }
        
        public static FtrDocument parse(final Reader reader) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(reader, FtrDocument.type, null);
        }
        
        public static FtrDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(reader, FtrDocument.type, xmlOptions);
        }
        
        public static FtrDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, FtrDocument.type, null);
        }
        
        public static FtrDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, FtrDocument.type, xmlOptions);
        }
        
        public static FtrDocument parse(final Node node) throws XmlException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(node, FtrDocument.type, null);
        }
        
        public static FtrDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(node, FtrDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static FtrDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, FtrDocument.type, null);
        }
        
        @Deprecated
        public static FtrDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (FtrDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, FtrDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, FtrDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, FtrDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
