// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTString extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTString.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctstring9c37type");
    
    String getVal();
    
    STString xgetVal();
    
    void setVal(final String p0);
    
    void xsetVal(final STString p0);
    
    public static final class Factory
    {
        public static CTString newInstance() {
            return (CTString)XmlBeans.getContextTypeLoader().newInstance(CTString.type, null);
        }
        
        public static CTString newInstance(final XmlOptions xmlOptions) {
            return (CTString)XmlBeans.getContextTypeLoader().newInstance(CTString.type, xmlOptions);
        }
        
        public static CTString parse(final String s) throws XmlException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(s, CTString.type, null);
        }
        
        public static CTString parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(s, CTString.type, xmlOptions);
        }
        
        public static CTString parse(final File file) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(file, CTString.type, null);
        }
        
        public static CTString parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(file, CTString.type, xmlOptions);
        }
        
        public static CTString parse(final URL url) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(url, CTString.type, null);
        }
        
        public static CTString parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(url, CTString.type, xmlOptions);
        }
        
        public static CTString parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(inputStream, CTString.type, null);
        }
        
        public static CTString parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(inputStream, CTString.type, xmlOptions);
        }
        
        public static CTString parse(final Reader reader) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(reader, CTString.type, null);
        }
        
        public static CTString parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(reader, CTString.type, xmlOptions);
        }
        
        public static CTString parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTString.type, null);
        }
        
        public static CTString parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTString.type, xmlOptions);
        }
        
        public static CTString parse(final Node node) throws XmlException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(node, CTString.type, null);
        }
        
        public static CTString parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(node, CTString.type, xmlOptions);
        }
        
        @Deprecated
        public static CTString parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTString.type, null);
        }
        
        @Deprecated
        public static CTString parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTString)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTString.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTString.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTString.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
