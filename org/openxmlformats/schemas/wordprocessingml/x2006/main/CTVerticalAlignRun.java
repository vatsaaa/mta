// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTVerticalAlignRun extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTVerticalAlignRun.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctverticalalignruncb8ctype");
    
    STVerticalAlignRun.Enum getVal();
    
    STVerticalAlignRun xgetVal();
    
    void setVal(final STVerticalAlignRun.Enum p0);
    
    void xsetVal(final STVerticalAlignRun p0);
    
    public static final class Factory
    {
        public static CTVerticalAlignRun newInstance() {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().newInstance(CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun newInstance(final XmlOptions xmlOptions) {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().newInstance(CTVerticalAlignRun.type, xmlOptions);
        }
        
        public static CTVerticalAlignRun parse(final String s) throws XmlException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(s, CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(s, CTVerticalAlignRun.type, xmlOptions);
        }
        
        public static CTVerticalAlignRun parse(final File file) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(file, CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(file, CTVerticalAlignRun.type, xmlOptions);
        }
        
        public static CTVerticalAlignRun parse(final URL url) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(url, CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(url, CTVerticalAlignRun.type, xmlOptions);
        }
        
        public static CTVerticalAlignRun parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(inputStream, CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(inputStream, CTVerticalAlignRun.type, xmlOptions);
        }
        
        public static CTVerticalAlignRun parse(final Reader reader) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(reader, CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(reader, CTVerticalAlignRun.type, xmlOptions);
        }
        
        public static CTVerticalAlignRun parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVerticalAlignRun.type, xmlOptions);
        }
        
        public static CTVerticalAlignRun parse(final Node node) throws XmlException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(node, CTVerticalAlignRun.type, null);
        }
        
        public static CTVerticalAlignRun parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(node, CTVerticalAlignRun.type, xmlOptions);
        }
        
        @Deprecated
        public static CTVerticalAlignRun parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVerticalAlignRun.type, null);
        }
        
        @Deprecated
        public static CTVerticalAlignRun parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTVerticalAlignRun)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVerticalAlignRun.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVerticalAlignRun.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVerticalAlignRun.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
