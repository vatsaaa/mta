// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;

public interface STTwipsMeasure extends STUnsignedDecimalNumber
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STTwipsMeasure.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("sttwipsmeasure1e23type");
    
    public static final class Factory
    {
        public static STTwipsMeasure newValue(final Object o) {
            return (STTwipsMeasure)STTwipsMeasure.type.newValue(o);
        }
        
        public static STTwipsMeasure newInstance() {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().newInstance(STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure newInstance(final XmlOptions xmlOptions) {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().newInstance(STTwipsMeasure.type, xmlOptions);
        }
        
        public static STTwipsMeasure parse(final String s) throws XmlException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(s, STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(s, STTwipsMeasure.type, xmlOptions);
        }
        
        public static STTwipsMeasure parse(final File file) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(file, STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(file, STTwipsMeasure.type, xmlOptions);
        }
        
        public static STTwipsMeasure parse(final URL url) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(url, STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(url, STTwipsMeasure.type, xmlOptions);
        }
        
        public static STTwipsMeasure parse(final InputStream inputStream) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(inputStream, STTwipsMeasure.type, xmlOptions);
        }
        
        public static STTwipsMeasure parse(final Reader reader) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(reader, STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(reader, STTwipsMeasure.type, xmlOptions);
        }
        
        public static STTwipsMeasure parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STTwipsMeasure.type, xmlOptions);
        }
        
        public static STTwipsMeasure parse(final Node node) throws XmlException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(node, STTwipsMeasure.type, null);
        }
        
        public static STTwipsMeasure parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(node, STTwipsMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static STTwipsMeasure parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTwipsMeasure.type, null);
        }
        
        @Deprecated
        public static STTwipsMeasure parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STTwipsMeasure)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STTwipsMeasure.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTwipsMeasure.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STTwipsMeasure.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
