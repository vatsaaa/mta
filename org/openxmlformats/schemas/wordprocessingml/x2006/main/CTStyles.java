// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTStyles extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTStyles.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctstyles8506type");
    
    CTDocDefaults getDocDefaults();
    
    boolean isSetDocDefaults();
    
    void setDocDefaults(final CTDocDefaults p0);
    
    CTDocDefaults addNewDocDefaults();
    
    void unsetDocDefaults();
    
    CTLatentStyles getLatentStyles();
    
    boolean isSetLatentStyles();
    
    void setLatentStyles(final CTLatentStyles p0);
    
    CTLatentStyles addNewLatentStyles();
    
    void unsetLatentStyles();
    
    List<CTStyle> getStyleList();
    
    @Deprecated
    CTStyle[] getStyleArray();
    
    CTStyle getStyleArray(final int p0);
    
    int sizeOfStyleArray();
    
    void setStyleArray(final CTStyle[] p0);
    
    void setStyleArray(final int p0, final CTStyle p1);
    
    CTStyle insertNewStyle(final int p0);
    
    CTStyle addNewStyle();
    
    void removeStyle(final int p0);
    
    public static final class Factory
    {
        public static CTStyles newInstance() {
            return (CTStyles)XmlBeans.getContextTypeLoader().newInstance(CTStyles.type, null);
        }
        
        public static CTStyles newInstance(final XmlOptions xmlOptions) {
            return (CTStyles)XmlBeans.getContextTypeLoader().newInstance(CTStyles.type, xmlOptions);
        }
        
        public static CTStyles parse(final String s) throws XmlException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(s, CTStyles.type, null);
        }
        
        public static CTStyles parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(s, CTStyles.type, xmlOptions);
        }
        
        public static CTStyles parse(final File file) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(file, CTStyles.type, null);
        }
        
        public static CTStyles parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(file, CTStyles.type, xmlOptions);
        }
        
        public static CTStyles parse(final URL url) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(url, CTStyles.type, null);
        }
        
        public static CTStyles parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(url, CTStyles.type, xmlOptions);
        }
        
        public static CTStyles parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(inputStream, CTStyles.type, null);
        }
        
        public static CTStyles parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(inputStream, CTStyles.type, xmlOptions);
        }
        
        public static CTStyles parse(final Reader reader) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(reader, CTStyles.type, null);
        }
        
        public static CTStyles parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(reader, CTStyles.type, xmlOptions);
        }
        
        public static CTStyles parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStyles.type, null);
        }
        
        public static CTStyles parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTStyles.type, xmlOptions);
        }
        
        public static CTStyles parse(final Node node) throws XmlException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(node, CTStyles.type, null);
        }
        
        public static CTStyles parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(node, CTStyles.type, xmlOptions);
        }
        
        @Deprecated
        public static CTStyles parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStyles.type, null);
        }
        
        @Deprecated
        public static CTStyles parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTStyles)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTStyles.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStyles.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTStyles.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
