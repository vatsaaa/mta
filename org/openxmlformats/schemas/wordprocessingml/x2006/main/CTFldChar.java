// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTFldChar extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTFldChar.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctfldchare83etype");
    
    CTText getFldData();
    
    boolean isSetFldData();
    
    void setFldData(final CTText p0);
    
    CTText addNewFldData();
    
    void unsetFldData();
    
    CTFFData getFfData();
    
    boolean isSetFfData();
    
    void setFfData(final CTFFData p0);
    
    CTFFData addNewFfData();
    
    void unsetFfData();
    
    CTTrackChangeNumbering getNumberingChange();
    
    boolean isSetNumberingChange();
    
    void setNumberingChange(final CTTrackChangeNumbering p0);
    
    CTTrackChangeNumbering addNewNumberingChange();
    
    void unsetNumberingChange();
    
    STFldCharType.Enum getFldCharType();
    
    STFldCharType xgetFldCharType();
    
    void setFldCharType(final STFldCharType.Enum p0);
    
    void xsetFldCharType(final STFldCharType p0);
    
    STOnOff.Enum getFldLock();
    
    STOnOff xgetFldLock();
    
    boolean isSetFldLock();
    
    void setFldLock(final STOnOff.Enum p0);
    
    void xsetFldLock(final STOnOff p0);
    
    void unsetFldLock();
    
    STOnOff.Enum getDirty();
    
    STOnOff xgetDirty();
    
    boolean isSetDirty();
    
    void setDirty(final STOnOff.Enum p0);
    
    void xsetDirty(final STOnOff p0);
    
    void unsetDirty();
    
    public static final class Factory
    {
        public static CTFldChar newInstance() {
            return (CTFldChar)XmlBeans.getContextTypeLoader().newInstance(CTFldChar.type, null);
        }
        
        public static CTFldChar newInstance(final XmlOptions xmlOptions) {
            return (CTFldChar)XmlBeans.getContextTypeLoader().newInstance(CTFldChar.type, xmlOptions);
        }
        
        public static CTFldChar parse(final String s) throws XmlException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(s, CTFldChar.type, null);
        }
        
        public static CTFldChar parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(s, CTFldChar.type, xmlOptions);
        }
        
        public static CTFldChar parse(final File file) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(file, CTFldChar.type, null);
        }
        
        public static CTFldChar parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(file, CTFldChar.type, xmlOptions);
        }
        
        public static CTFldChar parse(final URL url) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(url, CTFldChar.type, null);
        }
        
        public static CTFldChar parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(url, CTFldChar.type, xmlOptions);
        }
        
        public static CTFldChar parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(inputStream, CTFldChar.type, null);
        }
        
        public static CTFldChar parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(inputStream, CTFldChar.type, xmlOptions);
        }
        
        public static CTFldChar parse(final Reader reader) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(reader, CTFldChar.type, null);
        }
        
        public static CTFldChar parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(reader, CTFldChar.type, xmlOptions);
        }
        
        public static CTFldChar parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFldChar.type, null);
        }
        
        public static CTFldChar parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTFldChar.type, xmlOptions);
        }
        
        public static CTFldChar parse(final Node node) throws XmlException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(node, CTFldChar.type, null);
        }
        
        public static CTFldChar parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(node, CTFldChar.type, xmlOptions);
        }
        
        @Deprecated
        public static CTFldChar parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFldChar.type, null);
        }
        
        @Deprecated
        public static CTFldChar parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTFldChar)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTFldChar.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFldChar.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTFldChar.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
