// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface NumberingDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(NumberingDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("numbering1c4ddoctype");
    
    CTNumbering getNumbering();
    
    void setNumbering(final CTNumbering p0);
    
    CTNumbering addNewNumbering();
    
    public static final class Factory
    {
        public static NumberingDocument newInstance() {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().newInstance(NumberingDocument.type, null);
        }
        
        public static NumberingDocument newInstance(final XmlOptions xmlOptions) {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().newInstance(NumberingDocument.type, xmlOptions);
        }
        
        public static NumberingDocument parse(final String s) throws XmlException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(s, NumberingDocument.type, null);
        }
        
        public static NumberingDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(s, NumberingDocument.type, xmlOptions);
        }
        
        public static NumberingDocument parse(final File file) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(file, NumberingDocument.type, null);
        }
        
        public static NumberingDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(file, NumberingDocument.type, xmlOptions);
        }
        
        public static NumberingDocument parse(final URL url) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(url, NumberingDocument.type, null);
        }
        
        public static NumberingDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(url, NumberingDocument.type, xmlOptions);
        }
        
        public static NumberingDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(inputStream, NumberingDocument.type, null);
        }
        
        public static NumberingDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(inputStream, NumberingDocument.type, xmlOptions);
        }
        
        public static NumberingDocument parse(final Reader reader) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(reader, NumberingDocument.type, null);
        }
        
        public static NumberingDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(reader, NumberingDocument.type, xmlOptions);
        }
        
        public static NumberingDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, NumberingDocument.type, null);
        }
        
        public static NumberingDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, NumberingDocument.type, xmlOptions);
        }
        
        public static NumberingDocument parse(final Node node) throws XmlException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(node, NumberingDocument.type, null);
        }
        
        public static NumberingDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(node, NumberingDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static NumberingDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, NumberingDocument.type, null);
        }
        
        @Deprecated
        public static NumberingDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (NumberingDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, NumberingDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, NumberingDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, NumberingDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
