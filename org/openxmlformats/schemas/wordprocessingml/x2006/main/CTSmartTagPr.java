// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.wordprocessingml.x2006.main;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTSmartTagPr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTSmartTagPr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctsmarttagprf715type");
    
    List<CTAttr> getAttrList();
    
    @Deprecated
    CTAttr[] getAttrArray();
    
    CTAttr getAttrArray(final int p0);
    
    int sizeOfAttrArray();
    
    void setAttrArray(final CTAttr[] p0);
    
    void setAttrArray(final int p0, final CTAttr p1);
    
    CTAttr insertNewAttr(final int p0);
    
    CTAttr addNewAttr();
    
    void removeAttr(final int p0);
    
    public static final class Factory
    {
        public static CTSmartTagPr newInstance() {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().newInstance(CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr newInstance(final XmlOptions xmlOptions) {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().newInstance(CTSmartTagPr.type, xmlOptions);
        }
        
        public static CTSmartTagPr parse(final String s) throws XmlException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(s, CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(s, CTSmartTagPr.type, xmlOptions);
        }
        
        public static CTSmartTagPr parse(final File file) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(file, CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(file, CTSmartTagPr.type, xmlOptions);
        }
        
        public static CTSmartTagPr parse(final URL url) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(url, CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(url, CTSmartTagPr.type, xmlOptions);
        }
        
        public static CTSmartTagPr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(inputStream, CTSmartTagPr.type, xmlOptions);
        }
        
        public static CTSmartTagPr parse(final Reader reader) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(reader, CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(reader, CTSmartTagPr.type, xmlOptions);
        }
        
        public static CTSmartTagPr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTSmartTagPr.type, xmlOptions);
        }
        
        public static CTSmartTagPr parse(final Node node) throws XmlException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(node, CTSmartTagPr.type, null);
        }
        
        public static CTSmartTagPr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(node, CTSmartTagPr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTSmartTagPr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSmartTagPr.type, null);
        }
        
        @Deprecated
        public static CTSmartTagPr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTSmartTagPr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTSmartTagPr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSmartTagPr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTSmartTagPr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
