// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.relationships.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STRelationshipIdImpl extends JavaStringHolderEx implements STRelationshipId
{
    public STRelationshipIdImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STRelationshipIdImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
