// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.relationships;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STRelationshipId extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STRelationshipId.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("strelationshipid1e94type");
    
    public static final class Factory
    {
        public static STRelationshipId newValue(final Object o) {
            return (STRelationshipId)STRelationshipId.type.newValue(o);
        }
        
        public static STRelationshipId newInstance() {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().newInstance(STRelationshipId.type, null);
        }
        
        public static STRelationshipId newInstance(final XmlOptions xmlOptions) {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().newInstance(STRelationshipId.type, xmlOptions);
        }
        
        public static STRelationshipId parse(final String s) throws XmlException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(s, STRelationshipId.type, null);
        }
        
        public static STRelationshipId parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(s, STRelationshipId.type, xmlOptions);
        }
        
        public static STRelationshipId parse(final File file) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(file, STRelationshipId.type, null);
        }
        
        public static STRelationshipId parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(file, STRelationshipId.type, xmlOptions);
        }
        
        public static STRelationshipId parse(final URL url) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(url, STRelationshipId.type, null);
        }
        
        public static STRelationshipId parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(url, STRelationshipId.type, xmlOptions);
        }
        
        public static STRelationshipId parse(final InputStream inputStream) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(inputStream, STRelationshipId.type, null);
        }
        
        public static STRelationshipId parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(inputStream, STRelationshipId.type, xmlOptions);
        }
        
        public static STRelationshipId parse(final Reader reader) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(reader, STRelationshipId.type, null);
        }
        
        public static STRelationshipId parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(reader, STRelationshipId.type, xmlOptions);
        }
        
        public static STRelationshipId parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STRelationshipId.type, null);
        }
        
        public static STRelationshipId parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STRelationshipId.type, xmlOptions);
        }
        
        public static STRelationshipId parse(final Node node) throws XmlException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(node, STRelationshipId.type, null);
        }
        
        public static STRelationshipId parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(node, STRelationshipId.type, xmlOptions);
        }
        
        @Deprecated
        public static STRelationshipId parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STRelationshipId.type, null);
        }
        
        @Deprecated
        public static STRelationshipId parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STRelationshipId)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STRelationshipId.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STRelationshipId.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STRelationshipId.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
