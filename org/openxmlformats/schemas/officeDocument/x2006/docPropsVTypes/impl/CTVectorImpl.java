// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.impl;

import org.apache.xmlbeans.StringEnumAbstractBase;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.STVectorBaseType;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.CTCf;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.STClsid;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.STError;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.STCy;
import org.apache.xmlbeans.XmlBoolean;
import org.apache.xmlbeans.XmlDateTime;
import java.util.Calendar;
import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.XmlDouble;
import org.apache.xmlbeans.XmlFloat;
import org.apache.xmlbeans.XmlUnsignedLong;
import java.math.BigInteger;
import org.apache.xmlbeans.XmlUnsignedInt;
import org.apache.xmlbeans.XmlUnsignedShort;
import org.apache.xmlbeans.XmlUnsignedByte;
import org.apache.xmlbeans.XmlLong;
import org.apache.xmlbeans.XmlInt;
import org.apache.xmlbeans.XmlShort;
import org.apache.xmlbeans.XmlByte;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.CTVariant;
import java.util.List;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.CTVector;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class CTVectorImpl extends XmlComplexContentImpl implements CTVector
{
    private static final QName VARIANT$0;
    private static final QName I1$2;
    private static final QName I2$4;
    private static final QName I4$6;
    private static final QName I8$8;
    private static final QName UI1$10;
    private static final QName UI2$12;
    private static final QName UI4$14;
    private static final QName UI8$16;
    private static final QName R4$18;
    private static final QName R8$20;
    private static final QName LPSTR$22;
    private static final QName LPWSTR$24;
    private static final QName BSTR$26;
    private static final QName DATE$28;
    private static final QName FILETIME$30;
    private static final QName BOOL$32;
    private static final QName CY$34;
    private static final QName ERROR$36;
    private static final QName CLSID$38;
    private static final QName CF$40;
    private static final QName BASETYPE$42;
    private static final QName SIZE$44;
    
    public CTVectorImpl(final SchemaType type) {
        super(type);
    }
    
    public List<CTVariant> getVariantList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTVariant>)new CTVectorImpl.VariantList(this);
        }
    }
    
    public CTVariant[] getVariantArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.VARIANT$0, list);
            final CTVariant[] array = new CTVariant[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTVariant getVariantArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTVariant ctVariant = (CTVariant)this.get_store().find_element_user(CTVectorImpl.VARIANT$0, n);
            if (ctVariant == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctVariant;
        }
    }
    
    public int sizeOfVariantArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.VARIANT$0);
        }
    }
    
    public void setVariantArray(final CTVariant[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.VARIANT$0);
        }
    }
    
    public void setVariantArray(final int n, final CTVariant ctVariant) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTVariant ctVariant2 = (CTVariant)this.get_store().find_element_user(CTVectorImpl.VARIANT$0, n);
            if (ctVariant2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctVariant2.set(ctVariant);
        }
    }
    
    public CTVariant insertNewVariant(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTVariant)this.get_store().insert_element_user(CTVectorImpl.VARIANT$0, n);
        }
    }
    
    public CTVariant addNewVariant() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTVariant)this.get_store().add_element_user(CTVectorImpl.VARIANT$0);
        }
    }
    
    public void removeVariant(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.VARIANT$0, n);
        }
    }
    
    public List<Byte> getI1List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Byte>)new CTVectorImpl.I1List(this);
        }
    }
    
    public byte[] getI1Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.I1$2, list);
            final byte[] array = new byte[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getByteValue();
            }
            return array;
        }
    }
    
    public byte getI1Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I1$2, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getByteValue();
        }
    }
    
    public List<XmlByte> xgetI1List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlByte>)new CTVectorImpl.I1List(this);
        }
    }
    
    public XmlByte[] xgetI1Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.I1$2, list);
            final XmlByte[] array = new XmlByte[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlByte xgetI1Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlByte xmlByte = (XmlByte)this.get_store().find_element_user(CTVectorImpl.I1$2, n);
            if (xmlByte == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlByte;
        }
    }
    
    public int sizeOfI1Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.I1$2);
        }
    }
    
    public void setI1Array(final byte[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I1$2);
        }
    }
    
    public void setI1Array(final int n, final byte byteValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I1$2, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setByteValue(byteValue);
        }
    }
    
    public void xsetI1Array(final XmlByte[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I1$2);
        }
    }
    
    public void xsetI1Array(final int n, final XmlByte xmlByte) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlByte xmlByte2 = (XmlByte)this.get_store().find_element_user(CTVectorImpl.I1$2, n);
            if (xmlByte2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlByte2.set(xmlByte);
        }
    }
    
    public void insertI1(final int n, final byte byteValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.I1$2, n)).setByteValue(byteValue);
        }
    }
    
    public void addI1(final byte byteValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.I1$2)).setByteValue(byteValue);
        }
    }
    
    public XmlByte insertNewI1(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlByte)this.get_store().insert_element_user(CTVectorImpl.I1$2, n);
        }
    }
    
    public XmlByte addNewI1() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlByte)this.get_store().add_element_user(CTVectorImpl.I1$2);
        }
    }
    
    public void removeI1(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.I1$2, n);
        }
    }
    
    public List<Short> getI2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Short>)new CTVectorImpl.I2List(this);
        }
    }
    
    public short[] getI2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.I2$4, list);
            final short[] array = new short[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getShortValue();
            }
            return array;
        }
    }
    
    public short getI2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I2$4, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getShortValue();
        }
    }
    
    public List<XmlShort> xgetI2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlShort>)new CTVectorImpl.I2List(this);
        }
    }
    
    public XmlShort[] xgetI2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.I2$4, list);
            final XmlShort[] array = new XmlShort[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlShort xgetI2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlShort xmlShort = (XmlShort)this.get_store().find_element_user(CTVectorImpl.I2$4, n);
            if (xmlShort == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlShort;
        }
    }
    
    public int sizeOfI2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.I2$4);
        }
    }
    
    public void setI2Array(final short[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I2$4);
        }
    }
    
    public void setI2Array(final int n, final short shortValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I2$4, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setShortValue(shortValue);
        }
    }
    
    public void xsetI2Array(final XmlShort[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I2$4);
        }
    }
    
    public void xsetI2Array(final int n, final XmlShort xmlShort) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlShort xmlShort2 = (XmlShort)this.get_store().find_element_user(CTVectorImpl.I2$4, n);
            if (xmlShort2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlShort2.set(xmlShort);
        }
    }
    
    public void insertI2(final int n, final short shortValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.I2$4, n)).setShortValue(shortValue);
        }
    }
    
    public void addI2(final short shortValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.I2$4)).setShortValue(shortValue);
        }
    }
    
    public XmlShort insertNewI2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlShort)this.get_store().insert_element_user(CTVectorImpl.I2$4, n);
        }
    }
    
    public XmlShort addNewI2() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlShort)this.get_store().add_element_user(CTVectorImpl.I2$4);
        }
    }
    
    public void removeI2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.I2$4, n);
        }
    }
    
    public List<Integer> getI4List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Integer>)new CTVectorImpl.I4List(this);
        }
    }
    
    public int[] getI4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.I4$6, list);
            final int[] array = new int[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getIntValue();
            }
            return array;
        }
    }
    
    public int getI4Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I4$6, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getIntValue();
        }
    }
    
    public List<XmlInt> xgetI4List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlInt>)new CTVectorImpl.I4List(this);
        }
    }
    
    public XmlInt[] xgetI4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.I4$6, list);
            final XmlInt[] array = new XmlInt[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlInt xgetI4Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInt xmlInt = (XmlInt)this.get_store().find_element_user(CTVectorImpl.I4$6, n);
            if (xmlInt == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlInt;
        }
    }
    
    public int sizeOfI4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.I4$6);
        }
    }
    
    public void setI4Array(final int[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I4$6);
        }
    }
    
    public void setI4Array(final int n, final int intValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I4$6, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setIntValue(intValue);
        }
    }
    
    public void xsetI4Array(final XmlInt[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I4$6);
        }
    }
    
    public void xsetI4Array(final int n, final XmlInt xmlInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlInt xmlInt2 = (XmlInt)this.get_store().find_element_user(CTVectorImpl.I4$6, n);
            if (xmlInt2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlInt2.set(xmlInt);
        }
    }
    
    public void insertI4(final int n, final int intValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.I4$6, n)).setIntValue(intValue);
        }
    }
    
    public void addI4(final int intValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.I4$6)).setIntValue(intValue);
        }
    }
    
    public XmlInt insertNewI4(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInt)this.get_store().insert_element_user(CTVectorImpl.I4$6, n);
        }
    }
    
    public XmlInt addNewI4() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlInt)this.get_store().add_element_user(CTVectorImpl.I4$6);
        }
    }
    
    public void removeI4(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.I4$6, n);
        }
    }
    
    public List<Long> getI8List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Long>)new CTVectorImpl.I8List(this);
        }
    }
    
    public long[] getI8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.I8$8, list);
            final long[] array = new long[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getLongValue();
            }
            return array;
        }
    }
    
    public long getI8Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I8$8, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getLongValue();
        }
    }
    
    public List<XmlLong> xgetI8List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlLong>)new CTVectorImpl.I8List(this);
        }
    }
    
    public XmlLong[] xgetI8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.I8$8, list);
            final XmlLong[] array = new XmlLong[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlLong xgetI8Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlLong xmlLong = (XmlLong)this.get_store().find_element_user(CTVectorImpl.I8$8, n);
            if (xmlLong == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlLong;
        }
    }
    
    public int sizeOfI8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.I8$8);
        }
    }
    
    public void setI8Array(final long[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I8$8);
        }
    }
    
    public void setI8Array(final int n, final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.I8$8, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setLongValue(longValue);
        }
    }
    
    public void xsetI8Array(final XmlLong[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.I8$8);
        }
    }
    
    public void xsetI8Array(final int n, final XmlLong xmlLong) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlLong xmlLong2 = (XmlLong)this.get_store().find_element_user(CTVectorImpl.I8$8, n);
            if (xmlLong2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlLong2.set(xmlLong);
        }
    }
    
    public void insertI8(final int n, final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.I8$8, n)).setLongValue(longValue);
        }
    }
    
    public void addI8(final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.I8$8)).setLongValue(longValue);
        }
    }
    
    public XmlLong insertNewI8(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlLong)this.get_store().insert_element_user(CTVectorImpl.I8$8, n);
        }
    }
    
    public XmlLong addNewI8() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlLong)this.get_store().add_element_user(CTVectorImpl.I8$8);
        }
    }
    
    public void removeI8(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.I8$8, n);
        }
    }
    
    public List<Short> getUi1List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Short>)new CTVectorImpl.Ui1List(this);
        }
    }
    
    public short[] getUi1Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.UI1$10, list);
            final short[] array = new short[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getShortValue();
            }
            return array;
        }
    }
    
    public short getUi1Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI1$10, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getShortValue();
        }
    }
    
    public List<XmlUnsignedByte> xgetUi1List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlUnsignedByte>)new CTVectorImpl.Ui1List(this);
        }
    }
    
    public XmlUnsignedByte[] xgetUi1Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.UI1$10, list);
            final XmlUnsignedByte[] array = new XmlUnsignedByte[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlUnsignedByte xgetUi1Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedByte xmlUnsignedByte = (XmlUnsignedByte)this.get_store().find_element_user(CTVectorImpl.UI1$10, n);
            if (xmlUnsignedByte == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlUnsignedByte;
        }
    }
    
    public int sizeOfUi1Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.UI1$10);
        }
    }
    
    public void setUi1Array(final short[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI1$10);
        }
    }
    
    public void setUi1Array(final int n, final short shortValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI1$10, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setShortValue(shortValue);
        }
    }
    
    public void xsetUi1Array(final XmlUnsignedByte[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI1$10);
        }
    }
    
    public void xsetUi1Array(final int n, final XmlUnsignedByte xmlUnsignedByte) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedByte xmlUnsignedByte2 = (XmlUnsignedByte)this.get_store().find_element_user(CTVectorImpl.UI1$10, n);
            if (xmlUnsignedByte2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlUnsignedByte2.set(xmlUnsignedByte);
        }
    }
    
    public void insertUi1(final int n, final short shortValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.UI1$10, n)).setShortValue(shortValue);
        }
    }
    
    public void addUi1(final short shortValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.UI1$10)).setShortValue(shortValue);
        }
    }
    
    public XmlUnsignedByte insertNewUi1(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedByte)this.get_store().insert_element_user(CTVectorImpl.UI1$10, n);
        }
    }
    
    public XmlUnsignedByte addNewUi1() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedByte)this.get_store().add_element_user(CTVectorImpl.UI1$10);
        }
    }
    
    public void removeUi1(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.UI1$10, n);
        }
    }
    
    public List<Integer> getUi2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Integer>)new CTVectorImpl.Ui2List(this);
        }
    }
    
    public int[] getUi2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.UI2$12, list);
            final int[] array = new int[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getIntValue();
            }
            return array;
        }
    }
    
    public int getUi2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI2$12, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getIntValue();
        }
    }
    
    public List<XmlUnsignedShort> xgetUi2List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlUnsignedShort>)new CTVectorImpl.Ui2List(this);
        }
    }
    
    public XmlUnsignedShort[] xgetUi2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.UI2$12, list);
            final XmlUnsignedShort[] array = new XmlUnsignedShort[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlUnsignedShort xgetUi2Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedShort xmlUnsignedShort = (XmlUnsignedShort)this.get_store().find_element_user(CTVectorImpl.UI2$12, n);
            if (xmlUnsignedShort == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlUnsignedShort;
        }
    }
    
    public int sizeOfUi2Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.UI2$12);
        }
    }
    
    public void setUi2Array(final int[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI2$12);
        }
    }
    
    public void setUi2Array(final int n, final int intValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI2$12, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setIntValue(intValue);
        }
    }
    
    public void xsetUi2Array(final XmlUnsignedShort[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI2$12);
        }
    }
    
    public void xsetUi2Array(final int n, final XmlUnsignedShort xmlUnsignedShort) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedShort xmlUnsignedShort2 = (XmlUnsignedShort)this.get_store().find_element_user(CTVectorImpl.UI2$12, n);
            if (xmlUnsignedShort2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlUnsignedShort2.set(xmlUnsignedShort);
        }
    }
    
    public void insertUi2(final int n, final int intValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.UI2$12, n)).setIntValue(intValue);
        }
    }
    
    public void addUi2(final int intValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.UI2$12)).setIntValue(intValue);
        }
    }
    
    public XmlUnsignedShort insertNewUi2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedShort)this.get_store().insert_element_user(CTVectorImpl.UI2$12, n);
        }
    }
    
    public XmlUnsignedShort addNewUi2() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedShort)this.get_store().add_element_user(CTVectorImpl.UI2$12);
        }
    }
    
    public void removeUi2(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.UI2$12, n);
        }
    }
    
    public List<Long> getUi4List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Long>)new CTVectorImpl.Ui4List(this);
        }
    }
    
    public long[] getUi4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.UI4$14, list);
            final long[] array = new long[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getLongValue();
            }
            return array;
        }
    }
    
    public long getUi4Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI4$14, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getLongValue();
        }
    }
    
    public List<XmlUnsignedInt> xgetUi4List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlUnsignedInt>)new CTVectorImpl.Ui4List(this);
        }
    }
    
    public XmlUnsignedInt[] xgetUi4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.UI4$14, list);
            final XmlUnsignedInt[] array = new XmlUnsignedInt[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlUnsignedInt xgetUi4Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedInt xmlUnsignedInt = (XmlUnsignedInt)this.get_store().find_element_user(CTVectorImpl.UI4$14, n);
            if (xmlUnsignedInt == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlUnsignedInt;
        }
    }
    
    public int sizeOfUi4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.UI4$14);
        }
    }
    
    public void setUi4Array(final long[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI4$14);
        }
    }
    
    public void setUi4Array(final int n, final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI4$14, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setLongValue(longValue);
        }
    }
    
    public void xsetUi4Array(final XmlUnsignedInt[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI4$14);
        }
    }
    
    public void xsetUi4Array(final int n, final XmlUnsignedInt xmlUnsignedInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedInt xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().find_element_user(CTVectorImpl.UI4$14, n);
            if (xmlUnsignedInt2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlUnsignedInt2.set(xmlUnsignedInt);
        }
    }
    
    public void insertUi4(final int n, final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.UI4$14, n)).setLongValue(longValue);
        }
    }
    
    public void addUi4(final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.UI4$14)).setLongValue(longValue);
        }
    }
    
    public XmlUnsignedInt insertNewUi4(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedInt)this.get_store().insert_element_user(CTVectorImpl.UI4$14, n);
        }
    }
    
    public XmlUnsignedInt addNewUi4() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedInt)this.get_store().add_element_user(CTVectorImpl.UI4$14);
        }
    }
    
    public void removeUi4(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.UI4$14, n);
        }
    }
    
    public List<BigInteger> getUi8List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<BigInteger>)new CTVectorImpl.Ui8List(this);
        }
    }
    
    public BigInteger[] getUi8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.UI8$16, list);
            final BigInteger[] array = new BigInteger[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBigIntegerValue();
            }
            return array;
        }
    }
    
    public BigInteger getUi8Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI8$16, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBigIntegerValue();
        }
    }
    
    public List<XmlUnsignedLong> xgetUi8List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlUnsignedLong>)new CTVectorImpl.Ui8List(this);
        }
    }
    
    public XmlUnsignedLong[] xgetUi8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.UI8$16, list);
            final XmlUnsignedLong[] array = new XmlUnsignedLong[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlUnsignedLong xgetUi8Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedLong xmlUnsignedLong = (XmlUnsignedLong)this.get_store().find_element_user(CTVectorImpl.UI8$16, n);
            if (xmlUnsignedLong == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlUnsignedLong;
        }
    }
    
    public int sizeOfUi8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.UI8$16);
        }
    }
    
    public void setUi8Array(final BigInteger[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI8$16);
        }
    }
    
    public void setUi8Array(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.UI8$16, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void xsetUi8Array(final XmlUnsignedLong[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.UI8$16);
        }
    }
    
    public void xsetUi8Array(final int n, final XmlUnsignedLong xmlUnsignedLong) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlUnsignedLong xmlUnsignedLong2 = (XmlUnsignedLong)this.get_store().find_element_user(CTVectorImpl.UI8$16, n);
            if (xmlUnsignedLong2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlUnsignedLong2.set(xmlUnsignedLong);
        }
    }
    
    public void insertUi8(final int n, final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.UI8$16, n)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public void addUi8(final BigInteger bigIntegerValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.UI8$16)).setBigIntegerValue(bigIntegerValue);
        }
    }
    
    public XmlUnsignedLong insertNewUi8(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedLong)this.get_store().insert_element_user(CTVectorImpl.UI8$16, n);
        }
    }
    
    public XmlUnsignedLong addNewUi8() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedLong)this.get_store().add_element_user(CTVectorImpl.UI8$16);
        }
    }
    
    public void removeUi8(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.UI8$16, n);
        }
    }
    
    public List<Float> getR4List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Float>)new CTVectorImpl.R4List(this);
        }
    }
    
    public float[] getR4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.R4$18, list);
            final float[] array = new float[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getFloatValue();
            }
            return array;
        }
    }
    
    public float getR4Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.R4$18, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getFloatValue();
        }
    }
    
    public List<XmlFloat> xgetR4List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlFloat>)new CTVectorImpl.R4List(this);
        }
    }
    
    public XmlFloat[] xgetR4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.R4$18, list);
            final XmlFloat[] array = new XmlFloat[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlFloat xgetR4Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlFloat xmlFloat = (XmlFloat)this.get_store().find_element_user(CTVectorImpl.R4$18, n);
            if (xmlFloat == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlFloat;
        }
    }
    
    public int sizeOfR4Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.R4$18);
        }
    }
    
    public void setR4Array(final float[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.R4$18);
        }
    }
    
    public void setR4Array(final int n, final float floatValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.R4$18, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setFloatValue(floatValue);
        }
    }
    
    public void xsetR4Array(final XmlFloat[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.R4$18);
        }
    }
    
    public void xsetR4Array(final int n, final XmlFloat xmlFloat) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlFloat xmlFloat2 = (XmlFloat)this.get_store().find_element_user(CTVectorImpl.R4$18, n);
            if (xmlFloat2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlFloat2.set(xmlFloat);
        }
    }
    
    public void insertR4(final int n, final float floatValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.R4$18, n)).setFloatValue(floatValue);
        }
    }
    
    public void addR4(final float floatValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.R4$18)).setFloatValue(floatValue);
        }
    }
    
    public XmlFloat insertNewR4(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlFloat)this.get_store().insert_element_user(CTVectorImpl.R4$18, n);
        }
    }
    
    public XmlFloat addNewR4() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlFloat)this.get_store().add_element_user(CTVectorImpl.R4$18);
        }
    }
    
    public void removeR4(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.R4$18, n);
        }
    }
    
    public List<Double> getR8List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Double>)new CTVectorImpl.R8List(this);
        }
    }
    
    public double[] getR8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.R8$20, list);
            final double[] array = new double[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getDoubleValue();
            }
            return array;
        }
    }
    
    public double getR8Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.R8$20, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getDoubleValue();
        }
    }
    
    public List<XmlDouble> xgetR8List() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlDouble>)new CTVectorImpl.R8List(this);
        }
    }
    
    public XmlDouble[] xgetR8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.R8$20, list);
            final XmlDouble[] array = new XmlDouble[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlDouble xgetR8Array(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlDouble xmlDouble = (XmlDouble)this.get_store().find_element_user(CTVectorImpl.R8$20, n);
            if (xmlDouble == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlDouble;
        }
    }
    
    public int sizeOfR8Array() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.R8$20);
        }
    }
    
    public void setR8Array(final double[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.R8$20);
        }
    }
    
    public void setR8Array(final int n, final double doubleValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.R8$20, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setDoubleValue(doubleValue);
        }
    }
    
    public void xsetR8Array(final XmlDouble[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.R8$20);
        }
    }
    
    public void xsetR8Array(final int n, final XmlDouble xmlDouble) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlDouble xmlDouble2 = (XmlDouble)this.get_store().find_element_user(CTVectorImpl.R8$20, n);
            if (xmlDouble2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlDouble2.set(xmlDouble);
        }
    }
    
    public void insertR8(final int n, final double doubleValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.R8$20, n)).setDoubleValue(doubleValue);
        }
    }
    
    public void addR8(final double doubleValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.R8$20)).setDoubleValue(doubleValue);
        }
    }
    
    public XmlDouble insertNewR8(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlDouble)this.get_store().insert_element_user(CTVectorImpl.R8$20, n);
        }
    }
    
    public XmlDouble addNewR8() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlDouble)this.get_store().add_element_user(CTVectorImpl.R8$20);
        }
    }
    
    public void removeR8(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.R8$20, n);
        }
    }
    
    public List<String> getLpstrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTVectorImpl.LpstrList(this);
        }
    }
    
    public String[] getLpstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.LPSTR$22, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getLpstrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.LPSTR$22, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetLpstrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTVectorImpl.LpstrList(this);
        }
    }
    
    public XmlString[] xgetLpstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.LPSTR$22, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetLpstrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTVectorImpl.LPSTR$22, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfLpstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.LPSTR$22);
        }
    }
    
    public void setLpstrArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.LPSTR$22);
        }
    }
    
    public void setLpstrArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.LPSTR$22, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetLpstrArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.LPSTR$22);
        }
    }
    
    public void xsetLpstrArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTVectorImpl.LPSTR$22, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertLpstr(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.LPSTR$22, n)).setStringValue(stringValue);
        }
    }
    
    public void addLpstr(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.LPSTR$22)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewLpstr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTVectorImpl.LPSTR$22, n);
        }
    }
    
    public XmlString addNewLpstr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTVectorImpl.LPSTR$22);
        }
    }
    
    public void removeLpstr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.LPSTR$22, n);
        }
    }
    
    public List<String> getLpwstrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTVectorImpl.LpwstrList(this);
        }
    }
    
    public String[] getLpwstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.LPWSTR$24, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getLpwstrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.LPWSTR$24, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetLpwstrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTVectorImpl.LpwstrList(this);
        }
    }
    
    public XmlString[] xgetLpwstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.LPWSTR$24, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetLpwstrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTVectorImpl.LPWSTR$24, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfLpwstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.LPWSTR$24);
        }
    }
    
    public void setLpwstrArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.LPWSTR$24);
        }
    }
    
    public void setLpwstrArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.LPWSTR$24, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetLpwstrArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.LPWSTR$24);
        }
    }
    
    public void xsetLpwstrArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTVectorImpl.LPWSTR$24, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertLpwstr(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.LPWSTR$24, n)).setStringValue(stringValue);
        }
    }
    
    public void addLpwstr(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.LPWSTR$24)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewLpwstr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTVectorImpl.LPWSTR$24, n);
        }
    }
    
    public XmlString addNewLpwstr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTVectorImpl.LPWSTR$24);
        }
    }
    
    public void removeLpwstr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.LPWSTR$24, n);
        }
    }
    
    public List<String> getBstrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTVectorImpl.BstrList(this);
        }
    }
    
    public String[] getBstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.BSTR$26, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getBstrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.BSTR$26, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<XmlString> xgetBstrList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlString>)new CTVectorImpl.BstrList(this);
        }
    }
    
    public XmlString[] xgetBstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.BSTR$26, list);
            final XmlString[] array = new XmlString[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlString xgetBstrArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString = (XmlString)this.get_store().find_element_user(CTVectorImpl.BSTR$26, n);
            if (xmlString == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlString;
        }
    }
    
    public int sizeOfBstrArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.BSTR$26);
        }
    }
    
    public void setBstrArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.BSTR$26);
        }
    }
    
    public void setBstrArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.BSTR$26, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetBstrArray(final XmlString[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.BSTR$26);
        }
    }
    
    public void xsetBstrArray(final int n, final XmlString xmlString) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlString xmlString2 = (XmlString)this.get_store().find_element_user(CTVectorImpl.BSTR$26, n);
            if (xmlString2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlString2.set(xmlString);
        }
    }
    
    public void insertBstr(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.BSTR$26, n)).setStringValue(stringValue);
        }
    }
    
    public void addBstr(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.BSTR$26)).setStringValue(stringValue);
        }
    }
    
    public XmlString insertNewBstr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().insert_element_user(CTVectorImpl.BSTR$26, n);
        }
    }
    
    public XmlString addNewBstr() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlString)this.get_store().add_element_user(CTVectorImpl.BSTR$26);
        }
    }
    
    public void removeBstr(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.BSTR$26, n);
        }
    }
    
    public List<Calendar> getDateList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Calendar>)new CTVectorImpl.DateList(this);
        }
    }
    
    public Calendar[] getDateArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.DATE$28, list);
            final Calendar[] array = new Calendar[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getCalendarValue();
            }
            return array;
        }
    }
    
    public Calendar getDateArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.DATE$28, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getCalendarValue();
        }
    }
    
    public List<XmlDateTime> xgetDateList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlDateTime>)new CTVectorImpl.DateList(this);
        }
    }
    
    public XmlDateTime[] xgetDateArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.DATE$28, list);
            final XmlDateTime[] array = new XmlDateTime[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlDateTime xgetDateArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlDateTime xmlDateTime = (XmlDateTime)this.get_store().find_element_user(CTVectorImpl.DATE$28, n);
            if (xmlDateTime == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlDateTime;
        }
    }
    
    public int sizeOfDateArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.DATE$28);
        }
    }
    
    public void setDateArray(final Calendar[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.DATE$28);
        }
    }
    
    public void setDateArray(final int n, final Calendar calendarValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.DATE$28, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setCalendarValue(calendarValue);
        }
    }
    
    public void xsetDateArray(final XmlDateTime[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.DATE$28);
        }
    }
    
    public void xsetDateArray(final int n, final XmlDateTime xmlDateTime) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlDateTime xmlDateTime2 = (XmlDateTime)this.get_store().find_element_user(CTVectorImpl.DATE$28, n);
            if (xmlDateTime2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlDateTime2.set(xmlDateTime);
        }
    }
    
    public void insertDate(final int n, final Calendar calendarValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.DATE$28, n)).setCalendarValue(calendarValue);
        }
    }
    
    public void addDate(final Calendar calendarValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.DATE$28)).setCalendarValue(calendarValue);
        }
    }
    
    public XmlDateTime insertNewDate(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlDateTime)this.get_store().insert_element_user(CTVectorImpl.DATE$28, n);
        }
    }
    
    public XmlDateTime addNewDate() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlDateTime)this.get_store().add_element_user(CTVectorImpl.DATE$28);
        }
    }
    
    public void removeDate(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.DATE$28, n);
        }
    }
    
    public List<Calendar> getFiletimeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Calendar>)new CTVectorImpl.FiletimeList(this);
        }
    }
    
    public Calendar[] getFiletimeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.FILETIME$30, list);
            final Calendar[] array = new Calendar[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getCalendarValue();
            }
            return array;
        }
    }
    
    public Calendar getFiletimeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.FILETIME$30, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getCalendarValue();
        }
    }
    
    public List<XmlDateTime> xgetFiletimeList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlDateTime>)new CTVectorImpl.FiletimeList(this);
        }
    }
    
    public XmlDateTime[] xgetFiletimeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.FILETIME$30, list);
            final XmlDateTime[] array = new XmlDateTime[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlDateTime xgetFiletimeArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlDateTime xmlDateTime = (XmlDateTime)this.get_store().find_element_user(CTVectorImpl.FILETIME$30, n);
            if (xmlDateTime == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlDateTime;
        }
    }
    
    public int sizeOfFiletimeArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.FILETIME$30);
        }
    }
    
    public void setFiletimeArray(final Calendar[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.FILETIME$30);
        }
    }
    
    public void setFiletimeArray(final int n, final Calendar calendarValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.FILETIME$30, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setCalendarValue(calendarValue);
        }
    }
    
    public void xsetFiletimeArray(final XmlDateTime[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.FILETIME$30);
        }
    }
    
    public void xsetFiletimeArray(final int n, final XmlDateTime xmlDateTime) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlDateTime xmlDateTime2 = (XmlDateTime)this.get_store().find_element_user(CTVectorImpl.FILETIME$30, n);
            if (xmlDateTime2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlDateTime2.set(xmlDateTime);
        }
    }
    
    public void insertFiletime(final int n, final Calendar calendarValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.FILETIME$30, n)).setCalendarValue(calendarValue);
        }
    }
    
    public void addFiletime(final Calendar calendarValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.FILETIME$30)).setCalendarValue(calendarValue);
        }
    }
    
    public XmlDateTime insertNewFiletime(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlDateTime)this.get_store().insert_element_user(CTVectorImpl.FILETIME$30, n);
        }
    }
    
    public XmlDateTime addNewFiletime() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlDateTime)this.get_store().add_element_user(CTVectorImpl.FILETIME$30);
        }
    }
    
    public void removeFiletime(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.FILETIME$30, n);
        }
    }
    
    public List<Boolean> getBoolList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<Boolean>)new CTVectorImpl.BoolList(this);
        }
    }
    
    public boolean[] getBoolArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.BOOL$32, list);
            final boolean[] array = new boolean[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getBooleanValue();
            }
            return array;
        }
    }
    
    public boolean getBoolArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.BOOL$32, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getBooleanValue();
        }
    }
    
    public List<XmlBoolean> xgetBoolList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<XmlBoolean>)new CTVectorImpl.BoolList(this);
        }
    }
    
    public XmlBoolean[] xgetBoolArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.BOOL$32, list);
            final XmlBoolean[] array = new XmlBoolean[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public XmlBoolean xgetBoolArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlBoolean xmlBoolean = (XmlBoolean)this.get_store().find_element_user(CTVectorImpl.BOOL$32, n);
            if (xmlBoolean == null) {
                throw new IndexOutOfBoundsException();
            }
            return xmlBoolean;
        }
    }
    
    public int sizeOfBoolArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.BOOL$32);
        }
    }
    
    public void setBoolArray(final boolean[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.BOOL$32);
        }
    }
    
    public void setBoolArray(final int n, final boolean booleanValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.BOOL$32, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setBooleanValue(booleanValue);
        }
    }
    
    public void xsetBoolArray(final XmlBoolean[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.BOOL$32);
        }
    }
    
    public void xsetBoolArray(final int n, final XmlBoolean xmlBoolean) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final XmlBoolean xmlBoolean2 = (XmlBoolean)this.get_store().find_element_user(CTVectorImpl.BOOL$32, n);
            if (xmlBoolean2 == null) {
                throw new IndexOutOfBoundsException();
            }
            xmlBoolean2.set(xmlBoolean);
        }
    }
    
    public void insertBool(final int n, final boolean booleanValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.BOOL$32, n)).setBooleanValue(booleanValue);
        }
    }
    
    public void addBool(final boolean booleanValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.BOOL$32)).setBooleanValue(booleanValue);
        }
    }
    
    public XmlBoolean insertNewBool(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlBoolean)this.get_store().insert_element_user(CTVectorImpl.BOOL$32, n);
        }
    }
    
    public XmlBoolean addNewBool() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlBoolean)this.get_store().add_element_user(CTVectorImpl.BOOL$32);
        }
    }
    
    public void removeBool(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.BOOL$32, n);
        }
    }
    
    public List<String> getCyList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTVectorImpl.CyList(this);
        }
    }
    
    public String[] getCyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.CY$34, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getCyArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.CY$34, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<STCy> xgetCyList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STCy>)new CTVectorImpl.CyList(this);
        }
    }
    
    public STCy[] xgetCyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.CY$34, list);
            final STCy[] array = new STCy[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STCy xgetCyArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STCy stCy = (STCy)this.get_store().find_element_user(CTVectorImpl.CY$34, n);
            if (stCy == null) {
                throw new IndexOutOfBoundsException();
            }
            return stCy;
        }
    }
    
    public int sizeOfCyArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.CY$34);
        }
    }
    
    public void setCyArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.CY$34);
        }
    }
    
    public void setCyArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.CY$34, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetCyArray(final STCy[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTVectorImpl.CY$34);
        }
    }
    
    public void xsetCyArray(final int n, final STCy stCy) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STCy stCy2 = (STCy)this.get_store().find_element_user(CTVectorImpl.CY$34, n);
            if (stCy2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stCy2.set((XmlObject)stCy);
        }
    }
    
    public void insertCy(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.CY$34, n)).setStringValue(stringValue);
        }
    }
    
    public void addCy(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.CY$34)).setStringValue(stringValue);
        }
    }
    
    public STCy insertNewCy(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STCy)this.get_store().insert_element_user(CTVectorImpl.CY$34, n);
        }
    }
    
    public STCy addNewCy() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STCy)this.get_store().add_element_user(CTVectorImpl.CY$34);
        }
    }
    
    public void removeCy(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.CY$34, n);
        }
    }
    
    public List<String> getErrorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTVectorImpl.ErrorList(this);
        }
    }
    
    public String[] getErrorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.ERROR$36, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getErrorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.ERROR$36, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<STError> xgetErrorList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STError>)new CTVectorImpl.ErrorList(this);
        }
    }
    
    public STError[] xgetErrorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.ERROR$36, list);
            final STError[] array = new STError[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STError xgetErrorArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STError stError = (STError)this.get_store().find_element_user(CTVectorImpl.ERROR$36, n);
            if (stError == null) {
                throw new IndexOutOfBoundsException();
            }
            return stError;
        }
    }
    
    public int sizeOfErrorArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.ERROR$36);
        }
    }
    
    public void setErrorArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.ERROR$36);
        }
    }
    
    public void setErrorArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.ERROR$36, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetErrorArray(final STError[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTVectorImpl.ERROR$36);
        }
    }
    
    public void xsetErrorArray(final int n, final STError stError) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STError stError2 = (STError)this.get_store().find_element_user(CTVectorImpl.ERROR$36, n);
            if (stError2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stError2.set((XmlObject)stError);
        }
    }
    
    public void insertError(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.ERROR$36, n)).setStringValue(stringValue);
        }
    }
    
    public void addError(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.ERROR$36)).setStringValue(stringValue);
        }
    }
    
    public STError insertNewError(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STError)this.get_store().insert_element_user(CTVectorImpl.ERROR$36, n);
        }
    }
    
    public STError addNewError() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STError)this.get_store().add_element_user(CTVectorImpl.ERROR$36);
        }
    }
    
    public void removeError(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.ERROR$36, n);
        }
    }
    
    public List<String> getClsidList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<String>)new CTVectorImpl.ClsidList(this);
        }
    }
    
    public String[] getClsidArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList<SimpleValue> list = new ArrayList<SimpleValue>();
            this.get_store().find_all_element_users(CTVectorImpl.CLSID$38, list);
            final String[] array = new String[list.size()];
            for (int i = 0; i < list.size(); ++i) {
                array[i] = ((SimpleValue)list.get(i)).getStringValue();
            }
            return array;
        }
    }
    
    public String getClsidArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.CLSID$38, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            return simpleValue.getStringValue();
        }
    }
    
    public List<STClsid> xgetClsidList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<STClsid>)new CTVectorImpl.ClsidList(this);
        }
    }
    
    public STClsid[] xgetClsidArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.CLSID$38, list);
            final STClsid[] array = new STClsid[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public STClsid xgetClsidArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STClsid stClsid = (STClsid)this.get_store().find_element_user(CTVectorImpl.CLSID$38, n);
            if (stClsid == null) {
                throw new IndexOutOfBoundsException();
            }
            return stClsid;
        }
    }
    
    public int sizeOfClsidArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.CLSID$38);
        }
    }
    
    public void setClsidArray(final String[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.CLSID$38);
        }
    }
    
    public void setClsidArray(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_element_user(CTVectorImpl.CLSID$38, n);
            if (simpleValue == null) {
                throw new IndexOutOfBoundsException();
            }
            simpleValue.setStringValue(stringValue);
        }
    }
    
    public void xsetClsidArray(final STClsid[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper(sources, CTVectorImpl.CLSID$38);
        }
    }
    
    public void xsetClsidArray(final int n, final STClsid stClsid) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final STClsid stClsid2 = (STClsid)this.get_store().find_element_user(CTVectorImpl.CLSID$38, n);
            if (stClsid2 == null) {
                throw new IndexOutOfBoundsException();
            }
            stClsid2.set(stClsid);
        }
    }
    
    public void insertClsid(final int n, final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().insert_element_user(CTVectorImpl.CLSID$38, n)).setStringValue(stringValue);
        }
    }
    
    public void addClsid(final String stringValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            ((SimpleValue)this.get_store().add_element_user(CTVectorImpl.CLSID$38)).setStringValue(stringValue);
        }
    }
    
    public STClsid insertNewClsid(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STClsid)this.get_store().insert_element_user(CTVectorImpl.CLSID$38, n);
        }
    }
    
    public STClsid addNewClsid() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STClsid)this.get_store().add_element_user(CTVectorImpl.CLSID$38);
        }
    }
    
    public void removeClsid(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.CLSID$38, n);
        }
    }
    
    public List<CTCf> getCfList() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (List<CTCf>)new CTVectorImpl.CfList(this);
        }
    }
    
    public CTCf[] getCfArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final ArrayList list = new ArrayList();
            this.get_store().find_all_element_users(CTVectorImpl.CF$40, list);
            final CTCf[] array = new CTCf[list.size()];
            list.toArray(array);
            return array;
        }
    }
    
    public CTCf getCfArray(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCf ctCf = (CTCf)this.get_store().find_element_user(CTVectorImpl.CF$40, n);
            if (ctCf == null) {
                throw new IndexOutOfBoundsException();
            }
            return ctCf;
        }
    }
    
    public int sizeOfCfArray() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().count_elements(CTVectorImpl.CF$40);
        }
    }
    
    public void setCfArray(final CTCf[] sources) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.arraySetterHelper((XmlObject[])sources, CTVectorImpl.CF$40);
        }
    }
    
    public void setCfArray(final int n, final CTCf ctCf) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTCf ctCf2 = (CTCf)this.get_store().find_element_user(CTVectorImpl.CF$40, n);
            if (ctCf2 == null) {
                throw new IndexOutOfBoundsException();
            }
            ctCf2.set((XmlObject)ctCf);
        }
    }
    
    public CTCf insertNewCf(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCf)this.get_store().insert_element_user(CTVectorImpl.CF$40, n);
        }
    }
    
    public CTCf addNewCf() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTCf)this.get_store().add_element_user(CTVectorImpl.CF$40);
        }
    }
    
    public void removeCf(final int n) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_element(CTVectorImpl.CF$40, n);
        }
    }
    
    public STVectorBaseType.Enum getBaseType() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTVectorImpl.BASETYPE$42);
            if (simpleValue == null) {
                return null;
            }
            return (STVectorBaseType.Enum)simpleValue.getEnumValue();
        }
    }
    
    public STVectorBaseType xgetBaseType() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (STVectorBaseType)this.get_store().find_attribute_user(CTVectorImpl.BASETYPE$42);
        }
    }
    
    public void setBaseType(final STVectorBaseType.Enum enumValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTVectorImpl.BASETYPE$42);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTVectorImpl.BASETYPE$42);
            }
            simpleValue.setEnumValue((StringEnumAbstractBase)enumValue);
        }
    }
    
    public void xsetBaseType(final STVectorBaseType stVectorBaseType) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            STVectorBaseType stVectorBaseType2 = (STVectorBaseType)this.get_store().find_attribute_user(CTVectorImpl.BASETYPE$42);
            if (stVectorBaseType2 == null) {
                stVectorBaseType2 = (STVectorBaseType)this.get_store().add_attribute_user(CTVectorImpl.BASETYPE$42);
            }
            stVectorBaseType2.set((XmlObject)stVectorBaseType);
        }
    }
    
    public long getSize() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTVectorImpl.SIZE$44);
            if (simpleValue == null) {
                return 0L;
            }
            return simpleValue.getLongValue();
        }
    }
    
    public XmlUnsignedInt xgetSize() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (XmlUnsignedInt)this.get_store().find_attribute_user(CTVectorImpl.SIZE$44);
        }
    }
    
    public void setSize(final long longValue) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue simpleValue = (SimpleValue)this.get_store().find_attribute_user(CTVectorImpl.SIZE$44);
            if (simpleValue == null) {
                simpleValue = (SimpleValue)this.get_store().add_attribute_user(CTVectorImpl.SIZE$44);
            }
            simpleValue.setLongValue(longValue);
        }
    }
    
    public void xsetSize(final XmlUnsignedInt xmlUnsignedInt) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlUnsignedInt xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().find_attribute_user(CTVectorImpl.SIZE$44);
            if (xmlUnsignedInt2 == null) {
                xmlUnsignedInt2 = (XmlUnsignedInt)this.get_store().add_attribute_user(CTVectorImpl.SIZE$44);
            }
            xmlUnsignedInt2.set(xmlUnsignedInt);
        }
    }
    
    static {
        VARIANT$0 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "variant");
        I1$2 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "i1");
        I2$4 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "i2");
        I4$6 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "i4");
        I8$8 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "i8");
        UI1$10 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "ui1");
        UI2$12 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "ui2");
        UI4$14 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "ui4");
        UI8$16 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "ui8");
        R4$18 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "r4");
        R8$20 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "r8");
        LPSTR$22 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "lpstr");
        LPWSTR$24 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "lpwstr");
        BSTR$26 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "bstr");
        DATE$28 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "date");
        FILETIME$30 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "filetime");
        BOOL$32 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "bool");
        CY$34 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "cy");
        ERROR$36 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "error");
        CLSID$38 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "clsid");
        CF$40 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "cf");
        BASETYPE$42 = new QName("", "baseType");
        SIZE$44 = new QName("", "size");
    }
}
