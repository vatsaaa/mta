// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.impl;

import org.apache.xmlbeans.SchemaType;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.STClsid;
import org.apache.xmlbeans.impl.values.JavaStringHolderEx;

public class STClsidImpl extends JavaStringHolderEx implements STClsid
{
    public STClsidImpl(final SchemaType type) {
        super(type, false);
    }
    
    protected STClsidImpl(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
