// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlString;

public interface STClsid extends XmlString
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(STClsid.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("stclsida7datype");
    
    public static final class Factory
    {
        public static STClsid newValue(final Object o) {
            return (STClsid)STClsid.type.newValue(o);
        }
        
        public static STClsid newInstance() {
            return (STClsid)XmlBeans.getContextTypeLoader().newInstance(STClsid.type, null);
        }
        
        public static STClsid newInstance(final XmlOptions xmlOptions) {
            return (STClsid)XmlBeans.getContextTypeLoader().newInstance(STClsid.type, xmlOptions);
        }
        
        public static STClsid parse(final String s) throws XmlException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(s, STClsid.type, null);
        }
        
        public static STClsid parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(s, STClsid.type, xmlOptions);
        }
        
        public static STClsid parse(final File file) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(file, STClsid.type, null);
        }
        
        public static STClsid parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(file, STClsid.type, xmlOptions);
        }
        
        public static STClsid parse(final URL url) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(url, STClsid.type, null);
        }
        
        public static STClsid parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(url, STClsid.type, xmlOptions);
        }
        
        public static STClsid parse(final InputStream inputStream) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(inputStream, STClsid.type, null);
        }
        
        public static STClsid parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(inputStream, STClsid.type, xmlOptions);
        }
        
        public static STClsid parse(final Reader reader) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(reader, STClsid.type, null);
        }
        
        public static STClsid parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(reader, STClsid.type, xmlOptions);
        }
        
        public static STClsid parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STClsid.type, null);
        }
        
        public static STClsid parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, STClsid.type, xmlOptions);
        }
        
        public static STClsid parse(final Node node) throws XmlException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(node, STClsid.type, null);
        }
        
        public static STClsid parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(node, STClsid.type, xmlOptions);
        }
        
        @Deprecated
        public static STClsid parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STClsid.type, null);
        }
        
        @Deprecated
        public static STClsid parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (STClsid)XmlBeans.getContextTypeLoader().parse(xmlInputStream, STClsid.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STClsid.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, STClsid.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
