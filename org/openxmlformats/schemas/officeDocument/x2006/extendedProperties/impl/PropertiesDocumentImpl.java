// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.impl;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.CTProperties;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.PropertiesDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class PropertiesDocumentImpl extends XmlComplexContentImpl implements PropertiesDocument
{
    private static final QName PROPERTIES$0;
    
    public PropertiesDocumentImpl(final SchemaType type) {
        super(type);
    }
    
    public CTProperties getProperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            final CTProperties ctProperties = (CTProperties)this.get_store().find_element_user(PropertiesDocumentImpl.PROPERTIES$0, 0);
            if (ctProperties == null) {
                return null;
            }
            return ctProperties;
        }
    }
    
    public void setProperties(final CTProperties ctProperties) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            CTProperties ctProperties2 = (CTProperties)this.get_store().find_element_user(PropertiesDocumentImpl.PROPERTIES$0, 0);
            if (ctProperties2 == null) {
                ctProperties2 = (CTProperties)this.get_store().add_element_user(PropertiesDocumentImpl.PROPERTIES$0);
            }
            ctProperties2.set(ctProperties);
        }
    }
    
    public CTProperties addNewProperties() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return (CTProperties)this.get_store().add_element_user(PropertiesDocumentImpl.PROPERTIES$0);
        }
    }
    
    static {
        PROPERTIES$0 = new QName("http://schemas.openxmlformats.org/officeDocument/2006/extended-properties", "Properties");
    }
}
