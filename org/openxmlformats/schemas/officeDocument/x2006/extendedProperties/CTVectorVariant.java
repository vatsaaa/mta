// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.extendedProperties;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.CTVector;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTVectorVariant extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTVectorVariant.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctvectorvariant9d75type");
    
    CTVector getVector();
    
    void setVector(final CTVector p0);
    
    CTVector addNewVector();
    
    public static final class Factory
    {
        public static CTVectorVariant newInstance() {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().newInstance(CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant newInstance(final XmlOptions xmlOptions) {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().newInstance(CTVectorVariant.type, xmlOptions);
        }
        
        public static CTVectorVariant parse(final String s) throws XmlException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(s, CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(s, CTVectorVariant.type, xmlOptions);
        }
        
        public static CTVectorVariant parse(final File file) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(file, CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(file, CTVectorVariant.type, xmlOptions);
        }
        
        public static CTVectorVariant parse(final URL url) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(url, CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(url, CTVectorVariant.type, xmlOptions);
        }
        
        public static CTVectorVariant parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(inputStream, CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(inputStream, CTVectorVariant.type, xmlOptions);
        }
        
        public static CTVectorVariant parse(final Reader reader) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(reader, CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(reader, CTVectorVariant.type, xmlOptions);
        }
        
        public static CTVectorVariant parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVectorVariant.type, xmlOptions);
        }
        
        public static CTVectorVariant parse(final Node node) throws XmlException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(node, CTVectorVariant.type, null);
        }
        
        public static CTVectorVariant parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(node, CTVectorVariant.type, xmlOptions);
        }
        
        @Deprecated
        public static CTVectorVariant parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVectorVariant.type, null);
        }
        
        @Deprecated
        public static CTVectorVariant parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTVectorVariant)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVectorVariant.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVectorVariant.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVectorVariant.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
