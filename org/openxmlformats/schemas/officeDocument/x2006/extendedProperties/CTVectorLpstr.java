// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.extendedProperties;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.CTVector;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface CTVectorLpstr extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(CTVectorLpstr.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("ctvectorlpstr9b1dtype");
    
    CTVector getVector();
    
    void setVector(final CTVector p0);
    
    CTVector addNewVector();
    
    public static final class Factory
    {
        public static CTVectorLpstr newInstance() {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().newInstance(CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr newInstance(final XmlOptions xmlOptions) {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().newInstance(CTVectorLpstr.type, xmlOptions);
        }
        
        public static CTVectorLpstr parse(final String s) throws XmlException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(s, CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(s, CTVectorLpstr.type, xmlOptions);
        }
        
        public static CTVectorLpstr parse(final File file) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(file, CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(file, CTVectorLpstr.type, xmlOptions);
        }
        
        public static CTVectorLpstr parse(final URL url) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(url, CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(url, CTVectorLpstr.type, xmlOptions);
        }
        
        public static CTVectorLpstr parse(final InputStream inputStream) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(inputStream, CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(inputStream, CTVectorLpstr.type, xmlOptions);
        }
        
        public static CTVectorLpstr parse(final Reader reader) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(reader, CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(reader, CTVectorLpstr.type, xmlOptions);
        }
        
        public static CTVectorLpstr parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, CTVectorLpstr.type, xmlOptions);
        }
        
        public static CTVectorLpstr parse(final Node node) throws XmlException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(node, CTVectorLpstr.type, null);
        }
        
        public static CTVectorLpstr parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(node, CTVectorLpstr.type, xmlOptions);
        }
        
        @Deprecated
        public static CTVectorLpstr parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVectorLpstr.type, null);
        }
        
        @Deprecated
        public static CTVectorLpstr parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (CTVectorLpstr)XmlBeans.getContextTypeLoader().parse(xmlInputStream, CTVectorLpstr.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVectorLpstr.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, CTVectorLpstr.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
