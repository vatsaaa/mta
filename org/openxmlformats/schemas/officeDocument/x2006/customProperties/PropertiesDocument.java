// 
// Decompiled by Procyon v0.5.36
// 

package org.openxmlformats.schemas.officeDocument.x2006.customProperties;

import org.apache.xmlbeans.xml.stream.XMLStreamException;
import org.apache.xmlbeans.xml.stream.XMLInputStream;
import org.w3c.dom.Node;
import javax.xml.stream.XMLStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.File;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

public interface PropertiesDocument extends XmlObject
{
    public static final SchemaType type = (SchemaType)XmlBeans.typeSystemForClassLoader(PropertiesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sE130CAA0A01A7CDE5A2B4FEB8B311707").resolveHandle("properties288cdoctype");
    
    CTProperties getProperties();
    
    void setProperties(final CTProperties p0);
    
    CTProperties addNewProperties();
    
    public static final class Factory
    {
        public static PropertiesDocument newInstance() {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().newInstance(PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument newInstance(final XmlOptions xmlOptions) {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().newInstance(PropertiesDocument.type, xmlOptions);
        }
        
        public static PropertiesDocument parse(final String s) throws XmlException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(s, PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument parse(final String s, final XmlOptions xmlOptions) throws XmlException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(s, PropertiesDocument.type, xmlOptions);
        }
        
        public static PropertiesDocument parse(final File file) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(file, PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument parse(final File file, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(file, PropertiesDocument.type, xmlOptions);
        }
        
        public static PropertiesDocument parse(final URL url) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(url, PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument parse(final URL url, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(url, PropertiesDocument.type, xmlOptions);
        }
        
        public static PropertiesDocument parse(final InputStream inputStream) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument parse(final InputStream inputStream, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(inputStream, PropertiesDocument.type, xmlOptions);
        }
        
        public static PropertiesDocument parse(final Reader reader) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(reader, PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument parse(final Reader reader, final XmlOptions xmlOptions) throws XmlException, IOException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(reader, PropertiesDocument.type, xmlOptions);
        }
        
        public static PropertiesDocument parse(final XMLStreamReader xmlStreamReader) throws XmlException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument parse(final XMLStreamReader xmlStreamReader, final XmlOptions xmlOptions) throws XmlException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(xmlStreamReader, PropertiesDocument.type, xmlOptions);
        }
        
        public static PropertiesDocument parse(final Node node) throws XmlException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(node, PropertiesDocument.type, null);
        }
        
        public static PropertiesDocument parse(final Node node, final XmlOptions xmlOptions) throws XmlException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(node, PropertiesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static PropertiesDocument parse(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, PropertiesDocument.type, null);
        }
        
        @Deprecated
        public static PropertiesDocument parse(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return (PropertiesDocument)XmlBeans.getContextTypeLoader().parse(xmlInputStream, PropertiesDocument.type, xmlOptions);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, PropertiesDocument.type, null);
        }
        
        @Deprecated
        public static XMLInputStream newValidatingXMLInputStream(final XMLInputStream xmlInputStream, final XmlOptions xmlOptions) throws XmlException, XMLStreamException {
            return XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xmlInputStream, PropertiesDocument.type, xmlOptions);
        }
        
        private Factory() {
        }
    }
}
