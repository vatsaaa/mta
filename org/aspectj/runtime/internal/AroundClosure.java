// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal;

import org.aspectj.lang.ProceedingJoinPoint;

public abstract class AroundClosure
{
    protected Object[] state;
    protected int bitflags;
    protected Object[] preInitializationState;
    
    public AroundClosure() {
        this.bitflags = 1048576;
    }
    
    public AroundClosure(final Object[] state) {
        this.bitflags = 1048576;
        this.state = state;
    }
    
    public int getFlags() {
        return this.bitflags;
    }
    
    public Object[] getState() {
        return this.state;
    }
    
    public Object[] getPreInitializationState() {
        return this.preInitializationState;
    }
    
    public abstract Object run(final Object[] p0) throws Throwable;
    
    public ProceedingJoinPoint linkClosureAndJoinPoint() {
        final ProceedingJoinPoint jp = (ProceedingJoinPoint)this.state[this.state.length - 1];
        jp.set$AroundClosure(this);
        return jp;
    }
    
    public ProceedingJoinPoint linkClosureAndJoinPoint(final int flags) {
        final ProceedingJoinPoint jp = (ProceedingJoinPoint)this.state[this.state.length - 1];
        jp.set$AroundClosure(this);
        this.bitflags = flags;
        return jp;
    }
}
