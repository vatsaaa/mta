// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal.cflowstack;

import java.util.Iterator;
import java.util.Enumeration;
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;

public class ThreadCounterImpl11 implements ThreadCounter
{
    private Hashtable counters;
    private Thread cached_thread;
    private Counter cached_counter;
    private int change_count;
    private static final int COLLECT_AT = 20000;
    private static final int MIN_COLLECT_AT = 100;
    
    public ThreadCounterImpl11() {
        this.counters = new Hashtable();
        this.change_count = 0;
    }
    
    private synchronized Counter getThreadCounter() {
        if (Thread.currentThread() != this.cached_thread) {
            this.cached_thread = Thread.currentThread();
            this.cached_counter = this.counters.get(this.cached_thread);
            if (this.cached_counter == null) {
                this.cached_counter = new Counter();
                this.counters.put(this.cached_thread, this.cached_counter);
            }
            ++this.change_count;
            final int size = Math.max(1, this.counters.size());
            if (this.change_count > Math.max(100, 20000 / size)) {
                final List dead_stacks = new ArrayList();
                final Enumeration e = this.counters.keys();
                while (e.hasMoreElements()) {
                    final Thread t = e.nextElement();
                    if (!t.isAlive()) {
                        dead_stacks.add(t);
                    }
                }
                final Iterator e2 = dead_stacks.iterator();
                while (e2.hasNext()) {
                    final Thread t = e2.next();
                    this.counters.remove(t);
                }
                this.change_count = 0;
            }
        }
        return this.cached_counter;
    }
    
    public void inc() {
        final Counter threadCounter = this.getThreadCounter();
        ++threadCounter.value;
    }
    
    public void dec() {
        final Counter threadCounter = this.getThreadCounter();
        --threadCounter.value;
    }
    
    public boolean isNotZero() {
        return this.getThreadCounter().value != 0;
    }
    
    static class Counter
    {
        protected int value;
        
        Counter() {
            this.value = 0;
        }
    }
}
