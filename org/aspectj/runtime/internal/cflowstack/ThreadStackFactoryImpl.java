// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal.cflowstack;

import java.util.Stack;

public class ThreadStackFactoryImpl implements ThreadStackFactory
{
    public ThreadStack getNewThreadStack() {
        return new ThreadStackImpl();
    }
    
    public ThreadCounter getNewThreadCounter() {
        return new ThreadCounterImpl();
    }
    
    private static class ThreadStackImpl extends ThreadLocal implements ThreadStack
    {
        public Object initialValue() {
            return new Stack();
        }
        
        public Stack getThreadStack() {
            return this.get();
        }
    }
    
    private static class ThreadCounterImpl extends ThreadLocal implements ThreadCounter
    {
        public Object initialValue() {
            return new Counter();
        }
        
        public Counter getThreadCounter() {
            return this.get();
        }
        
        public void inc() {
            final Counter threadCounter = this.getThreadCounter();
            ++threadCounter.value;
        }
        
        public void dec() {
            final Counter threadCounter = this.getThreadCounter();
            --threadCounter.value;
        }
        
        public boolean isNotZero() {
            return this.getThreadCounter().value != 0;
        }
        
        static class Counter
        {
            protected int value;
            
            Counter() {
                this.value = 0;
            }
        }
    }
}
