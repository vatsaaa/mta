// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal.cflowstack;

public interface ThreadStackFactory
{
    ThreadStack getNewThreadStack();
    
    ThreadCounter getNewThreadCounter();
}
