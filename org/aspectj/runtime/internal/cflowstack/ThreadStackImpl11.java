// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal.cflowstack;

import java.util.Enumeration;
import java.util.Stack;
import java.util.Hashtable;

public class ThreadStackImpl11 implements ThreadStack
{
    private Hashtable stacks;
    private Thread cached_thread;
    private Stack cached_stack;
    private int change_count;
    private static final int COLLECT_AT = 20000;
    private static final int MIN_COLLECT_AT = 100;
    
    public ThreadStackImpl11() {
        this.stacks = new Hashtable();
        this.change_count = 0;
    }
    
    public synchronized Stack getThreadStack() {
        if (Thread.currentThread() != this.cached_thread) {
            this.cached_thread = Thread.currentThread();
            this.cached_stack = this.stacks.get(this.cached_thread);
            if (this.cached_stack == null) {
                this.cached_stack = new Stack();
                this.stacks.put(this.cached_thread, this.cached_stack);
            }
            ++this.change_count;
            final int size = Math.max(1, this.stacks.size());
            if (this.change_count > Math.max(100, 20000 / size)) {
                final Stack dead_stacks = new Stack();
                Enumeration e = this.stacks.keys();
                while (e.hasMoreElements()) {
                    final Thread t = e.nextElement();
                    if (!t.isAlive()) {
                        dead_stacks.push(t);
                    }
                }
                e = dead_stacks.elements();
                while (e.hasMoreElements()) {
                    final Thread t = e.nextElement();
                    this.stacks.remove(t);
                }
                this.change_count = 0;
            }
        }
        return this.cached_stack;
    }
}
