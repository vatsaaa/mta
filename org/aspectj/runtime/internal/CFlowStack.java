// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal;

import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl11;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl;
import org.aspectj.lang.NoAspectBoundException;
import org.aspectj.runtime.CFlow;
import java.util.Stack;
import org.aspectj.runtime.internal.cflowstack.ThreadStack;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactory;

public class CFlowStack
{
    private static ThreadStackFactory tsFactory;
    private ThreadStack stackProxy;
    
    public CFlowStack() {
        this.stackProxy = CFlowStack.tsFactory.getNewThreadStack();
    }
    
    private Stack getThreadStack() {
        return this.stackProxy.getThreadStack();
    }
    
    public void push(final Object obj) {
        this.getThreadStack().push(obj);
    }
    
    public void pushInstance(final Object obj) {
        this.getThreadStack().push(new CFlow(obj));
    }
    
    public void push(final Object[] obj) {
        this.getThreadStack().push(new CFlowPlusState(obj));
    }
    
    public void pop() {
        this.getThreadStack().pop();
    }
    
    public Object peek() {
        final Stack stack = this.getThreadStack();
        if (stack.isEmpty()) {
            throw new NoAspectBoundException();
        }
        return stack.peek();
    }
    
    public Object get(final int index) {
        final CFlow cf = this.peekCFlow();
        return (null == cf) ? null : cf.get(index);
    }
    
    public Object peekInstance() {
        final CFlow cf = this.peekCFlow();
        if (cf != null) {
            return cf.getAspect();
        }
        throw new NoAspectBoundException();
    }
    
    public CFlow peekCFlow() {
        final Stack stack = this.getThreadStack();
        if (stack.isEmpty()) {
            return null;
        }
        return stack.peek();
    }
    
    public CFlow peekTopCFlow() {
        final Stack stack = this.getThreadStack();
        if (stack.isEmpty()) {
            return null;
        }
        return (CFlow)stack.elementAt(0);
    }
    
    public boolean isValid() {
        return !this.getThreadStack().isEmpty();
    }
    
    private static ThreadStackFactory getThreadLocalStackFactory() {
        return new ThreadStackFactoryImpl();
    }
    
    private static ThreadStackFactory getThreadLocalStackFactoryFor11() {
        return new ThreadStackFactoryImpl11();
    }
    
    private static void selectFactoryForVMVersion() {
        final String override = getSystemPropertyWithoutSecurityException("aspectj.runtime.cflowstack.usethreadlocal", "unspecified");
        boolean useThreadLocalImplementation = false;
        if (override.equals("unspecified")) {
            final String v = System.getProperty("java.class.version", "0.0");
            useThreadLocalImplementation = (v.compareTo("46.0") >= 0);
        }
        else {
            useThreadLocalImplementation = (override.equals("yes") || override.equals("true"));
        }
        if (useThreadLocalImplementation) {
            CFlowStack.tsFactory = getThreadLocalStackFactory();
        }
        else {
            CFlowStack.tsFactory = getThreadLocalStackFactoryFor11();
        }
    }
    
    private static String getSystemPropertyWithoutSecurityException(final String aPropertyName, final String aDefaultValue) {
        try {
            return System.getProperty(aPropertyName, aDefaultValue);
        }
        catch (SecurityException ex) {
            return aDefaultValue;
        }
    }
    
    public static String getThreadStackFactoryClassName() {
        return CFlowStack.tsFactory.getClass().getName();
    }
    
    static {
        selectFactoryForVMVersion();
    }
}
