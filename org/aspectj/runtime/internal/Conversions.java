// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal;

public final class Conversions
{
    private Conversions() {
    }
    
    public static Object intObject(final int i) {
        return new Integer(i);
    }
    
    public static Object shortObject(final short i) {
        return new Short(i);
    }
    
    public static Object byteObject(final byte i) {
        return new Byte(i);
    }
    
    public static Object charObject(final char i) {
        return new Character(i);
    }
    
    public static Object longObject(final long i) {
        return new Long(i);
    }
    
    public static Object floatObject(final float i) {
        return new Float(i);
    }
    
    public static Object doubleObject(final double i) {
        return new Double(i);
    }
    
    public static Object booleanObject(final boolean i) {
        return new Boolean(i);
    }
    
    public static Object voidObject() {
        return null;
    }
    
    public static int intValue(final Object o) {
        if (o == null) {
            return 0;
        }
        if (o instanceof Number) {
            return ((Number)o).intValue();
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to int");
    }
    
    public static long longValue(final Object o) {
        if (o == null) {
            return 0L;
        }
        if (o instanceof Number) {
            return ((Number)o).longValue();
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to long");
    }
    
    public static float floatValue(final Object o) {
        if (o == null) {
            return 0.0f;
        }
        if (o instanceof Number) {
            return ((Number)o).floatValue();
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to float");
    }
    
    public static double doubleValue(final Object o) {
        if (o == null) {
            return 0.0;
        }
        if (o instanceof Number) {
            return ((Number)o).doubleValue();
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to double");
    }
    
    public static byte byteValue(final Object o) {
        if (o == null) {
            return 0;
        }
        if (o instanceof Number) {
            return ((Number)o).byteValue();
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to byte");
    }
    
    public static short shortValue(final Object o) {
        if (o == null) {
            return 0;
        }
        if (o instanceof Number) {
            return ((Number)o).shortValue();
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to short");
    }
    
    public static char charValue(final Object o) {
        if (o == null) {
            return '\0';
        }
        if (o instanceof Character) {
            return (char)o;
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to char");
    }
    
    public static boolean booleanValue(final Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof Boolean) {
            return (boolean)o;
        }
        throw new ClassCastException(o.getClass().getName() + " can not be converted to boolean");
    }
    
    public static Object voidValue(final Object o) {
        if (o == null) {
            return o;
        }
        return o;
    }
}
