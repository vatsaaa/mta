// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal;

import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl11;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactoryImpl;
import org.aspectj.runtime.internal.cflowstack.ThreadCounter;
import org.aspectj.runtime.internal.cflowstack.ThreadStackFactory;

public class CFlowCounter
{
    private static ThreadStackFactory tsFactory;
    private ThreadCounter flowHeightHandler;
    
    public CFlowCounter() {
        this.flowHeightHandler = CFlowCounter.tsFactory.getNewThreadCounter();
    }
    
    public void inc() {
        this.flowHeightHandler.inc();
    }
    
    public void dec() {
        this.flowHeightHandler.dec();
    }
    
    public boolean isValid() {
        return this.flowHeightHandler.isNotZero();
    }
    
    private static ThreadStackFactory getThreadLocalStackFactory() {
        return new ThreadStackFactoryImpl();
    }
    
    private static ThreadStackFactory getThreadLocalStackFactoryFor11() {
        return new ThreadStackFactoryImpl11();
    }
    
    private static void selectFactoryForVMVersion() {
        final String override = getSystemPropertyWithoutSecurityException("aspectj.runtime.cflowstack.usethreadlocal", "unspecified");
        boolean useThreadLocalImplementation = false;
        if (override.equals("unspecified")) {
            final String v = System.getProperty("java.class.version", "0.0");
            useThreadLocalImplementation = (v.compareTo("46.0") >= 0);
        }
        else {
            useThreadLocalImplementation = (override.equals("yes") || override.equals("true"));
        }
        if (useThreadLocalImplementation) {
            CFlowCounter.tsFactory = getThreadLocalStackFactory();
        }
        else {
            CFlowCounter.tsFactory = getThreadLocalStackFactoryFor11();
        }
    }
    
    private static String getSystemPropertyWithoutSecurityException(final String aPropertyName, final String aDefaultValue) {
        try {
            return System.getProperty(aPropertyName, aDefaultValue);
        }
        catch (SecurityException ex) {
            return aDefaultValue;
        }
    }
    
    public static String getThreadStackFactoryClassName() {
        return CFlowCounter.tsFactory.getClass().getName();
    }
    
    static {
        selectFactoryForVMVersion();
    }
}
