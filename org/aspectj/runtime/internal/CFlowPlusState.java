// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.internal;

import org.aspectj.runtime.CFlow;

public class CFlowPlusState extends CFlow
{
    private Object[] state;
    
    public CFlowPlusState(final Object[] state) {
        this.state = state;
    }
    
    public CFlowPlusState(final Object[] state, final Object _aspect) {
        super(_aspect);
        this.state = state;
    }
    
    public Object get(final int index) {
        return this.state[index];
    }
}
