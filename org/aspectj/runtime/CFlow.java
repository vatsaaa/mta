// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime;

public class CFlow
{
    private Object _aspect;
    
    public CFlow() {
        this(null);
    }
    
    public CFlow(final Object _aspect) {
        this._aspect = _aspect;
    }
    
    public Object getAspect() {
        return this._aspect;
    }
    
    public void setAspect(final Object _aspect) {
        this._aspect = _aspect;
    }
    
    public Object get(final int index) {
        return null;
    }
}
