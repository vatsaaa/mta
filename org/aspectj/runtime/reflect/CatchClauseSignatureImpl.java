// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.CatchClauseSignature;

class CatchClauseSignatureImpl extends SignatureImpl implements CatchClauseSignature
{
    Class parameterType;
    String parameterName;
    
    CatchClauseSignatureImpl(final Class declaringType, final Class parameterType, final String parameterName) {
        super(0, "catch", declaringType);
        this.parameterType = parameterType;
        this.parameterName = parameterName;
    }
    
    CatchClauseSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    public Class getParameterType() {
        if (this.parameterType == null) {
            this.parameterType = this.extractType(3);
        }
        return this.parameterType;
    }
    
    public String getParameterName() {
        if (this.parameterName == null) {
            this.parameterName = this.extractString(4);
        }
        return this.parameterName;
    }
    
    protected String createToString(final StringMaker sm) {
        return "catch(" + sm.makeTypeName(this.getParameterType()) + ")";
    }
}
