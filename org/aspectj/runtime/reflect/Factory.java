// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.UnlockSignature;
import org.aspectj.lang.reflect.LockSignature;
import org.aspectj.lang.reflect.CatchClauseSignature;
import org.aspectj.lang.reflect.InitializerSignature;
import org.aspectj.lang.reflect.AdviceSignature;
import org.aspectj.lang.reflect.FieldSignature;
import org.aspectj.lang.reflect.ConstructorSignature;
import java.util.StringTokenizer;
import org.aspectj.lang.reflect.MethodSignature;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Member;
import org.aspectj.lang.reflect.SourceLocation;
import org.aspectj.lang.Signature;
import org.aspectj.lang.JoinPoint;
import java.util.Hashtable;

public final class Factory
{
    Class lexicalClass;
    ClassLoader lookupClassLoader;
    String filename;
    int count;
    static Hashtable prims;
    private static Object[] NO_ARGS;
    
    static Class makeClass(final String s, final ClassLoader loader) {
        if (s.equals("*")) {
            return null;
        }
        final Class ret = Factory.prims.get(s);
        if (ret != null) {
            return ret;
        }
        try {
            if (loader == null) {
                return Class.forName(s);
            }
            return Class.forName(s, false, loader);
        }
        catch (ClassNotFoundException e) {
            return ClassNotFoundException.class;
        }
    }
    
    public Factory(final String filename, final Class lexicalClass) {
        this.filename = filename;
        this.lexicalClass = lexicalClass;
        this.count = 0;
        this.lookupClassLoader = lexicalClass.getClassLoader();
    }
    
    public JoinPoint.StaticPart makeSJP(final String kind, final String modifiers, final String methodName, final String declaringType, final String paramTypes, final String paramNames, final String exceptionTypes, final String returnType, final int l) {
        final Signature sig = this.makeMethodSig(modifiers, methodName, declaringType, paramTypes, paramNames, exceptionTypes, returnType);
        return new JoinPointImpl.StaticPartImpl(this.count++, kind, sig, this.makeSourceLoc(l, -1));
    }
    
    public JoinPoint.StaticPart makeSJP(final String kind, final String modifiers, final String methodName, final String declaringType, final String paramTypes, final String paramNames, final String returnType, final int l) {
        final Signature sig = this.makeMethodSig(modifiers, methodName, declaringType, paramTypes, paramNames, "", returnType);
        return new JoinPointImpl.StaticPartImpl(this.count++, kind, sig, this.makeSourceLoc(l, -1));
    }
    
    public JoinPoint.StaticPart makeSJP(final String kind, final Signature sig, final SourceLocation loc) {
        return new JoinPointImpl.StaticPartImpl(this.count++, kind, sig, loc);
    }
    
    public JoinPoint.StaticPart makeSJP(final String kind, final Signature sig, final int l, final int c) {
        return new JoinPointImpl.StaticPartImpl(this.count++, kind, sig, this.makeSourceLoc(l, c));
    }
    
    public JoinPoint.StaticPart makeSJP(final String kind, final Signature sig, final int l) {
        return new JoinPointImpl.StaticPartImpl(this.count++, kind, sig, this.makeSourceLoc(l, -1));
    }
    
    public JoinPoint.EnclosingStaticPart makeESJP(final String kind, final Signature sig, final SourceLocation loc) {
        return new JoinPointImpl.EnclosingStaticPartImpl(this.count++, kind, sig, loc);
    }
    
    public JoinPoint.EnclosingStaticPart makeESJP(final String kind, final Signature sig, final int l, final int c) {
        return new JoinPointImpl.EnclosingStaticPartImpl(this.count++, kind, sig, this.makeSourceLoc(l, c));
    }
    
    public JoinPoint.EnclosingStaticPart makeESJP(final String kind, final Signature sig, final int l) {
        return new JoinPointImpl.EnclosingStaticPartImpl(this.count++, kind, sig, this.makeSourceLoc(l, -1));
    }
    
    public static JoinPoint.StaticPart makeEncSJP(final Member member) {
        Signature sig = null;
        String kind = null;
        if (member instanceof Method) {
            final Method method = (Method)member;
            sig = new MethodSignatureImpl(method.getModifiers(), method.getName(), method.getDeclaringClass(), method.getParameterTypes(), new String[method.getParameterTypes().length], method.getExceptionTypes(), method.getReturnType());
            kind = "method-execution";
        }
        else {
            if (!(member instanceof Constructor)) {
                throw new IllegalArgumentException("member must be either a method or constructor");
            }
            final Constructor cons = (Constructor)member;
            sig = new ConstructorSignatureImpl(cons.getModifiers(), cons.getDeclaringClass(), cons.getParameterTypes(), new String[cons.getParameterTypes().length], cons.getExceptionTypes());
            kind = "constructor-execution";
        }
        return new JoinPointImpl.EnclosingStaticPartImpl(-1, kind, sig, null);
    }
    
    public static JoinPoint makeJP(final JoinPoint.StaticPart staticPart, final Object _this, final Object target) {
        return new JoinPointImpl(staticPart, _this, target, Factory.NO_ARGS);
    }
    
    public static JoinPoint makeJP(final JoinPoint.StaticPart staticPart, final Object _this, final Object target, final Object arg0) {
        return new JoinPointImpl(staticPart, _this, target, new Object[] { arg0 });
    }
    
    public static JoinPoint makeJP(final JoinPoint.StaticPart staticPart, final Object _this, final Object target, final Object arg0, final Object arg1) {
        return new JoinPointImpl(staticPart, _this, target, new Object[] { arg0, arg1 });
    }
    
    public static JoinPoint makeJP(final JoinPoint.StaticPart staticPart, final Object _this, final Object target, final Object[] args) {
        return new JoinPointImpl(staticPart, _this, target, args);
    }
    
    public MethodSignature makeMethodSig(final String stringRep) {
        final MethodSignatureImpl ret = new MethodSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public MethodSignature makeMethodSig(final String modifiers, final String methodName, final String declaringType, final String paramTypes, final String paramNames, final String exceptionTypes, final String returnType) {
        final int modifiersAsInt = Integer.parseInt(modifiers, 16);
        final Class declaringTypeClass = makeClass(declaringType, this.lookupClassLoader);
        StringTokenizer st = new StringTokenizer(paramTypes, ":");
        int numParams = st.countTokens();
        final Class[] paramTypeClasses = new Class[numParams];
        for (int i = 0; i < numParams; ++i) {
            paramTypeClasses[i] = makeClass(st.nextToken(), this.lookupClassLoader);
        }
        st = new StringTokenizer(paramNames, ":");
        numParams = st.countTokens();
        final String[] paramNamesArray = new String[numParams];
        for (int j = 0; j < numParams; ++j) {
            paramNamesArray[j] = st.nextToken();
        }
        st = new StringTokenizer(exceptionTypes, ":");
        numParams = st.countTokens();
        final Class[] exceptionTypeClasses = new Class[numParams];
        for (int k = 0; k < numParams; ++k) {
            exceptionTypeClasses[k] = makeClass(st.nextToken(), this.lookupClassLoader);
        }
        final Class returnTypeClass = makeClass(returnType, this.lookupClassLoader);
        final MethodSignatureImpl ret = new MethodSignatureImpl(modifiersAsInt, methodName, declaringTypeClass, paramTypeClasses, paramNamesArray, exceptionTypeClasses, returnTypeClass);
        return ret;
    }
    
    public MethodSignature makeMethodSig(final int modifiers, final String name, final Class declaringType, final Class[] parameterTypes, final String[] parameterNames, final Class[] exceptionTypes, final Class returnType) {
        final MethodSignatureImpl ret = new MethodSignatureImpl(modifiers, name, declaringType, parameterTypes, parameterNames, exceptionTypes, returnType);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public ConstructorSignature makeConstructorSig(final String stringRep) {
        final ConstructorSignatureImpl ret = new ConstructorSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public ConstructorSignature makeConstructorSig(final String modifiers, final String declaringType, final String paramTypes, final String paramNames, final String exceptionTypes) {
        final int modifiersAsInt = Integer.parseInt(modifiers, 16);
        final Class declaringTypeClass = makeClass(declaringType, this.lookupClassLoader);
        StringTokenizer st = new StringTokenizer(paramTypes, ":");
        int numParams = st.countTokens();
        final Class[] paramTypeClasses = new Class[numParams];
        for (int i = 0; i < numParams; ++i) {
            paramTypeClasses[i] = makeClass(st.nextToken(), this.lookupClassLoader);
        }
        st = new StringTokenizer(paramNames, ":");
        numParams = st.countTokens();
        final String[] paramNamesArray = new String[numParams];
        for (int j = 0; j < numParams; ++j) {
            paramNamesArray[j] = st.nextToken();
        }
        st = new StringTokenizer(exceptionTypes, ":");
        numParams = st.countTokens();
        final Class[] exceptionTypeClasses = new Class[numParams];
        for (int k = 0; k < numParams; ++k) {
            exceptionTypeClasses[k] = makeClass(st.nextToken(), this.lookupClassLoader);
        }
        final ConstructorSignatureImpl ret = new ConstructorSignatureImpl(modifiersAsInt, declaringTypeClass, paramTypeClasses, paramNamesArray, exceptionTypeClasses);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public ConstructorSignature makeConstructorSig(final int modifiers, final Class declaringType, final Class[] parameterTypes, final String[] parameterNames, final Class[] exceptionTypes) {
        final ConstructorSignatureImpl ret = new ConstructorSignatureImpl(modifiers, declaringType, parameterTypes, parameterNames, exceptionTypes);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public FieldSignature makeFieldSig(final String stringRep) {
        final FieldSignatureImpl ret = new FieldSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public FieldSignature makeFieldSig(final String modifiers, final String name, final String declaringType, final String fieldType) {
        final int modifiersAsInt = Integer.parseInt(modifiers, 16);
        final Class declaringTypeClass = makeClass(declaringType, this.lookupClassLoader);
        final Class fieldTypeClass = makeClass(fieldType, this.lookupClassLoader);
        final FieldSignatureImpl ret = new FieldSignatureImpl(modifiersAsInt, name, declaringTypeClass, fieldTypeClass);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public FieldSignature makeFieldSig(final int modifiers, final String name, final Class declaringType, final Class fieldType) {
        final FieldSignatureImpl ret = new FieldSignatureImpl(modifiers, name, declaringType, fieldType);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public AdviceSignature makeAdviceSig(final String stringRep) {
        final AdviceSignatureImpl ret = new AdviceSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public AdviceSignature makeAdviceSig(final String modifiers, final String name, final String declaringType, final String paramTypes, final String paramNames, final String exceptionTypes, final String returnType) {
        final int modifiersAsInt = Integer.parseInt(modifiers, 16);
        final Class declaringTypeClass = makeClass(declaringType, this.lookupClassLoader);
        StringTokenizer st = new StringTokenizer(paramTypes, ":");
        int numParams = st.countTokens();
        final Class[] paramTypeClasses = new Class[numParams];
        for (int i = 0; i < numParams; ++i) {
            paramTypeClasses[i] = makeClass(st.nextToken(), this.lookupClassLoader);
        }
        st = new StringTokenizer(paramNames, ":");
        numParams = st.countTokens();
        final String[] paramNamesArray = new String[numParams];
        for (int j = 0; j < numParams; ++j) {
            paramNamesArray[j] = st.nextToken();
        }
        st = new StringTokenizer(exceptionTypes, ":");
        numParams = st.countTokens();
        final Class[] exceptionTypeClasses = new Class[numParams];
        for (int k = 0; k < numParams; ++k) {
            exceptionTypeClasses[k] = makeClass(st.nextToken(), this.lookupClassLoader);
        }
        final Class returnTypeClass = makeClass(returnType, this.lookupClassLoader);
        final AdviceSignatureImpl ret = new AdviceSignatureImpl(modifiersAsInt, name, declaringTypeClass, paramTypeClasses, paramNamesArray, exceptionTypeClasses, returnTypeClass);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public AdviceSignature makeAdviceSig(final int modifiers, final String name, final Class declaringType, final Class[] parameterTypes, final String[] parameterNames, final Class[] exceptionTypes, final Class returnType) {
        final AdviceSignatureImpl ret = new AdviceSignatureImpl(modifiers, name, declaringType, parameterTypes, parameterNames, exceptionTypes, returnType);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public InitializerSignature makeInitializerSig(final String stringRep) {
        final InitializerSignatureImpl ret = new InitializerSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public InitializerSignature makeInitializerSig(final String modifiers, final String declaringType) {
        final int modifiersAsInt = Integer.parseInt(modifiers, 16);
        final Class declaringTypeClass = makeClass(declaringType, this.lookupClassLoader);
        final InitializerSignatureImpl ret = new InitializerSignatureImpl(modifiersAsInt, declaringTypeClass);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public InitializerSignature makeInitializerSig(final int modifiers, final Class declaringType) {
        final InitializerSignatureImpl ret = new InitializerSignatureImpl(modifiers, declaringType);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public CatchClauseSignature makeCatchClauseSig(final String stringRep) {
        final CatchClauseSignatureImpl ret = new CatchClauseSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public CatchClauseSignature makeCatchClauseSig(final String declaringType, final String parameterType, final String parameterName) {
        final Class declaringTypeClass = makeClass(declaringType, this.lookupClassLoader);
        StringTokenizer st = new StringTokenizer(parameterType, ":");
        final Class parameterTypeClass = makeClass(st.nextToken(), this.lookupClassLoader);
        st = new StringTokenizer(parameterName, ":");
        final String parameterNameForReturn = st.nextToken();
        final CatchClauseSignatureImpl ret = new CatchClauseSignatureImpl(declaringTypeClass, parameterTypeClass, parameterNameForReturn);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public CatchClauseSignature makeCatchClauseSig(final Class declaringType, final Class parameterType, final String parameterName) {
        final CatchClauseSignatureImpl ret = new CatchClauseSignatureImpl(declaringType, parameterType, parameterName);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public LockSignature makeLockSig(final String stringRep) {
        final LockSignatureImpl ret = new LockSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public LockSignature makeLockSig() {
        final Class declaringTypeClass = makeClass("Ljava/lang/Object;", this.lookupClassLoader);
        final LockSignatureImpl ret = new LockSignatureImpl(declaringTypeClass);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public LockSignature makeLockSig(final Class declaringType) {
        final LockSignatureImpl ret = new LockSignatureImpl(declaringType);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public UnlockSignature makeUnlockSig(final String stringRep) {
        final UnlockSignatureImpl ret = new UnlockSignatureImpl(stringRep);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public UnlockSignature makeUnlockSig() {
        final Class declaringTypeClass = makeClass("Ljava/lang/Object;", this.lookupClassLoader);
        final UnlockSignatureImpl ret = new UnlockSignatureImpl(declaringTypeClass);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public UnlockSignature makeUnlockSig(final Class declaringType) {
        final UnlockSignatureImpl ret = new UnlockSignatureImpl(declaringType);
        ret.setLookupClassLoader(this.lookupClassLoader);
        return ret;
    }
    
    public SourceLocation makeSourceLoc(final int line, final int col) {
        return new SourceLocationImpl(this.lexicalClass, this.filename, line);
    }
    
    static {
        (Factory.prims = new Hashtable()).put("void", Void.TYPE);
        Factory.prims.put("boolean", Boolean.TYPE);
        Factory.prims.put("byte", Byte.TYPE);
        Factory.prims.put("char", Character.TYPE);
        Factory.prims.put("short", Short.TYPE);
        Factory.prims.put("int", Integer.TYPE);
        Factory.prims.put("long", Long.TYPE);
        Factory.prims.put("float", Float.TYPE);
        Factory.prims.put("double", Double.TYPE);
        Factory.NO_ARGS = new Object[0];
    }
}
