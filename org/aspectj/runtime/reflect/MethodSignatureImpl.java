// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import java.util.Set;
import java.util.HashSet;
import java.lang.reflect.Method;
import org.aspectj.lang.reflect.MethodSignature;

class MethodSignatureImpl extends CodeSignatureImpl implements MethodSignature
{
    private Method method;
    Class returnType;
    
    MethodSignatureImpl(final int modifiers, final String name, final Class declaringType, final Class[] parameterTypes, final String[] parameterNames, final Class[] exceptionTypes, final Class returnType) {
        super(modifiers, name, declaringType, parameterTypes, parameterNames, exceptionTypes);
        this.returnType = returnType;
    }
    
    MethodSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    public Class getReturnType() {
        if (this.returnType == null) {
            this.returnType = this.extractType(6);
        }
        return this.returnType;
    }
    
    protected String createToString(final StringMaker sm) {
        final StringBuffer buf = new StringBuffer();
        buf.append(sm.makeModifiersString(this.getModifiers()));
        if (sm.includeArgs) {
            buf.append(sm.makeTypeName(this.getReturnType()));
        }
        if (sm.includeArgs) {
            buf.append(" ");
        }
        buf.append(sm.makePrimaryTypeName(this.getDeclaringType(), this.getDeclaringTypeName()));
        buf.append(".");
        buf.append(this.getName());
        sm.addSignature(buf, this.getParameterTypes());
        sm.addThrows(buf, this.getExceptionTypes());
        return buf.toString();
    }
    
    public Method getMethod() {
        if (this.method == null) {
            final Class dtype = this.getDeclaringType();
            try {
                this.method = dtype.getDeclaredMethod(this.getName(), (Class[])this.getParameterTypes());
            }
            catch (NoSuchMethodException nsmEx) {
                final Set searched = new HashSet();
                searched.add(dtype);
                this.method = this.search(dtype, this.getName(), this.getParameterTypes(), searched);
            }
        }
        return this.method;
    }
    
    private Method search(final Class type, final String name, final Class[] params, final Set searched) {
        if (type == null) {
            return null;
        }
        if (!searched.contains(type)) {
            searched.add(type);
            try {
                return type.getDeclaredMethod(name, (Class[])params);
            }
            catch (NoSuchMethodException ex) {}
        }
        Method m = this.search(type.getSuperclass(), name, params, searched);
        if (m != null) {
            return m;
        }
        final Class[] superinterfaces = type.getInterfaces();
        if (superinterfaces != null) {
            for (int i = 0; i < superinterfaces.length; ++i) {
                m = this.search(superinterfaces[i], name, params, searched);
                if (m != null) {
                    return m;
                }
            }
        }
        return null;
    }
}
