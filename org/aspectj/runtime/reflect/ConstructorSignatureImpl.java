// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import java.lang.reflect.Constructor;
import org.aspectj.lang.reflect.ConstructorSignature;

class ConstructorSignatureImpl extends CodeSignatureImpl implements ConstructorSignature
{
    private Constructor constructor;
    
    ConstructorSignatureImpl(final int modifiers, final Class declaringType, final Class[] parameterTypes, final String[] parameterNames, final Class[] exceptionTypes) {
        super(modifiers, "<init>", declaringType, parameterTypes, parameterNames, exceptionTypes);
    }
    
    ConstructorSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    public String getName() {
        return "<init>";
    }
    
    protected String createToString(final StringMaker sm) {
        final StringBuffer buf = new StringBuffer();
        buf.append(sm.makeModifiersString(this.getModifiers()));
        buf.append(sm.makePrimaryTypeName(this.getDeclaringType(), this.getDeclaringTypeName()));
        sm.addSignature(buf, this.getParameterTypes());
        sm.addThrows(buf, this.getExceptionTypes());
        return buf.toString();
    }
    
    public Constructor getConstructor() {
        if (this.constructor == null) {
            try {
                this.constructor = this.getDeclaringType().getDeclaredConstructor((Class[])this.getParameterTypes());
            }
            catch (Exception ex) {}
        }
        return this.constructor;
    }
}
