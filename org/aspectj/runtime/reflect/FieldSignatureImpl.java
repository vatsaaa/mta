// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import java.lang.reflect.Field;
import org.aspectj.lang.reflect.FieldSignature;

public class FieldSignatureImpl extends MemberSignatureImpl implements FieldSignature
{
    Class fieldType;
    private Field field;
    
    FieldSignatureImpl(final int modifiers, final String name, final Class declaringType, final Class fieldType) {
        super(modifiers, name, declaringType);
        this.fieldType = fieldType;
    }
    
    FieldSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    public Class getFieldType() {
        if (this.fieldType == null) {
            this.fieldType = this.extractType(3);
        }
        return this.fieldType;
    }
    
    protected String createToString(final StringMaker sm) {
        final StringBuffer buf = new StringBuffer();
        buf.append(sm.makeModifiersString(this.getModifiers()));
        if (sm.includeArgs) {
            buf.append(sm.makeTypeName(this.getFieldType()));
        }
        if (sm.includeArgs) {
            buf.append(" ");
        }
        buf.append(sm.makePrimaryTypeName(this.getDeclaringType(), this.getDeclaringTypeName()));
        buf.append(".");
        buf.append(this.getName());
        return buf.toString();
    }
    
    public Field getField() {
        if (this.field == null) {
            try {
                this.field = this.getDeclaringType().getDeclaredField(this.getName());
            }
            catch (Exception ex) {}
        }
        return this.field;
    }
}
