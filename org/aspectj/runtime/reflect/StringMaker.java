// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import java.lang.reflect.Modifier;

class StringMaker
{
    boolean shortTypeNames;
    boolean includeArgs;
    boolean includeThrows;
    boolean includeModifiers;
    boolean shortPrimaryTypeNames;
    boolean includeJoinPointTypeName;
    boolean includeEnclosingPoint;
    boolean shortKindName;
    int cacheOffset;
    static StringMaker shortStringMaker;
    static StringMaker middleStringMaker;
    static StringMaker longStringMaker;
    
    StringMaker() {
        this.shortTypeNames = true;
        this.includeArgs = true;
        this.includeThrows = false;
        this.includeModifiers = false;
        this.shortPrimaryTypeNames = false;
        this.includeJoinPointTypeName = true;
        this.includeEnclosingPoint = true;
        this.shortKindName = true;
    }
    
    String makeKindName(final String name) {
        final int dash = name.lastIndexOf(45);
        if (dash == -1) {
            return name;
        }
        return name.substring(dash + 1);
    }
    
    String makeModifiersString(final int modifiers) {
        if (!this.includeModifiers) {
            return "";
        }
        final String str = Modifier.toString(modifiers);
        if (str.length() == 0) {
            return "";
        }
        return str + " ";
    }
    
    String stripPackageName(final String name) {
        final int dot = name.lastIndexOf(46);
        if (dot == -1) {
            return name;
        }
        return name.substring(dot + 1);
    }
    
    String makeTypeName(final Class type, final String typeName, final boolean shortName) {
        if (type == null) {
            return "ANONYMOUS";
        }
        if (type.isArray()) {
            final Class componentType = type.getComponentType();
            return this.makeTypeName(componentType, componentType.getName(), shortName) + "[]";
        }
        if (shortName) {
            return this.stripPackageName(typeName).replace('$', '.');
        }
        return typeName.replace('$', '.');
    }
    
    public String makeTypeName(final Class type) {
        return this.makeTypeName(type, type.getName(), this.shortTypeNames);
    }
    
    public String makePrimaryTypeName(final Class type, final String typeName) {
        return this.makeTypeName(type, typeName, this.shortPrimaryTypeNames);
    }
    
    public void addTypeNames(final StringBuffer buf, final Class[] types) {
        for (int i = 0; i < types.length; ++i) {
            if (i > 0) {
                buf.append(", ");
            }
            buf.append(this.makeTypeName(types[i]));
        }
    }
    
    public void addSignature(final StringBuffer buf, final Class[] types) {
        if (types == null) {
            return;
        }
        if (this.includeArgs) {
            buf.append("(");
            this.addTypeNames(buf, types);
            buf.append(")");
            return;
        }
        if (types.length == 0) {
            buf.append("()");
            return;
        }
        buf.append("(..)");
    }
    
    public void addThrows(final StringBuffer buf, final Class[] types) {
        if (!this.includeThrows || types == null || types.length == 0) {
            return;
        }
        buf.append(" throws ");
        this.addTypeNames(buf, types);
    }
    
    static {
        StringMaker.shortStringMaker = new StringMaker();
        StringMaker.shortStringMaker.shortTypeNames = true;
        StringMaker.shortStringMaker.includeArgs = false;
        StringMaker.shortStringMaker.includeThrows = false;
        StringMaker.shortStringMaker.includeModifiers = false;
        StringMaker.shortStringMaker.shortPrimaryTypeNames = true;
        StringMaker.shortStringMaker.includeJoinPointTypeName = false;
        StringMaker.shortStringMaker.includeEnclosingPoint = false;
        StringMaker.shortStringMaker.cacheOffset = 0;
        StringMaker.middleStringMaker = new StringMaker();
        StringMaker.middleStringMaker.shortTypeNames = true;
        StringMaker.middleStringMaker.includeArgs = true;
        StringMaker.middleStringMaker.includeThrows = false;
        StringMaker.middleStringMaker.includeModifiers = false;
        StringMaker.middleStringMaker.shortPrimaryTypeNames = false;
        StringMaker.shortStringMaker.cacheOffset = 1;
        StringMaker.longStringMaker = new StringMaker();
        StringMaker.longStringMaker.shortTypeNames = false;
        StringMaker.longStringMaker.includeArgs = true;
        StringMaker.longStringMaker.includeThrows = false;
        StringMaker.longStringMaker.includeModifiers = true;
        StringMaker.longStringMaker.shortPrimaryTypeNames = false;
        StringMaker.longStringMaker.shortKindName = false;
        StringMaker.longStringMaker.cacheOffset = 2;
    }
}
