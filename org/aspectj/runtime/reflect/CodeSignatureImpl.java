// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.CodeSignature;

abstract class CodeSignatureImpl extends MemberSignatureImpl implements CodeSignature
{
    Class[] parameterTypes;
    String[] parameterNames;
    Class[] exceptionTypes;
    
    CodeSignatureImpl(final int modifiers, final String name, final Class declaringType, final Class[] parameterTypes, final String[] parameterNames, final Class[] exceptionTypes) {
        super(modifiers, name, declaringType);
        this.parameterTypes = parameterTypes;
        this.parameterNames = parameterNames;
        this.exceptionTypes = exceptionTypes;
    }
    
    CodeSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    public Class[] getParameterTypes() {
        if (this.parameterTypes == null) {
            this.parameterTypes = this.extractTypes(3);
        }
        return this.parameterTypes;
    }
    
    public String[] getParameterNames() {
        if (this.parameterNames == null) {
            this.parameterNames = this.extractStrings(4);
        }
        return this.parameterNames;
    }
    
    public Class[] getExceptionTypes() {
        if (this.exceptionTypes == null) {
            this.exceptionTypes = this.extractTypes(5);
        }
        return this.exceptionTypes;
    }
}
