// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import java.util.StringTokenizer;
import java.lang.reflect.Method;
import org.aspectj.lang.reflect.AdviceSignature;

class AdviceSignatureImpl extends CodeSignatureImpl implements AdviceSignature
{
    Class returnType;
    private Method adviceMethod;
    
    AdviceSignatureImpl(final int modifiers, final String name, final Class declaringType, final Class[] parameterTypes, final String[] parameterNames, final Class[] exceptionTypes, final Class returnType) {
        super(modifiers, name, declaringType, parameterTypes, parameterNames, exceptionTypes);
        this.adviceMethod = null;
        this.returnType = returnType;
    }
    
    AdviceSignatureImpl(final String stringRep) {
        super(stringRep);
        this.adviceMethod = null;
    }
    
    public Class getReturnType() {
        if (this.returnType == null) {
            this.returnType = this.extractType(6);
        }
        return this.returnType;
    }
    
    protected String createToString(final StringMaker sm) {
        final StringBuffer buf = new StringBuffer();
        if (sm.includeArgs) {
            buf.append(sm.makeTypeName(this.getReturnType()));
        }
        if (sm.includeArgs) {
            buf.append(" ");
        }
        buf.append(sm.makePrimaryTypeName(this.getDeclaringType(), this.getDeclaringTypeName()));
        buf.append(".");
        buf.append(this.toAdviceName(this.getName()));
        sm.addSignature(buf, this.getParameterTypes());
        sm.addThrows(buf, this.getExceptionTypes());
        return buf.toString();
    }
    
    private String toAdviceName(final String methodName) {
        if (methodName.indexOf(36) == -1) {
            return methodName;
        }
        final StringTokenizer strTok = new StringTokenizer(methodName, "$");
        while (strTok.hasMoreTokens()) {
            final String token = strTok.nextToken();
            if (token.startsWith("before") || token.startsWith("after") || token.startsWith("around")) {
                return token;
            }
        }
        return methodName;
    }
    
    public Method getAdvice() {
        if (this.adviceMethod == null) {
            try {
                this.adviceMethod = this.getDeclaringType().getDeclaredMethod(this.getName(), (Class[])this.getParameterTypes());
            }
            catch (Exception ex) {}
        }
        return this.adviceMethod;
    }
}
