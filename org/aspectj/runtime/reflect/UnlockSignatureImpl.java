// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.UnlockSignature;

class UnlockSignatureImpl extends SignatureImpl implements UnlockSignature
{
    private Class parameterType;
    
    UnlockSignatureImpl(final Class c) {
        super(8, "unlock", c);
        this.parameterType = c;
    }
    
    UnlockSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    protected String createToString(final StringMaker sm) {
        if (this.parameterType == null) {
            this.parameterType = this.extractType(3);
        }
        return "unlock(" + sm.makeTypeName(this.parameterType) + ")";
    }
    
    public Class getParameterType() {
        if (this.parameterType == null) {
            this.parameterType = this.extractType(3);
        }
        return this.parameterType;
    }
}
