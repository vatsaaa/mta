// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.MemberSignature;

abstract class MemberSignatureImpl extends SignatureImpl implements MemberSignature
{
    MemberSignatureImpl(final int modifiers, final String name, final Class declaringType) {
        super(modifiers, name, declaringType);
    }
    
    public MemberSignatureImpl(final String stringRep) {
        super(stringRep);
    }
}
