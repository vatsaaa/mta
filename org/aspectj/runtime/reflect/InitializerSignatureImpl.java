// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import java.lang.reflect.Modifier;
import java.lang.reflect.Constructor;
import org.aspectj.lang.reflect.InitializerSignature;

class InitializerSignatureImpl extends CodeSignatureImpl implements InitializerSignature
{
    private Constructor constructor;
    
    InitializerSignatureImpl(final int modifiers, final Class declaringType) {
        super(modifiers, Modifier.isStatic(modifiers) ? "<clinit>" : "<init>", declaringType, SignatureImpl.EMPTY_CLASS_ARRAY, SignatureImpl.EMPTY_STRING_ARRAY, SignatureImpl.EMPTY_CLASS_ARRAY);
    }
    
    InitializerSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    public String getName() {
        return Modifier.isStatic(this.getModifiers()) ? "<clinit>" : "<init>";
    }
    
    protected String createToString(final StringMaker sm) {
        final StringBuffer buf = new StringBuffer();
        buf.append(sm.makeModifiersString(this.getModifiers()));
        buf.append(sm.makePrimaryTypeName(this.getDeclaringType(), this.getDeclaringTypeName()));
        buf.append(".");
        buf.append(this.getName());
        return buf.toString();
    }
    
    public Constructor getInitializer() {
        if (this.constructor == null) {
            try {
                this.constructor = this.getDeclaringType().getDeclaredConstructor((Class[])this.getParameterTypes());
            }
            catch (Exception ex) {}
        }
        return this.constructor;
    }
}
