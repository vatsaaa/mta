// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.SourceLocation;
import org.aspectj.lang.Signature;
import org.aspectj.runtime.internal.AroundClosure;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

class JoinPointImpl implements ProceedingJoinPoint
{
    Object _this;
    Object target;
    Object[] args;
    JoinPoint.StaticPart staticPart;
    private AroundClosure arc;
    
    public JoinPointImpl(final JoinPoint.StaticPart staticPart, final Object _this, final Object target, final Object[] args) {
        this.staticPart = staticPart;
        this._this = _this;
        this.target = target;
        this.args = args;
    }
    
    public Object getThis() {
        return this._this;
    }
    
    public Object getTarget() {
        return this.target;
    }
    
    public Object[] getArgs() {
        if (this.args == null) {
            this.args = new Object[0];
        }
        final Object[] argsCopy = new Object[this.args.length];
        System.arraycopy(this.args, 0, argsCopy, 0, this.args.length);
        return argsCopy;
    }
    
    public JoinPoint.StaticPart getStaticPart() {
        return this.staticPart;
    }
    
    public String getKind() {
        return this.staticPart.getKind();
    }
    
    public Signature getSignature() {
        return this.staticPart.getSignature();
    }
    
    public SourceLocation getSourceLocation() {
        return this.staticPart.getSourceLocation();
    }
    
    public final String toString() {
        return this.staticPart.toString();
    }
    
    public final String toShortString() {
        return this.staticPart.toShortString();
    }
    
    public final String toLongString() {
        return this.staticPart.toLongString();
    }
    
    public void set$AroundClosure(final AroundClosure arc) {
        this.arc = arc;
    }
    
    public Object proceed() throws Throwable {
        if (this.arc == null) {
            return null;
        }
        return this.arc.run(this.arc.getState());
    }
    
    public Object proceed(final Object[] adviceBindings) throws Throwable {
        if (this.arc == null) {
            return null;
        }
        final int flags = this.arc.getFlags();
        final boolean unset = (flags & 0x100000) != 0x0;
        final boolean thisTargetTheSame = (flags & 0x10000) != 0x0;
        final boolean hasThis = (flags & 0x1000) != 0x0;
        final boolean bindsThis = (flags & 0x100) != 0x0;
        final boolean hasTarget = (flags & 0x10) != 0x0;
        final boolean bindsTarget = (flags & 0x1) != 0x0;
        final Object[] state = this.arc.getState();
        int firstArgumentIndexIntoAdviceBindings = 0;
        int firstArgumentIndexIntoState = 0;
        firstArgumentIndexIntoState += (hasThis ? 1 : 0);
        firstArgumentIndexIntoState += ((hasTarget && !thisTargetTheSame) ? 1 : 0);
        if (hasThis && bindsThis) {
            firstArgumentIndexIntoAdviceBindings = 1;
            state[0] = adviceBindings[0];
        }
        if (hasTarget && bindsTarget) {
            if (thisTargetTheSame) {
                firstArgumentIndexIntoAdviceBindings = 1 + (bindsThis ? 1 : 0);
                state[0] = adviceBindings[bindsThis];
            }
            else {
                firstArgumentIndexIntoAdviceBindings = (hasThis ? 1 : 0) + 1;
                state[hasThis] = adviceBindings[hasThis];
            }
        }
        for (int i = firstArgumentIndexIntoAdviceBindings; i < adviceBindings.length; ++i) {
            state[firstArgumentIndexIntoState + (i - firstArgumentIndexIntoAdviceBindings)] = adviceBindings[i];
        }
        return this.arc.run(state);
    }
    
    static class StaticPartImpl implements JoinPoint.StaticPart
    {
        String kind;
        Signature signature;
        SourceLocation sourceLocation;
        private int id;
        
        public StaticPartImpl(final int id, final String kind, final Signature signature, final SourceLocation sourceLocation) {
            this.kind = kind;
            this.signature = signature;
            this.sourceLocation = sourceLocation;
            this.id = id;
        }
        
        public int getId() {
            return this.id;
        }
        
        public String getKind() {
            return this.kind;
        }
        
        public Signature getSignature() {
            return this.signature;
        }
        
        public SourceLocation getSourceLocation() {
            return this.sourceLocation;
        }
        
        String toString(final StringMaker sm) {
            final StringBuffer buf = new StringBuffer();
            buf.append(sm.makeKindName(this.getKind()));
            buf.append("(");
            buf.append(((SignatureImpl)this.getSignature()).toString(sm));
            buf.append(")");
            return buf.toString();
        }
        
        public final String toString() {
            return this.toString(StringMaker.middleStringMaker);
        }
        
        public final String toShortString() {
            return this.toString(StringMaker.shortStringMaker);
        }
        
        public final String toLongString() {
            return this.toString(StringMaker.longStringMaker);
        }
    }
    
    static class EnclosingStaticPartImpl extends StaticPartImpl implements JoinPoint.EnclosingStaticPart
    {
        public EnclosingStaticPartImpl(final int count, final String kind, final Signature signature, final SourceLocation sourceLocation) {
            super(count, kind, signature, sourceLocation);
        }
    }
}
