// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import java.lang.ref.SoftReference;
import java.util.StringTokenizer;
import org.aspectj.lang.Signature;

abstract class SignatureImpl implements Signature
{
    private static boolean useCache;
    int modifiers;
    String name;
    String declaringTypeName;
    Class declaringType;
    Cache stringCache;
    private String stringRep;
    ClassLoader lookupClassLoader;
    static final char SEP = '-';
    static String[] EMPTY_STRING_ARRAY;
    static Class[] EMPTY_CLASS_ARRAY;
    static final String INNER_SEP = ":";
    
    SignatureImpl(final int modifiers, final String name, final Class declaringType) {
        this.modifiers = -1;
        this.lookupClassLoader = null;
        this.modifiers = modifiers;
        this.name = name;
        this.declaringType = declaringType;
    }
    
    protected abstract String createToString(final StringMaker p0);
    
    String toString(final StringMaker sm) {
        String result = null;
        if (SignatureImpl.useCache) {
            if (this.stringCache == null) {
                try {
                    this.stringCache = new CacheImpl();
                }
                catch (Throwable t) {
                    SignatureImpl.useCache = false;
                }
            }
            else {
                result = this.stringCache.get(sm.cacheOffset);
            }
        }
        if (result == null) {
            result = this.createToString(sm);
        }
        if (SignatureImpl.useCache) {
            this.stringCache.set(sm.cacheOffset, result);
        }
        return result;
    }
    
    public final String toString() {
        return this.toString(StringMaker.middleStringMaker);
    }
    
    public final String toShortString() {
        return this.toString(StringMaker.shortStringMaker);
    }
    
    public final String toLongString() {
        return this.toString(StringMaker.longStringMaker);
    }
    
    public int getModifiers() {
        if (this.modifiers == -1) {
            this.modifiers = this.extractInt(0);
        }
        return this.modifiers;
    }
    
    public String getName() {
        if (this.name == null) {
            this.name = this.extractString(1);
        }
        return this.name;
    }
    
    public Class getDeclaringType() {
        if (this.declaringType == null) {
            this.declaringType = this.extractType(2);
        }
        return this.declaringType;
    }
    
    public String getDeclaringTypeName() {
        if (this.declaringTypeName == null) {
            this.declaringTypeName = this.getDeclaringType().getName();
        }
        return this.declaringTypeName;
    }
    
    String fullTypeName(final Class type) {
        if (type == null) {
            return "ANONYMOUS";
        }
        if (type.isArray()) {
            return this.fullTypeName(type.getComponentType()) + "[]";
        }
        return type.getName().replace('$', '.');
    }
    
    String stripPackageName(final String name) {
        final int dot = name.lastIndexOf(46);
        if (dot == -1) {
            return name;
        }
        return name.substring(dot + 1);
    }
    
    String shortTypeName(final Class type) {
        if (type == null) {
            return "ANONYMOUS";
        }
        if (type.isArray()) {
            return this.shortTypeName(type.getComponentType()) + "[]";
        }
        return this.stripPackageName(type.getName()).replace('$', '.');
    }
    
    void addFullTypeNames(final StringBuffer buf, final Class[] types) {
        for (int i = 0; i < types.length; ++i) {
            if (i > 0) {
                buf.append(", ");
            }
            buf.append(this.fullTypeName(types[i]));
        }
    }
    
    void addShortTypeNames(final StringBuffer buf, final Class[] types) {
        for (int i = 0; i < types.length; ++i) {
            if (i > 0) {
                buf.append(", ");
            }
            buf.append(this.shortTypeName(types[i]));
        }
    }
    
    void addTypeArray(final StringBuffer buf, final Class[] types) {
        this.addFullTypeNames(buf, types);
    }
    
    public void setLookupClassLoader(final ClassLoader loader) {
        this.lookupClassLoader = loader;
    }
    
    private ClassLoader getLookupClassLoader() {
        if (this.lookupClassLoader == null) {
            this.lookupClassLoader = this.getClass().getClassLoader();
        }
        return this.lookupClassLoader;
    }
    
    public SignatureImpl(final String stringRep) {
        this.modifiers = -1;
        this.lookupClassLoader = null;
        this.stringRep = stringRep;
    }
    
    String extractString(int n) {
        int startIndex = 0;
        int endIndex = this.stringRep.indexOf(45);
        while (n-- > 0) {
            startIndex = endIndex + 1;
            endIndex = this.stringRep.indexOf(45, startIndex);
        }
        if (endIndex == -1) {
            endIndex = this.stringRep.length();
        }
        return this.stringRep.substring(startIndex, endIndex);
    }
    
    int extractInt(final int n) {
        final String s = this.extractString(n);
        return Integer.parseInt(s, 16);
    }
    
    Class extractType(final int n) {
        final String s = this.extractString(n);
        return Factory.makeClass(s, this.getLookupClassLoader());
    }
    
    String[] extractStrings(final int n) {
        final String s = this.extractString(n);
        final StringTokenizer st = new StringTokenizer(s, ":");
        final int N = st.countTokens();
        final String[] ret = new String[N];
        for (int i = 0; i < N; ++i) {
            ret[i] = st.nextToken();
        }
        return ret;
    }
    
    Class[] extractTypes(final int n) {
        final String s = this.extractString(n);
        final StringTokenizer st = new StringTokenizer(s, ":");
        final int N = st.countTokens();
        final Class[] ret = new Class[N];
        for (int i = 0; i < N; ++i) {
            ret[i] = Factory.makeClass(st.nextToken(), this.getLookupClassLoader());
        }
        return ret;
    }
    
    static void setUseCache(final boolean b) {
        SignatureImpl.useCache = b;
    }
    
    static boolean getUseCache() {
        return SignatureImpl.useCache;
    }
    
    static {
        SignatureImpl.useCache = true;
        SignatureImpl.EMPTY_STRING_ARRAY = new String[0];
        SignatureImpl.EMPTY_CLASS_ARRAY = new Class[0];
    }
    
    private static final class CacheImpl implements Cache
    {
        private SoftReference toStringCacheRef;
        
        public CacheImpl() {
            this.makeCache();
        }
        
        public String get(final int cacheOffset) {
            final String[] cachedArray = this.array();
            if (cachedArray == null) {
                return null;
            }
            return cachedArray[cacheOffset];
        }
        
        public void set(final int cacheOffset, final String result) {
            String[] cachedArray = this.array();
            if (cachedArray == null) {
                cachedArray = this.makeCache();
            }
            cachedArray[cacheOffset] = result;
        }
        
        private String[] array() {
            return this.toStringCacheRef.get();
        }
        
        private String[] makeCache() {
            final String[] array = new String[3];
            this.toStringCacheRef = new SoftReference((T)(Object)array);
            return array;
        }
    }
    
    private interface Cache
    {
        String get(final int p0);
        
        void set(final int p0, final String p1);
    }
}
