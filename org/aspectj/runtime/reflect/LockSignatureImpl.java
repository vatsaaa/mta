// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.runtime.reflect;

import org.aspectj.lang.reflect.LockSignature;

class LockSignatureImpl extends SignatureImpl implements LockSignature
{
    private Class parameterType;
    
    LockSignatureImpl(final Class c) {
        super(8, "lock", c);
        this.parameterType = c;
    }
    
    LockSignatureImpl(final String stringRep) {
        super(stringRep);
    }
    
    protected String createToString(final StringMaker sm) {
        if (this.parameterType == null) {
            this.parameterType = this.extractType(3);
        }
        return "lock(" + sm.makeTypeName(this.parameterType) + ")";
    }
    
    public Class getParameterType() {
        if (this.parameterType == null) {
            this.parameterType = this.extractType(3);
        }
        return this.parameterType;
    }
}
