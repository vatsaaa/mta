// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.InterTypeDeclaration;

public class InterTypeDeclarationImpl implements InterTypeDeclaration
{
    private AjType<?> declaringType;
    protected String targetTypeName;
    private AjType<?> targetType;
    private int modifiers;
    
    public InterTypeDeclarationImpl(final AjType<?> decType, final String target, final int mods) {
        this.declaringType = decType;
        this.targetTypeName = target;
        this.modifiers = mods;
        try {
            this.targetType = (AjType<?>)StringToType.stringToType(target, decType.getJavaClass());
        }
        catch (ClassNotFoundException ex) {}
    }
    
    public InterTypeDeclarationImpl(final AjType<?> decType, final AjType<?> targetType, final int mods) {
        this.declaringType = decType;
        this.targetType = targetType;
        this.targetTypeName = targetType.getName();
        this.modifiers = mods;
    }
    
    public AjType<?> getDeclaringType() {
        return this.declaringType;
    }
    
    public AjType<?> getTargetType() throws ClassNotFoundException {
        if (this.targetType == null) {
            throw new ClassNotFoundException(this.targetTypeName);
        }
        return this.targetType;
    }
    
    public int getModifiers() {
        return this.modifiers;
    }
}
