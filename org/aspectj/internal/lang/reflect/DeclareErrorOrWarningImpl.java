// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.PointcutExpression;
import org.aspectj.lang.reflect.DeclareErrorOrWarning;

public class DeclareErrorOrWarningImpl implements DeclareErrorOrWarning
{
    private PointcutExpression pc;
    private String msg;
    private boolean isError;
    private AjType declaringType;
    
    public DeclareErrorOrWarningImpl(final String pointcut, final String message, final boolean isError, final AjType decType) {
        this.pc = new PointcutExpressionImpl(pointcut);
        this.msg = message;
        this.isError = isError;
        this.declaringType = decType;
    }
    
    public AjType getDeclaringType() {
        return this.declaringType;
    }
    
    public PointcutExpression getPointcutExpression() {
        return this.pc;
    }
    
    public String getMessage() {
        return this.msg;
    }
    
    public boolean isError() {
        return this.isError;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("declare ");
        sb.append(this.isError() ? "error : " : "warning : ");
        sb.append(this.getPointcutExpression().asString());
        sb.append(" : ");
        sb.append("\"");
        sb.append(this.getMessage());
        sb.append("\"");
        return sb.toString();
    }
}
