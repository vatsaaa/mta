// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Modifier;
import org.aspectj.lang.reflect.AjTypeSystem;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.InterTypeFieldDeclaration;

public class InterTypeFieldDeclarationImpl extends InterTypeDeclarationImpl implements InterTypeFieldDeclaration
{
    private String name;
    private AjType<?> type;
    private Type genericType;
    
    public InterTypeFieldDeclarationImpl(final AjType<?> decType, final String target, final int mods, final String name, final AjType<?> type, final Type genericType) {
        super(decType, target, mods);
        this.name = name;
        this.type = type;
        this.genericType = genericType;
    }
    
    public InterTypeFieldDeclarationImpl(final AjType<?> decType, final AjType<?> targetType, final Field base) {
        super(decType, targetType, base.getModifiers());
        this.name = base.getName();
        this.type = AjTypeSystem.getAjType(base.getType());
        final Type gt = base.getGenericType();
        if (gt instanceof Class) {
            this.genericType = AjTypeSystem.getAjType((Class<Object>)gt);
        }
        else {
            this.genericType = gt;
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public AjType<?> getType() {
        return this.type;
    }
    
    public Type getGenericType() {
        return this.genericType;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(Modifier.toString(this.getModifiers()));
        sb.append(" ");
        sb.append(this.getType().toString());
        sb.append(" ");
        sb.append(this.targetTypeName);
        sb.append(".");
        sb.append(this.getName());
        return sb.toString();
    }
}
