// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.annotation.AdviceName;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.AjType;
import java.lang.reflect.Type;
import org.aspectj.lang.reflect.PointcutExpression;
import java.lang.reflect.Method;
import org.aspectj.lang.reflect.AdviceKind;
import org.aspectj.lang.reflect.Advice;

public class AdviceImpl implements Advice
{
    private static final String AJC_INTERNAL = "org.aspectj.runtime.internal";
    private final AdviceKind kind;
    private final Method adviceMethod;
    private PointcutExpression pointcutExpression;
    private boolean hasExtraParam;
    private Type[] genericParameterTypes;
    private AjType[] parameterTypes;
    private AjType[] exceptionTypes;
    
    protected AdviceImpl(final Method method, final String pointcut, final AdviceKind type) {
        this.hasExtraParam = false;
        this.kind = type;
        this.adviceMethod = method;
        this.pointcutExpression = new PointcutExpressionImpl(pointcut);
    }
    
    protected AdviceImpl(final Method method, final String pointcut, final AdviceKind type, final String extraParamName) {
        this(method, pointcut, type);
        this.hasExtraParam = true;
    }
    
    public AjType getDeclaringType() {
        return AjTypeSystem.getAjType(this.adviceMethod.getDeclaringClass());
    }
    
    public Type[] getGenericParameterTypes() {
        if (this.genericParameterTypes == null) {
            final Type[] genTypes = this.adviceMethod.getGenericParameterTypes();
            int syntheticCount = 0;
            for (final Type t : genTypes) {
                if (t instanceof Class && ((Class)t).getPackage().getName().equals("org.aspectj.runtime.internal")) {
                    ++syntheticCount;
                }
            }
            this.genericParameterTypes = new Type[genTypes.length - syntheticCount];
            for (int i = 0; i < this.genericParameterTypes.length; ++i) {
                if (genTypes[i] instanceof Class) {
                    this.genericParameterTypes[i] = AjTypeSystem.getAjType((Class<Object>)genTypes[i]);
                }
                else {
                    this.genericParameterTypes[i] = genTypes[i];
                }
            }
        }
        return this.genericParameterTypes;
    }
    
    public AjType<?>[] getParameterTypes() {
        if (this.parameterTypes == null) {
            final Class<?>[] ptypes = this.adviceMethod.getParameterTypes();
            int syntheticCount = 0;
            for (final Class<?> c : ptypes) {
                if (c.getPackage().getName().equals("org.aspectj.runtime.internal")) {
                    ++syntheticCount;
                }
            }
            this.parameterTypes = new AjType[ptypes.length - syntheticCount];
            for (int i = 0; i < this.parameterTypes.length; ++i) {
                this.parameterTypes[i] = AjTypeSystem.getAjType(ptypes[i]);
            }
        }
        return (AjType<?>[])this.parameterTypes;
    }
    
    public AjType<?>[] getExceptionTypes() {
        if (this.exceptionTypes == null) {
            final Class<?>[] exTypes = this.adviceMethod.getExceptionTypes();
            this.exceptionTypes = new AjType[exTypes.length];
            for (int i = 0; i < exTypes.length; ++i) {
                this.exceptionTypes[i] = AjTypeSystem.getAjType(exTypes[i]);
            }
        }
        return (AjType<?>[])this.exceptionTypes;
    }
    
    public AdviceKind getKind() {
        return this.kind;
    }
    
    public String getName() {
        String adviceName = this.adviceMethod.getName();
        if (adviceName.startsWith("ajc$")) {
            adviceName = "";
            final AdviceName name = this.adviceMethod.getAnnotation(AdviceName.class);
            if (name != null) {
                adviceName = name.value();
            }
        }
        return adviceName;
    }
    
    public PointcutExpression getPointcutExpression() {
        return this.pointcutExpression;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        if (this.getName().length() > 0) {
            sb.append("@AdviceName(\"");
            sb.append(this.getName());
            sb.append("\") ");
        }
        if (this.getKind() == AdviceKind.AROUND) {
            sb.append(this.adviceMethod.getGenericReturnType().toString());
            sb.append(" ");
        }
        switch (this.getKind()) {
            case AFTER: {
                sb.append("after(");
                break;
            }
            case AFTER_RETURNING: {
                sb.append("after(");
                break;
            }
            case AFTER_THROWING: {
                sb.append("after(");
                break;
            }
            case AROUND: {
                sb.append("around(");
                break;
            }
            case BEFORE: {
                sb.append("before(");
                break;
            }
        }
        final AjType<?>[] ptypes = this.getParameterTypes();
        int len = ptypes.length;
        if (this.hasExtraParam) {
            --len;
        }
        for (int i = 0; i < len; ++i) {
            sb.append(ptypes[i].getName());
            if (i + 1 < len) {
                sb.append(",");
            }
        }
        sb.append(") ");
        switch (this.getKind()) {
            case AFTER_RETURNING: {
                sb.append("returning");
                if (this.hasExtraParam) {
                    sb.append("(");
                    sb.append(ptypes[len - 1].getName());
                    sb.append(") ");
                }
            }
            case AFTER_THROWING: {
                sb.append("throwing");
                if (this.hasExtraParam) {
                    sb.append("(");
                    sb.append(ptypes[len - 1].getName());
                    sb.append(") ");
                    break;
                }
                break;
            }
        }
        final AjType<?>[] exTypes = this.getExceptionTypes();
        if (exTypes.length > 0) {
            sb.append("throws ");
            for (int j = 0; j < exTypes.length; ++j) {
                sb.append(exTypes[j].getName());
                if (j + 1 < exTypes.length) {
                    sb.append(",");
                }
            }
            sb.append(" ");
        }
        sb.append(": ");
        sb.append(this.getPointcutExpression().asString());
        return sb.toString();
    }
}
