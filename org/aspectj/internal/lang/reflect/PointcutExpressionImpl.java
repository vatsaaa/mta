// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.PointcutExpression;

public class PointcutExpressionImpl implements PointcutExpression
{
    private String expression;
    
    public PointcutExpressionImpl(final String aPointcutExpression) {
        this.expression = aPointcutExpression;
    }
    
    public String asString() {
        return this.expression;
    }
    
    @Override
    public String toString() {
        return this.asString();
    }
}
