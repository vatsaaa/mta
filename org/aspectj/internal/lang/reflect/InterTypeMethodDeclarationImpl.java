// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;
import org.aspectj.lang.reflect.AjTypeSystem;
import java.lang.reflect.Type;
import org.aspectj.lang.reflect.AjType;
import java.lang.reflect.Method;
import org.aspectj.lang.reflect.InterTypeMethodDeclaration;

public class InterTypeMethodDeclarationImpl extends InterTypeDeclarationImpl implements InterTypeMethodDeclaration
{
    private String name;
    private Method baseMethod;
    private int parameterAdjustmentFactor;
    private AjType<?>[] parameterTypes;
    private Type[] genericParameterTypes;
    private AjType<?> returnType;
    private Type genericReturnType;
    private AjType<?>[] exceptionTypes;
    
    public InterTypeMethodDeclarationImpl(final AjType<?> decType, final String target, final int mods, final String name, final Method itdInterMethod) {
        super(decType, target, mods);
        this.parameterAdjustmentFactor = 1;
        this.name = name;
        this.baseMethod = itdInterMethod;
    }
    
    public InterTypeMethodDeclarationImpl(final AjType<?> decType, final AjType<?> targetType, final Method base, final int modifiers) {
        super(decType, targetType, modifiers);
        this.parameterAdjustmentFactor = 1;
        this.parameterAdjustmentFactor = 0;
        this.name = base.getName();
        this.baseMethod = base;
    }
    
    public String getName() {
        return this.name;
    }
    
    public AjType<?> getReturnType() {
        return AjTypeSystem.getAjType(this.baseMethod.getReturnType());
    }
    
    public Type getGenericReturnType() {
        final Type gRet = this.baseMethod.getGenericReturnType();
        if (gRet instanceof Class) {
            return AjTypeSystem.getAjType((Class<Object>)gRet);
        }
        return gRet;
    }
    
    public AjType<?>[] getParameterTypes() {
        final Class<?>[] baseTypes = this.baseMethod.getParameterTypes();
        final AjType<?>[] ret = (AjType<?>[])new AjType[baseTypes.length - this.parameterAdjustmentFactor];
        for (int i = this.parameterAdjustmentFactor; i < baseTypes.length; ++i) {
            ret[i - this.parameterAdjustmentFactor] = AjTypeSystem.getAjType(baseTypes[i]);
        }
        return ret;
    }
    
    public Type[] getGenericParameterTypes() {
        final Type[] baseTypes = this.baseMethod.getGenericParameterTypes();
        final Type[] ret = new AjType[baseTypes.length - this.parameterAdjustmentFactor];
        for (int i = this.parameterAdjustmentFactor; i < baseTypes.length; ++i) {
            if (baseTypes[i] instanceof Class) {
                ret[i - this.parameterAdjustmentFactor] = AjTypeSystem.getAjType((Class<Object>)baseTypes[i]);
            }
            else {
                ret[i - this.parameterAdjustmentFactor] = baseTypes[i];
            }
        }
        return ret;
    }
    
    public TypeVariable<Method>[] getTypeParameters() {
        return this.baseMethod.getTypeParameters();
    }
    
    public AjType<?>[] getExceptionTypes() {
        final Class<?>[] baseTypes = this.baseMethod.getExceptionTypes();
        final AjType<?>[] ret = (AjType<?>[])new AjType[baseTypes.length];
        for (int i = 0; i < baseTypes.length; ++i) {
            ret[i] = AjTypeSystem.getAjType(baseTypes[i]);
        }
        return ret;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(Modifier.toString(this.getModifiers()));
        sb.append(" ");
        sb.append(this.getReturnType().toString());
        sb.append(" ");
        sb.append(this.targetTypeName);
        sb.append(".");
        sb.append(this.getName());
        sb.append("(");
        final AjType<?>[] pTypes = this.getParameterTypes();
        for (int i = 0; i < pTypes.length - 1; ++i) {
            sb.append(pTypes[i].toString());
            sb.append(", ");
        }
        if (pTypes.length > 0) {
            sb.append(pTypes[pTypes.length - 1].toString());
        }
        sb.append(")");
        return sb.toString();
    }
}
