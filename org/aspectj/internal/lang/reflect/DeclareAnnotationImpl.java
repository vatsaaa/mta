// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.SignaturePattern;
import org.aspectj.lang.reflect.TypePattern;
import org.aspectj.lang.reflect.AjType;
import java.lang.annotation.Annotation;
import org.aspectj.lang.reflect.DeclareAnnotation;

public class DeclareAnnotationImpl implements DeclareAnnotation
{
    private Annotation theAnnotation;
    private String annText;
    private AjType<?> declaringType;
    private Kind kind;
    private TypePattern typePattern;
    private SignaturePattern signaturePattern;
    
    public DeclareAnnotationImpl(final AjType<?> declaring, final String kindString, final String pattern, final Annotation ann, final String annText) {
        this.declaringType = declaring;
        if (kindString.equals("at_type")) {
            this.kind = Kind.Type;
        }
        else if (kindString.equals("at_field")) {
            this.kind = Kind.Field;
        }
        else if (kindString.equals("at_method")) {
            this.kind = Kind.Method;
        }
        else {
            if (!kindString.equals("at_constructor")) {
                throw new IllegalStateException("Unknown declare annotation kind: " + kindString);
            }
            this.kind = Kind.Constructor;
        }
        if (this.kind == Kind.Type) {
            this.typePattern = new TypePatternImpl(pattern);
        }
        else {
            this.signaturePattern = new SignaturePatternImpl(pattern);
        }
        this.theAnnotation = ann;
        this.annText = annText;
    }
    
    public AjType<?> getDeclaringType() {
        return this.declaringType;
    }
    
    public Kind getKind() {
        return this.kind;
    }
    
    public SignaturePattern getSignaturePattern() {
        return this.signaturePattern;
    }
    
    public TypePattern getTypePattern() {
        return this.typePattern;
    }
    
    public Annotation getAnnotation() {
        return this.theAnnotation;
    }
    
    public String getAnnotationAsText() {
        return this.annText;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("declare @");
        switch (this.getKind()) {
            case Type: {
                sb.append("type : ");
                sb.append(this.getTypePattern().asString());
                break;
            }
            case Method: {
                sb.append("method : ");
                sb.append(this.getSignaturePattern().asString());
                break;
            }
            case Field: {
                sb.append("field : ");
                sb.append(this.getSignaturePattern().asString());
                break;
            }
            case Constructor: {
                sb.append("constructor : ");
                sb.append(this.getSignaturePattern().asString());
                break;
            }
        }
        sb.append(" : ");
        sb.append(this.getAnnotationAsText());
        return sb.toString();
    }
}
