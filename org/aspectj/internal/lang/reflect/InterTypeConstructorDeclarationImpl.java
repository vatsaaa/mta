// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.AjType;
import java.lang.reflect.Method;
import org.aspectj.lang.reflect.InterTypeConstructorDeclaration;

public class InterTypeConstructorDeclarationImpl extends InterTypeDeclarationImpl implements InterTypeConstructorDeclaration
{
    private Method baseMethod;
    
    public InterTypeConstructorDeclarationImpl(final AjType<?> decType, final String target, final int mods, final Method baseMethod) {
        super(decType, target, mods);
        this.baseMethod = baseMethod;
    }
    
    public AjType<?>[] getParameterTypes() {
        final Class<?>[] baseTypes = this.baseMethod.getParameterTypes();
        final AjType<?>[] ret = (AjType<?>[])new AjType[baseTypes.length - 1];
        for (int i = 1; i < baseTypes.length; ++i) {
            ret[i - 1] = AjTypeSystem.getAjType(baseTypes[i]);
        }
        return ret;
    }
    
    public Type[] getGenericParameterTypes() {
        final Type[] baseTypes = this.baseMethod.getGenericParameterTypes();
        final Type[] ret = new AjType[baseTypes.length - 1];
        for (int i = 1; i < baseTypes.length; ++i) {
            if (baseTypes[i] instanceof Class) {
                ret[i - 1] = AjTypeSystem.getAjType((Class<Object>)baseTypes[i]);
            }
            else {
                ret[i - 1] = baseTypes[i];
            }
        }
        return ret;
    }
    
    public AjType<?>[] getExceptionTypes() {
        final Class<?>[] baseTypes = this.baseMethod.getExceptionTypes();
        final AjType<?>[] ret = (AjType<?>[])new AjType[baseTypes.length];
        for (int i = 0; i < baseTypes.length; ++i) {
            ret[i] = AjTypeSystem.getAjType(baseTypes[i]);
        }
        return ret;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(Modifier.toString(this.getModifiers()));
        sb.append(" ");
        sb.append(this.targetTypeName);
        sb.append(".new");
        sb.append("(");
        final AjType<?>[] pTypes = this.getParameterTypes();
        for (int i = 0; i < pTypes.length - 1; ++i) {
            sb.append(pTypes[i].toString());
            sb.append(", ");
        }
        if (pTypes.length > 0) {
            sb.append(pTypes[pTypes.length - 1].toString());
        }
        sb.append(")");
        return sb.toString();
    }
}
