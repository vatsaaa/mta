// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import java.lang.reflect.Type;
import org.aspectj.lang.reflect.TypePattern;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.DeclareParents;

public class DeclareParentsImpl implements DeclareParents
{
    private AjType<?> declaringType;
    private TypePattern targetTypesPattern;
    private Type[] parents;
    private String parentsString;
    private String firstMissingTypeName;
    private boolean isExtends;
    private boolean parentsError;
    
    public DeclareParentsImpl(final String targets, final String parentsAsString, final boolean isExtends, final AjType<?> declaring) {
        this.parentsError = false;
        this.targetTypesPattern = new TypePatternImpl(targets);
        this.isExtends = isExtends;
        this.declaringType = declaring;
        this.parentsString = parentsAsString;
        try {
            this.parents = StringToType.commaSeparatedListToTypeArray(parentsAsString, declaring.getJavaClass());
        }
        catch (ClassNotFoundException cnfEx) {
            this.parentsError = true;
            this.firstMissingTypeName = cnfEx.getMessage();
        }
    }
    
    public AjType getDeclaringType() {
        return this.declaringType;
    }
    
    public TypePattern getTargetTypesPattern() {
        return this.targetTypesPattern;
    }
    
    public boolean isExtends() {
        return this.isExtends;
    }
    
    public boolean isImplements() {
        return !this.isExtends;
    }
    
    public Type[] getParentTypes() throws ClassNotFoundException {
        if (this.parentsError) {
            throw new ClassNotFoundException(this.firstMissingTypeName);
        }
        return this.parents;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("declare parents : ");
        sb.append(this.getTargetTypesPattern().asString());
        sb.append(this.isExtends() ? " extends " : " implements ");
        sb.append(this.parentsString);
        return sb.toString();
    }
}
