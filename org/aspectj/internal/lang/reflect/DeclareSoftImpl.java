// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.PointcutExpression;
import org.aspectj.lang.reflect.AjType;
import org.aspectj.lang.reflect.DeclareSoft;

public class DeclareSoftImpl implements DeclareSoft
{
    private AjType<?> declaringType;
    private PointcutExpression pointcut;
    private AjType<?> exceptionType;
    private String missingTypeName;
    
    public DeclareSoftImpl(final AjType<?> declaringType, final String pcut, final String exceptionTypeName) {
        this.declaringType = declaringType;
        this.pointcut = new PointcutExpressionImpl(pcut);
        try {
            final ClassLoader cl = declaringType.getJavaClass().getClassLoader();
            this.exceptionType = AjTypeSystem.getAjType(Class.forName(exceptionTypeName, false, cl));
        }
        catch (ClassNotFoundException ex) {
            this.missingTypeName = exceptionTypeName;
        }
    }
    
    public AjType getDeclaringType() {
        return this.declaringType;
    }
    
    public AjType getSoftenedExceptionType() throws ClassNotFoundException {
        if (this.missingTypeName != null) {
            throw new ClassNotFoundException(this.missingTypeName);
        }
        return this.exceptionType;
    }
    
    public PointcutExpression getPointcutExpression() {
        return this.pointcut;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("declare soft : ");
        if (this.missingTypeName != null) {
            sb.append(this.exceptionType.getName());
        }
        else {
            sb.append(this.missingTypeName);
        }
        sb.append(" : ");
        sb.append(this.getPointcutExpression().asString());
        return sb.toString();
    }
}
