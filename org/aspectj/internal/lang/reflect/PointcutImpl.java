// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import java.util.StringTokenizer;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.AjType;
import java.lang.reflect.Method;
import org.aspectj.lang.reflect.PointcutExpression;
import org.aspectj.lang.reflect.Pointcut;

public class PointcutImpl implements Pointcut
{
    private final String name;
    private final PointcutExpression pc;
    private final Method baseMethod;
    private final AjType declaringType;
    private String[] parameterNames;
    
    protected PointcutImpl(final String name, final String pc, final Method method, final AjType declaringType, final String pNames) {
        this.parameterNames = new String[0];
        this.name = name;
        this.pc = new PointcutExpressionImpl(pc);
        this.baseMethod = method;
        this.declaringType = declaringType;
        this.parameterNames = this.splitOnComma(pNames);
    }
    
    public PointcutExpression getPointcutExpression() {
        return this.pc;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getModifiers() {
        return this.baseMethod.getModifiers();
    }
    
    public AjType<?>[] getParameterTypes() {
        final Class<?>[] baseParamTypes = this.baseMethod.getParameterTypes();
        final AjType<?>[] ajParamTypes = (AjType<?>[])new AjType[baseParamTypes.length];
        for (int i = 0; i < ajParamTypes.length; ++i) {
            ajParamTypes[i] = AjTypeSystem.getAjType(baseParamTypes[i]);
        }
        return ajParamTypes;
    }
    
    public AjType getDeclaringType() {
        return this.declaringType;
    }
    
    public String[] getParameterNames() {
        return this.parameterNames;
    }
    
    private String[] splitOnComma(final String s) {
        final StringTokenizer strTok = new StringTokenizer(s, ",");
        final String[] ret = new String[strTok.countTokens()];
        for (int i = 0; i < ret.length; ++i) {
            ret[i] = strTok.nextToken().trim();
        }
        return ret;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(this.getName());
        sb.append("(");
        final AjType<?>[] ptypes = this.getParameterTypes();
        for (int i = 0; i < ptypes.length; ++i) {
            sb.append(ptypes[i].getName());
            if (this.parameterNames != null && this.parameterNames[i] != null) {
                sb.append(" ");
                sb.append(this.parameterNames[i]);
            }
            if (i + 1 < ptypes.length) {
                sb.append(",");
            }
        }
        sb.append(") : ");
        sb.append(this.getPointcutExpression().asString());
        return sb.toString();
    }
}
