// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.TypeVariable;
import org.aspectj.lang.reflect.AjTypeSystem;
import java.util.StringTokenizer;
import java.lang.reflect.Type;

public class StringToType
{
    public static Type[] commaSeparatedListToTypeArray(final String typeNames, final Class classScope) throws ClassNotFoundException {
        final StringTokenizer strTok = new StringTokenizer(typeNames, ",");
        final Type[] ret = new Type[strTok.countTokens()];
        int index = 0;
        while (strTok.hasMoreTokens()) {
            final String typeName = strTok.nextToken().trim();
            ret[index++] = stringToType(typeName, classScope);
        }
        return ret;
    }
    
    public static Type stringToType(final String typeName, final Class classScope) throws ClassNotFoundException {
        try {
            if (typeName.indexOf("<") == -1) {
                return AjTypeSystem.getAjType(Class.forName(typeName, false, classScope.getClassLoader()));
            }
            return makeParameterizedType(typeName, classScope);
        }
        catch (ClassNotFoundException e) {
            final TypeVariable[] tVars = classScope.getTypeParameters();
            for (int i = 0; i < tVars.length; ++i) {
                if (tVars[i].getName().equals(typeName)) {
                    return tVars[i];
                }
            }
            throw new ClassNotFoundException(typeName);
        }
    }
    
    private static Type makeParameterizedType(final String typeName, final Class classScope) throws ClassNotFoundException {
        final int paramStart = typeName.indexOf(60);
        final String baseName = typeName.substring(0, paramStart);
        final Class baseClass = Class.forName(baseName, false, classScope.getClassLoader());
        final int paramEnd = typeName.lastIndexOf(62);
        final String params = typeName.substring(paramStart + 1, paramEnd);
        final Type[] typeParams = commaSeparatedListToTypeArray(params, classScope);
        return new ParameterizedType() {
            public Type[] getActualTypeArguments() {
                return typeParams;
            }
            
            public Type getRawType() {
                return baseClass;
            }
            
            public Type getOwnerType() {
                return baseClass.getEnclosingClass();
            }
        };
    }
}
