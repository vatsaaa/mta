// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.internal.lang.reflect;

import org.aspectj.internal.lang.annotation.ajcPrivileged;
import java.lang.reflect.TypeVariable;
import org.aspectj.internal.lang.annotation.ajcDeclarePrecedence;
import org.aspectj.lang.reflect.DeclarePrecedence;
import org.aspectj.internal.lang.annotation.ajcDeclareAnnotation;
import org.aspectj.lang.reflect.DeclareAnnotation;
import org.aspectj.internal.lang.annotation.ajcDeclareSoft;
import org.aspectj.lang.reflect.DeclareSoft;
import org.aspectj.internal.lang.annotation.ajcDeclareParents;
import org.aspectj.internal.lang.annotation.ajcDeclareEoW;
import org.aspectj.lang.reflect.DeclareErrorOrWarning;
import org.aspectj.lang.annotation.DeclareParents;
import java.lang.reflect.Modifier;
import org.aspectj.internal.lang.annotation.ajcITD;
import org.aspectj.lang.reflect.NoSuchAdviceException;
import java.util.Set;
import java.util.Collection;
import java.util.Arrays;
import java.util.EnumSet;
import org.aspectj.lang.reflect.AdviceKind;
import org.aspectj.lang.reflect.AjTypeSystem;
import org.aspectj.lang.reflect.NoSuchPointcutException;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Before;
import java.util.List;
import org.aspectj.lang.annotation.DeclareError;
import org.aspectj.lang.annotation.DeclareWarning;
import java.util.ArrayList;
import java.lang.reflect.Field;
import java.lang.annotation.Annotation;
import org.aspectj.lang.reflect.PerClauseKind;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.PerClause;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import org.aspectj.lang.reflect.InterTypeConstructorDeclaration;
import org.aspectj.lang.reflect.InterTypeFieldDeclaration;
import org.aspectj.lang.reflect.InterTypeMethodDeclaration;
import org.aspectj.lang.reflect.Advice;
import org.aspectj.lang.reflect.Pointcut;
import org.aspectj.lang.reflect.AjType;

public class AjTypeImpl<T> implements AjType<T>
{
    private static final String ajcMagic = "ajc$";
    private Class<T> clazz;
    private Pointcut[] declaredPointcuts;
    private Pointcut[] pointcuts;
    private Advice[] declaredAdvice;
    private Advice[] advice;
    private InterTypeMethodDeclaration[] declaredITDMethods;
    private InterTypeMethodDeclaration[] itdMethods;
    private InterTypeFieldDeclaration[] declaredITDFields;
    private InterTypeFieldDeclaration[] itdFields;
    private InterTypeConstructorDeclaration[] itdCons;
    private InterTypeConstructorDeclaration[] declaredITDCons;
    
    public AjTypeImpl(final Class<T> fromClass) {
        this.declaredPointcuts = null;
        this.pointcuts = null;
        this.declaredAdvice = null;
        this.advice = null;
        this.declaredITDMethods = null;
        this.itdMethods = null;
        this.declaredITDFields = null;
        this.itdFields = null;
        this.itdCons = null;
        this.declaredITDCons = null;
        this.clazz = fromClass;
    }
    
    public String getName() {
        return this.clazz.getName();
    }
    
    public Package getPackage() {
        return this.clazz.getPackage();
    }
    
    public AjType<?>[] getInterfaces() {
        final Class<?>[] baseInterfaces = this.clazz.getInterfaces();
        return this.toAjTypeArray(baseInterfaces);
    }
    
    public int getModifiers() {
        return this.clazz.getModifiers();
    }
    
    public Class<T> getJavaClass() {
        return this.clazz;
    }
    
    public AjType<? super T> getSupertype() {
        final Class<? super T> superclass = this.clazz.getSuperclass();
        return (superclass == null) ? null : new AjTypeImpl<Object>(superclass);
    }
    
    public Type getGenericSupertype() {
        return this.clazz.getGenericSuperclass();
    }
    
    public Method getEnclosingMethod() {
        return this.clazz.getEnclosingMethod();
    }
    
    public Constructor getEnclosingConstructor() {
        return this.clazz.getEnclosingConstructor();
    }
    
    public AjType<?> getEnclosingType() {
        final Class<?> enc = this.clazz.getEnclosingClass();
        return (enc != null) ? new AjTypeImpl<Object>(enc) : null;
    }
    
    public AjType<?> getDeclaringType() {
        final Class dec = this.clazz.getDeclaringClass();
        return (dec != null) ? new AjTypeImpl<Object>(dec) : null;
    }
    
    public PerClause getPerClause() {
        if (!this.isAspect()) {
            return null;
        }
        final Aspect aspectAnn = this.clazz.getAnnotation(Aspect.class);
        final String perClause = aspectAnn.value();
        if (perClause.equals("")) {
            if (this.getSupertype().isAspect()) {
                return this.getSupertype().getPerClause();
            }
            return new PerClauseImpl(PerClauseKind.SINGLETON);
        }
        else {
            if (perClause.startsWith("perthis(")) {
                return new PointcutBasedPerClauseImpl(PerClauseKind.PERTHIS, perClause.substring("perthis(".length(), perClause.length() - 1));
            }
            if (perClause.startsWith("pertarget(")) {
                return new PointcutBasedPerClauseImpl(PerClauseKind.PERTARGET, perClause.substring("pertarget(".length(), perClause.length() - 1));
            }
            if (perClause.startsWith("percflow(")) {
                return new PointcutBasedPerClauseImpl(PerClauseKind.PERCFLOW, perClause.substring("percflow(".length(), perClause.length() - 1));
            }
            if (perClause.startsWith("percflowbelow(")) {
                return new PointcutBasedPerClauseImpl(PerClauseKind.PERCFLOWBELOW, perClause.substring("percflowbelow(".length(), perClause.length() - 1));
            }
            if (perClause.startsWith("pertypewithin")) {
                return new TypePatternBasedPerClauseImpl(PerClauseKind.PERTYPEWITHIN, perClause.substring("pertypewithin(".length(), perClause.length() - 1));
            }
            throw new IllegalStateException("Per-clause not recognized: " + perClause);
        }
    }
    
    public boolean isAnnotationPresent(final Class<? extends Annotation> annotationType) {
        return this.clazz.isAnnotationPresent(annotationType);
    }
    
    public <A extends Annotation> A getAnnotation(final Class<A> annotationType) {
        return this.clazz.getAnnotation(annotationType);
    }
    
    public Annotation[] getAnnotations() {
        return this.clazz.getAnnotations();
    }
    
    public Annotation[] getDeclaredAnnotations() {
        return this.clazz.getDeclaredAnnotations();
    }
    
    public AjType<?>[] getAjTypes() {
        final Class[] classes = this.clazz.getClasses();
        return this.toAjTypeArray(classes);
    }
    
    public AjType<?>[] getDeclaredAjTypes() {
        final Class[] classes = this.clazz.getDeclaredClasses();
        return this.toAjTypeArray(classes);
    }
    
    public Constructor getConstructor(final AjType<?>... parameterTypes) throws NoSuchMethodException {
        return this.clazz.getConstructor(this.toClassArray(parameterTypes));
    }
    
    public Constructor[] getConstructors() {
        return this.clazz.getConstructors();
    }
    
    public Constructor getDeclaredConstructor(final AjType<?>... parameterTypes) throws NoSuchMethodException {
        return this.clazz.getDeclaredConstructor(this.toClassArray(parameterTypes));
    }
    
    public Constructor[] getDeclaredConstructors() {
        return this.clazz.getDeclaredConstructors();
    }
    
    public Field getDeclaredField(final String name) throws NoSuchFieldException {
        final Field f = this.clazz.getDeclaredField(name);
        if (f.getName().startsWith("ajc$")) {
            throw new NoSuchFieldException(name);
        }
        return f;
    }
    
    public Field[] getDeclaredFields() {
        final Field[] fields = this.clazz.getDeclaredFields();
        final List<Field> filteredFields = new ArrayList<Field>();
        for (final Field field : fields) {
            if (!field.getName().startsWith("ajc$") && !field.isAnnotationPresent(DeclareWarning.class) && !field.isAnnotationPresent(DeclareError.class)) {
                filteredFields.add(field);
            }
        }
        final Field[] ret = new Field[filteredFields.size()];
        filteredFields.toArray(ret);
        return ret;
    }
    
    public Field getField(final String name) throws NoSuchFieldException {
        final Field f = this.clazz.getField(name);
        if (f.getName().startsWith("ajc$")) {
            throw new NoSuchFieldException(name);
        }
        return f;
    }
    
    public Field[] getFields() {
        final Field[] fields = this.clazz.getFields();
        final List<Field> filteredFields = new ArrayList<Field>();
        for (final Field field : fields) {
            if (!field.getName().startsWith("ajc$") && !field.isAnnotationPresent(DeclareWarning.class) && !field.isAnnotationPresent(DeclareError.class)) {
                filteredFields.add(field);
            }
        }
        final Field[] ret = new Field[filteredFields.size()];
        filteredFields.toArray(ret);
        return ret;
    }
    
    public Method getDeclaredMethod(final String name, final AjType<?>... parameterTypes) throws NoSuchMethodException {
        final Method m = this.clazz.getDeclaredMethod(name, this.toClassArray(parameterTypes));
        if (!this.isReallyAMethod(m)) {
            throw new NoSuchMethodException(name);
        }
        return m;
    }
    
    public Method getMethod(final String name, final AjType<?>... parameterTypes) throws NoSuchMethodException {
        final Method m = this.clazz.getMethod(name, this.toClassArray(parameterTypes));
        if (!this.isReallyAMethod(m)) {
            throw new NoSuchMethodException(name);
        }
        return m;
    }
    
    public Method[] getDeclaredMethods() {
        final Method[] methods = this.clazz.getDeclaredMethods();
        final List<Method> filteredMethods = new ArrayList<Method>();
        for (final Method method : methods) {
            if (this.isReallyAMethod(method)) {
                filteredMethods.add(method);
            }
        }
        final Method[] ret = new Method[filteredMethods.size()];
        filteredMethods.toArray(ret);
        return ret;
    }
    
    public Method[] getMethods() {
        final Method[] methods = this.clazz.getMethods();
        final List<Method> filteredMethods = new ArrayList<Method>();
        for (final Method method : methods) {
            if (this.isReallyAMethod(method)) {
                filteredMethods.add(method);
            }
        }
        final Method[] ret = new Method[filteredMethods.size()];
        filteredMethods.toArray(ret);
        return ret;
    }
    
    private boolean isReallyAMethod(final Method method) {
        return !method.getName().startsWith("ajc$") && (method.getAnnotations().length == 0 || (!method.isAnnotationPresent(org.aspectj.lang.annotation.Pointcut.class) && !method.isAnnotationPresent(Before.class) && !method.isAnnotationPresent(After.class) && !method.isAnnotationPresent(AfterReturning.class) && !method.isAnnotationPresent(AfterThrowing.class) && !method.isAnnotationPresent(Around.class)));
    }
    
    public Pointcut getDeclaredPointcut(final String name) throws NoSuchPointcutException {
        final Pointcut[] arr$;
        final Pointcut[] pcs = arr$ = this.getDeclaredPointcuts();
        for (final Pointcut pc : arr$) {
            if (pc.getName().equals(name)) {
                return pc;
            }
        }
        throw new NoSuchPointcutException(name);
    }
    
    public Pointcut getPointcut(final String name) throws NoSuchPointcutException {
        final Pointcut[] arr$;
        final Pointcut[] pcs = arr$ = this.getPointcuts();
        for (final Pointcut pc : arr$) {
            if (pc.getName().equals(name)) {
                return pc;
            }
        }
        throw new NoSuchPointcutException(name);
    }
    
    public Pointcut[] getDeclaredPointcuts() {
        if (this.declaredPointcuts != null) {
            return this.declaredPointcuts;
        }
        final List<Pointcut> pointcuts = new ArrayList<Pointcut>();
        final Method[] arr$;
        final Method[] methods = arr$ = this.clazz.getDeclaredMethods();
        for (final Method method : arr$) {
            final Pointcut pc = this.asPointcut(method);
            if (pc != null) {
                pointcuts.add(pc);
            }
        }
        final Pointcut[] ret = new Pointcut[pointcuts.size()];
        pointcuts.toArray(ret);
        return this.declaredPointcuts = ret;
    }
    
    public Pointcut[] getPointcuts() {
        if (this.pointcuts != null) {
            return this.pointcuts;
        }
        final List<Pointcut> pcuts = new ArrayList<Pointcut>();
        final Method[] arr$;
        final Method[] methods = arr$ = this.clazz.getMethods();
        for (final Method method : arr$) {
            final Pointcut pc = this.asPointcut(method);
            if (pc != null) {
                pcuts.add(pc);
            }
        }
        final Pointcut[] ret = new Pointcut[pcuts.size()];
        pcuts.toArray(ret);
        return this.pointcuts = ret;
    }
    
    private Pointcut asPointcut(final Method method) {
        final org.aspectj.lang.annotation.Pointcut pcAnn = method.getAnnotation(org.aspectj.lang.annotation.Pointcut.class);
        if (pcAnn != null) {
            String name = method.getName();
            if (name.startsWith("ajc$")) {
                final int nameStart = name.indexOf("$$");
                name = name.substring(nameStart + 2, name.length());
                final int nextDollar = name.indexOf("$");
                if (nextDollar != -1) {
                    name = name.substring(0, nextDollar);
                }
            }
            return new PointcutImpl(name, pcAnn.value(), method, AjTypeSystem.getAjType(method.getDeclaringClass()), pcAnn.argNames());
        }
        return null;
    }
    
    public Advice[] getDeclaredAdvice(final AdviceKind... ofType) {
        Set<AdviceKind> types;
        if (ofType.length == 0) {
            types = EnumSet.allOf(AdviceKind.class);
        }
        else {
            types = EnumSet.noneOf(AdviceKind.class);
            types.addAll(Arrays.asList(ofType));
        }
        return this.getDeclaredAdvice(types);
    }
    
    public Advice[] getAdvice(final AdviceKind... ofType) {
        Set<AdviceKind> types;
        if (ofType.length == 0) {
            types = EnumSet.allOf(AdviceKind.class);
        }
        else {
            types = EnumSet.noneOf(AdviceKind.class);
            types.addAll(Arrays.asList(ofType));
        }
        return this.getAdvice(types);
    }
    
    private Advice[] getDeclaredAdvice(final Set ofAdviceTypes) {
        if (this.declaredAdvice == null) {
            this.initDeclaredAdvice();
        }
        final List<Advice> adviceList = new ArrayList<Advice>();
        for (final Advice a : this.declaredAdvice) {
            if (ofAdviceTypes.contains(a.getKind())) {
                adviceList.add(a);
            }
        }
        final Advice[] ret = new Advice[adviceList.size()];
        adviceList.toArray(ret);
        return ret;
    }
    
    private void initDeclaredAdvice() {
        final Method[] methods = this.clazz.getDeclaredMethods();
        final List<Advice> adviceList = new ArrayList<Advice>();
        for (final Method method : methods) {
            final Advice advice = this.asAdvice(method);
            if (advice != null) {
                adviceList.add(advice);
            }
        }
        adviceList.toArray(this.declaredAdvice = new Advice[adviceList.size()]);
    }
    
    private Advice[] getAdvice(final Set ofAdviceTypes) {
        if (this.advice == null) {
            this.initAdvice();
        }
        final List<Advice> adviceList = new ArrayList<Advice>();
        for (final Advice a : this.advice) {
            if (ofAdviceTypes.contains(a.getKind())) {
                adviceList.add(a);
            }
        }
        final Advice[] ret = new Advice[adviceList.size()];
        adviceList.toArray(ret);
        return ret;
    }
    
    private void initAdvice() {
        final Method[] methods = this.clazz.getMethods();
        final List<Advice> adviceList = new ArrayList<Advice>();
        for (final Method method : methods) {
            final Advice advice = this.asAdvice(method);
            if (advice != null) {
                adviceList.add(advice);
            }
        }
        adviceList.toArray(this.advice = new Advice[adviceList.size()]);
    }
    
    public Advice getAdvice(final String name) throws NoSuchAdviceException {
        if (name.equals("")) {
            throw new IllegalArgumentException("use getAdvice(AdviceType...) instead for un-named advice");
        }
        if (this.advice == null) {
            this.initAdvice();
        }
        for (final Advice a : this.advice) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        throw new NoSuchAdviceException(name);
    }
    
    public Advice getDeclaredAdvice(final String name) throws NoSuchAdviceException {
        if (name.equals("")) {
            throw new IllegalArgumentException("use getAdvice(AdviceType...) instead for un-named advice");
        }
        if (this.declaredAdvice == null) {
            this.initDeclaredAdvice();
        }
        for (final Advice a : this.declaredAdvice) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        throw new NoSuchAdviceException(name);
    }
    
    private Advice asAdvice(final Method method) {
        if (method.getAnnotations().length == 0) {
            return null;
        }
        final Before beforeAnn = method.getAnnotation(Before.class);
        if (beforeAnn != null) {
            return new AdviceImpl(method, beforeAnn.value(), AdviceKind.BEFORE);
        }
        final After afterAnn = method.getAnnotation(After.class);
        if (afterAnn != null) {
            return new AdviceImpl(method, afterAnn.value(), AdviceKind.AFTER);
        }
        final AfterReturning afterReturningAnn = method.getAnnotation(AfterReturning.class);
        if (afterReturningAnn != null) {
            String pcExpr = afterReturningAnn.pointcut();
            if (pcExpr.equals("")) {
                pcExpr = afterReturningAnn.value();
            }
            return new AdviceImpl(method, pcExpr, AdviceKind.AFTER_RETURNING, afterReturningAnn.returning());
        }
        final AfterThrowing afterThrowingAnn = method.getAnnotation(AfterThrowing.class);
        if (afterThrowingAnn != null) {
            String pcExpr2 = afterThrowingAnn.pointcut();
            if (pcExpr2 == null) {
                pcExpr2 = afterThrowingAnn.value();
            }
            return new AdviceImpl(method, pcExpr2, AdviceKind.AFTER_THROWING, afterThrowingAnn.throwing());
        }
        final Around aroundAnn = method.getAnnotation(Around.class);
        if (aroundAnn != null) {
            return new AdviceImpl(method, aroundAnn.value(), AdviceKind.AROUND);
        }
        return null;
    }
    
    public InterTypeMethodDeclaration getDeclaredITDMethod(final String name, final AjType<?> target, final AjType<?>... parameterTypes) throws NoSuchMethodException {
        final InterTypeMethodDeclaration[] arr$;
        final InterTypeMethodDeclaration[] itdms = arr$ = this.getDeclaredITDMethods();
        for (final InterTypeMethodDeclaration itdm : arr$) {
            Label_0127: {
                try {
                    if (itdm.getName().equals(name)) {
                        final AjType<?> itdTarget = itdm.getTargetType();
                        if (itdTarget.equals(target)) {
                            final AjType<?>[] ptypes = itdm.getParameterTypes();
                            if (ptypes.length == parameterTypes.length) {
                                for (int i = 0; i < ptypes.length; ++i) {
                                    if (!ptypes[i].equals(parameterTypes[i])) {
                                        break Label_0127;
                                    }
                                }
                                return itdm;
                            }
                        }
                    }
                }
                catch (ClassNotFoundException ex) {}
            }
        }
        throw new NoSuchMethodException(name);
    }
    
    public InterTypeMethodDeclaration[] getDeclaredITDMethods() {
        if (this.declaredITDMethods == null) {
            final List<InterTypeMethodDeclaration> itdms = new ArrayList<InterTypeMethodDeclaration>();
            final Method[] arr$;
            final Method[] baseMethods = arr$ = this.clazz.getDeclaredMethods();
            for (final Method m : arr$) {
                if (m.getName().contains("ajc$interMethodDispatch1$")) {
                    if (m.isAnnotationPresent(ajcITD.class)) {
                        final ajcITD ann = m.getAnnotation(ajcITD.class);
                        final InterTypeMethodDeclaration itdm = new InterTypeMethodDeclarationImpl(this, ann.targetType(), ann.modifiers(), ann.name(), m);
                        itdms.add(itdm);
                    }
                }
            }
            this.addAnnotationStyleITDMethods(itdms, false);
            itdms.toArray(this.declaredITDMethods = new InterTypeMethodDeclaration[itdms.size()]);
        }
        return this.declaredITDMethods;
    }
    
    public InterTypeMethodDeclaration getITDMethod(final String name, final AjType<?> target, final AjType<?>... parameterTypes) throws NoSuchMethodException {
        final InterTypeMethodDeclaration[] arr$;
        final InterTypeMethodDeclaration[] itdms = arr$ = this.getITDMethods();
        for (final InterTypeMethodDeclaration itdm : arr$) {
            Label_0127: {
                try {
                    if (itdm.getName().equals(name)) {
                        final AjType<?> itdTarget = itdm.getTargetType();
                        if (itdTarget.equals(target)) {
                            final AjType<?>[] ptypes = itdm.getParameterTypes();
                            if (ptypes.length == parameterTypes.length) {
                                for (int i = 0; i < ptypes.length; ++i) {
                                    if (!ptypes[i].equals(parameterTypes[i])) {
                                        break Label_0127;
                                    }
                                }
                                return itdm;
                            }
                        }
                    }
                }
                catch (ClassNotFoundException ex) {}
            }
        }
        throw new NoSuchMethodException(name);
    }
    
    public InterTypeMethodDeclaration[] getITDMethods() {
        if (this.itdMethods == null) {
            final List<InterTypeMethodDeclaration> itdms = new ArrayList<InterTypeMethodDeclaration>();
            final Method[] arr$;
            final Method[] baseMethods = arr$ = this.clazz.getDeclaredMethods();
            for (final Method m : arr$) {
                if (m.getName().contains("ajc$interMethod$")) {
                    if (m.isAnnotationPresent(ajcITD.class)) {
                        final ajcITD ann = m.getAnnotation(ajcITD.class);
                        if (Modifier.isPublic(ann.modifiers())) {
                            final InterTypeMethodDeclaration itdm = new InterTypeMethodDeclarationImpl(this, ann.targetType(), ann.modifiers(), ann.name(), m);
                            itdms.add(itdm);
                        }
                    }
                }
            }
            this.addAnnotationStyleITDMethods(itdms, true);
            itdms.toArray(this.itdMethods = new InterTypeMethodDeclaration[itdms.size()]);
        }
        return this.itdMethods;
    }
    
    private void addAnnotationStyleITDMethods(final List<InterTypeMethodDeclaration> toList, final boolean publicOnly) {
        if (this.isAspect()) {
            for (final Field f : this.clazz.getDeclaredFields()) {
                if (f.getType().isInterface()) {
                    if (f.isAnnotationPresent(DeclareParents.class)) {
                        final Class<DeclareParents> decPAnnClass = DeclareParents.class;
                        final DeclareParents decPAnn = f.getAnnotation(decPAnnClass);
                        if (decPAnn.defaultImpl() != decPAnnClass) {
                            for (final Method itdM : f.getType().getDeclaredMethods()) {
                                if (Modifier.isPublic(itdM.getModifiers()) || !publicOnly) {
                                    final InterTypeMethodDeclaration itdm = new InterTypeMethodDeclarationImpl(this, AjTypeSystem.getAjType(f.getType()), itdM, 1);
                                    toList.add(itdm);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void addAnnotationStyleITDFields(final List<InterTypeFieldDeclaration> toList, final boolean publicOnly) {
    }
    
    public InterTypeConstructorDeclaration getDeclaredITDConstructor(final AjType<?> target, final AjType<?>... parameterTypes) throws NoSuchMethodException {
        final InterTypeConstructorDeclaration[] arr$;
        final InterTypeConstructorDeclaration[] itdcs = arr$ = this.getDeclaredITDConstructors();
        for (final InterTypeConstructorDeclaration itdc : arr$) {
            Label_0108: {
                try {
                    final AjType<?> itdTarget = itdc.getTargetType();
                    if (itdTarget.equals(target)) {
                        final AjType<?>[] ptypes = itdc.getParameterTypes();
                        if (ptypes.length == parameterTypes.length) {
                            for (int i = 0; i < ptypes.length; ++i) {
                                if (!ptypes[i].equals(parameterTypes[i])) {
                                    break Label_0108;
                                }
                            }
                            return itdc;
                        }
                    }
                }
                catch (ClassNotFoundException ex) {}
            }
        }
        throw new NoSuchMethodException();
    }
    
    public InterTypeConstructorDeclaration[] getDeclaredITDConstructors() {
        if (this.declaredITDCons == null) {
            final List<InterTypeConstructorDeclaration> itdcs = new ArrayList<InterTypeConstructorDeclaration>();
            final Method[] arr$;
            final Method[] baseMethods = arr$ = this.clazz.getDeclaredMethods();
            for (final Method m : arr$) {
                if (m.getName().contains("ajc$postInterConstructor")) {
                    if (m.isAnnotationPresent(ajcITD.class)) {
                        final ajcITD ann = m.getAnnotation(ajcITD.class);
                        final InterTypeConstructorDeclaration itdc = new InterTypeConstructorDeclarationImpl(this, ann.targetType(), ann.modifiers(), m);
                        itdcs.add(itdc);
                    }
                }
            }
            itdcs.toArray(this.declaredITDCons = new InterTypeConstructorDeclaration[itdcs.size()]);
        }
        return this.declaredITDCons;
    }
    
    public InterTypeConstructorDeclaration getITDConstructor(final AjType<?> target, final AjType<?>... parameterTypes) throws NoSuchMethodException {
        final InterTypeConstructorDeclaration[] arr$;
        final InterTypeConstructorDeclaration[] itdcs = arr$ = this.getITDConstructors();
        for (final InterTypeConstructorDeclaration itdc : arr$) {
            Label_0108: {
                try {
                    final AjType<?> itdTarget = itdc.getTargetType();
                    if (itdTarget.equals(target)) {
                        final AjType<?>[] ptypes = itdc.getParameterTypes();
                        if (ptypes.length == parameterTypes.length) {
                            for (int i = 0; i < ptypes.length; ++i) {
                                if (!ptypes[i].equals(parameterTypes[i])) {
                                    break Label_0108;
                                }
                            }
                            return itdc;
                        }
                    }
                }
                catch (ClassNotFoundException ex) {}
            }
        }
        throw new NoSuchMethodException();
    }
    
    public InterTypeConstructorDeclaration[] getITDConstructors() {
        if (this.itdCons == null) {
            final List<InterTypeConstructorDeclaration> itdcs = new ArrayList<InterTypeConstructorDeclaration>();
            final Method[] arr$;
            final Method[] baseMethods = arr$ = this.clazz.getMethods();
            for (final Method m : arr$) {
                if (m.getName().contains("ajc$postInterConstructor")) {
                    if (m.isAnnotationPresent(ajcITD.class)) {
                        final ajcITD ann = m.getAnnotation(ajcITD.class);
                        if (Modifier.isPublic(ann.modifiers())) {
                            final InterTypeConstructorDeclaration itdc = new InterTypeConstructorDeclarationImpl(this, ann.targetType(), ann.modifiers(), m);
                            itdcs.add(itdc);
                        }
                    }
                }
            }
            itdcs.toArray(this.itdCons = new InterTypeConstructorDeclaration[itdcs.size()]);
        }
        return this.itdCons;
    }
    
    public InterTypeFieldDeclaration getDeclaredITDField(final String name, final AjType<?> target) throws NoSuchFieldException {
        final InterTypeFieldDeclaration[] arr$;
        final InterTypeFieldDeclaration[] itdfs = arr$ = this.getDeclaredITDFields();
        for (final InterTypeFieldDeclaration itdf : arr$) {
            if (itdf.getName().equals(name)) {
                try {
                    final AjType<?> itdTarget = itdf.getTargetType();
                    if (itdTarget.equals(target)) {
                        return itdf;
                    }
                }
                catch (ClassNotFoundException ex) {}
            }
        }
        throw new NoSuchFieldException(name);
    }
    
    public InterTypeFieldDeclaration[] getDeclaredITDFields() {
        final List<InterTypeFieldDeclaration> itdfs = new ArrayList<InterTypeFieldDeclaration>();
        if (this.declaredITDFields == null) {
            final Method[] arr$;
            final Method[] baseMethods = arr$ = this.clazz.getDeclaredMethods();
            for (final Method m : arr$) {
                if (m.isAnnotationPresent(ajcITD.class)) {
                    if (m.getName().contains("ajc$interFieldInit")) {
                        final ajcITD ann = m.getAnnotation(ajcITD.class);
                        final String interFieldInitMethodName = m.getName();
                        final String interFieldGetDispatchMethodName = interFieldInitMethodName.replace("FieldInit", "FieldGetDispatch");
                        try {
                            final Method dispatch = this.clazz.getDeclaredMethod(interFieldGetDispatchMethodName, m.getParameterTypes());
                            final InterTypeFieldDeclaration itdf = new InterTypeFieldDeclarationImpl(this, ann.targetType(), ann.modifiers(), ann.name(), AjTypeSystem.getAjType(dispatch.getReturnType()), dispatch.getGenericReturnType());
                            itdfs.add(itdf);
                        }
                        catch (NoSuchMethodException nsmEx) {
                            throw new IllegalStateException("Can't find field get dispatch method for " + m.getName());
                        }
                    }
                }
            }
            this.addAnnotationStyleITDFields(itdfs, false);
            itdfs.toArray(this.declaredITDFields = new InterTypeFieldDeclaration[itdfs.size()]);
        }
        return this.declaredITDFields;
    }
    
    public InterTypeFieldDeclaration getITDField(final String name, final AjType<?> target) throws NoSuchFieldException {
        final InterTypeFieldDeclaration[] arr$;
        final InterTypeFieldDeclaration[] itdfs = arr$ = this.getITDFields();
        for (final InterTypeFieldDeclaration itdf : arr$) {
            if (itdf.getName().equals(name)) {
                try {
                    final AjType<?> itdTarget = itdf.getTargetType();
                    if (itdTarget.equals(target)) {
                        return itdf;
                    }
                }
                catch (ClassNotFoundException ex) {}
            }
        }
        throw new NoSuchFieldException(name);
    }
    
    public InterTypeFieldDeclaration[] getITDFields() {
        final List<InterTypeFieldDeclaration> itdfs = new ArrayList<InterTypeFieldDeclaration>();
        if (this.itdFields == null) {
            final Method[] arr$;
            final Method[] baseMethods = arr$ = this.clazz.getMethods();
            for (final Method m : arr$) {
                if (m.isAnnotationPresent(ajcITD.class)) {
                    final ajcITD ann = m.getAnnotation(ajcITD.class);
                    if (m.getName().contains("ajc$interFieldInit")) {
                        if (Modifier.isPublic(ann.modifiers())) {
                            final String interFieldInitMethodName = m.getName();
                            final String interFieldGetDispatchMethodName = interFieldInitMethodName.replace("FieldInit", "FieldGetDispatch");
                            try {
                                final Method dispatch = m.getDeclaringClass().getDeclaredMethod(interFieldGetDispatchMethodName, m.getParameterTypes());
                                final InterTypeFieldDeclaration itdf = new InterTypeFieldDeclarationImpl(this, ann.targetType(), ann.modifiers(), ann.name(), AjTypeSystem.getAjType(dispatch.getReturnType()), dispatch.getGenericReturnType());
                                itdfs.add(itdf);
                            }
                            catch (NoSuchMethodException nsmEx) {
                                throw new IllegalStateException("Can't find field get dispatch method for " + m.getName());
                            }
                        }
                    }
                }
            }
            this.addAnnotationStyleITDFields(itdfs, true);
            itdfs.toArray(this.itdFields = new InterTypeFieldDeclaration[itdfs.size()]);
        }
        return this.itdFields;
    }
    
    public DeclareErrorOrWarning[] getDeclareErrorOrWarnings() {
        final List<DeclareErrorOrWarning> deows = new ArrayList<DeclareErrorOrWarning>();
        for (final Field field : this.clazz.getDeclaredFields()) {
            try {
                if (field.isAnnotationPresent(DeclareWarning.class)) {
                    final DeclareWarning dw = field.getAnnotation(DeclareWarning.class);
                    if (Modifier.isPublic(field.getModifiers()) && Modifier.isStatic(field.getModifiers())) {
                        final String message = (String)field.get(null);
                        final DeclareErrorOrWarningImpl deow = new DeclareErrorOrWarningImpl(dw.value(), message, false, this);
                        deows.add(deow);
                    }
                }
                else if (field.isAnnotationPresent(DeclareError.class)) {
                    final DeclareError de = field.getAnnotation(DeclareError.class);
                    if (Modifier.isPublic(field.getModifiers()) && Modifier.isStatic(field.getModifiers())) {
                        final String message = (String)field.get(null);
                        final DeclareErrorOrWarningImpl deow = new DeclareErrorOrWarningImpl(de.value(), message, true, this);
                        deows.add(deow);
                    }
                }
            }
            catch (IllegalArgumentException e) {}
            catch (IllegalAccessException ex) {}
        }
        for (final Method method : this.clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(ajcDeclareEoW.class)) {
                final ajcDeclareEoW deowAnn = method.getAnnotation(ajcDeclareEoW.class);
                final DeclareErrorOrWarning deow2 = new DeclareErrorOrWarningImpl(deowAnn.pointcut(), deowAnn.message(), deowAnn.isError(), this);
                deows.add(deow2);
            }
        }
        final DeclareErrorOrWarning[] ret = new DeclareErrorOrWarning[deows.size()];
        deows.toArray(ret);
        return ret;
    }
    
    public org.aspectj.lang.reflect.DeclareParents[] getDeclareParents() {
        final List<org.aspectj.lang.reflect.DeclareParents> decps = new ArrayList<org.aspectj.lang.reflect.DeclareParents>();
        for (final Method method : this.clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(ajcDeclareParents.class)) {
                final ajcDeclareParents decPAnn = method.getAnnotation(ajcDeclareParents.class);
                final DeclareParentsImpl decp = new DeclareParentsImpl(decPAnn.targetTypePattern(), decPAnn.parentTypes(), decPAnn.isExtends(), this);
                decps.add(decp);
            }
        }
        this.addAnnotationStyleDeclareParents(decps);
        if (this.getSupertype().isAspect()) {
            decps.addAll(Arrays.asList(this.getSupertype().getDeclareParents()));
        }
        final org.aspectj.lang.reflect.DeclareParents[] ret = new org.aspectj.lang.reflect.DeclareParents[decps.size()];
        decps.toArray(ret);
        return ret;
    }
    
    private void addAnnotationStyleDeclareParents(final List<org.aspectj.lang.reflect.DeclareParents> toList) {
        for (final Field f : this.clazz.getDeclaredFields()) {
            if (f.isAnnotationPresent(DeclareParents.class)) {
                if (f.getType().isInterface()) {
                    final DeclareParents ann = f.getAnnotation(DeclareParents.class);
                    final String parentType = f.getType().getName();
                    final DeclareParentsImpl decp = new DeclareParentsImpl(ann.value(), parentType, false, this);
                    toList.add(decp);
                }
            }
        }
    }
    
    public DeclareSoft[] getDeclareSofts() {
        final List<DeclareSoft> decs = new ArrayList<DeclareSoft>();
        for (final Method method : this.clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(ajcDeclareSoft.class)) {
                final ajcDeclareSoft decSAnn = method.getAnnotation(ajcDeclareSoft.class);
                final DeclareSoftImpl ds = new DeclareSoftImpl(this, decSAnn.pointcut(), decSAnn.exceptionType());
                decs.add(ds);
            }
        }
        if (this.getSupertype().isAspect()) {
            decs.addAll(Arrays.asList(this.getSupertype().getDeclareSofts()));
        }
        final DeclareSoft[] ret = new DeclareSoft[decs.size()];
        decs.toArray(ret);
        return ret;
    }
    
    public DeclareAnnotation[] getDeclareAnnotations() {
        final List<DeclareAnnotation> decAs = new ArrayList<DeclareAnnotation>();
        for (final Method method : this.clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(ajcDeclareAnnotation.class)) {
                final ajcDeclareAnnotation decAnn = method.getAnnotation(ajcDeclareAnnotation.class);
                Annotation targetAnnotation = null;
                final Annotation[] arr$2;
                final Annotation[] anns = arr$2 = method.getAnnotations();
                for (final Annotation ann : arr$2) {
                    if (ann.annotationType() != ajcDeclareAnnotation.class) {
                        targetAnnotation = ann;
                        break;
                    }
                }
                final DeclareAnnotationImpl da = new DeclareAnnotationImpl(this, decAnn.kind(), decAnn.pattern(), targetAnnotation, decAnn.annotation());
                decAs.add(da);
            }
        }
        if (this.getSupertype().isAspect()) {
            decAs.addAll(Arrays.asList(this.getSupertype().getDeclareAnnotations()));
        }
        final DeclareAnnotation[] ret = new DeclareAnnotation[decAs.size()];
        decAs.toArray(ret);
        return ret;
    }
    
    public DeclarePrecedence[] getDeclarePrecedence() {
        final List<DeclarePrecedence> decps = new ArrayList<DeclarePrecedence>();
        if (this.clazz.isAnnotationPresent(org.aspectj.lang.annotation.DeclarePrecedence.class)) {
            final org.aspectj.lang.annotation.DeclarePrecedence ann = this.clazz.getAnnotation(org.aspectj.lang.annotation.DeclarePrecedence.class);
            final DeclarePrecedenceImpl decp = new DeclarePrecedenceImpl(ann.value(), this);
            decps.add(decp);
        }
        for (final Method method : this.clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(ajcDeclarePrecedence.class)) {
                final ajcDeclarePrecedence decPAnn = method.getAnnotation(ajcDeclarePrecedence.class);
                final DeclarePrecedenceImpl decp2 = new DeclarePrecedenceImpl(decPAnn.value(), this);
                decps.add(decp2);
            }
        }
        if (this.getSupertype().isAspect()) {
            decps.addAll(Arrays.asList(this.getSupertype().getDeclarePrecedence()));
        }
        final DeclarePrecedence[] ret = new DeclarePrecedence[decps.size()];
        decps.toArray(ret);
        return ret;
    }
    
    public T[] getEnumConstants() {
        return this.clazz.getEnumConstants();
    }
    
    public TypeVariable<Class<T>>[] getTypeParameters() {
        return this.clazz.getTypeParameters();
    }
    
    public boolean isEnum() {
        return this.clazz.isEnum();
    }
    
    public boolean isInstance(final Object o) {
        return this.clazz.isInstance(o);
    }
    
    public boolean isInterface() {
        return this.clazz.isInterface();
    }
    
    public boolean isLocalClass() {
        return this.clazz.isLocalClass() && !this.isAspect();
    }
    
    public boolean isMemberClass() {
        return this.clazz.isMemberClass() && !this.isAspect();
    }
    
    public boolean isArray() {
        return this.clazz.isArray();
    }
    
    public boolean isPrimitive() {
        return this.clazz.isPrimitive();
    }
    
    public boolean isAspect() {
        return this.clazz.getAnnotation(Aspect.class) != null;
    }
    
    public boolean isMemberAspect() {
        return this.clazz.isMemberClass() && this.isAspect();
    }
    
    public boolean isPrivileged() {
        return this.isAspect() && this.clazz.isAnnotationPresent(ajcPrivileged.class);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof AjTypeImpl)) {
            return false;
        }
        final AjTypeImpl other = (AjTypeImpl)obj;
        return other.clazz.equals(this.clazz);
    }
    
    @Override
    public int hashCode() {
        return this.clazz.hashCode();
    }
    
    private AjType<?>[] toAjTypeArray(final Class<?>[] classes) {
        final AjType<?>[] ajtypes = (AjType<?>[])new AjType[classes.length];
        for (int i = 0; i < ajtypes.length; ++i) {
            ajtypes[i] = AjTypeSystem.getAjType(classes[i]);
        }
        return ajtypes;
    }
    
    private Class<?>[] toClassArray(final AjType<?>[] ajTypes) {
        final Class<?>[] classes = (Class<?>[])new Class[ajTypes.length];
        for (int i = 0; i < classes.length; ++i) {
            classes[i] = ajTypes[i].getJavaClass();
        }
        return classes;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
}
