// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang;

import java.lang.reflect.Modifier;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

public class Aspects
{
    private static final Class[] EMPTY_CLASS_ARRAY;
    private static final Class[] PEROBJECT_CLASS_ARRAY;
    private static final Class[] PERTYPEWITHIN_CLASS_ARRAY;
    private static final Object[] EMPTY_OBJECT_ARRAY;
    private static final String ASPECTOF = "aspectOf";
    private static final String HASASPECT = "hasAspect";
    
    public static <T> T aspectOf(final Class<T> aspectClass) throws NoAspectBoundException {
        try {
            return (T)getSingletonOrThreadAspectOf(aspectClass).invoke(null, Aspects.EMPTY_OBJECT_ARRAY);
        }
        catch (InvocationTargetException e) {
            throw new NoAspectBoundException(aspectClass.getName(), e);
        }
        catch (Exception e2) {
            throw new NoAspectBoundException(aspectClass.getName(), e2);
        }
    }
    
    public static <T> T aspectOf(final Class<T> aspectClass, final Object perObject) throws NoAspectBoundException {
        try {
            return (T)getPerObjectAspectOf(aspectClass).invoke(null, perObject);
        }
        catch (InvocationTargetException e) {
            throw new NoAspectBoundException(aspectClass.getName(), e);
        }
        catch (Exception e2) {
            throw new NoAspectBoundException(aspectClass.getName(), e2);
        }
    }
    
    public static <T> T aspectOf(final Class<T> aspectClass, final Class<?> perTypeWithin) throws NoAspectBoundException {
        try {
            return (T)getPerTypeWithinAspectOf(aspectClass).invoke(null, perTypeWithin);
        }
        catch (InvocationTargetException e) {
            throw new NoAspectBoundException(aspectClass.getName(), e);
        }
        catch (Exception e2) {
            throw new NoAspectBoundException(aspectClass.getName(), e2);
        }
    }
    
    public static boolean hasAspect(final Class<?> aspectClass) throws NoAspectBoundException {
        try {
            return (boolean)getSingletonOrThreadHasAspect(aspectClass).invoke(null, Aspects.EMPTY_OBJECT_ARRAY);
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public static boolean hasAspect(final Class<?> aspectClass, final Object perObject) throws NoAspectBoundException {
        try {
            return (boolean)getPerObjectHasAspect(aspectClass).invoke(null, perObject);
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public static boolean hasAspect(final Class<?> aspectClass, final Class<?> perTypeWithin) throws NoAspectBoundException {
        try {
            return (boolean)getPerTypeWithinHasAspect(aspectClass).invoke(null, perTypeWithin);
        }
        catch (Exception e) {
            return false;
        }
    }
    
    private static Method getSingletonOrThreadAspectOf(final Class<?> aspectClass) throws NoSuchMethodException {
        final Method method = aspectClass.getDeclaredMethod("aspectOf", (Class<?>[])Aspects.EMPTY_CLASS_ARRAY);
        return checkAspectOf(method, aspectClass);
    }
    
    private static Method getPerObjectAspectOf(final Class<?> aspectClass) throws NoSuchMethodException {
        final Method method = aspectClass.getDeclaredMethod("aspectOf", (Class<?>[])Aspects.PEROBJECT_CLASS_ARRAY);
        return checkAspectOf(method, aspectClass);
    }
    
    private static Method getPerTypeWithinAspectOf(final Class<?> aspectClass) throws NoSuchMethodException {
        final Method method = aspectClass.getDeclaredMethod("aspectOf", (Class<?>[])Aspects.PERTYPEWITHIN_CLASS_ARRAY);
        return checkAspectOf(method, aspectClass);
    }
    
    private static Method checkAspectOf(final Method method, final Class<?> aspectClass) throws NoSuchMethodException {
        method.setAccessible(true);
        if (!method.isAccessible() || !Modifier.isPublic(method.getModifiers()) || !Modifier.isStatic(method.getModifiers())) {
            throw new NoSuchMethodException(aspectClass.getName() + ".aspectOf(..) is not accessible public static");
        }
        return method;
    }
    
    private static Method getSingletonOrThreadHasAspect(final Class aspectClass) throws NoSuchMethodException {
        final Method method = aspectClass.getDeclaredMethod("hasAspect", (Class[])Aspects.EMPTY_CLASS_ARRAY);
        return checkHasAspect(method, aspectClass);
    }
    
    private static Method getPerObjectHasAspect(final Class aspectClass) throws NoSuchMethodException {
        final Method method = aspectClass.getDeclaredMethod("hasAspect", (Class[])Aspects.PEROBJECT_CLASS_ARRAY);
        return checkHasAspect(method, aspectClass);
    }
    
    private static Method getPerTypeWithinHasAspect(final Class aspectClass) throws NoSuchMethodException {
        final Method method = aspectClass.getDeclaredMethod("hasAspect", (Class[])Aspects.PERTYPEWITHIN_CLASS_ARRAY);
        return checkHasAspect(method, aspectClass);
    }
    
    private static Method checkHasAspect(final Method method, final Class aspectClass) throws NoSuchMethodException {
        method.setAccessible(true);
        if (!method.isAccessible() || !Modifier.isPublic(method.getModifiers()) || !Modifier.isStatic(method.getModifiers())) {
            throw new NoSuchMethodException(aspectClass.getName() + ".hasAspect(..) is not accessible public static");
        }
        return method;
    }
    
    static {
        EMPTY_CLASS_ARRAY = new Class[0];
        PEROBJECT_CLASS_ARRAY = new Class[] { Object.class };
        PERTYPEWITHIN_CLASS_ARRAY = new Class[] { Class.class };
        EMPTY_OBJECT_ARRAY = new Object[0];
    }
}
