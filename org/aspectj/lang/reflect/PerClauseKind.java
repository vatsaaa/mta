// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

public enum PerClauseKind
{
    SINGLETON, 
    PERTHIS, 
    PERTARGET, 
    PERCFLOW, 
    PERCFLOWBELOW, 
    PERTYPEWITHIN;
}
