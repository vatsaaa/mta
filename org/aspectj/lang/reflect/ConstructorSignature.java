// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Constructor;

public interface ConstructorSignature extends CodeSignature
{
    Constructor getConstructor();
}
