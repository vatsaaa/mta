// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.util.Collections;
import java.util.WeakHashMap;
import org.aspectj.internal.lang.reflect.AjTypeImpl;
import java.lang.ref.WeakReference;
import java.util.Map;

public class AjTypeSystem
{
    private static Map<Class, WeakReference<AjType>> ajTypes;
    
    public static <T> AjType<T> getAjType(final Class<T> fromClass) {
        final WeakReference<AjType> weakRefToAjType = AjTypeSystem.ajTypes.get(fromClass);
        if (weakRefToAjType == null) {
            final AjType<T> theAjType = new AjTypeImpl<T>(fromClass);
            AjTypeSystem.ajTypes.put(fromClass, new WeakReference<AjType>(theAjType));
            return theAjType;
        }
        AjType<T> theAjType = weakRefToAjType.get();
        if (theAjType != null) {
            return theAjType;
        }
        theAjType = new AjTypeImpl<T>(fromClass);
        AjTypeSystem.ajTypes.put(fromClass, new WeakReference<AjType>(theAjType));
        return theAjType;
    }
    
    static {
        AjTypeSystem.ajTypes = (Map<Class, WeakReference<AjType>>)Collections.synchronizedMap(new WeakHashMap<Class, WeakReference<AjType>>());
    }
}
