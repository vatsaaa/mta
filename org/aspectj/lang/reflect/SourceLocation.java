// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

public interface SourceLocation
{
    Class getWithinType();
    
    String getFileName();
    
    int getLine();
    
    int getColumn();
}
