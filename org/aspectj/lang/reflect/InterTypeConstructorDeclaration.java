// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Type;

public interface InterTypeConstructorDeclaration extends InterTypeDeclaration
{
    AjType<?>[] getParameterTypes();
    
    Type[] getGenericParameterTypes();
    
    AjType<?>[] getExceptionTypes();
}
