// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Type;

public interface Advice
{
    AjType getDeclaringType();
    
    AdviceKind getKind();
    
    String getName();
    
    AjType<?>[] getParameterTypes();
    
    Type[] getGenericParameterTypes();
    
    AjType<?>[] getExceptionTypes();
    
    PointcutExpression getPointcutExpression();
}
