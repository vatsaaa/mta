// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

public interface InterTypeDeclaration
{
    AjType<?> getDeclaringType();
    
    AjType<?> getTargetType() throws ClassNotFoundException;
    
    int getModifiers();
}
