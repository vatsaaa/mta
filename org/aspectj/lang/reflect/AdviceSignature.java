// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Method;

public interface AdviceSignature extends CodeSignature
{
    Class getReturnType();
    
    Method getAdvice();
}
