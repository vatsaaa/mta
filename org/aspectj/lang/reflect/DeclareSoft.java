// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

public interface DeclareSoft
{
    AjType getDeclaringType();
    
    AjType getSoftenedExceptionType() throws ClassNotFoundException;
    
    PointcutExpression getPointcutExpression();
}
