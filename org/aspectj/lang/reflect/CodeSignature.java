// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

public interface CodeSignature extends MemberSignature
{
    Class[] getParameterTypes();
    
    String[] getParameterNames();
    
    Class[] getExceptionTypes();
}
