// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Type;

public interface DeclareParents
{
    AjType getDeclaringType();
    
    TypePattern getTargetTypesPattern();
    
    boolean isExtends();
    
    boolean isImplements();
    
    Type[] getParentTypes() throws ClassNotFoundException;
}
