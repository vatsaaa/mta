// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Type;

public interface InterTypeFieldDeclaration extends InterTypeDeclaration
{
    String getName();
    
    AjType<?> getType();
    
    Type getGenericType();
}
