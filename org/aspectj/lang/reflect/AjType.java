// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.TypeVariable;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

public interface AjType<T> extends Type, AnnotatedElement
{
    String getName();
    
    Package getPackage();
    
    AjType<?>[] getInterfaces();
    
    int getModifiers();
    
    Class<T> getJavaClass();
    
    AjType<?> getSupertype();
    
    Type getGenericSupertype();
    
    Method getEnclosingMethod();
    
    Constructor getEnclosingConstructor();
    
    AjType<?> getEnclosingType();
    
    AjType<?> getDeclaringType();
    
    PerClause getPerClause();
    
    AjType<?>[] getAjTypes();
    
    AjType<?>[] getDeclaredAjTypes();
    
    Constructor getConstructor(final AjType<?>... p0) throws NoSuchMethodException;
    
    Constructor[] getConstructors();
    
    Constructor getDeclaredConstructor(final AjType<?>... p0) throws NoSuchMethodException;
    
    Constructor[] getDeclaredConstructors();
    
    Field getDeclaredField(final String p0) throws NoSuchFieldException;
    
    Field[] getDeclaredFields();
    
    Field getField(final String p0) throws NoSuchFieldException;
    
    Field[] getFields();
    
    Method getDeclaredMethod(final String p0, final AjType<?>... p1) throws NoSuchMethodException;
    
    Method getMethod(final String p0, final AjType<?>... p1) throws NoSuchMethodException;
    
    Method[] getDeclaredMethods();
    
    Method[] getMethods();
    
    Pointcut getDeclaredPointcut(final String p0) throws NoSuchPointcutException;
    
    Pointcut getPointcut(final String p0) throws NoSuchPointcutException;
    
    Pointcut[] getDeclaredPointcuts();
    
    Pointcut[] getPointcuts();
    
    Advice[] getDeclaredAdvice(final AdviceKind... p0);
    
    Advice[] getAdvice(final AdviceKind... p0);
    
    Advice getAdvice(final String p0) throws NoSuchAdviceException;
    
    Advice getDeclaredAdvice(final String p0) throws NoSuchAdviceException;
    
    InterTypeMethodDeclaration getDeclaredITDMethod(final String p0, final AjType<?> p1, final AjType<?>... p2) throws NoSuchMethodException;
    
    InterTypeMethodDeclaration[] getDeclaredITDMethods();
    
    InterTypeMethodDeclaration getITDMethod(final String p0, final AjType<?> p1, final AjType<?>... p2) throws NoSuchMethodException;
    
    InterTypeMethodDeclaration[] getITDMethods();
    
    InterTypeConstructorDeclaration getDeclaredITDConstructor(final AjType<?> p0, final AjType<?>... p1) throws NoSuchMethodException;
    
    InterTypeConstructorDeclaration[] getDeclaredITDConstructors();
    
    InterTypeConstructorDeclaration getITDConstructor(final AjType<?> p0, final AjType<?>... p1) throws NoSuchMethodException;
    
    InterTypeConstructorDeclaration[] getITDConstructors();
    
    InterTypeFieldDeclaration getDeclaredITDField(final String p0, final AjType<?> p1) throws NoSuchFieldException;
    
    InterTypeFieldDeclaration[] getDeclaredITDFields();
    
    InterTypeFieldDeclaration getITDField(final String p0, final AjType<?> p1) throws NoSuchFieldException;
    
    InterTypeFieldDeclaration[] getITDFields();
    
    DeclareErrorOrWarning[] getDeclareErrorOrWarnings();
    
    DeclareParents[] getDeclareParents();
    
    DeclareSoft[] getDeclareSofts();
    
    DeclareAnnotation[] getDeclareAnnotations();
    
    DeclarePrecedence[] getDeclarePrecedence();
    
    T[] getEnumConstants();
    
    TypeVariable<Class<T>>[] getTypeParameters();
    
    boolean isEnum();
    
    boolean isInstance(final Object p0);
    
    boolean isInterface();
    
    boolean isLocalClass();
    
    boolean isMemberClass();
    
    boolean isArray();
    
    boolean isPrimitive();
    
    boolean isAspect();
    
    boolean isMemberAspect();
    
    boolean isPrivileged();
}
