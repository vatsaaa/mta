// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.annotation.Annotation;

public interface DeclareAnnotation
{
    AjType<?> getDeclaringType();
    
    Kind getKind();
    
    SignaturePattern getSignaturePattern();
    
    TypePattern getTypePattern();
    
    Annotation getAnnotation();
    
    String getAnnotationAsText();
    
    public enum Kind
    {
        Field, 
        Method, 
        Constructor, 
        Type;
    }
}
