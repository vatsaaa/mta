// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import org.aspectj.lang.Signature;

public interface CatchClauseSignature extends Signature
{
    Class getParameterType();
    
    String getParameterName();
}
