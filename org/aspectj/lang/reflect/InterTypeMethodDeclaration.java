// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.Type;

public interface InterTypeMethodDeclaration extends InterTypeDeclaration
{
    String getName();
    
    AjType<?> getReturnType();
    
    Type getGenericReturnType();
    
    AjType<?>[] getParameterTypes();
    
    Type[] getGenericParameterTypes();
    
    TypeVariable<Method>[] getTypeParameters();
    
    AjType<?>[] getExceptionTypes();
}
