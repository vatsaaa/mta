// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

import java.lang.reflect.Field;

public interface FieldSignature extends MemberSignature
{
    Class getFieldType();
    
    Field getField();
}
