// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

public interface Pointcut
{
    String getName();
    
    int getModifiers();
    
    AjType<?>[] getParameterTypes();
    
    String[] getParameterNames();
    
    AjType getDeclaringType();
    
    PointcutExpression getPointcutExpression();
}
