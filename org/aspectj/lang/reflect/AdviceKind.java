// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang.reflect;

public enum AdviceKind
{
    BEFORE, 
    AFTER, 
    AFTER_RETURNING, 
    AFTER_THROWING, 
    AROUND;
}
