// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang;

import java.io.PrintWriter;
import java.io.PrintStream;

public class SoftException extends RuntimeException
{
    private static final boolean HAVE_JAVA_14;
    Throwable inner;
    
    public SoftException(final Throwable inner) {
        this.inner = inner;
    }
    
    public Throwable getWrappedThrowable() {
        return this.inner;
    }
    
    public Throwable getCause() {
        return this.inner;
    }
    
    public void printStackTrace() {
        this.printStackTrace(System.err);
    }
    
    public void printStackTrace(final PrintStream stream) {
        super.printStackTrace(stream);
        final Throwable _inner = this.inner;
        if (!SoftException.HAVE_JAVA_14 && null != _inner) {
            stream.print("Caused by: ");
            _inner.printStackTrace(stream);
        }
    }
    
    public void printStackTrace(final PrintWriter stream) {
        super.printStackTrace(stream);
        final Throwable _inner = this.inner;
        if (!SoftException.HAVE_JAVA_14 && null != _inner) {
            stream.print("Caused by: ");
            _inner.printStackTrace(stream);
        }
    }
    
    static {
        boolean java14 = false;
        try {
            Class.forName("java.nio.Buffer");
            java14 = true;
        }
        catch (Throwable t) {}
        HAVE_JAVA_14 = java14;
    }
}
