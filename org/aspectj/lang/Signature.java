// 
// Decompiled by Procyon v0.5.36
// 

package org.aspectj.lang;

public interface Signature
{
    String toString();
    
    String toShortString();
    
    String toLongString();
    
    String getName();
    
    int getModifiers();
    
    Class getDeclaringType();
    
    String getDeclaringTypeName();
}
