// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom;

import java.util.Iterator;
import org.jdom.filter.Filter;
import java.util.List;
import java.io.Serializable;

public interface Parent extends Cloneable, Serializable
{
    Object clone();
    
    List cloneContent();
    
    List getContent();
    
    Content getContent(final int p0);
    
    List getContent(final Filter p0);
    
    int getContentSize();
    
    Iterator getDescendants();
    
    Iterator getDescendants(final Filter p0);
    
    Document getDocument();
    
    Parent getParent();
    
    int indexOf(final Content p0);
    
    List removeContent();
    
    Content removeContent(final int p0);
    
    boolean removeContent(final Content p0);
    
    List removeContent(final Filter p0);
}
