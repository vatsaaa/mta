// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.transform;

import org.jdom.JDOMException;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotSupportedException;
import org.jdom.output.SAXOutputter;
import java.io.StringReader;
import org.jdom.output.XMLOutputter;
import java.io.Reader;
import org.xml.sax.XMLFilter;
import org.xml.sax.InputSource;
import java.util.ArrayList;
import org.jdom.Element;
import org.jdom.Document;
import java.util.List;
import org.xml.sax.XMLReader;
import javax.xml.transform.sax.SAXSource;

public class JDOMSource extends SAXSource
{
    private static final String CVS_ID = "@(#) $RCSfile: JDOMSource.java,v $ $Revision: 1.18 $ $Date: 2004/08/31 04:43:48 $ $Name: jdom_1_0 $";
    public static final String JDOM_FEATURE = "http://org.jdom.transform.JDOMSource/feature";
    private XMLReader xmlReader;
    
    public JDOMSource(final List source) {
        this.xmlReader = null;
        this.setNodes(source);
    }
    
    public JDOMSource(final Document source) {
        this.xmlReader = null;
        this.setDocument(source);
    }
    
    public JDOMSource(final Element source) {
        this.xmlReader = null;
        final List nodes = new ArrayList();
        nodes.add(source);
        this.setNodes(nodes);
    }
    
    public Document getDocument() {
        final Object src = ((JDOMInputSource)this.getInputSource()).getSource();
        Document doc = null;
        if (src instanceof Document) {
            doc = (Document)src;
        }
        return doc;
    }
    
    public List getNodes() {
        final Object src = ((JDOMInputSource)this.getInputSource()).getSource();
        List nodes = null;
        if (src instanceof List) {
            nodes = (List)src;
        }
        return nodes;
    }
    
    public XMLReader getXMLReader() {
        if (this.xmlReader == null) {
            this.xmlReader = new DocumentReader();
        }
        return this.xmlReader;
    }
    
    public void setDocument(final Document source) {
        super.setInputSource(new JDOMInputSource(source));
    }
    
    public void setInputSource(final InputSource inputSource) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
    
    public void setNodes(final List source) {
        super.setInputSource(new JDOMInputSource(source));
    }
    
    public void setXMLReader(final XMLReader reader) throws UnsupportedOperationException {
        if (reader instanceof XMLFilter) {
            XMLFilter filter;
            for (filter = (XMLFilter)reader; filter.getParent() instanceof XMLFilter; filter = (XMLFilter)filter.getParent()) {}
            filter.setParent(new DocumentReader());
            this.xmlReader = reader;
            return;
        }
        throw new UnsupportedOperationException();
    }
    
    private static class JDOMInputSource extends InputSource
    {
        private Object source;
        
        public JDOMInputSource(final List nodes) {
            this.source = null;
            this.source = nodes;
        }
        
        public JDOMInputSource(final Document document) {
            this.source = null;
            this.source = document;
        }
        
        public Reader getCharacterStream() {
            final Object src = this.getSource();
            Reader reader = null;
            if (src instanceof Document) {
                reader = new StringReader(new XMLOutputter().outputString((Document)src));
            }
            else if (src instanceof List) {
                reader = new StringReader(new XMLOutputter().outputString((List)src));
            }
            return reader;
        }
        
        public Object getSource() {
            return this.source;
        }
        
        public void setCharacterStream(final Reader characterStream) throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }
    }
    
    private static class DocumentReader extends SAXOutputter implements XMLReader
    {
        public DocumentReader() {
        }
        
        public void parse(final String systemId) throws SAXNotSupportedException {
            throw new SAXNotSupportedException("Only JDOM Documents are supported as input");
        }
        
        public void parse(final InputSource input) throws SAXException {
            if (input instanceof JDOMInputSource) {
                try {
                    final Object source = ((JDOMInputSource)input).getSource();
                    if (source instanceof Document) {
                        this.output((Document)source);
                        return;
                    }
                    this.output((List)source);
                    return;
                }
                catch (JDOMException e) {
                    throw new SAXException(e.getMessage(), e);
                }
                throw new SAXNotSupportedException("Only JDOM Documents are supported as input");
            }
            throw new SAXNotSupportedException("Only JDOM Documents are supported as input");
        }
    }
}
