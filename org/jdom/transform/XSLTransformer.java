// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.transform;

import javax.xml.transform.Result;
import java.util.List;
import org.jdom.Document;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import java.io.Reader;
import java.io.InputStream;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import javax.xml.transform.Templates;

public class XSLTransformer
{
    private static final String CVS_ID = "@(#) $RCSfile: XSLTransformer.java,v $ $Revision: 1.2 $ $Date: 2004/02/06 09:28:32 $ $Name: jdom_1_0 $";
    private Templates templates;
    
    public XSLTransformer(final File stylesheet) throws XSLTransformException {
        this(new StreamSource(stylesheet));
    }
    
    public XSLTransformer(final InputStream stylesheet) throws XSLTransformException {
        this(new StreamSource(stylesheet));
    }
    
    public XSLTransformer(final Reader stylesheet) throws XSLTransformException {
        this(new StreamSource(stylesheet));
    }
    
    public XSLTransformer(final String stylesheetSystemId) throws XSLTransformException {
        this(new StreamSource(stylesheetSystemId));
    }
    
    private XSLTransformer(final Source stylesheet) throws XSLTransformException {
        try {
            this.templates = TransformerFactory.newInstance().newTemplates(stylesheet);
        }
        catch (TransformerException e) {
            throw new XSLTransformException("Could not construct XSLTransformer", e);
        }
    }
    
    public XSLTransformer(final Document stylesheet) throws XSLTransformException {
        this(new JDOMSource(stylesheet));
    }
    
    public List transform(final List inputNodes) throws XSLTransformException {
        final JDOMSource source = new JDOMSource(inputNodes);
        final JDOMResult result = new JDOMResult();
        try {
            this.templates.newTransformer().transform(source, result);
            return result.getResult();
        }
        catch (TransformerException e) {
            throw new XSLTransformException("Could not perform transformation", e);
        }
    }
    
    public Document transform(final Document inputDoc) throws XSLTransformException {
        final JDOMSource source = new JDOMSource(inputDoc);
        final JDOMResult result = new JDOMResult();
        try {
            this.templates.newTransformer().transform(source, result);
            return result.getDocument();
        }
        catch (TransformerException e) {
            throw new XSLTransformException("Could not perform transformation", e);
        }
    }
}
