// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.input;

import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import javax.xml.parsers.SAXParser;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import org.jdom.JDOMException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.XMLReader;
import java.util.Map;

class JAXPParserFactory
{
    private static final String CVS_ID = "@(#) $RCSfile: JAXPParserFactory.java,v $ $Revision: 1.5 $ $Date: 2004/02/27 21:08:47 $ $Name: jdom_1_0 $";
    private static final String JAXP_SCHEMA_LANGUAGE_PROPERTY = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    private static final String JAXP_SCHEMA_LOCATION_PROPERTY = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    
    private JAXPParserFactory() {
    }
    
    public static XMLReader createParser(final boolean validating, final Map features, final Map properties) throws JDOMException {
        try {
            SAXParser parser = null;
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(validating);
            factory.setNamespaceAware(true);
            try {
                parser = factory.newSAXParser();
            }
            catch (ParserConfigurationException e) {
                throw new JDOMException("Could not allocate JAXP SAX Parser", e);
            }
            setProperty(parser, properties, "http://java.sun.com/xml/jaxp/properties/schemaLanguage");
            setProperty(parser, properties, "http://java.sun.com/xml/jaxp/properties/schemaSource");
            return parser.getXMLReader();
        }
        catch (SAXException e2) {
            throw new JDOMException("Could not allocate JAXP SAX Parser", e2);
        }
    }
    
    private static void setProperty(final SAXParser parser, final Map properties, final String name) throws JDOMException {
        try {
            if (properties.containsKey(name)) {
                parser.setProperty(name, properties.get(name));
            }
        }
        catch (SAXNotSupportedException ex) {
            throw new JDOMException(String.valueOf(name) + " property not supported for JAXP parser " + parser.getClass().getName());
        }
        catch (SAXNotRecognizedException ex2) {
            throw new JDOMException(String.valueOf(name) + " property not recognized for JAXP parser " + parser.getClass().getName());
        }
    }
}
