// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.filter;

import java.io.Serializable;

public interface Filter extends Serializable
{
    boolean matches(final Object p0);
}
