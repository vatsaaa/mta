// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.xpath;

import org.jaxen.BaseXPath;
import org.jdom.Document;
import org.jdom.Content;
import org.jdom.Attribute;
import org.jdom.Element;
import org.jaxen.SimpleNamespaceContext;
import org.jaxen.NamespaceContext;
import org.jaxen.SimpleVariableContext;
import java.util.List;
import org.jaxen.JaxenException;
import org.jdom.Namespace;
import org.jdom.JDOMException;
import org.jaxen.jdom.JDOMXPath;

class JaxenXPath extends XPath
{
    private static final String CVS_ID = "@(#) $RCSfile: JaxenXPath.java,v $ $Revision: 1.19 $ $Date: 2004/09/03 07:27:39 $ $Name: jdom_1_0 $";
    private transient JDOMXPath xPath;
    private Object currentContext;
    
    public JaxenXPath(final String expr) throws JDOMException {
        this.setXPath(expr);
    }
    
    public void addNamespace(final Namespace namespace) {
        try {
            ((BaseXPath)this.xPath).addNamespace(namespace.getPrefix(), namespace.getURI());
        }
        catch (JaxenException ex) {}
    }
    
    public boolean equals(final Object o) {
        if (o instanceof JaxenXPath) {
            final JaxenXPath x = (JaxenXPath)o;
            return super.equals(o) && ((BaseXPath)this.xPath).toString().equals(((BaseXPath)x.xPath).toString());
        }
        return false;
    }
    
    public String getXPath() {
        return ((BaseXPath)this.xPath).toString();
    }
    
    public int hashCode() {
        return this.xPath.hashCode();
    }
    
    public Number numberValueOf(final Object context) throws JDOMException {
        try {
            this.currentContext = context;
            return ((BaseXPath)this.xPath).numberValueOf(context);
        }
        catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + ((BaseXPath)this.xPath).toString() + "\": " + ((Throwable)ex1).getMessage(), (Throwable)ex1);
        }
        finally {
            this.currentContext = null;
        }
    }
    
    public List selectNodes(final Object context) throws JDOMException {
        try {
            this.currentContext = context;
            return ((BaseXPath)this.xPath).selectNodes(context);
        }
        catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + ((BaseXPath)this.xPath).toString() + "\": " + ((Throwable)ex1).getMessage(), (Throwable)ex1);
        }
        finally {
            this.currentContext = null;
        }
    }
    
    public Object selectSingleNode(final Object context) throws JDOMException {
        try {
            this.currentContext = context;
            return ((BaseXPath)this.xPath).selectSingleNode(context);
        }
        catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + ((BaseXPath)this.xPath).toString() + "\": " + ((Throwable)ex1).getMessage(), (Throwable)ex1);
        }
        finally {
            this.currentContext = null;
        }
    }
    
    public void setVariable(final String name, final Object value) throws IllegalArgumentException {
        final Object o = ((BaseXPath)this.xPath).getVariableContext();
        if (o instanceof SimpleVariableContext) {
            ((SimpleVariableContext)o).setVariableValue((String)null, name, value);
        }
    }
    
    private void setXPath(final String expr) throws JDOMException {
        try {
            ((BaseXPath)(this.xPath = new JDOMXPath(expr))).setNamespaceContext((NamespaceContext)new NSContext());
        }
        catch (Exception ex1) {
            throw new JDOMException("Invalid XPath expression: \"" + expr + "\"", ex1);
        }
    }
    
    public String toString() {
        return ((BaseXPath)this.xPath).toString();
    }
    
    public String valueOf(final Object context) throws JDOMException {
        try {
            this.currentContext = context;
            return ((BaseXPath)this.xPath).stringValueOf(context);
        }
        catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + ((BaseXPath)this.xPath).toString() + "\": " + ((Throwable)ex1).getMessage(), (Throwable)ex1);
        }
        finally {
            this.currentContext = null;
        }
    }
    
    private class NSContext extends SimpleNamespaceContext
    {
        public NSContext() {
        }
        
        public String translateNamespacePrefixToUri(final String prefix) {
            if (prefix == null || prefix.length() == 0) {
                return null;
            }
            String uri = super.translateNamespacePrefixToUri(prefix);
            if (uri == null) {
                final Object ctx = JaxenXPath.this.currentContext;
                if (ctx != null) {
                    Element elt = null;
                    if (ctx instanceof Element) {
                        elt = (Element)ctx;
                    }
                    else if (ctx instanceof Attribute) {
                        elt = ((Attribute)ctx).getParent();
                    }
                    else if (ctx instanceof Content) {
                        elt = ((Content)ctx).getParentElement();
                    }
                    else if (ctx instanceof Document) {
                        elt = ((Document)ctx).getRootElement();
                    }
                    if (elt != null) {
                        final Namespace ns = elt.getNamespace(prefix);
                        if (ns != null) {
                            uri = ns.getURI();
                        }
                    }
                }
            }
            return uri;
        }
    }
}
