// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom;

import java.util.Map;

public class DefaultJDOMFactory implements JDOMFactory
{
    private static final String CVS_ID = "@(#) $RCSfile: DefaultJDOMFactory.java,v $ $Revision: 1.6 $ $Date: 2004/09/01 05:25:38 $ $Name: jdom_1_0 $";
    
    public void addContent(final Parent parent, final Content child) {
        if (parent instanceof Document) {
            ((Document)parent).addContent(child);
        }
        else {
            ((Element)parent).addContent(child);
        }
    }
    
    public void addNamespaceDeclaration(final Element parent, final Namespace additional) {
        parent.addNamespaceDeclaration(additional);
    }
    
    public Attribute attribute(final String name, final String value) {
        return new Attribute(name, value);
    }
    
    public Attribute attribute(final String name, final String value, final int type) {
        return new Attribute(name, value, type);
    }
    
    public Attribute attribute(final String name, final String value, final int type, final Namespace namespace) {
        return new Attribute(name, value, type, namespace);
    }
    
    public Attribute attribute(final String name, final String value, final Namespace namespace) {
        return new Attribute(name, value, namespace);
    }
    
    public CDATA cdata(final String text) {
        return new CDATA(text);
    }
    
    public Comment comment(final String text) {
        return new Comment(text);
    }
    
    public DocType docType(final String elementName) {
        return new DocType(elementName);
    }
    
    public DocType docType(final String elementName, final String systemID) {
        return new DocType(elementName, systemID);
    }
    
    public DocType docType(final String elementName, final String publicID, final String systemID) {
        return new DocType(elementName, publicID, systemID);
    }
    
    public Document document(final Element rootElement) {
        return new Document(rootElement);
    }
    
    public Document document(final Element rootElement, final DocType docType) {
        return new Document(rootElement, docType);
    }
    
    public Document document(final Element rootElement, final DocType docType, final String baseURI) {
        return new Document(rootElement, docType, baseURI);
    }
    
    public Element element(final String name) {
        return new Element(name);
    }
    
    public Element element(final String name, final String uri) {
        return new Element(name, uri);
    }
    
    public Element element(final String name, final String prefix, final String uri) {
        return new Element(name, prefix, uri);
    }
    
    public Element element(final String name, final Namespace namespace) {
        return new Element(name, namespace);
    }
    
    public EntityRef entityRef(final String name) {
        return new EntityRef(name);
    }
    
    public EntityRef entityRef(final String name, final String systemID) {
        return new EntityRef(name, systemID);
    }
    
    public EntityRef entityRef(final String name, final String publicID, final String systemID) {
        return new EntityRef(name, publicID, systemID);
    }
    
    public ProcessingInstruction processingInstruction(final String target, final String data) {
        return new ProcessingInstruction(target, data);
    }
    
    public ProcessingInstruction processingInstruction(final String target, final Map data) {
        return new ProcessingInstruction(target, data);
    }
    
    public void setAttribute(final Element parent, final Attribute a) {
        parent.setAttribute(a);
    }
    
    public Text text(final String text) {
        return new Text(text);
    }
}
