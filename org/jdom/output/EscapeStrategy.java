// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.output;

public interface EscapeStrategy
{
    boolean shouldEscape(final char p0);
}
