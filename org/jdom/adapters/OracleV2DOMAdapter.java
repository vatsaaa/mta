// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.adapters;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.io.IOException;
import org.xml.sax.SAXParseException;
import org.xml.sax.InputSource;
import java.io.InputStream;
import org.jdom.JDOMException;
import org.w3c.dom.Document;

public class OracleV2DOMAdapter extends AbstractDOMAdapter
{
    private static final String CVS_ID = "@(#) $RCSfile: OracleV2DOMAdapter.java,v $ $Revision: 1.18 $ $Date: 2004/02/06 09:28:31 $ $Name: jdom_1_0 $";
    static /* synthetic */ Class class$org$xml$sax$InputSource;
    
    static /* synthetic */ Class class$(final String class$) {
        try {
            return Class.forName(class$);
        }
        catch (ClassNotFoundException forName) {
            throw new NoClassDefFoundError(forName.getMessage());
        }
    }
    
    public Document createDocument() throws JDOMException {
        try {
            return (Document)Class.forName("oracle.xml.parser.v2.XMLDocument").newInstance();
        }
        catch (Exception e) {
            throw new JDOMException(String.valueOf(e.getClass().getName()) + ": " + e.getMessage() + " when creating document", e);
        }
    }
    
    public Document getDocument(final InputStream in, final boolean validate) throws IOException, JDOMException {
        try {
            final Class parserClass = Class.forName("oracle.xml.parser.v2.DOMParser");
            final Object parser = parserClass.newInstance();
            final Method parse = parserClass.getMethod("parse", (OracleV2DOMAdapter.class$org$xml$sax$InputSource != null) ? OracleV2DOMAdapter.class$org$xml$sax$InputSource : (OracleV2DOMAdapter.class$org$xml$sax$InputSource = class$("org.xml.sax.InputSource")));
            parse.invoke(parser, new InputSource(in));
            final Method getDocument = parserClass.getMethod("getDocument", (Class[])null);
            final Document doc = (Document)getDocument.invoke(parser, (Object[])null);
            return doc;
        }
        catch (InvocationTargetException e) {
            final Throwable targetException = e.getTargetException();
            if (targetException instanceof SAXParseException) {
                final SAXParseException parseException = (SAXParseException)targetException;
                throw new JDOMException("Error on line " + parseException.getLineNumber() + " of XML document: " + parseException.getMessage(), parseException);
            }
            if (targetException instanceof IOException) {
                final IOException ioException = (IOException)targetException;
                throw ioException;
            }
            throw new JDOMException(targetException.getMessage(), targetException);
        }
        catch (Exception e2) {
            throw new JDOMException(String.valueOf(e2.getClass().getName()) + ": " + e2.getMessage(), e2);
        }
    }
}
