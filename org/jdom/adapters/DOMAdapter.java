// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.adapters;

import java.io.InputStream;
import java.io.IOException;
import java.io.File;
import org.jdom.DocType;
import org.jdom.JDOMException;
import org.w3c.dom.Document;

public interface DOMAdapter
{
    Document createDocument() throws JDOMException;
    
    Document createDocument(final DocType p0) throws JDOMException;
    
    Document getDocument(final File p0, final boolean p1) throws IOException, JDOMException;
    
    Document getDocument(final InputStream p0, final boolean p1) throws IOException, JDOMException;
}
