// 
// Decompiled by Procyon v0.5.36
// 

package org.jdom.adapters;

import java.lang.reflect.Method;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import org.w3c.dom.DocumentType;
import org.w3c.dom.DOMImplementation;
import org.jdom.DocType;
import org.jdom.JDOMException;
import org.w3c.dom.Document;

public abstract class AbstractDOMAdapter implements DOMAdapter
{
    private static final String CVS_ID = "@(#) $RCSfile: AbstractDOMAdapter.java,v $ $Revision: 1.20 $ $Date: 2004/02/06 09:28:31 $ $Name: jdom_1_0 $";
    static /* synthetic */ Class class$java$lang$String;
    
    static /* synthetic */ Class class$(final String class$) {
        try {
            return Class.forName(class$);
        }
        catch (ClassNotFoundException forName) {
            throw new NoClassDefFoundError(forName.getMessage());
        }
    }
    
    public abstract Document createDocument() throws JDOMException;
    
    public Document createDocument(final DocType doctype) throws JDOMException {
        if (doctype == null) {
            return this.createDocument();
        }
        final DOMImplementation domImpl = this.createDocument().getImplementation();
        final DocumentType domDocType = domImpl.createDocumentType(doctype.getElementName(), doctype.getPublicID(), doctype.getSystemID());
        this.setInternalSubset(domDocType, doctype.getInternalSubset());
        return domImpl.createDocument("http://temporary", doctype.getElementName(), domDocType);
    }
    
    public Document getDocument(final File filename, final boolean validate) throws IOException, JDOMException {
        return this.getDocument(new FileInputStream(filename), validate);
    }
    
    public abstract Document getDocument(final InputStream p0, final boolean p1) throws IOException, JDOMException;
    
    protected void setInternalSubset(final DocumentType dt, final String s) {
        if (dt == null || s == null) {
            return;
        }
        try {
            final Class dtclass = dt.getClass();
            final Method setInternalSubset = dtclass.getMethod("setInternalSubset", (AbstractDOMAdapter.class$java$lang$String != null) ? AbstractDOMAdapter.class$java$lang$String : (AbstractDOMAdapter.class$java$lang$String = class$("java.lang.String")));
            setInternalSubset.invoke(dt, s);
        }
        catch (Exception ex) {}
    }
}
