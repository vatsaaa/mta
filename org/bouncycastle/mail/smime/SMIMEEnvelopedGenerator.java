// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime;

import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.AlgorithmParameters;
import org.bouncycastle.cms.CMSEnvelopedDataStreamGenerator;
import java.io.IOException;
import org.bouncycastle.cms.CMSException;
import java.io.OutputStream;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import java.security.Provider;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import javax.crypto.SecretKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import javax.activation.MailcapCommandMap;
import javax.activation.CommandMap;

public class SMIMEEnvelopedGenerator extends SMIMEGenerator
{
    public static final String DES_EDE3_CBC;
    public static final String RC2_CBC;
    public static final String IDEA_CBC = "1.3.6.1.4.1.188.7.1.1.2";
    public static final String CAST5_CBC = "1.2.840.113533.7.66.10";
    public static final String AES128_CBC;
    public static final String AES192_CBC;
    public static final String AES256_CBC;
    public static final String CAMELLIA128_CBC;
    public static final String CAMELLIA192_CBC;
    public static final String CAMELLIA256_CBC;
    public static final String SEED_CBC;
    public static final String DES_EDE3_WRAP;
    public static final String AES128_WRAP;
    public static final String AES256_WRAP;
    public static final String CAMELLIA128_WRAP;
    public static final String CAMELLIA192_WRAP;
    public static final String CAMELLIA256_WRAP;
    public static final String SEED_WRAP;
    public static final String ECDH_SHA1KDF;
    private static final String ENCRYPTED_CONTENT_TYPE = "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data";
    private EnvelopedGenerator fact;
    
    private static MailcapCommandMap addCommands(final CommandMap commandMap) {
        final MailcapCommandMap mailcapCommandMap = (MailcapCommandMap)commandMap;
        mailcapCommandMap.addMailcap("application/pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_signature");
        mailcapCommandMap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        mailcapCommandMap.addMailcap("application/x-pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_signature");
        mailcapCommandMap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        mailcapCommandMap.addMailcap("multipart/signed;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.multipart_signed");
        return mailcapCommandMap;
    }
    
    public SMIMEEnvelopedGenerator() {
        this.fact = new EnvelopedGenerator();
    }
    
    public void addKeyTransRecipient(final X509Certificate x509Certificate) throws IllegalArgumentException {
        this.fact.addKeyTransRecipient(x509Certificate);
    }
    
    public void addKeyTransRecipient(final PublicKey publicKey, final byte[] array) throws IllegalArgumentException {
        this.fact.addKeyTransRecipient(publicKey, array);
    }
    
    public void addKEKRecipient(final SecretKey secretKey, final byte[] array) throws IllegalArgumentException {
        this.fact.addKEKRecipient(secretKey, array);
    }
    
    public void addKeyAgreementRecipient(final String s, final PrivateKey privateKey, final PublicKey publicKey, final X509Certificate x509Certificate, final String s2, final String s3) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        this.fact.addKeyAgreementRecipient(s, privateKey, publicKey, x509Certificate, s2, s3);
    }
    
    public void addKeyAgreementRecipient(final String s, final PrivateKey privateKey, final PublicKey publicKey, final X509Certificate x509Certificate, final String s2, final Provider provider) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        this.fact.addKeyAgreementRecipient(s, privateKey, publicKey, x509Certificate, s2, provider);
    }
    
    public void setBerEncodeRecipients(final boolean berEncodeRecipients) {
        this.fact.setBEREncodeRecipients(berEncodeRecipients);
    }
    
    private MimeBodyPart make(final MimeBodyPart mimeBodyPart, final String s, final int n, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        this.createSymmetricKeyGenerator(s, provider);
        try {
            final MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent((Object)new ContentEncryptor(mimeBodyPart, s, n, provider), "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data");
            mimeBodyPart2.addHeader("Content-Type", "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data");
            mimeBodyPart2.addHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
            mimeBodyPart2.addHeader("Content-Description", "S/MIME Encrypted Message");
            mimeBodyPart2.addHeader("Content-Transfer-Encoding", this.encoding);
            return mimeBodyPart2;
        }
        catch (MessagingException ex) {
            throw new SMIMEException("exception putting multi-part together.", (Exception)ex);
        }
    }
    
    public MimeBodyPart generate(final MimeBodyPart mimeBodyPart, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.make(this.makeContentBodyPart(mimeBodyPart), s, 0, SMIMEUtil.getProvider(s2));
    }
    
    public MimeBodyPart generate(final MimeBodyPart mimeBodyPart, final String s, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        return this.make(this.makeContentBodyPart(mimeBodyPart), s, 0, provider);
    }
    
    public MimeBodyPart generate(final MimeMessage mimeMessage, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.generate(mimeMessage, s, SMIMEUtil.getProvider(s2));
    }
    
    public MimeBodyPart generate(final MimeMessage mimeMessage, final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        try {
            mimeMessage.saveChanges();
        }
        catch (MessagingException ex) {
            throw new SMIMEException("unable to save message", (Exception)ex);
        }
        return this.make(this.makeContentBodyPart(mimeMessage), s, 0, provider);
    }
    
    public MimeBodyPart generate(final MimeBodyPart mimeBodyPart, final String s, final int n, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.generate(mimeBodyPart, s, n, SMIMEUtil.getProvider(s2));
    }
    
    public MimeBodyPart generate(final MimeBodyPart mimeBodyPart, final String s, final int n, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.make(this.makeContentBodyPart(mimeBodyPart), s, n, provider);
    }
    
    public MimeBodyPart generate(final MimeMessage mimeMessage, final String s, final int n, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.generate(mimeMessage, s, n, SMIMEUtil.getProvider(s2));
    }
    
    public MimeBodyPart generate(final MimeMessage mimeMessage, final String s, final int n, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        try {
            mimeMessage.saveChanges();
        }
        catch (MessagingException ex) {
            throw new SMIMEException("unable to save message", (Exception)ex);
        }
        return this.make(this.makeContentBodyPart(mimeMessage), s, n, provider);
    }
    
    static {
        DES_EDE3_CBC = CMSEnvelopedDataGenerator.DES_EDE3_CBC;
        RC2_CBC = CMSEnvelopedDataGenerator.RC2_CBC;
        AES128_CBC = CMSEnvelopedDataGenerator.AES128_CBC;
        AES192_CBC = CMSEnvelopedDataGenerator.AES192_CBC;
        AES256_CBC = CMSEnvelopedDataGenerator.AES256_CBC;
        CAMELLIA128_CBC = CMSEnvelopedDataGenerator.CAMELLIA128_CBC;
        CAMELLIA192_CBC = CMSEnvelopedDataGenerator.CAMELLIA192_CBC;
        CAMELLIA256_CBC = CMSEnvelopedDataGenerator.CAMELLIA256_CBC;
        SEED_CBC = CMSEnvelopedDataGenerator.SEED_CBC;
        DES_EDE3_WRAP = CMSEnvelopedDataGenerator.DES_EDE3_WRAP;
        AES128_WRAP = CMSEnvelopedDataGenerator.AES128_WRAP;
        AES256_WRAP = CMSEnvelopedDataGenerator.AES256_WRAP;
        CAMELLIA128_WRAP = CMSEnvelopedDataGenerator.CAMELLIA128_WRAP;
        CAMELLIA192_WRAP = CMSEnvelopedDataGenerator.CAMELLIA192_WRAP;
        CAMELLIA256_WRAP = CMSEnvelopedDataGenerator.CAMELLIA256_WRAP;
        SEED_WRAP = CMSEnvelopedDataGenerator.SEED_WRAP;
        ECDH_SHA1KDF = CMSEnvelopedDataGenerator.ECDH_SHA1KDF;
        CommandMap.setDefaultCommandMap((CommandMap)addCommands(CommandMap.getDefaultCommandMap()));
    }
    
    private class ContentEncryptor implements SMIMEStreamingProcessor
    {
        private final MimeBodyPart _content;
        private final String _encryptionOid;
        private final int _keySize;
        private final Provider _provider;
        private boolean _firstTime;
        
        ContentEncryptor(final MimeBodyPart content, final String encryptionOid, final int keySize, final Provider provider) {
            this._firstTime = true;
            this._content = content;
            this._encryptionOid = encryptionOid;
            this._keySize = keySize;
            this._provider = provider;
        }
        
        public void write(final OutputStream outputStream) throws IOException {
            try {
                OutputStream outputStream2;
                if (this._firstTime) {
                    if (this._keySize == 0) {
                        outputStream2 = SMIMEEnvelopedGenerator.this.fact.open(outputStream, this._encryptionOid, this._provider);
                    }
                    else {
                        outputStream2 = SMIMEEnvelopedGenerator.this.fact.open(outputStream, this._encryptionOid, this._keySize, this._provider);
                    }
                    this._firstTime = false;
                }
                else {
                    outputStream2 = SMIMEEnvelopedGenerator.this.fact.regenerate(outputStream, this._provider);
                }
                this._content.getDataHandler().setCommandMap((CommandMap)addCommands(CommandMap.getDefaultCommandMap()));
                this._content.writeTo(outputStream2);
                outputStream2.close();
            }
            catch (MessagingException ex) {
                throw new WrappingIOException(ex.toString(), (Throwable)ex);
            }
            catch (NoSuchAlgorithmException ex2) {
                throw new WrappingIOException(ex2.toString(), ex2);
            }
            catch (NoSuchProviderException ex3) {
                throw new WrappingIOException(ex3.toString(), ex3);
            }
            catch (CMSException ex4) {
                throw new WrappingIOException(ex4.toString(), ex4);
            }
        }
    }
    
    private class EnvelopedGenerator extends CMSEnvelopedDataStreamGenerator
    {
        private String _encryptionOID;
        private SecretKey _encKey;
        private AlgorithmParameters _params;
        private ASN1EncodableVector _recipientInfos;
        
        @Override
        protected OutputStream open(final OutputStream outputStream, final String encryptionOID, final SecretKey encKey, final AlgorithmParameters params, final ASN1EncodableVector recipientInfos, final Provider provider) throws NoSuchAlgorithmException, CMSException {
            this._encryptionOID = encryptionOID;
            this._encKey = encKey;
            this._params = params;
            this._recipientInfos = recipientInfos;
            return super.open(outputStream, encryptionOID, encKey, params, recipientInfos, provider);
        }
        
        OutputStream regenerate(final OutputStream outputStream, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
            return super.open(outputStream, this._encryptionOID, this._encKey, this._params, this._recipientInfos, provider);
        }
    }
    
    private static class WrappingIOException extends IOException
    {
        private Throwable cause;
        
        WrappingIOException(final String message, final Throwable cause) {
            super(message);
            this.cause = cause;
        }
        
        @Override
        public Throwable getCause() {
            return this.cause;
        }
    }
}
