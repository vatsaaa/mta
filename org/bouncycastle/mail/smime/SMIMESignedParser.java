// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime;

import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.internet.MimeMessage;
import javax.mail.Session;
import org.bouncycastle.cms.CMSException;
import javax.mail.internet.MimeMultipart;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import org.bouncycastle.cms.CMSTypedStream;
import javax.mail.BodyPart;
import java.io.File;
import java.io.IOException;
import javax.mail.MessagingException;
import java.io.InputStream;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import org.bouncycastle.cms.CMSSignedDataParser;

public class SMIMESignedParser extends CMSSignedDataParser
{
    Object message;
    MimeBodyPart content;
    
    private static InputStream getInputStream(final Part part) throws MessagingException {
        try {
            if (part.isMimeType("multipart/signed")) {
                throw new MessagingException("attempt to create signed data object from multipart content - use MimeMultipart constructor.");
            }
            return part.getInputStream();
        }
        catch (IOException obj) {
            throw new MessagingException("can't extract input stream: " + obj);
        }
    }
    
    private static File getTmpFile() throws MessagingException {
        try {
            return File.createTempFile("bcMail", ".mime");
        }
        catch (IOException obj) {
            throw new MessagingException("can't extract input stream: " + obj);
        }
    }
    
    private static CMSTypedStream getSignedInputStream(final BodyPart bodyPart, final String s, final File file) throws MessagingException {
        try {
            final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            SMIMEUtil.outputBodyPart(bufferedOutputStream, bodyPart, s);
            bufferedOutputStream.close();
            return new CMSTypedStream(new TemporaryFileInputStream(file));
        }
        catch (IOException obj) {
            throw new MessagingException("can't extract input stream: " + obj);
        }
    }
    
    public SMIMESignedParser(final MimeMultipart mimeMultipart) throws MessagingException, CMSException {
        this(mimeMultipart, getTmpFile());
    }
    
    public SMIMESignedParser(final MimeMultipart mimeMultipart, final File file) throws MessagingException, CMSException {
        this(mimeMultipart, "7bit", file);
    }
    
    public SMIMESignedParser(final MimeMultipart mimeMultipart, final String s) throws MessagingException, CMSException {
        this(mimeMultipart, s, getTmpFile());
    }
    
    public SMIMESignedParser(final MimeMultipart message, final String s, final File file) throws MessagingException, CMSException {
        super(getSignedInputStream(message.getBodyPart(0), s, file), getInputStream((Part)message.getBodyPart(1)));
        this.message = message;
        this.content = (MimeBodyPart)message.getBodyPart(0);
        this.drainContent();
    }
    
    public SMIMESignedParser(final Part message) throws MessagingException, CMSException, SMIMEException {
        super(getInputStream(message));
        this.message = message;
        final CMSTypedStream signedContent = this.getSignedContent();
        if (signedContent != null) {
            this.content = SMIMEUtil.toWriteOnceBodyPart(signedContent);
        }
    }
    
    public SMIMESignedParser(final Part message, final File file) throws MessagingException, CMSException, SMIMEException {
        super(getInputStream(message));
        this.message = message;
        final CMSTypedStream signedContent = this.getSignedContent();
        if (signedContent != null) {
            this.content = SMIMEUtil.toMimeBodyPart(signedContent, file);
        }
    }
    
    public MimeBodyPart getContent() {
        return this.content;
    }
    
    public MimeMessage getContentAsMimeMessage(final Session session) throws MessagingException, IOException {
        if (this.message instanceof MimeMultipart) {
            return new MimeMessage(session, ((MimeMultipart)this.message).getBodyPart(0).getInputStream());
        }
        return new MimeMessage(session, this.getSignedContent().getContentStream());
    }
    
    public Object getContentWithSignature() {
        return this.message;
    }
    
    private void drainContent() throws CMSException {
        try {
            this.getSignedContent().drain();
        }
        catch (IOException obj) {
            throw new CMSException("unable to read content for verification: " + obj, obj);
        }
    }
    
    static {
        final MailcapCommandMap defaultCommandMap = (MailcapCommandMap)CommandMap.getDefaultCommandMap();
        defaultCommandMap.addMailcap("application/pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_signature");
        defaultCommandMap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        defaultCommandMap.addMailcap("application/x-pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_signature");
        defaultCommandMap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        defaultCommandMap.addMailcap("multipart/signed;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.multipart_signed");
        CommandMap.setDefaultCommandMap((CommandMap)defaultCommandMap);
    }
    
    private static class TemporaryFileInputStream extends BufferedInputStream
    {
        private final File _file;
        
        TemporaryFileInputStream(final File file) throws FileNotFoundException {
            super(new FileInputStream(file));
            this._file = file;
        }
        
        @Override
        public void close() throws IOException {
            super.close();
            this._file.delete();
        }
    }
}
