// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime;

import java.io.IOException;
import org.bouncycastle.cms.CMSCompressedDataStreamGenerator;
import java.io.OutputStream;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

public class SMIMECompressedGenerator extends SMIMEGenerator
{
    public static final String ZLIB = "1.2.840.113549.1.9.16.3.8";
    private static final String COMPRESSED_CONTENT_TYPE = "application/pkcs7-mime; name=\"smime.p7z\"; smime-type=compressed-data";
    
    private MimeBodyPart make(final MimeBodyPart mimeBodyPart, final String s) throws SMIMEException {
        try {
            final MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent((Object)new ContentCompressor(mimeBodyPart, s), "application/pkcs7-mime; name=\"smime.p7z\"; smime-type=compressed-data");
            mimeBodyPart2.addHeader("Content-Type", "application/pkcs7-mime; name=\"smime.p7z\"; smime-type=compressed-data");
            mimeBodyPart2.addHeader("Content-Disposition", "attachment; filename=\"smime.p7z\"");
            mimeBodyPart2.addHeader("Content-Description", "S/MIME Compressed Message");
            mimeBodyPart2.addHeader("Content-Transfer-Encoding", this.encoding);
            return mimeBodyPart2;
        }
        catch (MessagingException ex) {
            throw new SMIMEException("exception putting multi-part together.", (Exception)ex);
        }
    }
    
    public MimeBodyPart generate(final MimeBodyPart mimeBodyPart, final String s) throws SMIMEException {
        return this.make(this.makeContentBodyPart(mimeBodyPart), s);
    }
    
    public MimeBodyPart generate(final MimeMessage mimeMessage, final String s) throws SMIMEException {
        try {
            mimeMessage.saveChanges();
        }
        catch (MessagingException ex) {
            throw new SMIMEException("unable to save message", (Exception)ex);
        }
        return this.make(this.makeContentBodyPart(mimeMessage), s);
    }
    
    static {
        final MailcapCommandMap defaultCommandMap = (MailcapCommandMap)CommandMap.getDefaultCommandMap();
        defaultCommandMap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        defaultCommandMap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        CommandMap.setDefaultCommandMap((CommandMap)defaultCommandMap);
    }
    
    private class ContentCompressor implements SMIMEStreamingProcessor
    {
        private final MimeBodyPart _content;
        private final String _compressionOid;
        
        ContentCompressor(final MimeBodyPart content, final String compressionOid) {
            this._content = content;
            this._compressionOid = compressionOid;
        }
        
        public void write(final OutputStream outputStream) throws IOException {
            final OutputStream open = new CMSCompressedDataStreamGenerator().open(outputStream, this._compressionOid);
            try {
                this._content.writeTo(open);
                open.close();
            }
            catch (MessagingException ex) {
                throw new IOException(ex.toString());
            }
        }
    }
}
