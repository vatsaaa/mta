// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.handlers;

import org.bouncycastle.mail.smime.SMIMEStreamingProcessor;
import java.io.BufferedInputStream;
import java.io.InputStream;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import java.io.OutputStream;
import java.io.IOException;
import javax.activation.DataSource;
import java.awt.datatransfer.DataFlavor;
import javax.activation.ActivationDataFlavor;
import javax.activation.DataContentHandler;

public class PKCS7ContentHandler implements DataContentHandler
{
    private final ActivationDataFlavor _adf;
    private final DataFlavor[] _dfs;
    
    PKCS7ContentHandler(final ActivationDataFlavor adf, final DataFlavor[] dfs) {
        this._adf = adf;
        this._dfs = dfs;
    }
    
    public Object getContent(final DataSource dataSource) throws IOException {
        return dataSource.getInputStream();
    }
    
    public Object getTransferData(final DataFlavor dataFlavor, final DataSource dataSource) throws IOException {
        if (this._adf.equals(dataFlavor)) {
            return this.getContent(dataSource);
        }
        return null;
    }
    
    public DataFlavor[] getTransferDataFlavors() {
        return this._dfs;
    }
    
    public void writeTo(final Object obj, final String s, final OutputStream outputStream) throws IOException {
        if (obj instanceof MimeBodyPart) {
            try {
                ((MimeBodyPart)obj).writeTo(outputStream);
                return;
            }
            catch (MessagingException ex) {
                throw new IOException(ex.getMessage());
            }
        }
        if (obj instanceof byte[]) {
            outputStream.write((byte[])obj);
        }
        else if (obj instanceof InputStream) {
            InputStream in = (InputStream)obj;
            if (!(in instanceof BufferedInputStream)) {
                in = new BufferedInputStream(in);
            }
            int read;
            while ((read = in.read()) >= 0) {
                outputStream.write(read);
            }
        }
        else {
            if (!(obj instanceof SMIMEStreamingProcessor)) {
                throw new IOException("unknown object in writeTo " + obj);
            }
            ((SMIMEStreamingProcessor)obj).write(outputStream);
        }
    }
}
