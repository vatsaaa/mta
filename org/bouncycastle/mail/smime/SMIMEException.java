// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime;

public class SMIMEException extends Exception
{
    Exception e;
    
    public SMIMEException(final String message) {
        super(message);
    }
    
    public SMIMEException(final String message, final Exception e) {
        super(message);
        this.e = e;
    }
    
    public Exception getUnderlyingException() {
        return this.e;
    }
    
    @Override
    public Throwable getCause() {
        return this.e;
    }
}
