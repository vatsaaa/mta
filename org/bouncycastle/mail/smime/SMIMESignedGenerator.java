// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime;

import java.io.IOException;
import java.util.Enumeration;
import org.bouncycastle.mail.smime.util.CRLFOutputStream;
import javax.mail.internet.ContentType;
import javax.mail.Multipart;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import org.bouncycastle.cms.CMSSignedDataStreamGenerator;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import javax.mail.internet.MimeMessage;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import javax.mail.MessagingException;
import javax.mail.BodyPart;
import java.util.Collection;
import javax.mail.internet.MimeMultipart;
import java.security.Provider;
import javax.mail.internet.MimeBodyPart;
import org.bouncycastle.cms.SignerInformation;
import java.util.HashSet;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.x509.X509Store;
import java.security.cert.CertStoreException;
import java.security.cert.CertStore;
import java.util.Iterator;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.ArrayList;
import javax.activation.MailcapCommandMap;
import javax.activation.CommandMap;
import java.util.Map;
import java.util.List;

public class SMIMESignedGenerator extends SMIMEGenerator
{
    public static final String DIGEST_SHA1;
    public static final String DIGEST_MD5;
    public static final String DIGEST_SHA224;
    public static final String DIGEST_SHA256;
    public static final String DIGEST_SHA384;
    public static final String DIGEST_SHA512;
    public static final String DIGEST_GOST3411;
    public static final String DIGEST_RIPEMD128;
    public static final String DIGEST_RIPEMD160;
    public static final String DIGEST_RIPEMD256;
    public static final String ENCRYPTION_RSA;
    public static final String ENCRYPTION_DSA;
    public static final String ENCRYPTION_ECDSA;
    public static final String ENCRYPTION_RSA_PSS;
    public static final String ENCRYPTION_GOST3410;
    public static final String ENCRYPTION_ECGOST3410;
    private static final String CERTIFICATE_MANAGEMENT_CONTENT = "application/pkcs7-mime; name=smime.p7c; smime-type=certs-only";
    private static final String DETACHED_SIGNATURE_TYPE = "application/pkcs7-signature; name=smime.p7s; smime-type=signed-data";
    private static final String ENCAPSULATED_SIGNED_CONTENT_TYPE = "application/pkcs7-mime; name=smime.p7m; smime-type=signed-data";
    private final String _defaultContentTransferEncoding;
    private List _certStores;
    private List _signers;
    private List _oldSigners;
    private List _attributeCerts;
    private Map _digests;
    
    private static MailcapCommandMap addCommands(final CommandMap commandMap) {
        final MailcapCommandMap mailcapCommandMap = (MailcapCommandMap)commandMap;
        mailcapCommandMap.addMailcap("application/pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_signature");
        mailcapCommandMap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        mailcapCommandMap.addMailcap("application/x-pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_signature");
        mailcapCommandMap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        mailcapCommandMap.addMailcap("multipart/signed;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.multipart_signed");
        return mailcapCommandMap;
    }
    
    public SMIMESignedGenerator() {
        this._certStores = new ArrayList();
        this._signers = new ArrayList();
        this._oldSigners = new ArrayList();
        this._attributeCerts = new ArrayList();
        this._digests = new HashMap();
        this._defaultContentTransferEncoding = "7bit";
    }
    
    public SMIMESignedGenerator(final String defaultContentTransferEncoding) {
        this._certStores = new ArrayList();
        this._signers = new ArrayList();
        this._oldSigners = new ArrayList();
        this._attributeCerts = new ArrayList();
        this._digests = new HashMap();
        this._defaultContentTransferEncoding = defaultContentTransferEncoding;
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s) throws IllegalArgumentException {
        this._signers.add(new Signer(privateKey, x509Certificate, s, null, null));
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2) throws IllegalArgumentException {
        this._signers.add(new Signer(privateKey, x509Certificate, s, s2, null, null));
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this._signers.add(new Signer(privateKey, x509Certificate, s, attributeTable, attributeTable2));
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this._signers.add(new Signer(privateKey, x509Certificate, s, s2, attributeTable, attributeTable2));
    }
    
    public void addSigners(final SignerInformationStore signerInformationStore) {
        final Iterator<Object> iterator = signerInformationStore.getSigners().iterator();
        while (iterator.hasNext()) {
            this._oldSigners.add(iterator.next());
        }
    }
    
    public void addCertificatesAndCRLs(final CertStore certStore) throws CertStoreException, SMIMEException {
        this._certStores.add(certStore);
    }
    
    public void addAttributeCertificates(final X509Store x509Store) throws CMSException {
        this._attributeCerts.add(x509Store);
    }
    
    private void addHashHeader(final StringBuffer sb, final List list) {
        int n = 0;
        final Iterator<Signer> iterator = list.iterator();
        final HashSet<String> set = new HashSet<String>();
        while (iterator.hasNext()) {
            final Signer next = iterator.next();
            String s;
            if (next instanceof Signer) {
                s = next.getDigestOID();
            }
            else {
                s = ((SignerInformation)next).getDigestAlgOID();
            }
            if (s.equals(SMIMESignedGenerator.DIGEST_SHA1)) {
                set.add("sha1");
            }
            else if (s.equals(SMIMESignedGenerator.DIGEST_MD5)) {
                set.add("md5");
            }
            else if (s.equals(SMIMESignedGenerator.DIGEST_SHA224)) {
                set.add("sha224");
            }
            else if (s.equals(SMIMESignedGenerator.DIGEST_SHA256)) {
                set.add("sha256");
            }
            else if (s.equals(SMIMESignedGenerator.DIGEST_SHA384)) {
                set.add("sha384");
            }
            else if (s.equals(SMIMESignedGenerator.DIGEST_SHA512)) {
                set.add("sha512");
            }
            else if (s.equals(SMIMESignedGenerator.DIGEST_GOST3411)) {
                set.add("gostr3411-94");
            }
            else {
                set.add("unknown");
            }
        }
        for (final String str : set) {
            if (n == 0) {
                if (set.size() != 1) {
                    sb.append("; micalg=\"");
                }
                else {
                    sb.append("; micalg=");
                }
            }
            else {
                sb.append(',');
            }
            sb.append(str);
            ++n;
        }
        if (n != 0 && set.size() != 1) {
            sb.append('\"');
        }
    }
    
    private MimeMultipart make(final MimeBodyPart mimeBodyPart, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        try {
            final MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent((Object)new ContentSigner(mimeBodyPart, false, provider), "application/pkcs7-signature; name=smime.p7s; smime-type=signed-data");
            mimeBodyPart2.addHeader("Content-Type", "application/pkcs7-signature; name=smime.p7s; smime-type=signed-data");
            mimeBodyPart2.addHeader("Content-Disposition", "attachment; filename=\"smime.p7s\"");
            mimeBodyPart2.addHeader("Content-Description", "S/MIME Cryptographic Signature");
            mimeBodyPart2.addHeader("Content-Transfer-Encoding", this.encoding);
            final StringBuffer sb = new StringBuffer("signed; protocol=\"application/pkcs7-signature\"");
            final ArrayList list = new ArrayList(this._signers);
            list.addAll(this._oldSigners);
            this.addHashHeader(sb, list);
            final MimeMultipart mimeMultipart = new MimeMultipart(sb.toString());
            mimeMultipart.addBodyPart((BodyPart)mimeBodyPart);
            mimeMultipart.addBodyPart((BodyPart)mimeBodyPart2);
            return mimeMultipart;
        }
        catch (MessagingException ex) {
            throw new SMIMEException("exception putting multi-part together.", (Exception)ex);
        }
    }
    
    private MimeBodyPart makeEncapsulated(final MimeBodyPart mimeBodyPart, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        try {
            final MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent((Object)new ContentSigner(mimeBodyPart, true, provider), "application/pkcs7-mime; name=smime.p7m; smime-type=signed-data");
            mimeBodyPart2.addHeader("Content-Type", "application/pkcs7-mime; name=smime.p7m; smime-type=signed-data");
            mimeBodyPart2.addHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
            mimeBodyPart2.addHeader("Content-Description", "S/MIME Cryptographic Signed Data");
            mimeBodyPart2.addHeader("Content-Transfer-Encoding", this.encoding);
            return mimeBodyPart2;
        }
        catch (MessagingException ex) {
            throw new SMIMEException("exception putting body part together.", (Exception)ex);
        }
    }
    
    public Map getGeneratedDigests() {
        return new HashMap(this._digests);
    }
    
    public MimeMultipart generate(final MimeBodyPart mimeBodyPart, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.make(this.makeContentBodyPart(mimeBodyPart), SMIMEUtil.getProvider(s));
    }
    
    public MimeMultipart generate(final MimeBodyPart mimeBodyPart, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        return this.make(this.makeContentBodyPart(mimeBodyPart), provider);
    }
    
    public MimeMultipart generate(final MimeMessage mimeMessage, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.generate(mimeMessage, SMIMEUtil.getProvider(s));
    }
    
    public MimeMultipart generate(final MimeMessage mimeMessage, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        try {
            mimeMessage.saveChanges();
        }
        catch (MessagingException ex) {
            throw new SMIMEException("unable to save message", (Exception)ex);
        }
        return this.make(this.makeContentBodyPart(mimeMessage), provider);
    }
    
    public MimeBodyPart generateEncapsulated(final MimeBodyPart mimeBodyPart, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.makeEncapsulated(this.makeContentBodyPart(mimeBodyPart), SMIMEUtil.getProvider(s));
    }
    
    public MimeBodyPart generateEncapsulated(final MimeBodyPart mimeBodyPart, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.makeEncapsulated(this.makeContentBodyPart(mimeBodyPart), provider);
    }
    
    public MimeBodyPart generateEncapsulated(final MimeMessage mimeMessage, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return this.generateEncapsulated(mimeMessage, SMIMEUtil.getProvider(s));
    }
    
    public MimeBodyPart generateEncapsulated(final MimeMessage mimeMessage, final Provider provider) throws NoSuchAlgorithmException, SMIMEException {
        try {
            mimeMessage.saveChanges();
        }
        catch (MessagingException ex) {
            throw new SMIMEException("unable to save message", (Exception)ex);
        }
        return this.makeEncapsulated(this.makeContentBodyPart(mimeMessage), provider);
    }
    
    public MimeBodyPart generateCertificateManagement(final String s) throws SMIMEException, NoSuchProviderException {
        return this.generateCertificateManagement(SMIMEUtil.getProvider(s));
    }
    
    public MimeBodyPart generateCertificateManagement(final Provider provider) throws SMIMEException {
        try {
            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent((Object)new ContentSigner(null, true, provider), "application/pkcs7-mime; name=smime.p7c; smime-type=certs-only");
            mimeBodyPart.addHeader("Content-Type", "application/pkcs7-mime; name=smime.p7c; smime-type=certs-only");
            mimeBodyPart.addHeader("Content-Disposition", "attachment; filename=\"smime.p7c\"");
            mimeBodyPart.addHeader("Content-Description", "S/MIME Certificate Management Message");
            mimeBodyPart.addHeader("Content-Transfer-Encoding", this.encoding);
            return mimeBodyPart;
        }
        catch (MessagingException ex) {
            throw new SMIMEException("exception putting body part together.", (Exception)ex);
        }
    }
    
    static {
        DIGEST_SHA1 = OIWObjectIdentifiers.idSHA1.getId();
        DIGEST_MD5 = PKCSObjectIdentifiers.md5.getId();
        DIGEST_SHA224 = NISTObjectIdentifiers.id_sha224.getId();
        DIGEST_SHA256 = NISTObjectIdentifiers.id_sha256.getId();
        DIGEST_SHA384 = NISTObjectIdentifiers.id_sha384.getId();
        DIGEST_SHA512 = NISTObjectIdentifiers.id_sha512.getId();
        DIGEST_GOST3411 = CryptoProObjectIdentifiers.gostR3411.getId();
        DIGEST_RIPEMD128 = TeleTrusTObjectIdentifiers.ripemd128.getId();
        DIGEST_RIPEMD160 = TeleTrusTObjectIdentifiers.ripemd160.getId();
        DIGEST_RIPEMD256 = TeleTrusTObjectIdentifiers.ripemd256.getId();
        ENCRYPTION_RSA = PKCSObjectIdentifiers.rsaEncryption.getId();
        ENCRYPTION_DSA = X9ObjectIdentifiers.id_dsa_with_sha1.getId();
        ENCRYPTION_ECDSA = X9ObjectIdentifiers.ecdsa_with_SHA1.getId();
        ENCRYPTION_RSA_PSS = PKCSObjectIdentifiers.id_RSASSA_PSS.getId();
        ENCRYPTION_GOST3410 = CryptoProObjectIdentifiers.gostR3410_94.getId();
        ENCRYPTION_ECGOST3410 = CryptoProObjectIdentifiers.gostR3410_2001.getId();
        CommandMap.setDefaultCommandMap((CommandMap)addCommands(CommandMap.getDefaultCommandMap()));
    }
    
    private class ContentSigner implements SMIMEStreamingProcessor
    {
        private final MimeBodyPart _content;
        private final boolean _encapsulate;
        private final Provider _provider;
        
        ContentSigner(final MimeBodyPart content, final boolean encapsulate, final Provider provider) {
            this._content = content;
            this._encapsulate = encapsulate;
            this._provider = provider;
        }
        
        protected CMSSignedDataStreamGenerator getGenerator() throws CMSException, CertStoreException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException {
            final CMSSignedDataStreamGenerator cmsSignedDataStreamGenerator = new CMSSignedDataStreamGenerator();
            final Iterator<CertStore> iterator = SMIMESignedGenerator.this._certStores.iterator();
            while (iterator.hasNext()) {
                cmsSignedDataStreamGenerator.addCertificatesAndCRLs(iterator.next());
            }
            final Iterator<X509Store> iterator2 = SMIMESignedGenerator.this._attributeCerts.iterator();
            while (iterator2.hasNext()) {
                cmsSignedDataStreamGenerator.addAttributeCertificates(iterator2.next());
            }
            for (final Signer signer : SMIMESignedGenerator.this._signers) {
                if (signer.getEncryptionOID() != null) {
                    cmsSignedDataStreamGenerator.addSigner(signer.getKey(), signer.getCert(), signer.getEncryptionOID(), signer.getDigestOID(), signer.getSignedAttr(), signer.getUnsignedAttr(), this._provider);
                }
                else {
                    cmsSignedDataStreamGenerator.addSigner(signer.getKey(), signer.getCert(), signer.getDigestOID(), signer.getSignedAttr(), signer.getUnsignedAttr(), this._provider);
                }
            }
            cmsSignedDataStreamGenerator.addSigners(new SignerInformationStore(SMIMESignedGenerator.this._oldSigners));
            return cmsSignedDataStreamGenerator;
        }
        
        private void writeBodyPart(OutputStream outputStream, final MimeBodyPart mimeBodyPart) throws IOException, MessagingException {
            if (mimeBodyPart.getContent() instanceof Multipart) {
                final Multipart multipart = (Multipart)mimeBodyPart.getContent();
                final String string = "--" + new ContentType(multipart.getContentType()).getParameter("boundary");
                final SMIMEUtil.LineOutputStream lineOutputStream = new SMIMEUtil.LineOutputStream(outputStream);
                final Enumeration allHeaderLines = mimeBodyPart.getAllHeaderLines();
                while (allHeaderLines.hasMoreElements()) {
                    lineOutputStream.writeln(allHeaderLines.nextElement());
                }
                lineOutputStream.writeln();
                SMIMEUtil.outputPreamble(lineOutputStream, mimeBodyPart, string);
                for (int i = 0; i < multipart.getCount(); ++i) {
                    lineOutputStream.writeln(string);
                    this.writeBodyPart(outputStream, (MimeBodyPart)multipart.getBodyPart(i));
                    lineOutputStream.writeln();
                }
                lineOutputStream.writeln(string + "--");
            }
            else {
                if (SMIMEUtil.isCanonicalisationRequired(mimeBodyPart, SMIMESignedGenerator.this._defaultContentTransferEncoding)) {
                    outputStream = new CRLFOutputStream(outputStream);
                }
                mimeBodyPart.writeTo(outputStream);
            }
        }
        
        public void write(final OutputStream outputStream) throws IOException {
            try {
                final CMSSignedDataStreamGenerator generator = this.getGenerator();
                final OutputStream open = generator.open(outputStream, this._encapsulate);
                if (this._content != null) {
                    if (!this._encapsulate) {
                        this.writeBodyPart(open, this._content);
                    }
                    else {
                        this._content.getDataHandler().setCommandMap((CommandMap)addCommands(CommandMap.getDefaultCommandMap()));
                        this._content.writeTo(open);
                    }
                }
                open.close();
                SMIMESignedGenerator.this._digests = generator.getGeneratedDigests();
            }
            catch (MessagingException ex) {
                throw new IOException(ex.toString());
            }
            catch (NoSuchAlgorithmException ex2) {
                throw new IOException(ex2.toString());
            }
            catch (NoSuchProviderException ex3) {
                throw new IOException(ex3.toString());
            }
            catch (CMSException ex4) {
                throw new IOException(ex4.toString());
            }
            catch (InvalidKeyException ex5) {
                throw new IOException(ex5.toString());
            }
            catch (CertStoreException ex6) {
                throw new IOException(ex6.toString());
            }
        }
    }
    
    private class Signer
    {
        final PrivateKey key;
        final X509Certificate cert;
        final String encryptionOID;
        final String digestOID;
        final AttributeTable signedAttr;
        final AttributeTable unsignedAttr;
        
        Signer(final SMIMESignedGenerator smimeSignedGenerator, final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2) {
            this(smimeSignedGenerator, privateKey, x509Certificate, null, s, attributeTable, attributeTable2);
        }
        
        Signer(final PrivateKey key, final X509Certificate cert, final String encryptionOID, final String digestOID, final AttributeTable signedAttr, final AttributeTable unsignedAttr) {
            this.key = key;
            this.cert = cert;
            this.encryptionOID = encryptionOID;
            this.digestOID = digestOID;
            this.signedAttr = signedAttr;
            this.unsignedAttr = unsignedAttr;
        }
        
        public X509Certificate getCert() {
            return this.cert;
        }
        
        public String getEncryptionOID() {
            return this.encryptionOID;
        }
        
        public String getDigestOID() {
            return this.digestOID;
        }
        
        public PrivateKey getKey() {
            return this.key;
        }
        
        public AttributeTable getSignedAttr() {
            return this.signedAttr;
        }
        
        public AttributeTable getUnsignedAttr() {
            return this.unsignedAttr;
        }
    }
}
