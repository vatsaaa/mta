// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.validator;

import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.X509Extensions;
import java.security.cert.TrustAnchor;
import java.util.LinkedHashSet;
import java.util.Collection;
import java.security.cert.CertSelector;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.Time;
import org.bouncycastle.asn1.cms.CMSAttributes;
import java.security.PublicKey;
import org.bouncycastle.i18n.filter.UntrustedInput;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.RSAPublicKey;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DERObject;
import java.security.cert.CertificateEncodingException;
import java.io.IOException;
import java.util.Vector;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.PrincipalUtil;
import java.util.HashSet;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.util.Iterator;
import org.bouncycastle.x509.CertPathReviewerException;
import java.security.GeneralSecurityException;
import org.bouncycastle.x509.PKIXCertPathReviewer;
import java.security.cert.CertPath;
import java.util.Set;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateExpiredException;
import org.bouncycastle.i18n.filter.TrustedInput;
import java.util.Date;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.security.cert.X509CertSelector;
import java.util.List;
import org.bouncycastle.cms.SignerInformation;
import java.util.ArrayList;
import javax.mail.Address;
import java.util.HashMap;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.Part;
import org.bouncycastle.i18n.ErrorBundle;
import org.bouncycastle.mail.smime.SMIMESigned;
import javax.mail.internet.MimeMultipart;
import java.security.cert.PKIXParameters;
import javax.mail.internet.MimeMessage;
import java.util.Map;
import org.bouncycastle.cms.SignerInformationStore;
import java.security.cert.CertStore;

public class SignedMailValidator
{
    private static final String RESOURCE_NAME = "org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages";
    private static final Class DEFAULT_CERT_PATH_REVIEWER;
    private static final String EXT_KEY_USAGE;
    private static final String SUBJECT_ALTERNATIVE_NAME;
    private static final int shortKeyLength = 512;
    private static final long THIRTY_YEARS_IN_MILLI_SEC = 946728000000L;
    private CertStore certs;
    private SignerInformationStore signers;
    private Map results;
    private String[] fromAddresses;
    private Class certPathReviewerClass;
    
    public SignedMailValidator(final MimeMessage mimeMessage, final PKIXParameters pkixParameters) throws SignedMailValidatorException {
        this(mimeMessage, pkixParameters, SignedMailValidator.DEFAULT_CERT_PATH_REVIEWER);
    }
    
    public SignedMailValidator(final MimeMessage mimeMessage, final PKIXParameters pkixParameters, final Class certPathReviewerClass) throws SignedMailValidatorException {
        this.certPathReviewerClass = certPathReviewerClass;
        if (!SignedMailValidator.DEFAULT_CERT_PATH_REVIEWER.isAssignableFrom(certPathReviewerClass)) {
            throw new IllegalArgumentException("certPathReviewerClass is not a subclass of " + SignedMailValidator.DEFAULT_CERT_PATH_REVIEWER.getName());
        }
        try {
            SMIMESigned smimeSigned;
            if (mimeMessage.isMimeType("multipart/signed")) {
                smimeSigned = new SMIMESigned((MimeMultipart)mimeMessage.getContent());
            }
            else {
                if (!mimeMessage.isMimeType("application/pkcs7-mime") && !mimeMessage.isMimeType("application/x-pkcs7-mime")) {
                    throw new SignedMailValidatorException(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.noSignedMessage"));
                }
                smimeSigned = new SMIMESigned((Part)mimeMessage);
            }
            this.certs = smimeSigned.getCertificatesAndCRLs("Collection", "BC");
            this.signers = smimeSigned.getSignerInfos();
            final Address[] from = mimeMessage.getFrom();
            InternetAddress internetAddress = null;
            try {
                if (mimeMessage.getHeader("Sender") != null) {
                    internetAddress = new InternetAddress(mimeMessage.getHeader("Sender")[0]);
                }
            }
            catch (MessagingException ex2) {}
            this.fromAddresses = new String[from.length + ((internetAddress != null) ? 1 : 0)];
            for (int i = 0; i < from.length; ++i) {
                this.fromAddresses[i] = ((InternetAddress)from[i]).getAddress();
            }
            if (internetAddress != null) {
                this.fromAddresses[from.length] = internetAddress.getAddress();
            }
            this.results = new HashMap();
        }
        catch (Exception ex) {
            if (ex instanceof SignedMailValidatorException) {
                throw (SignedMailValidatorException)ex;
            }
            throw new SignedMailValidatorException(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.exceptionReadingMessage", new Object[] { ex.getMessage(), ex, ex.getClass().getName() }), ex);
        }
        this.validateSignatures(pkixParameters);
    }
    
    protected void validateSignatures(final PKIXParameters pkixParameters) {
        final PKIXParameters pkixParameters2 = (PKIXParameters)pkixParameters.clone();
        pkixParameters2.addCertStore(this.certs);
        final Iterator<SignerInformation> iterator = (Iterator<SignerInformation>)this.signers.getSigners().iterator();
        while (iterator.hasNext()) {
            final ArrayList<ErrorBundle> list = new ArrayList<ErrorBundle>();
            final ArrayList<ErrorBundle> list2 = new ArrayList<ErrorBundle>();
            final SignerInformation signerInformation = iterator.next();
            X509Certificate x509Certificate = null;
            try {
                final Iterator iterator2 = findCerts(pkixParameters2.getCertStores(), signerInformation.getSID()).iterator();
                if (iterator2.hasNext()) {
                    x509Certificate = iterator2.next();
                }
            }
            catch (CertStoreException ex) {
                list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.exceptionRetrievingSignerCert", new Object[] { ex.getMessage(), ex, ex.getClass().getName() }));
            }
            if (x509Certificate != null) {
                boolean verify = false;
                try {
                    verify = signerInformation.verify(x509Certificate.getPublicKey(), "BC");
                    if (!verify) {
                        list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.signatureNotVerified"));
                    }
                }
                catch (Exception ex2) {
                    list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.exceptionVerifyingSignature", new Object[] { ex2.getMessage(), ex2, ex2.getClass().getName() }));
                }
                this.checkSignerCert(x509Certificate, list, list2);
                final AttributeTable signedAttributes = signerInformation.getSignedAttributes();
                if (signedAttributes != null && signedAttributes.get(PKCSObjectIdentifiers.id_aa_receiptRequest) != null) {
                    list2.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.signedReceiptRequest"));
                }
                Date signatureTime = getSignatureTime(signerInformation);
                if (signatureTime == null) {
                    list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.noSigningTime"));
                    signatureTime = new Date();
                }
                else {
                    try {
                        x509Certificate.checkValidity(signatureTime);
                    }
                    catch (CertificateExpiredException ex7) {
                        list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.certExpired", new Object[] { new TrustedInput(signatureTime), new TrustedInput(x509Certificate.getNotAfter()) }));
                    }
                    catch (CertificateNotYetValidException ex8) {
                        list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.certNotYetValid", new Object[] { new TrustedInput(signatureTime), new TrustedInput(x509Certificate.getNotBefore()) }));
                    }
                }
                pkixParameters2.setDate(signatureTime);
                try {
                    final ArrayList<CertStore> list3 = new ArrayList<CertStore>();
                    list3.add(this.certs);
                    final Object[] certPath = createCertPath(x509Certificate, pkixParameters2.getTrustAnchors(), pkixParameters.getCertStores(), list3);
                    final CertPath certPath2 = (CertPath)certPath[0];
                    final List list4 = (List)certPath[1];
                    PKIXCertPathReviewer pkixCertPathReviewer;
                    try {
                        pkixCertPathReviewer = this.certPathReviewerClass.newInstance();
                    }
                    catch (IllegalAccessException ex3) {
                        throw new IllegalArgumentException("Cannot instantiate object of type " + this.certPathReviewerClass.getName() + ": " + ex3.getMessage());
                    }
                    catch (InstantiationException ex4) {
                        throw new IllegalArgumentException("Cannot instantiate object of type " + this.certPathReviewerClass.getName() + ": " + ex4.getMessage());
                    }
                    pkixCertPathReviewer.init(certPath2, pkixParameters2);
                    if (!pkixCertPathReviewer.isValidCertPath()) {
                        list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.certPathInvalid"));
                    }
                    this.results.put(signerInformation, new ValidationResult(pkixCertPathReviewer, verify, list, list2, list4));
                }
                catch (GeneralSecurityException ex5) {
                    list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.exceptionCreateCertPath", new Object[] { ex5.getMessage(), ex5, ex5.getClass().getName() }));
                    this.results.put(signerInformation, new ValidationResult(null, verify, list, list2, null));
                }
                catch (CertPathReviewerException ex6) {
                    list.add(ex6.getErrorMessage());
                    this.results.put(signerInformation, new ValidationResult(null, verify, list, list2, null));
                }
            }
            else {
                list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.noSignerCert"));
                this.results.put(signerInformation, new ValidationResult(null, false, list, list2, null));
            }
        }
    }
    
    public static Set getEmailAddresses(final X509Certificate x509Certificate) throws IOException, CertificateEncodingException {
        final HashSet<String> set = new HashSet<String>();
        final X509Principal subjectX509Principal = PrincipalUtil.getSubjectX509Principal(x509Certificate);
        final Vector oiDs = subjectX509Principal.getOIDs();
        final Vector values = subjectX509Principal.getValues();
        for (int i = 0; i < oiDs.size(); ++i) {
            if (oiDs.get(i).equals(X509Principal.EmailAddress)) {
                set.add(values.get(i).toLowerCase());
                break;
            }
        }
        final byte[] extensionValue = x509Certificate.getExtensionValue(SignedMailValidator.SUBJECT_ALTERNATIVE_NAME);
        if (extensionValue != null) {
            final DERSequence derSequence = (DERSequence)getObject(extensionValue);
            for (int j = 0; j < derSequence.size(); ++j) {
                final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)derSequence.getObjectAt(j);
                if (asn1TaggedObject.getTagNo() == 1) {
                    set.add(DERIA5String.getInstance(asn1TaggedObject, true).getString().toLowerCase());
                }
            }
        }
        return set;
    }
    
    private static DERObject getObject(final byte[] array) throws IOException {
        return new ASN1InputStream(((ASN1OctetString)new ASN1InputStream(array).readObject()).getOctets()).readObject();
    }
    
    protected void checkSignerCert(final X509Certificate x509Certificate, final List list, final List list2) {
        final PublicKey publicKey = x509Certificate.getPublicKey();
        int value = -1;
        if (publicKey instanceof RSAPublicKey) {
            value = ((RSAPublicKey)publicKey).getModulus().bitLength();
        }
        else if (publicKey instanceof DSAPublicKey) {
            value = ((DSAPublicKey)publicKey).getParams().getP().bitLength();
        }
        if (value != -1 && value <= 512) {
            list2.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.shortSigningKey", new Object[] { new Integer(value) }));
        }
        if (x509Certificate.getNotAfter().getTime() - x509Certificate.getNotBefore().getTime() > 946728000000L) {
            list2.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.longValidity", new Object[] { new TrustedInput(x509Certificate.getNotBefore()), new TrustedInput(x509Certificate.getNotAfter()) }));
        }
        final boolean[] keyUsage = x509Certificate.getKeyUsage();
        if (keyUsage != null && !keyUsage[0] && !keyUsage[1]) {
            list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.signingNotPermitted"));
        }
        try {
            final byte[] extensionValue = x509Certificate.getExtensionValue(SignedMailValidator.EXT_KEY_USAGE);
            if (extensionValue != null) {
                final ExtendedKeyUsage instance = ExtendedKeyUsage.getInstance(getObject(extensionValue));
                if (!instance.hasKeyPurposeId(KeyPurposeId.anyExtendedKeyUsage) && !instance.hasKeyPurposeId(KeyPurposeId.id_kp_emailProtection)) {
                    list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.extKeyUsageNotPermitted"));
                }
            }
        }
        catch (Exception ex) {
            list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.extKeyUsageError", new Object[] { ex.getMessage(), ex, ex.getClass().getName() }));
        }
        try {
            final Set emailAddresses = getEmailAddresses(x509Certificate);
            if (emailAddresses.isEmpty()) {
                list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.noEmailInCert"));
            }
            else {
                boolean b = false;
                for (int i = 0; i < this.fromAddresses.length; ++i) {
                    if (emailAddresses.contains(this.fromAddresses[i].toLowerCase())) {
                        b = true;
                        break;
                    }
                }
                if (!b) {
                    list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.emailFromCertMismatch", new Object[] { new UntrustedInput(addressesToString(this.fromAddresses)), new UntrustedInput(emailAddresses) }));
                }
            }
        }
        catch (Exception ex2) {
            list.add(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.certGetEmailError", new Object[] { ex2.getMessage(), ex2, ex2.getClass().getName() }));
        }
    }
    
    static String addressesToString(final Object[] array) {
        if (array == null) {
            return "null";
        }
        final StringBuffer sb = new StringBuffer();
        sb.append('[');
        for (int i = 0; i != array.length; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(String.valueOf(array[i]));
        }
        return sb.append(']').toString();
    }
    
    public static Date getSignatureTime(final SignerInformation signerInformation) {
        final AttributeTable signedAttributes = signerInformation.getSignedAttributes();
        Date date = null;
        if (signedAttributes != null) {
            final Attribute value = signedAttributes.get(CMSAttributes.signingTime);
            if (value != null) {
                date = Time.getInstance(value.getAttrValues().getObjectAt(0).getDERObject()).getDate();
            }
        }
        return date;
    }
    
    private static List findCerts(final List list, final X509CertSelector selector) throws CertStoreException {
        final ArrayList list2 = new ArrayList();
        final Iterator<CertStore> iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.addAll(iterator.next().getCertificates(selector));
        }
        return list2;
    }
    
    private static X509Certificate findNextCert(final List list, final X509CertSelector x509CertSelector, final Set set) throws CertStoreException {
        final Iterator<X509Certificate> iterator = findCerts(list, x509CertSelector).iterator();
        boolean b = false;
        X509Certificate x509Certificate = null;
        while (iterator.hasNext()) {
            x509Certificate = iterator.next();
            if (!set.contains(x509Certificate)) {
                b = true;
                break;
            }
        }
        return b ? x509Certificate : null;
    }
    
    public static CertPath createCertPath(final X509Certificate x509Certificate, final Set set, final List list) throws GeneralSecurityException {
        return (CertPath)createCertPath(x509Certificate, set, list, null)[0];
    }
    
    public static Object[] createCertPath(final X509Certificate x509Certificate, final Set set, final List list, final List list2) throws GeneralSecurityException {
        final LinkedHashSet<X509Certificate> c = new LinkedHashSet<X509Certificate>();
        final ArrayList<Boolean> list3 = new ArrayList<Boolean>();
        X509Certificate x509Certificate2 = x509Certificate;
        c.add(x509Certificate2);
        list3.add(new Boolean(true));
        int n = 0;
        X509Certificate x509Certificate3 = null;
        while (x509Certificate2 != null && n == 0) {
            for (final TrustAnchor trustAnchor : set) {
                final X509Certificate trustedCert = trustAnchor.getTrustedCert();
                if (trustedCert != null) {
                    if (!trustedCert.getSubjectX500Principal().equals(x509Certificate2.getIssuerX500Principal())) {
                        continue;
                    }
                    try {
                        x509Certificate2.verify(trustedCert.getPublicKey(), "BC");
                        n = 1;
                        x509Certificate3 = trustedCert;
                        break;
                    }
                    catch (Exception ex3) {
                        continue;
                    }
                }
                if (trustAnchor.getCAName().equals(x509Certificate2.getIssuerX500Principal().getName())) {
                    try {
                        x509Certificate2.verify(trustAnchor.getCAPublicKey(), "BC");
                        n = 1;
                        break;
                    }
                    catch (Exception ex4) {}
                }
            }
            if (n == 0) {
                final X509CertSelector x509CertSelector = new X509CertSelector();
                try {
                    x509CertSelector.setSubject(x509Certificate2.getIssuerX500Principal().getEncoded());
                }
                catch (IOException ex) {
                    throw new IllegalStateException(ex.toString());
                }
                final byte[] extensionValue = x509Certificate2.getExtensionValue(X509Extensions.AuthorityKeyIdentifier.getId());
                if (extensionValue != null) {
                    try {
                        final AuthorityKeyIdentifier instance = AuthorityKeyIdentifier.getInstance(getObject(extensionValue));
                        if (instance.getKeyIdentifier() != null) {
                            x509CertSelector.setSubjectKeyIdentifier(new DEROctetString(instance.getKeyIdentifier()).getDEREncoded());
                        }
                    }
                    catch (IOException ex5) {}
                }
                boolean value = false;
                x509Certificate2 = findNextCert(list, x509CertSelector, c);
                if (x509Certificate2 == null && list2 != null) {
                    value = true;
                    x509Certificate2 = findNextCert(list2, x509CertSelector, c);
                }
                if (x509Certificate2 == null) {
                    continue;
                }
                c.add(x509Certificate2);
                list3.add(new Boolean(value));
            }
        }
        if (n != 0) {
            if (x509Certificate3 != null && x509Certificate3.getSubjectX500Principal().equals(x509Certificate3.getIssuerX500Principal())) {
                c.add(x509Certificate3);
                list3.add(new Boolean(false));
            }
            else {
                final X509CertSelector x509CertSelector2 = new X509CertSelector();
                try {
                    x509CertSelector2.setSubject(x509Certificate2.getIssuerX500Principal().getEncoded());
                    x509CertSelector2.setIssuer(x509Certificate2.getIssuerX500Principal().getEncoded());
                }
                catch (IOException ex2) {
                    throw new IllegalStateException(ex2.toString());
                }
                boolean value2 = false;
                X509Certificate x509Certificate4 = findNextCert(list, x509CertSelector2, c);
                if (x509Certificate4 == null && list2 != null) {
                    value2 = true;
                    x509Certificate4 = findNextCert(list2, x509CertSelector2, c);
                }
                if (x509Certificate4 != null) {
                    try {
                        x509Certificate2.verify(x509Certificate4.getPublicKey(), "BC");
                        c.add(x509Certificate4);
                        list3.add(new Boolean(value2));
                    }
                    catch (GeneralSecurityException ex6) {}
                }
            }
        }
        return new Object[] { CertificateFactory.getInstance("X.509", "BC").generateCertPath(new ArrayList<Certificate>(c)), list3 };
    }
    
    public CertStore getCertsAndCRLs() {
        return this.certs;
    }
    
    public SignerInformationStore getSignerInformationStore() {
        return this.signers;
    }
    
    public ValidationResult getValidationResult(final SignerInformation signerInformation) throws SignedMailValidatorException {
        if (this.signers.getSigners(signerInformation.getSID()).isEmpty()) {
            throw new SignedMailValidatorException(new ErrorBundle("org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages", "SignedMailValidator.wrongSigner"));
        }
        return this.results.get(signerInformation);
    }
    
    static {
        DEFAULT_CERT_PATH_REVIEWER = PKIXCertPathReviewer.class;
        EXT_KEY_USAGE = X509Extensions.ExtendedKeyUsage.getId();
        SUBJECT_ALTERNATIVE_NAME = X509Extensions.SubjectAlternativeName.getId();
    }
    
    public class ValidationResult
    {
        private PKIXCertPathReviewer review;
        private List errors;
        private List notifications;
        private List userProvidedCerts;
        private boolean signVerified;
        
        ValidationResult(final PKIXCertPathReviewer review, final boolean signVerified, final List errors, final List notifications, final List userProvidedCerts) {
            this.review = review;
            this.errors = errors;
            this.notifications = notifications;
            this.signVerified = signVerified;
            this.userProvidedCerts = userProvidedCerts;
        }
        
        public List getErrors() {
            return this.errors;
        }
        
        public List getNotifications() {
            return this.notifications;
        }
        
        public PKIXCertPathReviewer getCertPathReview() {
            return this.review;
        }
        
        public CertPath getCertPath() {
            return (this.review != null) ? this.review.getCertPath() : null;
        }
        
        public List getUserProvidedCerts() {
            return this.userProvidedCerts;
        }
        
        public boolean isVerifiedSignature() {
            return this.signVerified;
        }
        
        public boolean isValidSignature() {
            return this.review != null && this.signVerified && this.review.isValidCertPath() && this.errors.isEmpty();
        }
    }
}
