// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.examples;

import javax.mail.Part;
import javax.mail.internet.MimeMultipart;
import java.io.InputStream;
import javax.mail.internet.MimeMessage;
import org.bouncycastle.mail.smime.util.SharedFileInputStream;
import javax.mail.Authenticator;
import javax.mail.Session;
import java.util.Iterator;
import java.security.cert.CertStore;
import java.security.cert.CertSelector;
import java.security.cert.X509Certificate;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.mail.smime.SMIMESignedParser;

public class ReadLargeSignedMail
{
    private static void verify(final SMIMESignedParser smimeSignedParser) throws Exception {
        final CertStore certificatesAndCRLs = smimeSignedParser.getCertificatesAndCRLs("Collection", "BC");
        for (final SignerInformation signerInformation : smimeSignedParser.getSignerInfos().getSigners()) {
            if (signerInformation.verify((X509Certificate)certificatesAndCRLs.getCertificates(signerInformation.getSID()).iterator().next(), "BC")) {
                System.out.println("signature verified");
            }
            else {
                System.out.println("signature failed!");
            }
        }
    }
    
    public static void main(final String[] array) throws Exception {
        final MimeMessage mimeMessage = new MimeMessage(Session.getDefaultInstance(System.getProperties(), (Authenticator)null), (InputStream)new SharedFileInputStream("signed.message"));
        if (mimeMessage.isMimeType("multipart/signed")) {
            final SMIMESignedParser smimeSignedParser = new SMIMESignedParser((MimeMultipart)mimeMessage.getContent());
            System.out.println("Status:");
            verify(smimeSignedParser);
        }
        else if (mimeMessage.isMimeType("application/pkcs7-mime")) {
            final SMIMESignedParser smimeSignedParser2 = new SMIMESignedParser((Part)mimeMessage);
            System.out.println("Status:");
            verify(smimeSignedParser2);
        }
        else {
            System.err.println("Not a signed message!");
        }
    }
}
