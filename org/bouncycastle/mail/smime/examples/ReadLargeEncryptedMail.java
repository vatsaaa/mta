// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.examples;

import javax.mail.internet.MimeBodyPart;
import org.bouncycastle.mail.smime.SMIMEUtil;
import org.bouncycastle.mail.smime.SMIMEEnvelopedParser;
import java.io.InputStream;
import javax.mail.internet.MimeMessage;
import org.bouncycastle.mail.smime.util.SharedFileInputStream;
import javax.mail.Authenticator;
import javax.mail.Session;
import org.bouncycastle.cms.RecipientId;
import java.security.cert.X509Certificate;
import java.security.KeyStore;

public class ReadLargeEncryptedMail
{
    public static void main(final String[] array) throws Exception {
        if (array.length != 3) {
            System.err.println("usage: ReadLargeEncryptedMail pkcs12Keystore password outputFile");
            System.exit(0);
        }
        final KeyStore instance = KeyStore.getInstance("PKCS12", "BC");
        final String keyAlias = ExampleUtils.findKeyAlias(instance, array[0], array[1].toCharArray());
        final X509Certificate x509Certificate = (X509Certificate)instance.getCertificate(keyAlias);
        final RecipientId recipientId = new RecipientId();
        recipientId.setSerialNumber(x509Certificate.getSerialNumber());
        recipientId.setIssuer(x509Certificate.getIssuerX500Principal().getEncoded());
        ExampleUtils.dumpContent(SMIMEUtil.toMimeBodyPart(new SMIMEEnvelopedParser(new MimeMessage(Session.getDefaultInstance(System.getProperties(), (Authenticator)null), (InputStream)new SharedFileInputStream("encrypted.message"))).getRecipientInfos().get(recipientId).getContentStream(instance.getKey(keyAlias, null), "BC")), array[2]);
    }
}
