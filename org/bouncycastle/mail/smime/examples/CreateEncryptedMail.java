// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.examples;

import java.security.cert.Certificate;
import java.util.Enumeration;
import java.io.OutputStream;
import java.io.FileOutputStream;
import javax.mail.Message;
import javax.mail.Address;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import java.security.cert.X509Certificate;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
import java.io.InputStream;
import java.io.FileInputStream;
import java.security.KeyStore;

public class CreateEncryptedMail
{
    public static void main(final String[] array) throws Exception {
        if (array.length != 2) {
            System.err.println("usage: CreateEncryptedMail pkcs12Keystore password");
            System.exit(0);
        }
        final KeyStore instance = KeyStore.getInstance("PKCS12", "BC");
        instance.load(new FileInputStream(array[0]), array[1].toCharArray());
        final Enumeration<String> aliases = instance.aliases();
        String alias = null;
        while (aliases.hasMoreElements()) {
            final String alias2 = aliases.nextElement();
            if (instance.isKeyEntry(alias2)) {
                alias = alias2;
            }
        }
        if (alias == null) {
            System.err.println("can't find a private key!");
            System.exit(0);
        }
        final Certificate[] certificateChain = instance.getCertificateChain(alias);
        final SMIMEEnvelopedGenerator smimeEnvelopedGenerator = new SMIMEEnvelopedGenerator();
        smimeEnvelopedGenerator.addKeyTransRecipient((X509Certificate)certificateChain[0]);
        final MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setText("Hello world!");
        final MimeBodyPart generate = smimeEnvelopedGenerator.generate(mimeBodyPart, SMIMEEnvelopedGenerator.RC2_CBC, "BC");
        final Session defaultInstance = Session.getDefaultInstance(System.getProperties(), (Authenticator)null);
        final InternetAddress from = new InternetAddress("\"Eric H. Echidna\"<eric@bouncycastle.org>");
        final InternetAddress internetAddress = new InternetAddress("example@bouncycastle.org");
        final MimeMessage mimeMessage = new MimeMessage(defaultInstance);
        mimeMessage.setFrom((Address)from);
        mimeMessage.setRecipient(Message.RecipientType.TO, (Address)internetAddress);
        mimeMessage.setSubject("example encrypted message");
        mimeMessage.setContent(generate.getContent(), generate.getContentType());
        mimeMessage.saveChanges();
        mimeMessage.writeTo((OutputStream)new FileOutputStream("encrypted.message"));
    }
}
