// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.examples;

import java.security.cert.Certificate;
import java.io.OutputStream;
import java.io.FileOutputStream;
import javax.mail.Message;
import javax.mail.Address;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.activation.DataSource;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import java.io.File;
import javax.mail.internet.MimeBodyPart;
import java.security.cert.X509Certificate;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
import java.security.KeyStore;

public class CreateLargeEncryptedMail
{
    public static void main(final String[] array) throws Exception {
        if (array.length != 3) {
            System.err.println("usage: CreateLargeEncryptedMail pkcs12Keystore password inputFile");
            System.exit(0);
        }
        final KeyStore instance = KeyStore.getInstance("PKCS12", "BC");
        final Certificate[] certificateChain = instance.getCertificateChain(ExampleUtils.findKeyAlias(instance, array[0], array[1].toCharArray()));
        final SMIMEEnvelopedGenerator smimeEnvelopedGenerator = new SMIMEEnvelopedGenerator();
        smimeEnvelopedGenerator.addKeyTransRecipient((X509Certificate)certificateChain[0]);
        final MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setDataHandler(new DataHandler((DataSource)new FileDataSource(new File(array[2]))));
        mimeBodyPart.setHeader("Content-Type", "application/octet-stream");
        mimeBodyPart.setHeader("Content-Transfer-Encoding", "binary");
        final MimeBodyPart generate = smimeEnvelopedGenerator.generate(mimeBodyPart, SMIMEEnvelopedGenerator.RC2_CBC, "BC");
        final Session defaultInstance = Session.getDefaultInstance(System.getProperties(), (Authenticator)null);
        final InternetAddress from = new InternetAddress("\"Eric H. Echidna\"<eric@bouncycastle.org>");
        final InternetAddress internetAddress = new InternetAddress("example@bouncycastle.org");
        final MimeMessage mimeMessage = new MimeMessage(defaultInstance);
        mimeMessage.setFrom((Address)from);
        mimeMessage.setRecipient(Message.RecipientType.TO, (Address)internetAddress);
        mimeMessage.setSubject("example encrypted message");
        mimeMessage.setContent(generate.getContent(), generate.getContentType());
        mimeMessage.saveChanges();
        mimeMessage.writeTo((OutputStream)new FileOutputStream("encrypted.message"));
    }
}
