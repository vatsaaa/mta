// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.examples;

import javax.mail.internet.MimeMultipart;
import java.io.OutputStream;
import java.io.FileOutputStream;
import javax.mail.Message;
import javax.mail.Address;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.mail.smime.SMIMESignedGenerator;
import org.bouncycastle.asn1.smime.SMIMEEncryptionKeyPreferenceAttribute;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.smime.SMIMECapabilitiesAttribute;
import org.bouncycastle.asn1.smime.SMIMECapability;
import org.bouncycastle.asn1.smime.SMIMECapabilityVector;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertStore;
import java.util.Collection;
import java.security.cert.CollectionCertStoreParameters;
import java.util.ArrayList;
import java.security.SecureRandom;
import java.security.KeyPairGenerator;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.x509.X509Extensions;
import java.util.Date;
import java.math.BigInteger;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.asn1.x509.X509Name;
import java.security.cert.X509Certificate;
import java.security.KeyPair;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import java.security.PublicKey;

public class CreateSignedMail
{
    static int serialNo;
    
    static AuthorityKeyIdentifier createAuthorityKeyId(final PublicKey publicKey) throws IOException {
        return new AuthorityKeyIdentifier(new SubjectPublicKeyInfo((ASN1Sequence)new ASN1InputStream(new ByteArrayInputStream(publicKey.getEncoded())).readObject()));
    }
    
    static SubjectKeyIdentifier createSubjectKeyId(final PublicKey publicKey) throws IOException {
        return new SubjectKeyIdentifier(new SubjectPublicKeyInfo((ASN1Sequence)new ASN1InputStream(new ByteArrayInputStream(publicKey.getEncoded())).readObject()));
    }
    
    static X509Certificate makeCertificate(final KeyPair keyPair, final String s, final KeyPair keyPair2, final String s2) throws GeneralSecurityException, IOException {
        final X509Name x509Name = new X509Name(s);
        final PublicKey public1 = keyPair.getPublic();
        final PrivateKey private1 = keyPair2.getPrivate();
        final PublicKey public2 = keyPair2.getPublic();
        final X509V3CertificateGenerator x509V3CertificateGenerator = new X509V3CertificateGenerator();
        x509V3CertificateGenerator.setSerialNumber(BigInteger.valueOf(CreateSignedMail.serialNo++));
        x509V3CertificateGenerator.setIssuerDN(new X509Name(s2));
        x509V3CertificateGenerator.setNotBefore(new Date(System.currentTimeMillis()));
        x509V3CertificateGenerator.setNotAfter(new Date(System.currentTimeMillis() + 8640000000L));
        x509V3CertificateGenerator.setSubjectDN(new X509Name(s));
        x509V3CertificateGenerator.setPublicKey(public1);
        x509V3CertificateGenerator.setSignatureAlgorithm("MD5WithRSAEncryption");
        x509V3CertificateGenerator.addExtension(X509Extensions.SubjectKeyIdentifier, false, createSubjectKeyId(public1));
        x509V3CertificateGenerator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, createAuthorityKeyId(public2));
        return x509V3CertificateGenerator.generateX509Certificate(private1);
    }
    
    public static void main(final String[] array) throws Exception {
        final KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "BC");
        instance.initialize(1024, new SecureRandom());
        final String s = "O=Bouncy Castle, C=AU";
        final KeyPair generateKeyPair = instance.generateKeyPair();
        final X509Certificate certificate = makeCertificate(generateKeyPair, s, generateKeyPair, s);
        final String s2 = "CN=Eric H. Echidna, E=eric@bouncycastle.org, O=Bouncy Castle, C=AU";
        final KeyPair generateKeyPair2 = instance.generateKeyPair();
        final X509Certificate certificate2 = makeCertificate(generateKeyPair2, s2, generateKeyPair, s);
        final ArrayList<X509Certificate> collection = new ArrayList<X509Certificate>();
        collection.add(certificate2);
        collection.add(certificate);
        final CertStore instance2 = CertStore.getInstance("Collection", new CollectionCertStoreParameters(collection), "BC");
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final SMIMECapabilityVector smimeCapabilityVector = new SMIMECapabilityVector();
        smimeCapabilityVector.addCapability(SMIMECapability.dES_EDE3_CBC);
        smimeCapabilityVector.addCapability(SMIMECapability.rC2_CBC, 128);
        smimeCapabilityVector.addCapability(SMIMECapability.dES_CBC);
        asn1EncodableVector.add(new SMIMECapabilitiesAttribute(smimeCapabilityVector));
        asn1EncodableVector.add(new SMIMEEncryptionKeyPreferenceAttribute(new IssuerAndSerialNumber(new X509Name(s), certificate2.getSerialNumber())));
        final SMIMESignedGenerator smimeSignedGenerator = new SMIMESignedGenerator();
        smimeSignedGenerator.addSigner(generateKeyPair2.getPrivate(), certificate2, SMIMESignedGenerator.DIGEST_SHA1, new AttributeTable(asn1EncodableVector), null);
        smimeSignedGenerator.addCertificatesAndCRLs(instance2);
        final MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setText("Hello world!");
        final MimeMultipart generate = smimeSignedGenerator.generate(mimeBodyPart, "BC");
        final Session defaultInstance = Session.getDefaultInstance(System.getProperties(), (Authenticator)null);
        final InternetAddress from = new InternetAddress("\"Eric H. Echidna\"<eric@bouncycastle.org>");
        final InternetAddress internetAddress = new InternetAddress("example@bouncycastle.org");
        final MimeMessage mimeMessage = new MimeMessage(defaultInstance);
        mimeMessage.setFrom((Address)from);
        mimeMessage.setRecipient(Message.RecipientType.TO, (Address)internetAddress);
        mimeMessage.setSubject("example signed message");
        mimeMessage.setContent((Object)generate, generate.getContentType());
        mimeMessage.saveChanges();
        mimeMessage.writeTo((OutputStream)new FileOutputStream("signed.message"));
    }
    
    static {
        CreateSignedMail.serialNo = 1;
    }
}
