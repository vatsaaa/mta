// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.mail.smime.examples;

import javax.mail.internet.MimeBodyPart;
import java.util.Enumeration;
import org.bouncycastle.mail.smime.SMIMEUtil;
import org.bouncycastle.mail.smime.SMIMEEnveloped;
import javax.mail.internet.MimeMessage;
import javax.mail.Authenticator;
import javax.mail.Session;
import org.bouncycastle.cms.RecipientId;
import java.security.cert.X509Certificate;
import java.io.InputStream;
import java.io.FileInputStream;
import java.security.KeyStore;

public class ReadEncryptedMail
{
    public static void main(final String[] array) throws Exception {
        if (array.length != 2) {
            System.err.println("usage: ReadEncryptedMail pkcs12Keystore password");
            System.exit(0);
        }
        final KeyStore instance = KeyStore.getInstance("PKCS12", "BC");
        instance.load(new FileInputStream(array[0]), array[1].toCharArray());
        final Enumeration<String> aliases = instance.aliases();
        String s = null;
        while (aliases.hasMoreElements()) {
            final String alias = aliases.nextElement();
            if (instance.isKeyEntry(alias)) {
                s = alias;
            }
        }
        if (s == null) {
            System.err.println("can't find a private key!");
            System.exit(0);
        }
        final X509Certificate x509Certificate = (X509Certificate)instance.getCertificate(s);
        final RecipientId recipientId = new RecipientId();
        recipientId.setSerialNumber(x509Certificate.getSerialNumber());
        recipientId.setIssuer(x509Certificate.getIssuerX500Principal().getEncoded());
        final MimeBodyPart mimeBodyPart = SMIMEUtil.toMimeBodyPart(new SMIMEEnveloped(new MimeMessage(Session.getDefaultInstance(System.getProperties(), (Authenticator)null), (InputStream)new FileInputStream("encrypted.message"))).getRecipientInfos().get(recipientId).getContent(instance.getKey(s, null), "BC"));
        System.out.println("Message Contents");
        System.out.println("----------------");
        System.out.println(mimeBodyPart.getContent());
    }
}
