// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;

public class DerSequence extends DerObject implements Asn1Sequence
{
    private Asn1InputStream _aIn;
    
    DerSequence(final int n, final byte[] array) {
        super(n, 16, array);
        this._aIn = new Asn1InputStream(array);
    }
    
    public Asn1Object readObject() throws IOException {
        return this._aIn.readObject();
    }
}
