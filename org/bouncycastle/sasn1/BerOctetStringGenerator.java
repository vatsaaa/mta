// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import org.bouncycastle.asn1.DEROctetString;
import java.io.IOException;
import java.io.OutputStream;

public class BerOctetStringGenerator extends BerGenerator
{
    public BerOctetStringGenerator(final OutputStream outputStream) throws IOException {
        super(outputStream);
        this.writeBerHeader(36);
    }
    
    public BerOctetStringGenerator(final OutputStream outputStream, final int n, final boolean b) throws IOException {
        super(outputStream, n, b);
        this.writeBerHeader(36);
    }
    
    public OutputStream getOctetOutputStream() {
        return new BerOctetStream();
    }
    
    public OutputStream getOctetOutputStream(final byte[] array) {
        return new BufferedBerOctetStream(array);
    }
    
    private class BerOctetStream extends OutputStream
    {
        private byte[] _buf;
        
        private BerOctetStream() {
            this._buf = new byte[1];
        }
        
        @Override
        public void write(final int n) throws IOException {
            this._buf[0] = (byte)n;
            BerOctetStringGenerator.this._out.write(new DEROctetString(this._buf).getEncoded());
        }
        
        @Override
        public void write(final byte[] array) throws IOException {
            BerOctetStringGenerator.this._out.write(new DEROctetString(array).getEncoded());
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
            final byte[] array2 = new byte[n2];
            System.arraycopy(array, n, array2, 0, n2);
            BerOctetStringGenerator.this._out.write(new DEROctetString(array2).getEncoded());
        }
        
        @Override
        public void close() throws IOException {
            BerOctetStringGenerator.this.writeBerEnd();
        }
    }
    
    private class BufferedBerOctetStream extends OutputStream
    {
        private byte[] _buf;
        private int _off;
        
        BufferedBerOctetStream(final byte[] buf) {
            this._buf = buf;
            this._off = 0;
        }
        
        @Override
        public void write(final int n) throws IOException {
            this._buf[this._off++] = (byte)n;
            if (this._off == this._buf.length) {
                BerOctetStringGenerator.this._out.write(new DEROctetString(this._buf).getEncoded());
                this._off = 0;
            }
        }
        
        @Override
        public void close() throws IOException {
            if (this._off != 0) {
                final byte[] array = new byte[this._off];
                System.arraycopy(this._buf, 0, array, 0, this._off);
                BerOctetStringGenerator.this._out.write(new DEROctetString(array).getEncoded());
            }
            BerOctetStringGenerator.this.writeBerEnd();
        }
    }
}
