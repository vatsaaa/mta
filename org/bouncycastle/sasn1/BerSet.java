// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

public class BerSet extends Asn1Object implements Asn1Set
{
    private Asn1InputStream _aIn;
    
    protected BerSet(final int n, final InputStream inputStream) {
        super(n, 17, inputStream);
        this._aIn = new Asn1InputStream(inputStream);
    }
    
    public Asn1Object readObject() throws IOException {
        return this._aIn.readObject();
    }
}
