// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.InputStream;

public class DerOctetString extends DerObject implements Asn1OctetString
{
    protected DerOctetString(final int n, final byte[] array) {
        super(n, 4, array);
    }
    
    public InputStream getOctetStream() {
        if (this.isConstructed()) {
            return new ConstructedOctetStream(this.getRawContentStream());
        }
        return this.getRawContentStream();
    }
}
