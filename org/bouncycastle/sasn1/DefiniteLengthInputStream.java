// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

class DefiniteLengthInputStream extends LimitedInputStream
{
    private int _length;
    
    DefiniteLengthInputStream(final InputStream inputStream, final int length) {
        super(inputStream);
        this._length = length;
    }
    
    @Override
    public int read() throws IOException {
        if (this._length-- > 0) {
            return this._in.read();
        }
        this.setParentEofDetect(true);
        return -1;
    }
}
