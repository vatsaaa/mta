// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;

public class DerSequenceGenerator extends DerGenerator
{
    private final ByteArrayOutputStream _bOut;
    
    public DerSequenceGenerator(final OutputStream outputStream) throws IOException {
        super(outputStream);
        this._bOut = new ByteArrayOutputStream();
    }
    
    public DerSequenceGenerator(final OutputStream outputStream, final int n, final boolean b) throws IOException {
        super(outputStream, n, b);
        this._bOut = new ByteArrayOutputStream();
    }
    
    public void addObject(final DerObject derObject) throws IOException {
        this._bOut.write(derObject.getEncoded());
    }
    
    @Override
    public OutputStream getRawOutputStream() {
        return this._bOut;
    }
    
    public void close() throws IOException {
        this.writeDerEncoded(48, this._bOut.toByteArray());
    }
}
