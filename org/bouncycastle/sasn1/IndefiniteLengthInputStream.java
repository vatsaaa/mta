// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

class IndefiniteLengthInputStream extends LimitedInputStream
{
    private int _b1;
    private int _b2;
    private boolean _eofReached;
    private boolean _eofOn00;
    
    IndefiniteLengthInputStream(final InputStream inputStream) throws IOException {
        super(inputStream);
        this._eofReached = false;
        this._eofOn00 = true;
        this._b1 = inputStream.read();
        this._b2 = inputStream.read();
        this._eofReached = (this._b2 < 0);
    }
    
    void setEofOn00(final boolean eofOn00) {
        this._eofOn00 = eofOn00;
    }
    
    void checkForEof() throws IOException {
        if (this._eofOn00 && this._b1 == 0 && this._b2 == 0) {
            this.setParentEofDetect(this._eofReached = true);
        }
    }
    
    @Override
    public int read() throws IOException {
        this.checkForEof();
        if (this._eofReached) {
            return -1;
        }
        final int read = this._in.read();
        if (read < 0) {
            this._eofReached = true;
            return -1;
        }
        final int b1 = this._b1;
        this._b1 = this._b2;
        this._b2 = read;
        return b1;
    }
}
