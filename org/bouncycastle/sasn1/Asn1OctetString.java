// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.InputStream;

public interface Asn1OctetString
{
    InputStream getOctetStream();
}
