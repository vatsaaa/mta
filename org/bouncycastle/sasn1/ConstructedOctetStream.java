// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

class ConstructedOctetStream extends InputStream
{
    private final Asn1InputStream _aIn;
    private boolean _first;
    private InputStream _currentStream;
    
    ConstructedOctetStream(final InputStream inputStream) {
        this._first = true;
        this._aIn = new Asn1InputStream(inputStream);
    }
    
    @Override
    public int read() throws IOException {
        if (this._first) {
            final Asn1OctetString asn1OctetString = (Asn1OctetString)this._aIn.readObject();
            if (asn1OctetString == null) {
                return -1;
            }
            this._first = false;
            this._currentStream = asn1OctetString.getOctetStream();
        }
        else if (this._currentStream == null) {
            return -1;
        }
        final int read = this._currentStream.read();
        if (read >= 0) {
            return read;
        }
        final Asn1OctetString asn1OctetString2 = (Asn1OctetString)this._aIn.readObject();
        if (asn1OctetString2 == null) {
            this._currentStream = null;
            return -1;
        }
        this._currentStream = asn1OctetString2.getOctetStream();
        return this._currentStream.read();
    }
}
