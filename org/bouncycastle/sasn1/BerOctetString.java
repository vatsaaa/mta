// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.InputStream;

public class BerOctetString extends Asn1Object implements Asn1OctetString
{
    protected BerOctetString(final int n, final InputStream inputStream) {
        super(n, 4, inputStream);
    }
    
    public InputStream getOctetStream() {
        if (this.isConstructed()) {
            return new ConstructedOctetStream(this.getRawContentStream());
        }
        return this.getRawContentStream();
    }
}
