// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1.cms;

import org.bouncycastle.sasn1.Asn1TaggedObject;
import org.bouncycastle.sasn1.Asn1Set;
import java.io.IOException;
import org.bouncycastle.sasn1.Asn1Object;
import org.bouncycastle.sasn1.Asn1Integer;
import org.bouncycastle.sasn1.Asn1Sequence;

public class SignedDataParser
{
    private Asn1Sequence _seq;
    private Asn1Integer _version;
    private Asn1Object _nextObject;
    private boolean _certsCalled;
    private boolean _crlsCalled;
    
    public SignedDataParser(final Asn1Sequence seq) throws IOException {
        this._seq = seq;
        this._version = (Asn1Integer)seq.readObject();
    }
    
    public Asn1Integer getVersion() {
        return this._version;
    }
    
    public Asn1Set getDigestAlgorithms() throws IOException {
        return (Asn1Set)this._seq.readObject();
    }
    
    public ContentInfoParser getEncapContentInfo() throws IOException {
        return new ContentInfoParser((Asn1Sequence)this._seq.readObject());
    }
    
    public Asn1Set getCertificates() throws IOException {
        this._certsCalled = true;
        this._nextObject = this._seq.readObject();
        if (this._nextObject instanceof Asn1TaggedObject && ((Asn1TaggedObject)this._nextObject).getTagNumber() == 0) {
            final Asn1Set set = (Asn1Set)((Asn1TaggedObject)this._nextObject).getObject(17, false);
            this._nextObject = null;
            return set;
        }
        return null;
    }
    
    public Asn1Set getCrls() throws IOException {
        if (!this._certsCalled) {
            throw new IOException("getCerts() has not been called.");
        }
        this._crlsCalled = true;
        if (this._nextObject == null) {
            this._nextObject = this._seq.readObject();
        }
        if (this._nextObject instanceof Asn1TaggedObject && ((Asn1TaggedObject)this._nextObject).getTagNumber() == 1) {
            final Asn1Set set = (Asn1Set)((Asn1TaggedObject)this._nextObject).getObject(17, false);
            this._nextObject = null;
            return set;
        }
        return null;
    }
    
    public Asn1Set getSignerInfos() throws IOException {
        if (!this._certsCalled || !this._crlsCalled) {
            throw new IOException("getCerts() and/or getCrls() has not been called.");
        }
        if (this._nextObject == null) {
            this._nextObject = this._seq.readObject();
        }
        return (Asn1Set)this._nextObject;
    }
}
