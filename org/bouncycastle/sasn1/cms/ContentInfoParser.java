// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1.cms;

import org.bouncycastle.sasn1.Asn1Object;
import java.io.IOException;
import org.bouncycastle.sasn1.Asn1Sequence;
import org.bouncycastle.sasn1.Asn1TaggedObject;
import org.bouncycastle.sasn1.Asn1ObjectIdentifier;

public class ContentInfoParser
{
    private Asn1ObjectIdentifier contentType;
    private Asn1TaggedObject content;
    
    public ContentInfoParser(final Asn1Sequence asn1Sequence) throws IOException {
        this.contentType = (Asn1ObjectIdentifier)asn1Sequence.readObject();
        this.content = (Asn1TaggedObject)asn1Sequence.readObject();
    }
    
    public Asn1ObjectIdentifier getContentType() {
        return this.contentType;
    }
    
    public Asn1Object getContent(final int n) throws IOException {
        if (this.content != null) {
            return this.content.getObject(n, true);
        }
        return null;
    }
}
