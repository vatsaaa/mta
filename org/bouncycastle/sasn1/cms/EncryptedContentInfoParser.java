// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1.cms;

import org.bouncycastle.sasn1.Asn1Object;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.sasn1.DerSequence;
import org.bouncycastle.sasn1.Asn1Sequence;
import org.bouncycastle.sasn1.Asn1TaggedObject;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.sasn1.Asn1ObjectIdentifier;

public class EncryptedContentInfoParser
{
    private Asn1ObjectIdentifier _contentType;
    private AlgorithmIdentifier _contentEncryptionAlgorithm;
    private Asn1TaggedObject _encryptedContent;
    
    public EncryptedContentInfoParser(final Asn1Sequence asn1Sequence) throws IOException {
        this._contentType = (Asn1ObjectIdentifier)asn1Sequence.readObject();
        this._contentEncryptionAlgorithm = AlgorithmIdentifier.getInstance(new ASN1InputStream(((DerSequence)asn1Sequence.readObject()).getEncoded()).readObject());
        this._encryptedContent = (Asn1TaggedObject)asn1Sequence.readObject();
    }
    
    public Asn1ObjectIdentifier getContentType() {
        return this._contentType;
    }
    
    public AlgorithmIdentifier getContentEncryptionAlgorithm() {
        return this._contentEncryptionAlgorithm;
    }
    
    public Asn1Object getEncryptedContent(final int n) throws IOException {
        return this._encryptedContent.getObject(n, false);
    }
}
