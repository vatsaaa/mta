// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.OutputStream;

public class BerSequenceGenerator extends BerGenerator
{
    public BerSequenceGenerator(final OutputStream outputStream) throws IOException {
        super(outputStream);
        this.writeBerHeader(48);
    }
    
    public BerSequenceGenerator(final OutputStream outputStream, final int n, final boolean b) throws IOException {
        super(outputStream, n, b);
        this.writeBerHeader(48);
    }
    
    public void addObject(final DerObject derObject) throws IOException {
        this._out.write(derObject.getEncoded());
    }
    
    public void close() throws IOException {
        this.writeBerEnd();
    }
}
