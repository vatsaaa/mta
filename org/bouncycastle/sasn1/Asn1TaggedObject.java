// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

public class Asn1TaggedObject extends Asn1Object
{
    protected Asn1TaggedObject(final int n, final int n2, final InputStream inputStream) {
        super(n, n2, inputStream);
    }
    
    public Asn1Object getObject(final int n, final boolean b) throws IOException {
        if (b) {
            return new Asn1InputStream(this.getRawContentStream()).readObject();
        }
        switch (n) {
            case 17: {
                if (this.getRawContentStream() instanceof IndefiniteLengthInputStream) {
                    return new BerSet(32, this.getRawContentStream());
                }
                return new DerSet(32, ((DefiniteLengthInputStream)this.getRawContentStream()).toByteArray());
            }
            case 16: {
                if (this.getRawContentStream() instanceof IndefiniteLengthInputStream) {
                    return new BerSequence(32, this.getRawContentStream());
                }
                return new DerSequence(32, ((DefiniteLengthInputStream)this.getRawContentStream()).toByteArray());
            }
            case 4: {
                if (this.getRawContentStream() instanceof IndefiniteLengthInputStream) {
                    return new BerOctetString(32, this.getRawContentStream());
                }
                if (this.isConstructed()) {
                    return new DerOctetString(32, ((DefiniteLengthInputStream)this.getRawContentStream()).toByteArray());
                }
                return new DerOctetString(0, ((DefiniteLengthInputStream)this.getRawContentStream()).toByteArray());
            }
            default: {
                throw new RuntimeException("implicit tagging not implemented");
            }
        }
    }
}
