// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

abstract class LimitedInputStream extends InputStream
{
    protected final InputStream _in;
    
    LimitedInputStream(final InputStream in) {
        this._in = in;
    }
    
    byte[] toByteArray() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int read;
        while ((read = this.read()) >= 0) {
            byteArrayOutputStream.write(read);
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    InputStream getUnderlyingStream() {
        return this._in;
    }
    
    protected void setParentEofDetect(final boolean eofOn00) {
        if (this._in instanceof IndefiniteLengthInputStream) {
            ((IndefiniteLengthInputStream)this._in).setEofOn00(eofOn00);
        }
    }
}
