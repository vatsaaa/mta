// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.OutputStream;

public abstract class Asn1Generator
{
    protected OutputStream _out;
    
    public Asn1Generator(final OutputStream out) {
        this._out = out;
    }
    
    public abstract OutputStream getRawOutputStream();
}
