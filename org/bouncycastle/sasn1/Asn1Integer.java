// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.math.BigInteger;

public class Asn1Integer extends DerObject
{
    private BigInteger _value;
    
    protected Asn1Integer(final int n, final byte[] val) throws IOException {
        super(n, 2, val);
        this._value = new BigInteger(val);
    }
    
    public Asn1Integer(final long val) {
        this(BigInteger.valueOf(val));
    }
    
    public Asn1Integer(final BigInteger value) {
        super(0, 2, value.toByteArray());
        this._value = value;
    }
    
    public BigInteger getValue() {
        return this._value;
    }
}
