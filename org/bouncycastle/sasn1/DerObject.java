// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class DerObject extends Asn1Object
{
    private byte[] _content;
    
    DerObject(final int n, final int n2, final byte[] content) {
        super(n, n2, null);
        this._content = content;
    }
    
    @Override
    public int getTagNumber() {
        return this._tagNumber;
    }
    
    @Override
    public InputStream getRawContentStream() {
        return new ByteArrayInputStream(this._content);
    }
    
    public byte[] getEncoded() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.encode(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    void encode(final OutputStream outputStream) throws IOException {
        new BasicDerGenerator(outputStream).writeDerEncoded(this._baseTag | this._tagNumber, this._content);
    }
    
    private class BasicDerGenerator extends DerGenerator
    {
        protected BasicDerGenerator(final OutputStream outputStream) {
            super(outputStream);
        }
        
        @Override
        public OutputStream getRawOutputStream() {
            return this._out;
        }
    }
}
