// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.io.ByteArrayInputStream;

public class Asn1ObjectIdentifier extends DerObject
{
    private String _oid;
    
    Asn1ObjectIdentifier(final int n, final byte[] buf) throws IOException {
        super(n, 6, buf);
        final StringBuffer sb = new StringBuffer();
        long n2 = 0L;
        int n3 = 1;
        BigInteger obj = null;
        int read;
        while ((read = new ByteArrayInputStream(buf).read()) >= 0) {
            if (n2 < 36028797018963968L) {
                n2 = n2 * 128L + (read & 0x7F);
                if ((read & 0x80) != 0x0) {
                    continue;
                }
                if (n3 != 0) {
                    switch ((int)n2 / 40) {
                        case 0: {
                            sb.append('0');
                            break;
                        }
                        case 1: {
                            sb.append('1');
                            n2 -= 40L;
                            break;
                        }
                        default: {
                            sb.append('2');
                            n2 -= 80L;
                            break;
                        }
                    }
                    n3 = 0;
                }
                sb.append('.');
                sb.append(n2);
                n2 = 0L;
            }
            else {
                if (obj == null) {
                    obj = BigInteger.valueOf(n2);
                }
                obj = obj.shiftLeft(7).or(BigInteger.valueOf(read & 0x7F));
                if ((read & 0x80) != 0x0) {
                    continue;
                }
                sb.append('.');
                sb.append(obj);
                obj = null;
                n2 = 0L;
            }
        }
        this._oid = sb.toString();
    }
    
    public Asn1ObjectIdentifier(final String oid) throws IllegalArgumentException {
        super(0, 6, toByteArray(oid));
        this._oid = oid;
    }
    
    @Override
    public String toString() {
        return this._oid;
    }
    
    @Override
    public int hashCode() {
        return this._oid.hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Asn1ObjectIdentifier && this._oid.equals(((Asn1ObjectIdentifier)o)._oid);
    }
    
    private static void writeField(final OutputStream outputStream, final long n) throws IOException {
        if (n >= 128L) {
            if (n >= 16384L) {
                if (n >= 2097152L) {
                    if (n >= 268435456L) {
                        if (n >= 34359738368L) {
                            if (n >= 4398046511104L) {
                                if (n >= 562949953421312L) {
                                    if (n >= 72057594037927936L) {
                                        outputStream.write((int)(n >> 56) | 0x80);
                                    }
                                    outputStream.write((int)(n >> 49) | 0x80);
                                }
                                outputStream.write((int)(n >> 42) | 0x80);
                            }
                            outputStream.write((int)(n >> 35) | 0x80);
                        }
                        outputStream.write((int)(n >> 28) | 0x80);
                    }
                    outputStream.write((int)(n >> 21) | 0x80);
                }
                outputStream.write((int)(n >> 14) | 0x80);
            }
            outputStream.write((int)(n >> 7) | 0x80);
        }
        outputStream.write((int)n & 0x7F);
    }
    
    private static void writeField(final OutputStream outputStream, final BigInteger bigInteger) throws IOException {
        final int n = (bigInteger.bitLength() + 6) / 7;
        if (n == 0) {
            outputStream.write(0);
        }
        else {
            BigInteger shiftRight = bigInteger;
            final byte[] b = new byte[n];
            for (int i = n - 1; i >= 0; --i) {
                b[i] = (byte)((shiftRight.intValue() & 0x7F) | 0x80);
                shiftRight = shiftRight.shiftRight(7);
            }
            final byte[] array = b;
            final int n2 = n - 1;
            array[n2] &= 0x7F;
            outputStream.write(b);
        }
    }
    
    private static byte[] toByteArray(final String s) throws IllegalArgumentException {
        final OIDTokenizer oidTokenizer = new OIDTokenizer(s);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            writeField(byteArrayOutputStream, Integer.parseInt(oidTokenizer.nextToken()) * 40 + Integer.parseInt(oidTokenizer.nextToken()));
            while (oidTokenizer.hasMoreTokens()) {
                final String nextToken = oidTokenizer.nextToken();
                if (nextToken.length() < 18) {
                    writeField(byteArrayOutputStream, Long.parseLong(nextToken));
                }
                else {
                    writeField(byteArrayOutputStream, new BigInteger(nextToken));
                }
            }
        }
        catch (NumberFormatException ex) {
            throw new IllegalArgumentException("exception parsing field value: " + ex.getMessage());
        }
        catch (IOException ex2) {
            throw new IllegalArgumentException("exception converting to bytes: " + ex2.getMessage());
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    private static class OIDTokenizer
    {
        private String oid;
        private int index;
        
        public OIDTokenizer(final String oid) {
            this.oid = oid;
            this.index = 0;
        }
        
        public boolean hasMoreTokens() {
            return this.index != -1;
        }
        
        public String nextToken() {
            if (this.index == -1) {
                return null;
            }
            final int index = this.oid.indexOf(46, this.index);
            if (index == -1) {
                final String substring = this.oid.substring(this.index);
                this.index = -1;
                return substring;
            }
            final String substring2 = this.oid.substring(this.index, index);
            this.index = index + 1;
            return substring2;
        }
    }
}
