// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.InputStream;

public abstract class Asn1Object
{
    protected int _baseTag;
    protected int _tagNumber;
    protected InputStream _contentStream;
    
    protected Asn1Object(final int baseTag, final int tagNumber, final InputStream contentStream) {
        this._baseTag = baseTag;
        this._tagNumber = tagNumber;
        this._contentStream = contentStream;
    }
    
    public boolean isConstructed() {
        return (this._baseTag & 0x20) != 0x0;
    }
    
    public int getTagNumber() {
        return this._tagNumber;
    }
    
    public InputStream getRawContentStream() {
        return this._contentStream;
    }
}
