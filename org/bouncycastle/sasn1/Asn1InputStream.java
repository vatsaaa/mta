// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.sasn1;

import java.io.EOFException;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Asn1InputStream
{
    InputStream _in;
    private int _limit;
    private boolean _eofFound;
    
    public Asn1InputStream(final InputStream in) {
        this._in = in;
        this._limit = Integer.MAX_VALUE;
    }
    
    public Asn1InputStream(final InputStream in, final int limit) {
        this._in = in;
        this._limit = limit;
    }
    
    public Asn1InputStream(final byte[] buf) {
        this._in = new ByteArrayInputStream(buf);
        this._limit = buf.length;
    }
    
    InputStream getParentStream() {
        return this._in;
    }
    
    private int readLength() throws IOException {
        int read = this._in.read();
        if (read < 0) {
            throw new IOException("EOF found when length expected");
        }
        if (read == 128) {
            return -1;
        }
        if (read > 127) {
            final int n = read & 0x7F;
            if (n > 4) {
                throw new IOException("DER length more than 4 bytes");
            }
            read = 0;
            for (int i = 0; i < n; ++i) {
                final int read2 = this._in.read();
                if (read2 < 0) {
                    throw new IOException("EOF found reading length");
                }
                read = (read << 8) + read2;
            }
            if (read < 0) {
                throw new IOException("corrupted stream - negative length found");
            }
            if (read >= this._limit) {
                throw new IOException("corrupted stream - out of bounds length found");
            }
        }
        return read;
    }
    
    public Asn1Object readObject() throws IOException {
        final int read = this._in.read();
        if (read == -1) {
            if (this._eofFound) {
                throw new EOFException("attempt to read past end of file.");
            }
            this._eofFound = true;
            return null;
        }
        else {
            if (this._in instanceof IndefiniteLengthInputStream) {
                ((IndefiniteLengthInputStream)this._in).setEofOn00(false);
            }
            int n2;
            final int n = n2 = (read & 0xFFFFFFDF);
            if ((read & 0x80) != 0x0) {
                n2 = (read & 0x1F);
                if (n2 == 31) {
                    int n3 = 0;
                    int n4;
                    for (n4 = this._in.read(); n4 >= 0 && (n4 & 0x80) != 0x0; n4 = this._in.read()) {
                        n3 = (n3 | (n4 & 0x7F)) << 7;
                    }
                    if (n4 < 0) {
                        this._eofFound = true;
                        throw new EOFException("EOF encountered inside tag value.");
                    }
                    n2 = (n3 | (n4 & 0x7F));
                }
            }
            final int length = this.readLength();
            if (length < 0) {
                final IndefiniteLengthInputStream indefiniteLengthInputStream = new IndefiniteLengthInputStream(this._in);
                switch (n) {
                    case 5: {
                        return new Asn1Null(read);
                    }
                    case 4: {
                        return new BerOctetString(read, indefiniteLengthInputStream);
                    }
                    case 16: {
                        return new BerSequence(read, indefiniteLengthInputStream);
                    }
                    case 17: {
                        return new BerSet(read, indefiniteLengthInputStream);
                    }
                    default: {
                        return new Asn1TaggedObject(read, n2, indefiniteLengthInputStream);
                    }
                }
            }
            else {
                final DefiniteLengthInputStream definiteLengthInputStream = new DefiniteLengthInputStream(this._in, length);
                switch (n) {
                    case 2: {
                        return new Asn1Integer(read, definiteLengthInputStream.toByteArray());
                    }
                    case 5: {
                        return new Asn1Null(read);
                    }
                    case 6: {
                        return new Asn1ObjectIdentifier(read, definiteLengthInputStream.toByteArray());
                    }
                    case 4: {
                        return new DerOctetString(read, definiteLengthInputStream.toByteArray());
                    }
                    case 16: {
                        return new DerSequence(read, definiteLengthInputStream.toByteArray());
                    }
                    case 17: {
                        return new DerSet(read, definiteLengthInputStream.toByteArray());
                    }
                    default: {
                        return new Asn1TaggedObject(read, n2, definiteLengthInputStream);
                    }
                }
            }
        }
    }
}
