// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.Iterator;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.Time;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import java.security.GeneralSecurityException;
import java.security.Key;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.ASN1Null;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.DERObject;
import java.security.MessageDigest;
import java.security.Signature;
import java.security.SignatureException;
import java.security.InvalidKeyException;
import org.bouncycastle.util.Arrays;
import java.io.OutputStream;
import java.security.PublicKey;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Provider;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.CMSAttributes;
import java.util.Collection;
import java.util.ArrayList;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.SignerInfo;

public class SignerInformation
{
    private SignerId sid;
    private SignerInfo info;
    private AlgorithmIdentifier digestAlgorithm;
    private AlgorithmIdentifier encryptionAlgorithm;
    private final ASN1Set signedAttributeSet;
    private final ASN1Set unsignedAttributeSet;
    private CMSProcessable content;
    private byte[] signature;
    private DERObjectIdentifier contentType;
    private DigestCalculator digestCalculator;
    private byte[] resultDigest;
    private AttributeTable signedAttributeValues;
    private AttributeTable unsignedAttributeValues;
    
    SignerInformation(final SignerInfo info, final DERObjectIdentifier contentType, final CMSProcessable content, final DigestCalculator digestCalculator) {
        this.info = info;
        this.sid = new SignerId();
        this.contentType = contentType;
        try {
            final SignerIdentifier sid = info.getSID();
            if (sid.isTagged()) {
                this.sid.setSubjectKeyIdentifier(ASN1OctetString.getInstance(sid.getId()).getEncoded());
            }
            else {
                final IssuerAndSerialNumber instance = IssuerAndSerialNumber.getInstance(sid.getId());
                this.sid.setIssuer(instance.getName().getEncoded());
                this.sid.setSerialNumber(instance.getSerialNumber().getValue());
            }
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("invalid sid in SignerInfo");
        }
        this.digestAlgorithm = info.getDigestAlgorithm();
        this.signedAttributeSet = info.getAuthenticatedAttributes();
        this.unsignedAttributeSet = info.getUnauthenticatedAttributes();
        this.encryptionAlgorithm = info.getDigestEncryptionAlgorithm();
        this.signature = info.getEncryptedDigest().getOctets();
        this.content = content;
        this.digestCalculator = digestCalculator;
    }
    
    private byte[] encodeObj(final DEREncodable derEncodable) throws IOException {
        if (derEncodable != null) {
            return derEncodable.getDERObject().getEncoded();
        }
        return null;
    }
    
    public SignerId getSID() {
        return this.sid;
    }
    
    public int getVersion() {
        return this.info.getVersion().getValue().intValue();
    }
    
    public AlgorithmIdentifier getDigestAlgorithmID() {
        return this.digestAlgorithm;
    }
    
    public String getDigestAlgOID() {
        return this.digestAlgorithm.getObjectId().getId();
    }
    
    public byte[] getDigestAlgParams() {
        try {
            return this.encodeObj(this.digestAlgorithm.getParameters());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting digest parameters " + obj);
        }
    }
    
    public byte[] getContentDigest() {
        if (this.resultDigest == null) {
            throw new IllegalStateException("method can only be called after verify.");
        }
        return this.resultDigest.clone();
    }
    
    public String getEncryptionAlgOID() {
        return this.encryptionAlgorithm.getObjectId().getId();
    }
    
    public byte[] getEncryptionAlgParams() {
        try {
            return this.encodeObj(this.encryptionAlgorithm.getParameters());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AttributeTable getSignedAttributes() {
        if (this.signedAttributeSet != null && this.signedAttributeValues == null) {
            this.signedAttributeValues = new AttributeTable(this.signedAttributeSet);
        }
        return this.signedAttributeValues;
    }
    
    public AttributeTable getUnsignedAttributes() {
        if (this.unsignedAttributeSet != null && this.unsignedAttributeValues == null) {
            this.unsignedAttributeValues = new AttributeTable(this.unsignedAttributeSet);
        }
        return this.unsignedAttributeValues;
    }
    
    public byte[] getSignature() {
        return this.signature.clone();
    }
    
    public SignerInformationStore getCounterSignatures() {
        final AttributeTable unsignedAttributes = this.getUnsignedAttributes();
        if (unsignedAttributes == null) {
            return new SignerInformationStore(new ArrayList(0));
        }
        final ArrayList<SignerInformation> list = new ArrayList<SignerInformation>();
        final ASN1EncodableVector all = unsignedAttributes.getAll(CMSAttributes.counterSignature);
        for (int i = 0; i < all.size(); ++i) {
            final ASN1Set attrValues = ((Attribute)all.get(i)).getAttrValues();
            if (attrValues.size() < 1) {}
            final Enumeration objects = attrValues.getObjects();
            while (objects.hasMoreElements()) {
                final SignerInfo instance = SignerInfo.getInstance(objects.nextElement());
                list.add(new SignerInformation(instance, CMSAttributes.counterSignature, null, new CounterSignatureDigestCalculator(CMSSignedHelper.INSTANCE.getDigestAlgName(instance.getDigestAlgorithm().getObjectId().getId()), null, this.getSignature())));
            }
        }
        return new SignerInformationStore(list);
    }
    
    public byte[] getEncodedSignedAttributes() throws IOException {
        if (this.signedAttributeSet != null) {
            return this.signedAttributeSet.getEncoded("DER");
        }
        return null;
    }
    
    private boolean doVerify(final PublicKey publicKey, final Provider provider) throws CMSException, NoSuchAlgorithmException {
        final String digestAlgName = CMSSignedHelper.INSTANCE.getDigestAlgName(this.getDigestAlgOID());
        final Signature signatureInstance = CMSSignedHelper.INSTANCE.getSignatureInstance(digestAlgName + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(this.getEncryptionAlgOID()), provider);
        final MessageDigest digestInstance = CMSSignedHelper.INSTANCE.getDigestInstance(digestAlgName, provider);
        try {
            if (this.digestCalculator != null) {
                this.resultDigest = this.digestCalculator.getDigest();
            }
            else {
                if (this.content != null) {
                    this.content.write(new CMSSignedGenerator.DigOutputStream(digestInstance));
                }
                else if (this.signedAttributeSet == null) {
                    throw new CMSException("data not encapsulated in signature - use detached constructor.");
                }
                this.resultDigest = digestInstance.digest();
            }
        }
        catch (IOException ex) {
            throw new CMSException("can't process mime object to create signature.", ex);
        }
        final boolean equals = this.contentType.equals(CMSAttributes.counterSignature);
        final DERObject singleValuedSignedAttribute = this.getSingleValuedSignedAttribute(CMSAttributes.contentType, "content-type");
        if (singleValuedSignedAttribute == null) {
            if (!equals && this.signedAttributeSet != null) {
                throw new CMSException("The content-type attribute type MUST be present whenever signed attributes are present in signed-data");
            }
        }
        else {
            if (equals) {
                throw new CMSException("[For counter signatures,] the signedAttributes field MUST NOT contain a content-type attribute");
            }
            if (!(singleValuedSignedAttribute instanceof DERObjectIdentifier)) {
                throw new CMSException("content-type attribute value not of ASN.1 type 'OBJECT IDENTIFIER'");
            }
            if (!((DERObjectIdentifier)singleValuedSignedAttribute).equals(this.contentType)) {
                throw new CMSException("content-type attribute value does not match eContentType");
            }
        }
        final DERObject singleValuedSignedAttribute2 = this.getSingleValuedSignedAttribute(CMSAttributes.messageDigest, "message-digest");
        if (singleValuedSignedAttribute2 == null) {
            if (this.signedAttributeSet != null) {
                throw new CMSException("the message-digest signed attribute type MUST be present when there are any signed attributes present");
            }
        }
        else {
            if (!(singleValuedSignedAttribute2 instanceof ASN1OctetString)) {
                throw new CMSException("message-digest attribute value not of ASN.1 type 'OCTET STRING'");
            }
            if (!Arrays.constantTimeAreEqual(this.resultDigest, ((ASN1OctetString)singleValuedSignedAttribute2).getOctets())) {
                throw new CMSException("message-digest attribute value does not match calculated value");
            }
        }
        final AttributeTable signedAttributes = this.getSignedAttributes();
        if (signedAttributes != null && signedAttributes.getAll(CMSAttributes.counterSignature).size() > 0) {
            throw new CMSException("A countersignature attribute MUST NOT be a signed attribute");
        }
        final AttributeTable unsignedAttributes = this.getUnsignedAttributes();
        if (unsignedAttributes != null) {
            final ASN1EncodableVector all = unsignedAttributes.getAll(CMSAttributes.counterSignature);
            for (int i = 0; i < all.size(); ++i) {
                if (((Attribute)all.get(i)).getAttrValues().size() < 1) {
                    throw new CMSException("A countersignature attribute MUST contain at least one AttributeValue");
                }
            }
        }
        try {
            signatureInstance.initVerify(publicKey);
            if (this.signedAttributeSet == null) {
                if (this.digestCalculator != null) {
                    return this.verifyDigest(this.resultDigest, publicKey, this.getSignature(), provider);
                }
                if (this.content != null) {
                    this.content.write(new CMSSignedGenerator.SigOutputStream(signatureInstance));
                }
            }
            else {
                signatureInstance.update(this.getEncodedSignedAttributes());
            }
            return signatureInstance.verify(this.getSignature());
        }
        catch (InvalidKeyException ex2) {
            throw new CMSException("key not appropriate to signature in message.", ex2);
        }
        catch (IOException ex3) {
            throw new CMSException("can't process mime object to create signature.", ex3);
        }
        catch (SignatureException ex4) {
            throw new CMSException("invalid signature format in message: " + ex4.getMessage(), ex4);
        }
    }
    
    private boolean isNull(final DEREncodable derEncodable) {
        return derEncodable instanceof ASN1Null || derEncodable == null;
    }
    
    private DigestInfo derDecode(final byte[] array) throws IOException, CMSException {
        if (array[0] != 48) {
            throw new IOException("not a digest info object");
        }
        final DigestInfo digestInfo = new DigestInfo((ASN1Sequence)new ASN1InputStream(array).readObject());
        if (digestInfo.getEncoded().length != array.length) {
            throw new CMSException("malformed RSA signature");
        }
        return digestInfo;
    }
    
    private boolean verifyDigest(final byte[] data, final PublicKey publicKey, final byte[] array, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final String encryptionAlgName = CMSSignedHelper.INSTANCE.getEncryptionAlgName(this.getEncryptionAlgOID());
        try {
            if (encryptionAlgName.equals("RSA")) {
                final Cipher cipherInstance = CMSEnvelopedHelper.INSTANCE.getCipherInstance("RSA/ECB/PKCS1Padding", provider);
                cipherInstance.init(2, publicKey);
                final DigestInfo derDecode = this.derDecode(cipherInstance.doFinal(array));
                return derDecode.getAlgorithmId().getObjectId().equals(this.digestAlgorithm.getObjectId()) && this.isNull(derDecode.getAlgorithmId().getParameters()) && Arrays.constantTimeAreEqual(data, derDecode.getDigest());
            }
            if (encryptionAlgName.equals("DSA")) {
                final Signature signatureInstance = CMSSignedHelper.INSTANCE.getSignatureInstance("NONEwithDSA", provider);
                signatureInstance.initVerify(publicKey);
                signatureInstance.update(data);
                return signatureInstance.verify(array);
            }
            throw new CMSException("algorithm: " + encryptionAlgName + " not supported in base signatures.");
        }
        catch (GeneralSecurityException obj) {
            throw new CMSException("Exception processing signature: " + obj, obj);
        }
        catch (IOException obj2) {
            throw new CMSException("Exception decoding signature: " + obj2, obj2);
        }
    }
    
    public boolean verify(final PublicKey publicKey, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.verify(publicKey, CMSUtils.getProvider(s));
    }
    
    public boolean verify(final PublicKey publicKey, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        this.getSigningTime();
        return this.doVerify(publicKey, provider);
    }
    
    public boolean verify(final X509Certificate x509Certificate, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CertificateExpiredException, CertificateNotYetValidException, CMSException {
        return this.verify(x509Certificate, CMSUtils.getProvider(s));
    }
    
    public boolean verify(final X509Certificate x509Certificate, final Provider provider) throws NoSuchAlgorithmException, CertificateExpiredException, CertificateNotYetValidException, CMSException {
        final Time signingTime = this.getSigningTime();
        if (signingTime != null) {
            x509Certificate.checkValidity(signingTime.getDate());
        }
        return this.doVerify(x509Certificate.getPublicKey(), provider);
    }
    
    public SignerInfo toSignerInfo() {
        return this.info;
    }
    
    private DERObject getSingleValuedSignedAttribute(final DERObjectIdentifier derObjectIdentifier, final String str) throws CMSException {
        final AttributeTable unsignedAttributes = this.getUnsignedAttributes();
        if (unsignedAttributes != null && unsignedAttributes.getAll(derObjectIdentifier).size() > 0) {
            throw new CMSException("The " + str + " attribute MUST NOT be an unsigned attribute");
        }
        final AttributeTable signedAttributes = this.getSignedAttributes();
        if (signedAttributes == null) {
            return null;
        }
        final ASN1EncodableVector all = signedAttributes.getAll(derObjectIdentifier);
        switch (all.size()) {
            case 0: {
                return null;
            }
            case 1: {
                final ASN1Set attrValues = ((Attribute)all.get(0)).getAttrValues();
                if (attrValues.size() != 1) {
                    throw new CMSException("A " + str + " attribute MUST have a single attribute value");
                }
                return attrValues.getObjectAt(0).getDERObject();
            }
            default: {
                throw new CMSException("The SignedAttributes in a signerInfo MUST NOT include multiple instances of the " + str + " attribute");
            }
        }
    }
    
    private Time getSigningTime() throws CMSException {
        final DERObject singleValuedSignedAttribute = this.getSingleValuedSignedAttribute(CMSAttributes.signingTime, "signing-time");
        if (singleValuedSignedAttribute == null) {
            return null;
        }
        try {
            return Time.getInstance(singleValuedSignedAttribute);
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("signing-time attribute value not a valid 'Time' structure");
        }
    }
    
    public static SignerInformation replaceUnsignedAttributes(final SignerInformation signerInformation, final AttributeTable attributeTable) {
        final SignerInfo info = signerInformation.info;
        ASN1Set set = null;
        if (attributeTable != null) {
            set = new DERSet(attributeTable.toASN1EncodableVector());
        }
        return new SignerInformation(new SignerInfo(info.getSID(), info.getDigestAlgorithm(), info.getAuthenticatedAttributes(), info.getDigestEncryptionAlgorithm(), info.getEncryptedDigest(), set), signerInformation.contentType, signerInformation.content, null);
    }
    
    public static SignerInformation addCounterSigners(final SignerInformation signerInformation, final SignerInformationStore signerInformationStore) {
        final SignerInfo info = signerInformation.info;
        final AttributeTable unsignedAttributes = signerInformation.getUnsignedAttributes();
        ASN1EncodableVector asn1EncodableVector;
        if (unsignedAttributes != null) {
            asn1EncodableVector = unsignedAttributes.toASN1EncodableVector();
        }
        else {
            asn1EncodableVector = new ASN1EncodableVector();
        }
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        final Iterator<SignerInformation> iterator = signerInformationStore.getSigners().iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector2.add(iterator.next().toSignerInfo());
        }
        asn1EncodableVector.add(new Attribute(CMSAttributes.counterSignature, new DERSet(asn1EncodableVector2)));
        return new SignerInformation(new SignerInfo(info.getSID(), info.getDigestAlgorithm(), info.getAuthenticatedAttributes(), info.getDigestEncryptionAlgorithm(), info.getEncryptedDigest(), new DERSet(asn1EncodableVector)), signerInformation.contentType, signerInformation.content, null);
    }
}
