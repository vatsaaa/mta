// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.BufferedInputStream;
import java.io.IOException;
import org.bouncycastle.util.io.Streams;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.io.InputStream;

public class CMSTypedStream
{
    private static final int BUF_SIZ = 32768;
    private final String _oid;
    private final InputStream _in;
    
    public CMSTypedStream(final InputStream inputStream) {
        this(PKCSObjectIdentifiers.data.getId(), inputStream, 32768);
    }
    
    public CMSTypedStream(final String s, final InputStream inputStream) {
        this(s, inputStream, 32768);
    }
    
    public CMSTypedStream(final String oid, final InputStream inputStream, final int n) {
        this._oid = oid;
        this._in = new FullReaderStream(inputStream, n);
    }
    
    public String getContentType() {
        return this._oid;
    }
    
    public InputStream getContentStream() {
        return this._in;
    }
    
    public void drain() throws IOException {
        Streams.drain(this._in);
        this._in.close();
    }
    
    private class FullReaderStream extends InputStream
    {
        InputStream _stream;
        
        FullReaderStream(final InputStream in, final int size) {
            this._stream = new BufferedInputStream(in, size);
        }
        
        @Override
        public int read() throws IOException {
            return this._stream.read();
        }
        
        @Override
        public int read(final byte[] b, int off, int len) throws IOException {
            int n;
            int read;
            for (n = 0; len != 0 && (read = this._stream.read(b, off, len)) > 0; off += read, len -= read, n += read) {}
            if (n > 0) {
                return n;
            }
            return -1;
        }
        
        @Override
        public void close() throws IOException {
            this._stream.close();
        }
    }
}
