// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.DEROctetString;
import javax.crypto.Mac;
import java.io.IOException;
import java.security.spec.InvalidParameterSpecException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import java.security.Key;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.cms.OriginatorInfo;
import org.bouncycastle.asn1.cms.AuthenticatedData;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.BERSequenceGenerator;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Provider;
import javax.crypto.KeyGenerator;
import java.io.OutputStream;
import java.security.SecureRandom;

public class CMSAuthenticatedDataStreamGenerator extends CMSAuthenticatedGenerator
{
    private int _bufferSize;
    private boolean _berEncodeRecipientSet;
    
    public CMSAuthenticatedDataStreamGenerator() {
    }
    
    public CMSAuthenticatedDataStreamGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    public void setBufferSize(final int bufferSize) {
        this._bufferSize = bufferSize;
    }
    
    public void setBEREncodeRecipients(final boolean berEncodeRecipientSet) {
        this._berEncodeRecipientSet = berEncodeRecipientSet;
    }
    
    private OutputStream open(final OutputStream outputStream, final String s, final KeyGenerator keyGenerator, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final Provider provider2 = keyGenerator.getProvider();
        final SecretKey generateKey = keyGenerator.generateKey();
        final AlgorithmParameterSpec generateParameterSpec = this.generateParameterSpec(s, generateKey, provider2);
        final Iterator<RecipientInfoGenerator> iterator = this.recipientInfoGenerators.iterator();
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        while (iterator.hasNext()) {
            final RecipientInfoGenerator recipientInfoGenerator = iterator.next();
            try {
                asn1EncodableVector.add(recipientInfoGenerator.generate(generateKey, this.rand, provider));
            }
            catch (InvalidKeyException ex) {
                throw new CMSException("key inappropriate for algorithm.", ex);
            }
            catch (GeneralSecurityException ex2) {
                throw new CMSException("error making encrypted content.", ex2);
            }
        }
        return this.open(outputStream, s, generateKey, generateParameterSpec, asn1EncodableVector, provider2);
    }
    
    protected OutputStream open(final OutputStream outputStream, final String s, final SecretKey secretKey, final AlgorithmParameterSpec algorithmParameterSpec, final ASN1EncodableVector asn1EncodableVector, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.open(outputStream, s, secretKey, algorithmParameterSpec, asn1EncodableVector, CMSUtils.getProvider(s2));
    }
    
    protected OutputStream open(final OutputStream outputStream, final String s, final SecretKey key, final AlgorithmParameterSpec params, final ASN1EncodableVector asn1EncodableVector, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        try {
            final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
            berSequenceGenerator.addObject(CMSObjectIdentifiers.authenticatedData);
            final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
            berSequenceGenerator2.addObject(new DERInteger(AuthenticatedData.calculateVersion(null)));
            if (this._berEncodeRecipientSet) {
                berSequenceGenerator2.getRawOutputStream().write(new BERSet(asn1EncodableVector).getEncoded());
            }
            else {
                berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
            }
            final Mac mac = CMSEnvelopedHelper.INSTANCE.getMac(s, provider);
            mac.init(key, params);
            berSequenceGenerator2.getRawOutputStream().write(this.getAlgorithmIdentifier(s, params, provider).getEncoded());
            final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
            berSequenceGenerator3.addObject(CMSObjectIdentifiers.data);
            return new CmsAuthenticatedDataOutputStream(new MacOutputStream(CMSUtils.createBEROctetOutputStream(berSequenceGenerator3.getRawOutputStream(), 0, false, this._bufferSize), mac), berSequenceGenerator, berSequenceGenerator2, berSequenceGenerator3);
        }
        catch (InvalidKeyException ex) {
            throw new CMSException("key invalid in message.", ex);
        }
        catch (NoSuchPaddingException ex2) {
            throw new CMSException("required padding not supported.", ex2);
        }
        catch (InvalidAlgorithmParameterException ex3) {
            throw new CMSException("algorithm parameter invalid.", ex3);
        }
        catch (InvalidParameterSpecException ex4) {
            throw new CMSException("algorithm parameter spec invalid.", ex4);
        }
        catch (IOException ex5) {
            throw new CMSException("exception decoding algorithm parameters.", ex5);
        }
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        return this.open(outputStream, s, CMSUtils.getProvider(s2));
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException, IOException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(this.rand);
        return this.open(outputStream, s, symmetricKeyGenerator, provider);
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final int n, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        return this.open(outputStream, s, n, CMSUtils.getProvider(s2));
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final int keysize, final Provider provider) throws NoSuchAlgorithmException, CMSException, IOException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(keysize, this.rand);
        return this.open(outputStream, s, symmetricKeyGenerator, provider);
    }
    
    private class CmsAuthenticatedDataOutputStream extends OutputStream
    {
        private MacOutputStream macStream;
        private BERSequenceGenerator cGen;
        private BERSequenceGenerator envGen;
        private BERSequenceGenerator eiGen;
        
        public CmsAuthenticatedDataOutputStream(final MacOutputStream macStream, final BERSequenceGenerator cGen, final BERSequenceGenerator envGen, final BERSequenceGenerator eiGen) {
            this.macStream = macStream;
            this.cGen = cGen;
            this.envGen = envGen;
            this.eiGen = eiGen;
        }
        
        @Override
        public void write(final int n) throws IOException {
            this.macStream.write(n);
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
            this.macStream.write(array, n, n2);
        }
        
        @Override
        public void write(final byte[] array) throws IOException {
            this.macStream.write(array);
        }
        
        @Override
        public void close() throws IOException {
            this.macStream.close();
            this.eiGen.close();
            this.envGen.addObject(new DEROctetString(this.macStream.getMac()));
            this.envGen.close();
            this.cGen.close();
        }
    }
}
