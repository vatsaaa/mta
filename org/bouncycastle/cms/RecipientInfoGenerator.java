// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.security.Provider;
import java.security.SecureRandom;
import javax.crypto.SecretKey;

interface RecipientInfoGenerator
{
    RecipientInfo generate(final SecretKey p0, final SecureRandom p1, final Provider p2) throws GeneralSecurityException;
}
