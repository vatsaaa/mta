// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import javax.crypto.Mac;
import javax.crypto.Cipher;
import java.security.spec.InvalidParameterSpecException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import javax.crypto.CipherInputStream;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.ASN1Object;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.AlgorithmParameters;
import java.io.IOException;
import org.bouncycastle.asn1.DEREncodable;
import java.io.InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public abstract class RecipientInformation
{
    protected RecipientId rid;
    protected AlgorithmIdentifier encAlg;
    protected AlgorithmIdentifier macAlg;
    protected AlgorithmIdentifier authEncAlg;
    protected AlgorithmIdentifier keyEncAlg;
    protected InputStream data;
    private MacInputStream macStream;
    private byte[] resultMac;
    
    @Deprecated
    protected RecipientInformation(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final InputStream inputStream) {
        this(algorithmIdentifier, null, algorithmIdentifier2, inputStream);
    }
    
    @Deprecated
    protected RecipientInformation(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3, final InputStream inputStream) {
        this(algorithmIdentifier, algorithmIdentifier2, null, algorithmIdentifier3, inputStream);
    }
    
    RecipientInformation(final AlgorithmIdentifier encAlg, final AlgorithmIdentifier macAlg, final AlgorithmIdentifier authEncAlg, final AlgorithmIdentifier keyEncAlg, final InputStream data) {
        this.rid = new RecipientId();
        this.encAlg = encAlg;
        this.macAlg = macAlg;
        this.authEncAlg = authEncAlg;
        this.keyEncAlg = keyEncAlg;
        this.data = data;
    }
    
    AlgorithmIdentifier getActiveAlgID() {
        if (this.encAlg != null) {
            return this.encAlg;
        }
        if (this.macAlg != null) {
            return this.macAlg;
        }
        return this.authEncAlg;
    }
    
    public RecipientId getRID() {
        return this.rid;
    }
    
    private byte[] encodeObj(final DEREncodable derEncodable) throws IOException {
        if (derEncodable != null) {
            return derEncodable.getDERObject().getEncoded();
        }
        return null;
    }
    
    public String getKeyEncryptionAlgOID() {
        return this.keyEncAlg.getObjectId().getId();
    }
    
    public byte[] getKeyEncryptionAlgParams() {
        try {
            return this.encodeObj(this.keyEncAlg.getParameters());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AlgorithmParameters getKeyEncryptionAlgorithmParameters(final String s) throws CMSException, NoSuchProviderException {
        return this.getKeyEncryptionAlgorithmParameters(CMSUtils.getProvider(s));
    }
    
    public AlgorithmParameters getKeyEncryptionAlgorithmParameters(final Provider provider) throws CMSException {
        try {
            final byte[] encodeObj = this.encodeObj(this.keyEncAlg.getParameters());
            if (encodeObj == null) {
                return null;
            }
            final AlgorithmParameters algorithmParameters = CMSEnvelopedHelper.INSTANCE.createAlgorithmParameters(this.getKeyEncryptionAlgOID(), provider);
            algorithmParameters.init(encodeObj, "ASN.1");
            return algorithmParameters;
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find parameters for algorithm", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("can't find parse parameters", ex2);
        }
    }
    
    protected CMSTypedStream getContentFromSessionKey(final Key key, final Provider provider) throws CMSException {
        try {
            InputStream data = this.data;
            if (this.encAlg != null) {
                final String id = this.encAlg.getObjectId().getId();
                final Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(id, provider);
                final ASN1Object asn1Object = (ASN1Object)this.encAlg.getParameters();
                if (asn1Object != null && !(asn1Object instanceof ASN1Null)) {
                    try {
                        final AlgorithmParameters algorithmParameters = CMSEnvelopedHelper.INSTANCE.createAlgorithmParameters(id, symmetricCipher.getProvider());
                        algorithmParameters.init(asn1Object.getEncoded(), "ASN.1");
                        symmetricCipher.init(2, key, algorithmParameters);
                    }
                    catch (NoSuchAlgorithmException ex) {
                        if (!id.equals(CMSEnvelopedDataGenerator.DES_EDE3_CBC) && !id.equals("1.3.6.1.4.1.188.7.1.1.2") && !id.equals(CMSEnvelopedDataGenerator.AES128_CBC) && !id.equals(CMSEnvelopedDataGenerator.AES192_CBC) && !id.equals(CMSEnvelopedDataGenerator.AES256_CBC)) {
                            throw ex;
                        }
                        symmetricCipher.init(2, key, new IvParameterSpec(ASN1OctetString.getInstance(asn1Object).getOctets()));
                    }
                }
                else if (id.equals(CMSEnvelopedDataGenerator.DES_EDE3_CBC) || id.equals("1.3.6.1.4.1.188.7.1.1.2") || id.equals("1.2.840.113533.7.66.10")) {
                    symmetricCipher.init(2, key, new IvParameterSpec(new byte[8]));
                }
                else {
                    symmetricCipher.init(2, key);
                }
                data = new CipherInputStream(data, symmetricCipher);
            }
            if (this.macAlg != null) {
                final MacInputStream macInputStream = createMacInputStream(this.macAlg, key, data, provider);
                this.macStream = macInputStream;
                data = macInputStream;
            }
            if (this.authEncAlg != null) {
                throw new CMSException("AuthEnveloped data decryption not yet implemented");
            }
            return new CMSTypedStream(data);
        }
        catch (NoSuchAlgorithmException ex2) {
            throw new CMSException("can't find algorithm.", ex2);
        }
        catch (InvalidKeyException ex3) {
            throw new CMSException("key invalid in message.", ex3);
        }
        catch (NoSuchPaddingException ex4) {
            throw new CMSException("required padding not supported.", ex4);
        }
        catch (InvalidAlgorithmParameterException ex5) {
            throw new CMSException("algorithm parameters invalid.", ex5);
        }
        catch (InvalidParameterSpecException ex6) {
            throw new CMSException("MAC algorithm parameter spec invalid.", ex6);
        }
        catch (IOException ex7) {
            throw new CMSException("error decoding algorithm parameters.", ex7);
        }
    }
    
    private static MacInputStream createMacInputStream(final AlgorithmIdentifier algorithmIdentifier, final Key key, final InputStream inputStream, final Provider provider) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IOException, InvalidParameterSpecException {
        final Mac mac = CMSEnvelopedHelper.INSTANCE.getMac(algorithmIdentifier.getObjectId().getId(), provider);
        final ASN1Object asn1Object = (ASN1Object)algorithmIdentifier.getParameters();
        if (asn1Object != null && !(asn1Object instanceof ASN1Null)) {
            final AlgorithmParameters algorithmParameters = CMSEnvelopedHelper.INSTANCE.createAlgorithmParameters(algorithmIdentifier.getObjectId().getId(), provider);
            algorithmParameters.init(asn1Object.getEncoded(), "ASN.1");
            mac.init(key, algorithmParameters.getParameterSpec(IvParameterSpec.class));
        }
        else {
            mac.init(key);
        }
        return new MacInputStream(mac, inputStream);
    }
    
    public byte[] getContent(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContent(key, CMSUtils.getProvider(s));
    }
    
    public byte[] getContent(final Key key, final Provider provider) throws CMSException {
        try {
            if (this.data instanceof ByteArrayInputStream) {
                this.data.reset();
            }
            return CMSUtils.streamToByteArray(this.getContentStream(key, provider).getContentStream());
        }
        catch (IOException obj) {
            throw new RuntimeException("unable to parse internal stream: " + obj);
        }
    }
    
    public byte[] getMac() {
        if (this.macStream != null && this.resultMac == null) {
            this.resultMac = this.macStream.getMac();
        }
        return this.resultMac;
    }
    
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    public abstract CMSTypedStream getContentStream(final Key p0, final Provider p1) throws CMSException;
    
    private static class MacInputStream extends InputStream
    {
        private final InputStream inStream;
        private final Mac mac;
        
        MacInputStream(final Mac mac, final InputStream inStream) {
            this.inStream = inStream;
            this.mac = mac;
        }
        
        @Override
        public int read(final byte[] array) throws IOException {
            return this.read(array, 0, array.length);
        }
        
        @Override
        public int read(final byte[] array, final int n, final int len) throws IOException {
            final int read = this.inStream.read(array, n, len);
            if (read > 0) {
                this.mac.update(array, n, read);
            }
            return read;
        }
        
        @Override
        public int read() throws IOException {
            final int read = this.inStream.read();
            if (read > 0) {
                this.mac.update((byte)read);
            }
            return read;
        }
        
        public byte[] getMac() {
            return this.mac.doFinal();
        }
    }
}
