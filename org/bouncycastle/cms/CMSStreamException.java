// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.IOException;

public class CMSStreamException extends IOException
{
    private final Throwable underlying;
    
    CMSStreamException(final String message) {
        super(message);
        this.underlying = null;
    }
    
    CMSStreamException(final String message, final Throwable underlying) {
        super(message);
        this.underlying = underlying;
    }
    
    @Override
    public Throwable getCause() {
        return this.underlying;
    }
}
