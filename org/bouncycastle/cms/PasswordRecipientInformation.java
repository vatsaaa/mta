// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.asn1.ASN1OctetString;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import java.security.Key;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.DEREncodable;
import java.io.InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;

public class PasswordRecipientInformation extends RecipientInformation
{
    private PasswordRecipientInfo info;
    
    @Deprecated
    public PasswordRecipientInformation(final PasswordRecipientInfo passwordRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final InputStream inputStream) {
        this(passwordRecipientInfo, algorithmIdentifier, null, null, inputStream);
    }
    
    @Deprecated
    public PasswordRecipientInformation(final PasswordRecipientInfo passwordRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final InputStream inputStream) {
        this(passwordRecipientInfo, algorithmIdentifier, algorithmIdentifier2, null, inputStream);
    }
    
    PasswordRecipientInformation(final PasswordRecipientInfo info, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3, final InputStream inputStream) {
        super(algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, info.getKeyEncryptionAlgorithm(), inputStream);
        this.info = info;
        this.rid = new RecipientId();
    }
    
    public String getKeyDerivationAlgOID() {
        if (this.info.getKeyDerivationAlgorithm() != null) {
            return this.info.getKeyDerivationAlgorithm().getObjectId().getId();
        }
        return null;
    }
    
    public byte[] getKeyDerivationAlgParams() {
        try {
            if (this.info.getKeyDerivationAlgorithm() != null) {
                final DEREncodable parameters = this.info.getKeyDerivationAlgorithm().getParameters();
                if (parameters != null) {
                    return parameters.getDERObject().getEncoded();
                }
            }
            return null;
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AlgorithmParameters getKeyDerivationAlgParameters(final String s) throws NoSuchProviderException {
        return this.getKeyDerivationAlgParameters(CMSUtils.getProvider(s));
    }
    
    public AlgorithmParameters getKeyDerivationAlgParameters(final Provider provider) {
        try {
            if (this.info.getKeyDerivationAlgorithm() != null) {
                final DEREncodable parameters = this.info.getKeyDerivationAlgorithm().getParameters();
                if (parameters != null) {
                    final AlgorithmParameters instance = AlgorithmParameters.getInstance(this.info.getKeyDerivationAlgorithm().getObjectId().toString(), provider);
                    instance.init(parameters.getDERObject().getEncoded());
                    return instance;
                }
            }
            return null;
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final Provider provider) throws CMSException {
        try {
            final ASN1Sequence asn1Sequence = (ASN1Sequence)AlgorithmIdentifier.getInstance(this.info.getKeyEncryptionAlgorithm()).getParameters();
            final byte[] octets = this.info.getEncryptedKey().getOctets();
            final String id = DERObjectIdentifier.getInstance(asn1Sequence.getObjectAt(0)).getId();
            final Cipher instance = Cipher.getInstance(CMSEnvelopedHelper.INSTANCE.getRFC3211WrapperName(id), provider);
            instance.init(4, new SecretKeySpec(((CMSPBEKey)key).getEncoded(id), id), new IvParameterSpec(ASN1OctetString.getInstance(asn1Sequence.getObjectAt(1)).getOctets()));
            return this.getContentFromSessionKey(instance.unwrap(octets, this.getActiveAlgID().getObjectId().getId(), 3), provider);
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find algorithm.", ex);
        }
        catch (InvalidKeyException ex2) {
            throw new CMSException("key invalid in message.", ex2);
        }
        catch (NoSuchPaddingException ex3) {
            throw new CMSException("required padding not supported.", ex3);
        }
        catch (InvalidAlgorithmParameterException ex4) {
            throw new CMSException("invalid iv.", ex4);
        }
    }
}
