// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import javax.crypto.Mac;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.SecretKey;
import java.security.spec.InvalidParameterSpecException;
import java.security.NoSuchAlgorithmException;
import java.io.IOException;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.Provider;
import java.security.spec.AlgorithmParameterSpec;
import java.security.SecureRandom;

public class CMSAuthenticatedGenerator extends CMSEnvelopedGenerator
{
    public CMSAuthenticatedGenerator() {
    }
    
    public CMSAuthenticatedGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    protected AlgorithmIdentifier getAlgorithmIdentifier(final String s, final AlgorithmParameterSpec paramSpec, final Provider provider) throws IOException, NoSuchAlgorithmException, InvalidParameterSpecException {
        final AlgorithmParameters algorithmParameters = CMSEnvelopedHelper.INSTANCE.createAlgorithmParameters(s, provider);
        algorithmParameters.init(paramSpec);
        return this.getAlgorithmIdentifier(s, algorithmParameters);
    }
    
    protected AlgorithmParameterSpec generateParameterSpec(final String s, final SecretKey secretKey, final Provider provider) throws CMSException {
        try {
            if (s.equals(CMSAuthenticatedGenerator.RC2_CBC)) {
                final byte[] array = new byte[8];
                this.rand.nextBytes(array);
                return new RC2ParameterSpec(secretKey.getEncoded().length * 8, array);
            }
            return CMSEnvelopedHelper.INSTANCE.createAlgorithmParameterGenerator(s, provider).generateParameters().getParameterSpec(IvParameterSpec.class);
        }
        catch (GeneralSecurityException ex) {
            return null;
        }
    }
    
    protected static class MacOutputStream extends OutputStream
    {
        private final OutputStream out;
        private Mac mac;
        
        MacOutputStream(final OutputStream out, final Mac mac) {
            this.out = out;
            this.mac = mac;
        }
        
        @Override
        public void write(final byte[] array) throws IOException {
            this.mac.update(array, 0, array.length);
            this.out.write(array, 0, array.length);
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
            this.mac.update(array, n, n2);
            this.out.write(array, n, n2);
        }
        
        @Override
        public void write(final int n) throws IOException {
            this.mac.update((byte)n);
            this.out.write(n);
        }
        
        @Override
        public void close() throws IOException {
            this.out.close();
        }
        
        public byte[] getMac() {
            return this.mac.doFinal();
        }
    }
}
