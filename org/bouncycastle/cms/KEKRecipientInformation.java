// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.Key;
import java.io.InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;

public class KEKRecipientInformation extends RecipientInformation
{
    private KEKRecipientInfo info;
    
    @Deprecated
    public KEKRecipientInformation(final KEKRecipientInfo kekRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final InputStream inputStream) {
        this(kekRecipientInfo, algorithmIdentifier, null, null, inputStream);
    }
    
    @Deprecated
    public KEKRecipientInformation(final KEKRecipientInfo kekRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final InputStream inputStream) {
        this(kekRecipientInfo, algorithmIdentifier, algorithmIdentifier2, null, inputStream);
    }
    
    KEKRecipientInformation(final KEKRecipientInfo info, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3, final InputStream inputStream) {
        super(algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, info.getKeyEncryptionAlgorithm(), inputStream);
        this.info = info;
        (this.rid = new RecipientId()).setKeyIdentifier(info.getKekid().getKeyIdentifier().getOctets());
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final Provider provider) throws CMSException {
        try {
            final byte[] octets = this.info.getEncryptedKey().getOctets();
            final Cipher instance = Cipher.getInstance(this.keyEncAlg.getObjectId().getId(), provider);
            instance.init(4, key);
            return this.getContentFromSessionKey(instance.unwrap(octets, this.getActiveAlgID().getObjectId().getId(), 3), provider);
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find algorithm.", ex);
        }
        catch (InvalidKeyException ex2) {
            throw new CMSException("key invalid in message.", ex2);
        }
        catch (NoSuchPaddingException ex3) {
            throw new CMSException("required padding not supported.", ex3);
        }
    }
}
