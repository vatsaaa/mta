// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.Hashtable;
import java.security.MessageDigest;
import java.security.Signature;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.cms.CMSAttributes;
import java.util.Map;
import java.util.Collections;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.BERConstructedOctetString;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.security.cert.CertificateEncodingException;
import java.security.SignatureException;
import java.security.InvalidKeyException;
import java.io.IOException;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class CMSSignedDataGenerator extends CMSSignedGenerator
{
    List signerInfs;
    
    public CMSSignedDataGenerator() {
        this.signerInfs = new ArrayList();
    }
    
    public CMSSignedDataGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
        this.signerInfs = new ArrayList();
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s) throws IllegalArgumentException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(x509Certificate), s2, s, new DefaultSignedAttributeTableGenerator(), null, null));
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s) throws IllegalArgumentException {
        this.addSigner(privateKey, array, this.getEncOID(privateKey, s), s);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(array), s2, s, new DefaultSignedAttributeTableGenerator(), null, null));
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s, attributeTable, attributeTable2);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(x509Certificate), s2, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), attributeTable));
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.addSigner(privateKey, array, s, this.getEncOID(privateKey, s), new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2));
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(array), s2, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), attributeTable));
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(x509Certificate), s2, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, null));
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.addSigner(privateKey, array, s, this.getEncOID(privateKey, s), cmsAttributeTableGenerator, cmsAttributeTableGenerator2);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(array), s2, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, null));
    }
    
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, CMSUtils.getProvider(s));
    }
    
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(cmsProcessable, false, provider);
    }
    
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(s, cmsProcessable, b, CMSUtils.getProvider(s2), true);
    }
    
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(s, cmsProcessable, b, provider, true);
    }
    
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final String s2, final boolean b2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(s, cmsProcessable, b, CMSUtils.getProvider(s2), b2);
    }
    
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final Provider provider, final boolean b2) throws NoSuchAlgorithmException, CMSException {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        this._digests.clear();
        for (final SignerInformation signerInformation : this._signers) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(signerInformation.getDigestAlgorithmID()));
            asn1EncodableVector2.add(signerInformation.toSignerInfo());
        }
        final boolean b3 = s == null;
        final DERObjectIdentifier derObjectIdentifier = b3 ? CMSObjectIdentifiers.data : new DERObjectIdentifier(s);
        for (final SignerInf signerInf : this.signerInfs) {
            try {
                asn1EncodableVector.add(signerInf.getDigestAlgorithmID());
                asn1EncodableVector2.add(signerInf.toSignerInfo(derObjectIdentifier, cmsProcessable, this.rand, provider, b2, b3));
            }
            catch (IOException ex) {
                throw new CMSException("encoding error.", ex);
            }
            catch (InvalidKeyException ex2) {
                throw new CMSException("key inappropriate for signature.", ex2);
            }
            catch (SignatureException ex3) {
                throw new CMSException("error creating signature.", ex3);
            }
            catch (CertificateEncodingException ex4) {
                throw new CMSException("error creating sid.", ex4);
            }
        }
        ASN1Set berSetFromList = null;
        if (this._certs.size() != 0) {
            berSetFromList = CMSUtils.createBerSetFromList(this._certs);
        }
        ASN1Set berSetFromList2 = null;
        if (this._crls.size() != 0) {
            berSetFromList2 = CMSUtils.createBerSetFromList(this._crls);
        }
        DEREncodable derEncodable = null;
        if (b) {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (cmsProcessable != null) {
                try {
                    cmsProcessable.write(byteArrayOutputStream);
                }
                catch (IOException ex5) {
                    throw new CMSException("encapsulation error.", ex5);
                }
            }
            derEncodable = new BERConstructedOctetString(byteArrayOutputStream.toByteArray());
        }
        return new CMSSignedData(cmsProcessable, new ContentInfo(CMSObjectIdentifiers.signedData, new SignedData(new DERSet(asn1EncodableVector), new ContentInfo(derObjectIdentifier, derEncodable), berSetFromList, berSetFromList2, new DERSet(asn1EncodableVector2))));
    }
    
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final boolean b, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(CMSSignedDataGenerator.DATA, cmsProcessable, b, s);
    }
    
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final boolean b, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(CMSSignedDataGenerator.DATA, cmsProcessable, b, provider);
    }
    
    public SignerInformationStore generateCounterSigners(final SignerInformation signerInformation, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(null, new CMSProcessableByteArray(signerInformation.getSignature()), false, provider).getSignerInfos();
    }
    
    public SignerInformationStore generateCounterSigners(final SignerInformation signerInformation, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(null, new CMSProcessableByteArray(signerInformation.getSignature()), false, CMSUtils.getProvider(s)).getSignerInfos();
    }
    
    private class SignerInf
    {
        private final PrivateKey key;
        private final SignerIdentifier signerIdentifier;
        private final String digestOID;
        private final String encOID;
        private final CMSAttributeTableGenerator sAttr;
        private final CMSAttributeTableGenerator unsAttr;
        private final AttributeTable baseSignedTable;
        
        SignerInf(final PrivateKey key, final SignerIdentifier signerIdentifier, final String digestOID, final String encOID, final CMSAttributeTableGenerator sAttr, final CMSAttributeTableGenerator unsAttr, final AttributeTable baseSignedTable) {
            this.key = key;
            this.signerIdentifier = signerIdentifier;
            this.digestOID = digestOID;
            this.encOID = encOID;
            this.sAttr = sAttr;
            this.unsAttr = unsAttr;
            this.baseSignedTable = baseSignedTable;
        }
        
        AlgorithmIdentifier getDigestAlgorithmID() {
            return new AlgorithmIdentifier(new DERObjectIdentifier(this.digestOID), new DERNull());
        }
        
        SignerInfo toSignerInfo(final DERObjectIdentifier derObjectIdentifier, final CMSProcessable cmsProcessable, final SecureRandom random, final Provider provider, final boolean b, final boolean b2) throws IOException, SignatureException, InvalidKeyException, NoSuchAlgorithmException, CertificateEncodingException, CMSException {
            final AlgorithmIdentifier digestAlgorithmID = this.getDigestAlgorithmID();
            final String digestAlgName = CMSSignedHelper.INSTANCE.getDigestAlgName(this.digestOID);
            final Signature signatureInstance = CMSSignedHelper.INSTANCE.getSignatureInstance(digestAlgName + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(this.encOID), provider);
            final MessageDigest digestInstance = CMSSignedHelper.INSTANCE.getDigestInstance(digestAlgName, provider);
            final AlgorithmIdentifier encAlgorithmIdentifier = CMSSignedDataGenerator.this.getEncAlgorithmIdentifier(this.encOID, signatureInstance);
            if (cmsProcessable != null) {
                cmsProcessable.write(new DigOutputStream(digestInstance));
            }
            final byte[] digest = digestInstance.digest();
            CMSSignedDataGenerator.this._digests.put(this.digestOID, digest.clone());
            AttributeTable baseSignedTable;
            if (b) {
                final Map baseParameters = CMSSignedDataGenerator.this.getBaseParameters(derObjectIdentifier, digestAlgorithmID, digest);
                baseSignedTable = ((this.sAttr != null) ? this.sAttr.getAttributes(Collections.unmodifiableMap((Map<?, ?>)baseParameters)) : null);
            }
            else {
                baseSignedTable = this.baseSignedTable;
            }
            ASN1Set attributeSet = null;
            byte[] data;
            if (baseSignedTable != null) {
                if (b2) {
                    final Hashtable hashtable = baseSignedTable.toHashtable();
                    hashtable.remove(CMSAttributes.contentType);
                    baseSignedTable = new AttributeTable(hashtable);
                }
                attributeSet = CMSSignedDataGenerator.this.getAttributeSet(baseSignedTable);
                data = attributeSet.getEncoded("DER");
            }
            else {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (cmsProcessable != null) {
                    cmsProcessable.write(byteArrayOutputStream);
                }
                data = byteArrayOutputStream.toByteArray();
            }
            signatureInstance.initSign(this.key, random);
            signatureInstance.update(data);
            final byte[] sign = signatureInstance.sign();
            ASN1Set attributeSet2 = null;
            if (this.unsAttr != null) {
                final Map baseParameters2 = CMSSignedDataGenerator.this.getBaseParameters(derObjectIdentifier, digestAlgorithmID, digest);
                baseParameters2.put("encryptedDigest", sign.clone());
                attributeSet2 = CMSSignedDataGenerator.this.getAttributeSet(this.unsAttr.getAttributes(Collections.unmodifiableMap((Map<?, ?>)baseParameters2)));
            }
            return new SignerInfo(this.signerIdentifier, digestAlgorithmID, attributeSet, encAlgorithmIdentifier, new DEROctetString(sign), attributeSet2);
        }
    }
}
