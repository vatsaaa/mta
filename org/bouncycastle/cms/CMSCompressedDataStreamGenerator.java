// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.zip.DeflaterOutputStream;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequenceGenerator;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.BERSequenceGenerator;
import java.io.IOException;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import java.io.OutputStream;

public class CMSCompressedDataStreamGenerator
{
    public static final String ZLIB = "1.2.840.113549.1.9.16.3.8";
    private int _bufferSize;
    
    public void setBufferSize(final int bufferSize) {
        this._bufferSize = bufferSize;
    }
    
    public OutputStream open(final OutputStream outputStream, final String s) throws IOException {
        return this.open(outputStream, CMSObjectIdentifiers.data.getId(), s);
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final String s2) throws IOException {
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.compressedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(new DERInteger(0));
        final DERSequenceGenerator derSequenceGenerator = new DERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        derSequenceGenerator.addObject(new DERObjectIdentifier("1.2.840.113549.1.9.16.3.8"));
        derSequenceGenerator.close();
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(new DERObjectIdentifier(s));
        return new CmsCompressedOutputStream(new DeflaterOutputStream(CMSUtils.createBEROctetOutputStream(berSequenceGenerator3.getRawOutputStream(), 0, true, this._bufferSize)), berSequenceGenerator, berSequenceGenerator2, berSequenceGenerator3);
    }
    
    private class CmsCompressedOutputStream extends OutputStream
    {
        private DeflaterOutputStream _out;
        private BERSequenceGenerator _sGen;
        private BERSequenceGenerator _cGen;
        private BERSequenceGenerator _eiGen;
        
        CmsCompressedOutputStream(final DeflaterOutputStream out, final BERSequenceGenerator sGen, final BERSequenceGenerator cGen, final BERSequenceGenerator eiGen) {
            this._out = out;
            this._sGen = sGen;
            this._cGen = cGen;
            this._eiGen = eiGen;
        }
        
        @Override
        public void write(final int b) throws IOException {
            this._out.write(b);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            this._out.write(b, off, len);
        }
        
        @Override
        public void write(final byte[] b) throws IOException {
            this._out.write(b);
        }
        
        @Override
        public void close() throws IOException {
            this._out.close();
            this._eiGen.close();
            this._cGen.close();
            this._sGen.close();
        }
    }
}
