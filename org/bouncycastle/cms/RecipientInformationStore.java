// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.Map;
import java.util.List;

public class RecipientInformationStore
{
    private final List all;
    private final Map table;
    
    public RecipientInformationStore(final Collection c) {
        this.table = new HashMap();
        for (final RecipientInformation recipientInformation : c) {
            final RecipientId rid = recipientInformation.getRID();
            ArrayList<RecipientInformation> list = this.table.get(rid);
            if (list == null) {
                list = new ArrayList<RecipientInformation>(1);
                this.table.put(rid, list);
            }
            list.add(recipientInformation);
        }
        this.all = new ArrayList(c);
    }
    
    public RecipientInformation get(final RecipientId recipientId) {
        final ArrayList<RecipientInformation> list = this.table.get(recipientId);
        return (list == null) ? null : ((RecipientInformation)list.get(0));
    }
    
    public int size() {
        return this.all.size();
    }
    
    public Collection getRecipients() {
        return new ArrayList(this.all);
    }
    
    public Collection getRecipients(final RecipientId recipientId) {
        final ArrayList c = this.table.get(recipientId);
        return (c == null) ? new ArrayList() : new ArrayList(c);
    }
}
