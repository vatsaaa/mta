// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.util.io.Streams;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.BERSetParser;
import org.bouncycastle.asn1.ASN1SetParser;
import org.bouncycastle.asn1.DERTaggedObject;
import java.security.cert.CertStoreException;
import org.bouncycastle.asn1.ASN1Generator;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.BERSequenceGenerator;
import org.bouncycastle.asn1.ASN1StreamParser;
import org.bouncycastle.asn1.ASN1SequenceParser;
import java.io.OutputStream;
import java.security.DigestInputStream;
import java.security.NoSuchProviderException;
import org.bouncycastle.x509.NoSuchStoreException;
import java.util.Iterator;
import java.util.Collection;
import org.bouncycastle.asn1.cms.SignerInfo;
import java.security.MessageDigest;
import java.util.ArrayList;
import org.bouncycastle.asn1.cms.ContentInfoParser;
import org.bouncycastle.asn1.DEREncodable;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1OctetStringParser;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.util.HashMap;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.x509.X509Store;
import java.security.cert.CertStore;
import java.util.Map;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.SignedDataParser;

public class CMSSignedDataParser extends CMSContentInfoParser
{
    private static final CMSSignedHelper HELPER;
    private SignedDataParser _signedData;
    private DERObjectIdentifier _signedContentType;
    private CMSTypedStream _signedContent;
    private Map _digests;
    private CertStore _certStore;
    private SignerInformationStore _signerInfoStore;
    private X509Store _attributeStore;
    private ASN1Set _certSet;
    private ASN1Set _crlSet;
    private boolean _isCertCrlParsed;
    private X509Store _certificateStore;
    private X509Store _crlStore;
    
    public CMSSignedDataParser(final byte[] buf) throws CMSException {
        this(new ByteArrayInputStream(buf));
    }
    
    public CMSSignedDataParser(final CMSTypedStream cmsTypedStream, final byte[] buf) throws CMSException {
        this(cmsTypedStream, new ByteArrayInputStream(buf));
    }
    
    public CMSSignedDataParser(final InputStream inputStream) throws CMSException {
        this(null, inputStream);
    }
    
    public CMSSignedDataParser(final CMSTypedStream signedContent, final InputStream inputStream) throws CMSException {
        super(inputStream);
        try {
            this._signedContent = signedContent;
            this._signedData = SignedDataParser.getInstance(this._contentInfo.getContent(16));
            this._digests = new HashMap();
            DEREncodable object;
            while ((object = this._signedData.getDigestAlgorithms().readObject()) != null) {
                final AlgorithmIdentifier instance = AlgorithmIdentifier.getInstance(object.getDERObject());
                try {
                    final String digestAlgName = CMSSignedDataParser.HELPER.getDigestAlgName(instance.getObjectId().toString());
                    this._digests.put(digestAlgName, CMSSignedDataParser.HELPER.getDigestInstance(digestAlgName, null));
                }
                catch (NoSuchAlgorithmException ex2) {}
            }
            final ContentInfoParser encapContentInfo = this._signedData.getEncapContentInfo();
            final ASN1OctetStringParser asn1OctetStringParser = (ASN1OctetStringParser)encapContentInfo.getContent(4);
            if (asn1OctetStringParser != null) {
                final CMSTypedStream signedContent2 = new CMSTypedStream(encapContentInfo.getContentType().getId(), asn1OctetStringParser.getOctetStream());
                if (this._signedContent == null) {
                    this._signedContent = signedContent2;
                }
                else {
                    signedContent2.drain();
                }
            }
            if (signedContent == null) {
                this._signedContentType = encapContentInfo.getContentType();
            }
            else {
                this._signedContentType = new DERObjectIdentifier(this._signedContent.getContentType());
            }
        }
        catch (IOException ex) {
            throw new CMSException("io exception: " + ex.getMessage(), ex);
        }
        if (this._digests.isEmpty()) {
            throw new CMSException("no digests could be created for message.");
        }
    }
    
    public int getVersion() {
        return this._signedData.getVersion().getValue().intValue();
    }
    
    public SignerInformationStore getSignerInfos() throws CMSException {
        if (this._signerInfoStore == null) {
            this.populateCertCrlSets();
            final ArrayList<SignerInformation> list = new ArrayList<SignerInformation>();
            final HashMap<Object, Object> hashMap = new HashMap<Object, Object>();
            for (final Object next : this._digests.keySet()) {
                hashMap.put(next, ((MessageDigest)this._digests.get(next)).digest());
            }
            try {
                DEREncodable object;
                while ((object = this._signedData.getSignerInfos().readObject()) != null) {
                    final SignerInfo instance = SignerInfo.getInstance(object.getDERObject());
                    list.add(new SignerInformation(instance, this._signedContentType, null, new BaseDigestCalculator(hashMap.get(CMSSignedDataParser.HELPER.getDigestAlgName(instance.getDigestAlgorithm().getObjectId().getId())))));
                }
            }
            catch (IOException ex) {
                throw new CMSException("io exception: " + ex.getMessage(), ex);
            }
            this._signerInfoStore = new SignerInformationStore(list);
        }
        return this._signerInfoStore;
    }
    
    public X509Store getAttributeCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getAttributeCertificates(s, CMSUtils.getProvider(s2));
    }
    
    public X509Store getAttributeCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this._attributeStore == null) {
            this.populateCertCrlSets();
            this._attributeStore = CMSSignedDataParser.HELPER.createAttributeStore(s, provider, this._certSet);
        }
        return this._attributeStore;
    }
    
    public X509Store getCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCertificates(s, CMSUtils.getProvider(s2));
    }
    
    public X509Store getCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this._certificateStore == null) {
            this.populateCertCrlSets();
            this._certificateStore = CMSSignedDataParser.HELPER.createCertificateStore(s, provider, this._certSet);
        }
        return this._certificateStore;
    }
    
    public X509Store getCRLs(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCRLs(s, CMSUtils.getProvider(s2));
    }
    
    public X509Store getCRLs(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this._crlStore == null) {
            this.populateCertCrlSets();
            this._crlStore = CMSSignedDataParser.HELPER.createCRLsStore(s, provider, this._crlSet);
        }
        return this._crlStore;
    }
    
    public CertStore getCertificatesAndCRLs(final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.getCertificatesAndCRLs(s, CMSUtils.getProvider(s2));
    }
    
    public CertStore getCertificatesAndCRLs(final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        if (this._certStore == null) {
            this.populateCertCrlSets();
            this._certStore = CMSSignedDataParser.HELPER.createCertStore(s, provider, this._certSet, this._crlSet);
        }
        return this._certStore;
    }
    
    private void populateCertCrlSets() throws CMSException {
        if (this._isCertCrlParsed) {
            return;
        }
        this._isCertCrlParsed = true;
        try {
            this._certSet = getASN1Set(this._signedData.getCertificates());
            this._crlSet = getASN1Set(this._signedData.getCrls());
        }
        catch (IOException ex) {
            throw new CMSException("problem parsing cert/crl sets", ex);
        }
    }
    
    public String getSignedContentTypeOID() {
        return this._signedContentType.getId();
    }
    
    public CMSTypedStream getSignedContent() {
        if (this._signedContent != null) {
            InputStream contentStream = this._signedContent.getContentStream();
            final Iterator<MessageDigest> iterator = this._digests.values().iterator();
            while (iterator.hasNext()) {
                contentStream = new DigestInputStream(contentStream, iterator.next());
            }
            return new CMSTypedStream(this._signedContent.getContentType(), contentStream);
        }
        return null;
    }
    
    public static OutputStream replaceSigners(final InputStream inputStream, final SignerInformationStore signerInformationStore, final OutputStream outputStream) throws CMSException, IOException {
        final SignedDataParser instance = SignedDataParser.getInstance(new ContentInfoParser((ASN1SequenceParser)new ASN1StreamParser(inputStream, CMSUtils.getMaximumMemory()).readObject()).getContent(16));
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(instance.getVersion());
        instance.getDigestAlgorithms().getDERObject();
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<SignerInformation> iterator = signerInformationStore.getSigners().iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(iterator.next().getDigestAlgorithmID()));
        }
        berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
        final ContentInfoParser encapContentInfo = instance.getEncapContentInfo();
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(encapContentInfo.getContentType());
        final ASN1OctetStringParser asn1OctetStringParser = (ASN1OctetStringParser)encapContentInfo.getContent(4);
        if (asn1OctetStringParser != null) {
            pipeOctetString(asn1OctetStringParser, berSequenceGenerator3.getRawOutputStream());
        }
        berSequenceGenerator3.close();
        writeSetToGeneratorTagged(berSequenceGenerator2, instance.getCertificates(), 0);
        writeSetToGeneratorTagged(berSequenceGenerator2, instance.getCrls(), 1);
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        final Iterator<SignerInformation> iterator2 = signerInformationStore.getSigners().iterator();
        while (iterator2.hasNext()) {
            asn1EncodableVector2.add(iterator2.next().toSignerInfo());
        }
        berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector2).getEncoded());
        berSequenceGenerator2.close();
        berSequenceGenerator.close();
        return outputStream;
    }
    
    public static OutputStream replaceCertificatesAndCRLs(final InputStream inputStream, final CertStore certStore, final OutputStream outputStream) throws CMSException, IOException {
        final SignedDataParser instance = SignedDataParser.getInstance(new ContentInfoParser((ASN1SequenceParser)new ASN1StreamParser(inputStream, CMSUtils.getMaximumMemory()).readObject()).getContent(16));
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(instance.getVersion());
        berSequenceGenerator2.getRawOutputStream().write(instance.getDigestAlgorithms().getDERObject().getEncoded());
        final ContentInfoParser encapContentInfo = instance.getEncapContentInfo();
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(encapContentInfo.getContentType());
        final ASN1OctetStringParser asn1OctetStringParser = (ASN1OctetStringParser)encapContentInfo.getContent(4);
        if (asn1OctetStringParser != null) {
            pipeOctetString(asn1OctetStringParser, berSequenceGenerator3.getRawOutputStream());
        }
        berSequenceGenerator3.close();
        getASN1Set(instance.getCertificates());
        getASN1Set(instance.getCrls());
        ASN1Set berSetFromList;
        try {
            berSetFromList = CMSUtils.createBerSetFromList(CMSUtils.getCertificatesFromStore(certStore));
        }
        catch (CertStoreException ex) {
            throw new CMSException("error getting certs from certStore", ex);
        }
        if (berSetFromList.size() > 0) {
            berSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 0, berSetFromList).getEncoded());
        }
        ASN1Set berSetFromList2;
        try {
            berSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(certStore));
        }
        catch (CertStoreException ex2) {
            throw new CMSException("error getting crls from certStore", ex2);
        }
        if (berSetFromList2.size() > 0) {
            berSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 1, berSetFromList2).getEncoded());
        }
        berSequenceGenerator2.getRawOutputStream().write(instance.getSignerInfos().getDERObject().getEncoded());
        berSequenceGenerator2.close();
        berSequenceGenerator.close();
        return outputStream;
    }
    
    private static void writeSetToGeneratorTagged(final ASN1Generator asn1Generator, final ASN1SetParser asn1SetParser, final int n) throws IOException {
        final ASN1Set asn1Set = getASN1Set(asn1SetParser);
        if (asn1Set != null) {
            asn1Generator.getRawOutputStream().write(((asn1SetParser instanceof BERSetParser) ? new BERTaggedObject(false, n, asn1Set) : new DERTaggedObject(false, n, asn1Set)).getEncoded());
        }
    }
    
    private static ASN1Set getASN1Set(final ASN1SetParser asn1SetParser) {
        return (asn1SetParser == null) ? null : ASN1Set.getInstance(asn1SetParser.getDERObject());
    }
    
    private static void pipeOctetString(final ASN1OctetStringParser asn1OctetStringParser, final OutputStream outputStream) throws IOException {
        final OutputStream berOctetOutputStream = CMSUtils.createBEROctetOutputStream(outputStream, 0, true, 0);
        Streams.pipeAll(asn1OctetStringParser.getOctetStream(), berOctetOutputStream);
        berOctetOutputStream.close();
    }
    
    static {
        HELPER = CMSSignedHelper.INSTANCE;
    }
}
