// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import javax.crypto.Cipher;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientIdentifier;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.DEROctetString;
import java.security.Key;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.security.Provider;
import java.security.SecureRandom;
import javax.crypto.SecretKey;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Object;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1OctetString;
import java.security.PublicKey;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;

class KeyTransRecipientInfoGenerator implements RecipientInfoGenerator
{
    private TBSCertificateStructure recipientTBSCert;
    private PublicKey recipientPublicKey;
    private ASN1OctetString subjectKeyIdentifier;
    private SubjectPublicKeyInfo info;
    
    void setRecipientCert(final X509Certificate x509Certificate) {
        try {
            this.recipientTBSCert = CMSUtils.getTBSCertificateStructure(x509Certificate);
        }
        catch (CertificateEncodingException ex) {
            throw new IllegalArgumentException("can't extract TBS structure from this cert");
        }
        this.recipientPublicKey = x509Certificate.getPublicKey();
        this.info = this.recipientTBSCert.getSubjectPublicKeyInfo();
    }
    
    void setRecipientPublicKey(final PublicKey recipientPublicKey) {
        this.recipientPublicKey = recipientPublicKey;
        try {
            this.info = SubjectPublicKeyInfo.getInstance(ASN1Object.fromByteArray(recipientPublicKey.getEncoded()));
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("can't extract key algorithm from this key");
        }
    }
    
    void setSubjectKeyIdentifier(final ASN1OctetString subjectKeyIdentifier) {
        this.subjectKeyIdentifier = subjectKeyIdentifier;
    }
    
    public RecipientInfo generate(final SecretKey key, final SecureRandom secureRandom, final Provider provider) throws GeneralSecurityException {
        final AlgorithmIdentifier algorithmId = this.info.getAlgorithmId();
        final Cipher asymmetricCipher = CMSEnvelopedHelper.INSTANCE.createAsymmetricCipher(algorithmId.getObjectId().getId(), provider);
        DEROctetString derOctetString;
        try {
            asymmetricCipher.init(3, this.recipientPublicKey, secureRandom);
            derOctetString = new DEROctetString(asymmetricCipher.wrap(key));
        }
        catch (GeneralSecurityException ex) {
            asymmetricCipher.init(1, this.recipientPublicKey, secureRandom);
            derOctetString = new DEROctetString(asymmetricCipher.doFinal(key.getEncoded()));
        }
        catch (IllegalStateException ex2) {
            asymmetricCipher.init(1, this.recipientPublicKey, secureRandom);
            derOctetString = new DEROctetString(asymmetricCipher.doFinal(key.getEncoded()));
        }
        catch (UnsupportedOperationException ex3) {
            asymmetricCipher.init(1, this.recipientPublicKey, secureRandom);
            derOctetString = new DEROctetString(asymmetricCipher.doFinal(key.getEncoded()));
        }
        RecipientIdentifier recipientIdentifier;
        if (this.recipientTBSCert != null) {
            recipientIdentifier = new RecipientIdentifier(new IssuerAndSerialNumber(this.recipientTBSCert.getIssuer(), this.recipientTBSCert.getSerialNumber().getValue()));
        }
        else {
            recipientIdentifier = new RecipientIdentifier(this.subjectKeyIdentifier);
        }
        return new RecipientInfo(new KeyTransRecipientInfo(recipientIdentifier, algorithmId, derOctetString));
    }
}
