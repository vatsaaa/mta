// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.eac.EACObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import java.util.HashMap;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.cert.CRLException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertStoreParameters;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.CertStore;
import java.util.List;
import org.bouncycastle.x509.NoSuchStoreException;
import org.bouncycastle.asn1.DERObject;
import java.util.Enumeration;
import org.bouncycastle.x509.X509StoreParameters;
import java.util.Collection;
import org.bouncycastle.x509.X509CollectionStoreParameters;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.x509.X509V2AttributeCertificate;
import java.util.ArrayList;
import org.bouncycastle.x509.X509Store;
import org.bouncycastle.asn1.ASN1Set;
import java.security.Signature;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.security.Provider;
import org.bouncycastle.asn1.DERObjectIdentifier;
import java.util.Map;

class CMSSignedHelper
{
    static final CMSSignedHelper INSTANCE;
    private static final Map encryptionAlgs;
    private static final Map digestAlgs;
    private static final Map digestAliases;
    
    private static void addEntries(final DERObjectIdentifier derObjectIdentifier, final String s, final String s2) {
        CMSSignedHelper.digestAlgs.put(derObjectIdentifier.getId(), s);
        CMSSignedHelper.encryptionAlgs.put(derObjectIdentifier.getId(), s2);
    }
    
    String getDigestAlgName(final String s) {
        final String s2 = CMSSignedHelper.digestAlgs.get(s);
        if (s2 != null) {
            return s2;
        }
        return s;
    }
    
    String[] getDigestAliases(final String s) {
        final String[] array = CMSSignedHelper.digestAliases.get(s);
        if (array != null) {
            return array;
        }
        return new String[0];
    }
    
    String getEncryptionAlgName(final String s) {
        final String s2 = CMSSignedHelper.encryptionAlgs.get(s);
        if (s2 != null) {
            return s2;
        }
        return s;
    }
    
    MessageDigest getDigestInstance(final String s, final Provider provider) throws NoSuchAlgorithmException {
        try {
            return this.createDigestInstance(s, provider);
        }
        catch (NoSuchAlgorithmException ex) {
            final String[] digestAliases = this.getDigestAliases(s);
            int i = 0;
            while (i != digestAliases.length) {
                try {
                    return this.createDigestInstance(digestAliases[i], provider);
                }
                catch (NoSuchAlgorithmException ex2) {
                    ++i;
                    continue;
                }
                break;
            }
            if (provider != null) {
                return this.getDigestInstance(s, null);
            }
            throw ex;
        }
    }
    
    private MessageDigest createDigestInstance(final String s, final Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return MessageDigest.getInstance(s, provider);
        }
        return MessageDigest.getInstance(s);
    }
    
    Signature getSignatureInstance(final String s, final Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return Signature.getInstance(s, provider);
        }
        return Signature.getInstance(s);
    }
    
    X509Store createAttributeStore(final String str, final Provider provider, final ASN1Set set) throws NoSuchStoreException, CMSException {
        final ArrayList<X509V2AttributeCertificate> list = new ArrayList<X509V2AttributeCertificate>();
        if (set != null) {
            final Enumeration objects = set.getObjects();
            while (objects.hasMoreElements()) {
                try {
                    final DERObject derObject = objects.nextElement().getDERObject();
                    if (!(derObject instanceof ASN1TaggedObject)) {
                        continue;
                    }
                    final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)derObject;
                    if (asn1TaggedObject.getTagNo() != 2) {
                        continue;
                    }
                    list.add(new X509V2AttributeCertificate(ASN1Sequence.getInstance(asn1TaggedObject, false).getEncoded()));
                    continue;
                }
                catch (IOException ex) {
                    throw new CMSException("can't re-encode attribute certificate!", ex);
                }
                break;
            }
        }
        try {
            return X509Store.getInstance("AttributeCertificate/" + str, new X509CollectionStoreParameters(list), provider);
        }
        catch (IllegalArgumentException ex2) {
            throw new CMSException("can't setup the X509Store", ex2);
        }
    }
    
    X509Store createCertificateStore(final String str, final Provider provider, final ASN1Set set) throws NoSuchStoreException, CMSException {
        final ArrayList list = new ArrayList();
        if (set != null) {
            this.addCertsFromSet(list, set, provider);
        }
        try {
            return X509Store.getInstance("Certificate/" + str, new X509CollectionStoreParameters(list), provider);
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("can't setup the X509Store", ex);
        }
    }
    
    X509Store createCRLsStore(final String str, final Provider provider, final ASN1Set set) throws NoSuchStoreException, CMSException {
        final ArrayList list = new ArrayList();
        if (set != null) {
            this.addCRLsFromSet(list, set, provider);
        }
        try {
            return X509Store.getInstance("CRL/" + str, new X509CollectionStoreParameters(list), provider);
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("can't setup the X509Store", ex);
        }
    }
    
    CertStore createCertStore(final String s, final Provider provider, final ASN1Set set, final ASN1Set set2) throws CMSException, NoSuchAlgorithmException {
        final ArrayList<Object> list = new ArrayList<Object>();
        if (set != null) {
            this.addCertsFromSet(list, set, provider);
        }
        if (set2 != null) {
            this.addCRLsFromSet(list, set2, provider);
        }
        try {
            if (provider != null) {
                return CertStore.getInstance(s, new CollectionCertStoreParameters(list), provider);
            }
            return CertStore.getInstance(s, new CollectionCertStoreParameters(list));
        }
        catch (InvalidAlgorithmParameterException ex) {
            throw new CMSException("can't setup the CertStore", ex);
        }
    }
    
    private void addCertsFromSet(final List list, final ASN1Set set, final Provider provider) throws CMSException {
        CertificateFactory certificateFactory;
        try {
            if (provider != null) {
                certificateFactory = CertificateFactory.getInstance("X.509", provider);
            }
            else {
                certificateFactory = CertificateFactory.getInstance("X.509");
            }
        }
        catch (CertificateException ex) {
            throw new CMSException("can't get certificate factory.", ex);
        }
        final Enumeration objects = set.getObjects();
        while (objects.hasMoreElements()) {
            try {
                final DERObject derObject = objects.nextElement().getDERObject();
                if (!(derObject instanceof ASN1Sequence)) {
                    continue;
                }
                list.add(certificateFactory.generateCertificate(new ByteArrayInputStream(derObject.getEncoded())));
                continue;
            }
            catch (IOException ex2) {
                throw new CMSException("can't re-encode certificate!", ex2);
            }
            catch (CertificateException ex3) {
                throw new CMSException("can't re-encode certificate!", ex3);
            }
            break;
        }
    }
    
    private void addCRLsFromSet(final List list, final ASN1Set set, final Provider provider) throws CMSException {
        CertificateFactory certificateFactory;
        try {
            if (provider != null) {
                certificateFactory = CertificateFactory.getInstance("X.509", provider);
            }
            else {
                certificateFactory = CertificateFactory.getInstance("X.509");
            }
        }
        catch (CertificateException ex) {
            throw new CMSException("can't get certificate factory.", ex);
        }
        final Enumeration objects = set.getObjects();
        while (objects.hasMoreElements()) {
            try {
                list.add(certificateFactory.generateCRL(new ByteArrayInputStream(objects.nextElement().getDERObject().getEncoded())));
                continue;
            }
            catch (IOException ex2) {
                throw new CMSException("can't re-encode CRL!", ex2);
            }
            catch (CRLException ex3) {
                throw new CMSException("can't re-encode CRL!", ex3);
            }
            break;
        }
    }
    
    AlgorithmIdentifier fixAlgID(final AlgorithmIdentifier algorithmIdentifier) {
        if (algorithmIdentifier.getParameters() == null) {
            return new AlgorithmIdentifier(algorithmIdentifier.getObjectId(), DERNull.INSTANCE);
        }
        return algorithmIdentifier;
    }
    
    void setSigningEncryptionAlgorithmMapping(final DERObjectIdentifier derObjectIdentifier, final String s) {
        CMSSignedHelper.encryptionAlgs.put(derObjectIdentifier.getId(), s);
    }
    
    void setSigningDigestAlgorithmMapping(final DERObjectIdentifier derObjectIdentifier, final String s) {
        CMSSignedHelper.digestAlgs.put(derObjectIdentifier.getId(), s);
    }
    
    static {
        INSTANCE = new CMSSignedHelper();
        encryptionAlgs = new HashMap();
        digestAlgs = new HashMap();
        digestAliases = new HashMap();
        addEntries(NISTObjectIdentifiers.dsa_with_sha224, "SHA224", "DSA");
        addEntries(NISTObjectIdentifiers.dsa_with_sha256, "SHA256", "DSA");
        addEntries(NISTObjectIdentifiers.dsa_with_sha384, "SHA384", "DSA");
        addEntries(NISTObjectIdentifiers.dsa_with_sha512, "SHA512", "DSA");
        addEntries(OIWObjectIdentifiers.dsaWithSHA1, "SHA1", "DSA");
        addEntries(OIWObjectIdentifiers.md4WithRSA, "MD4", "RSA");
        addEntries(OIWObjectIdentifiers.md4WithRSAEncryption, "MD4", "RSA");
        addEntries(OIWObjectIdentifiers.md5WithRSA, "MD5", "RSA");
        addEntries(OIWObjectIdentifiers.sha1WithRSA, "SHA1", "RSA");
        addEntries(PKCSObjectIdentifiers.md2WithRSAEncryption, "MD2", "RSA");
        addEntries(PKCSObjectIdentifiers.md4WithRSAEncryption, "MD4", "RSA");
        addEntries(PKCSObjectIdentifiers.md5WithRSAEncryption, "MD5", "RSA");
        addEntries(PKCSObjectIdentifiers.sha1WithRSAEncryption, "SHA1", "RSA");
        addEntries(PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224", "RSA");
        addEntries(PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256", "RSA");
        addEntries(PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384", "RSA");
        addEntries(PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512", "RSA");
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA1, "SHA1", "ECDSA");
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA224, "SHA224", "ECDSA");
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA256, "SHA256", "ECDSA");
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA384, "SHA384", "ECDSA");
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA512, "SHA512", "ECDSA");
        addEntries(X9ObjectIdentifiers.id_dsa_with_sha1, "SHA1", "DSA");
        addEntries(EACObjectIdentifiers.id_TA_ECDSA_SHA_1, "SHA1", "ECDSA");
        addEntries(EACObjectIdentifiers.id_TA_ECDSA_SHA_224, "SHA224", "ECDSA");
        addEntries(EACObjectIdentifiers.id_TA_ECDSA_SHA_256, "SHA256", "ECDSA");
        addEntries(EACObjectIdentifiers.id_TA_ECDSA_SHA_384, "SHA384", "ECDSA");
        addEntries(EACObjectIdentifiers.id_TA_ECDSA_SHA_512, "SHA512", "ECDSA");
        addEntries(EACObjectIdentifiers.id_TA_RSA_v1_5_SHA_1, "SHA1", "RSA");
        addEntries(EACObjectIdentifiers.id_TA_RSA_v1_5_SHA_256, "SHA256", "RSA");
        addEntries(EACObjectIdentifiers.id_TA_RSA_PSS_SHA_1, "SHA1", "RSAandMGF1");
        addEntries(EACObjectIdentifiers.id_TA_RSA_PSS_SHA_256, "SHA256", "RSAandMGF1");
        CMSSignedHelper.encryptionAlgs.put(X9ObjectIdentifiers.id_dsa.getId(), "DSA");
        CMSSignedHelper.encryptionAlgs.put(PKCSObjectIdentifiers.rsaEncryption.getId(), "RSA");
        CMSSignedHelper.encryptionAlgs.put("1.3.36.3.3.1", "RSA");
        CMSSignedHelper.encryptionAlgs.put(X509ObjectIdentifiers.id_ea_rsa.getId(), "RSA");
        CMSSignedHelper.encryptionAlgs.put(CMSSignedDataGenerator.ENCRYPTION_RSA_PSS, "RSAandMGF1");
        CMSSignedHelper.encryptionAlgs.put(CryptoProObjectIdentifiers.gostR3410_94.getId(), "GOST3410");
        CMSSignedHelper.encryptionAlgs.put(CryptoProObjectIdentifiers.gostR3410_2001.getId(), "ECGOST3410");
        CMSSignedHelper.encryptionAlgs.put("1.3.6.1.4.1.5849.1.6.2", "ECGOST3410");
        CMSSignedHelper.encryptionAlgs.put("1.3.6.1.4.1.5849.1.1.5", "GOST3410");
        CMSSignedHelper.digestAlgs.put(PKCSObjectIdentifiers.md2.getId(), "MD2");
        CMSSignedHelper.digestAlgs.put(PKCSObjectIdentifiers.md4.getId(), "MD4");
        CMSSignedHelper.digestAlgs.put(PKCSObjectIdentifiers.md5.getId(), "MD5");
        CMSSignedHelper.digestAlgs.put(OIWObjectIdentifiers.idSHA1.getId(), "SHA1");
        CMSSignedHelper.digestAlgs.put(NISTObjectIdentifiers.id_sha224.getId(), "SHA224");
        CMSSignedHelper.digestAlgs.put(NISTObjectIdentifiers.id_sha256.getId(), "SHA256");
        CMSSignedHelper.digestAlgs.put(NISTObjectIdentifiers.id_sha384.getId(), "SHA384");
        CMSSignedHelper.digestAlgs.put(NISTObjectIdentifiers.id_sha512.getId(), "SHA512");
        CMSSignedHelper.digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd128.getId(), "RIPEMD128");
        CMSSignedHelper.digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd160.getId(), "RIPEMD160");
        CMSSignedHelper.digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd256.getId(), "RIPEMD256");
        CMSSignedHelper.digestAlgs.put(CryptoProObjectIdentifiers.gostR3411.getId(), "GOST3411");
        CMSSignedHelper.digestAlgs.put("1.3.6.1.4.1.5849.1.2.1", "GOST3411");
        CMSSignedHelper.digestAliases.put("SHA1", new String[] { "SHA-1" });
        CMSSignedHelper.digestAliases.put("SHA224", new String[] { "SHA-224" });
        CMSSignedHelper.digestAliases.put("SHA256", new String[] { "SHA-256" });
        CMSSignedHelper.digestAliases.put("SHA384", new String[] { "SHA-384" });
        CMSSignedHelper.digestAliases.put("SHA512", new String[] { "SHA-512" });
    }
}
