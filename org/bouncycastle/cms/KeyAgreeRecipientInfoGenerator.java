// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientEncryptedKey;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientIdentifier;
import java.security.Key;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import org.bouncycastle.asn1.cms.OriginatorIdentifierOrKey;
import org.bouncycastle.asn1.DERObjectIdentifier;

class KeyAgreeRecipientInfoGenerator implements RecipientInfoGenerator
{
    private DERObjectIdentifier algorithmOID;
    private OriginatorIdentifierOrKey originator;
    private TBSCertificateStructure recipientTBSCert;
    private ASN1OctetString ukm;
    private DERObjectIdentifier wrapAlgorithmOID;
    private SecretKey wrapKey;
    
    void setAlgorithmOID(final DERObjectIdentifier algorithmOID) {
        this.algorithmOID = algorithmOID;
    }
    
    void setOriginator(final OriginatorIdentifierOrKey originator) {
        this.originator = originator;
    }
    
    void setRecipientCert(final X509Certificate x509Certificate) {
        try {
            this.recipientTBSCert = CMSUtils.getTBSCertificateStructure(x509Certificate);
        }
        catch (CertificateEncodingException ex) {
            throw new IllegalArgumentException("can't extract TBS structure from this cert");
        }
    }
    
    void setUKM(final ASN1OctetString ukm) {
        this.ukm = ukm;
    }
    
    void setWrapAlgorithmOID(final DERObjectIdentifier wrapAlgorithmOID) {
        this.wrapAlgorithmOID = wrapAlgorithmOID;
    }
    
    void setWrapKey(final SecretKey wrapKey) {
        this.wrapKey = wrapKey;
    }
    
    public RecipientInfo generate(final SecretKey key, final SecureRandom random, final Provider provider) throws GeneralSecurityException {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.wrapAlgorithmOID);
        asn1EncodableVector.add(DERNull.INSTANCE);
        final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(this.algorithmOID, new DERSequence(asn1EncodableVector));
        final IssuerAndSerialNumber issuerAndSerialNumber = new IssuerAndSerialNumber(this.recipientTBSCert.getIssuer(), this.recipientTBSCert.getSerialNumber().getValue());
        final Cipher asymmetricCipher = CMSEnvelopedHelper.INSTANCE.createAsymmetricCipher(this.wrapAlgorithmOID.getId(), provider);
        asymmetricCipher.init(3, this.wrapKey, random);
        return new RecipientInfo(new KeyAgreeRecipientInfo(this.originator, this.ukm, algorithmIdentifier, new DERSequence(new RecipientEncryptedKey(new KeyAgreeRecipientIdentifier(issuerAndSerialNumber), new DEROctetString(asymmetricCipher.wrap(key))))));
    }
}
