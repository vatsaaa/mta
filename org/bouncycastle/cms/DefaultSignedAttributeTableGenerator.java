// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.cms.Time;
import java.util.Date;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSAttributes;
import java.util.Map;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.util.Hashtable;

public class DefaultSignedAttributeTableGenerator implements CMSAttributeTableGenerator
{
    private final Hashtable table;
    
    public DefaultSignedAttributeTableGenerator() {
        this.table = new Hashtable();
    }
    
    public DefaultSignedAttributeTableGenerator(final AttributeTable attributeTable) {
        if (attributeTable != null) {
            this.table = attributeTable.toHashtable();
        }
        else {
            this.table = new Hashtable();
        }
    }
    
    protected Hashtable createStandardAttributeTable(final Map map) {
        final Hashtable hashtable = (Hashtable)this.table.clone();
        if (!hashtable.containsKey(CMSAttributes.contentType)) {
            final Attribute value = new Attribute(CMSAttributes.contentType, new DERSet(map.get("contentType")));
            hashtable.put(value.getAttrType(), value);
        }
        if (!hashtable.containsKey(CMSAttributes.signingTime)) {
            final Attribute value2 = new Attribute(CMSAttributes.signingTime, new DERSet(new Time(new Date())));
            hashtable.put(value2.getAttrType(), value2);
        }
        if (!hashtable.containsKey(CMSAttributes.messageDigest)) {
            final Attribute value3 = new Attribute(CMSAttributes.messageDigest, new DERSet(new DEROctetString((byte[])(Object)map.get("digest"))));
            hashtable.put(value3.getAttrType(), value3);
        }
        return hashtable;
    }
    
    public AttributeTable getAttributes(final Map map) {
        return new AttributeTable(this.createStandardAttributeTable(map));
    }
}
