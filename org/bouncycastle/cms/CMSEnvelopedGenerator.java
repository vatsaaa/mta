// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.cms.OriginatorPublicKey;
import javax.crypto.spec.RC2ParameterSpec;
import java.security.AlgorithmParameterGenerator;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Object;
import java.security.AlgorithmParameters;
import java.security.KeyPair;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.asn1.DERObjectIdentifier;
import java.security.Key;
import javax.crypto.KeyAgreement;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.jce.spec.MQVPrivateKeySpec;
import org.bouncycastle.jce.spec.MQVPublicKeySpec;
import org.bouncycastle.asn1.cms.ecc.MQVuserKeyingMaterial;
import java.security.spec.AlgorithmParameterSpec;
import java.security.KeyPairGenerator;
import java.security.interfaces.ECPublicKey;
import java.io.IOException;
import org.bouncycastle.asn1.cms.OriginatorIdentifierOrKey;
import java.security.Provider;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.cms.OtherKeyAttribute;
import org.bouncycastle.asn1.DERGeneralizedTime;
import org.bouncycastle.asn1.cms.KEKIdentifier;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DEROctetString;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.security.SecureRandom;
import java.util.List;

public class CMSEnvelopedGenerator
{
    public static final String DES_EDE3_CBC;
    public static final String RC2_CBC;
    public static final String IDEA_CBC = "1.3.6.1.4.1.188.7.1.1.2";
    public static final String CAST5_CBC = "1.2.840.113533.7.66.10";
    public static final String AES128_CBC;
    public static final String AES192_CBC;
    public static final String AES256_CBC;
    public static final String CAMELLIA128_CBC;
    public static final String CAMELLIA192_CBC;
    public static final String CAMELLIA256_CBC;
    public static final String SEED_CBC;
    public static final String DES_EDE3_WRAP;
    public static final String AES128_WRAP;
    public static final String AES192_WRAP;
    public static final String AES256_WRAP;
    public static final String CAMELLIA128_WRAP;
    public static final String CAMELLIA192_WRAP;
    public static final String CAMELLIA256_WRAP;
    public static final String SEED_WRAP;
    public static final String ECDH_SHA1KDF;
    public static final String ECMQV_SHA1KDF;
    final List recipientInfoGenerators;
    final SecureRandom rand;
    
    public CMSEnvelopedGenerator() {
        this(new SecureRandom());
    }
    
    public CMSEnvelopedGenerator(final SecureRandom rand) {
        this.recipientInfoGenerators = new ArrayList();
        this.rand = rand;
    }
    
    public void addKeyTransRecipient(final X509Certificate recipientCert) throws IllegalArgumentException {
        final KeyTransRecipientInfoGenerator keyTransRecipientInfoGenerator = new KeyTransRecipientInfoGenerator();
        keyTransRecipientInfoGenerator.setRecipientCert(recipientCert);
        this.recipientInfoGenerators.add(keyTransRecipientInfoGenerator);
    }
    
    public void addKeyTransRecipient(final PublicKey recipientPublicKey, final byte[] array) throws IllegalArgumentException {
        final KeyTransRecipientInfoGenerator keyTransRecipientInfoGenerator = new KeyTransRecipientInfoGenerator();
        keyTransRecipientInfoGenerator.setRecipientPublicKey(recipientPublicKey);
        keyTransRecipientInfoGenerator.setSubjectKeyIdentifier(new DEROctetString(array));
        this.recipientInfoGenerators.add(keyTransRecipientInfoGenerator);
    }
    
    public void addKEKRecipient(final SecretKey wrapKey, final byte[] array) {
        final KEKRecipientInfoGenerator kekRecipientInfoGenerator = new KEKRecipientInfoGenerator();
        kekRecipientInfoGenerator.setKEKIdentifier(new KEKIdentifier(array, null, null));
        kekRecipientInfoGenerator.setWrapKey(wrapKey);
        this.recipientInfoGenerators.add(kekRecipientInfoGenerator);
    }
    
    public void addPasswordRecipient(final CMSPBEKey cmspbeKey, final String algorithm) {
        final PBKDF2Params pbkdf2Params = new PBKDF2Params(cmspbeKey.getSalt(), cmspbeKey.getIterationCount());
        final PasswordRecipientInfoGenerator passwordRecipientInfoGenerator = new PasswordRecipientInfoGenerator();
        passwordRecipientInfoGenerator.setDerivationAlg(new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBKDF2, pbkdf2Params));
        passwordRecipientInfoGenerator.setWrapKey(new SecretKeySpec(cmspbeKey.getEncoded(algorithm), algorithm));
        this.recipientInfoGenerators.add(passwordRecipientInfoGenerator);
    }
    
    public void addKeyAgreementRecipient(final String s, final PrivateKey privateKey, final PublicKey publicKey, final X509Certificate x509Certificate, final String s2, final String s3) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        this.addKeyAgreementRecipient(s, privateKey, publicKey, x509Certificate, s2, CMSUtils.getProvider(s3));
    }
    
    public void addKeyAgreementRecipient(final String s, PrivateKey key, final PublicKey publicKey, final X509Certificate recipientCert, final String algorithm, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        OriginatorIdentifierOrKey originator;
        try {
            originator = new OriginatorIdentifierOrKey(createOriginatorPublicKey(publicKey));
        }
        catch (IOException obj) {
            throw new InvalidKeyException("cannot extract originator public key: " + obj);
        }
        ASN1OctetString ukm = null;
        PublicKey publicKey2 = recipientCert.getPublicKey();
        if (s.equals(CMSEnvelopedGenerator.ECMQV_SHA1KDF)) {
            try {
                final ECParameterSpec params = ((ECPublicKey)publicKey).getParams();
                final KeyPairGenerator instance = KeyPairGenerator.getInstance(s, provider);
                instance.initialize(params, this.rand);
                final KeyPair generateKeyPair = instance.generateKeyPair();
                ukm = new DEROctetString(new MQVuserKeyingMaterial(createOriginatorPublicKey(generateKeyPair.getPublic()), null));
                publicKey2 = new MQVPublicKeySpec(publicKey2, publicKey2);
                key = new MQVPrivateKeySpec(key, generateKeyPair.getPrivate(), generateKeyPair.getPublic());
            }
            catch (InvalidAlgorithmParameterException obj2) {
                throw new InvalidKeyException("cannot determine MQV ephemeral key pair parameters from public key: " + obj2);
            }
            catch (IOException obj3) {
                throw new InvalidKeyException("cannot extract MQV ephemeral public key: " + obj3);
            }
        }
        final KeyAgreement instance2 = KeyAgreement.getInstance(s, provider);
        instance2.init(key, this.rand);
        instance2.doPhase(publicKey2, true);
        final SecretKey generateSecret = instance2.generateSecret(algorithm);
        final KeyAgreeRecipientInfoGenerator keyAgreeRecipientInfoGenerator = new KeyAgreeRecipientInfoGenerator();
        keyAgreeRecipientInfoGenerator.setAlgorithmOID(new DERObjectIdentifier(s));
        keyAgreeRecipientInfoGenerator.setOriginator(originator);
        keyAgreeRecipientInfoGenerator.setRecipientCert(recipientCert);
        keyAgreeRecipientInfoGenerator.setUKM(ukm);
        keyAgreeRecipientInfoGenerator.setWrapKey(generateSecret);
        keyAgreeRecipientInfoGenerator.setWrapAlgorithmOID(new DERObjectIdentifier(algorithm));
        this.recipientInfoGenerators.add(keyAgreeRecipientInfoGenerator);
    }
    
    protected AlgorithmIdentifier getAlgorithmIdentifier(final String s, final AlgorithmParameters algorithmParameters) throws IOException {
        ASN1Object asn1Object;
        if (algorithmParameters != null) {
            asn1Object = ASN1Object.fromByteArray(algorithmParameters.getEncoded("ASN.1"));
        }
        else {
            asn1Object = DERNull.INSTANCE;
        }
        return new AlgorithmIdentifier(new DERObjectIdentifier(s), asn1Object);
    }
    
    protected AlgorithmParameters generateParameters(final String algorithm, final SecretKey secretKey, final Provider provider) throws CMSException {
        try {
            final AlgorithmParameterGenerator instance = AlgorithmParameterGenerator.getInstance(algorithm, provider);
            if (algorithm.equals(CMSEnvelopedGenerator.RC2_CBC)) {
                final byte[] array = new byte[8];
                this.rand.nextBytes(array);
                try {
                    instance.init(new RC2ParameterSpec(secretKey.getEncoded().length * 8, array), this.rand);
                }
                catch (InvalidAlgorithmParameterException obj) {
                    throw new CMSException("parameters generation error: " + obj, obj);
                }
            }
            return instance.generateParameters();
        }
        catch (NoSuchAlgorithmException ex) {
            return null;
        }
    }
    
    private static OriginatorPublicKey createOriginatorPublicKey(final PublicKey publicKey) throws IOException {
        final SubjectPublicKeyInfo instance = SubjectPublicKeyInfo.getInstance(ASN1Object.fromByteArray(publicKey.getEncoded()));
        return new OriginatorPublicKey(new AlgorithmIdentifier(instance.getAlgorithmId().getObjectId(), DERNull.INSTANCE), instance.getPublicKeyData().getBytes());
    }
    
    static {
        DES_EDE3_CBC = PKCSObjectIdentifiers.des_EDE3_CBC.getId();
        RC2_CBC = PKCSObjectIdentifiers.RC2_CBC.getId();
        AES128_CBC = NISTObjectIdentifiers.id_aes128_CBC.getId();
        AES192_CBC = NISTObjectIdentifiers.id_aes192_CBC.getId();
        AES256_CBC = NISTObjectIdentifiers.id_aes256_CBC.getId();
        CAMELLIA128_CBC = NTTObjectIdentifiers.id_camellia128_cbc.getId();
        CAMELLIA192_CBC = NTTObjectIdentifiers.id_camellia192_cbc.getId();
        CAMELLIA256_CBC = NTTObjectIdentifiers.id_camellia256_cbc.getId();
        SEED_CBC = KISAObjectIdentifiers.id_seedCBC.getId();
        DES_EDE3_WRAP = PKCSObjectIdentifiers.id_alg_CMS3DESwrap.getId();
        AES128_WRAP = NISTObjectIdentifiers.id_aes128_wrap.getId();
        AES192_WRAP = NISTObjectIdentifiers.id_aes192_wrap.getId();
        AES256_WRAP = NISTObjectIdentifiers.id_aes256_wrap.getId();
        CAMELLIA128_WRAP = NTTObjectIdentifiers.id_camellia128_wrap.getId();
        CAMELLIA192_WRAP = NTTObjectIdentifiers.id_camellia192_wrap.getId();
        CAMELLIA256_WRAP = NTTObjectIdentifiers.id_camellia256_wrap.getId();
        SEED_WRAP = KISAObjectIdentifiers.id_npki_app_cmsSeed_wrap.getId();
        ECDH_SHA1KDF = X9ObjectIdentifiers.dhSinglePass_stdDH_sha1kdf_scheme.getId();
        ECMQV_SHA1KDF = X9ObjectIdentifiers.mqvSinglePass_sha1kdf_scheme.getId();
    }
}
