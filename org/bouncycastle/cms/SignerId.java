// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.util.Arrays;
import java.security.cert.X509CertSelector;

public class SignerId extends X509CertSelector
{
    @Override
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.getSubjectKeyIdentifier());
        if (this.getSerialNumber() != null) {
            hashCode ^= this.getSerialNumber().hashCode();
        }
        if (this.getIssuerAsString() != null) {
            hashCode ^= this.getIssuerAsString().hashCode();
        }
        return hashCode;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof SignerId)) {
            return false;
        }
        final SignerId signerId = (SignerId)o;
        return Arrays.areEqual(this.getSubjectKeyIdentifier(), signerId.getSubjectKeyIdentifier()) && this.equalsObj(this.getSerialNumber(), signerId.getSerialNumber()) && this.equalsObj(this.getIssuerAsString(), signerId.getIssuerAsString());
    }
    
    private boolean equalsObj(final Object o, final Object obj) {
        return (o != null) ? o.equals(obj) : (obj == null);
    }
}
