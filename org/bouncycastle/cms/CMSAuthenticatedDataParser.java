// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.util.Arrays;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1SetParser;
import java.util.Collection;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1OctetStringParser;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1SequenceParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.AuthenticatedDataParser;

public class CMSAuthenticatedDataParser extends CMSContentInfoParser
{
    RecipientInformationStore _recipientInfoStore;
    AuthenticatedDataParser authData;
    private AlgorithmIdentifier macAlg;
    private byte[] mac;
    private AttributeTable authAttrs;
    private AttributeTable unauthAttrs;
    private boolean authAttrNotRead;
    private boolean unauthAttrNotRead;
    
    public CMSAuthenticatedDataParser(final byte[] buf) throws CMSException, IOException {
        this(new ByteArrayInputStream(buf));
    }
    
    public CMSAuthenticatedDataParser(final InputStream inputStream) throws CMSException, IOException {
        super(inputStream);
        this.authAttrNotRead = true;
        this.authData = new AuthenticatedDataParser((ASN1SequenceParser)this._contentInfo.getContent(16));
        final ASN1SetParser recipientInfos = this.authData.getRecipientInfos();
        final ArrayList<Object> list = new ArrayList<Object>();
        DEREncodable object;
        while ((object = recipientInfos.readObject()) != null) {
            list.add(RecipientInfo.getInstance(object.getDERObject()));
        }
        this.macAlg = this.authData.getMacAlgorithm();
        this._recipientInfoStore = new RecipientInformationStore(CMSEnvelopedHelper.readRecipientInfos(list.iterator(), ((ASN1OctetStringParser)this.authData.getEnapsulatedContentInfo().getContent(4)).getOctetStream(), null, this.macAlg, null));
    }
    
    public String getMacAlgOID() {
        return this.macAlg.getObjectId().toString();
    }
    
    public byte[] getMacAlgParams() {
        try {
            return this.encodeObj(this.macAlg.getParameters());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AlgorithmParameters getMacAlgorithmParameters(final String s) throws CMSException, NoSuchProviderException {
        return this.getMacAlgorithmParameters(CMSUtils.getProvider(s));
    }
    
    public AlgorithmParameters getMacAlgorithmParameters(final Provider provider) throws CMSException {
        return CMSEnvelopedHelper.INSTANCE.getEncryptionAlgorithmParameters(this.getMacAlgOID(), this.getMacAlgParams(), provider);
    }
    
    public RecipientInformationStore getRecipientInfos() {
        return this._recipientInfoStore;
    }
    
    public byte[] getMac() throws IOException {
        if (this.mac == null) {
            this.getAuthAttrs();
            this.mac = this.authData.getMac().getOctets();
        }
        return Arrays.clone(this.mac);
    }
    
    public AttributeTable getAuthAttrs() throws IOException {
        if (this.authAttrs == null && this.authAttrNotRead) {
            final ASN1SetParser authAttrs = this.authData.getAuthAttrs();
            this.authAttrNotRead = false;
            if (authAttrs != null) {
                final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
                DEREncodable object;
                while ((object = authAttrs.readObject()) != null) {
                    asn1EncodableVector.add(((ASN1SequenceParser)object).getDERObject());
                }
                this.authAttrs = new AttributeTable(new DERSet(asn1EncodableVector));
            }
        }
        return this.authAttrs;
    }
    
    public AttributeTable getUnauthAttrs() throws IOException {
        if (this.unauthAttrs == null && this.unauthAttrNotRead) {
            final ASN1SetParser unauthAttrs = this.authData.getUnauthAttrs();
            this.unauthAttrNotRead = false;
            if (unauthAttrs != null) {
                final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
                DEREncodable object;
                while ((object = unauthAttrs.readObject()) != null) {
                    asn1EncodableVector.add(((ASN1SequenceParser)object).getDERObject());
                }
                this.unauthAttrs = new AttributeTable(new DERSet(asn1EncodableVector));
            }
        }
        return this.unauthAttrs;
    }
    
    private byte[] encodeObj(final DEREncodable derEncodable) throws IOException {
        if (derEncodable != null) {
            return derEncodable.getDERObject().getEncoded();
        }
        return null;
    }
}
