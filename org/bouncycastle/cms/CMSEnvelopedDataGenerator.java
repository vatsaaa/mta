// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.AlgorithmParameters;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.OriginatorInfo;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.EncryptedContentInfo;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.DEREncodable;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import org.bouncycastle.asn1.BERConstructedOctetString;
import java.io.OutputStream;
import javax.crypto.CipherOutputStream;
import java.io.ByteArrayOutputStream;
import java.security.Key;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Provider;
import javax.crypto.KeyGenerator;
import java.security.SecureRandom;

public class CMSEnvelopedDataGenerator extends CMSEnvelopedGenerator
{
    public CMSEnvelopedDataGenerator() {
    }
    
    public CMSEnvelopedDataGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    private CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final KeyGenerator keyGenerator, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final Provider provider2 = keyGenerator.getProvider();
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        SecretKey generateKey;
        AlgorithmIdentifier algorithmIdentifier;
        BERConstructedOctetString berConstructedOctetString;
        try {
            final Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(s, provider2);
            generateKey = keyGenerator.generateKey();
            AlgorithmParameters params = this.generateParameters(s, generateKey, provider2);
            symmetricCipher.init(1, generateKey, params, this.rand);
            if (params == null) {
                params = symmetricCipher.getParameters();
            }
            algorithmIdentifier = this.getAlgorithmIdentifier(s, params);
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            final CipherOutputStream cipherOutputStream = new CipherOutputStream(os, symmetricCipher);
            cmsProcessable.write(cipherOutputStream);
            cipherOutputStream.close();
            berConstructedOctetString = new BERConstructedOctetString(os.toByteArray());
        }
        catch (InvalidKeyException ex) {
            throw new CMSException("key invalid in message.", ex);
        }
        catch (NoSuchPaddingException ex2) {
            throw new CMSException("required padding not supported.", ex2);
        }
        catch (InvalidAlgorithmParameterException ex3) {
            throw new CMSException("algorithm parameters invalid.", ex3);
        }
        catch (IOException ex4) {
            throw new CMSException("exception decoding algorithm parameters.", ex4);
        }
        for (final RecipientInfoGenerator recipientInfoGenerator : this.recipientInfoGenerators) {
            try {
                asn1EncodableVector.add(recipientInfoGenerator.generate(generateKey, this.rand, provider));
            }
            catch (InvalidKeyException ex5) {
                throw new CMSException("key inappropriate for algorithm.", ex5);
            }
            catch (GeneralSecurityException ex6) {
                throw new CMSException("error making encrypted content.", ex6);
            }
        }
        return new CMSEnvelopedData(new ContentInfo(CMSObjectIdentifiers.envelopedData, new EnvelopedData(null, new DERSet(asn1EncodableVector), new EncryptedContentInfo(CMSObjectIdentifiers.data, algorithmIdentifier, berConstructedOctetString), null)));
    }
    
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, s, CMSUtils.getProvider(s2));
    }
    
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(this.rand);
        return this.generate(cmsProcessable, s, symmetricKeyGenerator, provider);
    }
    
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final int n, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, s, n, CMSUtils.getProvider(s2));
    }
    
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final int keysize, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(keysize, this.rand);
        return this.generate(cmsProcessable, s, symmetricKeyGenerator, provider);
    }
}
