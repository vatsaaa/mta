// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DEROctetString;
import java.security.Key;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.security.Provider;
import java.security.SecureRandom;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

class PasswordRecipientInfoGenerator implements RecipientInfoGenerator
{
    private AlgorithmIdentifier derivationAlg;
    private SecretKey wrapKey;
    
    void setDerivationAlg(final AlgorithmIdentifier derivationAlg) {
        this.derivationAlg = derivationAlg;
    }
    
    void setWrapKey(final SecretKey wrapKey) {
        this.wrapKey = wrapKey;
    }
    
    public RecipientInfo generate(final SecretKey key, final SecureRandom random, final Provider provider) throws GeneralSecurityException {
        final CMSEnvelopedHelper instance = CMSEnvelopedHelper.INSTANCE;
        final Cipher asymmetricCipher = instance.createAsymmetricCipher(instance.getRFC3211WrapperName(this.wrapKey.getAlgorithm()), provider);
        asymmetricCipher.init(3, this.wrapKey, random);
        final DEROctetString derOctetString = new DEROctetString(asymmetricCipher.wrap(key));
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(new DERObjectIdentifier(this.wrapKey.getAlgorithm()));
        asn1EncodableVector.add(new DEROctetString(asymmetricCipher.getIV()));
        return new RecipientInfo(new PasswordRecipientInfo(this.derivationAlg, new AlgorithmIdentifier(PKCSObjectIdentifiers.id_alg_PWRI_KEK, new DERSequence(asn1EncodableVector)), derOctetString));
    }
}
