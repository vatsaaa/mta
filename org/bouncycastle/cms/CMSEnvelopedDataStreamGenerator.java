// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import javax.crypto.Cipher;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.CipherOutputStream;
import java.security.Key;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.BERSequenceGenerator;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.security.AlgorithmParameters;
import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Provider;
import javax.crypto.KeyGenerator;
import java.io.OutputStream;
import org.bouncycastle.asn1.DERInteger;
import java.security.SecureRandom;

public class CMSEnvelopedDataStreamGenerator extends CMSEnvelopedGenerator
{
    private Object _originatorInfo;
    private Object _unprotectedAttributes;
    private int _bufferSize;
    private boolean _berEncodeRecipientSet;
    
    public CMSEnvelopedDataStreamGenerator() {
        this._originatorInfo = null;
        this._unprotectedAttributes = null;
    }
    
    public CMSEnvelopedDataStreamGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
        this._originatorInfo = null;
        this._unprotectedAttributes = null;
    }
    
    public void setBufferSize(final int bufferSize) {
        this._bufferSize = bufferSize;
    }
    
    public void setBEREncodeRecipients(final boolean berEncodeRecipientSet) {
        this._berEncodeRecipientSet = berEncodeRecipientSet;
    }
    
    private DERInteger getVersion() {
        if (this._originatorInfo != null || this._unprotectedAttributes != null) {
            return new DERInteger(2);
        }
        return new DERInteger(0);
    }
    
    private OutputStream open(final OutputStream outputStream, final String s, final KeyGenerator keyGenerator, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final Provider provider2 = keyGenerator.getProvider();
        final SecretKey generateKey = keyGenerator.generateKey();
        final AlgorithmParameters generateParameters = this.generateParameters(s, generateKey, provider2);
        final Iterator<RecipientInfoGenerator> iterator = this.recipientInfoGenerators.iterator();
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        while (iterator.hasNext()) {
            final RecipientInfoGenerator recipientInfoGenerator = iterator.next();
            try {
                asn1EncodableVector.add(recipientInfoGenerator.generate(generateKey, this.rand, provider));
            }
            catch (InvalidKeyException ex) {
                throw new CMSException("key inappropriate for algorithm.", ex);
            }
            catch (GeneralSecurityException ex2) {
                throw new CMSException("error making encrypted content.", ex2);
            }
        }
        return this.open(outputStream, s, generateKey, generateParameters, asn1EncodableVector, provider2);
    }
    
    protected OutputStream open(final OutputStream outputStream, final String s, final SecretKey secretKey, final AlgorithmParameters algorithmParameters, final ASN1EncodableVector asn1EncodableVector, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.open(outputStream, s, secretKey, algorithmParameters, asn1EncodableVector, CMSUtils.getProvider(s2));
    }
    
    protected OutputStream open(final OutputStream outputStream, final String s, final SecretKey key, AlgorithmParameters parameters, final ASN1EncodableVector asn1EncodableVector, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        try {
            final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
            berSequenceGenerator.addObject(CMSObjectIdentifiers.envelopedData);
            final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
            berSequenceGenerator2.addObject(this.getVersion());
            if (this._berEncodeRecipientSet) {
                berSequenceGenerator2.getRawOutputStream().write(new BERSet(asn1EncodableVector).getEncoded());
            }
            else {
                berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
            }
            final Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(s, provider);
            symmetricCipher.init(1, key, parameters, this.rand);
            final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
            berSequenceGenerator3.addObject(CMSObjectIdentifiers.data);
            if (parameters == null) {
                parameters = symmetricCipher.getParameters();
            }
            berSequenceGenerator3.getRawOutputStream().write(this.getAlgorithmIdentifier(s, parameters).getEncoded());
            return new CmsEnvelopedDataOutputStream(new CipherOutputStream(CMSUtils.createBEROctetOutputStream(berSequenceGenerator3.getRawOutputStream(), 0, false, this._bufferSize), symmetricCipher), berSequenceGenerator, berSequenceGenerator2, berSequenceGenerator3);
        }
        catch (InvalidKeyException ex) {
            throw new CMSException("key invalid in message.", ex);
        }
        catch (NoSuchPaddingException ex2) {
            throw new CMSException("required padding not supported.", ex2);
        }
        catch (InvalidAlgorithmParameterException ex3) {
            throw new CMSException("algorithm parameters invalid.", ex3);
        }
        catch (IOException ex4) {
            throw new CMSException("exception decoding algorithm parameters.", ex4);
        }
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        return this.open(outputStream, s, CMSUtils.getProvider(s2));
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException, IOException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(this.rand);
        return this.open(outputStream, s, symmetricKeyGenerator, provider);
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final int n, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        return this.open(outputStream, s, n, CMSUtils.getProvider(s2));
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final int keysize, final Provider provider) throws NoSuchAlgorithmException, CMSException, IOException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(keysize, this.rand);
        return this.open(outputStream, s, symmetricKeyGenerator, provider);
    }
    
    private class CmsEnvelopedDataOutputStream extends OutputStream
    {
        private CipherOutputStream _out;
        private BERSequenceGenerator _cGen;
        private BERSequenceGenerator _envGen;
        private BERSequenceGenerator _eiGen;
        
        public CmsEnvelopedDataOutputStream(final CipherOutputStream out, final BERSequenceGenerator cGen, final BERSequenceGenerator envGen, final BERSequenceGenerator eiGen) {
            this._out = out;
            this._cGen = cGen;
            this._envGen = envGen;
            this._eiGen = eiGen;
        }
        
        @Override
        public void write(final int b) throws IOException {
            this._out.write(b);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            this._out.write(b, off, len);
        }
        
        @Override
        public void write(final byte[] b) throws IOException {
            this._out.write(b);
        }
        
        @Override
        public void close() throws IOException {
            this._out.close();
            this._eiGen.close();
            this._envGen.close();
            this._cGen.close();
        }
    }
}
