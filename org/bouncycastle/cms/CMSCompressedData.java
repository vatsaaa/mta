// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.IOException;
import java.util.zip.InflaterInputStream;
import org.bouncycastle.asn1.cms.CompressedData;
import org.bouncycastle.asn1.ASN1OctetString;
import java.io.InputStream;
import org.bouncycastle.asn1.cms.ContentInfo;

public class CMSCompressedData
{
    ContentInfo contentInfo;
    
    public CMSCompressedData(final byte[] array) throws CMSException {
        this(CMSUtils.readContentInfo(array));
    }
    
    public CMSCompressedData(final InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }
    
    public CMSCompressedData(final ContentInfo contentInfo) throws CMSException {
        this.contentInfo = contentInfo;
    }
    
    public byte[] getContent() throws CMSException {
        final InflaterInputStream inflaterInputStream = new InflaterInputStream(((ASN1OctetString)CompressedData.getInstance(this.contentInfo.getContent()).getEncapContentInfo().getContent()).getOctetStream());
        try {
            return CMSUtils.streamToByteArray(inflaterInputStream);
        }
        catch (IOException ex) {
            throw new CMSException("exception reading compressed stream.", ex);
        }
    }
    
    public byte[] getContent(final int n) throws CMSException {
        final InflaterInputStream inflaterInputStream = new InflaterInputStream(((ASN1OctetString)CompressedData.getInstance(this.contentInfo.getContent()).getEncapContentInfo().getContent()).getOctetStream());
        try {
            return CMSUtils.streamToByteArray(inflaterInputStream, n);
        }
        catch (IOException ex) {
            throw new CMSException("exception reading compressed stream.", ex);
        }
    }
    
    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }
    
    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }
}
