// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchProviderException;
import javax.crypto.NoSuchPaddingException;
import java.security.spec.InvalidKeySpecException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import org.bouncycastle.jce.spec.MQVPrivateKeySpec;
import org.bouncycastle.jce.spec.MQVPublicKeySpec;
import org.bouncycastle.asn1.cms.ecc.MQVuserKeyingMaterial;
import javax.crypto.SecretKey;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1Object;
import java.security.KeyFactory;
import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.cms.OriginatorPublicKey;
import java.security.PublicKey;
import java.security.Provider;
import org.bouncycastle.asn1.cms.OriginatorIdentifierOrKey;
import java.security.Key;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.cms.RecipientEncryptedKey;
import java.io.InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;

public class KeyAgreeRecipientInformation extends RecipientInformation
{
    private KeyAgreeRecipientInfo info;
    private ASN1OctetString encryptedKey;
    
    @Deprecated
    public KeyAgreeRecipientInformation(final KeyAgreeRecipientInfo keyAgreeRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final InputStream inputStream) {
        this(keyAgreeRecipientInfo, algorithmIdentifier, null, null, inputStream);
    }
    
    @Deprecated
    public KeyAgreeRecipientInformation(final KeyAgreeRecipientInfo keyAgreeRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final InputStream inputStream) {
        this(keyAgreeRecipientInfo, algorithmIdentifier, algorithmIdentifier2, null, inputStream);
    }
    
    KeyAgreeRecipientInformation(final KeyAgreeRecipientInfo info, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3, final InputStream inputStream) {
        super(algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, info.getKeyEncryptionAlgorithm(), inputStream);
        this.info = info;
        this.rid = new RecipientId();
        try {
            final RecipientEncryptedKey instance = RecipientEncryptedKey.getInstance(this.info.getRecipientEncryptedKeys().getObjectAt(0));
            final KeyAgreeRecipientIdentifier identifier = instance.getIdentifier();
            final IssuerAndSerialNumber issuerAndSerialNumber = identifier.getIssuerAndSerialNumber();
            if (issuerAndSerialNumber != null) {
                this.rid.setIssuer(issuerAndSerialNumber.getName().getEncoded());
                this.rid.setSerialNumber(issuerAndSerialNumber.getSerialNumber().getValue());
            }
            else {
                this.rid.setSubjectKeyIdentifier(identifier.getRKeyID().getSubjectKeyIdentifier().getOctets());
            }
            this.encryptedKey = instance.getEncryptedKey();
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("invalid rid in KeyAgreeRecipientInformation");
        }
    }
    
    private PublicKey getSenderPublicKey(final Key key, final OriginatorIdentifierOrKey originatorIdentifierOrKey, final Provider provider) throws CMSException, GeneralSecurityException, IOException {
        final OriginatorPublicKey originatorKey = originatorIdentifierOrKey.getOriginatorKey();
        if (originatorKey != null) {
            return this.getPublicKeyFromOriginatorPublicKey(key, originatorKey, provider);
        }
        final OriginatorId originatorId = new OriginatorId();
        final IssuerAndSerialNumber issuerAndSerialNumber = originatorIdentifierOrKey.getIssuerAndSerialNumber();
        if (issuerAndSerialNumber != null) {
            originatorId.setIssuer(issuerAndSerialNumber.getName().getEncoded());
            originatorId.setSerialNumber(issuerAndSerialNumber.getSerialNumber().getValue());
        }
        else {
            originatorId.setSubjectKeyIdentifier(originatorIdentifierOrKey.getSubjectKeyIdentifier().getKeyIdentifier());
        }
        return this.getPublicKeyFromOriginatorId(originatorId, provider);
    }
    
    private PublicKey getPublicKeyFromOriginatorPublicKey(final Key key, final OriginatorPublicKey originatorPublicKey, final Provider provider) throws CMSException, GeneralSecurityException, IOException {
        return KeyFactory.getInstance(this.keyEncAlg.getObjectId().getId(), provider).generatePublic(new X509EncodedKeySpec(new SubjectPublicKeyInfo(PrivateKeyInfo.getInstance(ASN1Object.fromByteArray(key.getEncoded())).getAlgorithmId(), originatorPublicKey.getPublicKey().getBytes()).getEncoded()));
    }
    
    private PublicKey getPublicKeyFromOriginatorId(final OriginatorId originatorId, final Provider provider) throws CMSException {
        throw new CMSException("No support for 'originator' as IssuerAndSerialNumber or SubjectKeyIdentifier");
    }
    
    private SecretKey calculateAgreedWrapKey(final String algorithm, PublicKey key, PrivateKey key2, final Provider provider) throws CMSException, GeneralSecurityException, IOException {
        final String id = this.keyEncAlg.getObjectId().getId();
        if (id.equals(CMSEnvelopedGenerator.ECMQV_SHA1KDF)) {
            key = new MQVPublicKeySpec(key, this.getPublicKeyFromOriginatorPublicKey(key2, MQVuserKeyingMaterial.getInstance(ASN1Object.fromByteArray(this.info.getUserKeyingMaterial().getOctets())).getEphemeralPublicKey(), provider));
            key2 = new MQVPrivateKeySpec(key2, key2);
        }
        final KeyAgreement instance = KeyAgreement.getInstance(id, provider);
        instance.init(key2);
        instance.doPhase(key, true);
        return instance.generateSecret(algorithm);
    }
    
    private Key unwrapSessionKey(final String transformation, final SecretKey key, final Provider provider) throws GeneralSecurityException {
        final String id = this.getActiveAlgID().getObjectId().getId();
        final byte[] octets = this.encryptedKey.getOctets();
        final Cipher instance = Cipher.getInstance(transformation, provider);
        instance.init(4, key);
        return instance.unwrap(octets, id, 3);
    }
    
    protected Key getSessionKey(final Key key, final Provider provider) throws CMSException {
        try {
            final String id = DERObjectIdentifier.getInstance(ASN1Sequence.getInstance(this.keyEncAlg.getParameters()).getObjectAt(0)).getId();
            return this.unwrapSessionKey(id, this.calculateAgreedWrapKey(id, this.getSenderPublicKey(key, this.info.getOriginator(), provider), (PrivateKey)key, provider), provider);
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find algorithm.", ex);
        }
        catch (InvalidKeyException ex2) {
            throw new CMSException("key invalid in message.", ex2);
        }
        catch (InvalidKeySpecException ex3) {
            throw new CMSException("originator key spec invalid.", ex3);
        }
        catch (NoSuchPaddingException ex4) {
            throw new CMSException("required padding not supported.", ex4);
        }
        catch (Exception ex5) {
            throw new CMSException("originator key invalid.", ex5);
        }
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final Provider provider) throws CMSException {
        return this.getContentFromSessionKey(this.getSessionKey(key, provider), provider);
    }
}
