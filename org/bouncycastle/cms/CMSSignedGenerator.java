// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.SignatureException;
import java.security.MessageDigest;
import java.io.OutputStream;
import java.util.HashSet;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.x509.AttributeCertificate;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.util.Selector;
import org.bouncycastle.x509.X509Store;
import java.security.cert.CertStoreException;
import java.util.Collection;
import java.security.cert.CertStore;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.io.IOException;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.Signature;
import org.bouncycastle.jce.interfaces.GOST3410PrivateKey;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.ArrayList;
import java.security.SecureRandom;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CMSSignedGenerator
{
    public static final String DATA;
    public static final String DIGEST_SHA1;
    public static final String DIGEST_SHA224;
    public static final String DIGEST_SHA256;
    public static final String DIGEST_SHA384;
    public static final String DIGEST_SHA512;
    public static final String DIGEST_MD5;
    public static final String DIGEST_GOST3411;
    public static final String DIGEST_RIPEMD128;
    public static final String DIGEST_RIPEMD160;
    public static final String DIGEST_RIPEMD256;
    public static final String ENCRYPTION_RSA;
    public static final String ENCRYPTION_DSA;
    public static final String ENCRYPTION_ECDSA;
    public static final String ENCRYPTION_RSA_PSS;
    public static final String ENCRYPTION_GOST3410;
    public static final String ENCRYPTION_ECGOST3410;
    private static final String ENCRYPTION_ECDSA_WITH_SHA1;
    private static final String ENCRYPTION_ECDSA_WITH_SHA224;
    private static final String ENCRYPTION_ECDSA_WITH_SHA256;
    private static final String ENCRYPTION_ECDSA_WITH_SHA384;
    private static final String ENCRYPTION_ECDSA_WITH_SHA512;
    private static final Set NO_PARAMS;
    private static final Map EC_ALGORITHMS;
    protected List _certs;
    protected List _crls;
    protected List _signers;
    protected Map _digests;
    protected final SecureRandom rand;
    
    protected CMSSignedGenerator() {
        this(new SecureRandom());
    }
    
    protected CMSSignedGenerator(final SecureRandom rand) {
        this._certs = new ArrayList();
        this._crls = new ArrayList();
        this._signers = new ArrayList();
        this._digests = new HashMap();
        this.rand = rand;
    }
    
    protected String getEncOID(final PrivateKey privateKey, final String s) {
        String s2 = null;
        if (privateKey instanceof RSAPrivateKey || "RSA".equalsIgnoreCase(privateKey.getAlgorithm())) {
            s2 = CMSSignedGenerator.ENCRYPTION_RSA;
        }
        else if (privateKey instanceof DSAPrivateKey || "DSA".equalsIgnoreCase(privateKey.getAlgorithm())) {
            s2 = CMSSignedGenerator.ENCRYPTION_DSA;
            if (!s.equals(CMSSignedGenerator.DIGEST_SHA1)) {
                throw new IllegalArgumentException("can't mix DSA with anything but SHA1");
            }
        }
        else if ("ECDSA".equalsIgnoreCase(privateKey.getAlgorithm()) || "EC".equalsIgnoreCase(privateKey.getAlgorithm())) {
            s2 = CMSSignedGenerator.EC_ALGORITHMS.get(s);
            if (s2 == null) {
                throw new IllegalArgumentException("can't mix ECDSA with anything but SHA family digests");
            }
        }
        else if (privateKey instanceof GOST3410PrivateKey || "GOST3410".equalsIgnoreCase(privateKey.getAlgorithm())) {
            s2 = CMSSignedGenerator.ENCRYPTION_GOST3410;
        }
        else if ("ECGOST3410".equalsIgnoreCase(privateKey.getAlgorithm())) {
            s2 = CMSSignedGenerator.ENCRYPTION_ECGOST3410;
        }
        return s2;
    }
    
    protected AlgorithmIdentifier getEncAlgorithmIdentifier(final String s, final Signature signature) throws IOException {
        if (CMSSignedGenerator.NO_PARAMS.contains(s)) {
            return new AlgorithmIdentifier(new DERObjectIdentifier(s));
        }
        if (s.equals(CMSSignedGenerator.ENCRYPTION_RSA_PSS)) {
            return new AlgorithmIdentifier(new DERObjectIdentifier(s), ASN1Object.fromByteArray(signature.getParameters().getEncoded()));
        }
        return new AlgorithmIdentifier(new DERObjectIdentifier(s), new DERNull());
    }
    
    protected Map getBaseParameters(final DERObjectIdentifier derObjectIdentifier, final AlgorithmIdentifier algorithmIdentifier, final byte[] array) {
        final HashMap<String, DERObjectIdentifier> hashMap = new HashMap<String, DERObjectIdentifier>();
        hashMap.put("contentType", derObjectIdentifier);
        hashMap.put("digestAlgID", (DERObjectIdentifier)algorithmIdentifier);
        hashMap.put("digest", (DERObjectIdentifier)(Object)array.clone());
        return hashMap;
    }
    
    protected ASN1Set getAttributeSet(final AttributeTable attributeTable) {
        if (attributeTable != null) {
            return new DERSet(attributeTable.toASN1EncodableVector());
        }
        return null;
    }
    
    public void addCertificatesAndCRLs(final CertStore certStore) throws CertStoreException, CMSException {
        this._certs.addAll(CMSUtils.getCertificatesFromStore(certStore));
        this._crls.addAll(CMSUtils.getCRLsFromStore(certStore));
    }
    
    public void addAttributeCertificates(final X509Store x509Store) throws CMSException {
        try {
            final Iterator<X509AttributeCertificate> iterator = x509Store.getMatches(null).iterator();
            while (iterator.hasNext()) {
                this._certs.add(new DERTaggedObject(false, 2, AttributeCertificate.getInstance(ASN1Object.fromByteArray(iterator.next().getEncoded()))));
            }
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("error processing attribute certs", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("error processing attribute certs", ex2);
        }
    }
    
    public void addSigners(final SignerInformationStore signerInformationStore) {
        final Iterator<Object> iterator = signerInformationStore.getSigners().iterator();
        while (iterator.hasNext()) {
            this._signers.add(iterator.next());
        }
    }
    
    public Map getGeneratedDigests() {
        return new HashMap(this._digests);
    }
    
    static SignerIdentifier getSignerIdentifier(final X509Certificate x509Certificate) {
        TBSCertificateStructure tbsCertificateStructure;
        try {
            tbsCertificateStructure = CMSUtils.getTBSCertificateStructure(x509Certificate);
        }
        catch (CertificateEncodingException ex) {
            throw new IllegalArgumentException("can't extract TBS structure from this cert");
        }
        return new SignerIdentifier(new IssuerAndSerialNumber(tbsCertificateStructure.getIssuer(), tbsCertificateStructure.getSerialNumber().getValue()));
    }
    
    static SignerIdentifier getSignerIdentifier(final byte[] array) {
        return new SignerIdentifier(new DEROctetString(array));
    }
    
    static {
        DATA = CMSObjectIdentifiers.data.getId();
        DIGEST_SHA1 = OIWObjectIdentifiers.idSHA1.getId();
        DIGEST_SHA224 = NISTObjectIdentifiers.id_sha224.getId();
        DIGEST_SHA256 = NISTObjectIdentifiers.id_sha256.getId();
        DIGEST_SHA384 = NISTObjectIdentifiers.id_sha384.getId();
        DIGEST_SHA512 = NISTObjectIdentifiers.id_sha512.getId();
        DIGEST_MD5 = PKCSObjectIdentifiers.md5.getId();
        DIGEST_GOST3411 = CryptoProObjectIdentifiers.gostR3411.getId();
        DIGEST_RIPEMD128 = TeleTrusTObjectIdentifiers.ripemd128.getId();
        DIGEST_RIPEMD160 = TeleTrusTObjectIdentifiers.ripemd160.getId();
        DIGEST_RIPEMD256 = TeleTrusTObjectIdentifiers.ripemd256.getId();
        ENCRYPTION_RSA = PKCSObjectIdentifiers.rsaEncryption.getId();
        ENCRYPTION_DSA = X9ObjectIdentifiers.id_dsa_with_sha1.getId();
        ENCRYPTION_ECDSA = X9ObjectIdentifiers.ecdsa_with_SHA1.getId();
        ENCRYPTION_RSA_PSS = PKCSObjectIdentifiers.id_RSASSA_PSS.getId();
        ENCRYPTION_GOST3410 = CryptoProObjectIdentifiers.gostR3410_94.getId();
        ENCRYPTION_ECGOST3410 = CryptoProObjectIdentifiers.gostR3410_2001.getId();
        ENCRYPTION_ECDSA_WITH_SHA1 = X9ObjectIdentifiers.ecdsa_with_SHA1.getId();
        ENCRYPTION_ECDSA_WITH_SHA224 = X9ObjectIdentifiers.ecdsa_with_SHA224.getId();
        ENCRYPTION_ECDSA_WITH_SHA256 = X9ObjectIdentifiers.ecdsa_with_SHA256.getId();
        ENCRYPTION_ECDSA_WITH_SHA384 = X9ObjectIdentifiers.ecdsa_with_SHA384.getId();
        ENCRYPTION_ECDSA_WITH_SHA512 = X9ObjectIdentifiers.ecdsa_with_SHA512.getId();
        NO_PARAMS = new HashSet();
        EC_ALGORITHMS = new HashMap();
        CMSSignedGenerator.NO_PARAMS.add(CMSSignedGenerator.ENCRYPTION_DSA);
        CMSSignedGenerator.NO_PARAMS.add(CMSSignedGenerator.ENCRYPTION_ECDSA);
        CMSSignedGenerator.NO_PARAMS.add(CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA1);
        CMSSignedGenerator.NO_PARAMS.add(CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA224);
        CMSSignedGenerator.NO_PARAMS.add(CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA256);
        CMSSignedGenerator.NO_PARAMS.add(CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA384);
        CMSSignedGenerator.NO_PARAMS.add(CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA512);
        CMSSignedGenerator.EC_ALGORITHMS.put(CMSSignedGenerator.DIGEST_SHA1, CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA1);
        CMSSignedGenerator.EC_ALGORITHMS.put(CMSSignedGenerator.DIGEST_SHA224, CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA224);
        CMSSignedGenerator.EC_ALGORITHMS.put(CMSSignedGenerator.DIGEST_SHA256, CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA256);
        CMSSignedGenerator.EC_ALGORITHMS.put(CMSSignedGenerator.DIGEST_SHA384, CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA384);
        CMSSignedGenerator.EC_ALGORITHMS.put(CMSSignedGenerator.DIGEST_SHA512, CMSSignedGenerator.ENCRYPTION_ECDSA_WITH_SHA512);
    }
    
    static class DigOutputStream extends OutputStream
    {
        MessageDigest dig;
        
        public DigOutputStream(final MessageDigest dig) {
            this.dig = dig;
        }
        
        @Override
        public void write(final byte[] input, final int offset, final int len) throws IOException {
            this.dig.update(input, offset, len);
        }
        
        @Override
        public void write(final int n) throws IOException {
            this.dig.update((byte)n);
        }
    }
    
    static class SigOutputStream extends OutputStream
    {
        private final Signature sig;
        
        public SigOutputStream(final Signature sig) {
            this.sig = sig;
        }
        
        @Override
        public void write(final byte[] data, final int off, final int len) throws IOException {
            try {
                this.sig.update(data, off, len);
            }
            catch (SignatureException obj) {
                throw new CMSStreamException("signature problem: " + obj, obj);
            }
        }
        
        @Override
        public void write(final int n) throws IOException {
            try {
                this.sig.update((byte)n);
            }
            catch (SignatureException obj) {
                throw new CMSStreamException("signature problem: " + obj, obj);
            }
        }
    }
}
