// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.AlgorithmParameters;
import java.io.IOException;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.util.Arrays;
import java.util.List;
import java.util.Collection;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.AuthenticatedData;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.ContentInfo;

public class CMSAuthenticatedData
{
    RecipientInformationStore recipientInfoStore;
    ContentInfo contentInfo;
    private AlgorithmIdentifier macAlg;
    private ASN1Set authAttrs;
    private ASN1Set unauthAttrs;
    private byte[] mac;
    
    public CMSAuthenticatedData(final byte[] array) throws CMSException {
        this(CMSUtils.readContentInfo(array));
    }
    
    public CMSAuthenticatedData(final InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }
    
    public CMSAuthenticatedData(final ContentInfo contentInfo) throws CMSException {
        this.contentInfo = contentInfo;
        final AuthenticatedData instance = AuthenticatedData.getInstance(contentInfo.getContent());
        final ContentInfo encapsulatedContentInfo = instance.getEncapsulatedContentInfo();
        this.macAlg = instance.getMacAlgorithm();
        this.mac = instance.getMac().getOctets();
        final List recipientInfos = CMSEnvelopedHelper.readRecipientInfos(instance.getRecipientInfos(), ASN1OctetString.getInstance(encapsulatedContentInfo.getContent()).getOctets(), null, this.macAlg, null);
        this.authAttrs = instance.getAuthAttrs();
        this.recipientInfoStore = new RecipientInformationStore(recipientInfos);
        this.unauthAttrs = instance.getUnauthAttrs();
    }
    
    public byte[] getMac() {
        return Arrays.clone(this.mac);
    }
    
    private byte[] encodeObj(final DEREncodable derEncodable) throws IOException {
        if (derEncodable != null) {
            return derEncodable.getDERObject().getEncoded();
        }
        return null;
    }
    
    public String getMacAlgOID() {
        return this.macAlg.getObjectId().getId();
    }
    
    public byte[] getMacAlgParams() {
        try {
            return this.encodeObj(this.macAlg.getParameters());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AlgorithmParameters getMacAlgorithmParameters(final String s) throws CMSException, NoSuchProviderException {
        return this.getMacAlgorithmParameters(CMSUtils.getProvider(s));
    }
    
    public AlgorithmParameters getMacAlgorithmParameters(final Provider provider) throws CMSException {
        return CMSEnvelopedHelper.INSTANCE.getEncryptionAlgorithmParameters(this.getMacAlgOID(), this.getMacAlgParams(), provider);
    }
    
    public RecipientInformationStore getRecipientInfos() {
        return this.recipientInfoStore;
    }
    
    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }
    
    public AttributeTable getAuthAttrs() {
        if (this.authAttrs == null) {
            return null;
        }
        return new AttributeTable(this.authAttrs);
    }
    
    public AttributeTable getUnauthAttrs() {
        if (this.unauthAttrs == null) {
            return null;
        }
        return new AttributeTable(this.unauthAttrs);
    }
    
    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }
}
