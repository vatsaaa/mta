// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.cms.EncryptedContentInfoParser;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1SetParser;
import java.util.Collection;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1OctetStringParser;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1SequenceParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.EnvelopedDataParser;

public class CMSEnvelopedDataParser extends CMSContentInfoParser
{
    RecipientInformationStore _recipientInfoStore;
    EnvelopedDataParser _envelopedData;
    private AlgorithmIdentifier _encAlg;
    private AttributeTable _unprotectedAttributes;
    private boolean _attrNotRead;
    
    public CMSEnvelopedDataParser(final byte[] buf) throws CMSException, IOException {
        this(new ByteArrayInputStream(buf));
    }
    
    public CMSEnvelopedDataParser(final InputStream inputStream) throws CMSException, IOException {
        super(inputStream);
        this._attrNotRead = true;
        this._envelopedData = new EnvelopedDataParser((ASN1SequenceParser)this._contentInfo.getContent(16));
        final ASN1SetParser recipientInfos = this._envelopedData.getRecipientInfos();
        final ArrayList<Object> list = new ArrayList<Object>();
        DEREncodable object;
        while ((object = recipientInfos.readObject()) != null) {
            list.add(RecipientInfo.getInstance(object.getDERObject()));
        }
        final EncryptedContentInfoParser encryptedContentInfo = this._envelopedData.getEncryptedContentInfo();
        this._encAlg = encryptedContentInfo.getContentEncryptionAlgorithm();
        this._recipientInfoStore = new RecipientInformationStore(CMSEnvelopedHelper.readRecipientInfos(list.iterator(), ((ASN1OctetStringParser)encryptedContentInfo.getEncryptedContent(4)).getOctetStream(), this._encAlg, null, null));
    }
    
    public String getEncryptionAlgOID() {
        return this._encAlg.getObjectId().toString();
    }
    
    public byte[] getEncryptionAlgParams() {
        try {
            return this.encodeObj(this._encAlg.getParameters());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AlgorithmParameters getEncryptionAlgorithmParameters(final String s) throws CMSException, NoSuchProviderException {
        return this.getEncryptionAlgorithmParameters(CMSUtils.getProvider(s));
    }
    
    public AlgorithmParameters getEncryptionAlgorithmParameters(final Provider provider) throws CMSException {
        return CMSEnvelopedHelper.INSTANCE.getEncryptionAlgorithmParameters(this.getEncryptionAlgOID(), this.getEncryptionAlgParams(), provider);
    }
    
    public RecipientInformationStore getRecipientInfos() {
        return this._recipientInfoStore;
    }
    
    public AttributeTable getUnprotectedAttributes() throws IOException {
        if (this._unprotectedAttributes == null && this._attrNotRead) {
            final ASN1SetParser unprotectedAttrs = this._envelopedData.getUnprotectedAttrs();
            this._attrNotRead = false;
            if (unprotectedAttrs != null) {
                final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
                DEREncodable object;
                while ((object = unprotectedAttrs.readObject()) != null) {
                    asn1EncodableVector.add(((ASN1SequenceParser)object).getDERObject());
                }
                this._unprotectedAttributes = new AttributeTable(new DERSet(asn1EncodableVector));
            }
        }
        return this._unprotectedAttributes;
    }
    
    private byte[] encodeObj(final DEREncodable derEncodable) throws IOException {
        if (derEncodable != null) {
            return derEncodable.getDERObject().getEncoded();
        }
        return null;
    }
}
