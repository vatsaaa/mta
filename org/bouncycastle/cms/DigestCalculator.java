// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchAlgorithmException;

interface DigestCalculator
{
    byte[] getDigest() throws NoSuchAlgorithmException;
}
