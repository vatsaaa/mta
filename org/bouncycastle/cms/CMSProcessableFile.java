// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.File;

public class CMSProcessableFile implements CMSProcessable
{
    private static final int DEFAULT_BUF_SIZE = 32768;
    private final File _file;
    private final byte[] _buf;
    
    public CMSProcessableFile(final File file) {
        this(file, 32768);
    }
    
    public CMSProcessableFile(final File file, final int n) {
        this._file = file;
        this._buf = new byte[n];
    }
    
    public void write(final OutputStream outputStream) throws IOException, CMSException {
        final FileInputStream fileInputStream = new FileInputStream(this._file);
        int read;
        while ((read = fileInputStream.read(this._buf, 0, this._buf.length)) > 0) {
            outputStream.write(this._buf, 0, read);
        }
        fileInputStream.close();
    }
    
    public Object getContent() {
        return this._file;
    }
}
