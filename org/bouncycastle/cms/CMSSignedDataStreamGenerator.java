// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.Signature;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.DigestInfo;
import java.util.Map;
import java.util.Collections;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import java.security.cert.CertificateEncodingException;
import java.security.SignatureException;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERInteger;
import java.util.Iterator;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.BERSequenceGenerator;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.Provider;
import java.security.InvalidKeyException;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class CMSSignedDataStreamGenerator extends CMSSignedGenerator
{
    private List _signerInfs;
    private List _messageDigests;
    private int _bufferSize;
    
    public CMSSignedDataStreamGenerator() {
        this._signerInfs = new ArrayList();
        this._messageDigests = new ArrayList();
    }
    
    public CMSSignedDataStreamGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
        this._signerInfs = new ArrayList();
        this._messageDigests = new ArrayList();
    }
    
    public void setBufferSize(final int bufferSize) {
        this._bufferSize = bufferSize;
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, new DefaultSignedAttributeTableGenerator(), null, s2);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(), null, s3);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), s2);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), s3);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        final MessageDigest digestInstance = CMSSignedHelper.INSTANCE.getDigestInstance(CMSSignedHelper.INSTANCE.getDigestAlgName(s2), provider);
        this._signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(x509Certificate), s2, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, digestInstance, provider));
        this._messageDigests.add(digestInstance);
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s2));
    }
    
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s3));
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, new DefaultSignedAttributeTableGenerator(), null, s2);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, s2, new DefaultSignedAttributeTableGenerator(), null, s3);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, s2, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), s2);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, array, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, array, this.getEncOID(privateKey, s), s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        final MessageDigest digestInstance = CMSSignedHelper.INSTANCE.getDigestInstance(CMSSignedHelper.INSTANCE.getDigestAlgName(s2), provider);
        this._signerInfs.add(new SignerInf(privateKey, CMSSignedGenerator.getSignerIdentifier(array), s2, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, digestInstance, provider));
        this._messageDigests.add(digestInstance);
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s2));
    }
    
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s3));
    }
    
    public OutputStream open(final OutputStream outputStream) throws IOException {
        return this.open(outputStream, false);
    }
    
    public OutputStream open(final OutputStream outputStream, final boolean b) throws IOException {
        return this.open(outputStream, CMSSignedDataStreamGenerator.DATA, b);
    }
    
    public OutputStream open(final OutputStream outputStream, final boolean b, final OutputStream outputStream2) throws IOException {
        return this.open(outputStream, CMSSignedDataStreamGenerator.DATA, b, outputStream2);
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final boolean b) throws IOException {
        return this.open(outputStream, s, b, null);
    }
    
    public OutputStream open(final OutputStream outputStream, final String s, final boolean b, final OutputStream outputStream2) throws IOException {
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(this.calculateVersion(s));
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<SignerInformation> iterator = this._signers.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(iterator.next().getDigestAlgorithmID()));
        }
        final Iterator<SignerInf> iterator2 = this._signerInfs.iterator();
        while (iterator2.hasNext()) {
            asn1EncodableVector.add(iterator2.next().getDigestAlgorithmID());
        }
        berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(new DERObjectIdentifier(s));
        return new CmsSignedDataOutputStream(attachDigestsToOutputStream(this._messageDigests, getSafeTeeOutputStream(outputStream2, b ? CMSUtils.createBEROctetOutputStream(berSequenceGenerator3.getRawOutputStream(), 0, true, this._bufferSize) : null)), s, berSequenceGenerator, berSequenceGenerator2, berSequenceGenerator3);
    }
    
    private DERInteger calculateVersion(final String s) {
        boolean b = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean b4 = false;
        if (this._certs != null) {
            for (final ASN1TaggedObject next : this._certs) {
                if (next instanceof ASN1TaggedObject) {
                    final ASN1TaggedObject asn1TaggedObject = next;
                    if (asn1TaggedObject.getTagNo() == 1) {
                        b3 = true;
                    }
                    else if (asn1TaggedObject.getTagNo() == 2) {
                        b4 = true;
                    }
                    else {
                        if (asn1TaggedObject.getTagNo() != 3) {
                            continue;
                        }
                        b = true;
                    }
                }
            }
        }
        if (b) {
            return new DERInteger(5);
        }
        if (this._crls != null && !b) {
            final Iterator<Object> iterator2 = this._crls.iterator();
            while (iterator2.hasNext()) {
                if (iterator2.next() instanceof ASN1TaggedObject) {
                    b2 = true;
                }
            }
        }
        if (b2) {
            return new DERInteger(5);
        }
        if (b4) {
            return new DERInteger(4);
        }
        if (b3) {
            return new DERInteger(3);
        }
        if (!s.equals(CMSSignedDataStreamGenerator.DATA)) {
            return new DERInteger(3);
        }
        if (this.checkForVersion3(this._signers)) {
            return new DERInteger(3);
        }
        return new DERInteger(1);
    }
    
    private boolean checkForVersion3(final List list) {
        final Iterator<SignerInformation> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (SignerInfo.getInstance(iterator.next().toSignerInfo()).getVersion().getValue().intValue() == 3) {
                return true;
            }
        }
        return false;
    }
    
    private static OutputStream attachDigestsToOutputStream(final List list, final OutputStream outputStream) {
        OutputStream safeTeeOutputStream = outputStream;
        final Iterator<MessageDigest> iterator = list.iterator();
        while (iterator.hasNext()) {
            safeTeeOutputStream = getSafeTeeOutputStream(safeTeeOutputStream, new DigOutputStream(iterator.next()));
        }
        return safeTeeOutputStream;
    }
    
    private static OutputStream getSafeOutputStream(final OutputStream outputStream) {
        return (outputStream == null) ? new NullOutputStream() : outputStream;
    }
    
    private static OutputStream getSafeTeeOutputStream(final OutputStream outputStream, final OutputStream outputStream2) {
        return (outputStream == null) ? getSafeOutputStream(outputStream2) : ((outputStream2 == null) ? getSafeOutputStream(outputStream) : new TeeOutputStream(outputStream, outputStream2));
    }
    
    private class CmsSignedDataOutputStream extends OutputStream
    {
        private OutputStream _out;
        private DERObjectIdentifier _contentOID;
        private BERSequenceGenerator _sGen;
        private BERSequenceGenerator _sigGen;
        private BERSequenceGenerator _eiGen;
        
        public CmsSignedDataOutputStream(final OutputStream out, final String s, final BERSequenceGenerator sGen, final BERSequenceGenerator sigGen, final BERSequenceGenerator eiGen) {
            this._out = out;
            this._contentOID = new DERObjectIdentifier(s);
            this._sGen = sGen;
            this._sigGen = sigGen;
            this._eiGen = eiGen;
        }
        
        @Override
        public void write(final int n) throws IOException {
            this._out.write(n);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            this._out.write(b, off, len);
        }
        
        @Override
        public void write(final byte[] b) throws IOException {
            this._out.write(b);
        }
        
        @Override
        public void close() throws IOException {
            this._out.close();
            this._eiGen.close();
            CMSSignedDataStreamGenerator.this._digests.clear();
            if (CMSSignedDataStreamGenerator.this._certs.size() != 0) {
                this._sigGen.getRawOutputStream().write(new BERTaggedObject(false, 0, CMSUtils.createBerSetFromList(CMSSignedDataStreamGenerator.this._certs)).getEncoded());
            }
            if (CMSSignedDataStreamGenerator.this._crls.size() != 0) {
                this._sigGen.getRawOutputStream().write(new BERTaggedObject(false, 1, CMSUtils.createBerSetFromList(CMSSignedDataStreamGenerator.this._crls)).getEncoded());
            }
            final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
            final Iterator<SignerInformation> iterator = CMSSignedDataStreamGenerator.this._signers.iterator();
            while (iterator.hasNext()) {
                asn1EncodableVector.add(iterator.next().toSignerInfo());
            }
            for (final SignerInf signerInf : CMSSignedDataStreamGenerator.this._signerInfs) {
                try {
                    asn1EncodableVector.add(signerInf.toSignerInfo(this._contentOID));
                }
                catch (IOException ex) {
                    throw new CMSStreamException("encoding error.", ex);
                }
                catch (InvalidKeyException ex2) {
                    throw new CMSStreamException("key inappropriate for signature.", ex2);
                }
                catch (SignatureException ex3) {
                    throw new CMSStreamException("error creating signature.", ex3);
                }
                catch (CertificateEncodingException ex4) {
                    throw new CMSStreamException("error creating sid.", ex4);
                }
                catch (NoSuchAlgorithmException ex5) {
                    throw new CMSStreamException("unknown signature algorithm.", ex5);
                }
            }
            this._sigGen.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
            this._sigGen.close();
            this._sGen.close();
        }
    }
    
    private class SignerInf
    {
        private final PrivateKey _key;
        private final SignerIdentifier _signerIdentifier;
        private final String _digestOID;
        private final String _encOID;
        private final CMSAttributeTableGenerator _sAttr;
        private final CMSAttributeTableGenerator _unsAttr;
        private final MessageDigest _digest;
        private final Provider _sigProvider;
        
        SignerInf(final PrivateKey key, final SignerIdentifier signerIdentifier, final String digestOID, final String encOID, final CMSAttributeTableGenerator sAttr, final CMSAttributeTableGenerator unsAttr, final MessageDigest digest, final Provider sigProvider) {
            this._key = key;
            this._signerIdentifier = signerIdentifier;
            this._digestOID = digestOID;
            this._encOID = encOID;
            this._sAttr = sAttr;
            this._unsAttr = unsAttr;
            this._digest = digest;
            this._sigProvider = sigProvider;
        }
        
        AlgorithmIdentifier getDigestAlgorithmID() {
            return new AlgorithmIdentifier(new DERObjectIdentifier(this._digestOID), DERNull.INSTANCE);
        }
        
        SignerInfo toSignerInfo(final DERObjectIdentifier derObjectIdentifier) throws IOException, SignatureException, CertificateEncodingException, InvalidKeyException, NoSuchAlgorithmException {
            final String digestAlgName = CMSSignedHelper.INSTANCE.getDigestAlgName(this._digestOID);
            final String encryptionAlgName = CMSSignedHelper.INSTANCE.getEncryptionAlgName(this._encOID);
            final String string = digestAlgName + "with" + encryptionAlgName;
            final AlgorithmIdentifier digestAlgorithmID = this.getDigestAlgorithmID();
            final byte[] digest = this._digest.digest();
            CMSSignedDataStreamGenerator.this._digests.put(this._digestOID, digest.clone());
            byte[] data = digest;
            ASN1Set attributeSet = null;
            Signature signature;
            if (this._sAttr != null) {
                attributeSet = CMSSignedDataStreamGenerator.this.getAttributeSet(this._sAttr.getAttributes(Collections.unmodifiableMap((Map<?, ?>)CMSSignedDataStreamGenerator.this.getBaseParameters(derObjectIdentifier, digestAlgorithmID, digest))));
                data = attributeSet.getEncoded("DER");
                signature = CMSSignedHelper.INSTANCE.getSignatureInstance(string, this._sigProvider);
            }
            else if (encryptionAlgName.equals("RSA")) {
                data = new DigestInfo(digestAlgorithmID, digest).getEncoded("DER");
                signature = CMSSignedHelper.INSTANCE.getSignatureInstance("RSA", this._sigProvider);
            }
            else {
                if (!encryptionAlgName.equals("DSA")) {
                    throw new SignatureException("algorithm: " + encryptionAlgName + " not supported in base signatures.");
                }
                signature = CMSSignedHelper.INSTANCE.getSignatureInstance("NONEwithDSA", this._sigProvider);
            }
            signature.initSign(this._key, CMSSignedDataStreamGenerator.this.rand);
            signature.update(data);
            final byte[] sign = signature.sign();
            ASN1Set attributeSet2 = null;
            if (this._unsAttr != null) {
                final Map baseParameters = CMSSignedDataStreamGenerator.this.getBaseParameters(derObjectIdentifier, digestAlgorithmID, digest);
                baseParameters.put("encryptedDigest", sign.clone());
                attributeSet2 = CMSSignedDataStreamGenerator.this.getAttributeSet(this._unsAttr.getAttributes(Collections.unmodifiableMap((Map<?, ?>)baseParameters)));
            }
            return new SignerInfo(this._signerIdentifier, digestAlgorithmID, attributeSet, CMSSignedDataStreamGenerator.this.getEncAlgorithmIdentifier(this._encOID, signature), new DEROctetString(sign), attributeSet2);
        }
    }
    
    private static class NullOutputStream extends OutputStream
    {
        @Override
        public void write(final byte[] array) throws IOException {
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
        }
        
        @Override
        public void write(final int n) throws IOException {
        }
    }
    
    private static class TeeOutputStream extends OutputStream
    {
        private OutputStream s1;
        private OutputStream s2;
        
        public TeeOutputStream(final OutputStream s1, final OutputStream s2) {
            this.s1 = s1;
            this.s2 = s2;
        }
        
        @Override
        public void write(final byte[] array) throws IOException {
            this.s1.write(array);
            this.s2.write(array);
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
            this.s1.write(array, n, n2);
            this.s2.write(array, n, n2);
        }
        
        @Override
        public void write(final int n) throws IOException {
            this.s1.write(n);
            this.s2.write(n);
        }
        
        @Override
        public void close() throws IOException {
            this.s1.close();
            this.s2.close();
        }
    }
}
