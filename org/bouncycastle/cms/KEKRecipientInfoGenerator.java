// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObjectIdentifier;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;
import org.bouncycastle.asn1.DEROctetString;
import java.security.Key;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.security.Provider;
import java.security.SecureRandom;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.KEKIdentifier;
import javax.crypto.SecretKey;

class KEKRecipientInfoGenerator implements RecipientInfoGenerator
{
    private SecretKey wrapKey;
    private KEKIdentifier secKeyId;
    private AlgorithmIdentifier keyEncAlg;
    
    void setWrapKey(final SecretKey wrapKey) {
        this.wrapKey = wrapKey;
        this.keyEncAlg = determineKeyEncAlg(wrapKey);
    }
    
    void setKEKIdentifier(final KEKIdentifier secKeyId) {
        this.secKeyId = secKeyId;
    }
    
    public RecipientInfo generate(final SecretKey key, final SecureRandom random, final Provider provider) throws GeneralSecurityException {
        final Cipher asymmetricCipher = CMSEnvelopedHelper.INSTANCE.createAsymmetricCipher(this.keyEncAlg.getObjectId().getId(), provider);
        asymmetricCipher.init(3, this.wrapKey, random);
        return new RecipientInfo(new KEKRecipientInfo(this.secKeyId, this.keyEncAlg, new DEROctetString(asymmetricCipher.wrap(key))));
    }
    
    private static AlgorithmIdentifier determineKeyEncAlg(final SecretKey secretKey) {
        final String algorithm = secretKey.getAlgorithm();
        if (algorithm.startsWith("DES")) {
            return new AlgorithmIdentifier(new DERObjectIdentifier("1.2.840.113549.1.9.16.3.6"), new DERNull());
        }
        if (algorithm.startsWith("RC2")) {
            return new AlgorithmIdentifier(new DERObjectIdentifier("1.2.840.113549.1.9.16.3.7"), new DERInteger(58));
        }
        if (algorithm.startsWith("AES")) {
            final int n = secretKey.getEncoded().length * 8;
            DERObjectIdentifier derObjectIdentifier;
            if (n == 128) {
                derObjectIdentifier = NISTObjectIdentifiers.id_aes128_wrap;
            }
            else if (n == 192) {
                derObjectIdentifier = NISTObjectIdentifiers.id_aes192_wrap;
            }
            else {
                if (n != 256) {
                    throw new IllegalArgumentException("illegal keysize in AES");
                }
                derObjectIdentifier = NISTObjectIdentifiers.id_aes256_wrap;
            }
            return new AlgorithmIdentifier(derObjectIdentifier);
        }
        if (algorithm.startsWith("SEED")) {
            return new AlgorithmIdentifier(KISAObjectIdentifiers.id_npki_app_cmsSeed_wrap);
        }
        if (algorithm.startsWith("Camellia")) {
            final int n2 = secretKey.getEncoded().length * 8;
            DERObjectIdentifier derObjectIdentifier2;
            if (n2 == 128) {
                derObjectIdentifier2 = NTTObjectIdentifiers.id_camellia128_wrap;
            }
            else if (n2 == 192) {
                derObjectIdentifier2 = NTTObjectIdentifiers.id_camellia192_wrap;
            }
            else {
                if (n2 != 256) {
                    throw new IllegalArgumentException("illegal keysize in Camellia");
                }
                derObjectIdentifier2 = NTTObjectIdentifiers.id_camellia256_wrap;
            }
            return new AlgorithmIdentifier(derObjectIdentifier2);
        }
        throw new IllegalArgumentException("unknown algorithm");
    }
}
