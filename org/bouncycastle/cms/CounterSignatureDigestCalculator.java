// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchAlgorithmException;
import java.security.Provider;

class CounterSignatureDigestCalculator implements DigestCalculator
{
    private final String alg;
    private final Provider provider;
    private final byte[] data;
    
    CounterSignatureDigestCalculator(final String alg, final Provider provider, final byte[] data) {
        this.alg = alg;
        this.provider = provider;
        this.data = data;
    }
    
    public byte[] getDigest() throws NoSuchAlgorithmException {
        return CMSSignedHelper.INSTANCE.getDigestInstance(this.alg, this.provider).digest(this.data);
    }
}
