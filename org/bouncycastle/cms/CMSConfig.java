// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.DERObjectIdentifier;

public class CMSConfig
{
    public static void setSigningEncryptionAlgorithmMapping(final String s, final String s2) {
        CMSSignedHelper.INSTANCE.setSigningEncryptionAlgorithmMapping(new DERObjectIdentifier(s), s2);
    }
    
    public static void setSigningDigestAlgorithmMapping(final String s, final String s2) {
        CMSSignedHelper.INSTANCE.setSigningDigestAlgorithmMapping(new DERObjectIdentifier(s), s2);
    }
}
