// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.cert.CertStoreException;
import java.util.Iterator;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.NoSuchProviderException;
import org.bouncycastle.x509.NoSuchStoreException;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.ASN1Set;
import java.util.Collection;
import org.bouncycastle.asn1.cms.SignerInfo;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.InputStream;
import java.util.Map;
import org.bouncycastle.x509.X509Store;
import java.security.cert.CertStore;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.SignedData;

public class CMSSignedData
{
    private static final CMSSignedHelper HELPER;
    SignedData signedData;
    ContentInfo contentInfo;
    CMSProcessable signedContent;
    CertStore certStore;
    SignerInformationStore signerInfoStore;
    X509Store attributeStore;
    X509Store certificateStore;
    X509Store crlStore;
    private Map hashes;
    
    private CMSSignedData(final CMSSignedData cmsSignedData) {
        this.signedData = cmsSignedData.signedData;
        this.contentInfo = cmsSignedData.contentInfo;
        this.signedContent = cmsSignedData.signedContent;
        this.certStore = cmsSignedData.certStore;
        this.signerInfoStore = cmsSignedData.signerInfoStore;
    }
    
    public CMSSignedData(final byte[] array) throws CMSException {
        this(CMSUtils.readContentInfo(array));
    }
    
    public CMSSignedData(final CMSProcessable cmsProcessable, final byte[] array) throws CMSException {
        this(cmsProcessable, CMSUtils.readContentInfo(array));
    }
    
    public CMSSignedData(final Map map, final byte[] array) throws CMSException {
        this(map, CMSUtils.readContentInfo(array));
    }
    
    public CMSSignedData(final CMSProcessable cmsProcessable, final InputStream inputStream) throws CMSException {
        this(cmsProcessable, CMSUtils.readContentInfo((InputStream)new ASN1InputStream(inputStream)));
    }
    
    public CMSSignedData(final InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }
    
    public CMSSignedData(final CMSProcessable signedContent, final ContentInfo contentInfo) {
        this.signedContent = signedContent;
        this.contentInfo = contentInfo;
        this.signedData = SignedData.getInstance(this.contentInfo.getContent());
    }
    
    public CMSSignedData(final Map hashes, final ContentInfo contentInfo) {
        this.hashes = hashes;
        this.contentInfo = contentInfo;
        this.signedData = SignedData.getInstance(this.contentInfo.getContent());
    }
    
    public CMSSignedData(final ContentInfo contentInfo) {
        this.contentInfo = contentInfo;
        this.signedData = SignedData.getInstance(this.contentInfo.getContent());
        if (this.signedData.getEncapContentInfo().getContent() != null) {
            this.signedContent = new CMSProcessableByteArray(((ASN1OctetString)this.signedData.getEncapContentInfo().getContent()).getOctets());
        }
        else {
            this.signedContent = null;
        }
    }
    
    public int getVersion() {
        return this.signedData.getVersion().getValue().intValue();
    }
    
    public SignerInformationStore getSignerInfos() {
        if (this.signerInfoStore == null) {
            final ASN1Set signerInfos = this.signedData.getSignerInfos();
            final ArrayList<SignerInformation> list = new ArrayList<SignerInformation>();
            for (int i = 0; i != signerInfos.size(); ++i) {
                final SignerInfo instance = SignerInfo.getInstance(signerInfos.getObjectAt(i));
                final DERObjectIdentifier contentType = this.signedData.getEncapContentInfo().getContentType();
                if (this.hashes == null) {
                    list.add(new SignerInformation(instance, contentType, this.signedContent, null));
                }
                else {
                    list.add(new SignerInformation(instance, contentType, null, new BaseDigestCalculator(this.hashes.get(instance.getDigestAlgorithm().getObjectId().getId()))));
                }
            }
            this.signerInfoStore = new SignerInformationStore(list);
        }
        return this.signerInfoStore;
    }
    
    public X509Store getAttributeCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getAttributeCertificates(s, CMSUtils.getProvider(s2));
    }
    
    public X509Store getAttributeCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this.attributeStore == null) {
            this.attributeStore = CMSSignedData.HELPER.createAttributeStore(s, provider, this.signedData.getCertificates());
        }
        return this.attributeStore;
    }
    
    public X509Store getCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCertificates(s, CMSUtils.getProvider(s2));
    }
    
    public X509Store getCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this.certificateStore == null) {
            this.certificateStore = CMSSignedData.HELPER.createCertificateStore(s, provider, this.signedData.getCertificates());
        }
        return this.certificateStore;
    }
    
    public X509Store getCRLs(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCRLs(s, CMSUtils.getProvider(s2));
    }
    
    public X509Store getCRLs(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this.crlStore == null) {
            this.crlStore = CMSSignedData.HELPER.createCRLsStore(s, provider, this.signedData.getCRLs());
        }
        return this.crlStore;
    }
    
    public CertStore getCertificatesAndCRLs(final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.getCertificatesAndCRLs(s, CMSUtils.getProvider(s2));
    }
    
    public CertStore getCertificatesAndCRLs(final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        if (this.certStore == null) {
            this.certStore = CMSSignedData.HELPER.createCertStore(s, provider, this.signedData.getCertificates(), this.signedData.getCRLs());
        }
        return this.certStore;
    }
    
    public String getSignedContentTypeOID() {
        return this.signedData.getEncapContentInfo().getContentType().getId();
    }
    
    public CMSProcessable getSignedContent() {
        return this.signedContent;
    }
    
    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }
    
    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }
    
    public static CMSSignedData replaceSigners(final CMSSignedData cmsSignedData, final SignerInformationStore signerInfoStore) {
        final CMSSignedData cmsSignedData2 = new CMSSignedData(cmsSignedData);
        cmsSignedData2.signerInfoStore = signerInfoStore;
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        for (final SignerInformation signerInformation : signerInfoStore.getSigners()) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(signerInformation.getDigestAlgorithmID()));
            asn1EncodableVector2.add(signerInformation.toSignerInfo());
        }
        final DERSet set = new DERSet(asn1EncodableVector);
        final DERSet set2 = new DERSet(asn1EncodableVector2);
        final ASN1Sequence asn1Sequence = (ASN1Sequence)cmsSignedData.signedData.getDERObject();
        final ASN1EncodableVector asn1EncodableVector3 = new ASN1EncodableVector();
        asn1EncodableVector3.add(asn1Sequence.getObjectAt(0));
        asn1EncodableVector3.add(set);
        for (int i = 2; i != asn1Sequence.size() - 1; ++i) {
            asn1EncodableVector3.add(asn1Sequence.getObjectAt(i));
        }
        asn1EncodableVector3.add(set2);
        cmsSignedData2.signedData = SignedData.getInstance(new BERSequence(asn1EncodableVector3));
        cmsSignedData2.contentInfo = new ContentInfo(cmsSignedData2.contentInfo.getContentType(), cmsSignedData2.signedData);
        return cmsSignedData2;
    }
    
    public static CMSSignedData replaceCertificatesAndCRLs(final CMSSignedData cmsSignedData, final CertStore certStore) throws CMSException {
        final CMSSignedData cmsSignedData2 = new CMSSignedData(cmsSignedData);
        cmsSignedData2.certStore = certStore;
        ASN1Set set = null;
        ASN1Set set2 = null;
        try {
            final ASN1Set berSetFromList = CMSUtils.createBerSetFromList(CMSUtils.getCertificatesFromStore(certStore));
            if (berSetFromList.size() != 0) {
                set = berSetFromList;
            }
        }
        catch (CertStoreException ex) {
            throw new CMSException("error getting certs from certStore", ex);
        }
        try {
            final ASN1Set berSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(certStore));
            if (berSetFromList2.size() != 0) {
                set2 = berSetFromList2;
            }
        }
        catch (CertStoreException ex2) {
            throw new CMSException("error getting crls from certStore", ex2);
        }
        cmsSignedData2.signedData = new SignedData(cmsSignedData.signedData.getDigestAlgorithms(), cmsSignedData.signedData.getEncapContentInfo(), set, set2, cmsSignedData.signedData.getSignerInfos());
        cmsSignedData2.contentInfo = new ContentInfo(cmsSignedData2.contentInfo.getContentType(), cmsSignedData2.signedData);
        return cmsSignedData2;
    }
    
    static {
        HELPER = CMSSignedHelper.INSTANCE;
    }
}
