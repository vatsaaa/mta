// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.HashMap;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import java.util.Iterator;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1Set;
import java.io.IOException;
import javax.crypto.Mac;
import java.security.AlgorithmParameterGenerator;
import java.security.AlgorithmParameters;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import java.security.Provider;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.util.Map;

class CMSEnvelopedHelper
{
    static final CMSEnvelopedHelper INSTANCE;
    private static final Map KEYSIZES;
    private static final Map BASE_CIPHER_NAMES;
    private static final Map CIPHER_ALG_NAMES;
    private static final Map MAC_ALG_NAMES;
    
    private String getAsymmetricEncryptionAlgName(final String anObject) {
        if (PKCSObjectIdentifiers.rsaEncryption.getId().equals(anObject)) {
            return "RSA/ECB/PKCS1Padding";
        }
        return anObject;
    }
    
    Cipher createAsymmetricCipher(final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
        try {
            return this.getCipherInstance(this.getAsymmetricEncryptionAlgName(s), provider);
        }
        catch (NoSuchAlgorithmException ex) {
            return this.getCipherInstance(s, provider);
        }
    }
    
    KeyGenerator createSymmetricKeyGenerator(final String s, final Provider provider) throws NoSuchAlgorithmException {
        try {
            return this.createKeyGenerator(s, provider);
        }
        catch (NoSuchAlgorithmException ex) {
            try {
                final String s2 = CMSEnvelopedHelper.BASE_CIPHER_NAMES.get(s);
                if (s2 != null) {
                    return this.createKeyGenerator(s2, provider);
                }
            }
            catch (NoSuchAlgorithmException ex2) {}
            if (provider != null) {
                return this.createSymmetricKeyGenerator(s, null);
            }
            throw ex;
        }
    }
    
    AlgorithmParameters createAlgorithmParameters(final String s, final Provider provider) throws NoSuchAlgorithmException {
        try {
            return this.createAlgorithmParams(s, provider);
        }
        catch (NoSuchAlgorithmException ex) {
            try {
                final String s2 = CMSEnvelopedHelper.BASE_CIPHER_NAMES.get(s);
                if (s2 != null) {
                    return this.createAlgorithmParams(s2, provider);
                }
            }
            catch (NoSuchAlgorithmException ex2) {}
            throw ex;
        }
    }
    
    AlgorithmParameterGenerator createAlgorithmParameterGenerator(final String s, final Provider provider) throws NoSuchAlgorithmException {
        try {
            return this.createAlgorithmParamsGenerator(s, provider);
        }
        catch (NoSuchAlgorithmException ex) {
            try {
                final String s2 = CMSEnvelopedHelper.BASE_CIPHER_NAMES.get(s);
                if (s2 != null) {
                    return this.createAlgorithmParamsGenerator(s2, provider);
                }
            }
            catch (NoSuchAlgorithmException ex2) {}
            throw ex;
        }
    }
    
    String getRFC3211WrapperName(final String str) {
        final String str2 = CMSEnvelopedHelper.BASE_CIPHER_NAMES.get(str);
        if (str2 == null) {
            throw new IllegalArgumentException("no name for " + str);
        }
        return str2 + "RFC3211Wrap";
    }
    
    int getKeySize(final String str) {
        final Integer n = CMSEnvelopedHelper.KEYSIZES.get(str);
        if (n == null) {
            throw new IllegalArgumentException("no keysize for " + str);
        }
        return n;
    }
    
    Cipher getCipherInstance(final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchPaddingException {
        if (provider != null) {
            return Cipher.getInstance(s, provider);
        }
        return Cipher.getInstance(s);
    }
    
    private AlgorithmParameters createAlgorithmParams(final String s, final Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return AlgorithmParameters.getInstance(s, provider);
        }
        return AlgorithmParameters.getInstance(s);
    }
    
    private AlgorithmParameterGenerator createAlgorithmParamsGenerator(final String s, final Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return AlgorithmParameterGenerator.getInstance(s, provider);
        }
        return AlgorithmParameterGenerator.getInstance(s);
    }
    
    private KeyGenerator createKeyGenerator(final String s, final Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return KeyGenerator.getInstance(s, provider);
        }
        return KeyGenerator.getInstance(s);
    }
    
    Cipher getSymmetricCipher(final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchPaddingException {
        try {
            return this.getCipherInstance(s, provider);
        }
        catch (NoSuchAlgorithmException ex) {
            final String s2 = CMSEnvelopedHelper.CIPHER_ALG_NAMES.get(s);
            try {
                return this.getCipherInstance(s2, provider);
            }
            catch (NoSuchAlgorithmException ex2) {
                if (provider != null) {
                    return this.getSymmetricCipher(s, null);
                }
                throw ex;
            }
        }
    }
    
    private Mac createMac(final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchPaddingException {
        if (provider != null) {
            return Mac.getInstance(s, provider);
        }
        return Mac.getInstance(s);
    }
    
    Mac getMac(final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchPaddingException {
        try {
            return this.createMac(s, provider);
        }
        catch (NoSuchAlgorithmException ex) {
            final String s2 = CMSEnvelopedHelper.MAC_ALG_NAMES.get(s);
            try {
                return this.createMac(s2, provider);
            }
            catch (NoSuchAlgorithmException ex2) {
                if (provider != null) {
                    return this.getMac(s, null);
                }
                throw ex;
            }
        }
    }
    
    AlgorithmParameters getEncryptionAlgorithmParameters(final String s, final byte[] params, final Provider provider) throws CMSException {
        if (params == null) {
            return null;
        }
        try {
            final AlgorithmParameters algorithmParameters = this.createAlgorithmParameters(s, provider);
            algorithmParameters.init(params, "ASN.1");
            return algorithmParameters;
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find parameters for algorithm", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("can't find parse parameters", ex2);
        }
    }
    
    String getSymmetricCipherName(final String s) {
        final String s2 = CMSEnvelopedHelper.BASE_CIPHER_NAMES.get(s);
        if (s2 != null) {
            return s2;
        }
        return s;
    }
    
    static List readRecipientInfos(final ASN1Set set, final byte[] buf, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3) {
        final ArrayList list = new ArrayList();
        for (int i = 0; i != set.size(); ++i) {
            readRecipientInfo(list, RecipientInfo.getInstance(set.getObjectAt(i)), new ByteArrayInputStream(buf), algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3);
        }
        return list;
    }
    
    static List readRecipientInfos(final Iterator iterator, final InputStream inputStream, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3) {
        final ArrayList list = new ArrayList();
        while (iterator.hasNext()) {
            readRecipientInfo(list, iterator.next(), inputStream, algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3);
        }
        return list;
    }
    
    private static void readRecipientInfo(final List list, final RecipientInfo recipientInfo, final InputStream inputStream, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3) {
        final DEREncodable info = recipientInfo.getInfo();
        if (info instanceof KeyTransRecipientInfo) {
            list.add(new KeyTransRecipientInformation((KeyTransRecipientInfo)info, algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, inputStream));
        }
        else if (info instanceof KEKRecipientInfo) {
            list.add(new KEKRecipientInformation((KEKRecipientInfo)info, algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, inputStream));
        }
        else if (info instanceof KeyAgreeRecipientInfo) {
            list.add(new KeyAgreeRecipientInformation((KeyAgreeRecipientInfo)info, algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, inputStream));
        }
        else if (info instanceof PasswordRecipientInfo) {
            list.add(new PasswordRecipientInformation((PasswordRecipientInfo)info, algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, inputStream));
        }
    }
    
    static {
        INSTANCE = new CMSEnvelopedHelper();
        KEYSIZES = new HashMap();
        BASE_CIPHER_NAMES = new HashMap();
        CIPHER_ALG_NAMES = new HashMap();
        MAC_ALG_NAMES = new HashMap();
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, new Integer(192));
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.AES128_CBC, new Integer(128));
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.AES192_CBC, new Integer(192));
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.AES256_CBC, new Integer(256));
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDE");
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AES");
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AES");
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AES");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDE/CBC/PKCS5Padding");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AES/CBC/PKCS5Padding");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AES/CBC/PKCS5Padding");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AES/CBC/PKCS5Padding");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDEMac");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AESMac");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AESMac");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AESMac");
    }
}
