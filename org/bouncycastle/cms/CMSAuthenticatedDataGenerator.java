// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.SecretKey;
import javax.crypto.Mac;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.OriginatorInfo;
import org.bouncycastle.asn1.cms.AuthenticatedData;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.DEREncodable;
import java.security.spec.InvalidParameterSpecException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.BERConstructedOctetString;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.security.Key;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Provider;
import javax.crypto.KeyGenerator;
import java.security.SecureRandom;

public class CMSAuthenticatedDataGenerator extends CMSAuthenticatedGenerator
{
    public CMSAuthenticatedDataGenerator() {
    }
    
    public CMSAuthenticatedDataGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    private CMSAuthenticatedData generate(final CMSProcessable cmsProcessable, final String s, final KeyGenerator keyGenerator, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final Provider provider2 = keyGenerator.getProvider();
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        SecretKey generateKey;
        AlgorithmIdentifier algorithmIdentifier;
        BERConstructedOctetString berConstructedOctetString;
        DEROctetString derOctetString;
        try {
            final Mac mac = CMSEnvelopedHelper.INSTANCE.getMac(s, provider2);
            generateKey = keyGenerator.generateKey();
            final AlgorithmParameterSpec generateParameterSpec = this.generateParameterSpec(s, generateKey, provider2);
            mac.init(generateKey, generateParameterSpec);
            algorithmIdentifier = this.getAlgorithmIdentifier(s, generateParameterSpec, provider2);
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final MacOutputStream macOutputStream = new MacOutputStream(byteArrayOutputStream, mac);
            cmsProcessable.write(macOutputStream);
            macOutputStream.close();
            byteArrayOutputStream.close();
            berConstructedOctetString = new BERConstructedOctetString(byteArrayOutputStream.toByteArray());
            derOctetString = new DEROctetString(macOutputStream.getMac());
        }
        catch (InvalidKeyException ex) {
            throw new CMSException("key invalid in message.", ex);
        }
        catch (NoSuchPaddingException ex2) {
            throw new CMSException("required padding not supported.", ex2);
        }
        catch (InvalidAlgorithmParameterException ex3) {
            throw new CMSException("algorithm parameters invalid.", ex3);
        }
        catch (IOException ex4) {
            throw new CMSException("exception decoding algorithm parameters.", ex4);
        }
        catch (InvalidParameterSpecException ex5) {
            throw new CMSException("exception setting up parameters.", ex5);
        }
        for (final RecipientInfoGenerator recipientInfoGenerator : this.recipientInfoGenerators) {
            try {
                asn1EncodableVector.add(recipientInfoGenerator.generate(generateKey, this.rand, provider));
            }
            catch (InvalidKeyException ex6) {
                throw new CMSException("key inappropriate for algorithm.", ex6);
            }
            catch (GeneralSecurityException ex7) {
                throw new CMSException("error making encrypted content.", ex7);
            }
        }
        return new CMSAuthenticatedData(new ContentInfo(CMSObjectIdentifiers.authenticatedData, new AuthenticatedData(null, new DERSet(asn1EncodableVector), algorithmIdentifier, null, new ContentInfo(CMSObjectIdentifiers.data, berConstructedOctetString), null, derOctetString, null)));
    }
    
    public CMSAuthenticatedData generate(final CMSProcessable cmsProcessable, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, s, CMSUtils.getProvider(s2));
    }
    
    public CMSAuthenticatedData generate(final CMSProcessable cmsProcessable, final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(this.rand);
        return this.generate(cmsProcessable, s, symmetricKeyGenerator, provider);
    }
}
