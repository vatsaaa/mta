// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Collection;
import java.util.Map;

public class SignerInformationStore
{
    private Map table;
    
    public SignerInformationStore(final Collection collection) {
        this.table = new HashMap();
        for (final SignerInformation signerInformation : collection) {
            final SignerId sid = signerInformation.getSID();
            if (this.table.get(sid) == null) {
                this.table.put(sid, signerInformation);
            }
            else {
                final Object value = this.table.get(sid);
                if (value instanceof List) {
                    ((List<List>)value).add((List)signerInformation);
                }
                else {
                    final ArrayList<SignerInformation> list = new ArrayList<SignerInformation>();
                    list.add((List<List>)value);
                    list.add(signerInformation);
                    this.table.put(sid, list);
                }
            }
        }
    }
    
    public SignerInformation get(final SignerId signerId) {
        final SignerInformation value = this.table.get(signerId);
        if (value instanceof List) {
            return ((List<SignerInformation>)value).get(0);
        }
        return value;
    }
    
    public int size() {
        final Iterator<List> iterator = this.table.values().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final List next = iterator.next();
            if (next instanceof List) {
                n += next.size();
            }
            else {
                ++n;
            }
        }
        return n;
    }
    
    public Collection getSigners() {
        final ArrayList<List<?>> list = new ArrayList<List<?>>(this.table.size());
        for (final List<?> next : this.table.values()) {
            if (next instanceof List) {
                list.addAll((Collection<?>)next);
            }
            else {
                list.add(next);
            }
        }
        return list;
    }
    
    public Collection getSigners(final SignerId signerId) {
        final List value = this.table.get(signerId);
        if (value instanceof List) {
            return new ArrayList(value);
        }
        if (value != null) {
            return Collections.singletonList(value);
        }
        return new ArrayList();
    }
}
