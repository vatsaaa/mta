// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.util.Arrays;

class BaseDigestCalculator implements DigestCalculator
{
    private final byte[] digest;
    
    BaseDigestCalculator(final byte[] digest) {
        this.digest = digest;
    }
    
    public byte[] getDigest() {
        return Arrays.clone(this.digest);
    }
}
