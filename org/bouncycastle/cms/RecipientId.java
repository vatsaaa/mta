// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.math.BigInteger;
import org.bouncycastle.util.Arrays;
import java.security.cert.X509CertSelector;

public class RecipientId extends X509CertSelector
{
    byte[] keyIdentifier;
    
    public RecipientId() {
        this.keyIdentifier = null;
    }
    
    public void setKeyIdentifier(final byte[] keyIdentifier) {
        this.keyIdentifier = keyIdentifier;
    }
    
    public byte[] getKeyIdentifier() {
        return this.keyIdentifier;
    }
    
    @Override
    public int hashCode() {
        int n = Arrays.hashCode(this.keyIdentifier) ^ Arrays.hashCode(this.getSubjectKeyIdentifier());
        final BigInteger serialNumber = this.getSerialNumber();
        if (serialNumber != null) {
            n ^= serialNumber.hashCode();
        }
        final String issuerAsString = this.getIssuerAsString();
        if (issuerAsString != null) {
            n ^= issuerAsString.hashCode();
        }
        return n;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof RecipientId)) {
            return false;
        }
        final RecipientId recipientId = (RecipientId)o;
        return Arrays.areEqual(this.keyIdentifier, recipientId.keyIdentifier) && Arrays.areEqual(this.getSubjectKeyIdentifier(), recipientId.getSubjectKeyIdentifier()) && this.equalsObj(this.getSerialNumber(), recipientId.getSerialNumber()) && this.equalsObj(this.getIssuerAsString(), recipientId.getIssuerAsString());
    }
    
    private boolean equalsObj(final Object o, final Object obj) {
        return (o != null) ? o.equals(obj) : (obj == null);
    }
}
