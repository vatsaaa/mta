// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.Provider;
import org.bouncycastle.util.io.Streams;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import org.bouncycastle.asn1.BEROctetStringGenerator;
import java.io.OutputStream;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Set;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.security.cert.CRLSelector;
import org.bouncycastle.asn1.x509.CertificateList;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.util.Iterator;
import java.security.cert.CertificateEncodingException;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Object;
import java.security.cert.X509Certificate;
import java.security.cert.CertSelector;
import org.bouncycastle.asn1.x509.X509CertificateStructure;
import java.util.ArrayList;
import java.util.List;
import java.security.cert.CertStore;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.cms.ContentInfo;

class CMSUtils
{
    private static final Runtime RUNTIME;
    
    static int getMaximumMemory() {
        final long maxMemory = CMSUtils.RUNTIME.maxMemory();
        if (maxMemory > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int)maxMemory;
    }
    
    static ContentInfo readContentInfo(final byte[] array) throws CMSException {
        return readContentInfo(new ASN1InputStream(array));
    }
    
    static ContentInfo readContentInfo(final InputStream inputStream) throws CMSException {
        return readContentInfo(new ASN1InputStream(inputStream, getMaximumMemory()));
    }
    
    static List getCertificatesFromStore(final CertStore certStore) throws CertStoreException, CMSException {
        final ArrayList<X509CertificateStructure> list = new ArrayList<X509CertificateStructure>();
        try {
            final Iterator<? extends Certificate> iterator = certStore.getCertificates(null).iterator();
            while (iterator.hasNext()) {
                list.add(X509CertificateStructure.getInstance(ASN1Object.fromByteArray(((X509Certificate)iterator.next()).getEncoded())));
            }
            return list;
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("error processing certs", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("error processing certs", ex2);
        }
        catch (CertificateEncodingException ex3) {
            throw new CMSException("error encoding certs", ex3);
        }
    }
    
    static List getCRLsFromStore(final CertStore certStore) throws CertStoreException, CMSException {
        final ArrayList<CertificateList> list = new ArrayList<CertificateList>();
        try {
            final Iterator<? extends CRL> iterator = certStore.getCRLs(null).iterator();
            while (iterator.hasNext()) {
                list.add(CertificateList.getInstance(ASN1Object.fromByteArray(((X509CRL)iterator.next()).getEncoded())));
            }
            return list;
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("error processing crls", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("error processing crls", ex2);
        }
        catch (CRLException ex3) {
            throw new CMSException("error encoding crls", ex3);
        }
    }
    
    static ASN1Set createBerSetFromList(final List list) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<DEREncodable> iterator = list.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(iterator.next());
        }
        return new BERSet(asn1EncodableVector);
    }
    
    static ASN1Set createDerSetFromList(final List list) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<DEREncodable> iterator = list.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(iterator.next());
        }
        return new DERSet(asn1EncodableVector);
    }
    
    static OutputStream createBEROctetOutputStream(final OutputStream outputStream, final int n, final boolean b, final int n2) throws IOException {
        final BEROctetStringGenerator berOctetStringGenerator = new BEROctetStringGenerator(outputStream, n, b);
        if (n2 != 0) {
            return berOctetStringGenerator.getOctetOutputStream(new byte[n2]);
        }
        return berOctetStringGenerator.getOctetOutputStream();
    }
    
    static TBSCertificateStructure getTBSCertificateStructure(final X509Certificate x509Certificate) throws CertificateEncodingException {
        try {
            return TBSCertificateStructure.getInstance(ASN1Object.fromByteArray(x509Certificate.getTBSCertificate()));
        }
        catch (IOException ex) {
            throw new CertificateEncodingException(ex.toString());
        }
    }
    
    private static ContentInfo readContentInfo(final ASN1InputStream asn1InputStream) throws CMSException {
        try {
            return ContentInfo.getInstance(asn1InputStream.readObject());
        }
        catch (IOException ex) {
            throw new CMSException("IOException reading content.", ex);
        }
        catch (ClassCastException ex2) {
            throw new CMSException("Malformed content.", ex2);
        }
        catch (IllegalArgumentException ex3) {
            throw new CMSException("Malformed content.", ex3);
        }
    }
    
    public static byte[] streamToByteArray(final InputStream inputStream) throws IOException {
        return Streams.readAll(inputStream);
    }
    
    public static byte[] streamToByteArray(final InputStream inputStream, final int n) throws IOException {
        return Streams.readAllLimited(inputStream, n);
    }
    
    public static Provider getProvider(final String s) throws NoSuchProviderException {
        if (s == null) {
            return null;
        }
        final Provider provider = Security.getProvider(s);
        if (provider != null) {
            return provider;
        }
        throw new NoSuchProviderException("provider " + s + " not found.");
    }
    
    static {
        RUNTIME = Runtime.getRuntime();
    }
}
