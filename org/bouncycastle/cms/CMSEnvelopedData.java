// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.AlgorithmParameters;
import java.io.IOException;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.cms.EncryptedContentInfo;
import java.util.Collection;
import org.bouncycastle.asn1.cms.EnvelopedData;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.ContentInfo;

public class CMSEnvelopedData
{
    RecipientInformationStore recipientInfoStore;
    ContentInfo contentInfo;
    private AlgorithmIdentifier encAlg;
    private ASN1Set unprotectedAttributes;
    
    public CMSEnvelopedData(final byte[] array) throws CMSException {
        this(CMSUtils.readContentInfo(array));
    }
    
    public CMSEnvelopedData(final InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }
    
    public CMSEnvelopedData(final ContentInfo contentInfo) throws CMSException {
        this.contentInfo = contentInfo;
        final EnvelopedData instance = EnvelopedData.getInstance(contentInfo.getContent());
        final EncryptedContentInfo encryptedContentInfo = instance.getEncryptedContentInfo();
        this.encAlg = encryptedContentInfo.getContentEncryptionAlgorithm();
        this.recipientInfoStore = new RecipientInformationStore(CMSEnvelopedHelper.readRecipientInfos(instance.getRecipientInfos(), encryptedContentInfo.getEncryptedContent().getOctets(), this.encAlg, null, null));
        this.unprotectedAttributes = instance.getUnprotectedAttrs();
    }
    
    private byte[] encodeObj(final DEREncodable derEncodable) throws IOException {
        if (derEncodable != null) {
            return derEncodable.getDERObject().getEncoded();
        }
        return null;
    }
    
    public String getEncryptionAlgOID() {
        return this.encAlg.getObjectId().getId();
    }
    
    public byte[] getEncryptionAlgParams() {
        try {
            return this.encodeObj(this.encAlg.getParameters());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AlgorithmParameters getEncryptionAlgorithmParameters(final String s) throws CMSException, NoSuchProviderException {
        return this.getEncryptionAlgorithmParameters(CMSUtils.getProvider(s));
    }
    
    public AlgorithmParameters getEncryptionAlgorithmParameters(final Provider provider) throws CMSException {
        return CMSEnvelopedHelper.INSTANCE.getEncryptionAlgorithmParameters(this.getEncryptionAlgOID(), this.getEncryptionAlgParams(), provider);
    }
    
    public RecipientInformationStore getRecipientInfos() {
        return this.recipientInfoStore;
    }
    
    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }
    
    public AttributeTable getUnprotectedAttributes() {
        if (this.unprotectedAttributes == null) {
            return null;
        }
        return new AttributeTable(this.unprotectedAttributes);
    }
    
    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }
}
