// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.IOException;
import java.io.OutputStream;

public class CMSProcessableByteArray implements CMSProcessable
{
    private byte[] bytes;
    
    public CMSProcessableByteArray(final byte[] bytes) {
        this.bytes = bytes;
    }
    
    public void write(final OutputStream outputStream) throws IOException, CMSException {
        outputStream.write(this.bytes);
    }
    
    public Object getContent() {
        return this.bytes.clone();
    }
}
