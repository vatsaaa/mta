// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.cms.EncryptedContentInfo;
import java.util.Collection;
import org.bouncycastle.asn1.cms.AuthEnvelopedData;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.OriginatorInfo;
import org.bouncycastle.asn1.cms.ContentInfo;

class CMSAuthEnvelopedData
{
    RecipientInformationStore recipientInfoStore;
    ContentInfo contentInfo;
    private OriginatorInfo originator;
    private AlgorithmIdentifier authEncAlg;
    private ASN1Set authAttrs;
    private byte[] mac;
    private ASN1Set unauthAttrs;
    
    public CMSAuthEnvelopedData(final byte[] array) throws CMSException {
        this(CMSUtils.readContentInfo(array));
    }
    
    public CMSAuthEnvelopedData(final InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }
    
    public CMSAuthEnvelopedData(final ContentInfo contentInfo) throws CMSException {
        this.contentInfo = contentInfo;
        final AuthEnvelopedData instance = AuthEnvelopedData.getInstance(contentInfo.getContent());
        this.originator = instance.getOriginatorInfo();
        final EncryptedContentInfo authEncryptedContentInfo = instance.getAuthEncryptedContentInfo();
        this.authEncAlg = authEncryptedContentInfo.getContentEncryptionAlgorithm();
        this.recipientInfoStore = new RecipientInformationStore(CMSEnvelopedHelper.readRecipientInfos(instance.getRecipientInfos(), authEncryptedContentInfo.getEncryptedContent().getOctets(), null, null, this.authEncAlg));
        this.authAttrs = instance.getAuthAttrs();
        this.mac = instance.getMac().getOctets();
        this.unauthAttrs = instance.getUnauthAttrs();
    }
}
