// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;
import java.security.Provider;
import java.security.Key;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.RecipientIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.ASN1OctetString;
import java.io.InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;

public class KeyTransRecipientInformation extends RecipientInformation
{
    private KeyTransRecipientInfo info;
    
    @Deprecated
    public KeyTransRecipientInformation(final KeyTransRecipientInfo keyTransRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final InputStream inputStream) {
        this(keyTransRecipientInfo, algorithmIdentifier, null, null, inputStream);
    }
    
    @Deprecated
    public KeyTransRecipientInformation(final KeyTransRecipientInfo keyTransRecipientInfo, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final InputStream inputStream) {
        this(keyTransRecipientInfo, algorithmIdentifier, algorithmIdentifier2, null, inputStream);
    }
    
    KeyTransRecipientInformation(final KeyTransRecipientInfo info, final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final AlgorithmIdentifier algorithmIdentifier3, final InputStream inputStream) {
        super(algorithmIdentifier, algorithmIdentifier2, algorithmIdentifier3, info.getKeyEncryptionAlgorithm(), inputStream);
        this.info = info;
        this.rid = new RecipientId();
        final RecipientIdentifier recipientIdentifier = info.getRecipientIdentifier();
        try {
            if (recipientIdentifier.isTagged()) {
                this.rid.setSubjectKeyIdentifier(ASN1OctetString.getInstance(recipientIdentifier.getId()).getOctets());
            }
            else {
                final IssuerAndSerialNumber instance = IssuerAndSerialNumber.getInstance(recipientIdentifier.getId());
                this.rid.setIssuer(instance.getName().getEncoded());
                this.rid.setSerialNumber(instance.getSerialNumber().getValue());
            }
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("invalid rid in KeyTransRecipientInformation");
        }
    }
    
    private String getExchangeEncryptionAlgorithmName(final DERObjectIdentifier derObjectIdentifier) {
        if (PKCSObjectIdentifiers.rsaEncryption.equals(derObjectIdentifier)) {
            return "RSA/ECB/PKCS1Padding";
        }
        return derObjectIdentifier.getId();
    }
    
    protected Key getSessionKey(final Key key, final Provider provider) throws CMSException {
        final byte[] octets = this.info.getEncryptedKey().getOctets();
        final String exchangeEncryptionAlgorithmName = this.getExchangeEncryptionAlgorithmName(this.keyEncAlg.getObjectId());
        final String symmetricCipherName = CMSEnvelopedHelper.INSTANCE.getSymmetricCipherName(this.getActiveAlgID().getObjectId().getId());
        try {
            final Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(exchangeEncryptionAlgorithmName, provider);
            Key unwrap;
            try {
                symmetricCipher.init(4, key);
                unwrap = symmetricCipher.unwrap(octets, symmetricCipherName, 3);
            }
            catch (GeneralSecurityException ex6) {
                symmetricCipher.init(2, key);
                unwrap = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            }
            catch (IllegalStateException ex7) {
                symmetricCipher.init(2, key);
                unwrap = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            }
            catch (UnsupportedOperationException ex8) {
                symmetricCipher.init(2, key);
                unwrap = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            }
            catch (ProviderException ex9) {
                symmetricCipher.init(2, key);
                unwrap = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            }
            return unwrap;
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find algorithm.", ex);
        }
        catch (InvalidKeyException ex2) {
            throw new CMSException("key invalid in message.", ex2);
        }
        catch (NoSuchPaddingException ex3) {
            throw new CMSException("required padding not supported.", ex3);
        }
        catch (IllegalBlockSizeException ex4) {
            throw new CMSException("illegal blocksize in message.", ex4);
        }
        catch (BadPaddingException ex5) {
            throw new CMSException("bad padding in message.", ex5);
        }
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final Provider provider) throws CMSException {
        return this.getContentFromSessionKey(this.getSessionKey(key, provider), provider);
    }
}
