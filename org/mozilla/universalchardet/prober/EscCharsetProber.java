// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.statemachine.SMModel;
import org.mozilla.universalchardet.prober.statemachine.ISO2022KRSMModel;
import org.mozilla.universalchardet.prober.statemachine.ISO2022JPSMModel;
import org.mozilla.universalchardet.prober.statemachine.ISO2022CNSMModel;
import org.mozilla.universalchardet.prober.statemachine.HZSMModel;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;

public class EscCharsetProber extends CharsetProber
{
    private CodingStateMachine[] codingSM;
    private int activeSM;
    private ProbingState state;
    private String detectedCharset;
    private static final HZSMModel hzsModel;
    private static final ISO2022CNSMModel iso2022cnModel;
    private static final ISO2022JPSMModel iso2022jpModel;
    private static final ISO2022KRSMModel iso2022krModel;
    
    public EscCharsetProber() {
        (this.codingSM = new CodingStateMachine[4])[0] = new CodingStateMachine(EscCharsetProber.hzsModel);
        this.codingSM[1] = new CodingStateMachine(EscCharsetProber.iso2022cnModel);
        this.codingSM[2] = new CodingStateMachine(EscCharsetProber.iso2022jpModel);
        this.codingSM[3] = new CodingStateMachine(EscCharsetProber.iso2022krModel);
        this.reset();
    }
    
    @Override
    public String getCharSetName() {
        return this.detectedCharset;
    }
    
    @Override
    public float getConfidence() {
        return 0.99f;
    }
    
    @Override
    public ProbingState getState() {
        return this.state;
    }
    
    @Override
    public ProbingState handleData(final byte[] buf, final int offset, final int length) {
        for (int maxPos = offset + length, i = offset; i < maxPos && this.state == ProbingState.DETECTING; ++i) {
            for (int j = this.activeSM - 1; j >= 0; --j) {
                final int codingState = this.codingSM[j].nextState(buf[i]);
                if (codingState == 1) {
                    --this.activeSM;
                    if (this.activeSM <= 0) {
                        return this.state = ProbingState.NOT_ME;
                    }
                    if (j != this.activeSM) {
                        final CodingStateMachine t = this.codingSM[this.activeSM];
                        this.codingSM[this.activeSM] = this.codingSM[j];
                        this.codingSM[j] = t;
                    }
                }
                else if (codingState == 2) {
                    this.state = ProbingState.FOUND_IT;
                    this.detectedCharset = this.codingSM[j].getCodingStateMachine();
                    return this.state;
                }
            }
        }
        return this.state;
    }
    
    @Override
    public void reset() {
        this.state = ProbingState.DETECTING;
        for (int i = 0; i < this.codingSM.length; ++i) {
            this.codingSM[i].reset();
        }
        this.activeSM = this.codingSM.length;
        this.detectedCharset = null;
    }
    
    @Override
    public void setOption() {
    }
    
    static {
        hzsModel = new HZSMModel();
        iso2022cnModel = new ISO2022CNSMModel();
        iso2022jpModel = new ISO2022JPSMModel();
        iso2022krModel = new ISO2022KRSMModel();
    }
}
