// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober.sequence;

public abstract class SequenceModel
{
    protected short[] charToOrderMap;
    protected byte[] precedenceMatrix;
    protected float typicalPositiveRatio;
    protected boolean keepEnglishLetter;
    protected String charsetName;
    
    public SequenceModel(final short[] charToOrderMap, final byte[] precedenceMatrix, final float typicalPositiveRatio, final boolean keepEnglishLetter, final String charsetName) {
        this.charToOrderMap = charToOrderMap;
        this.precedenceMatrix = precedenceMatrix;
        this.typicalPositiveRatio = typicalPositiveRatio;
        this.keepEnglishLetter = keepEnglishLetter;
        this.charsetName = charsetName;
    }
    
    public short getOrder(final byte b) {
        final int c = b & 0xFF;
        return this.charToOrderMap[c];
    }
    
    public byte getPrecedence(final int pos) {
        return this.precedenceMatrix[pos];
    }
    
    public float getTypicalPositiveRatio() {
        return this.typicalPositiveRatio;
    }
    
    public boolean getKeepEnglishLetter() {
        return this.keepEnglishLetter;
    }
    
    public String getCharsetName() {
        return this.charsetName;
    }
}
