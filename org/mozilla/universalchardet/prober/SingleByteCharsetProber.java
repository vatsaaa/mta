// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.sequence.SequenceModel;

public class SingleByteCharsetProber extends CharsetProber
{
    public static final int SAMPLE_SIZE = 64;
    public static final int SB_ENOUGH_REL_THRESHOLD = 1024;
    public static final float POSITIVE_SHORTCUT_THRESHOLD = 0.95f;
    public static final float NEGATIVE_SHORTCUT_THRESHOLD = 0.05f;
    public static final int SYMBOL_CAT_ORDER = 250;
    public static final int NUMBER_OF_SEQ_CAT = 4;
    public static final int POSITIVE_CAT = 3;
    public static final int NEGATIVE_CAT = 0;
    private ProbingState state;
    private SequenceModel model;
    private boolean reversed;
    private short lastOrder;
    private int totalSeqs;
    private int[] seqCounters;
    private int totalChar;
    private int freqChar;
    private CharsetProber nameProber;
    
    public SingleByteCharsetProber(final SequenceModel model) {
        this.model = model;
        this.reversed = false;
        this.nameProber = null;
        this.seqCounters = new int[4];
        this.reset();
    }
    
    public SingleByteCharsetProber(final SequenceModel model, final boolean reversed, final CharsetProber nameProber) {
        this.model = model;
        this.reversed = reversed;
        this.nameProber = nameProber;
        this.seqCounters = new int[4];
        this.reset();
    }
    
    boolean keepEnglishLetters() {
        return this.model.getKeepEnglishLetter();
    }
    
    @Override
    public String getCharSetName() {
        if (this.nameProber == null) {
            return this.model.getCharsetName();
        }
        return this.nameProber.getCharSetName();
    }
    
    @Override
    public float getConfidence() {
        if (this.totalSeqs > 0) {
            float r = 1.0f * this.seqCounters[3] / this.totalSeqs / this.model.getTypicalPositiveRatio();
            r = r * this.freqChar / this.totalChar;
            if (r >= 1.0f) {
                r = 0.99f;
            }
            return r;
        }
        return 0.01f;
    }
    
    @Override
    public ProbingState getState() {
        return this.state;
    }
    
    @Override
    public ProbingState handleData(final byte[] buf, final int offset, final int length) {
        for (int maxPos = offset + length, i = offset; i < maxPos; ++i) {
            final short order = this.model.getOrder(buf[i]);
            if (order < 250) {
                ++this.totalChar;
            }
            if (order < 64) {
                ++this.freqChar;
                if (this.lastOrder < 64) {
                    ++this.totalSeqs;
                    if (!this.reversed) {
                        final int[] seqCounters = this.seqCounters;
                        final byte precedence = this.model.getPrecedence(this.lastOrder * 64 + order);
                        ++seqCounters[precedence];
                    }
                    else {
                        final int[] seqCounters2 = this.seqCounters;
                        final byte precedence2 = this.model.getPrecedence(order * 64 + this.lastOrder);
                        ++seqCounters2[precedence2];
                    }
                }
            }
            this.lastOrder = order;
        }
        if (this.state == ProbingState.DETECTING && this.totalSeqs > 1024) {
            final float cf = this.getConfidence();
            if (cf > 0.95f) {
                this.state = ProbingState.FOUND_IT;
            }
            else if (cf < 0.05f) {
                this.state = ProbingState.NOT_ME;
            }
        }
        return this.state;
    }
    
    @Override
    public void reset() {
        this.state = ProbingState.DETECTING;
        this.lastOrder = 255;
        for (int i = 0; i < 4; ++i) {
            this.seqCounters[i] = 0;
        }
        this.totalSeqs = 0;
        this.totalChar = 0;
        this.freqChar = 0;
    }
    
    @Override
    public void setOption() {
    }
}
