// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

public class MBCSGroupProber extends CharsetProber
{
    private ProbingState state;
    private CharsetProber[] probers;
    private boolean[] isActive;
    private int bestGuess;
    private int activeNum;
    
    public MBCSGroupProber() {
        this.probers = new CharsetProber[7];
        this.isActive = new boolean[7];
        this.probers[0] = new UTF8Prober();
        this.probers[1] = new SJISProber();
        this.probers[2] = new EUCJPProber();
        this.probers[3] = new GB18030Prober();
        this.probers[4] = new EUCKRProber();
        this.probers[5] = new Big5Prober();
        this.probers[6] = new EUCTWProber();
        this.reset();
    }
    
    @Override
    public String getCharSetName() {
        if (this.bestGuess == -1) {
            this.getConfidence();
            if (this.bestGuess == -1) {
                this.bestGuess = 0;
            }
        }
        return this.probers[this.bestGuess].getCharSetName();
    }
    
    @Override
    public float getConfidence() {
        float bestConf = 0.0f;
        if (this.state == ProbingState.FOUND_IT) {
            return 0.99f;
        }
        if (this.state == ProbingState.NOT_ME) {
            return 0.01f;
        }
        for (int i = 0; i < this.probers.length; ++i) {
            if (this.isActive[i]) {
                final float cf = this.probers[i].getConfidence();
                if (bestConf < cf) {
                    bestConf = cf;
                    this.bestGuess = i;
                }
            }
        }
        return bestConf;
    }
    
    @Override
    public ProbingState getState() {
        return this.state;
    }
    
    @Override
    public ProbingState handleData(final byte[] buf, final int offset, final int length) {
        boolean keepNext = true;
        final byte[] highbyteBuf = new byte[length];
        int highpos = 0;
        for (int maxPos = offset + length, i = offset; i < maxPos; ++i) {
            if ((buf[i] & 0x80) != 0x0) {
                highbyteBuf[highpos++] = buf[i];
                keepNext = true;
            }
            else if (keepNext) {
                highbyteBuf[highpos++] = buf[i];
                keepNext = false;
            }
        }
        for (int i = 0; i < this.probers.length; ++i) {
            if (this.isActive[i]) {
                final ProbingState st = this.probers[i].handleData(highbyteBuf, 0, highpos);
                if (st == ProbingState.FOUND_IT) {
                    this.bestGuess = i;
                    this.state = ProbingState.FOUND_IT;
                    break;
                }
                if (st == ProbingState.NOT_ME) {
                    this.isActive[i] = false;
                    --this.activeNum;
                    if (this.activeNum <= 0) {
                        this.state = ProbingState.NOT_ME;
                        break;
                    }
                }
            }
        }
        return this.state;
    }
    
    @Override
    public void reset() {
        this.activeNum = 0;
        for (int i = 0; i < this.probers.length; ++i) {
            this.probers[i].reset();
            this.isActive[i] = true;
            ++this.activeNum;
        }
        this.bestGuess = -1;
        this.state = ProbingState.DETECTING;
    }
    
    @Override
    public void setOption() {
    }
}
