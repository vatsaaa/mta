// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

import java.nio.ByteBuffer;
import org.mozilla.universalchardet.Constants;

public class Latin1Prober extends CharsetProber
{
    public static final byte UDF = 0;
    public static final byte OTH = 1;
    public static final byte ASC = 2;
    public static final byte ASS = 3;
    public static final byte ACV = 4;
    public static final byte ACO = 5;
    public static final byte ASV = 6;
    public static final byte ASO = 7;
    public static final int CLASS_NUM = 8;
    public static final int FREQ_CAT_NUM = 4;
    private ProbingState state;
    private byte lastCharClass;
    private int[] freqCounter;
    private static final byte[] latin1CharToClass;
    private static final byte[] latin1ClassModel;
    
    public Latin1Prober() {
        this.freqCounter = new int[4];
        this.reset();
    }
    
    @Override
    public String getCharSetName() {
        return Constants.CHARSET_WINDOWS_1252;
    }
    
    @Override
    public float getConfidence() {
        if (this.state == ProbingState.NOT_ME) {
            return 0.01f;
        }
        int total = 0;
        for (int i = 0; i < this.freqCounter.length; ++i) {
            total += this.freqCounter[i];
        }
        float confidence;
        if (total <= 0) {
            confidence = 0.0f;
        }
        else {
            confidence = this.freqCounter[3] * 1.0f / total;
            confidence -= this.freqCounter[1] * 20.0f / total;
        }
        if (confidence < 0.0f) {
            confidence = 0.0f;
        }
        confidence *= 0.5f;
        return confidence;
    }
    
    @Override
    public ProbingState getState() {
        return this.state;
    }
    
    @Override
    public ProbingState handleData(final byte[] buf, final int offset, final int length) {
        final ByteBuffer newBufTmp = this.filterWithEnglishLetters(buf, offset, length);
        final byte[] newBuf = newBufTmp.array();
        for (int newBufLen = newBufTmp.position(), i = 0; i < newBufLen; ++i) {
            final int c = newBuf[i] & 0xFF;
            final byte charClass = Latin1Prober.latin1CharToClass[c];
            final byte freq = Latin1Prober.latin1ClassModel[this.lastCharClass * 8 + charClass];
            if (freq == 0) {
                this.state = ProbingState.NOT_ME;
                break;
            }
            final int[] freqCounter = this.freqCounter;
            final byte b = freq;
            ++freqCounter[b];
            this.lastCharClass = charClass;
        }
        return this.state;
    }
    
    @Override
    public void reset() {
        this.state = ProbingState.DETECTING;
        this.lastCharClass = 1;
        for (int i = 0; i < this.freqCounter.length; ++i) {
            this.freqCounter[i] = 0;
        }
    }
    
    @Override
    public void setOption() {
    }
    
    static {
        latin1CharToClass = new byte[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 0, 1, 7, 1, 1, 1, 1, 1, 1, 5, 1, 5, 0, 5, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7, 1, 7, 0, 7, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 6, 6, 6, 6, 6, 1, 6, 6, 6, 6, 6, 7, 7, 7 };
        latin1ClassModel = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 3, 3, 0, 3, 3, 3, 1, 1, 3, 3, 0, 3, 3, 3, 1, 2, 1, 2, 0, 3, 3, 3, 3, 3, 3, 3, 0, 3, 1, 3, 1, 1, 1, 3, 0, 3, 1, 3, 1, 1, 3, 3 };
    }
}
