// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

import java.nio.ByteBuffer;

public abstract class CharsetProber
{
    public static final float SHORTCUT_THRESHOLD = 0.95f;
    public static final int ASCII_A = 97;
    public static final int ASCII_Z = 122;
    public static final int ASCII_A_CAPITAL = 65;
    public static final int ASCII_Z_CAPITAL = 90;
    public static final int ASCII_LT = 60;
    public static final int ASCII_GT = 62;
    public static final int ASCII_SP = 32;
    
    public abstract String getCharSetName();
    
    public abstract ProbingState handleData(final byte[] p0, final int p1, final int p2);
    
    public abstract ProbingState getState();
    
    public abstract void reset();
    
    public abstract float getConfidence();
    
    public abstract void setOption();
    
    public ByteBuffer filterWithoutEnglishLetters(final byte[] buf, final int offset, final int length) {
        final ByteBuffer out = ByteBuffer.allocate(length);
        boolean meetMSB = false;
        int prevPtr = offset;
        int curPtr = offset;
        for (int maxPtr = offset + length; curPtr < maxPtr; ++curPtr) {
            final byte c = buf[curPtr];
            if (!this.isAscii(c)) {
                meetMSB = true;
            }
            else if (this.isAsciiSymbol(c)) {
                if (meetMSB && curPtr > prevPtr) {
                    out.put(buf, prevPtr, curPtr - prevPtr);
                    out.put((byte)32);
                    prevPtr = curPtr + 1;
                    meetMSB = false;
                }
                else {
                    prevPtr = curPtr + 1;
                }
            }
        }
        if (meetMSB && curPtr > prevPtr) {
            out.put(buf, prevPtr, curPtr - prevPtr);
        }
        return out;
    }
    
    public ByteBuffer filterWithEnglishLetters(final byte[] buf, final int offset, final int length) {
        final ByteBuffer out = ByteBuffer.allocate(length);
        boolean isInTag = false;
        int prevPtr = offset;
        int curPtr = offset;
        for (int maxPtr = offset + length; curPtr < maxPtr; ++curPtr) {
            final byte c = buf[curPtr];
            if (c == 62) {
                isInTag = false;
            }
            else if (c == 60) {
                isInTag = true;
            }
            if (this.isAscii(c) && this.isAsciiSymbol(c)) {
                if (curPtr > prevPtr && !isInTag) {
                    out.put(buf, prevPtr, curPtr - prevPtr);
                    out.put((byte)32);
                    prevPtr = curPtr + 1;
                }
                else {
                    prevPtr = curPtr + 1;
                }
            }
        }
        if (!isInTag && curPtr > prevPtr) {
            out.put(buf, prevPtr, curPtr - prevPtr);
        }
        return out;
    }
    
    private boolean isAscii(final byte b) {
        return (b & 0x80) == 0x0;
    }
    
    private boolean isAsciiSymbol(final byte b) {
        final int c = b & 0xFF;
        return c < 65 || (c > 90 && c < 97) || c > 122;
    }
    
    public enum ProbingState
    {
        DETECTING, 
        FOUND_IT, 
        NOT_ME;
    }
}
