// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.sequence.HebrewModel;
import org.mozilla.universalchardet.prober.sequence.Win1251BulgarianModel;
import org.mozilla.universalchardet.prober.sequence.Latin5BulgarianModel;
import org.mozilla.universalchardet.prober.sequence.Win1253Model;
import org.mozilla.universalchardet.prober.sequence.Latin7Model;
import org.mozilla.universalchardet.prober.sequence.Ibm855Model;
import org.mozilla.universalchardet.prober.sequence.Ibm866Model;
import org.mozilla.universalchardet.prober.sequence.MacCyrillicModel;
import org.mozilla.universalchardet.prober.sequence.Latin5Model;
import org.mozilla.universalchardet.prober.sequence.Koi8rModel;
import org.mozilla.universalchardet.prober.sequence.Win1251Model;
import java.nio.ByteBuffer;
import org.mozilla.universalchardet.prober.sequence.SequenceModel;

public class SBCSGroupProber extends CharsetProber
{
    private ProbingState state;
    private CharsetProber[] probers;
    private boolean[] isActive;
    private int bestGuess;
    private int activeNum;
    private static final SequenceModel win1251Model;
    private static final SequenceModel koi8rModel;
    private static final SequenceModel latin5Model;
    private static final SequenceModel macCyrillicModel;
    private static final SequenceModel ibm866Model;
    private static final SequenceModel ibm855Model;
    private static final SequenceModel latin7Model;
    private static final SequenceModel win1253Model;
    private static final SequenceModel latin5BulgarianModel;
    private static final SequenceModel win1251BulgarianModel;
    private static final SequenceModel hebrewModel;
    
    public SBCSGroupProber() {
        this.probers = new CharsetProber[13];
        this.isActive = new boolean[13];
        this.probers[0] = new SingleByteCharsetProber(SBCSGroupProber.win1251Model);
        this.probers[1] = new SingleByteCharsetProber(SBCSGroupProber.koi8rModel);
        this.probers[2] = new SingleByteCharsetProber(SBCSGroupProber.latin5Model);
        this.probers[3] = new SingleByteCharsetProber(SBCSGroupProber.macCyrillicModel);
        this.probers[4] = new SingleByteCharsetProber(SBCSGroupProber.ibm866Model);
        this.probers[5] = new SingleByteCharsetProber(SBCSGroupProber.ibm855Model);
        this.probers[6] = new SingleByteCharsetProber(SBCSGroupProber.latin7Model);
        this.probers[7] = new SingleByteCharsetProber(SBCSGroupProber.win1253Model);
        this.probers[8] = new SingleByteCharsetProber(SBCSGroupProber.latin5BulgarianModel);
        this.probers[9] = new SingleByteCharsetProber(SBCSGroupProber.win1251BulgarianModel);
        final HebrewProber hebprober = new HebrewProber();
        this.probers[10] = hebprober;
        this.probers[11] = new SingleByteCharsetProber(SBCSGroupProber.hebrewModel, false, hebprober);
        this.probers[12] = new SingleByteCharsetProber(SBCSGroupProber.hebrewModel, true, hebprober);
        hebprober.setModalProbers(this.probers[11], this.probers[12]);
        this.reset();
    }
    
    @Override
    public String getCharSetName() {
        if (this.bestGuess == -1) {
            this.getConfidence();
            if (this.bestGuess == -1) {
                this.bestGuess = 0;
            }
        }
        return this.probers[this.bestGuess].getCharSetName();
    }
    
    @Override
    public float getConfidence() {
        float bestConf = 0.0f;
        if (this.state == ProbingState.FOUND_IT) {
            return 0.99f;
        }
        if (this.state == ProbingState.NOT_ME) {
            return 0.01f;
        }
        for (int i = 0; i < this.probers.length; ++i) {
            if (this.isActive[i]) {
                final float cf = this.probers[i].getConfidence();
                if (bestConf < cf) {
                    bestConf = cf;
                    this.bestGuess = i;
                }
            }
        }
        return bestConf;
    }
    
    @Override
    public ProbingState getState() {
        return this.state;
    }
    
    @Override
    public ProbingState handleData(final byte[] buf, final int offset, final int length) {
        final ByteBuffer newbuf = this.filterWithoutEnglishLetters(buf, offset, length);
        if (newbuf.position() != 0) {
            for (int i = 0; i < this.probers.length; ++i) {
                if (this.isActive[i]) {
                    final ProbingState st = this.probers[i].handleData(newbuf.array(), 0, newbuf.position());
                    if (st == ProbingState.FOUND_IT) {
                        this.bestGuess = i;
                        this.state = ProbingState.FOUND_IT;
                        break;
                    }
                    if (st == ProbingState.NOT_ME) {
                        this.isActive[i] = false;
                        --this.activeNum;
                        if (this.activeNum <= 0) {
                            this.state = ProbingState.NOT_ME;
                            break;
                        }
                    }
                }
            }
        }
        return this.state;
    }
    
    @Override
    public void reset() {
        this.activeNum = 0;
        for (int i = 0; i < this.probers.length; ++i) {
            this.probers[i].reset();
            this.isActive[i] = true;
            ++this.activeNum;
        }
        this.bestGuess = -1;
        this.state = ProbingState.DETECTING;
    }
    
    @Override
    public void setOption() {
    }
    
    static {
        win1251Model = new Win1251Model();
        koi8rModel = new Koi8rModel();
        latin5Model = new Latin5Model();
        macCyrillicModel = new MacCyrillicModel();
        ibm866Model = new Ibm866Model();
        ibm855Model = new Ibm855Model();
        latin7Model = new Latin7Model();
        win1253Model = new Win1253Model();
        latin5BulgarianModel = new Latin5BulgarianModel();
        win1251BulgarianModel = new Win1251BulgarianModel();
        hebrewModel = new HebrewModel();
    }
}
