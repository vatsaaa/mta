// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.statemachine.UTF8SMModel;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.statemachine.SMModel;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;

public class UTF8Prober extends CharsetProber
{
    public static final float ONE_CHAR_PROB = 0.5f;
    private CodingStateMachine codingSM;
    private ProbingState state;
    private int numOfMBChar;
    private static final SMModel smModel;
    
    public UTF8Prober() {
        this.numOfMBChar = 0;
        this.codingSM = new CodingStateMachine(UTF8Prober.smModel);
        this.reset();
    }
    
    @Override
    public String getCharSetName() {
        return Constants.CHARSET_UTF_8;
    }
    
    @Override
    public ProbingState handleData(final byte[] buf, final int offset, final int length) {
        for (int maxPos = offset + length, i = offset; i < maxPos; ++i) {
            final int codingState = this.codingSM.nextState(buf[i]);
            if (codingState == 1) {
                this.state = ProbingState.NOT_ME;
                break;
            }
            if (codingState == 2) {
                this.state = ProbingState.FOUND_IT;
                break;
            }
            if (codingState == 0 && this.codingSM.getCurrentCharLen() >= 2) {
                ++this.numOfMBChar;
            }
        }
        if (this.state == ProbingState.DETECTING && this.getConfidence() > 0.95f) {
            this.state = ProbingState.FOUND_IT;
        }
        return this.state;
    }
    
    @Override
    public ProbingState getState() {
        return this.state;
    }
    
    @Override
    public void reset() {
        this.codingSM.reset();
        this.numOfMBChar = 0;
        this.state = ProbingState.DETECTING;
    }
    
    @Override
    public float getConfidence() {
        float unlike = 0.99f;
        if (this.numOfMBChar < 6) {
            for (int i = 0; i < this.numOfMBChar; ++i) {
                unlike *= 0.5f;
            }
            return 1.0f - unlike;
        }
        return 0.99f;
    }
    
    @Override
    public void setOption() {
    }
    
    static {
        smModel = new UTF8SMModel();
    }
}
