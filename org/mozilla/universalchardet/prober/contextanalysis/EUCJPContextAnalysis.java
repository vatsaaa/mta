// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober.contextanalysis;

public class EUCJPContextAnalysis extends JapaneseContextAnalysis
{
    public static final int HIRAGANA_HIGHBYTE = 164;
    public static final int HIRAGANA_LOWBYTE_BEGIN = 161;
    public static final int HIRAGANA_LOWBYTE_END = 243;
    public static final int SINGLE_SHIFT_2 = 142;
    public static final int SINGLE_SHIFT_3 = 143;
    public static final int FIRSTPLANE_HIGHBYTE_BEGIN = 161;
    public static final int FIRSTPLANE_HIGHBYTE_END = 254;
    
    @Override
    protected void getOrder(final Order order, final byte[] buf, final int offset) {
        order.order = -1;
        order.charLength = 1;
        final int firstByte = buf[offset] & 0xFF;
        if (firstByte == 142 || (firstByte >= 161 && firstByte <= 254)) {
            order.charLength = 2;
        }
        else if (firstByte == 143) {
            order.charLength = 3;
        }
        if (firstByte == 164) {
            final int secondByte = buf[offset + 1] & 0xFF;
            if (secondByte >= 161 && secondByte <= 243) {
                order.order = secondByte - 161;
            }
        }
    }
    
    @Override
    protected int getOrder(final byte[] buf, final int offset) {
        final int highbyte = buf[offset] & 0xFF;
        if (highbyte == 164) {
            final int lowbyte = buf[offset + 1] & 0xFF;
            if (lowbyte >= 161 && lowbyte <= 243) {
                return lowbyte - 161;
            }
        }
        return -1;
    }
}
