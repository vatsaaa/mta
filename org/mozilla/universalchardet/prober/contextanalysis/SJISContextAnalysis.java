// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober.contextanalysis;

public class SJISContextAnalysis extends JapaneseContextAnalysis
{
    public static final int HIRAGANA_HIGHBYTE = 130;
    public static final int HIRAGANA_LOWBYTE_BEGIN = 159;
    public static final int HIRAGANA_LOWBYTE_END = 241;
    public static final int HIGHBYTE_BEGIN_1 = 129;
    public static final int HIGHBYTE_END_1 = 159;
    public static final int HIGHBYTE_BEGIN_2 = 224;
    public static final int HIGHBYTE_END_2 = 239;
    
    @Override
    protected void getOrder(final Order order, final byte[] buf, final int offset) {
        order.order = -1;
        order.charLength = 1;
        final int highbyte = buf[offset] & 0xFF;
        if ((highbyte >= 129 && highbyte <= 159) || (highbyte >= 224 && highbyte <= 239)) {
            order.charLength = 2;
        }
        if (highbyte == 130) {
            final int lowbyte = buf[offset + 1] & 0xFF;
            if (lowbyte >= 159 && lowbyte <= 241) {
                order.order = lowbyte - 159;
            }
        }
    }
    
    @Override
    protected int getOrder(final byte[] buf, final int offset) {
        final int highbyte = buf[offset] & 0xFF;
        if (highbyte == 130) {
            final int lowbyte = buf[offset + 1] & 0xFF;
            if (lowbyte >= 159 && lowbyte <= 241) {
                return lowbyte - 159;
            }
        }
        return -1;
    }
}
