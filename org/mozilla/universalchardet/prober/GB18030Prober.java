// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.statemachine.GB18030SMModel;
import java.util.Arrays;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.statemachine.SMModel;
import org.mozilla.universalchardet.prober.distributionanalysis.GB2312DistributionAnalysis;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;

public class GB18030Prober extends CharsetProber
{
    private CodingStateMachine codingSM;
    private ProbingState state;
    private GB2312DistributionAnalysis distributionAnalyzer;
    private byte[] lastChar;
    private static final SMModel smModel;
    
    public GB18030Prober() {
        this.codingSM = new CodingStateMachine(GB18030Prober.smModel);
        this.distributionAnalyzer = new GB2312DistributionAnalysis();
        this.lastChar = new byte[2];
        this.reset();
    }
    
    @Override
    public String getCharSetName() {
        return Constants.CHARSET_GB18030;
    }
    
    @Override
    public float getConfidence() {
        final float distribCf = this.distributionAnalyzer.getConfidence();
        return distribCf;
    }
    
    @Override
    public ProbingState getState() {
        return this.state;
    }
    
    @Override
    public ProbingState handleData(final byte[] buf, final int offset, final int length) {
        final int maxPos = offset + length;
        for (int i = offset; i < maxPos; ++i) {
            final int codingState = this.codingSM.nextState(buf[i]);
            if (codingState == 1) {
                this.state = ProbingState.NOT_ME;
                break;
            }
            if (codingState == 2) {
                this.state = ProbingState.FOUND_IT;
                break;
            }
            if (codingState == 0) {
                final int charLen = this.codingSM.getCurrentCharLen();
                if (i == offset) {
                    this.lastChar[1] = buf[offset];
                    this.distributionAnalyzer.handleOneChar(this.lastChar, 0, charLen);
                }
                else {
                    this.distributionAnalyzer.handleOneChar(buf, i - 1, charLen);
                }
            }
        }
        this.lastChar[0] = buf[maxPos - 1];
        if (this.state == ProbingState.DETECTING && this.distributionAnalyzer.gotEnoughData() && this.getConfidence() > 0.95f) {
            this.state = ProbingState.FOUND_IT;
        }
        return this.state;
    }
    
    @Override
    public void reset() {
        this.codingSM.reset();
        this.state = ProbingState.DETECTING;
        this.distributionAnalyzer.reset();
        Arrays.fill(this.lastChar, (byte)0);
    }
    
    @Override
    public void setOption() {
    }
    
    static {
        smModel = new GB18030SMModel();
    }
}
