// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober.distributionanalysis;

public abstract class CharDistributionAnalysis
{
    public static final float SURE_NO = 0.01f;
    public static final float SURE_YES = 0.99f;
    public static final int ENOUGH_DATA_THRESHOLD = 1024;
    public static final int MINIMUM_DATA_THRESHOLD = 4;
    private int freqChars;
    private int totalChars;
    protected int[] charToFreqOrder;
    protected float typicalDistributionRatio;
    protected boolean done;
    
    public CharDistributionAnalysis() {
        this.reset();
    }
    
    public void handleData(final byte[] buf, final int offset, final int length) {
    }
    
    public void handleOneChar(final byte[] buf, final int offset, final int charLength) {
        int order = -1;
        if (charLength == 2) {
            order = this.getOrder(buf, offset);
        }
        if (order >= 0) {
            ++this.totalChars;
            if (order < this.charToFreqOrder.length && 512 > this.charToFreqOrder[order]) {
                ++this.freqChars;
            }
        }
    }
    
    public float getConfidence() {
        if (this.totalChars <= 0 || this.freqChars <= 4) {
            return 0.01f;
        }
        if (this.totalChars != this.freqChars) {
            final float r = this.freqChars / (this.totalChars - this.freqChars) * this.typicalDistributionRatio;
            if (r < 0.99f) {
                return r;
            }
        }
        return 0.99f;
    }
    
    public void reset() {
        this.done = false;
        this.totalChars = 0;
        this.freqChars = 0;
    }
    
    public void setOption() {
    }
    
    public boolean gotEnoughData() {
        return this.totalChars > 1024;
    }
    
    protected abstract int getOrder(final byte[] p0, final int p1);
}
