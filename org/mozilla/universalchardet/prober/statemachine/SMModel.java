// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet.prober.statemachine;

public abstract class SMModel
{
    public static final int START = 0;
    public static final int ERROR = 1;
    public static final int ITSME = 2;
    protected PkgInt classTable;
    protected int classFactor;
    protected PkgInt stateTable;
    protected int[] charLenTable;
    protected String name;
    
    public SMModel(final PkgInt classTable, final int classFactor, final PkgInt stateTable, final int[] charLenTable, final String name) {
        this.classTable = classTable;
        this.classFactor = classFactor;
        this.stateTable = stateTable;
        this.charLenTable = charLenTable;
        this.name = name;
    }
    
    public int getClass(final byte b) {
        final int c = b & 0xFF;
        return this.classTable.unpack(c);
    }
    
    public int getNextState(final int cls, final int currentState) {
        return this.stateTable.unpack(currentState * this.classFactor + cls);
    }
    
    public int getCharLen(final int cls) {
        return this.charLenTable[cls];
    }
    
    public String getName() {
        return this.name;
    }
}
