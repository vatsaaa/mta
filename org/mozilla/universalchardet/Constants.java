// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet;

public final class Constants
{
    public static final String CHARSET_ISO_2022_JP;
    public static final String CHARSET_ISO_2022_CN;
    public static final String CHARSET_ISO_2022_KR;
    public static final String CHARSET_ISO_8859_5;
    public static final String CHARSET_ISO_8859_7;
    public static final String CHARSET_ISO_8859_8;
    public static final String CHARSET_BIG5;
    public static final String CHARSET_GB18030;
    public static final String CHARSET_EUC_JP;
    public static final String CHARSET_EUC_KR;
    public static final String CHARSET_EUC_TW;
    public static final String CHARSET_SHIFT_JIS;
    public static final String CHARSET_IBM855;
    public static final String CHARSET_IBM866;
    public static final String CHARSET_KOI8_R;
    public static final String CHARSET_MACCYRILLIC;
    public static final String CHARSET_WINDOWS_1251;
    public static final String CHARSET_WINDOWS_1252;
    public static final String CHARSET_WINDOWS_1253;
    public static final String CHARSET_WINDOWS_1255;
    public static final String CHARSET_UTF_8;
    public static final String CHARSET_UTF_16BE;
    public static final String CHARSET_UTF_16LE;
    public static final String CHARSET_UTF_32BE;
    public static final String CHARSET_UTF_32LE;
    public static final String CHARSET_HZ_GB_2312;
    public static final String CHARSET_X_ISO_10646_UCS_4_3412;
    public static final String CHARSET_X_ISO_10646_UCS_4_2143;
    
    static {
        CHARSET_ISO_2022_JP = "ISO-2022-JP".intern();
        CHARSET_ISO_2022_CN = "ISO-2022-CN".intern();
        CHARSET_ISO_2022_KR = "ISO-2022-KR".intern();
        CHARSET_ISO_8859_5 = "ISO-8859-5".intern();
        CHARSET_ISO_8859_7 = "ISO-8859-7".intern();
        CHARSET_ISO_8859_8 = "ISO-8859-8".intern();
        CHARSET_BIG5 = "BIG5".intern();
        CHARSET_GB18030 = "GB18030".intern();
        CHARSET_EUC_JP = "EUC-JP".intern();
        CHARSET_EUC_KR = "EUC-KR".intern();
        CHARSET_EUC_TW = "EUC-TW".intern();
        CHARSET_SHIFT_JIS = "SHIFT_JIS".intern();
        CHARSET_IBM855 = "IBM855".intern();
        CHARSET_IBM866 = "IBM866".intern();
        CHARSET_KOI8_R = "KOI8-R".intern();
        CHARSET_MACCYRILLIC = "MACCYRILLIC".intern();
        CHARSET_WINDOWS_1251 = "WINDOWS-1251".intern();
        CHARSET_WINDOWS_1252 = "WINDOWS-1252".intern();
        CHARSET_WINDOWS_1253 = "WINDOWS-1253".intern();
        CHARSET_WINDOWS_1255 = "WINDOWS-1255".intern();
        CHARSET_UTF_8 = "UTF-8".intern();
        CHARSET_UTF_16BE = "UTF-16BE".intern();
        CHARSET_UTF_16LE = "UTF-16LE".intern();
        CHARSET_UTF_32BE = "UTF-32BE".intern();
        CHARSET_UTF_32LE = "UTF-32LE".intern();
        CHARSET_HZ_GB_2312 = "HZ-GB-2312".intern();
        CHARSET_X_ISO_10646_UCS_4_3412 = "X-ISO-10646-UCS-4-3412".intern();
        CHARSET_X_ISO_10646_UCS_4_2143 = "X-ISO-10646-UCS-4-2143".intern();
    }
}
