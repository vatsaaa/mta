// 
// Decompiled by Procyon v0.5.36
// 

package org.mozilla.universalchardet;

import java.io.FileInputStream;
import org.mozilla.universalchardet.prober.EscCharsetProber;
import org.mozilla.universalchardet.prober.Latin1Prober;
import org.mozilla.universalchardet.prober.SBCSGroupProber;
import org.mozilla.universalchardet.prober.MBCSGroupProber;
import org.mozilla.universalchardet.prober.CharsetProber;

public class UniversalDetector
{
    public static final float SHORTCUT_THRESHOLD = 0.95f;
    public static final float MINIMUM_THRESHOLD = 0.2f;
    private InputState inputState;
    private boolean done;
    private boolean start;
    private boolean gotData;
    private byte lastChar;
    private String detectedCharset;
    private CharsetProber[] probers;
    private CharsetProber escCharsetProber;
    private CharsetListener listener;
    
    public UniversalDetector(final CharsetListener listener) {
        this.listener = listener;
        this.escCharsetProber = null;
        this.probers = new CharsetProber[3];
        for (int i = 0; i < this.probers.length; ++i) {
            this.probers[i] = null;
        }
        this.reset();
    }
    
    public boolean isDone() {
        return this.done;
    }
    
    public String getDetectedCharset() {
        return this.detectedCharset;
    }
    
    public void setListener(final CharsetListener listener) {
        this.listener = listener;
    }
    
    public CharsetListener getListener() {
        return this.listener;
    }
    
    public void handleData(final byte[] buf, final int offset, final int length) {
        if (this.done) {
            return;
        }
        if (length > 0) {
            this.gotData = true;
        }
        if (this.start) {
            this.start = false;
            if (length > 3) {
                final int b1 = buf[offset] & 0xFF;
                final int b2 = buf[offset + 1] & 0xFF;
                final int b3 = buf[offset + 2] & 0xFF;
                final int b4 = buf[offset + 3] & 0xFF;
                switch (b1) {
                    case 239: {
                        if (b2 == 187 && b3 == 191) {
                            this.detectedCharset = Constants.CHARSET_UTF_8;
                            break;
                        }
                        break;
                    }
                    case 254: {
                        if (b2 == 255 && b3 == 0 && b4 == 0) {
                            this.detectedCharset = Constants.CHARSET_X_ISO_10646_UCS_4_3412;
                            break;
                        }
                        if (b2 == 255) {
                            this.detectedCharset = Constants.CHARSET_UTF_16BE;
                            break;
                        }
                        break;
                    }
                    case 0: {
                        if (b2 == 0 && b3 == 254 && b4 == 255) {
                            this.detectedCharset = Constants.CHARSET_UTF_32BE;
                            break;
                        }
                        if (b2 == 0 && b3 == 255 && b4 == 254) {
                            this.detectedCharset = Constants.CHARSET_X_ISO_10646_UCS_4_2143;
                            break;
                        }
                        break;
                    }
                    case 255: {
                        if (b2 == 254 && b3 == 0 && b4 == 0) {
                            this.detectedCharset = Constants.CHARSET_UTF_32LE;
                            break;
                        }
                        if (b2 == 254) {
                            this.detectedCharset = Constants.CHARSET_UTF_16LE;
                            break;
                        }
                        break;
                    }
                }
                if (this.detectedCharset != null) {
                    this.done = true;
                    return;
                }
            }
        }
        for (int maxPos = offset + length, i = offset; i < maxPos; ++i) {
            final int c = buf[i] & 0xFF;
            if ((c & 0x80) != 0x0 && c != 160) {
                if (this.inputState != InputState.HIGHBYTE) {
                    this.inputState = InputState.HIGHBYTE;
                    if (this.escCharsetProber != null) {
                        this.escCharsetProber = null;
                    }
                    if (this.probers[0] == null) {
                        this.probers[0] = new MBCSGroupProber();
                    }
                    if (this.probers[1] == null) {
                        this.probers[1] = new SBCSGroupProber();
                    }
                    if (this.probers[2] == null) {
                        this.probers[2] = new Latin1Prober();
                    }
                }
            }
            else {
                if (this.inputState == InputState.PURE_ASCII && (c == 27 || (c == 123 && this.lastChar == 126))) {
                    this.inputState = InputState.ESC_ASCII;
                }
                this.lastChar = buf[i];
            }
        }
        if (this.inputState == InputState.ESC_ASCII) {
            if (this.escCharsetProber == null) {
                this.escCharsetProber = new EscCharsetProber();
            }
            final CharsetProber.ProbingState st = this.escCharsetProber.handleData(buf, offset, length);
            if (st == CharsetProber.ProbingState.FOUND_IT) {
                this.done = true;
                this.detectedCharset = this.escCharsetProber.getCharSetName();
            }
        }
        else if (this.inputState == InputState.HIGHBYTE) {
            for (int j = 0; j < this.probers.length; ++j) {
                final CharsetProber.ProbingState st = this.probers[j].handleData(buf, offset, length);
                if (st == CharsetProber.ProbingState.FOUND_IT) {
                    this.done = true;
                    this.detectedCharset = this.probers[j].getCharSetName();
                    return;
                }
            }
        }
    }
    
    public void dataEnd() {
        if (!this.gotData) {
            return;
        }
        if (this.detectedCharset != null) {
            this.done = true;
            if (this.listener != null) {
                this.listener.report(this.detectedCharset);
            }
            return;
        }
        if (this.inputState == InputState.HIGHBYTE) {
            float maxProberConfidence = 0.0f;
            int maxProber = 0;
            for (int i = 0; i < this.probers.length; ++i) {
                final float proberConfidence = this.probers[i].getConfidence();
                if (proberConfidence > maxProberConfidence) {
                    maxProberConfidence = proberConfidence;
                    maxProber = i;
                }
            }
            if (maxProberConfidence > 0.2f) {
                this.detectedCharset = this.probers[maxProber].getCharSetName();
                if (this.listener != null) {
                    this.listener.report(this.detectedCharset);
                }
            }
        }
        else if (this.inputState == InputState.ESC_ASCII) {}
    }
    
    public void reset() {
        this.done = false;
        this.start = true;
        this.detectedCharset = null;
        this.gotData = false;
        this.inputState = InputState.PURE_ASCII;
        this.lastChar = 0;
        if (this.escCharsetProber != null) {
            this.escCharsetProber.reset();
        }
        for (int i = 0; i < this.probers.length; ++i) {
            if (this.probers[i] != null) {
                this.probers[i].reset();
            }
        }
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("USAGE: java UniversalDetector filename");
            return;
        }
        final UniversalDetector detector = new UniversalDetector(new CharsetListener() {
            public void report(final String name) {
                System.out.println("charset = " + name);
            }
        });
        final byte[] buf = new byte[4096];
        final FileInputStream fis = new FileInputStream(args[0]);
        int nread;
        while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
            detector.handleData(buf, 0, nread);
        }
        detector.dataEnd();
    }
    
    public enum InputState
    {
        PURE_ASCII, 
        ESC_ASCII, 
        HIGHBYTE;
    }
}
