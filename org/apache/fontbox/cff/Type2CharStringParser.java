// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Type2CharStringParser
{
    private DataInput input;
    private int hstemCount;
    private int vstemCount;
    private List<Object> sequence;
    
    public Type2CharStringParser() {
        this.input = null;
        this.hstemCount = 0;
        this.vstemCount = 0;
        this.sequence = null;
    }
    
    public List<Object> parse(final byte[] bytes) throws IOException {
        this.input = new DataInput(bytes);
        this.hstemCount = 0;
        this.vstemCount = 0;
        this.sequence = new ArrayList<Object>();
        while (this.input.hasRemaining()) {
            final int b0 = this.input.readUnsignedByte();
            if (b0 >= 0 && b0 <= 27) {
                this.sequence.add(this.readCommand(b0));
            }
            else if (b0 == 28) {
                this.sequence.add(this.readNumber(b0));
            }
            else if (b0 >= 29 && b0 <= 31) {
                this.sequence.add(this.readCommand(b0));
            }
            else {
                if (b0 < 32 || b0 > 255) {
                    throw new IllegalArgumentException();
                }
                this.sequence.add(this.readNumber(b0));
            }
        }
        return this.sequence;
    }
    
    private CharStringCommand readCommand(final int b0) throws IOException {
        if (b0 == 1 || b0 == 18) {
            this.hstemCount += this.peekNumbers().size() / 2;
        }
        else if (b0 == 3 || b0 == 19 || b0 == 20 || b0 == 23) {
            this.vstemCount += this.peekNumbers().size() / 2;
        }
        if (b0 == 12) {
            final int b = this.input.readUnsignedByte();
            return new CharStringCommand(b0, b);
        }
        if (b0 == 19 || b0 == 20) {
            final int[] value = new int[1 + this.getMaskLength()];
            value[0] = b0;
            for (int i = 1; i < value.length; ++i) {
                value[i] = this.input.readUnsignedByte();
            }
            return new CharStringCommand(value);
        }
        return new CharStringCommand(b0);
    }
    
    private Integer readNumber(final int b0) throws IOException {
        if (b0 == 28) {
            final int b = this.input.readUnsignedByte();
            final int b2 = this.input.readUnsignedByte();
            return (int)(short)(b << 8 | b2);
        }
        if (b0 >= 32 && b0 <= 246) {
            return b0 - 139;
        }
        if (b0 >= 247 && b0 <= 250) {
            final int b = this.input.readUnsignedByte();
            return (b0 - 247) * 256 + b + 108;
        }
        if (b0 >= 251 && b0 <= 254) {
            final int b = this.input.readUnsignedByte();
            return -(b0 - 251) * 256 - b - 108;
        }
        if (b0 == 255) {
            final int b = this.input.readUnsignedByte();
            final int b2 = this.input.readUnsignedByte();
            final int b3 = this.input.readUnsignedByte();
            final int b4 = this.input.readUnsignedByte();
            return (int)(short)(b << 8 | b2);
        }
        throw new IllegalArgumentException();
    }
    
    private int getMaskLength() {
        int length = 1;
        int hintCount = this.hstemCount + this.vstemCount;
        while (true) {
            hintCount -= 8;
            if (hintCount <= 0) {
                break;
            }
            ++length;
        }
        return length;
    }
    
    private List<Number> peekNumbers() {
        final List<Number> numbers = new ArrayList<Number>();
        for (int i = this.sequence.size() - 1; i > -1; --i) {
            final Object object = this.sequence.get(i);
            if (!(object instanceof Number)) {
                return numbers;
            }
            final Number number = (Number)object;
            numbers.add(0, number);
        }
        return numbers;
    }
}
