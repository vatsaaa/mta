// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Iterator;
import java.util.Collection;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.io.IOException;

public class Type1FontFormatter
{
    private Type1FontFormatter() {
    }
    
    public static byte[] format(final CFFFont font) throws IOException {
        final DataOutput output = new DataOutput();
        printFont(font, output);
        return output.getBytes();
    }
    
    private static void printFont(final CFFFont font, final DataOutput output) throws IOException {
        output.println("%!FontType1-1.0 " + font.getName() + " " + font.getProperty("version"));
        printFontDictionary(font, output);
        for (int i = 0; i < 8; ++i) {
            final StringBuilder sb = new StringBuilder();
            for (int j = 0; j < 64; ++j) {
                sb.append("0");
            }
            output.println(sb.toString());
        }
        output.println("cleartomark");
    }
    
    private static void printFontDictionary(final CFFFont font, final DataOutput output) throws IOException {
        output.println("10 dict begin");
        output.println("/FontInfo 10 dict dup begin");
        output.println("/version (" + font.getProperty("version") + ") readonly def");
        output.println("/Notice (" + font.getProperty("Notice") + ") readonly def");
        output.println("/FullName (" + font.getProperty("FullName") + ") readonly def");
        output.println("/FamilyName (" + font.getProperty("FamilyName") + ") readonly def");
        output.println("/Weight (" + font.getProperty("Weight") + ") readonly def");
        output.println("/ItalicAngle " + font.getProperty("ItalicAngle") + " def");
        output.println("/isFixedPitch " + font.getProperty("isFixedPitch") + " def");
        output.println("/UnderlinePosition " + font.getProperty("UnderlinePosition") + " def");
        output.println("/UnderlineThickness " + font.getProperty("UnderlineThickness") + " def");
        output.println("end readonly def");
        output.println("/FontName /" + font.getName() + " def");
        output.println("/PaintType " + font.getProperty("PaintType") + " def");
        output.println("/FontType 1 def");
        final NumberFormat matrixFormat = new DecimalFormat("0.########", new DecimalFormatSymbols(Locale.US));
        output.println("/FontMatrix " + formatArray(font.getProperty("FontMatrix"), matrixFormat, false) + " readonly def");
        output.println("/FontBBox " + formatArray(font.getProperty("FontBBox"), false) + " readonly def");
        output.println("/StrokeWidth " + font.getProperty("StrokeWidth") + " def");
        final Collection<CFFFont.Mapping> mappings = font.getMappings();
        output.println("/Encoding 256 array");
        output.println("0 1 255 {1 index exch /.notdef put} for");
        for (final CFFFont.Mapping mapping : mappings) {
            output.println("dup " + mapping.getCode() + " /" + mapping.getName() + " put");
        }
        output.println("readonly def");
        output.println("currentdict end");
        final DataOutput eexecOutput = new DataOutput();
        printEexecFontDictionary(font, eexecOutput);
        output.println("currentfile eexec");
        final byte[] eexecBytes = Type1FontUtil.eexecEncrypt(eexecOutput.getBytes());
        final String hexString = Type1FontUtil.hexEncode(eexecBytes);
        String hexLine;
        for (int i = 0; i < hexString.length(); i += hexLine.length()) {
            hexLine = hexString.substring(i, Math.min(i + 72, hexString.length()));
            output.println(hexLine);
        }
    }
    
    private static void printEexecFontDictionary(final CFFFont font, final DataOutput output) throws IOException {
        output.println("dup /Private 15 dict dup begin");
        output.println("/RD {string currentfile exch readstring pop} executeonly def");
        output.println("/ND {noaccess def} executeonly def");
        output.println("/NP {noaccess put} executeonly def");
        output.println("/BlueValues " + formatArray(font.getProperty("BlueValues"), true) + " ND");
        output.println("/OtherBlues " + formatArray(font.getProperty("OtherBlues"), true) + " ND");
        output.println("/BlueScale " + font.getProperty("BlueScale") + " def");
        output.println("/BlueShift " + font.getProperty("BlueShift") + " def");
        output.println("/BlueFuzz " + font.getProperty("BlueFuzz") + " def");
        output.println("/StdHW " + formatArray(font.getProperty("StdHW"), true) + " ND");
        output.println("/StdVW " + formatArray(font.getProperty("StdVW"), true) + " ND");
        output.println("/ForceBold " + font.getProperty("ForceBold") + " def");
        output.println("/MinFeature {16 16} def");
        output.println("/password 5839 def");
        final Collection<CFFFont.Mapping> mappings = font.getMappings();
        output.println("2 index /CharStrings " + mappings.size() + " dict dup begin");
        final Type1CharStringFormatter formatter = new Type1CharStringFormatter();
        for (final CFFFont.Mapping mapping : mappings) {
            final byte[] type1Bytes = formatter.format(mapping.toType1Sequence());
            final byte[] charstringBytes = Type1FontUtil.charstringEncrypt(type1Bytes, 4);
            output.print("/" + mapping.getName() + " " + charstringBytes.length + " RD ");
            output.write(charstringBytes);
            output.print(" ND");
            output.println();
        }
        output.println("end");
        output.println("end");
        output.println("readonly put");
        output.println("noaccess put");
        output.println("dup /FontName get exch definefont pop");
        output.println("mark currentfile closefile");
    }
    
    private static String formatArray(final Object object, final boolean executable) {
        return formatArray(object, null, executable);
    }
    
    private static String formatArray(final Object object, final NumberFormat format, final boolean executable) {
        final StringBuffer sb = new StringBuffer();
        sb.append(executable ? "{" : "[");
        if (object instanceof Collection) {
            String sep = "";
            final Collection<?> elements = (Collection<?>)object;
            for (final Object element : elements) {
                sb.append(sep).append(formatElement(element, format));
                sep = " ";
            }
        }
        else if (object instanceof Number) {
            sb.append(formatElement(object, format));
        }
        sb.append(executable ? "}" : "]");
        return sb.toString();
    }
    
    private static String formatElement(final Object object, final NumberFormat format) {
        if (format != null) {
            if (object instanceof Double || object instanceof Float) {
                final Number number = (Number)object;
                return format.format(number.doubleValue());
            }
            if (object instanceof Long || object instanceof Integer) {
                final Number number = (Number)object;
                return format.format(number.longValue());
            }
        }
        return String.valueOf(object);
    }
}
