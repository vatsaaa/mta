// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.List;
import java.awt.geom.Point2D;
import java.awt.geom.GeneralPath;

public class CharStringRenderer extends CharStringHandler
{
    private boolean isCharstringType1;
    private boolean isFirstCommand;
    private GeneralPath path;
    private Point2D sidebearingPoint;
    private Point2D referencePoint;
    private int width;
    
    public CharStringRenderer() {
        this.isCharstringType1 = true;
        this.isFirstCommand = true;
        this.path = null;
        this.sidebearingPoint = null;
        this.referencePoint = null;
        this.width = 0;
        this.isCharstringType1 = true;
    }
    
    public CharStringRenderer(final boolean isType1) {
        this.isCharstringType1 = true;
        this.isFirstCommand = true;
        this.path = null;
        this.sidebearingPoint = null;
        this.referencePoint = null;
        this.width = 0;
        this.isCharstringType1 = isType1;
    }
    
    public GeneralPath render(final List<Object> sequence) throws IOException {
        this.path = new GeneralPath();
        this.sidebearingPoint = new Point2D.Float(0.0f, 0.0f);
        this.referencePoint = null;
        this.setWidth(0);
        this.handleSequence(sequence);
        return this.path;
    }
    
    @Override
    public List<Integer> handleCommand(final List<Integer> numbers, final CharStringCommand command) {
        if (this.isCharstringType1) {
            this.handleCommandType1(numbers, command);
        }
        else {
            this.handleCommandType2(numbers, command);
        }
        return null;
    }
    
    private void handleCommandType2(final List<Integer> numbers, final CharStringCommand command) {
        final String name = CharStringCommand.TYPE2_VOCABULARY.get(command.getKey());
        if ("vmoveto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 2) {
                this.setWidth(numbers.get(0));
                this.rmoveTo(0, numbers.get(1));
            }
            else {
                this.rmoveTo(0, numbers.get(0));
            }
        }
        else if ("rlineto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 3) {
                this.setWidth(numbers.get(0));
                this.rlineTo(numbers.get(1), numbers.get(2));
            }
            else {
                this.rlineTo(numbers.get(0), numbers.get(1));
            }
        }
        else if ("hlineto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 2) {
                this.setWidth(numbers.get(0));
                this.rlineTo(numbers.get(1), 0);
            }
            else {
                this.rlineTo(numbers.get(0), 0);
            }
        }
        else if ("vlineto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 2) {
                this.setWidth(numbers.get(0));
                this.rlineTo(0, numbers.get(1));
            }
            else {
                this.rlineTo(0, numbers.get(0));
            }
        }
        else if ("rrcurveto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 7) {
                this.setWidth(numbers.get(0));
                this.rrcurveTo(numbers.get(1), numbers.get(2), numbers.get(3), numbers.get(4), numbers.get(5), numbers.get(6));
            }
            else {
                this.rrcurveTo(numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3), numbers.get(4), numbers.get(5));
            }
        }
        else if ("closepath".equals(name)) {
            this.closePath();
        }
        else if ("rmoveto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 3) {
                this.setWidth(numbers.get(0));
                this.rmoveTo(numbers.get(1), numbers.get(2));
            }
            else {
                this.rmoveTo(numbers.get(0), numbers.get(1));
            }
        }
        else if ("hmoveto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 2) {
                this.setWidth(numbers.get(0));
                this.rmoveTo(numbers.get(1), 0);
            }
            else {
                this.rmoveTo(numbers.get(0), 0);
            }
        }
        else if ("vhcurveto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 5) {
                this.setWidth(numbers.get(0));
                this.rrcurveTo(0, numbers.get(1), numbers.get(2), numbers.get(3), numbers.get(4), 0);
            }
            else {
                this.rrcurveTo(0, numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3), 0);
            }
        }
        else if ("hvcurveto".equals(name)) {
            if (this.isFirstCommand && numbers.size() == 5) {
                this.setWidth(numbers.get(0));
                this.rrcurveTo(numbers.get(1), 0, numbers.get(2), numbers.get(3), 0, numbers.get(4));
            }
            else {
                this.rrcurveTo(numbers.get(0), 0, numbers.get(1), numbers.get(2), 0, numbers.get(3));
            }
        }
        else if ("hstem".equals(name)) {
            if (numbers.size() % 2 == 1) {
                this.setWidth(numbers.get(0));
            }
        }
        else if ("vstem".equals(name)) {
            if (numbers.size() % 2 == 1) {
                this.setWidth(numbers.get(0));
            }
        }
        else if ("hstemhm".equals(name)) {
            if (numbers.size() % 2 == 1) {
                this.setWidth(numbers.get(0));
            }
        }
        else if ("hstemhm".equals(name)) {
            if (numbers.size() % 2 == 1) {
                this.setWidth(numbers.get(0));
            }
        }
        else if ("cntrmask".equals(name)) {
            if (numbers.size() == 1) {
                this.setWidth(numbers.get(0));
            }
        }
        else if ("hintmask".equals(name)) {
            if (numbers.size() == 1) {
                this.setWidth(numbers.get(0));
            }
        }
        else if ("endchar".equals(name) && numbers.size() == 1) {
            this.setWidth(numbers.get(0));
        }
        if (this.isFirstCommand) {
            this.isFirstCommand = false;
        }
    }
    
    private void handleCommandType1(final List<Integer> numbers, final CharStringCommand command) {
        final String name = CharStringCommand.TYPE1_VOCABULARY.get(command.getKey());
        if ("vmoveto".equals(name)) {
            this.rmoveTo(0, numbers.get(0));
        }
        else if ("rlineto".equals(name)) {
            this.rlineTo(numbers.get(0), numbers.get(1));
        }
        else if ("hlineto".equals(name)) {
            this.rlineTo(numbers.get(0), 0);
        }
        else if ("vlineto".equals(name)) {
            this.rlineTo(0, numbers.get(0));
        }
        else if ("rrcurveto".equals(name)) {
            this.rrcurveTo(numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3), numbers.get(4), numbers.get(5));
        }
        else if ("closepath".equals(name)) {
            this.closePath();
        }
        else if ("sbw".equals(name)) {
            this.pointSb(numbers.get(0), numbers.get(1));
            this.setWidth(numbers.get(2));
        }
        else if ("hsbw".equals(name)) {
            this.pointSb(numbers.get(0), 0);
            this.setWidth(numbers.get(1));
        }
        else if ("rmoveto".equals(name)) {
            this.rmoveTo(numbers.get(0), numbers.get(1));
        }
        else if ("hmoveto".equals(name)) {
            this.rmoveTo(numbers.get(0), 0);
        }
        else if ("vhcurveto".equals(name)) {
            this.rrcurveTo(0, numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3), 0);
        }
        else if ("hvcurveto".equals(name)) {
            this.rrcurveTo(numbers.get(0), 0, numbers.get(1), numbers.get(2), 0, numbers.get(3));
        }
    }
    
    private void rmoveTo(final Number dx, final Number dy) {
        Point2D point = this.referencePoint;
        if (point == null) {
            point = this.sidebearingPoint;
        }
        this.referencePoint = null;
        this.path.moveTo((float)(point.getX() + dx.doubleValue()), (float)(point.getY() + dy.doubleValue()));
    }
    
    private void rlineTo(final Number dx, final Number dy) {
        final Point2D point = this.path.getCurrentPoint();
        this.path.lineTo((float)(point.getX() + dx.doubleValue()), (float)(point.getY() + dy.doubleValue()));
    }
    
    private void rrcurveTo(final Number dx1, final Number dy1, final Number dx2, final Number dy2, final Number dx3, final Number dy3) {
        final Point2D point = this.path.getCurrentPoint();
        final float x1 = (float)point.getX() + dx1.floatValue();
        final float y1 = (float)point.getY() + dy1.floatValue();
        final float x2 = x1 + dx2.floatValue();
        final float y2 = y1 + dy2.floatValue();
        final float x3 = x2 + dx3.floatValue();
        final float y3 = y2 + dy3.floatValue();
        this.path.curveTo(x1, y1, x2, y2, x3, y3);
    }
    
    private void closePath() {
        this.referencePoint = this.path.getCurrentPoint();
        this.path.closePath();
    }
    
    private void pointSb(final Number x, final Number y) {
        this.sidebearingPoint = new Point2D.Float(x.floatValue(), y.floatValue());
    }
    
    public Rectangle2D getBounds() {
        return this.path.getBounds2D();
    }
    
    public int getWidth() {
        return this.width;
    }
    
    private void setWidth(final int width) {
        this.width = width;
    }
}
