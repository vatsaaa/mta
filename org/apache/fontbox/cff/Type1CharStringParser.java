// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Type1CharStringParser
{
    private DataInput input;
    private List<Object> sequence;
    
    public Type1CharStringParser() {
        this.input = null;
        this.sequence = null;
    }
    
    public List<Object> parse(final byte[] bytes) throws IOException {
        this.input = new DataInput(bytes);
        this.sequence = new ArrayList<Object>();
        while (this.input.hasRemaining()) {
            final int b0 = this.input.readUnsignedByte();
            if (b0 >= 0 && b0 <= 31) {
                this.sequence.add(this.readCommand(b0));
            }
            else {
                if (b0 < 32 || b0 > 255) {
                    throw new IllegalArgumentException();
                }
                this.sequence.add(this.readNumber(b0));
            }
        }
        return this.sequence;
    }
    
    private CharStringCommand readCommand(final int b0) throws IOException {
        if (b0 == 12) {
            final int b = this.input.readUnsignedByte();
            return new CharStringCommand(b0, b);
        }
        return new CharStringCommand(b0);
    }
    
    private Integer readNumber(final int b0) throws IOException {
        if (b0 >= 32 && b0 <= 246) {
            return b0 - 139;
        }
        if (b0 >= 247 && b0 <= 250) {
            final int b = this.input.readUnsignedByte();
            return (b0 - 247) * 256 + b + 108;
        }
        if (b0 >= 251 && b0 <= 254) {
            final int b = this.input.readUnsignedByte();
            return -(b0 - 251) * 256 - b - 108;
        }
        if (b0 == 255) {
            final int b = this.input.readUnsignedByte();
            final int b2 = this.input.readUnsignedByte();
            final int b3 = this.input.readUnsignedByte();
            final int b4 = this.input.readUnsignedByte();
            return b << 24 | b2 << 16 | b3 << 8 | b4;
        }
        throw new IllegalArgumentException();
    }
}
