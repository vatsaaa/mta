// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.io.EOFException;
import java.io.IOException;

public class DataInput
{
    private byte[] inputBuffer;
    private int bufferPosition;
    
    public DataInput(final byte[] buffer) {
        this.inputBuffer = null;
        this.bufferPosition = 0;
        this.inputBuffer = buffer;
    }
    
    public boolean hasRemaining() {
        return this.bufferPosition < this.inputBuffer.length;
    }
    
    public int getPosition() {
        return this.bufferPosition;
    }
    
    public void setPosition(final int position) {
        this.bufferPosition = position;
    }
    
    public String getString() throws IOException {
        return new String(this.inputBuffer, "ISO-8859-1");
    }
    
    public byte readByte() throws IOException {
        return (byte)this.readUnsignedByte();
    }
    
    public int readUnsignedByte() throws IOException {
        final int b = this.read();
        if (b < 0) {
            throw new EOFException();
        }
        return b;
    }
    
    public short readShort() throws IOException {
        return (short)this.readUnsignedShort();
    }
    
    public int readUnsignedShort() throws IOException {
        final int b1 = this.read();
        final int b2 = this.read();
        if ((b1 | b2) < 0) {
            throw new EOFException();
        }
        return b1 << 8 | b2;
    }
    
    public int readInt() throws IOException {
        final int b1 = this.read();
        final int b2 = this.read();
        final int b3 = this.read();
        final int b4 = this.read();
        if ((b1 | b2 | b3 | b4) < 0) {
            throw new EOFException();
        }
        return b1 << 24 | b2 << 16 | b3 << 8 | b4;
    }
    
    public byte[] readBytes(final int length) throws IOException {
        final byte[] bytes = new byte[length];
        for (int i = 0; i < length; ++i) {
            bytes[i] = this.readByte();
        }
        return bytes;
    }
    
    private int read() {
        try {
            final int value = this.inputBuffer[this.bufferPosition] & 0xFF;
            ++this.bufferPosition;
            return value;
        }
        catch (RuntimeException re) {
            return -1;
        }
    }
}
