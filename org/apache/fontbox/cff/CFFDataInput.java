// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.io.IOException;

public class CFFDataInput extends DataInput
{
    public CFFDataInput(final byte[] buffer) {
        super(buffer);
    }
    
    public int readCard8() throws IOException {
        return this.readUnsignedByte();
    }
    
    public int readCard16() throws IOException {
        return this.readUnsignedShort();
    }
    
    public int readOffset(final int offSize) throws IOException {
        int value = 0;
        for (int i = 0; i < offSize; ++i) {
            value = (value << 8 | this.readUnsignedByte());
        }
        return value;
    }
    
    public int readOffSize() throws IOException {
        return this.readUnsignedByte();
    }
    
    public int readSID() throws IOException {
        return this.readUnsignedShort();
    }
}
