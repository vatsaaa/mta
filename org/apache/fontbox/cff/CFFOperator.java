// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class CFFOperator
{
    private Key operatorKey;
    private String operatorName;
    private static Map<Key, CFFOperator> keyMap;
    private static Map<String, CFFOperator> nameMap;
    
    private CFFOperator(final Key key, final String name) {
        this.operatorKey = null;
        this.operatorName = null;
        this.setKey(key);
        this.setName(name);
    }
    
    public Key getKey() {
        return this.operatorKey;
    }
    
    private void setKey(final Key key) {
        this.operatorKey = key;
    }
    
    public String getName() {
        return this.operatorName;
    }
    
    private void setName(final String name) {
        this.operatorName = name;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    @Override
    public int hashCode() {
        return this.getKey().hashCode();
    }
    
    @Override
    public boolean equals(final Object object) {
        if (object instanceof CFFOperator) {
            final CFFOperator that = (CFFOperator)object;
            return this.getKey().equals(that.getKey());
        }
        return false;
    }
    
    private static void register(final Key key, final String name) {
        final CFFOperator operator = new CFFOperator(key, name);
        CFFOperator.keyMap.put(key, operator);
        CFFOperator.nameMap.put(name, operator);
    }
    
    public static CFFOperator getOperator(final Key key) {
        return CFFOperator.keyMap.get(key);
    }
    
    public static CFFOperator getOperator(final String name) {
        return CFFOperator.nameMap.get(name);
    }
    
    static {
        CFFOperator.keyMap = new LinkedHashMap<Key, CFFOperator>();
        CFFOperator.nameMap = new LinkedHashMap<String, CFFOperator>();
        register(new Key(0), "version");
        register(new Key(1), "Notice");
        register(new Key(12, 0), "Copyright");
        register(new Key(2), "FullName");
        register(new Key(3), "FamilyName");
        register(new Key(4), "Weight");
        register(new Key(12, 1), "isFixedPitch");
        register(new Key(12, 2), "ItalicAngle");
        register(new Key(12, 3), "UnderlinePosition");
        register(new Key(12, 4), "UnderlineThickness");
        register(new Key(12, 5), "PaintType");
        register(new Key(12, 6), "CharstringType");
        register(new Key(12, 7), "FontMatrix");
        register(new Key(13), "UniqueID");
        register(new Key(5), "FontBBox");
        register(new Key(12, 8), "StrokeWidth");
        register(new Key(14), "XUID");
        register(new Key(15), "charset");
        register(new Key(16), "Encoding");
        register(new Key(17), "CharStrings");
        register(new Key(18), "Private");
        register(new Key(12, 20), "SyntheticBase");
        register(new Key(12, 21), "PostScript");
        register(new Key(12, 22), "BaseFontName");
        register(new Key(12, 23), "BaseFontBlend");
        register(new Key(12, 30), "ROS");
        register(new Key(12, 31), "CIDFontVersion");
        register(new Key(12, 32), "CIDFontRevision");
        register(new Key(12, 33), "CIDFontType");
        register(new Key(12, 34), "CIDCount");
        register(new Key(12, 35), "UIDBase");
        register(new Key(12, 36), "FDArray");
        register(new Key(12, 37), "FDSelect");
        register(new Key(12, 38), "FontName");
        register(new Key(6), "BlueValues");
        register(new Key(7), "OtherBlues");
        register(new Key(8), "FamilyBlues");
        register(new Key(9), "FamilyOtherBlues");
        register(new Key(12, 9), "BlueScale");
        register(new Key(12, 10), "BlueShift");
        register(new Key(12, 11), "BlueFuzz");
        register(new Key(10), "StdHW");
        register(new Key(11), "StdVW");
        register(new Key(12, 12), "StemSnapH");
        register(new Key(12, 13), "StemSnapV");
        register(new Key(12, 14), "ForceBold");
        register(new Key(12, 15), "LanguageGroup");
        register(new Key(12, 16), "ExpansionFactor");
        register(new Key(12, 17), "initialRandomSeed");
        register(new Key(19), "Subrs");
        register(new Key(20), "defaultWidthX");
        register(new Key(21), "nominalWidthX");
    }
    
    public static class Key
    {
        private int[] value;
        
        public Key(final int b0) {
            this(new int[] { b0 });
        }
        
        public Key(final int b0, final int b1) {
            this(new int[] { b0, b1 });
        }
        
        private Key(final int[] value) {
            this.value = null;
            this.setValue(value);
        }
        
        public int[] getValue() {
            return this.value;
        }
        
        private void setValue(final int[] value) {
            this.value = value;
        }
        
        @Override
        public String toString() {
            return Arrays.toString(this.getValue());
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(this.getValue());
        }
        
        @Override
        public boolean equals(final Object object) {
            if (object instanceof Key) {
                final Key that = (Key)object;
                return Arrays.equals(this.getValue(), that.getValue());
            }
            return false;
        }
    }
}
