// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Arrays;

public class IndexData
{
    private int count;
    private int[] offset;
    private int[] data;
    
    public IndexData(final int count) {
        this.count = count;
        this.offset = new int[count + 1];
    }
    
    public byte[] getBytes(final int index) {
        final int length = this.offset[index + 1] - this.offset[index];
        final byte[] bytes = new byte[length];
        for (int i = 0; i < length; ++i) {
            bytes[i] = (byte)this.data[this.offset[index] - 1 + i];
        }
        return bytes;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "[count=" + this.count + ", offset=" + Arrays.toString(this.offset) + ", data=" + Arrays.toString(this.data) + "]";
    }
    
    public int getCount() {
        return this.count;
    }
    
    public void setOffset(final int index, final int value) {
        this.offset[index] = value;
    }
    
    public int getOffset(final int index) {
        return this.offset[index];
    }
    
    public void initData(final int dataSize) {
        this.data = new int[dataSize];
    }
    
    public void setData(final int index, final int value) {
        this.data[index] = value;
    }
}
