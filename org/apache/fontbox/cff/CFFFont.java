// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import org.apache.fontbox.cff.charset.CFFCharset;
import org.apache.fontbox.cff.encoding.CFFEncoding;
import java.util.Map;

public class CFFFont
{
    private String fontname;
    private Map<String, Object> topDict;
    private Map<String, Object> privateDict;
    private CFFEncoding fontEncoding;
    private CFFCharset fontCharset;
    private Map<String, byte[]> charStringsDict;
    private IndexData globalSubrIndex;
    private IndexData localSubrIndex;
    
    public CFFFont() {
        this.fontname = null;
        this.topDict = new LinkedHashMap<String, Object>();
        this.privateDict = new LinkedHashMap<String, Object>();
        this.fontEncoding = null;
        this.fontCharset = null;
        this.charStringsDict = new LinkedHashMap<String, byte[]>();
        this.globalSubrIndex = null;
        this.localSubrIndex = null;
    }
    
    public String getName() {
        return this.fontname;
    }
    
    public void setName(final String name) {
        this.fontname = name;
    }
    
    public Object getProperty(final String name) {
        final Object topDictValue = this.topDict.get(name);
        if (topDictValue != null) {
            return topDictValue;
        }
        final Object privateDictValue = this.privateDict.get(name);
        if (privateDictValue != null) {
            return privateDictValue;
        }
        return null;
    }
    
    public void addValueToTopDict(final String name, final Object value) {
        if (value != null) {
            this.topDict.put(name, value);
        }
    }
    
    public Map<String, Object> getTopDict() {
        return this.topDict;
    }
    
    public void addValueToPrivateDict(final String name, final Object value) {
        if (value != null) {
            this.privateDict.put(name, value);
        }
    }
    
    public Map<String, Object> getPrivateDict() {
        return this.privateDict;
    }
    
    public Collection<Mapping> getMappings() {
        final List<Mapping> mappings = new ArrayList<Mapping>();
        final Set<String> mappedNames = new HashSet<String>();
        for (final CFFEncoding.Entry entry : this.fontEncoding.getEntries()) {
            final String charName = this.fontCharset.getName(entry.getSID());
            if (charName == null) {
                continue;
            }
            final byte[] bytes = this.charStringsDict.get(charName);
            if (bytes == null) {
                continue;
            }
            final Mapping mapping = new Mapping();
            mapping.setCode(entry.getCode());
            mapping.setSID(entry.getSID());
            mapping.setName(charName);
            mapping.setBytes(bytes);
            mappings.add(mapping);
            mappedNames.add(charName);
        }
        if (this.fontEncoding instanceof CFFParser.EmbeddedEncoding) {
            final CFFParser.EmbeddedEncoding embeddedEncoding = (CFFParser.EmbeddedEncoding)this.fontEncoding;
            for (final CFFParser.EmbeddedEncoding.Supplement supplement : embeddedEncoding.getSupplements()) {
                final String charName2 = this.fontCharset.getName(supplement.getGlyph());
                if (charName2 == null) {
                    continue;
                }
                final byte[] bytes2 = this.charStringsDict.get(charName2);
                if (bytes2 == null) {
                    continue;
                }
                final Mapping mapping2 = new Mapping();
                mapping2.setCode(supplement.getCode());
                mapping2.setSID(supplement.getGlyph());
                mapping2.setName(charName2);
                mapping2.setBytes(bytes2);
                mappings.add(mapping2);
                mappedNames.add(charName2);
            }
        }
        int code = 256;
        for (final CFFCharset.Entry entry2 : this.fontCharset.getEntries()) {
            final String name = entry2.getName();
            if (mappedNames.contains(name)) {
                continue;
            }
            final byte[] bytes2 = this.charStringsDict.get(name);
            if (bytes2 == null) {
                continue;
            }
            final Mapping mapping2 = new Mapping();
            mapping2.setCode(code++);
            mapping2.setSID(entry2.getSID());
            mapping2.setName(name);
            mapping2.setBytes(bytes2);
            mappings.add(mapping2);
            mappedNames.add(name);
        }
        return mappings;
    }
    
    public int getWidth(final int SID) throws IOException {
        final int nominalWidth = this.privateDict.containsKey("nominalWidthX") ? this.privateDict.get("nominalWidthX").intValue() : 0;
        final int defaultWidth = this.privateDict.containsKey("defaultWidthX") ? this.privateDict.get("defaultWidthX").intValue() : 1000;
        for (final Mapping m : this.getMappings()) {
            if (m.getSID() == SID) {
                CharStringRenderer csr = null;
                if (((Number)this.getProperty("CharstringType")).intValue() == 2) {
                    final List<Object> lSeq = m.toType2Sequence();
                    csr = new CharStringRenderer(false);
                    csr.render(lSeq);
                }
                else {
                    final List<Object> lSeq = m.toType1Sequence();
                    csr = new CharStringRenderer();
                    csr.render(lSeq);
                }
                return (csr.getWidth() != 0) ? (csr.getWidth() + nominalWidth) : defaultWidth;
            }
        }
        return this.getNotDefWidth(defaultWidth, nominalWidth);
    }
    
    protected int getNotDefWidth(final int defaultWidth, final int nominalWidth) throws IOException {
        final byte[] glyphDesc = this.getCharStringsDict().get(".notdef");
        CharStringRenderer csr;
        if (((Number)this.getProperty("CharstringType")).intValue() == 2) {
            final Type2CharStringParser parser = new Type2CharStringParser();
            final List<Object> lSeq = parser.parse(glyphDesc);
            csr = new CharStringRenderer(false);
            csr.render(lSeq);
        }
        else {
            final Type1CharStringParser parser2 = new Type1CharStringParser();
            final List<Object> lSeq = parser2.parse(glyphDesc);
            csr = new CharStringRenderer();
            csr.render(lSeq);
        }
        return (csr.getWidth() != 0) ? (csr.getWidth() + nominalWidth) : defaultWidth;
    }
    
    public CFFEncoding getEncoding() {
        return this.fontEncoding;
    }
    
    public void setEncoding(final CFFEncoding encoding) {
        this.fontEncoding = encoding;
    }
    
    public CFFCharset getCharset() {
        return this.fontCharset;
    }
    
    public void setCharset(final CFFCharset charset) {
        this.fontCharset = charset;
    }
    
    public Map<String, byte[]> getCharStringsDict() {
        return this.charStringsDict;
    }
    
    public CharStringConverter createConverter() {
        final Number defaultWidthX = (Number)this.getProperty("defaultWidthX");
        final Number nominalWidthX = (Number)this.getProperty("nominalWidthX");
        return new CharStringConverter(defaultWidthX.intValue(), nominalWidthX.intValue(), this.getGlobalSubrIndex(), this.getLocalSubrIndex());
    }
    
    public CharStringRenderer createRenderer() {
        return new CharStringRenderer();
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "[name=" + this.fontname + ", topDict=" + this.topDict + ", privateDict=" + this.privateDict + ", encoding=" + this.fontEncoding + ", charset=" + this.fontCharset + ", charStringsDict=" + this.charStringsDict + "]";
    }
    
    public void setGlobalSubrIndex(final IndexData globalSubrIndex) {
        this.globalSubrIndex = globalSubrIndex;
    }
    
    public IndexData getGlobalSubrIndex() {
        return this.globalSubrIndex;
    }
    
    public IndexData getLocalSubrIndex() {
        return this.localSubrIndex;
    }
    
    public void setLocalSubrIndex(final IndexData localSubrIndex) {
        this.localSubrIndex = localSubrIndex;
    }
    
    public class Mapping
    {
        private int mappedCode;
        private int mappedSID;
        private String mappedName;
        private byte[] mappedBytes;
        
        public List<Object> toType1Sequence() throws IOException {
            final CharStringConverter converter = CFFFont.this.createConverter();
            return converter.convert(this.toType2Sequence());
        }
        
        public List<Object> toType2Sequence() throws IOException {
            final Type2CharStringParser parser = new Type2CharStringParser();
            return parser.parse(this.getBytes());
        }
        
        public int getCode() {
            return this.mappedCode;
        }
        
        private void setCode(final int code) {
            this.mappedCode = code;
        }
        
        public int getSID() {
            return this.mappedSID;
        }
        
        private void setSID(final int sid) {
            this.mappedSID = sid;
        }
        
        public String getName() {
            return this.mappedName;
        }
        
        private void setName(final String name) {
            this.mappedName = name;
        }
        
        public byte[] getBytes() {
            return this.mappedBytes;
        }
        
        private void setBytes(final byte[] bytes) {
            this.mappedBytes = bytes;
        }
    }
}
