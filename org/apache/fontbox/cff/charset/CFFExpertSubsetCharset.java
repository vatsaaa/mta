// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff.charset;

public class CFFExpertSubsetCharset extends CFFCharset
{
    private static final CFFExpertSubsetCharset INSTANCE;
    
    private CFFExpertSubsetCharset() {
    }
    
    public static CFFExpertSubsetCharset getInstance() {
        return CFFExpertSubsetCharset.INSTANCE;
    }
    
    static {
        (INSTANCE = new CFFExpertSubsetCharset()).register(1, "space");
        CFFExpertSubsetCharset.INSTANCE.register(13, "comma");
        CFFExpertSubsetCharset.INSTANCE.register(14, "hyphen");
        CFFExpertSubsetCharset.INSTANCE.register(15, "period");
        CFFExpertSubsetCharset.INSTANCE.register(27, "colon");
        CFFExpertSubsetCharset.INSTANCE.register(28, "semicolon");
        CFFExpertSubsetCharset.INSTANCE.register(99, "fraction");
        CFFExpertSubsetCharset.INSTANCE.register(109, "fi");
        CFFExpertSubsetCharset.INSTANCE.register(110, "fl");
        CFFExpertSubsetCharset.INSTANCE.register(150, "onesuperior");
        CFFExpertSubsetCharset.INSTANCE.register(155, "onehalf");
        CFFExpertSubsetCharset.INSTANCE.register(158, "onequarter");
        CFFExpertSubsetCharset.INSTANCE.register(163, "threequarters");
        CFFExpertSubsetCharset.INSTANCE.register(164, "twosuperior");
        CFFExpertSubsetCharset.INSTANCE.register(169, "threesuperior");
        CFFExpertSubsetCharset.INSTANCE.register(231, "dollaroldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(232, "dollarsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(235, "parenleftsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(236, "parenrightsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(237, "twodotenleader");
        CFFExpertSubsetCharset.INSTANCE.register(238, "onedotenleader");
        CFFExpertSubsetCharset.INSTANCE.register(239, "zerooldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(240, "oneoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(241, "twooldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(242, "threeoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(243, "fouroldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(244, "fiveoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(245, "sixoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(246, "sevenoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(247, "eightoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(248, "nineoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(249, "commasuperior");
        CFFExpertSubsetCharset.INSTANCE.register(250, "threequartersemdash");
        CFFExpertSubsetCharset.INSTANCE.register(251, "periodsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(253, "asuperior");
        CFFExpertSubsetCharset.INSTANCE.register(254, "bsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(255, "centsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(256, "dsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(257, "esuperior");
        CFFExpertSubsetCharset.INSTANCE.register(258, "isuperior");
        CFFExpertSubsetCharset.INSTANCE.register(259, "lsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(260, "msuperior");
        CFFExpertSubsetCharset.INSTANCE.register(261, "nsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(262, "osuperior");
        CFFExpertSubsetCharset.INSTANCE.register(263, "rsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(264, "ssuperior");
        CFFExpertSubsetCharset.INSTANCE.register(265, "tsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(266, "ff");
        CFFExpertSubsetCharset.INSTANCE.register(267, "ffi");
        CFFExpertSubsetCharset.INSTANCE.register(268, "ffl");
        CFFExpertSubsetCharset.INSTANCE.register(269, "parenleftinferior");
        CFFExpertSubsetCharset.INSTANCE.register(270, "parenrightinferior");
        CFFExpertSubsetCharset.INSTANCE.register(272, "hyphensuperior");
        CFFExpertSubsetCharset.INSTANCE.register(300, "colonmonetary");
        CFFExpertSubsetCharset.INSTANCE.register(301, "onefitted");
        CFFExpertSubsetCharset.INSTANCE.register(302, "rupiah");
        CFFExpertSubsetCharset.INSTANCE.register(305, "centoldstyle");
        CFFExpertSubsetCharset.INSTANCE.register(314, "figuredash");
        CFFExpertSubsetCharset.INSTANCE.register(315, "hypheninferior");
        CFFExpertSubsetCharset.INSTANCE.register(320, "oneeighth");
        CFFExpertSubsetCharset.INSTANCE.register(321, "threeeighths");
        CFFExpertSubsetCharset.INSTANCE.register(322, "fiveeighths");
        CFFExpertSubsetCharset.INSTANCE.register(323, "seveneighths");
        CFFExpertSubsetCharset.INSTANCE.register(324, "onethird");
        CFFExpertSubsetCharset.INSTANCE.register(325, "twothirds");
        CFFExpertSubsetCharset.INSTANCE.register(326, "zerosuperior");
        CFFExpertSubsetCharset.INSTANCE.register(327, "foursuperior");
        CFFExpertSubsetCharset.INSTANCE.register(328, "fivesuperior");
        CFFExpertSubsetCharset.INSTANCE.register(329, "sixsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(330, "sevensuperior");
        CFFExpertSubsetCharset.INSTANCE.register(331, "eightsuperior");
        CFFExpertSubsetCharset.INSTANCE.register(332, "ninesuperior");
        CFFExpertSubsetCharset.INSTANCE.register(333, "zeroinferior");
        CFFExpertSubsetCharset.INSTANCE.register(334, "oneinferior");
        CFFExpertSubsetCharset.INSTANCE.register(335, "twoinferior");
        CFFExpertSubsetCharset.INSTANCE.register(336, "threeinferior");
        CFFExpertSubsetCharset.INSTANCE.register(337, "fourinferior");
        CFFExpertSubsetCharset.INSTANCE.register(338, "fiveinferior");
        CFFExpertSubsetCharset.INSTANCE.register(339, "sixinferior");
        CFFExpertSubsetCharset.INSTANCE.register(340, "seveninferior");
        CFFExpertSubsetCharset.INSTANCE.register(341, "eightinferior");
        CFFExpertSubsetCharset.INSTANCE.register(342, "nineinferior");
        CFFExpertSubsetCharset.INSTANCE.register(343, "centinferior");
        CFFExpertSubsetCharset.INSTANCE.register(344, "dollarinferior");
        CFFExpertSubsetCharset.INSTANCE.register(345, "periodinferior");
        CFFExpertSubsetCharset.INSTANCE.register(346, "commainferior");
    }
}
