// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff.charset;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public abstract class CFFCharset
{
    private List<Entry> entries;
    
    public CFFCharset() {
        this.entries = new ArrayList<Entry>();
    }
    
    public boolean isFontSpecific() {
        return false;
    }
    
    public int getSID(final String name) {
        for (final Entry entry : this.entries) {
            if (entry.entryName.equals(name)) {
                return entry.entrySID;
            }
        }
        return -1;
    }
    
    public String getName(final int sid) {
        for (final Entry entry : this.entries) {
            if (entry.entrySID == sid) {
                return entry.entryName;
            }
        }
        return null;
    }
    
    public void register(final int sid, final String name) {
        this.entries.add(new Entry(sid, name));
    }
    
    public void addEntry(final Entry entry) {
        this.entries.add(entry);
    }
    
    public List<Entry> getEntries() {
        return this.entries;
    }
    
    public static class Entry
    {
        private int entrySID;
        private String entryName;
        
        protected Entry(final int sid, final String name) {
            this.entrySID = sid;
            this.entryName = name;
        }
        
        public int getSID() {
            return this.entrySID;
        }
        
        public String getName() {
            return this.entryName;
        }
        
        @Override
        public String toString() {
            return "[sid=" + this.entrySID + ", name=" + this.entryName + "]";
        }
    }
}
