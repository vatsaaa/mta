// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Collections;
import java.util.Set;
import java.util.Iterator;
import org.apache.fontbox.cff.encoding.CFFEncoding;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.LinkedList;
import org.apache.fontbox.cff.encoding.CFFExpertEncoding;
import org.apache.fontbox.cff.encoding.CFFStandardEncoding;
import org.apache.fontbox.cff.charset.CFFCharset;
import org.apache.fontbox.cff.charset.CFFExpertSubsetCharset;
import org.apache.fontbox.cff.charset.CFFExpertCharset;
import org.apache.fontbox.cff.charset.CFFISOAdobeCharset;
import java.util.Arrays;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CFFParser
{
    private CFFDataInput input;
    private Header header;
    private IndexData nameIndex;
    private IndexData topDictIndex;
    private IndexData stringIndex;
    
    public CFFParser() {
        this.input = null;
        this.header = null;
        this.nameIndex = null;
        this.topDictIndex = null;
        this.stringIndex = null;
    }
    
    public List<CFFFont> parse(final byte[] bytes) throws IOException {
        this.input = new CFFDataInput(bytes);
        this.header = readHeader(this.input);
        this.nameIndex = readIndexData(this.input);
        this.topDictIndex = readIndexData(this.input);
        this.stringIndex = readIndexData(this.input);
        final IndexData globalSubrIndex = readIndexData(this.input);
        final List<CFFFont> fonts = new ArrayList<CFFFont>();
        for (int i = 0; i < this.nameIndex.getCount(); ++i) {
            final CFFFont font = this.parseFont(i);
            font.setGlobalSubrIndex(globalSubrIndex);
            fonts.add(font);
        }
        return fonts;
    }
    
    private static Header readHeader(final CFFDataInput input) throws IOException {
        final Header header = new Header();
        header.major = input.readCard8();
        header.minor = input.readCard8();
        header.hdrSize = input.readCard8();
        header.offSize = input.readOffSize();
        return header;
    }
    
    private static IndexData readIndexData(final CFFDataInput input) throws IOException {
        final int count = input.readCard16();
        final IndexData index = new IndexData(count);
        if (count == 0) {
            return index;
        }
        final int offSize = input.readOffSize();
        for (int i = 0; i <= count; ++i) {
            index.setOffset(i, input.readOffset(offSize));
        }
        final int dataSize = index.getOffset(count) - index.getOffset(0);
        index.initData(dataSize);
        for (int j = 0; j < dataSize; ++j) {
            index.setData(j, input.readCard8());
        }
        return index;
    }
    
    private static DictData readDictData(final CFFDataInput input) throws IOException {
        final DictData dict = new DictData();
        dict.entries = (List<DictData.Entry>)new ArrayList();
        while (input.hasRemaining()) {
            final DictData.Entry entry = readEntry(input);
            dict.entries.add(entry);
        }
        return dict;
    }
    
    private static DictData.Entry readEntry(final CFFDataInput input) throws IOException {
        final DictData.Entry entry = new DictData.Entry();
        while (true) {
            final int b0 = input.readUnsignedByte();
            if (b0 >= 0 && b0 <= 21) {
                entry.operator = readOperator(input, b0);
                return entry;
            }
            if (b0 == 28 || b0 == 29) {
                entry.operands.add(readIntegerNumber(input, b0));
            }
            else if (b0 == 30) {
                entry.operands.add(readRealNumber(input, b0));
            }
            else {
                if (b0 < 32 || b0 > 254) {
                    throw new IllegalArgumentException();
                }
                entry.operands.add(readIntegerNumber(input, b0));
            }
        }
    }
    
    private static CFFOperator readOperator(final CFFDataInput input, final int b0) throws IOException {
        final CFFOperator.Key key = readOperatorKey(input, b0);
        return CFFOperator.getOperator(key);
    }
    
    private static CFFOperator.Key readOperatorKey(final CFFDataInput input, final int b0) throws IOException {
        if (b0 == 12) {
            final int b = input.readUnsignedByte();
            return new CFFOperator.Key(b0, b);
        }
        return new CFFOperator.Key(b0);
    }
    
    private static Integer readIntegerNumber(final CFFDataInput input, final int b0) throws IOException {
        if (b0 == 28) {
            final int b = input.readUnsignedByte();
            final int b2 = input.readUnsignedByte();
            return (int)(short)(b << 8 | b2);
        }
        if (b0 == 29) {
            final int b = input.readUnsignedByte();
            final int b2 = input.readUnsignedByte();
            final int b3 = input.readUnsignedByte();
            final int b4 = input.readUnsignedByte();
            return b << 24 | b2 << 16 | b3 << 8 | b4;
        }
        if (b0 >= 32 && b0 <= 246) {
            return b0 - 139;
        }
        if (b0 >= 247 && b0 <= 250) {
            final int b = input.readUnsignedByte();
            return (b0 - 247) * 256 + b + 108;
        }
        if (b0 >= 251 && b0 <= 254) {
            final int b = input.readUnsignedByte();
            return -(b0 - 251) * 256 - b - 108;
        }
        throw new IllegalArgumentException();
    }
    
    private static Double readRealNumber(final CFFDataInput input, final int b0) throws IOException {
        final StringBuffer sb = new StringBuffer();
        boolean done = false;
        while (!done) {
            final int b = input.readUnsignedByte();
            final int[] arr$;
            final int[] nibbles = arr$ = new int[] { b / 16, b % 16 };
            for (final int nibble : arr$) {
                switch (nibble) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9: {
                        sb.append(nibble);
                        break;
                    }
                    case 10: {
                        sb.append(".");
                        break;
                    }
                    case 11: {
                        sb.append("E");
                        break;
                    }
                    case 12: {
                        sb.append("E-");
                        break;
                    }
                    case 13: {
                        break;
                    }
                    case 14: {
                        sb.append("-");
                        break;
                    }
                    case 15: {
                        done = true;
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException();
                    }
                }
            }
        }
        return Double.valueOf(sb.toString());
    }
    
    private CFFFont parseFont(final int index) throws IOException {
        CFFFont font = null;
        final DataInput nameInput = new DataInput(this.nameIndex.getBytes(index));
        final String name = nameInput.getString();
        final CFFDataInput topDictInput = new CFFDataInput(this.topDictIndex.getBytes(index));
        final DictData topDict = readDictData(topDictInput);
        final DictData.Entry syntheticBaseEntry = topDict.getEntry("SyntheticBase");
        if (syntheticBaseEntry != null) {
            throw new IOException("Synthetic Fonts are not supported");
        }
        final DictData.Entry rosEntry = topDict.getEntry("ROS");
        if (rosEntry != null) {
            font = new CFFFontROS();
            ((CFFFontROS)font).setRegistry(this.readString(rosEntry.getNumber(0).intValue()));
            ((CFFFontROS)font).setOrdering(this.readString(rosEntry.getNumber(1).intValue()));
            ((CFFFontROS)font).setSupplement(rosEntry.getNumber(2).intValue());
        }
        if (font == null) {
            font = new CFFFont();
        }
        font.setName(name);
        font.addValueToTopDict("version", this.getString(topDict, "version"));
        font.addValueToTopDict("Notice", this.getString(topDict, "Notice"));
        font.addValueToTopDict("Copyright", this.getString(topDict, "Copyright"));
        font.addValueToTopDict("FullName", this.getString(topDict, "FullName"));
        font.addValueToTopDict("FamilyName", this.getString(topDict, "FamilyName"));
        font.addValueToTopDict("Weight", this.getString(topDict, "Weight"));
        font.addValueToTopDict("isFixedPitch", this.getBoolean(topDict, "isFixedPitch", false));
        font.addValueToTopDict("ItalicAngle", this.getNumber(topDict, "ItalicAngle", 0));
        font.addValueToTopDict("UnderlinePosition", this.getNumber(topDict, "UnderlinePosition", -100));
        font.addValueToTopDict("UnderlineThickness", this.getNumber(topDict, "UnderlineThickness", 50));
        font.addValueToTopDict("PaintType", this.getNumber(topDict, "PaintType", 0));
        font.addValueToTopDict("CharstringType", this.getNumber(topDict, "CharstringType", 2));
        font.addValueToTopDict("FontMatrix", this.getArray(topDict, "FontMatrix", Arrays.asList(0.001, 0.0, 0.0, 0.001, 0.0, 0.0)));
        font.addValueToTopDict("UniqueID", this.getNumber(topDict, "UniqueID", null));
        font.addValueToTopDict("FontBBox", this.getArray(topDict, "FontBBox", Arrays.asList(0, 0, 0, 0)));
        font.addValueToTopDict("StrokeWidth", this.getNumber(topDict, "StrokeWidth", 0));
        font.addValueToTopDict("XUID", this.getArray(topDict, "XUID", null));
        final DictData.Entry charStringsEntry = topDict.getEntry("CharStrings");
        final int charStringsOffset = charStringsEntry.getNumber(0).intValue();
        this.input.setPosition(charStringsOffset);
        final IndexData charStringsIndex = readIndexData(this.input);
        final DictData.Entry charsetEntry = topDict.getEntry("charset");
        final int charsetId = (charsetEntry != null) ? charsetEntry.getNumber(0).intValue() : 0;
        CFFCharset charset;
        if (charsetId == 0) {
            charset = CFFISOAdobeCharset.getInstance();
        }
        else if (charsetId == 1) {
            charset = CFFExpertCharset.getInstance();
        }
        else if (charsetId == 2) {
            charset = CFFExpertSubsetCharset.getInstance();
        }
        else {
            this.input.setPosition(charsetId);
            charset = this.readCharset(this.input, charStringsIndex.getCount());
        }
        font.setCharset(charset);
        font.getCharStringsDict().put(".notdef", charStringsIndex.getBytes(0));
        final int[] gids = new int[charStringsIndex.getCount()];
        final List<CFFCharset.Entry> glyphEntries = charset.getEntries();
        for (int i = 1; i < charStringsIndex.getCount(); ++i) {
            final CFFCharset.Entry glyphEntry = glyphEntries.get(i - 1);
            gids[i - 1] = glyphEntry.getSID();
            font.getCharStringsDict().put(glyphEntry.getName(), charStringsIndex.getBytes(i));
        }
        final DictData.Entry encodingEntry = topDict.getEntry("Encoding");
        final int encodingId = (encodingEntry != null) ? encodingEntry.getNumber(0).intValue() : 0;
        CFFEncoding encoding;
        if (encodingId == 0 || rosEntry != null) {
            encoding = CFFStandardEncoding.getInstance();
        }
        else if (encodingId == 1) {
            encoding = CFFExpertEncoding.getInstance();
        }
        else {
            this.input.setPosition(encodingId);
            encoding = this.readEncoding(this.input, gids);
        }
        font.setEncoding(encoding);
        if (rosEntry != null) {
            final DictData.Entry fdArrayEntry = topDict.getEntry("FDArray");
            if (fdArrayEntry == null) {
                throw new IOException("FDArray is missing for a CIDKeyed Font.");
            }
            final int fontDictOffset = fdArrayEntry.getNumber(0).intValue();
            this.input.setPosition(fontDictOffset);
            final IndexData fdIndex = readIndexData(this.input);
            final List<Map<String, Object>> privateDictionaries = new LinkedList<Map<String, Object>>();
            final List<Map<String, Object>> fontDictionaries = new LinkedList<Map<String, Object>>();
            final CFFFontROS fontRos = (CFFFontROS)font;
            for (int j = 0; j < fdIndex.getCount(); ++j) {
                final byte[] b = fdIndex.getBytes(j);
                final CFFDataInput fontDictInput = new CFFDataInput(b);
                final DictData fontDictData = readDictData(fontDictInput);
                final Map<String, Object> fontDictMap = new LinkedHashMap<String, Object>();
                fontDictMap.put("FontName", this.getString(fontDictData, "FontName"));
                fontDictMap.put("FontType", this.getNumber(fontDictData, "FontType", 0));
                fontDictMap.put("FontBBox", this.getDelta(fontDictData, "FontBBox", null));
                fontDictMap.put("FontMatrix", this.getDelta(fontDictData, "FontMatrix", null));
                fontDictionaries.add(fontDictMap);
                final DictData.Entry privateEntry = fontDictData.getEntry("Private");
                if (privateEntry == null) {
                    throw new IOException("Missing Private Dictionary");
                }
                final int privateOffset = privateEntry.getNumber(1).intValue();
                this.input.setPosition(privateOffset);
                final int privateSize = privateEntry.getNumber(0).intValue();
                final CFFDataInput privateDictData = new CFFDataInput(this.input.readBytes(privateSize));
                final DictData privateDict = readDictData(privateDictData);
                final Map<String, Object> privDict = new LinkedHashMap<String, Object>();
                privDict.put("BlueValues", this.getDelta(privateDict, "BlueValues", null));
                privDict.put("OtherBlues", this.getDelta(privateDict, "OtherBlues", null));
                privDict.put("FamilyBlues", this.getDelta(privateDict, "FamilyBlues", null));
                privDict.put("FamilyOtherBlues", this.getDelta(privateDict, "FamilyOtherBlues", null));
                privDict.put("BlueScale", this.getNumber(privateDict, "BlueScale", 0.039625));
                privDict.put("BlueShift", this.getNumber(privateDict, "BlueShift", 7));
                privDict.put("BlueFuzz", this.getNumber(privateDict, "BlueFuzz", 1));
                privDict.put("StdHW", this.getNumber(privateDict, "StdHW", null));
                privDict.put("StdVW", this.getNumber(privateDict, "StdVW", null));
                privDict.put("StemSnapH", this.getDelta(privateDict, "StemSnapH", null));
                privDict.put("StemSnapV", this.getDelta(privateDict, "StemSnapV", null));
                privDict.put("ForceBold", this.getBoolean(privateDict, "ForceBold", false));
                privDict.put("LanguageGroup", this.getNumber(privateDict, "LanguageGroup", 0));
                privDict.put("ExpansionFactor", this.getNumber(privateDict, "ExpansionFactor", 0.06));
                privDict.put("initialRandomSeed", this.getNumber(privateDict, "initialRandomSeed", 0));
                privDict.put("defaultWidthX", this.getNumber(privateDict, "defaultWidthX", 0));
                privDict.put("nominalWidthX", this.getNumber(privateDict, "nominalWidthX", 0));
                final int localSubrOffset = (int)this.getNumber(privateDict, "Subrs", 0);
                if (localSubrOffset == 0) {
                    font.setLocalSubrIndex(new IndexData(0));
                }
                else {
                    this.input.setPosition(privateOffset + localSubrOffset);
                    font.setLocalSubrIndex(readIndexData(this.input));
                }
                privateDictionaries.add(privDict);
            }
            fontRos.setFontDict(fontDictionaries);
            fontRos.setPrivDict(privateDictionaries);
            final DictData.Entry fdSelectEntry = topDict.getEntry("FDSelect");
            final int fdSelectPos = fdSelectEntry.getNumber(0).intValue();
            this.input.setPosition(fdSelectPos);
            final CIDKeyedFDSelect fdSelect = this.readFDSelect(this.input, charStringsIndex.getCount(), fontRos);
            font.addValueToPrivateDict("defaultWidthX", 1000);
            font.addValueToPrivateDict("nominalWidthX", 0);
            fontRos.setFdSelect(fdSelect);
        }
        else {
            final DictData.Entry privateEntry2 = topDict.getEntry("Private");
            final int privateOffset2 = privateEntry2.getNumber(1).intValue();
            this.input.setPosition(privateOffset2);
            final int privateSize2 = privateEntry2.getNumber(0).intValue();
            final CFFDataInput privateDictData2 = new CFFDataInput(this.input.readBytes(privateSize2));
            final DictData privateDict2 = readDictData(privateDictData2);
            font.addValueToPrivateDict("BlueValues", this.getDelta(privateDict2, "BlueValues", null));
            font.addValueToPrivateDict("OtherBlues", this.getDelta(privateDict2, "OtherBlues", null));
            font.addValueToPrivateDict("FamilyBlues", this.getDelta(privateDict2, "FamilyBlues", null));
            font.addValueToPrivateDict("FamilyOtherBlues", this.getDelta(privateDict2, "FamilyOtherBlues", null));
            font.addValueToPrivateDict("BlueScale", this.getNumber(privateDict2, "BlueScale", 0.039625));
            font.addValueToPrivateDict("BlueShift", this.getNumber(privateDict2, "BlueShift", 7));
            font.addValueToPrivateDict("BlueFuzz", this.getNumber(privateDict2, "BlueFuzz", 1));
            font.addValueToPrivateDict("StdHW", this.getNumber(privateDict2, "StdHW", null));
            font.addValueToPrivateDict("StdVW", this.getNumber(privateDict2, "StdVW", null));
            font.addValueToPrivateDict("StemSnapH", this.getDelta(privateDict2, "StemSnapH", null));
            font.addValueToPrivateDict("StemSnapV", this.getDelta(privateDict2, "StemSnapV", null));
            font.addValueToPrivateDict("ForceBold", this.getBoolean(privateDict2, "ForceBold", false));
            font.addValueToPrivateDict("LanguageGroup", this.getNumber(privateDict2, "LanguageGroup", 0));
            font.addValueToPrivateDict("ExpansionFactor", this.getNumber(privateDict2, "ExpansionFactor", 0.06));
            font.addValueToPrivateDict("initialRandomSeed", this.getNumber(privateDict2, "initialRandomSeed", 0));
            font.addValueToPrivateDict("defaultWidthX", this.getNumber(privateDict2, "defaultWidthX", 0));
            font.addValueToPrivateDict("nominalWidthX", this.getNumber(privateDict2, "nominalWidthX", 0));
            final int localSubrOffset2 = (int)this.getNumber(privateDict2, "Subrs", 0);
            if (localSubrOffset2 == 0) {
                font.setLocalSubrIndex(new IndexData(0));
            }
            else {
                this.input.setPosition(privateOffset2 + localSubrOffset2);
                font.setLocalSubrIndex(readIndexData(this.input));
            }
        }
        return font;
    }
    
    private String readString(final int index) throws IOException {
        if (index >= 0 && index <= 390) {
            return CFFStandardString.getName(index);
        }
        if (index - 391 <= this.stringIndex.getCount()) {
            final DataInput dataInput = new DataInput(this.stringIndex.getBytes(index - 391));
            return dataInput.getString();
        }
        return CFFStandardString.getName(0);
    }
    
    private String getString(final DictData dict, final String name) throws IOException {
        final DictData.Entry entry = dict.getEntry(name);
        return (entry != null) ? this.readString(entry.getNumber(0).intValue()) : null;
    }
    
    private Boolean getBoolean(final DictData dict, final String name, final boolean defaultValue) throws IOException {
        final DictData.Entry entry = dict.getEntry(name);
        return (entry != null) ? entry.getBoolean(0) : defaultValue;
    }
    
    private Number getNumber(final DictData dict, final String name, final Number defaultValue) throws IOException {
        final DictData.Entry entry = dict.getEntry(name);
        return (entry != null) ? entry.getNumber(0) : defaultValue;
    }
    
    private List<Number> getArray(final DictData dict, final String name, final List<Number> defaultValue) throws IOException {
        final DictData.Entry entry = dict.getEntry(name);
        return (entry != null) ? entry.getArray() : defaultValue;
    }
    
    private List<Number> getDelta(final DictData dict, final String name, final List<Number> defaultValue) throws IOException {
        final DictData.Entry entry = dict.getEntry(name);
        return (entry != null) ? entry.getArray() : defaultValue;
    }
    
    private CFFEncoding readEncoding(final CFFDataInput dataInput, final int[] gids) throws IOException {
        final int format = dataInput.readCard8();
        final int baseFormat = format & 0x7F;
        if (baseFormat == 0) {
            return this.readFormat0Encoding(dataInput, format, gids);
        }
        if (baseFormat == 1) {
            return this.readFormat1Encoding(dataInput, format, gids);
        }
        throw new IllegalArgumentException();
    }
    
    private Format0Encoding readFormat0Encoding(final CFFDataInput dataInput, final int format, final int[] gids) throws IOException {
        final Format0Encoding encoding = new Format0Encoding();
        encoding.format = format;
        encoding.nCodes = dataInput.readCard8();
        encoding.code = new int[encoding.nCodes];
        for (int i = 0; i < encoding.code.length; ++i) {
            encoding.register(encoding.code[i] = dataInput.readCard8(), gids[i]);
        }
        if ((format & 0x80) != 0x0) {
            this.readSupplement(dataInput, encoding);
        }
        return encoding;
    }
    
    private Format1Encoding readFormat1Encoding(final CFFDataInput dataInput, final int format, final int[] gids) throws IOException {
        final Format1Encoding encoding = new Format1Encoding();
        encoding.format = format;
        encoding.nRanges = dataInput.readCard8();
        int count = 0;
        encoding.range = new Format1Encoding.Range1[encoding.nRanges];
        for (int i = 0; i < encoding.range.length; ++i) {
            final Format1Encoding.Range1 range = new Format1Encoding.Range1();
            range.first = dataInput.readCard8();
            range.nLeft = dataInput.readCard8();
            encoding.range[i] = range;
            for (int j = 0; j < 1 + range.nLeft; ++j) {
                encoding.register(range.first + j, gids[count + j]);
            }
            count += 1 + range.nLeft;
        }
        if ((format & 0x80) != 0x0) {
            this.readSupplement(dataInput, encoding);
        }
        return encoding;
    }
    
    private void readSupplement(final CFFDataInput dataInput, final EmbeddedEncoding encoding) throws IOException {
        encoding.nSups = dataInput.readCard8();
        encoding.supplement = new EmbeddedEncoding.Supplement[encoding.nSups];
        for (int i = 0; i < encoding.supplement.length; ++i) {
            final EmbeddedEncoding.Supplement supplement = new EmbeddedEncoding.Supplement();
            supplement.code = dataInput.readCard8();
            supplement.glyph = dataInput.readSID();
            encoding.supplement[i] = supplement;
        }
    }
    
    private CIDKeyedFDSelect readFDSelect(final CFFDataInput dataInput, final int nGlyphs, final CFFFontROS ros) throws IOException {
        final int format = dataInput.readCard8();
        if (format == 0) {
            return this.readFormat0FDSelect(dataInput, format, nGlyphs, ros);
        }
        if (format == 3) {
            return this.readFormat3FDSelect(dataInput, format, nGlyphs, ros);
        }
        throw new IllegalArgumentException();
    }
    
    private Format0FDSelect readFormat0FDSelect(final CFFDataInput dataInput, final int format, final int nGlyphs, final CFFFontROS ros) throws IOException {
        final Format0FDSelect fdselect = new Format0FDSelect(ros);
        fdselect.format = format;
        fdselect.fds = new int[nGlyphs];
        for (int i = 0; i < fdselect.fds.length; ++i) {
            fdselect.fds[i] = dataInput.readCard8();
        }
        return fdselect;
    }
    
    private Format3FDSelect readFormat3FDSelect(final CFFDataInput dataInput, final int format, final int nGlyphs, final CFFFontROS ros) throws IOException {
        final Format3FDSelect fdselect = new Format3FDSelect(ros);
        fdselect.format = format;
        fdselect.nbRanges = dataInput.readCard16();
        fdselect.range3 = new Range3[fdselect.nbRanges];
        for (int i = 0; i < fdselect.nbRanges; ++i) {
            final Range3 r3 = new Range3();
            r3.first = dataInput.readCard16();
            r3.fd = dataInput.readCard8();
            fdselect.range3[i] = r3;
        }
        fdselect.sentinel = dataInput.readCard16();
        return fdselect;
    }
    
    private CFFCharset readCharset(final CFFDataInput dataInput, final int nGlyphs) throws IOException {
        final int format = dataInput.readCard8();
        if (format == 0) {
            return this.readFormat0Charset(dataInput, format, nGlyphs);
        }
        if (format == 1) {
            return this.readFormat1Charset(dataInput, format, nGlyphs);
        }
        if (format == 2) {
            return this.readFormat2Charset(dataInput, format, nGlyphs);
        }
        throw new IllegalArgumentException();
    }
    
    private Format0Charset readFormat0Charset(final CFFDataInput dataInput, final int format, final int nGlyphs) throws IOException {
        final Format0Charset charset = new Format0Charset();
        charset.format = format;
        charset.glyph = new int[nGlyphs - 1];
        for (int i = 0; i < charset.glyph.length; ++i) {
            charset.register(charset.glyph[i] = dataInput.readSID(), this.readString(charset.glyph[i]));
        }
        return charset;
    }
    
    private Format1Charset readFormat1Charset(final CFFDataInput dataInput, final int format, final int nGlyphs) throws IOException {
        final Format1Charset charset = new Format1Charset();
        charset.format = format;
        final List<Format1Charset.Range1> ranges = new ArrayList<Format1Charset.Range1>();
        Format1Charset.Range1 range;
        for (int i = 0; i < nGlyphs - 1; i += 1 + range.nLeft) {
            range = new Format1Charset.Range1();
            range.first = dataInput.readSID();
            range.nLeft = dataInput.readCard8();
            ranges.add(range);
            for (int j = 0; j < 1 + range.nLeft; ++j) {
                charset.register(range.first + j, this.readString(range.first + j));
            }
        }
        charset.range = ranges.toArray(new Format1Charset.Range1[0]);
        return charset;
    }
    
    private Format2Charset readFormat2Charset(final CFFDataInput dataInput, final int format, final int nGlyphs) throws IOException {
        final Format2Charset charset = new Format2Charset();
        charset.format = format;
        charset.range = new Format2Charset.Range2[0];
        Format2Charset.Range2 range;
        for (int i = 0; i < nGlyphs - 1; i += 1 + range.nLeft) {
            final Format2Charset.Range2[] newRange = new Format2Charset.Range2[charset.range.length + 1];
            System.arraycopy(charset.range, 0, newRange, 0, charset.range.length);
            charset.range = newRange;
            range = new Format2Charset.Range2();
            range.first = dataInput.readSID();
            range.nLeft = dataInput.readCard16();
            charset.range[charset.range.length - 1] = range;
            for (int j = 0; j < 1 + range.nLeft; ++j) {
                charset.register(range.first + j, this.readString(range.first + j));
            }
        }
        return charset;
    }
    
    private static class Format3FDSelect extends CIDKeyedFDSelect
    {
        private int format;
        private int nbRanges;
        private Range3[] range3;
        private int sentinel;
        
        private Format3FDSelect(final CFFFontROS _owner) {
            super(_owner);
        }
        
        @Override
        public int getFd(final int glyph) {
            for (int i = 0; i < this.nbRanges; ++i) {
                if (this.range3[i].first >= glyph) {
                    if (i + 1 < this.nbRanges) {
                        if (this.range3[i + 1].first > glyph) {
                            return this.range3[i].fd;
                        }
                    }
                    else {
                        if (this.sentinel > glyph) {
                            return this.range3[i].fd;
                        }
                        return -1;
                    }
                }
            }
            return 0;
        }
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[format=" + this.format + " nbRanges=" + this.nbRanges + ", range3=" + Arrays.toString(this.range3) + " sentinel=" + this.sentinel + "]";
        }
    }
    
    private static class Range3
    {
        private int first;
        private int fd;
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[first=" + this.first + ", fd=" + this.fd + "]";
        }
    }
    
    private static class Format0FDSelect extends CIDKeyedFDSelect
    {
        private int format;
        private int[] fds;
        
        private Format0FDSelect(final CFFFontROS _owner) {
            super(_owner);
        }
        
        @Override
        public int getFd(final int glyph) {
            for (final CFFFont.Mapping mapping : this.owner.getMappings()) {
                if (mapping.getSID() == glyph) {
                    int index = 0;
                    final Map<String, byte[]> charString = this.owner.getCharStringsDict();
                    final Set<String> keys = charString.keySet();
                    for (final String str : keys) {
                        if (mapping.getName().equals(str)) {
                            return this.fds[index];
                        }
                        ++index;
                    }
                }
            }
            return -1;
        }
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[format=" + this.format + ", fds=" + Arrays.toString(this.fds) + "]";
        }
    }
    
    private static class Header
    {
        private int major;
        private int minor;
        private int hdrSize;
        private int offSize;
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[major=" + this.major + ", minor=" + this.minor + ", hdrSize=" + this.hdrSize + ", offSize=" + this.offSize + "]";
        }
    }
    
    private static class DictData
    {
        private List<Entry> entries;
        
        private DictData() {
            this.entries = null;
        }
        
        public Entry getEntry(final CFFOperator.Key key) {
            return this.getEntry(CFFOperator.getOperator(key));
        }
        
        public Entry getEntry(final String name) {
            return this.getEntry(CFFOperator.getOperator(name));
        }
        
        private Entry getEntry(final CFFOperator operator) {
            for (final Entry entry : this.entries) {
                if (entry != null && entry.operator != null && entry.operator.equals(operator)) {
                    return entry;
                }
            }
            return null;
        }
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[entries=" + this.entries + "]";
        }
        
        private static class Entry
        {
            private List<Number> operands;
            private CFFOperator operator;
            
            private Entry() {
                this.operands = new ArrayList<Number>();
                this.operator = null;
            }
            
            public Number getNumber(final int index) {
                return this.operands.get(index);
            }
            
            public Boolean getBoolean(final int index) {
                final Number operand = this.operands.get(index);
                if (operand instanceof Integer) {
                    switch (operand.intValue()) {
                        case 0: {
                            return Boolean.FALSE;
                        }
                        case 1: {
                            return Boolean.TRUE;
                        }
                    }
                }
                throw new IllegalArgumentException();
            }
            
            public Integer getSID(final int index) {
                final Number operand = this.operands.get(index);
                if (operand instanceof Integer) {
                    return (Integer)operand;
                }
                throw new IllegalArgumentException();
            }
            
            public List<Number> getArray() {
                return this.operands;
            }
            
            public List<Number> getDelta() {
                return this.operands;
            }
            
            @Override
            public String toString() {
                return this.getClass().getName() + "[operands=" + this.operands + ", operator=" + this.operator + "]";
            }
        }
    }
    
    abstract static class EmbeddedEncoding extends CFFEncoding
    {
        private int nSups;
        private Supplement[] supplement;
        
        @Override
        public boolean isFontSpecific() {
            return true;
        }
        
        List<Supplement> getSupplements() {
            if (this.supplement == null) {
                return Collections.emptyList();
            }
            return Arrays.asList(this.supplement);
        }
        
        static class Supplement
        {
            private int code;
            private int glyph;
            
            int getCode() {
                return this.code;
            }
            
            int getGlyph() {
                return this.glyph;
            }
            
            @Override
            public String toString() {
                return this.getClass().getName() + "[code=" + this.code + ", glyph=" + this.glyph + "]";
            }
        }
    }
    
    private static class Format0Encoding extends EmbeddedEncoding
    {
        private int format;
        private int nCodes;
        private int[] code;
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[format=" + this.format + ", nCodes=" + this.nCodes + ", code=" + Arrays.toString(this.code) + ", supplement=" + Arrays.toString(this.supplement) + "]";
        }
    }
    
    private static class Format1Encoding extends EmbeddedEncoding
    {
        private int format;
        private int nRanges;
        private Range1[] range;
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[format=" + this.format + ", nRanges=" + this.nRanges + ", range=" + Arrays.toString(this.range) + ", supplement=" + Arrays.toString(this.supplement) + "]";
        }
        
        private static class Range1
        {
            private int first;
            private int nLeft;
            
            @Override
            public String toString() {
                return this.getClass().getName() + "[first=" + this.first + ", nLeft=" + this.nLeft + "]";
            }
        }
    }
    
    abstract static class EmbeddedCharset extends CFFCharset
    {
        @Override
        public boolean isFontSpecific() {
            return true;
        }
    }
    
    private static class Format0Charset extends EmbeddedCharset
    {
        private int format;
        private int[] glyph;
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[format=" + this.format + ", glyph=" + Arrays.toString(this.glyph) + "]";
        }
    }
    
    private static class Format1Charset extends EmbeddedCharset
    {
        private int format;
        private Range1[] range;
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[format=" + this.format + ", range=" + Arrays.toString(this.range) + "]";
        }
        
        private static class Range1
        {
            private int first;
            private int nLeft;
            
            @Override
            public String toString() {
                return this.getClass().getName() + "[first=" + this.first + ", nLeft=" + this.nLeft + "]";
            }
        }
    }
    
    private static class Format2Charset extends EmbeddedCharset
    {
        private int format;
        private Range2[] range;
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[format=" + this.format + ", range=" + Arrays.toString(this.range) + "]";
        }
        
        private static class Range2
        {
            private int first;
            private int nLeft;
            
            @Override
            public String toString() {
                return this.getClass().getName() + "[first=" + this.first + ", nLeft=" + this.nLeft + "]";
            }
        }
    }
}
