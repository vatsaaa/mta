// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class CharStringCommand
{
    private Key commandKey;
    public static final Map<Key, String> TYPE1_VOCABULARY;
    public static final Map<Key, String> TYPE2_VOCABULARY;
    
    public CharStringCommand(final int b0) {
        this.commandKey = null;
        this.setKey(new Key(b0));
    }
    
    public CharStringCommand(final int b0, final int b1) {
        this.commandKey = null;
        this.setKey(new Key(b0, b1));
    }
    
    public CharStringCommand(final int[] values) {
        this.commandKey = null;
        this.setKey(new Key(values));
    }
    
    public Key getKey() {
        return this.commandKey;
    }
    
    private void setKey(final Key key) {
        this.commandKey = key;
    }
    
    @Override
    public String toString() {
        return this.getKey().toString();
    }
    
    @Override
    public int hashCode() {
        return this.getKey().hashCode();
    }
    
    @Override
    public boolean equals(final Object object) {
        if (object instanceof CharStringCommand) {
            final CharStringCommand that = (CharStringCommand)object;
            return this.getKey().equals(that.getKey());
        }
        return false;
    }
    
    static {
        Map<Key, String> map = new LinkedHashMap<Key, String>();
        map.put(new Key(1), "hstem");
        map.put(new Key(3), "vstem");
        map.put(new Key(4), "vmoveto");
        map.put(new Key(5), "rlineto");
        map.put(new Key(6), "hlineto");
        map.put(new Key(7), "vlineto");
        map.put(new Key(8), "rrcurveto");
        map.put(new Key(9), "closepath");
        map.put(new Key(10), "callsubr");
        map.put(new Key(11), "return");
        map.put(new Key(12), "escape");
        map.put(new Key(12, 0), "dotsection");
        map.put(new Key(12, 1), "vstem3");
        map.put(new Key(12, 2), "hstem3");
        map.put(new Key(12, 6), "seac");
        map.put(new Key(12, 7), "sbw");
        map.put(new Key(12, 12), "div");
        map.put(new Key(12, 16), "callothersubr");
        map.put(new Key(12, 17), "pop");
        map.put(new Key(12, 33), "setcurrentpoint");
        map.put(new Key(13), "hsbw");
        map.put(new Key(14), "endchar");
        map.put(new Key(21), "rmoveto");
        map.put(new Key(22), "hmoveto");
        map.put(new Key(30), "vhcurveto");
        map.put(new Key(31), "hvcurveto");
        TYPE1_VOCABULARY = Collections.unmodifiableMap((Map<? extends Key, ? extends String>)map);
        map = new LinkedHashMap<Key, String>();
        map.put(new Key(1), "hstem");
        map.put(new Key(3), "vstem");
        map.put(new Key(4), "vmoveto");
        map.put(new Key(5), "rlineto");
        map.put(new Key(6), "hlineto");
        map.put(new Key(7), "vlineto");
        map.put(new Key(8), "rrcurveto");
        map.put(new Key(10), "callsubr");
        map.put(new Key(11), "return");
        map.put(new Key(12), "escape");
        map.put(new Key(12, 3), "and");
        map.put(new Key(12, 4), "or");
        map.put(new Key(12, 5), "not");
        map.put(new Key(12, 9), "abs");
        map.put(new Key(12, 10), "add");
        map.put(new Key(12, 11), "sub");
        map.put(new Key(12, 12), "div");
        map.put(new Key(12, 14), "neg");
        map.put(new Key(12, 15), "eq");
        map.put(new Key(12, 18), "drop");
        map.put(new Key(12, 20), "put");
        map.put(new Key(12, 21), "get");
        map.put(new Key(12, 22), "ifelse");
        map.put(new Key(12, 23), "random");
        map.put(new Key(12, 24), "mul");
        map.put(new Key(12, 26), "sqrt");
        map.put(new Key(12, 27), "dup");
        map.put(new Key(12, 28), "exch");
        map.put(new Key(12, 29), "index");
        map.put(new Key(12, 30), "roll");
        map.put(new Key(12, 34), "hflex");
        map.put(new Key(12, 35), "flex");
        map.put(new Key(12, 36), "hflex1");
        map.put(new Key(12, 37), "flex1");
        map.put(new Key(14), "endchar");
        map.put(new Key(18), "hstemhm");
        map.put(new Key(19), "hintmask");
        map.put(new Key(20), "cntrmask");
        map.put(new Key(21), "rmoveto");
        map.put(new Key(22), "hmoveto");
        map.put(new Key(23), "vstemhm");
        map.put(new Key(24), "rcurveline");
        map.put(new Key(25), "rlinecurve");
        map.put(new Key(26), "vvcurveto");
        map.put(new Key(27), "hhcurveto");
        map.put(new Key(28), "shortint");
        map.put(new Key(29), "callgsubr");
        map.put(new Key(30), "vhcurveto");
        map.put(new Key(31), "hvcurveto");
        TYPE2_VOCABULARY = Collections.unmodifiableMap((Map<? extends Key, ? extends String>)map);
    }
    
    public static class Key
    {
        private int[] keyValues;
        
        public Key(final int b0) {
            this.keyValues = null;
            this.setValue(new int[] { b0 });
        }
        
        public Key(final int b0, final int b1) {
            this.keyValues = null;
            this.setValue(new int[] { b0, b1 });
        }
        
        public Key(final int[] values) {
            this.keyValues = null;
            this.setValue(values);
        }
        
        public int[] getValue() {
            return this.keyValues;
        }
        
        private void setValue(final int[] value) {
            this.keyValues = value;
        }
        
        @Override
        public String toString() {
            return Arrays.toString(this.getValue());
        }
        
        @Override
        public int hashCode() {
            if (this.keyValues[0] == 12 && this.keyValues.length > 1) {
                return this.keyValues[0] ^ this.keyValues[1];
            }
            return this.keyValues[0];
        }
        
        @Override
        public boolean equals(final Object object) {
            if (!(object instanceof Key)) {
                return false;
            }
            final Key that = (Key)object;
            if (this.keyValues[0] != 12 || that.keyValues[0] != 12) {
                return this.keyValues[0] == that.keyValues[0];
            }
            if (this.keyValues.length > 1 && that.keyValues.length > 1) {
                return this.keyValues[1] == that.keyValues[1];
            }
            return this.keyValues.length == that.keyValues.length;
        }
    }
}
