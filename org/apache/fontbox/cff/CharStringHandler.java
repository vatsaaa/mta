// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Collection;
import java.util.List;

public abstract class CharStringHandler
{
    public List<Integer> handleSequence(final List<Object> sequence) {
        List<Integer> numbers = null;
        int offset = 0;
        for (int size = sequence.size(), i = 0; i < size; ++i) {
            final Object object = sequence.get(i);
            if (object instanceof CharStringCommand) {
                if (numbers == null) {
                    numbers = sequence.subList(offset, i);
                }
                else {
                    numbers.addAll((Collection<? extends Integer>)sequence.subList(offset, i));
                }
                final List<Integer> stack = this.handleCommand(numbers, (CharStringCommand)object);
                if (stack != null && !stack.isEmpty()) {
                    numbers = stack;
                }
                else {
                    numbers = null;
                }
                offset = i + 1;
            }
        }
        if (numbers != null && !numbers.isEmpty()) {
            return numbers;
        }
        return null;
    }
    
    public abstract List<Integer> handleCommand(final List<Integer> p0, final CharStringCommand p1);
}
