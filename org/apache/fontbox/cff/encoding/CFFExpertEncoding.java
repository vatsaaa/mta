// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff.encoding;

public class CFFExpertEncoding extends CFFEncoding
{
    private static final CFFExpertEncoding INSTANCE;
    
    private CFFExpertEncoding() {
    }
    
    public static CFFExpertEncoding getInstance() {
        return CFFExpertEncoding.INSTANCE;
    }
    
    static {
        (INSTANCE = new CFFExpertEncoding()).register(0, 0);
        CFFExpertEncoding.INSTANCE.register(1, 0);
        CFFExpertEncoding.INSTANCE.register(2, 0);
        CFFExpertEncoding.INSTANCE.register(3, 0);
        CFFExpertEncoding.INSTANCE.register(4, 0);
        CFFExpertEncoding.INSTANCE.register(5, 0);
        CFFExpertEncoding.INSTANCE.register(6, 0);
        CFFExpertEncoding.INSTANCE.register(7, 0);
        CFFExpertEncoding.INSTANCE.register(8, 0);
        CFFExpertEncoding.INSTANCE.register(9, 0);
        CFFExpertEncoding.INSTANCE.register(10, 0);
        CFFExpertEncoding.INSTANCE.register(11, 0);
        CFFExpertEncoding.INSTANCE.register(12, 0);
        CFFExpertEncoding.INSTANCE.register(13, 0);
        CFFExpertEncoding.INSTANCE.register(14, 0);
        CFFExpertEncoding.INSTANCE.register(15, 0);
        CFFExpertEncoding.INSTANCE.register(16, 0);
        CFFExpertEncoding.INSTANCE.register(17, 0);
        CFFExpertEncoding.INSTANCE.register(18, 0);
        CFFExpertEncoding.INSTANCE.register(19, 0);
        CFFExpertEncoding.INSTANCE.register(20, 0);
        CFFExpertEncoding.INSTANCE.register(21, 0);
        CFFExpertEncoding.INSTANCE.register(22, 0);
        CFFExpertEncoding.INSTANCE.register(23, 0);
        CFFExpertEncoding.INSTANCE.register(24, 0);
        CFFExpertEncoding.INSTANCE.register(25, 0);
        CFFExpertEncoding.INSTANCE.register(26, 0);
        CFFExpertEncoding.INSTANCE.register(27, 0);
        CFFExpertEncoding.INSTANCE.register(28, 0);
        CFFExpertEncoding.INSTANCE.register(29, 0);
        CFFExpertEncoding.INSTANCE.register(30, 0);
        CFFExpertEncoding.INSTANCE.register(31, 0);
        CFFExpertEncoding.INSTANCE.register(32, 1);
        CFFExpertEncoding.INSTANCE.register(33, 229);
        CFFExpertEncoding.INSTANCE.register(34, 230);
        CFFExpertEncoding.INSTANCE.register(35, 0);
        CFFExpertEncoding.INSTANCE.register(36, 231);
        CFFExpertEncoding.INSTANCE.register(37, 232);
        CFFExpertEncoding.INSTANCE.register(38, 233);
        CFFExpertEncoding.INSTANCE.register(39, 234);
        CFFExpertEncoding.INSTANCE.register(40, 235);
        CFFExpertEncoding.INSTANCE.register(41, 236);
        CFFExpertEncoding.INSTANCE.register(42, 237);
        CFFExpertEncoding.INSTANCE.register(43, 238);
        CFFExpertEncoding.INSTANCE.register(44, 13);
        CFFExpertEncoding.INSTANCE.register(45, 14);
        CFFExpertEncoding.INSTANCE.register(46, 15);
        CFFExpertEncoding.INSTANCE.register(47, 99);
        CFFExpertEncoding.INSTANCE.register(48, 239);
        CFFExpertEncoding.INSTANCE.register(49, 240);
        CFFExpertEncoding.INSTANCE.register(50, 241);
        CFFExpertEncoding.INSTANCE.register(51, 242);
        CFFExpertEncoding.INSTANCE.register(52, 243);
        CFFExpertEncoding.INSTANCE.register(53, 244);
        CFFExpertEncoding.INSTANCE.register(54, 245);
        CFFExpertEncoding.INSTANCE.register(55, 246);
        CFFExpertEncoding.INSTANCE.register(56, 247);
        CFFExpertEncoding.INSTANCE.register(57, 248);
        CFFExpertEncoding.INSTANCE.register(58, 27);
        CFFExpertEncoding.INSTANCE.register(59, 28);
        CFFExpertEncoding.INSTANCE.register(60, 249);
        CFFExpertEncoding.INSTANCE.register(61, 250);
        CFFExpertEncoding.INSTANCE.register(62, 251);
        CFFExpertEncoding.INSTANCE.register(63, 252);
        CFFExpertEncoding.INSTANCE.register(64, 0);
        CFFExpertEncoding.INSTANCE.register(65, 253);
        CFFExpertEncoding.INSTANCE.register(66, 254);
        CFFExpertEncoding.INSTANCE.register(67, 255);
        CFFExpertEncoding.INSTANCE.register(68, 256);
        CFFExpertEncoding.INSTANCE.register(69, 257);
        CFFExpertEncoding.INSTANCE.register(70, 0);
        CFFExpertEncoding.INSTANCE.register(71, 0);
        CFFExpertEncoding.INSTANCE.register(72, 0);
        CFFExpertEncoding.INSTANCE.register(73, 258);
        CFFExpertEncoding.INSTANCE.register(74, 0);
        CFFExpertEncoding.INSTANCE.register(75, 0);
        CFFExpertEncoding.INSTANCE.register(76, 259);
        CFFExpertEncoding.INSTANCE.register(77, 260);
        CFFExpertEncoding.INSTANCE.register(78, 261);
        CFFExpertEncoding.INSTANCE.register(79, 262);
        CFFExpertEncoding.INSTANCE.register(80, 0);
        CFFExpertEncoding.INSTANCE.register(81, 0);
        CFFExpertEncoding.INSTANCE.register(82, 263);
        CFFExpertEncoding.INSTANCE.register(83, 264);
        CFFExpertEncoding.INSTANCE.register(84, 265);
        CFFExpertEncoding.INSTANCE.register(85, 0);
        CFFExpertEncoding.INSTANCE.register(86, 266);
        CFFExpertEncoding.INSTANCE.register(87, 109);
        CFFExpertEncoding.INSTANCE.register(88, 110);
        CFFExpertEncoding.INSTANCE.register(89, 267);
        CFFExpertEncoding.INSTANCE.register(90, 268);
        CFFExpertEncoding.INSTANCE.register(91, 269);
        CFFExpertEncoding.INSTANCE.register(92, 0);
        CFFExpertEncoding.INSTANCE.register(93, 270);
        CFFExpertEncoding.INSTANCE.register(94, 271);
        CFFExpertEncoding.INSTANCE.register(95, 272);
        CFFExpertEncoding.INSTANCE.register(96, 273);
        CFFExpertEncoding.INSTANCE.register(97, 274);
        CFFExpertEncoding.INSTANCE.register(98, 275);
        CFFExpertEncoding.INSTANCE.register(99, 276);
        CFFExpertEncoding.INSTANCE.register(100, 277);
        CFFExpertEncoding.INSTANCE.register(101, 278);
        CFFExpertEncoding.INSTANCE.register(102, 279);
        CFFExpertEncoding.INSTANCE.register(103, 280);
        CFFExpertEncoding.INSTANCE.register(104, 281);
        CFFExpertEncoding.INSTANCE.register(105, 282);
        CFFExpertEncoding.INSTANCE.register(106, 283);
        CFFExpertEncoding.INSTANCE.register(107, 284);
        CFFExpertEncoding.INSTANCE.register(108, 285);
        CFFExpertEncoding.INSTANCE.register(109, 286);
        CFFExpertEncoding.INSTANCE.register(110, 287);
        CFFExpertEncoding.INSTANCE.register(111, 288);
        CFFExpertEncoding.INSTANCE.register(112, 289);
        CFFExpertEncoding.INSTANCE.register(113, 290);
        CFFExpertEncoding.INSTANCE.register(114, 291);
        CFFExpertEncoding.INSTANCE.register(115, 292);
        CFFExpertEncoding.INSTANCE.register(116, 293);
        CFFExpertEncoding.INSTANCE.register(117, 294);
        CFFExpertEncoding.INSTANCE.register(118, 295);
        CFFExpertEncoding.INSTANCE.register(119, 296);
        CFFExpertEncoding.INSTANCE.register(120, 297);
        CFFExpertEncoding.INSTANCE.register(121, 298);
        CFFExpertEncoding.INSTANCE.register(122, 299);
        CFFExpertEncoding.INSTANCE.register(123, 300);
        CFFExpertEncoding.INSTANCE.register(124, 301);
        CFFExpertEncoding.INSTANCE.register(125, 302);
        CFFExpertEncoding.INSTANCE.register(126, 303);
        CFFExpertEncoding.INSTANCE.register(127, 0);
        CFFExpertEncoding.INSTANCE.register(128, 0);
        CFFExpertEncoding.INSTANCE.register(129, 0);
        CFFExpertEncoding.INSTANCE.register(130, 0);
        CFFExpertEncoding.INSTANCE.register(131, 0);
        CFFExpertEncoding.INSTANCE.register(132, 0);
        CFFExpertEncoding.INSTANCE.register(133, 0);
        CFFExpertEncoding.INSTANCE.register(134, 0);
        CFFExpertEncoding.INSTANCE.register(135, 0);
        CFFExpertEncoding.INSTANCE.register(136, 0);
        CFFExpertEncoding.INSTANCE.register(137, 0);
        CFFExpertEncoding.INSTANCE.register(138, 0);
        CFFExpertEncoding.INSTANCE.register(139, 0);
        CFFExpertEncoding.INSTANCE.register(140, 0);
        CFFExpertEncoding.INSTANCE.register(141, 0);
        CFFExpertEncoding.INSTANCE.register(142, 0);
        CFFExpertEncoding.INSTANCE.register(143, 0);
        CFFExpertEncoding.INSTANCE.register(144, 0);
        CFFExpertEncoding.INSTANCE.register(145, 0);
        CFFExpertEncoding.INSTANCE.register(146, 0);
        CFFExpertEncoding.INSTANCE.register(147, 0);
        CFFExpertEncoding.INSTANCE.register(148, 0);
        CFFExpertEncoding.INSTANCE.register(149, 0);
        CFFExpertEncoding.INSTANCE.register(150, 0);
        CFFExpertEncoding.INSTANCE.register(151, 0);
        CFFExpertEncoding.INSTANCE.register(152, 0);
        CFFExpertEncoding.INSTANCE.register(153, 0);
        CFFExpertEncoding.INSTANCE.register(154, 0);
        CFFExpertEncoding.INSTANCE.register(155, 0);
        CFFExpertEncoding.INSTANCE.register(156, 0);
        CFFExpertEncoding.INSTANCE.register(157, 0);
        CFFExpertEncoding.INSTANCE.register(158, 0);
        CFFExpertEncoding.INSTANCE.register(159, 0);
        CFFExpertEncoding.INSTANCE.register(160, 0);
        CFFExpertEncoding.INSTANCE.register(161, 304);
        CFFExpertEncoding.INSTANCE.register(162, 305);
        CFFExpertEncoding.INSTANCE.register(163, 306);
        CFFExpertEncoding.INSTANCE.register(164, 0);
        CFFExpertEncoding.INSTANCE.register(165, 0);
        CFFExpertEncoding.INSTANCE.register(166, 307);
        CFFExpertEncoding.INSTANCE.register(167, 308);
        CFFExpertEncoding.INSTANCE.register(168, 309);
        CFFExpertEncoding.INSTANCE.register(169, 310);
        CFFExpertEncoding.INSTANCE.register(170, 311);
        CFFExpertEncoding.INSTANCE.register(171, 0);
        CFFExpertEncoding.INSTANCE.register(172, 312);
        CFFExpertEncoding.INSTANCE.register(173, 0);
        CFFExpertEncoding.INSTANCE.register(174, 0);
        CFFExpertEncoding.INSTANCE.register(175, 313);
        CFFExpertEncoding.INSTANCE.register(176, 0);
        CFFExpertEncoding.INSTANCE.register(177, 0);
        CFFExpertEncoding.INSTANCE.register(178, 314);
        CFFExpertEncoding.INSTANCE.register(179, 315);
        CFFExpertEncoding.INSTANCE.register(180, 0);
        CFFExpertEncoding.INSTANCE.register(181, 0);
        CFFExpertEncoding.INSTANCE.register(182, 316);
        CFFExpertEncoding.INSTANCE.register(183, 317);
        CFFExpertEncoding.INSTANCE.register(184, 318);
        CFFExpertEncoding.INSTANCE.register(185, 0);
        CFFExpertEncoding.INSTANCE.register(186, 0);
        CFFExpertEncoding.INSTANCE.register(187, 0);
        CFFExpertEncoding.INSTANCE.register(188, 158);
        CFFExpertEncoding.INSTANCE.register(189, 155);
        CFFExpertEncoding.INSTANCE.register(190, 163);
        CFFExpertEncoding.INSTANCE.register(191, 319);
        CFFExpertEncoding.INSTANCE.register(192, 320);
        CFFExpertEncoding.INSTANCE.register(193, 321);
        CFFExpertEncoding.INSTANCE.register(194, 322);
        CFFExpertEncoding.INSTANCE.register(195, 323);
        CFFExpertEncoding.INSTANCE.register(196, 324);
        CFFExpertEncoding.INSTANCE.register(197, 325);
        CFFExpertEncoding.INSTANCE.register(198, 0);
        CFFExpertEncoding.INSTANCE.register(199, 0);
        CFFExpertEncoding.INSTANCE.register(200, 326);
        CFFExpertEncoding.INSTANCE.register(201, 150);
        CFFExpertEncoding.INSTANCE.register(202, 164);
        CFFExpertEncoding.INSTANCE.register(203, 169);
        CFFExpertEncoding.INSTANCE.register(204, 327);
        CFFExpertEncoding.INSTANCE.register(205, 328);
        CFFExpertEncoding.INSTANCE.register(206, 329);
        CFFExpertEncoding.INSTANCE.register(207, 330);
        CFFExpertEncoding.INSTANCE.register(208, 331);
        CFFExpertEncoding.INSTANCE.register(209, 332);
        CFFExpertEncoding.INSTANCE.register(210, 333);
        CFFExpertEncoding.INSTANCE.register(211, 334);
        CFFExpertEncoding.INSTANCE.register(212, 335);
        CFFExpertEncoding.INSTANCE.register(213, 336);
        CFFExpertEncoding.INSTANCE.register(214, 337);
        CFFExpertEncoding.INSTANCE.register(215, 338);
        CFFExpertEncoding.INSTANCE.register(216, 339);
        CFFExpertEncoding.INSTANCE.register(217, 340);
        CFFExpertEncoding.INSTANCE.register(218, 341);
        CFFExpertEncoding.INSTANCE.register(219, 342);
        CFFExpertEncoding.INSTANCE.register(220, 343);
        CFFExpertEncoding.INSTANCE.register(221, 344);
        CFFExpertEncoding.INSTANCE.register(222, 345);
        CFFExpertEncoding.INSTANCE.register(223, 346);
        CFFExpertEncoding.INSTANCE.register(224, 347);
        CFFExpertEncoding.INSTANCE.register(225, 348);
        CFFExpertEncoding.INSTANCE.register(226, 349);
        CFFExpertEncoding.INSTANCE.register(227, 350);
        CFFExpertEncoding.INSTANCE.register(228, 351);
        CFFExpertEncoding.INSTANCE.register(229, 352);
        CFFExpertEncoding.INSTANCE.register(230, 353);
        CFFExpertEncoding.INSTANCE.register(231, 354);
        CFFExpertEncoding.INSTANCE.register(232, 355);
        CFFExpertEncoding.INSTANCE.register(233, 356);
        CFFExpertEncoding.INSTANCE.register(234, 357);
        CFFExpertEncoding.INSTANCE.register(235, 358);
        CFFExpertEncoding.INSTANCE.register(236, 359);
        CFFExpertEncoding.INSTANCE.register(237, 360);
        CFFExpertEncoding.INSTANCE.register(238, 361);
        CFFExpertEncoding.INSTANCE.register(239, 362);
        CFFExpertEncoding.INSTANCE.register(240, 363);
        CFFExpertEncoding.INSTANCE.register(241, 364);
        CFFExpertEncoding.INSTANCE.register(242, 365);
        CFFExpertEncoding.INSTANCE.register(243, 366);
        CFFExpertEncoding.INSTANCE.register(244, 367);
        CFFExpertEncoding.INSTANCE.register(245, 368);
        CFFExpertEncoding.INSTANCE.register(246, 369);
        CFFExpertEncoding.INSTANCE.register(247, 370);
        CFFExpertEncoding.INSTANCE.register(248, 371);
        CFFExpertEncoding.INSTANCE.register(249, 372);
        CFFExpertEncoding.INSTANCE.register(250, 373);
        CFFExpertEncoding.INSTANCE.register(251, 374);
        CFFExpertEncoding.INSTANCE.register(252, 375);
        CFFExpertEncoding.INSTANCE.register(253, 376);
        CFFExpertEncoding.INSTANCE.register(254, 377);
        CFFExpertEncoding.INSTANCE.register(255, 378);
    }
}
