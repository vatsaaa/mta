// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff.encoding;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public abstract class CFFEncoding
{
    private List<Entry> entries;
    
    public CFFEncoding() {
        this.entries = new ArrayList<Entry>();
    }
    
    public boolean isFontSpecific() {
        return false;
    }
    
    public int getCode(final int sid) {
        for (final Entry entry : this.entries) {
            if (entry.entrySID == sid) {
                return entry.entryCode;
            }
        }
        return -1;
    }
    
    public int getSID(final int code) {
        for (final Entry entry : this.entries) {
            if (entry.entryCode == code) {
                return entry.entrySID;
            }
        }
        return -1;
    }
    
    public void register(final int code, final int sid) {
        this.entries.add(new Entry(code, sid));
    }
    
    public void addEntry(final Entry entry) {
        this.entries.add(entry);
    }
    
    public List<Entry> getEntries() {
        return this.entries;
    }
    
    public static class Entry
    {
        private int entryCode;
        private int entrySID;
        
        protected Entry(final int code, final int sid) {
            this.entryCode = code;
            this.entrySID = sid;
        }
        
        public int getCode() {
            return this.entryCode;
        }
        
        public int getSID() {
            return this.entrySID;
        }
        
        @Override
        public String toString() {
            return "[code=" + this.entryCode + ", sid=" + this.entrySID + "]";
        }
    }
}
