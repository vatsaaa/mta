// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

public abstract class CIDKeyedFDSelect
{
    protected CFFFontROS owner;
    
    public CIDKeyedFDSelect(final CFFFontROS _owner) {
        this.owner = null;
        this.owner = _owner;
    }
    
    public abstract int getFd(final int p0);
}
