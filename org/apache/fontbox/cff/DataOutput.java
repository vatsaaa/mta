// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.io.IOException;
import java.io.ByteArrayOutputStream;

public class DataOutput
{
    private ByteArrayOutputStream outputBuffer;
    private String outputEncoding;
    
    public DataOutput() {
        this("ISO-8859-1");
    }
    
    public DataOutput(final String encoding) {
        this.outputBuffer = new ByteArrayOutputStream();
        this.outputEncoding = null;
        this.outputEncoding = encoding;
    }
    
    public byte[] getBytes() {
        return this.outputBuffer.toByteArray();
    }
    
    public void write(final int value) {
        this.outputBuffer.write(value);
    }
    
    public void write(final byte[] buffer) {
        this.outputBuffer.write(buffer, 0, buffer.length);
    }
    
    public void write(final byte[] buffer, final int offset, final int length) {
        this.outputBuffer.write(buffer, offset, length);
    }
    
    public void print(final String string) throws IOException {
        this.write(string.getBytes(this.outputEncoding));
    }
    
    public void println(final String string) throws IOException {
        this.write(string.getBytes(this.outputEncoding));
        this.write(10);
    }
    
    public void println() {
        this.write(10);
    }
}
