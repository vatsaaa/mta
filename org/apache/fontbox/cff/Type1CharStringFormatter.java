// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Iterator;
import java.util.List;
import java.io.ByteArrayOutputStream;

public class Type1CharStringFormatter
{
    private ByteArrayOutputStream output;
    
    public Type1CharStringFormatter() {
        this.output = null;
    }
    
    public byte[] format(final List<Object> sequence) {
        this.output = new ByteArrayOutputStream();
        for (final Object object : sequence) {
            if (object instanceof CharStringCommand) {
                this.writeCommand((CharStringCommand)object);
            }
            else {
                if (!(object instanceof Integer)) {
                    throw new IllegalArgumentException();
                }
                this.writeNumber((Integer)object);
            }
        }
        return this.output.toByteArray();
    }
    
    private void writeCommand(final CharStringCommand command) {
        final int[] value = command.getKey().getValue();
        for (int i = 0; i < value.length; ++i) {
            this.output.write(value[i]);
        }
    }
    
    private void writeNumber(final Integer number) {
        final int value = number;
        if (value >= -107 && value <= 107) {
            this.output.write(value + 139);
        }
        else if (value >= 108 && value <= 1131) {
            final int b1 = (value - 108) % 256;
            final int b2 = (value - 108 - b1) / 256 + 247;
            this.output.write(b2);
            this.output.write(b1);
        }
        else if (value >= -1131 && value <= -108) {
            final int b1 = -((value + 108) % 256);
            final int b2 = -((value + 108 + b1) / 256 - 251);
            this.output.write(b2);
            this.output.write(b1);
        }
        else {
            final int b1 = value >>> 24 & 0xFF;
            final int b3 = value >>> 16 & 0xFF;
            final int b4 = value >>> 8 & 0xFF;
            final int b5 = value >>> 0 & 0xFF;
            this.output.write(255);
            this.output.write(b1);
            this.output.write(b3);
            this.output.write(b4);
            this.output.write(b5);
        }
    }
}
