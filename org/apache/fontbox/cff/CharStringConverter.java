// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Collections;
import java.util.Arrays;
import java.io.IOException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class CharStringConverter extends CharStringHandler
{
    private int defaultWidthX;
    private int nominalWidthX;
    private List<Object> sequence;
    private int pathCount;
    private IndexData globalSubrIndex;
    private IndexData localSubrIndex;
    
    public CharStringConverter(final int defaultWidth, final int nominalWidth, final IndexData fontGlobalSubrIndex, final IndexData fontLocalSubrIndex) {
        this.defaultWidthX = 0;
        this.nominalWidthX = 0;
        this.sequence = null;
        this.pathCount = 0;
        this.globalSubrIndex = null;
        this.localSubrIndex = null;
        this.defaultWidthX = defaultWidth;
        this.nominalWidthX = nominalWidth;
        this.globalSubrIndex = fontGlobalSubrIndex;
        this.localSubrIndex = fontLocalSubrIndex;
    }
    
    public List<Object> convert(final List<Object> commandSequence) {
        this.sequence = new ArrayList<Object>();
        this.pathCount = 0;
        this.handleSequence(commandSequence);
        return this.sequence;
    }
    
    @Override
    public List<Integer> handleCommand(final List<Integer> numbers, final CharStringCommand command) {
        if (CharStringCommand.TYPE1_VOCABULARY.containsKey(command.getKey())) {
            return this.handleType1Command(numbers, command);
        }
        return this.handleType2Command(numbers, command);
    }
    
    private List<Integer> handleType1Command(List<Integer> numbers, final CharStringCommand command) {
        final String name = CharStringCommand.TYPE1_VOCABULARY.get(command.getKey());
        if ("hstem".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() % 2 != 0);
            this.expandStemHints(numbers, true);
        }
        else if ("vstem".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() % 2 != 0);
            this.expandStemHints(numbers, false);
        }
        else if ("vmoveto".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() > 1);
            this.markPath();
            this.addCommand(numbers, command);
        }
        else if ("rlineto".equals(name)) {
            this.addCommandList(split(numbers, 2), command);
        }
        else if ("hlineto".equals(name)) {
            this.drawAlternatingLine(numbers, true);
        }
        else if ("vlineto".equals(name)) {
            this.drawAlternatingLine(numbers, false);
        }
        else if ("rrcurveto".equals(name)) {
            this.addCommandList(split(numbers, 6), command);
        }
        else if ("endchar".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() > 0);
            this.closePath();
            this.addCommand(numbers, command);
        }
        else if ("rmoveto".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() > 2);
            this.markPath();
            this.addCommand(numbers, command);
        }
        else if ("hmoveto".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() > 1);
            this.markPath();
            this.addCommand(numbers, command);
        }
        else if ("vhcurveto".equals(name)) {
            this.drawAlternatingCurve(numbers, false);
        }
        else if ("hvcurveto".equals(name)) {
            this.drawAlternatingCurve(numbers, true);
        }
        else {
            if ("callsubr".equals(name)) {
                int bias = 0;
                final int nSubrs = this.localSubrIndex.getCount();
                if (nSubrs < 1240) {
                    bias = 107;
                }
                else if (nSubrs < 33900) {
                    bias = 1131;
                }
                else {
                    bias = 32768;
                }
                List<Integer> result = null;
                final int subrNumber = bias + numbers.get(numbers.size() - 1);
                if (subrNumber < this.localSubrIndex.getCount()) {
                    final Type2CharStringParser parser = new Type2CharStringParser();
                    final byte[] bytes = this.localSubrIndex.getBytes(subrNumber);
                    List<Object> parsed = null;
                    try {
                        parsed = parser.parse(bytes);
                        parsed.addAll(0, numbers.subList(0, numbers.size() - 1));
                        result = this.handleSequence(parsed);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return result;
            }
            if ("return".equals(name)) {
                return numbers;
            }
            this.addCommand(numbers, command);
        }
        return null;
    }
    
    private List<Integer> handleType2Command(List<Integer> numbers, final CharStringCommand command) {
        final String name = CharStringCommand.TYPE2_VOCABULARY.get(command.getKey());
        if ("hflex".equals(name)) {
            final List<Integer> first = Arrays.asList(numbers.get(0), 0, numbers.get(1), numbers.get(2), numbers.get(3), 0);
            final List<Integer> second = Arrays.asList(numbers.get(4), 0, numbers.get(5), -numbers.get(2), numbers.get(6), 0);
            this.addCommandList(Arrays.asList(first, second), new CharStringCommand(8));
        }
        else if ("flex".equals(name)) {
            final List<Integer> first = numbers.subList(0, 6);
            final List<Integer> second = numbers.subList(6, 12);
            this.addCommandList(Arrays.asList(first, second), new CharStringCommand(8));
        }
        else if ("hflex1".equals(name)) {
            final List<Integer> first = Arrays.asList(numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3), numbers.get(4), 0);
            final List<Integer> second = Arrays.asList(numbers.get(5), 0, numbers.get(6), numbers.get(7), numbers.get(8), 0);
            this.addCommandList(Arrays.asList(first, second), new CharStringCommand(8));
        }
        else if ("flex1".equals(name)) {
            int dx = 0;
            int dy = 0;
            for (int i = 0; i < 5; ++i) {
                dx += numbers.get(i * 2);
                dy += numbers.get(i * 2 + 1);
            }
            final List<Integer> first2 = numbers.subList(0, 6);
            final List<Integer> second2 = Arrays.asList(numbers.get(6), numbers.get(7), numbers.get(8), numbers.get(9), (Math.abs(dx) > Math.abs(dy)) ? numbers.get(10) : (-dx), (Math.abs(dx) > Math.abs(dy)) ? (-dy) : numbers.get(10));
            this.addCommandList(Arrays.asList(first2, second2), new CharStringCommand(8));
        }
        else if ("hstemhm".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() % 2 != 0);
            this.expandStemHints(numbers, true);
        }
        else if ("hintmask".equals(name) || "cntrmask".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() % 2 != 0);
            if (numbers.size() > 0) {
                this.expandStemHints(numbers, false);
            }
        }
        else if ("vstemhm".equals(name)) {
            numbers = this.clearStack(numbers, numbers.size() % 2 != 0);
            this.expandStemHints(numbers, false);
        }
        else if ("rcurveline".equals(name)) {
            this.addCommandList(split(numbers.subList(0, numbers.size() - 2), 6), new CharStringCommand(8));
            this.addCommand(numbers.subList(numbers.size() - 2, numbers.size()), new CharStringCommand(5));
        }
        else if ("rlinecurve".equals(name)) {
            this.addCommandList(split(numbers.subList(0, numbers.size() - 6), 2), new CharStringCommand(5));
            this.addCommand(numbers.subList(numbers.size() - 6, numbers.size()), new CharStringCommand(8));
        }
        else if ("vvcurveto".equals(name)) {
            this.drawCurve(numbers, false);
        }
        else if ("hhcurveto".equals(name)) {
            this.drawCurve(numbers, true);
        }
        else {
            if ("callgsubr".equals(name)) {
                int bias = 0;
                final int nSubrs = this.globalSubrIndex.getCount();
                if (nSubrs < 1240) {
                    bias = 107;
                }
                else if (nSubrs < 33900) {
                    bias = 1131;
                }
                else {
                    bias = 32768;
                }
                List<Integer> result = null;
                final int subrNumber = bias + numbers.get(numbers.size() - 1);
                if (subrNumber < nSubrs) {
                    final Type2CharStringParser parser = new Type2CharStringParser();
                    final byte[] bytes = this.globalSubrIndex.getBytes(subrNumber);
                    List<Object> parsed = null;
                    try {
                        parsed = parser.parse(bytes);
                        parsed.addAll(0, numbers.subList(0, numbers.size() - 1));
                        result = this.handleSequence(parsed);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return result;
            }
            this.addCommand(numbers, command);
        }
        return null;
    }
    
    private List<Integer> clearStack(List<Integer> numbers, final boolean flag) {
        if (this.sequence.size() == 0) {
            if (flag) {
                this.addCommand(Arrays.asList(0, numbers.get(0) + this.nominalWidthX), new CharStringCommand(13));
                numbers = numbers.subList(1, numbers.size());
            }
            else {
                this.addCommand(Arrays.asList(0, this.defaultWidthX), new CharStringCommand(13));
            }
        }
        return numbers;
    }
    
    private void expandStemHints(final List<Integer> numbers, final boolean horizontal) {
    }
    
    private void markPath() {
        if (this.pathCount > 0) {
            this.closePath();
        }
        ++this.pathCount;
    }
    
    private void closePath() {
        final CharStringCommand command = (this.pathCount > 0) ? this.sequence.get(this.sequence.size() - 1) : null;
        final CharStringCommand closepathCommand = new CharStringCommand(9);
        if (command != null && !closepathCommand.equals(command)) {
            this.addCommand(Collections.emptyList(), closepathCommand);
        }
    }
    
    private void drawAlternatingLine(List<Integer> numbers, boolean horizontal) {
        while (numbers.size() > 0) {
            this.addCommand(numbers.subList(0, 1), new CharStringCommand(horizontal ? 6 : 7));
            numbers = numbers.subList(1, numbers.size());
            horizontal = !horizontal;
        }
    }
    
    private void drawAlternatingCurve(List<Integer> numbers, boolean horizontal) {
        while (numbers.size() > 0) {
            final boolean last = numbers.size() == 5;
            if (horizontal) {
                this.addCommand(Arrays.asList(numbers.get(0), 0, numbers.get(1), numbers.get(2), last ? numbers.get(4) : 0, numbers.get(3)), new CharStringCommand(8));
            }
            else {
                this.addCommand(Arrays.asList(0, numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3), last ? numbers.get(4) : 0), new CharStringCommand(8));
            }
            numbers = numbers.subList(last ? 5 : 4, numbers.size());
            horizontal = !horizontal;
        }
    }
    
    private void drawCurve(List<Integer> numbers, final boolean horizontal) {
        while (numbers.size() > 0) {
            final boolean first = numbers.size() % 4 == 1;
            if (horizontal) {
                this.addCommand(Arrays.asList(numbers.get(first ? 1 : 0), first ? numbers.get(0) : 0, numbers.get(first ? 2 : 1), numbers.get(first ? 3 : 2), numbers.get(first ? 4 : 3), 0), new CharStringCommand(8));
            }
            else {
                this.addCommand(Arrays.asList(first ? numbers.get(0) : 0, numbers.get(first ? 1 : 0), numbers.get(first ? 2 : 1), numbers.get(first ? 3 : 2), 0, numbers.get(first ? 4 : 3)), new CharStringCommand(8));
            }
            numbers = numbers.subList(first ? 5 : 4, numbers.size());
        }
    }
    
    private void addCommandList(final List<List<Integer>> numbers, final CharStringCommand command) {
        for (int i = 0; i < numbers.size(); ++i) {
            this.addCommand(numbers.get(i), command);
        }
    }
    
    private void addCommand(final List<Integer> numbers, final CharStringCommand command) {
        this.sequence.addAll(numbers);
        this.sequence.add(command);
    }
    
    private static <E> List<List<E>> split(final List<E> list, final int size) {
        final List<List<E>> result = new ArrayList<List<E>>();
        for (int i = 0; i < list.size() / size; ++i) {
            result.add(list.subList(i * size, (i + 1) * size));
        }
        return result;
    }
}
