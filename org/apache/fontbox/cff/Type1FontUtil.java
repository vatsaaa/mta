// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

public class Type1FontUtil
{
    private Type1FontUtil() {
    }
    
    public static String hexEncode(final byte[] bytes) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; ++i) {
            final String string = Integer.toHexString(bytes[i] & 0xFF);
            if (string.length() == 1) {
                sb.append("0");
            }
            sb.append(string.toUpperCase());
        }
        return sb.toString();
    }
    
    public static byte[] hexDecode(final String string) {
        if (string.length() % 2 != 0) {
            throw new IllegalArgumentException();
        }
        final byte[] bytes = new byte[string.length() / 2];
        for (int i = 0; i < string.length(); i += 2) {
            bytes[i / 2] = (byte)Integer.parseInt(string.substring(i, i + 2), 16);
        }
        return bytes;
    }
    
    public static byte[] eexecEncrypt(final byte[] buffer) {
        return encrypt(buffer, 55665, 4);
    }
    
    public static byte[] charstringEncrypt(final byte[] buffer, final int n) {
        return encrypt(buffer, 4330, n);
    }
    
    private static byte[] encrypt(final byte[] plaintextBytes, int r, final int n) {
        final byte[] buffer = new byte[plaintextBytes.length + n];
        for (int i = 0; i < n; ++i) {
            buffer[i] = 0;
        }
        System.arraycopy(plaintextBytes, 0, buffer, n, buffer.length - n);
        final int c1 = 52845;
        final int c2 = 22719;
        final byte[] ciphertextBytes = new byte[buffer.length];
        for (int j = 0; j < buffer.length; ++j) {
            final int plain = buffer[j] & 0xFF;
            final int cipher = plain ^ r >> 8;
            ciphertextBytes[j] = (byte)cipher;
            r = ((cipher + r) * c1 + c2 & 0xFFFF);
        }
        return ciphertextBytes;
    }
    
    public static byte[] eexecDecrypt(final byte[] buffer) {
        return decrypt(buffer, 55665, 4);
    }
    
    public static byte[] charstringDecrypt(final byte[] buffer, final int n) {
        return decrypt(buffer, 4330, n);
    }
    
    private static byte[] decrypt(final byte[] ciphertextBytes, int r, final int n) {
        final byte[] buffer = new byte[ciphertextBytes.length];
        final int c1 = 52845;
        final int c2 = 22719;
        for (int i = 0; i < ciphertextBytes.length; ++i) {
            final int cipher = ciphertextBytes[i] & 0xFF;
            final int plain = cipher ^ r >> 8;
            buffer[i] = (byte)plain;
            r = ((cipher + r) * c1 + c2 & 0xFFFF);
        }
        final byte[] plaintextBytes = new byte[ciphertextBytes.length - n];
        System.arraycopy(buffer, n, plaintextBytes, 0, plaintextBytes.length);
        return plaintextBytes;
    }
}
