// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.awt.geom.Rectangle2D;
import org.apache.fontbox.cff.encoding.CFFEncoding;
import java.util.List;
import java.io.IOException;

public class AFMFormatter
{
    private AFMFormatter() {
    }
    
    public static byte[] format(final CFFFont font) throws IOException {
        final DataOutput output = new DataOutput();
        printFont(font, output);
        return output.getBytes();
    }
    
    private static void printFont(final CFFFont font, final DataOutput output) throws IOException {
        printFontMetrics(font, output);
    }
    
    private static void printFontMetrics(final CFFFont font, final DataOutput output) throws IOException {
        final List<CharMetric> metrics = renderFont(font);
        output.println("StartFontMetrics 2.0");
        output.println("FontName " + font.getName());
        output.println("FullName " + font.getProperty("FullName"));
        output.println("FamilyName " + font.getProperty("FamilyName"));
        output.println("Weight " + font.getProperty("Weight"));
        final CFFEncoding encoding = font.getEncoding();
        if (encoding.isFontSpecific()) {
            output.println("EncodingScheme FontSpecific");
        }
        final Rectangle2D bounds = getBounds(metrics);
        output.println("FontBBox " + (int)bounds.getX() + " " + (int)bounds.getY() + " " + (int)bounds.getMaxX() + " " + (int)bounds.getMaxY());
        printDirectionMetrics(font, output);
        printCharMetrics(font, metrics, output);
        output.println("EndFontMetrics");
    }
    
    private static void printDirectionMetrics(final CFFFont font, final DataOutput output) throws IOException {
        output.println("UnderlinePosition " + font.getProperty("UnderlinePosition"));
        output.println("UnderlineThickness " + font.getProperty("UnderlineThickness"));
        output.println("ItalicAngle " + font.getProperty("ItalicAngle"));
        output.println("IsFixedPitch " + font.getProperty("isFixedPitch"));
    }
    
    private static void printCharMetrics(final CFFFont font, final List<CharMetric> metrics, final DataOutput output) throws IOException {
        output.println("StartCharMetrics " + metrics.size());
        Collections.sort(metrics);
        for (final CharMetric metric : metrics) {
            output.print("C " + metric.code + " ;");
            output.print(" ");
            output.print("WX " + metric.width + " ;");
            output.print(" ");
            output.print("N " + metric.name + " ;");
            output.print(" ");
            output.print("B " + (int)metric.bounds.getX() + " " + (int)metric.bounds.getY() + " " + (int)metric.bounds.getMaxX() + " " + (int)metric.bounds.getMaxY() + " ;");
            output.println();
        }
        output.println("EndCharMetrics");
    }
    
    private static List<CharMetric> renderFont(final CFFFont font) throws IOException {
        final List<CharMetric> metrics = new ArrayList<CharMetric>();
        final CharStringRenderer renderer = font.createRenderer();
        final Collection<CFFFont.Mapping> mappings = font.getMappings();
        for (final CFFFont.Mapping mapping : mappings) {
            final CharMetric metric = new CharMetric();
            metric.code = mapping.getCode();
            metric.name = mapping.getName();
            renderer.render(mapping.toType1Sequence());
            metric.width = renderer.getWidth();
            metric.bounds = renderer.getBounds();
            metrics.add(metric);
        }
        return metrics;
    }
    
    private static Rectangle2D getBounds(final List<CharMetric> metrics) {
        Rectangle2D bounds = null;
        for (final CharMetric metric : metrics) {
            if (bounds == null) {
                bounds = new Rectangle2D.Double();
                bounds.setFrame(metric.bounds);
            }
            else {
                Rectangle2D.union(bounds, metric.bounds, bounds);
            }
        }
        return bounds;
    }
    
    private static class CharMetric implements Comparable<CharMetric>
    {
        private int code;
        private String name;
        private int width;
        private Rectangle2D bounds;
        
        public int compareTo(final CharMetric that) {
            return this.code - that.code;
        }
    }
}
