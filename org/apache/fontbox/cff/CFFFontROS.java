// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cff;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;

public class CFFFontROS extends CFFFont
{
    private String registry;
    private String ordering;
    private int supplement;
    private List<Map<String, Object>> fontDictionaries;
    private List<Map<String, Object>> privateDictionaries;
    private CIDKeyedFDSelect fdSelect;
    
    public CFFFontROS() {
        this.fontDictionaries = new LinkedList<Map<String, Object>>();
        this.privateDictionaries = new LinkedList<Map<String, Object>>();
        this.fdSelect = null;
    }
    
    public String getRegistry() {
        return this.registry;
    }
    
    public void setRegistry(final String registry) {
        this.registry = registry;
    }
    
    public String getOrdering() {
        return this.ordering;
    }
    
    public void setOrdering(final String ordering) {
        this.ordering = ordering;
    }
    
    public int getSupplement() {
        return this.supplement;
    }
    
    public void setSupplement(final int supplement) {
        this.supplement = supplement;
    }
    
    public List<Map<String, Object>> getFontDict() {
        return this.fontDictionaries;
    }
    
    public void setFontDict(final List<Map<String, Object>> fontDict) {
        this.fontDictionaries = fontDict;
    }
    
    public List<Map<String, Object>> getPrivDict() {
        return this.privateDictionaries;
    }
    
    public void setPrivDict(final List<Map<String, Object>> privDict) {
        this.privateDictionaries = privDict;
    }
    
    public CIDKeyedFDSelect getFdSelect() {
        return this.fdSelect;
    }
    
    public void setFdSelect(final CIDKeyedFDSelect fdSelect) {
        this.fdSelect = fdSelect;
    }
    
    @Override
    public int getWidth(final int CID) throws IOException {
        final int fdArrayIndex = this.fdSelect.getFd(CID);
        if (fdArrayIndex == -1 && CID == 0) {
            return super.getWidth(CID);
        }
        if (fdArrayIndex == -1) {
            return 1000;
        }
        final Map<String, Object> fontDict = this.fontDictionaries.get(fdArrayIndex);
        final Map<String, Object> privDict = this.privateDictionaries.get(fdArrayIndex);
        final int nominalWidth = privDict.containsKey("nominalWidthX") ? privDict.get("nominalWidthX").intValue() : 0;
        final int defaultWidth = privDict.containsKey("defaultWidthX") ? privDict.get("defaultWidthX").intValue() : 1000;
        for (final Mapping m : this.getMappings()) {
            if (m.getSID() == CID) {
                CharStringRenderer csr = null;
                final Number charStringType = (Number)this.getProperty("CharstringType");
                if (charStringType.intValue() == 2) {
                    final List<Object> lSeq = m.toType2Sequence();
                    csr = new CharStringRenderer(false);
                    csr.render(lSeq);
                }
                else {
                    final List<Object> lSeq = m.toType1Sequence();
                    csr = new CharStringRenderer();
                    csr.render(lSeq);
                }
                return (csr.getWidth() != 0) ? (csr.getWidth() + nominalWidth) : defaultWidth;
            }
        }
        return this.getNotDefWidth(defaultWidth, nominalWidth);
    }
}
