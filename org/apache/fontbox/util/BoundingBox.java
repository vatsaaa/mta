// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.util;

import java.awt.Point;

public class BoundingBox
{
    private float lowerLeftX;
    private float lowerLeftY;
    private float upperRightX;
    private float upperRightY;
    
    public BoundingBox() {
    }
    
    public BoundingBox(final float minX, final float minY, final float maxX, final float maxY) {
        this.lowerLeftX = minX;
        this.lowerLeftY = minY;
        this.upperRightX = maxX;
        this.upperRightY = maxY;
    }
    
    public float getLowerLeftX() {
        return this.lowerLeftX;
    }
    
    public void setLowerLeftX(final float lowerLeftXValue) {
        this.lowerLeftX = lowerLeftXValue;
    }
    
    public float getLowerLeftY() {
        return this.lowerLeftY;
    }
    
    public void setLowerLeftY(final float lowerLeftYValue) {
        this.lowerLeftY = lowerLeftYValue;
    }
    
    public float getUpperRightX() {
        return this.upperRightX;
    }
    
    public void setUpperRightX(final float upperRightXValue) {
        this.upperRightX = upperRightXValue;
    }
    
    public float getUpperRightY() {
        return this.upperRightY;
    }
    
    public void setUpperRightY(final float upperRightYValue) {
        this.upperRightY = upperRightYValue;
    }
    
    public float getWidth() {
        return this.getUpperRightX() - this.getLowerLeftX();
    }
    
    public float getHeight() {
        return this.getUpperRightY() - this.getLowerLeftY();
    }
    
    public boolean contains(final float x, final float y) {
        return x >= this.lowerLeftX && x <= this.upperRightX && y >= this.lowerLeftY && y <= this.upperRightY;
    }
    
    public boolean contains(final Point point) {
        return this.contains((float)point.getX(), (float)point.getY());
    }
    
    @Override
    public String toString() {
        return "[" + this.getLowerLeftX() + "," + this.getLowerLeftY() + "," + this.getUpperRightX() + "," + this.getUpperRightY() + "]";
    }
}
