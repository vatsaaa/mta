// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.afm;

public class KernPair
{
    private String firstKernCharacter;
    private String secondKernCharacter;
    private float x;
    private float y;
    
    public String getFirstKernCharacter() {
        return this.firstKernCharacter;
    }
    
    public void setFirstKernCharacter(final String firstKernCharacterValue) {
        this.firstKernCharacter = firstKernCharacterValue;
    }
    
    public String getSecondKernCharacter() {
        return this.secondKernCharacter;
    }
    
    public void setSecondKernCharacter(final String secondKernCharacterValue) {
        this.secondKernCharacter = secondKernCharacterValue;
    }
    
    public float getX() {
        return this.x;
    }
    
    public void setX(final float xValue) {
        this.x = xValue;
    }
    
    public float getY() {
        return this.y;
    }
    
    public void setY(final float yValue) {
        this.y = yValue;
    }
}
