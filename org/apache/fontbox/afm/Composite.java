// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.afm;

import java.util.ArrayList;
import java.util.List;

public class Composite
{
    private String name;
    private List<CompositePart> parts;
    
    public Composite() {
        this.parts = new ArrayList<CompositePart>();
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String nameValue) {
        this.name = nameValue;
    }
    
    public void addPart(final CompositePart part) {
        this.parts.add(part);
    }
    
    public List<CompositePart> getParts() {
        return this.parts;
    }
    
    public void setParts(final List<CompositePart> partsList) {
        this.parts = partsList;
    }
}
