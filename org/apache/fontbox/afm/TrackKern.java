// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.afm;

public class TrackKern
{
    private int degree;
    private float minPointSize;
    private float minKern;
    private float maxPointSize;
    private float maxKern;
    
    public int getDegree() {
        return this.degree;
    }
    
    public void setDegree(final int degreeValue) {
        this.degree = degreeValue;
    }
    
    public float getMaxKern() {
        return this.maxKern;
    }
    
    public void setMaxKern(final float maxKernValue) {
        this.maxKern = maxKernValue;
    }
    
    public float getMaxPointSize() {
        return this.maxPointSize;
    }
    
    public void setMaxPointSize(final float maxPointSizeValue) {
        this.maxPointSize = maxPointSizeValue;
    }
    
    public float getMinKern() {
        return this.minKern;
    }
    
    public void setMinKern(final float minKernValue) {
        this.minKern = minKernValue;
    }
    
    public float getMinPointSize() {
        return this.minPointSize;
    }
    
    public void setMinPointSize(final float minPointSizeValue) {
        this.minPointSize = minPointSizeValue;
    }
}
