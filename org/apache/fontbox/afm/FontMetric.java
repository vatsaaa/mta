// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.afm;

import java.util.Iterator;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import org.apache.fontbox.util.BoundingBox;

public class FontMetric
{
    private float afmVersion;
    private int metricSets;
    private String fontName;
    private String fullName;
    private String familyName;
    private String weight;
    private BoundingBox fontBBox;
    private String fontVersion;
    private String notice;
    private String encodingScheme;
    private int mappingScheme;
    private int escChar;
    private String characterSet;
    private int characters;
    private boolean isBaseFont;
    private float[] vVector;
    private boolean isFixedV;
    private float capHeight;
    private float xHeight;
    private float ascender;
    private float descender;
    private List<String> comments;
    private float underlinePosition;
    private float underlineThickness;
    private float italicAngle;
    private float[] charWidth;
    private boolean isFixedPitch;
    private float standardHorizontalWidth;
    private float standardVerticalWidth;
    private List<CharMetric> charMetrics;
    private Map<String, CharMetric> charMetricsMap;
    private List<TrackKern> trackKern;
    private List<Composite> composites;
    private List<KernPair> kernPairs;
    private List<KernPair> kernPairs0;
    private List<KernPair> kernPairs1;
    
    public FontMetric() {
        this.metricSets = 0;
        this.comments = new ArrayList<String>();
        this.charMetrics = new ArrayList<CharMetric>();
        this.charMetricsMap = new HashMap<String, CharMetric>();
        this.trackKern = new ArrayList<TrackKern>();
        this.composites = new ArrayList<Composite>();
        this.kernPairs = new ArrayList<KernPair>();
        this.kernPairs0 = new ArrayList<KernPair>();
        this.kernPairs1 = new ArrayList<KernPair>();
    }
    
    public float getCharacterWidth(final String name) throws IOException {
        float result = 0.0f;
        final CharMetric metric = this.charMetricsMap.get(name);
        if (metric == null) {
            result = 0.0f;
        }
        else {
            result = metric.getWx();
        }
        return result;
    }
    
    public float getCharacterHeight(final String name) throws IOException {
        float result = 0.0f;
        final CharMetric metric = this.charMetricsMap.get(name);
        if (metric == null) {
            result = 0.0f;
        }
        else if (metric.getWy() == 0.0f) {
            result = metric.getBoundingBox().getHeight();
        }
        else {
            result = metric.getWy();
        }
        return result;
    }
    
    public float getAverageCharacterWidth() throws IOException {
        float average = 0.0f;
        float totalWidths = 0.0f;
        float characterCount = 0.0f;
        for (final CharMetric metric : this.charMetricsMap.values()) {
            if (metric.getWx() > 0.0f) {
                totalWidths += metric.getWx();
                ++characterCount;
            }
        }
        if (totalWidths > 0.0f) {
            average = totalWidths / characterCount;
        }
        return average;
    }
    
    public void addComment(final String comment) {
        this.comments.add(comment);
    }
    
    public List<String> getComments() {
        return this.comments;
    }
    
    public float getAFMVersion() {
        return this.afmVersion;
    }
    
    public int getMetricSets() {
        return this.metricSets;
    }
    
    public void setAFMVersion(final float afmVersionValue) {
        this.afmVersion = afmVersionValue;
    }
    
    public void setMetricSets(final int metricSetsValue) {
        if (metricSetsValue < 0 || metricSetsValue > 2) {
            throw new RuntimeException("The metricSets attribute must be in the set {0,1,2} and not '" + metricSetsValue + "'");
        }
        this.metricSets = metricSetsValue;
    }
    
    public String getFontName() {
        return this.fontName;
    }
    
    public void setFontName(final String name) {
        this.fontName = name;
    }
    
    public String getFullName() {
        return this.fullName;
    }
    
    public void setFullName(final String fullNameValue) {
        this.fullName = fullNameValue;
    }
    
    public String getFamilyName() {
        return this.familyName;
    }
    
    public void setFamilyName(final String familyNameValue) {
        this.familyName = familyNameValue;
    }
    
    public String getWeight() {
        return this.weight;
    }
    
    public void setWeight(final String weightValue) {
        this.weight = weightValue;
    }
    
    public BoundingBox getFontBBox() {
        return this.fontBBox;
    }
    
    public void setFontBBox(final BoundingBox bBox) {
        this.fontBBox = bBox;
    }
    
    public String getNotice() {
        return this.notice;
    }
    
    public void setNotice(final String noticeValue) {
        this.notice = noticeValue;
    }
    
    public String getEncodingScheme() {
        return this.encodingScheme;
    }
    
    public void setEncodingScheme(final String encodingSchemeValue) {
        this.encodingScheme = encodingSchemeValue;
    }
    
    public int getMappingScheme() {
        return this.mappingScheme;
    }
    
    public void setMappingScheme(final int mappingSchemeValue) {
        this.mappingScheme = mappingSchemeValue;
    }
    
    public int getEscChar() {
        return this.escChar;
    }
    
    public void setEscChar(final int escCharValue) {
        this.escChar = escCharValue;
    }
    
    public String getCharacterSet() {
        return this.characterSet;
    }
    
    public void setCharacterSet(final String characterSetValue) {
        this.characterSet = characterSetValue;
    }
    
    public int getCharacters() {
        return this.characters;
    }
    
    public void setCharacters(final int charactersValue) {
        this.characters = charactersValue;
    }
    
    public boolean isBaseFont() {
        return this.isBaseFont;
    }
    
    public void setIsBaseFont(final boolean isBaseFontValue) {
        this.isBaseFont = isBaseFontValue;
    }
    
    public float[] getVVector() {
        return this.vVector;
    }
    
    public void setVVector(final float[] vVectorValue) {
        this.vVector = vVectorValue;
    }
    
    public boolean isFixedV() {
        return this.isFixedV;
    }
    
    public void setIsFixedV(final boolean isFixedVValue) {
        this.isFixedV = isFixedVValue;
    }
    
    public float getCapHeight() {
        return this.capHeight;
    }
    
    public void setCapHeight(final float capHeightValue) {
        this.capHeight = capHeightValue;
    }
    
    public float getXHeight() {
        return this.xHeight;
    }
    
    public void setXHeight(final float xHeightValue) {
        this.xHeight = xHeightValue;
    }
    
    public float getAscender() {
        return this.ascender;
    }
    
    public void setAscender(final float ascenderValue) {
        this.ascender = ascenderValue;
    }
    
    public float getDescender() {
        return this.descender;
    }
    
    public void setDescender(final float descenderValue) {
        this.descender = descenderValue;
    }
    
    public String getFontVersion() {
        return this.fontVersion;
    }
    
    public void setFontVersion(final String fontVersionValue) {
        this.fontVersion = fontVersionValue;
    }
    
    public float getUnderlinePosition() {
        return this.underlinePosition;
    }
    
    public void setUnderlinePosition(final float underlinePositionValue) {
        this.underlinePosition = underlinePositionValue;
    }
    
    public float getUnderlineThickness() {
        return this.underlineThickness;
    }
    
    public void setUnderlineThickness(final float underlineThicknessValue) {
        this.underlineThickness = underlineThicknessValue;
    }
    
    public float getItalicAngle() {
        return this.italicAngle;
    }
    
    public void setItalicAngle(final float italicAngleValue) {
        this.italicAngle = italicAngleValue;
    }
    
    public float[] getCharWidth() {
        return this.charWidth;
    }
    
    public void setCharWidth(final float[] charWidthValue) {
        this.charWidth = charWidthValue;
    }
    
    public boolean isFixedPitch() {
        return this.isFixedPitch;
    }
    
    public void setFixedPitch(final boolean isFixedPitchValue) {
        this.isFixedPitch = isFixedPitchValue;
    }
    
    public List<CharMetric> getCharMetrics() {
        return this.charMetrics;
    }
    
    public void setCharMetrics(final List<CharMetric> charMetricsValue) {
        this.charMetrics = charMetricsValue;
    }
    
    public void addCharMetric(final CharMetric metric) {
        this.charMetrics.add(metric);
        this.charMetricsMap.put(metric.getName(), metric);
    }
    
    public List<TrackKern> getTrackKern() {
        return this.trackKern;
    }
    
    public void setTrackKern(final List<TrackKern> trackKernValue) {
        this.trackKern = trackKernValue;
    }
    
    public void addTrackKern(final TrackKern kern) {
        this.trackKern.add(kern);
    }
    
    public List<Composite> getComposites() {
        return this.composites;
    }
    
    public void setComposites(final List<Composite> compositesList) {
        this.composites = compositesList;
    }
    
    public void addComposite(final Composite composite) {
        this.composites.add(composite);
    }
    
    public List<KernPair> getKernPairs() {
        return this.kernPairs;
    }
    
    public void addKernPair(final KernPair kernPair) {
        this.kernPairs.add(kernPair);
    }
    
    public void setKernPairs(final List<KernPair> kernPairsList) {
        this.kernPairs = kernPairsList;
    }
    
    public List<KernPair> getKernPairs0() {
        return this.kernPairs0;
    }
    
    public void addKernPair0(final KernPair kernPair) {
        this.kernPairs0.add(kernPair);
    }
    
    public void setKernPairs0(final List<KernPair> kernPairs0List) {
        this.kernPairs0 = kernPairs0List;
    }
    
    public List<KernPair> getKernPairs1() {
        return this.kernPairs1;
    }
    
    public void addKernPair1(final KernPair kernPair) {
        this.kernPairs1.add(kernPair);
    }
    
    public void setKernPairs1(final List<KernPair> kernPairs1List) {
        this.kernPairs1 = kernPairs1List;
    }
    
    public float getStandardHorizontalWidth() {
        return this.standardHorizontalWidth;
    }
    
    public void setStandardHorizontalWidth(final float standardHorizontalWidthValue) {
        this.standardHorizontalWidth = standardHorizontalWidthValue;
    }
    
    public float getStandardVerticalWidth() {
        return this.standardVerticalWidth;
    }
    
    public void setStandardVerticalWidth(final float standardVerticalWidthValue) {
        this.standardVerticalWidth = standardVerticalWidthValue;
    }
}
