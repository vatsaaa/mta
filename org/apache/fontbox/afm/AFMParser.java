// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.afm;

import java.util.StringTokenizer;
import org.apache.fontbox.util.BoundingBox;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;
import java.io.InputStream;

public class AFMParser
{
    public static final String COMMENT = "Comment";
    public static final String START_FONT_METRICS = "StartFontMetrics";
    public static final String END_FONT_METRICS = "EndFontMetrics";
    public static final String FONT_NAME = "FontName";
    public static final String FULL_NAME = "FullName";
    public static final String FAMILY_NAME = "FamilyName";
    public static final String WEIGHT = "Weight";
    public static final String FONT_BBOX = "FontBBox";
    public static final String VERSION = "Version";
    public static final String NOTICE = "Notice";
    public static final String ENCODING_SCHEME = "EncodingScheme";
    public static final String MAPPING_SCHEME = "MappingScheme";
    public static final String ESC_CHAR = "EscChar";
    public static final String CHARACTER_SET = "CharacterSet";
    public static final String CHARACTERS = "Characters";
    public static final String IS_BASE_FONT = "IsBaseFont";
    public static final String V_VECTOR = "VVector";
    public static final String IS_FIXED_V = "IsFixedV";
    public static final String CAP_HEIGHT = "CapHeight";
    public static final String X_HEIGHT = "XHeight";
    public static final String ASCENDER = "Ascender";
    public static final String DESCENDER = "Descender";
    public static final String UNDERLINE_POSITION = "UnderlinePosition";
    public static final String UNDERLINE_THICKNESS = "UnderlineThickness";
    public static final String ITALIC_ANGLE = "ItalicAngle";
    public static final String CHAR_WIDTH = "CharWidth";
    public static final String IS_FIXED_PITCH = "IsFixedPitch";
    public static final String START_CHAR_METRICS = "StartCharMetrics";
    public static final String END_CHAR_METRICS = "EndCharMetrics";
    public static final String CHARMETRICS_C = "C";
    public static final String CHARMETRICS_CH = "CH";
    public static final String CHARMETRICS_WX = "WX";
    public static final String CHARMETRICS_W0X = "W0X";
    public static final String CHARMETRICS_W1X = "W1X";
    public static final String CHARMETRICS_WY = "WY";
    public static final String CHARMETRICS_W0Y = "W0Y";
    public static final String CHARMETRICS_W1Y = "W1Y";
    public static final String CHARMETRICS_W = "W";
    public static final String CHARMETRICS_W0 = "W0";
    public static final String CHARMETRICS_W1 = "W1";
    public static final String CHARMETRICS_VV = "VV";
    public static final String CHARMETRICS_N = "N";
    public static final String CHARMETRICS_B = "B";
    public static final String CHARMETRICS_L = "L";
    public static final String STD_HW = "StdHW";
    public static final String STD_VW = "StdVW";
    public static final String START_TRACK_KERN = "StartTrackKern";
    public static final String END_TRACK_KERN = "EndTrackKern";
    public static final String START_KERN_DATA = "StartKernData";
    public static final String END_KERN_DATA = "EndKernData";
    public static final String START_KERN_PAIRS = "StartKernPairs";
    public static final String END_KERN_PAIRS = "EndKernPairs";
    public static final String START_KERN_PAIRS0 = "StartKernPairs0";
    public static final String START_KERN_PAIRS1 = "StartKernPairs1";
    public static final String START_COMPOSITES = "StartComposites";
    public static final String END_COMPOSITES = "EndComposites";
    public static final String CC = "CC";
    public static final String PCC = "PCC";
    public static final String KERN_PAIR_KP = "KP";
    public static final String KERN_PAIR_KPH = "KPH";
    public static final String KERN_PAIR_KPX = "KPX";
    public static final String KERN_PAIR_KPY = "KPY";
    private static final int BITS_IN_HEX = 16;
    private InputStream input;
    private FontMetric result;
    
    public static void main(final String[] args) throws IOException {
        final File afmDir = new File("Resources/afm");
        final File[] files = afmDir.listFiles();
        for (int i = 0; i < files.length; ++i) {
            if (files[i].getPath().toUpperCase().endsWith(".AFM")) {
                final long start = System.currentTimeMillis();
                final FileInputStream input = new FileInputStream(files[i]);
                final AFMParser parser = new AFMParser(input);
                parser.parse();
                final long stop = System.currentTimeMillis();
                System.out.println("Parsing:" + files[i].getPath() + " " + (stop - start));
            }
        }
    }
    
    public AFMParser(final InputStream in) {
        this.input = in;
    }
    
    public void parse() throws IOException {
        this.result = this.parseFontMetric();
    }
    
    public FontMetric getResult() {
        return this.result;
    }
    
    private FontMetric parseFontMetric() throws IOException {
        final FontMetric fontMetrics = new FontMetric();
        final String startFontMetrics = this.readString();
        if (!"StartFontMetrics".equals(startFontMetrics)) {
            throw new IOException("Error: The AFM file should start with StartFontMetrics and not '" + startFontMetrics + "'");
        }
        fontMetrics.setAFMVersion(this.readFloat());
        String nextCommand = null;
        while (!"EndFontMetrics".equals(nextCommand = this.readString())) {
            if ("FontName".equals(nextCommand)) {
                fontMetrics.setFontName(this.readLine());
            }
            else if ("FullName".equals(nextCommand)) {
                fontMetrics.setFullName(this.readLine());
            }
            else if ("FamilyName".equals(nextCommand)) {
                fontMetrics.setFamilyName(this.readLine());
            }
            else if ("Weight".equals(nextCommand)) {
                fontMetrics.setWeight(this.readLine());
            }
            else if ("FontBBox".equals(nextCommand)) {
                final BoundingBox bBox = new BoundingBox();
                bBox.setLowerLeftX(this.readFloat());
                bBox.setLowerLeftY(this.readFloat());
                bBox.setUpperRightX(this.readFloat());
                bBox.setUpperRightY(this.readFloat());
                fontMetrics.setFontBBox(bBox);
            }
            else if ("Version".equals(nextCommand)) {
                fontMetrics.setFontVersion(this.readLine());
            }
            else if ("Notice".equals(nextCommand)) {
                fontMetrics.setNotice(this.readLine());
            }
            else if ("EncodingScheme".equals(nextCommand)) {
                fontMetrics.setEncodingScheme(this.readLine());
            }
            else if ("MappingScheme".equals(nextCommand)) {
                fontMetrics.setMappingScheme(this.readInt());
            }
            else if ("EscChar".equals(nextCommand)) {
                fontMetrics.setEscChar(this.readInt());
            }
            else if ("CharacterSet".equals(nextCommand)) {
                fontMetrics.setCharacterSet(this.readLine());
            }
            else if ("Characters".equals(nextCommand)) {
                fontMetrics.setCharacters(this.readInt());
            }
            else if ("IsBaseFont".equals(nextCommand)) {
                fontMetrics.setIsBaseFont(this.readBoolean());
            }
            else if ("VVector".equals(nextCommand)) {
                final float[] vector = { this.readFloat(), this.readFloat() };
                fontMetrics.setVVector(vector);
            }
            else if ("IsFixedV".equals(nextCommand)) {
                fontMetrics.setIsFixedV(this.readBoolean());
            }
            else if ("CapHeight".equals(nextCommand)) {
                fontMetrics.setCapHeight(this.readFloat());
            }
            else if ("XHeight".equals(nextCommand)) {
                fontMetrics.setXHeight(this.readFloat());
            }
            else if ("Ascender".equals(nextCommand)) {
                fontMetrics.setAscender(this.readFloat());
            }
            else if ("Descender".equals(nextCommand)) {
                fontMetrics.setDescender(this.readFloat());
            }
            else if ("StdHW".equals(nextCommand)) {
                fontMetrics.setStandardHorizontalWidth(this.readFloat());
            }
            else if ("StdVW".equals(nextCommand)) {
                fontMetrics.setStandardVerticalWidth(this.readFloat());
            }
            else if ("Comment".equals(nextCommand)) {
                fontMetrics.addComment(this.readLine());
            }
            else if ("UnderlinePosition".equals(nextCommand)) {
                fontMetrics.setUnderlinePosition(this.readFloat());
            }
            else if ("UnderlineThickness".equals(nextCommand)) {
                fontMetrics.setUnderlineThickness(this.readFloat());
            }
            else if ("ItalicAngle".equals(nextCommand)) {
                fontMetrics.setItalicAngle(this.readFloat());
            }
            else if ("CharWidth".equals(nextCommand)) {
                final float[] widths = { this.readFloat(), this.readFloat() };
                fontMetrics.setCharWidth(widths);
            }
            else if ("IsFixedPitch".equals(nextCommand)) {
                fontMetrics.setFixedPitch(this.readBoolean());
            }
            else if ("StartCharMetrics".equals(nextCommand)) {
                for (int count = this.readInt(), i = 0; i < count; ++i) {
                    final CharMetric charMetric = this.parseCharMetric();
                    fontMetrics.addCharMetric(charMetric);
                }
                final String end = this.readString();
                if (!end.equals("EndCharMetrics")) {
                    throw new IOException("Error: Expected 'EndCharMetrics' actual '" + end + "'");
                }
                continue;
            }
            else if ("StartComposites".equals(nextCommand)) {
                for (int count = this.readInt(), i = 0; i < count; ++i) {
                    final Composite part = this.parseComposite();
                    fontMetrics.addComposite(part);
                }
                final String end = this.readString();
                if (!end.equals("EndComposites")) {
                    throw new IOException("Error: Expected 'EndComposites' actual '" + end + "'");
                }
                continue;
            }
            else {
                if (!"StartKernData".equals(nextCommand)) {
                    throw new IOException("Unknown AFM key '" + nextCommand + "'");
                }
                this.parseKernData(fontMetrics);
            }
        }
        return fontMetrics;
    }
    
    private void parseKernData(final FontMetric fontMetrics) throws IOException {
        String nextCommand = null;
        while (!(nextCommand = this.readString()).equals("EndKernData")) {
            if ("StartTrackKern".equals(nextCommand)) {
                for (int count = this.readInt(), i = 0; i < count; ++i) {
                    final TrackKern kern = new TrackKern();
                    kern.setDegree(this.readInt());
                    kern.setMinPointSize(this.readFloat());
                    kern.setMinKern(this.readFloat());
                    kern.setMaxPointSize(this.readFloat());
                    kern.setMaxKern(this.readFloat());
                    fontMetrics.addTrackKern(kern);
                }
                final String end = this.readString();
                if (!end.equals("EndTrackKern")) {
                    throw new IOException("Error: Expected 'EndTrackKern' actual '" + end + "'");
                }
                continue;
            }
            else if ("StartKernPairs".equals(nextCommand)) {
                for (int count = this.readInt(), i = 0; i < count; ++i) {
                    final KernPair pair = this.parseKernPair();
                    fontMetrics.addKernPair(pair);
                }
                final String end = this.readString();
                if (!end.equals("EndKernPairs")) {
                    throw new IOException("Error: Expected 'EndKernPairs' actual '" + end + "'");
                }
                continue;
            }
            else if ("StartKernPairs0".equals(nextCommand)) {
                for (int count = this.readInt(), i = 0; i < count; ++i) {
                    final KernPair pair = this.parseKernPair();
                    fontMetrics.addKernPair0(pair);
                }
                final String end = this.readString();
                if (!end.equals("EndKernPairs")) {
                    throw new IOException("Error: Expected 'EndKernPairs' actual '" + end + "'");
                }
                continue;
            }
            else {
                if (!"StartKernPairs1".equals(nextCommand)) {
                    throw new IOException("Unknown kerning data type '" + nextCommand + "'");
                }
                for (int count = this.readInt(), i = 0; i < count; ++i) {
                    final KernPair pair = this.parseKernPair();
                    fontMetrics.addKernPair1(pair);
                }
                final String end = this.readString();
                if (!end.equals("EndKernPairs")) {
                    throw new IOException("Error: Expected 'EndKernPairs' actual '" + end + "'");
                }
                continue;
            }
        }
    }
    
    private KernPair parseKernPair() throws IOException {
        final KernPair kernPair = new KernPair();
        final String cmd = this.readString();
        if ("KP".equals(cmd)) {
            final String first = this.readString();
            final String second = this.readString();
            final float x = this.readFloat();
            final float y = this.readFloat();
            kernPair.setFirstKernCharacter(first);
            kernPair.setSecondKernCharacter(second);
            kernPair.setX(x);
            kernPair.setY(y);
        }
        else if ("KPH".equals(cmd)) {
            final String first = this.hexToString(this.readString());
            final String second = this.hexToString(this.readString());
            final float x = this.readFloat();
            final float y = this.readFloat();
            kernPair.setFirstKernCharacter(first);
            kernPair.setSecondKernCharacter(second);
            kernPair.setX(x);
            kernPair.setY(y);
        }
        else if ("KPX".equals(cmd)) {
            final String first = this.readString();
            final String second = this.readString();
            final float x = this.readFloat();
            kernPair.setFirstKernCharacter(first);
            kernPair.setSecondKernCharacter(second);
            kernPair.setX(x);
            kernPair.setY(0.0f);
        }
        else {
            if (!"KPY".equals(cmd)) {
                throw new IOException("Error expected kern pair command actual='" + cmd + "'");
            }
            final String first = this.readString();
            final String second = this.readString();
            final float y2 = this.readFloat();
            kernPair.setFirstKernCharacter(first);
            kernPair.setSecondKernCharacter(second);
            kernPair.setX(0.0f);
            kernPair.setY(y2);
        }
        return kernPair;
    }
    
    private String hexToString(String hexString) throws IOException {
        if (hexString.length() < 2) {
            throw new IOException("Error: Expected hex string of length >= 2 not='" + hexString);
        }
        if (hexString.charAt(0) != '<' || hexString.charAt(hexString.length() - 1) != '>') {
            throw new IOException("String should be enclosed by angle brackets '" + hexString + "'");
        }
        hexString = hexString.substring(1, hexString.length() - 1);
        final byte[] data = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i += 2) {
            final String hex = "" + hexString.charAt(i) + hexString.charAt(i + 1);
            try {
                data[i / 2] = (byte)Integer.parseInt(hex, 16);
            }
            catch (NumberFormatException e) {
                throw new IOException("Error parsing AFM file:" + e);
            }
        }
        return new String(data, "ISO-8859-1");
    }
    
    private Composite parseComposite() throws IOException {
        final Composite composite = new Composite();
        final String partData = this.readLine();
        final StringTokenizer tokenizer = new StringTokenizer(partData, " ;");
        final String cc = tokenizer.nextToken();
        if (!cc.equals("CC")) {
            throw new IOException("Expected 'CC' actual='" + cc + "'");
        }
        final String name = tokenizer.nextToken();
        composite.setName(name);
        int partCount;
        try {
            partCount = Integer.parseInt(tokenizer.nextToken());
        }
        catch (NumberFormatException e) {
            throw new IOException("Error parsing AFM document:" + e);
        }
        for (int i = 0; i < partCount; ++i) {
            final CompositePart part = new CompositePart();
            final String pcc = tokenizer.nextToken();
            if (!pcc.equals("PCC")) {
                throw new IOException("Expected 'PCC' actual='" + pcc + "'");
            }
            final String partName = tokenizer.nextToken();
            try {
                final int x = Integer.parseInt(tokenizer.nextToken());
                final int y = Integer.parseInt(tokenizer.nextToken());
                part.setName(partName);
                part.setXDisplacement(x);
                part.setYDisplacement(y);
                composite.addPart(part);
            }
            catch (NumberFormatException e2) {
                throw new IOException("Error parsing AFM document:" + e2);
            }
        }
        return composite;
    }
    
    private CharMetric parseCharMetric() throws IOException {
        final CharMetric charMetric = new CharMetric();
        final String metrics = this.readLine();
        final StringTokenizer metricsTokenizer = new StringTokenizer(metrics);
        try {
            while (metricsTokenizer.hasMoreTokens()) {
                final String nextCommand = metricsTokenizer.nextToken();
                if (nextCommand.equals("C")) {
                    final String charCode = metricsTokenizer.nextToken();
                    charMetric.setCharacterCode(Integer.parseInt(charCode));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("CH")) {
                    final String charCode = metricsTokenizer.nextToken();
                    charMetric.setCharacterCode(Integer.parseInt(charCode, 16));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("WX")) {
                    final String wx = metricsTokenizer.nextToken();
                    charMetric.setWx(Float.parseFloat(wx));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("W0X")) {
                    final String w0x = metricsTokenizer.nextToken();
                    charMetric.setW0x(Float.parseFloat(w0x));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("W1X")) {
                    final String w1x = metricsTokenizer.nextToken();
                    charMetric.setW0x(Float.parseFloat(w1x));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("WY")) {
                    final String wy = metricsTokenizer.nextToken();
                    charMetric.setWy(Float.parseFloat(wy));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("W0Y")) {
                    final String w0y = metricsTokenizer.nextToken();
                    charMetric.setW0y(Float.parseFloat(w0y));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("W1Y")) {
                    final String w1y = metricsTokenizer.nextToken();
                    charMetric.setW0y(Float.parseFloat(w1y));
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("W")) {
                    final String w0 = metricsTokenizer.nextToken();
                    final String w2 = metricsTokenizer.nextToken();
                    final float[] w3 = { Float.parseFloat(w0), Float.parseFloat(w2) };
                    charMetric.setW(w3);
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("W0")) {
                    final String w4 = metricsTokenizer.nextToken();
                    final String w5 = metricsTokenizer.nextToken();
                    final float[] w6 = { Float.parseFloat(w4), Float.parseFloat(w5) };
                    charMetric.setW0(w6);
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("W1")) {
                    final String w7 = metricsTokenizer.nextToken();
                    final String w8 = metricsTokenizer.nextToken();
                    final float[] w9 = { Float.parseFloat(w7), Float.parseFloat(w8) };
                    charMetric.setW1(w9);
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("VV")) {
                    final String vv0 = metricsTokenizer.nextToken();
                    final String vv2 = metricsTokenizer.nextToken();
                    final float[] vv3 = { Float.parseFloat(vv0), Float.parseFloat(vv2) };
                    charMetric.setVv(vv3);
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("N")) {
                    final String name = metricsTokenizer.nextToken();
                    charMetric.setName(name);
                    this.verifySemicolon(metricsTokenizer);
                }
                else if (nextCommand.equals("B")) {
                    final String llx = metricsTokenizer.nextToken();
                    final String lly = metricsTokenizer.nextToken();
                    final String urx = metricsTokenizer.nextToken();
                    final String ury = metricsTokenizer.nextToken();
                    final BoundingBox box = new BoundingBox();
                    box.setLowerLeftX(Float.parseFloat(llx));
                    box.setLowerLeftY(Float.parseFloat(lly));
                    box.setUpperRightX(Float.parseFloat(urx));
                    box.setUpperRightY(Float.parseFloat(ury));
                    charMetric.setBoundingBox(box);
                    this.verifySemicolon(metricsTokenizer);
                }
                else {
                    if (!nextCommand.equals("L")) {
                        throw new IOException("Unknown CharMetrics command '" + nextCommand + "'");
                    }
                    final String successor = metricsTokenizer.nextToken();
                    final String ligature = metricsTokenizer.nextToken();
                    final Ligature lig = new Ligature();
                    lig.setSuccessor(successor);
                    lig.setLigature(ligature);
                    charMetric.addLigature(lig);
                    this.verifySemicolon(metricsTokenizer);
                }
            }
        }
        catch (NumberFormatException e) {
            throw new IOException("Error: Corrupt AFM document:" + e);
        }
        return charMetric;
    }
    
    private void verifySemicolon(final StringTokenizer tokenizer) throws IOException {
        if (!tokenizer.hasMoreTokens()) {
            throw new IOException("CharMetrics is missing a semicolon after a command");
        }
        final String semicolon = tokenizer.nextToken();
        if (!semicolon.equals(";")) {
            throw new IOException("Error: Expected semicolon in stream actual='" + semicolon + "'");
        }
    }
    
    private boolean readBoolean() throws IOException {
        final String theBoolean = this.readString();
        return Boolean.valueOf(theBoolean);
    }
    
    private int readInt() throws IOException {
        final String theInt = this.readString();
        try {
            return Integer.parseInt(theInt);
        }
        catch (NumberFormatException e) {
            throw new IOException("Error parsing AFM document:" + e);
        }
    }
    
    private float readFloat() throws IOException {
        final String theFloat = this.readString();
        return Float.parseFloat(theFloat);
    }
    
    private String readLine() throws IOException {
        final StringBuffer buf = new StringBuffer();
        int nextByte;
        for (nextByte = this.input.read(); this.isWhitespace(nextByte); nextByte = this.input.read()) {}
        buf.append((char)nextByte);
        while (!this.isEOL(nextByte = this.input.read())) {
            buf.append((char)nextByte);
        }
        return buf.toString();
    }
    
    private String readString() throws IOException {
        final StringBuffer buf = new StringBuffer();
        int nextByte;
        for (nextByte = this.input.read(); this.isWhitespace(nextByte); nextByte = this.input.read()) {}
        buf.append((char)nextByte);
        while (!this.isWhitespace(nextByte = this.input.read())) {
            buf.append((char)nextByte);
        }
        return buf.toString();
    }
    
    private boolean isEOL(final int character) {
        return character == 13 || character == 10;
    }
    
    private boolean isWhitespace(final int character) {
        return character == 32 || character == 9 || character == 13 || character == 10;
    }
}
