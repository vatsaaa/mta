// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cmap;

import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import org.apache.fontbox.util.ResourceLoader;
import java.io.PushbackInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;

public class CMapParser
{
    private static final String BEGIN_CODESPACE_RANGE = "begincodespacerange";
    private static final String BEGIN_BASE_FONT_CHAR = "beginbfchar";
    private static final String BEGIN_BASE_FONT_RANGE = "beginbfrange";
    private static final String BEGIN_CID_CHAR = "begincidchar";
    private static final String BEGIN_CID_RANGE = "begincidrange";
    private static final String USECMAP = "usecmap";
    private static final String END_CODESPACE_RANGE = "endcodespacerange";
    private static final String END_BASE_FONT_CHAR = "endbfchar";
    private static final String END_BASE_FONT_RANGE = "endbfrange";
    private static final String END_CID_CHAR = "endcidchar";
    private static final String END_CID_RANGE = "endcidrange";
    private static final String WMODE = "WMode";
    private static final String CMAP_NAME = "CMapName";
    private static final String CMAP_VERSION = "CMapVersion";
    private static final String CMAP_TYPE = "CMapType";
    private static final String REGISTRY = "Registry";
    private static final String ORDERING = "Ordering";
    private static final String SUPPLEMENT = "Supplement";
    private static final String MARK_END_OF_DICTIONARY = ">>";
    private static final String MARK_END_OF_ARRAY = "]";
    private byte[] tokenParserByteBuffer;
    
    public CMapParser() {
        this.tokenParserByteBuffer = new byte[512];
    }
    
    public CMap parse(final File file) throws IOException {
        final String rootDir = file.getParent() + File.separator;
        FileInputStream input = null;
        try {
            input = new FileInputStream(file);
            return this.parse(rootDir, input);
        }
        finally {
            if (input != null) {
                input.close();
            }
        }
    }
    
    public CMap parse(final String resourceRoot, final InputStream input) throws IOException {
        final PushbackInputStream cmapStream = new PushbackInputStream(input);
        final CMap result = new CMap();
        Object previousToken = null;
        Object token = null;
        while ((token = this.parseNextToken(cmapStream)) != null) {
            if (token instanceof Operator) {
                final Operator op = (Operator)token;
                if (op.op.equals("usecmap")) {
                    final LiteralName useCmapName = (LiteralName)previousToken;
                    final InputStream useStream = ResourceLoader.loadResource(resourceRoot + useCmapName.name);
                    if (useStream == null) {
                        throw new IOException("Error: Could not find referenced cmap stream " + useCmapName.name);
                    }
                    final CMap useCMap = this.parse(resourceRoot, useStream);
                    result.useCmap(useCMap);
                }
                else if (op.op.equals("begincodespacerange")) {
                    final Number cosCount = (Number)previousToken;
                    int j = 0;
                    while (j < cosCount.intValue()) {
                        final Object nextToken = this.parseNextToken(cmapStream);
                        if (nextToken instanceof Operator) {
                            if (!((Operator)nextToken).op.equals("endcodespacerange")) {
                                throw new IOException("Error : ~codespacerange contains an unexpected operator : " + ((Operator)nextToken).op);
                            }
                            break;
                        }
                        else {
                            final byte[] startRange = (byte[])nextToken;
                            final byte[] endRange = (byte[])this.parseNextToken(cmapStream);
                            final CodespaceRange range = new CodespaceRange();
                            range.setStart(startRange);
                            range.setEnd(endRange);
                            result.addCodespaceRange(range);
                            ++j;
                        }
                    }
                }
                else if (op.op.equals("beginbfchar")) {
                    final Number cosCount = (Number)previousToken;
                    int j = 0;
                    while (j < cosCount.intValue()) {
                        Object nextToken = this.parseNextToken(cmapStream);
                        if (nextToken instanceof Operator) {
                            if (!((Operator)nextToken).op.equals("endbfchar")) {
                                throw new IOException("Error : ~bfchar contains an unexpected operator : " + ((Operator)nextToken).op);
                            }
                            break;
                        }
                        else {
                            final byte[] inputCode = (byte[])nextToken;
                            nextToken = this.parseNextToken(cmapStream);
                            if (nextToken instanceof byte[]) {
                                final byte[] bytes = (byte[])nextToken;
                                final String value = this.createStringFromBytes(bytes);
                                result.addMapping(inputCode, value);
                            }
                            else {
                                if (!(nextToken instanceof LiteralName)) {
                                    throw new IOException("Error parsing CMap beginbfchar, expected{COSString or COSName} and not " + nextToken);
                                }
                                result.addMapping(inputCode, ((LiteralName)nextToken).name);
                            }
                            ++j;
                        }
                    }
                }
                else if (op.op.equals("beginbfrange")) {
                    final Number cosCount = (Number)previousToken;
                    int j = 0;
                    while (j < cosCount.intValue()) {
                        Object nextToken = this.parseNextToken(cmapStream);
                        if (nextToken instanceof Operator) {
                            if (!((Operator)nextToken).op.equals("endbfrange")) {
                                throw new IOException("Error : ~bfrange contains an unexpected operator : " + ((Operator)nextToken).op);
                            }
                            break;
                        }
                        else {
                            final byte[] startCode = (byte[])nextToken;
                            final byte[] endCode = (byte[])this.parseNextToken(cmapStream);
                            nextToken = this.parseNextToken(cmapStream);
                            List<byte[]> array = null;
                            byte[] tokenBytes = null;
                            if (nextToken instanceof List) {
                                array = (List<byte[]>)nextToken;
                                tokenBytes = array.get(0);
                            }
                            else {
                                tokenBytes = (byte[])nextToken;
                            }
                            String value2 = null;
                            int arrayIndex = 0;
                            boolean done = false;
                            while (!done) {
                                if (this.compare(startCode, endCode) >= 0) {
                                    done = true;
                                }
                                value2 = this.createStringFromBytes(tokenBytes);
                                result.addMapping(startCode, value2);
                                this.increment(startCode);
                                if (array == null) {
                                    this.increment(tokenBytes);
                                }
                                else {
                                    if (++arrayIndex >= array.size()) {
                                        continue;
                                    }
                                    tokenBytes = array.get(arrayIndex);
                                }
                            }
                            ++j;
                        }
                    }
                }
                else if (op.op.equals("begincidchar")) {
                    final Number cosCount = (Number)previousToken;
                    int j = 0;
                    while (j < cosCount.intValue()) {
                        final Object nextToken = this.parseNextToken(cmapStream);
                        if (nextToken instanceof Operator) {
                            if (!((Operator)nextToken).op.equals("endcidchar")) {
                                throw new IOException("Error : ~cidchar contains an unexpected operator : " + ((Operator)nextToken).op);
                            }
                            break;
                        }
                        else {
                            final byte[] inputCode = (byte[])nextToken;
                            final int mappedCode = (int)this.parseNextToken(cmapStream);
                            final String mappedStr = this.createStringFromBytes(inputCode);
                            result.addCIDMapping(mappedCode, mappedStr);
                            ++j;
                        }
                    }
                }
                else if (op.op.equals("begincidrange")) {
                    final int numberOfLines = (int)previousToken;
                    int n = 0;
                    while (n < numberOfLines) {
                        final Object nextToken = this.parseNextToken(cmapStream);
                        if (nextToken instanceof Operator) {
                            if (!((Operator)nextToken).op.equals("endcidrange")) {
                                throw new IOException("Error : ~cidrange contains an unexpected operator : " + ((Operator)nextToken).op);
                            }
                            break;
                        }
                        else {
                            final byte[] startCode = (byte[])nextToken;
                            final int start = this.createIntFromBytes(startCode);
                            final byte[] endCode2 = (byte[])this.parseNextToken(cmapStream);
                            final int end = this.createIntFromBytes(endCode2);
                            int mappedCode2 = (int)this.parseNextToken(cmapStream);
                            if (startCode.length <= 2 && endCode2.length <= 2) {
                                result.addCIDRange((char)start, (char)end, mappedCode2);
                            }
                            else {
                                final int endOfMappings = mappedCode2 + end - start;
                                while (mappedCode2 <= endOfMappings) {
                                    final String mappedStr2 = this.createStringFromBytes(startCode);
                                    result.addCIDMapping(mappedCode2++, mappedStr2);
                                    this.increment(startCode);
                                }
                            }
                            ++n;
                        }
                    }
                }
            }
            else if (token instanceof LiteralName) {
                final LiteralName literal = (LiteralName)token;
                if ("WMode".equals(literal.name)) {
                    final Object next = this.parseNextToken(cmapStream);
                    if (next instanceof Integer) {
                        result.setWMode((int)next);
                    }
                }
                else if ("CMapName".equals(literal.name)) {
                    final Object next = this.parseNextToken(cmapStream);
                    if (next instanceof LiteralName) {
                        result.setName(((LiteralName)next).name);
                    }
                }
                else if ("CMapVersion".equals(literal.name)) {
                    final Object next = this.parseNextToken(cmapStream);
                    if (next instanceof Number) {
                        result.setVersion(((Number)next).toString());
                    }
                    else if (next instanceof String) {
                        result.setVersion((String)next);
                    }
                }
                else if ("CMapType".equals(literal.name)) {
                    final Object next = this.parseNextToken(cmapStream);
                    if (next instanceof Integer) {
                        result.setType((int)next);
                    }
                }
                else if ("Registry".equals(literal.name)) {
                    final Object next = this.parseNextToken(cmapStream);
                    if (next instanceof String) {
                        result.setRegistry((String)next);
                    }
                }
                else if ("Ordering".equals(literal.name)) {
                    final Object next = this.parseNextToken(cmapStream);
                    if (next instanceof String) {
                        result.setOrdering((String)next);
                    }
                }
                else if ("Supplement".equals(literal.name)) {
                    final Object next = this.parseNextToken(cmapStream);
                    if (next instanceof Integer) {
                        result.setSupplement((int)next);
                    }
                }
            }
            previousToken = token;
        }
        return result;
    }
    
    private Object parseNextToken(final PushbackInputStream is) throws IOException {
        Object retval = null;
        int nextByte;
        for (nextByte = is.read(); nextByte == 9 || nextByte == 32 || nextByte == 13 || nextByte == 10; nextByte = is.read()) {}
        switch (nextByte) {
            case 37: {
                final StringBuffer buffer = new StringBuffer();
                buffer.append((char)nextByte);
                this.readUntilEndOfLine(is, buffer);
                retval = buffer.toString();
                break;
            }
            case 40: {
                final StringBuffer buffer = new StringBuffer();
                for (int stringByte = is.read(); stringByte != -1 && stringByte != 41; stringByte = is.read()) {
                    buffer.append((char)stringByte);
                }
                retval = buffer.toString();
                break;
            }
            case 62: {
                final int secondCloseBrace = is.read();
                if (secondCloseBrace == 62) {
                    retval = ">>";
                    break;
                }
                throw new IOException("Error: expected the end of a dictionary.");
            }
            case 93: {
                retval = "]";
                break;
            }
            case 91: {
                final List<Object> list = new ArrayList<Object>();
                for (Object nextToken = this.parseNextToken(is); nextToken != null && nextToken != "]"; nextToken = this.parseNextToken(is)) {
                    list.add(nextToken);
                }
                retval = list;
                break;
            }
            case 60: {
                int theNextByte = is.read();
                if (theNextByte == 60) {
                    final Map<String, Object> result = new HashMap<String, Object>();
                    for (Object key = this.parseNextToken(is); key instanceof LiteralName && key != ">>"; key = this.parseNextToken(is)) {
                        final Object value = this.parseNextToken(is);
                        result.put(((LiteralName)key).name, value);
                    }
                    retval = result;
                    break;
                }
                int multiplyer = 16;
                int bufferIndex = -1;
                while (theNextByte != -1 && theNextByte != 62) {
                    int intValue = 0;
                    if (theNextByte >= 48 && theNextByte <= 57) {
                        intValue = theNextByte - 48;
                    }
                    else if (theNextByte >= 65 && theNextByte <= 70) {
                        intValue = 10 + theNextByte - 65;
                    }
                    else if (theNextByte >= 97 && theNextByte <= 102) {
                        intValue = 10 + theNextByte - 97;
                    }
                    else {
                        if (theNextByte == 32) {
                            theNextByte = is.read();
                            continue;
                        }
                        throw new IOException("Error: expected hex character and not " + (char)theNextByte + ":" + theNextByte);
                    }
                    intValue *= multiplyer;
                    if (multiplyer == 16) {
                        ++bufferIndex;
                        this.tokenParserByteBuffer[bufferIndex] = 0;
                        multiplyer = 1;
                    }
                    else {
                        multiplyer = 16;
                    }
                    final byte[] tokenParserByteBuffer = this.tokenParserByteBuffer;
                    final int n = bufferIndex;
                    tokenParserByteBuffer[n] += (byte)intValue;
                    theNextByte = is.read();
                }
                final byte[] finalResult = new byte[bufferIndex + 1];
                System.arraycopy(this.tokenParserByteBuffer, 0, finalResult, 0, bufferIndex + 1);
                retval = finalResult;
                break;
            }
            case 47: {
                final StringBuffer buffer = new StringBuffer();
                for (int stringByte = is.read(); !this.isWhitespaceOrEOF(stringByte); stringByte = is.read()) {
                    buffer.append((char)stringByte);
                }
                retval = new LiteralName(buffer.toString());
                break;
            }
            case -1: {
                break;
            }
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57: {
                final StringBuffer buffer = new StringBuffer();
                buffer.append((char)nextByte);
                for (nextByte = is.read(); !this.isWhitespaceOrEOF(nextByte) && (Character.isDigit((char)nextByte) || nextByte == 46); nextByte = is.read()) {
                    buffer.append((char)nextByte);
                }
                is.unread(nextByte);
                final String value2 = buffer.toString();
                if (value2.indexOf(46) >= 0) {
                    retval = new Double(value2);
                    break;
                }
                retval = new Integer(value2);
                break;
            }
            default: {
                final StringBuffer buffer = new StringBuffer();
                buffer.append((char)nextByte);
                for (nextByte = is.read(); !this.isWhitespaceOrEOF(nextByte); nextByte = is.read()) {
                    buffer.append((char)nextByte);
                }
                retval = new Operator(buffer.toString());
                break;
            }
        }
        return retval;
    }
    
    private void readUntilEndOfLine(final InputStream is, final StringBuffer buf) throws IOException {
        for (int nextByte = is.read(); nextByte != -1 && nextByte != 13 && nextByte != 10; nextByte = is.read()) {
            buf.append((char)nextByte);
        }
    }
    
    private boolean isWhitespaceOrEOF(final int aByte) {
        return aByte == -1 || aByte == 32 || aByte == 13 || aByte == 10;
    }
    
    private void increment(final byte[] data) {
        this.increment(data, data.length - 1);
    }
    
    private void increment(final byte[] data, final int position) {
        if (position > 0 && (data[position] + 256) % 256 == 255) {
            data[position] = 0;
            this.increment(data, position - 1);
        }
        else {
            ++data[position];
        }
    }
    
    private int createIntFromBytes(final byte[] bytes) {
        int intValue = (bytes[0] + 256) % 256;
        if (bytes.length == 2) {
            intValue <<= 8;
            intValue += (bytes[1] + 256) % 256;
        }
        return intValue;
    }
    
    private String createStringFromBytes(final byte[] bytes) throws IOException {
        String retval = null;
        if (bytes.length == 1) {
            retval = new String(bytes, "ISO-8859-1");
        }
        else {
            retval = new String(bytes, "UTF-16BE");
        }
        return retval;
    }
    
    private int compare(final byte[] first, final byte[] second) {
        int retval = 1;
        final int firstLength = first.length;
        int i = 0;
        while (i < firstLength) {
            if (first[i] == second[i]) {
                ++i;
            }
            else {
                if ((first[i] + 256) % 256 < (second[i] + 256) % 256) {
                    retval = -1;
                    break;
                }
                retval = 1;
                break;
            }
        }
        return retval;
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("usage: java org.pdfbox.cmapparser.CMapParser <CMAP File>");
            System.exit(-1);
        }
        final CMapParser parser = new CMapParser();
        final File cmapFile = new File(args[0]);
        final CMap result = parser.parse(cmapFile);
        System.out.println("Result:" + result);
    }
    
    private class LiteralName
    {
        private String name;
        
        private LiteralName(final String theName) {
            this.name = theName;
        }
    }
    
    private class Operator
    {
        private String op;
        
        private Operator(final String theOp) {
            this.op = theOp;
        }
    }
}
