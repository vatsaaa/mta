// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cmap;

public class CodespaceRange
{
    private byte[] start;
    private byte[] end;
    
    public byte[] getEnd() {
        return this.end;
    }
    
    public void setEnd(final byte[] endBytes) {
        this.end = endBytes;
    }
    
    public byte[] getStart() {
        return this.start;
    }
    
    public void setStart(final byte[] startBytes) {
        this.start = startBytes;
    }
    
    public boolean isInRange(final byte[] code, final int offset, final int length) {
        if (length < this.start.length || length > this.end.length) {
            return false;
        }
        if (this.end.length == length) {
            for (int i = 0; i < this.end.length; ++i) {
                final int endInt = this.end[i] & 0xFF;
                final int codeInt = code[offset + i] & 0xFF;
                if (endInt < codeInt) {
                    return false;
                }
            }
        }
        if (this.start.length == length) {
            for (int i = 0; i < this.end.length; ++i) {
                final int startInt = this.start[i] & 0xFF;
                final int codeInt = code[offset + i] & 0xFF;
                if (startInt > codeInt) {
                    return false;
                }
            }
        }
        return true;
    }
}
