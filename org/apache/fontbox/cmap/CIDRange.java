// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cmap;

class CIDRange
{
    private char from;
    private char to;
    private int cid;
    
    public CIDRange(final char from, final char to, final int cid) {
        this.from = from;
        this.to = to;
        this.cid = cid;
    }
    
    public int map(final char ch) {
        if (this.from <= ch && ch <= this.to) {
            return this.cid + (ch - this.from);
        }
        return -1;
    }
    
    public int unmap(final int code) {
        if (this.cid <= code && code <= this.cid + (this.to - this.from)) {
            return this.from + (code - this.cid);
        }
        return -1;
    }
}
