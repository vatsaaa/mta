// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.cmap;

import java.util.Collection;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

public class CMap
{
    private int wmode;
    private String cmapName;
    private String cmapVersion;
    private int cmapType;
    private String registry;
    private String ordering;
    private int supplement;
    private List<CodespaceRange> codeSpaceRanges;
    private Map<Integer, String> singleByteMappings;
    private Map<Integer, String> doubleByteMappings;
    private final Map<Integer, String> cid2charMappings;
    private final Map<String, Integer> char2CIDMappings;
    private final List<CIDRange> cidRanges;
    
    public CMap() {
        this.wmode = 0;
        this.cmapName = null;
        this.cmapVersion = null;
        this.cmapType = -1;
        this.registry = null;
        this.ordering = null;
        this.supplement = 0;
        this.codeSpaceRanges = new ArrayList<CodespaceRange>();
        this.singleByteMappings = new HashMap<Integer, String>();
        this.doubleByteMappings = new HashMap<Integer, String>();
        this.cid2charMappings = new HashMap<Integer, String>();
        this.char2CIDMappings = new HashMap<String, Integer>();
        this.cidRanges = new LinkedList<CIDRange>();
    }
    
    public boolean hasOneByteMappings() {
        return this.singleByteMappings.size() > 0;
    }
    
    public boolean hasTwoByteMappings() {
        return this.doubleByteMappings.size() > 0;
    }
    
    public boolean hasCIDMappings() {
        return !this.char2CIDMappings.isEmpty() || !this.cidRanges.isEmpty();
    }
    
    public String lookup(final byte[] code, final int offset, final int length) {
        return this.lookup(this.getCodeFromArray(code, offset, length), length);
    }
    
    public String lookup(final int code, final int length) {
        String result = null;
        if (length == 1) {
            result = this.singleByteMappings.get(code);
        }
        else if (length == 2) {
            result = this.doubleByteMappings.get(code);
        }
        return result;
    }
    
    public String lookupCID(final int cid) {
        if (this.cid2charMappings.containsKey(cid)) {
            return this.cid2charMappings.get(cid);
        }
        for (final CIDRange range : this.cidRanges) {
            final int ch = range.unmap(cid);
            if (ch != -1) {
                return Character.toString((char)ch);
            }
        }
        return null;
    }
    
    public int lookupCID(final byte[] code, final int offset, final int length) {
        if (!this.isInCodeSpaceRanges(code, offset, length)) {
            return -1;
        }
        final int codeAsInt = this.getCodeFromArray(code, offset, length);
        if (this.char2CIDMappings.containsKey(codeAsInt)) {
            return this.char2CIDMappings.get(codeAsInt);
        }
        for (final CIDRange range : this.cidRanges) {
            final int ch = range.map((char)codeAsInt);
            if (ch != -1) {
                return ch;
            }
        }
        return -1;
    }
    
    private int getCodeFromArray(final byte[] data, final int offset, final int length) {
        int code = 0;
        for (int i = 0; i < length; ++i) {
            code <<= 8;
            code |= (data[offset + i] + 256) % 256;
        }
        return code;
    }
    
    public void addMapping(final byte[] src, final String dest) throws IOException {
        final int srcLength = src.length;
        final int intSrc = this.getCodeFromArray(src, 0, srcLength);
        if (srcLength == 1) {
            this.singleByteMappings.put(intSrc, dest);
        }
        else {
            if (srcLength != 2) {
                throw new IOException("Mapping code should be 1 or two bytes and not " + src.length);
            }
            this.doubleByteMappings.put(intSrc, dest);
        }
    }
    
    public void addCIDMapping(final int src, final String dest) throws IOException {
        this.cid2charMappings.put(src, dest);
        this.char2CIDMappings.put(dest, src);
    }
    
    public void addCIDRange(final char from, final char to, final int cid) {
        this.cidRanges.add(0, new CIDRange(from, to, cid));
    }
    
    public void addCodespaceRange(final CodespaceRange range) {
        this.codeSpaceRanges.add(range);
    }
    
    public List<CodespaceRange> getCodeSpaceRanges() {
        return this.codeSpaceRanges;
    }
    
    public void useCmap(final CMap cmap) {
        this.codeSpaceRanges.addAll(cmap.codeSpaceRanges);
        this.singleByteMappings.putAll(cmap.singleByteMappings);
        this.doubleByteMappings.putAll(cmap.doubleByteMappings);
    }
    
    public boolean isInCodeSpaceRanges(final byte[] code) {
        return this.isInCodeSpaceRanges(code, 0, code.length);
    }
    
    public boolean isInCodeSpaceRanges(final byte[] code, final int offset, final int length) {
        for (final CodespaceRange range : this.codeSpaceRanges) {
            if (range != null && range.isInRange(code, offset, length)) {
                return true;
            }
        }
        return false;
    }
    
    public int getWMode() {
        return this.wmode;
    }
    
    public void setWMode(final int newWMode) {
        this.wmode = newWMode;
    }
    
    public String getName() {
        return this.cmapName;
    }
    
    public void setName(final String name) {
        this.cmapName = name;
    }
    
    public String getVersion() {
        return this.cmapVersion;
    }
    
    public void setVersion(final String version) {
        this.cmapVersion = version;
    }
    
    public int getType() {
        return this.cmapType;
    }
    
    public void setType(final int type) {
        this.cmapType = type;
    }
    
    public String getRegistry() {
        return this.registry;
    }
    
    public void setRegistry(final String newRegistry) {
        this.registry = newRegistry;
    }
    
    public String getOrdering() {
        return this.ordering;
    }
    
    public void setOrdering(final String newOrdering) {
        this.ordering = newOrdering;
    }
    
    public int getSupplement() {
        return this.supplement;
    }
    
    public void setSupplement(final int newSupplement) {
        this.supplement = newSupplement;
    }
}
