// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class HorizontalMetricsTable extends TTFTable
{
    public static final String TAG = "hmtx";
    private int[] advanceWidth;
    private short[] leftSideBearing;
    private short[] nonHorizontalLeftSideBearing;
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        final HorizontalHeaderTable hHeader = ttf.getHorizontalHeader();
        final MaximumProfileTable maxp = ttf.getMaximumProfile();
        final int numHMetrics = hHeader.getNumberOfHMetrics();
        final int numGlyphs = maxp.getNumGlyphs();
        this.advanceWidth = new int[numHMetrics];
        this.leftSideBearing = new short[numHMetrics];
        for (int i = 0; i < numHMetrics; ++i) {
            this.advanceWidth[i] = data.readUnsignedShort();
            this.leftSideBearing[i] = data.readSignedShort();
        }
        final int numberNonHorizontal = numGlyphs - numHMetrics;
        this.nonHorizontalLeftSideBearing = new short[numberNonHorizontal];
        for (int j = 0; j < numberNonHorizontal; ++j) {
            this.nonHorizontalLeftSideBearing[j] = data.readSignedShort();
        }
    }
    
    public int[] getAdvanceWidth() {
        return this.advanceWidth;
    }
    
    public void setAdvanceWidth(final int[] advanceWidthValue) {
        this.advanceWidth = advanceWidthValue;
    }
}
