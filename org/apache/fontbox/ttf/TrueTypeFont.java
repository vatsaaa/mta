// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.InputStream;
import java.util.Collection;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TrueTypeFont
{
    private float version;
    private Map<String, TTFTable> tables;
    private TTFDataStream data;
    
    TrueTypeFont(final TTFDataStream fontData) {
        this.tables = new HashMap<String, TTFTable>();
        this.data = fontData;
    }
    
    public void close() throws IOException {
        this.data.close();
    }
    
    public float getVersion() {
        return this.version;
    }
    
    public void setVersion(final float versionValue) {
        this.version = versionValue;
    }
    
    public void addTable(final TTFTable table) {
        this.tables.put(table.getTag(), table);
    }
    
    public Collection<TTFTable> getTables() {
        return this.tables.values();
    }
    
    public NamingTable getNaming() {
        return this.tables.get("name");
    }
    
    public PostScriptTable getPostScript() {
        return this.tables.get("post");
    }
    
    public OS2WindowsMetricsTable getOS2Windows() {
        return this.tables.get("OS/2");
    }
    
    public MaximumProfileTable getMaximumProfile() {
        return this.tables.get("maxp");
    }
    
    public HeaderTable getHeader() {
        return this.tables.get("head");
    }
    
    public HorizontalHeaderTable getHorizontalHeader() {
        return this.tables.get("hhea");
    }
    
    public HorizontalMetricsTable getHorizontalMetrics() {
        return this.tables.get("hmtx");
    }
    
    public IndexToLocationTable getIndexToLocation() {
        return this.tables.get("loca");
    }
    
    public GlyphTable getGlyph() {
        return this.tables.get("glyf");
    }
    
    public CMAPTable getCMAP() {
        return this.tables.get("cmap");
    }
    
    public InputStream getOriginalData() throws IOException {
        return this.data.getOriginalData();
    }
}
