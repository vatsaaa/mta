// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CMAPEncodingEntry
{
    private int platformId;
    private int platformEncodingId;
    private long subTableOffset;
    private int[] glyphIdToCharacterCode;
    private Map<Integer, Integer> characterCodeToGlyphId;
    
    public CMAPEncodingEntry() {
        this.characterCodeToGlyphId = new HashMap<Integer, Integer>();
    }
    
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        this.platformId = data.readUnsignedShort();
        this.platformEncodingId = data.readUnsignedShort();
        this.subTableOffset = data.readUnsignedInt();
    }
    
    public void initSubtable(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        data.seek(ttf.getCMAP().getOffset() + this.subTableOffset);
        final int subtableFormat = data.readUnsignedShort();
        int numGlyphs;
        if (subtableFormat < 8) {
            final long length = data.readUnsignedShort();
            final long version = data.readUnsignedShort();
            numGlyphs = ttf.getMaximumProfile().getNumGlyphs();
        }
        else {
            data.readUnsignedShort();
            final long length = data.readUnsignedInt();
            final long version = data.readUnsignedInt();
            numGlyphs = ttf.getMaximumProfile().getNumGlyphs();
        }
        switch (subtableFormat) {
            case 0: {
                this.processSubtype0(ttf, data);
                break;
            }
            case 2: {
                this.processSubtype2(ttf, data, numGlyphs);
                break;
            }
            case 4: {
                this.processSubtype4(ttf, data, numGlyphs);
                break;
            }
            case 6: {
                this.processSubtype6(ttf, data, numGlyphs);
                break;
            }
            case 8: {
                this.processSubtype8(ttf, data, numGlyphs);
                break;
            }
            case 10: {
                this.processSubtype10(ttf, data, numGlyphs);
                break;
            }
            case 12: {
                this.processSubtype12(ttf, data, numGlyphs);
                break;
            }
            case 13: {
                this.processSubtype13(ttf, data, numGlyphs);
                break;
            }
            case 14: {
                this.processSubtype14(ttf, data, numGlyphs);
                break;
            }
            default: {
                throw new IOException("Unknown cmap format:" + subtableFormat);
            }
        }
    }
    
    protected void processSubtype8(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        final int[] is32 = data.readUnsignedByteArray(8192);
        final long nbGroups = data.readUnsignedInt();
        if (nbGroups > 65536L) {
            throw new IOException("CMap ( Subtype8 ) is invalid");
        }
        this.glyphIdToCharacterCode = new int[numGlyphs];
        for (long i = 0L; i <= nbGroups; ++i) {
            final long firstCode = data.readUnsignedInt();
            final long endCode = data.readUnsignedInt();
            final long startGlyph = data.readUnsignedInt();
            if (firstCode > endCode || 0L > firstCode) {
                throw new IOException("Range invalid");
            }
            for (long j = firstCode; j <= endCode; ++j) {
                if (j > 2147483647L) {
                    throw new IOException("[Sub Format 8] Invalid Character code");
                }
                int currentCharCode;
                if ((is32[(int)j / 8] & 1 << (int)j % 8) == 0x0) {
                    currentCharCode = (int)j;
                }
                else {
                    final long LEAD_OFFSET = 55232L;
                    final long SURROGATE_OFFSET = -56613888L;
                    final long lead = LEAD_OFFSET + (j >> 10);
                    final long trail = 56320L + (j & 0x3FFL);
                    final long codepoint = (lead << 10) + trail + SURROGATE_OFFSET;
                    if (codepoint > 2147483647L) {
                        throw new IOException("[Sub Format 8] Invalid Character code");
                    }
                    currentCharCode = (int)codepoint;
                }
                final long glyphIndex = startGlyph + (j - firstCode);
                if (glyphIndex > numGlyphs || glyphIndex > 2147483647L) {
                    throw new IOException("CMap contains an invalid glyph index");
                }
                this.glyphIdToCharacterCode[(int)glyphIndex] = currentCharCode;
                this.characterCodeToGlyphId.put(currentCharCode, (int)glyphIndex);
            }
        }
    }
    
    protected void processSubtype10(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        final long startCode = data.readUnsignedInt();
        final long numChars = data.readUnsignedInt();
        if (numChars > 2147483647L) {
            throw new IOException("Invalid number of Characters");
        }
        if (startCode < 0L || startCode > 1114111L || startCode + numChars > 1114111L || (startCode + numChars >= 55296L && startCode + numChars <= 57343L)) {
            throw new IOException("Invalid Characters codes");
        }
    }
    
    protected void processSubtype12(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        final long nbGroups = data.readUnsignedInt();
        this.glyphIdToCharacterCode = new int[numGlyphs];
        for (long i = 0L; i <= nbGroups; ++i) {
            final long firstCode = data.readUnsignedInt();
            final long endCode = data.readUnsignedInt();
            final long startGlyph = data.readUnsignedInt();
            if (firstCode < 0L || firstCode > 1114111L || (firstCode >= 55296L && firstCode <= 57343L)) {
                throw new IOException("Invalid Characters codes");
            }
            if (endCode > 0L && (endCode < firstCode || endCode > 1114111L || (endCode >= 55296L && endCode <= 57343L))) {
                throw new IOException("Invalid Characters codes");
            }
            for (long j = 0L; j <= endCode - firstCode; ++j) {
                if (firstCode + j > 2147483647L) {
                    throw new IOException("Character Code greater than Integer.MAX_VALUE");
                }
                final long glyphIndex = startGlyph + j;
                if (glyphIndex > numGlyphs || glyphIndex > 2147483647L) {
                    throw new IOException("CMap contains an invalid glyph index");
                }
                this.glyphIdToCharacterCode[(int)glyphIndex] = (int)(firstCode + j);
                this.characterCodeToGlyphId.put((int)(firstCode + j), (int)glyphIndex);
            }
        }
    }
    
    protected void processSubtype13(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        for (long nbGroups = data.readUnsignedInt(), i = 0L; i <= nbGroups; ++i) {
            final long firstCode = data.readUnsignedInt();
            final long endCode = data.readUnsignedInt();
            final long glyphId = data.readUnsignedInt();
            if (glyphId > numGlyphs) {
                throw new IOException("CMap contains an invalid glyph index");
            }
            if (firstCode < 0L || firstCode > 1114111L || (firstCode >= 55296L && firstCode <= 57343L)) {
                throw new IOException("Invalid Characters codes");
            }
            if (endCode > 0L && (endCode < firstCode || endCode > 1114111L || (endCode >= 55296L && endCode <= 57343L))) {
                throw new IOException("Invalid Characters codes");
            }
            for (long j = 0L; j <= endCode - firstCode; ++j) {
                if (firstCode + j > 2147483647L) {
                    throw new IOException("Character Code greater than Integer.MAX_VALUE");
                }
                this.glyphIdToCharacterCode[(int)glyphId] = (int)(firstCode + j);
                this.characterCodeToGlyphId.put((int)(firstCode + j), (int)glyphId);
            }
        }
    }
    
    protected void processSubtype14(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        throw new IOException("CMap subtype 14 not yet implemented");
    }
    
    protected void processSubtype6(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        final int firstCode = data.readUnsignedShort();
        final int entryCount = data.readUnsignedShort();
        this.glyphIdToCharacterCode = new int[numGlyphs];
        final int[] glyphIdArray = data.readUnsignedShortArray(entryCount);
        for (int i = 0; i < entryCount; ++i) {
            this.glyphIdToCharacterCode[glyphIdArray[i]] = firstCode + i;
            this.characterCodeToGlyphId.put(firstCode + i, glyphIdArray[i]);
        }
    }
    
    protected void processSubtype4(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        final int segCountX2 = data.readUnsignedShort();
        final int segCount = segCountX2 / 2;
        final int searchRange = data.readUnsignedShort();
        final int entrySelector = data.readUnsignedShort();
        final int rangeShift = data.readUnsignedShort();
        final int[] endCount = data.readUnsignedShortArray(segCount);
        final int reservedPad = data.readUnsignedShort();
        final int[] startCount = data.readUnsignedShortArray(segCount);
        final int[] idDelta = data.readUnsignedShortArray(segCount);
        final int[] idRangeOffset = data.readUnsignedShortArray(segCount);
        this.glyphIdToCharacterCode = new int[numGlyphs];
        final long currentPosition = data.getCurrentPosition();
        for (int i = 0; i < segCount; ++i) {
            final int start = startCount[i];
            final int end = endCount[i];
            final int delta = idDelta[i];
            final int rangeOffset = idRangeOffset[i];
            if (start != 65535 && end != 65535) {
                for (int j = start; j <= end; ++j) {
                    if (rangeOffset == 0) {
                        this.glyphIdToCharacterCode[(j + delta) % 65536] = j;
                        this.characterCodeToGlyphId.put(j, (j + delta) % 65536);
                    }
                    else {
                        final long glyphOffset = currentPosition + (rangeOffset / 2 + (j - start) + (i - segCount)) * 2;
                        data.seek(glyphOffset);
                        int glyphIndex = data.readUnsignedShort();
                        if (glyphIndex != 0) {
                            glyphIndex += delta;
                            glyphIndex %= 65536;
                            if (this.glyphIdToCharacterCode[glyphIndex] == 0) {
                                this.glyphIdToCharacterCode[glyphIndex] = j;
                                this.characterCodeToGlyphId.put(j, glyphIndex);
                            }
                        }
                    }
                }
            }
        }
    }
    
    protected void processSubtype2(final TrueTypeFont ttf, final TTFDataStream data, final int numGlyphs) throws IOException {
        final int[] subHeaderKeys = new int[256];
        int maxSubHeaderIndex = 0;
        for (int i = 0; i < 256; ++i) {
            subHeaderKeys[i] = data.readUnsignedShort();
            maxSubHeaderIndex = Math.max(maxSubHeaderIndex, subHeaderKeys[i] / 8);
        }
        final SubHeader[] subHeaders = new SubHeader[maxSubHeaderIndex + 1];
        for (int j = 0; j <= maxSubHeaderIndex; ++j) {
            final int firstCode = data.readUnsignedShort();
            final int entryCount = data.readUnsignedShort();
            final short idDelta = data.readSignedShort();
            final int idRangeOffset = data.readUnsignedShort();
            subHeaders[j] = new SubHeader(firstCode, entryCount, idDelta, idRangeOffset);
        }
        final long startGlyphIndexOffset = data.getCurrentPosition();
        this.glyphIdToCharacterCode = new int[numGlyphs];
        for (int k = 0; k <= maxSubHeaderIndex; ++k) {
            final SubHeader sh = subHeaders[k];
            final int firstCode2 = sh.getFirstCode();
            for (int l = 0; l < sh.getEntryCount(); ++l) {
                int charCode = k * 8;
                charCode = (charCode << 8) + (firstCode2 + l);
                data.seek(startGlyphIndexOffset + sh.getIdRangeOffset() + l * 2);
                int p = data.readUnsignedShort();
                p += sh.getIdDelta() % 65536;
                this.glyphIdToCharacterCode[p] = charCode;
                this.characterCodeToGlyphId.put(charCode, p);
            }
        }
    }
    
    protected void processSubtype0(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        final byte[] glyphMapping = data.read(256);
        this.glyphIdToCharacterCode = new int[256];
        for (int i = 0; i < glyphMapping.length; ++i) {
            final int glyphIndex = (glyphMapping[i] + 256) % 256;
            this.glyphIdToCharacterCode[glyphIndex] = i;
            this.characterCodeToGlyphId.put(i, glyphIndex);
        }
    }
    
    public int[] getGlyphIdToCharacterCode() {
        return this.glyphIdToCharacterCode;
    }
    
    public void setGlyphIdToCharacterCode(final int[] glyphIdToCharacterCodeValue) {
        this.glyphIdToCharacterCode = glyphIdToCharacterCodeValue;
    }
    
    public int getPlatformEncodingId() {
        return this.platformEncodingId;
    }
    
    public void setPlatformEncodingId(final int platformEncodingIdValue) {
        this.platformEncodingId = platformEncodingIdValue;
    }
    
    public int getPlatformId() {
        return this.platformId;
    }
    
    public void setPlatformId(final int platformIdValue) {
        this.platformId = platformIdValue;
    }
    
    public int getGlyphId(final int characterCode) {
        if (this.characterCodeToGlyphId.containsKey(characterCode)) {
            return this.characterCodeToGlyphId.get(characterCode);
        }
        return 0;
    }
    
    private class SubHeader
    {
        private int firstCode;
        private int entryCount;
        private short idDelta;
        private int idRangeOffset;
        
        private SubHeader(final int firstCode, final int entryCount, final short idDelta, final int idRangeOffset) {
            this.firstCode = firstCode;
            this.entryCount = entryCount;
            this.idDelta = idDelta;
            this.idRangeOffset = idRangeOffset;
        }
        
        private int getFirstCode() {
            return this.firstCode;
        }
        
        private int getEntryCount() {
            return this.entryCount;
        }
        
        private short getIdDelta() {
            return this.idDelta;
        }
        
        private int getIdRangeOffset() {
            return this.idRangeOffset;
        }
    }
}
