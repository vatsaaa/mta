// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class OS2WindowsMetricsTable extends TTFTable
{
    public static final int WEIGHT_CLASS_THIN = 100;
    public static final int WEIGHT_CLASS_ULTRA_LIGHT = 200;
    public static final int WEIGHT_CLASS_LIGHT = 300;
    public static final int WEIGHT_CLASS_NORMAL = 400;
    public static final int WEIGHT_CLASS_MEDIUM = 500;
    public static final int WEIGHT_CLASS_SEMI_BOLD = 600;
    public static final int WEIGHT_CLASS_BOLD = 700;
    public static final int WEIGHT_CLASS_EXTRA_BOLD = 800;
    public static final int WEIGHT_CLASS_BLACK = 900;
    public static final int WIDTH_CLASS_ULTRA_CONDENSED = 1;
    public static final int WIDTH_CLASS_EXTRA_CONDENSED = 2;
    public static final int WIDTH_CLASS_CONDENSED = 3;
    public static final int WIDTH_CLASS_SEMI_CONDENSED = 4;
    public static final int WIDTH_CLASS_MEDIUM = 5;
    public static final int WIDTH_CLASS_SEMI_EXPANDED = 6;
    public static final int WIDTH_CLASS_EXPANDED = 7;
    public static final int WIDTH_CLASS_EXTRA_EXPANDED = 8;
    public static final int WIDTH_CLASS_ULTRA_EXPANDED = 9;
    public static final int FAMILY_CLASS_NO_CLASSIFICATION = 0;
    public static final int FAMILY_CLASS_OLDSTYLE_SERIFS = 1;
    public static final int FAMILY_CLASS_TRANSITIONAL_SERIFS = 2;
    public static final int FAMILY_CLASS_MODERN_SERIFS = 3;
    public static final int FAMILY_CLASS_CLAREDON_SERIFS = 4;
    public static final int FAMILY_CLASS_SLAB_SERIFS = 5;
    public static final int FAMILY_CLASS_FREEFORM_SERIFS = 7;
    public static final int FAMILY_CLASS_SANS_SERIF = 8;
    public static final int FAMILY_CLASS_ORNAMENTALS = 9;
    public static final int FAMILY_CLASS_SCRIPTS = 10;
    public static final int FAMILY_CLASS_SYMBOLIC = 12;
    private int version;
    private short averageCharWidth;
    private int weightClass;
    private int widthClass;
    private short fsType;
    private short subscriptXSize;
    private short subscriptYSize;
    private short subscriptXOffset;
    private short subscriptYOffset;
    private short superscriptXSize;
    private short superscriptYSize;
    private short superscriptXOffset;
    private short superscriptYOffset;
    private short strikeoutSize;
    private short strikeoutPosition;
    private int familyClass;
    private int familySubClass;
    private byte[] panose;
    private long unicodeRange1;
    private long unicodeRange2;
    private long unicodeRange3;
    private long unicodeRange4;
    private String achVendId;
    private int fsSelection;
    private int firstCharIndex;
    private int lastCharIndex;
    private int typoAscender;
    private int typoDescender;
    private int typeLineGap;
    private int winAscent;
    private int winDescent;
    private long codePageRange1;
    private long codePageRange2;
    public static final String TAG = "OS/2";
    
    public OS2WindowsMetricsTable() {
        this.panose = new byte[10];
        this.codePageRange1 = -1L;
        this.codePageRange2 = -1L;
    }
    
    public String getAchVendId() {
        return this.achVendId;
    }
    
    public void setAchVendId(final String achVendIdValue) {
        this.achVendId = achVendIdValue;
    }
    
    public short getAverageCharWidth() {
        return this.averageCharWidth;
    }
    
    public void setAverageCharWidth(final short averageCharWidthValue) {
        this.averageCharWidth = averageCharWidthValue;
    }
    
    public long getCodePageRange1() {
        return this.codePageRange1;
    }
    
    public void setCodePageRange1(final long codePageRange1Value) {
        this.codePageRange1 = codePageRange1Value;
    }
    
    public long getCodePageRange2() {
        return this.codePageRange2;
    }
    
    public void setCodePageRange2(final long codePageRange2Value) {
        this.codePageRange2 = codePageRange2Value;
    }
    
    public int getFamilyClass() {
        return this.familyClass;
    }
    
    public void setFamilyClass(final int familyClassValue) {
        this.familyClass = familyClassValue;
    }
    
    public int getFamilySubClass() {
        return this.familySubClass;
    }
    
    public void setFamilySubClass(final int familySubClassValue) {
        this.familySubClass = familySubClassValue;
    }
    
    public int getFirstCharIndex() {
        return this.firstCharIndex;
    }
    
    public void setFirstCharIndex(final int firstCharIndexValue) {
        this.firstCharIndex = firstCharIndexValue;
    }
    
    public int getFsSelection() {
        return this.fsSelection;
    }
    
    public void setFsSelection(final int fsSelectionValue) {
        this.fsSelection = fsSelectionValue;
    }
    
    public short getFsType() {
        return this.fsType;
    }
    
    public void setFsType(final short fsTypeValue) {
        this.fsType = fsTypeValue;
    }
    
    public int getLastCharIndex() {
        return this.lastCharIndex;
    }
    
    public void setLastCharIndex(final int lastCharIndexValue) {
        this.lastCharIndex = lastCharIndexValue;
    }
    
    public byte[] getPanose() {
        return this.panose;
    }
    
    public void setPanose(final byte[] panoseValue) {
        this.panose = panoseValue;
    }
    
    public short getStrikeoutPosition() {
        return this.strikeoutPosition;
    }
    
    public void setStrikeoutPosition(final short strikeoutPositionValue) {
        this.strikeoutPosition = strikeoutPositionValue;
    }
    
    public short getStrikeoutSize() {
        return this.strikeoutSize;
    }
    
    public void setStrikeoutSize(final short strikeoutSizeValue) {
        this.strikeoutSize = strikeoutSizeValue;
    }
    
    public short getSubscriptXOffset() {
        return this.subscriptXOffset;
    }
    
    public void setSubscriptXOffset(final short subscriptXOffsetValue) {
        this.subscriptXOffset = subscriptXOffsetValue;
    }
    
    public short getSubscriptXSize() {
        return this.subscriptXSize;
    }
    
    public void setSubscriptXSize(final short subscriptXSizeValue) {
        this.subscriptXSize = subscriptXSizeValue;
    }
    
    public short getSubscriptYOffset() {
        return this.subscriptYOffset;
    }
    
    public void setSubscriptYOffset(final short subscriptYOffsetValue) {
        this.subscriptYOffset = subscriptYOffsetValue;
    }
    
    public short getSubscriptYSize() {
        return this.subscriptYSize;
    }
    
    public void setSubscriptYSize(final short subscriptYSizeValue) {
        this.subscriptYSize = subscriptYSizeValue;
    }
    
    public short getSuperscriptXOffset() {
        return this.superscriptXOffset;
    }
    
    public void setSuperscriptXOffset(final short superscriptXOffsetValue) {
        this.superscriptXOffset = superscriptXOffsetValue;
    }
    
    public short getSuperscriptXSize() {
        return this.superscriptXSize;
    }
    
    public void setSuperscriptXSize(final short superscriptXSizeValue) {
        this.superscriptXSize = superscriptXSizeValue;
    }
    
    public short getSuperscriptYOffset() {
        return this.superscriptYOffset;
    }
    
    public void setSuperscriptYOffset(final short superscriptYOffsetValue) {
        this.superscriptYOffset = superscriptYOffsetValue;
    }
    
    public short getSuperscriptYSize() {
        return this.superscriptYSize;
    }
    
    public void setSuperscriptYSize(final short superscriptYSizeValue) {
        this.superscriptYSize = superscriptYSizeValue;
    }
    
    public int getTypeLineGap() {
        return this.typeLineGap;
    }
    
    public void setTypeLineGap(final int typeLineGapValue) {
        this.typeLineGap = typeLineGapValue;
    }
    
    public int getTypoAscender() {
        return this.typoAscender;
    }
    
    public void setTypoAscender(final int typoAscenderValue) {
        this.typoAscender = typoAscenderValue;
    }
    
    public int getTypoDescender() {
        return this.typoDescender;
    }
    
    public void setTypoDescender(final int typoDescenderValue) {
        this.typoDescender = typoDescenderValue;
    }
    
    public long getUnicodeRange1() {
        return this.unicodeRange1;
    }
    
    public void setUnicodeRange1(final long unicodeRange1Value) {
        this.unicodeRange1 = unicodeRange1Value;
    }
    
    public long getUnicodeRange2() {
        return this.unicodeRange2;
    }
    
    public void setUnicodeRange2(final long unicodeRange2Value) {
        this.unicodeRange2 = unicodeRange2Value;
    }
    
    public long getUnicodeRange3() {
        return this.unicodeRange3;
    }
    
    public void setUnicodeRange3(final long unicodeRange3Value) {
        this.unicodeRange3 = unicodeRange3Value;
    }
    
    public long getUnicodeRange4() {
        return this.unicodeRange4;
    }
    
    public void setUnicodeRange4(final long unicodeRange4Value) {
        this.unicodeRange4 = unicodeRange4Value;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int versionValue) {
        this.version = versionValue;
    }
    
    public int getWeightClass() {
        return this.weightClass;
    }
    
    public void setWeightClass(final int weightClassValue) {
        this.weightClass = weightClassValue;
    }
    
    public int getWidthClass() {
        return this.widthClass;
    }
    
    public void setWidthClass(final int widthClassValue) {
        this.widthClass = widthClassValue;
    }
    
    public int getWinAscent() {
        return this.winAscent;
    }
    
    public void setWinAscent(final int winAscentValue) {
        this.winAscent = winAscentValue;
    }
    
    public int getWinDescent() {
        return this.winDescent;
    }
    
    public void setWinDescent(final int winDescentValue) {
        this.winDescent = winDescentValue;
    }
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        this.version = data.readUnsignedShort();
        this.averageCharWidth = data.readSignedShort();
        this.weightClass = data.readUnsignedShort();
        this.widthClass = data.readUnsignedShort();
        this.fsType = data.readSignedShort();
        this.subscriptXSize = data.readSignedShort();
        this.subscriptYSize = data.readSignedShort();
        this.subscriptXOffset = data.readSignedShort();
        this.subscriptYOffset = data.readSignedShort();
        this.superscriptXSize = data.readSignedShort();
        this.superscriptYSize = data.readSignedShort();
        this.superscriptXOffset = data.readSignedShort();
        this.superscriptYOffset = data.readSignedShort();
        this.strikeoutSize = data.readSignedShort();
        this.strikeoutPosition = data.readSignedShort();
        this.familyClass = data.read();
        this.familySubClass = data.read();
        this.panose = data.read(10);
        this.unicodeRange1 = data.readUnsignedInt();
        this.unicodeRange2 = data.readUnsignedInt();
        this.unicodeRange3 = data.readUnsignedInt();
        this.unicodeRange4 = data.readUnsignedInt();
        this.achVendId = data.readString(4);
        this.fsSelection = data.readUnsignedShort();
        this.firstCharIndex = data.readUnsignedShort();
        this.lastCharIndex = data.readUnsignedShort();
        this.typoAscender = data.readSignedShort();
        this.typoDescender = data.readSignedShort();
        this.typeLineGap = data.readSignedShort();
        this.winAscent = data.readUnsignedShort();
        this.winDescent = data.readUnsignedShort();
        if (this.version >= 1) {
            this.codePageRange1 = data.readUnsignedInt();
            this.codePageRange2 = data.readUnsignedInt();
        }
    }
}
