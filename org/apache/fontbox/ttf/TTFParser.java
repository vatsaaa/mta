// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;

public class TTFParser extends AbstractTTFParser
{
    public TTFParser() {
        super(false);
    }
    
    public TTFParser(final boolean isEmbedded) {
        super(isEmbedded);
    }
    
    public static void main(final String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("usage: java org.pdfbox.ttf.TTFParser <ttf-file>");
            System.exit(-1);
        }
        final TTFParser parser = new TTFParser();
        final TrueTypeFont font = parser.parseTTF(args[0]);
        System.out.println("Font:" + font);
    }
    
    @Override
    protected void parseTables(final TrueTypeFont font, final TTFDataStream raf) throws IOException {
        super.parseTables(font, raf);
        if (font.getCMAP() == null) {
            throw new IOException("cmap is mandatory");
        }
    }
}
