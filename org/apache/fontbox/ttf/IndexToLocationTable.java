// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class IndexToLocationTable extends TTFTable
{
    private static final short SHORT_OFFSETS = 0;
    private static final short LONG_OFFSETS = 1;
    public static final String TAG = "loca";
    private long[] offsets;
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        final HeaderTable head = ttf.getHeader();
        final MaximumProfileTable maxp = ttf.getMaximumProfile();
        final int numGlyphs = maxp.getNumGlyphs();
        this.offsets = new long[numGlyphs + 1];
        for (int i = 0; i < numGlyphs + 1; ++i) {
            if (head.getIndexToLocFormat() == 0) {
                this.offsets[i] = data.readUnsignedShort() * 2;
            }
            else {
                if (head.getIndexToLocFormat() != 1) {
                    throw new IOException("Error:TTF.loca unknown offset format.");
                }
                this.offsets[i] = data.readUnsignedInt();
            }
        }
    }
    
    public long[] getOffsets() {
        return this.offsets;
    }
    
    public void setOffsets(final long[] offsetsValue) {
        this.offsets = offsetsValue;
    }
}
