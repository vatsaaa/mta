// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.awt.geom.Point2D;
import java.awt.geom.GeneralPath;

public class Glyph2D
{
    private short leftSideBearing;
    private int advanceWidth;
    private Point[] points;
    private GeneralPath glyphPath;
    
    public Glyph2D(final GlyphDescription gd, final short lsb, final int advance) {
        this.leftSideBearing = 0;
        this.advanceWidth = 0;
        this.leftSideBearing = lsb;
        this.advanceWidth = advance;
        this.describe(gd);
    }
    
    public int getAdvanceWidth() {
        return this.advanceWidth;
    }
    
    public short getLeftSideBearing() {
        return this.leftSideBearing;
    }
    
    private void describe(final GlyphDescription gd) {
        int endPtIndex = 0;
        this.points = new Point[gd.getPointCount()];
        for (int i = 0; i < gd.getPointCount(); ++i) {
            final boolean endPt = gd.getEndPtOfContours(endPtIndex) == i;
            if (endPt) {
                ++endPtIndex;
            }
            this.points[i] = new Point(gd.getXCoordinate(i), gd.getYCoordinate(i), (gd.getFlags(i) & 0x1) != 0x0, endPt);
        }
    }
    
    public GeneralPath getPath() {
        if (this.glyphPath == null) {
            this.glyphPath = this.calculatePath();
        }
        return this.glyphPath;
    }
    
    private GeneralPath calculatePath() {
        final GeneralPath path = new GeneralPath();
        final int numberOfPoints = this.points.length;
        int i = 0;
        boolean endOfContour = true;
        Point startingPoint = null;
        Point lastCtrlPoint = null;
        while (i < numberOfPoints) {
            final Point point = this.points[i % numberOfPoints];
            final Point nextPoint1 = this.points[(i + 1) % numberOfPoints];
            final Point nextPoint2 = this.points[(i + 2) % numberOfPoints];
            if (endOfContour) {
                if (point.endOfContour) {
                    ++i;
                    continue;
                }
                path.moveTo((float)point.x, (float)point.y);
                endOfContour = false;
                startingPoint = point;
            }
            if (point.onCurve && nextPoint1.onCurve) {
                path.lineTo((float)nextPoint1.x, (float)nextPoint1.y);
                ++i;
                if (!point.endOfContour && !nextPoint1.endOfContour) {
                    continue;
                }
                endOfContour = true;
                path.closePath();
            }
            else if (point.onCurve && !nextPoint1.onCurve && nextPoint2.onCurve) {
                if (nextPoint1.endOfContour) {
                    path.quadTo((float)nextPoint1.x, (float)nextPoint1.y, (float)startingPoint.x, (float)startingPoint.y);
                }
                else {
                    path.quadTo((float)nextPoint1.x, (float)nextPoint1.y, (float)nextPoint2.x, (float)nextPoint2.y);
                }
                if (nextPoint1.endOfContour || nextPoint2.endOfContour) {
                    endOfContour = true;
                    path.closePath();
                }
                i += 2;
                lastCtrlPoint = nextPoint1;
            }
            else if (point.onCurve && !nextPoint1.onCurve && !nextPoint2.onCurve) {
                final int endPointX = this.midValue(nextPoint1.x, nextPoint2.x);
                final int endPointY = this.midValue(nextPoint1.y, nextPoint2.y);
                path.quadTo((float)nextPoint1.x, (float)nextPoint1.y, (float)endPointX, (float)endPointY);
                if (point.endOfContour || nextPoint1.endOfContour || nextPoint2.endOfContour) {
                    path.quadTo((float)nextPoint2.x, (float)nextPoint2.y, (float)startingPoint.x, (float)startingPoint.y);
                    endOfContour = true;
                    path.closePath();
                }
                i += 2;
                lastCtrlPoint = nextPoint1;
            }
            else if (!point.onCurve && !nextPoint1.onCurve) {
                final Point2D lastEndPoint = path.getCurrentPoint();
                lastCtrlPoint = new Point(this.midValue(lastCtrlPoint.x, (int)lastEndPoint.getX()), this.midValue(lastCtrlPoint.y, (int)lastEndPoint.getY()));
                final int endPointX2 = this.midValue((int)lastEndPoint.getX(), nextPoint1.x);
                final int endPointY2 = this.midValue((int)lastEndPoint.getY(), nextPoint1.y);
                path.quadTo((float)lastCtrlPoint.x, (float)lastCtrlPoint.y, (float)endPointX2, (float)endPointY2);
                if (point.endOfContour || nextPoint1.endOfContour) {
                    endOfContour = true;
                    path.closePath();
                }
                ++i;
            }
            else {
                if (point.onCurve || !nextPoint1.onCurve) {
                    System.err.println("Unknown glyph command!!");
                    break;
                }
                path.quadTo((float)point.x, (float)point.y, (float)nextPoint1.x, (float)nextPoint1.y);
                if (point.endOfContour || nextPoint1.endOfContour) {
                    endOfContour = true;
                    path.closePath();
                }
                ++i;
                lastCtrlPoint = point;
            }
        }
        return path;
    }
    
    private int midValue(final int a, final int b) {
        return a + (b - a) / 2;
    }
    
    private class Point
    {
        public int x;
        public int y;
        public boolean onCurve;
        public boolean endOfContour;
        
        public Point(final int xValue, final int yValue, final boolean onCurveValue, final boolean endOfContourValue) {
            this.x = 0;
            this.y = 0;
            this.onCurve = true;
            this.endOfContour = false;
            this.x = xValue;
            this.y = yValue;
            this.onCurve = onCurveValue;
            this.endOfContour = endOfContourValue;
        }
        
        public Point(final Glyph2D glyph2D, final int xValue, final int yValue) {
            this(glyph2D, xValue, yValue, false, false);
        }
    }
}
