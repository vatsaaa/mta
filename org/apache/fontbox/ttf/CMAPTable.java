// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class CMAPTable extends TTFTable
{
    public static final String TAG = "cmap";
    public static final int PLATFORM_WINDOWS = 3;
    public static final int ENCODING_SYMBOL = 0;
    public static final int ENCODING_UNICODE = 1;
    public static final int ENCODING_SHIFT_JIS = 2;
    public static final int ENCODING_BIG5 = 3;
    public static final int ENCODING_PRC = 4;
    public static final int ENCODING_WANSUNG = 5;
    public static final int ENCODING_JOHAB = 6;
    private CMAPEncodingEntry[] cmaps;
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        final int version = data.readUnsignedShort();
        final int numberOfTables = data.readUnsignedShort();
        this.cmaps = new CMAPEncodingEntry[numberOfTables];
        for (int i = 0; i < numberOfTables; ++i) {
            final CMAPEncodingEntry cmap = new CMAPEncodingEntry();
            cmap.initData(ttf, data);
            this.cmaps[i] = cmap;
        }
        for (int i = 0; i < numberOfTables; ++i) {
            this.cmaps[i].initSubtable(ttf, data);
        }
    }
    
    public CMAPEncodingEntry[] getCmaps() {
        return this.cmaps;
    }
    
    public void setCmaps(final CMAPEncodingEntry[] cmapsValue) {
        this.cmaps = cmapsValue;
    }
}
