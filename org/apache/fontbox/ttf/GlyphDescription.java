// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

public interface GlyphDescription
{
    int getEndPtOfContours(final int p0);
    
    byte getFlags(final int p0);
    
    short getXCoordinate(final int p0);
    
    short getYCoordinate(final int p0);
    
    boolean isComposite();
    
    int getPointCount();
    
    int getContourCount();
    
    void resolve();
}
