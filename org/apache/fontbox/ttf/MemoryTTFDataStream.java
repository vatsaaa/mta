// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class MemoryTTFDataStream extends TTFDataStream
{
    private byte[] data;
    private int currentPosition;
    
    public MemoryTTFDataStream(final InputStream is) throws IOException {
        this.data = null;
        this.currentPosition = 0;
        try {
            final ByteArrayOutputStream output = new ByteArrayOutputStream(is.available());
            final byte[] buffer = new byte[1024];
            int amountRead = 0;
            while ((amountRead = is.read(buffer)) != -1) {
                output.write(buffer, 0, amountRead);
            }
            this.data = output.toByteArray();
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    @Override
    public long readLong() throws IOException {
        return ((long)this.readSignedInt() << 32) + ((long)this.readSignedInt() & 0xFFFFFFFFL);
    }
    
    public int readSignedInt() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        final int ch3 = this.read();
        final int ch4 = this.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0) {
            throw new EOFException();
        }
        return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0);
    }
    
    @Override
    public int read() throws IOException {
        int retval = -1;
        if (this.currentPosition < this.data.length) {
            retval = this.data[this.currentPosition];
        }
        ++this.currentPosition;
        return (retval + 256) % 256;
    }
    
    @Override
    public int readUnsignedShort() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (ch1 << 8) + (ch2 << 0);
    }
    
    @Override
    public short readSignedShort() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (short)((ch1 << 8) + (ch2 << 0));
    }
    
    @Override
    public void close() throws IOException {
        this.data = null;
    }
    
    @Override
    public void seek(final long pos) throws IOException {
        this.currentPosition = (int)pos;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        if (this.currentPosition < this.data.length) {
            final int amountRead = Math.min(len, this.data.length - this.currentPosition);
            System.arraycopy(this.data, this.currentPosition, b, off, amountRead);
            this.currentPosition += amountRead;
            return amountRead;
        }
        return -1;
    }
    
    @Override
    public long getCurrentPosition() throws IOException {
        return this.currentPosition;
    }
    
    @Override
    public InputStream getOriginalData() throws IOException {
        return new ByteArrayInputStream(this.data);
    }
}
