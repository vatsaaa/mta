// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class GlyphTable extends TTFTable
{
    public static final String TAG = "glyf";
    private GlyphData[] glyphs;
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        final MaximumProfileTable maxp = ttf.getMaximumProfile();
        final IndexToLocationTable loc = ttf.getIndexToLocation();
        final long[] offsets = loc.getOffsets();
        final int numGlyphs = maxp.getNumGlyphs();
        final long endOfGlyphs = offsets[numGlyphs];
        long currentOffset = -1L;
        final long offset = this.getOffset();
        this.glyphs = new GlyphData[numGlyphs];
        for (int i = 0; i < numGlyphs && endOfGlyphs != offsets[i]; ++i) {
            if (currentOffset != offsets[i]) {
                currentOffset = offsets[i];
                this.glyphs[i] = new GlyphData();
                data.seek(offset + offsets[i]);
                this.glyphs[i].initData(ttf, data);
            }
        }
        for (int i = 0; i < numGlyphs; ++i) {
            final GlyphData glyph = this.glyphs[i];
            if (glyph != null && glyph.getDescription().isComposite()) {
                glyph.getDescription().resolve();
            }
        }
    }
    
    public GlyphData[] getGlyphs() {
        return this.glyphs;
    }
    
    public void setGlyphs(final GlyphData[] glyphsValue) {
        this.glyphs = glyphsValue;
    }
}
