// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class HorizontalHeaderTable extends TTFTable
{
    public static final String TAG = "hhea";
    private float version;
    private short ascender;
    private short descender;
    private short lineGap;
    private int advanceWidthMax;
    private short minLeftSideBearing;
    private short minRightSideBearing;
    private short xMaxExtent;
    private short caretSlopeRise;
    private short caretSlopeRun;
    private short reserved1;
    private short reserved2;
    private short reserved3;
    private short reserved4;
    private short reserved5;
    private short metricDataFormat;
    private int numberOfHMetrics;
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        this.version = data.read32Fixed();
        this.ascender = data.readSignedShort();
        this.descender = data.readSignedShort();
        this.lineGap = data.readSignedShort();
        this.advanceWidthMax = data.readUnsignedShort();
        this.minLeftSideBearing = data.readSignedShort();
        this.minRightSideBearing = data.readSignedShort();
        this.xMaxExtent = data.readSignedShort();
        this.caretSlopeRise = data.readSignedShort();
        this.caretSlopeRun = data.readSignedShort();
        this.reserved1 = data.readSignedShort();
        this.reserved2 = data.readSignedShort();
        this.reserved3 = data.readSignedShort();
        this.reserved4 = data.readSignedShort();
        this.reserved5 = data.readSignedShort();
        this.metricDataFormat = data.readSignedShort();
        this.numberOfHMetrics = data.readUnsignedShort();
    }
    
    public int getAdvanceWidthMax() {
        return this.advanceWidthMax;
    }
    
    public void setAdvanceWidthMax(final int advanceWidthMaxValue) {
        this.advanceWidthMax = advanceWidthMaxValue;
    }
    
    public short getAscender() {
        return this.ascender;
    }
    
    public void setAscender(final short ascenderValue) {
        this.ascender = ascenderValue;
    }
    
    public short getCaretSlopeRise() {
        return this.caretSlopeRise;
    }
    
    public void setCaretSlopeRise(final short caretSlopeRiseValue) {
        this.caretSlopeRise = caretSlopeRiseValue;
    }
    
    public short getCaretSlopeRun() {
        return this.caretSlopeRun;
    }
    
    public void setCaretSlopeRun(final short caretSlopeRunValue) {
        this.caretSlopeRun = caretSlopeRunValue;
    }
    
    public short getDescender() {
        return this.descender;
    }
    
    public void setDescender(final short descenderValue) {
        this.descender = descenderValue;
    }
    
    public short getLineGap() {
        return this.lineGap;
    }
    
    public void setLineGap(final short lineGapValue) {
        this.lineGap = lineGapValue;
    }
    
    public short getMetricDataFormat() {
        return this.metricDataFormat;
    }
    
    public void setMetricDataFormat(final short metricDataFormatValue) {
        this.metricDataFormat = metricDataFormatValue;
    }
    
    public short getMinLeftSideBearing() {
        return this.minLeftSideBearing;
    }
    
    public void setMinLeftSideBearing(final short minLeftSideBearingValue) {
        this.minLeftSideBearing = minLeftSideBearingValue;
    }
    
    public short getMinRightSideBearing() {
        return this.minRightSideBearing;
    }
    
    public void setMinRightSideBearing(final short minRightSideBearingValue) {
        this.minRightSideBearing = minRightSideBearingValue;
    }
    
    public int getNumberOfHMetrics() {
        return this.numberOfHMetrics;
    }
    
    public void setNumberOfHMetrics(final int numberOfHMetricsValue) {
        this.numberOfHMetrics = numberOfHMetricsValue;
    }
    
    public short getReserved1() {
        return this.reserved1;
    }
    
    public void setReserved1(final short reserved1Value) {
        this.reserved1 = reserved1Value;
    }
    
    public short getReserved2() {
        return this.reserved2;
    }
    
    public void setReserved2(final short reserved2Value) {
        this.reserved2 = reserved2Value;
    }
    
    public short getReserved3() {
        return this.reserved3;
    }
    
    public void setReserved3(final short reserved3Value) {
        this.reserved3 = reserved3Value;
    }
    
    public short getReserved4() {
        return this.reserved4;
    }
    
    public void setReserved4(final short reserved4Value) {
        this.reserved4 = reserved4Value;
    }
    
    public short getReserved5() {
        return this.reserved5;
    }
    
    public void setReserved5(final short reserved5Value) {
        this.reserved5 = reserved5Value;
    }
    
    public float getVersion() {
        return this.version;
    }
    
    public void setVersion(final float versionValue) {
        this.version = versionValue;
    }
    
    public short getXMaxExtent() {
        return this.xMaxExtent;
    }
    
    public void setXMaxExtent(final short maxExtentValue) {
        this.xMaxExtent = maxExtentValue;
    }
}
