// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;

public class CIDFontType2Parser extends AbstractTTFParser
{
    public CIDFontType2Parser() {
        super(false);
    }
    
    public CIDFontType2Parser(final boolean isEmbedded) {
        super(isEmbedded);
    }
}
