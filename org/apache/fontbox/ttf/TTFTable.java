// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class TTFTable
{
    private String tag;
    private long checkSum;
    private long offset;
    private long length;
    
    public long getCheckSum() {
        return this.checkSum;
    }
    
    public void setCheckSum(final long checkSumValue) {
        this.checkSum = checkSumValue;
    }
    
    public long getLength() {
        return this.length;
    }
    
    public void setLength(final long lengthValue) {
        this.length = lengthValue;
    }
    
    public long getOffset() {
        return this.offset;
    }
    
    public void setOffset(final long offsetValue) {
        this.offset = offsetValue;
    }
    
    public String getTag() {
        return this.tag;
    }
    
    public void setTag(final String tagValue) {
        this.tag = tagValue;
    }
    
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
    }
}
