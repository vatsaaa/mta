// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class NameRecord
{
    public static final int PLATFORM_APPLE_UNICODE = 0;
    public static final int PLATFORM_MACINTOSH = 1;
    public static final int PLATFORM_ISO = 2;
    public static final int PLATFORM_WINDOWS = 3;
    public static final int PLATFORM_ENCODING_WINDOWS_UNDEFINED = 0;
    public static final int PLATFORM_ENCODING_WINDOWS_UNICODE = 1;
    public static final int NAME_COPYRIGHT = 0;
    public static final int NAME_FONT_FAMILY_NAME = 1;
    public static final int NAME_FONT_SUB_FAMILY_NAME = 2;
    public static final int NAME_UNIQUE_FONT_ID = 3;
    public static final int NAME_FULL_FONT_NAME = 4;
    public static final int NAME_VERSION = 5;
    public static final int NAME_POSTSCRIPT_NAME = 6;
    public static final int NAME_TRADEMARK = 7;
    private int platformId;
    private int platformEncodingId;
    private int languageId;
    private int nameId;
    private int stringLength;
    private int stringOffset;
    private String string;
    
    public int getStringLength() {
        return this.stringLength;
    }
    
    public void setStringLength(final int stringLengthValue) {
        this.stringLength = stringLengthValue;
    }
    
    public int getStringOffset() {
        return this.stringOffset;
    }
    
    public void setStringOffset(final int stringOffsetValue) {
        this.stringOffset = stringOffsetValue;
    }
    
    public int getLanguageId() {
        return this.languageId;
    }
    
    public void setLanguageId(final int languageIdValue) {
        this.languageId = languageIdValue;
    }
    
    public int getNameId() {
        return this.nameId;
    }
    
    public void setNameId(final int nameIdValue) {
        this.nameId = nameIdValue;
    }
    
    public int getPlatformEncodingId() {
        return this.platformEncodingId;
    }
    
    public void setPlatformEncodingId(final int platformEncodingIdValue) {
        this.platformEncodingId = platformEncodingIdValue;
    }
    
    public int getPlatformId() {
        return this.platformId;
    }
    
    public void setPlatformId(final int platformIdValue) {
        this.platformId = platformIdValue;
    }
    
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        this.platformId = data.readUnsignedShort();
        this.platformEncodingId = data.readUnsignedShort();
        this.languageId = data.readUnsignedShort();
        this.nameId = data.readUnsignedShort();
        this.stringLength = data.readUnsignedShort();
        this.stringOffset = data.readUnsignedShort();
    }
    
    @Override
    public String toString() {
        return "platform=" + this.platformId + " pEncoding=" + this.platformEncodingId + " language=" + this.languageId + " name=" + this.nameId;
    }
    
    public String getString() {
        return this.string;
    }
    
    public void setString(final String stringValue) {
        this.string = stringValue;
    }
}
