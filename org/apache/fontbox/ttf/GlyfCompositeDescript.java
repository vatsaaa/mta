// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.util.Iterator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GlyfCompositeDescript extends GlyfDescript
{
    private List<GlyfCompositeComp> components;
    private GlyphData[] glyphs;
    private boolean beingResolved;
    private boolean resolved;
    
    public GlyfCompositeDescript(final TTFDataStream bais, final GlyphTable glyphTable) throws IOException {
        super((short)(-1), bais);
        this.components = new ArrayList<GlyfCompositeComp>();
        this.glyphs = null;
        this.beingResolved = false;
        this.resolved = false;
        this.glyphs = glyphTable.getGlyphs();
        GlyfCompositeComp comp;
        do {
            comp = new GlyfCompositeComp(bais);
            this.components.add(comp);
        } while ((comp.getFlags() & 0x20) != 0x0);
        if ((comp.getFlags() & 0x100) != 0x0) {
            this.readInstructions(bais, bais.read() << 8 | bais.read());
        }
    }
    
    @Override
    public void resolve() {
        if (this.resolved) {
            return;
        }
        if (this.beingResolved) {
            System.err.println("Circular reference in GlyfCompositeDesc");
            return;
        }
        this.beingResolved = true;
        int firstIndex = 0;
        int firstContour = 0;
        for (final GlyfCompositeComp comp : this.components) {
            comp.setFirstIndex(firstIndex);
            comp.setFirstContour(firstContour);
            final GlyphDescription desc = this.getGlypDescription(comp.getGlyphIndex());
            if (desc != null) {
                desc.resolve();
                firstIndex += desc.getPointCount();
                firstContour += desc.getContourCount();
            }
        }
        this.resolved = true;
        this.beingResolved = false;
    }
    
    public int getEndPtOfContours(final int i) {
        final GlyfCompositeComp c = this.getCompositeCompEndPt(i);
        if (c != null) {
            final GlyphDescription gd = this.getGlypDescription(c.getGlyphIndex());
            return gd.getEndPtOfContours(i - c.getFirstContour()) + c.getFirstIndex();
        }
        return 0;
    }
    
    public byte getFlags(final int i) {
        final GlyfCompositeComp c = this.getCompositeComp(i);
        if (c != null) {
            final GlyphDescription gd = this.getGlypDescription(c.getGlyphIndex());
            return gd.getFlags(i - c.getFirstIndex());
        }
        return 0;
    }
    
    public short getXCoordinate(final int i) {
        final GlyfCompositeComp c = this.getCompositeComp(i);
        if (c != null) {
            final GlyphDescription gd = this.getGlypDescription(c.getGlyphIndex());
            final int n = i - c.getFirstIndex();
            final int x = gd.getXCoordinate(n);
            final int y = gd.getYCoordinate(n);
            short x2 = (short)c.scaleX(x, y);
            x2 += (short)c.getXTranslate();
            return x2;
        }
        return 0;
    }
    
    public short getYCoordinate(final int i) {
        final GlyfCompositeComp c = this.getCompositeComp(i);
        if (c != null) {
            final GlyphDescription gd = this.getGlypDescription(c.getGlyphIndex());
            final int n = i - c.getFirstIndex();
            final int x = gd.getXCoordinate(n);
            final int y = gd.getYCoordinate(n);
            short y2 = (short)c.scaleY(x, y);
            y2 += (short)c.getYTranslate();
            return y2;
        }
        return 0;
    }
    
    public boolean isComposite() {
        return true;
    }
    
    public int getPointCount() {
        if (!this.resolved) {
            System.err.println("getPointCount called on unresolved GlyfCompositeDescript");
        }
        final GlyfCompositeComp c = this.components.get(this.components.size() - 1);
        return c.getFirstIndex() + this.getGlypDescription(c.getGlyphIndex()).getPointCount();
    }
    
    @Override
    public int getContourCount() {
        if (!this.resolved) {
            System.err.println("getContourCount called on unresolved GlyfCompositeDescript");
        }
        final GlyfCompositeComp c = this.components.get(this.components.size() - 1);
        return c.getFirstContour() + this.getGlypDescription(c.getGlyphIndex()).getContourCount();
    }
    
    public int getComponentCount() {
        return this.components.size();
    }
    
    private GlyfCompositeComp getCompositeComp(final int i) {
        for (int n = 0; n < this.components.size(); ++n) {
            final GlyfCompositeComp c = this.components.get(n);
            final GlyphDescription gd = this.getGlypDescription(c.getGlyphIndex());
            if (c.getFirstIndex() <= i && i < c.getFirstIndex() + gd.getPointCount()) {
                return c;
            }
        }
        return null;
    }
    
    private GlyfCompositeComp getCompositeCompEndPt(final int i) {
        for (int j = 0; j < this.components.size(); ++j) {
            final GlyfCompositeComp c = this.components.get(j);
            final GlyphDescription gd = this.getGlypDescription(c.getGlyphIndex());
            if (c.getFirstContour() <= i && i < c.getFirstContour() + gd.getContourCount()) {
                return c;
            }
        }
        return null;
    }
    
    private GlyphDescription getGlypDescription(final int index) {
        if (this.glyphs != null && index < this.glyphs.length) {
            final GlyphData glyph = this.glyphs[index];
            if (glyph != null) {
                return glyph.getDescription();
            }
        }
        return null;
    }
}
