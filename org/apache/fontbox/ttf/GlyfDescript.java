// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public abstract class GlyfDescript implements GlyphDescription
{
    public static final byte ON_CURVE = 1;
    public static final byte X_SHORT_VECTOR = 2;
    public static final byte Y_SHORT_VECTOR = 4;
    public static final byte REPEAT = 8;
    public static final byte X_DUAL = 16;
    public static final byte Y_DUAL = 32;
    private int[] instructions;
    private int contourCount;
    
    protected GlyfDescript(final short numberOfContours, final TTFDataStream bais) throws IOException {
        this.contourCount = numberOfContours;
    }
    
    public void resolve() {
    }
    
    public int getContourCount() {
        return this.contourCount;
    }
    
    public int[] getInstructions() {
        return this.instructions;
    }
    
    protected void readInstructions(final TTFDataStream bais, final int count) throws IOException {
        this.instructions = new int[count];
        this.instructions = bais.readUnsignedByteArray(count);
    }
}
