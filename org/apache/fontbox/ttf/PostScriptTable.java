// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;
import org.apache.fontbox.encoding.MacRomanEncoding;

public class PostScriptTable extends TTFTable
{
    private float formatType;
    private float italicAngle;
    private short underlinePosition;
    private short underlineThickness;
    private long isFixedPitch;
    private long minMemType42;
    private long maxMemType42;
    private long mimMemType1;
    private long maxMemType1;
    private String[] glyphNames;
    public static final String TAG = "post";
    
    public PostScriptTable() {
        this.glyphNames = null;
    }
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        final MaximumProfileTable maxp = ttf.getMaximumProfile();
        this.formatType = data.read32Fixed();
        this.italicAngle = data.read32Fixed();
        this.underlinePosition = data.readSignedShort();
        this.underlineThickness = data.readSignedShort();
        this.isFixedPitch = data.readUnsignedInt();
        this.minMemType42 = data.readUnsignedInt();
        this.maxMemType42 = data.readUnsignedInt();
        this.mimMemType1 = data.readUnsignedInt();
        this.maxMemType1 = data.readUnsignedInt();
        final MacRomanEncoding encoding = new MacRomanEncoding();
        if (this.formatType == 1.0f) {
            this.glyphNames = new String[258];
            for (int i = 0; i < this.glyphNames.length; ++i) {
                final String name = encoding.getName(i);
                if (name != null) {
                    this.glyphNames[i] = name;
                }
            }
        }
        else if (this.formatType == 2.0f) {
            final int numGlyphs = data.readUnsignedShort();
            final int[] glyphNameIndex = new int[numGlyphs];
            this.glyphNames = new String[numGlyphs];
            int maxIndex = Integer.MIN_VALUE;
            for (int j = 0; j < numGlyphs; ++j) {
                final int index = data.readUnsignedShort();
                if ((glyphNameIndex[j] = index) <= 32767) {
                    maxIndex = Math.max(maxIndex, index);
                }
            }
            String[] nameArray = null;
            if (maxIndex >= 258) {
                nameArray = new String[maxIndex - 258 + 1];
                for (int k = 0; k < maxIndex - 258 + 1; ++k) {
                    final int numberOfChars = data.read();
                    nameArray[k] = data.readString(numberOfChars);
                }
            }
            for (int k = 0; k < numGlyphs; ++k) {
                final int index2 = glyphNameIndex[k];
                if (index2 < 258) {
                    this.glyphNames[k] = encoding.getName(index2);
                }
                else if (index2 >= 258 && index2 <= 32767) {
                    this.glyphNames[k] = nameArray[index2 - 258];
                }
                else {
                    this.glyphNames[k] = ".undefined";
                }
            }
        }
        else if (this.formatType == 2.5f) {
            final int[] glyphNameIndex2 = new int[maxp.getNumGlyphs()];
            for (int l = 0; l < glyphNameIndex2.length; ++l) {
                final int offset = data.readSignedByte();
                glyphNameIndex2[l] = l + 1 + offset;
            }
            this.glyphNames = new String[glyphNameIndex2.length];
            for (int l = 0; l < this.glyphNames.length; ++l) {
                final String name2 = encoding.getName(glyphNameIndex2[l]);
                if (name2 != null) {
                    this.glyphNames[l] = name2;
                }
            }
        }
        else if (this.formatType == 3.0f) {}
    }
    
    public float getFormatType() {
        return this.formatType;
    }
    
    public void setFormatType(final float formatTypeValue) {
        this.formatType = formatTypeValue;
    }
    
    public long getIsFixedPitch() {
        return this.isFixedPitch;
    }
    
    public void setIsFixedPitch(final long isFixedPitchValue) {
        this.isFixedPitch = isFixedPitchValue;
    }
    
    public float getItalicAngle() {
        return this.italicAngle;
    }
    
    public void setItalicAngle(final float italicAngleValue) {
        this.italicAngle = italicAngleValue;
    }
    
    public long getMaxMemType1() {
        return this.maxMemType1;
    }
    
    public void setMaxMemType1(final long maxMemType1Value) {
        this.maxMemType1 = maxMemType1Value;
    }
    
    public long getMaxMemType42() {
        return this.maxMemType42;
    }
    
    public void setMaxMemType42(final long maxMemType42Value) {
        this.maxMemType42 = maxMemType42Value;
    }
    
    public long getMimMemType1() {
        return this.mimMemType1;
    }
    
    public void setMimMemType1(final long mimMemType1Value) {
        this.mimMemType1 = mimMemType1Value;
    }
    
    public long getMinMemType42() {
        return this.minMemType42;
    }
    
    public void setMinMemType42(final long minMemType42Value) {
        this.minMemType42 = minMemType42Value;
    }
    
    public short getUnderlinePosition() {
        return this.underlinePosition;
    }
    
    public void setUnderlinePosition(final short underlinePositionValue) {
        this.underlinePosition = underlinePositionValue;
    }
    
    public short getUnderlineThickness() {
        return this.underlineThickness;
    }
    
    public void setUnderlineThickness(final short underlineThicknessValue) {
        this.underlineThickness = underlineThicknessValue;
    }
    
    public String[] getGlyphNames() {
        return this.glyphNames;
    }
    
    public void setGlyphNames(final String[] glyphNamesValue) {
        this.glyphNames = glyphNamesValue;
    }
}
