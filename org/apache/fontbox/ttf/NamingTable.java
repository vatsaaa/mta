// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NamingTable extends TTFTable
{
    public static final String TAG = "name";
    private List<NameRecord> nameRecords;
    
    public NamingTable() {
        this.nameRecords = new ArrayList<NameRecord>();
    }
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        final int formatSelector = data.readUnsignedShort();
        final int numberOfNameRecords = data.readUnsignedShort();
        final int offsetToStartOfStringStorage = data.readUnsignedShort();
        for (int i = 0; i < numberOfNameRecords; ++i) {
            final NameRecord nr = new NameRecord();
            nr.initData(ttf, data);
            this.nameRecords.add(nr);
        }
        for (int i = 0; i < numberOfNameRecords; ++i) {
            final NameRecord nr = this.nameRecords.get(i);
            data.seek(this.getOffset() + 6L + numberOfNameRecords * 2 * 6 + nr.getStringOffset());
            final int platform = nr.getPlatformId();
            final int encoding = nr.getPlatformEncodingId();
            String charset = "ISO-8859-1";
            if (platform == 3 && encoding == 1) {
                charset = "UTF-16";
            }
            else if (platform == 2) {
                if (encoding == 0) {
                    charset = "US-ASCII";
                }
                else if (encoding == 1) {
                    charset = "ISO-10646-1";
                }
                else if (encoding == 2) {
                    charset = "ISO-8859-1";
                }
            }
            final String string = data.readString(nr.getStringLength(), charset);
            nr.setString(string);
        }
    }
    
    public List<NameRecord> getNameRecords() {
        return this.nameRecords;
    }
}
