// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;

public class MaximumProfileTable extends TTFTable
{
    public static final String TAG = "maxp";
    private float version;
    private int numGlyphs;
    private int maxPoints;
    private int maxContours;
    private int maxCompositePoints;
    private int maxCompositeContours;
    private int maxZones;
    private int maxTwilightPoints;
    private int maxStorage;
    private int maxFunctionDefs;
    private int maxInstructionDefs;
    private int maxStackElements;
    private int maxSizeOfInstructions;
    private int maxComponentElements;
    private int maxComponentDepth;
    
    public int getMaxComponentDepth() {
        return this.maxComponentDepth;
    }
    
    public void setMaxComponentDepth(final int maxComponentDepthValue) {
        this.maxComponentDepth = maxComponentDepthValue;
    }
    
    public int getMaxComponentElements() {
        return this.maxComponentElements;
    }
    
    public void setMaxComponentElements(final int maxComponentElementsValue) {
        this.maxComponentElements = maxComponentElementsValue;
    }
    
    public int getMaxCompositeContours() {
        return this.maxCompositeContours;
    }
    
    public void setMaxCompositeContours(final int maxCompositeContoursValue) {
        this.maxCompositeContours = maxCompositeContoursValue;
    }
    
    public int getMaxCompositePoints() {
        return this.maxCompositePoints;
    }
    
    public void setMaxCompositePoints(final int maxCompositePointsValue) {
        this.maxCompositePoints = maxCompositePointsValue;
    }
    
    public int getMaxContours() {
        return this.maxContours;
    }
    
    public void setMaxContours(final int maxContoursValue) {
        this.maxContours = maxContoursValue;
    }
    
    public int getMaxFunctionDefs() {
        return this.maxFunctionDefs;
    }
    
    public void setMaxFunctionDefs(final int maxFunctionDefsValue) {
        this.maxFunctionDefs = maxFunctionDefsValue;
    }
    
    public int getMaxInstructionDefs() {
        return this.maxInstructionDefs;
    }
    
    public void setMaxInstructionDefs(final int maxInstructionDefsValue) {
        this.maxInstructionDefs = maxInstructionDefsValue;
    }
    
    public int getMaxPoints() {
        return this.maxPoints;
    }
    
    public void setMaxPoints(final int maxPointsValue) {
        this.maxPoints = maxPointsValue;
    }
    
    public int getMaxSizeOfInstructions() {
        return this.maxSizeOfInstructions;
    }
    
    public void setMaxSizeOfInstructions(final int maxSizeOfInstructionsValue) {
        this.maxSizeOfInstructions = maxSizeOfInstructionsValue;
    }
    
    public int getMaxStackElements() {
        return this.maxStackElements;
    }
    
    public void setMaxStackElements(final int maxStackElementsValue) {
        this.maxStackElements = maxStackElementsValue;
    }
    
    public int getMaxStorage() {
        return this.maxStorage;
    }
    
    public void setMaxStorage(final int maxStorageValue) {
        this.maxStorage = maxStorageValue;
    }
    
    public int getMaxTwilightPoints() {
        return this.maxTwilightPoints;
    }
    
    public void setMaxTwilightPoints(final int maxTwilightPointsValue) {
        this.maxTwilightPoints = maxTwilightPointsValue;
    }
    
    public int getMaxZones() {
        return this.maxZones;
    }
    
    public void setMaxZones(final int maxZonesValue) {
        this.maxZones = maxZonesValue;
    }
    
    public int getNumGlyphs() {
        return this.numGlyphs;
    }
    
    public void setNumGlyphs(final int numGlyphsValue) {
        this.numGlyphs = numGlyphsValue;
    }
    
    public float getVersion() {
        return this.version;
    }
    
    public void setVersion(final float versionValue) {
        this.version = versionValue;
    }
    
    @Override
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        this.version = data.read32Fixed();
        this.numGlyphs = data.readUnsignedShort();
        this.maxPoints = data.readUnsignedShort();
        this.maxContours = data.readUnsignedShort();
        this.maxCompositePoints = data.readUnsignedShort();
        this.maxCompositeContours = data.readUnsignedShort();
        this.maxZones = data.readUnsignedShort();
        this.maxTwilightPoints = data.readUnsignedShort();
        this.maxStorage = data.readUnsignedShort();
        this.maxFunctionDefs = data.readUnsignedShort();
        this.maxInstructionDefs = data.readUnsignedShort();
        this.maxStackElements = data.readUnsignedShort();
        this.maxSizeOfInstructions = data.readUnsignedShort();
        this.maxComponentElements = data.readUnsignedShort();
        this.maxComponentDepth = data.readUnsignedShort();
    }
}
