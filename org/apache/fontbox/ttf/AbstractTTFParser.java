// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.File;
import java.io.IOException;

abstract class AbstractTTFParser
{
    protected boolean isEmbedded;
    
    public AbstractTTFParser(final boolean isEmbedded) {
        this.isEmbedded = false;
        this.isEmbedded = isEmbedded;
    }
    
    public TrueTypeFont parseTTF(final String ttfFile) throws IOException {
        final RAFDataStream raf = new RAFDataStream(ttfFile, "r");
        return this.parseTTF(raf);
    }
    
    public TrueTypeFont parseTTF(final File ttfFile) throws IOException {
        final RAFDataStream raf = new RAFDataStream(ttfFile, "r");
        return this.parseTTF(raf);
    }
    
    public TrueTypeFont parseTTF(final InputStream ttfData) throws IOException {
        return this.parseTTF(new MemoryTTFDataStream(ttfData));
    }
    
    public TrueTypeFont parseTTF(final TTFDataStream raf) throws IOException {
        final TrueTypeFont font = new TrueTypeFont(raf);
        font.setVersion(raf.read32Fixed());
        final int numberOfTables = raf.readUnsignedShort();
        final int searchRange = raf.readUnsignedShort();
        final int entrySelector = raf.readUnsignedShort();
        final int rangeShift = raf.readUnsignedShort();
        for (int i = 0; i < numberOfTables; ++i) {
            final TTFTable table = this.readTableDirectory(raf);
            font.addTable(table);
        }
        this.parseTables(font, raf);
        return font;
    }
    
    protected void parseTables(final TrueTypeFont font, final TTFDataStream raf) throws IOException {
        final List<TTFTable> initialized = new ArrayList<TTFTable>();
        final HeaderTable head = font.getHeader();
        if (head == null) {
            throw new IOException("head is mandatory");
        }
        raf.seek(head.getOffset());
        head.initData(font, raf);
        initialized.add(head);
        final HorizontalHeaderTable hh = font.getHorizontalHeader();
        if (hh == null) {
            throw new IOException("hhead is mandatory");
        }
        raf.seek(hh.getOffset());
        hh.initData(font, raf);
        initialized.add(hh);
        final MaximumProfileTable maxp = font.getMaximumProfile();
        if (maxp == null) {
            throw new IOException("maxp is mandatory");
        }
        raf.seek(maxp.getOffset());
        maxp.initData(font, raf);
        initialized.add(maxp);
        final PostScriptTable post = font.getPostScript();
        if (post != null) {
            raf.seek(post.getOffset());
            post.initData(font, raf);
            initialized.add(post);
        }
        else if (!this.isEmbedded) {
            throw new IOException("post is mandatory");
        }
        final IndexToLocationTable loc = font.getIndexToLocation();
        if (loc == null) {
            throw new IOException("loca is mandatory");
        }
        raf.seek(loc.getOffset());
        loc.initData(font, raf);
        initialized.add(loc);
        boolean cvt = false;
        boolean prep = false;
        boolean fpgm = false;
        for (final TTFTable table : font.getTables()) {
            if (!initialized.contains(table)) {
                raf.seek(table.getOffset());
                table.initData(font, raf);
            }
            if (table.getTag().startsWith("cvt")) {
                cvt = true;
            }
            else if ("prep".equals(table.getTag())) {
                prep = true;
            }
            else {
                if (!"fpgm".equals(table.getTag())) {
                    continue;
                }
                fpgm = true;
            }
        }
        if (font.getGlyph() == null) {
            throw new IOException("glyf is mandatory");
        }
        if (font.getNaming() == null && !this.isEmbedded) {
            throw new IOException("name is mandatory");
        }
        if (font.getHorizontalMetrics() == null) {
            throw new IOException("hmtx is mandatory");
        }
        if (this.isEmbedded) {
            if (!fpgm) {
                throw new IOException("fpgm is mandatory");
            }
            if (!prep) {
                throw new IOException("prep is mandatory");
            }
            if (!cvt) {
                throw new IOException("cvt_ is mandatory");
            }
        }
    }
    
    private TTFTable readTableDirectory(final TTFDataStream raf) throws IOException {
        TTFTable retval = null;
        final String tag = raf.readString(4);
        if (tag.equals("cmap")) {
            retval = new CMAPTable();
        }
        else if (tag.equals("glyf")) {
            retval = new GlyphTable();
        }
        else if (tag.equals("head")) {
            retval = new HeaderTable();
        }
        else if (tag.equals("hhea")) {
            retval = new HorizontalHeaderTable();
        }
        else if (tag.equals("hmtx")) {
            retval = new HorizontalMetricsTable();
        }
        else if (tag.equals("loca")) {
            retval = new IndexToLocationTable();
        }
        else if (tag.equals("maxp")) {
            retval = new MaximumProfileTable();
        }
        else if (tag.equals("name")) {
            retval = new NamingTable();
        }
        else if (tag.equals("OS/2")) {
            retval = new OS2WindowsMetricsTable();
        }
        else if (tag.equals("post")) {
            retval = new PostScriptTable();
        }
        else if (tag.equals("DSIG")) {
            retval = new DigitalSignatureTable();
        }
        else {
            retval = new TTFTable();
        }
        retval.setTag(tag);
        retval.setCheckSum(raf.readUnsignedInt());
        retval.setOffset(raf.readUnsignedInt());
        retval.setLength(raf.readUnsignedInt());
        return retval;
    }
}
