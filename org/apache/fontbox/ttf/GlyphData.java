// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.IOException;
import org.apache.fontbox.util.BoundingBox;

public class GlyphData
{
    private short xMin;
    private short yMin;
    private short xMax;
    private short yMax;
    private BoundingBox boundingBox;
    private short numberOfContours;
    private GlyfDescript glyphDescription;
    
    public GlyphData() {
        this.boundingBox = null;
        this.glyphDescription = null;
    }
    
    public void initData(final TrueTypeFont ttf, final TTFDataStream data) throws IOException {
        this.numberOfContours = data.readSignedShort();
        this.xMin = data.readSignedShort();
        this.yMin = data.readSignedShort();
        this.xMax = data.readSignedShort();
        this.yMax = data.readSignedShort();
        this.boundingBox = new BoundingBox(this.xMin, this.yMin, this.xMax, this.yMax);
        if (this.numberOfContours >= 0) {
            this.glyphDescription = new GlyfSimpleDescript(this.numberOfContours, data);
        }
        else {
            this.glyphDescription = new GlyfCompositeDescript(data, ttf.getGlyph());
        }
    }
    
    public BoundingBox getBoundingBox() {
        return this.boundingBox;
    }
    
    public void setBoundingBox(final BoundingBox boundingBoxValue) {
        this.boundingBox = boundingBoxValue;
    }
    
    public short getNumberOfContours() {
        return this.numberOfContours;
    }
    
    public void setNumberOfContours(final short numberOfContoursValue) {
        this.numberOfContours = numberOfContoursValue;
    }
    
    public GlyphDescription getDescription() {
        return this.glyphDescription;
    }
    
    public short getXMaximum() {
        return this.xMax;
    }
    
    public short getXMinimum() {
        return this.xMin;
    }
    
    public short getYMaximum() {
        return this.yMax;
    }
    
    public short getYMinimum() {
        return this.yMin;
    }
}
