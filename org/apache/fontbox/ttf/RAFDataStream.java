// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.RandomAccessFile;

public class RAFDataStream extends TTFDataStream
{
    private RandomAccessFile raf;
    private File ttfFile;
    
    public RAFDataStream(final String name, final String mode) throws FileNotFoundException {
        this(new File(name), mode);
    }
    
    public RAFDataStream(final File file, final String mode) throws FileNotFoundException {
        this.raf = null;
        this.ttfFile = null;
        this.raf = new RandomAccessFile(file, mode);
        this.ttfFile = file;
    }
    
    @Override
    public short readSignedShort() throws IOException {
        return this.raf.readShort();
    }
    
    @Override
    public long getCurrentPosition() throws IOException {
        return this.raf.getFilePointer();
    }
    
    @Override
    public void close() throws IOException {
        this.raf.close();
        this.raf = null;
    }
    
    @Override
    public int read() throws IOException {
        return this.raf.read();
    }
    
    @Override
    public int readUnsignedShort() throws IOException {
        return this.raf.readUnsignedShort();
    }
    
    @Override
    public long readLong() throws IOException {
        return this.raf.readLong();
    }
    
    @Override
    public void seek(final long pos) throws IOException {
        this.raf.seek(pos);
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        return this.raf.read(b, off, len);
    }
    
    @Override
    public InputStream getOriginalData() throws IOException {
        return new FileInputStream(this.ttfFile);
    }
}
