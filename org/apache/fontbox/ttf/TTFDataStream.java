// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.ttf;

import java.io.InputStream;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.io.EOFException;
import java.io.IOException;

public abstract class TTFDataStream
{
    public float read32Fixed() throws IOException {
        float retval = 0.0f;
        retval = this.readSignedShort();
        retval += (float)(this.readUnsignedShort() / 65536.0);
        return retval;
    }
    
    public String readString(final int length) throws IOException {
        return this.readString(length, "ISO-8859-1");
    }
    
    public String readString(final int length, final String charset) throws IOException {
        final byte[] buffer = this.read(length);
        return new String(buffer, charset);
    }
    
    public abstract int read() throws IOException;
    
    public abstract long readLong() throws IOException;
    
    public int readSignedByte() throws IOException {
        final int signedByte = this.read();
        return (signedByte < 127) ? signedByte : (signedByte - 256);
    }
    
    public long readUnsignedInt() throws IOException {
        final long byte1 = this.read();
        final long byte2 = this.read();
        final long byte3 = this.read();
        final long byte4 = this.read();
        if (byte4 < 0L) {
            throw new EOFException();
        }
        return (byte1 << 24) + (byte2 << 16) + (byte3 << 8) + (byte4 << 0);
    }
    
    public abstract int readUnsignedShort() throws IOException;
    
    public int[] readUnsignedByteArray(final int length) throws IOException {
        final int[] array = new int[length];
        for (int i = 0; i < length; ++i) {
            array[i] = this.read();
        }
        return array;
    }
    
    public int[] readUnsignedShortArray(final int length) throws IOException {
        final int[] array = new int[length];
        for (int i = 0; i < length; ++i) {
            array[i] = this.readUnsignedShort();
        }
        return array;
    }
    
    public abstract short readSignedShort() throws IOException;
    
    public Calendar readInternationalDate() throws IOException {
        final long secondsSince1904 = this.readLong();
        final GregorianCalendar cal = new GregorianCalendar(1904, 0, 1);
        long millisFor1904 = cal.getTimeInMillis();
        millisFor1904 += secondsSince1904 * 1000L;
        cal.setTimeInMillis(millisFor1904);
        return cal;
    }
    
    public abstract void close() throws IOException;
    
    public abstract void seek(final long p0) throws IOException;
    
    public byte[] read(final int numberOfBytes) throws IOException {
        byte[] data;
        int amountRead;
        int totalAmountRead;
        for (data = new byte[numberOfBytes], amountRead = 0, totalAmountRead = 0; totalAmountRead < numberOfBytes && (amountRead = this.read(data, totalAmountRead, numberOfBytes - totalAmountRead)) != -1; totalAmountRead += amountRead) {}
        if (totalAmountRead == numberOfBytes) {
            return data;
        }
        throw new IOException("Unexpected end of TTF stream reached");
    }
    
    public abstract int read(final byte[] p0, final int p1, final int p2) throws IOException;
    
    public abstract long getCurrentPosition() throws IOException;
    
    public abstract InputStream getOriginalData() throws IOException;
}
