// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.pfb;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class PfbParser
{
    private static final int PFB_HEADER_LENGTH = 18;
    private static final int START_MARKER = 128;
    private static final int ASCII_MARKER = 1;
    private static final int BINARY_MARKER = 2;
    private static final int[] PFB_RECORDS;
    private static final int BUFFER_SIZE = 65535;
    private byte[] pfbdata;
    private int[] lengths;
    
    public PfbParser(final String filename) throws IOException {
        this(new BufferedInputStream(new FileInputStream(filename), 65535));
    }
    
    public PfbParser(final InputStream in) throws IOException {
        final byte[] pfb = this.readPfbInput(in);
        this.parsePfb(pfb);
    }
    
    private void parsePfb(final byte[] pfb) throws IOException {
        final ByteArrayInputStream in = new ByteArrayInputStream(pfb);
        this.pfbdata = new byte[pfb.length - 18];
        this.lengths = new int[PfbParser.PFB_RECORDS.length];
        int pointer = 0;
        for (int records = 0; records < PfbParser.PFB_RECORDS.length; ++records) {
            if (in.read() != 128) {
                throw new IOException("Start marker missing");
            }
            if (in.read() != PfbParser.PFB_RECORDS[records]) {
                throw new IOException("Incorrect record type");
            }
            int size = in.read();
            size += in.read() << 8;
            size += in.read() << 16;
            size += in.read() << 24;
            this.lengths[records] = size;
            final int got = in.read(this.pfbdata, pointer, size);
            if (got < 0) {
                throw new EOFException();
            }
            pointer += got;
        }
    }
    
    private byte[] readPfbInput(final InputStream in) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final byte[] tmpbuf = new byte[65535];
        int amountRead = -1;
        while ((amountRead = in.read(tmpbuf)) != -1) {
            out.write(tmpbuf, 0, amountRead);
        }
        return out.toByteArray();
    }
    
    public int[] getLengths() {
        return this.lengths;
    }
    
    public byte[] getPfbdata() {
        return this.pfbdata;
    }
    
    public InputStream getInputStream() {
        return new ByteArrayInputStream(this.pfbdata);
    }
    
    public int size() {
        return this.pfbdata.length;
    }
    
    static {
        PFB_RECORDS = new int[] { 1, 2, 1 };
    }
}
