// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.fontbox.encoding;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class Encoding
{
    private static final String NOTDEF = ".notdef";
    protected Map<Integer, String> codeToName;
    protected Map<String, Integer> nameToCode;
    private static final Map<String, String> NAME_TO_CHARACTER;
    private static final Map<String, String> CHARACTER_TO_NAME;
    
    public Encoding() {
        this.codeToName = new HashMap<Integer, String>();
        this.nameToCode = new HashMap<String, Integer>();
    }
    
    protected void addCharacterEncoding(final int code, final String name) {
        this.codeToName.put(code, name);
        this.nameToCode.put(name, code);
    }
    
    public int getCode(final String name) throws IOException {
        final Integer code = this.nameToCode.get(name);
        if (code == null) {
            throw new IOException("No character code for character name '" + name + "'");
        }
        return code;
    }
    
    public String getName(final int code) throws IOException {
        String name = this.codeToName.get(code);
        if (name == null) {
            name = ".notdef";
        }
        return name;
    }
    
    public String getNameFromCharacter(final char c) throws IOException {
        final String name = Encoding.CHARACTER_TO_NAME.get(c);
        if (name == null) {
            throw new IOException("No name for character '" + c + "'");
        }
        return name;
    }
    
    public String getCharacter(final int code) throws IOException {
        return getCharacter(this.getName(code));
    }
    
    public static String getCharacter(final String name) {
        String character = Encoding.NAME_TO_CHARACTER.get(name);
        if (character == null) {
            character = name;
        }
        return character;
    }
    
    static {
        NAME_TO_CHARACTER = new HashMap<String, String>();
        CHARACTER_TO_NAME = new HashMap<String, String>();
    }
}
