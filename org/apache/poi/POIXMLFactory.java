// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackageRelationship;

public abstract class POIXMLFactory
{
    public abstract POIXMLDocumentPart createDocumentPart(final POIXMLDocumentPart p0, final PackageRelationship p1, final PackagePart p2);
    
    public abstract POIXMLDocumentPart newDocumentPart(final POIXMLRelation p0);
}
