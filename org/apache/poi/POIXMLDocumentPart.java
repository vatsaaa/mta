// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.net.URI;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.exceptions.PartAlreadyExistsException;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import java.util.Set;
import java.io.IOException;
import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import java.util.LinkedHashMap;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.util.Map;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.xmlbeans.XmlOptions;
import org.apache.poi.util.POILogger;

public class POIXMLDocumentPart
{
    private static final POILogger logger;
    public static final XmlOptions DEFAULT_XML_OPTIONS;
    private PackagePart packagePart;
    private PackageRelationship packageRel;
    private POIXMLDocumentPart parent;
    private Map<String, POIXMLDocumentPart> relations;
    private int relationCounter;
    
    int incrementRelationCounter() {
        return ++this.relationCounter;
    }
    
    int decrementRelationCounter() {
        return --this.relationCounter;
    }
    
    int getRelationCounter() {
        return this.relationCounter;
    }
    
    public POIXMLDocumentPart(final OPCPackage pkg) {
        this.relations = new LinkedHashMap<String, POIXMLDocumentPart>();
        this.relationCounter = 0;
        final PackageRelationship coreRel = pkg.getRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument").getRelationship(0);
        this.packagePart = pkg.getPart(coreRel);
        this.packageRel = coreRel;
    }
    
    public POIXMLDocumentPart() {
        this.relations = new LinkedHashMap<String, POIXMLDocumentPart>();
        this.relationCounter = 0;
    }
    
    public POIXMLDocumentPart(final PackagePart part, final PackageRelationship rel) {
        this.relations = new LinkedHashMap<String, POIXMLDocumentPart>();
        this.relationCounter = 0;
        this.packagePart = part;
        this.packageRel = rel;
    }
    
    public POIXMLDocumentPart(final POIXMLDocumentPart parent, final PackagePart part, final PackageRelationship rel) {
        this.relations = new LinkedHashMap<String, POIXMLDocumentPart>();
        this.relationCounter = 0;
        this.packagePart = part;
        this.packageRel = rel;
        this.parent = parent;
    }
    
    protected final void rebase(final OPCPackage pkg) throws InvalidFormatException {
        final PackageRelationshipCollection cores = this.packagePart.getRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument");
        if (cores.size() != 1) {
            throw new IllegalStateException("Tried to rebase using http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument but found " + cores.size() + " parts of the right type");
        }
        this.packageRel = cores.getRelationship(0);
        this.packagePart = this.packagePart.getRelatedPart(this.packageRel);
    }
    
    public final PackagePart getPackagePart() {
        return this.packagePart;
    }
    
    public final PackageRelationship getPackageRelationship() {
        return this.packageRel;
    }
    
    public final List<POIXMLDocumentPart> getRelations() {
        return Collections.unmodifiableList((List<? extends POIXMLDocumentPart>)new ArrayList<POIXMLDocumentPart>(this.relations.values()));
    }
    
    public final POIXMLDocumentPart getRelationById(final String id) {
        return this.relations.get(id);
    }
    
    public final String getRelationId(final POIXMLDocumentPart part) {
        for (final Map.Entry<String, POIXMLDocumentPart> entry : this.relations.entrySet()) {
            if (entry.getValue() == part) {
                return entry.getKey();
            }
        }
        return null;
    }
    
    public final void addRelation(final String id, final POIXMLDocumentPart part) {
        this.relations.put(id, part);
        part.incrementRelationCounter();
    }
    
    protected final void removeRelation(final POIXMLDocumentPart part) {
        this.removeRelation(part, true);
    }
    
    protected final boolean removeRelation(final POIXMLDocumentPart part, final boolean removeUnusedParts) {
        final String id = this.getRelationId(part);
        if (id == null) {
            return false;
        }
        part.decrementRelationCounter();
        this.getPackagePart().removeRelationship(id);
        this.relations.remove(id);
        if (removeUnusedParts && part.getRelationCounter() == 0) {
            try {
                part.onDocumentRemove();
            }
            catch (IOException e) {
                throw new POIXMLException(e);
            }
            this.getPackagePart().getPackage().removePart(part.getPackagePart());
        }
        return true;
    }
    
    public final POIXMLDocumentPart getParent() {
        return this.parent;
    }
    
    @Override
    public String toString() {
        return (this.packagePart == null) ? null : this.packagePart.toString();
    }
    
    protected void commit() throws IOException {
    }
    
    protected final void onSave(final Set<PackagePart> alreadySaved) throws IOException {
        this.commit();
        alreadySaved.add(this.getPackagePart());
        for (final POIXMLDocumentPart p : this.relations.values()) {
            if (!alreadySaved.contains(p.getPackagePart())) {
                p.onSave(alreadySaved);
            }
        }
    }
    
    public final POIXMLDocumentPart createRelationship(final POIXMLRelation descriptor, final POIXMLFactory factory) {
        return this.createRelationship(descriptor, factory, -1, false);
    }
    
    public final POIXMLDocumentPart createRelationship(final POIXMLRelation descriptor, final POIXMLFactory factory, final int idx) {
        return this.createRelationship(descriptor, factory, idx, false);
    }
    
    protected final POIXMLDocumentPart createRelationship(final POIXMLRelation descriptor, final POIXMLFactory factory, final int idx, final boolean noRelation) {
        try {
            final PackagePartName ppName = PackagingURIHelper.createPartName(descriptor.getFileName(idx));
            PackageRelationship rel = null;
            final PackagePart part = this.packagePart.getPackage().createPart(ppName, descriptor.getContentType());
            if (!noRelation) {
                rel = this.packagePart.addRelationship(ppName, TargetMode.INTERNAL, descriptor.getRelation());
            }
            final POIXMLDocumentPart doc = factory.newDocumentPart(descriptor);
            doc.packageRel = rel;
            doc.packagePart = part;
            doc.parent = this;
            if (!noRelation) {
                this.addRelation(rel.getId(), doc);
            }
            return doc;
        }
        catch (PartAlreadyExistsException pae) {
            throw pae;
        }
        catch (Exception e) {
            throw new POIXMLException(e);
        }
    }
    
    protected void read(final POIXMLFactory factory, final Map<PackagePart, POIXMLDocumentPart> context) throws OpenXML4JException {
        final PackageRelationshipCollection rels = this.packagePart.getRelationships();
        for (final PackageRelationship rel : rels) {
            if (rel.getTargetMode() == TargetMode.INTERNAL) {
                final URI uri = rel.getTargetURI();
                PackagePart p;
                if (uri.getRawFragment() != null) {
                    p = null;
                }
                else {
                    final PackagePartName relName = PackagingURIHelper.createPartName(uri);
                    p = this.packagePart.getPackage().getPart(relName);
                    if (p == null) {
                        POIXMLDocumentPart.logger.log(POILogger.ERROR, "Skipped invalid entry " + rel.getTargetURI());
                        continue;
                    }
                }
                if (!context.containsKey(p)) {
                    final POIXMLDocumentPart childPart = factory.createDocumentPart(this, rel, p);
                    (childPart.parent = this).addRelation(rel.getId(), childPart);
                    if (p == null) {
                        continue;
                    }
                    context.put(p, childPart);
                    if (!p.hasRelationships()) {
                        continue;
                    }
                    childPart.read(factory, context);
                }
                else {
                    this.addRelation(rel.getId(), context.get(p));
                }
            }
        }
    }
    
    protected PackagePart getTargetPart(final PackageRelationship rel) throws InvalidFormatException {
        return this.getPackagePart().getRelatedPart(rel);
    }
    
    protected void onDocumentCreate() throws IOException {
    }
    
    protected void onDocumentRead() throws IOException {
    }
    
    protected void onDocumentRemove() throws IOException {
    }
    
    static {
        logger = POILogFactory.getLogger(POIXMLDocumentPart.class);
        (DEFAULT_XML_OPTIONS = new XmlOptions()).setSaveOuter();
        POIXMLDocumentPart.DEFAULT_XML_OPTIONS.setUseDefaultNamespace();
        POIXMLDocumentPart.DEFAULT_XML_OPTIONS.setSaveAggressiveNamespaces();
    }
}
