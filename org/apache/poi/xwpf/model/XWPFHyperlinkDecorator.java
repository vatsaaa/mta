// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.model;

import java.util.Iterator;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHyperlink;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

@Deprecated
public class XWPFHyperlinkDecorator extends XWPFParagraphDecorator
{
    private StringBuffer hyperlinkText;
    
    public XWPFHyperlinkDecorator(final XWPFParagraphDecorator nextDecorator, final boolean outputHyperlinkUrls) {
        this(nextDecorator.paragraph, nextDecorator, outputHyperlinkUrls);
    }
    
    public XWPFHyperlinkDecorator(final XWPFParagraph prgrph, final XWPFParagraphDecorator nextDecorator, final boolean outputHyperlinkUrls) {
        super(prgrph, nextDecorator);
        this.hyperlinkText = new StringBuffer();
        for (final CTHyperlink link : this.paragraph.getCTP().getHyperlinkList()) {
            for (final CTR r : link.getRList()) {
                for (final CTText text : r.getTList()) {
                    this.hyperlinkText.append(text.getStringValue());
                }
            }
            if (outputHyperlinkUrls && this.paragraph.getDocument().getHyperlinkByID(link.getId()) != null) {
                this.hyperlinkText.append(" <" + this.paragraph.getDocument().getHyperlinkByID(link.getId()).getURL() + ">");
            }
        }
    }
    
    @Override
    public String getText() {
        return super.getText() + (Object)this.hyperlinkText;
    }
}
