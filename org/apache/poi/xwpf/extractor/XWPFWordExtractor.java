// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.extractor;

import org.apache.poi.xwpf.usermodel.XWPFHyperlink;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import java.util.Iterator;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.POIXMLException;
import org.apache.poi.xwpf.model.XWPFParagraphDecorator;
import org.apache.poi.xwpf.model.XWPFCommentsDecorator;
import org.apache.poi.xwpf.usermodel.XWPFHyperlinkRun;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.POIXMLDocument;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRelation;
import org.apache.poi.POIXMLTextExtractor;

public class XWPFWordExtractor extends POIXMLTextExtractor
{
    public static final XWPFRelation[] SUPPORTED_TYPES;
    private XWPFDocument document;
    private boolean fetchHyperlinks;
    
    public XWPFWordExtractor(final OPCPackage container) throws XmlException, OpenXML4JException, IOException {
        this(new XWPFDocument(container));
    }
    
    public XWPFWordExtractor(final XWPFDocument document) {
        super(document);
        this.fetchHyperlinks = false;
        this.document = document;
    }
    
    public void setFetchHyperlinks(final boolean fetch) {
        this.fetchHyperlinks = fetch;
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Use:");
            System.err.println("  HXFWordExtractor <filename.docx>");
            System.exit(1);
        }
        final POIXMLTextExtractor extractor = new XWPFWordExtractor(POIXMLDocument.openPackage(args[0]));
        System.out.println(extractor.getText());
    }
    
    @Override
    public String getText() {
        final StringBuffer text = new StringBuffer();
        final XWPFHeaderFooterPolicy hfPolicy = this.document.getHeaderFooterPolicy();
        this.extractHeaders(text, hfPolicy);
        final Iterator<XWPFParagraph> i = this.document.getParagraphsIterator();
        while (i.hasNext()) {
            final XWPFParagraph paragraph = i.next();
            try {
                CTSectPr ctSectPr = null;
                if (paragraph.getCTP().getPPr() != null) {
                    ctSectPr = paragraph.getCTP().getPPr().getSectPr();
                }
                XWPFHeaderFooterPolicy headerFooterPolicy = null;
                if (ctSectPr != null) {
                    headerFooterPolicy = new XWPFHeaderFooterPolicy(this.document, ctSectPr);
                    this.extractHeaders(text, headerFooterPolicy);
                }
                for (final XWPFRun run : paragraph.getRuns()) {
                    text.append(run.toString());
                    if (run instanceof XWPFHyperlinkRun && this.fetchHyperlinks) {
                        final XWPFHyperlink link = ((XWPFHyperlinkRun)run).getHyperlink(this.document);
                        if (link == null) {
                            continue;
                        }
                        text.append(" <" + link.getURL() + ">");
                    }
                }
                final XWPFCommentsDecorator decorator = new XWPFCommentsDecorator(paragraph, null);
                text.append(decorator.getCommentText()).append('\n');
                final String footnameText = paragraph.getFootnoteText();
                if (footnameText != null && footnameText.length() > 0) {
                    text.append(footnameText + "\n");
                }
                if (ctSectPr == null) {
                    continue;
                }
                this.extractFooters(text, headerFooterPolicy);
            }
            catch (IOException e) {
                throw new POIXMLException(e);
            }
            catch (XmlException e2) {
                throw new POIXMLException(e2);
            }
        }
        final Iterator<XWPFTable> j = this.document.getTablesIterator();
        while (j.hasNext()) {
            text.append(j.next().getText()).append('\n');
        }
        this.extractFooters(text, hfPolicy);
        return text.toString();
    }
    
    private void extractFooters(final StringBuffer text, final XWPFHeaderFooterPolicy hfPolicy) {
        if (hfPolicy.getFirstPageFooter() != null) {
            text.append(hfPolicy.getFirstPageFooter().getText());
        }
        if (hfPolicy.getEvenPageFooter() != null) {
            text.append(hfPolicy.getEvenPageFooter().getText());
        }
        if (hfPolicy.getDefaultFooter() != null) {
            text.append(hfPolicy.getDefaultFooter().getText());
        }
    }
    
    private void extractHeaders(final StringBuffer text, final XWPFHeaderFooterPolicy hfPolicy) {
        if (hfPolicy.getFirstPageHeader() != null) {
            text.append(hfPolicy.getFirstPageHeader().getText());
        }
        if (hfPolicy.getEvenPageHeader() != null) {
            text.append(hfPolicy.getEvenPageHeader().getText());
        }
        if (hfPolicy.getDefaultHeader() != null) {
            text.append(hfPolicy.getDefaultHeader().getText());
        }
    }
    
    static {
        SUPPORTED_TYPES = new XWPFRelation[] { XWPFRelation.DOCUMENT, XWPFRelation.TEMPLATE, XWPFRelation.MACRO_DOCUMENT, XWPFRelation.MACRO_TEMPLATE_DOCUMENT };
    }
}
