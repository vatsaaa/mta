// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTNum;

public class XWPFNum
{
    private CTNum ctNum;
    protected XWPFNumbering numbering;
    
    public XWPFNum() {
        this.ctNum = null;
        this.numbering = null;
    }
    
    public XWPFNum(final CTNum ctNum) {
        this.ctNum = ctNum;
        this.numbering = null;
    }
    
    public XWPFNum(final XWPFNumbering numbering) {
        this.ctNum = null;
        this.numbering = numbering;
    }
    
    public XWPFNum(final CTNum ctNum, final XWPFNumbering numbering) {
        this.ctNum = ctNum;
        this.numbering = numbering;
    }
    
    public XWPFNumbering getNumbering() {
        return this.numbering;
    }
    
    public CTNum getCTNum() {
        return this.ctNum;
    }
    
    public void setNumbering(final XWPFNumbering numbering) {
        this.numbering = numbering;
    }
    
    public void setCTNum(final CTNum ctNum) {
        this.ctNum = ctNum;
    }
}
