// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlCursor;
import org.apache.poi.util.IOUtils;
import java.io.InputStream;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagePartName;
import java.io.OutputStream;
import org.apache.poi.POIXMLRelation;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.POIXMLException;
import org.apache.poi.POIXMLFactory;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import java.util.Collections;
import org.apache.poi.util.Internal;
import java.util.Iterator;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHdrFtr;
import java.util.List;
import org.apache.poi.POIXMLDocumentPart;

public abstract class XWPFHeaderFooter extends POIXMLDocumentPart implements IBody
{
    List<XWPFParagraph> paragraphs;
    List<XWPFTable> tables;
    List<XWPFPictureData> pictures;
    List<IBodyElement> bodyElements;
    CTHdrFtr headerFooter;
    XWPFDocument document;
    
    XWPFHeaderFooter(final XWPFDocument doc, final CTHdrFtr hdrFtr) {
        this.paragraphs = new ArrayList<XWPFParagraph>(1);
        this.tables = new ArrayList<XWPFTable>(1);
        this.pictures = new ArrayList<XWPFPictureData>();
        this.bodyElements = new ArrayList<IBodyElement>(1);
        if (doc == null) {
            throw new NullPointerException();
        }
        this.document = doc;
        this.headerFooter = hdrFtr;
        this.readHdrFtr();
    }
    
    protected XWPFHeaderFooter() {
        this.paragraphs = new ArrayList<XWPFParagraph>(1);
        this.tables = new ArrayList<XWPFTable>(1);
        this.pictures = new ArrayList<XWPFPictureData>();
        this.bodyElements = new ArrayList<IBodyElement>(1);
        this.headerFooter = CTHdrFtr.Factory.newInstance();
        this.readHdrFtr();
    }
    
    public XWPFHeaderFooter(final POIXMLDocumentPart parent, final PackagePart part, final PackageRelationship rel) throws IOException {
        super(parent, part, rel);
        this.paragraphs = new ArrayList<XWPFParagraph>(1);
        this.tables = new ArrayList<XWPFTable>(1);
        this.pictures = new ArrayList<XWPFPictureData>();
        this.bodyElements = new ArrayList<IBodyElement>(1);
        this.document = (XWPFDocument)this.getParent();
        if (this.document == null) {
            throw new NullPointerException();
        }
    }
    
    @Override
    protected void onDocumentRead() throws IOException {
        for (final POIXMLDocumentPart poixmlDocumentPart : this.getRelations()) {
            if (poixmlDocumentPart instanceof XWPFPictureData) {
                final XWPFPictureData xwpfPicData = (XWPFPictureData)poixmlDocumentPart;
                this.pictures.add(xwpfPicData);
                this.document.registerPackagePictureData(xwpfPicData);
            }
        }
    }
    
    @Internal
    public CTHdrFtr _getHdrFtr() {
        return this.headerFooter;
    }
    
    public List<IBodyElement> getBodyElements() {
        return Collections.unmodifiableList((List<? extends IBodyElement>)this.bodyElements);
    }
    
    public List<XWPFParagraph> getParagraphs() {
        return Collections.unmodifiableList((List<? extends XWPFParagraph>)this.paragraphs);
    }
    
    public List<XWPFTable> getTables() throws ArrayIndexOutOfBoundsException {
        return Collections.unmodifiableList((List<? extends XWPFTable>)this.tables);
    }
    
    public String getText() {
        final StringBuffer t = new StringBuffer();
        for (int i = 0; i < this.paragraphs.size(); ++i) {
            if (!this.paragraphs.get(i).isEmpty()) {
                final String text = this.paragraphs.get(i).getText();
                if (text != null && text.length() > 0) {
                    t.append(text);
                    t.append('\n');
                }
            }
        }
        final List<XWPFTable> tables = this.getTables();
        for (int j = 0; j < tables.size(); ++j) {
            final String text2 = tables.get(j).getText();
            if (text2 != null && text2.length() > 0) {
                t.append(text2);
                t.append('\n');
            }
        }
        return t.toString();
    }
    
    public void setHeaderFooter(final CTHdrFtr headerFooter) {
        this.headerFooter = headerFooter;
        this.readHdrFtr();
    }
    
    public XWPFTable getTable(final CTTbl ctTable) {
        for (final XWPFTable table : this.tables) {
            if (table == null) {
                return null;
            }
            if (table.getCTTbl().equals(ctTable)) {
                return table;
            }
        }
        return null;
    }
    
    public XWPFParagraph getParagraph(final CTP p) {
        for (final XWPFParagraph paragraph : this.paragraphs) {
            if (paragraph.getCTP().equals(p)) {
                return paragraph;
            }
        }
        return null;
    }
    
    public XWPFParagraph getParagraphArray(final int pos) {
        return this.paragraphs.get(pos);
    }
    
    public List<XWPFParagraph> getListParagraph() {
        return this.paragraphs;
    }
    
    public List<XWPFPictureData> getAllPictures() {
        return Collections.unmodifiableList((List<? extends XWPFPictureData>)this.pictures);
    }
    
    public List<XWPFPictureData> getAllPackagePictures() {
        return this.document.getAllPackagePictures();
    }
    
    public String addPictureData(final byte[] pictureData, final int format) throws InvalidFormatException {
        XWPFPictureData xwpfPicData = this.document.findPackagePictureData(pictureData, format);
        final POIXMLRelation relDesc = XWPFPictureData.RELATIONS[format];
        if (xwpfPicData == null) {
            final int idx = this.document.getNextPicNameNumber(format);
            xwpfPicData = (XWPFPictureData)this.createRelationship(relDesc, XWPFFactory.getInstance(), idx);
            final PackagePart picDataPart = xwpfPicData.getPackagePart();
            OutputStream out = null;
            try {
                out = picDataPart.getOutputStream();
                out.write(pictureData);
            }
            catch (IOException e) {
                throw new POIXMLException(e);
            }
            finally {
                try {
                    out.close();
                }
                catch (IOException ex) {}
            }
            this.document.registerPackagePictureData(xwpfPicData);
            this.pictures.add(xwpfPicData);
            return this.getRelationId(xwpfPicData);
        }
        if (!this.getRelations().contains(xwpfPicData)) {
            final PackagePart picDataPart2 = xwpfPicData.getPackagePart();
            final TargetMode targetMode = TargetMode.INTERNAL;
            final PackagePartName partName = picDataPart2.getPartName();
            final String relation = relDesc.getRelation();
            final PackageRelationship relShip = this.getPackagePart().addRelationship(partName, targetMode, relation);
            final String id = relShip.getId();
            this.addRelation(id, xwpfPicData);
            this.pictures.add(xwpfPicData);
            return id;
        }
        return this.getRelationId(xwpfPicData);
    }
    
    public String addPictureData(final InputStream is, final int format) throws InvalidFormatException, IOException {
        final byte[] data = IOUtils.toByteArray(is);
        return this.addPictureData(data, format);
    }
    
    public XWPFPictureData getPictureDataByID(final String blipID) {
        final POIXMLDocumentPart relatedPart = this.getRelationById(blipID);
        if (relatedPart != null && relatedPart instanceof XWPFPictureData) {
            return (XWPFPictureData)relatedPart;
        }
        return null;
    }
    
    public XWPFParagraph insertNewParagraph(final XmlCursor cursor) {
        if (this.isCursorInHdrF(cursor)) {
            final String uri = CTP.type.getName().getNamespaceURI();
            final String localPart = "p";
            cursor.beginElement(localPart, uri);
            cursor.toParent();
            final CTP p = (CTP)cursor.getObject();
            final XWPFParagraph newP = new XWPFParagraph(p, this);
            XmlObject o;
            for (o = null; !(o instanceof CTP) && cursor.toPrevSibling(); o = cursor.getObject()) {}
            if (!(o instanceof CTP) || o == p) {
                this.paragraphs.add(0, newP);
            }
            else {
                final int pos = this.paragraphs.indexOf(this.getParagraph((CTP)o)) + 1;
                this.paragraphs.add(pos, newP);
            }
            int i = 0;
            cursor.toCursor(p.newCursor());
            while (cursor.toPrevSibling()) {
                o = cursor.getObject();
                if (o instanceof CTP || o instanceof CTTbl) {
                    ++i;
                }
            }
            this.bodyElements.add(i, newP);
            cursor.toCursor(p.newCursor());
            cursor.toEndToken();
            return newP;
        }
        return null;
    }
    
    public XWPFTable insertNewTbl(XmlCursor cursor) {
        if (this.isCursorInHdrF(cursor)) {
            final String uri = CTTbl.type.getName().getNamespaceURI();
            final String localPart = "tbl";
            cursor.beginElement(localPart, uri);
            cursor.toParent();
            final CTTbl t = (CTTbl)cursor.getObject();
            final XWPFTable newT = new XWPFTable(t, this);
            cursor.removeXmlContents();
            XmlObject o;
            for (o = null; !(o instanceof CTTbl) && cursor.toPrevSibling(); o = cursor.getObject()) {}
            if (!(o instanceof CTTbl)) {
                this.tables.add(0, newT);
            }
            else {
                final int pos = this.tables.indexOf(this.getTable((CTTbl)o)) + 1;
                this.tables.add(pos, newT);
            }
            int i = 0;
            cursor = t.newCursor();
            while (cursor.toPrevSibling()) {
                o = cursor.getObject();
                if (o instanceof CTP || o instanceof CTTbl) {
                    ++i;
                }
            }
            this.bodyElements.add(i, newT);
            cursor = t.newCursor();
            cursor.toEndToken();
            return newT;
        }
        return null;
    }
    
    private boolean isCursorInHdrF(final XmlCursor cursor) {
        final XmlCursor verify = cursor.newCursor();
        verify.toParent();
        return verify.getObject() == this.headerFooter;
    }
    
    public POIXMLDocumentPart getOwner() {
        return this;
    }
    
    public XWPFTable getTableArray(final int pos) {
        if (pos > 0 && pos < this.tables.size()) {
            return this.tables.get(pos);
        }
        return null;
    }
    
    public void insertTable(final int pos, final XWPFTable table) {
        this.bodyElements.add(pos, table);
        int i;
        for (i = 0; i < this.headerFooter.getTblList().size(); ++i) {
            final CTTbl tbl = this.headerFooter.getTblArray(i);
            if (tbl == table.getCTTbl()) {
                break;
            }
        }
        this.tables.add(i, table);
    }
    
    public void readHdrFtr() {
        this.bodyElements = new ArrayList<IBodyElement>();
        this.paragraphs = new ArrayList<XWPFParagraph>();
        this.tables = new ArrayList<XWPFTable>();
        final XmlCursor cursor = this.headerFooter.newCursor();
        cursor.selectPath("./*");
        while (cursor.toNextSelection()) {
            final XmlObject o = cursor.getObject();
            if (o instanceof CTP) {
                final XWPFParagraph p = new XWPFParagraph((CTP)o, this);
                this.paragraphs.add(p);
                this.bodyElements.add(p);
            }
            if (o instanceof CTTbl) {
                final XWPFTable t = new XWPFTable((CTTbl)o, this);
                this.tables.add(t);
                this.bodyElements.add(t);
            }
        }
        cursor.dispose();
    }
    
    public XWPFTableCell getTableCell(final CTTc cell) {
        final XmlCursor cursor = cell.newCursor();
        cursor.toParent();
        XmlObject o = cursor.getObject();
        if (!(o instanceof CTRow)) {
            return null;
        }
        final CTRow row = (CTRow)o;
        cursor.toParent();
        o = cursor.getObject();
        cursor.dispose();
        if (!(o instanceof CTTbl)) {
            return null;
        }
        final CTTbl tbl = (CTTbl)o;
        final XWPFTable table = this.getTable(tbl);
        if (table == null) {
            return null;
        }
        final XWPFTableRow tableRow = table.getRow(row);
        if (row == null) {
            return null;
        }
        return tableRow.getTableCell(cell);
    }
    
    public void setXWPFDocument(final XWPFDocument doc) {
        this.document = doc;
    }
    
    public XWPFDocument getXWPFDocument() {
        if (this.document != null) {
            return this.document;
        }
        return (XWPFDocument)this.getParent();
    }
    
    public POIXMLDocumentPart getPart() {
        return this;
    }
}
