// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.util.HashMap;
import java.util.Map;

public enum LineSpacingRule
{
    AUTO(1), 
    EXACT(2), 
    AT_LEAST(3);
    
    private final int value;
    private static Map<Integer, LineSpacingRule> imap;
    
    private LineSpacingRule(final int val) {
        this.value = val;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public static LineSpacingRule valueOf(final int type) {
        final LineSpacingRule lineType = LineSpacingRule.imap.get(new Integer(type));
        if (lineType == null) {
            throw new IllegalArgumentException("Unknown line type: " + type);
        }
        return lineType;
    }
    
    static {
        LineSpacingRule.imap = new HashMap<Integer, LineSpacingRule>();
        for (final LineSpacingRule p : values()) {
            LineSpacingRule.imap.put(new Integer(p.getValue()), p);
        }
    }
}
