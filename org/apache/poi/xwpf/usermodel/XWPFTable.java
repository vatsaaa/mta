// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.apache.poi.POIXMLDocumentPart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblCellMar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDecimalNumber;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTString;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;
import java.math.BigInteger;
import java.util.Iterator;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import java.util.ArrayList;
import java.util.HashMap;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import java.util.EnumMap;
import java.util.List;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;

public class XWPFTable implements IBodyElement
{
    protected StringBuffer text;
    private CTTbl ctTbl;
    protected List<XWPFTableRow> tableRows;
    protected List<String> styleIDs;
    private static EnumMap<XWPFBorderType, STBorder.Enum> xwpfBorderTypeMap;
    private static HashMap<Integer, XWPFBorderType> stBorderTypeMap;
    protected IBody part;
    
    public XWPFTable(final CTTbl table, final IBody part, final int row, final int col) {
        this(table, part);
        for (int i = 0; i < row; ++i) {
            final XWPFTableRow tabRow = (this.getRow(i) == null) ? this.createRow() : this.getRow(i);
            for (int k = 0; k < col; ++k) {
                if (tabRow.getCell(k) == null) {
                    tabRow.createCell();
                }
            }
        }
    }
    
    public XWPFTable(final CTTbl table, final IBody part) {
        this.text = new StringBuffer();
        this.part = part;
        this.ctTbl = table;
        this.tableRows = new ArrayList<XWPFTableRow>();
        if (table.sizeOfTrArray() == 0) {
            this.createEmptyTable(table);
        }
        for (final CTRow row : table.getTrList()) {
            final StringBuffer rowText = new StringBuffer();
            final XWPFTableRow tabRow = new XWPFTableRow(row, this);
            this.tableRows.add(tabRow);
            for (final CTTc cell : row.getTcList()) {
                for (final CTP ctp : cell.getPList()) {
                    final XWPFParagraph p = new XWPFParagraph(ctp, part);
                    if (rowText.length() > 0) {
                        rowText.append('\t');
                    }
                    rowText.append(p.getText());
                }
            }
            if (rowText.length() > 0) {
                this.text.append(rowText);
                this.text.append('\n');
            }
        }
    }
    
    private void createEmptyTable(final CTTbl table) {
        table.addNewTr().addNewTc().addNewP();
        final CTTblPr tblpro = table.addNewTblPr();
        tblpro.addNewTblW().setW(new BigInteger("0"));
        tblpro.getTblW().setType(STTblWidth.AUTO);
        final CTTblBorders borders = tblpro.addNewTblBorders();
        borders.addNewBottom().setVal(STBorder.SINGLE);
        borders.addNewInsideH().setVal(STBorder.SINGLE);
        borders.addNewInsideV().setVal(STBorder.SINGLE);
        borders.addNewLeft().setVal(STBorder.SINGLE);
        borders.addNewRight().setVal(STBorder.SINGLE);
        borders.addNewTop().setVal(STBorder.SINGLE);
        this.getRows();
    }
    
    @Internal
    public CTTbl getCTTbl() {
        return this.ctTbl;
    }
    
    public String getText() {
        return this.text.toString();
    }
    
    public void addNewRowBetween(final int start, final int end) {
    }
    
    public void addNewCol() {
        if (this.ctTbl.sizeOfTrArray() == 0) {
            this.createRow();
        }
        for (int i = 0; i < this.ctTbl.sizeOfTrArray(); ++i) {
            final XWPFTableRow tabRow = new XWPFTableRow(this.ctTbl.getTrArray(i), this);
            tabRow.createCell();
        }
    }
    
    public XWPFTableRow createRow() {
        final int sizeCol = (this.ctTbl.sizeOfTrArray() > 0) ? this.ctTbl.getTrArray(0).sizeOfTcArray() : 0;
        final XWPFTableRow tabRow = new XWPFTableRow(this.ctTbl.addNewTr(), this);
        this.addColumn(tabRow, sizeCol);
        this.tableRows.add(tabRow);
        return tabRow;
    }
    
    public XWPFTableRow getRow(final int pos) {
        if (pos >= 0 && pos < this.ctTbl.sizeOfTrArray()) {
            return this.getRows().get(pos);
        }
        return null;
    }
    
    public void setWidth(final int width) {
        final CTTblPr tblPr = this.getTrPr();
        final CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr.addNewTblW();
        tblWidth.setW(new BigInteger("" + width));
    }
    
    public int getWidth() {
        final CTTblPr tblPr = this.getTrPr();
        return tblPr.isSetTblW() ? tblPr.getTblW().getW().intValue() : -1;
    }
    
    public int getNumberOfRows() {
        return this.ctTbl.sizeOfTrArray();
    }
    
    private CTTblPr getTrPr() {
        return (this.ctTbl.getTblPr() != null) ? this.ctTbl.getTblPr() : this.ctTbl.addNewTblPr();
    }
    
    private void addColumn(final XWPFTableRow tabRow, final int sizeCol) {
        if (sizeCol > 0) {
            for (int i = 0; i < sizeCol; ++i) {
                tabRow.createCell();
            }
        }
    }
    
    public String getStyleID() {
        String styleId = null;
        final CTTblPr tblPr = this.ctTbl.getTblPr();
        if (tblPr != null) {
            final CTString styleStr = tblPr.getTblStyle();
            if (styleStr != null) {
                styleId = styleStr.getVal();
            }
        }
        return styleId;
    }
    
    public void setStyleID(final String styleName) {
        final CTTblPr tblPr = this.getTrPr();
        CTString styleStr = tblPr.getTblStyle();
        if (styleStr == null) {
            styleStr = tblPr.addNewTblStyle();
        }
        styleStr.setVal(styleName);
    }
    
    public XWPFBorderType getInsideHBorderType() {
        XWPFBorderType bt = null;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideH()) {
                final CTBorder border = ctb.getInsideH();
                bt = XWPFTable.stBorderTypeMap.get(border.getVal().intValue());
            }
        }
        return bt;
    }
    
    public int getInsideHBorderSize() {
        int size = -1;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideH()) {
                final CTBorder border = ctb.getInsideH();
                size = border.getSz().intValue();
            }
        }
        return size;
    }
    
    public int getInsideHBorderSpace() {
        int space = -1;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideH()) {
                final CTBorder border = ctb.getInsideH();
                space = border.getSpace().intValue();
            }
        }
        return space;
    }
    
    public String getInsideHBorderColor() {
        String color = null;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideH()) {
                final CTBorder border = ctb.getInsideH();
                color = border.xgetColor().getStringValue();
            }
        }
        return color;
    }
    
    public XWPFBorderType getInsideVBorderType() {
        XWPFBorderType bt = null;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideV()) {
                final CTBorder border = ctb.getInsideV();
                bt = XWPFTable.stBorderTypeMap.get(border.getVal().intValue());
            }
        }
        return bt;
    }
    
    public int getInsideVBorderSize() {
        int size = -1;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideV()) {
                final CTBorder border = ctb.getInsideV();
                size = border.getSz().intValue();
            }
        }
        return size;
    }
    
    public int getInsideVBorderSpace() {
        int space = -1;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideV()) {
                final CTBorder border = ctb.getInsideV();
                space = border.getSpace().intValue();
            }
        }
        return space;
    }
    
    public String getInsideVBorderColor() {
        String color = null;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblBorders()) {
            final CTTblBorders ctb = tblPr.getTblBorders();
            if (ctb.isSetInsideV()) {
                final CTBorder border = ctb.getInsideV();
                color = border.xgetColor().getStringValue();
            }
        }
        return color;
    }
    
    public int getRowBandSize() {
        int size = 0;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblStyleRowBandSize()) {
            final CTDecimalNumber rowSize = tblPr.getTblStyleRowBandSize();
            size = rowSize.getVal().intValue();
        }
        return size;
    }
    
    public void setRowBandSize(final int size) {
        final CTTblPr tblPr = this.getTrPr();
        final CTDecimalNumber rowSize = tblPr.isSetTblStyleRowBandSize() ? tblPr.getTblStyleRowBandSize() : tblPr.addNewTblStyleRowBandSize();
        rowSize.setVal(BigInteger.valueOf(size));
    }
    
    public int getColBandSize() {
        int size = 0;
        final CTTblPr tblPr = this.getTrPr();
        if (tblPr.isSetTblStyleColBandSize()) {
            final CTDecimalNumber colSize = tblPr.getTblStyleColBandSize();
            size = colSize.getVal().intValue();
        }
        return size;
    }
    
    public void setColBandSize(final int size) {
        final CTTblPr tblPr = this.getTrPr();
        final CTDecimalNumber colSize = tblPr.isSetTblStyleColBandSize() ? tblPr.getTblStyleColBandSize() : tblPr.addNewTblStyleColBandSize();
        colSize.setVal(BigInteger.valueOf(size));
    }
    
    public void setInsideHBorder(final XWPFBorderType type, final int size, final int space, final String rgbColor) {
        final CTTblPr tblPr = this.getTrPr();
        final CTTblBorders ctb = tblPr.isSetTblBorders() ? tblPr.getTblBorders() : tblPr.addNewTblBorders();
        final CTBorder b = ctb.isSetInsideH() ? ctb.getInsideH() : ctb.addNewInsideH();
        b.setVal(XWPFTable.xwpfBorderTypeMap.get(type));
        b.setSz(BigInteger.valueOf(size));
        b.setSpace(BigInteger.valueOf(space));
        b.setColor(rgbColor);
    }
    
    public void setInsideVBorder(final XWPFBorderType type, final int size, final int space, final String rgbColor) {
        final CTTblPr tblPr = this.getTrPr();
        final CTTblBorders ctb = tblPr.isSetTblBorders() ? tblPr.getTblBorders() : tblPr.addNewTblBorders();
        final CTBorder b = ctb.isSetInsideV() ? ctb.getInsideV() : ctb.addNewInsideV();
        b.setVal(XWPFTable.xwpfBorderTypeMap.get(type));
        b.setSz(BigInteger.valueOf(size));
        b.setSpace(BigInteger.valueOf(space));
        b.setColor(rgbColor);
    }
    
    public int getCellMarginTop() {
        int margin = 0;
        final CTTblPr tblPr = this.getTrPr();
        final CTTblCellMar tcm = tblPr.getTblCellMar();
        if (tcm != null) {
            final CTTblWidth tw = tcm.getTop();
            if (tw != null) {
                margin = tw.getW().intValue();
            }
        }
        return margin;
    }
    
    public int getCellMarginLeft() {
        int margin = 0;
        final CTTblPr tblPr = this.getTrPr();
        final CTTblCellMar tcm = tblPr.getTblCellMar();
        if (tcm != null) {
            final CTTblWidth tw = tcm.getLeft();
            if (tw != null) {
                margin = tw.getW().intValue();
            }
        }
        return margin;
    }
    
    public int getCellMarginBottom() {
        int margin = 0;
        final CTTblPr tblPr = this.getTrPr();
        final CTTblCellMar tcm = tblPr.getTblCellMar();
        if (tcm != null) {
            final CTTblWidth tw = tcm.getBottom();
            if (tw != null) {
                margin = tw.getW().intValue();
            }
        }
        return margin;
    }
    
    public int getCellMarginRight() {
        int margin = 0;
        final CTTblPr tblPr = this.getTrPr();
        final CTTblCellMar tcm = tblPr.getTblCellMar();
        if (tcm != null) {
            final CTTblWidth tw = tcm.getRight();
            if (tw != null) {
                margin = tw.getW().intValue();
            }
        }
        return margin;
    }
    
    public void setCellMargins(final int top, final int left, final int bottom, final int right) {
        final CTTblPr tblPr = this.getTrPr();
        final CTTblCellMar tcm = tblPr.isSetTblCellMar() ? tblPr.getTblCellMar() : tblPr.addNewTblCellMar();
        CTTblWidth tw = tcm.isSetLeft() ? tcm.getLeft() : tcm.addNewLeft();
        tw.setType(STTblWidth.DXA);
        tw.setW(BigInteger.valueOf(left));
        tw = (tcm.isSetTop() ? tcm.getTop() : tcm.addNewTop());
        tw.setType(STTblWidth.DXA);
        tw.setW(BigInteger.valueOf(top));
        tw = (tcm.isSetBottom() ? tcm.getBottom() : tcm.addNewBottom());
        tw.setType(STTblWidth.DXA);
        tw.setW(BigInteger.valueOf(bottom));
        tw = (tcm.isSetRight() ? tcm.getRight() : tcm.addNewRight());
        tw.setType(STTblWidth.DXA);
        tw.setW(BigInteger.valueOf(right));
    }
    
    public void addRow(final XWPFTableRow row) {
        this.ctTbl.addNewTr();
        this.ctTbl.setTrArray(this.getNumberOfRows() - 1, row.getCtRow());
        this.tableRows.add(row);
    }
    
    public boolean addRow(final XWPFTableRow row, final int pos) {
        if (pos >= 0 && pos <= this.tableRows.size()) {
            this.ctTbl.insertNewTr(pos);
            this.ctTbl.setTrArray(pos, row.getCtRow());
            this.tableRows.add(pos, row);
            return true;
        }
        return false;
    }
    
    public XWPFTableRow insertNewTableRow(final int pos) {
        if (pos >= 0 && pos <= this.tableRows.size()) {
            final CTRow row = this.ctTbl.insertNewTr(pos);
            final XWPFTableRow tableRow = new XWPFTableRow(row, this);
            this.tableRows.add(pos, tableRow);
            return tableRow;
        }
        return null;
    }
    
    public boolean removeRow(final int pos) throws IndexOutOfBoundsException {
        if (pos >= 0 && pos < this.tableRows.size()) {
            if (this.ctTbl.sizeOfTrArray() > 0) {
                this.ctTbl.removeTr(pos);
            }
            this.tableRows.remove(pos);
            return true;
        }
        return false;
    }
    
    public List<XWPFTableRow> getRows() {
        return this.tableRows;
    }
    
    public BodyElementType getElementType() {
        return BodyElementType.TABLE;
    }
    
    public IBody getBody() {
        return this.part;
    }
    
    public POIXMLDocumentPart getPart() {
        if (this.part != null) {
            return this.part.getPart();
        }
        return null;
    }
    
    public BodyType getPartType() {
        return this.part.getPartType();
    }
    
    public XWPFTableRow getRow(final CTRow row) {
        for (int i = 0; i < this.getRows().size(); ++i) {
            if (this.getRows().get(i).getCtRow() == row) {
                return this.getRow(i);
            }
        }
        return null;
    }
    
    static {
        (XWPFTable.xwpfBorderTypeMap = new EnumMap<XWPFBorderType, STBorder.Enum>(XWPFBorderType.class)).put(XWPFBorderType.NIL, STBorder.Enum.forInt(1));
        XWPFTable.xwpfBorderTypeMap.put(XWPFBorderType.NONE, STBorder.Enum.forInt(2));
        XWPFTable.xwpfBorderTypeMap.put(XWPFBorderType.SINGLE, STBorder.Enum.forInt(3));
        XWPFTable.xwpfBorderTypeMap.put(XWPFBorderType.THICK, STBorder.Enum.forInt(4));
        XWPFTable.xwpfBorderTypeMap.put(XWPFBorderType.DOUBLE, STBorder.Enum.forInt(5));
        XWPFTable.xwpfBorderTypeMap.put(XWPFBorderType.DOTTED, STBorder.Enum.forInt(6));
        XWPFTable.xwpfBorderTypeMap.put(XWPFBorderType.DASHED, STBorder.Enum.forInt(7));
        XWPFTable.xwpfBorderTypeMap.put(XWPFBorderType.DOT_DASH, STBorder.Enum.forInt(8));
        (XWPFTable.stBorderTypeMap = new HashMap<Integer, XWPFBorderType>()).put(1, XWPFBorderType.NIL);
        XWPFTable.stBorderTypeMap.put(2, XWPFBorderType.NONE);
        XWPFTable.stBorderTypeMap.put(3, XWPFBorderType.SINGLE);
        XWPFTable.stBorderTypeMap.put(4, XWPFBorderType.THICK);
        XWPFTable.stBorderTypeMap.put(5, XWPFBorderType.DOUBLE);
        XWPFTable.stBorderTypeMap.put(6, XWPFBorderType.DOTTED);
        XWPFTable.stBorderTypeMap.put(7, XWPFBorderType.DASHED);
        XWPFTable.stBorderTypeMap.put(8, XWPFBorderType.DOT_DASH);
    }
    
    public enum XWPFBorderType
    {
        NIL, 
        NONE, 
        SINGLE, 
        THICK, 
        DOUBLE, 
        DOTTED, 
        DASHED, 
        DOT_DASH;
    }
}
