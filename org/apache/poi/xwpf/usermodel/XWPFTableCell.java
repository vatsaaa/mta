// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.apache.poi.POIXMLDocumentPart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVerticalJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;
import java.util.Iterator;
import java.util.Collections;
import org.apache.poi.util.Internal;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import java.util.ArrayList;
import java.util.HashMap;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;
import java.util.EnumMap;
import java.util.List;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;

public class XWPFTableCell implements IBody
{
    private final CTTc ctTc;
    protected List<XWPFParagraph> paragraphs;
    protected List<XWPFTable> tables;
    protected List<IBodyElement> bodyElements;
    protected IBody part;
    private XWPFTableRow tableRow;
    private static EnumMap<XWPFVertAlign, STVerticalJc.Enum> alignMap;
    private static HashMap<Integer, XWPFVertAlign> stVertAlignTypeMap;
    
    public XWPFTableCell(final CTTc cell, final XWPFTableRow tableRow, final IBody part) {
        this.paragraphs = null;
        this.tables = null;
        this.bodyElements = null;
        this.tableRow = null;
        this.ctTc = cell;
        this.part = part;
        this.tableRow = tableRow;
        if (cell.getPList().size() < 1) {
            cell.addNewP();
        }
        this.bodyElements = new ArrayList<IBodyElement>();
        this.paragraphs = new ArrayList<XWPFParagraph>();
        this.tables = new ArrayList<XWPFTable>();
        final XmlCursor cursor = this.ctTc.newCursor();
        cursor.selectPath("./*");
        while (cursor.toNextSelection()) {
            final XmlObject o = cursor.getObject();
            if (o instanceof CTP) {
                final XWPFParagraph p = new XWPFParagraph((CTP)o, this);
                this.paragraphs.add(p);
                this.bodyElements.add(p);
            }
            if (o instanceof CTTbl) {
                final XWPFTable t = new XWPFTable((CTTbl)o, this);
                this.tables.add(t);
                this.bodyElements.add(t);
            }
        }
        cursor.dispose();
    }
    
    @Internal
    public CTTc getCTTc() {
        return this.ctTc;
    }
    
    public List<IBodyElement> getBodyElements() {
        return Collections.unmodifiableList((List<? extends IBodyElement>)this.bodyElements);
    }
    
    public void setParagraph(final XWPFParagraph p) {
        if (this.ctTc.sizeOfPArray() == 0) {
            this.ctTc.addNewP();
        }
        this.ctTc.setPArray(0, p.getCTP());
    }
    
    public List<XWPFParagraph> getParagraphs() {
        return this.paragraphs;
    }
    
    public XWPFParagraph addParagraph() {
        final XWPFParagraph p = new XWPFParagraph(this.ctTc.addNewP(), this);
        this.addParagraph(p);
        return p;
    }
    
    public void addParagraph(final XWPFParagraph p) {
        this.paragraphs.add(p);
    }
    
    public void removeParagraph(final int pos) {
        this.paragraphs.remove(pos);
        this.ctTc.removeP(pos);
    }
    
    public XWPFParagraph getParagraph(final CTP p) {
        for (final XWPFParagraph paragraph : this.paragraphs) {
            if (p.equals(paragraph.getCTP())) {
                return paragraph;
            }
        }
        return null;
    }
    
    public void setText(final String text) {
        final CTP ctP = (this.ctTc.sizeOfPArray() == 0) ? this.ctTc.addNewP() : this.ctTc.getPArray(0);
        final XWPFParagraph par = new XWPFParagraph(ctP, this);
        par.createRun().setText(text);
    }
    
    public XWPFTableRow getTableRow() {
        return this.tableRow;
    }
    
    public void setColor(final String rgbStr) {
        final CTTcPr tcpr = this.ctTc.isSetTcPr() ? this.ctTc.getTcPr() : this.ctTc.addNewTcPr();
        final CTShd ctshd = tcpr.isSetShd() ? tcpr.getShd() : tcpr.addNewShd();
        ctshd.setColor("auto");
        ctshd.setVal(STShd.CLEAR);
        ctshd.setFill(rgbStr);
    }
    
    public String getColor() {
        String color = null;
        final CTTcPr tcpr = this.ctTc.getTcPr();
        if (tcpr != null) {
            final CTShd ctshd = tcpr.getShd();
            if (ctshd != null) {
                color = ctshd.xgetFill().getStringValue();
            }
        }
        return color;
    }
    
    public void setVerticalAlignment(final XWPFVertAlign vAlign) {
        final CTTcPr tcpr = this.ctTc.isSetTcPr() ? this.ctTc.getTcPr() : this.ctTc.addNewTcPr();
        final CTVerticalJc va = tcpr.addNewVAlign();
        va.setVal(XWPFTableCell.alignMap.get(vAlign));
    }
    
    public XWPFVertAlign getVerticalAlignment() {
        XWPFVertAlign vAlign = null;
        final CTTcPr tcpr = this.ctTc.getTcPr();
        if (this.ctTc != null) {
            final CTVerticalJc va = tcpr.getVAlign();
            vAlign = XWPFTableCell.stVertAlignTypeMap.get(va.getVal().intValue());
        }
        return vAlign;
    }
    
    public XWPFParagraph insertNewParagraph(final XmlCursor cursor) {
        if (!this.isCursorInTableCell(cursor)) {
            return null;
        }
        final String uri = CTP.type.getName().getNamespaceURI();
        final String localPart = "p";
        cursor.beginElement(localPart, uri);
        cursor.toParent();
        final CTP p = (CTP)cursor.getObject();
        final XWPFParagraph newP = new XWPFParagraph(p, this);
        XmlObject o;
        for (o = null; !(o instanceof CTP) && cursor.toPrevSibling(); o = cursor.getObject()) {}
        if (!(o instanceof CTP) || o == p) {
            this.paragraphs.add(0, newP);
        }
        else {
            final int pos = this.paragraphs.indexOf(this.getParagraph((CTP)o)) + 1;
            this.paragraphs.add(pos, newP);
        }
        int i = 0;
        cursor.toCursor(p.newCursor());
        while (cursor.toPrevSibling()) {
            o = cursor.getObject();
            if (o instanceof CTP || o instanceof CTTbl) {
                ++i;
            }
        }
        this.bodyElements.add(i, newP);
        cursor.toCursor(p.newCursor());
        cursor.toEndToken();
        return newP;
    }
    
    public XWPFTable insertNewTbl(XmlCursor cursor) {
        if (this.isCursorInTableCell(cursor)) {
            final String uri = CTTbl.type.getName().getNamespaceURI();
            final String localPart = "tbl";
            cursor.beginElement(localPart, uri);
            cursor.toParent();
            final CTTbl t = (CTTbl)cursor.getObject();
            final XWPFTable newT = new XWPFTable(t, this);
            cursor.removeXmlContents();
            XmlObject o;
            for (o = null; !(o instanceof CTTbl) && cursor.toPrevSibling(); o = cursor.getObject()) {}
            if (!(o instanceof CTTbl)) {
                this.tables.add(0, newT);
            }
            else {
                final int pos = this.tables.indexOf(this.getTable((CTTbl)o)) + 1;
                this.tables.add(pos, newT);
            }
            int i = 0;
            cursor = t.newCursor();
            while (cursor.toPrevSibling()) {
                o = cursor.getObject();
                if (o instanceof CTP || o instanceof CTTbl) {
                    ++i;
                }
            }
            this.bodyElements.add(i, newT);
            cursor = t.newCursor();
            cursor.toEndToken();
            return newT;
        }
        return null;
    }
    
    private boolean isCursorInTableCell(final XmlCursor cursor) {
        final XmlCursor verify = cursor.newCursor();
        verify.toParent();
        return verify.getObject() == this.ctTc;
    }
    
    public XWPFParagraph getParagraphArray(final int pos) {
        if (pos > 0 && pos < this.paragraphs.size()) {
            return this.paragraphs.get(pos);
        }
        return null;
    }
    
    public POIXMLDocumentPart getPart() {
        return this.tableRow.getTable().getPart();
    }
    
    public BodyType getPartType() {
        return BodyType.TABLECELL;
    }
    
    public XWPFTable getTable(final CTTbl ctTable) {
        for (int i = 0; i < this.tables.size(); ++i) {
            if (this.getTables().get(i).getCTTbl() == ctTable) {
                return this.getTables().get(i);
            }
        }
        return null;
    }
    
    public XWPFTable getTableArray(final int pos) {
        if (pos > 0 && pos < this.tables.size()) {
            return this.tables.get(pos);
        }
        return null;
    }
    
    public List<XWPFTable> getTables() {
        return Collections.unmodifiableList((List<? extends XWPFTable>)this.tables);
    }
    
    public void insertTable(final int pos, final XWPFTable table) {
        this.bodyElements.add(pos, table);
        int i;
        for (i = 0; i < this.ctTc.getTblList().size(); ++i) {
            final CTTbl tbl = this.ctTc.getTblArray(i);
            if (tbl == table.getCTTbl()) {
                break;
            }
        }
        this.tables.add(i, table);
    }
    
    public String getText() {
        final StringBuffer text = new StringBuffer();
        for (final XWPFParagraph p : this.paragraphs) {
            text.append(p.getText());
        }
        return text.toString();
    }
    
    public XWPFTableCell getTableCell(final CTTc cell) {
        final XmlCursor cursor = cell.newCursor();
        cursor.toParent();
        XmlObject o = cursor.getObject();
        if (!(o instanceof CTRow)) {
            return null;
        }
        final CTRow row = (CTRow)o;
        cursor.toParent();
        o = cursor.getObject();
        cursor.dispose();
        if (!(o instanceof CTTbl)) {
            return null;
        }
        final CTTbl tbl = (CTTbl)o;
        final XWPFTable table = this.getTable(tbl);
        if (table == null) {
            return null;
        }
        final XWPFTableRow tableRow = table.getRow(row);
        if (tableRow == null) {
            return null;
        }
        return tableRow.getTableCell(cell);
    }
    
    public XWPFDocument getXWPFDocument() {
        return this.part.getXWPFDocument();
    }
    
    static {
        (XWPFTableCell.alignMap = new EnumMap<XWPFVertAlign, STVerticalJc.Enum>(XWPFVertAlign.class)).put(XWPFVertAlign.TOP, STVerticalJc.Enum.forInt(1));
        XWPFTableCell.alignMap.put(XWPFVertAlign.CENTER, STVerticalJc.Enum.forInt(2));
        XWPFTableCell.alignMap.put(XWPFVertAlign.BOTH, STVerticalJc.Enum.forInt(3));
        XWPFTableCell.alignMap.put(XWPFVertAlign.BOTTOM, STVerticalJc.Enum.forInt(4));
        (XWPFTableCell.stVertAlignTypeMap = new HashMap<Integer, XWPFVertAlign>()).put(1, XWPFVertAlign.TOP);
        XWPFTableCell.stVertAlignTypeMap.put(2, XWPFVertAlign.CENTER);
        XWPFTableCell.stVertAlignTypeMap.put(3, XWPFVertAlign.BOTH);
        XWPFTableCell.stVertAlignTypeMap.put(4, XWPFVertAlign.BOTTOM);
    }
    
    public enum XWPFVertAlign
    {
        TOP, 
        CENTER, 
        BOTH, 
        BOTTOM;
    }
}
