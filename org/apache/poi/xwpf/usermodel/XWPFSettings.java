// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.SettingsDocument;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocProtect;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STDocProtect;
import java.math.BigInteger;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTZoom;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSettings;
import org.apache.poi.POIXMLDocumentPart;

public class XWPFSettings extends POIXMLDocumentPart
{
    private CTSettings ctSettings;
    
    public XWPFSettings(final PackagePart part, final PackageRelationship rel) throws IOException {
        super(part, rel);
    }
    
    public XWPFSettings() {
        this.ctSettings = CTSettings.Factory.newInstance();
    }
    
    @Override
    protected void onDocumentRead() throws IOException {
        super.onDocumentRead();
        this.readFrom(this.getPackagePart().getInputStream());
    }
    
    public long getZoomPercent() {
        CTZoom zoom;
        if (!this.ctSettings.isSetZoom()) {
            zoom = this.ctSettings.addNewZoom();
        }
        else {
            zoom = this.ctSettings.getZoom();
        }
        return zoom.getPercent().longValue();
    }
    
    public void setZoomPercent(final long zoomPercent) {
        if (!this.ctSettings.isSetZoom()) {
            this.ctSettings.addNewZoom();
        }
        final CTZoom zoom = this.ctSettings.getZoom();
        zoom.setPercent(BigInteger.valueOf(zoomPercent));
    }
    
    public boolean isEnforcedWith(final STDocProtect.Enum editValue) {
        final CTDocProtect ctDocProtect = this.ctSettings.getDocumentProtection();
        return ctDocProtect != null && ctDocProtect.getEnforcement().equals(STOnOff.X_1) && ctDocProtect.getEdit().equals(editValue);
    }
    
    public void setEnforcementEditValue(final STDocProtect.Enum editValue) {
        this.safeGetDocumentProtection().setEnforcement(STOnOff.X_1);
        this.safeGetDocumentProtection().setEdit(editValue);
    }
    
    public void removeEnforcement() {
        this.safeGetDocumentProtection().setEnforcement(STOnOff.X_0);
    }
    
    public void setUpdateFields() {
        final CTOnOff onOff = CTOnOff.Factory.newInstance();
        onOff.setVal(STOnOff.TRUE);
        this.ctSettings.setUpdateFields(onOff);
    }
    
    boolean isUpdateFields() {
        return this.ctSettings.isSetUpdateFields() && this.ctSettings.getUpdateFields().getVal() == STOnOff.TRUE;
    }
    
    @Override
    protected void commit() throws IOException {
        if (this.ctSettings == null) {
            throw new IllegalStateException("Unable to write out settings that were never read in!");
        }
        final XmlOptions xmlOptions = new XmlOptions(XWPFSettings.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTSettings.type.getName().getNamespaceURI(), "settings"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "w");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.ctSettings.save(out, xmlOptions);
        out.close();
    }
    
    private CTDocProtect safeGetDocumentProtection() {
        CTDocProtect documentProtection = this.ctSettings.getDocumentProtection();
        if (documentProtection == null) {
            documentProtection = CTDocProtect.Factory.newInstance();
            this.ctSettings.setDocumentProtection(documentProtection);
        }
        return this.ctSettings.getDocumentProtection();
    }
    
    private void readFrom(final InputStream inputStream) {
        try {
            this.ctSettings = SettingsDocument.Factory.parse(inputStream).getSettings();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
