// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTEmpty;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPTab;
import org.apache.xmlbeans.XmlCursor;
import javax.xml.namespace.QName;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetGeometry2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlip;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlipFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualPictureProperties;
import org.openxmlformats.schemas.drawingml.x2006.picture.CTPictureNonVisual;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObjectData;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.main.STShapeType;
import org.apache.xmlbeans.XmlToken;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObject;
import java.io.InputStream;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBrClear;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBrType;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSignedHpsMeasure;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHpsMeasure;
import java.math.BigInteger;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFonts;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVerticalAlignRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalAlignRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTUnderline;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STUnderline;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.apache.xmlbeans.XmlString;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTColor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.apache.poi.util.Internal;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import org.apache.xmlbeans.impl.values.XmlAnyTypeImpl;
import org.w3c.dom.NodeList;
import java.util.Iterator;
import org.openxmlformats.schemas.drawingml.x2006.picture.CTPicture;
import org.w3c.dom.Text;
import java.util.Collection;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTInline;
import org.openxmlformats.schemas.drawingml.x2006.wordprocessingDrawing.CTAnchor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDrawing;
import java.util.List;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;

public class XWPFRun
{
    private CTR run;
    private String pictureText;
    private XWPFParagraph paragraph;
    private List<XWPFPicture> pictures;
    
    public XWPFRun(final CTR r, final XWPFParagraph p) {
        this.run = r;
        this.paragraph = p;
        final List<CTDrawing> drawingList = r.getDrawingList();
        for (final CTDrawing ctDrawing : drawingList) {
            final List<CTAnchor> anchorList = ctDrawing.getAnchorList();
            for (final CTAnchor anchor : anchorList) {
                if (anchor.getDocPr() != null) {
                    this.getDocument().getDrawingIdManager().reserve(anchor.getDocPr().getId());
                }
            }
            final List<CTInline> inlineList = ctDrawing.getInlineList();
            for (final CTInline inline : inlineList) {
                if (inline.getDocPr() != null) {
                    this.getDocument().getDrawingIdManager().reserve(inline.getDocPr().getId());
                }
            }
        }
        final StringBuffer text = new StringBuffer();
        final List<XmlObject> pictTextObjs = new ArrayList<XmlObject>();
        pictTextObjs.addAll(r.getPictList());
        pictTextObjs.addAll(drawingList);
        for (final XmlObject o : pictTextObjs) {
            final XmlObject[] t = o.selectPath("declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' .//w:t");
            for (int m = 0; m < t.length; ++m) {
                final NodeList kids = t[m].getDomNode().getChildNodes();
                for (int n = 0; n < kids.getLength(); ++n) {
                    if (kids.item(n) instanceof Text) {
                        if (text.length() > 0) {
                            text.append("\n");
                        }
                        text.append(kids.item(n).getNodeValue());
                    }
                }
            }
        }
        this.pictureText = text.toString();
        this.pictures = new ArrayList<XWPFPicture>();
        for (final XmlObject o : pictTextObjs) {
            for (final CTPicture pict : this.getCTPictures(o)) {
                final XWPFPicture picture = new XWPFPicture(pict, this);
                this.pictures.add(picture);
            }
        }
    }
    
    private List<CTPicture> getCTPictures(final XmlObject o) {
        final List<CTPicture> pictures = new ArrayList<CTPicture>();
        final XmlObject[] arr$;
        final XmlObject[] picts = arr$ = o.selectPath("declare namespace pic='" + CTPicture.type.getName().getNamespaceURI() + "' .//pic:pic");
        for (XmlObject pict : arr$) {
            if (pict instanceof XmlAnyTypeImpl) {
                try {
                    pict = CTPicture.Factory.parse(pict.toString());
                }
                catch (XmlException e) {
                    throw new POIXMLException(e);
                }
            }
            if (pict instanceof CTPicture) {
                pictures.add((CTPicture)pict);
            }
        }
        return pictures;
    }
    
    @Internal
    public CTR getCTR() {
        return this.run;
    }
    
    public XWPFParagraph getParagraph() {
        return this.paragraph;
    }
    
    public XWPFDocument getDocument() {
        if (this.paragraph != null) {
            return this.paragraph.getDocument();
        }
        return null;
    }
    
    private boolean isCTOnOff(final CTOnOff onoff) {
        return !onoff.isSetVal() || onoff.getVal() == STOnOff.ON || onoff.getVal() == STOnOff.TRUE;
    }
    
    public boolean isBold() {
        final CTRPr pr = this.run.getRPr();
        return pr != null && pr.isSetB() && this.isCTOnOff(pr.getB());
    }
    
    public void setBold(final boolean value) {
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTOnOff bold = pr.isSetB() ? pr.getB() : pr.addNewB();
        bold.setVal(value ? STOnOff.TRUE : STOnOff.FALSE);
    }
    
    public String getColor() {
        String color = null;
        if (this.run.isSetRPr()) {
            final CTRPr pr = this.run.getRPr();
            if (pr.isSetColor()) {
                final CTColor clr = pr.getColor();
                color = clr.xgetVal().getStringValue();
            }
        }
        return color;
    }
    
    public void setColor(final String rgbStr) {
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTColor color = pr.isSetColor() ? pr.getColor() : pr.addNewColor();
        color.setVal(rgbStr);
    }
    
    public String getText(final int pos) {
        return (this.run.sizeOfTArray() == 0) ? null : this.run.getTArray(pos).getStringValue();
    }
    
    public String getPictureText() {
        return this.pictureText;
    }
    
    public void setText(final String value) {
        this.setText(value, this.run.getTList().size());
    }
    
    public void setText(final String value, final int pos) {
        if (pos > this.run.sizeOfTArray()) {
            throw new ArrayIndexOutOfBoundsException("Value too large for the parameter position in XWPFRun.setText(String value,int pos)");
        }
        final CTText t = (pos < this.run.sizeOfTArray() && pos >= 0) ? this.run.getTArray(pos) : this.run.addNewT();
        t.setStringValue(value);
        preserveSpaces(t);
    }
    
    public boolean isItalic() {
        final CTRPr pr = this.run.getRPr();
        return pr != null && pr.isSetI() && this.isCTOnOff(pr.getI());
    }
    
    public void setItalic(final boolean value) {
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTOnOff italic = pr.isSetI() ? pr.getI() : pr.addNewI();
        italic.setVal(value ? STOnOff.TRUE : STOnOff.FALSE);
    }
    
    public UnderlinePatterns getUnderline() {
        final CTRPr pr = this.run.getRPr();
        return (pr != null && pr.isSetU()) ? UnderlinePatterns.valueOf(pr.getU().getVal().intValue()) : UnderlinePatterns.NONE;
    }
    
    public void setUnderline(final UnderlinePatterns value) {
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTUnderline underline = (pr.getU() == null) ? pr.addNewU() : pr.getU();
        underline.setVal(STUnderline.Enum.forInt(value.getValue()));
    }
    
    public boolean isStrike() {
        final CTRPr pr = this.run.getRPr();
        return pr != null && pr.isSetStrike() && this.isCTOnOff(pr.getStrike());
    }
    
    public void setStrike(final boolean value) {
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTOnOff strike = pr.isSetStrike() ? pr.getStrike() : pr.addNewStrike();
        strike.setVal(value ? STOnOff.TRUE : STOnOff.FALSE);
    }
    
    public VerticalAlign getSubscript() {
        final CTRPr pr = this.run.getRPr();
        return (pr != null && pr.isSetVertAlign()) ? VerticalAlign.valueOf(pr.getVertAlign().getVal().intValue()) : VerticalAlign.BASELINE;
    }
    
    public void setSubscript(final VerticalAlign valign) {
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTVerticalAlignRun ctValign = pr.isSetVertAlign() ? pr.getVertAlign() : pr.addNewVertAlign();
        ctValign.setVal(STVerticalAlignRun.Enum.forInt(valign.getValue()));
    }
    
    public String getFontFamily() {
        final CTRPr pr = this.run.getRPr();
        return (pr != null && pr.isSetRFonts()) ? pr.getRFonts().getAscii() : null;
    }
    
    public void setFontFamily(final String fontFamily) {
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTFonts fonts = pr.isSetRFonts() ? pr.getRFonts() : pr.addNewRFonts();
        fonts.setAscii(fontFamily);
    }
    
    public int getFontSize() {
        final CTRPr pr = this.run.getRPr();
        return (pr != null && pr.isSetSz()) ? pr.getSz().getVal().divide(new BigInteger("2")).intValue() : -1;
    }
    
    public void setFontSize(final int size) {
        final BigInteger bint = new BigInteger("" + size);
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTHpsMeasure ctSize = pr.isSetSz() ? pr.getSz() : pr.addNewSz();
        ctSize.setVal(bint.multiply(new BigInteger("2")));
    }
    
    public int getTextPosition() {
        final CTRPr pr = this.run.getRPr();
        return (pr != null && pr.isSetPosition()) ? pr.getPosition().getVal().intValue() : -1;
    }
    
    public void setTextPosition(final int val) {
        final BigInteger bint = new BigInteger("" + val);
        final CTRPr pr = this.run.isSetRPr() ? this.run.getRPr() : this.run.addNewRPr();
        final CTSignedHpsMeasure position = pr.isSetPosition() ? pr.getPosition() : pr.addNewPosition();
        position.setVal(bint);
    }
    
    public void removeBreak() {
    }
    
    public void addBreak() {
        this.run.addNewBr();
    }
    
    public void addBreak(final BreakType type) {
        final CTBr br = this.run.addNewBr();
        br.setType(STBrType.Enum.forInt(type.getValue()));
    }
    
    public void addBreak(final BreakClear clear) {
        final CTBr br = this.run.addNewBr();
        br.setType(STBrType.Enum.forInt(BreakType.TEXT_WRAPPING.getValue()));
        br.setClear(STBrClear.Enum.forInt(clear.getValue()));
    }
    
    public void addCarriageReturn() {
        this.run.addNewCr();
    }
    
    public void removeCarriageReturn() {
    }
    
    public XWPFPicture addPicture(final InputStream pictureData, final int pictureType, final String filename, final int width, final int height) throws InvalidFormatException, IOException {
        final XWPFDocument doc = this.paragraph.document;
        final String relationId = doc.addPictureData(pictureData, pictureType);
        final XWPFPictureData picData = (XWPFPictureData)doc.getRelationById(relationId);
        try {
            final CTDrawing drawing = this.run.addNewDrawing();
            final CTInline inline = drawing.addNewInline();
            final String xml = "<a:graphic xmlns:a=\"" + CTGraphicalObject.type.getName().getNamespaceURI() + "\">" + "<a:graphicData uri=\"" + CTPicture.type.getName().getNamespaceURI() + "\">" + "<pic:pic xmlns:pic=\"" + CTPicture.type.getName().getNamespaceURI() + "\" />" + "</a:graphicData>" + "</a:graphic>";
            inline.set(XmlToken.Factory.parse(xml));
            inline.setDistT(0L);
            inline.setDistR(0L);
            inline.setDistB(0L);
            inline.setDistL(0L);
            final CTNonVisualDrawingProps docPr = inline.addNewDocPr();
            final long id = this.getParagraph().document.getDrawingIdManager().reserveNew();
            docPr.setId(id);
            docPr.setName("Drawing " + id);
            docPr.setDescr(filename);
            final CTPositiveSize2D extent = inline.addNewExtent();
            extent.setCx(width);
            extent.setCy(height);
            final CTGraphicalObject graphic = inline.getGraphic();
            final CTGraphicalObjectData graphicData = graphic.getGraphicData();
            final CTPicture pic = this.getCTPictures(graphicData).get(0);
            final CTPictureNonVisual nvPicPr = pic.addNewNvPicPr();
            final CTNonVisualDrawingProps cNvPr = nvPicPr.addNewCNvPr();
            cNvPr.setId(0L);
            cNvPr.setName("Picture " + id);
            cNvPr.setDescr(filename);
            final CTNonVisualPictureProperties cNvPicPr = nvPicPr.addNewCNvPicPr();
            cNvPicPr.addNewPicLocks().setNoChangeAspect(true);
            final CTBlipFillProperties blipFill = pic.addNewBlipFill();
            final CTBlip blip = blipFill.addNewBlip();
            blip.setEmbed(picData.getPackageRelationship().getId());
            blipFill.addNewStretch().addNewFillRect();
            final CTShapeProperties spPr = pic.addNewSpPr();
            final CTTransform2D xfrm = spPr.addNewXfrm();
            final CTPoint2D off = xfrm.addNewOff();
            off.setX(0L);
            off.setY(0L);
            final CTPositiveSize2D ext = xfrm.addNewExt();
            ext.setCx(width);
            ext.setCy(height);
            final CTPresetGeometry2D prstGeom = spPr.addNewPrstGeom();
            prstGeom.setPrst(STShapeType.RECT);
            prstGeom.addNewAvLst();
            final XWPFPicture xwpfPicture = new XWPFPicture(pic, this);
            this.pictures.add(xwpfPicture);
            return xwpfPicture;
        }
        catch (XmlException e) {
            throw new IllegalStateException(e);
        }
    }
    
    public List<XWPFPicture> getEmbeddedPictures() {
        return this.pictures;
    }
    
    static void preserveSpaces(final XmlString xs) {
        final String text = xs.getStringValue();
        if (text != null && (text.startsWith(" ") || text.endsWith(" "))) {
            final XmlCursor c = xs.newCursor();
            c.toNextToken();
            c.insertAttributeWithValue(new QName("http://www.w3.org/XML/1998/namespace", "space"), "preserve");
            c.dispose();
        }
    }
    
    @Override
    public String toString() {
        final StringBuffer text = new StringBuffer();
        final XmlCursor c = this.run.newCursor();
        c.selectPath("./*");
        while (c.toNextSelection()) {
            final XmlObject o = c.getObject();
            if (o instanceof CTText) {
                final String tagName = o.getDomNode().getNodeName();
                if (!"w:instrText".equals(tagName)) {
                    text.append(((CTText)o).getStringValue());
                }
            }
            if (o instanceof CTPTab) {
                text.append("\t");
            }
            if (o instanceof CTBr) {
                text.append("\n");
            }
            if (o instanceof CTEmpty) {
                final String tagName = o.getDomNode().getNodeName();
                if ("w:tab".equals(tagName)) {
                    text.append("\t");
                }
                if ("w:br".equals(tagName)) {
                    text.append("\n");
                }
                if (!"w:cr".equals(tagName)) {
                    continue;
                }
                text.append("\n");
            }
        }
        c.dispose();
        if (this.pictureText != null && this.pictureText.length() > 0) {
            text.append("\n").append(this.pictureText);
        }
        return text.toString();
    }
}
