// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

public enum BodyType
{
    DOCUMENT, 
    HEADER, 
    FOOTER, 
    FOOTNOTE, 
    TABLECELL;
}
