// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.apache.poi.POIXMLDocumentPart;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import java.util.Iterator;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFtnEdn;
import java.util.List;

public class XWPFFootnote implements Iterable<XWPFParagraph>, IBody
{
    private List<XWPFParagraph> paragraphs;
    private List<XWPFTable> tables;
    private List<XWPFPictureData> pictures;
    private List<IBodyElement> bodyElements;
    private CTFtnEdn ctFtnEdn;
    private XWPFFootnotes footnotes;
    
    public XWPFFootnote(final CTFtnEdn note, final XWPFFootnotes xFootnotes) {
        this.paragraphs = new ArrayList<XWPFParagraph>();
        this.tables = new ArrayList<XWPFTable>();
        this.pictures = new ArrayList<XWPFPictureData>();
        this.bodyElements = new ArrayList<IBodyElement>();
        this.footnotes = xFootnotes;
        this.ctFtnEdn = note;
        for (final CTP p : this.ctFtnEdn.getPList()) {
            this.paragraphs.add(new XWPFParagraph(p, this));
        }
    }
    
    public XWPFFootnote(final XWPFDocument document, final CTFtnEdn body) {
        this.paragraphs = new ArrayList<XWPFParagraph>();
        this.tables = new ArrayList<XWPFTable>();
        this.pictures = new ArrayList<XWPFPictureData>();
        this.bodyElements = new ArrayList<IBodyElement>();
        for (final CTP p : body.getPList()) {
            this.paragraphs.add(new XWPFParagraph(p, document));
        }
    }
    
    public List<XWPFParagraph> getParagraphs() {
        return this.paragraphs;
    }
    
    public Iterator<XWPFParagraph> iterator() {
        return this.paragraphs.iterator();
    }
    
    public List<XWPFTable> getTables() {
        return this.tables;
    }
    
    public List<XWPFPictureData> getPictures() {
        return this.pictures;
    }
    
    public List<IBodyElement> getBodyElements() {
        return this.bodyElements;
    }
    
    public CTFtnEdn getCTFtnEdn() {
        return this.ctFtnEdn;
    }
    
    public void setCTFtnEdn(final CTFtnEdn footnote) {
        this.ctFtnEdn = footnote;
    }
    
    public XWPFTable getTableArray(final int pos) {
        if (pos > 0 && pos < this.tables.size()) {
            return this.tables.get(pos);
        }
        return null;
    }
    
    public void insertTable(final int pos, final XWPFTable table) {
        this.bodyElements.add(pos, table);
        int i;
        for (i = 0; i < this.ctFtnEdn.getTblList().size(); ++i) {
            final CTTbl tbl = this.ctFtnEdn.getTblArray(i);
            if (tbl == table.getCTTbl()) {
                break;
            }
        }
        this.tables.add(i, table);
    }
    
    public XWPFTable getTable(final CTTbl ctTable) {
        for (final XWPFTable table : this.tables) {
            if (table == null) {
                return null;
            }
            if (table.getCTTbl().equals(ctTable)) {
                return table;
            }
        }
        return null;
    }
    
    public XWPFParagraph getParagraph(final CTP p) {
        for (final XWPFParagraph paragraph : this.paragraphs) {
            if (paragraph.getCTP().equals(p)) {
                return paragraph;
            }
        }
        return null;
    }
    
    public XWPFParagraph getParagraphArray(final int pos) {
        return this.paragraphs.get(pos);
    }
    
    public XWPFTableCell getTableCell(final CTTc cell) {
        final XmlCursor cursor = cell.newCursor();
        cursor.toParent();
        XmlObject o = cursor.getObject();
        if (!(o instanceof CTRow)) {
            return null;
        }
        final CTRow row = (CTRow)o;
        cursor.toParent();
        o = cursor.getObject();
        cursor.dispose();
        if (!(o instanceof CTTbl)) {
            return null;
        }
        final CTTbl tbl = (CTTbl)o;
        final XWPFTable table = this.getTable(tbl);
        if (table == null) {
            return null;
        }
        final XWPFTableRow tableRow = table.getRow(row);
        if (row == null) {
            return null;
        }
        return tableRow.getTableCell(cell);
    }
    
    private boolean isCursorInFtn(final XmlCursor cursor) {
        final XmlCursor verify = cursor.newCursor();
        verify.toParent();
        return verify.getObject() == this.ctFtnEdn;
    }
    
    public POIXMLDocumentPart getOwner() {
        return this.footnotes;
    }
    
    public XWPFTable insertNewTbl(XmlCursor cursor) {
        if (this.isCursorInFtn(cursor)) {
            final String uri = CTTbl.type.getName().getNamespaceURI();
            final String localPart = "tbl";
            cursor.beginElement(localPart, uri);
            cursor.toParent();
            final CTTbl t = (CTTbl)cursor.getObject();
            final XWPFTable newT = new XWPFTable(t, this);
            cursor.removeXmlContents();
            XmlObject o;
            for (o = null; !(o instanceof CTTbl) && cursor.toPrevSibling(); o = cursor.getObject()) {}
            if (!(o instanceof CTTbl)) {
                this.tables.add(0, newT);
            }
            else {
                final int pos = this.tables.indexOf(this.getTable((CTTbl)o)) + 1;
                this.tables.add(pos, newT);
            }
            int i = 0;
            cursor = t.newCursor();
            while (cursor.toPrevSibling()) {
                o = cursor.getObject();
                if (o instanceof CTP || o instanceof CTTbl) {
                    ++i;
                }
            }
            this.bodyElements.add(i, newT);
            cursor = t.newCursor();
            cursor.toEndToken();
            return newT;
        }
        return null;
    }
    
    public XWPFParagraph insertNewParagraph(final XmlCursor cursor) {
        if (this.isCursorInFtn(cursor)) {
            final String uri = CTP.type.getName().getNamespaceURI();
            final String localPart = "p";
            cursor.beginElement(localPart, uri);
            cursor.toParent();
            final CTP p = (CTP)cursor.getObject();
            final XWPFParagraph newP = new XWPFParagraph(p, this);
            XmlObject o;
            for (o = null; !(o instanceof CTP) && cursor.toPrevSibling(); o = cursor.getObject()) {}
            if (!(o instanceof CTP) || o == p) {
                this.paragraphs.add(0, newP);
            }
            else {
                final int pos = this.paragraphs.indexOf(this.getParagraph((CTP)o)) + 1;
                this.paragraphs.add(pos, newP);
            }
            int i = 0;
            cursor.toCursor(p.newCursor());
            while (cursor.toPrevSibling()) {
                o = cursor.getObject();
                if (o instanceof CTP || o instanceof CTTbl) {
                    ++i;
                }
            }
            this.bodyElements.add(i, newP);
            cursor.toCursor(p.newCursor());
            cursor.toEndToken();
            return newP;
        }
        return null;
    }
    
    public XWPFTable addNewTbl(final CTTbl table) {
        final CTTbl newTable = this.ctFtnEdn.addNewTbl();
        newTable.set(table);
        final XWPFTable xTable = new XWPFTable(newTable, this);
        this.tables.add(xTable);
        return xTable;
    }
    
    public XWPFParagraph addNewParagraph(final CTP paragraph) {
        final CTP newPara = this.ctFtnEdn.addNewP();
        newPara.set(paragraph);
        final XWPFParagraph xPara = new XWPFParagraph(newPara, this);
        this.paragraphs.add(xPara);
        return xPara;
    }
    
    public XWPFDocument getXWPFDocument() {
        return this.footnotes.getXWPFDocument();
    }
    
    public POIXMLDocumentPart getPart() {
        return this.footnotes;
    }
    
    public BodyType getPartType() {
        return BodyType.FOOTNOTE;
    }
}
