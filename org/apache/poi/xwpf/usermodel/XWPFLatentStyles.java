// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.util.Iterator;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLsdException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLatentStyles;

public class XWPFLatentStyles
{
    private CTLatentStyles latentStyles;
    protected XWPFStyles styles;
    
    protected XWPFLatentStyles() {
    }
    
    protected XWPFLatentStyles(final CTLatentStyles latentStyles) {
        this(latentStyles, null);
    }
    
    protected XWPFLatentStyles(final CTLatentStyles latentStyles, final XWPFStyles styles) {
        this.latentStyles = latentStyles;
        this.styles = styles;
    }
    
    protected boolean isLatentStyle(final String latentStyleID) {
        final Iterator i$ = this.latentStyles.getLsdExceptionList().iterator();
        if (i$.hasNext()) {
            final CTLsdException lsd = i$.next();
            if (lsd.getName().equals(latentStyleID)) {}
            return true;
        }
        return false;
    }
}
