// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.util.HashMap;
import java.util.Map;

public enum TextAlignment
{
    TOP(1), 
    CENTER(2), 
    BASELINE(3), 
    BOTTOM(4), 
    AUTO(5);
    
    private final int value;
    private static Map<Integer, TextAlignment> imap;
    
    private TextAlignment(final int val) {
        this.value = val;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public static TextAlignment valueOf(final int type) {
        final TextAlignment align = TextAlignment.imap.get(new Integer(type));
        if (align == null) {
            throw new IllegalArgumentException("Unknown text alignment: " + type);
        }
        return align;
    }
    
    static {
        TextAlignment.imap = new HashMap<Integer, TextAlignment>();
        for (final TextAlignment p : values()) {
            TextAlignment.imap.put(new Integer(p.getValue()), p);
        }
    }
}
