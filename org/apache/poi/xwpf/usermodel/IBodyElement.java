// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.apache.poi.POIXMLDocumentPart;

public interface IBodyElement
{
    IBody getBody();
    
    POIXMLDocumentPart getPart();
    
    BodyType getPartType();
    
    BodyElementType getElementType();
}
