// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import java.util.Iterator;
import java.util.ArrayList;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHeight;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTrPr;
import java.math.BigInteger;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.apache.poi.util.Internal;
import java.util.List;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;

public class XWPFTableRow
{
    private CTRow ctRow;
    private XWPFTable table;
    private List<XWPFTableCell> tableCells;
    
    public XWPFTableRow(final CTRow row, final XWPFTable table) {
        this.table = table;
        this.ctRow = row;
        this.getTableCells();
    }
    
    @Internal
    public CTRow getCtRow() {
        return this.ctRow;
    }
    
    public XWPFTableCell createCell() {
        final XWPFTableCell tableCell = new XWPFTableCell(this.ctRow.addNewTc(), this, this.table.getBody());
        this.tableCells.add(tableCell);
        return tableCell;
    }
    
    public XWPFTableCell getCell(final int pos) {
        if (pos >= 0 && pos < this.ctRow.sizeOfTcArray()) {
            return this.getTableCells().get(pos);
        }
        return null;
    }
    
    public void removeCell(final int pos) {
        if (pos >= 0 && pos < this.ctRow.sizeOfTcArray()) {
            this.tableCells.remove(pos);
        }
    }
    
    public XWPFTableCell addNewTableCell() {
        final CTTc cell = this.ctRow.addNewTc();
        final XWPFTableCell tableCell = new XWPFTableCell(cell, this, this.table.getBody());
        this.tableCells.add(tableCell);
        return tableCell;
    }
    
    public void setHeight(final int height) {
        final CTTrPr properties = this.getTrPr();
        final CTHeight h = (properties.sizeOfTrHeightArray() == 0) ? properties.addNewTrHeight() : properties.getTrHeightArray(0);
        h.setVal(new BigInteger("" + height));
    }
    
    public int getHeight() {
        final CTTrPr properties = this.getTrPr();
        return (properties.sizeOfTrHeightArray() == 0) ? 0 : properties.getTrHeightArray(0).getVal().intValue();
    }
    
    private CTTrPr getTrPr() {
        return this.ctRow.isSetTrPr() ? this.ctRow.getTrPr() : this.ctRow.addNewTrPr();
    }
    
    public XWPFTable getTable() {
        return this.table;
    }
    
    public List<XWPFTableCell> getTableCells() {
        if (this.tableCells == null) {
            final List<XWPFTableCell> cells = new ArrayList<XWPFTableCell>();
            for (final CTTc tableCell : this.ctRow.getTcList()) {
                cells.add(new XWPFTableCell(tableCell, this, this.table.getBody()));
            }
            this.tableCells = cells;
        }
        return this.tableCells;
    }
    
    public XWPFTableCell getTableCell(final CTTc cell) {
        for (int i = 0; i < this.tableCells.size(); ++i) {
            if (this.tableCells.get(i).getCTTc() == cell) {
                return this.tableCells.get(i);
            }
        }
        return null;
    }
    
    public void setCantSplitRow(final boolean split) {
        final CTTrPr trpr = this.getTrPr();
        final CTOnOff onoff = trpr.addNewCantSplit();
        onoff.setVal(split ? STOnOff.ON : STOnOff.OFF);
    }
    
    public boolean isCantSplitRow() {
        boolean isCant = false;
        final CTTrPr trpr = this.getTrPr();
        if (trpr.sizeOfCantSplitArray() > 0) {
            final CTOnOff onoff = trpr.getCantSplitList().get(0);
            isCant = onoff.getVal().equals(STOnOff.ON);
        }
        return isCant;
    }
    
    public void setRepeatHeader(final boolean repeat) {
        final CTTrPr trpr = this.getTrPr();
        final CTOnOff onoff = trpr.addNewTblHeader();
        onoff.setVal(repeat ? STOnOff.ON : STOnOff.OFF);
    }
    
    public boolean isRepeatHeader() {
        boolean repeat = false;
        final CTTrPr trpr = this.getTrPr();
        if (trpr.sizeOfTblHeaderArray() > 0) {
            final CTOnOff rpt = trpr.getTblHeaderList().get(0);
            repeat = rpt.getVal().equals(STOnOff.ON);
        }
        return repeat;
    }
}
