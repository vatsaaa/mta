// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFonts;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPrDefault;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLanguage;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocDefaults;
import java.io.OutputStream;
import java.util.Map;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import java.util.Iterator;
import java.io.InputStream;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyle;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.StylesDocument;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyles;
import java.util.List;
import org.apache.poi.POIXMLDocumentPart;

public class XWPFStyles extends POIXMLDocumentPart
{
    private List<XWPFStyle> listStyle;
    private CTStyles ctStyles;
    XWPFLatentStyles latentStyles;
    
    public XWPFStyles(final PackagePart part, final PackageRelationship rel) throws IOException, OpenXML4JException {
        super(part, rel);
        this.listStyle = new ArrayList<XWPFStyle>();
    }
    
    public XWPFStyles() {
        this.listStyle = new ArrayList<XWPFStyle>();
    }
    
    @Override
    protected void onDocumentRead() throws IOException {
        try {
            final InputStream is = this.getPackagePart().getInputStream();
            final StylesDocument stylesDoc = StylesDocument.Factory.parse(is);
            this.ctStyles = stylesDoc.getStyles();
            this.latentStyles = new XWPFLatentStyles(this.ctStyles.getLatentStyles(), this);
        }
        catch (XmlException e) {
            throw new POIXMLException("Unable to read styles", e);
        }
        for (final CTStyle style : this.ctStyles.getStyleList()) {
            this.listStyle.add(new XWPFStyle(style, this));
        }
    }
    
    @Override
    protected void commit() throws IOException {
        if (this.ctStyles == null) {
            throw new IllegalStateException("Unable to write out styles that were never read in!");
        }
        final XmlOptions xmlOptions = new XmlOptions(XWPFStyles.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTStyles.type.getName().getNamespaceURI(), "styles"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "r");
        map.put("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "w");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.ctStyles.save(out, xmlOptions);
        out.close();
    }
    
    public void setStyles(final CTStyles styles) {
        this.ctStyles = styles;
    }
    
    public boolean styleExist(final String styleID) {
        for (final XWPFStyle style : this.listStyle) {
            if (style.getStyleId().equals(styleID)) {
                return true;
            }
        }
        return false;
    }
    
    public void addStyle(final XWPFStyle style) {
        this.listStyle.add(style);
        this.ctStyles.addNewStyle();
        final int pos = this.ctStyles.getStyleList().size() - 1;
        this.ctStyles.setStyleArray(pos, style.getCTStyle());
    }
    
    public XWPFStyle getStyle(final String styleID) {
        for (final XWPFStyle style : this.listStyle) {
            if (style.getStyleId().equals(styleID)) {
                return style;
            }
        }
        return null;
    }
    
    public List<XWPFStyle> getUsedStyleList(final XWPFStyle style) {
        final List<XWPFStyle> usedStyleList = new ArrayList<XWPFStyle>();
        usedStyleList.add(style);
        return this.getUsedStyleList(style, usedStyleList);
    }
    
    private List<XWPFStyle> getUsedStyleList(final XWPFStyle style, final List<XWPFStyle> usedStyleList) {
        final String basisStyleID = style.getBasisStyleID();
        final XWPFStyle basisStyle = this.getStyle(basisStyleID);
        if (basisStyle != null && !usedStyleList.contains(basisStyle)) {
            usedStyleList.add(basisStyle);
            this.getUsedStyleList(basisStyle, usedStyleList);
        }
        final String linkStyleID = style.getLinkStyleID();
        final XWPFStyle linkStyle = this.getStyle(linkStyleID);
        if (linkStyle != null && !usedStyleList.contains(linkStyle)) {
            usedStyleList.add(linkStyle);
            this.getUsedStyleList(linkStyle, usedStyleList);
        }
        final String nextStyleID = style.getNextStyleID();
        final XWPFStyle nextStyle = this.getStyle(nextStyleID);
        if (nextStyle != null && !usedStyleList.contains(nextStyle)) {
            usedStyleList.add(linkStyle);
            this.getUsedStyleList(linkStyle, usedStyleList);
        }
        return usedStyleList;
    }
    
    public void setSpellingLanguage(final String strSpellingLanguage) {
        CTDocDefaults docDefaults = null;
        CTRPr runProps = null;
        CTLanguage lang = null;
        if (this.ctStyles.isSetDocDefaults()) {
            docDefaults = this.ctStyles.getDocDefaults();
            if (docDefaults.isSetRPrDefault()) {
                final CTRPrDefault RPrDefault = docDefaults.getRPrDefault();
                if (RPrDefault.isSetRPr()) {
                    runProps = RPrDefault.getRPr();
                    if (runProps.isSetLang()) {
                        lang = runProps.getLang();
                    }
                }
            }
        }
        if (docDefaults == null) {
            docDefaults = this.ctStyles.addNewDocDefaults();
        }
        if (runProps == null) {
            runProps = docDefaults.addNewRPrDefault().addNewRPr();
        }
        if (lang == null) {
            lang = runProps.addNewLang();
        }
        lang.setVal((Object)strSpellingLanguage);
        lang.setBidi((Object)strSpellingLanguage);
    }
    
    public void setEastAsia(final String strEastAsia) {
        CTDocDefaults docDefaults = null;
        CTRPr runProps = null;
        CTLanguage lang = null;
        if (this.ctStyles.isSetDocDefaults()) {
            docDefaults = this.ctStyles.getDocDefaults();
            if (docDefaults.isSetRPrDefault()) {
                final CTRPrDefault RPrDefault = docDefaults.getRPrDefault();
                if (RPrDefault.isSetRPr()) {
                    runProps = RPrDefault.getRPr();
                    if (runProps.isSetLang()) {
                        lang = runProps.getLang();
                    }
                }
            }
        }
        if (docDefaults == null) {
            docDefaults = this.ctStyles.addNewDocDefaults();
        }
        if (runProps == null) {
            runProps = docDefaults.addNewRPrDefault().addNewRPr();
        }
        if (lang == null) {
            lang = runProps.addNewLang();
        }
        lang.setEastAsia((Object)strEastAsia);
    }
    
    public void setDefaultFonts(final CTFonts fonts) {
        CTDocDefaults docDefaults = null;
        CTRPr runProps = null;
        if (this.ctStyles.isSetDocDefaults()) {
            docDefaults = this.ctStyles.getDocDefaults();
            if (docDefaults.isSetRPrDefault()) {
                final CTRPrDefault RPrDefault = docDefaults.getRPrDefault();
                if (RPrDefault.isSetRPr()) {
                    runProps = RPrDefault.getRPr();
                }
            }
        }
        if (docDefaults == null) {
            docDefaults = this.ctStyles.addNewDocDefaults();
        }
        if (runProps == null) {
            runProps = docDefaults.addNewRPrDefault().addNewRPr();
        }
        runProps.setRFonts(fonts);
    }
    
    public XWPFLatentStyles getLatentStyles() {
        return this.latentStyles;
    }
    
    public XWPFStyle getStyleWithSameName(final XWPFStyle style) {
        for (final XWPFStyle ownStyle : this.listStyle) {
            if (ownStyle.hasSameName(style)) {
                return ownStyle;
            }
        }
        return null;
    }
}
