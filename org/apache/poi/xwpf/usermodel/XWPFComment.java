// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.util.Iterator;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTComment;

public class XWPFComment
{
    protected String id;
    protected String author;
    protected StringBuffer text;
    
    public XWPFComment(final CTComment comment, final XWPFDocument document) {
        this.text = new StringBuffer();
        this.id = comment.getId().toString();
        this.author = comment.getAuthor();
        for (final CTP ctp : comment.getPList()) {
            final XWPFParagraph p = new XWPFParagraph(ctp, document);
            this.text.append(p.getText());
        }
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getAuthor() {
        return this.author;
    }
    
    public String getText() {
        return this.text.toString();
    }
}
