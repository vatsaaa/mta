// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.util.HashMap;
import java.util.Map;

public enum BreakType
{
    PAGE(1), 
    COLUMN(2), 
    TEXT_WRAPPING(3);
    
    private final int value;
    private static Map<Integer, BreakType> imap;
    
    private BreakType(final int val) {
        this.value = val;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public static BreakType valueOf(final int type) {
        final BreakType bType = BreakType.imap.get(new Integer(type));
        if (bType == null) {
            throw new IllegalArgumentException("Unknown break type: " + type);
        }
        return bType;
    }
    
    static {
        BreakType.imap = new HashMap<Integer, BreakType>();
        for (final BreakType p : values()) {
            BreakType.imap.put(new Integer(p.getValue()), p);
        }
    }
}
