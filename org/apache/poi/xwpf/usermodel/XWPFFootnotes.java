// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.apache.xmlbeans.XmlObject;
import java.io.OutputStream;
import java.util.Map;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import java.util.Iterator;
import java.io.InputStream;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFtnEdn;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.FootnotesDocument;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFootnotes;
import java.util.List;
import org.apache.poi.POIXMLDocumentPart;

public class XWPFFootnotes extends POIXMLDocumentPart
{
    private List<XWPFFootnote> listFootnote;
    private CTFootnotes ctFootnotes;
    protected XWPFDocument document;
    
    public XWPFFootnotes(final PackagePart part, final PackageRelationship rel) throws IOException, OpenXML4JException {
        super(part, rel);
        this.listFootnote = new ArrayList<XWPFFootnote>();
    }
    
    public XWPFFootnotes() {
        this.listFootnote = new ArrayList<XWPFFootnote>();
    }
    
    @Override
    protected void onDocumentRead() throws IOException {
        try {
            final InputStream is = this.getPackagePart().getInputStream();
            final FootnotesDocument notesDoc = FootnotesDocument.Factory.parse(is);
            this.ctFootnotes = notesDoc.getFootnotes();
        }
        catch (XmlException e) {
            throw new POIXMLException();
        }
        for (final CTFtnEdn note : this.ctFootnotes.getFootnoteList()) {
            this.listFootnote.add(new XWPFFootnote(note, this));
        }
    }
    
    @Override
    protected void commit() throws IOException {
        final XmlOptions xmlOptions = new XmlOptions(XWPFFootnotes.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTFootnotes.type.getName().getNamespaceURI(), "footnotes"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "r");
        map.put("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "w");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.ctFootnotes.save(out, xmlOptions);
        out.close();
    }
    
    public List<XWPFFootnote> getFootnotesList() {
        return this.listFootnote;
    }
    
    public XWPFFootnote getFootnoteById(final int id) {
        for (final XWPFFootnote note : this.listFootnote) {
            if (note.getCTFtnEdn().getId().intValue() == id) {
                return note;
            }
        }
        return null;
    }
    
    public void setFootnotes(final CTFootnotes footnotes) {
        this.ctFootnotes = footnotes;
    }
    
    public void addFootnote(final XWPFFootnote footnote) {
        this.listFootnote.add(footnote);
        this.ctFootnotes.addNewFootnote().set(footnote.getCTFtnEdn());
    }
    
    public XWPFFootnote addFootnote(final CTFtnEdn note) {
        final CTFtnEdn newNote = this.ctFootnotes.addNewFootnote();
        newNote.set(note);
        final XWPFFootnote xNote = new XWPFFootnote(newNote, this);
        this.listFootnote.add(xNote);
        return xNote;
    }
    
    public void setXWPFDocument(final XWPFDocument doc) {
        this.document = doc;
    }
    
    public XWPFDocument getXWPFDocument() {
        if (this.document != null) {
            return this.document;
        }
        return (XWPFDocument)this.getParent();
    }
}
