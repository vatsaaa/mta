// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import org.apache.poi.POIXMLDocumentPart;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTProofErr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTString;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTInd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STLineSpacingRule;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSpacing;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPBdr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTextAlignment;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTextAlignment;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import java.math.BigInteger;
import java.util.Collections;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtContentRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSmartTagRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSimpleField;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRunTrackChange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHyperlink;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import java.util.Iterator;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFtnEdnRef;
import org.apache.xmlbeans.XmlObject;
import java.util.ArrayList;
import java.util.List;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;

public class XWPFParagraph implements IBodyElement
{
    private final CTP paragraph;
    protected IBody part;
    protected XWPFDocument document;
    protected List<XWPFRun> runs;
    private StringBuffer footnoteText;
    
    public XWPFParagraph(final CTP prgrph, final IBody part) {
        this.footnoteText = new StringBuffer();
        this.paragraph = prgrph;
        this.part = part;
        this.document = part.getXWPFDocument();
        if (this.document == null) {
            throw new NullPointerException();
        }
        this.runs = new ArrayList<XWPFRun>();
        this.buildRunsInOrderFromXml(this.paragraph);
        for (final XWPFRun run : this.runs) {
            final CTR r = run.getCTR();
            final XmlCursor c = r.newCursor();
            c.selectPath("child::*");
            while (c.toNextSelection()) {
                final XmlObject o = c.getObject();
                if (o instanceof CTFtnEdnRef) {
                    final CTFtnEdnRef ftn = (CTFtnEdnRef)o;
                    this.footnoteText.append("[").append(ftn.getId()).append(": ");
                    final XWPFFootnote footnote = ftn.getDomNode().getLocalName().equals("footnoteReference") ? this.document.getFootnoteByID(ftn.getId().intValue()) : this.document.getEndnoteByID(ftn.getId().intValue());
                    boolean first = true;
                    for (final XWPFParagraph p : footnote.getParagraphs()) {
                        if (!first) {
                            this.footnoteText.append("\n");
                            first = false;
                        }
                        this.footnoteText.append(p.getText());
                    }
                    this.footnoteText.append("]");
                }
            }
            c.dispose();
        }
    }
    
    private void buildRunsInOrderFromXml(final XmlObject object) {
        final XmlCursor c = object.newCursor();
        c.selectPath("child::*");
        while (c.toNextSelection()) {
            final XmlObject o = c.getObject();
            if (o instanceof CTR) {
                this.runs.add(new XWPFRun((CTR)o, this));
            }
            if (o instanceof CTHyperlink) {
                final CTHyperlink link = (CTHyperlink)o;
                for (final CTR r : link.getRList()) {
                    this.runs.add(new XWPFHyperlinkRun(link, r, this));
                }
            }
            if (o instanceof CTSdtRun) {
                final CTSdtContentRun run = ((CTSdtRun)o).getSdtContent();
                for (final CTR r : run.getRList()) {
                    this.runs.add(new XWPFRun(r, this));
                }
            }
            if (o instanceof CTRunTrackChange) {
                for (final CTR r2 : ((CTRunTrackChange)o).getRList()) {
                    this.runs.add(new XWPFRun(r2, this));
                }
            }
            if (o instanceof CTSimpleField) {
                for (final CTR r2 : ((CTSimpleField)o).getRList()) {
                    this.runs.add(new XWPFRun(r2, this));
                }
            }
            if (o instanceof CTSmartTagRun) {
                this.buildRunsInOrderFromXml(o);
            }
        }
        c.dispose();
    }
    
    @Internal
    public CTP getCTP() {
        return this.paragraph;
    }
    
    public List<XWPFRun> getRuns() {
        return Collections.unmodifiableList((List<? extends XWPFRun>)this.runs);
    }
    
    public boolean isEmpty() {
        return !this.paragraph.getDomNode().hasChildNodes();
    }
    
    public XWPFDocument getDocument() {
        return this.document;
    }
    
    public String getText() {
        final StringBuffer out = new StringBuffer();
        for (final XWPFRun run : this.runs) {
            out.append(run.toString());
        }
        out.append(this.footnoteText);
        return out.toString();
    }
    
    public String getStyleID() {
        if (this.paragraph.getPPr() != null && this.paragraph.getPPr().getPStyle() != null && this.paragraph.getPPr().getPStyle().getVal() != null) {
            return this.paragraph.getPPr().getPStyle().getVal();
        }
        return null;
    }
    
    public BigInteger getNumID() {
        if (this.paragraph.getPPr() != null && this.paragraph.getPPr().getNumPr() != null && this.paragraph.getPPr().getNumPr().getNumId() != null) {
            return this.paragraph.getPPr().getNumPr().getNumId().getVal();
        }
        return null;
    }
    
    public void setNumID(final BigInteger numPos) {
        if (this.paragraph.getPPr() == null) {
            this.paragraph.addNewPPr();
        }
        if (this.paragraph.getPPr().getNumPr() == null) {
            this.paragraph.getPPr().addNewNumPr();
        }
        if (this.paragraph.getPPr().getNumPr().getNumId() == null) {
            this.paragraph.getPPr().getNumPr().addNewNumId();
        }
        this.paragraph.getPPr().getNumPr().getNumId().setVal(numPos);
    }
    
    public String getParagraphText() {
        final StringBuffer out = new StringBuffer();
        for (final XWPFRun run : this.runs) {
            out.append(run.toString());
        }
        return out.toString();
    }
    
    public String getPictureText() {
        final StringBuffer out = new StringBuffer();
        for (final XWPFRun run : this.runs) {
            out.append(run.getPictureText());
        }
        return out.toString();
    }
    
    public String getFootnoteText() {
        return this.footnoteText.toString();
    }
    
    public XWPFRun createRun() {
        final XWPFRun xwpfRun = new XWPFRun(this.paragraph.addNewR(), this);
        this.runs.add(xwpfRun);
        return xwpfRun;
    }
    
    public ParagraphAlignment getAlignment() {
        final CTPPr pr = this.getCTPPr();
        return (pr == null || !pr.isSetJc()) ? ParagraphAlignment.LEFT : ParagraphAlignment.valueOf(pr.getJc().getVal().intValue());
    }
    
    public void setAlignment(final ParagraphAlignment align) {
        final CTPPr pr = this.getCTPPr();
        final CTJc jc = pr.isSetJc() ? pr.getJc() : pr.addNewJc();
        final STJc.Enum en = STJc.Enum.forInt(align.getValue());
        jc.setVal(en);
    }
    
    public TextAlignment getVerticalAlignment() {
        final CTPPr pr = this.getCTPPr();
        return (pr == null || !pr.isSetTextAlignment()) ? TextAlignment.AUTO : TextAlignment.valueOf(pr.getTextAlignment().getVal().intValue());
    }
    
    public void setVerticalAlignment(final TextAlignment valign) {
        final CTPPr pr = this.getCTPPr();
        final CTTextAlignment textAlignment = pr.isSetTextAlignment() ? pr.getTextAlignment() : pr.addNewTextAlignment();
        final STTextAlignment.Enum en = STTextAlignment.Enum.forInt(valign.getValue());
        textAlignment.setVal(en);
    }
    
    public void setBorderTop(final Borders border) {
        final CTPBdr ct = this.getCTPBrd(true);
        final CTBorder pr = (ct != null && ct.isSetTop()) ? ct.getTop() : ct.addNewTop();
        if (border.getValue() == Borders.NONE.getValue()) {
            ct.unsetTop();
        }
        else {
            pr.setVal(STBorder.Enum.forInt(border.getValue()));
        }
    }
    
    public Borders getBorderTop() {
        final CTPBdr border = this.getCTPBrd(false);
        CTBorder ct = null;
        if (border != null) {
            ct = border.getTop();
        }
        final STBorder.Enum ptrn = (ct != null) ? ct.getVal() : STBorder.NONE;
        return Borders.valueOf(ptrn.intValue());
    }
    
    public void setBorderBottom(final Borders border) {
        final CTPBdr ct = this.getCTPBrd(true);
        final CTBorder pr = ct.isSetBottom() ? ct.getBottom() : ct.addNewBottom();
        if (border.getValue() == Borders.NONE.getValue()) {
            ct.unsetBottom();
        }
        else {
            pr.setVal(STBorder.Enum.forInt(border.getValue()));
        }
    }
    
    public Borders getBorderBottom() {
        final CTPBdr border = this.getCTPBrd(false);
        CTBorder ct = null;
        if (border != null) {
            ct = border.getBottom();
        }
        final STBorder.Enum ptrn = (ct != null) ? ct.getVal() : STBorder.NONE;
        return Borders.valueOf(ptrn.intValue());
    }
    
    public void setBorderLeft(final Borders border) {
        final CTPBdr ct = this.getCTPBrd(true);
        final CTBorder pr = ct.isSetLeft() ? ct.getLeft() : ct.addNewLeft();
        if (border.getValue() == Borders.NONE.getValue()) {
            ct.unsetLeft();
        }
        else {
            pr.setVal(STBorder.Enum.forInt(border.getValue()));
        }
    }
    
    public Borders getBorderLeft() {
        final CTPBdr border = this.getCTPBrd(false);
        CTBorder ct = null;
        if (border != null) {
            ct = border.getLeft();
        }
        final STBorder.Enum ptrn = (ct != null) ? ct.getVal() : STBorder.NONE;
        return Borders.valueOf(ptrn.intValue());
    }
    
    public void setBorderRight(final Borders border) {
        final CTPBdr ct = this.getCTPBrd(true);
        final CTBorder pr = ct.isSetRight() ? ct.getRight() : ct.addNewRight();
        if (border.getValue() == Borders.NONE.getValue()) {
            ct.unsetRight();
        }
        else {
            pr.setVal(STBorder.Enum.forInt(border.getValue()));
        }
    }
    
    public Borders getBorderRight() {
        final CTPBdr border = this.getCTPBrd(false);
        CTBorder ct = null;
        if (border != null) {
            ct = border.getRight();
        }
        final STBorder.Enum ptrn = (ct != null) ? ct.getVal() : STBorder.NONE;
        return Borders.valueOf(ptrn.intValue());
    }
    
    public void setBorderBetween(final Borders border) {
        final CTPBdr ct = this.getCTPBrd(true);
        final CTBorder pr = ct.isSetBetween() ? ct.getBetween() : ct.addNewBetween();
        if (border.getValue() == Borders.NONE.getValue()) {
            ct.unsetBetween();
        }
        else {
            pr.setVal(STBorder.Enum.forInt(border.getValue()));
        }
    }
    
    public Borders getBorderBetween() {
        final CTPBdr border = this.getCTPBrd(false);
        CTBorder ct = null;
        if (border != null) {
            ct = border.getBetween();
        }
        final STBorder.Enum ptrn = (ct != null) ? ct.getVal() : STBorder.NONE;
        return Borders.valueOf(ptrn.intValue());
    }
    
    public void setPageBreak(final boolean pageBreak) {
        final CTPPr ppr = this.getCTPPr();
        final CTOnOff ct_pageBreak = ppr.isSetPageBreakBefore() ? ppr.getPageBreakBefore() : ppr.addNewPageBreakBefore();
        if (pageBreak) {
            ct_pageBreak.setVal(STOnOff.TRUE);
        }
        else {
            ct_pageBreak.setVal(STOnOff.FALSE);
        }
    }
    
    public boolean isPageBreak() {
        final CTPPr ppr = this.getCTPPr();
        final CTOnOff ct_pageBreak = ppr.isSetPageBreakBefore() ? ppr.getPageBreakBefore() : null;
        return ct_pageBreak != null && ct_pageBreak.getVal().intValue() == 1;
    }
    
    public void setSpacingAfter(final int spaces) {
        final CTSpacing spacing = this.getCTSpacing(true);
        if (spacing != null) {
            final BigInteger bi = new BigInteger("" + spaces);
            spacing.setAfter(bi);
        }
    }
    
    public int getSpacingAfter() {
        final CTSpacing spacing = this.getCTSpacing(false);
        return (spacing != null && spacing.isSetAfter()) ? spacing.getAfter().intValue() : -1;
    }
    
    public void setSpacingAfterLines(final int spaces) {
        final CTSpacing spacing = this.getCTSpacing(true);
        final BigInteger bi = new BigInteger("" + spaces);
        spacing.setAfterLines(bi);
    }
    
    public int getSpacingAfterLines() {
        final CTSpacing spacing = this.getCTSpacing(false);
        return (spacing != null && spacing.isSetAfterLines()) ? spacing.getAfterLines().intValue() : -1;
    }
    
    public void setSpacingBefore(final int spaces) {
        final CTSpacing spacing = this.getCTSpacing(true);
        final BigInteger bi = new BigInteger("" + spaces);
        spacing.setBefore(bi);
    }
    
    public int getSpacingBefore() {
        final CTSpacing spacing = this.getCTSpacing(false);
        return (spacing != null && spacing.isSetBefore()) ? spacing.getBefore().intValue() : -1;
    }
    
    public void setSpacingBeforeLines(final int spaces) {
        final CTSpacing spacing = this.getCTSpacing(true);
        final BigInteger bi = new BigInteger("" + spaces);
        spacing.setBeforeLines(bi);
    }
    
    public int getSpacingBeforeLines() {
        final CTSpacing spacing = this.getCTSpacing(false);
        return (spacing != null && spacing.isSetBeforeLines()) ? spacing.getBeforeLines().intValue() : -1;
    }
    
    public void setSpacingLineRule(final LineSpacingRule rule) {
        final CTSpacing spacing = this.getCTSpacing(true);
        spacing.setLineRule(STLineSpacingRule.Enum.forInt(rule.getValue()));
    }
    
    public LineSpacingRule getSpacingLineRule() {
        final CTSpacing spacing = this.getCTSpacing(false);
        return (spacing != null && spacing.isSetLineRule()) ? LineSpacingRule.valueOf(spacing.getLineRule().intValue()) : LineSpacingRule.AUTO;
    }
    
    public void setIndentationLeft(final int indentation) {
        final CTInd indent = this.getCTInd(true);
        final BigInteger bi = new BigInteger("" + indentation);
        indent.setLeft(bi);
    }
    
    public int getIndentationLeft() {
        final CTInd indentation = this.getCTInd(false);
        return (indentation != null && indentation.isSetLeft()) ? indentation.getLeft().intValue() : -1;
    }
    
    public void setIndentationRight(final int indentation) {
        final CTInd indent = this.getCTInd(true);
        final BigInteger bi = new BigInteger("" + indentation);
        indent.setRight(bi);
    }
    
    public int getIndentationRight() {
        final CTInd indentation = this.getCTInd(false);
        return (indentation != null && indentation.isSetRight()) ? indentation.getRight().intValue() : -1;
    }
    
    public void setIndentationHanging(final int indentation) {
        final CTInd indent = this.getCTInd(true);
        final BigInteger bi = new BigInteger("" + indentation);
        indent.setHanging(bi);
    }
    
    public int getIndentationHanging() {
        final CTInd indentation = this.getCTInd(false);
        return (indentation != null && indentation.isSetHanging()) ? indentation.getHanging().intValue() : -1;
    }
    
    public void setIndentationFirstLine(final int indentation) {
        final CTInd indent = this.getCTInd(true);
        final BigInteger bi = new BigInteger("" + indentation);
        indent.setFirstLine(bi);
    }
    
    public int getIndentationFirstLine() {
        final CTInd indentation = this.getCTInd(false);
        return (indentation != null && indentation.isSetFirstLine()) ? indentation.getFirstLine().intValue() : -1;
    }
    
    public void setWordWrap(final boolean wrap) {
        final CTOnOff wordWrap = this.getCTPPr().isSetWordWrap() ? this.getCTPPr().getWordWrap() : this.getCTPPr().addNewWordWrap();
        if (wrap) {
            wordWrap.setVal(STOnOff.TRUE);
        }
        else {
            wordWrap.unsetVal();
        }
    }
    
    public boolean isWordWrap() {
        final CTOnOff wordWrap = this.getCTPPr().isSetWordWrap() ? this.getCTPPr().getWordWrap() : null;
        return wordWrap != null && (wordWrap.getVal() == STOnOff.ON || wordWrap.getVal() == STOnOff.TRUE || wordWrap.getVal() == STOnOff.X_1);
    }
    
    public void setStyle(final String newStyle) {
        final CTPPr pr = this.getCTPPr();
        final CTString style = (pr.getPStyle() != null) ? pr.getPStyle() : pr.addNewPStyle();
        style.setVal(newStyle);
    }
    
    public String getStyle() {
        final CTPPr pr = this.getCTPPr();
        final CTString style = pr.isSetPStyle() ? pr.getPStyle() : null;
        return (style != null) ? style.getVal() : null;
    }
    
    private CTPBdr getCTPBrd(final boolean create) {
        final CTPPr pr = this.getCTPPr();
        CTPBdr ct = pr.isSetPBdr() ? pr.getPBdr() : null;
        if (create && ct == null) {
            ct = pr.addNewPBdr();
        }
        return ct;
    }
    
    private CTSpacing getCTSpacing(final boolean create) {
        final CTPPr pr = this.getCTPPr();
        CTSpacing ct = (pr.getSpacing() == null) ? null : pr.getSpacing();
        if (create && ct == null) {
            ct = pr.addNewSpacing();
        }
        return ct;
    }
    
    private CTInd getCTInd(final boolean create) {
        final CTPPr pr = this.getCTPPr();
        CTInd ct = (pr.getInd() == null) ? null : pr.getInd();
        if (create && ct == null) {
            ct = pr.addNewInd();
        }
        return ct;
    }
    
    private CTPPr getCTPPr() {
        final CTPPr pr = (this.paragraph.getPPr() == null) ? this.paragraph.addNewPPr() : this.paragraph.getPPr();
        return pr;
    }
    
    protected void addRun(final CTR run) {
        final int pos = this.paragraph.getRList().size();
        this.paragraph.addNewR();
        this.paragraph.setRArray(pos, run);
    }
    
    public TextSegement searchText(final String searched, final PositionInParagraph startPos) {
        final int startRun = startPos.getRun();
        final int startText = startPos.getText();
        final int startChar = startPos.getChar();
        int beginRunPos = 0;
        int candCharPos = 0;
        boolean newList = false;
        for (int runPos = startRun; runPos < this.paragraph.getRList().size(); ++runPos) {
            int beginTextPos = 0;
            int beginCharPos = 0;
            int textPos = 0;
            int charPos = 0;
            final CTR ctRun = this.paragraph.getRArray(runPos);
            final XmlCursor c = ctRun.newCursor();
            c.selectPath("./*");
            while (c.toNextSelection()) {
                final XmlObject o = c.getObject();
                if (o instanceof CTText) {
                    if (textPos >= startText) {
                        final String candidate = ((CTText)o).getStringValue();
                        if (runPos == startRun) {
                            charPos = startChar;
                        }
                        else {
                            charPos = 0;
                        }
                        while (charPos < candidate.length()) {
                            if (candidate.charAt(charPos) == searched.charAt(0) && candCharPos == 0) {
                                beginTextPos = textPos;
                                beginCharPos = charPos;
                                beginRunPos = runPos;
                                newList = true;
                            }
                            if (candidate.charAt(charPos) == searched.charAt(candCharPos)) {
                                if (candCharPos + 1 < searched.length()) {
                                    ++candCharPos;
                                }
                                else if (newList) {
                                    final TextSegement segement = new TextSegement();
                                    segement.setBeginRun(beginRunPos);
                                    segement.setBeginText(beginTextPos);
                                    segement.setBeginChar(beginCharPos);
                                    segement.setEndRun(runPos);
                                    segement.setEndText(textPos);
                                    segement.setEndChar(charPos);
                                    return segement;
                                }
                            }
                            else {
                                candCharPos = 0;
                            }
                            ++charPos;
                        }
                    }
                    ++textPos;
                }
                else if (o instanceof CTProofErr) {
                    c.removeXml();
                }
                else {
                    if (o instanceof CTRPr) {
                        continue;
                    }
                    candCharPos = 0;
                }
            }
            c.dispose();
        }
        return null;
    }
    
    public XWPFRun insertNewRun(final int pos) {
        if (pos >= 0 && pos <= this.paragraph.sizeOfRArray()) {
            final CTR ctRun = this.paragraph.insertNewR(pos);
            final XWPFRun newRun = new XWPFRun(ctRun, this);
            this.runs.add(pos, newRun);
            return newRun;
        }
        return null;
    }
    
    public String getText(final TextSegement segment) {
        final int runBegin = segment.getBeginRun();
        final int textBegin = segment.getBeginText();
        final int charBegin = segment.getBeginChar();
        final int runEnd = segment.getEndRun();
        final int textEnd = segment.getEndText();
        final int charEnd = segment.getEndChar();
        final StringBuffer out = new StringBuffer();
        for (int i = runBegin; i <= runEnd; ++i) {
            int startText = 0;
            int endText = this.paragraph.getRArray(i).getTList().size() - 1;
            if (i == runBegin) {
                startText = textBegin;
            }
            if (i == runEnd) {
                endText = textEnd;
            }
            for (int j = startText; j <= endText; ++j) {
                final String tmpText = this.paragraph.getRArray(i).getTArray(j).getStringValue();
                int startChar = 0;
                int endChar = tmpText.length() - 1;
                if (j == textBegin && i == runBegin) {
                    startChar = charBegin;
                }
                if (j == textEnd && i == runEnd) {
                    endChar = charEnd;
                }
                out.append(tmpText.substring(startChar, endChar + 1));
            }
        }
        return out.toString();
    }
    
    public boolean removeRun(final int pos) {
        if (pos >= 0 && pos < this.paragraph.sizeOfRArray()) {
            this.getCTP().removeR(pos);
            this.runs.remove(pos);
            return true;
        }
        return false;
    }
    
    public BodyElementType getElementType() {
        return BodyElementType.PARAGRAPH;
    }
    
    public IBody getBody() {
        return this.part;
    }
    
    public POIXMLDocumentPart getPart() {
        if (this.part != null) {
            return this.part.getPart();
        }
        return null;
    }
    
    public BodyType getPartType() {
        return this.part.getPartType();
    }
    
    public void addRun(final XWPFRun r) {
        if (!this.runs.contains(r)) {
            this.runs.add(r);
        }
    }
    
    public XWPFRun getRun(final CTR r) {
        for (int i = 0; i < this.getRuns().size(); ++i) {
            if (this.getRuns().get(i).getCTR() == r) {
                return this.getRuns().get(i);
            }
        }
        return null;
    }
}
