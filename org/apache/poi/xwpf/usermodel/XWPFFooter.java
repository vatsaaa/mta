// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.io.InputStream;
import org.apache.poi.POIXMLException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.FtrDocument;
import java.io.OutputStream;
import java.util.Map;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTNumbering;
import org.apache.xmlbeans.XmlOptions;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.POIXMLDocumentPart;
import java.io.IOException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHdrFtr;

public class XWPFFooter extends XWPFHeaderFooter
{
    public XWPFFooter() {
    }
    
    public XWPFFooter(final XWPFDocument doc, final CTHdrFtr hdrFtr) throws IOException {
        super(doc, hdrFtr);
        final XmlCursor cursor = this.headerFooter.newCursor();
        cursor.selectPath("./*");
        while (cursor.toNextSelection()) {
            final XmlObject o = cursor.getObject();
            if (o instanceof CTP) {
                final XWPFParagraph p = new XWPFParagraph((CTP)o, this);
                this.paragraphs.add(p);
                this.bodyElements.add(p);
            }
            if (o instanceof CTTbl) {
                final XWPFTable t = new XWPFTable((CTTbl)o, this);
                this.tables.add(t);
                this.bodyElements.add(t);
            }
        }
        cursor.dispose();
    }
    
    public XWPFFooter(final POIXMLDocumentPart parent, final PackagePart part, final PackageRelationship rel) throws IOException {
        super(parent, part, rel);
    }
    
    @Override
    protected void commit() throws IOException {
        final XmlOptions xmlOptions = new XmlOptions(XWPFFooter.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTNumbering.type.getName().getNamespaceURI(), "ftr"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put("http://schemas.openxmlformats.org/markup-compatibility/2006", "ve");
        map.put("urn:schemas-microsoft-com:office:office", "o");
        map.put("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "r");
        map.put("http://schemas.openxmlformats.org/officeDocument/2006/math", "m");
        map.put("urn:schemas-microsoft-com:vml", "v");
        map.put("http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing", "wp");
        map.put("urn:schemas-microsoft-com:office:word", "w10");
        map.put("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "w");
        map.put("http://schemas.microsoft.com/office/word/2006/wordml", "wne");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        super._getHdrFtr().save(out, xmlOptions);
        out.close();
    }
    
    @Override
    protected void onDocumentRead() throws IOException {
        super.onDocumentRead();
        FtrDocument ftrDocument = null;
        try {
            final InputStream is = this.getPackagePart().getInputStream();
            ftrDocument = FtrDocument.Factory.parse(is);
            this.headerFooter = ftrDocument.getFtr();
            final XmlCursor cursor = this.headerFooter.newCursor();
            cursor.selectPath("./*");
            while (cursor.toNextSelection()) {
                final XmlObject o = cursor.getObject();
                if (o instanceof CTP) {
                    final XWPFParagraph p = new XWPFParagraph((CTP)o, this);
                    this.paragraphs.add(p);
                    this.bodyElements.add(p);
                }
                if (o instanceof CTTbl) {
                    final XWPFTable t = new XWPFTable((CTTbl)o, this);
                    this.tables.add(t);
                    this.bodyElements.add(t);
                }
            }
            cursor.dispose();
        }
        catch (Exception e) {
            throw new POIXMLException(e);
        }
    }
    
    public BodyType getPartType() {
        return BodyType.FOOTER;
    }
}
