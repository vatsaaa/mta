// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.util.HashMap;
import java.util.Map;

public enum BreakClear
{
    NONE(1), 
    LEFT(2), 
    RIGHT(3), 
    ALL(4);
    
    private final int value;
    private static Map<Integer, BreakClear> imap;
    
    private BreakClear(final int val) {
        this.value = val;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public static BreakClear valueOf(final int type) {
        final BreakClear bType = BreakClear.imap.get(new Integer(type));
        if (bType == null) {
            throw new IllegalArgumentException("Unknown break clear type: " + type);
        }
        return bType;
    }
    
    static {
        BreakClear.imap = new HashMap<Integer, BreakClear>();
        for (final BreakClear p : values()) {
            BreakClear.imap.put(new Integer(p.getValue()), p);
        }
    }
}
