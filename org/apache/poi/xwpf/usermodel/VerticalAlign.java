// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xwpf.usermodel;

import java.util.HashMap;
import java.util.Map;

public enum VerticalAlign
{
    BASELINE(1), 
    SUPERSCRIPT(2), 
    SUBSCRIPT(3);
    
    private final int value;
    private static Map<Integer, VerticalAlign> imap;
    
    private VerticalAlign(final int val) {
        this.value = val;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public static VerticalAlign valueOf(final int type) {
        final VerticalAlign align = VerticalAlign.imap.get(new Integer(type));
        if (align == null) {
            throw new IllegalArgumentException("Unknown vertical alignment: " + type);
        }
        return align;
    }
    
    static {
        VerticalAlign.imap = new HashMap<Integer, VerticalAlign>();
        for (final VerticalAlign p : values()) {
            VerticalAlign.imap.put(new Integer(p.getValue()), p);
        }
    }
}
