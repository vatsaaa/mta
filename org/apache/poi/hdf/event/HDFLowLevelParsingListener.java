// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.event;

import org.apache.poi.hdf.model.hdftypes.StyleSheet;
import org.apache.poi.hdf.model.hdftypes.ListTables;
import org.apache.poi.hdf.model.hdftypes.FontTable;
import org.apache.poi.hdf.model.hdftypes.TextPiece;
import org.apache.poi.hdf.model.hdftypes.ChpxNode;
import org.apache.poi.hdf.model.hdftypes.PapxNode;
import org.apache.poi.hdf.model.hdftypes.SepxNode;
import org.apache.poi.hdf.model.hdftypes.DocumentProperties;

@Deprecated
public interface HDFLowLevelParsingListener
{
    void mainDocument(final byte[] p0);
    
    void tableStream(final byte[] p0);
    
    void document(final DocumentProperties p0);
    
    void bodySection(final SepxNode p0);
    
    void paragraph(final PapxNode p0);
    
    void characterRun(final ChpxNode p0);
    
    void hdrSection(final SepxNode p0);
    
    void endSections();
    
    void text(final TextPiece p0);
    
    void fonts(final FontTable p0);
    
    void lists(final ListTables p0);
    
    void styleSheet(final StyleSheet p0);
    
    void miscellaneous(final int p0, final int p1, final int p2, final int p3, final int p4);
}
