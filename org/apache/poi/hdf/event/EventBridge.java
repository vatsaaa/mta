// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.event;

import org.apache.poi.hdf.model.util.NumberFormatter;
import org.apache.poi.hdf.model.hdftypes.LVL;
import org.apache.poi.hdf.model.hdftypes.FontTable;
import org.apache.poi.hdf.model.hdftypes.TextPiece;
import org.apache.poi.hdf.model.hdftypes.CharacterProperties;
import org.apache.poi.hdf.model.hdftypes.ChpxNode;
import org.apache.poi.hdf.model.hdftypes.TableProperties;
import org.apache.poi.hdf.model.hdftypes.ParagraphProperties;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hdf.model.hdftypes.PapxNode;
import org.apache.poi.hdf.model.hdftypes.HeaderFooter;
import org.apache.poi.hdf.model.hdftypes.SectionProperties;
import org.apache.poi.hdf.model.hdftypes.SepxNode;
import java.util.ArrayList;
import org.apache.poi.hdf.model.util.BTreeSet;
import org.apache.poi.hdf.model.hdftypes.ListTables;
import org.apache.poi.hdf.model.hdftypes.StyleDescription;
import org.apache.poi.hdf.model.hdftypes.DocumentProperties;
import org.apache.poi.hdf.model.hdftypes.StyleSheet;

@Deprecated
public final class EventBridge implements HDFLowLevelParsingListener
{
    private static int HEADER_EVEN_INDEX;
    private static int HEADER_ODD_INDEX;
    private static int FOOTER_EVEN_INDEX;
    private static int FOOTER_ODD_INDEX;
    private static int HEADER_FIRST_INDEX;
    private static int FOOTER_FIRST_INDEX;
    HDFParsingListener _listener;
    StyleSheet _stsh;
    DocumentProperties _dop;
    StyleDescription _currentStd;
    ListTables _listTables;
    byte[] _mainDocument;
    byte[] _tableStream;
    int _fcMin;
    int _ccpText;
    int _ccpFtn;
    int _hdrSize;
    int _hdrOffset;
    BTreeSet _text;
    private boolean _beginHeaders;
    BTreeSet _hdrSections;
    BTreeSet _hdrParagraphs;
    BTreeSet _hdrCharacterRuns;
    int _sectionCounter;
    ArrayList _hdrs;
    private boolean _holdParagraph;
    private int _endHoldIndex;
    private ArrayList _onHold;
    
    public EventBridge(final HDFParsingListener listener) {
        this._text = new BTreeSet();
        this._hdrSections = new BTreeSet();
        this._hdrParagraphs = new BTreeSet();
        this._hdrCharacterRuns = new BTreeSet();
        this._sectionCounter = 1;
        this._hdrs = new ArrayList();
        this._holdParagraph = false;
        this._endHoldIndex = -1;
        this._listener = listener;
    }
    
    public void mainDocument(final byte[] mainDocument) {
        this._mainDocument = mainDocument;
    }
    
    public void tableStream(final byte[] tableStream) {
        this._tableStream = tableStream;
    }
    
    public void miscellaneous(final int fcMin, final int ccpText, final int ccpFtn, final int fcPlcfhdd, final int lcbPlcfhdd) {
        this._fcMin = fcMin;
        this._ccpText = ccpText;
        this._ccpFtn = ccpFtn;
        this._hdrOffset = fcPlcfhdd;
        this._hdrSize = lcbPlcfhdd;
    }
    
    public void document(final DocumentProperties dop) {
        this._dop = dop;
    }
    
    public void bodySection(final SepxNode sepx) {
        final SectionProperties sep = (SectionProperties)StyleSheet.uncompressProperty(sepx.getSepx(), new SectionProperties(), this._stsh);
        final HeaderFooter[] hdrArray = this.findSectionHdrFtrs(this._sectionCounter);
        this._hdrs.add(hdrArray);
        this._listener.section(sep, sepx.getStart() - this._fcMin, sepx.getEnd() - this._fcMin);
        ++this._sectionCounter;
    }
    
    public void hdrSection(final SepxNode sepx) {
        this._beginHeaders = true;
        this._hdrSections.add(sepx);
    }
    
    public void endSections() {
        for (int x = 1; x < this._sectionCounter; ++x) {
            final HeaderFooter[] hdrArray = this._hdrs.get(x - 1);
            HeaderFooter hf = null;
            if (!hdrArray[0].isEmpty()) {
                hf = hdrArray[0];
                this._listener.header(x - 1, 1);
                this.flushHeaderProps(hf.getStart(), hf.getEnd());
            }
            if (!hdrArray[1].isEmpty()) {
                hf = hdrArray[1];
                this._listener.header(x - 1, 2);
                this.flushHeaderProps(hf.getStart(), hf.getEnd());
            }
            if (!hdrArray[2].isEmpty()) {
                hf = hdrArray[2];
                this._listener.footer(x - 1, 3);
                this.flushHeaderProps(hf.getStart(), hf.getEnd());
            }
            if (!hdrArray[3].isEmpty()) {
                hf = hdrArray[2];
                this._listener.footer(x - 1, 3);
                this.flushHeaderProps(hf.getStart(), hf.getEnd());
            }
            if (!hdrArray[4].isEmpty()) {
                hf = hdrArray[4];
                this._listener.header(x - 1, 5);
                this.flushHeaderProps(hf.getStart(), hf.getEnd());
            }
            if (!hdrArray[5].isEmpty()) {
                hf = hdrArray[5];
                this._listener.footer(x - 1, 6);
                this.flushHeaderProps(hf.getStart(), hf.getEnd());
            }
        }
    }
    
    public void paragraph(final PapxNode papx) {
        if (this._beginHeaders) {
            this._hdrParagraphs.add(papx);
        }
        final byte[] bytePapx = papx.getPapx();
        final int istd = LittleEndian.getShort(bytePapx, 0);
        this._currentStd = this._stsh.getStyleDescription(istd);
        final ParagraphProperties pap = (ParagraphProperties)StyleSheet.uncompressProperty(bytePapx, this._currentStd.getPAP(), this._stsh);
        if (pap.getFTtp() > 0) {
            final TableProperties tap = (TableProperties)StyleSheet.uncompressProperty(bytePapx, new TableProperties(), this._stsh);
            this._listener.tableRowEnd(tap, papx.getStart() - this._fcMin, papx.getEnd() - this._fcMin);
        }
        else if (pap.getIlfo() > 0) {
            this._holdParagraph = true;
            this._endHoldIndex = papx.getEnd();
            this._onHold.add(papx);
        }
        else {
            this._listener.paragraph(pap, papx.getStart() - this._fcMin, papx.getEnd() - this._fcMin);
        }
    }
    
    public void characterRun(final ChpxNode chpx) {
        if (this._beginHeaders) {
            this._hdrCharacterRuns.add(chpx);
        }
        final int start = chpx.getStart();
        final int end = chpx.getEnd();
        if (this._holdParagraph) {
            this._onHold.add(chpx);
            if (end >= this._endHoldIndex) {
                this._holdParagraph = false;
                this._endHoldIndex = -1;
                this.flushHeldParagraph();
                this._onHold = new ArrayList();
            }
        }
        final byte[] byteChpx = chpx.getChpx();
        final CharacterProperties chp = (CharacterProperties)StyleSheet.uncompressProperty(byteChpx, this._currentStd.getCHP(), this._stsh);
        final ArrayList textList = BTreeSet.findProperties(start, end, this._text.root);
        final String text = this.getTextFromNodes(textList, start, end);
        this._listener.characterRun(chp, text, start - this._fcMin, end - this._fcMin);
    }
    
    public void text(final TextPiece t) {
        this._text.add(t);
    }
    
    public void fonts(final FontTable fontTbl) {
    }
    
    public void lists(final ListTables listTbl) {
        this._listTables = listTbl;
    }
    
    public void styleSheet(final StyleSheet stsh) {
        this._stsh = stsh;
    }
    
    private void flushHeaderProps(final int start, final int end) {
        final ArrayList list = BTreeSet.findProperties(start, end, this._hdrSections.root);
        for (int size = list.size(), x = 0; x < size; ++x) {
            final SepxNode oldNode = list.get(x);
            final int secStart = Math.max(oldNode.getStart(), start);
            final int secEnd = Math.min(oldNode.getEnd(), end);
            final ArrayList parList = BTreeSet.findProperties(secStart, secEnd, this._hdrParagraphs.root);
            for (int parSize = parList.size(), y = 0; y < parSize; ++y) {
                final PapxNode oldParNode = parList.get(y);
                final int parStart = Math.max(oldParNode.getStart(), secStart);
                final int parEnd = Math.min(oldParNode.getEnd(), secEnd);
                final PapxNode parNode = new PapxNode(parStart, parEnd, oldParNode.getPapx());
                this.paragraph(parNode);
                final ArrayList charList = BTreeSet.findProperties(parStart, parEnd, this._hdrCharacterRuns.root);
                for (int charSize = charList.size(), z = 0; z < charSize; ++z) {
                    final ChpxNode oldCharNode = charList.get(z);
                    final int charStart = Math.max(oldCharNode.getStart(), parStart);
                    final int charEnd = Math.min(oldCharNode.getEnd(), parEnd);
                    final ChpxNode charNode = new ChpxNode(charStart, charEnd, oldCharNode.getChpx());
                    this.characterRun(charNode);
                }
            }
        }
    }
    
    private String getTextFromNodes(final ArrayList list, final int start, final int end) {
        final int size = list.size();
        final StringBuffer sb = new StringBuffer();
        for (int x = 0; x < size; ++x) {
            final TextPiece piece = list.get(x);
            final int charStart = Math.max(start, piece.getStart());
            final int charEnd = Math.min(end, piece.getEnd());
            if (piece.usesUnicode()) {
                for (int y = charStart; y < charEnd; y += 2) {
                    sb.append((char)LittleEndian.getShort(this._mainDocument, y));
                }
            }
            else {
                for (int y = charStart; y < charEnd; ++y) {
                    sb.append(this._mainDocument[y]);
                }
            }
        }
        return sb.toString();
    }
    
    private void flushHeldParagraph() {
        final PapxNode papx = this._onHold.get(0);
        final byte[] bytePapx = papx.getPapx();
        final int istd = LittleEndian.getShort(bytePapx, 0);
        final StyleDescription std = this._stsh.getStyleDescription(istd);
        ParagraphProperties pap = (ParagraphProperties)StyleSheet.uncompressProperty(bytePapx, this._currentStd.getPAP(), this._stsh);
        final LVL lvl = this._listTables.getLevel(pap.getIlfo(), pap.getIlvl());
        pap = (ParagraphProperties)StyleSheet.uncompressProperty(lvl._papx, pap, this._stsh, false);
        final int size = this._onHold.size() - 1;
        CharacterProperties numChp = (CharacterProperties)StyleSheet.uncompressProperty(this._onHold.get(size).getChpx(), std.getCHP(), this._stsh);
        numChp = (CharacterProperties)StyleSheet.uncompressProperty(lvl._chpx, numChp, this._stsh);
        final String bulletText = this.getBulletText(lvl, pap);
        this._listener.listEntry(bulletText, numChp, pap, papx.getStart() - this._fcMin, papx.getEnd() - this._fcMin);
        for (int x = 1; x <= size; ++x) {
            this.characterRun(this._onHold.get(x));
        }
    }
    
    private String getBulletText(final LVL lvl, final ParagraphProperties pap) {
        final StringBuffer bulletBuffer = new StringBuffer();
        for (int x = 0; x < lvl._xst.length; ++x) {
            if (lvl._xst[x] < '\t') {
                final LVL numLevel = this._listTables.getLevel(pap.getIlfo(), lvl._xst[x]);
                int num = numLevel._iStartAt;
                if (lvl == numLevel) {
                    final LVL lvl2 = numLevel;
                    ++lvl2._iStartAt;
                }
                else if (num > 1) {
                    --num;
                }
                bulletBuffer.append(NumberFormatter.getNumber(num, lvl._nfc));
            }
            else {
                bulletBuffer.append(lvl._xst[x]);
            }
        }
        switch (lvl._ixchFollow) {
            case 0: {
                bulletBuffer.append('\t');
                break;
            }
            case 1: {
                bulletBuffer.append(' ');
                break;
            }
        }
        return bulletBuffer.toString();
    }
    
    private HeaderFooter[] findSectionHdrFtrs(final int index) {
        final HeaderFooter[] hdrArray = new HeaderFooter[6];
        for (int x = 1; x < 7; ++x) {
            hdrArray[x - 1] = this.createSectionHdrFtr(index, x);
        }
        return hdrArray;
    }
    
    private HeaderFooter createSectionHdrFtr(final int index, final int type) {
        if (this._hdrSize < 50) {
            return new HeaderFooter(0, 0, 0);
        }
        int end;
        int start = end = this._fcMin + this._ccpText + this._ccpFtn;
        int arrayIndex = 0;
        switch (type) {
            case 1: {
                arrayIndex = EventBridge.HEADER_EVEN_INDEX + index * 6;
                break;
            }
            case 3: {
                arrayIndex = EventBridge.FOOTER_EVEN_INDEX + index * 6;
                break;
            }
            case 2: {
                arrayIndex = EventBridge.HEADER_ODD_INDEX + index * 6;
                break;
            }
            case 4: {
                arrayIndex = EventBridge.FOOTER_ODD_INDEX + index * 6;
                break;
            }
            case 5: {
                arrayIndex = EventBridge.HEADER_FIRST_INDEX + index * 6;
                break;
            }
            case 6: {
                arrayIndex = EventBridge.FOOTER_FIRST_INDEX + index * 6;
                break;
            }
        }
        start += LittleEndian.getInt(this._tableStream, this._hdrOffset + arrayIndex * 4);
        end += LittleEndian.getInt(this._tableStream, this._hdrOffset + (arrayIndex + 1) * 4);
        HeaderFooter retValue = new HeaderFooter(type, start, end);
        if (end - start == 0 && index > 1) {
            retValue = this.createSectionHdrFtr(type, index - 1);
        }
        return retValue;
    }
    
    static {
        EventBridge.HEADER_EVEN_INDEX = 0;
        EventBridge.HEADER_ODD_INDEX = 1;
        EventBridge.FOOTER_EVEN_INDEX = 2;
        EventBridge.FOOTER_ODD_INDEX = 3;
        EventBridge.HEADER_FIRST_INDEX = 4;
        EventBridge.FOOTER_FIRST_INDEX = 5;
    }
}
