// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.event;

import org.apache.poi.hdf.model.hdftypes.TableProperties;
import org.apache.poi.hdf.model.hdftypes.CharacterProperties;
import org.apache.poi.hdf.model.hdftypes.ParagraphProperties;
import org.apache.poi.hdf.model.hdftypes.SectionProperties;
import org.apache.poi.hdf.model.hdftypes.DocumentProperties;

@Deprecated
public interface HDFParsingListener
{
    void document(final DocumentProperties p0);
    
    void section(final SectionProperties p0, final int p1, final int p2);
    
    void paragraph(final ParagraphProperties p0, final int p1, final int p2);
    
    void listEntry(final String p0, final CharacterProperties p1, final ParagraphProperties p2, final int p3, final int p4);
    
    void paragraphInTableRow(final ParagraphProperties p0, final int p1, final int p2);
    
    void characterRun(final CharacterProperties p0, final String p1, final int p2, final int p3);
    
    void tableRowEnd(final TableProperties p0, final int p1, final int p2);
    
    void header(final int p0, final int p1);
    
    void footer(final int p0, final int p1);
}
