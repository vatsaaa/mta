// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class TAP
{
    short _jc;
    int _dxaGapHalf;
    int _dyaRowHeight;
    boolean _fCantSplit;
    boolean _fTableHeader;
    boolean _fLastRow;
    short _itcMac;
    short[] _rgdxaCenter;
    short[] _brcLeft;
    short[] _brcRight;
    short[] _brcTop;
    short[] _brcBottom;
    short[] _brcHorizontal;
    short[] _brcVertical;
    TC[] _rgtc;
    
    public TAP() {
        this._brcLeft = new short[2];
        this._brcRight = new short[2];
        this._brcTop = new short[2];
        this._brcBottom = new short[2];
        this._brcHorizontal = new short[2];
        this._brcVertical = new short[2];
    }
}
