// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class StyleSheet
{
    private static final int NIL_STYLE = 4095;
    private static final int PAP_TYPE = 1;
    private static final int CHP_TYPE = 2;
    private static final int SEP_TYPE = 4;
    private static final int TAP_TYPE = 5;
    StyleDescription _nilStyle;
    StyleDescription[] _styleDescriptions;
    
    public StyleSheet(final byte[] styleSheet) {
        this._nilStyle = new StyleDescription();
        final int stshiLength = Utils.convertBytesToShort(styleSheet, 0);
        final int stdCount = Utils.convertBytesToShort(styleSheet, 2);
        final int baseLength = Utils.convertBytesToShort(styleSheet, 4);
        final int[] rgftc = { Utils.convertBytesToInt(styleSheet, 14), Utils.convertBytesToInt(styleSheet, 18), Utils.convertBytesToInt(styleSheet, 22) };
        int offset = 0;
        this._styleDescriptions = new StyleDescription[stdCount];
        for (int x = 0; x < stdCount; ++x) {
            int stdOffset = 2 + stshiLength + offset;
            final int stdSize = Utils.convertBytesToShort(styleSheet, stdOffset);
            if (stdSize > 0) {
                final byte[] std = new byte[stdSize];
                stdOffset += 2;
                System.arraycopy(styleSheet, stdOffset, std, 0, stdSize);
                final StyleDescription aStyle = new StyleDescription(std, baseLength, true);
                this._styleDescriptions[x] = aStyle;
            }
            offset += stdSize + 2;
        }
        for (int x = 0; x < this._styleDescriptions.length; ++x) {
            if (this._styleDescriptions[x] != null) {
                this.createPap(x);
                this.createChp(x);
            }
        }
    }
    
    private void createPap(final int istd) {
        final StyleDescription sd = this._styleDescriptions[istd];
        PAP pap = sd.getPAP();
        final byte[] papx = sd.getPAPX();
        final int baseIndex = sd.getBaseStyle();
        if (pap == null && papx != null) {
            PAP parentPAP = this._nilStyle.getPAP();
            if (baseIndex != 4095) {
                parentPAP = this._styleDescriptions[baseIndex].getPAP();
                if (parentPAP == null) {
                    this.createPap(baseIndex);
                    parentPAP = this._styleDescriptions[baseIndex].getPAP();
                }
            }
            pap = (PAP)uncompressProperty(papx, parentPAP, this);
            sd.setPAP(pap);
        }
    }
    
    private void createChp(final int istd) {
        final StyleDescription sd = this._styleDescriptions[istd];
        CHP chp = sd.getCHP();
        final byte[] chpx = sd.getCHPX();
        final int baseIndex = sd.getBaseStyle();
        if (chp == null && chpx != null) {
            CHP parentCHP = this._nilStyle.getCHP();
            if (baseIndex != 4095) {
                parentCHP = this._styleDescriptions[baseIndex].getCHP();
                if (parentCHP == null) {
                    this.createChp(baseIndex);
                    parentCHP = this._styleDescriptions[baseIndex].getCHP();
                }
            }
            chp = (CHP)uncompressProperty(chpx, parentCHP, this);
            sd.setCHP(chp);
        }
    }
    
    public StyleDescription getStyleDescription(final int x) {
        return this._styleDescriptions[x];
    }
    
    static void doCHPOperation(final CHP oldCHP, final CHP newCHP, final int operand, final int param, final byte[] varParam, final byte[] grpprl, final int offset, final StyleSheet styleSheet) {
        switch (operand) {
            case 0: {
                newCHP._fRMarkDel = getFlag(param);
                break;
            }
            case 1: {
                newCHP._fRMark = getFlag(param);
            }
            case 3: {
                newCHP._fcPic = param;
                newCHP._fSpec = true;
                break;
            }
            case 4: {
                newCHP._ibstRMark = (short)param;
                break;
            }
            case 5: {
                newCHP._dttmRMark[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newCHP._dttmRMark[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 6: {
                newCHP._fData = getFlag(param);
            }
            case 8: {
                final short chsDiff = (short)((param & 0xFF0000) >>> 8);
                newCHP._fChsDiff = getFlag(chsDiff);
                newCHP._chse = (short)(param & 0xFFFF);
                break;
            }
            case 9: {
                newCHP._fSpec = true;
                newCHP._ftcSym = Utils.convertBytesToShort(varParam, 0);
                newCHP._xchSym = Utils.convertBytesToShort(varParam, 2);
                break;
            }
            case 10: {
                newCHP._fOle2 = getFlag(param);
            }
            case 12: {
                newCHP._icoHighlight = (byte)param;
                newCHP._highlighted = getFlag(param);
            }
            case 14: {
                newCHP._fcObj = param;
            }
            case 15: {}
            case 16: {}
            case 17: {}
            case 18: {}
            case 19: {}
            case 20: {}
            case 21: {}
            case 22: {}
            case 23: {}
            case 24: {}
            case 25: {}
            case 26: {}
            case 27: {}
            case 28: {}
            case 29: {}
            case 30: {}
            case 31: {}
            case 32: {}
            case 33: {}
            case 34: {}
            case 35: {}
            case 36: {}
            case 37: {}
            case 38: {}
            case 39: {}
            case 40: {}
            case 41: {}
            case 42: {}
            case 43: {}
            case 44: {}
            case 45: {}
            case 46: {}
            case 48: {
                newCHP._istd = param;
            }
            case 50: {
                newCHP._bold = false;
                newCHP._italic = false;
                newCHP._fOutline = false;
                newCHP._fStrike = false;
                newCHP._fShadow = false;
                newCHP._fSmallCaps = false;
                newCHP._fCaps = false;
                newCHP._fVanish = false;
                newCHP._kul = 0;
                newCHP._ico = 0;
                break;
            }
            case 51: {
                newCHP.copy(oldCHP);
            }
            case 53: {
                newCHP._bold = getCHPFlag((byte)param, oldCHP._bold);
                break;
            }
            case 54: {
                newCHP._italic = getCHPFlag((byte)param, oldCHP._italic);
                break;
            }
            case 55: {
                newCHP._fStrike = getCHPFlag((byte)param, oldCHP._fStrike);
                break;
            }
            case 56: {
                newCHP._fOutline = getCHPFlag((byte)param, oldCHP._fOutline);
                break;
            }
            case 57: {
                newCHP._fShadow = getCHPFlag((byte)param, oldCHP._fShadow);
                break;
            }
            case 58: {
                newCHP._fSmallCaps = getCHPFlag((byte)param, oldCHP._fSmallCaps);
                break;
            }
            case 59: {
                newCHP._fCaps = getCHPFlag((byte)param, oldCHP._fCaps);
                break;
            }
            case 60: {
                newCHP._fVanish = getCHPFlag((byte)param, oldCHP._fVanish);
                break;
            }
            case 61: {
                newCHP._ftc = (short)param;
                break;
            }
            case 62: {
                newCHP._kul = (byte)param;
                break;
            }
            case 63: {
                final int hps = param & 0xFF;
                if (hps != 0) {
                    newCHP._hps = hps;
                }
                final byte cInc = (byte)((byte)(param & 0xFE00) >>> 4 >> 1);
                if (cInc != 0) {
                    newCHP._hps = Math.max(newCHP._hps + cInc * 2, 2);
                }
                final byte hpsPos = (byte)((param & 0xFF0000) >>> 8);
                if (hpsPos != 128) {
                    newCHP._hpsPos = hpsPos;
                }
                final boolean fAdjust = (param & 0x100) > 0;
                if (fAdjust && hpsPos != 128 && hpsPos != 0 && oldCHP._hpsPos == 0) {
                    newCHP._hps = Math.max(newCHP._hps - 2, 2);
                }
                if (fAdjust && hpsPos == 0 && oldCHP._hpsPos != 0) {
                    newCHP._hps = Math.max(newCHP._hps + 2, 2);
                    break;
                }
                break;
            }
            case 64: {
                newCHP._dxaSpace = param;
                break;
            }
            case 65: {
                newCHP._lidDefault = (short)param;
                break;
            }
            case 66: {
                newCHP._ico = (byte)param;
                break;
            }
            case 67: {
                newCHP._hps = param;
                break;
            }
            case 68: {
                final byte hpsLvl = (byte)param;
                newCHP._hps = Math.max(newCHP._hps + hpsLvl * 2, 2);
                break;
            }
            case 69: {
                newCHP._hpsPos = (short)param;
                break;
            }
            case 70: {
                if (param != 0) {
                    if (oldCHP._hpsPos == 0) {
                        newCHP._hps = Math.max(newCHP._hps - 2, 2);
                        break;
                    }
                    break;
                }
                else {
                    if (oldCHP._hpsPos != 0) {
                        newCHP._hps = Math.max(newCHP._hps + 2, 2);
                        break;
                    }
                    break;
                }
                break;
            }
            case 71: {
                CHP genCHP = new CHP();
                genCHP._ftc = 4;
                genCHP = (CHP)uncompressProperty(varParam, genCHP, styleSheet);
                final CHP styleCHP = styleSheet.getStyleDescription(oldCHP._baseIstd).getCHP();
                if (genCHP._bold == newCHP._bold) {
                    newCHP._bold = styleCHP._bold;
                }
                if (genCHP._italic == newCHP._italic) {
                    newCHP._italic = styleCHP._italic;
                }
                if (genCHP._fSmallCaps == newCHP._fSmallCaps) {
                    newCHP._fSmallCaps = styleCHP._fSmallCaps;
                }
                if (genCHP._fVanish == newCHP._fVanish) {
                    newCHP._fVanish = styleCHP._fVanish;
                }
                if (genCHP._fStrike == newCHP._fStrike) {
                    newCHP._fStrike = styleCHP._fStrike;
                }
                if (genCHP._fCaps == newCHP._fCaps) {
                    newCHP._fCaps = styleCHP._fCaps;
                }
                if (genCHP._ftcAscii == newCHP._ftcAscii) {
                    newCHP._ftcAscii = styleCHP._ftcAscii;
                }
                if (genCHP._ftcFE == newCHP._ftcFE) {
                    newCHP._ftcFE = styleCHP._ftcFE;
                }
                if (genCHP._ftcOther == newCHP._ftcOther) {
                    newCHP._ftcOther = styleCHP._ftcOther;
                }
                if (genCHP._hps == newCHP._hps) {
                    newCHP._hps = styleCHP._hps;
                }
                if (genCHP._hpsPos == newCHP._hpsPos) {
                    newCHP._hpsPos = styleCHP._hpsPos;
                }
                if (genCHP._kul == newCHP._kul) {
                    newCHP._kul = styleCHP._kul;
                }
                if (genCHP._dxaSpace == newCHP._dxaSpace) {
                    newCHP._dxaSpace = styleCHP._dxaSpace;
                }
                if (genCHP._ico == newCHP._ico) {
                    newCHP._ico = styleCHP._ico;
                }
                if (genCHP._lidDefault == newCHP._lidDefault) {
                    newCHP._lidDefault = styleCHP._lidDefault;
                }
                if (genCHP._lidFE == newCHP._lidFE) {
                    newCHP._lidFE = styleCHP._lidFE;
                    break;
                }
                break;
            }
            case 72: {
                newCHP._iss = (byte)param;
                break;
            }
            case 73: {
                newCHP._hps = Utils.convertBytesToShort(varParam, 0);
                break;
            }
            case 74: {
                final int increment = Utils.convertBytesToShort(varParam, 0);
                newCHP._hps = Math.max(newCHP._hps + increment, 8);
                break;
            }
            case 75: {
                newCHP._hpsKern = param;
                break;
            }
            case 76: {
                doCHPOperation(oldCHP, newCHP, 71, param, varParam, grpprl, offset, styleSheet);
                break;
            }
            case 77: {
                final float percentage = param / 100.0f;
                final int add = (int)(percentage * newCHP._hps);
                newCHP._hps += add;
                break;
            }
            case 78: {
                newCHP._ysr = (byte)param;
                break;
            }
            case 79: {
                newCHP._ftcAscii = (short)param;
                break;
            }
            case 80: {
                newCHP._ftcFE = (short)param;
                break;
            }
            case 81: {
                newCHP._ftcOther = (short)param;
            }
            case 83: {
                newCHP._fDStrike = getFlag(param);
                break;
            }
            case 84: {
                newCHP._fImprint = getFlag(param);
                break;
            }
            case 85: {
                newCHP._fSpec = getFlag(param);
                break;
            }
            case 86: {
                newCHP._fObj = getFlag(param);
                break;
            }
            case 87: {
                newCHP._fPropMark = getFlag(varParam[0]);
                newCHP._ibstPropRMark = Utils.convertBytesToShort(varParam, 1);
                newCHP._dttmPropRMark = Utils.convertBytesToInt(varParam, 3);
                break;
            }
            case 88: {
                newCHP._fEmboss = getFlag(param);
                break;
            }
            case 89: {
                newCHP._sfxtText = (byte)param;
            }
            case 90: {}
            case 91: {}
            case 92: {}
            case 93: {}
            case 94: {}
            case 95: {}
            case 96: {}
            case 98: {
                newCHP._fDispFldRMark = getFlag(varParam[0]);
                newCHP._ibstDispFldRMark = Utils.convertBytesToShort(varParam, 1);
                newCHP._dttmDispFldRMark = Utils.convertBytesToInt(varParam, 3);
                System.arraycopy(varParam, 7, newCHP._xstDispFldRMark, 0, 32);
                break;
            }
            case 99: {
                newCHP._ibstRMarkDel = (short)param;
                break;
            }
            case 100: {
                newCHP._dttmRMarkDel[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newCHP._dttmRMarkDel[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 101: {
                newCHP._brc[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newCHP._brc[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 102: {
                newCHP._shd = (short)param;
            }
            case 103: {}
            case 104: {}
            case 105: {}
            case 106: {}
            case 107: {}
            case 109: {
                newCHP._lidDefault = (short)param;
                break;
            }
            case 110: {
                newCHP._lidFE = (short)param;
                break;
            }
            case 111: {
                newCHP._idctHint = (byte)param;
                break;
            }
        }
    }
    
    static Object uncompressProperty(final byte[] grpprl, final Object parent, final StyleSheet styleSheet) {
        return uncompressProperty(grpprl, parent, styleSheet, true);
    }
    
    static Object uncompressProperty(final byte[] grpprl, final Object parent, final StyleSheet styleSheet, final boolean doIstd) {
        Object newProperty = null;
        int offset = 0;
        int propertyType = 1;
        if (parent instanceof PAP) {
            try {
                newProperty = ((PAP)parent).clone();
            }
            catch (Exception ex) {}
            if (doIstd) {
                ((PAP)newProperty)._istd = Utils.convertBytesToShort(grpprl, 0);
                offset = 2;
            }
        }
        else if (parent instanceof CHP) {
            try {
                newProperty = ((CHP)parent).clone();
                ((CHP)newProperty)._baseIstd = ((CHP)parent)._istd;
            }
            catch (Exception ex2) {}
            propertyType = 2;
        }
        else if (parent instanceof SEP) {
            newProperty = parent;
            propertyType = 4;
        }
        else {
            if (!(parent instanceof TAP)) {
                return null;
            }
            newProperty = parent;
            propertyType = 5;
            offset = 2;
        }
        while (offset < grpprl.length) {
            final short sprm = Utils.convertBytesToShort(grpprl, offset);
            offset += 2;
            final byte spra = (byte)((sprm & 0xE000) >> 13);
            int opSize = 0;
            int param = 0;
            byte[] varParam = null;
            switch (spra) {
                case 0:
                case 1: {
                    opSize = 1;
                    param = grpprl[offset];
                    break;
                }
                case 2: {
                    opSize = 2;
                    param = Utils.convertBytesToShort(grpprl, offset);
                    break;
                }
                case 3: {
                    opSize = 4;
                    param = Utils.convertBytesToInt(grpprl, offset);
                    break;
                }
                case 4:
                case 5: {
                    opSize = 2;
                    param = Utils.convertBytesToShort(grpprl, offset);
                    break;
                }
                case 6: {
                    if (sprm != -10744) {
                        opSize = Utils.convertUnsignedByteToInt(grpprl[offset]);
                        ++offset;
                    }
                    else {
                        opSize = Utils.convertBytesToShort(grpprl, offset) - 1;
                        offset += 2;
                    }
                    varParam = new byte[opSize];
                    System.arraycopy(grpprl, offset, varParam, 0, opSize);
                    break;
                }
                case 7: {
                    opSize = 3;
                    param = Utils.convertBytesToInt((byte)0, grpprl[offset + 2], grpprl[offset + 1], grpprl[offset]);
                    break;
                }
                default: {
                    throw new RuntimeException("unrecognized pap opcode");
                }
            }
            offset += opSize;
            final short operand = (short)(sprm & 0x1FF);
            final byte type = (byte)((sprm & 0x1C00) >> 10);
            switch (propertyType) {
                case 1: {
                    if (type == 1) {
                        doPAPOperation((PAP)newProperty, operand, param, varParam, grpprl, offset, spra);
                        continue;
                    }
                    continue;
                }
                case 2: {
                    doCHPOperation((CHP)parent, (CHP)newProperty, operand, param, varParam, grpprl, offset, styleSheet);
                    continue;
                }
                case 4: {
                    doSEPOperation((SEP)newProperty, operand, param, varParam);
                    continue;
                }
                case 5: {
                    if (type == 5) {
                        doTAPOperation((TAP)newProperty, operand, param, varParam);
                        continue;
                    }
                    continue;
                }
            }
        }
        return newProperty;
    }
    
    static void doPAPOperation(final PAP newPAP, final int operand, final int param, final byte[] varParam, final byte[] grpprl, final int offset, final int spra) {
        switch (operand) {
            case 0: {
                newPAP._istd = param;
            }
            case 2: {
                if (newPAP._istd > 9 && newPAP._istd < 1) {
                    break;
                }
                newPAP._istd += param;
                if (param > 0) {
                    newPAP._istd = Math.max(newPAP._istd, 9);
                    break;
                }
                newPAP._istd = Math.min(newPAP._istd, 1);
                break;
            }
            case 3: {
                newPAP._jc = (byte)param;
                break;
            }
            case 4: {
                newPAP._fSideBySide = (byte)param;
                break;
            }
            case 5: {
                newPAP._fKeep = (byte)param;
                break;
            }
            case 6: {
                newPAP._fKeepFollow = (byte)param;
                break;
            }
            case 7: {
                newPAP._fPageBreakBefore = (byte)param;
                break;
            }
            case 8: {
                newPAP._brcl = (byte)param;
                break;
            }
            case 9: {
                newPAP._brcp = (byte)param;
                break;
            }
            case 10: {
                newPAP._ilvl = (byte)param;
                break;
            }
            case 11: {
                newPAP._ilfo = param;
                break;
            }
            case 12: {
                newPAP._fNoLnn = (byte)param;
            }
            case 14: {
                newPAP._dxaRight = param;
                break;
            }
            case 15: {
                newPAP._dxaLeft = param;
                break;
            }
            case 16: {
                newPAP._dxaLeft += param;
                newPAP._dxaLeft = Math.max(0, newPAP._dxaLeft);
                break;
            }
            case 17: {
                newPAP._dxaLeft1 = param;
                break;
            }
            case 18: {
                newPAP._lspd[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newPAP._lspd[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 19: {
                newPAP._dyaBefore = param;
                break;
            }
            case 20: {
                newPAP._dyaAfter = param;
            }
            case 22: {
                newPAP._fInTable = (byte)param;
                break;
            }
            case 23: {
                newPAP._fTtp = (byte)param;
                break;
            }
            case 24: {
                newPAP._dxaAbs = param;
                break;
            }
            case 25: {
                newPAP._dyaAbs = param;
                break;
            }
            case 26: {
                newPAP._dxaWidth = param;
            }
            case 28: {
                newPAP._brcTop1 = (short)param;
                break;
            }
            case 29: {
                newPAP._brcLeft1 = (short)param;
                break;
            }
            case 30: {
                newPAP._brcBottom1 = (short)param;
                break;
            }
            case 31: {
                newPAP._brcRight1 = (short)param;
                break;
            }
            case 32: {
                newPAP._brcBetween1 = (short)param;
                break;
            }
            case 33: {
                newPAP._brcBar1 = (byte)param;
                break;
            }
            case 34: {
                newPAP._dxaFromText = param;
                break;
            }
            case 35: {
                newPAP._wr = (byte)param;
                break;
            }
            case 36: {
                newPAP._brcTop[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newPAP._brcTop[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 37: {
                newPAP._brcLeft[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newPAP._brcLeft[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 38: {
                newPAP._brcBottom[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newPAP._brcBottom[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 39: {
                newPAP._brcRight[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newPAP._brcRight[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 40: {
                newPAP._brcBetween[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newPAP._brcBetween[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 41: {
                newPAP._brcBar[0] = Utils.convertBytesToShort(grpprl, offset - 4);
                newPAP._brcBar[1] = Utils.convertBytesToShort(grpprl, offset - 2);
                break;
            }
            case 42: {
                newPAP._fNoAutoHyph = (byte)param;
                break;
            }
            case 43: {
                newPAP._dyaHeight = param;
                break;
            }
            case 44: {
                newPAP._dcs = param;
                break;
            }
            case 45: {
                newPAP._shd = param;
                break;
            }
            case 46: {
                newPAP._dyaFromText = param;
                break;
            }
            case 47: {
                newPAP._dxaFromText = param;
                break;
            }
            case 48: {
                newPAP._fLocked = (byte)param;
                break;
            }
            case 49: {
                newPAP._fWindowControl = (byte)param;
            }
            case 51: {
                newPAP._fKinsoku = (byte)param;
                break;
            }
            case 52: {
                newPAP._fWordWrap = (byte)param;
                break;
            }
            case 53: {
                newPAP._fOverflowPunct = (byte)param;
                break;
            }
            case 54: {
                newPAP._fTopLinePunct = (byte)param;
                break;
            }
            case 55: {
                newPAP._fAutoSpaceDE = (byte)param;
                break;
            }
            case 56: {
                newPAP._fAutoSpaceDN = (byte)param;
                break;
            }
            case 57: {
                newPAP._wAlignFont = param;
                break;
            }
            case 58: {
                newPAP._fontAlign = (short)param;
            }
            case 62: {
                newPAP._anld = varParam;
            }
            case 63: {}
            case 64: {}
            case 65: {}
            case 67: {}
            case 69: {
                if (spra == 6) {
                    newPAP._numrm = varParam;
                    break;
                }
                break;
            }
            case 71: {
                newPAP._fUsePgsuSettings = (byte)param;
                break;
            }
            case 72: {
                newPAP._fAdjustRight = (byte)param;
                break;
            }
        }
    }
    
    static void doTAPOperation(final TAP newTAP, final int operand, final int param, final byte[] varParam) {
        switch (operand) {
            case 0: {
                newTAP._jc = (short)param;
                break;
            }
            case 1: {
                final int adjust = param - (newTAP._rgdxaCenter[0] + newTAP._dxaGapHalf);
                for (int x = 0; x < newTAP._itcMac; ++x) {
                    final short[] rgdxaCenter2 = newTAP._rgdxaCenter;
                    final int n = x;
                    rgdxaCenter2[n] += (short)adjust;
                }
                break;
            }
            case 2: {
                if (newTAP._rgdxaCenter != null) {
                    final int adjust = newTAP._dxaGapHalf - param;
                    final short[] rgdxaCenter3 = newTAP._rgdxaCenter;
                    final int n2 = 0;
                    rgdxaCenter3[n2] += (short)adjust;
                }
                newTAP._dxaGapHalf = param;
                break;
            }
            case 3: {
                newTAP._fCantSplit = getFlag(param);
                break;
            }
            case 4: {
                newTAP._fTableHeader = getFlag(param);
                break;
            }
            case 5: {
                newTAP._brcTop[0] = Utils.convertBytesToShort(varParam, 0);
                newTAP._brcTop[1] = Utils.convertBytesToShort(varParam, 2);
                newTAP._brcLeft[0] = Utils.convertBytesToShort(varParam, 4);
                newTAP._brcLeft[1] = Utils.convertBytesToShort(varParam, 6);
                newTAP._brcBottom[0] = Utils.convertBytesToShort(varParam, 8);
                newTAP._brcBottom[1] = Utils.convertBytesToShort(varParam, 10);
                newTAP._brcRight[0] = Utils.convertBytesToShort(varParam, 12);
                newTAP._brcRight[1] = Utils.convertBytesToShort(varParam, 14);
                newTAP._brcHorizontal[0] = Utils.convertBytesToShort(varParam, 16);
                newTAP._brcHorizontal[1] = Utils.convertBytesToShort(varParam, 18);
                newTAP._brcVertical[0] = Utils.convertBytesToShort(varParam, 20);
                newTAP._brcVertical[1] = Utils.convertBytesToShort(varParam, 22);
            }
            case 7: {
                newTAP._dyaRowHeight = param;
                break;
            }
            case 8: {
                newTAP._itcMac = varParam[0];
                newTAP._rgdxaCenter = new short[varParam[0] + 1];
                newTAP._rgtc = new TC[varParam[0]];
                for (int x2 = 0; x2 < newTAP._itcMac; ++x2) {
                    newTAP._rgdxaCenter[x2] = Utils.convertBytesToShort(varParam, 1 + x2 * 2);
                    newTAP._rgtc[x2] = TC.convertBytesToTC(varParam, 1 + (varParam[0] + 1) * 2 + x2 * 20);
                }
                newTAP._rgdxaCenter[newTAP._itcMac] = Utils.convertBytesToShort(varParam, 1 + newTAP._itcMac * 2);
            }
            case 9: {}
            case 32: {
                for (int x2 = varParam[0]; x2 < varParam[1]; ++x2) {
                    if ((varParam[2] & 0x8) > 0) {
                        newTAP._rgtc[x2]._brcRight[0] = Utils.convertBytesToShort(varParam, 6);
                        newTAP._rgtc[x2]._brcRight[1] = Utils.convertBytesToShort(varParam, 8);
                    }
                    else if ((varParam[2] & 0x4) > 0) {
                        newTAP._rgtc[x2]._brcBottom[0] = Utils.convertBytesToShort(varParam, 6);
                        newTAP._rgtc[x2]._brcBottom[1] = Utils.convertBytesToShort(varParam, 8);
                    }
                    else if ((varParam[2] & 0x2) > 0) {
                        newTAP._rgtc[x2]._brcLeft[0] = Utils.convertBytesToShort(varParam, 6);
                        newTAP._rgtc[x2]._brcLeft[1] = Utils.convertBytesToShort(varParam, 8);
                    }
                    else if ((varParam[2] & 0x1) > 0) {
                        newTAP._rgtc[x2]._brcTop[0] = Utils.convertBytesToShort(varParam, 6);
                        newTAP._rgtc[x2]._brcTop[1] = Utils.convertBytesToShort(varParam, 8);
                    }
                }
                break;
            }
            case 33: {
                int index = (param & 0xFF000000) >> 24;
                final int count = (param & 0xFF0000) >> 16;
                final int width = param & 0xFFFF;
                final short[] rgdxaCenter = new short[newTAP._itcMac + count + 1];
                final TC[] rgtc = new TC[newTAP._itcMac + count];
                if (index >= newTAP._itcMac) {
                    index = newTAP._itcMac;
                    System.arraycopy(newTAP._rgdxaCenter, 0, rgdxaCenter, 0, newTAP._itcMac + 1);
                    System.arraycopy(newTAP._rgtc, 0, rgtc, 0, newTAP._itcMac);
                }
                else {
                    System.arraycopy(newTAP._rgdxaCenter, 0, rgdxaCenter, 0, index + 1);
                    System.arraycopy(newTAP._rgdxaCenter, index + 1, rgdxaCenter, index + count, newTAP._itcMac - index);
                    System.arraycopy(newTAP._rgtc, 0, rgtc, 0, index);
                    System.arraycopy(newTAP._rgtc, index, rgtc, index + count, newTAP._itcMac - index);
                }
                for (int x3 = index; x3 < index + count; ++x3) {
                    rgtc[x3] = new TC();
                    rgdxaCenter[x3] = (short)(rgdxaCenter[x3 - 1] + width);
                }
                rgdxaCenter[index + count] = (short)(rgdxaCenter[index + count - 1] + width);
            }
        }
    }
    
    static void doSEPOperation(final SEP newSEP, final int operand, final int param, final byte[] varParam) {
        switch (operand) {
            case 0: {
                newSEP._cnsPgn = (byte)param;
                break;
            }
            case 1: {
                newSEP._iHeadingPgn = (byte)param;
                break;
            }
            case 2: {
                newSEP._olstAnn = varParam;
            }
            case 3: {}
            case 5: {
                newSEP._fEvenlySpaced = getFlag(param);
                break;
            }
            case 6: {
                newSEP._fUnlocked = getFlag(param);
                break;
            }
            case 7: {
                newSEP._dmBinFirst = (short)param;
                break;
            }
            case 8: {
                newSEP._dmBinOther = (short)param;
                break;
            }
            case 9: {
                newSEP._bkc = (byte)param;
                break;
            }
            case 10: {
                newSEP._fTitlePage = getFlag(param);
                break;
            }
            case 11: {
                newSEP._ccolM1 = (short)param;
                break;
            }
            case 12: {
                newSEP._dxaColumns = param;
                break;
            }
            case 13: {
                newSEP._fAutoPgn = getFlag(param);
                break;
            }
            case 14: {
                newSEP._nfcPgn = (byte)param;
                break;
            }
            case 15: {
                newSEP._dyaPgn = (short)param;
                break;
            }
            case 16: {
                newSEP._dxaPgn = (short)param;
                break;
            }
            case 17: {
                newSEP._fPgnRestart = getFlag(param);
                break;
            }
            case 18: {
                newSEP._fEndNote = getFlag(param);
                break;
            }
            case 19: {
                newSEP._lnc = (byte)param;
                break;
            }
            case 20: {
                newSEP._grpfIhdt = (byte)param;
                break;
            }
            case 21: {
                newSEP._nLnnMod = (short)param;
                break;
            }
            case 22: {
                newSEP._dxaLnn = param;
                break;
            }
            case 23: {
                newSEP._dyaHdrTop = param;
                break;
            }
            case 24: {
                newSEP._dyaHdrBottom = param;
                break;
            }
            case 25: {
                newSEP._fLBetween = getFlag(param);
                break;
            }
            case 26: {
                newSEP._vjc = (byte)param;
                break;
            }
            case 27: {
                newSEP._lnnMin = (short)param;
                break;
            }
            case 28: {
                newSEP._pgnStart = (short)param;
                break;
            }
            case 29: {
                newSEP._dmOrientPage = (byte)param;
            }
            case 31: {
                newSEP._xaPage = param;
                break;
            }
            case 32: {
                newSEP._yaPage = param;
                break;
            }
            case 33: {
                newSEP._dxaLeft = param;
                break;
            }
            case 34: {
                newSEP._dxaRight = param;
                break;
            }
            case 35: {
                newSEP._dyaTop = param;
                break;
            }
            case 36: {
                newSEP._dyaBottom = param;
                break;
            }
            case 37: {
                newSEP._dzaGutter = param;
                break;
            }
            case 38: {
                newSEP._dmPaperReq = (short)param;
                break;
            }
            case 39: {
                newSEP._fPropMark = getFlag(varParam[0]);
            }
            case 40: {}
            case 41: {}
            case 43: {
                newSEP._brcTop[0] = (short)(param & 0xFFFF);
                newSEP._brcTop[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 44: {
                newSEP._brcLeft[0] = (short)(param & 0xFFFF);
                newSEP._brcLeft[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 45: {
                newSEP._brcBottom[0] = (short)(param & 0xFFFF);
                newSEP._brcBottom[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 46: {
                newSEP._brcRight[0] = (short)(param & 0xFFFF);
                newSEP._brcRight[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 47: {
                newSEP._pgbProp = (short)param;
                break;
            }
            case 48: {
                newSEP._dxtCharSpace = param;
                break;
            }
            case 49: {
                newSEP._dyaLinePitch = param;
                break;
            }
            case 51: {
                newSEP._wTextFlow = (short)param;
                break;
            }
        }
    }
    
    private static boolean getCHPFlag(final byte x, final boolean oldVal) {
        switch (x) {
            case 0: {
                return false;
            }
            case 1: {
                return true;
            }
            case Byte.MIN_VALUE: {
                return oldVal;
            }
            case -127: {
                return !oldVal;
            }
            default: {
                return false;
            }
        }
    }
    
    public static boolean getFlag(final int x) {
        return x != 0;
    }
}
