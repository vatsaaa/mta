// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class PAP implements Cloneable
{
    int _istd;
    byte _jc;
    byte _fKeep;
    byte _fKeepFollow;
    byte _fPageBreakBefore;
    byte _positionByte;
    byte _brcp;
    byte _brcl;
    byte _ilvl;
    byte _fNoLnn;
    int _ilfo;
    byte _fSideBySide;
    byte _fNoAutoHyph;
    byte _fWindowControl;
    int _dxaRight;
    int _dxaLeft;
    int _dxaLeft1;
    int[] _lspd;
    int _dyaBefore;
    int _dyaAfter;
    byte[] _phe;
    byte _fCrLf;
    byte _fUsePgsuSettings;
    byte _fAdjustRight;
    byte _fKinsoku;
    byte _fWordWrap;
    byte _fOverflowPunct;
    byte _fTopLinePunct;
    byte _fAutoSpaceDE;
    byte _fAutoSpaceDN;
    int _wAlignFont;
    short _fontAlign;
    byte _fInTable;
    byte _fTtp;
    byte _wr;
    byte _fLocked;
    int _dxaAbs;
    int _dyaAbs;
    int _dxaWidth;
    short[] _brcTop;
    short[] _brcLeft;
    short[] _brcBottom;
    short[] _brcRight;
    short[] _brcBetween;
    short[] _brcBar;
    short _brcTop1;
    short _brcLeft1;
    short _brcBottom1;
    short _brcRight1;
    short _brcBetween1;
    short _brcBar1;
    int _dxaFromText;
    int _dyaFromText;
    int _dyaHeight;
    int _shd;
    int _dcs;
    byte[] _anld;
    short _fPropRMark;
    short _ibstPropRMark;
    byte[] _dttmPropRMark;
    byte[] _numrm;
    short _itbdMac;
    
    public PAP() {
        this._lspd = new int[2];
        this._phe = new byte[12];
        this._brcTop = new short[2];
        this._brcLeft = new short[2];
        this._brcBottom = new short[2];
        this._brcRight = new short[2];
        this._brcBetween = new short[2];
        this._brcBar = new short[2];
        this._anld = new byte[84];
        this._dttmPropRMark = new byte[4];
        this._numrm = new byte[8];
        this._fWindowControl = 1;
        this._lspd[1] = 1;
        this._ilvl = 9;
    }
    
    public Object clone() throws CloneNotSupportedException {
        final PAP clone = (PAP)super.clone();
        clone._brcBar = new short[2];
        clone._brcBottom = new short[2];
        clone._brcLeft = new short[2];
        clone._brcBetween = new short[2];
        clone._brcRight = new short[2];
        clone._brcTop = new short[2];
        clone._lspd = new int[2];
        clone._phe = new byte[12];
        clone._anld = new byte[84];
        clone._dttmPropRMark = new byte[4];
        clone._numrm = new byte[8];
        System.arraycopy(this._brcBar, 0, clone._brcBar, 0, 2);
        System.arraycopy(this._brcBottom, 0, clone._brcBottom, 0, 2);
        System.arraycopy(this._brcLeft, 0, clone._brcLeft, 0, 2);
        System.arraycopy(this._brcBetween, 0, clone._brcBetween, 0, 2);
        System.arraycopy(this._brcRight, 0, clone._brcRight, 0, 2);
        System.arraycopy(this._brcTop, 0, clone._brcTop, 0, 2);
        System.arraycopy(this._lspd, 0, clone._lspd, 0, 2);
        System.arraycopy(this._phe, 0, clone._phe, 0, 12);
        System.arraycopy(this._anld, 0, clone._anld, 0, 84);
        System.arraycopy(this._dttmPropRMark, 0, clone._dttmPropRMark, 0, 4);
        System.arraycopy(this._numrm, 0, clone._numrm, 0, 8);
        return clone;
    }
}
