// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor.util;

import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.Iterator;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.AbstractSet;

@Deprecated
public final class BTreeSet extends AbstractSet implements Set
{
    public BTreeNode root;
    private Comparator comparator;
    int order;
    int size;
    
    public BTreeSet() {
        this(6);
    }
    
    public BTreeSet(final Collection c) {
        this(6);
        this.addAll(c);
    }
    
    public BTreeSet(final int order) {
        this(order, null);
    }
    
    public BTreeSet(final int order, final Comparator comparator) {
        this.comparator = null;
        this.size = 0;
        this.order = order;
        this.comparator = comparator;
        this.root = new BTreeNode(null);
    }
    
    @Override
    public boolean add(final Object x) throws IllegalArgumentException {
        if (x == null) {
            throw new IllegalArgumentException();
        }
        return this.root.insert(x, -1);
    }
    
    @Override
    public boolean contains(final Object x) {
        return this.root.includes(x);
    }
    
    @Override
    public boolean remove(final Object x) {
        return x != null && this.root.delete(x, -1);
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    @Override
    public void clear() {
        this.root = new BTreeNode(null);
        this.size = 0;
    }
    
    @Override
    public Iterator iterator() {
        return new BTIterator();
    }
    
    int compare(final Object x, final Object y) {
        return (this.comparator == null) ? ((Comparable)x).compareTo(y) : this.comparator.compare(x, y);
    }
    
    private final class BTIterator implements Iterator
    {
        private int index;
        Stack parentIndex;
        private Object lastReturned;
        private Object next;
        BTreeNode currentNode;
        
        BTIterator() {
            this.index = 0;
            this.parentIndex = new Stack();
            this.lastReturned = null;
            this.currentNode = this.firstNode();
            this.next = this.nextElement();
        }
        
        public boolean hasNext() {
            return this.next != null;
        }
        
        public Object next() {
            if (this.next == null) {
                throw new NoSuchElementException();
            }
            this.lastReturned = this.next;
            this.next = this.nextElement();
            return this.lastReturned;
        }
        
        public void remove() {
            if (this.lastReturned == null) {
                throw new NoSuchElementException();
            }
            BTreeSet.this.remove(this.lastReturned);
            this.lastReturned = null;
        }
        
        private BTreeNode firstNode() {
            BTreeNode temp = BTreeSet.this.root;
            while (temp._entries[0].child != null) {
                temp = temp._entries[0].child;
                this.parentIndex.push(0);
            }
            return temp;
        }
        
        private Object nextElement() {
            if (!this.currentNode.isLeaf()) {
                this.currentNode = this.currentNode._entries[this.index].child;
                this.parentIndex.push(this.index);
                while (this.currentNode._entries[0].child != null) {
                    this.currentNode = this.currentNode._entries[0].child;
                    this.parentIndex.push(0);
                }
                this.index = 1;
                return this.currentNode._entries[0].element;
            }
            if (this.index < this.currentNode._nrElements) {
                return this.currentNode._entries[this.index++].element;
            }
            if (!this.parentIndex.empty()) {
                this.currentNode = this.currentNode._parent;
                this.index = this.parentIndex.pop();
                while (this.index == this.currentNode._nrElements && !this.parentIndex.empty()) {
                    this.currentNode = this.currentNode._parent;
                    this.index = this.parentIndex.pop();
                }
                if (this.index == this.currentNode._nrElements) {
                    return null;
                }
                return this.currentNode._entries[this.index++].element;
            }
            else {
                if (this.index == this.currentNode._nrElements) {
                    return null;
                }
                return this.currentNode._entries[this.index++].element;
            }
        }
    }
    
    public static class Entry
    {
        public Object element;
        public BTreeNode child;
    }
    
    public class BTreeNode
    {
        public Entry[] _entries;
        public BTreeNode _parent;
        int _nrElements;
        private final int MIN;
        
        BTreeNode(final BTreeNode parent) {
            this._nrElements = 0;
            this.MIN = (BTreeSet.this.order - 1) / 2;
            this._parent = parent;
            (this._entries = new Entry[BTreeSet.this.order])[0] = new Entry();
        }
        
        boolean insert(final Object x, final int parentIndex) {
            if (this.isFull()) {
                final Object splitNode = this._entries[this._nrElements / 2].element;
                final BTreeNode rightSibling = this.split();
                if (this.isRoot()) {
                    this.splitRoot(splitNode, this, rightSibling);
                    if (BTreeSet.this.compare(x, BTreeSet.this.root._entries[0].element) < 0) {
                        this.insert(x, 0);
                    }
                    else {
                        rightSibling.insert(x, 1);
                    }
                    return false;
                }
                this._parent.insertSplitNode(splitNode, this, rightSibling, parentIndex);
                if (BTreeSet.this.compare(x, this._parent._entries[parentIndex].element) < 0) {
                    return this.insert(x, parentIndex);
                }
                return rightSibling.insert(x, parentIndex + 1);
            }
            else {
                if (!this.isLeaf()) {
                    final int insertAt = this.childToInsertAt(x, true);
                    return insertAt != -1 && this._entries[insertAt].child.insert(x, insertAt);
                }
                final int insertAt = this.childToInsertAt(x, true);
                if (insertAt == -1) {
                    return false;
                }
                this.insertNewElement(x, insertAt);
                final BTreeSet this$0 = BTreeSet.this;
                ++this$0.size;
                return true;
            }
        }
        
        boolean includes(final Object x) {
            final int index = this.childToInsertAt(x, true);
            return index == -1 || (this._entries[index] != null && this._entries[index].child != null && this._entries[index].child.includes(x));
        }
        
        boolean delete(final Object x, int parentIndex) {
            int i = this.childToInsertAt(x, true);
            int priorParentIndex = parentIndex;
            BTreeNode temp = this;
            Label_0073: {
                if (i != -1) {
                    while (temp._entries[i] != null && temp._entries[i].child != null) {
                        temp = temp._entries[i].child;
                        priorParentIndex = parentIndex;
                        parentIndex = i;
                        i = temp.childToInsertAt(x, true);
                        if (i == -1) {
                            break Label_0073;
                        }
                    }
                    return false;
                }
            }
            if (!temp.isLeaf()) {
                temp.switchWithSuccessor(x);
                parentIndex = temp.childToInsertAt(x, false) + 1;
                return temp._entries[parentIndex].child.delete(x, parentIndex);
            }
            if (temp._nrElements > this.MIN) {
                temp.deleteElement(x);
                final BTreeSet this$0 = BTreeSet.this;
                --this$0.size;
                return true;
            }
            temp.prepareForDeletion(parentIndex);
            temp.deleteElement(x);
            final BTreeSet this$2 = BTreeSet.this;
            --this$2.size;
            temp.fixAfterDeletion(priorParentIndex);
            return true;
        }
        
        private boolean isFull() {
            return this._nrElements == BTreeSet.this.order - 1;
        }
        
        boolean isLeaf() {
            return this._entries[0].child == null;
        }
        
        private boolean isRoot() {
            return this._parent == null;
        }
        
        private BTreeNode split() {
            final BTreeNode rightSibling = new BTreeNode(this._parent);
            int index = this._nrElements / 2;
            this._entries[index++].element = null;
            int i = 0;
            for (int nr = this._nrElements; index <= nr; ++index) {
                rightSibling._entries[i] = this._entries[index];
                if (rightSibling._entries[i] != null && rightSibling._entries[i].child != null) {
                    rightSibling._entries[i].child._parent = rightSibling;
                }
                this._entries[index] = null;
                --this._nrElements;
                final BTreeNode bTreeNode = rightSibling;
                ++bTreeNode._nrElements;
                ++i;
            }
            final BTreeNode bTreeNode2 = rightSibling;
            --bTreeNode2._nrElements;
            return rightSibling;
        }
        
        private void splitRoot(final Object splitNode, final BTreeNode left, final BTreeNode right) {
            final BTreeNode newRoot = new BTreeNode(null);
            newRoot._entries[0].element = splitNode;
            newRoot._entries[0].child = left;
            newRoot._entries[1] = new Entry();
            newRoot._entries[1].child = right;
            newRoot._nrElements = 1;
            final BTreeNode bTreeNode = newRoot;
            right._parent = bTreeNode;
            left._parent = bTreeNode;
            BTreeSet.this.root = newRoot;
        }
        
        private void insertSplitNode(final Object splitNode, final BTreeNode left, final BTreeNode right, final int insertAt) {
            for (int i = this._nrElements; i >= insertAt; --i) {
                this._entries[i + 1] = this._entries[i];
            }
            this._entries[insertAt] = new Entry();
            this._entries[insertAt].element = splitNode;
            this._entries[insertAt].child = left;
            this._entries[insertAt + 1].child = right;
            ++this._nrElements;
        }
        
        private void insertNewElement(final Object x, final int insertAt) {
            for (int i = this._nrElements; i > insertAt; --i) {
                this._entries[i] = this._entries[i - 1];
            }
            this._entries[insertAt] = new Entry();
            this._entries[insertAt].element = x;
            ++this._nrElements;
        }
        
        private int childToInsertAt(final Object x, final boolean position) {
            int index = this._nrElements / 2;
            if (this._entries[index] == null || this._entries[index].element == null) {
                return index;
            }
            int lo = 0;
            int hi = this._nrElements - 1;
            while (lo <= hi) {
                if (BTreeSet.this.compare(x, this._entries[index].element) > 0) {
                    lo = index + 1;
                    index = (hi + lo) / 2;
                }
                else {
                    hi = index - 1;
                    index = (hi + lo) / 2;
                }
            }
            ++hi;
            if (this._entries[hi] == null || this._entries[hi].element == null) {
                return hi;
            }
            return position ? ((BTreeSet.this.compare(x, this._entries[hi].element) == 0) ? -1 : hi) : hi;
        }
        
        private void deleteElement(final Object x) {
            int index;
            for (index = this.childToInsertAt(x, false); index < this._nrElements - 1; ++index) {
                this._entries[index] = this._entries[index + 1];
            }
            if (this._nrElements == 1) {
                this._entries[index] = new Entry();
            }
            else {
                this._entries[index] = null;
            }
            --this._nrElements;
        }
        
        private void prepareForDeletion(final int parentIndex) {
            if (this.isRoot()) {
                return;
            }
            if (parentIndex != 0 && this._parent._entries[parentIndex - 1].child._nrElements > this.MIN) {
                this.stealLeft(parentIndex);
                return;
            }
            if (parentIndex < this._entries.length && this._parent._entries[parentIndex + 1] != null && this._parent._entries[parentIndex + 1].child != null && this._parent._entries[parentIndex + 1].child._nrElements > this.MIN) {
                this.stealRight(parentIndex);
                return;
            }
            if (parentIndex != 0) {
                this.mergeLeft(parentIndex);
                return;
            }
            this.mergeRight(parentIndex);
        }
        
        private void fixAfterDeletion(final int parentIndex) {
            if (this.isRoot() || this._parent.isRoot()) {
                return;
            }
            if (this._parent._nrElements < this.MIN) {
                final BTreeNode temp = this._parent;
                temp.prepareForDeletion(parentIndex);
                if (temp._parent == null) {
                    return;
                }
                if (!temp._parent.isRoot() && temp._parent._nrElements < this.MIN) {
                    BTreeNode x;
                    int i;
                    for (x = temp._parent._parent, i = 0; i < this._entries.length && x._entries[i].child != temp._parent; ++i) {}
                    temp._parent.fixAfterDeletion(i);
                }
            }
        }
        
        private void switchWithSuccessor(final Object x) {
            final int index = this.childToInsertAt(x, false);
            BTreeNode temp;
            for (temp = this._entries[index + 1].child; temp._entries[0] != null && temp._entries[0].child != null; temp = temp._entries[0].child) {}
            final Object successor = temp._entries[0].element;
            temp._entries[0].element = this._entries[index].element;
            this._entries[index].element = successor;
        }
        
        private void stealLeft(final int parentIndex) {
            final BTreeNode p = this._parent;
            final BTreeNode ls = this._parent._entries[parentIndex - 1].child;
            if (this.isLeaf()) {
                final int add = this.childToInsertAt(p._entries[parentIndex - 1].element, true);
                this.insertNewElement(p._entries[parentIndex - 1].element, add);
                p._entries[parentIndex - 1].element = ls._entries[ls._nrElements - 1].element;
                ls._entries[ls._nrElements - 1] = null;
                final BTreeNode bTreeNode = ls;
                --bTreeNode._nrElements;
            }
            else {
                this._entries[0].element = p._entries[parentIndex - 1].element;
                p._entries[parentIndex - 1].element = ls._entries[ls._nrElements - 1].element;
                this._entries[0].child = ls._entries[ls._nrElements].child;
                this._entries[0].child._parent = this;
                ls._entries[ls._nrElements] = null;
                ls._entries[ls._nrElements - 1].element = null;
                ++this._nrElements;
                final BTreeNode bTreeNode2 = ls;
                --bTreeNode2._nrElements;
            }
        }
        
        private void stealRight(final int parentIndex) {
            final BTreeNode p = this._parent;
            final BTreeNode rs = p._entries[parentIndex + 1].child;
            if (this.isLeaf()) {
                this._entries[this._nrElements] = new Entry();
                this._entries[this._nrElements].element = p._entries[parentIndex].element;
                p._entries[parentIndex].element = rs._entries[0].element;
                for (int i = 0; i < rs._nrElements; ++i) {
                    rs._entries[i] = rs._entries[i + 1];
                }
                rs._entries[rs._nrElements - 1] = null;
                ++this._nrElements;
                final BTreeNode bTreeNode = rs;
                --bTreeNode._nrElements;
            }
            else {
                for (int i = 0; i <= this._nrElements; ++i) {
                    this._entries[i] = this._entries[i + 1];
                }
                this._entries[this._nrElements].element = p._entries[parentIndex].element;
                p._entries[parentIndex].element = rs._entries[0].element;
                this._entries[this._nrElements + 1] = new Entry();
                this._entries[this._nrElements + 1].child = rs._entries[0].child;
                this._entries[this._nrElements + 1].child._parent = this;
                for (int i = 0; i <= rs._nrElements; ++i) {
                    rs._entries[i] = rs._entries[i + 1];
                }
                rs._entries[rs._nrElements] = null;
                ++this._nrElements;
                final BTreeNode bTreeNode2 = rs;
                --bTreeNode2._nrElements;
            }
        }
        
        private void mergeLeft(final int parentIndex) {
            final BTreeNode p = this._parent;
            final BTreeNode ls = p._entries[parentIndex - 1].child;
            if (this.isLeaf()) {
                final int add = this.childToInsertAt(p._entries[parentIndex - 1].element, true);
                this.insertNewElement(p._entries[parentIndex - 1].element, add);
                p._entries[parentIndex - 1].element = null;
                int i = this._nrElements - 1;
                final int nr = ls._nrElements;
                while (i >= 0) {
                    this._entries[i + nr] = this._entries[i];
                    --i;
                }
                for (i = ls._nrElements - 1; i >= 0; --i) {
                    this._entries[i] = ls._entries[i];
                    ++this._nrElements;
                }
                if (p._nrElements == this.MIN && p != BTreeSet.this.root) {
                    int x = parentIndex - 1;
                    for (int y = parentIndex - 2; y >= 0; --y) {
                        p._entries[x] = p._entries[y];
                        --x;
                    }
                    p._entries[0] = new Entry();
                    p._entries[0].child = ls;
                }
                else {
                    int x = parentIndex - 1;
                    for (int y = parentIndex; y <= p._nrElements; ++y) {
                        p._entries[x] = p._entries[y];
                        ++x;
                    }
                    p._entries[p._nrElements] = null;
                }
                final BTreeNode bTreeNode = p;
                --bTreeNode._nrElements;
                if (p.isRoot() && p._nrElements == 0) {
                    BTreeSet.this.root = this;
                    this._parent = null;
                }
            }
            else {
                this._entries[0].element = p._entries[parentIndex - 1].element;
                this._entries[0].child = ls._entries[ls._nrElements].child;
                ++this._nrElements;
                int x2 = this._nrElements;
                final int nr2 = ls._nrElements;
                while (x2 >= 0) {
                    this._entries[x2 + nr2] = this._entries[x2];
                    --x2;
                }
                for (x2 = ls._nrElements - 1; x2 >= 0; --x2) {
                    this._entries[x2] = ls._entries[x2];
                    this._entries[x2].child._parent = this;
                    ++this._nrElements;
                }
                if (p._nrElements == this.MIN && p != BTreeSet.this.root) {
                    x2 = parentIndex - 1;
                    for (int y2 = parentIndex - 2; y2 >= 0; ++y2) {
                        System.out.println(x2 + " " + y2);
                        p._entries[x2] = p._entries[y2];
                        ++x2;
                    }
                    p._entries[0] = new Entry();
                }
                else {
                    x2 = parentIndex - 1;
                    for (int y2 = parentIndex; y2 <= p._nrElements; ++y2) {
                        p._entries[x2] = p._entries[y2];
                        ++x2;
                    }
                    p._entries[p._nrElements] = null;
                }
                final BTreeNode bTreeNode2 = p;
                --bTreeNode2._nrElements;
                if (p.isRoot() && p._nrElements == 0) {
                    BTreeSet.this.root = this;
                    this._parent = null;
                }
            }
        }
        
        private void mergeRight(int parentIndex) {
            final BTreeNode p = this._parent;
            final BTreeNode rs = p._entries[parentIndex + 1].child;
            if (this.isLeaf()) {
                this._entries[this._nrElements] = new Entry();
                this._entries[this._nrElements].element = p._entries[parentIndex].element;
                ++this._nrElements;
                for (int i = 0, nr = this._nrElements; i < rs._nrElements; ++i, ++nr) {
                    this._entries[nr] = rs._entries[i];
                    ++this._nrElements;
                }
                p._entries[parentIndex].element = p._entries[parentIndex + 1].element;
                if (p._nrElements == this.MIN && p != BTreeSet.this.root) {
                    int x = parentIndex + 1;
                    for (int y = parentIndex; y >= 0; --y) {
                        p._entries[x] = p._entries[y];
                        --x;
                    }
                    p._entries[0] = new Entry();
                    p._entries[0].child = rs;
                }
                else {
                    int x = parentIndex + 1;
                    for (int y = parentIndex + 2; y <= p._nrElements; ++y) {
                        p._entries[x] = p._entries[y];
                        ++x;
                    }
                    p._entries[p._nrElements] = null;
                }
                final BTreeNode bTreeNode = p;
                --bTreeNode._nrElements;
                if (p.isRoot() && p._nrElements == 0) {
                    BTreeSet.this.root = this;
                    this._parent = null;
                }
            }
            else {
                this._entries[this._nrElements].element = p._entries[parentIndex].element;
                ++this._nrElements;
                int x = this._nrElements + 1;
                for (int y = 0; y <= rs._nrElements; ++y) {
                    this._entries[x] = rs._entries[y];
                    rs._entries[y].child._parent = this;
                    ++this._nrElements;
                    ++x;
                }
                --this._nrElements;
                p._entries[++parentIndex].child = this;
                if (p._nrElements == this.MIN && p != BTreeSet.this.root) {
                    x = parentIndex - 1;
                    for (int y = parentIndex - 2; y >= 0; --y) {
                        p._entries[x] = p._entries[y];
                        --x;
                    }
                    p._entries[0] = new Entry();
                }
                else {
                    x = parentIndex - 1;
                    for (int y = parentIndex; y <= p._nrElements; ++y) {
                        p._entries[x] = p._entries[y];
                        ++x;
                    }
                    p._entries[p._nrElements] = null;
                }
                final BTreeNode bTreeNode2 = p;
                --bTreeNode2._nrElements;
                if (p.isRoot() && p._nrElements == 0) {
                    BTreeSet.this.root = this;
                    this._parent = null;
                }
            }
        }
    }
}
