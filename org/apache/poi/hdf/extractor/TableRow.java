// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

import java.util.ArrayList;

@Deprecated
public final class TableRow
{
    TAP _descriptor;
    ArrayList _cells;
    
    public TableRow(final ArrayList cells, final TAP descriptor) {
        this._cells = cells;
        this._descriptor = descriptor;
    }
    
    public TAP getTAP() {
        return this._descriptor;
    }
    
    public ArrayList getCells() {
        return this._cells;
    }
}
