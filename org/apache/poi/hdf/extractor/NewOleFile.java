// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Vector;
import java.util.Hashtable;
import java.io.RandomAccessFile;

@Deprecated
public final class NewOleFile extends RandomAccessFile
{
    private byte[] LAOLA_ID_ARRAY;
    private int _num_bbd_blocks;
    private int _root_startblock;
    private int _sbd_startblock;
    private long _size;
    private int[] _bbd_list;
    protected int[] _big_block_depot;
    protected int[] _small_block_depot;
    Hashtable _propertySetsHT;
    Vector _propertySetsV;
    
    public NewOleFile(final String fileName, final String mode) throws FileNotFoundException {
        super(fileName, mode);
        this.LAOLA_ID_ARRAY = new byte[] { -48, -49, 17, -32, -95, -79, 26, -31 };
        this._propertySetsHT = new Hashtable();
        this._propertySetsV = new Vector();
        try {
            this.init();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    private void init() throws IOException {
        for (int x = 0; x < this.LAOLA_ID_ARRAY.length; ++x) {
            if (this.LAOLA_ID_ARRAY[x] != this.readByte()) {
                throw new IOException("Not an OLE file");
            }
        }
        this._size = this.length();
        this._num_bbd_blocks = this.readInt(44L);
        this._root_startblock = this.readInt(48L);
        this._sbd_startblock = this.readInt(60L);
        this._bbd_list = new int[this._num_bbd_blocks];
        if (this._num_bbd_blocks <= 109) {
            this.seek(76L);
            for (int x = 0; x < this._num_bbd_blocks; ++x) {
                this._bbd_list[x] = this.readIntLE();
            }
        }
        else {
            this.populateBbdList();
        }
        this._big_block_depot = new int[this._num_bbd_blocks * 128];
        int counter = 0;
        for (int x2 = 0; x2 < this._num_bbd_blocks; ++x2) {
            final byte[] bigBlock = new byte[512];
            final int offset = (this._bbd_list[x2] + 1) * 512;
            this.seek(offset);
            for (int y = 0; y < 128; ++y) {
                this._big_block_depot[counter++] = this.readIntLE();
            }
        }
        this._small_block_depot = this.createSmallBlockDepot();
        final int[] rootChain = this.readChain(this._big_block_depot, this._root_startblock);
        this.initializePropertySets(rootChain);
    }
    
    public static void main(final String[] args) {
        try {
            final NewOleFile file = new NewOleFile(args[0], "r");
        }
        catch (Exception ex) {}
    }
    
    protected int[] readChain(final int[] blockChain, final int startBlock) {
        final int[] tempChain = new int[blockChain.length];
        tempChain[0] = startBlock;
        int x = 1;
        while (true) {
            final int nextVal = blockChain[tempChain[x - 1]];
            if (nextVal == -2) {
                break;
            }
            tempChain[x] = nextVal;
            ++x;
        }
        final int[] newChain = new int[x];
        System.arraycopy(tempChain, 0, newChain, 0, x);
        return newChain;
    }
    
    private void initializePropertySets(final int[] rootChain) throws IOException {
        for (int x = 0; x < rootChain.length; ++x) {
            final int offset = (rootChain[x] + 1) * 512;
            this.seek(offset);
            for (int y = 0; y < 4; ++y) {
                final byte[] propArray = new byte[128];
                this.read(propArray);
                final int nameSize = Utils.convertBytesToShort(propArray[65], propArray[64]) / 2 - 1;
                if (nameSize > 0) {
                    final StringBuffer nameBuffer = new StringBuffer(nameSize);
                    for (int z = 0; z < nameSize; ++z) {
                        nameBuffer.append((char)propArray[z * 2]);
                    }
                    final int type = propArray[66];
                    final int previous_pps = Utils.convertBytesToInt(propArray[71], propArray[70], propArray[69], propArray[68]);
                    final int next_pps = Utils.convertBytesToInt(propArray[75], propArray[74], propArray[73], propArray[72]);
                    final int pps_dir = Utils.convertBytesToInt(propArray[79], propArray[78], propArray[77], propArray[76]);
                    final int pps_sb = Utils.convertBytesToInt(propArray[119], propArray[118], propArray[117], propArray[116]);
                    final int pps_size = Utils.convertBytesToInt(propArray[123], propArray[122], propArray[121], propArray[120]);
                    final PropertySet propSet = new PropertySet(nameBuffer.toString(), type, previous_pps, next_pps, pps_dir, pps_sb, pps_size, x * 4 + y);
                    this._propertySetsHT.put(nameBuffer.toString(), propSet);
                    this._propertySetsV.add(propSet);
                }
            }
        }
    }
    
    private int[] createSmallBlockDepot() throws IOException {
        final int[] sbd_list = this.readChain(this._big_block_depot, this._sbd_startblock);
        final int[] small_block_depot = new int[sbd_list.length * 128];
        for (int x = 0; x < sbd_list.length && sbd_list[x] != -2; ++x) {
            final int offset = (sbd_list[x] + 1) * 512;
            this.seek(offset);
            for (int y = 0; y < 128; ++y) {
                small_block_depot[y] = this.readIntLE();
            }
        }
        return small_block_depot;
    }
    
    private void populateBbdList() throws IOException {
        this.seek(76L);
        for (int x = 0; x < 109; ++x) {
            this._bbd_list[x] = this.readIntLE();
        }
        int pos = 109;
        int remainder = this._num_bbd_blocks - 109;
        this.seek(72L);
        final int numLists = this.readIntLE();
        this.seek(68L);
        int firstList = this.readIntLE();
        firstList = (firstList + 1) * 512;
        for (int y = 0; y < numLists; ++y) {
            final int size = Math.min(127, remainder);
            for (int z = 0; z < size; ++z) {
                this.seek(firstList + z * 4);
                this._bbd_list[pos++] = this.readIntLE();
            }
            if (size == 127) {
                this.seek(firstList + 508);
                firstList = this.readIntLE();
                firstList = (firstList + 1) * 512;
                remainder -= 127;
            }
        }
    }
    
    private int readInt(final long offset) throws IOException {
        this.seek(offset);
        return this.readIntLE();
    }
    
    private int readIntLE() throws IOException {
        final byte[] intBytes = new byte[4];
        this.read(intBytes);
        return Utils.convertBytesToInt(intBytes[3], intBytes[2], intBytes[1], intBytes[0]);
    }
}
