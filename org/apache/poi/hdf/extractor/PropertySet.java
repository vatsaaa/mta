// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class PropertySet
{
    private String _name;
    private int _type;
    private int _previous;
    private int _next;
    private int _dir;
    private int _sb;
    private int _size;
    private int _num;
    
    public PropertySet(final String name, final int type, final int previous, final int next, final int dir, final int sb, final int size, final int num) {
        this._name = name;
        this._type = type;
        this._previous = previous;
        this._next = next;
        this._dir = dir;
        this._sb = sb;
        this._size = size;
        this._num = num;
    }
    
    public int getSize() {
        return this._size;
    }
    
    public int getStartBlock() {
        return this._sb;
    }
}
