// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor.data;

import org.apache.poi.hdf.extractor.StyleSheet;
import org.apache.poi.hdf.extractor.Utils;
import java.util.Hashtable;

@Deprecated
public final class ListTables
{
    LFO[] _pllfo;
    Hashtable _lists;
    
    public ListTables(final byte[] plcflst, final byte[] plflfo) {
        this._lists = new Hashtable();
        this.initLST(plcflst);
        this.initLFO(plflfo);
    }
    
    public LVL getLevel(final int list, final int level) {
        final LFO override = this._pllfo[list - 1];
        for (int x = 0; x < override._clfolvl; ++x) {
            if (override._levels[x]._ilvl == level) {
                final LFOLVL lfolvl = override._levels[x];
                if (lfolvl._fFormatting) {
                    final LST lst = this._lists.get(override._lsid);
                    final LVL lvl = lfolvl._override;
                    lvl._istd = Utils.convertBytesToShort(lst._rgistd, level * 2);
                    return lvl;
                }
                if (lfolvl._fStartAt) {
                    final LST lst = this._lists.get(override._lsid);
                    final LVL lvl = lst._levels[level];
                    final LVL newLvl = (LVL)lvl.clone();
                    newLvl._istd = Utils.convertBytesToShort(lst._rgistd, level * 2);
                    newLvl._iStartAt = lfolvl._iStartAt;
                    return newLvl;
                }
            }
        }
        final LST lst2 = this._lists.get(override._lsid);
        final LVL lvl2 = lst2._levels[level];
        lvl2._istd = Utils.convertBytesToShort(lst2._rgistd, level * 2);
        return lvl2;
    }
    
    private void initLST(final byte[] plcflst) {
        final short length = Utils.convertBytesToShort(plcflst, 0);
        int nextLevelOffset = 0;
        for (int x = 0; x < length; ++x) {
            final LST lst = new LST();
            lst._lsid = Utils.convertBytesToInt(plcflst, 2 + x * 28);
            lst._tplc = Utils.convertBytesToInt(plcflst, 6 + x * 28);
            System.arraycopy(plcflst, 10 + x * 28, lst._rgistd, 0, 18);
            final byte code = plcflst[28 + x * 28];
            lst._fSimpleList = StyleSheet.getFlag(code & 0x1);
            this._lists.put(lst._lsid, lst);
            if (lst._fSimpleList) {
                lst._levels = new LVL[1];
            }
            else {
                lst._levels = new LVL[9];
            }
            for (int y = 0; y < lst._levels.length; ++y) {
                final int offset = 2 + length * 28 + nextLevelOffset;
                lst._levels[y] = new LVL();
                nextLevelOffset += this.createLVL(plcflst, offset, lst._levels[y]);
            }
        }
    }
    
    private void initLFO(final byte[] plflfo) {
        final int lfoSize = Utils.convertBytesToInt(plflfo, 0);
        this._pllfo = new LFO[lfoSize];
        for (int x = 0; x < lfoSize; ++x) {
            final LFO nextLFO = new LFO();
            nextLFO._lsid = Utils.convertBytesToInt(plflfo, 4 + x * 16);
            nextLFO._clfolvl = plflfo[16 + x * 16];
            nextLFO._levels = new LFOLVL[nextLFO._clfolvl];
            this._pllfo[x] = nextLFO;
        }
        final int lfolvlOffset = lfoSize * 16 + 4;
        int lvlOffset = 0;
        int lfolvlNum = 0;
        for (int x2 = 0; x2 < lfoSize; ++x2) {
            for (int y = 0; y < this._pllfo[x2]._clfolvl; ++y) {
                int offset = lfolvlOffset + lfolvlNum * 8 + lvlOffset;
                final LFOLVL lfolvl = new LFOLVL();
                lfolvl._iStartAt = Utils.convertBytesToInt(plflfo, offset);
                lfolvl._ilvl = Utils.convertBytesToInt(plflfo, offset + 4);
                lfolvl._fStartAt = StyleSheet.getFlag(lfolvl._ilvl & 0x10);
                lfolvl._fFormatting = StyleSheet.getFlag(lfolvl._ilvl & 0x20);
                lfolvl._ilvl &= 0xF;
                ++lfolvlNum;
                if (lfolvl._fFormatting) {
                    offset = lfolvlOffset + lfolvlNum * 12 + lvlOffset;
                    lfolvl._override = new LVL();
                    lvlOffset += this.createLVL(plflfo, offset, lfolvl._override);
                }
                this._pllfo[x2]._levels[y] = lfolvl;
            }
        }
    }
    
    private int createLVL(final byte[] data, int offset, final LVL lvl) {
        lvl._iStartAt = Utils.convertBytesToInt(data, offset);
        lvl._nfc = data[offset + 4];
        final int code = Utils.convertBytesToInt(data, offset + 5);
        lvl._jc = (byte)(code & 0x3);
        lvl._fLegal = StyleSheet.getFlag(code & 0x4);
        lvl._fNoRestart = StyleSheet.getFlag(code & 0x8);
        lvl._fPrev = StyleSheet.getFlag(code & 0x10);
        lvl._fPrevSpace = StyleSheet.getFlag(code & 0x20);
        lvl._fWord6 = StyleSheet.getFlag(code & 0x40);
        System.arraycopy(data, offset + 6, lvl._rgbxchNums, 0, 9);
        lvl._ixchFollow = data[offset + 15];
        final int chpxSize = data[offset + 24];
        final int papxSize = data[offset + 25];
        lvl._chpx = new byte[chpxSize];
        System.arraycopy(data, offset + 28, lvl._papx = new byte[papxSize], 0, papxSize);
        System.arraycopy(data, offset + 28 + papxSize, lvl._chpx, 0, chpxSize);
        offset += 28 + papxSize + chpxSize;
        final int xstSize = Utils.convertBytesToShort(data, offset);
        lvl._xst = new char[xstSize];
        offset += 2;
        for (int x = 0; x < xstSize; ++x) {
            lvl._xst[x] = (char)Utils.convertBytesToShort(data, offset + x * 2);
        }
        return 28 + papxSize + chpxSize + 2 + xstSize * 2;
    }
}
