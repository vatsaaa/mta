// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class CHP implements Cloneable
{
    boolean _bold;
    boolean _italic;
    boolean _fRMarkDel;
    boolean _fOutline;
    boolean _fSmallCaps;
    boolean _fCaps;
    boolean _fVanish;
    boolean _fRMark;
    boolean _fSpec;
    boolean _fStrike;
    boolean _fObj;
    boolean _fShadow;
    boolean _fLowerCase;
    boolean _fData;
    boolean _fOle2;
    boolean _fEmboss;
    boolean _fImprint;
    boolean _fDStrike;
    short _ftcAscii;
    short _ftcFE;
    short _ftcOther;
    short _ftc;
    int _hps;
    int _dxaSpace;
    byte _iss;
    byte _kul;
    byte _ico;
    short _hpsPos;
    short _lidDefault;
    short _lidFE;
    byte _idctHint;
    int _wCharScale;
    short _chse;
    int _specialFC;
    short _ibstRMark;
    short _ibstRMarkDel;
    int[] _dttmRMark;
    int[] _dttmRMarkDel;
    int _istd;
    int _baseIstd;
    int _fcPic;
    short _ftcSym;
    short _xchSym;
    byte _ysr;
    byte _chYsr;
    int _hpsKern;
    int _fcObj;
    byte _icoHighlight;
    boolean _fChsDiff;
    boolean _highlighted;
    boolean _fPropMark;
    short _ibstPropRMark;
    int _dttmPropRMark;
    byte _sfxtText;
    boolean _fDispFldRMark;
    short _ibstDispFldRMark;
    int _dttmDispFldRMark;
    byte[] _xstDispFldRMark;
    short _shd;
    short[] _brc;
    short _paddingStart;
    short _paddingEnd;
    
    public CHP() {
        this._dttmRMark = new int[2];
        this._dttmRMarkDel = new int[2];
        this._baseIstd = -1;
        this._xstDispFldRMark = new byte[32];
        this._brc = new short[2];
        this._paddingStart = 0;
        this._paddingEnd = 0;
        this._istd = 10;
        this._hps = 20;
        this._lidDefault = 1024;
        this._lidFE = 1024;
    }
    
    public void copy(final CHP toCopy) {
        this._bold = toCopy._bold;
        this._italic = toCopy._italic;
        this._fRMarkDel = toCopy._fRMarkDel;
        this._fOutline = toCopy._fOutline;
        this._fSmallCaps = toCopy._fSmallCaps;
        this._fCaps = toCopy._fCaps;
        this._fVanish = toCopy._fVanish;
        this._fRMark = toCopy._fRMark;
        this._fSpec = toCopy._fSpec;
        this._fStrike = toCopy._fStrike;
        this._fObj = toCopy._fObj;
        this._fShadow = toCopy._fShadow;
        this._fLowerCase = toCopy._fLowerCase;
        this._fData = toCopy._fData;
        this._fOle2 = toCopy._fOle2;
        this._fEmboss = toCopy._fEmboss;
        this._fImprint = toCopy._fImprint;
        this._fDStrike = toCopy._fDStrike;
        this._ftcAscii = toCopy._ftcAscii;
        this._ftcFE = toCopy._ftcFE;
        this._ftcOther = toCopy._ftcOther;
        this._ftc = toCopy._ftc;
        this._hps = toCopy._hps;
        this._dxaSpace = toCopy._dxaSpace;
        this._iss = toCopy._iss;
        this._kul = toCopy._kul;
        this._ico = toCopy._ico;
        this._hpsPos = toCopy._hpsPos;
        this._lidDefault = toCopy._lidDefault;
        this._lidFE = toCopy._lidFE;
        this._idctHint = toCopy._idctHint;
        this._wCharScale = toCopy._wCharScale;
        this._chse = toCopy._chse;
        this._specialFC = toCopy._specialFC;
        this._ibstRMark = toCopy._ibstRMark;
        this._ibstRMarkDel = toCopy._ibstRMarkDel;
        this._dttmRMark = toCopy._dttmRMark;
        this._dttmRMarkDel = toCopy._dttmRMarkDel;
        this._istd = toCopy._istd;
        this._baseIstd = toCopy._baseIstd;
        this._fcPic = toCopy._fcPic;
        this._ftcSym = toCopy._ftcSym;
        this._xchSym = toCopy._xchSym;
        this._ysr = toCopy._ysr;
        this._chYsr = toCopy._chYsr;
        this._hpsKern = toCopy._hpsKern;
        this._fcObj = toCopy._fcObj;
        this._icoHighlight = toCopy._icoHighlight;
        this._fChsDiff = toCopy._fChsDiff;
        this._highlighted = toCopy._highlighted;
        this._fPropMark = toCopy._fPropMark;
        this._ibstPropRMark = toCopy._ibstPropRMark;
        this._dttmPropRMark = toCopy._dttmPropRMark;
        this._sfxtText = toCopy._sfxtText;
        this._fDispFldRMark = toCopy._fDispFldRMark;
        this._ibstDispFldRMark = toCopy._ibstDispFldRMark;
        this._dttmDispFldRMark = toCopy._dttmDispFldRMark;
        this._xstDispFldRMark = toCopy._xstDispFldRMark;
        this._shd = toCopy._shd;
        this._brc = toCopy._brc;
    }
    
    public Object clone() throws CloneNotSupportedException {
        final CHP clone = (CHP)super.clone();
        clone._brc = new short[2];
        System.arraycopy(this._brc, 0, clone._brc, 0, 2);
        return clone;
    }
}
