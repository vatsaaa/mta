// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class FontTable
{
    String[] fontNames;
    
    public FontTable(final byte[] fontTable) {
        final int size = Utils.convertBytesToShort(fontTable, 0);
        this.fontNames = new String[size];
        int currentIndex = 4;
        for (int x = 0; x < size; ++x) {
            final byte ffnLength = fontTable[currentIndex];
            int nameOffset = currentIndex + 40;
            final StringBuffer nameBuf = new StringBuffer();
            for (char ch = Utils.getUnicodeCharacter(fontTable, nameOffset); ch != '\0'; ch = Utils.getUnicodeCharacter(fontTable, nameOffset)) {
                nameBuf.append(ch);
                nameOffset += 2;
            }
            this.fontNames[x] = nameBuf.toString();
            if (this.fontNames[x].startsWith("Times")) {
                this.fontNames[x] = "Times";
            }
            currentIndex += ffnLength + 1;
        }
    }
    
    public String getFont(final int index) {
        return this.fontNames[index];
    }
}
