// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class SEP
{
    int _index;
    byte _bkc;
    boolean _fTitlePage;
    boolean _fAutoPgn;
    byte _nfcPgn;
    boolean _fUnlocked;
    byte _cnsPgn;
    boolean _fPgnRestart;
    boolean _fEndNote;
    byte _lnc;
    byte _grpfIhdt;
    short _nLnnMod;
    int _dxaLnn;
    short _dxaPgn;
    short _dyaPgn;
    boolean _fLBetween;
    byte _vjc;
    short _dmBinFirst;
    short _dmBinOther;
    short _dmPaperReq;
    short[] _brcTop;
    short[] _brcLeft;
    short[] _brcBottom;
    short[] _brcRight;
    boolean _fPropMark;
    int _dxtCharSpace;
    int _dyaLinePitch;
    short _clm;
    byte _dmOrientPage;
    byte _iHeadingPgn;
    short _pgnStart;
    short _lnnMin;
    short _wTextFlow;
    short _pgbProp;
    int _xaPage;
    int _yaPage;
    int _dxaLeft;
    int _dxaRight;
    int _dyaTop;
    int _dyaBottom;
    int _dzaGutter;
    int _dyaHdrTop;
    int _dyaHdrBottom;
    short _ccolM1;
    boolean _fEvenlySpaced;
    int _dxaColumns;
    int[] _rgdxaColumnWidthSpacing;
    byte _dmOrientFirst;
    byte[] _olstAnn;
    
    public SEP() {
        this._brcTop = new short[2];
        this._brcLeft = new short[2];
        this._brcBottom = new short[2];
        this._brcRight = new short[2];
        this._bkc = 2;
        this._dyaPgn = 720;
        this._dxaPgn = 720;
        this._fEndNote = true;
        this._fEvenlySpaced = true;
        this._xaPage = 12240;
        this._yaPage = 15840;
        this._dyaHdrTop = 720;
        this._dyaHdrBottom = 720;
        this._dmOrientPage = 1;
        this._dxaColumns = 720;
        this._dyaTop = 1440;
        this._dxaLeft = 1800;
        this._dyaBottom = 1440;
        this._dxaRight = 1800;
        this._pgnStart = 1;
    }
}
