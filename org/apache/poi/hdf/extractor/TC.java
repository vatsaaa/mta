// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class TC
{
    boolean _fFirstMerged;
    boolean _fMerged;
    boolean _fVertical;
    boolean _fBackward;
    boolean _fRotateFont;
    boolean _fVertMerge;
    boolean _fVertRestart;
    short _vertAlign;
    short[] _brcTop;
    short[] _brcLeft;
    short[] _brcBottom;
    short[] _brcRight;
    
    public TC() {
        this._brcTop = new short[2];
        this._brcLeft = new short[2];
        this._brcBottom = new short[2];
        this._brcRight = new short[2];
    }
    
    static TC convertBytesToTC(final byte[] array, final int offset) {
        final TC tc = new TC();
        final int rgf = Utils.convertBytesToShort(array, offset);
        tc._fFirstMerged = ((rgf & 0x1) > 0);
        tc._fMerged = ((rgf & 0x2) > 0);
        tc._fVertical = ((rgf & 0x4) > 0);
        tc._fBackward = ((rgf & 0x8) > 0);
        tc._fRotateFont = ((rgf & 0x10) > 0);
        tc._fVertMerge = ((rgf & 0x20) > 0);
        tc._fVertRestart = ((rgf & 0x40) > 0);
        tc._vertAlign = (short)((rgf & 0x180) >> 7);
        tc._brcTop[0] = Utils.convertBytesToShort(array, offset + 4);
        tc._brcTop[1] = Utils.convertBytesToShort(array, offset + 6);
        tc._brcLeft[0] = Utils.convertBytesToShort(array, offset + 8);
        tc._brcLeft[1] = Utils.convertBytesToShort(array, offset + 10);
        tc._brcBottom[0] = Utils.convertBytesToShort(array, offset + 12);
        tc._brcBottom[1] = Utils.convertBytesToShort(array, offset + 14);
        tc._brcRight[0] = Utils.convertBytesToShort(array, offset + 16);
        tc._brcRight[1] = Utils.convertBytesToShort(array, offset + 18);
        return tc;
    }
}
