// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

@Deprecated
public final class Utils
{
    public static short convertBytesToShort(final byte firstByte, final byte secondByte) {
        return (short)convertBytesToInt((byte)0, (byte)0, firstByte, secondByte);
    }
    
    public static int convertBytesToInt(final byte firstByte, final byte secondByte, final byte thirdByte, final byte fourthByte) {
        final int firstInt = 0xFF & firstByte;
        final int secondInt = 0xFF & secondByte;
        final int thirdInt = 0xFF & thirdByte;
        final int fourthInt = 0xFF & fourthByte;
        return firstInt << 24 | secondInt << 16 | thirdInt << 8 | fourthInt;
    }
    
    public static short convertBytesToShort(final byte[] array, final int offset) {
        return convertBytesToShort(array[offset + 1], array[offset]);
    }
    
    public static int convertBytesToInt(final byte[] array, final int offset) {
        return convertBytesToInt(array[offset + 3], array[offset + 2], array[offset + 1], array[offset]);
    }
    
    public static int convertUnsignedByteToInt(final byte b) {
        return 0xFF & b;
    }
    
    public static char getUnicodeCharacter(final byte[] array, final int offset) {
        return (char)convertBytesToShort(array, offset);
    }
}
