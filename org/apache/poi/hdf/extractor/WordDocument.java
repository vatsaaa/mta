// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.extractor;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.util.Collection;
import org.apache.poi.hdf.extractor.util.PropertyNode;
import org.apache.poi.hdf.extractor.util.NumberFormatter;
import org.apache.poi.hdf.extractor.data.LVL;
import org.apache.poi.hdf.extractor.util.PapxNode;
import org.apache.poi.hdf.extractor.util.ChpxNode;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.hdf.extractor.util.SepxNode;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Writer;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.poi.hdf.extractor.util.BTreeSet;
import org.apache.poi.hdf.extractor.data.DOP;
import org.apache.poi.hdf.extractor.data.ListTables;

@Deprecated
public final class WordDocument
{
    private static final float K_1440_0F = 1440.0f;
    byte[] _header;
    StyleSheet _styleSheet;
    ListTables _listTables;
    DOP _docProps;
    int _currentList;
    int _tableSize;
    int _sectionCounter;
    FontTable _fonts;
    BTreeSet _text;
    BTreeSet _characterTable;
    BTreeSet _paragraphTable;
    BTreeSet _sectionTable;
    StringBuffer _headerBuffer;
    StringBuffer _bodyBuffer;
    StringBuffer _cellBuffer;
    ArrayList _cells;
    ArrayList _table;
    byte[] _plcfHdd;
    int _fcMin;
    int _ccpText;
    int _ccpFtn;
    private static String _outName;
    private InputStream istream;
    private POIFSFileSystem filesystem;
    private static int HEADER_EVEN_INDEX;
    private static int HEADER_ODD_INDEX;
    private static int FOOTER_EVEN_INDEX;
    private static int FOOTER_ODD_INDEX;
    private static int HEADER_FIRST_INDEX;
    private static int FOOTER_FIRST_INDEX;
    
    public static void main(final String[] args) {
        try {
            WordDocument._outName = args[1];
            final WordDocument file = new WordDocument(args[0]);
            file.closeDoc();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
    
    public void writeAllText(final Writer out) throws IOException {
        final int textStart = Utils.convertBytesToInt(this._header, 24);
        final int textEnd = Utils.convertBytesToInt(this._header, 28);
        final ArrayList textPieces = this.findProperties(textStart, textEnd, this._text.root);
        for (int size = textPieces.size(), x = 0; x < size; ++x) {
            final TextPiece nextPiece = textPieces.get(x);
            final int start = nextPiece.getStart();
            final int end = nextPiece.getEnd();
            final boolean unicode = nextPiece.usesUnicode();
            int add = 1;
            if (unicode) {
                add = 2;
                for (int y = start; y < end; y += add) {
                    final char ch = (char)Utils.convertBytesToShort(this._header, y);
                    out.write(ch);
                }
            }
            else {
                final String sText = new String(this._header, start, end - start);
                out.write(sText);
            }
        }
    }
    
    public WordDocument(final String fileName) throws IOException {
        this(new FileInputStream(fileName));
    }
    
    public WordDocument(final InputStream inputStream) throws IOException {
        this._docProps = new DOP();
        this._currentList = -1;
        this._sectionCounter = 1;
        this._text = new BTreeSet();
        this._characterTable = new BTreeSet();
        this._paragraphTable = new BTreeSet();
        this._sectionTable = new BTreeSet();
        this._headerBuffer = new StringBuffer();
        this._bodyBuffer = new StringBuffer();
        this.istream = inputStream;
        this.filesystem = new POIFSFileSystem(this.istream);
        this.readFIB();
        final ArrayList sections = this.findProperties(this._fcMin, this._fcMin + this._ccpText, this._sectionTable.root);
        for (int size = sections.size(), x = 0; x < size; ++x) {
            final SepxNode node = sections.get(x);
            final int start = node.getStart();
            final int end = node.getEnd();
            final SEP sep = (SEP)StyleSheet.uncompressProperty(node.getSepx(), new SEP(), this._styleSheet);
            this.writeSection(Math.max(this._fcMin, start), Math.min(this._fcMin + this._ccpText, end), sep, this._text, this._paragraphTable, this._characterTable, this._styleSheet);
        }
        this.istream.close();
    }
    
    private void readFIB() throws IOException {
        final DocumentEntry headerProps = (DocumentEntry)this.filesystem.getRoot().getEntry("WordDocument");
        this._header = new byte[headerProps.getSize()];
        this.filesystem.createDocumentInputStream("WordDocument").read(this._header);
        final int info = LittleEndian.getShort(this._header, 10);
        this._fcMin = LittleEndian.getInt(this._header, 24);
        this._ccpText = LittleEndian.getInt(this._header, 76);
        this._ccpFtn = LittleEndian.getInt(this._header, 80);
        final int charPLC = LittleEndian.getInt(this._header, 250);
        final int charPlcSize = LittleEndian.getInt(this._header, 254);
        final int parPLC = LittleEndian.getInt(this._header, 258);
        final int parPlcSize = LittleEndian.getInt(this._header, 262);
        final boolean useTable1 = (info & 0x200) != 0x0;
        this.processComplexFile(useTable1, charPLC, charPlcSize, parPLC, parPlcSize);
    }
    
    private void processComplexFile(final boolean useTable1, final int charTable, final int charPlcSize, final int parTable, final int parPlcSize) throws IOException {
        final int complexOffset = LittleEndian.getInt(this._header, 418);
        String tablename = null;
        DocumentEntry tableEntry = null;
        if (useTable1) {
            tablename = "1Table";
        }
        else {
            tablename = "0Table";
        }
        tableEntry = (DocumentEntry)this.filesystem.getRoot().getEntry(tablename);
        final int size = tableEntry.getSize();
        final byte[] tableStream = new byte[size];
        this.filesystem.createDocumentInputStream(tablename).read(tableStream);
        this.initDocProperties(tableStream);
        this.initPclfHdd(tableStream);
        this.findText(tableStream, complexOffset);
        this.findFormatting(tableStream, charTable, charPlcSize, parTable, parPlcSize);
    }
    
    private void findText(final byte[] tableStream, final int complexOffset) throws IOException {
        int pos;
        int skip;
        for (pos = complexOffset; tableStream[pos] == 1; ++pos, skip = LittleEndian.getShort(tableStream, pos), pos += 2 + skip) {}
        if (tableStream[pos] != 2) {
            throw new IOException("corrupted Word file");
        }
        final int pieceTableSize = LittleEndian.getInt(tableStream, ++pos);
        pos += 4;
        for (int pieces = (pieceTableSize - 4) / 12, x = 0; x < pieces; ++x) {
            int filePos = LittleEndian.getInt(tableStream, pos + (pieces + 1) * 4 + x * 8 + 2);
            boolean unicode = false;
            if ((filePos & 0x40000000) == 0x0) {
                unicode = true;
            }
            else {
                unicode = false;
                filePos &= 0xBFFFFFFF;
                filePos /= 2;
            }
            final int totLength = LittleEndian.getInt(tableStream, pos + (x + 1) * 4) - LittleEndian.getInt(tableStream, pos + x * 4);
            final TextPiece piece = new TextPiece(filePos, totLength, unicode);
            this._text.add(piece);
        }
    }
    
    private void findFormatting(final byte[] tableStream, final int charOffset, final int charPlcSize, final int parOffset, final int parPlcSize) {
        this.openDoc();
        this.createStyleSheet(tableStream);
        this.createListTables(tableStream);
        this.createFontTable(tableStream);
        for (int arraySize = (charPlcSize - 4) / 8, x = 0; x < arraySize; ++x) {
            final int PN = LittleEndian.getInt(tableStream, charOffset + (4 * (arraySize + 1) + 4 * x));
            final byte[] fkp = new byte[512];
            System.arraycopy(this._header, PN * 512, fkp, 0, 512);
            for (int crun = Utils.convertUnsignedByteToInt(fkp[511]), y = 0; y < crun; ++y) {
                final int fcStart = LittleEndian.getInt(fkp, y * 4);
                final int fcEnd = LittleEndian.getInt(fkp, (y + 1) * 4);
                int chpxOffset = 2 * Utils.convertUnsignedByteToInt(fkp[(crun + 1) * 4 + y]);
                if (chpxOffset == 0) {
                    this._characterTable.add(new ChpxNode(fcStart, fcEnd, new byte[0]));
                }
                else {
                    final int size = Utils.convertUnsignedByteToInt(fkp[chpxOffset]);
                    final byte[] chpx = new byte[size];
                    System.arraycopy(fkp, ++chpxOffset, chpx, 0, size);
                    this._characterTable.add(new ChpxNode(fcStart, fcEnd, chpx));
                }
            }
        }
        for (int arraySize = (parPlcSize - 4) / 8, x = 0; x < arraySize; ++x) {
            final int PN = LittleEndian.getInt(tableStream, parOffset + (4 * (arraySize + 1) + 4 * x));
            final byte[] fkp = new byte[512];
            System.arraycopy(this._header, PN * 512, fkp, 0, 512);
            for (int crun = Utils.convertUnsignedByteToInt(fkp[511]), y = 0; y < crun; ++y) {
                final int fcStart = LittleEndian.getInt(fkp, y * 4);
                final int fcEnd = LittleEndian.getInt(fkp, (y + 1) * 4);
                int papxOffset = 2 * Utils.convertUnsignedByteToInt(fkp[(crun + 1) * 4 + y * 13]);
                int size = 2 * Utils.convertUnsignedByteToInt(fkp[papxOffset]);
                if (size == 0) {
                    size = 2 * Utils.convertUnsignedByteToInt(fkp[++papxOffset]);
                }
                else {
                    --size;
                }
                final byte[] papx = new byte[size];
                System.arraycopy(fkp, ++papxOffset, papx, 0, size);
                this._paragraphTable.add(new PapxNode(fcStart, fcEnd, papx));
            }
        }
        final int fcMin = Utils.convertBytesToInt(this._header, 24);
        final int plcfsedFC = Utils.convertBytesToInt(this._header, 202);
        final int plcfsedSize = Utils.convertBytesToInt(this._header, 206);
        final byte[] plcfsed = new byte[plcfsedSize];
        System.arraycopy(tableStream, plcfsedFC, plcfsed, 0, plcfsedSize);
        for (int arraySize = (plcfsedSize - 4) / 16, x2 = 0; x2 < arraySize; ++x2) {
            final int sectionStart = Utils.convertBytesToInt(plcfsed, x2 * 4) + fcMin;
            final int sectionEnd = Utils.convertBytesToInt(plcfsed, (x2 + 1) * 4) + fcMin;
            final int sepxStart = Utils.convertBytesToInt(plcfsed, 4 * (arraySize + 1) + x2 * 12 + 2);
            final int sepxSize = Utils.convertBytesToShort(this._header, sepxStart);
            final byte[] sepx = new byte[sepxSize];
            System.arraycopy(this._header, sepxStart + 2, sepx, 0, sepxSize);
            final SepxNode node = new SepxNode(x2 + 1, sectionStart, sectionEnd, sepx);
            this._sectionTable.add(node);
        }
    }
    
    public void openDoc() {
        this._headerBuffer.append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\r\n");
        this._headerBuffer.append("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\">\r\n");
        this._headerBuffer.append("<fo:layout-master-set>\r\n");
    }
    
    private HeaderFooter findSectionHdrFtr(final int type, final int index) {
        if (this._plcfHdd.length < 50) {
            return new HeaderFooter(0, 0, 0);
        }
        int end;
        int start = end = this._fcMin + this._ccpText + this._ccpFtn;
        int arrayIndex = 0;
        switch (type) {
            case 1: {
                arrayIndex = WordDocument.HEADER_EVEN_INDEX + index * 6;
                break;
            }
            case 3: {
                arrayIndex = WordDocument.FOOTER_EVEN_INDEX + index * 6;
                break;
            }
            case 2: {
                arrayIndex = WordDocument.HEADER_ODD_INDEX + index * 6;
                break;
            }
            case 4: {
                arrayIndex = WordDocument.FOOTER_ODD_INDEX + index * 6;
                break;
            }
            case 5: {
                arrayIndex = WordDocument.HEADER_FIRST_INDEX + index * 6;
                break;
            }
            case 6: {
                arrayIndex = WordDocument.FOOTER_FIRST_INDEX + index * 6;
                break;
            }
        }
        start += Utils.convertBytesToInt(this._plcfHdd, arrayIndex * 4);
        end += Utils.convertBytesToInt(this._plcfHdd, (arrayIndex + 1) * 4);
        HeaderFooter retValue = new HeaderFooter(type, start, end);
        if (end - start == 0 && index > 1) {
            retValue = this.findSectionHdrFtr(type, index - 1);
        }
        return retValue;
    }
    
    private void initDocProperties(final byte[] tableStream) {
        final int pos = LittleEndian.getInt(this._header, 402);
        final int size = LittleEndian.getInt(this._header, 406);
        final byte[] dop = new byte[size];
        System.arraycopy(tableStream, pos, dop, 0, size);
        this._docProps._fFacingPages = ((dop[0] & 0x1) > 0);
        this._docProps._fpc = (dop[0] & 0x60) >> 5;
        short num = LittleEndian.getShort(dop, 2);
        this._docProps._rncFtn = (num & 0x3);
        this._docProps._nFtn = (short)(num & 0xFFFC) >> 2;
        num = LittleEndian.getShort(dop, 52);
        this._docProps._rncEdn = (num & 0x3);
        this._docProps._nEdn = (short)(num & 0xFFFC) >> 2;
        num = LittleEndian.getShort(dop, 54);
        this._docProps._epc = (num & 0x3);
    }
    
    public void writeSection(final int start, final int end, final SEP sep, final BTreeSet text, final BTreeSet paragraphTable, final BTreeSet characterTable, final StyleSheet stylesheet) {
        final HeaderFooter titleHeader = this.findSectionHdrFtr(5, this._sectionCounter);
        final HeaderFooter titleFooter = this.findSectionHdrFtr(6, this._sectionCounter);
        final HeaderFooter oddHeader = this.findSectionHdrFtr(2, this._sectionCounter);
        final HeaderFooter evenHeader = this.findSectionHdrFtr(1, this._sectionCounter);
        final HeaderFooter oddFooter = this.findSectionHdrFtr(4, this._sectionCounter);
        final HeaderFooter evenFooter = this.findSectionHdrFtr(3, this._sectionCounter);
        String titlePage = null;
        String evenPage = null;
        String oddPage = null;
        String regPage = null;
        String sequenceName = null;
        if (this._docProps._fFacingPages) {
            if (sep._fTitlePage) {
                final String before = this.createRegion(true, titleHeader, sep, "title-header" + this._sectionCounter);
                final String after = this.createRegion(false, titleFooter, sep, "title-footer" + this._sectionCounter);
                titlePage = this.createPageMaster(sep, "first", this._sectionCounter, before, after);
            }
            String before = this.createRegion(true, evenHeader, sep, "even-header" + this._sectionCounter);
            String after = this.createRegion(false, evenFooter, sep, "even-footer" + this._sectionCounter);
            evenPage = this.createPageMaster(sep, "even", this._sectionCounter, before, after);
            before = this.createRegion(true, oddHeader, sep, "odd-header" + this._sectionCounter);
            after = this.createRegion(false, oddFooter, sep, "odd-footer" + this._sectionCounter);
            oddPage = this.createPageMaster(sep, "odd", this._sectionCounter, before, after);
            sequenceName = this.createEvenOddPageSequence(titlePage, evenPage, oddPage, this._sectionCounter);
            this.openPage(sequenceName, "reference");
            if (sep._fTitlePage) {
                if (!titleHeader.isEmpty()) {
                    this.addStaticContent("title-header" + this._sectionCounter, titleHeader);
                }
                if (!titleFooter.isEmpty()) {
                    this.addStaticContent("title-footer" + this._sectionCounter, titleFooter);
                }
            }
            if (!oddHeader.isEmpty()) {
                this.addStaticContent("odd-header" + this._sectionCounter, oddHeader);
            }
            if (!oddFooter.isEmpty()) {
                this.addStaticContent("odd-footer" + this._sectionCounter, oddFooter);
            }
            if (!evenHeader.isEmpty()) {
                this.addStaticContent("even-header" + this._sectionCounter, evenHeader);
            }
            if (!evenFooter.isEmpty()) {
                this.addStaticContent("even-footer" + this._sectionCounter, evenFooter);
            }
            this.openFlow();
            this.addBlockContent(start, end, text, paragraphTable, characterTable);
            this.closeFlow();
            this.closePage();
        }
        else {
            String before = this.createRegion(true, oddHeader, sep, null);
            String after = this.createRegion(false, oddFooter, sep, null);
            regPage = this.createPageMaster(sep, "page", this._sectionCounter, before, after);
            if (sep._fTitlePage) {
                before = this.createRegion(true, titleHeader, sep, "title-header" + this._sectionCounter);
                after = this.createRegion(false, titleFooter, sep, "title-footer" + this._sectionCounter);
                titlePage = this.createPageMaster(sep, "first", this._sectionCounter, before, after);
                sequenceName = this.createPageSequence(titlePage, regPage, this._sectionCounter);
                this.openPage(sequenceName, "reference");
                if (!titleHeader.isEmpty()) {
                    this.addStaticContent("title-header" + this._sectionCounter, titleHeader);
                }
                if (!titleFooter.isEmpty()) {
                    this.addStaticContent("title-footer" + this._sectionCounter, titleFooter);
                }
            }
            else {
                this.openPage(regPage, "name");
            }
            if (!oddHeader.isEmpty()) {
                this.addStaticContent("xsl-region-before", oddHeader);
            }
            if (!oddFooter.isEmpty()) {
                this.addStaticContent("xsl-region-after", oddFooter);
            }
            this.openFlow();
            this.addBlockContent(start, end, text, paragraphTable, characterTable);
            this.closeFlow();
            this.closePage();
        }
        ++this._sectionCounter;
    }
    
    private int calculateHeaderHeight(final int start, final int end, final int pageWidth) {
        final ArrayList paragraphs = this.findProperties(start, end, this._paragraphTable.root);
        int size = paragraphs.size();
        final ArrayList lineHeights = new ArrayList();
        for (int x = 0; x < size; ++x) {
            final PapxNode node = paragraphs.get(x);
            final int parStart = Math.max(node.getStart(), start);
            final int parEnd = Math.min(node.getEnd(), end);
            int lineWidth = 0;
            int maxHeight = 0;
            final ArrayList textRuns = this.findProperties(parStart, parEnd, this._characterTable.root);
            for (int charSize = textRuns.size(), y = 0; y < charSize; ++y) {
                final ChpxNode charNode = textRuns.get(y);
                final int istd = Utils.convertBytesToShort(node.getPapx(), 0);
                final StyleDescription sd = this._styleSheet.getStyleDescription(istd);
                final CHP chp = (CHP)StyleSheet.uncompressProperty(charNode.getChpx(), sd.getCHP(), this._styleSheet);
                final int height = 10;
                maxHeight = Math.max(maxHeight, height);
                final int charStart = Math.max(parStart, charNode.getStart());
                final int charEnd = Math.min(parEnd, charNode.getEnd());
                final ArrayList text = this.findProperties(charStart, charEnd, this._text.root);
                final int textSize = text.size();
                final StringBuffer buf = new StringBuffer();
                for (int z = 0; z < textSize; ++z) {
                    final TextPiece piece = text.get(z);
                    final int textStart = Math.max(piece.getStart(), charStart);
                    final int textEnd = Math.min(piece.getEnd(), charEnd);
                    if (piece.usesUnicode()) {
                        this.addUnicodeText(textStart, textEnd, buf);
                    }
                    else {
                        this.addText(textStart, textEnd, buf);
                    }
                }
                final String tempString = buf.toString();
                lineWidth += 10 * tempString.length();
                if (lineWidth > pageWidth) {
                    lineHeights.add(maxHeight);
                    maxHeight = 0;
                    lineWidth = 0;
                }
            }
            lineHeights.add(maxHeight);
        }
        int sum = 0;
        size = lineHeights.size();
        for (int x2 = 0; x2 < size; ++x2) {
            final Integer height2 = lineHeights.get(x2);
            sum += height2;
        }
        return sum;
    }
    
    private String createRegion(final boolean before, final HeaderFooter header, final SEP sep, final String name) {
        if (header.isEmpty()) {
            return "";
        }
        String region = "region-name=\"" + name + "\"";
        if (name == null) {
            region = "";
        }
        final int height = this.calculateHeaderHeight(header.getStart(), header.getEnd(), sep._xaPage / 20);
        int marginTop = 0;
        int marginBottom = 0;
        int extent = 0;
        String where = null;
        String align = null;
        if (before) {
            where = "before";
            align = "before";
            marginTop = sep._dyaHdrTop / 20;
            extent = height + marginTop;
            sep._dyaTop = Math.max(extent * 20, sep._dyaTop);
        }
        else {
            where = "after";
            align = "after";
            marginBottom = sep._dyaHdrBottom / 20;
            extent = height + marginBottom;
            sep._dyaBottom = Math.max(extent * 20, sep._dyaBottom);
        }
        final int marginLeft = sep._dxaLeft / 20;
        final int marginRight = sep._dxaRight / 20;
        return "<fo:region-" + where + " display-align=\"" + align + "\" extent=\"" + extent + "pt\" " + region + "/>";
    }
    
    private String createRegion(final String where, final String name) {
        return "<fo:region-" + where + " overflow=\"scroll\" region-name=\"" + name + "\"/>";
    }
    
    private String createEvenOddPageSequence(final String titlePage, final String evenPage, final String oddPage, final int counter) {
        final String name = "my-sequence" + counter;
        this._headerBuffer.append("<fo:page-sequence-master master-name=\"" + name + "\"> ");
        this._headerBuffer.append("<fo:repeatable-page-master-alternatives>");
        if (titlePage != null) {
            this._headerBuffer.append("<fo:conditional-page-master-reference page-position=\"first\" master-reference=\"" + titlePage + "\"/>");
        }
        this._headerBuffer.append("<fo:conditional-page-master-reference odd-or-even=\"odd\" ");
        this._headerBuffer.append("master-reference=\"" + oddPage + "\"/> ");
        this._headerBuffer.append("<fo:conditional-page-master-reference odd-or-even=\"even\" ");
        this._headerBuffer.append("master-reference=\"" + evenPage + "\"/> ");
        this._headerBuffer.append("</fo:repeatable-page-master-alternatives>");
        this._headerBuffer.append("</fo:page-sequence-master>");
        return name;
    }
    
    private String createPageSequence(final String titlePage, final String regPage, final int counter) {
        String name = null;
        if (titlePage != null) {
            name = "my-sequence" + counter;
            this._headerBuffer.append("<fo:page-sequence-master master-name=\"" + name + "\"> ");
            this._headerBuffer.append("<fo:single-page-master-reference master-reference=\"" + titlePage + "\"/>");
            this._headerBuffer.append("<fo:repeatable-page-master-reference master-reference=\"" + regPage + "\"/>");
            this._headerBuffer.append("</fo:page-sequence-master>");
        }
        return name;
    }
    
    private void addBlockContent(final int start, final int end, final BTreeSet text, final BTreeSet paragraphTable, final BTreeSet characterTable) {
        final BTreeSet.BTreeNode root = paragraphTable.root;
        final ArrayList pars = this.findProperties(start, end, root);
        for (int size = pars.size(), c = 0; c < size; ++c) {
            final PapxNode currentNode = pars.get(c);
            this.createParagraph(start, end, currentNode, characterTable, text);
        }
    }
    
    private String getTextAlignment(final byte jc) {
        switch (jc) {
            case 0: {
                return "start";
            }
            case 1: {
                return "center";
            }
            case 2: {
                return "end";
            }
            case 3: {
                return "justify";
            }
            default: {
                return "left";
            }
        }
    }
    
    private void createParagraph(final int start, final int end, final PapxNode currentNode, final BTreeSet characterTable, final BTreeSet text) {
        StringBuffer blockBuffer = this._bodyBuffer;
        final byte[] papx = currentNode.getPapx();
        final int istd = Utils.convertBytesToShort(papx, 0);
        final StyleDescription std = this._styleSheet.getStyleDescription(istd);
        final PAP pap = (PAP)StyleSheet.uncompressProperty(papx, std.getPAP(), this._styleSheet);
        if (pap._fInTable > 0) {
            if (pap._fTtp != 0) {
                if (this._table == null) {
                    this._table = new ArrayList();
                }
                final TAP tap = (TAP)StyleSheet.uncompressProperty(papx, new TAP(), this._styleSheet);
                final TableRow nextRow = new TableRow(this._cells, tap);
                this._table.add(nextRow);
                this._cells = null;
                return;
            }
            if (this._cellBuffer == null) {
                this._cellBuffer = new StringBuffer();
            }
            blockBuffer = this._cellBuffer;
        }
        else {
            this.printTable();
        }
        if (pap._ilfo > 0) {
            final LVL lvl = this._listTables.getLevel(pap._ilfo, pap._ilvl);
            this.addListParagraphContent(lvl, blockBuffer, pap, currentNode, start, end, std);
        }
        else {
            this.addParagraphContent(blockBuffer, pap, currentNode, start, end, std);
        }
    }
    
    private void addListParagraphContent(final LVL lvl, final StringBuffer blockBuffer, PAP pap, final PapxNode currentNode, final int start, final int end, final StyleDescription std) {
        pap = (PAP)StyleSheet.uncompressProperty(lvl._papx, pap, this._styleSheet, false);
        this.addParagraphProperties(pap, blockBuffer);
        final ArrayList charRuns = this.findProperties(Math.max(currentNode.getStart(), start), Math.min(currentNode.getEnd(), end), this._characterTable.root);
        final int len = charRuns.size();
        CHP numChp = (CHP)StyleSheet.uncompressProperty(charRuns.get(len - 1).getChpx(), std.getCHP(), this._styleSheet);
        numChp = (CHP)StyleSheet.uncompressProperty(lvl._chpx, numChp, this._styleSheet);
        int indent = -1 * pap._dxaLeft1;
        final String bulletText = this.getBulletText(lvl, pap);
        indent -= bulletText.length() * 10 * 20;
        if (indent > 0) {
            numChp._paddingEnd = (short)indent;
        }
        this.addCharacterProperties(numChp, blockBuffer);
        final int listNum = 0;
        blockBuffer.append(bulletText);
        switch (lvl._ixchFollow) {
            case 0: {
                this.addText('\t', blockBuffer);
                break;
            }
            case 1: {
                this.addText(' ', blockBuffer);
                break;
            }
        }
        this.closeLine(blockBuffer);
        for (int x = 0; x < len; ++x) {
            final ChpxNode charNode = charRuns.get(x);
            final byte[] chpx = charNode.getChpx();
            final CHP chp = (CHP)StyleSheet.uncompressProperty(chpx, std.getCHP(), this._styleSheet);
            this.addCharacterProperties(chp, blockBuffer);
            int charStart = Math.max(charNode.getStart(), currentNode.getStart());
            int charEnd = Math.min(charNode.getEnd(), currentNode.getEnd());
            final ArrayList textRuns = this.findProperties(charStart, charEnd, this._text.root);
            for (int textRunLen = textRuns.size(), y = 0; y < textRunLen; ++y) {
                final TextPiece piece = textRuns.get(y);
                charStart = Math.max(charStart, piece.getStart());
                charEnd = Math.min(charEnd, piece.getEnd());
                if (piece.usesUnicode()) {
                    this.addUnicodeText(charStart, charEnd, blockBuffer);
                }
                else {
                    this.addText(charStart, charEnd, blockBuffer);
                }
                this.closeLine(blockBuffer);
            }
        }
        this.closeBlock(blockBuffer);
    }
    
    private void addParagraphContent(final StringBuffer blockBuffer, final PAP pap, final PapxNode currentNode, final int start, final int end, final StyleDescription std) {
        this.addParagraphProperties(pap, blockBuffer);
        final ArrayList charRuns = this.findProperties(Math.max(currentNode.getStart(), start), Math.min(currentNode.getEnd(), end), this._characterTable.root);
        for (int len = charRuns.size(), x = 0; x < len; ++x) {
            final ChpxNode charNode = charRuns.get(x);
            final byte[] chpx = charNode.getChpx();
            final CHP chp = (CHP)StyleSheet.uncompressProperty(chpx, std.getCHP(), this._styleSheet);
            this.addCharacterProperties(chp, blockBuffer);
            int charStart = Math.max(charNode.getStart(), currentNode.getStart());
            int charEnd = Math.min(charNode.getEnd(), currentNode.getEnd());
            final ArrayList textRuns = this.findProperties(charStart, charEnd, this._text.root);
            for (int textRunLen = textRuns.size(), y = 0; y < textRunLen; ++y) {
                final TextPiece piece = textRuns.get(y);
                charStart = Math.max(charStart, piece.getStart());
                charEnd = Math.min(charEnd, piece.getEnd());
                if (piece.usesUnicode()) {
                    this.addUnicodeText(charStart, charEnd, blockBuffer);
                }
                else {
                    this.addText(charStart, charEnd, blockBuffer);
                }
                this.closeLine(blockBuffer);
            }
        }
        this.closeBlock(blockBuffer);
    }
    
    private void addText(final int start, final int end, final StringBuffer buf) {
        for (int x = start; x < end; ++x) {
            char ch = '?';
            ch = (char)this._header[x];
            this.addText(ch, buf);
        }
    }
    
    private void addText(final char ch, final StringBuffer buf) {
        int num = '\uffff' & ch;
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '_' || ch == ' ' || ch == '-' || ch == '.' || ch == '$') {
            buf.append(ch);
        }
        else if (num == 7 && this._cellBuffer != null) {
            if (this._cells == null) {
                this._cells = new ArrayList();
            }
            this.closeLine(this._cellBuffer);
            this.closeBlock(this._cellBuffer);
            this._cells.add(this._cellBuffer.toString());
            this._cellBuffer = null;
        }
        else {
            if (num < 32) {
                num = 32;
            }
            buf.append("&#");
            buf.append(num);
            buf.append(';');
        }
    }
    
    private void addUnicodeText(final int start, final int end, final StringBuffer buf) {
        for (int x = start; x < end; x += 2) {
            final char ch = Utils.getUnicodeCharacter(this._header, x);
            this.addText(ch, buf);
        }
    }
    
    private void addParagraphProperties(final PAP pap, final StringBuffer buf) {
        buf.append("<fo:block ");
        buf.append("text-align=\"" + this.getTextAlignment(pap._jc) + "\"\r\n");
        buf.append("linefeed-treatment=\"preserve\" ");
        buf.append("white-space-collapse=\"false\" ");
        if (pap._fKeep > 0) {
            buf.append("keep-together.within-page=\"always\"\r\n");
        }
        if (pap._fKeepFollow > 0) {
            buf.append("keep-with-next.within-page=\"always\"\r\n");
        }
        if (pap._fPageBreakBefore > 0) {
            buf.append("break-before=\"page\"\r\n");
        }
        if (pap._fNoAutoHyph == 0) {
            buf.append("hyphenate=\"true\"\r\n");
        }
        else {
            buf.append("hyphenate=\"false\"\r\n");
        }
        if (pap._dxaLeft > 0) {
            buf.append("start-indent=\"" + pap._dxaLeft / 1440.0f + "in\"\r\n");
        }
        if (pap._dxaRight > 0) {
            buf.append("end-indent=\"" + pap._dxaRight / 1440.0f + "in\"\r\n");
        }
        if (pap._dxaLeft1 != 0) {
            buf.append("text-indent=\"" + pap._dxaLeft1 / 1440.0f + "in\"\r\n");
        }
        if (pap._lspd[1] == 0) {}
        this.addBorder(buf, pap._brcTop, "top");
        this.addBorder(buf, pap._brcBottom, "bottom");
        this.addBorder(buf, pap._brcLeft, "left");
        this.addBorder(buf, pap._brcRight, "right");
        buf.append(">");
    }
    
    private void addCharacterProperties(final CHP chp, final StringBuffer buf) {
        buf.append("<fo:inline ");
        buf.append("font-family=\"" + this._fonts.getFont(chp._ftcAscii) + "\" ");
        buf.append("font-size=\"" + chp._hps / 2 + "pt\" ");
        buf.append("color=\"" + this.getColor(chp._ico) + "\" ");
        this.addBorder(buf, chp._brc, "top");
        this.addBorder(buf, chp._brc, "bottom");
        this.addBorder(buf, chp._brc, "left");
        this.addBorder(buf, chp._brc, "right");
        if (chp._italic) {
            buf.append("font-style=\"italic\" ");
        }
        if (chp._bold) {
            buf.append("font-weight=\"bold\" ");
        }
        if (chp._fSmallCaps) {
            buf.append("font-variant=\"small-caps\" ");
        }
        if (chp._fCaps) {
            buf.append("text-transform=\"uppercase\" ");
        }
        if (chp._fStrike || chp._fDStrike) {
            buf.append("text-decoration=\"line-through\" ");
        }
        if (chp._fShadow) {
            final int size = chp._hps / 24;
            buf.append("text-shadow=\"" + size + "pt\"");
        }
        if (chp._fLowerCase) {
            buf.append("text-transform=\"lowercase\" ");
        }
        if (chp._kul > 0) {
            buf.append("text-decoration=\"underline\" ");
        }
        if (chp._highlighted) {
            buf.append("background-color=\"" + this.getColor(chp._icoHighlight) + "\" ");
        }
        if (chp._paddingStart != 0) {
            buf.append("padding-start=\"" + chp._paddingStart / 1440.0f + "in\" ");
        }
        if (chp._paddingEnd != 0) {
            buf.append("padding-end=\"" + chp._paddingEnd / 1440.0f + "in\" ");
        }
        buf.append(">");
    }
    
    private void addStaticContent(final String flowName, final HeaderFooter content) {
        this._bodyBuffer.append("<fo:static-content flow-name=\"" + flowName + "\">");
        this.addBlockContent(content.getStart(), content.getEnd(), this._text, this._paragraphTable, this._characterTable);
        this._bodyBuffer.append("</fo:static-content>");
    }
    
    private String getBulletText(final LVL lvl, final PAP pap) {
        final StringBuffer bulletBuffer = new StringBuffer();
        for (int x = 0; x < lvl._xst.length; ++x) {
            if (lvl._xst[x] < '\t') {
                final LVL numLevel = this._listTables.getLevel(pap._ilfo, lvl._xst[x]);
                int num = numLevel._iStartAt;
                if (lvl == numLevel) {
                    final LVL lvl2 = numLevel;
                    ++lvl2._iStartAt;
                }
                else if (num > 1) {
                    --num;
                }
                bulletBuffer.append(NumberFormatter.getNumber(num, lvl._nfc));
            }
            else {
                bulletBuffer.append(lvl._xst[x]);
            }
        }
        return bulletBuffer.toString();
    }
    
    private ArrayList findProperties(final int start, final int end, final BTreeSet.BTreeNode root) {
        final ArrayList results = new ArrayList();
        final BTreeSet.Entry[] entries = root._entries;
        for (int x = 0; x < entries.length && entries[x] != null; ++x) {
            final BTreeSet.BTreeNode child = entries[x].child;
            final PropertyNode xNode = (PropertyNode)entries[x].element;
            if (xNode != null) {
                final int xStart = xNode.getStart();
                final int xEnd = xNode.getEnd();
                if (xStart < end) {
                    if (xStart >= start) {
                        if (child != null) {
                            final ArrayList beforeItems = this.findProperties(start, end, child);
                            results.addAll(beforeItems);
                        }
                        results.add(xNode);
                    }
                    else if (start < xEnd) {
                        results.add(xNode);
                    }
                }
                else {
                    if (child != null) {
                        final ArrayList beforeItems = this.findProperties(start, end, child);
                        results.addAll(beforeItems);
                        break;
                    }
                    break;
                }
            }
            else if (child != null) {
                final ArrayList afterItems = this.findProperties(start, end, child);
                results.addAll(afterItems);
            }
        }
        return results;
    }
    
    private void openPage(final String page, final String type) {
        this._bodyBuffer.append("<fo:page-sequence master-reference=\"" + page + "\">\r\n");
    }
    
    private void openFlow() {
        this._bodyBuffer.append("<fo:flow flow-name=\"xsl-region-body\">\r\n");
    }
    
    private void closeFlow() {
        this._bodyBuffer.append("</fo:flow>\r\n");
    }
    
    private void closePage() {
        this._bodyBuffer.append("</fo:page-sequence>\r\n");
    }
    
    private void closeLine(final StringBuffer buf) {
        buf.append("</fo:inline>");
    }
    
    private void closeBlock(final StringBuffer buf) {
        buf.append("</fo:block>\r\n");
    }
    
    private ArrayList findPAPProperties(final int start, final int end, final BTreeSet.BTreeNode root) {
        final ArrayList results = new ArrayList();
        final BTreeSet.Entry[] entries = root._entries;
        for (int x = 0; x < entries.length && entries[x] != null; ++x) {
            final BTreeSet.BTreeNode child = entries[x].child;
            final PapxNode papxNode = (PapxNode)entries[x].element;
            if (papxNode != null) {
                final int papxStart = papxNode.getStart();
                if (papxStart < end) {
                    if (papxStart >= start) {
                        if (child != null) {
                            final ArrayList beforeItems = this.findPAPProperties(start, end, child);
                            results.addAll(beforeItems);
                        }
                        results.add(papxNode);
                    }
                }
                else {
                    if (child != null) {
                        final ArrayList beforeItems = this.findPAPProperties(start, end, child);
                        results.addAll(beforeItems);
                        break;
                    }
                    break;
                }
            }
            else if (child != null) {
                final ArrayList afterItems = this.findPAPProperties(start, end, child);
                results.addAll(afterItems);
            }
        }
        return results;
    }
    
    private String createPageMaster(final SEP sep, final String type, final int section, final String regionBefore, final String regionAfter) {
        final float height = sep._yaPage / 1440.0f;
        final float width = sep._xaPage / 1440.0f;
        final float leftMargin = sep._dxaLeft / 1440.0f;
        final float rightMargin = sep._dxaRight / 1440.0f;
        final float topMargin = sep._dyaTop / 1440.0f;
        final float bottomMargin = sep._dyaBottom / 1440.0f;
        final String thisPage = type + "-page" + section;
        this._headerBuffer.append("<fo:simple-page-master master-name=\"" + thisPage + "\"\r\n");
        this._headerBuffer.append("page-height=\"" + height + "in\"\r\n");
        this._headerBuffer.append("page-width=\"" + width + "in\"\r\n");
        this._headerBuffer.append(">\r\n");
        this._headerBuffer.append("<fo:region-body ");
        this._headerBuffer.append("margin=\"" + topMargin + "in " + rightMargin + "in " + bottomMargin + "in " + leftMargin + "in\"\r\n");
        this.addBorder(this._headerBuffer, sep._brcTop, "top");
        this.addBorder(this._headerBuffer, sep._brcBottom, "bottom");
        this.addBorder(this._headerBuffer, sep._brcLeft, "left");
        this.addBorder(this._headerBuffer, sep._brcRight, "right");
        if (sep._ccolM1 > 0) {
            this._headerBuffer.append("column-count=\"" + (sep._ccolM1 + 1) + "\" ");
            if (sep._fEvenlySpaced) {
                this._headerBuffer.append("column-gap=\"" + sep._dxaColumns / 1440.0f + "in\"");
            }
            else {
                this._headerBuffer.append("column-gap=\"0.25in\"");
            }
        }
        this._headerBuffer.append("/>\r\n");
        if (regionBefore != null) {
            this._headerBuffer.append(regionBefore);
        }
        if (regionAfter != null) {
            this._headerBuffer.append(regionAfter);
        }
        this._headerBuffer.append("</fo:simple-page-master>\r\n");
        return thisPage;
    }
    
    private void addBorder(final StringBuffer buf, final short[] brc, final String where) {
        if ((brc[0] & 0xFF00) != 0x0 && brc[0] != -1) {
            final int type = (brc[0] & 0xFF00) >> 8;
            final float width = (brc[0] & 0xFF) / 8.0f;
            final String style = this.getBorderStyle(brc[0]);
            final String color = this.getColor(brc[1] & 0xFF);
            final String thickness = this.getBorderThickness(brc[0]);
            buf.append("border-" + where + "-style=\"" + style + "\"\r\n");
            buf.append("border-" + where + "-color=\"" + color + "\"\r\n");
            buf.append("border-" + where + "-width=\"" + width + "pt\"\r\n");
        }
    }
    
    public void closeDoc() {
        this._headerBuffer.append("</fo:layout-master-set>");
        this._bodyBuffer.append("</fo:root>");
        try {
            final OutputStreamWriter test = new OutputStreamWriter(new FileOutputStream(WordDocument._outName), "8859_1");
            test.write(this._headerBuffer.toString());
            test.write(this._bodyBuffer.toString());
            test.flush();
            test.close();
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    private String getBorderThickness(final int style) {
        switch (style) {
            case 1: {
                return "medium";
            }
            case 2: {
                return "thick";
            }
            case 3: {
                return "medium";
            }
            case 5: {
                return "thin";
            }
            default: {
                return "medium";
            }
        }
    }
    
    private String getColor(final int ico) {
        switch (ico) {
            case 1: {
                return "black";
            }
            case 2: {
                return "blue";
            }
            case 3: {
                return "cyan";
            }
            case 4: {
                return "green";
            }
            case 5: {
                return "magenta";
            }
            case 6: {
                return "red";
            }
            case 7: {
                return "yellow";
            }
            case 8: {
                return "white";
            }
            case 9: {
                return "darkblue";
            }
            case 10: {
                return "darkcyan";
            }
            case 11: {
                return "darkgreen";
            }
            case 12: {
                return "darkmagenta";
            }
            case 13: {
                return "darkred";
            }
            case 14: {
                return "darkyellow";
            }
            case 15: {
                return "darkgray";
            }
            case 16: {
                return "lightgray";
            }
            default: {
                return "black";
            }
        }
    }
    
    private String getBorderStyle(final int type) {
        switch (type) {
            case 1:
            case 2: {
                return "solid";
            }
            case 3: {
                return "double";
            }
            case 5: {
                return "solid";
            }
            case 6: {
                return "dotted";
            }
            case 7:
            case 8: {
                return "dashed";
            }
            case 9: {
                return "dotted";
            }
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19: {
                return "double";
            }
            case 20: {
                return "solid";
            }
            case 21: {
                return "double";
            }
            case 22: {
                return "dashed";
            }
            case 23: {
                return "dashed";
            }
            case 24: {
                return "ridge";
            }
            case 25: {
                return "grooved";
            }
            default: {
                return "solid";
            }
        }
    }
    
    private void createListTables(final byte[] tableStream) {
        final int lfoOffset = LittleEndian.getInt(this._header, 746);
        final int lfoSize = LittleEndian.getInt(this._header, 750);
        final byte[] plflfo = new byte[lfoSize];
        System.arraycopy(tableStream, lfoOffset, plflfo, 0, lfoSize);
        final int lstOffset = LittleEndian.getInt(this._header, 738);
        int lstSize = LittleEndian.getInt(this._header, 738);
        if (lstOffset > 0 && lstSize > 0) {
            lstSize = lfoOffset - lstOffset;
            final byte[] plcflst = new byte[lstSize];
            System.arraycopy(tableStream, lstOffset, plcflst, 0, lstSize);
            this._listTables = new ListTables(plcflst, plflfo);
        }
    }
    
    private void createStyleSheet(final byte[] tableStream) {
        final int stshIndex = LittleEndian.getInt(this._header, 162);
        final int stshSize = LittleEndian.getInt(this._header, 166);
        final byte[] stsh = new byte[stshSize];
        System.arraycopy(tableStream, stshIndex, stsh, 0, stshSize);
        this._styleSheet = new StyleSheet(stsh);
    }
    
    private void createFontTable(final byte[] tableStream) {
        final int fontTableIndex = LittleEndian.getInt(this._header, 274);
        final int fontTableSize = LittleEndian.getInt(this._header, 278);
        final byte[] fontTable = new byte[fontTableSize];
        System.arraycopy(tableStream, fontTableIndex, fontTable, 0, fontTableSize);
        this._fonts = new FontTable(fontTable);
    }
    
    private void overrideCellBorder(final int row, final int col, final int height, final int width, final TC tc, final TAP tap) {
        if (row == 0) {
            if (tc._brcTop[0] == 0 || tc._brcTop[0] == -1) {
                tc._brcTop = tap._brcTop;
            }
            if (tc._brcBottom[0] == 0 || tc._brcBottom[0] == -1) {
                tc._brcBottom = tap._brcHorizontal;
            }
        }
        else if (row == height - 1) {
            if (tc._brcTop[0] == 0 || tc._brcTop[0] == -1) {
                tc._brcTop = tap._brcHorizontal;
            }
            if (tc._brcBottom[0] == 0 || tc._brcBottom[0] == -1) {
                tc._brcBottom = tap._brcBottom;
            }
        }
        else {
            if (tc._brcTop[0] == 0 || tc._brcTop[0] == -1) {
                tc._brcTop = tap._brcHorizontal;
            }
            if (tc._brcBottom[0] == 0 || tc._brcBottom[0] == -1) {
                tc._brcBottom = tap._brcHorizontal;
            }
        }
        if (col == 0) {
            if (tc._brcLeft[0] == 0 || tc._brcLeft[0] == -1) {
                tc._brcLeft = tap._brcLeft;
            }
            if (tc._brcRight[0] == 0 || tc._brcRight[0] == -1) {
                tc._brcRight = tap._brcVertical;
            }
        }
        else if (col == width - 1) {
            if (tc._brcLeft[0] == 0 || tc._brcLeft[0] == -1) {
                tc._brcLeft = tap._brcVertical;
            }
            if (tc._brcRight[0] == 0 || tc._brcRight[0] == -1) {
                tc._brcRight = tap._brcRight;
            }
        }
        else {
            if (tc._brcLeft[0] == 0 || tc._brcLeft[0] == -1) {
                tc._brcLeft = tap._brcVertical;
            }
            if (tc._brcRight[0] == 0 || tc._brcRight[0] == -1) {
                tc._brcRight = tap._brcVertical;
            }
        }
    }
    
    private void printTable() {
        if (this._table != null) {
            final int size = this._table.size();
            final StringBuffer tableHeaderBuffer = new StringBuffer();
            final StringBuffer tableBodyBuffer = new StringBuffer();
            for (int x = 0; x < size; ++x) {
                StringBuffer rowBuffer = tableBodyBuffer;
                final TableRow row = this._table.get(x);
                final TAP tap = row.getTAP();
                final ArrayList cells = row.getCells();
                if (tap._fTableHeader) {
                    rowBuffer = tableHeaderBuffer;
                }
                rowBuffer.append("<fo:table-row ");
                if (tap._dyaRowHeight > 0) {
                    rowBuffer.append("height=\"" + tap._dyaRowHeight / 1440.0f + "in\" ");
                }
                if (tap._fCantSplit) {
                    rowBuffer.append("keep-together=\"always\" ");
                }
                rowBuffer.append(">");
                for (int y = 0; y < tap._itcMac; ++y) {
                    final TC tc = tap._rgtc[y];
                    this.overrideCellBorder(x, y, size, tap._itcMac, tc, tap);
                    rowBuffer.append("<fo:table-cell ");
                    rowBuffer.append("width=\"" + (tap._rgdxaCenter[y + 1] - tap._rgdxaCenter[y]) / 1440.0f + "in\" ");
                    rowBuffer.append("padding-start=\"" + tap._dxaGapHalf / 1440.0f + "in\" ");
                    rowBuffer.append("padding-end=\"" + tap._dxaGapHalf / 1440.0f + "in\" ");
                    this.addBorder(rowBuffer, tc._brcTop, "top");
                    this.addBorder(rowBuffer, tc._brcLeft, "left");
                    this.addBorder(rowBuffer, tc._brcBottom, "bottom");
                    this.addBorder(rowBuffer, tc._brcRight, "right");
                    rowBuffer.append(">");
                    rowBuffer.append(cells.get(y));
                    rowBuffer.append("</fo:table-cell>");
                }
                rowBuffer.append("</fo:table-row>");
            }
            final StringBuffer tableBuffer = new StringBuffer();
            tableBuffer.append("<fo:table>");
            if (tableHeaderBuffer.length() > 0) {
                tableBuffer.append("<fo:table-header>");
                tableBuffer.append(tableHeaderBuffer.toString());
                tableBuffer.append("</fo:table-header>");
            }
            tableBuffer.append("<fo:table-body>");
            tableBuffer.append(tableBodyBuffer.toString());
            tableBuffer.append("</fo:table-body>");
            tableBuffer.append("</fo:table>");
            this._bodyBuffer.append(tableBuffer.toString());
            this._table = null;
        }
    }
    
    private void initPclfHdd(final byte[] tableStream) {
        final int size = Utils.convertBytesToInt(this._header, 246);
        final int pos = Utils.convertBytesToInt(this._header, 242);
        System.arraycopy(tableStream, pos, this._plcfHdd = new byte[size], 0, size);
    }
    
    static {
        WordDocument.HEADER_EVEN_INDEX = 0;
        WordDocument.HEADER_ODD_INDEX = 1;
        WordDocument.FOOTER_EVEN_INDEX = 2;
        WordDocument.FOOTER_ODD_INDEX = 3;
        WordDocument.HEADER_FIRST_INDEX = 4;
        WordDocument.FOOTER_FIRST_INDEX = 5;
    }
}
