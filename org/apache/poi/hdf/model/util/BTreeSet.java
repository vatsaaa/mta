// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.util;

import java.util.NoSuchElementException;
import java.util.Stack;
import org.apache.poi.hdf.model.hdftypes.PropertyNode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.util.Comparator;
import java.util.AbstractSet;

@Deprecated
public final class BTreeSet extends AbstractSet
{
    public BTreeNode root;
    private Comparator comparator;
    private int order;
    int size;
    
    public BTreeSet() {
        this(6);
    }
    
    public BTreeSet(final Collection c) {
        this(6);
        this.addAll(c);
    }
    
    public BTreeSet(final int order) {
        this(order, null);
    }
    
    public BTreeSet(final int order, final Comparator comparator) {
        this.comparator = null;
        this.size = 0;
        this.order = order;
        this.comparator = comparator;
        this.root = new BTreeNode(null);
    }
    
    @Override
    public boolean add(final Object x) throws IllegalArgumentException {
        if (x == null) {
            throw new IllegalArgumentException();
        }
        return this.root.insert(x, -1);
    }
    
    @Override
    public boolean contains(final Object x) {
        return this.root.includes(x);
    }
    
    @Override
    public boolean remove(final Object x) {
        return x != null && this.root.delete(x, -1);
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    @Override
    public void clear() {
        this.root = new BTreeNode(null);
        this.size = 0;
    }
    
    @Override
    public java.util.Iterator iterator() {
        return new Iterator();
    }
    
    public static ArrayList findProperties(final int start, final int end, final BTreeNode root) {
        final ArrayList results = new ArrayList();
        final Entry[] entries = root.entries;
        for (int x = 0; x < entries.length && entries[x] != null; ++x) {
            final BTreeNode child = entries[x].child;
            final PropertyNode xNode = (PropertyNode)entries[x].element;
            if (xNode != null) {
                final int xStart = xNode.getStart();
                final int xEnd = xNode.getEnd();
                if (xStart < end) {
                    if (xStart >= start) {
                        if (child != null) {
                            final ArrayList beforeItems = findProperties(start, end, child);
                            results.addAll(beforeItems);
                        }
                        results.add(xNode);
                    }
                    else if (start < xEnd) {
                        results.add(xNode);
                    }
                }
                else {
                    if (child != null) {
                        final ArrayList beforeItems = findProperties(start, end, child);
                        results.addAll(beforeItems);
                        break;
                    }
                    break;
                }
            }
            else if (child != null) {
                final ArrayList afterItems = findProperties(start, end, child);
                results.addAll(afterItems);
            }
        }
        return results;
    }
    
    int compare(final Object x, final Object y) {
        return (this.comparator == null) ? ((Comparable)x).compareTo(y) : this.comparator.compare(x, y);
    }
    
    private class Iterator implements java.util.Iterator
    {
        private int index;
        private Stack parentIndex;
        private Object lastReturned;
        private Object next;
        private BTreeNode currentNode;
        
        Iterator() {
            this.index = 0;
            this.parentIndex = new Stack();
            this.lastReturned = null;
            this.currentNode = this.firstNode();
            this.next = this.nextElement();
        }
        
        public boolean hasNext() {
            return this.next != null;
        }
        
        public Object next() {
            if (this.next == null) {
                throw new NoSuchElementException();
            }
            this.lastReturned = this.next;
            this.next = this.nextElement();
            return this.lastReturned;
        }
        
        public void remove() {
            if (this.lastReturned == null) {
                throw new NoSuchElementException();
            }
            BTreeSet.this.remove(this.lastReturned);
            this.lastReturned = null;
        }
        
        private BTreeNode firstNode() {
            BTreeNode temp = BTreeSet.this.root;
            while (temp.entries[0].child != null) {
                temp = temp.entries[0].child;
                this.parentIndex.push(0);
            }
            return temp;
        }
        
        private Object nextElement() {
            if (!this.currentNode.isLeaf()) {
                this.currentNode = this.currentNode.entries[this.index].child;
                this.parentIndex.push(this.index);
                while (this.currentNode.entries[0].child != null) {
                    this.currentNode = this.currentNode.entries[0].child;
                    this.parentIndex.push(0);
                }
                this.index = 1;
                return this.currentNode.entries[0].element;
            }
            if (this.index < this.currentNode.nrElements) {
                return this.currentNode.entries[this.index++].element;
            }
            if (!this.parentIndex.empty()) {
                this.currentNode = this.currentNode.parent;
                this.index = this.parentIndex.pop();
                while (this.index == this.currentNode.nrElements && !this.parentIndex.empty()) {
                    this.currentNode = this.currentNode.parent;
                    this.index = this.parentIndex.pop();
                }
                if (this.index == this.currentNode.nrElements) {
                    return null;
                }
                return this.currentNode.entries[this.index++].element;
            }
            else {
                if (this.index == this.currentNode.nrElements) {
                    return null;
                }
                return this.currentNode.entries[this.index++].element;
            }
        }
    }
    
    public static class Entry
    {
        public Object element;
        public BTreeNode child;
    }
    
    public class BTreeNode
    {
        public Entry[] entries;
        public BTreeNode parent;
        private int nrElements;
        private final int MIN;
        
        BTreeNode(final BTreeNode parent) {
            this.nrElements = 0;
            this.MIN = (BTreeSet.this.order - 1) / 2;
            this.parent = parent;
            (this.entries = new Entry[BTreeSet.this.order])[0] = new Entry();
        }
        
        boolean insert(final Object x, final int parentIndex) {
            if (this.isFull()) {
                final Object splitNode = this.entries[this.nrElements / 2].element;
                final BTreeNode rightSibling = this.split();
                if (this.isRoot()) {
                    this.splitRoot(splitNode, this, rightSibling);
                    if (BTreeSet.this.compare(x, BTreeSet.this.root.entries[0].element) < 0) {
                        this.insert(x, 0);
                    }
                    else {
                        rightSibling.insert(x, 1);
                    }
                    return false;
                }
                this.parent.insertSplitNode(splitNode, this, rightSibling, parentIndex);
                if (BTreeSet.this.compare(x, this.parent.entries[parentIndex].element) < 0) {
                    return this.insert(x, parentIndex);
                }
                return rightSibling.insert(x, parentIndex + 1);
            }
            else {
                if (!this.isLeaf()) {
                    final int insertAt = this.childToInsertAt(x, true);
                    return insertAt != -1 && this.entries[insertAt].child.insert(x, insertAt);
                }
                final int insertAt = this.childToInsertAt(x, true);
                if (insertAt == -1) {
                    return false;
                }
                this.insertNewElement(x, insertAt);
                final BTreeSet this$0 = BTreeSet.this;
                ++this$0.size;
                return true;
            }
        }
        
        boolean includes(final Object x) {
            final int index = this.childToInsertAt(x, true);
            return index == -1 || (this.entries[index] != null && this.entries[index].child != null && this.entries[index].child.includes(x));
        }
        
        boolean delete(final Object x, int parentIndex) {
            int i = this.childToInsertAt(x, true);
            int priorParentIndex = parentIndex;
            BTreeNode temp = this;
            Label_0073: {
                if (i != -1) {
                    while (temp.entries[i] != null && temp.entries[i].child != null) {
                        temp = temp.entries[i].child;
                        priorParentIndex = parentIndex;
                        parentIndex = i;
                        i = temp.childToInsertAt(x, true);
                        if (i == -1) {
                            break Label_0073;
                        }
                    }
                    return false;
                }
            }
            if (!temp.isLeaf()) {
                temp.switchWithSuccessor(x);
                parentIndex = temp.childToInsertAt(x, false) + 1;
                return temp.entries[parentIndex].child.delete(x, parentIndex);
            }
            if (temp.nrElements > this.MIN) {
                temp.deleteElement(x);
                final BTreeSet this$0 = BTreeSet.this;
                --this$0.size;
                return true;
            }
            temp.prepareForDeletion(parentIndex);
            temp.deleteElement(x);
            final BTreeSet this$2 = BTreeSet.this;
            --this$2.size;
            temp.fixAfterDeletion(priorParentIndex);
            return true;
        }
        
        private boolean isFull() {
            return this.nrElements == BTreeSet.this.order - 1;
        }
        
        private boolean isLeaf() {
            return this.entries[0].child == null;
        }
        
        private boolean isRoot() {
            return this.parent == null;
        }
        
        private BTreeNode split() {
            final BTreeNode rightSibling = new BTreeNode(this.parent);
            int index = this.nrElements / 2;
            this.entries[index++].element = null;
            int i = 0;
            for (int nr = this.nrElements; index <= nr; ++index) {
                rightSibling.entries[i] = this.entries[index];
                if (rightSibling.entries[i] != null && rightSibling.entries[i].child != null) {
                    rightSibling.entries[i].child.parent = rightSibling;
                }
                this.entries[index] = null;
                --this.nrElements;
                final BTreeNode bTreeNode = rightSibling;
                ++bTreeNode.nrElements;
                ++i;
            }
            final BTreeNode bTreeNode2 = rightSibling;
            --bTreeNode2.nrElements;
            return rightSibling;
        }
        
        private void splitRoot(final Object splitNode, final BTreeNode left, final BTreeNode right) {
            final BTreeNode newRoot = new BTreeNode(null);
            newRoot.entries[0].element = splitNode;
            newRoot.entries[0].child = left;
            newRoot.entries[1] = new Entry();
            newRoot.entries[1].child = right;
            newRoot.nrElements = 1;
            final BTreeNode bTreeNode = newRoot;
            right.parent = bTreeNode;
            left.parent = bTreeNode;
            BTreeSet.this.root = newRoot;
        }
        
        private void insertSplitNode(final Object splitNode, final BTreeNode left, final BTreeNode right, final int insertAt) {
            for (int i = this.nrElements; i >= insertAt; --i) {
                this.entries[i + 1] = this.entries[i];
            }
            this.entries[insertAt] = new Entry();
            this.entries[insertAt].element = splitNode;
            this.entries[insertAt].child = left;
            this.entries[insertAt + 1].child = right;
            ++this.nrElements;
        }
        
        private void insertNewElement(final Object x, final int insertAt) {
            for (int i = this.nrElements; i > insertAt; --i) {
                this.entries[i] = this.entries[i - 1];
            }
            this.entries[insertAt] = new Entry();
            this.entries[insertAt].element = x;
            ++this.nrElements;
        }
        
        private int childToInsertAt(final Object x, final boolean position) {
            int index = this.nrElements / 2;
            if (this.entries[index] == null || this.entries[index].element == null) {
                return index;
            }
            int lo = 0;
            int hi = this.nrElements - 1;
            while (lo <= hi) {
                if (BTreeSet.this.compare(x, this.entries[index].element) > 0) {
                    lo = index + 1;
                    index = (hi + lo) / 2;
                }
                else {
                    hi = index - 1;
                    index = (hi + lo) / 2;
                }
            }
            ++hi;
            if (this.entries[hi] == null || this.entries[hi].element == null) {
                return hi;
            }
            return position ? ((BTreeSet.this.compare(x, this.entries[hi].element) == 0) ? -1 : hi) : hi;
        }
        
        private void deleteElement(final Object x) {
            int index;
            for (index = this.childToInsertAt(x, false); index < this.nrElements - 1; ++index) {
                this.entries[index] = this.entries[index + 1];
            }
            if (this.nrElements == 1) {
                this.entries[index] = new Entry();
            }
            else {
                this.entries[index] = null;
            }
            --this.nrElements;
        }
        
        private void prepareForDeletion(final int parentIndex) {
            if (this.isRoot()) {
                return;
            }
            if (parentIndex != 0 && this.parent.entries[parentIndex - 1].child.nrElements > this.MIN) {
                this.stealLeft(parentIndex);
                return;
            }
            if (parentIndex < this.entries.length && this.parent.entries[parentIndex + 1] != null && this.parent.entries[parentIndex + 1].child != null && this.parent.entries[parentIndex + 1].child.nrElements > this.MIN) {
                this.stealRight(parentIndex);
                return;
            }
            if (parentIndex != 0) {
                this.mergeLeft(parentIndex);
                return;
            }
            this.mergeRight(parentIndex);
        }
        
        private void fixAfterDeletion(final int parentIndex) {
            if (this.isRoot() || this.parent.isRoot()) {
                return;
            }
            if (this.parent.nrElements < this.MIN) {
                final BTreeNode temp = this.parent;
                temp.prepareForDeletion(parentIndex);
                if (temp.parent == null) {
                    return;
                }
                if (!temp.parent.isRoot() && temp.parent.nrElements < this.MIN) {
                    BTreeNode x;
                    int i;
                    for (x = temp.parent.parent, i = 0; i < this.entries.length && x.entries[i].child != temp.parent; ++i) {}
                    temp.parent.fixAfterDeletion(i);
                }
            }
        }
        
        private void switchWithSuccessor(final Object x) {
            final int index = this.childToInsertAt(x, false);
            BTreeNode temp;
            for (temp = this.entries[index + 1].child; temp.entries[0] != null && temp.entries[0].child != null; temp = temp.entries[0].child) {}
            final Object successor = temp.entries[0].element;
            temp.entries[0].element = this.entries[index].element;
            this.entries[index].element = successor;
        }
        
        private void stealLeft(final int parentIndex) {
            final BTreeNode p = this.parent;
            final BTreeNode ls = this.parent.entries[parentIndex - 1].child;
            if (this.isLeaf()) {
                final int add = this.childToInsertAt(p.entries[parentIndex - 1].element, true);
                this.insertNewElement(p.entries[parentIndex - 1].element, add);
                p.entries[parentIndex - 1].element = ls.entries[ls.nrElements - 1].element;
                ls.entries[ls.nrElements - 1] = null;
                final BTreeNode bTreeNode = ls;
                --bTreeNode.nrElements;
            }
            else {
                this.entries[0].element = p.entries[parentIndex - 1].element;
                p.entries[parentIndex - 1].element = ls.entries[ls.nrElements - 1].element;
                this.entries[0].child = ls.entries[ls.nrElements].child;
                this.entries[0].child.parent = this;
                ls.entries[ls.nrElements] = null;
                ls.entries[ls.nrElements - 1].element = null;
                ++this.nrElements;
                final BTreeNode bTreeNode2 = ls;
                --bTreeNode2.nrElements;
            }
        }
        
        private void stealRight(final int parentIndex) {
            final BTreeNode p = this.parent;
            final BTreeNode rs = p.entries[parentIndex + 1].child;
            if (this.isLeaf()) {
                this.entries[this.nrElements] = new Entry();
                this.entries[this.nrElements].element = p.entries[parentIndex].element;
                p.entries[parentIndex].element = rs.entries[0].element;
                for (int i = 0; i < rs.nrElements; ++i) {
                    rs.entries[i] = rs.entries[i + 1];
                }
                rs.entries[rs.nrElements - 1] = null;
                ++this.nrElements;
                final BTreeNode bTreeNode = rs;
                --bTreeNode.nrElements;
            }
            else {
                for (int i = 0; i <= this.nrElements; ++i) {
                    this.entries[i] = this.entries[i + 1];
                }
                this.entries[this.nrElements].element = p.entries[parentIndex].element;
                p.entries[parentIndex].element = rs.entries[0].element;
                this.entries[this.nrElements + 1] = new Entry();
                this.entries[this.nrElements + 1].child = rs.entries[0].child;
                this.entries[this.nrElements + 1].child.parent = this;
                for (int i = 0; i <= rs.nrElements; ++i) {
                    rs.entries[i] = rs.entries[i + 1];
                }
                rs.entries[rs.nrElements] = null;
                ++this.nrElements;
                final BTreeNode bTreeNode2 = rs;
                --bTreeNode2.nrElements;
            }
        }
        
        private void mergeLeft(final int parentIndex) {
            final BTreeNode p = this.parent;
            final BTreeNode ls = p.entries[parentIndex - 1].child;
            if (this.isLeaf()) {
                final int add = this.childToInsertAt(p.entries[parentIndex - 1].element, true);
                this.insertNewElement(p.entries[parentIndex - 1].element, add);
                p.entries[parentIndex - 1].element = null;
                int i = this.nrElements - 1;
                final int nr = ls.nrElements;
                while (i >= 0) {
                    this.entries[i + nr] = this.entries[i];
                    --i;
                }
                for (i = ls.nrElements - 1; i >= 0; --i) {
                    this.entries[i] = ls.entries[i];
                    ++this.nrElements;
                }
                if (p.nrElements == this.MIN && p != BTreeSet.this.root) {
                    int x = parentIndex - 1;
                    for (int y = parentIndex - 2; y >= 0; --y) {
                        p.entries[x] = p.entries[y];
                        --x;
                    }
                    p.entries[0] = new Entry();
                    p.entries[0].child = ls;
                }
                else {
                    int x = parentIndex - 1;
                    for (int y = parentIndex; y <= p.nrElements; ++y) {
                        p.entries[x] = p.entries[y];
                        ++x;
                    }
                    p.entries[p.nrElements] = null;
                }
                final BTreeNode bTreeNode = p;
                --bTreeNode.nrElements;
                if (p.isRoot() && p.nrElements == 0) {
                    BTreeSet.this.root = this;
                    this.parent = null;
                }
            }
            else {
                this.entries[0].element = p.entries[parentIndex - 1].element;
                this.entries[0].child = ls.entries[ls.nrElements].child;
                ++this.nrElements;
                int x2 = this.nrElements;
                final int nr2 = ls.nrElements;
                while (x2 >= 0) {
                    this.entries[x2 + nr2] = this.entries[x2];
                    --x2;
                }
                for (x2 = ls.nrElements - 1; x2 >= 0; --x2) {
                    this.entries[x2] = ls.entries[x2];
                    this.entries[x2].child.parent = this;
                    ++this.nrElements;
                }
                if (p.nrElements == this.MIN && p != BTreeSet.this.root) {
                    x2 = parentIndex - 1;
                    for (int y2 = parentIndex - 2; y2 >= 0; ++y2) {
                        System.out.println(x2 + " " + y2);
                        p.entries[x2] = p.entries[y2];
                        ++x2;
                    }
                    p.entries[0] = new Entry();
                }
                else {
                    x2 = parentIndex - 1;
                    for (int y2 = parentIndex; y2 <= p.nrElements; ++y2) {
                        p.entries[x2] = p.entries[y2];
                        ++x2;
                    }
                    p.entries[p.nrElements] = null;
                }
                final BTreeNode bTreeNode2 = p;
                --bTreeNode2.nrElements;
                if (p.isRoot() && p.nrElements == 0) {
                    BTreeSet.this.root = this;
                    this.parent = null;
                }
            }
        }
        
        private void mergeRight(int parentIndex) {
            final BTreeNode p = this.parent;
            final BTreeNode rs = p.entries[parentIndex + 1].child;
            if (this.isLeaf()) {
                this.entries[this.nrElements] = new Entry();
                this.entries[this.nrElements].element = p.entries[parentIndex].element;
                ++this.nrElements;
                for (int i = 0, nr = this.nrElements; i < rs.nrElements; ++i, ++nr) {
                    this.entries[nr] = rs.entries[i];
                    ++this.nrElements;
                }
                p.entries[parentIndex].element = p.entries[parentIndex + 1].element;
                if (p.nrElements == this.MIN && p != BTreeSet.this.root) {
                    int x = parentIndex + 1;
                    for (int y = parentIndex; y >= 0; --y) {
                        p.entries[x] = p.entries[y];
                        --x;
                    }
                    p.entries[0] = new Entry();
                    p.entries[0].child = rs;
                }
                else {
                    int x = parentIndex + 1;
                    for (int y = parentIndex + 2; y <= p.nrElements; ++y) {
                        p.entries[x] = p.entries[y];
                        ++x;
                    }
                    p.entries[p.nrElements] = null;
                }
                final BTreeNode bTreeNode = p;
                --bTreeNode.nrElements;
                if (p.isRoot() && p.nrElements == 0) {
                    BTreeSet.this.root = this;
                    this.parent = null;
                }
            }
            else {
                this.entries[this.nrElements].element = p.entries[parentIndex].element;
                ++this.nrElements;
                int x = this.nrElements + 1;
                for (int y = 0; y <= rs.nrElements; ++y) {
                    this.entries[x] = rs.entries[y];
                    rs.entries[y].child.parent = this;
                    ++this.nrElements;
                    ++x;
                }
                --this.nrElements;
                p.entries[++parentIndex].child = this;
                if (p.nrElements == this.MIN && p != BTreeSet.this.root) {
                    x = parentIndex - 1;
                    for (int y = parentIndex - 2; y >= 0; --y) {
                        p.entries[x] = p.entries[y];
                        --x;
                    }
                    p.entries[0] = new Entry();
                }
                else {
                    x = parentIndex - 1;
                    for (int y = parentIndex; y <= p.nrElements; ++y) {
                        p.entries[x] = p.entries[y];
                        ++x;
                    }
                    p.entries[p.nrElements] = null;
                }
                final BTreeNode bTreeNode2 = p;
                --bTreeNode2.nrElements;
                if (p.isRoot() && p.nrElements == 0) {
                    BTreeSet.this.root = this;
                    this.parent = null;
                }
            }
        }
    }
}
