// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.util;

import org.apache.poi.hdf.model.hdftypes.FormattedDiskPage;

@Deprecated
public final class ParsingState
{
    int _currentPageIndex;
    FormattedDiskPage _fkp;
    int _currentPropIndex;
    
    public ParsingState(final int firstPage, final FormattedDiskPage fkp) {
        this._currentPageIndex = 0;
        this._currentPropIndex = 0;
        this._fkp = fkp;
    }
    
    public int getCurrentPageIndex() {
        return this._currentPageIndex;
    }
    
    public FormattedDiskPage getFkp() {
        return this._fkp;
    }
    
    public int getCurrentPropIndex() {
        return this._currentPropIndex;
    }
    
    public void setState(final int currentPageIndex, final FormattedDiskPage fkp, final int currentPropIndex) {
        this._currentPageIndex = currentPageIndex;
        this._fkp = fkp;
        this._currentPropIndex = currentPropIndex;
    }
}
