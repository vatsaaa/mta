// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.util;

@Deprecated
public final class NumberFormatter
{
    private static final int ARABIC = 0;
    private static final int UPPER_ROMAN = 1;
    private static final int LOWER_ROMAN = 2;
    private static final int UPPER_LETTER = 3;
    private static final int LOWER_LETTER = 4;
    private static final int ORDINAL = 5;
    private static String[] _arabic;
    private static String[] _roman;
    private static String[] _letter;
    
    public static String getNumber(final int num, final int style) {
        switch (style) {
            case 0: {
                return NumberFormatter._arabic[num - 1];
            }
            case 1: {
                return NumberFormatter._roman[num - 1].toUpperCase();
            }
            case 2: {
                return NumberFormatter._roman[num - 1];
            }
            case 3: {
                return NumberFormatter._letter[num - 1].toUpperCase();
            }
            case 4: {
                return NumberFormatter._letter[num - 1];
            }
            case 5: {
                return NumberFormatter._arabic[num - 1];
            }
            default: {
                return NumberFormatter._arabic[num - 1];
            }
        }
    }
    
    static {
        NumberFormatter._arabic = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53" };
        NumberFormatter._roman = new String[] { "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x", "xi", "xii", "xiii", "xiv", "xv", "xvi", "xvii", "xviii", "xix", "xx", "xxi", "xxii", "xxiii", "xxiv", "xxv", "xxvi", "xxvii", "xxviii", "xxix", "xxx", "xxxi", "xxxii", "xxxiii", "xxxiv", "xxxv", "xxxvi", "xxxvii", "xxxvii", "xxxviii", "xxxix", "xl", "xli", "xlii", "xliii", "xliv", "xlv", "xlvi", "xlvii", "xlviii", "xlix", "l" };
        NumberFormatter._letter = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z" };
    }
}
