// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model;

import org.apache.poi.hdf.model.hdftypes.FontTable;
import org.apache.poi.hdf.model.hdftypes.ListTables;
import org.apache.poi.hdf.model.hdftypes.StyleSheet;
import org.apache.poi.hdf.model.hdftypes.DocumentProperties;
import org.apache.poi.hdf.model.hdftypes.SepxNode;
import org.apache.poi.hdf.model.hdftypes.PAPFormattedDiskPage;
import org.apache.poi.hdf.model.hdftypes.PapxNode;
import org.apache.poi.hdf.model.hdftypes.FormattedDiskPage;
import org.apache.poi.hdf.model.hdftypes.CHPFormattedDiskPage;
import org.apache.poi.hdf.model.hdftypes.ChpxNode;
import org.apache.poi.hdf.model.hdftypes.PlexOfCps;
import org.apache.poi.hdf.model.hdftypes.TextPiece;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import java.io.InputStream;
import java.io.FileInputStream;
import org.apache.poi.hdf.model.util.ParsingState;
import org.apache.poi.hdf.event.HDFLowLevelParsingListener;
import org.apache.poi.hdf.model.hdftypes.FileInformationBlock;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

@Deprecated
public final class HDFObjectFactory
{
    private POIFSFileSystem _filesystem;
    private FileInformationBlock _fib;
    private HDFLowLevelParsingListener _listener;
    private ParsingState _charParsingState;
    private ParsingState _parParsingState;
    byte[] _mainDocument;
    byte[] _tableBuffer;
    
    public static void main(final String[] args) {
        try {
            final HDFObjectFactory f = new HDFObjectFactory(new FileInputStream("c:\\test.doc"));
            final int k = 0;
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    protected HDFObjectFactory(final InputStream istream, final HDFLowLevelParsingListener l) throws IOException {
        if (l == null) {
            this._listener = new HDFObjectModel();
        }
        else {
            this._listener = l;
        }
        this._filesystem = new POIFSFileSystem(istream);
        final DocumentEntry headerProps = (DocumentEntry)this._filesystem.getRoot().getEntry("WordDocument");
        this._mainDocument = new byte[headerProps.getSize()];
        this._filesystem.createDocumentInputStream("WordDocument").read(this._mainDocument);
        this._fib = new FileInformationBlock(this._mainDocument);
        this.initTableStream();
        this.initTextPieces();
        this.initFormattingProperties();
    }
    
    public HDFObjectFactory(final InputStream istream) throws IOException {
        this(istream, null);
    }
    
    public static List getTypes(final InputStream istream) throws IOException {
        final List results = new ArrayList(1);
        final POIFSFileSystem filesystem = new POIFSFileSystem(istream);
        final DocumentEntry headerProps = (DocumentEntry)filesystem.getRoot().getEntry("WordDocument");
        final byte[] mainDocument = new byte[headerProps.getSize()];
        filesystem.createDocumentInputStream("WordDocument").read(mainDocument);
        final FileInformationBlock fib = new FileInformationBlock(mainDocument);
        results.add(fib);
        return results;
    }
    
    private void initTableStream() throws IOException {
        String tablename = null;
        if (this._fib.isFWhichTblStm()) {
            tablename = "1Table";
        }
        else {
            tablename = "0Table";
        }
        final DocumentEntry tableEntry = (DocumentEntry)this._filesystem.getRoot().getEntry(tablename);
        final int size = tableEntry.getSize();
        this._tableBuffer = new byte[size];
        this._filesystem.createDocumentInputStream(tablename).read(this._tableBuffer);
    }
    
    private void initTextPieces() throws IOException {
        int pos;
        int skip;
        for (pos = this._fib.getFcClx(); this._tableBuffer[pos] == 1; ++pos, skip = LittleEndian.getShort(this._tableBuffer, pos), pos += 2 + skip) {}
        if (this._tableBuffer[pos] != 2) {
            throw new IOException("The text piece table is corrupted");
        }
        final int pieceTableSize = LittleEndian.getInt(this._tableBuffer, ++pos);
        pos += 4;
        for (int pieces = (pieceTableSize - 4) / 12, x = 0; x < pieces; ++x) {
            int filePos = LittleEndian.getInt(this._tableBuffer, pos + (pieces + 1) * 4 + x * 8 + 2);
            boolean unicode = false;
            if ((filePos & 0x40000000) == 0x0) {
                unicode = true;
            }
            else {
                unicode = false;
                filePos &= 0xBFFFFFFF;
                filePos /= 2;
            }
            final int totLength = LittleEndian.getInt(this._tableBuffer, pos + (x + 1) * 4) - LittleEndian.getInt(this._tableBuffer, pos + x * 4);
            final TextPiece piece = new TextPiece(filePos, totLength, unicode);
            this._listener.text(piece);
        }
    }
    
    private void initFormattingProperties() {
        this.createStyleSheet();
        this.createListTables();
        this.createFontTable();
        this.initDocumentProperties();
        this.initSectionProperties();
    }
    
    private void initCharacterProperties(final int charOffset, final PlexOfCps charPlcf, final int start, final int end) {
        final int charPlcfLen = charPlcf.length();
        int currentPageIndex = this._charParsingState.getCurrentPageIndex();
        FormattedDiskPage fkp = this._charParsingState.getFkp();
        int currentChpxIndex = this._charParsingState.getCurrentPropIndex();
        int currentArraySize = fkp.size();
        int charStart = 0;
        int charEnd = 0;
        do {
            if (currentChpxIndex < currentArraySize) {
                charStart = fkp.getStart(currentChpxIndex);
                charEnd = fkp.getEnd(currentChpxIndex);
                final byte[] chpx = fkp.getGrpprl(currentChpxIndex);
                this._listener.characterRun(new ChpxNode(Math.max(charStart, start), Math.min(charEnd, end), chpx));
                if (charEnd >= end) {
                    this._charParsingState.setState(currentPageIndex, fkp, currentChpxIndex);
                    break;
                }
                ++currentChpxIndex;
            }
            else {
                final int currentCharPage = LittleEndian.getInt(this._tableBuffer, charOffset + charPlcf.getStructOffset(++currentPageIndex));
                final byte[] byteFkp = new byte[512];
                System.arraycopy(this._mainDocument, currentCharPage * 512, byteFkp, 0, 512);
                fkp = new CHPFormattedDiskPage(byteFkp);
                currentChpxIndex = 0;
                currentArraySize = fkp.size();
            }
        } while (currentPageIndex < charPlcfLen);
    }
    
    private void initParagraphProperties(final int parOffset, final PlexOfCps parPlcf, final int charOffset, final PlexOfCps charPlcf, final int start, final int end) {
        final int parPlcfLen = parPlcf.length();
        int currentPageIndex = this._parParsingState.getCurrentPageIndex();
        FormattedDiskPage fkp = this._parParsingState.getFkp();
        int currentPapxIndex = this._parParsingState.getCurrentPropIndex();
        int currentArraySize = fkp.size();
        do {
            if (currentPapxIndex < currentArraySize) {
                final int parStart = fkp.getStart(currentPapxIndex);
                final int parEnd = fkp.getEnd(currentPapxIndex);
                final byte[] papx = fkp.getGrpprl(currentPapxIndex);
                this._listener.paragraph(new PapxNode(Math.max(parStart, start), Math.min(parEnd, end), papx));
                this.initCharacterProperties(charOffset, charPlcf, Math.max(start, parStart), Math.min(parEnd, end));
                if (parEnd >= end) {
                    this._parParsingState.setState(currentPageIndex, fkp, currentPapxIndex);
                    break;
                }
                ++currentPapxIndex;
            }
            else {
                final int currentParPage = LittleEndian.getInt(this._tableBuffer, parOffset + parPlcf.getStructOffset(++currentPageIndex));
                final byte[] byteFkp = new byte[512];
                System.arraycopy(this._mainDocument, currentParPage * 512, byteFkp, 0, 512);
                fkp = new PAPFormattedDiskPage(byteFkp);
                currentPapxIndex = 0;
                currentArraySize = fkp.size();
            }
        } while (currentPageIndex < parPlcfLen);
    }
    
    private void initParagraphProperties() {
        final int parOffset = this._fib.getFcPlcfbtePapx();
        final int parPlcSize = this._fib.getLcbPlcfbtePapx();
        final int charOffset = this._fib.getFcPlcfbteChpx();
        final int charPlcSize = this._fib.getLcbPlcfbteChpx();
        final PlexOfCps charPlcf = new PlexOfCps(charPlcSize, 4);
        final PlexOfCps parPlcf = new PlexOfCps(parPlcSize, 4);
        int currentCharPage = LittleEndian.getInt(this._tableBuffer, charOffset + charPlcf.getStructOffset(0));
        final int charPlcfLen = charPlcf.length();
        int currentPageIndex = 0;
        byte[] fkp = new byte[512];
        System.arraycopy(this._mainDocument, currentCharPage * 512, fkp, 0, 512);
        CHPFormattedDiskPage cfkp = new CHPFormattedDiskPage(fkp);
        int currentChpxIndex = 0;
        int currentArraySize = cfkp.size();
        for (int arraySize = parPlcf.length(), x = 0; x < arraySize; ++x) {
            final int PN = LittleEndian.getInt(this._tableBuffer, parOffset + parPlcf.getStructOffset(x));
            fkp = new byte[512];
            System.arraycopy(this._mainDocument, PN * 512, fkp, 0, 512);
            final PAPFormattedDiskPage pfkp = new PAPFormattedDiskPage(fkp);
            for (int crun = pfkp.size(), y = 0; y < crun; ++y) {
                final int fcStart = pfkp.getStart(y);
                final int fcEnd = pfkp.getEnd(y);
                final byte[] papx = pfkp.getGrpprl(y);
                this._listener.paragraph(new PapxNode(fcStart, fcEnd, papx));
                int charStart = 0;
                int charEnd = 0;
                do {
                    if (currentChpxIndex < currentArraySize) {
                        charStart = cfkp.getStart(currentChpxIndex);
                        charEnd = cfkp.getEnd(currentChpxIndex);
                        final byte[] chpx = cfkp.getGrpprl(currentChpxIndex);
                        this._listener.characterRun(new ChpxNode(charStart, charEnd, chpx));
                        if (charEnd >= fcEnd) {
                            break;
                        }
                        ++currentChpxIndex;
                    }
                    else {
                        currentCharPage = LittleEndian.getInt(this._tableBuffer, charOffset + charPlcf.getStructOffset(++currentPageIndex));
                        fkp = new byte[512];
                        System.arraycopy(this._mainDocument, currentCharPage * 512, fkp, 0, 512);
                        cfkp = new CHPFormattedDiskPage(fkp);
                        currentChpxIndex = 0;
                        currentArraySize = cfkp.size();
                    }
                } while (currentCharPage <= charPlcfLen + 1);
            }
        }
    }
    
    private void initParsingStates(final int parOffset, final PlexOfCps parPlcf, final int charOffset, final PlexOfCps charPlcf) {
        final int currentCharPage = LittleEndian.getInt(this._tableBuffer, charOffset + charPlcf.getStructOffset(0));
        byte[] fkp = new byte[512];
        System.arraycopy(this._mainDocument, currentCharPage * 512, fkp, 0, 512);
        final CHPFormattedDiskPage cfkp = new CHPFormattedDiskPage(fkp);
        this._charParsingState = new ParsingState(currentCharPage, cfkp);
        final int currentParPage = LittleEndian.getInt(this._tableBuffer, parOffset + parPlcf.getStructOffset(0));
        fkp = new byte[512];
        System.arraycopy(this._mainDocument, currentParPage * 512, fkp, 0, 512);
        final PAPFormattedDiskPage pfkp = new PAPFormattedDiskPage(fkp);
        this._parParsingState = new ParsingState(currentParPage, pfkp);
    }
    
    private void initSectionProperties() {
        final int ccpText = this._fib.getCcpText();
        final int ccpFtn = this._fib.getCcpFtn();
        final int fcMin = this._fib.getFcMin();
        final int plcfsedFC = this._fib.getFcPlcfsed();
        final int plcfsedSize = this._fib.getLcbPlcfsed();
        final int parOffset = this._fib.getFcPlcfbtePapx();
        final int parPlcSize = this._fib.getLcbPlcfbtePapx();
        final int charOffset = this._fib.getFcPlcfbteChpx();
        final int charPlcSize = this._fib.getLcbPlcfbteChpx();
        final PlexOfCps charPlcf = new PlexOfCps(charPlcSize, 4);
        final PlexOfCps parPlcf = new PlexOfCps(parPlcSize, 4);
        this.initParsingStates(parOffset, parPlcf, charOffset, charPlcf);
        final PlexOfCps plcfsed = new PlexOfCps(plcfsedSize, 12);
        final int arraySize = plcfsed.length();
        final int start = fcMin;
        final int end = fcMin + ccpText;
        int x = 0;
        int sectionEnd = 0;
        while (x < arraySize) {
            final int sectionStart = LittleEndian.getInt(this._tableBuffer, plcfsedFC + plcfsed.getIntOffset(x)) + fcMin;
            sectionEnd = LittleEndian.getInt(this._tableBuffer, plcfsedFC + plcfsed.getIntOffset(x + 1)) + fcMin;
            final int sepxStart = LittleEndian.getInt(this._tableBuffer, plcfsedFC + plcfsed.getStructOffset(x) + 2);
            final int sepxSize = LittleEndian.getShort(this._mainDocument, sepxStart);
            final byte[] sepx = new byte[sepxSize];
            System.arraycopy(this._mainDocument, sepxStart + 2, sepx, 0, sepxSize);
            final SepxNode node = new SepxNode(x + 1, sectionStart, sectionEnd, sepx);
            this._listener.bodySection(node);
            this.initParagraphProperties(parOffset, parPlcf, charOffset, charPlcf, sectionStart, Math.min(end, sectionEnd));
            if (sectionEnd > end) {
                break;
            }
            ++x;
        }
        while (x < arraySize) {
            final int sectionStart = LittleEndian.getInt(this._tableBuffer, plcfsedFC + plcfsed.getIntOffset(x)) + fcMin;
            sectionEnd = LittleEndian.getInt(this._tableBuffer, plcfsedFC + plcfsed.getIntOffset(x + 1)) + fcMin;
            final int sepxStart = LittleEndian.getInt(this._tableBuffer, plcfsedFC + plcfsed.getStructOffset(x) + 2);
            final int sepxSize = LittleEndian.getShort(this._mainDocument, sepxStart);
            final byte[] sepx = new byte[sepxSize];
            System.arraycopy(this._mainDocument, sepxStart + 2, sepx, 0, sepxSize);
            final SepxNode node = new SepxNode(x + 1, sectionStart, sectionEnd, sepx);
            this._listener.hdrSection(node);
            this.initParagraphProperties(parOffset, parPlcf, charOffset, charPlcf, Math.max(sectionStart, end), sectionEnd);
            ++x;
        }
        this._listener.endSections();
    }
    
    private void initDocumentProperties() {
        final int pos = this._fib.getFcDop();
        final int size = this._fib.getLcbDop();
        final byte[] dopArray = new byte[size];
        System.arraycopy(this._tableBuffer, pos, dopArray, 0, size);
        this._listener.document(new DocumentProperties(dopArray));
    }
    
    private void createStyleSheet() {
        final int stshIndex = this._fib.getFcStshf();
        final int stshSize = this._fib.getLcbStshf();
        final byte[] stsh = new byte[stshSize];
        System.arraycopy(this._tableBuffer, stshIndex, stsh, 0, stshSize);
        this._listener.styleSheet(new StyleSheet(stsh));
    }
    
    private void createListTables() {
        final int lfoOffset = this._fib.getFcPlfLfo();
        final int lfoSize = this._fib.getLcbPlfLfo();
        final byte[] plflfo = new byte[lfoSize];
        System.arraycopy(this._tableBuffer, lfoOffset, plflfo, 0, lfoSize);
        final int lstOffset = this._fib.getFcPlcfLst();
        int lstSize = this._fib.getLcbPlcfLst();
        if (lstOffset > 0 && lstSize > 0) {
            lstSize = lfoOffset - lstOffset;
            final byte[] plcflst = new byte[lstSize];
            System.arraycopy(this._tableBuffer, lstOffset, plcflst, 0, lstSize);
            this._listener.lists(new ListTables(plcflst, plflfo));
        }
    }
    
    private void createFontTable() {
        final int fontTableIndex = this._fib.getFcSttbfffn();
        final int fontTableSize = this._fib.getLcbSttbfffn();
        final byte[] fontTable = new byte[fontTableSize];
        System.arraycopy(this._tableBuffer, fontTableIndex, fontTable, 0, fontTableSize);
        this._listener.fonts(new FontTable(fontTable));
    }
}
