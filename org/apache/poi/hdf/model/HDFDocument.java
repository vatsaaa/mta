// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model;

import java.io.IOException;
import org.apache.poi.hdf.event.HDFLowLevelParsingListener;
import org.apache.poi.hdf.event.EventBridge;
import org.apache.poi.hdf.event.HDFParsingListener;
import java.io.InputStream;

@Deprecated
public final class HDFDocument
{
    HDFObjectModel _model;
    
    public HDFDocument(final InputStream in, final HDFParsingListener listener) throws IOException {
        final EventBridge eb = new EventBridge(listener);
        final HDFObjectFactory factory = new HDFObjectFactory(in, eb);
    }
    
    public HDFDocument(final InputStream in) throws IOException {
        this._model = new HDFObjectModel();
        final HDFObjectFactory factory = new HDFObjectFactory(in, this._model);
    }
}
