// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model;

import org.apache.poi.hdf.model.hdftypes.TextPiece;
import org.apache.poi.hdf.model.hdftypes.ChpxNode;
import org.apache.poi.hdf.model.hdftypes.PapxNode;
import org.apache.poi.hdf.model.hdftypes.SepxNode;
import org.apache.poi.hdf.model.util.BTreeSet;
import org.apache.poi.hdf.model.hdftypes.FontTable;
import org.apache.poi.hdf.model.hdftypes.ListTables;
import org.apache.poi.hdf.model.hdftypes.StyleSheet;
import org.apache.poi.hdf.model.hdftypes.DocumentProperties;
import org.apache.poi.hdf.event.HDFLowLevelParsingListener;

@Deprecated
public final class HDFObjectModel implements HDFLowLevelParsingListener
{
    private byte[] _mainDocument;
    private DocumentProperties _dop;
    private StyleSheet _styleSheet;
    private ListTables _listTables;
    private FontTable _fonts;
    int _fcMin;
    BTreeSet _text;
    BTreeSet _sections;
    BTreeSet _paragraphs;
    BTreeSet _characterRuns;
    
    public HDFObjectModel() {
        this._text = new BTreeSet();
        this._sections = new BTreeSet();
        this._paragraphs = new BTreeSet();
        this._characterRuns = new BTreeSet();
    }
    
    public void mainDocument(final byte[] mainDocument) {
        this._mainDocument = mainDocument;
    }
    
    public void tableStream(final byte[] tableStream) {
    }
    
    public void miscellaneous(final int fcMin, final int ccpText, final int ccpFtn, final int fcPlcfhdd, final int lcbPlcfhdd) {
        this._fcMin = fcMin;
    }
    
    public void document(final DocumentProperties dop) {
        this._dop = dop;
    }
    
    public void bodySection(final SepxNode sepx) {
        this._sections.add(sepx);
    }
    
    public void hdrSection(final SepxNode sepx) {
        this._sections.add(sepx);
    }
    
    public void endSections() {
    }
    
    public void paragraph(final PapxNode papx) {
        this._paragraphs.add(papx);
    }
    
    public void characterRun(final ChpxNode chpx) {
        this._characterRuns.add(chpx);
    }
    
    public void text(final TextPiece t) {
        this._text.add(t);
    }
    
    public void fonts(final FontTable fontTbl) {
        this._fonts = fontTbl;
    }
    
    public void lists(final ListTables listTbl) {
        this._listTables = listTbl;
    }
    
    public void styleSheet(final StyleSheet stsh) {
        this._styleSheet = stsh;
    }
}
