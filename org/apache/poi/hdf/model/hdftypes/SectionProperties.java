// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.hdf.model.hdftypes.definitions.SEPAbstractType;

@Deprecated
public final class SectionProperties extends SEPAbstractType implements HDFType
{
    public SectionProperties() {
        this.setBkc((byte)2);
        this.setDyaPgn(720);
        this.setDxaPgn(720);
        this.setFEndNote(true);
        this.setFEvenlySpaced(true);
        this.setXaPage(12240);
        this.setYaPage(15840);
        this.setDyaHdrTop(720);
        this.setDyaHdrBottom(720);
        this.setDmOrientPage((byte)1);
        this.setDxaColumns(720);
        this.setDyaTop(1440);
        this.setDxaLeft(1800);
        this.setDyaBottom(1440);
        this.setDxaRight(1800);
        this.setPgnStart(1);
    }
}
