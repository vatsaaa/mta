// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.util.LittleEndian;

@Deprecated
public abstract class FormattedDiskPage
{
    protected byte[] _fkp;
    protected int _crun;
    
    public FormattedDiskPage(final byte[] fkp) {
        this._crun = LittleEndian.getUnsignedByte(fkp, 511);
        this._fkp = fkp;
    }
    
    public int getStart(final int index) {
        return LittleEndian.getInt(this._fkp, index * 4);
    }
    
    public int getEnd(final int index) {
        return LittleEndian.getInt(this._fkp, (index + 1) * 4);
    }
    
    public int size() {
        return this._crun;
    }
    
    public abstract byte[] getGrpprl(final int p0);
}
