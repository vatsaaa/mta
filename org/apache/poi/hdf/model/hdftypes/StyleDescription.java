// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.util.LittleEndian;

@Deprecated
public final class StyleDescription implements HDFType
{
    private static int PARAGRAPH_STYLE;
    private static int CHARACTER_STYLE;
    int _baseStyleIndex;
    int _styleTypeCode;
    int _numUPX;
    byte[] _papx;
    byte[] _chpx;
    ParagraphProperties _pap;
    CharacterProperties _chp;
    
    public StyleDescription() {
        this._pap = new ParagraphProperties();
        this._chp = new CharacterProperties();
    }
    
    public StyleDescription(final byte[] std, final int baseLength, final boolean word9) {
        int infoShort = LittleEndian.getShort(std, 2);
        this._styleTypeCode = (infoShort & 0xF);
        this._baseStyleIndex = (infoShort & 0xFFF0) >> 4;
        infoShort = LittleEndian.getShort(std, 4);
        this._numUPX = (infoShort & 0xF);
        int nameLength = 0;
        int multiplier = 1;
        if (word9) {
            nameLength = LittleEndian.getShort(std, baseLength);
            multiplier = 2;
        }
        else {
            nameLength = std[baseLength];
        }
        final int grupxStart = multiplier + (nameLength + 1) * multiplier + baseLength;
        int offset = 0;
        for (int x = 0; x < this._numUPX; ++x) {
            int upxSize = LittleEndian.getShort(std, grupxStart + offset);
            if (this._styleTypeCode == StyleDescription.PARAGRAPH_STYLE) {
                if (x == 0) {
                    System.arraycopy(std, grupxStart + offset + 2, this._papx = new byte[upxSize], 0, upxSize);
                }
                else if (x == 1) {
                    System.arraycopy(std, grupxStart + offset + 2, this._chpx = new byte[upxSize], 0, upxSize);
                }
            }
            else if (this._styleTypeCode == StyleDescription.CHARACTER_STYLE && x == 0) {
                System.arraycopy(std, grupxStart + offset + 2, this._chpx = new byte[upxSize], 0, upxSize);
            }
            if (upxSize % 2 == 1) {
                ++upxSize;
            }
            offset += 2 + upxSize;
        }
    }
    
    public int getBaseStyle() {
        return this._baseStyleIndex;
    }
    
    public byte[] getCHPX() {
        return this._chpx;
    }
    
    public byte[] getPAPX() {
        return this._papx;
    }
    
    public ParagraphProperties getPAP() {
        return this._pap;
    }
    
    public CharacterProperties getCHP() {
        return this._chp;
    }
    
    public void setPAP(final ParagraphProperties pap) {
        this._pap = pap;
    }
    
    public void setCHP(final CharacterProperties chp) {
        this._chp = chp;
    }
    
    static {
        StyleDescription.PARAGRAPH_STYLE = 1;
        StyleDescription.CHARACTER_STYLE = 2;
    }
}
