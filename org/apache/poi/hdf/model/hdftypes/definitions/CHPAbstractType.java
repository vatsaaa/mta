// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes.definitions;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.BitField;
import org.apache.poi.hdf.model.hdftypes.HDFType;

@Deprecated
public abstract class CHPAbstractType implements HDFType
{
    private short field_1_chse;
    private int field_2_format_flags;
    private static BitField fBold;
    private static BitField fItalic;
    private static BitField fRMarkDel;
    private static BitField fOutline;
    private static BitField fFldVanish;
    private static BitField fSmallCaps;
    private static BitField fCaps;
    private static BitField fVanish;
    private static BitField fRMark;
    private static BitField fSpec;
    private static BitField fStrike;
    private static BitField fObj;
    private static BitField fShadow;
    private static BitField fLowerCase;
    private static BitField fData;
    private static BitField fOle2;
    private int field_3_format_flags1;
    private static BitField fEmboss;
    private static BitField fImprint;
    private static BitField fDStrike;
    private static BitField fUsePgsuSettings;
    private int field_4_ftcAscii;
    private int field_5_ftcFE;
    private int field_6_ftcOther;
    private int field_7_hps;
    private int field_8_dxaSpace;
    private byte field_9_iss;
    private byte field_10_kul;
    private byte field_11_ico;
    private int field_12_hpsPos;
    private int field_13_lidDefault;
    private int field_14_lidFE;
    private byte field_15_idctHint;
    private int field_16_wCharScale;
    private int field_17_fcPic;
    private int field_18_fcObj;
    private int field_19_lTagObj;
    private int field_20_ibstRMark;
    private int field_21_ibstRMarkDel;
    private short[] field_22_dttmRMark;
    private short[] field_23_dttmRMarkDel;
    private int field_24_istd;
    private int field_25_baseIstd;
    private int field_26_ftcSym;
    private int field_27_xchSym;
    private int field_28_idslRMReason;
    private int field_29_idslReasonDel;
    private byte field_30_ysr;
    private byte field_31_chYsr;
    private int field_32_hpsKern;
    private short field_33_Highlight;
    private static BitField icoHighlight;
    private static BitField fHighlight;
    private static BitField kcd;
    private static BitField fNavHighlight;
    private static BitField fChsDiff;
    private static BitField fMacChs;
    private static BitField fFtcAsciSym;
    private short field_34_fPropMark;
    private int field_35_ibstPropRMark;
    private int field_36_dttmPropRMark;
    private byte field_37_sfxtText;
    private byte field_38_fDispFldRMark;
    private int field_39_ibstDispFldRMark;
    private int field_40_dttmDispFldRMark;
    private byte[] field_41_xstDispFldRMark;
    private int field_42_shd;
    private short[] field_43_brc;
    
    public int getSize() {
        return 130;
    }
    
    public short getChse() {
        return this.field_1_chse;
    }
    
    public void setChse(final short field_1_chse) {
        this.field_1_chse = field_1_chse;
    }
    
    public int getFormat_flags() {
        return this.field_2_format_flags;
    }
    
    public void setFormat_flags(final int field_2_format_flags) {
        this.field_2_format_flags = field_2_format_flags;
    }
    
    public int getFormat_flags1() {
        return this.field_3_format_flags1;
    }
    
    public void setFormat_flags1(final int field_3_format_flags1) {
        this.field_3_format_flags1 = field_3_format_flags1;
    }
    
    public int getFtcAscii() {
        return this.field_4_ftcAscii;
    }
    
    public void setFtcAscii(final int field_4_ftcAscii) {
        this.field_4_ftcAscii = field_4_ftcAscii;
    }
    
    public int getFtcFE() {
        return this.field_5_ftcFE;
    }
    
    public void setFtcFE(final int field_5_ftcFE) {
        this.field_5_ftcFE = field_5_ftcFE;
    }
    
    public int getFtcOther() {
        return this.field_6_ftcOther;
    }
    
    public void setFtcOther(final int field_6_ftcOther) {
        this.field_6_ftcOther = field_6_ftcOther;
    }
    
    public int getHps() {
        return this.field_7_hps;
    }
    
    public void setHps(final int field_7_hps) {
        this.field_7_hps = field_7_hps;
    }
    
    public int getDxaSpace() {
        return this.field_8_dxaSpace;
    }
    
    public void setDxaSpace(final int field_8_dxaSpace) {
        this.field_8_dxaSpace = field_8_dxaSpace;
    }
    
    public byte getIss() {
        return this.field_9_iss;
    }
    
    public void setIss(final byte field_9_iss) {
        this.field_9_iss = field_9_iss;
    }
    
    public byte getKul() {
        return this.field_10_kul;
    }
    
    public void setKul(final byte field_10_kul) {
        this.field_10_kul = field_10_kul;
    }
    
    public byte getIco() {
        return this.field_11_ico;
    }
    
    public void setIco(final byte field_11_ico) {
        this.field_11_ico = field_11_ico;
    }
    
    public int getHpsPos() {
        return this.field_12_hpsPos;
    }
    
    public void setHpsPos(final int field_12_hpsPos) {
        this.field_12_hpsPos = field_12_hpsPos;
    }
    
    public int getLidDefault() {
        return this.field_13_lidDefault;
    }
    
    public void setLidDefault(final int field_13_lidDefault) {
        this.field_13_lidDefault = field_13_lidDefault;
    }
    
    public int getLidFE() {
        return this.field_14_lidFE;
    }
    
    public void setLidFE(final int field_14_lidFE) {
        this.field_14_lidFE = field_14_lidFE;
    }
    
    public byte getIdctHint() {
        return this.field_15_idctHint;
    }
    
    public void setIdctHint(final byte field_15_idctHint) {
        this.field_15_idctHint = field_15_idctHint;
    }
    
    public int getWCharScale() {
        return this.field_16_wCharScale;
    }
    
    public void setWCharScale(final int field_16_wCharScale) {
        this.field_16_wCharScale = field_16_wCharScale;
    }
    
    public int getFcPic() {
        return this.field_17_fcPic;
    }
    
    public void setFcPic(final int field_17_fcPic) {
        this.field_17_fcPic = field_17_fcPic;
    }
    
    public int getFcObj() {
        return this.field_18_fcObj;
    }
    
    public void setFcObj(final int field_18_fcObj) {
        this.field_18_fcObj = field_18_fcObj;
    }
    
    public int getLTagObj() {
        return this.field_19_lTagObj;
    }
    
    public void setLTagObj(final int field_19_lTagObj) {
        this.field_19_lTagObj = field_19_lTagObj;
    }
    
    public int getIbstRMark() {
        return this.field_20_ibstRMark;
    }
    
    public void setIbstRMark(final int field_20_ibstRMark) {
        this.field_20_ibstRMark = field_20_ibstRMark;
    }
    
    public int getIbstRMarkDel() {
        return this.field_21_ibstRMarkDel;
    }
    
    public void setIbstRMarkDel(final int field_21_ibstRMarkDel) {
        this.field_21_ibstRMarkDel = field_21_ibstRMarkDel;
    }
    
    public short[] getDttmRMark() {
        return this.field_22_dttmRMark;
    }
    
    public void setDttmRMark(final short[] field_22_dttmRMark) {
        this.field_22_dttmRMark = field_22_dttmRMark;
    }
    
    public short[] getDttmRMarkDel() {
        return this.field_23_dttmRMarkDel;
    }
    
    public void setDttmRMarkDel(final short[] field_23_dttmRMarkDel) {
        this.field_23_dttmRMarkDel = field_23_dttmRMarkDel;
    }
    
    public int getIstd() {
        return this.field_24_istd;
    }
    
    public void setIstd(final int field_24_istd) {
        this.field_24_istd = field_24_istd;
    }
    
    public int getBaseIstd() {
        return this.field_25_baseIstd;
    }
    
    public void setBaseIstd(final int field_25_baseIstd) {
        this.field_25_baseIstd = field_25_baseIstd;
    }
    
    public int getFtcSym() {
        return this.field_26_ftcSym;
    }
    
    public void setFtcSym(final int field_26_ftcSym) {
        this.field_26_ftcSym = field_26_ftcSym;
    }
    
    public int getXchSym() {
        return this.field_27_xchSym;
    }
    
    public void setXchSym(final int field_27_xchSym) {
        this.field_27_xchSym = field_27_xchSym;
    }
    
    public int getIdslRMReason() {
        return this.field_28_idslRMReason;
    }
    
    public void setIdslRMReason(final int field_28_idslRMReason) {
        this.field_28_idslRMReason = field_28_idslRMReason;
    }
    
    public int getIdslReasonDel() {
        return this.field_29_idslReasonDel;
    }
    
    public void setIdslReasonDel(final int field_29_idslReasonDel) {
        this.field_29_idslReasonDel = field_29_idslReasonDel;
    }
    
    public byte getYsr() {
        return this.field_30_ysr;
    }
    
    public void setYsr(final byte field_30_ysr) {
        this.field_30_ysr = field_30_ysr;
    }
    
    public byte getChYsr() {
        return this.field_31_chYsr;
    }
    
    public void setChYsr(final byte field_31_chYsr) {
        this.field_31_chYsr = field_31_chYsr;
    }
    
    public int getHpsKern() {
        return this.field_32_hpsKern;
    }
    
    public void setHpsKern(final int field_32_hpsKern) {
        this.field_32_hpsKern = field_32_hpsKern;
    }
    
    public short getHighlight() {
        return this.field_33_Highlight;
    }
    
    public void setHighlight(final short field_33_Highlight) {
        this.field_33_Highlight = field_33_Highlight;
    }
    
    public short getFPropMark() {
        return this.field_34_fPropMark;
    }
    
    public void setFPropMark(final short field_34_fPropMark) {
        this.field_34_fPropMark = field_34_fPropMark;
    }
    
    public int getIbstPropRMark() {
        return this.field_35_ibstPropRMark;
    }
    
    public void setIbstPropRMark(final int field_35_ibstPropRMark) {
        this.field_35_ibstPropRMark = field_35_ibstPropRMark;
    }
    
    public int getDttmPropRMark() {
        return this.field_36_dttmPropRMark;
    }
    
    public void setDttmPropRMark(final int field_36_dttmPropRMark) {
        this.field_36_dttmPropRMark = field_36_dttmPropRMark;
    }
    
    public byte getSfxtText() {
        return this.field_37_sfxtText;
    }
    
    public void setSfxtText(final byte field_37_sfxtText) {
        this.field_37_sfxtText = field_37_sfxtText;
    }
    
    public byte getFDispFldRMark() {
        return this.field_38_fDispFldRMark;
    }
    
    public void setFDispFldRMark(final byte field_38_fDispFldRMark) {
        this.field_38_fDispFldRMark = field_38_fDispFldRMark;
    }
    
    public int getIbstDispFldRMark() {
        return this.field_39_ibstDispFldRMark;
    }
    
    public void setIbstDispFldRMark(final int field_39_ibstDispFldRMark) {
        this.field_39_ibstDispFldRMark = field_39_ibstDispFldRMark;
    }
    
    public int getDttmDispFldRMark() {
        return this.field_40_dttmDispFldRMark;
    }
    
    public void setDttmDispFldRMark(final int field_40_dttmDispFldRMark) {
        this.field_40_dttmDispFldRMark = field_40_dttmDispFldRMark;
    }
    
    public byte[] getXstDispFldRMark() {
        return this.field_41_xstDispFldRMark;
    }
    
    public void setXstDispFldRMark(final byte[] field_41_xstDispFldRMark) {
        this.field_41_xstDispFldRMark = field_41_xstDispFldRMark;
    }
    
    public int getShd() {
        return this.field_42_shd;
    }
    
    public void setShd(final int field_42_shd) {
        this.field_42_shd = field_42_shd;
    }
    
    public short[] getBrc() {
        return this.field_43_brc;
    }
    
    public void setBrc(final short[] field_43_brc) {
        this.field_43_brc = field_43_brc;
    }
    
    public void setFBold(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fBold.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFBold() {
        return CHPAbstractType.fBold.isSet(this.field_2_format_flags);
    }
    
    public void setFItalic(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fItalic.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFItalic() {
        return CHPAbstractType.fItalic.isSet(this.field_2_format_flags);
    }
    
    public void setFRMarkDel(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fRMarkDel.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFRMarkDel() {
        return CHPAbstractType.fRMarkDel.isSet(this.field_2_format_flags);
    }
    
    public void setFOutline(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fOutline.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFOutline() {
        return CHPAbstractType.fOutline.isSet(this.field_2_format_flags);
    }
    
    public void setFFldVanish(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fFldVanish.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFFldVanish() {
        return CHPAbstractType.fFldVanish.isSet(this.field_2_format_flags);
    }
    
    public void setFSmallCaps(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fSmallCaps.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFSmallCaps() {
        return CHPAbstractType.fSmallCaps.isSet(this.field_2_format_flags);
    }
    
    public void setFCaps(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fCaps.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFCaps() {
        return CHPAbstractType.fCaps.isSet(this.field_2_format_flags);
    }
    
    public void setFVanish(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fVanish.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFVanish() {
        return CHPAbstractType.fVanish.isSet(this.field_2_format_flags);
    }
    
    public void setFRMark(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fRMark.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFRMark() {
        return CHPAbstractType.fRMark.isSet(this.field_2_format_flags);
    }
    
    public void setFSpec(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fSpec.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFSpec() {
        return CHPAbstractType.fSpec.isSet(this.field_2_format_flags);
    }
    
    public void setFStrike(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fStrike.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFStrike() {
        return CHPAbstractType.fStrike.isSet(this.field_2_format_flags);
    }
    
    public void setFObj(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fObj.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFObj() {
        return CHPAbstractType.fObj.isSet(this.field_2_format_flags);
    }
    
    public void setFShadow(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fShadow.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFShadow() {
        return CHPAbstractType.fShadow.isSet(this.field_2_format_flags);
    }
    
    public void setFLowerCase(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fLowerCase.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFLowerCase() {
        return CHPAbstractType.fLowerCase.isSet(this.field_2_format_flags);
    }
    
    public void setFData(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fData.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFData() {
        return CHPAbstractType.fData.isSet(this.field_2_format_flags);
    }
    
    public void setFOle2(final boolean value) {
        this.field_2_format_flags = CHPAbstractType.fOle2.setBoolean(this.field_2_format_flags, value);
    }
    
    public boolean isFOle2() {
        return CHPAbstractType.fOle2.isSet(this.field_2_format_flags);
    }
    
    public void setFEmboss(final boolean value) {
        this.field_3_format_flags1 = CHPAbstractType.fEmboss.setBoolean(this.field_3_format_flags1, value);
    }
    
    public boolean isFEmboss() {
        return CHPAbstractType.fEmboss.isSet(this.field_3_format_flags1);
    }
    
    public void setFImprint(final boolean value) {
        this.field_3_format_flags1 = CHPAbstractType.fImprint.setBoolean(this.field_3_format_flags1, value);
    }
    
    public boolean isFImprint() {
        return CHPAbstractType.fImprint.isSet(this.field_3_format_flags1);
    }
    
    public void setFDStrike(final boolean value) {
        this.field_3_format_flags1 = CHPAbstractType.fDStrike.setBoolean(this.field_3_format_flags1, value);
    }
    
    public boolean isFDStrike() {
        return CHPAbstractType.fDStrike.isSet(this.field_3_format_flags1);
    }
    
    public void setFUsePgsuSettings(final boolean value) {
        this.field_3_format_flags1 = CHPAbstractType.fUsePgsuSettings.setBoolean(this.field_3_format_flags1, value);
    }
    
    public boolean isFUsePgsuSettings() {
        return CHPAbstractType.fUsePgsuSettings.isSet(this.field_3_format_flags1);
    }
    
    public void setIcoHighlight(final byte value) {
        this.field_33_Highlight = (short)CHPAbstractType.icoHighlight.setValue(this.field_33_Highlight, value);
    }
    
    public byte getIcoHighlight() {
        return (byte)CHPAbstractType.icoHighlight.getValue(this.field_33_Highlight);
    }
    
    public void setFHighlight(final boolean value) {
        this.field_33_Highlight = (short)CHPAbstractType.fHighlight.setBoolean(this.field_33_Highlight, value);
    }
    
    public boolean isFHighlight() {
        return CHPAbstractType.fHighlight.isSet(this.field_33_Highlight);
    }
    
    public void setKcd(final byte value) {
        this.field_33_Highlight = (short)CHPAbstractType.kcd.setValue(this.field_33_Highlight, value);
    }
    
    public byte getKcd() {
        return (byte)CHPAbstractType.kcd.getValue(this.field_33_Highlight);
    }
    
    public void setFNavHighlight(final boolean value) {
        this.field_33_Highlight = (short)CHPAbstractType.fNavHighlight.setBoolean(this.field_33_Highlight, value);
    }
    
    public boolean isFNavHighlight() {
        return CHPAbstractType.fNavHighlight.isSet(this.field_33_Highlight);
    }
    
    public void setFChsDiff(final boolean value) {
        this.field_33_Highlight = (short)CHPAbstractType.fChsDiff.setBoolean(this.field_33_Highlight, value);
    }
    
    public boolean isFChsDiff() {
        return CHPAbstractType.fChsDiff.isSet(this.field_33_Highlight);
    }
    
    public void setFMacChs(final boolean value) {
        this.field_33_Highlight = (short)CHPAbstractType.fMacChs.setBoolean(this.field_33_Highlight, value);
    }
    
    public boolean isFMacChs() {
        return CHPAbstractType.fMacChs.isSet(this.field_33_Highlight);
    }
    
    public void setFFtcAsciSym(final boolean value) {
        this.field_33_Highlight = (short)CHPAbstractType.fFtcAsciSym.setBoolean(this.field_33_Highlight, value);
    }
    
    public boolean isFFtcAsciSym() {
        return CHPAbstractType.fFtcAsciSym.isSet(this.field_33_Highlight);
    }
    
    static {
        CHPAbstractType.fBold = BitFieldFactory.getInstance(1);
        CHPAbstractType.fItalic = BitFieldFactory.getInstance(2);
        CHPAbstractType.fRMarkDel = BitFieldFactory.getInstance(4);
        CHPAbstractType.fOutline = BitFieldFactory.getInstance(8);
        CHPAbstractType.fFldVanish = BitFieldFactory.getInstance(16);
        CHPAbstractType.fSmallCaps = BitFieldFactory.getInstance(32);
        CHPAbstractType.fCaps = BitFieldFactory.getInstance(64);
        CHPAbstractType.fVanish = BitFieldFactory.getInstance(128);
        CHPAbstractType.fRMark = BitFieldFactory.getInstance(256);
        CHPAbstractType.fSpec = BitFieldFactory.getInstance(512);
        CHPAbstractType.fStrike = BitFieldFactory.getInstance(1024);
        CHPAbstractType.fObj = BitFieldFactory.getInstance(2048);
        CHPAbstractType.fShadow = BitFieldFactory.getInstance(4096);
        CHPAbstractType.fLowerCase = BitFieldFactory.getInstance(8192);
        CHPAbstractType.fData = BitFieldFactory.getInstance(16384);
        CHPAbstractType.fOle2 = BitFieldFactory.getInstance(32768);
        CHPAbstractType.fEmboss = BitFieldFactory.getInstance(1);
        CHPAbstractType.fImprint = BitFieldFactory.getInstance(2);
        CHPAbstractType.fDStrike = BitFieldFactory.getInstance(4);
        CHPAbstractType.fUsePgsuSettings = BitFieldFactory.getInstance(8);
        CHPAbstractType.icoHighlight = BitFieldFactory.getInstance(31);
        CHPAbstractType.fHighlight = BitFieldFactory.getInstance(32);
        CHPAbstractType.kcd = BitFieldFactory.getInstance(448);
        CHPAbstractType.fNavHighlight = BitFieldFactory.getInstance(512);
        CHPAbstractType.fChsDiff = BitFieldFactory.getInstance(1024);
        CHPAbstractType.fMacChs = BitFieldFactory.getInstance(2048);
        CHPAbstractType.fFtcAsciSym = BitFieldFactory.getInstance(4096);
    }
}
