// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes.definitions;

import org.apache.poi.hdf.model.hdftypes.HDFType;

@Deprecated
public abstract class TAPAbstractType implements HDFType
{
    private int field_1_jc;
    private int field_2_dxaGapHalf;
    private int field_3_dyaRowHeight;
    private boolean field_4_fCantSplit;
    private boolean field_5_fTableHeader;
    private int field_6_tlp;
    private short field_7_itcMac;
    private short[] field_8_rgdxaCenter;
    private TCAbstractType[] field_9_rgtc;
    private byte[] field_10_rgshd;
    private short[] field_11_brcBottom;
    private short[] field_12_brcTop;
    private short[] field_13_brcLeft;
    private short[] field_14_brcRight;
    private short[] field_15_brcVertical;
    private short[] field_16_brcHorizontal;
    
    public int getSize() {
        return 174;
    }
    
    public int getJc() {
        return this.field_1_jc;
    }
    
    public void setJc(final int field_1_jc) {
        this.field_1_jc = field_1_jc;
    }
    
    public int getDxaGapHalf() {
        return this.field_2_dxaGapHalf;
    }
    
    public void setDxaGapHalf(final int field_2_dxaGapHalf) {
        this.field_2_dxaGapHalf = field_2_dxaGapHalf;
    }
    
    public int getDyaRowHeight() {
        return this.field_3_dyaRowHeight;
    }
    
    public void setDyaRowHeight(final int field_3_dyaRowHeight) {
        this.field_3_dyaRowHeight = field_3_dyaRowHeight;
    }
    
    public boolean getFCantSplit() {
        return this.field_4_fCantSplit;
    }
    
    public void setFCantSplit(final boolean field_4_fCantSplit) {
        this.field_4_fCantSplit = field_4_fCantSplit;
    }
    
    public boolean getFTableHeader() {
        return this.field_5_fTableHeader;
    }
    
    public void setFTableHeader(final boolean field_5_fTableHeader) {
        this.field_5_fTableHeader = field_5_fTableHeader;
    }
    
    public int getTlp() {
        return this.field_6_tlp;
    }
    
    public void setTlp(final int field_6_tlp) {
        this.field_6_tlp = field_6_tlp;
    }
    
    public short getItcMac() {
        return this.field_7_itcMac;
    }
    
    public void setItcMac(final short field_7_itcMac) {
        this.field_7_itcMac = field_7_itcMac;
    }
    
    public short[] getRgdxaCenter() {
        return this.field_8_rgdxaCenter;
    }
    
    public void setRgdxaCenter(final short[] field_8_rgdxaCenter) {
        this.field_8_rgdxaCenter = field_8_rgdxaCenter;
    }
    
    public TCAbstractType[] getRgtc() {
        return this.field_9_rgtc;
    }
    
    public void setRgtc(final TCAbstractType[] field_9_rgtc) {
        this.field_9_rgtc = field_9_rgtc;
    }
    
    public byte[] getRgshd() {
        return this.field_10_rgshd;
    }
    
    public void setRgshd(final byte[] field_10_rgshd) {
        this.field_10_rgshd = field_10_rgshd;
    }
    
    public short[] getBrcBottom() {
        return this.field_11_brcBottom;
    }
    
    public void setBrcBottom(final short[] field_11_brcBottom) {
        this.field_11_brcBottom = field_11_brcBottom;
    }
    
    public short[] getBrcTop() {
        return this.field_12_brcTop;
    }
    
    public void setBrcTop(final short[] field_12_brcTop) {
        this.field_12_brcTop = field_12_brcTop;
    }
    
    public short[] getBrcLeft() {
        return this.field_13_brcLeft;
    }
    
    public void setBrcLeft(final short[] field_13_brcLeft) {
        this.field_13_brcLeft = field_13_brcLeft;
    }
    
    public short[] getBrcRight() {
        return this.field_14_brcRight;
    }
    
    public void setBrcRight(final short[] field_14_brcRight) {
        this.field_14_brcRight = field_14_brcRight;
    }
    
    public short[] getBrcVertical() {
        return this.field_15_brcVertical;
    }
    
    public void setBrcVertical(final short[] field_15_brcVertical) {
        this.field_15_brcVertical = field_15_brcVertical;
    }
    
    public short[] getBrcHorizontal() {
        return this.field_16_brcHorizontal;
    }
    
    public void setBrcHorizontal(final short[] field_16_brcHorizontal) {
        this.field_16_brcHorizontal = field_16_brcHorizontal;
    }
}
