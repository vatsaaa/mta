// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes.definitions;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.hdf.model.hdftypes.HDFType;

@Deprecated
public abstract class DOPAbstractType implements HDFType
{
    private byte field_1_formatFlags;
    private static BitField fFacingPages;
    private static BitField fWidowControl;
    private static BitField fPMHMainDoc;
    private static BitField grfSupression;
    private static BitField fpc;
    private static BitField unused1;
    private byte field_2_unused2;
    private short field_3_footnoteInfo;
    private static BitField rncFtn;
    private static BitField nFtn;
    private byte field_4_fOutlineDirtySave;
    private byte field_5_docinfo;
    private static BitField fOnlyMacPics;
    private static BitField fOnlyWinPics;
    private static BitField fLabelDoc;
    private static BitField fHyphCapitals;
    private static BitField fAutoHyphen;
    private static BitField fFormNoFields;
    private static BitField fLinkStyles;
    private static BitField fRevMarking;
    private byte field_6_docinfo1;
    private static BitField fBackup;
    private static BitField fExactCWords;
    private static BitField fPagHidden;
    private static BitField fPagResults;
    private static BitField fLockAtn;
    private static BitField fMirrorMargins;
    private static BitField unused3;
    private static BitField fDfltTrueType;
    private byte field_7_docinfo2;
    private static BitField fPagSupressTopSpacing;
    private static BitField fProtEnabled;
    private static BitField fDispFormFldSel;
    private static BitField fRMView;
    private static BitField fRMPrint;
    private static BitField unused4;
    private static BitField fLockRev;
    private static BitField fEmbedFonts;
    private short field_8_docinfo3;
    private static BitField oldfNoTabForInd;
    private static BitField oldfNoSpaceRaiseLower;
    private static BitField oldfSuppressSpbfAfterPageBreak;
    private static BitField oldfWrapTrailSpaces;
    private static BitField oldfMapPrintTextColor;
    private static BitField oldfNoColumnBalance;
    private static BitField oldfConvMailMergeEsc;
    private static BitField oldfSupressTopSpacing;
    private static BitField oldfOrigWordTableRules;
    private static BitField oldfTransparentMetafiles;
    private static BitField oldfShowBreaksInFrames;
    private static BitField oldfSwapBordersFacingPgs;
    private static BitField unused5;
    private int field_9_dxaTab;
    private int field_10_wSpare;
    private int field_11_dxaHotz;
    private int field_12_cConsexHypLim;
    private int field_13_wSpare2;
    private int field_14_dttmCreated;
    private int field_15_dttmRevised;
    private int field_16_dttmLastPrint;
    private int field_17_nRevision;
    private int field_18_tmEdited;
    private int field_19_cWords;
    private int field_20_cCh;
    private int field_21_cPg;
    private int field_22_cParas;
    private short field_23_Edn;
    private static BitField rncEdn;
    private static BitField nEdn;
    private short field_24_Edn1;
    private static BitField epc;
    private static BitField nfcFtnRef1;
    private static BitField nfcEdnRef1;
    private static BitField fPrintFormData;
    private static BitField fSaveFormData;
    private static BitField fShadeFormData;
    private static BitField fWCFtnEdn;
    private int field_25_cLines;
    private int field_26_cWordsFtnEnd;
    private int field_27_cChFtnEdn;
    private short field_28_cPgFtnEdn;
    private int field_29_cParasFtnEdn;
    private int field_30_cLinesFtnEdn;
    private int field_31_lKeyProtDoc;
    private short field_32_view;
    private static BitField wvkSaved;
    private static BitField wScaleSaved;
    private static BitField zkSaved;
    private static BitField fRotateFontW6;
    private static BitField iGutterPos;
    private int field_33_docinfo4;
    private static BitField fNoTabForInd;
    private static BitField fNoSpaceRaiseLower;
    private static BitField fSupressSpdfAfterPageBreak;
    private static BitField fWrapTrailSpaces;
    private static BitField fMapPrintTextColor;
    private static BitField fNoColumnBalance;
    private static BitField fConvMailMergeEsc;
    private static BitField fSupressTopSpacing;
    private static BitField fOrigWordTableRules;
    private static BitField fTransparentMetafiles;
    private static BitField fShowBreaksInFrames;
    private static BitField fSwapBordersFacingPgs;
    private static BitField fSuppressTopSPacingMac5;
    private static BitField fTruncDxaExpand;
    private static BitField fPrintBodyBeforeHdr;
    private static BitField fNoLeading;
    private static BitField fMWSmallCaps;
    private short field_34_adt;
    private byte[] field_35_doptypography;
    private byte[] field_36_dogrid;
    private short field_37_docinfo5;
    private static BitField lvl;
    private static BitField fGramAllDone;
    private static BitField fGramAllClean;
    private static BitField fSubsetFonts;
    private static BitField fHideLastVersion;
    private static BitField fHtmlDoc;
    private static BitField fSnapBorder;
    private static BitField fIncludeHeader;
    private static BitField fIncludeFooter;
    private static BitField fForcePageSizePag;
    private static BitField fMinFontSizePag;
    private short field_38_docinfo6;
    private static BitField fHaveVersions;
    private static BitField fAutoVersions;
    private byte[] field_39_asumyi;
    private int field_40_cChWS;
    private int field_41_cChWSFtnEdn;
    private int field_42_grfDocEvents;
    private int field_43_virusinfo;
    private static BitField fVirusPrompted;
    private static BitField fVirusLoadSafe;
    private static BitField KeyVirusSession30;
    private byte[] field_44_Spare;
    private int field_45_reserved1;
    private int field_46_reserved2;
    private int field_47_cDBC;
    private int field_48_cDBCFtnEdn;
    private int field_49_reserved;
    private short field_50_nfcFtnRef;
    private short field_51_nfcEdnRef;
    private short field_52_hpsZoonFontPag;
    private short field_53_dywDispPag;
    
    protected void fillFields(final byte[] data, final short size, final int offset) {
        this.field_1_formatFlags = data[0 + offset];
        this.field_2_unused2 = data[1 + offset];
        this.field_3_footnoteInfo = LittleEndian.getShort(data, 2 + offset);
        this.field_4_fOutlineDirtySave = data[4 + offset];
        this.field_5_docinfo = data[5 + offset];
        this.field_6_docinfo1 = data[6 + offset];
        this.field_7_docinfo2 = data[7 + offset];
        this.field_8_docinfo3 = LittleEndian.getShort(data, 8 + offset);
        this.field_9_dxaTab = LittleEndian.getShort(data, 10 + offset);
        this.field_10_wSpare = LittleEndian.getShort(data, 12 + offset);
        this.field_11_dxaHotz = LittleEndian.getShort(data, 14 + offset);
        this.field_12_cConsexHypLim = LittleEndian.getShort(data, 16 + offset);
        this.field_13_wSpare2 = LittleEndian.getShort(data, 18 + offset);
        this.field_14_dttmCreated = LittleEndian.getInt(data, 20 + offset);
        this.field_15_dttmRevised = LittleEndian.getInt(data, 24 + offset);
        this.field_16_dttmLastPrint = LittleEndian.getInt(data, 28 + offset);
        this.field_17_nRevision = LittleEndian.getShort(data, 32 + offset);
        this.field_18_tmEdited = LittleEndian.getInt(data, 34 + offset);
        this.field_19_cWords = LittleEndian.getInt(data, 38 + offset);
        this.field_20_cCh = LittleEndian.getInt(data, 42 + offset);
        this.field_21_cPg = LittleEndian.getShort(data, 46 + offset);
        this.field_22_cParas = LittleEndian.getInt(data, 48 + offset);
        this.field_23_Edn = LittleEndian.getShort(data, 52 + offset);
        this.field_24_Edn1 = LittleEndian.getShort(data, 54 + offset);
        this.field_25_cLines = LittleEndian.getInt(data, 56 + offset);
        this.field_26_cWordsFtnEnd = LittleEndian.getInt(data, 60 + offset);
        this.field_27_cChFtnEdn = LittleEndian.getInt(data, 64 + offset);
        this.field_28_cPgFtnEdn = LittleEndian.getShort(data, 68 + offset);
        this.field_29_cParasFtnEdn = LittleEndian.getInt(data, 70 + offset);
        this.field_30_cLinesFtnEdn = LittleEndian.getInt(data, 74 + offset);
        this.field_31_lKeyProtDoc = LittleEndian.getInt(data, 78 + offset);
        this.field_32_view = LittleEndian.getShort(data, 82 + offset);
        this.field_33_docinfo4 = LittleEndian.getInt(data, 84 + offset);
        this.field_34_adt = LittleEndian.getShort(data, 88 + offset);
        this.field_35_doptypography = LittleEndian.getByteArray(data, 90 + offset, 310);
        this.field_36_dogrid = LittleEndian.getByteArray(data, 400 + offset, 10);
        this.field_37_docinfo5 = LittleEndian.getShort(data, 410 + offset);
        this.field_38_docinfo6 = LittleEndian.getShort(data, 412 + offset);
        this.field_39_asumyi = LittleEndian.getByteArray(data, 414 + offset, 12);
        this.field_40_cChWS = LittleEndian.getInt(data, 426 + offset);
        this.field_41_cChWSFtnEdn = LittleEndian.getInt(data, 430 + offset);
        this.field_42_grfDocEvents = LittleEndian.getInt(data, 434 + offset);
        this.field_43_virusinfo = LittleEndian.getInt(data, 438 + offset);
        this.field_44_Spare = LittleEndian.getByteArray(data, 442 + offset, 30);
        this.field_45_reserved1 = LittleEndian.getInt(data, 472 + offset);
        this.field_46_reserved2 = LittleEndian.getInt(data, 476 + offset);
        this.field_47_cDBC = LittleEndian.getInt(data, 480 + offset);
        this.field_48_cDBCFtnEdn = LittleEndian.getInt(data, 484 + offset);
        this.field_49_reserved = LittleEndian.getInt(data, 488 + offset);
        this.field_50_nfcFtnRef = LittleEndian.getShort(data, 492 + offset);
        this.field_51_nfcEdnRef = LittleEndian.getShort(data, 494 + offset);
        this.field_52_hpsZoonFontPag = LittleEndian.getShort(data, 496 + offset);
        this.field_53_dywDispPag = LittleEndian.getShort(data, 498 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        data[0 + offset] = this.field_1_formatFlags;
        data[1 + offset] = this.field_2_unused2;
        LittleEndian.putShort(data, 2 + offset, this.field_3_footnoteInfo);
        data[4 + offset] = this.field_4_fOutlineDirtySave;
        data[5 + offset] = this.field_5_docinfo;
        data[6 + offset] = this.field_6_docinfo1;
        data[7 + offset] = this.field_7_docinfo2;
        LittleEndian.putShort(data, 8 + offset, this.field_8_docinfo3);
        LittleEndian.putShort(data, 10 + offset, (short)this.field_9_dxaTab);
        LittleEndian.putShort(data, 12 + offset, (short)this.field_10_wSpare);
        LittleEndian.putShort(data, 14 + offset, (short)this.field_11_dxaHotz);
        LittleEndian.putShort(data, 16 + offset, (short)this.field_12_cConsexHypLim);
        LittleEndian.putShort(data, 18 + offset, (short)this.field_13_wSpare2);
        LittleEndian.putInt(data, 20 + offset, this.field_14_dttmCreated);
        LittleEndian.putInt(data, 24 + offset, this.field_15_dttmRevised);
        LittleEndian.putInt(data, 28 + offset, this.field_16_dttmLastPrint);
        LittleEndian.putShort(data, 32 + offset, (short)this.field_17_nRevision);
        LittleEndian.putInt(data, 34 + offset, this.field_18_tmEdited);
        LittleEndian.putInt(data, 38 + offset, this.field_19_cWords);
        LittleEndian.putInt(data, 42 + offset, this.field_20_cCh);
        LittleEndian.putShort(data, 46 + offset, (short)this.field_21_cPg);
        LittleEndian.putInt(data, 48 + offset, this.field_22_cParas);
        LittleEndian.putShort(data, 52 + offset, this.field_23_Edn);
        LittleEndian.putShort(data, 54 + offset, this.field_24_Edn1);
        LittleEndian.putInt(data, 56 + offset, this.field_25_cLines);
        LittleEndian.putInt(data, 60 + offset, this.field_26_cWordsFtnEnd);
        LittleEndian.putInt(data, 64 + offset, this.field_27_cChFtnEdn);
        LittleEndian.putShort(data, 68 + offset, this.field_28_cPgFtnEdn);
        LittleEndian.putInt(data, 70 + offset, this.field_29_cParasFtnEdn);
        LittleEndian.putInt(data, 74 + offset, this.field_30_cLinesFtnEdn);
        LittleEndian.putInt(data, 78 + offset, this.field_31_lKeyProtDoc);
        LittleEndian.putShort(data, 82 + offset, this.field_32_view);
        LittleEndian.putInt(data, 84 + offset, this.field_33_docinfo4);
        LittleEndian.putShort(data, 88 + offset, this.field_34_adt);
        LittleEndian.putShort(data, 410 + offset, this.field_37_docinfo5);
        LittleEndian.putShort(data, 412 + offset, this.field_38_docinfo6);
        LittleEndian.putInt(data, 426 + offset, this.field_40_cChWS);
        LittleEndian.putInt(data, 430 + offset, this.field_41_cChWSFtnEdn);
        LittleEndian.putInt(data, 434 + offset, this.field_42_grfDocEvents);
        LittleEndian.putInt(data, 438 + offset, this.field_43_virusinfo);
        LittleEndian.putInt(data, 472 + offset, this.field_45_reserved1);
        LittleEndian.putInt(data, 476 + offset, this.field_46_reserved2);
        LittleEndian.putInt(data, 480 + offset, this.field_47_cDBC);
        LittleEndian.putInt(data, 484 + offset, this.field_48_cDBCFtnEdn);
        LittleEndian.putInt(data, 488 + offset, this.field_49_reserved);
        LittleEndian.putShort(data, 492 + offset, this.field_50_nfcFtnRef);
        LittleEndian.putShort(data, 494 + offset, this.field_51_nfcEdnRef);
        LittleEndian.putShort(data, 496 + offset, this.field_52_hpsZoonFontPag);
        LittleEndian.putShort(data, 498 + offset, this.field_53_dywDispPag);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[DOP]\n");
        buffer.append("    .formatFlags          = ");
        buffer.append(HexDump.byteToHex(this.getFormatFlags()));
        buffer.append(" (").append(this.getFormatFlags()).append(" )\n");
        buffer.append("         .fFacingPages             = ").append(this.isFFacingPages()).append('\n');
        buffer.append("         .fWidowControl            = ").append(this.isFWidowControl()).append('\n');
        buffer.append("         .fPMHMainDoc              = ").append(this.isFPMHMainDoc()).append('\n');
        buffer.append("         .grfSupression            = ").append(this.getGrfSupression()).append('\n');
        buffer.append("         .fpc                      = ").append(this.getFpc()).append('\n');
        buffer.append("         .unused1                  = ").append(this.isUnused1()).append('\n');
        buffer.append("    .unused2              = ");
        buffer.append(HexDump.byteToHex(this.getUnused2()));
        buffer.append(" (").append(this.getUnused2()).append(" )\n");
        buffer.append("    .footnoteInfo         = ");
        buffer.append(HexDump.shortToHex(this.getFootnoteInfo()));
        buffer.append(" (").append(this.getFootnoteInfo()).append(" )\n");
        buffer.append("         .rncFtn                   = ").append(this.getRncFtn()).append('\n');
        buffer.append("         .nFtn                     = ").append(this.getNFtn()).append('\n');
        buffer.append("    .fOutlineDirtySave    = ");
        buffer.append(HexDump.byteToHex(this.getFOutlineDirtySave()));
        buffer.append(" (").append(this.getFOutlineDirtySave()).append(" )\n");
        buffer.append("    .docinfo              = ");
        buffer.append(HexDump.byteToHex(this.getDocinfo()));
        buffer.append(" (").append(this.getDocinfo()).append(" )\n");
        buffer.append("         .fOnlyMacPics             = ").append(this.isFOnlyMacPics()).append('\n');
        buffer.append("         .fOnlyWinPics             = ").append(this.isFOnlyWinPics()).append('\n');
        buffer.append("         .fLabelDoc                = ").append(this.isFLabelDoc()).append('\n');
        buffer.append("         .fHyphCapitals            = ").append(this.isFHyphCapitals()).append('\n');
        buffer.append("         .fAutoHyphen              = ").append(this.isFAutoHyphen()).append('\n');
        buffer.append("         .fFormNoFields            = ").append(this.isFFormNoFields()).append('\n');
        buffer.append("         .fLinkStyles              = ").append(this.isFLinkStyles()).append('\n');
        buffer.append("         .fRevMarking              = ").append(this.isFRevMarking()).append('\n');
        buffer.append("    .docinfo1             = ");
        buffer.append(HexDump.byteToHex(this.getDocinfo1()));
        buffer.append(" (").append(this.getDocinfo1()).append(" )\n");
        buffer.append("         .fBackup                  = ").append(this.isFBackup()).append('\n');
        buffer.append("         .fExactCWords             = ").append(this.isFExactCWords()).append('\n');
        buffer.append("         .fPagHidden               = ").append(this.isFPagHidden()).append('\n');
        buffer.append("         .fPagResults              = ").append(this.isFPagResults()).append('\n');
        buffer.append("         .fLockAtn                 = ").append(this.isFLockAtn()).append('\n');
        buffer.append("         .fMirrorMargins           = ").append(this.isFMirrorMargins()).append('\n');
        buffer.append("         .unused3                  = ").append(this.isUnused3()).append('\n');
        buffer.append("         .fDfltTrueType            = ").append(this.isFDfltTrueType()).append('\n');
        buffer.append("    .docinfo2             = ");
        buffer.append(HexDump.byteToHex(this.getDocinfo2()));
        buffer.append(" (").append(this.getDocinfo2()).append(" )\n");
        buffer.append("         .fPagSupressTopSpacing     = ").append(this.isFPagSupressTopSpacing()).append('\n');
        buffer.append("         .fProtEnabled             = ").append(this.isFProtEnabled()).append('\n');
        buffer.append("         .fDispFormFldSel          = ").append(this.isFDispFormFldSel()).append('\n');
        buffer.append("         .fRMView                  = ").append(this.isFRMView()).append('\n');
        buffer.append("         .fRMPrint                 = ").append(this.isFRMPrint()).append('\n');
        buffer.append("         .unused4                  = ").append(this.isUnused4()).append('\n');
        buffer.append("         .fLockRev                 = ").append(this.isFLockRev()).append('\n');
        buffer.append("         .fEmbedFonts              = ").append(this.isFEmbedFonts()).append('\n');
        buffer.append("    .docinfo3             = ");
        buffer.append(HexDump.shortToHex(this.getDocinfo3()));
        buffer.append(" (").append(this.getDocinfo3()).append(" )\n");
        buffer.append("         .oldfNoTabForInd          = ").append(this.isOldfNoTabForInd()).append('\n');
        buffer.append("         .oldfNoSpaceRaiseLower     = ").append(this.isOldfNoSpaceRaiseLower()).append('\n');
        buffer.append("         .oldfSuppressSpbfAfterPageBreak     = ").append(this.isOldfSuppressSpbfAfterPageBreak()).append('\n');
        buffer.append("         .oldfWrapTrailSpaces      = ").append(this.isOldfWrapTrailSpaces()).append('\n');
        buffer.append("         .oldfMapPrintTextColor     = ").append(this.isOldfMapPrintTextColor()).append('\n');
        buffer.append("         .oldfNoColumnBalance      = ").append(this.isOldfNoColumnBalance()).append('\n');
        buffer.append("         .oldfConvMailMergeEsc     = ").append(this.isOldfConvMailMergeEsc()).append('\n');
        buffer.append("         .oldfSupressTopSpacing     = ").append(this.isOldfSupressTopSpacing()).append('\n');
        buffer.append("         .oldfOrigWordTableRules     = ").append(this.isOldfOrigWordTableRules()).append('\n');
        buffer.append("         .oldfTransparentMetafiles     = ").append(this.isOldfTransparentMetafiles()).append('\n');
        buffer.append("         .oldfShowBreaksInFrames     = ").append(this.isOldfShowBreaksInFrames()).append('\n');
        buffer.append("         .oldfSwapBordersFacingPgs     = ").append(this.isOldfSwapBordersFacingPgs()).append('\n');
        buffer.append("         .unused5                  = ").append(this.getUnused5()).append('\n');
        buffer.append("    .dxaTab               = ");
        buffer.append(HexDump.intToHex(this.getDxaTab()));
        buffer.append(" (").append(this.getDxaTab()).append(" )\n");
        buffer.append("    .wSpare               = ");
        buffer.append(HexDump.intToHex(this.getWSpare()));
        buffer.append(" (").append(this.getWSpare()).append(" )\n");
        buffer.append("    .dxaHotz              = ");
        buffer.append(HexDump.intToHex(this.getDxaHotz()));
        buffer.append(" (").append(this.getDxaHotz()).append(" )\n");
        buffer.append("    .cConsexHypLim        = ");
        buffer.append(HexDump.intToHex(this.getCConsexHypLim()));
        buffer.append(" (").append(this.getCConsexHypLim()).append(" )\n");
        buffer.append("    .wSpare2              = ");
        buffer.append(HexDump.intToHex(this.getWSpare2()));
        buffer.append(" (").append(this.getWSpare2()).append(" )\n");
        buffer.append("    .dttmCreated          = ");
        buffer.append(HexDump.intToHex(this.getDttmCreated()));
        buffer.append(" (").append(this.getDttmCreated()).append(" )\n");
        buffer.append("    .dttmRevised          = ");
        buffer.append(HexDump.intToHex(this.getDttmRevised()));
        buffer.append(" (").append(this.getDttmRevised()).append(" )\n");
        buffer.append("    .dttmLastPrint        = ");
        buffer.append(HexDump.intToHex(this.getDttmLastPrint()));
        buffer.append(" (").append(this.getDttmLastPrint()).append(" )\n");
        buffer.append("    .nRevision            = ");
        buffer.append(HexDump.intToHex(this.getNRevision()));
        buffer.append(" (").append(this.getNRevision()).append(" )\n");
        buffer.append("    .tmEdited             = ");
        buffer.append(HexDump.intToHex(this.getTmEdited()));
        buffer.append(" (").append(this.getTmEdited()).append(" )\n");
        buffer.append("    .cWords               = ");
        buffer.append(HexDump.intToHex(this.getCWords()));
        buffer.append(" (").append(this.getCWords()).append(" )\n");
        buffer.append("    .cCh                  = ");
        buffer.append(HexDump.intToHex(this.getCCh()));
        buffer.append(" (").append(this.getCCh()).append(" )\n");
        buffer.append("    .cPg                  = ");
        buffer.append(HexDump.intToHex(this.getCPg()));
        buffer.append(" (").append(this.getCPg()).append(" )\n");
        buffer.append("    .cParas               = ");
        buffer.append(HexDump.intToHex(this.getCParas()));
        buffer.append(" (").append(this.getCParas()).append(" )\n");
        buffer.append("    .Edn                  = ");
        buffer.append(HexDump.shortToHex(this.getEdn()));
        buffer.append(" (").append(this.getEdn()).append(" )\n");
        buffer.append("         .rncEdn                   = ").append(this.getRncEdn()).append('\n');
        buffer.append("         .nEdn                     = ").append(this.getNEdn()).append('\n');
        buffer.append("    .Edn1                 = ");
        buffer.append(HexDump.shortToHex(this.getEdn1()));
        buffer.append(" (").append(this.getEdn1()).append(" )\n");
        buffer.append("         .epc                      = ").append(this.getEpc()).append('\n');
        buffer.append("         .nfcFtnRef1               = ").append(this.getNfcFtnRef1()).append('\n');
        buffer.append("         .nfcEdnRef1               = ").append(this.getNfcEdnRef1()).append('\n');
        buffer.append("         .fPrintFormData           = ").append(this.isFPrintFormData()).append('\n');
        buffer.append("         .fSaveFormData            = ").append(this.isFSaveFormData()).append('\n');
        buffer.append("         .fShadeFormData           = ").append(this.isFShadeFormData()).append('\n');
        buffer.append("         .fWCFtnEdn                = ").append(this.isFWCFtnEdn()).append('\n');
        buffer.append("    .cLines               = ");
        buffer.append(HexDump.intToHex(this.getCLines()));
        buffer.append(" (").append(this.getCLines()).append(" )\n");
        buffer.append("    .cWordsFtnEnd         = ");
        buffer.append(HexDump.intToHex(this.getCWordsFtnEnd()));
        buffer.append(" (").append(this.getCWordsFtnEnd()).append(" )\n");
        buffer.append("    .cChFtnEdn            = ");
        buffer.append(HexDump.intToHex(this.getCChFtnEdn()));
        buffer.append(" (").append(this.getCChFtnEdn()).append(" )\n");
        buffer.append("    .cPgFtnEdn            = ");
        buffer.append(HexDump.shortToHex(this.getCPgFtnEdn()));
        buffer.append(" (").append(this.getCPgFtnEdn()).append(" )\n");
        buffer.append("    .cParasFtnEdn         = ");
        buffer.append(HexDump.intToHex(this.getCParasFtnEdn()));
        buffer.append(" (").append(this.getCParasFtnEdn()).append(" )\n");
        buffer.append("    .cLinesFtnEdn         = ");
        buffer.append(HexDump.intToHex(this.getCLinesFtnEdn()));
        buffer.append(" (").append(this.getCLinesFtnEdn()).append(" )\n");
        buffer.append("    .lKeyProtDoc          = ");
        buffer.append(HexDump.intToHex(this.getLKeyProtDoc()));
        buffer.append(" (").append(this.getLKeyProtDoc()).append(" )\n");
        buffer.append("    .view                 = ");
        buffer.append(HexDump.shortToHex(this.getView()));
        buffer.append(" (").append(this.getView()).append(" )\n");
        buffer.append("         .wvkSaved                 = ").append(this.getWvkSaved()).append('\n');
        buffer.append("         .wScaleSaved              = ").append(this.getWScaleSaved()).append('\n');
        buffer.append("         .zkSaved                  = ").append(this.getZkSaved()).append('\n');
        buffer.append("         .fRotateFontW6            = ").append(this.isFRotateFontW6()).append('\n');
        buffer.append("         .iGutterPos               = ").append(this.isIGutterPos()).append('\n');
        buffer.append("    .docinfo4             = ");
        buffer.append(HexDump.intToHex(this.getDocinfo4()));
        buffer.append(" (").append(this.getDocinfo4()).append(" )\n");
        buffer.append("         .fNoTabForInd             = ").append(this.isFNoTabForInd()).append('\n');
        buffer.append("         .fNoSpaceRaiseLower       = ").append(this.isFNoSpaceRaiseLower()).append('\n');
        buffer.append("         .fSupressSpdfAfterPageBreak     = ").append(this.isFSupressSpdfAfterPageBreak()).append('\n');
        buffer.append("         .fWrapTrailSpaces         = ").append(this.isFWrapTrailSpaces()).append('\n');
        buffer.append("         .fMapPrintTextColor       = ").append(this.isFMapPrintTextColor()).append('\n');
        buffer.append("         .fNoColumnBalance         = ").append(this.isFNoColumnBalance()).append('\n');
        buffer.append("         .fConvMailMergeEsc        = ").append(this.isFConvMailMergeEsc()).append('\n');
        buffer.append("         .fSupressTopSpacing       = ").append(this.isFSupressTopSpacing()).append('\n');
        buffer.append("         .fOrigWordTableRules      = ").append(this.isFOrigWordTableRules()).append('\n');
        buffer.append("         .fTransparentMetafiles     = ").append(this.isFTransparentMetafiles()).append('\n');
        buffer.append("         .fShowBreaksInFrames      = ").append(this.isFShowBreaksInFrames()).append('\n');
        buffer.append("         .fSwapBordersFacingPgs     = ").append(this.isFSwapBordersFacingPgs()).append('\n');
        buffer.append("         .fSuppressTopSPacingMac5     = ").append(this.isFSuppressTopSPacingMac5()).append('\n');
        buffer.append("         .fTruncDxaExpand          = ").append(this.isFTruncDxaExpand()).append('\n');
        buffer.append("         .fPrintBodyBeforeHdr      = ").append(this.isFPrintBodyBeforeHdr()).append('\n');
        buffer.append("         .fNoLeading               = ").append(this.isFNoLeading()).append('\n');
        buffer.append("         .fMWSmallCaps             = ").append(this.isFMWSmallCaps()).append('\n');
        buffer.append("    .adt                  = ");
        buffer.append(HexDump.shortToHex(this.getAdt()));
        buffer.append(" (").append(this.getAdt()).append(" )\n");
        buffer.append("    .doptypography        = ");
        buffer.append(HexDump.toHex(this.getDoptypography()));
        buffer.append(" (").append(this.getDoptypography()).append(" )\n");
        buffer.append("    .dogrid               = ");
        buffer.append(HexDump.toHex(this.getDogrid()));
        buffer.append(" (").append(this.getDogrid()).append(" )\n");
        buffer.append("    .docinfo5             = ");
        buffer.append(HexDump.shortToHex(this.getDocinfo5()));
        buffer.append(" (").append(this.getDocinfo5()).append(" )\n");
        buffer.append("         .lvl                      = ").append(this.getLvl()).append('\n');
        buffer.append("         .fGramAllDone             = ").append(this.isFGramAllDone()).append('\n');
        buffer.append("         .fGramAllClean            = ").append(this.isFGramAllClean()).append('\n');
        buffer.append("         .fSubsetFonts             = ").append(this.isFSubsetFonts()).append('\n');
        buffer.append("         .fHideLastVersion         = ").append(this.isFHideLastVersion()).append('\n');
        buffer.append("         .fHtmlDoc                 = ").append(this.isFHtmlDoc()).append('\n');
        buffer.append("         .fSnapBorder              = ").append(this.isFSnapBorder()).append('\n');
        buffer.append("         .fIncludeHeader           = ").append(this.isFIncludeHeader()).append('\n');
        buffer.append("         .fIncludeFooter           = ").append(this.isFIncludeFooter()).append('\n');
        buffer.append("         .fForcePageSizePag        = ").append(this.isFForcePageSizePag()).append('\n');
        buffer.append("         .fMinFontSizePag          = ").append(this.isFMinFontSizePag()).append('\n');
        buffer.append("    .docinfo6             = ");
        buffer.append(HexDump.shortToHex(this.getDocinfo6()));
        buffer.append(" (").append(this.getDocinfo6()).append(" )\n");
        buffer.append("         .fHaveVersions            = ").append(this.isFHaveVersions()).append('\n');
        buffer.append("         .fAutoVersions            = ").append(this.isFAutoVersions()).append('\n');
        buffer.append("    .asumyi               = ");
        buffer.append(HexDump.toHex(this.getAsumyi()));
        buffer.append(" (").append(this.getAsumyi()).append(" )\n");
        buffer.append("    .cChWS                = ");
        buffer.append(HexDump.intToHex(this.getCChWS()));
        buffer.append(" (").append(this.getCChWS()).append(" )\n");
        buffer.append("    .cChWSFtnEdn          = ");
        buffer.append(HexDump.intToHex(this.getCChWSFtnEdn()));
        buffer.append(" (").append(this.getCChWSFtnEdn()).append(" )\n");
        buffer.append("    .grfDocEvents         = ");
        buffer.append(HexDump.intToHex(this.getGrfDocEvents()));
        buffer.append(" (").append(this.getGrfDocEvents()).append(" )\n");
        buffer.append("    .virusinfo            = ");
        buffer.append(HexDump.intToHex(this.getVirusinfo()));
        buffer.append(" (").append(this.getVirusinfo()).append(" )\n");
        buffer.append("         .fVirusPrompted           = ").append(this.isFVirusPrompted()).append('\n');
        buffer.append("         .fVirusLoadSafe           = ").append(this.isFVirusLoadSafe()).append('\n');
        buffer.append("         .KeyVirusSession30        = ").append(this.getKeyVirusSession30()).append('\n');
        buffer.append("    .Spare                = ");
        buffer.append(HexDump.toHex(this.getSpare()));
        buffer.append(" (").append(this.getSpare()).append(" )\n");
        buffer.append("    .reserved1            = ");
        buffer.append(HexDump.intToHex(this.getReserved1()));
        buffer.append(" (").append(this.getReserved1()).append(" )\n");
        buffer.append("    .reserved2            = ");
        buffer.append(HexDump.intToHex(this.getReserved2()));
        buffer.append(" (").append(this.getReserved2()).append(" )\n");
        buffer.append("    .cDBC                 = ");
        buffer.append(HexDump.intToHex(this.getCDBC()));
        buffer.append(" (").append(this.getCDBC()).append(" )\n");
        buffer.append("    .cDBCFtnEdn           = ");
        buffer.append(HexDump.intToHex(this.getCDBCFtnEdn()));
        buffer.append(" (").append(this.getCDBCFtnEdn()).append(" )\n");
        buffer.append("    .reserved             = ");
        buffer.append(HexDump.intToHex(this.getReserved()));
        buffer.append(" (").append(this.getReserved()).append(" )\n");
        buffer.append("    .nfcFtnRef            = ");
        buffer.append(HexDump.shortToHex(this.getNfcFtnRef()));
        buffer.append(" (").append(this.getNfcFtnRef()).append(" )\n");
        buffer.append("    .nfcEdnRef            = ");
        buffer.append(HexDump.shortToHex(this.getNfcEdnRef()));
        buffer.append(" (").append(this.getNfcEdnRef()).append(" )\n");
        buffer.append("    .hpsZoonFontPag       = ");
        buffer.append(HexDump.shortToHex(this.getHpsZoonFontPag()));
        buffer.append(" (").append(this.getHpsZoonFontPag()).append(" )\n");
        buffer.append("    .dywDispPag           = ");
        buffer.append(HexDump.shortToHex(this.getDywDispPag()));
        buffer.append(" (").append(this.getDywDispPag()).append(" )\n");
        buffer.append("[/DOP]\n");
        return buffer.toString();
    }
    
    public int getSize() {
        return 504;
    }
    
    public byte getFormatFlags() {
        return this.field_1_formatFlags;
    }
    
    public void setFormatFlags(final byte field_1_formatFlags) {
        this.field_1_formatFlags = field_1_formatFlags;
    }
    
    public byte getUnused2() {
        return this.field_2_unused2;
    }
    
    public void setUnused2(final byte field_2_unused2) {
        this.field_2_unused2 = field_2_unused2;
    }
    
    public short getFootnoteInfo() {
        return this.field_3_footnoteInfo;
    }
    
    public void setFootnoteInfo(final short field_3_footnoteInfo) {
        this.field_3_footnoteInfo = field_3_footnoteInfo;
    }
    
    public byte getFOutlineDirtySave() {
        return this.field_4_fOutlineDirtySave;
    }
    
    public void setFOutlineDirtySave(final byte field_4_fOutlineDirtySave) {
        this.field_4_fOutlineDirtySave = field_4_fOutlineDirtySave;
    }
    
    public byte getDocinfo() {
        return this.field_5_docinfo;
    }
    
    public void setDocinfo(final byte field_5_docinfo) {
        this.field_5_docinfo = field_5_docinfo;
    }
    
    public byte getDocinfo1() {
        return this.field_6_docinfo1;
    }
    
    public void setDocinfo1(final byte field_6_docinfo1) {
        this.field_6_docinfo1 = field_6_docinfo1;
    }
    
    public byte getDocinfo2() {
        return this.field_7_docinfo2;
    }
    
    public void setDocinfo2(final byte field_7_docinfo2) {
        this.field_7_docinfo2 = field_7_docinfo2;
    }
    
    public short getDocinfo3() {
        return this.field_8_docinfo3;
    }
    
    public void setDocinfo3(final short field_8_docinfo3) {
        this.field_8_docinfo3 = field_8_docinfo3;
    }
    
    public int getDxaTab() {
        return this.field_9_dxaTab;
    }
    
    public void setDxaTab(final int field_9_dxaTab) {
        this.field_9_dxaTab = field_9_dxaTab;
    }
    
    public int getWSpare() {
        return this.field_10_wSpare;
    }
    
    public void setWSpare(final int field_10_wSpare) {
        this.field_10_wSpare = field_10_wSpare;
    }
    
    public int getDxaHotz() {
        return this.field_11_dxaHotz;
    }
    
    public void setDxaHotz(final int field_11_dxaHotz) {
        this.field_11_dxaHotz = field_11_dxaHotz;
    }
    
    public int getCConsexHypLim() {
        return this.field_12_cConsexHypLim;
    }
    
    public void setCConsexHypLim(final int field_12_cConsexHypLim) {
        this.field_12_cConsexHypLim = field_12_cConsexHypLim;
    }
    
    public int getWSpare2() {
        return this.field_13_wSpare2;
    }
    
    public void setWSpare2(final int field_13_wSpare2) {
        this.field_13_wSpare2 = field_13_wSpare2;
    }
    
    public int getDttmCreated() {
        return this.field_14_dttmCreated;
    }
    
    public void setDttmCreated(final int field_14_dttmCreated) {
        this.field_14_dttmCreated = field_14_dttmCreated;
    }
    
    public int getDttmRevised() {
        return this.field_15_dttmRevised;
    }
    
    public void setDttmRevised(final int field_15_dttmRevised) {
        this.field_15_dttmRevised = field_15_dttmRevised;
    }
    
    public int getDttmLastPrint() {
        return this.field_16_dttmLastPrint;
    }
    
    public void setDttmLastPrint(final int field_16_dttmLastPrint) {
        this.field_16_dttmLastPrint = field_16_dttmLastPrint;
    }
    
    public int getNRevision() {
        return this.field_17_nRevision;
    }
    
    public void setNRevision(final int field_17_nRevision) {
        this.field_17_nRevision = field_17_nRevision;
    }
    
    public int getTmEdited() {
        return this.field_18_tmEdited;
    }
    
    public void setTmEdited(final int field_18_tmEdited) {
        this.field_18_tmEdited = field_18_tmEdited;
    }
    
    public int getCWords() {
        return this.field_19_cWords;
    }
    
    public void setCWords(final int field_19_cWords) {
        this.field_19_cWords = field_19_cWords;
    }
    
    public int getCCh() {
        return this.field_20_cCh;
    }
    
    public void setCCh(final int field_20_cCh) {
        this.field_20_cCh = field_20_cCh;
    }
    
    public int getCPg() {
        return this.field_21_cPg;
    }
    
    public void setCPg(final int field_21_cPg) {
        this.field_21_cPg = field_21_cPg;
    }
    
    public int getCParas() {
        return this.field_22_cParas;
    }
    
    public void setCParas(final int field_22_cParas) {
        this.field_22_cParas = field_22_cParas;
    }
    
    public short getEdn() {
        return this.field_23_Edn;
    }
    
    public void setEdn(final short field_23_Edn) {
        this.field_23_Edn = field_23_Edn;
    }
    
    public short getEdn1() {
        return this.field_24_Edn1;
    }
    
    public void setEdn1(final short field_24_Edn1) {
        this.field_24_Edn1 = field_24_Edn1;
    }
    
    public int getCLines() {
        return this.field_25_cLines;
    }
    
    public void setCLines(final int field_25_cLines) {
        this.field_25_cLines = field_25_cLines;
    }
    
    public int getCWordsFtnEnd() {
        return this.field_26_cWordsFtnEnd;
    }
    
    public void setCWordsFtnEnd(final int field_26_cWordsFtnEnd) {
        this.field_26_cWordsFtnEnd = field_26_cWordsFtnEnd;
    }
    
    public int getCChFtnEdn() {
        return this.field_27_cChFtnEdn;
    }
    
    public void setCChFtnEdn(final int field_27_cChFtnEdn) {
        this.field_27_cChFtnEdn = field_27_cChFtnEdn;
    }
    
    public short getCPgFtnEdn() {
        return this.field_28_cPgFtnEdn;
    }
    
    public void setCPgFtnEdn(final short field_28_cPgFtnEdn) {
        this.field_28_cPgFtnEdn = field_28_cPgFtnEdn;
    }
    
    public int getCParasFtnEdn() {
        return this.field_29_cParasFtnEdn;
    }
    
    public void setCParasFtnEdn(final int field_29_cParasFtnEdn) {
        this.field_29_cParasFtnEdn = field_29_cParasFtnEdn;
    }
    
    public int getCLinesFtnEdn() {
        return this.field_30_cLinesFtnEdn;
    }
    
    public void setCLinesFtnEdn(final int field_30_cLinesFtnEdn) {
        this.field_30_cLinesFtnEdn = field_30_cLinesFtnEdn;
    }
    
    public int getLKeyProtDoc() {
        return this.field_31_lKeyProtDoc;
    }
    
    public void setLKeyProtDoc(final int field_31_lKeyProtDoc) {
        this.field_31_lKeyProtDoc = field_31_lKeyProtDoc;
    }
    
    public short getView() {
        return this.field_32_view;
    }
    
    public void setView(final short field_32_view) {
        this.field_32_view = field_32_view;
    }
    
    public int getDocinfo4() {
        return this.field_33_docinfo4;
    }
    
    public void setDocinfo4(final int field_33_docinfo4) {
        this.field_33_docinfo4 = field_33_docinfo4;
    }
    
    public short getAdt() {
        return this.field_34_adt;
    }
    
    public void setAdt(final short field_34_adt) {
        this.field_34_adt = field_34_adt;
    }
    
    public byte[] getDoptypography() {
        return this.field_35_doptypography;
    }
    
    public void setDoptypography(final byte[] field_35_doptypography) {
        this.field_35_doptypography = field_35_doptypography;
    }
    
    public byte[] getDogrid() {
        return this.field_36_dogrid;
    }
    
    public void setDogrid(final byte[] field_36_dogrid) {
        this.field_36_dogrid = field_36_dogrid;
    }
    
    public short getDocinfo5() {
        return this.field_37_docinfo5;
    }
    
    public void setDocinfo5(final short field_37_docinfo5) {
        this.field_37_docinfo5 = field_37_docinfo5;
    }
    
    public short getDocinfo6() {
        return this.field_38_docinfo6;
    }
    
    public void setDocinfo6(final short field_38_docinfo6) {
        this.field_38_docinfo6 = field_38_docinfo6;
    }
    
    public byte[] getAsumyi() {
        return this.field_39_asumyi;
    }
    
    public void setAsumyi(final byte[] field_39_asumyi) {
        this.field_39_asumyi = field_39_asumyi;
    }
    
    public int getCChWS() {
        return this.field_40_cChWS;
    }
    
    public void setCChWS(final int field_40_cChWS) {
        this.field_40_cChWS = field_40_cChWS;
    }
    
    public int getCChWSFtnEdn() {
        return this.field_41_cChWSFtnEdn;
    }
    
    public void setCChWSFtnEdn(final int field_41_cChWSFtnEdn) {
        this.field_41_cChWSFtnEdn = field_41_cChWSFtnEdn;
    }
    
    public int getGrfDocEvents() {
        return this.field_42_grfDocEvents;
    }
    
    public void setGrfDocEvents(final int field_42_grfDocEvents) {
        this.field_42_grfDocEvents = field_42_grfDocEvents;
    }
    
    public int getVirusinfo() {
        return this.field_43_virusinfo;
    }
    
    public void setVirusinfo(final int field_43_virusinfo) {
        this.field_43_virusinfo = field_43_virusinfo;
    }
    
    public byte[] getSpare() {
        return this.field_44_Spare;
    }
    
    public void setSpare(final byte[] field_44_Spare) {
        this.field_44_Spare = field_44_Spare;
    }
    
    public int getReserved1() {
        return this.field_45_reserved1;
    }
    
    public void setReserved1(final int field_45_reserved1) {
        this.field_45_reserved1 = field_45_reserved1;
    }
    
    public int getReserved2() {
        return this.field_46_reserved2;
    }
    
    public void setReserved2(final int field_46_reserved2) {
        this.field_46_reserved2 = field_46_reserved2;
    }
    
    public int getCDBC() {
        return this.field_47_cDBC;
    }
    
    public void setCDBC(final int field_47_cDBC) {
        this.field_47_cDBC = field_47_cDBC;
    }
    
    public int getCDBCFtnEdn() {
        return this.field_48_cDBCFtnEdn;
    }
    
    public void setCDBCFtnEdn(final int field_48_cDBCFtnEdn) {
        this.field_48_cDBCFtnEdn = field_48_cDBCFtnEdn;
    }
    
    public int getReserved() {
        return this.field_49_reserved;
    }
    
    public void setReserved(final int field_49_reserved) {
        this.field_49_reserved = field_49_reserved;
    }
    
    public short getNfcFtnRef() {
        return this.field_50_nfcFtnRef;
    }
    
    public void setNfcFtnRef(final short field_50_nfcFtnRef) {
        this.field_50_nfcFtnRef = field_50_nfcFtnRef;
    }
    
    public short getNfcEdnRef() {
        return this.field_51_nfcEdnRef;
    }
    
    public void setNfcEdnRef(final short field_51_nfcEdnRef) {
        this.field_51_nfcEdnRef = field_51_nfcEdnRef;
    }
    
    public short getHpsZoonFontPag() {
        return this.field_52_hpsZoonFontPag;
    }
    
    public void setHpsZoonFontPag(final short field_52_hpsZoonFontPag) {
        this.field_52_hpsZoonFontPag = field_52_hpsZoonFontPag;
    }
    
    public short getDywDispPag() {
        return this.field_53_dywDispPag;
    }
    
    public void setDywDispPag(final short field_53_dywDispPag) {
        this.field_53_dywDispPag = field_53_dywDispPag;
    }
    
    public void setFFacingPages(final boolean value) {
        this.field_1_formatFlags = (byte)DOPAbstractType.fFacingPages.setBoolean(this.field_1_formatFlags, value);
    }
    
    public boolean isFFacingPages() {
        return DOPAbstractType.fFacingPages.isSet(this.field_1_formatFlags);
    }
    
    public void setFWidowControl(final boolean value) {
        this.field_1_formatFlags = (byte)DOPAbstractType.fWidowControl.setBoolean(this.field_1_formatFlags, value);
    }
    
    public boolean isFWidowControl() {
        return DOPAbstractType.fWidowControl.isSet(this.field_1_formatFlags);
    }
    
    public void setFPMHMainDoc(final boolean value) {
        this.field_1_formatFlags = (byte)DOPAbstractType.fPMHMainDoc.setBoolean(this.field_1_formatFlags, value);
    }
    
    public boolean isFPMHMainDoc() {
        return DOPAbstractType.fPMHMainDoc.isSet(this.field_1_formatFlags);
    }
    
    public void setGrfSupression(final byte value) {
        this.field_1_formatFlags = (byte)DOPAbstractType.grfSupression.setValue(this.field_1_formatFlags, value);
    }
    
    public byte getGrfSupression() {
        return (byte)DOPAbstractType.grfSupression.getValue(this.field_1_formatFlags);
    }
    
    public void setFpc(final byte value) {
        this.field_1_formatFlags = (byte)DOPAbstractType.fpc.setValue(this.field_1_formatFlags, value);
    }
    
    public byte getFpc() {
        return (byte)DOPAbstractType.fpc.getValue(this.field_1_formatFlags);
    }
    
    public void setUnused1(final boolean value) {
        this.field_1_formatFlags = (byte)DOPAbstractType.unused1.setBoolean(this.field_1_formatFlags, value);
    }
    
    public boolean isUnused1() {
        return DOPAbstractType.unused1.isSet(this.field_1_formatFlags);
    }
    
    public void setRncFtn(final byte value) {
        this.field_3_footnoteInfo = (short)DOPAbstractType.rncFtn.setValue(this.field_3_footnoteInfo, value);
    }
    
    public byte getRncFtn() {
        return (byte)DOPAbstractType.rncFtn.getValue(this.field_3_footnoteInfo);
    }
    
    public void setNFtn(final short value) {
        this.field_3_footnoteInfo = (short)DOPAbstractType.nFtn.setValue(this.field_3_footnoteInfo, value);
    }
    
    public short getNFtn() {
        return (short)DOPAbstractType.nFtn.getValue(this.field_3_footnoteInfo);
    }
    
    public void setFOnlyMacPics(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fOnlyMacPics.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFOnlyMacPics() {
        return DOPAbstractType.fOnlyMacPics.isSet(this.field_5_docinfo);
    }
    
    public void setFOnlyWinPics(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fOnlyWinPics.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFOnlyWinPics() {
        return DOPAbstractType.fOnlyWinPics.isSet(this.field_5_docinfo);
    }
    
    public void setFLabelDoc(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fLabelDoc.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFLabelDoc() {
        return DOPAbstractType.fLabelDoc.isSet(this.field_5_docinfo);
    }
    
    public void setFHyphCapitals(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fHyphCapitals.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFHyphCapitals() {
        return DOPAbstractType.fHyphCapitals.isSet(this.field_5_docinfo);
    }
    
    public void setFAutoHyphen(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fAutoHyphen.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFAutoHyphen() {
        return DOPAbstractType.fAutoHyphen.isSet(this.field_5_docinfo);
    }
    
    public void setFFormNoFields(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fFormNoFields.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFFormNoFields() {
        return DOPAbstractType.fFormNoFields.isSet(this.field_5_docinfo);
    }
    
    public void setFLinkStyles(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fLinkStyles.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFLinkStyles() {
        return DOPAbstractType.fLinkStyles.isSet(this.field_5_docinfo);
    }
    
    public void setFRevMarking(final boolean value) {
        this.field_5_docinfo = (byte)DOPAbstractType.fRevMarking.setBoolean(this.field_5_docinfo, value);
    }
    
    public boolean isFRevMarking() {
        return DOPAbstractType.fRevMarking.isSet(this.field_5_docinfo);
    }
    
    public void setFBackup(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.fBackup.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isFBackup() {
        return DOPAbstractType.fBackup.isSet(this.field_6_docinfo1);
    }
    
    public void setFExactCWords(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.fExactCWords.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isFExactCWords() {
        return DOPAbstractType.fExactCWords.isSet(this.field_6_docinfo1);
    }
    
    public void setFPagHidden(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.fPagHidden.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isFPagHidden() {
        return DOPAbstractType.fPagHidden.isSet(this.field_6_docinfo1);
    }
    
    public void setFPagResults(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.fPagResults.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isFPagResults() {
        return DOPAbstractType.fPagResults.isSet(this.field_6_docinfo1);
    }
    
    public void setFLockAtn(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.fLockAtn.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isFLockAtn() {
        return DOPAbstractType.fLockAtn.isSet(this.field_6_docinfo1);
    }
    
    public void setFMirrorMargins(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.fMirrorMargins.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isFMirrorMargins() {
        return DOPAbstractType.fMirrorMargins.isSet(this.field_6_docinfo1);
    }
    
    public void setUnused3(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.unused3.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isUnused3() {
        return DOPAbstractType.unused3.isSet(this.field_6_docinfo1);
    }
    
    public void setFDfltTrueType(final boolean value) {
        this.field_6_docinfo1 = (byte)DOPAbstractType.fDfltTrueType.setBoolean(this.field_6_docinfo1, value);
    }
    
    public boolean isFDfltTrueType() {
        return DOPAbstractType.fDfltTrueType.isSet(this.field_6_docinfo1);
    }
    
    public void setFPagSupressTopSpacing(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.fPagSupressTopSpacing.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isFPagSupressTopSpacing() {
        return DOPAbstractType.fPagSupressTopSpacing.isSet(this.field_7_docinfo2);
    }
    
    public void setFProtEnabled(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.fProtEnabled.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isFProtEnabled() {
        return DOPAbstractType.fProtEnabled.isSet(this.field_7_docinfo2);
    }
    
    public void setFDispFormFldSel(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.fDispFormFldSel.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isFDispFormFldSel() {
        return DOPAbstractType.fDispFormFldSel.isSet(this.field_7_docinfo2);
    }
    
    public void setFRMView(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.fRMView.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isFRMView() {
        return DOPAbstractType.fRMView.isSet(this.field_7_docinfo2);
    }
    
    public void setFRMPrint(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.fRMPrint.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isFRMPrint() {
        return DOPAbstractType.fRMPrint.isSet(this.field_7_docinfo2);
    }
    
    public void setUnused4(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.unused4.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isUnused4() {
        return DOPAbstractType.unused4.isSet(this.field_7_docinfo2);
    }
    
    public void setFLockRev(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.fLockRev.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isFLockRev() {
        return DOPAbstractType.fLockRev.isSet(this.field_7_docinfo2);
    }
    
    public void setFEmbedFonts(final boolean value) {
        this.field_7_docinfo2 = (byte)DOPAbstractType.fEmbedFonts.setBoolean(this.field_7_docinfo2, value);
    }
    
    public boolean isFEmbedFonts() {
        return DOPAbstractType.fEmbedFonts.isSet(this.field_7_docinfo2);
    }
    
    public void setOldfNoTabForInd(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfNoTabForInd.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfNoTabForInd() {
        return DOPAbstractType.oldfNoTabForInd.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfNoSpaceRaiseLower(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfNoSpaceRaiseLower.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfNoSpaceRaiseLower() {
        return DOPAbstractType.oldfNoSpaceRaiseLower.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfSuppressSpbfAfterPageBreak(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfSuppressSpbfAfterPageBreak.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfSuppressSpbfAfterPageBreak() {
        return DOPAbstractType.oldfSuppressSpbfAfterPageBreak.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfWrapTrailSpaces(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfWrapTrailSpaces.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfWrapTrailSpaces() {
        return DOPAbstractType.oldfWrapTrailSpaces.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfMapPrintTextColor(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfMapPrintTextColor.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfMapPrintTextColor() {
        return DOPAbstractType.oldfMapPrintTextColor.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfNoColumnBalance(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfNoColumnBalance.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfNoColumnBalance() {
        return DOPAbstractType.oldfNoColumnBalance.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfConvMailMergeEsc(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfConvMailMergeEsc.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfConvMailMergeEsc() {
        return DOPAbstractType.oldfConvMailMergeEsc.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfSupressTopSpacing(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfSupressTopSpacing.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfSupressTopSpacing() {
        return DOPAbstractType.oldfSupressTopSpacing.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfOrigWordTableRules(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfOrigWordTableRules.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfOrigWordTableRules() {
        return DOPAbstractType.oldfOrigWordTableRules.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfTransparentMetafiles(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfTransparentMetafiles.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfTransparentMetafiles() {
        return DOPAbstractType.oldfTransparentMetafiles.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfShowBreaksInFrames(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfShowBreaksInFrames.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfShowBreaksInFrames() {
        return DOPAbstractType.oldfShowBreaksInFrames.isSet(this.field_8_docinfo3);
    }
    
    public void setOldfSwapBordersFacingPgs(final boolean value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.oldfSwapBordersFacingPgs.setBoolean(this.field_8_docinfo3, value);
    }
    
    public boolean isOldfSwapBordersFacingPgs() {
        return DOPAbstractType.oldfSwapBordersFacingPgs.isSet(this.field_8_docinfo3);
    }
    
    public void setUnused5(final byte value) {
        this.field_8_docinfo3 = (short)DOPAbstractType.unused5.setValue(this.field_8_docinfo3, value);
    }
    
    public byte getUnused5() {
        return (byte)DOPAbstractType.unused5.getValue(this.field_8_docinfo3);
    }
    
    public void setRncEdn(final byte value) {
        this.field_23_Edn = (short)DOPAbstractType.rncEdn.setValue(this.field_23_Edn, value);
    }
    
    public byte getRncEdn() {
        return (byte)DOPAbstractType.rncEdn.getValue(this.field_23_Edn);
    }
    
    public void setNEdn(final short value) {
        this.field_23_Edn = (short)DOPAbstractType.nEdn.setValue(this.field_23_Edn, value);
    }
    
    public short getNEdn() {
        return (short)DOPAbstractType.nEdn.getValue(this.field_23_Edn);
    }
    
    public void setEpc(final byte value) {
        this.field_24_Edn1 = (short)DOPAbstractType.epc.setValue(this.field_24_Edn1, value);
    }
    
    public byte getEpc() {
        return (byte)DOPAbstractType.epc.getValue(this.field_24_Edn1);
    }
    
    public void setNfcFtnRef1(final byte value) {
        this.field_24_Edn1 = (short)DOPAbstractType.nfcFtnRef1.setValue(this.field_24_Edn1, value);
    }
    
    public byte getNfcFtnRef1() {
        return (byte)DOPAbstractType.nfcFtnRef1.getValue(this.field_24_Edn1);
    }
    
    public void setNfcEdnRef1(final byte value) {
        this.field_24_Edn1 = (short)DOPAbstractType.nfcEdnRef1.setValue(this.field_24_Edn1, value);
    }
    
    public byte getNfcEdnRef1() {
        return (byte)DOPAbstractType.nfcEdnRef1.getValue(this.field_24_Edn1);
    }
    
    public void setFPrintFormData(final boolean value) {
        this.field_24_Edn1 = (short)DOPAbstractType.fPrintFormData.setBoolean(this.field_24_Edn1, value);
    }
    
    public boolean isFPrintFormData() {
        return DOPAbstractType.fPrintFormData.isSet(this.field_24_Edn1);
    }
    
    public void setFSaveFormData(final boolean value) {
        this.field_24_Edn1 = (short)DOPAbstractType.fSaveFormData.setBoolean(this.field_24_Edn1, value);
    }
    
    public boolean isFSaveFormData() {
        return DOPAbstractType.fSaveFormData.isSet(this.field_24_Edn1);
    }
    
    public void setFShadeFormData(final boolean value) {
        this.field_24_Edn1 = (short)DOPAbstractType.fShadeFormData.setBoolean(this.field_24_Edn1, value);
    }
    
    public boolean isFShadeFormData() {
        return DOPAbstractType.fShadeFormData.isSet(this.field_24_Edn1);
    }
    
    public void setFWCFtnEdn(final boolean value) {
        this.field_24_Edn1 = (short)DOPAbstractType.fWCFtnEdn.setBoolean(this.field_24_Edn1, value);
    }
    
    public boolean isFWCFtnEdn() {
        return DOPAbstractType.fWCFtnEdn.isSet(this.field_24_Edn1);
    }
    
    public void setWvkSaved(final byte value) {
        this.field_32_view = (short)DOPAbstractType.wvkSaved.setValue(this.field_32_view, value);
    }
    
    public byte getWvkSaved() {
        return (byte)DOPAbstractType.wvkSaved.getValue(this.field_32_view);
    }
    
    public void setWScaleSaved(final short value) {
        this.field_32_view = (short)DOPAbstractType.wScaleSaved.setValue(this.field_32_view, value);
    }
    
    public short getWScaleSaved() {
        return (short)DOPAbstractType.wScaleSaved.getValue(this.field_32_view);
    }
    
    public void setZkSaved(final byte value) {
        this.field_32_view = (short)DOPAbstractType.zkSaved.setValue(this.field_32_view, value);
    }
    
    public byte getZkSaved() {
        return (byte)DOPAbstractType.zkSaved.getValue(this.field_32_view);
    }
    
    public void setFRotateFontW6(final boolean value) {
        this.field_32_view = (short)DOPAbstractType.fRotateFontW6.setBoolean(this.field_32_view, value);
    }
    
    public boolean isFRotateFontW6() {
        return DOPAbstractType.fRotateFontW6.isSet(this.field_32_view);
    }
    
    public void setIGutterPos(final boolean value) {
        this.field_32_view = (short)DOPAbstractType.iGutterPos.setBoolean(this.field_32_view, value);
    }
    
    public boolean isIGutterPos() {
        return DOPAbstractType.iGutterPos.isSet(this.field_32_view);
    }
    
    public void setFNoTabForInd(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fNoTabForInd.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFNoTabForInd() {
        return DOPAbstractType.fNoTabForInd.isSet(this.field_33_docinfo4);
    }
    
    public void setFNoSpaceRaiseLower(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fNoSpaceRaiseLower.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFNoSpaceRaiseLower() {
        return DOPAbstractType.fNoSpaceRaiseLower.isSet(this.field_33_docinfo4);
    }
    
    public void setFSupressSpdfAfterPageBreak(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fSupressSpdfAfterPageBreak.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFSupressSpdfAfterPageBreak() {
        return DOPAbstractType.fSupressSpdfAfterPageBreak.isSet(this.field_33_docinfo4);
    }
    
    public void setFWrapTrailSpaces(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fWrapTrailSpaces.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFWrapTrailSpaces() {
        return DOPAbstractType.fWrapTrailSpaces.isSet(this.field_33_docinfo4);
    }
    
    public void setFMapPrintTextColor(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fMapPrintTextColor.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFMapPrintTextColor() {
        return DOPAbstractType.fMapPrintTextColor.isSet(this.field_33_docinfo4);
    }
    
    public void setFNoColumnBalance(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fNoColumnBalance.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFNoColumnBalance() {
        return DOPAbstractType.fNoColumnBalance.isSet(this.field_33_docinfo4);
    }
    
    public void setFConvMailMergeEsc(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fConvMailMergeEsc.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFConvMailMergeEsc() {
        return DOPAbstractType.fConvMailMergeEsc.isSet(this.field_33_docinfo4);
    }
    
    public void setFSupressTopSpacing(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fSupressTopSpacing.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFSupressTopSpacing() {
        return DOPAbstractType.fSupressTopSpacing.isSet(this.field_33_docinfo4);
    }
    
    public void setFOrigWordTableRules(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fOrigWordTableRules.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFOrigWordTableRules() {
        return DOPAbstractType.fOrigWordTableRules.isSet(this.field_33_docinfo4);
    }
    
    public void setFTransparentMetafiles(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fTransparentMetafiles.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFTransparentMetafiles() {
        return DOPAbstractType.fTransparentMetafiles.isSet(this.field_33_docinfo4);
    }
    
    public void setFShowBreaksInFrames(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fShowBreaksInFrames.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFShowBreaksInFrames() {
        return DOPAbstractType.fShowBreaksInFrames.isSet(this.field_33_docinfo4);
    }
    
    public void setFSwapBordersFacingPgs(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fSwapBordersFacingPgs.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFSwapBordersFacingPgs() {
        return DOPAbstractType.fSwapBordersFacingPgs.isSet(this.field_33_docinfo4);
    }
    
    public void setFSuppressTopSPacingMac5(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fSuppressTopSPacingMac5.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFSuppressTopSPacingMac5() {
        return DOPAbstractType.fSuppressTopSPacingMac5.isSet(this.field_33_docinfo4);
    }
    
    public void setFTruncDxaExpand(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fTruncDxaExpand.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFTruncDxaExpand() {
        return DOPAbstractType.fTruncDxaExpand.isSet(this.field_33_docinfo4);
    }
    
    public void setFPrintBodyBeforeHdr(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fPrintBodyBeforeHdr.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFPrintBodyBeforeHdr() {
        return DOPAbstractType.fPrintBodyBeforeHdr.isSet(this.field_33_docinfo4);
    }
    
    public void setFNoLeading(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fNoLeading.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFNoLeading() {
        return DOPAbstractType.fNoLeading.isSet(this.field_33_docinfo4);
    }
    
    public void setFMWSmallCaps(final boolean value) {
        this.field_33_docinfo4 = DOPAbstractType.fMWSmallCaps.setBoolean(this.field_33_docinfo4, value);
    }
    
    public boolean isFMWSmallCaps() {
        return DOPAbstractType.fMWSmallCaps.isSet(this.field_33_docinfo4);
    }
    
    public void setLvl(final byte value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.lvl.setValue(this.field_37_docinfo5, value);
    }
    
    public byte getLvl() {
        return (byte)DOPAbstractType.lvl.getValue(this.field_37_docinfo5);
    }
    
    public void setFGramAllDone(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fGramAllDone.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFGramAllDone() {
        return DOPAbstractType.fGramAllDone.isSet(this.field_37_docinfo5);
    }
    
    public void setFGramAllClean(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fGramAllClean.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFGramAllClean() {
        return DOPAbstractType.fGramAllClean.isSet(this.field_37_docinfo5);
    }
    
    public void setFSubsetFonts(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fSubsetFonts.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFSubsetFonts() {
        return DOPAbstractType.fSubsetFonts.isSet(this.field_37_docinfo5);
    }
    
    public void setFHideLastVersion(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fHideLastVersion.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFHideLastVersion() {
        return DOPAbstractType.fHideLastVersion.isSet(this.field_37_docinfo5);
    }
    
    public void setFHtmlDoc(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fHtmlDoc.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFHtmlDoc() {
        return DOPAbstractType.fHtmlDoc.isSet(this.field_37_docinfo5);
    }
    
    public void setFSnapBorder(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fSnapBorder.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFSnapBorder() {
        return DOPAbstractType.fSnapBorder.isSet(this.field_37_docinfo5);
    }
    
    public void setFIncludeHeader(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fIncludeHeader.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFIncludeHeader() {
        return DOPAbstractType.fIncludeHeader.isSet(this.field_37_docinfo5);
    }
    
    public void setFIncludeFooter(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fIncludeFooter.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFIncludeFooter() {
        return DOPAbstractType.fIncludeFooter.isSet(this.field_37_docinfo5);
    }
    
    public void setFForcePageSizePag(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fForcePageSizePag.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFForcePageSizePag() {
        return DOPAbstractType.fForcePageSizePag.isSet(this.field_37_docinfo5);
    }
    
    public void setFMinFontSizePag(final boolean value) {
        this.field_37_docinfo5 = (short)DOPAbstractType.fMinFontSizePag.setBoolean(this.field_37_docinfo5, value);
    }
    
    public boolean isFMinFontSizePag() {
        return DOPAbstractType.fMinFontSizePag.isSet(this.field_37_docinfo5);
    }
    
    public void setFHaveVersions(final boolean value) {
        this.field_38_docinfo6 = (short)DOPAbstractType.fHaveVersions.setBoolean(this.field_38_docinfo6, value);
    }
    
    public boolean isFHaveVersions() {
        return DOPAbstractType.fHaveVersions.isSet(this.field_38_docinfo6);
    }
    
    public void setFAutoVersions(final boolean value) {
        this.field_38_docinfo6 = (short)DOPAbstractType.fAutoVersions.setBoolean(this.field_38_docinfo6, value);
    }
    
    public boolean isFAutoVersions() {
        return DOPAbstractType.fAutoVersions.isSet(this.field_38_docinfo6);
    }
    
    public void setFVirusPrompted(final boolean value) {
        this.field_43_virusinfo = DOPAbstractType.fVirusPrompted.setBoolean(this.field_43_virusinfo, value);
    }
    
    public boolean isFVirusPrompted() {
        return DOPAbstractType.fVirusPrompted.isSet(this.field_43_virusinfo);
    }
    
    public void setFVirusLoadSafe(final boolean value) {
        this.field_43_virusinfo = DOPAbstractType.fVirusLoadSafe.setBoolean(this.field_43_virusinfo, value);
    }
    
    public boolean isFVirusLoadSafe() {
        return DOPAbstractType.fVirusLoadSafe.isSet(this.field_43_virusinfo);
    }
    
    public void setKeyVirusSession30(final int value) {
        this.field_43_virusinfo = DOPAbstractType.KeyVirusSession30.setValue(this.field_43_virusinfo, value);
    }
    
    public int getKeyVirusSession30() {
        return DOPAbstractType.KeyVirusSession30.getValue(this.field_43_virusinfo);
    }
    
    static {
        DOPAbstractType.fFacingPages = BitFieldFactory.getInstance(1);
        DOPAbstractType.fWidowControl = BitFieldFactory.getInstance(2);
        DOPAbstractType.fPMHMainDoc = BitFieldFactory.getInstance(4);
        DOPAbstractType.grfSupression = BitFieldFactory.getInstance(24);
        DOPAbstractType.fpc = BitFieldFactory.getInstance(96);
        DOPAbstractType.unused1 = BitFieldFactory.getInstance(128);
        DOPAbstractType.rncFtn = BitFieldFactory.getInstance(3);
        DOPAbstractType.nFtn = BitFieldFactory.getInstance(65532);
        DOPAbstractType.fOnlyMacPics = BitFieldFactory.getInstance(1);
        DOPAbstractType.fOnlyWinPics = BitFieldFactory.getInstance(2);
        DOPAbstractType.fLabelDoc = BitFieldFactory.getInstance(4);
        DOPAbstractType.fHyphCapitals = BitFieldFactory.getInstance(8);
        DOPAbstractType.fAutoHyphen = BitFieldFactory.getInstance(16);
        DOPAbstractType.fFormNoFields = BitFieldFactory.getInstance(32);
        DOPAbstractType.fLinkStyles = BitFieldFactory.getInstance(64);
        DOPAbstractType.fRevMarking = BitFieldFactory.getInstance(128);
        DOPAbstractType.fBackup = BitFieldFactory.getInstance(1);
        DOPAbstractType.fExactCWords = BitFieldFactory.getInstance(2);
        DOPAbstractType.fPagHidden = BitFieldFactory.getInstance(4);
        DOPAbstractType.fPagResults = BitFieldFactory.getInstance(8);
        DOPAbstractType.fLockAtn = BitFieldFactory.getInstance(16);
        DOPAbstractType.fMirrorMargins = BitFieldFactory.getInstance(32);
        DOPAbstractType.unused3 = BitFieldFactory.getInstance(64);
        DOPAbstractType.fDfltTrueType = BitFieldFactory.getInstance(128);
        DOPAbstractType.fPagSupressTopSpacing = BitFieldFactory.getInstance(1);
        DOPAbstractType.fProtEnabled = BitFieldFactory.getInstance(2);
        DOPAbstractType.fDispFormFldSel = BitFieldFactory.getInstance(4);
        DOPAbstractType.fRMView = BitFieldFactory.getInstance(8);
        DOPAbstractType.fRMPrint = BitFieldFactory.getInstance(16);
        DOPAbstractType.unused4 = BitFieldFactory.getInstance(32);
        DOPAbstractType.fLockRev = BitFieldFactory.getInstance(64);
        DOPAbstractType.fEmbedFonts = BitFieldFactory.getInstance(128);
        DOPAbstractType.oldfNoTabForInd = BitFieldFactory.getInstance(1);
        DOPAbstractType.oldfNoSpaceRaiseLower = BitFieldFactory.getInstance(2);
        DOPAbstractType.oldfSuppressSpbfAfterPageBreak = BitFieldFactory.getInstance(4);
        DOPAbstractType.oldfWrapTrailSpaces = BitFieldFactory.getInstance(8);
        DOPAbstractType.oldfMapPrintTextColor = BitFieldFactory.getInstance(16);
        DOPAbstractType.oldfNoColumnBalance = BitFieldFactory.getInstance(32);
        DOPAbstractType.oldfConvMailMergeEsc = BitFieldFactory.getInstance(64);
        DOPAbstractType.oldfSupressTopSpacing = BitFieldFactory.getInstance(128);
        DOPAbstractType.oldfOrigWordTableRules = BitFieldFactory.getInstance(256);
        DOPAbstractType.oldfTransparentMetafiles = BitFieldFactory.getInstance(512);
        DOPAbstractType.oldfShowBreaksInFrames = BitFieldFactory.getInstance(1024);
        DOPAbstractType.oldfSwapBordersFacingPgs = BitFieldFactory.getInstance(2048);
        DOPAbstractType.unused5 = BitFieldFactory.getInstance(61440);
        DOPAbstractType.rncEdn = BitFieldFactory.getInstance(3);
        DOPAbstractType.nEdn = BitFieldFactory.getInstance(65532);
        DOPAbstractType.epc = BitFieldFactory.getInstance(3);
        DOPAbstractType.nfcFtnRef1 = BitFieldFactory.getInstance(60);
        DOPAbstractType.nfcEdnRef1 = BitFieldFactory.getInstance(960);
        DOPAbstractType.fPrintFormData = BitFieldFactory.getInstance(1024);
        DOPAbstractType.fSaveFormData = BitFieldFactory.getInstance(2048);
        DOPAbstractType.fShadeFormData = BitFieldFactory.getInstance(4096);
        DOPAbstractType.fWCFtnEdn = BitFieldFactory.getInstance(32768);
        DOPAbstractType.wvkSaved = BitFieldFactory.getInstance(7);
        DOPAbstractType.wScaleSaved = BitFieldFactory.getInstance(4088);
        DOPAbstractType.zkSaved = BitFieldFactory.getInstance(12288);
        DOPAbstractType.fRotateFontW6 = BitFieldFactory.getInstance(16384);
        DOPAbstractType.iGutterPos = BitFieldFactory.getInstance(32768);
        DOPAbstractType.fNoTabForInd = BitFieldFactory.getInstance(1);
        DOPAbstractType.fNoSpaceRaiseLower = BitFieldFactory.getInstance(2);
        DOPAbstractType.fSupressSpdfAfterPageBreak = BitFieldFactory.getInstance(4);
        DOPAbstractType.fWrapTrailSpaces = BitFieldFactory.getInstance(8);
        DOPAbstractType.fMapPrintTextColor = BitFieldFactory.getInstance(16);
        DOPAbstractType.fNoColumnBalance = BitFieldFactory.getInstance(32);
        DOPAbstractType.fConvMailMergeEsc = BitFieldFactory.getInstance(64);
        DOPAbstractType.fSupressTopSpacing = BitFieldFactory.getInstance(128);
        DOPAbstractType.fOrigWordTableRules = BitFieldFactory.getInstance(256);
        DOPAbstractType.fTransparentMetafiles = BitFieldFactory.getInstance(512);
        DOPAbstractType.fShowBreaksInFrames = BitFieldFactory.getInstance(1024);
        DOPAbstractType.fSwapBordersFacingPgs = BitFieldFactory.getInstance(2048);
        DOPAbstractType.fSuppressTopSPacingMac5 = BitFieldFactory.getInstance(65536);
        DOPAbstractType.fTruncDxaExpand = BitFieldFactory.getInstance(131072);
        DOPAbstractType.fPrintBodyBeforeHdr = BitFieldFactory.getInstance(262144);
        DOPAbstractType.fNoLeading = BitFieldFactory.getInstance(524288);
        DOPAbstractType.fMWSmallCaps = BitFieldFactory.getInstance(2097152);
        DOPAbstractType.lvl = BitFieldFactory.getInstance(30);
        DOPAbstractType.fGramAllDone = BitFieldFactory.getInstance(32);
        DOPAbstractType.fGramAllClean = BitFieldFactory.getInstance(64);
        DOPAbstractType.fSubsetFonts = BitFieldFactory.getInstance(128);
        DOPAbstractType.fHideLastVersion = BitFieldFactory.getInstance(256);
        DOPAbstractType.fHtmlDoc = BitFieldFactory.getInstance(512);
        DOPAbstractType.fSnapBorder = BitFieldFactory.getInstance(2048);
        DOPAbstractType.fIncludeHeader = BitFieldFactory.getInstance(4096);
        DOPAbstractType.fIncludeFooter = BitFieldFactory.getInstance(8192);
        DOPAbstractType.fForcePageSizePag = BitFieldFactory.getInstance(16384);
        DOPAbstractType.fMinFontSizePag = BitFieldFactory.getInstance(32768);
        DOPAbstractType.fHaveVersions = BitFieldFactory.getInstance(1);
        DOPAbstractType.fAutoVersions = BitFieldFactory.getInstance(2);
        DOPAbstractType.fVirusPrompted = BitFieldFactory.getInstance(1);
        DOPAbstractType.fVirusLoadSafe = BitFieldFactory.getInstance(2);
        DOPAbstractType.KeyVirusSession30 = BitFieldFactory.getInstance(-4);
    }
}
