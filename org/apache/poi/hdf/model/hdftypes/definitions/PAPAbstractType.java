// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes.definitions;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.BitField;
import org.apache.poi.hdf.model.hdftypes.HDFType;

@Deprecated
public abstract class PAPAbstractType implements HDFType
{
    private int field_1_istd;
    private byte field_2_jc;
    private byte field_3_fKeep;
    private byte field_4_fKeepFollow;
    private byte field_5_fPageBreakBefore;
    private byte field_6_fBrLnAbove;
    private byte field_7_fBrLnBelow;
    private byte field_8_pcVert;
    private byte field_9_pcHorz;
    private byte field_10_brcp;
    private byte field_11_brcl;
    private byte field_12_ilvl;
    private byte field_13_fNoLnn;
    private int field_14_ilfo;
    private byte field_15_fSideBySide;
    private byte field_16_fNoAutoHyph;
    private byte field_17_fWidowControl;
    private int field_18_dxaRight;
    private int field_19_dxaLeft;
    private int field_20_dxaLeft1;
    private short[] field_21_lspd;
    private int field_22_dyaBefore;
    private int field_23_dyaAfter;
    private byte[] field_24_phe;
    private byte field_25_fCrLf;
    private byte field_26_fUsePgsuSettings;
    private byte field_27_fAdjustRight;
    private byte field_28_fKinsoku;
    private byte field_29_fWordWrap;
    private byte field_30_fOverflowPunct;
    private byte field_31_fTopLinePunct;
    private byte field_32_fAutoSpaceDE;
    private byte field_33_fAutoSpaceDN;
    private int field_34_wAlignFont;
    private short field_35_fontAlign;
    private static BitField fVertical;
    private static BitField fBackward;
    private static BitField fRotateFont;
    private byte field_36_fBackward;
    private byte field_37_fRotateFont;
    private byte field_38_fInTable;
    private byte field_39_fTtp;
    private byte field_40_wr;
    private byte field_41_fLocked;
    private byte[] field_42_ptap;
    private int field_43_dxaAbs;
    private int field_44_dyaAbs;
    private int field_45_dxaWidth;
    private short[] field_46_brcTop;
    private short[] field_47_brcLeft;
    private short[] field_48_brcBottom;
    private short[] field_49_brcRight;
    private short[] field_50_brcBetween;
    private short[] field_51_brcBar;
    private int field_52_dxaFromText;
    private int field_53_dyaFromText;
    private int field_54_dyaHeight;
    private byte field_55_fMinHeight;
    private short field_56_shd;
    private short field_57_dcs;
    private byte field_58_lvl;
    private byte field_59_fNumRMIns;
    private byte[] field_60_anld;
    private int field_61_fPropRMark;
    private int field_62_ibstPropRMark;
    private byte[] field_63_dttmPropRMark;
    private byte[] field_64_numrm;
    private int field_65_itbdMac;
    private byte[] field_66_rgdxaTab;
    private byte[] field_67_rgtbd;
    
    public int getSize() {
        return 612;
    }
    
    public int getIstd() {
        return this.field_1_istd;
    }
    
    public void setIstd(final int field_1_istd) {
        this.field_1_istd = field_1_istd;
    }
    
    public byte getJc() {
        return this.field_2_jc;
    }
    
    public void setJc(final byte field_2_jc) {
        this.field_2_jc = field_2_jc;
    }
    
    public byte getFKeep() {
        return this.field_3_fKeep;
    }
    
    public void setFKeep(final byte field_3_fKeep) {
        this.field_3_fKeep = field_3_fKeep;
    }
    
    public byte getFKeepFollow() {
        return this.field_4_fKeepFollow;
    }
    
    public void setFKeepFollow(final byte field_4_fKeepFollow) {
        this.field_4_fKeepFollow = field_4_fKeepFollow;
    }
    
    public byte getFPageBreakBefore() {
        return this.field_5_fPageBreakBefore;
    }
    
    public void setFPageBreakBefore(final byte field_5_fPageBreakBefore) {
        this.field_5_fPageBreakBefore = field_5_fPageBreakBefore;
    }
    
    public byte getFBrLnAbove() {
        return this.field_6_fBrLnAbove;
    }
    
    public void setFBrLnAbove(final byte field_6_fBrLnAbove) {
        this.field_6_fBrLnAbove = field_6_fBrLnAbove;
    }
    
    public byte getFBrLnBelow() {
        return this.field_7_fBrLnBelow;
    }
    
    public void setFBrLnBelow(final byte field_7_fBrLnBelow) {
        this.field_7_fBrLnBelow = field_7_fBrLnBelow;
    }
    
    public byte getPcVert() {
        return this.field_8_pcVert;
    }
    
    public void setPcVert(final byte field_8_pcVert) {
        this.field_8_pcVert = field_8_pcVert;
    }
    
    public byte getPcHorz() {
        return this.field_9_pcHorz;
    }
    
    public void setPcHorz(final byte field_9_pcHorz) {
        this.field_9_pcHorz = field_9_pcHorz;
    }
    
    public byte getBrcp() {
        return this.field_10_brcp;
    }
    
    public void setBrcp(final byte field_10_brcp) {
        this.field_10_brcp = field_10_brcp;
    }
    
    public byte getBrcl() {
        return this.field_11_brcl;
    }
    
    public void setBrcl(final byte field_11_brcl) {
        this.field_11_brcl = field_11_brcl;
    }
    
    public byte getIlvl() {
        return this.field_12_ilvl;
    }
    
    public void setIlvl(final byte field_12_ilvl) {
        this.field_12_ilvl = field_12_ilvl;
    }
    
    public byte getFNoLnn() {
        return this.field_13_fNoLnn;
    }
    
    public void setFNoLnn(final byte field_13_fNoLnn) {
        this.field_13_fNoLnn = field_13_fNoLnn;
    }
    
    public int getIlfo() {
        return this.field_14_ilfo;
    }
    
    public void setIlfo(final int field_14_ilfo) {
        this.field_14_ilfo = field_14_ilfo;
    }
    
    public byte getFSideBySide() {
        return this.field_15_fSideBySide;
    }
    
    public void setFSideBySide(final byte field_15_fSideBySide) {
        this.field_15_fSideBySide = field_15_fSideBySide;
    }
    
    public byte getFNoAutoHyph() {
        return this.field_16_fNoAutoHyph;
    }
    
    public void setFNoAutoHyph(final byte field_16_fNoAutoHyph) {
        this.field_16_fNoAutoHyph = field_16_fNoAutoHyph;
    }
    
    public byte getFWidowControl() {
        return this.field_17_fWidowControl;
    }
    
    public void setFWidowControl(final byte field_17_fWidowControl) {
        this.field_17_fWidowControl = field_17_fWidowControl;
    }
    
    public int getDxaRight() {
        return this.field_18_dxaRight;
    }
    
    public void setDxaRight(final int field_18_dxaRight) {
        this.field_18_dxaRight = field_18_dxaRight;
    }
    
    public int getDxaLeft() {
        return this.field_19_dxaLeft;
    }
    
    public void setDxaLeft(final int field_19_dxaLeft) {
        this.field_19_dxaLeft = field_19_dxaLeft;
    }
    
    public int getDxaLeft1() {
        return this.field_20_dxaLeft1;
    }
    
    public void setDxaLeft1(final int field_20_dxaLeft1) {
        this.field_20_dxaLeft1 = field_20_dxaLeft1;
    }
    
    public short[] getLspd() {
        return this.field_21_lspd;
    }
    
    public void setLspd(final short[] field_21_lspd) {
        this.field_21_lspd = field_21_lspd;
    }
    
    public int getDyaBefore() {
        return this.field_22_dyaBefore;
    }
    
    public void setDyaBefore(final int field_22_dyaBefore) {
        this.field_22_dyaBefore = field_22_dyaBefore;
    }
    
    public int getDyaAfter() {
        return this.field_23_dyaAfter;
    }
    
    public void setDyaAfter(final int field_23_dyaAfter) {
        this.field_23_dyaAfter = field_23_dyaAfter;
    }
    
    public byte[] getPhe() {
        return this.field_24_phe;
    }
    
    public void setPhe(final byte[] field_24_phe) {
        this.field_24_phe = field_24_phe;
    }
    
    public byte getFCrLf() {
        return this.field_25_fCrLf;
    }
    
    public void setFCrLf(final byte field_25_fCrLf) {
        this.field_25_fCrLf = field_25_fCrLf;
    }
    
    public byte getFUsePgsuSettings() {
        return this.field_26_fUsePgsuSettings;
    }
    
    public void setFUsePgsuSettings(final byte field_26_fUsePgsuSettings) {
        this.field_26_fUsePgsuSettings = field_26_fUsePgsuSettings;
    }
    
    public byte getFAdjustRight() {
        return this.field_27_fAdjustRight;
    }
    
    public void setFAdjustRight(final byte field_27_fAdjustRight) {
        this.field_27_fAdjustRight = field_27_fAdjustRight;
    }
    
    public byte getFKinsoku() {
        return this.field_28_fKinsoku;
    }
    
    public void setFKinsoku(final byte field_28_fKinsoku) {
        this.field_28_fKinsoku = field_28_fKinsoku;
    }
    
    public byte getFWordWrap() {
        return this.field_29_fWordWrap;
    }
    
    public void setFWordWrap(final byte field_29_fWordWrap) {
        this.field_29_fWordWrap = field_29_fWordWrap;
    }
    
    public byte getFOverflowPunct() {
        return this.field_30_fOverflowPunct;
    }
    
    public void setFOverflowPunct(final byte field_30_fOverflowPunct) {
        this.field_30_fOverflowPunct = field_30_fOverflowPunct;
    }
    
    public byte getFTopLinePunct() {
        return this.field_31_fTopLinePunct;
    }
    
    public void setFTopLinePunct(final byte field_31_fTopLinePunct) {
        this.field_31_fTopLinePunct = field_31_fTopLinePunct;
    }
    
    public byte getFAutoSpaceDE() {
        return this.field_32_fAutoSpaceDE;
    }
    
    public void setFAutoSpaceDE(final byte field_32_fAutoSpaceDE) {
        this.field_32_fAutoSpaceDE = field_32_fAutoSpaceDE;
    }
    
    public byte getFAutoSpaceDN() {
        return this.field_33_fAutoSpaceDN;
    }
    
    public void setFAutoSpaceDN(final byte field_33_fAutoSpaceDN) {
        this.field_33_fAutoSpaceDN = field_33_fAutoSpaceDN;
    }
    
    public int getWAlignFont() {
        return this.field_34_wAlignFont;
    }
    
    public void setWAlignFont(final int field_34_wAlignFont) {
        this.field_34_wAlignFont = field_34_wAlignFont;
    }
    
    public short getFontAlign() {
        return this.field_35_fontAlign;
    }
    
    public void setFontAlign(final short field_35_fontAlign) {
        this.field_35_fontAlign = field_35_fontAlign;
    }
    
    public byte getFBackward() {
        return this.field_36_fBackward;
    }
    
    public void setFBackward(final byte field_36_fBackward) {
        this.field_36_fBackward = field_36_fBackward;
    }
    
    public byte getFRotateFont() {
        return this.field_37_fRotateFont;
    }
    
    public void setFRotateFont(final byte field_37_fRotateFont) {
        this.field_37_fRotateFont = field_37_fRotateFont;
    }
    
    public byte getFInTable() {
        return this.field_38_fInTable;
    }
    
    public void setFInTable(final byte field_38_fInTable) {
        this.field_38_fInTable = field_38_fInTable;
    }
    
    public byte getFTtp() {
        return this.field_39_fTtp;
    }
    
    public void setFTtp(final byte field_39_fTtp) {
        this.field_39_fTtp = field_39_fTtp;
    }
    
    public byte getWr() {
        return this.field_40_wr;
    }
    
    public void setWr(final byte field_40_wr) {
        this.field_40_wr = field_40_wr;
    }
    
    public byte getFLocked() {
        return this.field_41_fLocked;
    }
    
    public void setFLocked(final byte field_41_fLocked) {
        this.field_41_fLocked = field_41_fLocked;
    }
    
    public byte[] getPtap() {
        return this.field_42_ptap;
    }
    
    public void setPtap(final byte[] field_42_ptap) {
        this.field_42_ptap = field_42_ptap;
    }
    
    public int getDxaAbs() {
        return this.field_43_dxaAbs;
    }
    
    public void setDxaAbs(final int field_43_dxaAbs) {
        this.field_43_dxaAbs = field_43_dxaAbs;
    }
    
    public int getDyaAbs() {
        return this.field_44_dyaAbs;
    }
    
    public void setDyaAbs(final int field_44_dyaAbs) {
        this.field_44_dyaAbs = field_44_dyaAbs;
    }
    
    public int getDxaWidth() {
        return this.field_45_dxaWidth;
    }
    
    public void setDxaWidth(final int field_45_dxaWidth) {
        this.field_45_dxaWidth = field_45_dxaWidth;
    }
    
    public short[] getBrcTop() {
        return this.field_46_brcTop;
    }
    
    public void setBrcTop(final short[] field_46_brcTop) {
        this.field_46_brcTop = field_46_brcTop;
    }
    
    public short[] getBrcLeft() {
        return this.field_47_brcLeft;
    }
    
    public void setBrcLeft(final short[] field_47_brcLeft) {
        this.field_47_brcLeft = field_47_brcLeft;
    }
    
    public short[] getBrcBottom() {
        return this.field_48_brcBottom;
    }
    
    public void setBrcBottom(final short[] field_48_brcBottom) {
        this.field_48_brcBottom = field_48_brcBottom;
    }
    
    public short[] getBrcRight() {
        return this.field_49_brcRight;
    }
    
    public void setBrcRight(final short[] field_49_brcRight) {
        this.field_49_brcRight = field_49_brcRight;
    }
    
    public short[] getBrcBetween() {
        return this.field_50_brcBetween;
    }
    
    public void setBrcBetween(final short[] field_50_brcBetween) {
        this.field_50_brcBetween = field_50_brcBetween;
    }
    
    public short[] getBrcBar() {
        return this.field_51_brcBar;
    }
    
    public void setBrcBar(final short[] field_51_brcBar) {
        this.field_51_brcBar = field_51_brcBar;
    }
    
    public int getDxaFromText() {
        return this.field_52_dxaFromText;
    }
    
    public void setDxaFromText(final int field_52_dxaFromText) {
        this.field_52_dxaFromText = field_52_dxaFromText;
    }
    
    public int getDyaFromText() {
        return this.field_53_dyaFromText;
    }
    
    public void setDyaFromText(final int field_53_dyaFromText) {
        this.field_53_dyaFromText = field_53_dyaFromText;
    }
    
    public int getDyaHeight() {
        return this.field_54_dyaHeight;
    }
    
    public void setDyaHeight(final int field_54_dyaHeight) {
        this.field_54_dyaHeight = field_54_dyaHeight;
    }
    
    public byte getFMinHeight() {
        return this.field_55_fMinHeight;
    }
    
    public void setFMinHeight(final byte field_55_fMinHeight) {
        this.field_55_fMinHeight = field_55_fMinHeight;
    }
    
    public short getShd() {
        return this.field_56_shd;
    }
    
    public void setShd(final short field_56_shd) {
        this.field_56_shd = field_56_shd;
    }
    
    public short getDcs() {
        return this.field_57_dcs;
    }
    
    public void setDcs(final short field_57_dcs) {
        this.field_57_dcs = field_57_dcs;
    }
    
    public byte getLvl() {
        return this.field_58_lvl;
    }
    
    public void setLvl(final byte field_58_lvl) {
        this.field_58_lvl = field_58_lvl;
    }
    
    public byte getFNumRMIns() {
        return this.field_59_fNumRMIns;
    }
    
    public void setFNumRMIns(final byte field_59_fNumRMIns) {
        this.field_59_fNumRMIns = field_59_fNumRMIns;
    }
    
    public byte[] getAnld() {
        return this.field_60_anld;
    }
    
    public void setAnld(final byte[] field_60_anld) {
        this.field_60_anld = field_60_anld;
    }
    
    public int getFPropRMark() {
        return this.field_61_fPropRMark;
    }
    
    public void setFPropRMark(final int field_61_fPropRMark) {
        this.field_61_fPropRMark = field_61_fPropRMark;
    }
    
    public int getIbstPropRMark() {
        return this.field_62_ibstPropRMark;
    }
    
    public void setIbstPropRMark(final int field_62_ibstPropRMark) {
        this.field_62_ibstPropRMark = field_62_ibstPropRMark;
    }
    
    public byte[] getDttmPropRMark() {
        return this.field_63_dttmPropRMark;
    }
    
    public void setDttmPropRMark(final byte[] field_63_dttmPropRMark) {
        this.field_63_dttmPropRMark = field_63_dttmPropRMark;
    }
    
    public byte[] getNumrm() {
        return this.field_64_numrm;
    }
    
    public void setNumrm(final byte[] field_64_numrm) {
        this.field_64_numrm = field_64_numrm;
    }
    
    public int getItbdMac() {
        return this.field_65_itbdMac;
    }
    
    public void setItbdMac(final int field_65_itbdMac) {
        this.field_65_itbdMac = field_65_itbdMac;
    }
    
    public byte[] getRgdxaTab() {
        return this.field_66_rgdxaTab;
    }
    
    public void setRgdxaTab(final byte[] field_66_rgdxaTab) {
        this.field_66_rgdxaTab = field_66_rgdxaTab;
    }
    
    public byte[] getRgtbd() {
        return this.field_67_rgtbd;
    }
    
    public void setRgtbd(final byte[] field_67_rgtbd) {
        this.field_67_rgtbd = field_67_rgtbd;
    }
    
    public void setFVertical(final boolean value) {
        this.field_35_fontAlign = (short)PAPAbstractType.fVertical.setBoolean(this.field_35_fontAlign, value);
    }
    
    public boolean isFVertical() {
        return PAPAbstractType.fVertical.isSet(this.field_35_fontAlign);
    }
    
    public void setFBackward(final boolean value) {
        this.field_35_fontAlign = (short)PAPAbstractType.fBackward.setBoolean(this.field_35_fontAlign, value);
    }
    
    public boolean isFBackward() {
        return PAPAbstractType.fBackward.isSet(this.field_35_fontAlign);
    }
    
    public void setFRotateFont(final boolean value) {
        this.field_35_fontAlign = (short)PAPAbstractType.fRotateFont.setBoolean(this.field_35_fontAlign, value);
    }
    
    public boolean isFRotateFont() {
        return PAPAbstractType.fRotateFont.isSet(this.field_35_fontAlign);
    }
    
    static {
        PAPAbstractType.fVertical = BitFieldFactory.getInstance(1);
        PAPAbstractType.fBackward = BitFieldFactory.getInstance(2);
        PAPAbstractType.fRotateFont = BitFieldFactory.getInstance(4);
    }
}
