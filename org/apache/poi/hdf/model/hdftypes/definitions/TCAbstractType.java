// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes.definitions;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.BitField;
import org.apache.poi.hdf.model.hdftypes.HDFType;

@Deprecated
public abstract class TCAbstractType implements HDFType
{
    private short field_1_rgf;
    private static BitField fFirstMerged;
    private static BitField fMerged;
    private static BitField fVertical;
    private static BitField fBackward;
    private static BitField fRotateFont;
    private static BitField fVertMerge;
    private static BitField fVertRestart;
    private static BitField vertAlign;
    private short field_2_unused;
    private short[] field_3_brcTop;
    private short[] field_4_brcLeft;
    private short[] field_5_brcBottom;
    private short[] field_6_brcRight;
    
    public int getSize() {
        return 24;
    }
    
    public short getRgf() {
        return this.field_1_rgf;
    }
    
    public void setRgf(final short field_1_rgf) {
        this.field_1_rgf = field_1_rgf;
    }
    
    public short getUnused() {
        return this.field_2_unused;
    }
    
    public void setUnused(final short field_2_unused) {
        this.field_2_unused = field_2_unused;
    }
    
    public short[] getBrcTop() {
        return this.field_3_brcTop;
    }
    
    public void setBrcTop(final short[] field_3_brcTop) {
        this.field_3_brcTop = field_3_brcTop;
    }
    
    public short[] getBrcLeft() {
        return this.field_4_brcLeft;
    }
    
    public void setBrcLeft(final short[] field_4_brcLeft) {
        this.field_4_brcLeft = field_4_brcLeft;
    }
    
    public short[] getBrcBottom() {
        return this.field_5_brcBottom;
    }
    
    public void setBrcBottom(final short[] field_5_brcBottom) {
        this.field_5_brcBottom = field_5_brcBottom;
    }
    
    public short[] getBrcRight() {
        return this.field_6_brcRight;
    }
    
    public void setBrcRight(final short[] field_6_brcRight) {
        this.field_6_brcRight = field_6_brcRight;
    }
    
    public void setFFirstMerged(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fFirstMerged.setBoolean(this.field_1_rgf, value);
    }
    
    public boolean isFFirstMerged() {
        return TCAbstractType.fFirstMerged.isSet(this.field_1_rgf);
    }
    
    public void setFMerged(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fMerged.setBoolean(this.field_1_rgf, value);
    }
    
    public boolean isFMerged() {
        return TCAbstractType.fMerged.isSet(this.field_1_rgf);
    }
    
    public void setFVertical(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fVertical.setBoolean(this.field_1_rgf, value);
    }
    
    public boolean isFVertical() {
        return TCAbstractType.fVertical.isSet(this.field_1_rgf);
    }
    
    public void setFBackward(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fBackward.setBoolean(this.field_1_rgf, value);
    }
    
    public boolean isFBackward() {
        return TCAbstractType.fBackward.isSet(this.field_1_rgf);
    }
    
    public void setFRotateFont(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fRotateFont.setBoolean(this.field_1_rgf, value);
    }
    
    public boolean isFRotateFont() {
        return TCAbstractType.fRotateFont.isSet(this.field_1_rgf);
    }
    
    public void setFVertMerge(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fVertMerge.setBoolean(this.field_1_rgf, value);
    }
    
    public boolean isFVertMerge() {
        return TCAbstractType.fVertMerge.isSet(this.field_1_rgf);
    }
    
    public void setFVertRestart(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fVertRestart.setBoolean(this.field_1_rgf, value);
    }
    
    public boolean isFVertRestart() {
        return TCAbstractType.fVertRestart.isSet(this.field_1_rgf);
    }
    
    public void setVertAlign(final byte value) {
        this.field_1_rgf = (short)TCAbstractType.vertAlign.setValue(this.field_1_rgf, value);
    }
    
    public byte getVertAlign() {
        return (byte)TCAbstractType.vertAlign.getValue(this.field_1_rgf);
    }
    
    static {
        TCAbstractType.fFirstMerged = BitFieldFactory.getInstance(1);
        TCAbstractType.fMerged = BitFieldFactory.getInstance(2);
        TCAbstractType.fVertical = BitFieldFactory.getInstance(4);
        TCAbstractType.fBackward = BitFieldFactory.getInstance(8);
        TCAbstractType.fRotateFont = BitFieldFactory.getInstance(16);
        TCAbstractType.fVertMerge = BitFieldFactory.getInstance(32);
        TCAbstractType.fVertRestart = BitFieldFactory.getInstance(64);
        TCAbstractType.vertAlign = BitFieldFactory.getInstance(384);
    }
}
