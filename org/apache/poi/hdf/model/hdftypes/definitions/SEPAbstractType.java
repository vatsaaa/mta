// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes.definitions;

import org.apache.poi.hdf.model.hdftypes.HDFType;

@Deprecated
public abstract class SEPAbstractType implements HDFType
{
    private byte field_1_bkc;
    private boolean field_2_fTitlePage;
    private boolean field_3_fAutoPgn;
    private byte field_4_nfcPgn;
    private boolean field_5_fUnlocked;
    private byte field_6_cnsPgn;
    private boolean field_7_fPgnRestart;
    private boolean field_8_fEndNote;
    private byte field_9_lnc;
    private byte field_10_grpfIhdt;
    private int field_11_nLnnMod;
    private int field_12_dxaLnn;
    private int field_13_dxaPgn;
    private int field_14_dyaPgn;
    private boolean field_15_fLBetween;
    private byte field_16_vjc;
    private int field_17_dmBinFirst;
    private int field_18_dmBinOther;
    private int field_19_dmPaperReq;
    private short[] field_20_brcTop;
    private short[] field_21_brcLeft;
    private short[] field_22_brcBottom;
    private short[] field_23_brcRight;
    private boolean field_24_fPropMark;
    private int field_25_ibstPropRMark;
    private int field_26_dttmPropRMark;
    private int field_27_dxtCharSpace;
    private int field_28_dyaLinePitch;
    private int field_29_clm;
    private int field_30_unused2;
    private byte field_31_dmOrientPage;
    private byte field_32_iHeadingPgn;
    private int field_33_pgnStart;
    private int field_34_lnnMin;
    private int field_35_wTextFlow;
    private short field_36_unused3;
    private int field_37_pgbProp;
    private short field_38_unused4;
    private int field_39_xaPage;
    private int field_40_yaPage;
    private int field_41_xaPageNUp;
    private int field_42_yaPageNUp;
    private int field_43_dxaLeft;
    private int field_44_dxaRight;
    private int field_45_dyaTop;
    private int field_46_dyaBottom;
    private int field_47_dzaGutter;
    private int field_48_dyaHdrTop;
    private int field_49_dyaHdrBottom;
    private int field_50_ccolM1;
    private boolean field_51_fEvenlySpaced;
    private byte field_52_unused5;
    private int field_53_dxaColumns;
    private int[] field_54_rgdxaColumn;
    private int field_55_dxaColumnWidth;
    private byte field_56_dmOrientFirst;
    private byte field_57_fLayout;
    private short field_58_unused6;
    private byte[] field_59_olstAnm;
    
    public int getSize() {
        return 701;
    }
    
    public byte getBkc() {
        return this.field_1_bkc;
    }
    
    public void setBkc(final byte field_1_bkc) {
        this.field_1_bkc = field_1_bkc;
    }
    
    public boolean getFTitlePage() {
        return this.field_2_fTitlePage;
    }
    
    public void setFTitlePage(final boolean field_2_fTitlePage) {
        this.field_2_fTitlePage = field_2_fTitlePage;
    }
    
    public boolean getFAutoPgn() {
        return this.field_3_fAutoPgn;
    }
    
    public void setFAutoPgn(final boolean field_3_fAutoPgn) {
        this.field_3_fAutoPgn = field_3_fAutoPgn;
    }
    
    public byte getNfcPgn() {
        return this.field_4_nfcPgn;
    }
    
    public void setNfcPgn(final byte field_4_nfcPgn) {
        this.field_4_nfcPgn = field_4_nfcPgn;
    }
    
    public boolean getFUnlocked() {
        return this.field_5_fUnlocked;
    }
    
    public void setFUnlocked(final boolean field_5_fUnlocked) {
        this.field_5_fUnlocked = field_5_fUnlocked;
    }
    
    public byte getCnsPgn() {
        return this.field_6_cnsPgn;
    }
    
    public void setCnsPgn(final byte field_6_cnsPgn) {
        this.field_6_cnsPgn = field_6_cnsPgn;
    }
    
    public boolean getFPgnRestart() {
        return this.field_7_fPgnRestart;
    }
    
    public void setFPgnRestart(final boolean field_7_fPgnRestart) {
        this.field_7_fPgnRestart = field_7_fPgnRestart;
    }
    
    public boolean getFEndNote() {
        return this.field_8_fEndNote;
    }
    
    public void setFEndNote(final boolean field_8_fEndNote) {
        this.field_8_fEndNote = field_8_fEndNote;
    }
    
    public byte getLnc() {
        return this.field_9_lnc;
    }
    
    public void setLnc(final byte field_9_lnc) {
        this.field_9_lnc = field_9_lnc;
    }
    
    public byte getGrpfIhdt() {
        return this.field_10_grpfIhdt;
    }
    
    public void setGrpfIhdt(final byte field_10_grpfIhdt) {
        this.field_10_grpfIhdt = field_10_grpfIhdt;
    }
    
    public int getNLnnMod() {
        return this.field_11_nLnnMod;
    }
    
    public void setNLnnMod(final int field_11_nLnnMod) {
        this.field_11_nLnnMod = field_11_nLnnMod;
    }
    
    public int getDxaLnn() {
        return this.field_12_dxaLnn;
    }
    
    public void setDxaLnn(final int field_12_dxaLnn) {
        this.field_12_dxaLnn = field_12_dxaLnn;
    }
    
    public int getDxaPgn() {
        return this.field_13_dxaPgn;
    }
    
    public void setDxaPgn(final int field_13_dxaPgn) {
        this.field_13_dxaPgn = field_13_dxaPgn;
    }
    
    public int getDyaPgn() {
        return this.field_14_dyaPgn;
    }
    
    public void setDyaPgn(final int field_14_dyaPgn) {
        this.field_14_dyaPgn = field_14_dyaPgn;
    }
    
    public boolean getFLBetween() {
        return this.field_15_fLBetween;
    }
    
    public void setFLBetween(final boolean field_15_fLBetween) {
        this.field_15_fLBetween = field_15_fLBetween;
    }
    
    public byte getVjc() {
        return this.field_16_vjc;
    }
    
    public void setVjc(final byte field_16_vjc) {
        this.field_16_vjc = field_16_vjc;
    }
    
    public int getDmBinFirst() {
        return this.field_17_dmBinFirst;
    }
    
    public void setDmBinFirst(final int field_17_dmBinFirst) {
        this.field_17_dmBinFirst = field_17_dmBinFirst;
    }
    
    public int getDmBinOther() {
        return this.field_18_dmBinOther;
    }
    
    public void setDmBinOther(final int field_18_dmBinOther) {
        this.field_18_dmBinOther = field_18_dmBinOther;
    }
    
    public int getDmPaperReq() {
        return this.field_19_dmPaperReq;
    }
    
    public void setDmPaperReq(final int field_19_dmPaperReq) {
        this.field_19_dmPaperReq = field_19_dmPaperReq;
    }
    
    public short[] getBrcTop() {
        return this.field_20_brcTop;
    }
    
    public void setBrcTop(final short[] field_20_brcTop) {
        this.field_20_brcTop = field_20_brcTop;
    }
    
    public short[] getBrcLeft() {
        return this.field_21_brcLeft;
    }
    
    public void setBrcLeft(final short[] field_21_brcLeft) {
        this.field_21_brcLeft = field_21_brcLeft;
    }
    
    public short[] getBrcBottom() {
        return this.field_22_brcBottom;
    }
    
    public void setBrcBottom(final short[] field_22_brcBottom) {
        this.field_22_brcBottom = field_22_brcBottom;
    }
    
    public short[] getBrcRight() {
        return this.field_23_brcRight;
    }
    
    public void setBrcRight(final short[] field_23_brcRight) {
        this.field_23_brcRight = field_23_brcRight;
    }
    
    public boolean getFPropMark() {
        return this.field_24_fPropMark;
    }
    
    public void setFPropMark(final boolean field_24_fPropMark) {
        this.field_24_fPropMark = field_24_fPropMark;
    }
    
    public int getIbstPropRMark() {
        return this.field_25_ibstPropRMark;
    }
    
    public void setIbstPropRMark(final int field_25_ibstPropRMark) {
        this.field_25_ibstPropRMark = field_25_ibstPropRMark;
    }
    
    public int getDttmPropRMark() {
        return this.field_26_dttmPropRMark;
    }
    
    public void setDttmPropRMark(final int field_26_dttmPropRMark) {
        this.field_26_dttmPropRMark = field_26_dttmPropRMark;
    }
    
    public int getDxtCharSpace() {
        return this.field_27_dxtCharSpace;
    }
    
    public void setDxtCharSpace(final int field_27_dxtCharSpace) {
        this.field_27_dxtCharSpace = field_27_dxtCharSpace;
    }
    
    public int getDyaLinePitch() {
        return this.field_28_dyaLinePitch;
    }
    
    public void setDyaLinePitch(final int field_28_dyaLinePitch) {
        this.field_28_dyaLinePitch = field_28_dyaLinePitch;
    }
    
    public int getClm() {
        return this.field_29_clm;
    }
    
    public void setClm(final int field_29_clm) {
        this.field_29_clm = field_29_clm;
    }
    
    public int getUnused2() {
        return this.field_30_unused2;
    }
    
    public void setUnused2(final int field_30_unused2) {
        this.field_30_unused2 = field_30_unused2;
    }
    
    public byte getDmOrientPage() {
        return this.field_31_dmOrientPage;
    }
    
    public void setDmOrientPage(final byte field_31_dmOrientPage) {
        this.field_31_dmOrientPage = field_31_dmOrientPage;
    }
    
    public byte getIHeadingPgn() {
        return this.field_32_iHeadingPgn;
    }
    
    public void setIHeadingPgn(final byte field_32_iHeadingPgn) {
        this.field_32_iHeadingPgn = field_32_iHeadingPgn;
    }
    
    public int getPgnStart() {
        return this.field_33_pgnStart;
    }
    
    public void setPgnStart(final int field_33_pgnStart) {
        this.field_33_pgnStart = field_33_pgnStart;
    }
    
    public int getLnnMin() {
        return this.field_34_lnnMin;
    }
    
    public void setLnnMin(final int field_34_lnnMin) {
        this.field_34_lnnMin = field_34_lnnMin;
    }
    
    public int getWTextFlow() {
        return this.field_35_wTextFlow;
    }
    
    public void setWTextFlow(final int field_35_wTextFlow) {
        this.field_35_wTextFlow = field_35_wTextFlow;
    }
    
    public short getUnused3() {
        return this.field_36_unused3;
    }
    
    public void setUnused3(final short field_36_unused3) {
        this.field_36_unused3 = field_36_unused3;
    }
    
    public int getPgbProp() {
        return this.field_37_pgbProp;
    }
    
    public void setPgbProp(final int field_37_pgbProp) {
        this.field_37_pgbProp = field_37_pgbProp;
    }
    
    public short getUnused4() {
        return this.field_38_unused4;
    }
    
    public void setUnused4(final short field_38_unused4) {
        this.field_38_unused4 = field_38_unused4;
    }
    
    public int getXaPage() {
        return this.field_39_xaPage;
    }
    
    public void setXaPage(final int field_39_xaPage) {
        this.field_39_xaPage = field_39_xaPage;
    }
    
    public int getYaPage() {
        return this.field_40_yaPage;
    }
    
    public void setYaPage(final int field_40_yaPage) {
        this.field_40_yaPage = field_40_yaPage;
    }
    
    public int getXaPageNUp() {
        return this.field_41_xaPageNUp;
    }
    
    public void setXaPageNUp(final int field_41_xaPageNUp) {
        this.field_41_xaPageNUp = field_41_xaPageNUp;
    }
    
    public int getYaPageNUp() {
        return this.field_42_yaPageNUp;
    }
    
    public void setYaPageNUp(final int field_42_yaPageNUp) {
        this.field_42_yaPageNUp = field_42_yaPageNUp;
    }
    
    public int getDxaLeft() {
        return this.field_43_dxaLeft;
    }
    
    public void setDxaLeft(final int field_43_dxaLeft) {
        this.field_43_dxaLeft = field_43_dxaLeft;
    }
    
    public int getDxaRight() {
        return this.field_44_dxaRight;
    }
    
    public void setDxaRight(final int field_44_dxaRight) {
        this.field_44_dxaRight = field_44_dxaRight;
    }
    
    public int getDyaTop() {
        return this.field_45_dyaTop;
    }
    
    public void setDyaTop(final int field_45_dyaTop) {
        this.field_45_dyaTop = field_45_dyaTop;
    }
    
    public int getDyaBottom() {
        return this.field_46_dyaBottom;
    }
    
    public void setDyaBottom(final int field_46_dyaBottom) {
        this.field_46_dyaBottom = field_46_dyaBottom;
    }
    
    public int getDzaGutter() {
        return this.field_47_dzaGutter;
    }
    
    public void setDzaGutter(final int field_47_dzaGutter) {
        this.field_47_dzaGutter = field_47_dzaGutter;
    }
    
    public int getDyaHdrTop() {
        return this.field_48_dyaHdrTop;
    }
    
    public void setDyaHdrTop(final int field_48_dyaHdrTop) {
        this.field_48_dyaHdrTop = field_48_dyaHdrTop;
    }
    
    public int getDyaHdrBottom() {
        return this.field_49_dyaHdrBottom;
    }
    
    public void setDyaHdrBottom(final int field_49_dyaHdrBottom) {
        this.field_49_dyaHdrBottom = field_49_dyaHdrBottom;
    }
    
    public int getCcolM1() {
        return this.field_50_ccolM1;
    }
    
    public void setCcolM1(final int field_50_ccolM1) {
        this.field_50_ccolM1 = field_50_ccolM1;
    }
    
    public boolean getFEvenlySpaced() {
        return this.field_51_fEvenlySpaced;
    }
    
    public void setFEvenlySpaced(final boolean field_51_fEvenlySpaced) {
        this.field_51_fEvenlySpaced = field_51_fEvenlySpaced;
    }
    
    public byte getUnused5() {
        return this.field_52_unused5;
    }
    
    public void setUnused5(final byte field_52_unused5) {
        this.field_52_unused5 = field_52_unused5;
    }
    
    public int getDxaColumns() {
        return this.field_53_dxaColumns;
    }
    
    public void setDxaColumns(final int field_53_dxaColumns) {
        this.field_53_dxaColumns = field_53_dxaColumns;
    }
    
    public int[] getRgdxaColumn() {
        return this.field_54_rgdxaColumn;
    }
    
    public void setRgdxaColumn(final int[] field_54_rgdxaColumn) {
        this.field_54_rgdxaColumn = field_54_rgdxaColumn;
    }
    
    public int getDxaColumnWidth() {
        return this.field_55_dxaColumnWidth;
    }
    
    public void setDxaColumnWidth(final int field_55_dxaColumnWidth) {
        this.field_55_dxaColumnWidth = field_55_dxaColumnWidth;
    }
    
    public byte getDmOrientFirst() {
        return this.field_56_dmOrientFirst;
    }
    
    public void setDmOrientFirst(final byte field_56_dmOrientFirst) {
        this.field_56_dmOrientFirst = field_56_dmOrientFirst;
    }
    
    public byte getFLayout() {
        return this.field_57_fLayout;
    }
    
    public void setFLayout(final byte field_57_fLayout) {
        this.field_57_fLayout = field_57_fLayout;
    }
    
    public short getUnused6() {
        return this.field_58_unused6;
    }
    
    public void setUnused6(final short field_58_unused6) {
        this.field_58_unused6 = field_58_unused6;
    }
    
    public byte[] getOlstAnm() {
        return this.field_59_olstAnm;
    }
    
    public void setOlstAnm(final byte[] field_59_olstAnm) {
        this.field_59_olstAnm = field_59_olstAnm;
    }
}
