// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes.definitions;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.hdf.model.hdftypes.HDFType;

@Deprecated
public abstract class FIBAbstractType implements HDFType
{
    private int field_1_wIdent;
    private int field_2_nFib;
    private int field_3_nProduct;
    private int field_4_lid;
    private int field_5_pnNext;
    private short field_6_options;
    private static BitField fDot;
    private static BitField fGlsy;
    private static BitField fComplex;
    private static BitField fHasPic;
    private static BitField cQuickSaves;
    private static BitField fEncrypted;
    private static BitField fWhichTblStm;
    private static BitField fReadOnlyRecommended;
    private static BitField fWriteReservation;
    private static BitField fExtChar;
    private static BitField fLoadOverride;
    private static BitField fFarEast;
    private static BitField fCrypto;
    private int field_7_nFibBack;
    private int field_8_lKey;
    private int field_9_envr;
    private short field_10_history;
    private static BitField fMac;
    private static BitField fEmptySpecial;
    private static BitField fLoadOverridePage;
    private static BitField fFutureSavedUndo;
    private static BitField fWord97Saved;
    private static BitField fSpare0;
    private int field_11_chs;
    private int field_12_chsTables;
    private int field_13_fcMin;
    private int field_14_fcMac;
    private int field_15_csw;
    private int field_16_wMagicCreated;
    private int field_17_wMagicRevised;
    private int field_18_wMagicCreatedPrivate;
    private int field_19_wMagicRevisedPrivate;
    private int field_20_pnFbpChpFirst_W6;
    private int field_21_pnChpFirst_W6;
    private int field_22_cpnBteChp_W6;
    private int field_23_pnFbpPapFirst_W6;
    private int field_24_pnPapFirst_W6;
    private int field_25_cpnBtePap_W6;
    private int field_26_pnFbpLvcFirst_W6;
    private int field_27_pnLvcFirst_W6;
    private int field_28_cpnBteLvc_W6;
    private int field_29_lidFE;
    private int field_30_clw;
    private int field_31_cbMac;
    private int field_32_lProductCreated;
    private int field_33_lProductRevised;
    private int field_34_ccpText;
    private int field_35_ccpFtn;
    private int field_36_ccpHdd;
    private int field_37_ccpMcr;
    private int field_38_ccpAtn;
    private int field_39_ccpEdn;
    private int field_40_ccpTxbx;
    private int field_41_ccpHdrTxbx;
    private int field_42_pnFbpChpFirst;
    private int field_43_pnChpFirst;
    private int field_44_cpnBteChp;
    private int field_45_pnFbpPapFirst;
    private int field_46_pnPapFirst;
    private int field_47_cpnBtePap;
    private int field_48_pnFbpLvcFirst;
    private int field_49_pnLvcFirst;
    private int field_50_cpnBteLvc;
    private int field_51_fcIslandFirst;
    private int field_52_fcIslandLim;
    private int field_53_cfclcb;
    private int field_54_fcStshfOrig;
    private int field_55_lcbStshfOrig;
    private int field_56_fcStshf;
    private int field_57_lcbStshf;
    private int field_58_fcPlcffndRef;
    private int field_59_lcbPlcffndRef;
    private int field_60_fcPlcffndTxt;
    private int field_61_lcbPlcffndTxt;
    private int field_62_fcPlcfandRef;
    private int field_63_lcbPlcfandRef;
    private int field_64_fcPlcfandTxt;
    private int field_65_lcbPlcfandTxt;
    private int field_66_fcPlcfsed;
    private int field_67_lcbPlcfsed;
    private int field_68_fcPlcpad;
    private int field_69_lcbPlcpad;
    private int field_70_fcPlcfphe;
    private int field_71_lcbPlcfphe;
    private int field_72_fcSttbfglsy;
    private int field_73_lcbSttbfglsy;
    private int field_74_fcPlcfglsy;
    private int field_75_lcbPlcfglsy;
    private int field_76_fcPlcfhdd;
    private int field_77_lcbPlcfhdd;
    private int field_78_fcPlcfbteChpx;
    private int field_79_lcbPlcfbteChpx;
    private int field_80_fcPlcfbtePapx;
    private int field_81_lcbPlcfbtePapx;
    private int field_82_fcPlcfsea;
    private int field_83_lcbPlcfsea;
    private int field_84_fcSttbfffn;
    private int field_85_lcbSttbfffn;
    private int field_86_fcPlcffldMom;
    private int field_87_lcbPlcffldMom;
    private int field_88_fcPlcffldHdr;
    private int field_89_lcbPlcffldHdr;
    private int field_90_fcPlcffldFtn;
    private int field_91_lcbPlcffldFtn;
    private int field_92_fcPlcffldAtn;
    private int field_93_lcbPlcffldAtn;
    private int field_94_fcPlcffldMcr;
    private int field_95_lcbPlcffldMcr;
    private int field_96_fcSttbfbkmk;
    private int field_97_lcbSttbfbkmk;
    private int field_98_fcPlcfbkf;
    private int field_99_lcbPlcfbkf;
    private int field_100_fcPlcfbkl;
    private int field_101_lcbPlcfbkl;
    private int field_102_fcCmds;
    private int field_103_lcbCmds;
    private int field_104_fcPlcmcr;
    private int field_105_lcbPlcmcr;
    private int field_106_fcSttbfmcr;
    private int field_107_lcbSttbfmcr;
    private int field_108_fcPrDrvr;
    private int field_109_lcbPrDrvr;
    private int field_110_fcPrEnvPort;
    private int field_111_lcbPrEnvPort;
    private int field_112_fcPrEnvLand;
    private int field_113_lcbPrEnvLand;
    private int field_114_fcWss;
    private int field_115_lcbWss;
    private int field_116_fcDop;
    private int field_117_lcbDop;
    private int field_118_fcSttbfAssoc;
    private int field_119_lcbSttbfAssoc;
    private int field_120_fcClx;
    private int field_121_lcbClx;
    private int field_122_fcPlcfpgdFtn;
    private int field_123_lcbPlcfpgdFtn;
    private int field_124_fcAutosaveSource;
    private int field_125_lcbAutosaveSource;
    private int field_126_fcGrpXstAtnOwners;
    private int field_127_lcbGrpXstAtnOwners;
    private int field_128_fcSttbfAtnbkmk;
    private int field_129_lcbSttbfAtnbkmk;
    private int field_130_fcPlcdoaMom;
    private int field_131_lcbPlcdoaMom;
    private int field_132_fcPlcdoaHdr;
    private int field_133_lcbPlcdoaHdr;
    private int field_134_fcPlcspaMom;
    private int field_135_lcbPlcspaMom;
    private int field_136_fcPlcspaHdr;
    private int field_137_lcbPlcspaHdr;
    private int field_138_fcPlcfAtnbkf;
    private int field_139_lcbPlcfAtnbkf;
    private int field_140_fcPlcfAtnbkl;
    private int field_141_lcbPlcfAtnbkl;
    private int field_142_fcPms;
    private int field_143_lcbPms;
    private int field_144_fcFormFldSttbs;
    private int field_145_lcbFormFldSttbs;
    private int field_146_fcPlcfendRef;
    private int field_147_lcbPlcfendRef;
    private int field_148_fcPlcfendTxt;
    private int field_149_lcbPlcfendTxt;
    private int field_150_fcPlcffldEdn;
    private int field_151_lcbPlcffldEdn;
    private int field_152_fcPlcfpgdEdn;
    private int field_153_lcbPlcfpgdEdn;
    private int field_154_fcDggInfo;
    private int field_155_lcbDggInfo;
    private int field_156_fcSttbfRMark;
    private int field_157_lcbSttbfRMark;
    private int field_158_fcSttbCaption;
    private int field_159_lcbSttbCaption;
    private int field_160_fcSttbAutoCaption;
    private int field_161_lcbSttbAutoCaption;
    private int field_162_fcPlcfwkb;
    private int field_163_lcbPlcfwkb;
    private int field_164_fcPlcfspl;
    private int field_165_lcbPlcfspl;
    private int field_166_fcPlcftxbxTxt;
    private int field_167_lcbPlcftxbxTxt;
    private int field_168_fcPlcffldTxbx;
    private int field_169_lcbPlcffldTxbx;
    private int field_170_fcPlcfhdrtxbxTxt;
    private int field_171_lcbPlcfhdrtxbxTxt;
    private int field_172_fcPlcffldHdrTxbx;
    private int field_173_lcbPlcffldHdrTxbx;
    private int field_174_fcStwUser;
    private int field_175_lcbStwUser;
    private int field_176_fcSttbttmbd;
    private int field_177_cbSttbttmbd;
    private int field_178_fcUnused;
    private int field_179_lcbUnused;
    private int field_180_fcPgdMother;
    private int field_181_lcbPgdMother;
    private int field_182_fcBkdMother;
    private int field_183_lcbBkdMother;
    private int field_184_fcPgdFtn;
    private int field_185_lcbPgdFtn;
    private int field_186_fcBkdFtn;
    private int field_187_lcbBkdFtn;
    private int field_188_fcPgdEdn;
    private int field_189_lcbPgdEdn;
    private int field_190_fcBkdEdn;
    private int field_191_lcbBkdEdn;
    private int field_192_fcSttbfIntlFld;
    private int field_193_lcbSttbfIntlFld;
    private int field_194_fcRouteSlip;
    private int field_195_lcbRouteSlip;
    private int field_196_fcSttbSavedBy;
    private int field_197_lcbSttbSavedBy;
    private int field_198_fcSttbFnm;
    private int field_199_lcbSttbFnm;
    private int field_200_fcPlcfLst;
    private int field_201_lcbPlcfLst;
    private int field_202_fcPlfLfo;
    private int field_203_lcbPlfLfo;
    private int field_204_fcPlcftxbxBkd;
    private int field_205_lcbPlcftxbxBkd;
    private int field_206_fcPlcftxbxHdrBkd;
    private int field_207_lcbPlcftxbxHdrBkd;
    private int field_208_fcDocUndo;
    private int field_209_lcbDocUndo;
    private int field_210_fcRgbuse;
    private int field_211_lcbRgbuse;
    private int field_212_fcUsp;
    private int field_213_lcbUsp;
    private int field_214_fcUskf;
    private int field_215_lcbUskf;
    private int field_216_fcPlcupcRgbuse;
    private int field_217_lcbPlcupcRgbuse;
    private int field_218_fcPlcupcUsp;
    private int field_219_lcbPlcupcUsp;
    private int field_220_fcSttbGlsyStyle;
    private int field_221_lcbSttbGlsyStyle;
    private int field_222_fcPlgosl;
    private int field_223_lcbPlgosl;
    private int field_224_fcPlcocx;
    private int field_225_lcbPlcocx;
    private int field_226_fcPlcfbteLvc;
    private int field_227_lcbPlcfbteLvc;
    private int field_228_dwLowDateTime;
    private int field_229_dwHighDateTime;
    private int field_230_fcPlcflvc;
    private int field_231_lcbPlcflvc;
    private int field_232_fcPlcasumy;
    private int field_233_lcbPlcasumy;
    private int field_234_fcPlcfgram;
    private int field_235_lcbPlcfgram;
    private int field_236_fcSttbListNames;
    private int field_237_lcbSttbListNames;
    private int field_238_fcSttbfUssr;
    private int field_239_lcbSttbfUssr;
    
    protected void fillFields(final byte[] data, final short size, final int offset) {
        this.field_1_wIdent = LittleEndian.getShort(data, 0 + offset);
        this.field_2_nFib = LittleEndian.getShort(data, 2 + offset);
        this.field_3_nProduct = LittleEndian.getShort(data, 4 + offset);
        this.field_4_lid = LittleEndian.getShort(data, 6 + offset);
        this.field_5_pnNext = LittleEndian.getShort(data, 8 + offset);
        this.field_6_options = LittleEndian.getShort(data, 10 + offset);
        this.field_7_nFibBack = LittleEndian.getShort(data, 12 + offset);
        this.field_8_lKey = LittleEndian.getShort(data, 14 + offset);
        this.field_9_envr = LittleEndian.getShort(data, 16 + offset);
        this.field_10_history = LittleEndian.getShort(data, 18 + offset);
        this.field_11_chs = LittleEndian.getShort(data, 20 + offset);
        this.field_12_chsTables = LittleEndian.getShort(data, 22 + offset);
        this.field_13_fcMin = LittleEndian.getInt(data, 24 + offset);
        this.field_14_fcMac = LittleEndian.getInt(data, 28 + offset);
        this.field_15_csw = LittleEndian.getShort(data, 32 + offset);
        this.field_16_wMagicCreated = LittleEndian.getShort(data, 34 + offset);
        this.field_17_wMagicRevised = LittleEndian.getShort(data, 36 + offset);
        this.field_18_wMagicCreatedPrivate = LittleEndian.getShort(data, 38 + offset);
        this.field_19_wMagicRevisedPrivate = LittleEndian.getShort(data, 40 + offset);
        this.field_20_pnFbpChpFirst_W6 = LittleEndian.getShort(data, 42 + offset);
        this.field_21_pnChpFirst_W6 = LittleEndian.getShort(data, 44 + offset);
        this.field_22_cpnBteChp_W6 = LittleEndian.getShort(data, 46 + offset);
        this.field_23_pnFbpPapFirst_W6 = LittleEndian.getShort(data, 48 + offset);
        this.field_24_pnPapFirst_W6 = LittleEndian.getShort(data, 50 + offset);
        this.field_25_cpnBtePap_W6 = LittleEndian.getShort(data, 52 + offset);
        this.field_26_pnFbpLvcFirst_W6 = LittleEndian.getShort(data, 54 + offset);
        this.field_27_pnLvcFirst_W6 = LittleEndian.getShort(data, 56 + offset);
        this.field_28_cpnBteLvc_W6 = LittleEndian.getShort(data, 58 + offset);
        this.field_29_lidFE = LittleEndian.getShort(data, 60 + offset);
        this.field_30_clw = LittleEndian.getShort(data, 62 + offset);
        this.field_31_cbMac = LittleEndian.getInt(data, 64 + offset);
        this.field_32_lProductCreated = LittleEndian.getInt(data, 68 + offset);
        this.field_33_lProductRevised = LittleEndian.getInt(data, 72 + offset);
        this.field_34_ccpText = LittleEndian.getInt(data, 76 + offset);
        this.field_35_ccpFtn = LittleEndian.getInt(data, 80 + offset);
        this.field_36_ccpHdd = LittleEndian.getInt(data, 84 + offset);
        this.field_37_ccpMcr = LittleEndian.getInt(data, 88 + offset);
        this.field_38_ccpAtn = LittleEndian.getInt(data, 92 + offset);
        this.field_39_ccpEdn = LittleEndian.getInt(data, 96 + offset);
        this.field_40_ccpTxbx = LittleEndian.getInt(data, 100 + offset);
        this.field_41_ccpHdrTxbx = LittleEndian.getInt(data, 104 + offset);
        this.field_42_pnFbpChpFirst = LittleEndian.getInt(data, 108 + offset);
        this.field_43_pnChpFirst = LittleEndian.getInt(data, 112 + offset);
        this.field_44_cpnBteChp = LittleEndian.getInt(data, 116 + offset);
        this.field_45_pnFbpPapFirst = LittleEndian.getInt(data, 120 + offset);
        this.field_46_pnPapFirst = LittleEndian.getInt(data, 124 + offset);
        this.field_47_cpnBtePap = LittleEndian.getInt(data, 128 + offset);
        this.field_48_pnFbpLvcFirst = LittleEndian.getInt(data, 132 + offset);
        this.field_49_pnLvcFirst = LittleEndian.getInt(data, 136 + offset);
        this.field_50_cpnBteLvc = LittleEndian.getInt(data, 140 + offset);
        this.field_51_fcIslandFirst = LittleEndian.getInt(data, 144 + offset);
        this.field_52_fcIslandLim = LittleEndian.getInt(data, 148 + offset);
        this.field_53_cfclcb = LittleEndian.getShort(data, 152 + offset);
        this.field_54_fcStshfOrig = LittleEndian.getInt(data, 154 + offset);
        this.field_55_lcbStshfOrig = LittleEndian.getInt(data, 158 + offset);
        this.field_56_fcStshf = LittleEndian.getInt(data, 162 + offset);
        this.field_57_lcbStshf = LittleEndian.getInt(data, 166 + offset);
        this.field_58_fcPlcffndRef = LittleEndian.getInt(data, 170 + offset);
        this.field_59_lcbPlcffndRef = LittleEndian.getInt(data, 174 + offset);
        this.field_60_fcPlcffndTxt = LittleEndian.getInt(data, 178 + offset);
        this.field_61_lcbPlcffndTxt = LittleEndian.getInt(data, 182 + offset);
        this.field_62_fcPlcfandRef = LittleEndian.getInt(data, 186 + offset);
        this.field_63_lcbPlcfandRef = LittleEndian.getInt(data, 190 + offset);
        this.field_64_fcPlcfandTxt = LittleEndian.getInt(data, 194 + offset);
        this.field_65_lcbPlcfandTxt = LittleEndian.getInt(data, 198 + offset);
        this.field_66_fcPlcfsed = LittleEndian.getInt(data, 202 + offset);
        this.field_67_lcbPlcfsed = LittleEndian.getInt(data, 206 + offset);
        this.field_68_fcPlcpad = LittleEndian.getInt(data, 210 + offset);
        this.field_69_lcbPlcpad = LittleEndian.getInt(data, 214 + offset);
        this.field_70_fcPlcfphe = LittleEndian.getInt(data, 218 + offset);
        this.field_71_lcbPlcfphe = LittleEndian.getInt(data, 222 + offset);
        this.field_72_fcSttbfglsy = LittleEndian.getInt(data, 226 + offset);
        this.field_73_lcbSttbfglsy = LittleEndian.getInt(data, 230 + offset);
        this.field_74_fcPlcfglsy = LittleEndian.getInt(data, 234 + offset);
        this.field_75_lcbPlcfglsy = LittleEndian.getInt(data, 238 + offset);
        this.field_76_fcPlcfhdd = LittleEndian.getInt(data, 242 + offset);
        this.field_77_lcbPlcfhdd = LittleEndian.getInt(data, 246 + offset);
        this.field_78_fcPlcfbteChpx = LittleEndian.getInt(data, 250 + offset);
        this.field_79_lcbPlcfbteChpx = LittleEndian.getInt(data, 254 + offset);
        this.field_80_fcPlcfbtePapx = LittleEndian.getInt(data, 258 + offset);
        this.field_81_lcbPlcfbtePapx = LittleEndian.getInt(data, 262 + offset);
        this.field_82_fcPlcfsea = LittleEndian.getInt(data, 266 + offset);
        this.field_83_lcbPlcfsea = LittleEndian.getInt(data, 270 + offset);
        this.field_84_fcSttbfffn = LittleEndian.getInt(data, 274 + offset);
        this.field_85_lcbSttbfffn = LittleEndian.getInt(data, 278 + offset);
        this.field_86_fcPlcffldMom = LittleEndian.getInt(data, 282 + offset);
        this.field_87_lcbPlcffldMom = LittleEndian.getInt(data, 286 + offset);
        this.field_88_fcPlcffldHdr = LittleEndian.getInt(data, 290 + offset);
        this.field_89_lcbPlcffldHdr = LittleEndian.getInt(data, 294 + offset);
        this.field_90_fcPlcffldFtn = LittleEndian.getInt(data, 298 + offset);
        this.field_91_lcbPlcffldFtn = LittleEndian.getInt(data, 302 + offset);
        this.field_92_fcPlcffldAtn = LittleEndian.getInt(data, 306 + offset);
        this.field_93_lcbPlcffldAtn = LittleEndian.getInt(data, 310 + offset);
        this.field_94_fcPlcffldMcr = LittleEndian.getInt(data, 314 + offset);
        this.field_95_lcbPlcffldMcr = LittleEndian.getInt(data, 318 + offset);
        this.field_96_fcSttbfbkmk = LittleEndian.getInt(data, 322 + offset);
        this.field_97_lcbSttbfbkmk = LittleEndian.getInt(data, 326 + offset);
        this.field_98_fcPlcfbkf = LittleEndian.getInt(data, 330 + offset);
        this.field_99_lcbPlcfbkf = LittleEndian.getInt(data, 334 + offset);
        this.field_100_fcPlcfbkl = LittleEndian.getInt(data, 338 + offset);
        this.field_101_lcbPlcfbkl = LittleEndian.getInt(data, 342 + offset);
        this.field_102_fcCmds = LittleEndian.getInt(data, 346 + offset);
        this.field_103_lcbCmds = LittleEndian.getInt(data, 350 + offset);
        this.field_104_fcPlcmcr = LittleEndian.getInt(data, 354 + offset);
        this.field_105_lcbPlcmcr = LittleEndian.getInt(data, 358 + offset);
        this.field_106_fcSttbfmcr = LittleEndian.getInt(data, 362 + offset);
        this.field_107_lcbSttbfmcr = LittleEndian.getInt(data, 366 + offset);
        this.field_108_fcPrDrvr = LittleEndian.getInt(data, 370 + offset);
        this.field_109_lcbPrDrvr = LittleEndian.getInt(data, 374 + offset);
        this.field_110_fcPrEnvPort = LittleEndian.getInt(data, 378 + offset);
        this.field_111_lcbPrEnvPort = LittleEndian.getInt(data, 382 + offset);
        this.field_112_fcPrEnvLand = LittleEndian.getInt(data, 386 + offset);
        this.field_113_lcbPrEnvLand = LittleEndian.getInt(data, 390 + offset);
        this.field_114_fcWss = LittleEndian.getInt(data, 394 + offset);
        this.field_115_lcbWss = LittleEndian.getInt(data, 398 + offset);
        this.field_116_fcDop = LittleEndian.getInt(data, 402 + offset);
        this.field_117_lcbDop = LittleEndian.getInt(data, 406 + offset);
        this.field_118_fcSttbfAssoc = LittleEndian.getInt(data, 410 + offset);
        this.field_119_lcbSttbfAssoc = LittleEndian.getInt(data, 414 + offset);
        this.field_120_fcClx = LittleEndian.getInt(data, 418 + offset);
        this.field_121_lcbClx = LittleEndian.getInt(data, 422 + offset);
        this.field_122_fcPlcfpgdFtn = LittleEndian.getInt(data, 426 + offset);
        this.field_123_lcbPlcfpgdFtn = LittleEndian.getInt(data, 430 + offset);
        this.field_124_fcAutosaveSource = LittleEndian.getInt(data, 434 + offset);
        this.field_125_lcbAutosaveSource = LittleEndian.getInt(data, 438 + offset);
        this.field_126_fcGrpXstAtnOwners = LittleEndian.getInt(data, 442 + offset);
        this.field_127_lcbGrpXstAtnOwners = LittleEndian.getInt(data, 446 + offset);
        this.field_128_fcSttbfAtnbkmk = LittleEndian.getInt(data, 450 + offset);
        this.field_129_lcbSttbfAtnbkmk = LittleEndian.getInt(data, 454 + offset);
        this.field_130_fcPlcdoaMom = LittleEndian.getInt(data, 458 + offset);
        this.field_131_lcbPlcdoaMom = LittleEndian.getInt(data, 462 + offset);
        this.field_132_fcPlcdoaHdr = LittleEndian.getInt(data, 466 + offset);
        this.field_133_lcbPlcdoaHdr = LittleEndian.getInt(data, 470 + offset);
        this.field_134_fcPlcspaMom = LittleEndian.getInt(data, 474 + offset);
        this.field_135_lcbPlcspaMom = LittleEndian.getInt(data, 478 + offset);
        this.field_136_fcPlcspaHdr = LittleEndian.getInt(data, 482 + offset);
        this.field_137_lcbPlcspaHdr = LittleEndian.getInt(data, 486 + offset);
        this.field_138_fcPlcfAtnbkf = LittleEndian.getInt(data, 490 + offset);
        this.field_139_lcbPlcfAtnbkf = LittleEndian.getInt(data, 494 + offset);
        this.field_140_fcPlcfAtnbkl = LittleEndian.getInt(data, 498 + offset);
        this.field_141_lcbPlcfAtnbkl = LittleEndian.getInt(data, 502 + offset);
        this.field_142_fcPms = LittleEndian.getInt(data, 506 + offset);
        this.field_143_lcbPms = LittleEndian.getInt(data, 510 + offset);
        this.field_144_fcFormFldSttbs = LittleEndian.getInt(data, 514 + offset);
        this.field_145_lcbFormFldSttbs = LittleEndian.getInt(data, 518 + offset);
        this.field_146_fcPlcfendRef = LittleEndian.getInt(data, 522 + offset);
        this.field_147_lcbPlcfendRef = LittleEndian.getInt(data, 526 + offset);
        this.field_148_fcPlcfendTxt = LittleEndian.getInt(data, 530 + offset);
        this.field_149_lcbPlcfendTxt = LittleEndian.getInt(data, 534 + offset);
        this.field_150_fcPlcffldEdn = LittleEndian.getInt(data, 538 + offset);
        this.field_151_lcbPlcffldEdn = LittleEndian.getInt(data, 542 + offset);
        this.field_152_fcPlcfpgdEdn = LittleEndian.getInt(data, 546 + offset);
        this.field_153_lcbPlcfpgdEdn = LittleEndian.getInt(data, 550 + offset);
        this.field_154_fcDggInfo = LittleEndian.getInt(data, 554 + offset);
        this.field_155_lcbDggInfo = LittleEndian.getInt(data, 558 + offset);
        this.field_156_fcSttbfRMark = LittleEndian.getInt(data, 562 + offset);
        this.field_157_lcbSttbfRMark = LittleEndian.getInt(data, 566 + offset);
        this.field_158_fcSttbCaption = LittleEndian.getInt(data, 570 + offset);
        this.field_159_lcbSttbCaption = LittleEndian.getInt(data, 574 + offset);
        this.field_160_fcSttbAutoCaption = LittleEndian.getInt(data, 578 + offset);
        this.field_161_lcbSttbAutoCaption = LittleEndian.getInt(data, 582 + offset);
        this.field_162_fcPlcfwkb = LittleEndian.getInt(data, 586 + offset);
        this.field_163_lcbPlcfwkb = LittleEndian.getInt(data, 590 + offset);
        this.field_164_fcPlcfspl = LittleEndian.getInt(data, 594 + offset);
        this.field_165_lcbPlcfspl = LittleEndian.getInt(data, 598 + offset);
        this.field_166_fcPlcftxbxTxt = LittleEndian.getInt(data, 602 + offset);
        this.field_167_lcbPlcftxbxTxt = LittleEndian.getInt(data, 606 + offset);
        this.field_168_fcPlcffldTxbx = LittleEndian.getInt(data, 610 + offset);
        this.field_169_lcbPlcffldTxbx = LittleEndian.getInt(data, 614 + offset);
        this.field_170_fcPlcfhdrtxbxTxt = LittleEndian.getInt(data, 618 + offset);
        this.field_171_lcbPlcfhdrtxbxTxt = LittleEndian.getInt(data, 622 + offset);
        this.field_172_fcPlcffldHdrTxbx = LittleEndian.getInt(data, 626 + offset);
        this.field_173_lcbPlcffldHdrTxbx = LittleEndian.getInt(data, 630 + offset);
        this.field_174_fcStwUser = LittleEndian.getInt(data, 634 + offset);
        this.field_175_lcbStwUser = LittleEndian.getInt(data, 638 + offset);
        this.field_176_fcSttbttmbd = LittleEndian.getInt(data, 642 + offset);
        this.field_177_cbSttbttmbd = LittleEndian.getInt(data, 646 + offset);
        this.field_178_fcUnused = LittleEndian.getInt(data, 650 + offset);
        this.field_179_lcbUnused = LittleEndian.getInt(data, 654 + offset);
        this.field_180_fcPgdMother = LittleEndian.getInt(data, 658 + offset);
        this.field_181_lcbPgdMother = LittleEndian.getInt(data, 662 + offset);
        this.field_182_fcBkdMother = LittleEndian.getInt(data, 666 + offset);
        this.field_183_lcbBkdMother = LittleEndian.getInt(data, 670 + offset);
        this.field_184_fcPgdFtn = LittleEndian.getInt(data, 674 + offset);
        this.field_185_lcbPgdFtn = LittleEndian.getInt(data, 678 + offset);
        this.field_186_fcBkdFtn = LittleEndian.getInt(data, 682 + offset);
        this.field_187_lcbBkdFtn = LittleEndian.getInt(data, 686 + offset);
        this.field_188_fcPgdEdn = LittleEndian.getInt(data, 690 + offset);
        this.field_189_lcbPgdEdn = LittleEndian.getInt(data, 694 + offset);
        this.field_190_fcBkdEdn = LittleEndian.getInt(data, 698 + offset);
        this.field_191_lcbBkdEdn = LittleEndian.getInt(data, 702 + offset);
        this.field_192_fcSttbfIntlFld = LittleEndian.getInt(data, 706 + offset);
        this.field_193_lcbSttbfIntlFld = LittleEndian.getInt(data, 710 + offset);
        this.field_194_fcRouteSlip = LittleEndian.getInt(data, 714 + offset);
        this.field_195_lcbRouteSlip = LittleEndian.getInt(data, 718 + offset);
        this.field_196_fcSttbSavedBy = LittleEndian.getInt(data, 722 + offset);
        this.field_197_lcbSttbSavedBy = LittleEndian.getInt(data, 726 + offset);
        this.field_198_fcSttbFnm = LittleEndian.getInt(data, 730 + offset);
        this.field_199_lcbSttbFnm = LittleEndian.getInt(data, 734 + offset);
        this.field_200_fcPlcfLst = LittleEndian.getInt(data, 738 + offset);
        this.field_201_lcbPlcfLst = LittleEndian.getInt(data, 742 + offset);
        this.field_202_fcPlfLfo = LittleEndian.getInt(data, 746 + offset);
        this.field_203_lcbPlfLfo = LittleEndian.getInt(data, 750 + offset);
        this.field_204_fcPlcftxbxBkd = LittleEndian.getInt(data, 754 + offset);
        this.field_205_lcbPlcftxbxBkd = LittleEndian.getInt(data, 758 + offset);
        this.field_206_fcPlcftxbxHdrBkd = LittleEndian.getInt(data, 762 + offset);
        this.field_207_lcbPlcftxbxHdrBkd = LittleEndian.getInt(data, 766 + offset);
        this.field_208_fcDocUndo = LittleEndian.getInt(data, 770 + offset);
        this.field_209_lcbDocUndo = LittleEndian.getInt(data, 774 + offset);
        this.field_210_fcRgbuse = LittleEndian.getInt(data, 778 + offset);
        this.field_211_lcbRgbuse = LittleEndian.getInt(data, 782 + offset);
        this.field_212_fcUsp = LittleEndian.getInt(data, 786 + offset);
        this.field_213_lcbUsp = LittleEndian.getInt(data, 790 + offset);
        this.field_214_fcUskf = LittleEndian.getInt(data, 794 + offset);
        this.field_215_lcbUskf = LittleEndian.getInt(data, 798 + offset);
        this.field_216_fcPlcupcRgbuse = LittleEndian.getInt(data, 802 + offset);
        this.field_217_lcbPlcupcRgbuse = LittleEndian.getInt(data, 806 + offset);
        this.field_218_fcPlcupcUsp = LittleEndian.getInt(data, 810 + offset);
        this.field_219_lcbPlcupcUsp = LittleEndian.getInt(data, 814 + offset);
        this.field_220_fcSttbGlsyStyle = LittleEndian.getInt(data, 818 + offset);
        this.field_221_lcbSttbGlsyStyle = LittleEndian.getInt(data, 822 + offset);
        this.field_222_fcPlgosl = LittleEndian.getInt(data, 826 + offset);
        this.field_223_lcbPlgosl = LittleEndian.getInt(data, 830 + offset);
        this.field_224_fcPlcocx = LittleEndian.getInt(data, 834 + offset);
        this.field_225_lcbPlcocx = LittleEndian.getInt(data, 838 + offset);
        this.field_226_fcPlcfbteLvc = LittleEndian.getInt(data, 842 + offset);
        this.field_227_lcbPlcfbteLvc = LittleEndian.getInt(data, 846 + offset);
        this.field_228_dwLowDateTime = LittleEndian.getInt(data, 850 + offset);
        this.field_229_dwHighDateTime = LittleEndian.getInt(data, 854 + offset);
        this.field_230_fcPlcflvc = LittleEndian.getInt(data, 858 + offset);
        this.field_231_lcbPlcflvc = LittleEndian.getInt(data, 862 + offset);
        this.field_232_fcPlcasumy = LittleEndian.getInt(data, 866 + offset);
        this.field_233_lcbPlcasumy = LittleEndian.getInt(data, 870 + offset);
        this.field_234_fcPlcfgram = LittleEndian.getInt(data, 874 + offset);
        this.field_235_lcbPlcfgram = LittleEndian.getInt(data, 878 + offset);
        this.field_236_fcSttbListNames = LittleEndian.getInt(data, 882 + offset);
        this.field_237_lcbSttbListNames = LittleEndian.getInt(data, 886 + offset);
        this.field_238_fcSttbfUssr = LittleEndian.getInt(data, 890 + offset);
        this.field_239_lcbSttbfUssr = LittleEndian.getInt(data, 894 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, (short)this.field_1_wIdent);
        LittleEndian.putShort(data, 2 + offset, (short)this.field_2_nFib);
        LittleEndian.putShort(data, 4 + offset, (short)this.field_3_nProduct);
        LittleEndian.putShort(data, 6 + offset, (short)this.field_4_lid);
        LittleEndian.putShort(data, 8 + offset, (short)this.field_5_pnNext);
        LittleEndian.putShort(data, 10 + offset, this.field_6_options);
        LittleEndian.putShort(data, 12 + offset, (short)this.field_7_nFibBack);
        LittleEndian.putShort(data, 14 + offset, (short)this.field_8_lKey);
        LittleEndian.putShort(data, 16 + offset, (short)this.field_9_envr);
        LittleEndian.putShort(data, 18 + offset, this.field_10_history);
        LittleEndian.putShort(data, 20 + offset, (short)this.field_11_chs);
        LittleEndian.putShort(data, 22 + offset, (short)this.field_12_chsTables);
        LittleEndian.putInt(data, 24 + offset, this.field_13_fcMin);
        LittleEndian.putInt(data, 28 + offset, this.field_14_fcMac);
        LittleEndian.putShort(data, 32 + offset, (short)this.field_15_csw);
        LittleEndian.putShort(data, 34 + offset, (short)this.field_16_wMagicCreated);
        LittleEndian.putShort(data, 36 + offset, (short)this.field_17_wMagicRevised);
        LittleEndian.putShort(data, 38 + offset, (short)this.field_18_wMagicCreatedPrivate);
        LittleEndian.putShort(data, 40 + offset, (short)this.field_19_wMagicRevisedPrivate);
        LittleEndian.putShort(data, 42 + offset, (short)this.field_20_pnFbpChpFirst_W6);
        LittleEndian.putShort(data, 44 + offset, (short)this.field_21_pnChpFirst_W6);
        LittleEndian.putShort(data, 46 + offset, (short)this.field_22_cpnBteChp_W6);
        LittleEndian.putShort(data, 48 + offset, (short)this.field_23_pnFbpPapFirst_W6);
        LittleEndian.putShort(data, 50 + offset, (short)this.field_24_pnPapFirst_W6);
        LittleEndian.putShort(data, 52 + offset, (short)this.field_25_cpnBtePap_W6);
        LittleEndian.putShort(data, 54 + offset, (short)this.field_26_pnFbpLvcFirst_W6);
        LittleEndian.putShort(data, 56 + offset, (short)this.field_27_pnLvcFirst_W6);
        LittleEndian.putShort(data, 58 + offset, (short)this.field_28_cpnBteLvc_W6);
        LittleEndian.putShort(data, 60 + offset, (short)this.field_29_lidFE);
        LittleEndian.putShort(data, 62 + offset, (short)this.field_30_clw);
        LittleEndian.putInt(data, 64 + offset, this.field_31_cbMac);
        LittleEndian.putInt(data, 68 + offset, this.field_32_lProductCreated);
        LittleEndian.putInt(data, 72 + offset, this.field_33_lProductRevised);
        LittleEndian.putInt(data, 76 + offset, this.field_34_ccpText);
        LittleEndian.putInt(data, 80 + offset, this.field_35_ccpFtn);
        LittleEndian.putInt(data, 84 + offset, this.field_36_ccpHdd);
        LittleEndian.putInt(data, 88 + offset, this.field_37_ccpMcr);
        LittleEndian.putInt(data, 92 + offset, this.field_38_ccpAtn);
        LittleEndian.putInt(data, 96 + offset, this.field_39_ccpEdn);
        LittleEndian.putInt(data, 100 + offset, this.field_40_ccpTxbx);
        LittleEndian.putInt(data, 104 + offset, this.field_41_ccpHdrTxbx);
        LittleEndian.putInt(data, 108 + offset, this.field_42_pnFbpChpFirst);
        LittleEndian.putInt(data, 112 + offset, this.field_43_pnChpFirst);
        LittleEndian.putInt(data, 116 + offset, this.field_44_cpnBteChp);
        LittleEndian.putInt(data, 120 + offset, this.field_45_pnFbpPapFirst);
        LittleEndian.putInt(data, 124 + offset, this.field_46_pnPapFirst);
        LittleEndian.putInt(data, 128 + offset, this.field_47_cpnBtePap);
        LittleEndian.putInt(data, 132 + offset, this.field_48_pnFbpLvcFirst);
        LittleEndian.putInt(data, 136 + offset, this.field_49_pnLvcFirst);
        LittleEndian.putInt(data, 140 + offset, this.field_50_cpnBteLvc);
        LittleEndian.putInt(data, 144 + offset, this.field_51_fcIslandFirst);
        LittleEndian.putInt(data, 148 + offset, this.field_52_fcIslandLim);
        LittleEndian.putShort(data, 152 + offset, (short)this.field_53_cfclcb);
        LittleEndian.putInt(data, 154 + offset, this.field_54_fcStshfOrig);
        LittleEndian.putInt(data, 158 + offset, this.field_55_lcbStshfOrig);
        LittleEndian.putInt(data, 162 + offset, this.field_56_fcStshf);
        LittleEndian.putInt(data, 166 + offset, this.field_57_lcbStshf);
        LittleEndian.putInt(data, 170 + offset, this.field_58_fcPlcffndRef);
        LittleEndian.putInt(data, 174 + offset, this.field_59_lcbPlcffndRef);
        LittleEndian.putInt(data, 178 + offset, this.field_60_fcPlcffndTxt);
        LittleEndian.putInt(data, 182 + offset, this.field_61_lcbPlcffndTxt);
        LittleEndian.putInt(data, 186 + offset, this.field_62_fcPlcfandRef);
        LittleEndian.putInt(data, 190 + offset, this.field_63_lcbPlcfandRef);
        LittleEndian.putInt(data, 194 + offset, this.field_64_fcPlcfandTxt);
        LittleEndian.putInt(data, 198 + offset, this.field_65_lcbPlcfandTxt);
        LittleEndian.putInt(data, 202 + offset, this.field_66_fcPlcfsed);
        LittleEndian.putInt(data, 206 + offset, this.field_67_lcbPlcfsed);
        LittleEndian.putInt(data, 210 + offset, this.field_68_fcPlcpad);
        LittleEndian.putInt(data, 214 + offset, this.field_69_lcbPlcpad);
        LittleEndian.putInt(data, 218 + offset, this.field_70_fcPlcfphe);
        LittleEndian.putInt(data, 222 + offset, this.field_71_lcbPlcfphe);
        LittleEndian.putInt(data, 226 + offset, this.field_72_fcSttbfglsy);
        LittleEndian.putInt(data, 230 + offset, this.field_73_lcbSttbfglsy);
        LittleEndian.putInt(data, 234 + offset, this.field_74_fcPlcfglsy);
        LittleEndian.putInt(data, 238 + offset, this.field_75_lcbPlcfglsy);
        LittleEndian.putInt(data, 242 + offset, this.field_76_fcPlcfhdd);
        LittleEndian.putInt(data, 246 + offset, this.field_77_lcbPlcfhdd);
        LittleEndian.putInt(data, 250 + offset, this.field_78_fcPlcfbteChpx);
        LittleEndian.putInt(data, 254 + offset, this.field_79_lcbPlcfbteChpx);
        LittleEndian.putInt(data, 258 + offset, this.field_80_fcPlcfbtePapx);
        LittleEndian.putInt(data, 262 + offset, this.field_81_lcbPlcfbtePapx);
        LittleEndian.putInt(data, 266 + offset, this.field_82_fcPlcfsea);
        LittleEndian.putInt(data, 270 + offset, this.field_83_lcbPlcfsea);
        LittleEndian.putInt(data, 274 + offset, this.field_84_fcSttbfffn);
        LittleEndian.putInt(data, 278 + offset, this.field_85_lcbSttbfffn);
        LittleEndian.putInt(data, 282 + offset, this.field_86_fcPlcffldMom);
        LittleEndian.putInt(data, 286 + offset, this.field_87_lcbPlcffldMom);
        LittleEndian.putInt(data, 290 + offset, this.field_88_fcPlcffldHdr);
        LittleEndian.putInt(data, 294 + offset, this.field_89_lcbPlcffldHdr);
        LittleEndian.putInt(data, 298 + offset, this.field_90_fcPlcffldFtn);
        LittleEndian.putInt(data, 302 + offset, this.field_91_lcbPlcffldFtn);
        LittleEndian.putInt(data, 306 + offset, this.field_92_fcPlcffldAtn);
        LittleEndian.putInt(data, 310 + offset, this.field_93_lcbPlcffldAtn);
        LittleEndian.putInt(data, 314 + offset, this.field_94_fcPlcffldMcr);
        LittleEndian.putInt(data, 318 + offset, this.field_95_lcbPlcffldMcr);
        LittleEndian.putInt(data, 322 + offset, this.field_96_fcSttbfbkmk);
        LittleEndian.putInt(data, 326 + offset, this.field_97_lcbSttbfbkmk);
        LittleEndian.putInt(data, 330 + offset, this.field_98_fcPlcfbkf);
        LittleEndian.putInt(data, 334 + offset, this.field_99_lcbPlcfbkf);
        LittleEndian.putInt(data, 338 + offset, this.field_100_fcPlcfbkl);
        LittleEndian.putInt(data, 342 + offset, this.field_101_lcbPlcfbkl);
        LittleEndian.putInt(data, 346 + offset, this.field_102_fcCmds);
        LittleEndian.putInt(data, 350 + offset, this.field_103_lcbCmds);
        LittleEndian.putInt(data, 354 + offset, this.field_104_fcPlcmcr);
        LittleEndian.putInt(data, 358 + offset, this.field_105_lcbPlcmcr);
        LittleEndian.putInt(data, 362 + offset, this.field_106_fcSttbfmcr);
        LittleEndian.putInt(data, 366 + offset, this.field_107_lcbSttbfmcr);
        LittleEndian.putInt(data, 370 + offset, this.field_108_fcPrDrvr);
        LittleEndian.putInt(data, 374 + offset, this.field_109_lcbPrDrvr);
        LittleEndian.putInt(data, 378 + offset, this.field_110_fcPrEnvPort);
        LittleEndian.putInt(data, 382 + offset, this.field_111_lcbPrEnvPort);
        LittleEndian.putInt(data, 386 + offset, this.field_112_fcPrEnvLand);
        LittleEndian.putInt(data, 390 + offset, this.field_113_lcbPrEnvLand);
        LittleEndian.putInt(data, 394 + offset, this.field_114_fcWss);
        LittleEndian.putInt(data, 398 + offset, this.field_115_lcbWss);
        LittleEndian.putInt(data, 402 + offset, this.field_116_fcDop);
        LittleEndian.putInt(data, 406 + offset, this.field_117_lcbDop);
        LittleEndian.putInt(data, 410 + offset, this.field_118_fcSttbfAssoc);
        LittleEndian.putInt(data, 414 + offset, this.field_119_lcbSttbfAssoc);
        LittleEndian.putInt(data, 418 + offset, this.field_120_fcClx);
        LittleEndian.putInt(data, 422 + offset, this.field_121_lcbClx);
        LittleEndian.putInt(data, 426 + offset, this.field_122_fcPlcfpgdFtn);
        LittleEndian.putInt(data, 430 + offset, this.field_123_lcbPlcfpgdFtn);
        LittleEndian.putInt(data, 434 + offset, this.field_124_fcAutosaveSource);
        LittleEndian.putInt(data, 438 + offset, this.field_125_lcbAutosaveSource);
        LittleEndian.putInt(data, 442 + offset, this.field_126_fcGrpXstAtnOwners);
        LittleEndian.putInt(data, 446 + offset, this.field_127_lcbGrpXstAtnOwners);
        LittleEndian.putInt(data, 450 + offset, this.field_128_fcSttbfAtnbkmk);
        LittleEndian.putInt(data, 454 + offset, this.field_129_lcbSttbfAtnbkmk);
        LittleEndian.putInt(data, 458 + offset, this.field_130_fcPlcdoaMom);
        LittleEndian.putInt(data, 462 + offset, this.field_131_lcbPlcdoaMom);
        LittleEndian.putInt(data, 466 + offset, this.field_132_fcPlcdoaHdr);
        LittleEndian.putInt(data, 470 + offset, this.field_133_lcbPlcdoaHdr);
        LittleEndian.putInt(data, 474 + offset, this.field_134_fcPlcspaMom);
        LittleEndian.putInt(data, 478 + offset, this.field_135_lcbPlcspaMom);
        LittleEndian.putInt(data, 482 + offset, this.field_136_fcPlcspaHdr);
        LittleEndian.putInt(data, 486 + offset, this.field_137_lcbPlcspaHdr);
        LittleEndian.putInt(data, 490 + offset, this.field_138_fcPlcfAtnbkf);
        LittleEndian.putInt(data, 494 + offset, this.field_139_lcbPlcfAtnbkf);
        LittleEndian.putInt(data, 498 + offset, this.field_140_fcPlcfAtnbkl);
        LittleEndian.putInt(data, 502 + offset, this.field_141_lcbPlcfAtnbkl);
        LittleEndian.putInt(data, 506 + offset, this.field_142_fcPms);
        LittleEndian.putInt(data, 510 + offset, this.field_143_lcbPms);
        LittleEndian.putInt(data, 514 + offset, this.field_144_fcFormFldSttbs);
        LittleEndian.putInt(data, 518 + offset, this.field_145_lcbFormFldSttbs);
        LittleEndian.putInt(data, 522 + offset, this.field_146_fcPlcfendRef);
        LittleEndian.putInt(data, 526 + offset, this.field_147_lcbPlcfendRef);
        LittleEndian.putInt(data, 530 + offset, this.field_148_fcPlcfendTxt);
        LittleEndian.putInt(data, 534 + offset, this.field_149_lcbPlcfendTxt);
        LittleEndian.putInt(data, 538 + offset, this.field_150_fcPlcffldEdn);
        LittleEndian.putInt(data, 542 + offset, this.field_151_lcbPlcffldEdn);
        LittleEndian.putInt(data, 546 + offset, this.field_152_fcPlcfpgdEdn);
        LittleEndian.putInt(data, 550 + offset, this.field_153_lcbPlcfpgdEdn);
        LittleEndian.putInt(data, 554 + offset, this.field_154_fcDggInfo);
        LittleEndian.putInt(data, 558 + offset, this.field_155_lcbDggInfo);
        LittleEndian.putInt(data, 562 + offset, this.field_156_fcSttbfRMark);
        LittleEndian.putInt(data, 566 + offset, this.field_157_lcbSttbfRMark);
        LittleEndian.putInt(data, 570 + offset, this.field_158_fcSttbCaption);
        LittleEndian.putInt(data, 574 + offset, this.field_159_lcbSttbCaption);
        LittleEndian.putInt(data, 578 + offset, this.field_160_fcSttbAutoCaption);
        LittleEndian.putInt(data, 582 + offset, this.field_161_lcbSttbAutoCaption);
        LittleEndian.putInt(data, 586 + offset, this.field_162_fcPlcfwkb);
        LittleEndian.putInt(data, 590 + offset, this.field_163_lcbPlcfwkb);
        LittleEndian.putInt(data, 594 + offset, this.field_164_fcPlcfspl);
        LittleEndian.putInt(data, 598 + offset, this.field_165_lcbPlcfspl);
        LittleEndian.putInt(data, 602 + offset, this.field_166_fcPlcftxbxTxt);
        LittleEndian.putInt(data, 606 + offset, this.field_167_lcbPlcftxbxTxt);
        LittleEndian.putInt(data, 610 + offset, this.field_168_fcPlcffldTxbx);
        LittleEndian.putInt(data, 614 + offset, this.field_169_lcbPlcffldTxbx);
        LittleEndian.putInt(data, 618 + offset, this.field_170_fcPlcfhdrtxbxTxt);
        LittleEndian.putInt(data, 622 + offset, this.field_171_lcbPlcfhdrtxbxTxt);
        LittleEndian.putInt(data, 626 + offset, this.field_172_fcPlcffldHdrTxbx);
        LittleEndian.putInt(data, 630 + offset, this.field_173_lcbPlcffldHdrTxbx);
        LittleEndian.putInt(data, 634 + offset, this.field_174_fcStwUser);
        LittleEndian.putInt(data, 638 + offset, this.field_175_lcbStwUser);
        LittleEndian.putInt(data, 642 + offset, this.field_176_fcSttbttmbd);
        LittleEndian.putInt(data, 646 + offset, this.field_177_cbSttbttmbd);
        LittleEndian.putInt(data, 650 + offset, this.field_178_fcUnused);
        LittleEndian.putInt(data, 654 + offset, this.field_179_lcbUnused);
        LittleEndian.putInt(data, 658 + offset, this.field_180_fcPgdMother);
        LittleEndian.putInt(data, 662 + offset, this.field_181_lcbPgdMother);
        LittleEndian.putInt(data, 666 + offset, this.field_182_fcBkdMother);
        LittleEndian.putInt(data, 670 + offset, this.field_183_lcbBkdMother);
        LittleEndian.putInt(data, 674 + offset, this.field_184_fcPgdFtn);
        LittleEndian.putInt(data, 678 + offset, this.field_185_lcbPgdFtn);
        LittleEndian.putInt(data, 682 + offset, this.field_186_fcBkdFtn);
        LittleEndian.putInt(data, 686 + offset, this.field_187_lcbBkdFtn);
        LittleEndian.putInt(data, 690 + offset, this.field_188_fcPgdEdn);
        LittleEndian.putInt(data, 694 + offset, this.field_189_lcbPgdEdn);
        LittleEndian.putInt(data, 698 + offset, this.field_190_fcBkdEdn);
        LittleEndian.putInt(data, 702 + offset, this.field_191_lcbBkdEdn);
        LittleEndian.putInt(data, 706 + offset, this.field_192_fcSttbfIntlFld);
        LittleEndian.putInt(data, 710 + offset, this.field_193_lcbSttbfIntlFld);
        LittleEndian.putInt(data, 714 + offset, this.field_194_fcRouteSlip);
        LittleEndian.putInt(data, 718 + offset, this.field_195_lcbRouteSlip);
        LittleEndian.putInt(data, 722 + offset, this.field_196_fcSttbSavedBy);
        LittleEndian.putInt(data, 726 + offset, this.field_197_lcbSttbSavedBy);
        LittleEndian.putInt(data, 730 + offset, this.field_198_fcSttbFnm);
        LittleEndian.putInt(data, 734 + offset, this.field_199_lcbSttbFnm);
        LittleEndian.putInt(data, 738 + offset, this.field_200_fcPlcfLst);
        LittleEndian.putInt(data, 742 + offset, this.field_201_lcbPlcfLst);
        LittleEndian.putInt(data, 746 + offset, this.field_202_fcPlfLfo);
        LittleEndian.putInt(data, 750 + offset, this.field_203_lcbPlfLfo);
        LittleEndian.putInt(data, 754 + offset, this.field_204_fcPlcftxbxBkd);
        LittleEndian.putInt(data, 758 + offset, this.field_205_lcbPlcftxbxBkd);
        LittleEndian.putInt(data, 762 + offset, this.field_206_fcPlcftxbxHdrBkd);
        LittleEndian.putInt(data, 766 + offset, this.field_207_lcbPlcftxbxHdrBkd);
        LittleEndian.putInt(data, 770 + offset, this.field_208_fcDocUndo);
        LittleEndian.putInt(data, 774 + offset, this.field_209_lcbDocUndo);
        LittleEndian.putInt(data, 778 + offset, this.field_210_fcRgbuse);
        LittleEndian.putInt(data, 782 + offset, this.field_211_lcbRgbuse);
        LittleEndian.putInt(data, 786 + offset, this.field_212_fcUsp);
        LittleEndian.putInt(data, 790 + offset, this.field_213_lcbUsp);
        LittleEndian.putInt(data, 794 + offset, this.field_214_fcUskf);
        LittleEndian.putInt(data, 798 + offset, this.field_215_lcbUskf);
        LittleEndian.putInt(data, 802 + offset, this.field_216_fcPlcupcRgbuse);
        LittleEndian.putInt(data, 806 + offset, this.field_217_lcbPlcupcRgbuse);
        LittleEndian.putInt(data, 810 + offset, this.field_218_fcPlcupcUsp);
        LittleEndian.putInt(data, 814 + offset, this.field_219_lcbPlcupcUsp);
        LittleEndian.putInt(data, 818 + offset, this.field_220_fcSttbGlsyStyle);
        LittleEndian.putInt(data, 822 + offset, this.field_221_lcbSttbGlsyStyle);
        LittleEndian.putInt(data, 826 + offset, this.field_222_fcPlgosl);
        LittleEndian.putInt(data, 830 + offset, this.field_223_lcbPlgosl);
        LittleEndian.putInt(data, 834 + offset, this.field_224_fcPlcocx);
        LittleEndian.putInt(data, 838 + offset, this.field_225_lcbPlcocx);
        LittleEndian.putInt(data, 842 + offset, this.field_226_fcPlcfbteLvc);
        LittleEndian.putInt(data, 846 + offset, this.field_227_lcbPlcfbteLvc);
        LittleEndian.putInt(data, 850 + offset, this.field_228_dwLowDateTime);
        LittleEndian.putInt(data, 854 + offset, this.field_229_dwHighDateTime);
        LittleEndian.putInt(data, 858 + offset, this.field_230_fcPlcflvc);
        LittleEndian.putInt(data, 862 + offset, this.field_231_lcbPlcflvc);
        LittleEndian.putInt(data, 866 + offset, this.field_232_fcPlcasumy);
        LittleEndian.putInt(data, 870 + offset, this.field_233_lcbPlcasumy);
        LittleEndian.putInt(data, 874 + offset, this.field_234_fcPlcfgram);
        LittleEndian.putInt(data, 878 + offset, this.field_235_lcbPlcfgram);
        LittleEndian.putInt(data, 882 + offset, this.field_236_fcSttbListNames);
        LittleEndian.putInt(data, 886 + offset, this.field_237_lcbSttbListNames);
        LittleEndian.putInt(data, 890 + offset, this.field_238_fcSttbfUssr);
        LittleEndian.putInt(data, 894 + offset, this.field_239_lcbSttbfUssr);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FIB]\n");
        buffer.append("    .wIdent               = ");
        buffer.append(HexDump.intToHex(this.getWIdent()));
        buffer.append(" (").append(this.getWIdent()).append(" )\n");
        buffer.append("    .nFib                 = ");
        buffer.append(HexDump.intToHex(this.getNFib()));
        buffer.append(" (").append(this.getNFib()).append(" )\n");
        buffer.append("    .nProduct             = ");
        buffer.append(HexDump.intToHex(this.getNProduct()));
        buffer.append(" (").append(this.getNProduct()).append(" )\n");
        buffer.append("    .lid                  = ");
        buffer.append(HexDump.intToHex(this.getLid()));
        buffer.append(" (").append(this.getLid()).append(" )\n");
        buffer.append("    .pnNext               = ");
        buffer.append(HexDump.intToHex(this.getPnNext()));
        buffer.append(" (").append(this.getPnNext()).append(" )\n");
        buffer.append("    .options              = ");
        buffer.append(HexDump.shortToHex(this.getOptions()));
        buffer.append(" (").append(this.getOptions()).append(" )\n");
        buffer.append("         .fDot                     = ").append(this.isFDot()).append('\n');
        buffer.append("         .fGlsy                    = ").append(this.isFGlsy()).append('\n');
        buffer.append("         .fComplex                 = ").append(this.isFComplex()).append('\n');
        buffer.append("         .fHasPic                  = ").append(this.isFHasPic()).append('\n');
        buffer.append("         .cQuickSaves              = ").append(this.getCQuickSaves()).append('\n');
        buffer.append("         .fEncrypted               = ").append(this.isFEncrypted()).append('\n');
        buffer.append("         .fWhichTblStm             = ").append(this.isFWhichTblStm()).append('\n');
        buffer.append("         .fReadOnlyRecommended     = ").append(this.isFReadOnlyRecommended()).append('\n');
        buffer.append("         .fWriteReservation        = ").append(this.isFWriteReservation()).append('\n');
        buffer.append("         .fExtChar                 = ").append(this.isFExtChar()).append('\n');
        buffer.append("         .fLoadOverride            = ").append(this.isFLoadOverride()).append('\n');
        buffer.append("         .fFarEast                 = ").append(this.isFFarEast()).append('\n');
        buffer.append("         .fCrypto                  = ").append(this.isFCrypto()).append('\n');
        buffer.append("    .nFibBack             = ");
        buffer.append(HexDump.intToHex(this.getNFibBack()));
        buffer.append(" (").append(this.getNFibBack()).append(" )\n");
        buffer.append("    .lKey                 = ");
        buffer.append(HexDump.intToHex(this.getLKey()));
        buffer.append(" (").append(this.getLKey()).append(" )\n");
        buffer.append("    .envr                 = ");
        buffer.append(HexDump.intToHex(this.getEnvr()));
        buffer.append(" (").append(this.getEnvr()).append(" )\n");
        buffer.append("    .history              = ");
        buffer.append(HexDump.shortToHex(this.getHistory()));
        buffer.append(" (").append(this.getHistory()).append(" )\n");
        buffer.append("         .fMac                     = ").append(this.isFMac()).append('\n');
        buffer.append("         .fEmptySpecial            = ").append(this.isFEmptySpecial()).append('\n');
        buffer.append("         .fLoadOverridePage        = ").append(this.isFLoadOverridePage()).append('\n');
        buffer.append("         .fFutureSavedUndo         = ").append(this.isFFutureSavedUndo()).append('\n');
        buffer.append("         .fWord97Saved             = ").append(this.isFWord97Saved()).append('\n');
        buffer.append("         .fSpare0                  = ").append(this.getFSpare0()).append('\n');
        buffer.append("    .chs                  = ");
        buffer.append(HexDump.intToHex(this.getChs()));
        buffer.append(" (").append(this.getChs()).append(" )\n");
        buffer.append("    .chsTables            = ");
        buffer.append(HexDump.intToHex(this.getChsTables()));
        buffer.append(" (").append(this.getChsTables()).append(" )\n");
        buffer.append("    .fcMin                = ");
        buffer.append(HexDump.intToHex(this.getFcMin()));
        buffer.append(" (").append(this.getFcMin()).append(" )\n");
        buffer.append("    .fcMac                = ");
        buffer.append(HexDump.intToHex(this.getFcMac()));
        buffer.append(" (").append(this.getFcMac()).append(" )\n");
        buffer.append("    .csw                  = ");
        buffer.append(HexDump.intToHex(this.getCsw()));
        buffer.append(" (").append(this.getCsw()).append(" )\n");
        buffer.append("    .wMagicCreated        = ");
        buffer.append(HexDump.intToHex(this.getWMagicCreated()));
        buffer.append(" (").append(this.getWMagicCreated()).append(" )\n");
        buffer.append("    .wMagicRevised        = ");
        buffer.append(HexDump.intToHex(this.getWMagicRevised()));
        buffer.append(" (").append(this.getWMagicRevised()).append(" )\n");
        buffer.append("    .wMagicCreatedPrivate = ");
        buffer.append(HexDump.intToHex(this.getWMagicCreatedPrivate()));
        buffer.append(" (").append(this.getWMagicCreatedPrivate()).append(" )\n");
        buffer.append("    .wMagicRevisedPrivate = ");
        buffer.append(HexDump.intToHex(this.getWMagicRevisedPrivate()));
        buffer.append(" (").append(this.getWMagicRevisedPrivate()).append(" )\n");
        buffer.append("    .pnFbpChpFirst_W6     = ");
        buffer.append(HexDump.intToHex(this.getPnFbpChpFirst_W6()));
        buffer.append(" (").append(this.getPnFbpChpFirst_W6()).append(" )\n");
        buffer.append("    .pnChpFirst_W6        = ");
        buffer.append(HexDump.intToHex(this.getPnChpFirst_W6()));
        buffer.append(" (").append(this.getPnChpFirst_W6()).append(" )\n");
        buffer.append("    .cpnBteChp_W6         = ");
        buffer.append(HexDump.intToHex(this.getCpnBteChp_W6()));
        buffer.append(" (").append(this.getCpnBteChp_W6()).append(" )\n");
        buffer.append("    .pnFbpPapFirst_W6     = ");
        buffer.append(HexDump.intToHex(this.getPnFbpPapFirst_W6()));
        buffer.append(" (").append(this.getPnFbpPapFirst_W6()).append(" )\n");
        buffer.append("    .pnPapFirst_W6        = ");
        buffer.append(HexDump.intToHex(this.getPnPapFirst_W6()));
        buffer.append(" (").append(this.getPnPapFirst_W6()).append(" )\n");
        buffer.append("    .cpnBtePap_W6         = ");
        buffer.append(HexDump.intToHex(this.getCpnBtePap_W6()));
        buffer.append(" (").append(this.getCpnBtePap_W6()).append(" )\n");
        buffer.append("    .pnFbpLvcFirst_W6     = ");
        buffer.append(HexDump.intToHex(this.getPnFbpLvcFirst_W6()));
        buffer.append(" (").append(this.getPnFbpLvcFirst_W6()).append(" )\n");
        buffer.append("    .pnLvcFirst_W6        = ");
        buffer.append(HexDump.intToHex(this.getPnLvcFirst_W6()));
        buffer.append(" (").append(this.getPnLvcFirst_W6()).append(" )\n");
        buffer.append("    .cpnBteLvc_W6         = ");
        buffer.append(HexDump.intToHex(this.getCpnBteLvc_W6()));
        buffer.append(" (").append(this.getCpnBteLvc_W6()).append(" )\n");
        buffer.append("    .lidFE                = ");
        buffer.append(HexDump.intToHex(this.getLidFE()));
        buffer.append(" (").append(this.getLidFE()).append(" )\n");
        buffer.append("    .clw                  = ");
        buffer.append(HexDump.intToHex(this.getClw()));
        buffer.append(" (").append(this.getClw()).append(" )\n");
        buffer.append("    .cbMac                = ");
        buffer.append(HexDump.intToHex(this.getCbMac()));
        buffer.append(" (").append(this.getCbMac()).append(" )\n");
        buffer.append("    .lProductCreated      = ");
        buffer.append(HexDump.intToHex(this.getLProductCreated()));
        buffer.append(" (").append(this.getLProductCreated()).append(" )\n");
        buffer.append("    .lProductRevised      = ");
        buffer.append(HexDump.intToHex(this.getLProductRevised()));
        buffer.append(" (").append(this.getLProductRevised()).append(" )\n");
        buffer.append("    .ccpText              = ");
        buffer.append(HexDump.intToHex(this.getCcpText()));
        buffer.append(" (").append(this.getCcpText()).append(" )\n");
        buffer.append("    .ccpFtn               = ");
        buffer.append(HexDump.intToHex(this.getCcpFtn()));
        buffer.append(" (").append(this.getCcpFtn()).append(" )\n");
        buffer.append("    .ccpHdd               = ");
        buffer.append(HexDump.intToHex(this.getCcpHdd()));
        buffer.append(" (").append(this.getCcpHdd()).append(" )\n");
        buffer.append("    .ccpMcr               = ");
        buffer.append(HexDump.intToHex(this.getCcpMcr()));
        buffer.append(" (").append(this.getCcpMcr()).append(" )\n");
        buffer.append("    .ccpAtn               = ");
        buffer.append(HexDump.intToHex(this.getCcpAtn()));
        buffer.append(" (").append(this.getCcpAtn()).append(" )\n");
        buffer.append("    .ccpEdn               = ");
        buffer.append(HexDump.intToHex(this.getCcpEdn()));
        buffer.append(" (").append(this.getCcpEdn()).append(" )\n");
        buffer.append("    .ccpTxbx              = ");
        buffer.append(HexDump.intToHex(this.getCcpTxbx()));
        buffer.append(" (").append(this.getCcpTxbx()).append(" )\n");
        buffer.append("    .ccpHdrTxbx           = ");
        buffer.append(HexDump.intToHex(this.getCcpHdrTxbx()));
        buffer.append(" (").append(this.getCcpHdrTxbx()).append(" )\n");
        buffer.append("    .pnFbpChpFirst        = ");
        buffer.append(HexDump.intToHex(this.getPnFbpChpFirst()));
        buffer.append(" (").append(this.getPnFbpChpFirst()).append(" )\n");
        buffer.append("    .pnChpFirst           = ");
        buffer.append(HexDump.intToHex(this.getPnChpFirst()));
        buffer.append(" (").append(this.getPnChpFirst()).append(" )\n");
        buffer.append("    .cpnBteChp            = ");
        buffer.append(HexDump.intToHex(this.getCpnBteChp()));
        buffer.append(" (").append(this.getCpnBteChp()).append(" )\n");
        buffer.append("    .pnFbpPapFirst        = ");
        buffer.append(HexDump.intToHex(this.getPnFbpPapFirst()));
        buffer.append(" (").append(this.getPnFbpPapFirst()).append(" )\n");
        buffer.append("    .pnPapFirst           = ");
        buffer.append(HexDump.intToHex(this.getPnPapFirst()));
        buffer.append(" (").append(this.getPnPapFirst()).append(" )\n");
        buffer.append("    .cpnBtePap            = ");
        buffer.append(HexDump.intToHex(this.getCpnBtePap()));
        buffer.append(" (").append(this.getCpnBtePap()).append(" )\n");
        buffer.append("    .pnFbpLvcFirst        = ");
        buffer.append(HexDump.intToHex(this.getPnFbpLvcFirst()));
        buffer.append(" (").append(this.getPnFbpLvcFirst()).append(" )\n");
        buffer.append("    .pnLvcFirst           = ");
        buffer.append(HexDump.intToHex(this.getPnLvcFirst()));
        buffer.append(" (").append(this.getPnLvcFirst()).append(" )\n");
        buffer.append("    .cpnBteLvc            = ");
        buffer.append(HexDump.intToHex(this.getCpnBteLvc()));
        buffer.append(" (").append(this.getCpnBteLvc()).append(" )\n");
        buffer.append("    .fcIslandFirst        = ");
        buffer.append(HexDump.intToHex(this.getFcIslandFirst()));
        buffer.append(" (").append(this.getFcIslandFirst()).append(" )\n");
        buffer.append("    .fcIslandLim          = ");
        buffer.append(HexDump.intToHex(this.getFcIslandLim()));
        buffer.append(" (").append(this.getFcIslandLim()).append(" )\n");
        buffer.append("    .cfclcb               = ");
        buffer.append(HexDump.intToHex(this.getCfclcb()));
        buffer.append(" (").append(this.getCfclcb()).append(" )\n");
        buffer.append("    .fcStshfOrig          = ");
        buffer.append(HexDump.intToHex(this.getFcStshfOrig()));
        buffer.append(" (").append(this.getFcStshfOrig()).append(" )\n");
        buffer.append("    .lcbStshfOrig         = ");
        buffer.append(HexDump.intToHex(this.getLcbStshfOrig()));
        buffer.append(" (").append(this.getLcbStshfOrig()).append(" )\n");
        buffer.append("    .fcStshf              = ");
        buffer.append(HexDump.intToHex(this.getFcStshf()));
        buffer.append(" (").append(this.getFcStshf()).append(" )\n");
        buffer.append("    .lcbStshf             = ");
        buffer.append(HexDump.intToHex(this.getLcbStshf()));
        buffer.append(" (").append(this.getLcbStshf()).append(" )\n");
        buffer.append("    .fcPlcffndRef         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffndRef()));
        buffer.append(" (").append(this.getFcPlcffndRef()).append(" )\n");
        buffer.append("    .lcbPlcffndRef        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffndRef()));
        buffer.append(" (").append(this.getLcbPlcffndRef()).append(" )\n");
        buffer.append("    .fcPlcffndTxt         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffndTxt()));
        buffer.append(" (").append(this.getFcPlcffndTxt()).append(" )\n");
        buffer.append("    .lcbPlcffndTxt        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffndTxt()));
        buffer.append(" (").append(this.getLcbPlcffndTxt()).append(" )\n");
        buffer.append("    .fcPlcfandRef         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfandRef()));
        buffer.append(" (").append(this.getFcPlcfandRef()).append(" )\n");
        buffer.append("    .lcbPlcfandRef        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfandRef()));
        buffer.append(" (").append(this.getLcbPlcfandRef()).append(" )\n");
        buffer.append("    .fcPlcfandTxt         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfandTxt()));
        buffer.append(" (").append(this.getFcPlcfandTxt()).append(" )\n");
        buffer.append("    .lcbPlcfandTxt        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfandTxt()));
        buffer.append(" (").append(this.getLcbPlcfandTxt()).append(" )\n");
        buffer.append("    .fcPlcfsed            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfsed()));
        buffer.append(" (").append(this.getFcPlcfsed()).append(" )\n");
        buffer.append("    .lcbPlcfsed           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfsed()));
        buffer.append(" (").append(this.getLcbPlcfsed()).append(" )\n");
        buffer.append("    .fcPlcpad             = ");
        buffer.append(HexDump.intToHex(this.getFcPlcpad()));
        buffer.append(" (").append(this.getFcPlcpad()).append(" )\n");
        buffer.append("    .lcbPlcpad            = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcpad()));
        buffer.append(" (").append(this.getLcbPlcpad()).append(" )\n");
        buffer.append("    .fcPlcfphe            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfphe()));
        buffer.append(" (").append(this.getFcPlcfphe()).append(" )\n");
        buffer.append("    .lcbPlcfphe           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfphe()));
        buffer.append(" (").append(this.getLcbPlcfphe()).append(" )\n");
        buffer.append("    .fcSttbfglsy          = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfglsy()));
        buffer.append(" (").append(this.getFcSttbfglsy()).append(" )\n");
        buffer.append("    .lcbSttbfglsy         = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfglsy()));
        buffer.append(" (").append(this.getLcbSttbfglsy()).append(" )\n");
        buffer.append("    .fcPlcfglsy           = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfglsy()));
        buffer.append(" (").append(this.getFcPlcfglsy()).append(" )\n");
        buffer.append("    .lcbPlcfglsy          = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfglsy()));
        buffer.append(" (").append(this.getLcbPlcfglsy()).append(" )\n");
        buffer.append("    .fcPlcfhdd            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfhdd()));
        buffer.append(" (").append(this.getFcPlcfhdd()).append(" )\n");
        buffer.append("    .lcbPlcfhdd           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfhdd()));
        buffer.append(" (").append(this.getLcbPlcfhdd()).append(" )\n");
        buffer.append("    .fcPlcfbteChpx        = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfbteChpx()));
        buffer.append(" (").append(this.getFcPlcfbteChpx()).append(" )\n");
        buffer.append("    .lcbPlcfbteChpx       = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfbteChpx()));
        buffer.append(" (").append(this.getLcbPlcfbteChpx()).append(" )\n");
        buffer.append("    .fcPlcfbtePapx        = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfbtePapx()));
        buffer.append(" (").append(this.getFcPlcfbtePapx()).append(" )\n");
        buffer.append("    .lcbPlcfbtePapx       = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfbtePapx()));
        buffer.append(" (").append(this.getLcbPlcfbtePapx()).append(" )\n");
        buffer.append("    .fcPlcfsea            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfsea()));
        buffer.append(" (").append(this.getFcPlcfsea()).append(" )\n");
        buffer.append("    .lcbPlcfsea           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfsea()));
        buffer.append(" (").append(this.getLcbPlcfsea()).append(" )\n");
        buffer.append("    .fcSttbfffn           = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfffn()));
        buffer.append(" (").append(this.getFcSttbfffn()).append(" )\n");
        buffer.append("    .lcbSttbfffn          = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfffn()));
        buffer.append(" (").append(this.getLcbSttbfffn()).append(" )\n");
        buffer.append("    .fcPlcffldMom         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldMom()));
        buffer.append(" (").append(this.getFcPlcffldMom()).append(" )\n");
        buffer.append("    .lcbPlcffldMom        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldMom()));
        buffer.append(" (").append(this.getLcbPlcffldMom()).append(" )\n");
        buffer.append("    .fcPlcffldHdr         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldHdr()));
        buffer.append(" (").append(this.getFcPlcffldHdr()).append(" )\n");
        buffer.append("    .lcbPlcffldHdr        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldHdr()));
        buffer.append(" (").append(this.getLcbPlcffldHdr()).append(" )\n");
        buffer.append("    .fcPlcffldFtn         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldFtn()));
        buffer.append(" (").append(this.getFcPlcffldFtn()).append(" )\n");
        buffer.append("    .lcbPlcffldFtn        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldFtn()));
        buffer.append(" (").append(this.getLcbPlcffldFtn()).append(" )\n");
        buffer.append("    .fcPlcffldAtn         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldAtn()));
        buffer.append(" (").append(this.getFcPlcffldAtn()).append(" )\n");
        buffer.append("    .lcbPlcffldAtn        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldAtn()));
        buffer.append(" (").append(this.getLcbPlcffldAtn()).append(" )\n");
        buffer.append("    .fcPlcffldMcr         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldMcr()));
        buffer.append(" (").append(this.getFcPlcffldMcr()).append(" )\n");
        buffer.append("    .lcbPlcffldMcr        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldMcr()));
        buffer.append(" (").append(this.getLcbPlcffldMcr()).append(" )\n");
        buffer.append("    .fcSttbfbkmk          = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfbkmk()));
        buffer.append(" (").append(this.getFcSttbfbkmk()).append(" )\n");
        buffer.append("    .lcbSttbfbkmk         = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfbkmk()));
        buffer.append(" (").append(this.getLcbSttbfbkmk()).append(" )\n");
        buffer.append("    .fcPlcfbkf            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfbkf()));
        buffer.append(" (").append(this.getFcPlcfbkf()).append(" )\n");
        buffer.append("    .lcbPlcfbkf           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfbkf()));
        buffer.append(" (").append(this.getLcbPlcfbkf()).append(" )\n");
        buffer.append("    .fcPlcfbkl            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfbkl()));
        buffer.append(" (").append(this.getFcPlcfbkl()).append(" )\n");
        buffer.append("    .lcbPlcfbkl           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfbkl()));
        buffer.append(" (").append(this.getLcbPlcfbkl()).append(" )\n");
        buffer.append("    .fcCmds               = ");
        buffer.append(HexDump.intToHex(this.getFcCmds()));
        buffer.append(" (").append(this.getFcCmds()).append(" )\n");
        buffer.append("    .lcbCmds              = ");
        buffer.append(HexDump.intToHex(this.getLcbCmds()));
        buffer.append(" (").append(this.getLcbCmds()).append(" )\n");
        buffer.append("    .fcPlcmcr             = ");
        buffer.append(HexDump.intToHex(this.getFcPlcmcr()));
        buffer.append(" (").append(this.getFcPlcmcr()).append(" )\n");
        buffer.append("    .lcbPlcmcr            = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcmcr()));
        buffer.append(" (").append(this.getLcbPlcmcr()).append(" )\n");
        buffer.append("    .fcSttbfmcr           = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfmcr()));
        buffer.append(" (").append(this.getFcSttbfmcr()).append(" )\n");
        buffer.append("    .lcbSttbfmcr          = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfmcr()));
        buffer.append(" (").append(this.getLcbSttbfmcr()).append(" )\n");
        buffer.append("    .fcPrDrvr             = ");
        buffer.append(HexDump.intToHex(this.getFcPrDrvr()));
        buffer.append(" (").append(this.getFcPrDrvr()).append(" )\n");
        buffer.append("    .lcbPrDrvr            = ");
        buffer.append(HexDump.intToHex(this.getLcbPrDrvr()));
        buffer.append(" (").append(this.getLcbPrDrvr()).append(" )\n");
        buffer.append("    .fcPrEnvPort          = ");
        buffer.append(HexDump.intToHex(this.getFcPrEnvPort()));
        buffer.append(" (").append(this.getFcPrEnvPort()).append(" )\n");
        buffer.append("    .lcbPrEnvPort         = ");
        buffer.append(HexDump.intToHex(this.getLcbPrEnvPort()));
        buffer.append(" (").append(this.getLcbPrEnvPort()).append(" )\n");
        buffer.append("    .fcPrEnvLand          = ");
        buffer.append(HexDump.intToHex(this.getFcPrEnvLand()));
        buffer.append(" (").append(this.getFcPrEnvLand()).append(" )\n");
        buffer.append("    .lcbPrEnvLand         = ");
        buffer.append(HexDump.intToHex(this.getLcbPrEnvLand()));
        buffer.append(" (").append(this.getLcbPrEnvLand()).append(" )\n");
        buffer.append("    .fcWss                = ");
        buffer.append(HexDump.intToHex(this.getFcWss()));
        buffer.append(" (").append(this.getFcWss()).append(" )\n");
        buffer.append("    .lcbWss               = ");
        buffer.append(HexDump.intToHex(this.getLcbWss()));
        buffer.append(" (").append(this.getLcbWss()).append(" )\n");
        buffer.append("    .fcDop                = ");
        buffer.append(HexDump.intToHex(this.getFcDop()));
        buffer.append(" (").append(this.getFcDop()).append(" )\n");
        buffer.append("    .lcbDop               = ");
        buffer.append(HexDump.intToHex(this.getLcbDop()));
        buffer.append(" (").append(this.getLcbDop()).append(" )\n");
        buffer.append("    .fcSttbfAssoc         = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfAssoc()));
        buffer.append(" (").append(this.getFcSttbfAssoc()).append(" )\n");
        buffer.append("    .lcbSttbfAssoc        = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfAssoc()));
        buffer.append(" (").append(this.getLcbSttbfAssoc()).append(" )\n");
        buffer.append("    .fcClx                = ");
        buffer.append(HexDump.intToHex(this.getFcClx()));
        buffer.append(" (").append(this.getFcClx()).append(" )\n");
        buffer.append("    .lcbClx               = ");
        buffer.append(HexDump.intToHex(this.getLcbClx()));
        buffer.append(" (").append(this.getLcbClx()).append(" )\n");
        buffer.append("    .fcPlcfpgdFtn         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfpgdFtn()));
        buffer.append(" (").append(this.getFcPlcfpgdFtn()).append(" )\n");
        buffer.append("    .lcbPlcfpgdFtn        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfpgdFtn()));
        buffer.append(" (").append(this.getLcbPlcfpgdFtn()).append(" )\n");
        buffer.append("    .fcAutosaveSource     = ");
        buffer.append(HexDump.intToHex(this.getFcAutosaveSource()));
        buffer.append(" (").append(this.getFcAutosaveSource()).append(" )\n");
        buffer.append("    .lcbAutosaveSource    = ");
        buffer.append(HexDump.intToHex(this.getLcbAutosaveSource()));
        buffer.append(" (").append(this.getLcbAutosaveSource()).append(" )\n");
        buffer.append("    .fcGrpXstAtnOwners    = ");
        buffer.append(HexDump.intToHex(this.getFcGrpXstAtnOwners()));
        buffer.append(" (").append(this.getFcGrpXstAtnOwners()).append(" )\n");
        buffer.append("    .lcbGrpXstAtnOwners   = ");
        buffer.append(HexDump.intToHex(this.getLcbGrpXstAtnOwners()));
        buffer.append(" (").append(this.getLcbGrpXstAtnOwners()).append(" )\n");
        buffer.append("    .fcSttbfAtnbkmk       = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfAtnbkmk()));
        buffer.append(" (").append(this.getFcSttbfAtnbkmk()).append(" )\n");
        buffer.append("    .lcbSttbfAtnbkmk      = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfAtnbkmk()));
        buffer.append(" (").append(this.getLcbSttbfAtnbkmk()).append(" )\n");
        buffer.append("    .fcPlcdoaMom          = ");
        buffer.append(HexDump.intToHex(this.getFcPlcdoaMom()));
        buffer.append(" (").append(this.getFcPlcdoaMom()).append(" )\n");
        buffer.append("    .lcbPlcdoaMom         = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcdoaMom()));
        buffer.append(" (").append(this.getLcbPlcdoaMom()).append(" )\n");
        buffer.append("    .fcPlcdoaHdr          = ");
        buffer.append(HexDump.intToHex(this.getFcPlcdoaHdr()));
        buffer.append(" (").append(this.getFcPlcdoaHdr()).append(" )\n");
        buffer.append("    .lcbPlcdoaHdr         = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcdoaHdr()));
        buffer.append(" (").append(this.getLcbPlcdoaHdr()).append(" )\n");
        buffer.append("    .fcPlcspaMom          = ");
        buffer.append(HexDump.intToHex(this.getFcPlcspaMom()));
        buffer.append(" (").append(this.getFcPlcspaMom()).append(" )\n");
        buffer.append("    .lcbPlcspaMom         = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcspaMom()));
        buffer.append(" (").append(this.getLcbPlcspaMom()).append(" )\n");
        buffer.append("    .fcPlcspaHdr          = ");
        buffer.append(HexDump.intToHex(this.getFcPlcspaHdr()));
        buffer.append(" (").append(this.getFcPlcspaHdr()).append(" )\n");
        buffer.append("    .lcbPlcspaHdr         = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcspaHdr()));
        buffer.append(" (").append(this.getLcbPlcspaHdr()).append(" )\n");
        buffer.append("    .fcPlcfAtnbkf         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfAtnbkf()));
        buffer.append(" (").append(this.getFcPlcfAtnbkf()).append(" )\n");
        buffer.append("    .lcbPlcfAtnbkf        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfAtnbkf()));
        buffer.append(" (").append(this.getLcbPlcfAtnbkf()).append(" )\n");
        buffer.append("    .fcPlcfAtnbkl         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfAtnbkl()));
        buffer.append(" (").append(this.getFcPlcfAtnbkl()).append(" )\n");
        buffer.append("    .lcbPlcfAtnbkl        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfAtnbkl()));
        buffer.append(" (").append(this.getLcbPlcfAtnbkl()).append(" )\n");
        buffer.append("    .fcPms                = ");
        buffer.append(HexDump.intToHex(this.getFcPms()));
        buffer.append(" (").append(this.getFcPms()).append(" )\n");
        buffer.append("    .lcbPms               = ");
        buffer.append(HexDump.intToHex(this.getLcbPms()));
        buffer.append(" (").append(this.getLcbPms()).append(" )\n");
        buffer.append("    .fcFormFldSttbs       = ");
        buffer.append(HexDump.intToHex(this.getFcFormFldSttbs()));
        buffer.append(" (").append(this.getFcFormFldSttbs()).append(" )\n");
        buffer.append("    .lcbFormFldSttbs      = ");
        buffer.append(HexDump.intToHex(this.getLcbFormFldSttbs()));
        buffer.append(" (").append(this.getLcbFormFldSttbs()).append(" )\n");
        buffer.append("    .fcPlcfendRef         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfendRef()));
        buffer.append(" (").append(this.getFcPlcfendRef()).append(" )\n");
        buffer.append("    .lcbPlcfendRef        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfendRef()));
        buffer.append(" (").append(this.getLcbPlcfendRef()).append(" )\n");
        buffer.append("    .fcPlcfendTxt         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfendTxt()));
        buffer.append(" (").append(this.getFcPlcfendTxt()).append(" )\n");
        buffer.append("    .lcbPlcfendTxt        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfendTxt()));
        buffer.append(" (").append(this.getLcbPlcfendTxt()).append(" )\n");
        buffer.append("    .fcPlcffldEdn         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldEdn()));
        buffer.append(" (").append(this.getFcPlcffldEdn()).append(" )\n");
        buffer.append("    .lcbPlcffldEdn        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldEdn()));
        buffer.append(" (").append(this.getLcbPlcffldEdn()).append(" )\n");
        buffer.append("    .fcPlcfpgdEdn         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfpgdEdn()));
        buffer.append(" (").append(this.getFcPlcfpgdEdn()).append(" )\n");
        buffer.append("    .lcbPlcfpgdEdn        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfpgdEdn()));
        buffer.append(" (").append(this.getLcbPlcfpgdEdn()).append(" )\n");
        buffer.append("    .fcDggInfo            = ");
        buffer.append(HexDump.intToHex(this.getFcDggInfo()));
        buffer.append(" (").append(this.getFcDggInfo()).append(" )\n");
        buffer.append("    .lcbDggInfo           = ");
        buffer.append(HexDump.intToHex(this.getLcbDggInfo()));
        buffer.append(" (").append(this.getLcbDggInfo()).append(" )\n");
        buffer.append("    .fcSttbfRMark         = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfRMark()));
        buffer.append(" (").append(this.getFcSttbfRMark()).append(" )\n");
        buffer.append("    .lcbSttbfRMark        = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfRMark()));
        buffer.append(" (").append(this.getLcbSttbfRMark()).append(" )\n");
        buffer.append("    .fcSttbCaption        = ");
        buffer.append(HexDump.intToHex(this.getFcSttbCaption()));
        buffer.append(" (").append(this.getFcSttbCaption()).append(" )\n");
        buffer.append("    .lcbSttbCaption       = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbCaption()));
        buffer.append(" (").append(this.getLcbSttbCaption()).append(" )\n");
        buffer.append("    .fcSttbAutoCaption    = ");
        buffer.append(HexDump.intToHex(this.getFcSttbAutoCaption()));
        buffer.append(" (").append(this.getFcSttbAutoCaption()).append(" )\n");
        buffer.append("    .lcbSttbAutoCaption   = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbAutoCaption()));
        buffer.append(" (").append(this.getLcbSttbAutoCaption()).append(" )\n");
        buffer.append("    .fcPlcfwkb            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfwkb()));
        buffer.append(" (").append(this.getFcPlcfwkb()).append(" )\n");
        buffer.append("    .lcbPlcfwkb           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfwkb()));
        buffer.append(" (").append(this.getLcbPlcfwkb()).append(" )\n");
        buffer.append("    .fcPlcfspl            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfspl()));
        buffer.append(" (").append(this.getFcPlcfspl()).append(" )\n");
        buffer.append("    .lcbPlcfspl           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfspl()));
        buffer.append(" (").append(this.getLcbPlcfspl()).append(" )\n");
        buffer.append("    .fcPlcftxbxTxt        = ");
        buffer.append(HexDump.intToHex(this.getFcPlcftxbxTxt()));
        buffer.append(" (").append(this.getFcPlcftxbxTxt()).append(" )\n");
        buffer.append("    .lcbPlcftxbxTxt       = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcftxbxTxt()));
        buffer.append(" (").append(this.getLcbPlcftxbxTxt()).append(" )\n");
        buffer.append("    .fcPlcffldTxbx        = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldTxbx()));
        buffer.append(" (").append(this.getFcPlcffldTxbx()).append(" )\n");
        buffer.append("    .lcbPlcffldTxbx       = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldTxbx()));
        buffer.append(" (").append(this.getLcbPlcffldTxbx()).append(" )\n");
        buffer.append("    .fcPlcfhdrtxbxTxt     = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfhdrtxbxTxt()));
        buffer.append(" (").append(this.getFcPlcfhdrtxbxTxt()).append(" )\n");
        buffer.append("    .lcbPlcfhdrtxbxTxt    = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfhdrtxbxTxt()));
        buffer.append(" (").append(this.getLcbPlcfhdrtxbxTxt()).append(" )\n");
        buffer.append("    .fcPlcffldHdrTxbx     = ");
        buffer.append(HexDump.intToHex(this.getFcPlcffldHdrTxbx()));
        buffer.append(" (").append(this.getFcPlcffldHdrTxbx()).append(" )\n");
        buffer.append("    .lcbPlcffldHdrTxbx    = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcffldHdrTxbx()));
        buffer.append(" (").append(this.getLcbPlcffldHdrTxbx()).append(" )\n");
        buffer.append("    .fcStwUser            = ");
        buffer.append(HexDump.intToHex(this.getFcStwUser()));
        buffer.append(" (").append(this.getFcStwUser()).append(" )\n");
        buffer.append("    .lcbStwUser           = ");
        buffer.append(HexDump.intToHex(this.getLcbStwUser()));
        buffer.append(" (").append(this.getLcbStwUser()).append(" )\n");
        buffer.append("    .fcSttbttmbd          = ");
        buffer.append(HexDump.intToHex(this.getFcSttbttmbd()));
        buffer.append(" (").append(this.getFcSttbttmbd()).append(" )\n");
        buffer.append("    .cbSttbttmbd          = ");
        buffer.append(HexDump.intToHex(this.getCbSttbttmbd()));
        buffer.append(" (").append(this.getCbSttbttmbd()).append(" )\n");
        buffer.append("    .fcUnused             = ");
        buffer.append(HexDump.intToHex(this.getFcUnused()));
        buffer.append(" (").append(this.getFcUnused()).append(" )\n");
        buffer.append("    .lcbUnused            = ");
        buffer.append(HexDump.intToHex(this.getLcbUnused()));
        buffer.append(" (").append(this.getLcbUnused()).append(" )\n");
        buffer.append("    .fcPgdMother          = ");
        buffer.append(HexDump.intToHex(this.getFcPgdMother()));
        buffer.append(" (").append(this.getFcPgdMother()).append(" )\n");
        buffer.append("    .lcbPgdMother         = ");
        buffer.append(HexDump.intToHex(this.getLcbPgdMother()));
        buffer.append(" (").append(this.getLcbPgdMother()).append(" )\n");
        buffer.append("    .fcBkdMother          = ");
        buffer.append(HexDump.intToHex(this.getFcBkdMother()));
        buffer.append(" (").append(this.getFcBkdMother()).append(" )\n");
        buffer.append("    .lcbBkdMother         = ");
        buffer.append(HexDump.intToHex(this.getLcbBkdMother()));
        buffer.append(" (").append(this.getLcbBkdMother()).append(" )\n");
        buffer.append("    .fcPgdFtn             = ");
        buffer.append(HexDump.intToHex(this.getFcPgdFtn()));
        buffer.append(" (").append(this.getFcPgdFtn()).append(" )\n");
        buffer.append("    .lcbPgdFtn            = ");
        buffer.append(HexDump.intToHex(this.getLcbPgdFtn()));
        buffer.append(" (").append(this.getLcbPgdFtn()).append(" )\n");
        buffer.append("    .fcBkdFtn             = ");
        buffer.append(HexDump.intToHex(this.getFcBkdFtn()));
        buffer.append(" (").append(this.getFcBkdFtn()).append(" )\n");
        buffer.append("    .lcbBkdFtn            = ");
        buffer.append(HexDump.intToHex(this.getLcbBkdFtn()));
        buffer.append(" (").append(this.getLcbBkdFtn()).append(" )\n");
        buffer.append("    .fcPgdEdn             = ");
        buffer.append(HexDump.intToHex(this.getFcPgdEdn()));
        buffer.append(" (").append(this.getFcPgdEdn()).append(" )\n");
        buffer.append("    .lcbPgdEdn            = ");
        buffer.append(HexDump.intToHex(this.getLcbPgdEdn()));
        buffer.append(" (").append(this.getLcbPgdEdn()).append(" )\n");
        buffer.append("    .fcBkdEdn             = ");
        buffer.append(HexDump.intToHex(this.getFcBkdEdn()));
        buffer.append(" (").append(this.getFcBkdEdn()).append(" )\n");
        buffer.append("    .lcbBkdEdn            = ");
        buffer.append(HexDump.intToHex(this.getLcbBkdEdn()));
        buffer.append(" (").append(this.getLcbBkdEdn()).append(" )\n");
        buffer.append("    .fcSttbfIntlFld       = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfIntlFld()));
        buffer.append(" (").append(this.getFcSttbfIntlFld()).append(" )\n");
        buffer.append("    .lcbSttbfIntlFld      = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfIntlFld()));
        buffer.append(" (").append(this.getLcbSttbfIntlFld()).append(" )\n");
        buffer.append("    .fcRouteSlip          = ");
        buffer.append(HexDump.intToHex(this.getFcRouteSlip()));
        buffer.append(" (").append(this.getFcRouteSlip()).append(" )\n");
        buffer.append("    .lcbRouteSlip         = ");
        buffer.append(HexDump.intToHex(this.getLcbRouteSlip()));
        buffer.append(" (").append(this.getLcbRouteSlip()).append(" )\n");
        buffer.append("    .fcSttbSavedBy        = ");
        buffer.append(HexDump.intToHex(this.getFcSttbSavedBy()));
        buffer.append(" (").append(this.getFcSttbSavedBy()).append(" )\n");
        buffer.append("    .lcbSttbSavedBy       = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbSavedBy()));
        buffer.append(" (").append(this.getLcbSttbSavedBy()).append(" )\n");
        buffer.append("    .fcSttbFnm            = ");
        buffer.append(HexDump.intToHex(this.getFcSttbFnm()));
        buffer.append(" (").append(this.getFcSttbFnm()).append(" )\n");
        buffer.append("    .lcbSttbFnm           = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbFnm()));
        buffer.append(" (").append(this.getLcbSttbFnm()).append(" )\n");
        buffer.append("    .fcPlcfLst            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfLst()));
        buffer.append(" (").append(this.getFcPlcfLst()).append(" )\n");
        buffer.append("    .lcbPlcfLst           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfLst()));
        buffer.append(" (").append(this.getLcbPlcfLst()).append(" )\n");
        buffer.append("    .fcPlfLfo             = ");
        buffer.append(HexDump.intToHex(this.getFcPlfLfo()));
        buffer.append(" (").append(this.getFcPlfLfo()).append(" )\n");
        buffer.append("    .lcbPlfLfo            = ");
        buffer.append(HexDump.intToHex(this.getLcbPlfLfo()));
        buffer.append(" (").append(this.getLcbPlfLfo()).append(" )\n");
        buffer.append("    .fcPlcftxbxBkd        = ");
        buffer.append(HexDump.intToHex(this.getFcPlcftxbxBkd()));
        buffer.append(" (").append(this.getFcPlcftxbxBkd()).append(" )\n");
        buffer.append("    .lcbPlcftxbxBkd       = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcftxbxBkd()));
        buffer.append(" (").append(this.getLcbPlcftxbxBkd()).append(" )\n");
        buffer.append("    .fcPlcftxbxHdrBkd     = ");
        buffer.append(HexDump.intToHex(this.getFcPlcftxbxHdrBkd()));
        buffer.append(" (").append(this.getFcPlcftxbxHdrBkd()).append(" )\n");
        buffer.append("    .lcbPlcftxbxHdrBkd    = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcftxbxHdrBkd()));
        buffer.append(" (").append(this.getLcbPlcftxbxHdrBkd()).append(" )\n");
        buffer.append("    .fcDocUndo            = ");
        buffer.append(HexDump.intToHex(this.getFcDocUndo()));
        buffer.append(" (").append(this.getFcDocUndo()).append(" )\n");
        buffer.append("    .lcbDocUndo           = ");
        buffer.append(HexDump.intToHex(this.getLcbDocUndo()));
        buffer.append(" (").append(this.getLcbDocUndo()).append(" )\n");
        buffer.append("    .fcRgbuse             = ");
        buffer.append(HexDump.intToHex(this.getFcRgbuse()));
        buffer.append(" (").append(this.getFcRgbuse()).append(" )\n");
        buffer.append("    .lcbRgbuse            = ");
        buffer.append(HexDump.intToHex(this.getLcbRgbuse()));
        buffer.append(" (").append(this.getLcbRgbuse()).append(" )\n");
        buffer.append("    .fcUsp                = ");
        buffer.append(HexDump.intToHex(this.getFcUsp()));
        buffer.append(" (").append(this.getFcUsp()).append(" )\n");
        buffer.append("    .lcbUsp               = ");
        buffer.append(HexDump.intToHex(this.getLcbUsp()));
        buffer.append(" (").append(this.getLcbUsp()).append(" )\n");
        buffer.append("    .fcUskf               = ");
        buffer.append(HexDump.intToHex(this.getFcUskf()));
        buffer.append(" (").append(this.getFcUskf()).append(" )\n");
        buffer.append("    .lcbUskf              = ");
        buffer.append(HexDump.intToHex(this.getLcbUskf()));
        buffer.append(" (").append(this.getLcbUskf()).append(" )\n");
        buffer.append("    .fcPlcupcRgbuse       = ");
        buffer.append(HexDump.intToHex(this.getFcPlcupcRgbuse()));
        buffer.append(" (").append(this.getFcPlcupcRgbuse()).append(" )\n");
        buffer.append("    .lcbPlcupcRgbuse      = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcupcRgbuse()));
        buffer.append(" (").append(this.getLcbPlcupcRgbuse()).append(" )\n");
        buffer.append("    .fcPlcupcUsp          = ");
        buffer.append(HexDump.intToHex(this.getFcPlcupcUsp()));
        buffer.append(" (").append(this.getFcPlcupcUsp()).append(" )\n");
        buffer.append("    .lcbPlcupcUsp         = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcupcUsp()));
        buffer.append(" (").append(this.getLcbPlcupcUsp()).append(" )\n");
        buffer.append("    .fcSttbGlsyStyle      = ");
        buffer.append(HexDump.intToHex(this.getFcSttbGlsyStyle()));
        buffer.append(" (").append(this.getFcSttbGlsyStyle()).append(" )\n");
        buffer.append("    .lcbSttbGlsyStyle     = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbGlsyStyle()));
        buffer.append(" (").append(this.getLcbSttbGlsyStyle()).append(" )\n");
        buffer.append("    .fcPlgosl             = ");
        buffer.append(HexDump.intToHex(this.getFcPlgosl()));
        buffer.append(" (").append(this.getFcPlgosl()).append(" )\n");
        buffer.append("    .lcbPlgosl            = ");
        buffer.append(HexDump.intToHex(this.getLcbPlgosl()));
        buffer.append(" (").append(this.getLcbPlgosl()).append(" )\n");
        buffer.append("    .fcPlcocx             = ");
        buffer.append(HexDump.intToHex(this.getFcPlcocx()));
        buffer.append(" (").append(this.getFcPlcocx()).append(" )\n");
        buffer.append("    .lcbPlcocx            = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcocx()));
        buffer.append(" (").append(this.getLcbPlcocx()).append(" )\n");
        buffer.append("    .fcPlcfbteLvc         = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfbteLvc()));
        buffer.append(" (").append(this.getFcPlcfbteLvc()).append(" )\n");
        buffer.append("    .lcbPlcfbteLvc        = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfbteLvc()));
        buffer.append(" (").append(this.getLcbPlcfbteLvc()).append(" )\n");
        buffer.append("    .dwLowDateTime        = ");
        buffer.append(HexDump.intToHex(this.getDwLowDateTime()));
        buffer.append(" (").append(this.getDwLowDateTime()).append(" )\n");
        buffer.append("    .dwHighDateTime       = ");
        buffer.append(HexDump.intToHex(this.getDwHighDateTime()));
        buffer.append(" (").append(this.getDwHighDateTime()).append(" )\n");
        buffer.append("    .fcPlcflvc            = ");
        buffer.append(HexDump.intToHex(this.getFcPlcflvc()));
        buffer.append(" (").append(this.getFcPlcflvc()).append(" )\n");
        buffer.append("    .lcbPlcflvc           = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcflvc()));
        buffer.append(" (").append(this.getLcbPlcflvc()).append(" )\n");
        buffer.append("    .fcPlcasumy           = ");
        buffer.append(HexDump.intToHex(this.getFcPlcasumy()));
        buffer.append(" (").append(this.getFcPlcasumy()).append(" )\n");
        buffer.append("    .lcbPlcasumy          = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcasumy()));
        buffer.append(" (").append(this.getLcbPlcasumy()).append(" )\n");
        buffer.append("    .fcPlcfgram           = ");
        buffer.append(HexDump.intToHex(this.getFcPlcfgram()));
        buffer.append(" (").append(this.getFcPlcfgram()).append(" )\n");
        buffer.append("    .lcbPlcfgram          = ");
        buffer.append(HexDump.intToHex(this.getLcbPlcfgram()));
        buffer.append(" (").append(this.getLcbPlcfgram()).append(" )\n");
        buffer.append("    .fcSttbListNames      = ");
        buffer.append(HexDump.intToHex(this.getFcSttbListNames()));
        buffer.append(" (").append(this.getFcSttbListNames()).append(" )\n");
        buffer.append("    .lcbSttbListNames     = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbListNames()));
        buffer.append(" (").append(this.getLcbSttbListNames()).append(" )\n");
        buffer.append("    .fcSttbfUssr          = ");
        buffer.append(HexDump.intToHex(this.getFcSttbfUssr()));
        buffer.append(" (").append(this.getFcSttbfUssr()).append(" )\n");
        buffer.append("    .lcbSttbfUssr         = ");
        buffer.append(HexDump.intToHex(this.getLcbSttbfUssr()));
        buffer.append(" (").append(this.getLcbSttbfUssr()).append(" )\n");
        buffer.append("[/FIB]\n");
        return buffer.toString();
    }
    
    public int getSize() {
        return 902;
    }
    
    public int getWIdent() {
        return this.field_1_wIdent;
    }
    
    public void setWIdent(final int field_1_wIdent) {
        this.field_1_wIdent = field_1_wIdent;
    }
    
    public int getNFib() {
        return this.field_2_nFib;
    }
    
    public void setNFib(final int field_2_nFib) {
        this.field_2_nFib = field_2_nFib;
    }
    
    public int getNProduct() {
        return this.field_3_nProduct;
    }
    
    public void setNProduct(final int field_3_nProduct) {
        this.field_3_nProduct = field_3_nProduct;
    }
    
    public int getLid() {
        return this.field_4_lid;
    }
    
    public void setLid(final int field_4_lid) {
        this.field_4_lid = field_4_lid;
    }
    
    public int getPnNext() {
        return this.field_5_pnNext;
    }
    
    public void setPnNext(final int field_5_pnNext) {
        this.field_5_pnNext = field_5_pnNext;
    }
    
    public short getOptions() {
        return this.field_6_options;
    }
    
    public void setOptions(final short field_6_options) {
        this.field_6_options = field_6_options;
    }
    
    public int getNFibBack() {
        return this.field_7_nFibBack;
    }
    
    public void setNFibBack(final int field_7_nFibBack) {
        this.field_7_nFibBack = field_7_nFibBack;
    }
    
    public int getLKey() {
        return this.field_8_lKey;
    }
    
    public void setLKey(final int field_8_lKey) {
        this.field_8_lKey = field_8_lKey;
    }
    
    public int getEnvr() {
        return this.field_9_envr;
    }
    
    public void setEnvr(final int field_9_envr) {
        this.field_9_envr = field_9_envr;
    }
    
    public short getHistory() {
        return this.field_10_history;
    }
    
    public void setHistory(final short field_10_history) {
        this.field_10_history = field_10_history;
    }
    
    public int getChs() {
        return this.field_11_chs;
    }
    
    public void setChs(final int field_11_chs) {
        this.field_11_chs = field_11_chs;
    }
    
    public int getChsTables() {
        return this.field_12_chsTables;
    }
    
    public void setChsTables(final int field_12_chsTables) {
        this.field_12_chsTables = field_12_chsTables;
    }
    
    public int getFcMin() {
        return this.field_13_fcMin;
    }
    
    public void setFcMin(final int field_13_fcMin) {
        this.field_13_fcMin = field_13_fcMin;
    }
    
    public int getFcMac() {
        return this.field_14_fcMac;
    }
    
    public void setFcMac(final int field_14_fcMac) {
        this.field_14_fcMac = field_14_fcMac;
    }
    
    public int getCsw() {
        return this.field_15_csw;
    }
    
    public void setCsw(final int field_15_csw) {
        this.field_15_csw = field_15_csw;
    }
    
    public int getWMagicCreated() {
        return this.field_16_wMagicCreated;
    }
    
    public void setWMagicCreated(final int field_16_wMagicCreated) {
        this.field_16_wMagicCreated = field_16_wMagicCreated;
    }
    
    public int getWMagicRevised() {
        return this.field_17_wMagicRevised;
    }
    
    public void setWMagicRevised(final int field_17_wMagicRevised) {
        this.field_17_wMagicRevised = field_17_wMagicRevised;
    }
    
    public int getWMagicCreatedPrivate() {
        return this.field_18_wMagicCreatedPrivate;
    }
    
    public void setWMagicCreatedPrivate(final int field_18_wMagicCreatedPrivate) {
        this.field_18_wMagicCreatedPrivate = field_18_wMagicCreatedPrivate;
    }
    
    public int getWMagicRevisedPrivate() {
        return this.field_19_wMagicRevisedPrivate;
    }
    
    public void setWMagicRevisedPrivate(final int field_19_wMagicRevisedPrivate) {
        this.field_19_wMagicRevisedPrivate = field_19_wMagicRevisedPrivate;
    }
    
    public int getPnFbpChpFirst_W6() {
        return this.field_20_pnFbpChpFirst_W6;
    }
    
    public void setPnFbpChpFirst_W6(final int field_20_pnFbpChpFirst_W6) {
        this.field_20_pnFbpChpFirst_W6 = field_20_pnFbpChpFirst_W6;
    }
    
    public int getPnChpFirst_W6() {
        return this.field_21_pnChpFirst_W6;
    }
    
    public void setPnChpFirst_W6(final int field_21_pnChpFirst_W6) {
        this.field_21_pnChpFirst_W6 = field_21_pnChpFirst_W6;
    }
    
    public int getCpnBteChp_W6() {
        return this.field_22_cpnBteChp_W6;
    }
    
    public void setCpnBteChp_W6(final int field_22_cpnBteChp_W6) {
        this.field_22_cpnBteChp_W6 = field_22_cpnBteChp_W6;
    }
    
    public int getPnFbpPapFirst_W6() {
        return this.field_23_pnFbpPapFirst_W6;
    }
    
    public void setPnFbpPapFirst_W6(final int field_23_pnFbpPapFirst_W6) {
        this.field_23_pnFbpPapFirst_W6 = field_23_pnFbpPapFirst_W6;
    }
    
    public int getPnPapFirst_W6() {
        return this.field_24_pnPapFirst_W6;
    }
    
    public void setPnPapFirst_W6(final int field_24_pnPapFirst_W6) {
        this.field_24_pnPapFirst_W6 = field_24_pnPapFirst_W6;
    }
    
    public int getCpnBtePap_W6() {
        return this.field_25_cpnBtePap_W6;
    }
    
    public void setCpnBtePap_W6(final int field_25_cpnBtePap_W6) {
        this.field_25_cpnBtePap_W6 = field_25_cpnBtePap_W6;
    }
    
    public int getPnFbpLvcFirst_W6() {
        return this.field_26_pnFbpLvcFirst_W6;
    }
    
    public void setPnFbpLvcFirst_W6(final int field_26_pnFbpLvcFirst_W6) {
        this.field_26_pnFbpLvcFirst_W6 = field_26_pnFbpLvcFirst_W6;
    }
    
    public int getPnLvcFirst_W6() {
        return this.field_27_pnLvcFirst_W6;
    }
    
    public void setPnLvcFirst_W6(final int field_27_pnLvcFirst_W6) {
        this.field_27_pnLvcFirst_W6 = field_27_pnLvcFirst_W6;
    }
    
    public int getCpnBteLvc_W6() {
        return this.field_28_cpnBteLvc_W6;
    }
    
    public void setCpnBteLvc_W6(final int field_28_cpnBteLvc_W6) {
        this.field_28_cpnBteLvc_W6 = field_28_cpnBteLvc_W6;
    }
    
    public int getLidFE() {
        return this.field_29_lidFE;
    }
    
    public void setLidFE(final int field_29_lidFE) {
        this.field_29_lidFE = field_29_lidFE;
    }
    
    public int getClw() {
        return this.field_30_clw;
    }
    
    public void setClw(final int field_30_clw) {
        this.field_30_clw = field_30_clw;
    }
    
    public int getCbMac() {
        return this.field_31_cbMac;
    }
    
    public void setCbMac(final int field_31_cbMac) {
        this.field_31_cbMac = field_31_cbMac;
    }
    
    public int getLProductCreated() {
        return this.field_32_lProductCreated;
    }
    
    public void setLProductCreated(final int field_32_lProductCreated) {
        this.field_32_lProductCreated = field_32_lProductCreated;
    }
    
    public int getLProductRevised() {
        return this.field_33_lProductRevised;
    }
    
    public void setLProductRevised(final int field_33_lProductRevised) {
        this.field_33_lProductRevised = field_33_lProductRevised;
    }
    
    public int getCcpText() {
        return this.field_34_ccpText;
    }
    
    public void setCcpText(final int field_34_ccpText) {
        this.field_34_ccpText = field_34_ccpText;
    }
    
    public int getCcpFtn() {
        return this.field_35_ccpFtn;
    }
    
    public void setCcpFtn(final int field_35_ccpFtn) {
        this.field_35_ccpFtn = field_35_ccpFtn;
    }
    
    public int getCcpHdd() {
        return this.field_36_ccpHdd;
    }
    
    public void setCcpHdd(final int field_36_ccpHdd) {
        this.field_36_ccpHdd = field_36_ccpHdd;
    }
    
    public int getCcpMcr() {
        return this.field_37_ccpMcr;
    }
    
    public void setCcpMcr(final int field_37_ccpMcr) {
        this.field_37_ccpMcr = field_37_ccpMcr;
    }
    
    public int getCcpAtn() {
        return this.field_38_ccpAtn;
    }
    
    public void setCcpAtn(final int field_38_ccpAtn) {
        this.field_38_ccpAtn = field_38_ccpAtn;
    }
    
    public int getCcpEdn() {
        return this.field_39_ccpEdn;
    }
    
    public void setCcpEdn(final int field_39_ccpEdn) {
        this.field_39_ccpEdn = field_39_ccpEdn;
    }
    
    public int getCcpTxbx() {
        return this.field_40_ccpTxbx;
    }
    
    public void setCcpTxbx(final int field_40_ccpTxbx) {
        this.field_40_ccpTxbx = field_40_ccpTxbx;
    }
    
    public int getCcpHdrTxbx() {
        return this.field_41_ccpHdrTxbx;
    }
    
    public void setCcpHdrTxbx(final int field_41_ccpHdrTxbx) {
        this.field_41_ccpHdrTxbx = field_41_ccpHdrTxbx;
    }
    
    public int getPnFbpChpFirst() {
        return this.field_42_pnFbpChpFirst;
    }
    
    public void setPnFbpChpFirst(final int field_42_pnFbpChpFirst) {
        this.field_42_pnFbpChpFirst = field_42_pnFbpChpFirst;
    }
    
    public int getPnChpFirst() {
        return this.field_43_pnChpFirst;
    }
    
    public void setPnChpFirst(final int field_43_pnChpFirst) {
        this.field_43_pnChpFirst = field_43_pnChpFirst;
    }
    
    public int getCpnBteChp() {
        return this.field_44_cpnBteChp;
    }
    
    public void setCpnBteChp(final int field_44_cpnBteChp) {
        this.field_44_cpnBteChp = field_44_cpnBteChp;
    }
    
    public int getPnFbpPapFirst() {
        return this.field_45_pnFbpPapFirst;
    }
    
    public void setPnFbpPapFirst(final int field_45_pnFbpPapFirst) {
        this.field_45_pnFbpPapFirst = field_45_pnFbpPapFirst;
    }
    
    public int getPnPapFirst() {
        return this.field_46_pnPapFirst;
    }
    
    public void setPnPapFirst(final int field_46_pnPapFirst) {
        this.field_46_pnPapFirst = field_46_pnPapFirst;
    }
    
    public int getCpnBtePap() {
        return this.field_47_cpnBtePap;
    }
    
    public void setCpnBtePap(final int field_47_cpnBtePap) {
        this.field_47_cpnBtePap = field_47_cpnBtePap;
    }
    
    public int getPnFbpLvcFirst() {
        return this.field_48_pnFbpLvcFirst;
    }
    
    public void setPnFbpLvcFirst(final int field_48_pnFbpLvcFirst) {
        this.field_48_pnFbpLvcFirst = field_48_pnFbpLvcFirst;
    }
    
    public int getPnLvcFirst() {
        return this.field_49_pnLvcFirst;
    }
    
    public void setPnLvcFirst(final int field_49_pnLvcFirst) {
        this.field_49_pnLvcFirst = field_49_pnLvcFirst;
    }
    
    public int getCpnBteLvc() {
        return this.field_50_cpnBteLvc;
    }
    
    public void setCpnBteLvc(final int field_50_cpnBteLvc) {
        this.field_50_cpnBteLvc = field_50_cpnBteLvc;
    }
    
    public int getFcIslandFirst() {
        return this.field_51_fcIslandFirst;
    }
    
    public void setFcIslandFirst(final int field_51_fcIslandFirst) {
        this.field_51_fcIslandFirst = field_51_fcIslandFirst;
    }
    
    public int getFcIslandLim() {
        return this.field_52_fcIslandLim;
    }
    
    public void setFcIslandLim(final int field_52_fcIslandLim) {
        this.field_52_fcIslandLim = field_52_fcIslandLim;
    }
    
    public int getCfclcb() {
        return this.field_53_cfclcb;
    }
    
    public void setCfclcb(final int field_53_cfclcb) {
        this.field_53_cfclcb = field_53_cfclcb;
    }
    
    public int getFcStshfOrig() {
        return this.field_54_fcStshfOrig;
    }
    
    public void setFcStshfOrig(final int field_54_fcStshfOrig) {
        this.field_54_fcStshfOrig = field_54_fcStshfOrig;
    }
    
    public int getLcbStshfOrig() {
        return this.field_55_lcbStshfOrig;
    }
    
    public void setLcbStshfOrig(final int field_55_lcbStshfOrig) {
        this.field_55_lcbStshfOrig = field_55_lcbStshfOrig;
    }
    
    public int getFcStshf() {
        return this.field_56_fcStshf;
    }
    
    public void setFcStshf(final int field_56_fcStshf) {
        this.field_56_fcStshf = field_56_fcStshf;
    }
    
    public int getLcbStshf() {
        return this.field_57_lcbStshf;
    }
    
    public void setLcbStshf(final int field_57_lcbStshf) {
        this.field_57_lcbStshf = field_57_lcbStshf;
    }
    
    public int getFcPlcffndRef() {
        return this.field_58_fcPlcffndRef;
    }
    
    public void setFcPlcffndRef(final int field_58_fcPlcffndRef) {
        this.field_58_fcPlcffndRef = field_58_fcPlcffndRef;
    }
    
    public int getLcbPlcffndRef() {
        return this.field_59_lcbPlcffndRef;
    }
    
    public void setLcbPlcffndRef(final int field_59_lcbPlcffndRef) {
        this.field_59_lcbPlcffndRef = field_59_lcbPlcffndRef;
    }
    
    public int getFcPlcffndTxt() {
        return this.field_60_fcPlcffndTxt;
    }
    
    public void setFcPlcffndTxt(final int field_60_fcPlcffndTxt) {
        this.field_60_fcPlcffndTxt = field_60_fcPlcffndTxt;
    }
    
    public int getLcbPlcffndTxt() {
        return this.field_61_lcbPlcffndTxt;
    }
    
    public void setLcbPlcffndTxt(final int field_61_lcbPlcffndTxt) {
        this.field_61_lcbPlcffndTxt = field_61_lcbPlcffndTxt;
    }
    
    public int getFcPlcfandRef() {
        return this.field_62_fcPlcfandRef;
    }
    
    public void setFcPlcfandRef(final int field_62_fcPlcfandRef) {
        this.field_62_fcPlcfandRef = field_62_fcPlcfandRef;
    }
    
    public int getLcbPlcfandRef() {
        return this.field_63_lcbPlcfandRef;
    }
    
    public void setLcbPlcfandRef(final int field_63_lcbPlcfandRef) {
        this.field_63_lcbPlcfandRef = field_63_lcbPlcfandRef;
    }
    
    public int getFcPlcfandTxt() {
        return this.field_64_fcPlcfandTxt;
    }
    
    public void setFcPlcfandTxt(final int field_64_fcPlcfandTxt) {
        this.field_64_fcPlcfandTxt = field_64_fcPlcfandTxt;
    }
    
    public int getLcbPlcfandTxt() {
        return this.field_65_lcbPlcfandTxt;
    }
    
    public void setLcbPlcfandTxt(final int field_65_lcbPlcfandTxt) {
        this.field_65_lcbPlcfandTxt = field_65_lcbPlcfandTxt;
    }
    
    public int getFcPlcfsed() {
        return this.field_66_fcPlcfsed;
    }
    
    public void setFcPlcfsed(final int field_66_fcPlcfsed) {
        this.field_66_fcPlcfsed = field_66_fcPlcfsed;
    }
    
    public int getLcbPlcfsed() {
        return this.field_67_lcbPlcfsed;
    }
    
    public void setLcbPlcfsed(final int field_67_lcbPlcfsed) {
        this.field_67_lcbPlcfsed = field_67_lcbPlcfsed;
    }
    
    public int getFcPlcpad() {
        return this.field_68_fcPlcpad;
    }
    
    public void setFcPlcpad(final int field_68_fcPlcpad) {
        this.field_68_fcPlcpad = field_68_fcPlcpad;
    }
    
    public int getLcbPlcpad() {
        return this.field_69_lcbPlcpad;
    }
    
    public void setLcbPlcpad(final int field_69_lcbPlcpad) {
        this.field_69_lcbPlcpad = field_69_lcbPlcpad;
    }
    
    public int getFcPlcfphe() {
        return this.field_70_fcPlcfphe;
    }
    
    public void setFcPlcfphe(final int field_70_fcPlcfphe) {
        this.field_70_fcPlcfphe = field_70_fcPlcfphe;
    }
    
    public int getLcbPlcfphe() {
        return this.field_71_lcbPlcfphe;
    }
    
    public void setLcbPlcfphe(final int field_71_lcbPlcfphe) {
        this.field_71_lcbPlcfphe = field_71_lcbPlcfphe;
    }
    
    public int getFcSttbfglsy() {
        return this.field_72_fcSttbfglsy;
    }
    
    public void setFcSttbfglsy(final int field_72_fcSttbfglsy) {
        this.field_72_fcSttbfglsy = field_72_fcSttbfglsy;
    }
    
    public int getLcbSttbfglsy() {
        return this.field_73_lcbSttbfglsy;
    }
    
    public void setLcbSttbfglsy(final int field_73_lcbSttbfglsy) {
        this.field_73_lcbSttbfglsy = field_73_lcbSttbfglsy;
    }
    
    public int getFcPlcfglsy() {
        return this.field_74_fcPlcfglsy;
    }
    
    public void setFcPlcfglsy(final int field_74_fcPlcfglsy) {
        this.field_74_fcPlcfglsy = field_74_fcPlcfglsy;
    }
    
    public int getLcbPlcfglsy() {
        return this.field_75_lcbPlcfglsy;
    }
    
    public void setLcbPlcfglsy(final int field_75_lcbPlcfglsy) {
        this.field_75_lcbPlcfglsy = field_75_lcbPlcfglsy;
    }
    
    public int getFcPlcfhdd() {
        return this.field_76_fcPlcfhdd;
    }
    
    public void setFcPlcfhdd(final int field_76_fcPlcfhdd) {
        this.field_76_fcPlcfhdd = field_76_fcPlcfhdd;
    }
    
    public int getLcbPlcfhdd() {
        return this.field_77_lcbPlcfhdd;
    }
    
    public void setLcbPlcfhdd(final int field_77_lcbPlcfhdd) {
        this.field_77_lcbPlcfhdd = field_77_lcbPlcfhdd;
    }
    
    public int getFcPlcfbteChpx() {
        return this.field_78_fcPlcfbteChpx;
    }
    
    public void setFcPlcfbteChpx(final int field_78_fcPlcfbteChpx) {
        this.field_78_fcPlcfbteChpx = field_78_fcPlcfbteChpx;
    }
    
    public int getLcbPlcfbteChpx() {
        return this.field_79_lcbPlcfbteChpx;
    }
    
    public void setLcbPlcfbteChpx(final int field_79_lcbPlcfbteChpx) {
        this.field_79_lcbPlcfbteChpx = field_79_lcbPlcfbteChpx;
    }
    
    public int getFcPlcfbtePapx() {
        return this.field_80_fcPlcfbtePapx;
    }
    
    public void setFcPlcfbtePapx(final int field_80_fcPlcfbtePapx) {
        this.field_80_fcPlcfbtePapx = field_80_fcPlcfbtePapx;
    }
    
    public int getLcbPlcfbtePapx() {
        return this.field_81_lcbPlcfbtePapx;
    }
    
    public void setLcbPlcfbtePapx(final int field_81_lcbPlcfbtePapx) {
        this.field_81_lcbPlcfbtePapx = field_81_lcbPlcfbtePapx;
    }
    
    public int getFcPlcfsea() {
        return this.field_82_fcPlcfsea;
    }
    
    public void setFcPlcfsea(final int field_82_fcPlcfsea) {
        this.field_82_fcPlcfsea = field_82_fcPlcfsea;
    }
    
    public int getLcbPlcfsea() {
        return this.field_83_lcbPlcfsea;
    }
    
    public void setLcbPlcfsea(final int field_83_lcbPlcfsea) {
        this.field_83_lcbPlcfsea = field_83_lcbPlcfsea;
    }
    
    public int getFcSttbfffn() {
        return this.field_84_fcSttbfffn;
    }
    
    public void setFcSttbfffn(final int field_84_fcSttbfffn) {
        this.field_84_fcSttbfffn = field_84_fcSttbfffn;
    }
    
    public int getLcbSttbfffn() {
        return this.field_85_lcbSttbfffn;
    }
    
    public void setLcbSttbfffn(final int field_85_lcbSttbfffn) {
        this.field_85_lcbSttbfffn = field_85_lcbSttbfffn;
    }
    
    public int getFcPlcffldMom() {
        return this.field_86_fcPlcffldMom;
    }
    
    public void setFcPlcffldMom(final int field_86_fcPlcffldMom) {
        this.field_86_fcPlcffldMom = field_86_fcPlcffldMom;
    }
    
    public int getLcbPlcffldMom() {
        return this.field_87_lcbPlcffldMom;
    }
    
    public void setLcbPlcffldMom(final int field_87_lcbPlcffldMom) {
        this.field_87_lcbPlcffldMom = field_87_lcbPlcffldMom;
    }
    
    public int getFcPlcffldHdr() {
        return this.field_88_fcPlcffldHdr;
    }
    
    public void setFcPlcffldHdr(final int field_88_fcPlcffldHdr) {
        this.field_88_fcPlcffldHdr = field_88_fcPlcffldHdr;
    }
    
    public int getLcbPlcffldHdr() {
        return this.field_89_lcbPlcffldHdr;
    }
    
    public void setLcbPlcffldHdr(final int field_89_lcbPlcffldHdr) {
        this.field_89_lcbPlcffldHdr = field_89_lcbPlcffldHdr;
    }
    
    public int getFcPlcffldFtn() {
        return this.field_90_fcPlcffldFtn;
    }
    
    public void setFcPlcffldFtn(final int field_90_fcPlcffldFtn) {
        this.field_90_fcPlcffldFtn = field_90_fcPlcffldFtn;
    }
    
    public int getLcbPlcffldFtn() {
        return this.field_91_lcbPlcffldFtn;
    }
    
    public void setLcbPlcffldFtn(final int field_91_lcbPlcffldFtn) {
        this.field_91_lcbPlcffldFtn = field_91_lcbPlcffldFtn;
    }
    
    public int getFcPlcffldAtn() {
        return this.field_92_fcPlcffldAtn;
    }
    
    public void setFcPlcffldAtn(final int field_92_fcPlcffldAtn) {
        this.field_92_fcPlcffldAtn = field_92_fcPlcffldAtn;
    }
    
    public int getLcbPlcffldAtn() {
        return this.field_93_lcbPlcffldAtn;
    }
    
    public void setLcbPlcffldAtn(final int field_93_lcbPlcffldAtn) {
        this.field_93_lcbPlcffldAtn = field_93_lcbPlcffldAtn;
    }
    
    public int getFcPlcffldMcr() {
        return this.field_94_fcPlcffldMcr;
    }
    
    public void setFcPlcffldMcr(final int field_94_fcPlcffldMcr) {
        this.field_94_fcPlcffldMcr = field_94_fcPlcffldMcr;
    }
    
    public int getLcbPlcffldMcr() {
        return this.field_95_lcbPlcffldMcr;
    }
    
    public void setLcbPlcffldMcr(final int field_95_lcbPlcffldMcr) {
        this.field_95_lcbPlcffldMcr = field_95_lcbPlcffldMcr;
    }
    
    public int getFcSttbfbkmk() {
        return this.field_96_fcSttbfbkmk;
    }
    
    public void setFcSttbfbkmk(final int field_96_fcSttbfbkmk) {
        this.field_96_fcSttbfbkmk = field_96_fcSttbfbkmk;
    }
    
    public int getLcbSttbfbkmk() {
        return this.field_97_lcbSttbfbkmk;
    }
    
    public void setLcbSttbfbkmk(final int field_97_lcbSttbfbkmk) {
        this.field_97_lcbSttbfbkmk = field_97_lcbSttbfbkmk;
    }
    
    public int getFcPlcfbkf() {
        return this.field_98_fcPlcfbkf;
    }
    
    public void setFcPlcfbkf(final int field_98_fcPlcfbkf) {
        this.field_98_fcPlcfbkf = field_98_fcPlcfbkf;
    }
    
    public int getLcbPlcfbkf() {
        return this.field_99_lcbPlcfbkf;
    }
    
    public void setLcbPlcfbkf(final int field_99_lcbPlcfbkf) {
        this.field_99_lcbPlcfbkf = field_99_lcbPlcfbkf;
    }
    
    public int getFcPlcfbkl() {
        return this.field_100_fcPlcfbkl;
    }
    
    public void setFcPlcfbkl(final int field_100_fcPlcfbkl) {
        this.field_100_fcPlcfbkl = field_100_fcPlcfbkl;
    }
    
    public int getLcbPlcfbkl() {
        return this.field_101_lcbPlcfbkl;
    }
    
    public void setLcbPlcfbkl(final int field_101_lcbPlcfbkl) {
        this.field_101_lcbPlcfbkl = field_101_lcbPlcfbkl;
    }
    
    public int getFcCmds() {
        return this.field_102_fcCmds;
    }
    
    public void setFcCmds(final int field_102_fcCmds) {
        this.field_102_fcCmds = field_102_fcCmds;
    }
    
    public int getLcbCmds() {
        return this.field_103_lcbCmds;
    }
    
    public void setLcbCmds(final int field_103_lcbCmds) {
        this.field_103_lcbCmds = field_103_lcbCmds;
    }
    
    public int getFcPlcmcr() {
        return this.field_104_fcPlcmcr;
    }
    
    public void setFcPlcmcr(final int field_104_fcPlcmcr) {
        this.field_104_fcPlcmcr = field_104_fcPlcmcr;
    }
    
    public int getLcbPlcmcr() {
        return this.field_105_lcbPlcmcr;
    }
    
    public void setLcbPlcmcr(final int field_105_lcbPlcmcr) {
        this.field_105_lcbPlcmcr = field_105_lcbPlcmcr;
    }
    
    public int getFcSttbfmcr() {
        return this.field_106_fcSttbfmcr;
    }
    
    public void setFcSttbfmcr(final int field_106_fcSttbfmcr) {
        this.field_106_fcSttbfmcr = field_106_fcSttbfmcr;
    }
    
    public int getLcbSttbfmcr() {
        return this.field_107_lcbSttbfmcr;
    }
    
    public void setLcbSttbfmcr(final int field_107_lcbSttbfmcr) {
        this.field_107_lcbSttbfmcr = field_107_lcbSttbfmcr;
    }
    
    public int getFcPrDrvr() {
        return this.field_108_fcPrDrvr;
    }
    
    public void setFcPrDrvr(final int field_108_fcPrDrvr) {
        this.field_108_fcPrDrvr = field_108_fcPrDrvr;
    }
    
    public int getLcbPrDrvr() {
        return this.field_109_lcbPrDrvr;
    }
    
    public void setLcbPrDrvr(final int field_109_lcbPrDrvr) {
        this.field_109_lcbPrDrvr = field_109_lcbPrDrvr;
    }
    
    public int getFcPrEnvPort() {
        return this.field_110_fcPrEnvPort;
    }
    
    public void setFcPrEnvPort(final int field_110_fcPrEnvPort) {
        this.field_110_fcPrEnvPort = field_110_fcPrEnvPort;
    }
    
    public int getLcbPrEnvPort() {
        return this.field_111_lcbPrEnvPort;
    }
    
    public void setLcbPrEnvPort(final int field_111_lcbPrEnvPort) {
        this.field_111_lcbPrEnvPort = field_111_lcbPrEnvPort;
    }
    
    public int getFcPrEnvLand() {
        return this.field_112_fcPrEnvLand;
    }
    
    public void setFcPrEnvLand(final int field_112_fcPrEnvLand) {
        this.field_112_fcPrEnvLand = field_112_fcPrEnvLand;
    }
    
    public int getLcbPrEnvLand() {
        return this.field_113_lcbPrEnvLand;
    }
    
    public void setLcbPrEnvLand(final int field_113_lcbPrEnvLand) {
        this.field_113_lcbPrEnvLand = field_113_lcbPrEnvLand;
    }
    
    public int getFcWss() {
        return this.field_114_fcWss;
    }
    
    public void setFcWss(final int field_114_fcWss) {
        this.field_114_fcWss = field_114_fcWss;
    }
    
    public int getLcbWss() {
        return this.field_115_lcbWss;
    }
    
    public void setLcbWss(final int field_115_lcbWss) {
        this.field_115_lcbWss = field_115_lcbWss;
    }
    
    public int getFcDop() {
        return this.field_116_fcDop;
    }
    
    public void setFcDop(final int field_116_fcDop) {
        this.field_116_fcDop = field_116_fcDop;
    }
    
    public int getLcbDop() {
        return this.field_117_lcbDop;
    }
    
    public void setLcbDop(final int field_117_lcbDop) {
        this.field_117_lcbDop = field_117_lcbDop;
    }
    
    public int getFcSttbfAssoc() {
        return this.field_118_fcSttbfAssoc;
    }
    
    public void setFcSttbfAssoc(final int field_118_fcSttbfAssoc) {
        this.field_118_fcSttbfAssoc = field_118_fcSttbfAssoc;
    }
    
    public int getLcbSttbfAssoc() {
        return this.field_119_lcbSttbfAssoc;
    }
    
    public void setLcbSttbfAssoc(final int field_119_lcbSttbfAssoc) {
        this.field_119_lcbSttbfAssoc = field_119_lcbSttbfAssoc;
    }
    
    public int getFcClx() {
        return this.field_120_fcClx;
    }
    
    public void setFcClx(final int field_120_fcClx) {
        this.field_120_fcClx = field_120_fcClx;
    }
    
    public int getLcbClx() {
        return this.field_121_lcbClx;
    }
    
    public void setLcbClx(final int field_121_lcbClx) {
        this.field_121_lcbClx = field_121_lcbClx;
    }
    
    public int getFcPlcfpgdFtn() {
        return this.field_122_fcPlcfpgdFtn;
    }
    
    public void setFcPlcfpgdFtn(final int field_122_fcPlcfpgdFtn) {
        this.field_122_fcPlcfpgdFtn = field_122_fcPlcfpgdFtn;
    }
    
    public int getLcbPlcfpgdFtn() {
        return this.field_123_lcbPlcfpgdFtn;
    }
    
    public void setLcbPlcfpgdFtn(final int field_123_lcbPlcfpgdFtn) {
        this.field_123_lcbPlcfpgdFtn = field_123_lcbPlcfpgdFtn;
    }
    
    public int getFcAutosaveSource() {
        return this.field_124_fcAutosaveSource;
    }
    
    public void setFcAutosaveSource(final int field_124_fcAutosaveSource) {
        this.field_124_fcAutosaveSource = field_124_fcAutosaveSource;
    }
    
    public int getLcbAutosaveSource() {
        return this.field_125_lcbAutosaveSource;
    }
    
    public void setLcbAutosaveSource(final int field_125_lcbAutosaveSource) {
        this.field_125_lcbAutosaveSource = field_125_lcbAutosaveSource;
    }
    
    public int getFcGrpXstAtnOwners() {
        return this.field_126_fcGrpXstAtnOwners;
    }
    
    public void setFcGrpXstAtnOwners(final int field_126_fcGrpXstAtnOwners) {
        this.field_126_fcGrpXstAtnOwners = field_126_fcGrpXstAtnOwners;
    }
    
    public int getLcbGrpXstAtnOwners() {
        return this.field_127_lcbGrpXstAtnOwners;
    }
    
    public void setLcbGrpXstAtnOwners(final int field_127_lcbGrpXstAtnOwners) {
        this.field_127_lcbGrpXstAtnOwners = field_127_lcbGrpXstAtnOwners;
    }
    
    public int getFcSttbfAtnbkmk() {
        return this.field_128_fcSttbfAtnbkmk;
    }
    
    public void setFcSttbfAtnbkmk(final int field_128_fcSttbfAtnbkmk) {
        this.field_128_fcSttbfAtnbkmk = field_128_fcSttbfAtnbkmk;
    }
    
    public int getLcbSttbfAtnbkmk() {
        return this.field_129_lcbSttbfAtnbkmk;
    }
    
    public void setLcbSttbfAtnbkmk(final int field_129_lcbSttbfAtnbkmk) {
        this.field_129_lcbSttbfAtnbkmk = field_129_lcbSttbfAtnbkmk;
    }
    
    public int getFcPlcdoaMom() {
        return this.field_130_fcPlcdoaMom;
    }
    
    public void setFcPlcdoaMom(final int field_130_fcPlcdoaMom) {
        this.field_130_fcPlcdoaMom = field_130_fcPlcdoaMom;
    }
    
    public int getLcbPlcdoaMom() {
        return this.field_131_lcbPlcdoaMom;
    }
    
    public void setLcbPlcdoaMom(final int field_131_lcbPlcdoaMom) {
        this.field_131_lcbPlcdoaMom = field_131_lcbPlcdoaMom;
    }
    
    public int getFcPlcdoaHdr() {
        return this.field_132_fcPlcdoaHdr;
    }
    
    public void setFcPlcdoaHdr(final int field_132_fcPlcdoaHdr) {
        this.field_132_fcPlcdoaHdr = field_132_fcPlcdoaHdr;
    }
    
    public int getLcbPlcdoaHdr() {
        return this.field_133_lcbPlcdoaHdr;
    }
    
    public void setLcbPlcdoaHdr(final int field_133_lcbPlcdoaHdr) {
        this.field_133_lcbPlcdoaHdr = field_133_lcbPlcdoaHdr;
    }
    
    public int getFcPlcspaMom() {
        return this.field_134_fcPlcspaMom;
    }
    
    public void setFcPlcspaMom(final int field_134_fcPlcspaMom) {
        this.field_134_fcPlcspaMom = field_134_fcPlcspaMom;
    }
    
    public int getLcbPlcspaMom() {
        return this.field_135_lcbPlcspaMom;
    }
    
    public void setLcbPlcspaMom(final int field_135_lcbPlcspaMom) {
        this.field_135_lcbPlcspaMom = field_135_lcbPlcspaMom;
    }
    
    public int getFcPlcspaHdr() {
        return this.field_136_fcPlcspaHdr;
    }
    
    public void setFcPlcspaHdr(final int field_136_fcPlcspaHdr) {
        this.field_136_fcPlcspaHdr = field_136_fcPlcspaHdr;
    }
    
    public int getLcbPlcspaHdr() {
        return this.field_137_lcbPlcspaHdr;
    }
    
    public void setLcbPlcspaHdr(final int field_137_lcbPlcspaHdr) {
        this.field_137_lcbPlcspaHdr = field_137_lcbPlcspaHdr;
    }
    
    public int getFcPlcfAtnbkf() {
        return this.field_138_fcPlcfAtnbkf;
    }
    
    public void setFcPlcfAtnbkf(final int field_138_fcPlcfAtnbkf) {
        this.field_138_fcPlcfAtnbkf = field_138_fcPlcfAtnbkf;
    }
    
    public int getLcbPlcfAtnbkf() {
        return this.field_139_lcbPlcfAtnbkf;
    }
    
    public void setLcbPlcfAtnbkf(final int field_139_lcbPlcfAtnbkf) {
        this.field_139_lcbPlcfAtnbkf = field_139_lcbPlcfAtnbkf;
    }
    
    public int getFcPlcfAtnbkl() {
        return this.field_140_fcPlcfAtnbkl;
    }
    
    public void setFcPlcfAtnbkl(final int field_140_fcPlcfAtnbkl) {
        this.field_140_fcPlcfAtnbkl = field_140_fcPlcfAtnbkl;
    }
    
    public int getLcbPlcfAtnbkl() {
        return this.field_141_lcbPlcfAtnbkl;
    }
    
    public void setLcbPlcfAtnbkl(final int field_141_lcbPlcfAtnbkl) {
        this.field_141_lcbPlcfAtnbkl = field_141_lcbPlcfAtnbkl;
    }
    
    public int getFcPms() {
        return this.field_142_fcPms;
    }
    
    public void setFcPms(final int field_142_fcPms) {
        this.field_142_fcPms = field_142_fcPms;
    }
    
    public int getLcbPms() {
        return this.field_143_lcbPms;
    }
    
    public void setLcbPms(final int field_143_lcbPms) {
        this.field_143_lcbPms = field_143_lcbPms;
    }
    
    public int getFcFormFldSttbs() {
        return this.field_144_fcFormFldSttbs;
    }
    
    public void setFcFormFldSttbs(final int field_144_fcFormFldSttbs) {
        this.field_144_fcFormFldSttbs = field_144_fcFormFldSttbs;
    }
    
    public int getLcbFormFldSttbs() {
        return this.field_145_lcbFormFldSttbs;
    }
    
    public void setLcbFormFldSttbs(final int field_145_lcbFormFldSttbs) {
        this.field_145_lcbFormFldSttbs = field_145_lcbFormFldSttbs;
    }
    
    public int getFcPlcfendRef() {
        return this.field_146_fcPlcfendRef;
    }
    
    public void setFcPlcfendRef(final int field_146_fcPlcfendRef) {
        this.field_146_fcPlcfendRef = field_146_fcPlcfendRef;
    }
    
    public int getLcbPlcfendRef() {
        return this.field_147_lcbPlcfendRef;
    }
    
    public void setLcbPlcfendRef(final int field_147_lcbPlcfendRef) {
        this.field_147_lcbPlcfendRef = field_147_lcbPlcfendRef;
    }
    
    public int getFcPlcfendTxt() {
        return this.field_148_fcPlcfendTxt;
    }
    
    public void setFcPlcfendTxt(final int field_148_fcPlcfendTxt) {
        this.field_148_fcPlcfendTxt = field_148_fcPlcfendTxt;
    }
    
    public int getLcbPlcfendTxt() {
        return this.field_149_lcbPlcfendTxt;
    }
    
    public void setLcbPlcfendTxt(final int field_149_lcbPlcfendTxt) {
        this.field_149_lcbPlcfendTxt = field_149_lcbPlcfendTxt;
    }
    
    public int getFcPlcffldEdn() {
        return this.field_150_fcPlcffldEdn;
    }
    
    public void setFcPlcffldEdn(final int field_150_fcPlcffldEdn) {
        this.field_150_fcPlcffldEdn = field_150_fcPlcffldEdn;
    }
    
    public int getLcbPlcffldEdn() {
        return this.field_151_lcbPlcffldEdn;
    }
    
    public void setLcbPlcffldEdn(final int field_151_lcbPlcffldEdn) {
        this.field_151_lcbPlcffldEdn = field_151_lcbPlcffldEdn;
    }
    
    public int getFcPlcfpgdEdn() {
        return this.field_152_fcPlcfpgdEdn;
    }
    
    public void setFcPlcfpgdEdn(final int field_152_fcPlcfpgdEdn) {
        this.field_152_fcPlcfpgdEdn = field_152_fcPlcfpgdEdn;
    }
    
    public int getLcbPlcfpgdEdn() {
        return this.field_153_lcbPlcfpgdEdn;
    }
    
    public void setLcbPlcfpgdEdn(final int field_153_lcbPlcfpgdEdn) {
        this.field_153_lcbPlcfpgdEdn = field_153_lcbPlcfpgdEdn;
    }
    
    public int getFcDggInfo() {
        return this.field_154_fcDggInfo;
    }
    
    public void setFcDggInfo(final int field_154_fcDggInfo) {
        this.field_154_fcDggInfo = field_154_fcDggInfo;
    }
    
    public int getLcbDggInfo() {
        return this.field_155_lcbDggInfo;
    }
    
    public void setLcbDggInfo(final int field_155_lcbDggInfo) {
        this.field_155_lcbDggInfo = field_155_lcbDggInfo;
    }
    
    public int getFcSttbfRMark() {
        return this.field_156_fcSttbfRMark;
    }
    
    public void setFcSttbfRMark(final int field_156_fcSttbfRMark) {
        this.field_156_fcSttbfRMark = field_156_fcSttbfRMark;
    }
    
    public int getLcbSttbfRMark() {
        return this.field_157_lcbSttbfRMark;
    }
    
    public void setLcbSttbfRMark(final int field_157_lcbSttbfRMark) {
        this.field_157_lcbSttbfRMark = field_157_lcbSttbfRMark;
    }
    
    public int getFcSttbCaption() {
        return this.field_158_fcSttbCaption;
    }
    
    public void setFcSttbCaption(final int field_158_fcSttbCaption) {
        this.field_158_fcSttbCaption = field_158_fcSttbCaption;
    }
    
    public int getLcbSttbCaption() {
        return this.field_159_lcbSttbCaption;
    }
    
    public void setLcbSttbCaption(final int field_159_lcbSttbCaption) {
        this.field_159_lcbSttbCaption = field_159_lcbSttbCaption;
    }
    
    public int getFcSttbAutoCaption() {
        return this.field_160_fcSttbAutoCaption;
    }
    
    public void setFcSttbAutoCaption(final int field_160_fcSttbAutoCaption) {
        this.field_160_fcSttbAutoCaption = field_160_fcSttbAutoCaption;
    }
    
    public int getLcbSttbAutoCaption() {
        return this.field_161_lcbSttbAutoCaption;
    }
    
    public void setLcbSttbAutoCaption(final int field_161_lcbSttbAutoCaption) {
        this.field_161_lcbSttbAutoCaption = field_161_lcbSttbAutoCaption;
    }
    
    public int getFcPlcfwkb() {
        return this.field_162_fcPlcfwkb;
    }
    
    public void setFcPlcfwkb(final int field_162_fcPlcfwkb) {
        this.field_162_fcPlcfwkb = field_162_fcPlcfwkb;
    }
    
    public int getLcbPlcfwkb() {
        return this.field_163_lcbPlcfwkb;
    }
    
    public void setLcbPlcfwkb(final int field_163_lcbPlcfwkb) {
        this.field_163_lcbPlcfwkb = field_163_lcbPlcfwkb;
    }
    
    public int getFcPlcfspl() {
        return this.field_164_fcPlcfspl;
    }
    
    public void setFcPlcfspl(final int field_164_fcPlcfspl) {
        this.field_164_fcPlcfspl = field_164_fcPlcfspl;
    }
    
    public int getLcbPlcfspl() {
        return this.field_165_lcbPlcfspl;
    }
    
    public void setLcbPlcfspl(final int field_165_lcbPlcfspl) {
        this.field_165_lcbPlcfspl = field_165_lcbPlcfspl;
    }
    
    public int getFcPlcftxbxTxt() {
        return this.field_166_fcPlcftxbxTxt;
    }
    
    public void setFcPlcftxbxTxt(final int field_166_fcPlcftxbxTxt) {
        this.field_166_fcPlcftxbxTxt = field_166_fcPlcftxbxTxt;
    }
    
    public int getLcbPlcftxbxTxt() {
        return this.field_167_lcbPlcftxbxTxt;
    }
    
    public void setLcbPlcftxbxTxt(final int field_167_lcbPlcftxbxTxt) {
        this.field_167_lcbPlcftxbxTxt = field_167_lcbPlcftxbxTxt;
    }
    
    public int getFcPlcffldTxbx() {
        return this.field_168_fcPlcffldTxbx;
    }
    
    public void setFcPlcffldTxbx(final int field_168_fcPlcffldTxbx) {
        this.field_168_fcPlcffldTxbx = field_168_fcPlcffldTxbx;
    }
    
    public int getLcbPlcffldTxbx() {
        return this.field_169_lcbPlcffldTxbx;
    }
    
    public void setLcbPlcffldTxbx(final int field_169_lcbPlcffldTxbx) {
        this.field_169_lcbPlcffldTxbx = field_169_lcbPlcffldTxbx;
    }
    
    public int getFcPlcfhdrtxbxTxt() {
        return this.field_170_fcPlcfhdrtxbxTxt;
    }
    
    public void setFcPlcfhdrtxbxTxt(final int field_170_fcPlcfhdrtxbxTxt) {
        this.field_170_fcPlcfhdrtxbxTxt = field_170_fcPlcfhdrtxbxTxt;
    }
    
    public int getLcbPlcfhdrtxbxTxt() {
        return this.field_171_lcbPlcfhdrtxbxTxt;
    }
    
    public void setLcbPlcfhdrtxbxTxt(final int field_171_lcbPlcfhdrtxbxTxt) {
        this.field_171_lcbPlcfhdrtxbxTxt = field_171_lcbPlcfhdrtxbxTxt;
    }
    
    public int getFcPlcffldHdrTxbx() {
        return this.field_172_fcPlcffldHdrTxbx;
    }
    
    public void setFcPlcffldHdrTxbx(final int field_172_fcPlcffldHdrTxbx) {
        this.field_172_fcPlcffldHdrTxbx = field_172_fcPlcffldHdrTxbx;
    }
    
    public int getLcbPlcffldHdrTxbx() {
        return this.field_173_lcbPlcffldHdrTxbx;
    }
    
    public void setLcbPlcffldHdrTxbx(final int field_173_lcbPlcffldHdrTxbx) {
        this.field_173_lcbPlcffldHdrTxbx = field_173_lcbPlcffldHdrTxbx;
    }
    
    public int getFcStwUser() {
        return this.field_174_fcStwUser;
    }
    
    public void setFcStwUser(final int field_174_fcStwUser) {
        this.field_174_fcStwUser = field_174_fcStwUser;
    }
    
    public int getLcbStwUser() {
        return this.field_175_lcbStwUser;
    }
    
    public void setLcbStwUser(final int field_175_lcbStwUser) {
        this.field_175_lcbStwUser = field_175_lcbStwUser;
    }
    
    public int getFcSttbttmbd() {
        return this.field_176_fcSttbttmbd;
    }
    
    public void setFcSttbttmbd(final int field_176_fcSttbttmbd) {
        this.field_176_fcSttbttmbd = field_176_fcSttbttmbd;
    }
    
    public int getCbSttbttmbd() {
        return this.field_177_cbSttbttmbd;
    }
    
    public void setCbSttbttmbd(final int field_177_cbSttbttmbd) {
        this.field_177_cbSttbttmbd = field_177_cbSttbttmbd;
    }
    
    public int getFcUnused() {
        return this.field_178_fcUnused;
    }
    
    public void setFcUnused(final int field_178_fcUnused) {
        this.field_178_fcUnused = field_178_fcUnused;
    }
    
    public int getLcbUnused() {
        return this.field_179_lcbUnused;
    }
    
    public void setLcbUnused(final int field_179_lcbUnused) {
        this.field_179_lcbUnused = field_179_lcbUnused;
    }
    
    public int getFcPgdMother() {
        return this.field_180_fcPgdMother;
    }
    
    public void setFcPgdMother(final int field_180_fcPgdMother) {
        this.field_180_fcPgdMother = field_180_fcPgdMother;
    }
    
    public int getLcbPgdMother() {
        return this.field_181_lcbPgdMother;
    }
    
    public void setLcbPgdMother(final int field_181_lcbPgdMother) {
        this.field_181_lcbPgdMother = field_181_lcbPgdMother;
    }
    
    public int getFcBkdMother() {
        return this.field_182_fcBkdMother;
    }
    
    public void setFcBkdMother(final int field_182_fcBkdMother) {
        this.field_182_fcBkdMother = field_182_fcBkdMother;
    }
    
    public int getLcbBkdMother() {
        return this.field_183_lcbBkdMother;
    }
    
    public void setLcbBkdMother(final int field_183_lcbBkdMother) {
        this.field_183_lcbBkdMother = field_183_lcbBkdMother;
    }
    
    public int getFcPgdFtn() {
        return this.field_184_fcPgdFtn;
    }
    
    public void setFcPgdFtn(final int field_184_fcPgdFtn) {
        this.field_184_fcPgdFtn = field_184_fcPgdFtn;
    }
    
    public int getLcbPgdFtn() {
        return this.field_185_lcbPgdFtn;
    }
    
    public void setLcbPgdFtn(final int field_185_lcbPgdFtn) {
        this.field_185_lcbPgdFtn = field_185_lcbPgdFtn;
    }
    
    public int getFcBkdFtn() {
        return this.field_186_fcBkdFtn;
    }
    
    public void setFcBkdFtn(final int field_186_fcBkdFtn) {
        this.field_186_fcBkdFtn = field_186_fcBkdFtn;
    }
    
    public int getLcbBkdFtn() {
        return this.field_187_lcbBkdFtn;
    }
    
    public void setLcbBkdFtn(final int field_187_lcbBkdFtn) {
        this.field_187_lcbBkdFtn = field_187_lcbBkdFtn;
    }
    
    public int getFcPgdEdn() {
        return this.field_188_fcPgdEdn;
    }
    
    public void setFcPgdEdn(final int field_188_fcPgdEdn) {
        this.field_188_fcPgdEdn = field_188_fcPgdEdn;
    }
    
    public int getLcbPgdEdn() {
        return this.field_189_lcbPgdEdn;
    }
    
    public void setLcbPgdEdn(final int field_189_lcbPgdEdn) {
        this.field_189_lcbPgdEdn = field_189_lcbPgdEdn;
    }
    
    public int getFcBkdEdn() {
        return this.field_190_fcBkdEdn;
    }
    
    public void setFcBkdEdn(final int field_190_fcBkdEdn) {
        this.field_190_fcBkdEdn = field_190_fcBkdEdn;
    }
    
    public int getLcbBkdEdn() {
        return this.field_191_lcbBkdEdn;
    }
    
    public void setLcbBkdEdn(final int field_191_lcbBkdEdn) {
        this.field_191_lcbBkdEdn = field_191_lcbBkdEdn;
    }
    
    public int getFcSttbfIntlFld() {
        return this.field_192_fcSttbfIntlFld;
    }
    
    public void setFcSttbfIntlFld(final int field_192_fcSttbfIntlFld) {
        this.field_192_fcSttbfIntlFld = field_192_fcSttbfIntlFld;
    }
    
    public int getLcbSttbfIntlFld() {
        return this.field_193_lcbSttbfIntlFld;
    }
    
    public void setLcbSttbfIntlFld(final int field_193_lcbSttbfIntlFld) {
        this.field_193_lcbSttbfIntlFld = field_193_lcbSttbfIntlFld;
    }
    
    public int getFcRouteSlip() {
        return this.field_194_fcRouteSlip;
    }
    
    public void setFcRouteSlip(final int field_194_fcRouteSlip) {
        this.field_194_fcRouteSlip = field_194_fcRouteSlip;
    }
    
    public int getLcbRouteSlip() {
        return this.field_195_lcbRouteSlip;
    }
    
    public void setLcbRouteSlip(final int field_195_lcbRouteSlip) {
        this.field_195_lcbRouteSlip = field_195_lcbRouteSlip;
    }
    
    public int getFcSttbSavedBy() {
        return this.field_196_fcSttbSavedBy;
    }
    
    public void setFcSttbSavedBy(final int field_196_fcSttbSavedBy) {
        this.field_196_fcSttbSavedBy = field_196_fcSttbSavedBy;
    }
    
    public int getLcbSttbSavedBy() {
        return this.field_197_lcbSttbSavedBy;
    }
    
    public void setLcbSttbSavedBy(final int field_197_lcbSttbSavedBy) {
        this.field_197_lcbSttbSavedBy = field_197_lcbSttbSavedBy;
    }
    
    public int getFcSttbFnm() {
        return this.field_198_fcSttbFnm;
    }
    
    public void setFcSttbFnm(final int field_198_fcSttbFnm) {
        this.field_198_fcSttbFnm = field_198_fcSttbFnm;
    }
    
    public int getLcbSttbFnm() {
        return this.field_199_lcbSttbFnm;
    }
    
    public void setLcbSttbFnm(final int field_199_lcbSttbFnm) {
        this.field_199_lcbSttbFnm = field_199_lcbSttbFnm;
    }
    
    public int getFcPlcfLst() {
        return this.field_200_fcPlcfLst;
    }
    
    public void setFcPlcfLst(final int field_200_fcPlcfLst) {
        this.field_200_fcPlcfLst = field_200_fcPlcfLst;
    }
    
    public int getLcbPlcfLst() {
        return this.field_201_lcbPlcfLst;
    }
    
    public void setLcbPlcfLst(final int field_201_lcbPlcfLst) {
        this.field_201_lcbPlcfLst = field_201_lcbPlcfLst;
    }
    
    public int getFcPlfLfo() {
        return this.field_202_fcPlfLfo;
    }
    
    public void setFcPlfLfo(final int field_202_fcPlfLfo) {
        this.field_202_fcPlfLfo = field_202_fcPlfLfo;
    }
    
    public int getLcbPlfLfo() {
        return this.field_203_lcbPlfLfo;
    }
    
    public void setLcbPlfLfo(final int field_203_lcbPlfLfo) {
        this.field_203_lcbPlfLfo = field_203_lcbPlfLfo;
    }
    
    public int getFcPlcftxbxBkd() {
        return this.field_204_fcPlcftxbxBkd;
    }
    
    public void setFcPlcftxbxBkd(final int field_204_fcPlcftxbxBkd) {
        this.field_204_fcPlcftxbxBkd = field_204_fcPlcftxbxBkd;
    }
    
    public int getLcbPlcftxbxBkd() {
        return this.field_205_lcbPlcftxbxBkd;
    }
    
    public void setLcbPlcftxbxBkd(final int field_205_lcbPlcftxbxBkd) {
        this.field_205_lcbPlcftxbxBkd = field_205_lcbPlcftxbxBkd;
    }
    
    public int getFcPlcftxbxHdrBkd() {
        return this.field_206_fcPlcftxbxHdrBkd;
    }
    
    public void setFcPlcftxbxHdrBkd(final int field_206_fcPlcftxbxHdrBkd) {
        this.field_206_fcPlcftxbxHdrBkd = field_206_fcPlcftxbxHdrBkd;
    }
    
    public int getLcbPlcftxbxHdrBkd() {
        return this.field_207_lcbPlcftxbxHdrBkd;
    }
    
    public void setLcbPlcftxbxHdrBkd(final int field_207_lcbPlcftxbxHdrBkd) {
        this.field_207_lcbPlcftxbxHdrBkd = field_207_lcbPlcftxbxHdrBkd;
    }
    
    public int getFcDocUndo() {
        return this.field_208_fcDocUndo;
    }
    
    public void setFcDocUndo(final int field_208_fcDocUndo) {
        this.field_208_fcDocUndo = field_208_fcDocUndo;
    }
    
    public int getLcbDocUndo() {
        return this.field_209_lcbDocUndo;
    }
    
    public void setLcbDocUndo(final int field_209_lcbDocUndo) {
        this.field_209_lcbDocUndo = field_209_lcbDocUndo;
    }
    
    public int getFcRgbuse() {
        return this.field_210_fcRgbuse;
    }
    
    public void setFcRgbuse(final int field_210_fcRgbuse) {
        this.field_210_fcRgbuse = field_210_fcRgbuse;
    }
    
    public int getLcbRgbuse() {
        return this.field_211_lcbRgbuse;
    }
    
    public void setLcbRgbuse(final int field_211_lcbRgbuse) {
        this.field_211_lcbRgbuse = field_211_lcbRgbuse;
    }
    
    public int getFcUsp() {
        return this.field_212_fcUsp;
    }
    
    public void setFcUsp(final int field_212_fcUsp) {
        this.field_212_fcUsp = field_212_fcUsp;
    }
    
    public int getLcbUsp() {
        return this.field_213_lcbUsp;
    }
    
    public void setLcbUsp(final int field_213_lcbUsp) {
        this.field_213_lcbUsp = field_213_lcbUsp;
    }
    
    public int getFcUskf() {
        return this.field_214_fcUskf;
    }
    
    public void setFcUskf(final int field_214_fcUskf) {
        this.field_214_fcUskf = field_214_fcUskf;
    }
    
    public int getLcbUskf() {
        return this.field_215_lcbUskf;
    }
    
    public void setLcbUskf(final int field_215_lcbUskf) {
        this.field_215_lcbUskf = field_215_lcbUskf;
    }
    
    public int getFcPlcupcRgbuse() {
        return this.field_216_fcPlcupcRgbuse;
    }
    
    public void setFcPlcupcRgbuse(final int field_216_fcPlcupcRgbuse) {
        this.field_216_fcPlcupcRgbuse = field_216_fcPlcupcRgbuse;
    }
    
    public int getLcbPlcupcRgbuse() {
        return this.field_217_lcbPlcupcRgbuse;
    }
    
    public void setLcbPlcupcRgbuse(final int field_217_lcbPlcupcRgbuse) {
        this.field_217_lcbPlcupcRgbuse = field_217_lcbPlcupcRgbuse;
    }
    
    public int getFcPlcupcUsp() {
        return this.field_218_fcPlcupcUsp;
    }
    
    public void setFcPlcupcUsp(final int field_218_fcPlcupcUsp) {
        this.field_218_fcPlcupcUsp = field_218_fcPlcupcUsp;
    }
    
    public int getLcbPlcupcUsp() {
        return this.field_219_lcbPlcupcUsp;
    }
    
    public void setLcbPlcupcUsp(final int field_219_lcbPlcupcUsp) {
        this.field_219_lcbPlcupcUsp = field_219_lcbPlcupcUsp;
    }
    
    public int getFcSttbGlsyStyle() {
        return this.field_220_fcSttbGlsyStyle;
    }
    
    public void setFcSttbGlsyStyle(final int field_220_fcSttbGlsyStyle) {
        this.field_220_fcSttbGlsyStyle = field_220_fcSttbGlsyStyle;
    }
    
    public int getLcbSttbGlsyStyle() {
        return this.field_221_lcbSttbGlsyStyle;
    }
    
    public void setLcbSttbGlsyStyle(final int field_221_lcbSttbGlsyStyle) {
        this.field_221_lcbSttbGlsyStyle = field_221_lcbSttbGlsyStyle;
    }
    
    public int getFcPlgosl() {
        return this.field_222_fcPlgosl;
    }
    
    public void setFcPlgosl(final int field_222_fcPlgosl) {
        this.field_222_fcPlgosl = field_222_fcPlgosl;
    }
    
    public int getLcbPlgosl() {
        return this.field_223_lcbPlgosl;
    }
    
    public void setLcbPlgosl(final int field_223_lcbPlgosl) {
        this.field_223_lcbPlgosl = field_223_lcbPlgosl;
    }
    
    public int getFcPlcocx() {
        return this.field_224_fcPlcocx;
    }
    
    public void setFcPlcocx(final int field_224_fcPlcocx) {
        this.field_224_fcPlcocx = field_224_fcPlcocx;
    }
    
    public int getLcbPlcocx() {
        return this.field_225_lcbPlcocx;
    }
    
    public void setLcbPlcocx(final int field_225_lcbPlcocx) {
        this.field_225_lcbPlcocx = field_225_lcbPlcocx;
    }
    
    public int getFcPlcfbteLvc() {
        return this.field_226_fcPlcfbteLvc;
    }
    
    public void setFcPlcfbteLvc(final int field_226_fcPlcfbteLvc) {
        this.field_226_fcPlcfbteLvc = field_226_fcPlcfbteLvc;
    }
    
    public int getLcbPlcfbteLvc() {
        return this.field_227_lcbPlcfbteLvc;
    }
    
    public void setLcbPlcfbteLvc(final int field_227_lcbPlcfbteLvc) {
        this.field_227_lcbPlcfbteLvc = field_227_lcbPlcfbteLvc;
    }
    
    public int getDwLowDateTime() {
        return this.field_228_dwLowDateTime;
    }
    
    public void setDwLowDateTime(final int field_228_dwLowDateTime) {
        this.field_228_dwLowDateTime = field_228_dwLowDateTime;
    }
    
    public int getDwHighDateTime() {
        return this.field_229_dwHighDateTime;
    }
    
    public void setDwHighDateTime(final int field_229_dwHighDateTime) {
        this.field_229_dwHighDateTime = field_229_dwHighDateTime;
    }
    
    public int getFcPlcflvc() {
        return this.field_230_fcPlcflvc;
    }
    
    public void setFcPlcflvc(final int field_230_fcPlcflvc) {
        this.field_230_fcPlcflvc = field_230_fcPlcflvc;
    }
    
    public int getLcbPlcflvc() {
        return this.field_231_lcbPlcflvc;
    }
    
    public void setLcbPlcflvc(final int field_231_lcbPlcflvc) {
        this.field_231_lcbPlcflvc = field_231_lcbPlcflvc;
    }
    
    public int getFcPlcasumy() {
        return this.field_232_fcPlcasumy;
    }
    
    public void setFcPlcasumy(final int field_232_fcPlcasumy) {
        this.field_232_fcPlcasumy = field_232_fcPlcasumy;
    }
    
    public int getLcbPlcasumy() {
        return this.field_233_lcbPlcasumy;
    }
    
    public void setLcbPlcasumy(final int field_233_lcbPlcasumy) {
        this.field_233_lcbPlcasumy = field_233_lcbPlcasumy;
    }
    
    public int getFcPlcfgram() {
        return this.field_234_fcPlcfgram;
    }
    
    public void setFcPlcfgram(final int field_234_fcPlcfgram) {
        this.field_234_fcPlcfgram = field_234_fcPlcfgram;
    }
    
    public int getLcbPlcfgram() {
        return this.field_235_lcbPlcfgram;
    }
    
    public void setLcbPlcfgram(final int field_235_lcbPlcfgram) {
        this.field_235_lcbPlcfgram = field_235_lcbPlcfgram;
    }
    
    public int getFcSttbListNames() {
        return this.field_236_fcSttbListNames;
    }
    
    public void setFcSttbListNames(final int field_236_fcSttbListNames) {
        this.field_236_fcSttbListNames = field_236_fcSttbListNames;
    }
    
    public int getLcbSttbListNames() {
        return this.field_237_lcbSttbListNames;
    }
    
    public void setLcbSttbListNames(final int field_237_lcbSttbListNames) {
        this.field_237_lcbSttbListNames = field_237_lcbSttbListNames;
    }
    
    public int getFcSttbfUssr() {
        return this.field_238_fcSttbfUssr;
    }
    
    public void setFcSttbfUssr(final int field_238_fcSttbfUssr) {
        this.field_238_fcSttbfUssr = field_238_fcSttbfUssr;
    }
    
    public int getLcbSttbfUssr() {
        return this.field_239_lcbSttbfUssr;
    }
    
    public void setLcbSttbfUssr(final int field_239_lcbSttbfUssr) {
        this.field_239_lcbSttbfUssr = field_239_lcbSttbfUssr;
    }
    
    public void setFDot(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fDot.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFDot() {
        return FIBAbstractType.fDot.isSet(this.field_6_options);
    }
    
    public void setFGlsy(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fGlsy.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFGlsy() {
        return FIBAbstractType.fGlsy.isSet(this.field_6_options);
    }
    
    public void setFComplex(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fComplex.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFComplex() {
        return FIBAbstractType.fComplex.isSet(this.field_6_options);
    }
    
    public void setFHasPic(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fHasPic.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFHasPic() {
        return FIBAbstractType.fHasPic.isSet(this.field_6_options);
    }
    
    public void setCQuickSaves(final byte value) {
        this.field_6_options = (short)FIBAbstractType.cQuickSaves.setValue(this.field_6_options, value);
    }
    
    public byte getCQuickSaves() {
        return (byte)FIBAbstractType.cQuickSaves.getValue(this.field_6_options);
    }
    
    public void setFEncrypted(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fEncrypted.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFEncrypted() {
        return FIBAbstractType.fEncrypted.isSet(this.field_6_options);
    }
    
    public void setFWhichTblStm(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fWhichTblStm.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFWhichTblStm() {
        return FIBAbstractType.fWhichTblStm.isSet(this.field_6_options);
    }
    
    public void setFReadOnlyRecommended(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fReadOnlyRecommended.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFReadOnlyRecommended() {
        return FIBAbstractType.fReadOnlyRecommended.isSet(this.field_6_options);
    }
    
    public void setFWriteReservation(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fWriteReservation.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFWriteReservation() {
        return FIBAbstractType.fWriteReservation.isSet(this.field_6_options);
    }
    
    public void setFExtChar(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fExtChar.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFExtChar() {
        return FIBAbstractType.fExtChar.isSet(this.field_6_options);
    }
    
    public void setFLoadOverride(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fLoadOverride.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFLoadOverride() {
        return FIBAbstractType.fLoadOverride.isSet(this.field_6_options);
    }
    
    public void setFFarEast(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fFarEast.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFFarEast() {
        return FIBAbstractType.fFarEast.isSet(this.field_6_options);
    }
    
    public void setFCrypto(final boolean value) {
        this.field_6_options = (short)FIBAbstractType.fCrypto.setBoolean(this.field_6_options, value);
    }
    
    public boolean isFCrypto() {
        return FIBAbstractType.fCrypto.isSet(this.field_6_options);
    }
    
    public void setFMac(final boolean value) {
        this.field_10_history = (short)FIBAbstractType.fMac.setBoolean(this.field_10_history, value);
    }
    
    public boolean isFMac() {
        return FIBAbstractType.fMac.isSet(this.field_10_history);
    }
    
    public void setFEmptySpecial(final boolean value) {
        this.field_10_history = (short)FIBAbstractType.fEmptySpecial.setBoolean(this.field_10_history, value);
    }
    
    public boolean isFEmptySpecial() {
        return FIBAbstractType.fEmptySpecial.isSet(this.field_10_history);
    }
    
    public void setFLoadOverridePage(final boolean value) {
        this.field_10_history = (short)FIBAbstractType.fLoadOverridePage.setBoolean(this.field_10_history, value);
    }
    
    public boolean isFLoadOverridePage() {
        return FIBAbstractType.fLoadOverridePage.isSet(this.field_10_history);
    }
    
    public void setFFutureSavedUndo(final boolean value) {
        this.field_10_history = (short)FIBAbstractType.fFutureSavedUndo.setBoolean(this.field_10_history, value);
    }
    
    public boolean isFFutureSavedUndo() {
        return FIBAbstractType.fFutureSavedUndo.isSet(this.field_10_history);
    }
    
    public void setFWord97Saved(final boolean value) {
        this.field_10_history = (short)FIBAbstractType.fWord97Saved.setBoolean(this.field_10_history, value);
    }
    
    public boolean isFWord97Saved() {
        return FIBAbstractType.fWord97Saved.isSet(this.field_10_history);
    }
    
    public void setFSpare0(final byte value) {
        this.field_10_history = (short)FIBAbstractType.fSpare0.setValue(this.field_10_history, value);
    }
    
    public byte getFSpare0() {
        return (byte)FIBAbstractType.fSpare0.getValue(this.field_10_history);
    }
    
    static {
        FIBAbstractType.fDot = BitFieldFactory.getInstance(1);
        FIBAbstractType.fGlsy = BitFieldFactory.getInstance(2);
        FIBAbstractType.fComplex = BitFieldFactory.getInstance(4);
        FIBAbstractType.fHasPic = BitFieldFactory.getInstance(8);
        FIBAbstractType.cQuickSaves = BitFieldFactory.getInstance(240);
        FIBAbstractType.fEncrypted = BitFieldFactory.getInstance(256);
        FIBAbstractType.fWhichTblStm = BitFieldFactory.getInstance(512);
        FIBAbstractType.fReadOnlyRecommended = BitFieldFactory.getInstance(1024);
        FIBAbstractType.fWriteReservation = BitFieldFactory.getInstance(2048);
        FIBAbstractType.fExtChar = BitFieldFactory.getInstance(4096);
        FIBAbstractType.fLoadOverride = BitFieldFactory.getInstance(8192);
        FIBAbstractType.fFarEast = BitFieldFactory.getInstance(16384);
        FIBAbstractType.fCrypto = BitFieldFactory.getInstance(32768);
        FIBAbstractType.fMac = BitFieldFactory.getInstance(1);
        FIBAbstractType.fEmptySpecial = BitFieldFactory.getInstance(2);
        FIBAbstractType.fLoadOverridePage = BitFieldFactory.getInstance(4);
        FIBAbstractType.fFutureSavedUndo = BitFieldFactory.getInstance(8);
        FIBAbstractType.fWord97Saved = BitFieldFactory.getInstance(16);
        FIBAbstractType.fSpare0 = BitFieldFactory.getInstance(254);
    }
}
