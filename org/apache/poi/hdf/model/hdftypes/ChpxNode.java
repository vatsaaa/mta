// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public final class ChpxNode extends PropertyNode
{
    public ChpxNode(final int fcStart, final int fcEnd, final byte[] chpx) {
        super(fcStart, fcEnd, chpx);
    }
    
    public byte[] getChpx() {
        return super.getGrpprl();
    }
}
