// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.util.LittleEndian;

@Deprecated
public final class PAPFormattedDiskPage extends FormattedDiskPage
{
    public PAPFormattedDiskPage(final byte[] fkp) {
        super(fkp);
    }
    
    @Override
    public byte[] getGrpprl(final int index) {
        int papxOffset = 2 * LittleEndian.getUnsignedByte(this._fkp, (this._crun + 1) * 4 + index * 13);
        int size = 2 * LittleEndian.getUnsignedByte(this._fkp, papxOffset);
        if (size == 0) {
            size = 2 * LittleEndian.getUnsignedByte(this._fkp, ++papxOffset);
        }
        else {
            --size;
        }
        final byte[] papx = new byte[size];
        System.arraycopy(this._fkp, ++papxOffset, papx, 0, size);
        return papx;
    }
}
