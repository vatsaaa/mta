// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public final class TextPiece extends PropertyNode implements Comparable
{
    private boolean _usesUnicode;
    private int _length;
    
    public TextPiece(final int start, final int length, final boolean unicode) {
        super(start, start + length, null);
        this._usesUnicode = unicode;
        this._length = length;
    }
    
    public boolean usesUnicode() {
        return this._usesUnicode;
    }
}
