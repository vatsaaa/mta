// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public abstract class PropertyNode implements Comparable
{
    private byte[] _grpprl;
    private int _fcStart;
    private int _fcEnd;
    
    public PropertyNode(final int fcStart, final int fcEnd, final byte[] grpprl) {
        this._fcStart = fcStart;
        this._fcEnd = fcEnd;
        this._grpprl = grpprl;
    }
    
    public int getStart() {
        return this._fcStart;
    }
    
    public int getEnd() {
        return this._fcEnd;
    }
    
    protected byte[] getGrpprl() {
        return this._grpprl;
    }
    
    public int compareTo(final Object o) {
        final int fcEnd = ((PropertyNode)o).getEnd();
        if (this._fcEnd == fcEnd) {
            return 0;
        }
        if (this._fcEnd < fcEnd) {
            return -1;
        }
        return 1;
    }
}
