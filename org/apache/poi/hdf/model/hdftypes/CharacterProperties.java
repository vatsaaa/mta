// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.hdf.model.hdftypes.definitions.CHPAbstractType;

@Deprecated
public final class CharacterProperties extends CHPAbstractType implements Cloneable
{
    public CharacterProperties() {
        this.setDttmRMark(new short[2]);
        this.setDttmRMarkDel(new short[2]);
        this.setXstDispFldRMark(new byte[32]);
        this.setBrc(new short[2]);
        this.setHps(20);
        this.setFcPic(-1);
        this.setIstd(10);
        this.setLidFE(1024);
        this.setLidDefault(1024);
        this.setWCharScale(100);
    }
    
    public Object clone() throws CloneNotSupportedException {
        final CharacterProperties clone = (CharacterProperties)super.clone();
        clone.setBrc(new short[2]);
        System.arraycopy(this.getBrc(), 0, clone.getBrc(), 0, 2);
        System.arraycopy(this.getDttmRMark(), 0, clone.getDttmRMark(), 0, 2);
        System.arraycopy(this.getDttmRMarkDel(), 0, clone.getDttmRMarkDel(), 0, 2);
        System.arraycopy(this.getXstDispFldRMark(), 0, clone.getXstDispFldRMark(), 0, 32);
        return clone;
    }
}
