// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public final class PlexOfCps
{
    private int _count;
    private int _offset;
    private int _sizeOfStruct;
    
    public PlexOfCps(final int size, final int sizeOfStruct) {
        this._count = (size - 4) / (4 + sizeOfStruct);
        this._sizeOfStruct = sizeOfStruct;
    }
    
    public int getIntOffset(final int index) {
        return index * 4;
    }
    
    public int length() {
        return this._count;
    }
    
    public int getStructOffset(final int index) {
        return 4 * (this._count + 1) + this._sizeOfStruct * index;
    }
}
