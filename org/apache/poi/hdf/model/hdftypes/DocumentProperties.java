// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.util.LittleEndian;

@Deprecated
public final class DocumentProperties implements HDFType
{
    public boolean _fFacingPages;
    public int _fpc;
    public int _epc;
    public int _rncFtn;
    public int _nFtn;
    public int _rncEdn;
    public int _nEdn;
    
    public DocumentProperties(final byte[] dopArray) {
        this._fFacingPages = ((dopArray[0] & 0x1) > 0);
        this._fpc = (dopArray[0] & 0x60) >> 5;
        short num = LittleEndian.getShort(dopArray, 2);
        this._rncFtn = (num & 0x3);
        this._nFtn = (short)(num & 0xFFFC) >> 2;
        num = LittleEndian.getShort(dopArray, 52);
        this._rncEdn = (num & 0x3);
        this._nEdn = (short)(num & 0xFFFC) >> 2;
        num = LittleEndian.getShort(dopArray, 54);
        this._epc = (num & 0x3);
    }
}
