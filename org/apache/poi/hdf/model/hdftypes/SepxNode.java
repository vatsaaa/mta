// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public final class SepxNode extends PropertyNode
{
    int _index;
    
    public SepxNode(final int index, final int start, final int end, final byte[] sepx) {
        super(start, end, sepx);
    }
    
    public byte[] getSepx() {
        return this.getGrpprl();
    }
    
    @Override
    public int compareTo(final Object obj) {
        return 0;
    }
}
