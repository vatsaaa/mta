// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.util.LittleEndian;

@Deprecated
public final class FontTable implements HDFType
{
    String[] fontNames;
    
    public FontTable(final byte[] fontTable) {
        final int size = LittleEndian.getShort(fontTable, 0);
        this.fontNames = new String[size];
        int currentIndex = 4;
        for (int x = 0; x < size; ++x) {
            final byte ffnLength = fontTable[currentIndex];
            int nameOffset = currentIndex + 40;
            final StringBuffer nameBuf = new StringBuffer();
            for (char ch = (char)LittleEndian.getShort(fontTable, nameOffset); ch != '\0'; ch = (char)LittleEndian.getShort(fontTable, nameOffset)) {
                nameBuf.append(ch);
                nameOffset += 2;
            }
            this.fontNames[x] = nameBuf.toString();
            currentIndex += ffnLength + 1;
        }
    }
    
    public String getFont(final int index) {
        return this.fontNames[index];
    }
}
