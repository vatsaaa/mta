// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.util.LittleEndian;

@Deprecated
public final class CHPFormattedDiskPage extends FormattedDiskPage
{
    public CHPFormattedDiskPage(final byte[] fkp) {
        super(fkp);
    }
    
    @Override
    public byte[] getGrpprl(final int index) {
        int chpxOffset = 2 * LittleEndian.getUnsignedByte(this._fkp, (this._crun + 1) * 4 + index);
        if (chpxOffset == 0) {
            return new byte[0];
        }
        final int size = LittleEndian.getUnsignedByte(this._fkp, chpxOffset);
        final byte[] chpx = new byte[size];
        System.arraycopy(this._fkp, ++chpxOffset, chpx, 0, size);
        return chpx;
    }
}
