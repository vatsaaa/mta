// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.hdf.model.hdftypes.definitions.TCAbstractType;

@Deprecated
public final class TableCellDescriptor extends TCAbstractType implements HDFType
{
    static TableCellDescriptor convertBytesToTC(final byte[] array, final int offset) {
        final TableCellDescriptor tc = new TableCellDescriptor();
        final int rgf = LittleEndian.getShort(array, offset);
        tc.setFFirstMerged((rgf & 0x1) > 0);
        tc.setFMerged((rgf & 0x2) > 0);
        tc.setFVertical((rgf & 0x4) > 0);
        tc.setFBackward((rgf & 0x8) > 0);
        tc.setFRotateFont((rgf & 0x10) > 0);
        tc.setFVertMerge((rgf & 0x20) > 0);
        tc.setFVertRestart((rgf & 0x40) > 0);
        tc.setVertAlign((byte)((rgf & 0x180) >> 7));
        final short[] brcTop = new short[2];
        final short[] brcLeft = new short[2];
        final short[] brcBottom = new short[2];
        final short[] brcRight = new short[2];
        brcTop[0] = LittleEndian.getShort(array, offset + 4);
        brcTop[1] = LittleEndian.getShort(array, offset + 6);
        brcLeft[0] = LittleEndian.getShort(array, offset + 8);
        brcLeft[1] = LittleEndian.getShort(array, offset + 10);
        brcBottom[0] = LittleEndian.getShort(array, offset + 12);
        brcBottom[1] = LittleEndian.getShort(array, offset + 14);
        brcRight[0] = LittleEndian.getShort(array, offset + 16);
        brcRight[1] = LittleEndian.getShort(array, offset + 18);
        return tc;
    }
}
