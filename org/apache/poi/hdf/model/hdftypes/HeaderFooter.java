// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public final class HeaderFooter
{
    public static final int HEADER_EVEN = 1;
    public static final int HEADER_ODD = 2;
    public static final int FOOTER_EVEN = 3;
    public static final int FOOTER_ODD = 4;
    public static final int HEADER_FIRST = 5;
    public static final int FOOTER_FIRST = 6;
    private int _type;
    private int _start;
    private int _end;
    
    public HeaderFooter(final int type, final int startFC, final int endFC) {
        this._type = type;
        this._start = startFC;
        this._end = endFC;
    }
    
    public int getStart() {
        return this._start;
    }
    
    public int getEnd() {
        return this._end;
    }
    
    public boolean isEmpty() {
        return this._start - this._end == 0;
    }
}
