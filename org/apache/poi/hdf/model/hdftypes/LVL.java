// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public final class LVL
{
    public int _iStartAt;
    public byte _nfc;
    byte _jc;
    boolean _fLegal;
    boolean _fNoRestart;
    boolean _fPrev;
    boolean _fPrevSpace;
    boolean _fWord6;
    public byte[] _rgbxchNums;
    public byte _ixchFollow;
    public int _dxaSpace;
    public int _dxaIndent;
    public byte[] _chpx;
    public byte[] _papx;
    public char[] _xst;
    public short _istd;
    
    public LVL() {
        this._rgbxchNums = new byte[9];
    }
    
    public Object clone() {
        LVL obj = null;
        try {
            obj = (LVL)super.clone();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
}
