// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.hdf.model.hdftypes.definitions.TCAbstractType;
import org.apache.poi.util.LittleEndian;

@Deprecated
public final class StyleSheet implements HDFType
{
    private static final int NIL_STYLE = 4095;
    private static final int PAP_TYPE = 1;
    private static final int CHP_TYPE = 2;
    private static final int SEP_TYPE = 4;
    private static final int TAP_TYPE = 5;
    StyleDescription _nilStyle;
    StyleDescription[] _styleDescriptions;
    
    public StyleSheet(final byte[] styleSheet) {
        this._nilStyle = new StyleDescription();
        final int stshiLength = LittleEndian.getShort(styleSheet, 0);
        final int stdCount = LittleEndian.getShort(styleSheet, 2);
        final int baseLength = LittleEndian.getShort(styleSheet, 4);
        final int[] rgftc = { LittleEndian.getInt(styleSheet, 14), LittleEndian.getInt(styleSheet, 18), LittleEndian.getInt(styleSheet, 22) };
        int offset = 0;
        this._styleDescriptions = new StyleDescription[stdCount];
        for (int x = 0; x < stdCount; ++x) {
            int stdOffset = 2 + stshiLength + offset;
            final int stdSize = LittleEndian.getShort(styleSheet, stdOffset);
            if (stdSize > 0) {
                final byte[] std = new byte[stdSize];
                stdOffset += 2;
                System.arraycopy(styleSheet, stdOffset, std, 0, stdSize);
                final StyleDescription aStyle = new StyleDescription(std, baseLength, true);
                this._styleDescriptions[x] = aStyle;
            }
            offset += stdSize + 2;
        }
        for (int x = 0; x < this._styleDescriptions.length; ++x) {
            if (this._styleDescriptions[x] != null) {
                this.createPap(x);
                this.createChp(x);
            }
        }
    }
    
    private void createPap(final int istd) {
        final StyleDescription sd = this._styleDescriptions[istd];
        ParagraphProperties pap = sd.getPAP();
        final byte[] papx = sd.getPAPX();
        final int baseIndex = sd.getBaseStyle();
        if (pap == null && papx != null) {
            ParagraphProperties parentPAP = this._nilStyle.getPAP();
            if (baseIndex != 4095) {
                parentPAP = this._styleDescriptions[baseIndex].getPAP();
                if (parentPAP == null) {
                    this.createPap(baseIndex);
                    parentPAP = this._styleDescriptions[baseIndex].getPAP();
                }
            }
            pap = (ParagraphProperties)uncompressProperty(papx, parentPAP, this);
            sd.setPAP(pap);
        }
    }
    
    private void createChp(final int istd) {
        final StyleDescription sd = this._styleDescriptions[istd];
        CharacterProperties chp = sd.getCHP();
        final byte[] chpx = sd.getCHPX();
        final int baseIndex = sd.getBaseStyle();
        if (chp == null && chpx != null) {
            CharacterProperties parentCHP = this._nilStyle.getCHP();
            if (baseIndex != 4095) {
                parentCHP = this._styleDescriptions[baseIndex].getCHP();
                if (parentCHP == null) {
                    this.createChp(baseIndex);
                    parentCHP = this._styleDescriptions[baseIndex].getCHP();
                }
            }
            chp = (CharacterProperties)uncompressProperty(chpx, parentCHP, this);
            sd.setCHP(chp);
        }
    }
    
    public StyleDescription getStyleDescription(final int x) {
        return this._styleDescriptions[x];
    }
    
    static void doCHPOperation(final CharacterProperties oldCHP, CharacterProperties newCHP, final int operand, final int param, final byte[] varParam, final byte[] grpprl, final int offset, final StyleSheet styleSheet) {
        switch (operand) {
            case 0: {
                newCHP.setFRMarkDel(getFlag(param));
                break;
            }
            case 1: {
                newCHP.setFRMark(getFlag(param));
            }
            case 3: {
                newCHP.setFcPic(param);
                newCHP.setFSpec(true);
                break;
            }
            case 4: {
                newCHP.setIbstRMark((short)param);
                break;
            }
            case 5: {
                final short[] dttmRMark = { LittleEndian.getShort(grpprl, offset - 4), LittleEndian.getShort(grpprl, offset - 2) };
                newCHP.setDttmRMark(dttmRMark);
                break;
            }
            case 6: {
                newCHP.setFData(getFlag(param));
            }
            case 8: {
                final short chsDiff = (short)((param & 0xFF0000) >>> 8);
                newCHP.setFChsDiff(getFlag(chsDiff));
                newCHP.setChse((short)(param & 0xFFFF));
                break;
            }
            case 9: {
                newCHP.setFSpec(true);
                newCHP.setFtcSym(LittleEndian.getShort(varParam, 0));
                newCHP.setXchSym(LittleEndian.getShort(varParam, 2));
                break;
            }
            case 10: {
                newCHP.setFOle2(getFlag(param));
            }
            case 12: {
                newCHP.setIcoHighlight((byte)param);
                newCHP.setFHighlight(getFlag(param));
            }
            case 14: {
                newCHP.setFcObj(param);
            }
            case 15: {}
            case 16: {}
            case 17: {}
            case 18: {}
            case 19: {}
            case 20: {}
            case 21: {}
            case 22: {}
            case 23: {}
            case 24: {}
            case 25: {}
            case 26: {}
            case 27: {}
            case 28: {}
            case 29: {}
            case 30: {}
            case 31: {}
            case 32: {}
            case 33: {}
            case 34: {}
            case 35: {}
            case 36: {}
            case 37: {}
            case 38: {}
            case 39: {}
            case 40: {}
            case 41: {}
            case 42: {}
            case 43: {}
            case 44: {}
            case 45: {}
            case 46: {}
            case 48: {
                newCHP.setIstd(param);
            }
            case 50: {
                newCHP.setFBold(false);
                newCHP.setFItalic(false);
                newCHP.setFOutline(false);
                newCHP.setFStrike(false);
                newCHP.setFShadow(false);
                newCHP.setFSmallCaps(false);
                newCHP.setFCaps(false);
                newCHP.setFVanish(false);
                newCHP.setKul((byte)0);
                newCHP.setIco((byte)0);
                break;
            }
            case 51: {
                try {
                    newCHP = (CharacterProperties)oldCHP.clone();
                }
                catch (CloneNotSupportedException ex) {}
            }
            case 53: {
                newCHP.setFBold(getCHPFlag((byte)param, oldCHP.isFBold()));
                break;
            }
            case 54: {
                newCHP.setFItalic(getCHPFlag((byte)param, oldCHP.isFItalic()));
                break;
            }
            case 55: {
                newCHP.setFStrike(getCHPFlag((byte)param, oldCHP.isFStrike()));
                break;
            }
            case 56: {
                newCHP.setFOutline(getCHPFlag((byte)param, oldCHP.isFOutline()));
                break;
            }
            case 57: {
                newCHP.setFShadow(getCHPFlag((byte)param, oldCHP.isFShadow()));
                break;
            }
            case 58: {
                newCHP.setFSmallCaps(getCHPFlag((byte)param, oldCHP.isFSmallCaps()));
                break;
            }
            case 59: {
                newCHP.setFCaps(getCHPFlag((byte)param, oldCHP.isFCaps()));
                break;
            }
            case 60: {
                newCHP.setFVanish(getCHPFlag((byte)param, oldCHP.isFVanish()));
                break;
            }
            case 61: {
                newCHP.setFtcAscii((short)param);
                break;
            }
            case 62: {
                newCHP.setKul((byte)param);
                break;
            }
            case 63: {
                final int hps = param & 0xFF;
                if (hps != 0) {
                    newCHP.setHps(hps);
                }
                final byte cInc = (byte)((byte)(param & 0xFE00) >>> 4 >> 1);
                if (cInc != 0) {
                    newCHP.setHps(Math.max(newCHP.getHps() + cInc * 2, 2));
                }
                final byte hpsPos = (byte)((param & 0xFF0000) >>> 8);
                if (hpsPos != 128) {
                    newCHP.setHpsPos(hpsPos);
                }
                final boolean fAdjust = (param & 0x100) > 0;
                if (fAdjust && hpsPos != 128 && hpsPos != 0 && oldCHP.getHpsPos() == 0) {
                    newCHP.setHps(Math.max(newCHP.getHps() - 2, 2));
                }
                if (fAdjust && hpsPos == 0 && oldCHP.getHpsPos() != 0) {
                    newCHP.setHps(Math.max(newCHP.getHps() + 2, 2));
                    break;
                }
                break;
            }
            case 64: {
                newCHP.setDxaSpace(param);
                break;
            }
            case 65: {
                newCHP.setLidDefault((short)param);
                break;
            }
            case 66: {
                newCHP.setIco((byte)param);
                break;
            }
            case 67: {
                newCHP.setHps(param);
                break;
            }
            case 68: {
                final byte hpsLvl = (byte)param;
                newCHP.setHps(Math.max(newCHP.getHps() + hpsLvl * 2, 2));
                break;
            }
            case 69: {
                newCHP.setHpsPos((short)param);
                break;
            }
            case 70: {
                if (param != 0) {
                    if (oldCHP.getHpsPos() == 0) {
                        newCHP.setHps(Math.max(newCHP.getHps() - 2, 2));
                        break;
                    }
                    break;
                }
                else {
                    if (oldCHP.getHpsPos() != 0) {
                        newCHP.setHps(Math.max(newCHP.getHps() + 2, 2));
                        break;
                    }
                    break;
                }
                break;
            }
            case 71: {
                CharacterProperties genCHP = new CharacterProperties();
                genCHP.setFtcAscii(4);
                genCHP = (CharacterProperties)uncompressProperty(varParam, genCHP, styleSheet);
                final CharacterProperties styleCHP = styleSheet.getStyleDescription(oldCHP.getBaseIstd()).getCHP();
                if (genCHP.isFBold() == newCHP.isFBold()) {
                    newCHP.setFBold(styleCHP.isFBold());
                }
                if (genCHP.isFItalic() == newCHP.isFItalic()) {
                    newCHP.setFItalic(styleCHP.isFItalic());
                }
                if (genCHP.isFSmallCaps() == newCHP.isFSmallCaps()) {
                    newCHP.setFSmallCaps(styleCHP.isFSmallCaps());
                }
                if (genCHP.isFVanish() == newCHP.isFVanish()) {
                    newCHP.setFVanish(styleCHP.isFVanish());
                }
                if (genCHP.isFStrike() == newCHP.isFStrike()) {
                    newCHP.setFStrike(styleCHP.isFStrike());
                }
                if (genCHP.isFCaps() == newCHP.isFCaps()) {
                    newCHP.setFCaps(styleCHP.isFCaps());
                }
                if (genCHP.getFtcAscii() == newCHP.getFtcAscii()) {
                    newCHP.setFtcAscii(styleCHP.getFtcAscii());
                }
                if (genCHP.getFtcFE() == newCHP.getFtcFE()) {
                    newCHP.setFtcFE(styleCHP.getFtcFE());
                }
                if (genCHP.getFtcOther() == newCHP.getFtcOther()) {
                    newCHP.setFtcOther(styleCHP.getFtcOther());
                }
                if (genCHP.getHps() == newCHP.getHps()) {
                    newCHP.setHps(styleCHP.getHps());
                }
                if (genCHP.getHpsPos() == newCHP.getHpsPos()) {
                    newCHP.setHpsPos(styleCHP.getHpsPos());
                }
                if (genCHP.getKul() == newCHP.getKul()) {
                    newCHP.setKul(styleCHP.getKul());
                }
                if (genCHP.getDxaSpace() == newCHP.getDxaSpace()) {
                    newCHP.setDxaSpace(styleCHP.getDxaSpace());
                }
                if (genCHP.getIco() == newCHP.getIco()) {
                    newCHP.setIco(styleCHP.getIco());
                }
                if (genCHP.getLidDefault() == newCHP.getLidDefault()) {
                    newCHP.setLidDefault(styleCHP.getLidDefault());
                }
                if (genCHP.getLidFE() == newCHP.getLidFE()) {
                    newCHP.setLidFE(styleCHP.getLidFE());
                    break;
                }
                break;
            }
            case 72: {
                newCHP.setIss((byte)param);
                break;
            }
            case 73: {
                newCHP.setHps(LittleEndian.getShort(varParam, 0));
                break;
            }
            case 74: {
                final int increment = LittleEndian.getShort(varParam, 0);
                newCHP.setHps(Math.max(newCHP.getHps() + increment, 8));
                break;
            }
            case 75: {
                newCHP.setHpsKern(param);
                break;
            }
            case 76: {
                doCHPOperation(oldCHP, newCHP, 71, param, varParam, grpprl, offset, styleSheet);
                break;
            }
            case 77: {
                final float percentage = param / 100.0f;
                final int add = (int)(percentage * newCHP.getHps());
                newCHP.setHps(newCHP.getHps() + add);
                break;
            }
            case 78: {
                newCHP.setYsr((byte)param);
                break;
            }
            case 79: {
                newCHP.setFtcAscii((short)param);
                break;
            }
            case 80: {
                newCHP.setFtcFE((short)param);
                break;
            }
            case 81: {
                newCHP.setFtcOther((short)param);
            }
            case 83: {
                newCHP.setFDStrike(getFlag(param));
                break;
            }
            case 84: {
                newCHP.setFImprint(getFlag(param));
                break;
            }
            case 85: {
                newCHP.setFSpec(getFlag(param));
                break;
            }
            case 86: {
                newCHP.setFObj(getFlag(param));
                break;
            }
            case 87: {
                newCHP.setFPropMark(varParam[0]);
                newCHP.setIbstPropRMark(LittleEndian.getShort(varParam, 1));
                newCHP.setDttmPropRMark(LittleEndian.getInt(varParam, 3));
                break;
            }
            case 88: {
                newCHP.setFEmboss(getFlag(param));
                break;
            }
            case 89: {
                newCHP.setSfxtText((byte)param);
            }
            case 90: {}
            case 91: {}
            case 92: {}
            case 93: {}
            case 94: {}
            case 95: {}
            case 96: {}
            case 98: {
                final byte[] xstDispFldRMark = new byte[32];
                newCHP.setFDispFldRMark(varParam[0]);
                newCHP.setIbstDispFldRMark(LittleEndian.getShort(varParam, 1));
                newCHP.setDttmDispFldRMark(LittleEndian.getInt(varParam, 3));
                System.arraycopy(varParam, 7, xstDispFldRMark, 0, 32);
                newCHP.setXstDispFldRMark(xstDispFldRMark);
                break;
            }
            case 99: {
                newCHP.setIbstRMarkDel((short)param);
                break;
            }
            case 100: {
                final short[] dttmRMarkDel = { LittleEndian.getShort(grpprl, offset - 4), LittleEndian.getShort(grpprl, offset - 2) };
                newCHP.setDttmRMarkDel(dttmRMarkDel);
                break;
            }
            case 101: {
                final short[] brc = { LittleEndian.getShort(grpprl, offset - 4), LittleEndian.getShort(grpprl, offset - 2) };
                newCHP.setBrc(brc);
                break;
            }
            case 102: {
                newCHP.setShd((short)param);
            }
            case 103: {}
            case 104: {}
            case 105: {}
            case 106: {}
            case 107: {}
            case 109: {
                newCHP.setLidDefault((short)param);
                break;
            }
            case 110: {
                newCHP.setLidFE((short)param);
                break;
            }
            case 111: {
                newCHP.setIdctHint((byte)param);
                break;
            }
        }
    }
    
    public static Object uncompressProperty(final byte[] grpprl, final Object parent, final StyleSheet styleSheet) {
        return uncompressProperty(grpprl, parent, styleSheet, true);
    }
    
    public static Object uncompressProperty(final byte[] grpprl, final Object parent, final StyleSheet styleSheet, final boolean doIstd) {
        Object newProperty = null;
        int offset = 0;
        int propertyType = 1;
        if (parent instanceof ParagraphProperties) {
            try {
                newProperty = ((ParagraphProperties)parent).clone();
            }
            catch (Exception ex) {}
            if (doIstd) {
                ((ParagraphProperties)newProperty).setIstd(LittleEndian.getShort(grpprl, 0));
                offset = 2;
            }
        }
        else if (parent instanceof CharacterProperties) {
            try {
                newProperty = ((CharacterProperties)parent).clone();
                ((CharacterProperties)newProperty).setBaseIstd(((CharacterProperties)parent).getIstd());
            }
            catch (Exception ex2) {}
            propertyType = 2;
        }
        else if (parent instanceof SectionProperties) {
            newProperty = parent;
            propertyType = 4;
        }
        else {
            if (!(parent instanceof TableProperties)) {
                return null;
            }
            newProperty = parent;
            propertyType = 5;
            offset = 2;
        }
        while (offset < grpprl.length) {
            final short sprm = LittleEndian.getShort(grpprl, offset);
            offset += 2;
            final byte spra = (byte)((sprm & 0xE000) >> 13);
            int opSize = 0;
            int param = 0;
            byte[] varParam = null;
            switch (spra) {
                case 0:
                case 1: {
                    opSize = 1;
                    param = grpprl[offset];
                    break;
                }
                case 2: {
                    opSize = 2;
                    param = LittleEndian.getShort(grpprl, offset);
                    break;
                }
                case 3: {
                    opSize = 4;
                    param = LittleEndian.getInt(grpprl, offset);
                    break;
                }
                case 4:
                case 5: {
                    opSize = 2;
                    param = LittleEndian.getShort(grpprl, offset);
                    break;
                }
                case 6: {
                    if (sprm != -10744) {
                        opSize = LittleEndian.getUnsignedByte(grpprl, offset);
                        ++offset;
                    }
                    else {
                        opSize = LittleEndian.getShort(grpprl, offset) - 1;
                        offset += 2;
                    }
                    varParam = new byte[opSize];
                    System.arraycopy(grpprl, offset, varParam, 0, opSize);
                    break;
                }
                case 7: {
                    opSize = 3;
                    final byte[] threeByteInt = { grpprl[offset], grpprl[offset + 1], grpprl[offset + 2], 0 };
                    param = LittleEndian.getInt(threeByteInt, 0);
                    break;
                }
                default: {
                    throw new RuntimeException("unrecognized pap opcode");
                }
            }
            offset += opSize;
            final short operand = (short)(sprm & 0x1FF);
            final byte type = (byte)((sprm & 0x1C00) >> 10);
            switch (propertyType) {
                case 1: {
                    if (type == 1) {
                        doPAPOperation((ParagraphProperties)newProperty, operand, param, varParam, grpprl, offset, spra);
                        continue;
                    }
                    continue;
                }
                case 2: {
                    doCHPOperation((CharacterProperties)parent, (CharacterProperties)newProperty, operand, param, varParam, grpprl, offset, styleSheet);
                    continue;
                }
                case 4: {
                    doSEPOperation((SectionProperties)newProperty, operand, param, varParam);
                    continue;
                }
                case 5: {
                    if (type == 5) {
                        doTAPOperation((TableProperties)newProperty, operand, param, varParam);
                        continue;
                    }
                    continue;
                }
            }
        }
        return newProperty;
    }
    
    static void doPAPOperation(final ParagraphProperties newPAP, final int operand, final int param, final byte[] varParam, final byte[] grpprl, final int offset, final int spra) {
        switch (operand) {
            case 0: {
                newPAP.setIstd(param);
            }
            case 2: {
                if (newPAP.getIstd() > 9 && newPAP.getIstd() < 1) {
                    break;
                }
                newPAP.setIstd(newPAP.getIstd() + param);
                if (param > 0) {
                    newPAP.setIstd(Math.max(newPAP.getIstd(), 9));
                    break;
                }
                newPAP.setIstd(Math.min(newPAP.getIstd(), 1));
                break;
            }
            case 3: {
                newPAP.setJc((byte)param);
                break;
            }
            case 4: {
                newPAP.setFSideBySide((byte)param);
                break;
            }
            case 5: {
                newPAP.setFKeep((byte)param);
                break;
            }
            case 6: {
                newPAP.setFKeepFollow((byte)param);
                break;
            }
            case 7: {
                newPAP.setFPageBreakBefore((byte)param);
                break;
            }
            case 8: {
                newPAP.setBrcl((byte)param);
                break;
            }
            case 9: {
                newPAP.setBrcp((byte)param);
                break;
            }
            case 10: {
                newPAP.setIlvl((byte)param);
                break;
            }
            case 11: {
                newPAP.setIlfo(param);
                break;
            }
            case 12: {
                newPAP.setFNoLnn((byte)param);
            }
            case 14: {
                newPAP.setDxaRight(param);
                break;
            }
            case 15: {
                newPAP.setDxaLeft(param);
                break;
            }
            case 16: {
                newPAP.setDxaLeft(newPAP.getDxaLeft() + param);
                newPAP.setDxaLeft(Math.max(0, newPAP.getDxaLeft()));
                break;
            }
            case 17: {
                newPAP.setDxaLeft1(param);
                break;
            }
            case 18: {
                final short[] lspd = newPAP.getLspd();
                lspd[0] = LittleEndian.getShort(grpprl, offset - 4);
                lspd[1] = LittleEndian.getShort(grpprl, offset - 2);
                break;
            }
            case 19: {
                newPAP.setDyaBefore(param);
                break;
            }
            case 20: {
                newPAP.setDyaAfter(param);
            }
            case 22: {
                newPAP.setFInTable((byte)param);
                break;
            }
            case 23: {
                newPAP.setFTtp((byte)param);
                break;
            }
            case 24: {
                newPAP.setDxaAbs(param);
                break;
            }
            case 25: {
                newPAP.setDyaAbs(param);
                break;
            }
            case 26: {
                newPAP.setDxaWidth(param);
            }
            case 27: {}
            case 28: {}
            case 29: {}
            case 30: {}
            case 31: {}
            case 32: {}
            case 34: {
                newPAP.setDxaFromText(param);
                break;
            }
            case 35: {
                newPAP.setWr((byte)param);
                break;
            }
            case 36: {
                final short[] brcTop = newPAP.getBrcTop();
                brcTop[0] = LittleEndian.getShort(grpprl, offset - 4);
                brcTop[1] = LittleEndian.getShort(grpprl, offset - 2);
                break;
            }
            case 37: {
                final short[] brcLeft = newPAP.getBrcLeft();
                brcLeft[0] = LittleEndian.getShort(grpprl, offset - 4);
                brcLeft[1] = LittleEndian.getShort(grpprl, offset - 2);
                break;
            }
            case 38: {
                final short[] brcBottom = newPAP.getBrcBottom();
                brcBottom[0] = LittleEndian.getShort(grpprl, offset - 4);
                brcBottom[1] = LittleEndian.getShort(grpprl, offset - 2);
                break;
            }
            case 39: {
                final short[] brcRight = newPAP.getBrcRight();
                brcRight[0] = LittleEndian.getShort(grpprl, offset - 4);
                brcRight[1] = LittleEndian.getShort(grpprl, offset - 2);
                break;
            }
            case 40: {
                final short[] brcBetween = newPAP.getBrcBetween();
                brcBetween[0] = LittleEndian.getShort(grpprl, offset - 4);
                brcBetween[1] = LittleEndian.getShort(grpprl, offset - 2);
                break;
            }
            case 41: {
                final short[] brcBar = newPAP.getBrcBar();
                brcBar[0] = LittleEndian.getShort(grpprl, offset - 4);
                brcBar[1] = LittleEndian.getShort(grpprl, offset - 2);
                break;
            }
            case 42: {
                newPAP.setFNoAutoHyph((byte)param);
                break;
            }
            case 43: {
                newPAP.setDyaHeight(param);
                break;
            }
            case 44: {
                newPAP.setDcs((short)param);
                break;
            }
            case 45: {
                newPAP.setShd((short)param);
                break;
            }
            case 46: {
                newPAP.setDyaFromText(param);
                break;
            }
            case 47: {
                newPAP.setDxaFromText(param);
                break;
            }
            case 48: {
                newPAP.setFLocked((byte)param);
                break;
            }
            case 49: {
                newPAP.setFWidowControl((byte)param);
            }
            case 51: {
                newPAP.setFKinsoku((byte)param);
                break;
            }
            case 52: {
                newPAP.setFWordWrap((byte)param);
                break;
            }
            case 53: {
                newPAP.setFOverflowPunct((byte)param);
                break;
            }
            case 54: {
                newPAP.setFTopLinePunct((byte)param);
                break;
            }
            case 55: {
                newPAP.setFAutoSpaceDE((byte)param);
                break;
            }
            case 56: {
                newPAP.setFAutoSpaceDN((byte)param);
                break;
            }
            case 57: {
                newPAP.setWAlignFont(param);
                break;
            }
            case 58: {
                newPAP.setFontAlign((short)param);
            }
            case 62: {
                newPAP.setAnld(varParam);
            }
            case 63: {}
            case 64: {}
            case 65: {}
            case 67: {}
            case 69: {
                if (spra == 6) {
                    newPAP.setNumrm(varParam);
                    break;
                }
                break;
            }
            case 71: {
                newPAP.setFUsePgsuSettings((byte)param);
                break;
            }
            case 72: {
                newPAP.setFAdjustRight((byte)param);
                break;
            }
        }
    }
    
    static void doTAPOperation(final TableProperties newTAP, final int operand, final int param, final byte[] varParam) {
        switch (operand) {
            case 0: {
                newTAP.setJc((short)param);
                break;
            }
            case 1: {
                final short[] rgdxaCenter = newTAP.getRgdxaCenter();
                final short itcMac = newTAP.getItcMac();
                final int adjust = param - (rgdxaCenter[0] + newTAP.getDxaGapHalf());
                for (int x = 0; x < itcMac; ++x) {
                    final short[] array = rgdxaCenter;
                    final int n = x;
                    array[n] += (short)adjust;
                }
                break;
            }
            case 2: {
                final short[] rgdxaCenter = newTAP.getRgdxaCenter();
                if (rgdxaCenter != null) {
                    final int adjust2 = newTAP.getDxaGapHalf() - param;
                    final short[] array2 = rgdxaCenter;
                    final int n2 = 0;
                    array2[n2] += (short)adjust2;
                }
                newTAP.setDxaGapHalf(param);
                break;
            }
            case 3: {
                newTAP.setFCantSplit(getFlag(param));
                break;
            }
            case 4: {
                newTAP.setFTableHeader(getFlag(param));
                break;
            }
            case 5: {
                final short[] brcTop = newTAP.getBrcTop();
                final short[] brcLeft = newTAP.getBrcLeft();
                final short[] brcBottom = newTAP.getBrcBottom();
                final short[] brcRight = newTAP.getBrcRight();
                final short[] brcVertical = newTAP.getBrcVertical();
                final short[] brcHorizontal = newTAP.getBrcHorizontal();
                brcTop[0] = LittleEndian.getShort(varParam, 0);
                brcTop[1] = LittleEndian.getShort(varParam, 2);
                brcLeft[0] = LittleEndian.getShort(varParam, 4);
                brcLeft[1] = LittleEndian.getShort(varParam, 6);
                brcBottom[0] = LittleEndian.getShort(varParam, 8);
                brcBottom[1] = LittleEndian.getShort(varParam, 10);
                brcRight[0] = LittleEndian.getShort(varParam, 12);
                brcRight[1] = LittleEndian.getShort(varParam, 14);
                brcHorizontal[0] = LittleEndian.getShort(varParam, 16);
                brcHorizontal[1] = LittleEndian.getShort(varParam, 18);
                brcVertical[0] = LittleEndian.getShort(varParam, 20);
                brcVertical[1] = LittleEndian.getShort(varParam, 22);
            }
            case 7: {
                newTAP.setDyaRowHeight(param);
                break;
            }
            case 8: {
                final short[] rgdxaCenter = new short[varParam[0] + 1];
                final TableCellDescriptor[] rgtc = new TableCellDescriptor[varParam[0]];
                final short itcMac2 = varParam[0];
                newTAP.setItcMac(itcMac2);
                newTAP.setRgdxaCenter(rgdxaCenter);
                newTAP.setRgtc(rgtc);
                for (int x = 0; x < itcMac2; ++x) {
                    rgdxaCenter[x] = LittleEndian.getShort(varParam, 1 + x * 2);
                    rgtc[x] = TableCellDescriptor.convertBytesToTC(varParam, 1 + (itcMac2 + 1) * 2 + x * 20);
                }
                rgdxaCenter[itcMac2] = LittleEndian.getShort(varParam, 1 + itcMac2 * 2);
            }
            case 9: {}
            case 32: {
                final TCAbstractType[] rgtc2 = newTAP.getRgtc();
                for (int x2 = varParam[0]; x2 < varParam[1]; ++x2) {
                    if ((varParam[2] & 0x8) > 0) {
                        final short[] brcRight2 = rgtc2[x2].getBrcRight();
                        brcRight2[0] = LittleEndian.getShort(varParam, 6);
                        brcRight2[1] = LittleEndian.getShort(varParam, 8);
                    }
                    else if ((varParam[2] & 0x4) > 0) {
                        final short[] brcBottom = rgtc2[x2].getBrcBottom();
                        brcBottom[0] = LittleEndian.getShort(varParam, 6);
                        brcBottom[1] = LittleEndian.getShort(varParam, 8);
                    }
                    else if ((varParam[2] & 0x2) > 0) {
                        final short[] brcLeft2 = rgtc2[x2].getBrcLeft();
                        brcLeft2[0] = LittleEndian.getShort(varParam, 6);
                        brcLeft2[1] = LittleEndian.getShort(varParam, 8);
                    }
                    else if ((varParam[2] & 0x1) > 0) {
                        final short[] brcTop2 = rgtc2[x2].getBrcTop();
                        brcTop2[0] = LittleEndian.getShort(varParam, 6);
                        brcTop2[1] = LittleEndian.getShort(varParam, 8);
                    }
                }
                break;
            }
            case 33: {
                int index = (param & 0xFF000000) >> 24;
                final int count = (param & 0xFF0000) >> 16;
                final int width = param & 0xFFFF;
                final int itcMac3 = newTAP.getItcMac();
                final short[] rgdxaCenter2 = new short[itcMac3 + count + 1];
                final TableCellDescriptor[] rgtc3 = new TableCellDescriptor[itcMac3 + count];
                if (index >= itcMac3) {
                    index = itcMac3;
                    System.arraycopy(newTAP.getRgdxaCenter(), 0, rgdxaCenter2, 0, itcMac3 + 1);
                    System.arraycopy(newTAP.getRgtc(), 0, rgtc3, 0, itcMac3);
                }
                else {
                    System.arraycopy(newTAP.getRgdxaCenter(), 0, rgdxaCenter2, 0, index + 1);
                    System.arraycopy(newTAP.getRgdxaCenter(), index + 1, rgdxaCenter2, index + count, itcMac3 - index);
                    System.arraycopy(newTAP.getRgtc(), 0, rgtc3, 0, index);
                    System.arraycopy(newTAP.getRgtc(), index, rgtc3, index + count, itcMac3 - index);
                }
                for (int x3 = index; x3 < index + count; ++x3) {
                    rgtc3[x3] = new TableCellDescriptor();
                    rgdxaCenter2[x3] = (short)(rgdxaCenter2[x3 - 1] + width);
                }
                rgdxaCenter2[index + count] = (short)(rgdxaCenter2[index + count - 1] + width);
            }
        }
    }
    
    static void doSEPOperation(final SectionProperties newSEP, final int operand, final int param, final byte[] varParam) {
        switch (operand) {
            case 0: {
                newSEP.setCnsPgn((byte)param);
                break;
            }
            case 1: {
                newSEP.setIHeadingPgn((byte)param);
                break;
            }
            case 2: {
                newSEP.setOlstAnm(varParam);
            }
            case 3: {}
            case 5: {
                newSEP.setFEvenlySpaced(getFlag(param));
                break;
            }
            case 6: {
                newSEP.setFUnlocked(getFlag(param));
                break;
            }
            case 7: {
                newSEP.setDmBinFirst((short)param);
                break;
            }
            case 8: {
                newSEP.setDmBinOther((short)param);
                break;
            }
            case 9: {
                newSEP.setBkc((byte)param);
                break;
            }
            case 10: {
                newSEP.setFTitlePage(getFlag(param));
                break;
            }
            case 11: {
                newSEP.setCcolM1((short)param);
                break;
            }
            case 12: {
                newSEP.setDxaColumns(param);
                break;
            }
            case 13: {
                newSEP.setFAutoPgn(getFlag(param));
                break;
            }
            case 14: {
                newSEP.setNfcPgn((byte)param);
                break;
            }
            case 15: {
                newSEP.setDyaPgn((short)param);
                break;
            }
            case 16: {
                newSEP.setDxaPgn((short)param);
                break;
            }
            case 17: {
                newSEP.setFPgnRestart(getFlag(param));
                break;
            }
            case 18: {
                newSEP.setFEndNote(getFlag(param));
                break;
            }
            case 19: {
                newSEP.setLnc((byte)param);
                break;
            }
            case 20: {
                newSEP.setGrpfIhdt((byte)param);
                break;
            }
            case 21: {
                newSEP.setNLnnMod((short)param);
                break;
            }
            case 22: {
                newSEP.setDxaLnn(param);
                break;
            }
            case 23: {
                newSEP.setDyaHdrTop(param);
                break;
            }
            case 24: {
                newSEP.setDyaHdrBottom(param);
                break;
            }
            case 25: {
                newSEP.setFLBetween(getFlag(param));
                break;
            }
            case 26: {
                newSEP.setVjc((byte)param);
                break;
            }
            case 27: {
                newSEP.setLnnMin((short)param);
                break;
            }
            case 28: {
                newSEP.setPgnStart((short)param);
                break;
            }
            case 29: {
                newSEP.setDmOrientPage((byte)param);
            }
            case 31: {
                newSEP.setXaPage(param);
                break;
            }
            case 32: {
                newSEP.setYaPage(param);
                break;
            }
            case 33: {
                newSEP.setDxaLeft(param);
                break;
            }
            case 34: {
                newSEP.setDxaRight(param);
                break;
            }
            case 35: {
                newSEP.setDyaTop(param);
                break;
            }
            case 36: {
                newSEP.setDyaBottom(param);
                break;
            }
            case 37: {
                newSEP.setDzaGutter(param);
                break;
            }
            case 38: {
                newSEP.setDmPaperReq((short)param);
                break;
            }
            case 39: {
                newSEP.setFPropMark(getFlag(varParam[0]));
            }
            case 40: {}
            case 41: {}
            case 43: {
                final short[] brcTop = newSEP.getBrcTop();
                brcTop[0] = (short)(param & 0xFFFF);
                brcTop[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 44: {
                final short[] brcLeft = newSEP.getBrcLeft();
                brcLeft[0] = (short)(param & 0xFFFF);
                brcLeft[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 45: {
                final short[] brcBottom = newSEP.getBrcBottom();
                brcBottom[0] = (short)(param & 0xFFFF);
                brcBottom[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 46: {
                final short[] brcRight = newSEP.getBrcRight();
                brcRight[0] = (short)(param & 0xFFFF);
                brcRight[1] = (short)((param & 0xFFFF0000) >> 16);
                break;
            }
            case 47: {
                newSEP.setPgbProp(param);
                break;
            }
            case 48: {
                newSEP.setDxtCharSpace(param);
                break;
            }
            case 49: {
                newSEP.setDyaLinePitch(param);
                break;
            }
            case 51: {
                newSEP.setWTextFlow((short)param);
                break;
            }
        }
    }
    
    private static boolean getCHPFlag(final byte x, final boolean oldVal) {
        switch (x) {
            case 0: {
                return false;
            }
            case 1: {
                return true;
            }
            case Byte.MIN_VALUE: {
                return oldVal;
            }
            case -127: {
                return !oldVal;
            }
            default: {
                return false;
            }
        }
    }
    
    public static boolean getFlag(final int x) {
        return x != 0;
    }
}
