// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.hdf.model.hdftypes.definitions.PAPAbstractType;

@Deprecated
public final class ParagraphProperties extends PAPAbstractType implements Cloneable
{
    public ParagraphProperties() {
        final short[] lspd = new short[2];
        this.setFWidowControl((byte)1);
        lspd[1] = 1;
        this.setIlvl((byte)9);
        this.setLspd(lspd);
        this.setBrcBar(new short[2]);
        this.setBrcBottom(new short[2]);
        this.setBrcLeft(new short[2]);
        this.setBrcBetween(new short[2]);
        this.setBrcRight(new short[2]);
        this.setBrcTop(new short[2]);
        this.setPhe(new byte[12]);
        this.setAnld(new byte[84]);
        this.setDttmPropRMark(new byte[4]);
        this.setNumrm(new byte[8]);
    }
    
    public Object clone() throws CloneNotSupportedException {
        final ParagraphProperties clone = (ParagraphProperties)super.clone();
        final short[] brcBar = new short[2];
        final short[] brcBottom = new short[2];
        final short[] brcLeft = new short[2];
        final short[] brcBetween = new short[2];
        final short[] brcRight = new short[2];
        final short[] brcTop = new short[2];
        final short[] lspd = new short[2];
        final byte[] phe = new byte[12];
        final byte[] anld = new byte[84];
        final byte[] dttmPropRMark = new byte[4];
        final byte[] numrm = new byte[8];
        System.arraycopy(this.getBrcBar(), 0, brcBar, 0, 2);
        System.arraycopy(this.getBrcBottom(), 0, brcBottom, 0, 2);
        System.arraycopy(this.getBrcLeft(), 0, brcLeft, 0, 2);
        System.arraycopy(this.getBrcBetween(), 0, brcBetween, 0, 2);
        System.arraycopy(this.getBrcRight(), 0, brcRight, 0, 2);
        System.arraycopy(this.getBrcTop(), 0, brcTop, 0, 2);
        System.arraycopy(this.getLspd(), 0, lspd, 0, 2);
        System.arraycopy(this.getPhe(), 0, phe, 0, 12);
        System.arraycopy(this.getAnld(), 0, anld, 0, 84);
        System.arraycopy(this.getDttmPropRMark(), 0, dttmPropRMark, 0, 4);
        System.arraycopy(this.getNumrm(), 0, numrm, 0, 8);
        clone.setBrcBar(brcBar);
        clone.setBrcBottom(brcBottom);
        clone.setBrcLeft(brcLeft);
        clone.setBrcBetween(brcBetween);
        clone.setBrcRight(brcRight);
        clone.setBrcTop(brcTop);
        clone.setLspd(lspd);
        clone.setPhe(phe);
        clone.setAnld(anld);
        clone.setDttmPropRMark(dttmPropRMark);
        clone.setNumrm(numrm);
        return clone;
    }
}
