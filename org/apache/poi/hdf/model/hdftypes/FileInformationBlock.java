// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

import org.apache.poi.hdf.model.hdftypes.definitions.FIBAbstractType;

@Deprecated
public final class FileInformationBlock extends FIBAbstractType
{
    public FileInformationBlock(final byte[] mainDocument) {
        this.fillFields(mainDocument, (short)0, 0);
    }
}
