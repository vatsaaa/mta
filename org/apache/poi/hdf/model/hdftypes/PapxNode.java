// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdf.model.hdftypes;

@Deprecated
public final class PapxNode extends PropertyNode
{
    public PapxNode(final int fcStart, final int fcEnd, final byte[] papx) {
        super(fcStart, fcEnd, papx);
    }
    
    public byte[] getPapx() {
        return super.getGrpprl();
    }
}
