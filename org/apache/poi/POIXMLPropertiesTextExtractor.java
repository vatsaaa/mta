// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.CTProperties;
import org.apache.poi.openxml4j.opc.internal.PackagePropertiesPart;
import java.util.Date;

public class POIXMLPropertiesTextExtractor extends POIXMLTextExtractor
{
    public POIXMLPropertiesTextExtractor(final POIXMLDocument doc) {
        super(doc);
    }
    
    public POIXMLPropertiesTextExtractor(final POIXMLTextExtractor otherExtractor) {
        super(otherExtractor.getDocument());
    }
    
    private void appendIfPresent(final StringBuffer text, final String thing, final boolean value) {
        this.appendIfPresent(text, thing, Boolean.toString(value));
    }
    
    private void appendIfPresent(final StringBuffer text, final String thing, final int value) {
        this.appendIfPresent(text, thing, Integer.toString(value));
    }
    
    private void appendIfPresent(final StringBuffer text, final String thing, final Date value) {
        if (value == null) {
            return;
        }
        this.appendIfPresent(text, thing, value.toString());
    }
    
    private void appendIfPresent(final StringBuffer text, final String thing, final String value) {
        if (value == null) {
            return;
        }
        text.append(thing);
        text.append(" = ");
        text.append(value);
        text.append("\n");
    }
    
    public String getCorePropertiesText() {
        final StringBuffer text = new StringBuffer();
        final PackagePropertiesPart props = this.getDocument().getProperties().getCoreProperties().getUnderlyingProperties();
        this.appendIfPresent(text, "Category", props.getCategoryProperty().getValue());
        this.appendIfPresent(text, "Category", props.getCategoryProperty().getValue());
        this.appendIfPresent(text, "ContentStatus", props.getContentStatusProperty().getValue());
        this.appendIfPresent(text, "ContentType", props.getContentTypeProperty().getValue());
        this.appendIfPresent(text, "Created", props.getCreatedProperty().getValue());
        this.appendIfPresent(text, "CreatedString", props.getCreatedPropertyString());
        this.appendIfPresent(text, "Creator", props.getCreatorProperty().getValue());
        this.appendIfPresent(text, "Description", props.getDescriptionProperty().getValue());
        this.appendIfPresent(text, "Identifier", props.getIdentifierProperty().getValue());
        this.appendIfPresent(text, "Keywords", props.getKeywordsProperty().getValue());
        this.appendIfPresent(text, "Language", props.getLanguageProperty().getValue());
        this.appendIfPresent(text, "LastModifiedBy", props.getLastModifiedByProperty().getValue());
        this.appendIfPresent(text, "LastPrinted", props.getLastPrintedProperty().getValue());
        this.appendIfPresent(text, "LastPrintedString", props.getLastPrintedPropertyString());
        this.appendIfPresent(text, "Modified", props.getModifiedProperty().getValue());
        this.appendIfPresent(text, "ModifiedString", props.getModifiedPropertyString());
        this.appendIfPresent(text, "Revision", props.getRevisionProperty().getValue());
        this.appendIfPresent(text, "Subject", props.getSubjectProperty().getValue());
        this.appendIfPresent(text, "Title", props.getTitleProperty().getValue());
        this.appendIfPresent(text, "Version", props.getVersionProperty().getValue());
        return text.toString();
    }
    
    public String getExtendedPropertiesText() {
        final StringBuffer text = new StringBuffer();
        final CTProperties props = this.getDocument().getProperties().getExtendedProperties().getUnderlyingProperties();
        this.appendIfPresent(text, "Application", props.getApplication());
        this.appendIfPresent(text, "AppVersion", props.getAppVersion());
        this.appendIfPresent(text, "Characters", props.getCharacters());
        this.appendIfPresent(text, "CharactersWithSpaces", props.getCharactersWithSpaces());
        this.appendIfPresent(text, "Company", props.getCompany());
        this.appendIfPresent(text, "HyperlinkBase", props.getHyperlinkBase());
        this.appendIfPresent(text, "HyperlinksChanged", props.getHyperlinksChanged());
        this.appendIfPresent(text, "Lines", props.getLines());
        this.appendIfPresent(text, "LinksUpToDate", props.getLinksUpToDate());
        this.appendIfPresent(text, "Manager", props.getManager());
        this.appendIfPresent(text, "Pages", props.getPages());
        this.appendIfPresent(text, "Paragraphs", props.getParagraphs());
        this.appendIfPresent(text, "PresentationFormat", props.getPresentationFormat());
        this.appendIfPresent(text, "Template", props.getTemplate());
        this.appendIfPresent(text, "TotalTime", props.getTotalTime());
        return text.toString();
    }
    
    public String getCustomPropertiesText() {
        final StringBuffer text = new StringBuffer();
        final org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperties props = this.getDocument().getProperties().getCustomProperties().getUnderlyingProperties();
        final List<CTProperty> properties = props.getPropertyList();
        for (final CTProperty property : properties) {
            String val = "(not implemented!)";
            if (property.isSetLpwstr()) {
                val = property.getLpwstr();
            }
            else if (property.isSetLpstr()) {
                val = property.getLpstr();
            }
            else if (property.isSetDate()) {
                val = property.getDate().toString();
            }
            else if (property.isSetFiletime()) {
                val = property.getFiletime().toString();
            }
            else if (property.isSetBool()) {
                val = Boolean.toString(property.getBool());
            }
            else if (property.isSetI1()) {
                val = Integer.toString(property.getI1());
            }
            else if (property.isSetI2()) {
                val = Integer.toString(property.getI2());
            }
            else if (property.isSetI4()) {
                val = Integer.toString(property.getI4());
            }
            else if (property.isSetI8()) {
                val = Long.toString(property.getI8());
            }
            else if (property.isSetInt()) {
                val = Integer.toString(property.getInt());
            }
            else if (property.isSetUi1()) {
                val = Integer.toString(property.getUi1());
            }
            else if (property.isSetUi2()) {
                val = Integer.toString(property.getUi2());
            }
            else if (property.isSetUi4()) {
                val = Long.toString(property.getUi4());
            }
            else if (property.isSetUi8()) {
                val = property.getUi8().toString();
            }
            else if (property.isSetUint()) {
                val = Long.toString(property.getUint());
            }
            else if (property.isSetR4()) {
                val = Float.toString(property.getR4());
            }
            else if (property.isSetR8()) {
                val = Double.toString(property.getR8());
            }
            else if (property.isSetDecimal()) {
                final BigDecimal d = property.getDecimal();
                if (d == null) {
                    val = null;
                }
                else {
                    val = d.toPlainString();
                }
            }
            else if (!property.isSetArray()) {
                if (!property.isSetVector()) {
                    if (!property.isSetBlob()) {
                        if (!property.isSetOblob()) {
                            if (!property.isSetStream() && !property.isSetOstream()) {
                                if (!property.isSetVstream()) {
                                    if (property.isSetStorage() || property.isSetOstorage()) {}
                                }
                            }
                        }
                    }
                }
            }
            text.append(property.getName() + " = " + val + "\n");
        }
        return text.toString();
    }
    
    @Override
    public String getText() {
        try {
            return this.getCorePropertiesText() + this.getExtendedPropertiesText() + this.getCustomPropertiesText();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public POIXMLPropertiesTextExtractor getMetadataTextExtractor() {
        throw new IllegalStateException("You already have the Metadata Text Extractor, not recursing!");
    }
}
