// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.extractor;

import java.io.FileInputStream;
import org.apache.poi.hpbf.model.qcbits.QCBit;
import org.apache.poi.hpbf.model.qcbits.QCPLCBit;
import org.apache.poi.hpbf.model.qcbits.QCTextBit;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.POIDocument;
import org.apache.poi.hpbf.HPBFDocument;
import org.apache.poi.POIOLE2TextExtractor;

public final class PublisherTextExtractor extends POIOLE2TextExtractor
{
    private HPBFDocument doc;
    private boolean hyperlinksByDefault;
    
    public PublisherTextExtractor(final HPBFDocument doc) {
        super(doc);
        this.hyperlinksByDefault = false;
        this.doc = doc;
    }
    
    public PublisherTextExtractor(final DirectoryNode dir) throws IOException {
        this(new HPBFDocument(dir));
    }
    
    public PublisherTextExtractor(final POIFSFileSystem fs) throws IOException {
        this(new HPBFDocument(fs));
    }
    
    public PublisherTextExtractor(final NPOIFSFileSystem fs) throws IOException {
        this(new HPBFDocument(fs));
    }
    
    public PublisherTextExtractor(final InputStream is) throws IOException {
        this(new POIFSFileSystem(is));
    }
    
    @Deprecated
    public PublisherTextExtractor(final DirectoryNode dir, final POIFSFileSystem fs) throws IOException {
        this(new HPBFDocument(dir, fs));
    }
    
    public void setHyperlinksByDefault(final boolean hyperlinksByDefault) {
        this.hyperlinksByDefault = hyperlinksByDefault;
    }
    
    @Override
    public String getText() {
        final StringBuffer text = new StringBuffer();
        final QCBit[] bits = this.doc.getQuillContents().getBits();
        for (int i = 0; i < bits.length; ++i) {
            if (bits[i] != null && bits[i] instanceof QCTextBit) {
                final QCTextBit t = (QCTextBit)bits[i];
                text.append(t.getText().replace('\r', '\n'));
            }
        }
        if (this.hyperlinksByDefault) {
            for (int i = 0; i < bits.length; ++i) {
                if (bits[i] != null && bits[i] instanceof QCPLCBit.Type12) {
                    final QCPLCBit.Type12 hyperlinks = (QCPLCBit.Type12)bits[i];
                    for (int j = 0; j < hyperlinks.getNumberOfHyperlinks(); ++j) {
                        text.append("<");
                        text.append(hyperlinks.getHyperlink(j));
                        text.append(">\n");
                    }
                }
            }
        }
        return text.toString();
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length == 0) {
            System.err.println("Use:");
            System.err.println("  PublisherTextExtractor <file.pub>");
        }
        for (int i = 0; i < args.length; ++i) {
            final PublisherTextExtractor te = new PublisherTextExtractor(new FileInputStream(args[i]));
            System.out.println(te.getText());
        }
    }
}
