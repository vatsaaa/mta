// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf;

import java.io.OutputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hpbf.model.EscherDelayStm;
import org.apache.poi.hpbf.model.EscherStm;
import org.apache.poi.hpbf.model.QuillContents;
import org.apache.poi.hpbf.model.MainContents;
import org.apache.poi.POIDocument;

public final class HPBFDocument extends POIDocument
{
    private MainContents mainContents;
    private QuillContents quillContents;
    private EscherStm escherStm;
    private EscherDelayStm escherDelayStm;
    
    public HPBFDocument(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public HPBFDocument(final NPOIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public HPBFDocument(final InputStream inp) throws IOException {
        this(new POIFSFileSystem(inp));
    }
    
    @Deprecated
    public HPBFDocument(final DirectoryNode dir, final POIFSFileSystem fs) throws IOException {
        this(dir);
    }
    
    public HPBFDocument(final DirectoryNode dir) throws IOException {
        super(dir);
        this.mainContents = new MainContents(dir);
        this.quillContents = new QuillContents(dir);
        this.escherStm = new EscherStm(dir);
        this.escherDelayStm = new EscherDelayStm(dir);
    }
    
    public MainContents getMainContents() {
        return this.mainContents;
    }
    
    public QuillContents getQuillContents() {
        return this.quillContents;
    }
    
    public EscherStm getEscherStm() {
        return this.escherStm;
    }
    
    public EscherDelayStm getEscherDelayStm() {
        return this.escherDelayStm;
    }
    
    @Override
    public void write(final OutputStream out) throws IOException {
        throw new IllegalStateException("Writing is not yet implemented, see http://poi.apache.org/hpbf/");
    }
}
