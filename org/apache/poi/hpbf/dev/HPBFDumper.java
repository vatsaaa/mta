// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.dev;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.StringUtil;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import java.io.FileInputStream;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public final class HPBFDumper
{
    private POIFSFileSystem fs;
    
    public HPBFDumper(final POIFSFileSystem fs) {
        this.fs = fs;
    }
    
    public HPBFDumper(final InputStream inp) throws IOException {
        this(new POIFSFileSystem(inp));
    }
    
    private static byte[] getData(final DirectoryNode dir, final String name) throws IOException {
        final DocumentEntry docProps = (DocumentEntry)dir.getEntry(name);
        final byte[] d = new byte[docProps.getSize()];
        dir.createDocumentInputStream(name).read(d);
        return d;
    }
    
    private String dumpBytes(final byte[] data, final int offset, final int len) {
        final StringBuffer ret = new StringBuffer();
        for (int i = 0; i < len; ++i) {
            final int j = i + offset;
            int b = data[j];
            if (b < 0) {
                b += 256;
            }
            final String bs = Integer.toHexString(b);
            if (bs.length() == 1) {
                ret.append('0');
            }
            ret.append(bs);
            ret.append(' ');
        }
        return ret.toString();
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Use:");
            System.err.println("  HPBFDumper <filename>");
            System.exit(1);
        }
        final HPBFDumper dump = new HPBFDumper(new FileInputStream(args[0]));
        System.out.println("Dumping " + args[0]);
        dump.dumpContents();
        dump.dumpEnvelope();
        dump.dumpEscher();
        dump.dump001CompObj(dump.fs.getRoot());
        dump.dumpQuill();
    }
    
    public void dumpEscher() throws IOException {
        final DirectoryNode escherDir = (DirectoryNode)this.fs.getRoot().getEntry("Escher");
        this.dumpEscherStm(escherDir);
        this.dumpEscherDelayStm(escherDir);
    }
    
    private void dumpEscherStream(final byte[] data) {
        final DefaultEscherRecordFactory erf = new DefaultEscherRecordFactory();
        int left = data.length;
        while (left > 0) {
            final EscherRecord er = erf.createRecord(data, 0);
            er.fillFields(data, 0, erf);
            left -= er.getRecordSize();
            System.out.println(er.toString());
        }
    }
    
    protected void dumpEscherStm(final DirectoryNode escherDir) throws IOException {
        final byte[] data = getData(escherDir, "EscherStm");
        System.out.println("");
        System.out.println("EscherStm - " + data.length + " bytes long:");
        if (data.length > 0) {
            this.dumpEscherStream(data);
        }
    }
    
    protected void dumpEscherDelayStm(final DirectoryNode escherDir) throws IOException {
        final byte[] data = getData(escherDir, "EscherDelayStm");
        System.out.println("");
        System.out.println("EscherDelayStm - " + data.length + " bytes long:");
        if (data.length > 0) {
            this.dumpEscherStream(data);
        }
    }
    
    public void dumpEnvelope() throws IOException {
        final byte[] data = getData(this.fs.getRoot(), "Envelope");
        System.out.println("");
        System.out.println("Envelope - " + data.length + " bytes long:");
    }
    
    public void dumpContents() throws IOException {
        final byte[] data = getData(this.fs.getRoot(), "Contents");
        System.out.println("");
        System.out.println("Contents - " + data.length + " bytes long:");
    }
    
    public void dumpCONTENTSraw(final DirectoryNode dir) throws IOException {
        final byte[] data = getData(dir, "CONTENTS");
        System.out.println("");
        System.out.println("CONTENTS - " + data.length + " bytes long:");
        System.out.println(new String(data, 0, 8) + this.dumpBytes(data, 8, 24));
        int pos = 32;
        int blen;
        for (boolean sixNotEight = true; pos < 512; pos += 4 + blen, sixNotEight = !sixNotEight) {
            if (sixNotEight) {
                System.out.println(this.dumpBytes(data, pos, 2));
                pos += 2;
            }
            final String text = new String(data, pos, 4);
            blen = 8;
            if (sixNotEight) {
                blen = 6;
            }
            System.out.println(text + " " + this.dumpBytes(data, pos + 4, blen));
        }
        int textStop = -1;
        for (int i = 512; i < data.length - 2 && textStop == -1; ++i) {
            if (data[i] == 0 && data[i + 1] == 0 && data[i + 2] == 0) {
                textStop = i;
            }
        }
        if (textStop > 0) {
            final int len = (textStop - 512) / 2;
            System.out.println("");
            System.out.println(StringUtil.getFromUnicodeLE(data, 512, len));
        }
    }
    
    public void dumpCONTENTSguessed(final DirectoryNode dir) throws IOException {
        final byte[] data = getData(dir, "CONTENTS");
        System.out.println("");
        System.out.println("CONTENTS - " + data.length + " bytes long:");
        final String[] startType = new String[20];
        final String[] endType = new String[20];
        final int[] optA = new int[20];
        final int[] optB = new int[20];
        final int[] optC = new int[20];
        final int[] from = new int[20];
        final int[] len = new int[20];
        for (int i = 0; i < 20; ++i) {
            final int offset = 32 + i * 24;
            if (data[offset] == 24 && data[offset + 1] == 0) {
                startType[i] = new String(data, offset + 2, 4);
                optA[i] = LittleEndian.getUShort(data, offset + 6);
                optB[i] = LittleEndian.getUShort(data, offset + 8);
                optC[i] = LittleEndian.getUShort(data, offset + 10);
                endType[i] = new String(data, offset + 12, 4);
                from[i] = (int)LittleEndian.getUInt(data, offset + 16);
                len[i] = (int)LittleEndian.getUInt(data, offset + 20);
            }
        }
        final String text = StringUtil.getFromUnicodeLE(data, from[0], len[0] / 2);
        for (int j = 0; j < 20; ++j) {
            String num = Integer.toString(j);
            if (j < 10) {
                num = "0" + j;
            }
            System.out.print(num + " ");
            if (startType[j] == null) {
                System.out.println("(not present)");
            }
            else {
                System.out.println("\t" + startType[j] + " " + optA[j] + " " + optB[j] + " " + optC[j]);
                System.out.println("\t" + endType[j] + " " + "from: " + Integer.toHexString(from[j]) + " (" + from[j] + ")" + ", len: " + Integer.toHexString(len[j]) + " (" + len[j] + ")");
            }
        }
        System.out.println("");
        System.out.println("TEXT:");
        System.out.println(text);
        System.out.println("");
        for (int j = 0; j < 20; ++j) {
            if (startType[j] != null) {
                final int start = from[j];
                System.out.println(startType[j] + " -> " + endType[j] + " @ " + Integer.toHexString(start) + " (" + start + ")");
                System.out.println("\t" + this.dumpBytes(data, start, 4));
                System.out.println("\t" + this.dumpBytes(data, start + 4, 4));
                System.out.println("\t" + this.dumpBytes(data, start + 8, 4));
                System.out.println("\t(etc)");
            }
        }
    }
    
    protected void dump001CompObj(final DirectoryNode dir) {
    }
    
    public void dumpQuill() throws IOException {
        final DirectoryNode quillDir = (DirectoryNode)this.fs.getRoot().getEntry("Quill");
        final DirectoryNode quillSubDir = (DirectoryNode)quillDir.getEntry("QuillSub");
        this.dump001CompObj(quillSubDir);
        this.dumpCONTENTSraw(quillSubDir);
        this.dumpCONTENTSguessed(quillSubDir);
    }
}
