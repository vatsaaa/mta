// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DirectoryNode;

public abstract class HPBFPart
{
    protected byte[] data;
    
    public HPBFPart(final DirectoryNode baseDir, final String[] path) throws IOException {
        final DirectoryNode dir = this.getDir(path, baseDir);
        final String name = path[path.length - 1];
        DocumentEntry docProps;
        try {
            docProps = (DocumentEntry)dir.getEntry(name);
        }
        catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File invalid - failed to find document entry '" + name + "'");
        }
        this.data = new byte[docProps.getSize()];
        dir.createDocumentInputStream(name).read(this.data);
    }
    
    private DirectoryNode getDir(final String[] path, final DirectoryNode baseDir) {
        DirectoryNode dir = baseDir;
        for (int i = 0; i < path.length - 1; ++i) {
            try {
                dir = (DirectoryNode)dir.getEntry(path[i]);
            }
            catch (FileNotFoundException e) {
                throw new IllegalArgumentException("File invalid - failed to find directory entry '" + path[i] + "'");
            }
        }
        return dir;
    }
    
    public void writeOut(final DirectoryNode baseDir) throws IOException {
        final String[] path = this.getPath();
        DirectoryNode dir = baseDir;
        for (int i = 0; i < path.length - 1; ++i) {
            try {
                dir = (DirectoryNode)dir.getEntry(path[i]);
            }
            catch (FileNotFoundException e) {
                dir.createDirectory(path[i]);
            }
        }
        this.generateData();
        final ByteArrayInputStream bais = new ByteArrayInputStream(this.data);
        dir.createDocument(path[path.length - 1], bais);
    }
    
    protected abstract void generateData();
    
    public byte[] getData() {
        return this.data;
    }
    
    public final String[] getPath() {
        return null;
    }
}
