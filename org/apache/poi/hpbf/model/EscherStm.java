// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model;

import java.io.IOException;
import org.apache.poi.poifs.filesystem.DirectoryNode;

public final class EscherStm extends EscherPart
{
    private static final String[] PATH;
    
    public EscherStm(final DirectoryNode baseDir) throws IOException {
        super(baseDir, EscherStm.PATH);
    }
    
    static {
        PATH = new String[] { "Escher", "EscherStm" };
    }
}
