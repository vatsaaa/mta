// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model;

import java.io.IOException;
import org.apache.poi.poifs.filesystem.DirectoryNode;

public final class MainContents extends HPBFPart
{
    private static final String[] PATH;
    
    public MainContents(final DirectoryNode baseDir) throws IOException {
        super(baseDir, MainContents.PATH);
    }
    
    @Override
    protected void generateData() {
    }
    
    static {
        PATH = new String[] { "Contents" };
    }
}
