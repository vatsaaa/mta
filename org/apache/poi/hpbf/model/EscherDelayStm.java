// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model;

import java.io.IOException;
import org.apache.poi.poifs.filesystem.DirectoryNode;

public final class EscherDelayStm extends EscherPart
{
    private static final String[] PATH;
    
    public EscherDelayStm(final DirectoryNode baseDir) throws IOException {
        super(baseDir, EscherDelayStm.PATH);
    }
    
    static {
        PATH = new String[] { "Escher", "EscherDelayStm" };
    }
}
