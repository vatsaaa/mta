// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model;

import java.io.IOException;
import org.apache.poi.hpbf.model.qcbits.UnknownQCBit;
import org.apache.poi.hpbf.model.qcbits.QCPLCBit;
import org.apache.poi.hpbf.model.qcbits.QCTextBit;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.hpbf.model.qcbits.QCBit;

public final class QuillContents extends HPBFPart
{
    private static final String[] PATH;
    private QCBit[] bits;
    
    public QuillContents(final DirectoryNode baseDir) throws IOException {
        super(baseDir, QuillContents.PATH);
        final String f8 = new String(this.data, 0, 8);
        if (!f8.equals("CHNKINK ")) {
            throw new IllegalArgumentException("Expecting 'CHNKINK ' but was '" + f8 + "'");
        }
        this.bits = new QCBit[20];
        for (int i = 0; i < 20; ++i) {
            final int offset = 32 + i * 24;
            if (this.data[offset] == 24 && this.data[offset + 1] == 0) {
                final String thingType = new String(this.data, offset + 2, 4);
                final int optA = LittleEndian.getUShort(this.data, offset + 6);
                final int optB = LittleEndian.getUShort(this.data, offset + 8);
                final int optC = LittleEndian.getUShort(this.data, offset + 10);
                final String bitType = new String(this.data, offset + 12, 4);
                final int from = (int)LittleEndian.getUInt(this.data, offset + 16);
                final int len = (int)LittleEndian.getUInt(this.data, offset + 20);
                final byte[] bitData = new byte[len];
                System.arraycopy(this.data, from, bitData, 0, len);
                if (bitType.equals("TEXT")) {
                    this.bits[i] = new QCTextBit(thingType, bitType, bitData);
                }
                else if (bitType.equals("PLC ")) {
                    this.bits[i] = QCPLCBit.createQCPLCBit(thingType, bitType, bitData);
                }
                else {
                    this.bits[i] = new UnknownQCBit(thingType, bitType, bitData);
                }
                this.bits[i].setOptA(optA);
                this.bits[i].setOptB(optB);
                this.bits[i].setOptC(optC);
                this.bits[i].setDataOffset(from);
            }
        }
    }
    
    public QCBit[] getBits() {
        return this.bits;
    }
    
    @Override
    protected void generateData() {
        throw new IllegalStateException("Not done yet!");
    }
    
    static {
        PATH = new String[] { "Quill", "QuillSub", "CONTENTS" };
    }
}
