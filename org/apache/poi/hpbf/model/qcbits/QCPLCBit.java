// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model.qcbits;

import org.apache.poi.util.StringUtil;
import org.apache.poi.util.LittleEndian;

public abstract class QCPLCBit extends QCBit
{
    protected int numberOfPLCs;
    protected int typeOfPLCS;
    protected int[] preData;
    protected long[] plcValA;
    protected long[] plcValB;
    
    private QCPLCBit(final String thingType, final String bitType, final byte[] data) {
        super(thingType, bitType, data);
        this.numberOfPLCs = (int)LittleEndian.getUInt(data, 0);
        this.typeOfPLCS = (int)LittleEndian.getUInt(data, 4);
        this.plcValA = new long[this.numberOfPLCs];
        this.plcValB = new long[this.numberOfPLCs];
    }
    
    public int getNumberOfPLCs() {
        return this.numberOfPLCs;
    }
    
    public int getTypeOfPLCS() {
        return this.typeOfPLCS;
    }
    
    public int[] getPreData() {
        return this.preData;
    }
    
    public long[] getPlcValA() {
        return this.plcValA;
    }
    
    public long[] getPlcValB() {
        return this.plcValB;
    }
    
    public static QCPLCBit createQCPLCBit(final String thingType, final String bitType, final byte[] data) {
        final int type = (int)LittleEndian.getUInt(data, 4);
        switch (type) {
            case 0: {
                return new Type0(thingType, bitType, data);
            }
            case 4: {
                return new Type4(thingType, bitType, data);
            }
            case 8: {
                return new Type8(thingType, bitType, data);
            }
            case 12: {
                return new Type12(thingType, bitType, data);
            }
            default: {
                throw new IllegalArgumentException("Sorry, I don't know how to deal with PLCs of type " + type);
            }
        }
    }
    
    public static class Type0 extends QCPLCBit
    {
        private Type0(final String thingType, final String bitType, final byte[] data) {
            super(thingType, bitType, data, null);
            (this.preData = new int[4])[0] = LittleEndian.getUShort(data, 8);
            this.preData[1] = LittleEndian.getUShort(data, 10);
            this.preData[2] = LittleEndian.getUShort(data, 12);
            this.preData[3] = LittleEndian.getUShort(data, 14);
            for (int i = 0; i < this.numberOfPLCs; ++i) {
                this.plcValA[i] = LittleEndian.getUShort(data, 16 + 4 * i);
                this.plcValB[i] = LittleEndian.getUShort(data, 16 + 4 * i + 2);
            }
        }
    }
    
    public static class Type4 extends QCPLCBit
    {
        private Type4(final String thingType, final String bitType, final byte[] data) {
            super(thingType, bitType, data, null);
            (this.preData = new int[4])[0] = LittleEndian.getUShort(data, 8);
            this.preData[1] = LittleEndian.getUShort(data, 10);
            this.preData[2] = LittleEndian.getUShort(data, 12);
            this.preData[3] = LittleEndian.getUShort(data, 14);
            for (int i = 0; i < this.numberOfPLCs; ++i) {
                this.plcValA[i] = LittleEndian.getUInt(data, 16 + 8 * i);
                this.plcValB[i] = LittleEndian.getUInt(data, 16 + 8 * i + 4);
            }
        }
    }
    
    public static class Type8 extends QCPLCBit
    {
        private Type8(final String thingType, final String bitType, final byte[] data) {
            super(thingType, bitType, data, null);
            (this.preData = new int[7])[0] = LittleEndian.getUShort(data, 8);
            this.preData[1] = LittleEndian.getUShort(data, 10);
            this.preData[2] = LittleEndian.getUShort(data, 12);
            this.preData[3] = LittleEndian.getUShort(data, 14);
            this.preData[4] = LittleEndian.getUShort(data, 16);
            this.preData[5] = LittleEndian.getUShort(data, 18);
            this.preData[6] = LittleEndian.getUShort(data, 20);
            for (int i = 0; i < this.numberOfPLCs; ++i) {
                this.plcValA[i] = LittleEndian.getUInt(data, 22 + 8 * i);
                this.plcValB[i] = LittleEndian.getUInt(data, 22 + 8 * i + 4);
            }
        }
    }
    
    public static class Type12 extends QCPLCBit
    {
        private String[] hyperlinks;
        private static final int oneStartsAt = 76;
        private static final int twoStartsAt = 104;
        private static final int threePlusIncrement = 22;
        
        private Type12(final String thingType, final String bitType, final byte[] data) {
            super(thingType, bitType, data, null);
            if (data.length == 52) {
                this.hyperlinks = new String[0];
            }
            else {
                this.hyperlinks = new String[this.numberOfPLCs];
            }
            this.preData = new int[1 + this.numberOfPLCs + 1];
            for (int i = 0; i < this.preData.length; ++i) {
                this.preData[i] = (int)LittleEndian.getUInt(data, 8 + i * 4);
            }
            int at = 12 + this.numberOfPLCs * 4 + 4;
            int until = 52;
            if (this.numberOfPLCs == 1 && this.hyperlinks.length == 1) {
                until = 76;
            }
            else if (this.numberOfPLCs >= 2) {
                until = 104 + (this.numberOfPLCs - 2) * 22;
            }
            this.plcValA = new long[(until - at) / 2];
            this.plcValB = new long[0];
            for (int j = 0; j < this.plcValA.length; ++j) {
                this.plcValA[j] = LittleEndian.getUShort(data, at + j * 2);
            }
            at = until;
            for (int j = 0; j < this.hyperlinks.length; ++j) {
                final int len = LittleEndian.getUShort(data, at);
                final int first = LittleEndian.getUShort(data, at + 2);
                if (first == 0) {
                    this.hyperlinks[j] = "";
                    at += len;
                }
                else {
                    this.hyperlinks[j] = StringUtil.getFromUnicodeLE(data, at + 2, len);
                    at += 2 + 2 * len;
                }
            }
        }
        
        public int getNumberOfHyperlinks() {
            return this.hyperlinks.length;
        }
        
        public String getHyperlink(final int number) {
            return this.hyperlinks[number];
        }
        
        public int getTextStartAt(final int number) {
            return this.preData[1 + number];
        }
        
        public int getAllTextEndAt() {
            return this.preData[this.numberOfPLCs + 1];
        }
    }
}
