// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model.qcbits;

import org.apache.poi.util.StringUtil;

public final class QCTextBit extends QCBit
{
    public QCTextBit(final String thingType, final String bitType, final byte[] data) {
        super(thingType, bitType, data);
    }
    
    public String getText() {
        return StringUtil.getFromUnicodeLE(this.data, 0, this.data.length / 2);
    }
    
    public void setText(final String text) {
        StringUtil.putUnicodeLE(text, this.data = new byte[text.length() * 2], 0);
    }
}
