// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model.qcbits;

public abstract class QCBit
{
    protected String thingType;
    protected String bitType;
    protected byte[] data;
    protected int optA;
    protected int optB;
    protected int optC;
    protected int dataOffset;
    
    public QCBit(final String thingType, final String bitType, final byte[] data) {
        this.thingType = thingType;
        this.bitType = bitType;
        this.data = data;
    }
    
    public String getThingType() {
        return this.thingType;
    }
    
    public String getBitType() {
        return this.bitType;
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    public int getOptA() {
        return this.optA;
    }
    
    public void setOptA(final int optA) {
        this.optA = optA;
    }
    
    public int getOptB() {
        return this.optB;
    }
    
    public void setOptB(final int optB) {
        this.optB = optB;
    }
    
    public int getOptC() {
        return this.optC;
    }
    
    public void setOptC(final int optC) {
        this.optC = optC;
    }
    
    public int getDataOffset() {
        return this.dataOffset;
    }
    
    public void setDataOffset(final int offset) {
        this.dataOffset = offset;
    }
    
    public int getLength() {
        return this.data.length;
    }
}
