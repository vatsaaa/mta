// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model.qcbits;

public final class UnknownQCBit extends QCBit
{
    public UnknownQCBit(final String thingType, final String bitType, final byte[] data) {
        super(thingType, bitType, data);
    }
}
