// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpbf.model;

import java.io.IOException;
import org.apache.poi.ddf.EscherRecordFactory;
import java.util.ArrayList;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.ddf.EscherRecord;

public abstract class EscherPart extends HPBFPart
{
    private EscherRecord[] records;
    
    public EscherPart(final DirectoryNode baseDir, final String[] parts) throws IOException {
        super(baseDir, parts);
        final DefaultEscherRecordFactory erf = new DefaultEscherRecordFactory();
        final ArrayList<EscherRecord> ec = new ArrayList<EscherRecord>();
        int left = this.data.length;
        while (left > 0) {
            final EscherRecord er = erf.createRecord(this.data, 0);
            er.fillFields(this.data, 0, erf);
            left -= er.getRecordSize();
            ec.add(er);
        }
        this.records = ec.toArray(new EscherRecord[ec.size()]);
    }
    
    public EscherRecord[] getEscherRecords() {
        return this.records;
    }
    
    @Override
    protected void generateData() {
        int size = 0;
        for (int i = 0; i < this.records.length; ++i) {
            size += this.records[i].getRecordSize();
        }
        this.data = new byte[size];
        size = 0;
        for (int i = 0; i < this.records.length; ++i) {
            final int thisSize = this.records[i].serialize(size, this.data);
            size += thisSize;
        }
    }
}
