// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.FilterInputStream;

public class LittleEndianInputStream extends FilterInputStream implements LittleEndianInput
{
    public LittleEndianInputStream(final InputStream is) {
        super(is);
    }
    
    @Override
    public int available() {
        try {
            return super.available();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public byte readByte() {
        return (byte)this.readUByte();
    }
    
    public int readUByte() {
        int ch;
        try {
            ch = this.in.read();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        checkEOF(ch);
        return ch;
    }
    
    public double readDouble() {
        return Double.longBitsToDouble(this.readLong());
    }
    
    public int readInt() {
        int ch1;
        int ch2;
        int ch3;
        int ch4;
        try {
            ch1 = this.in.read();
            ch2 = this.in.read();
            ch3 = this.in.read();
            ch4 = this.in.read();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        checkEOF(ch1 | ch2 | ch3 | ch4);
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0);
    }
    
    public long readLong() {
        int b0;
        int b2;
        int b3;
        int b4;
        int b5;
        int b6;
        int b7;
        int b8;
        try {
            b0 = this.in.read();
            b2 = this.in.read();
            b3 = this.in.read();
            b4 = this.in.read();
            b5 = this.in.read();
            b6 = this.in.read();
            b7 = this.in.read();
            b8 = this.in.read();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        checkEOF(b0 | b2 | b3 | b4 | b5 | b6 | b7 | b8);
        return ((long)b8 << 56) + ((long)b7 << 48) + ((long)b6 << 40) + ((long)b5 << 32) + ((long)b4 << 24) + (b3 << 16) + (b2 << 8) + (b0 << 0);
    }
    
    public short readShort() {
        return (short)this.readUShort();
    }
    
    public int readUShort() {
        int ch1;
        int ch2;
        try {
            ch1 = this.in.read();
            ch2 = this.in.read();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        checkEOF(ch1 | ch2);
        return (ch2 << 8) + (ch1 << 0);
    }
    
    private static void checkEOF(final int value) {
        if (value < 0) {
            throw new RuntimeException("Unexpected end-of-file");
        }
    }
    
    public void readFully(final byte[] buf) {
        this.readFully(buf, 0, buf.length);
    }
    
    public void readFully(final byte[] buf, final int off, final int len) {
        for (int max = off + len, i = off; i < max; ++i) {
            int ch;
            try {
                ch = this.in.read();
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            checkEOF(ch);
            buf[i] = (byte)ch;
        }
    }
}
