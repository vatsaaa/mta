// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.util;

import java.util.Iterator;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.io.UnsupportedEncodingException;

public class StringUtil
{
    private static final String ENCODING_ISO_8859_1 = "ISO-8859-1";
    
    private StringUtil() {
    }
    
    public static String getFromUnicodeLE(final byte[] string, final int offset, final int len) throws ArrayIndexOutOfBoundsException, IllegalArgumentException {
        if (offset < 0 || offset >= string.length) {
            throw new ArrayIndexOutOfBoundsException("Illegal offset " + offset + " (String data is of length " + string.length + ")");
        }
        if (len < 0 || (string.length - offset) / 2 < len) {
            throw new IllegalArgumentException("Illegal length " + len);
        }
        try {
            return new String(string, offset, len * 2, "UTF-16LE");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String getFromUnicodeLE(final byte[] string) {
        if (string.length == 0) {
            return "";
        }
        return getFromUnicodeLE(string, 0, string.length / 2);
    }
    
    public static String getFromCompressedUnicode(final byte[] string, final int offset, final int len) {
        try {
            final int len_to_use = Math.min(len, string.length - offset);
            return new String(string, offset, len_to_use, "ISO-8859-1");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String readCompressedUnicode(final LittleEndianInput in, final int nChars) {
        final char[] buf = new char[nChars];
        for (int i = 0; i < buf.length; ++i) {
            buf[i] = (char)in.readUByte();
        }
        return new String(buf);
    }
    
    public static String readUnicodeString(final LittleEndianInput in) {
        final int nChars = in.readUShort();
        final byte flag = in.readByte();
        if ((flag & 0x1) == 0x0) {
            return readCompressedUnicode(in, nChars);
        }
        return readUnicodeLE(in, nChars);
    }
    
    public static String readUnicodeString(final LittleEndianInput in, final int nChars) {
        final byte is16Bit = in.readByte();
        if ((is16Bit & 0x1) == 0x0) {
            return readCompressedUnicode(in, nChars);
        }
        return readUnicodeLE(in, nChars);
    }
    
    public static void writeUnicodeString(final LittleEndianOutput out, final String value) {
        final int nChars = value.length();
        out.writeShort(nChars);
        final boolean is16Bit = hasMultibyte(value);
        out.writeByte(is16Bit ? 1 : 0);
        if (is16Bit) {
            putUnicodeLE(value, out);
        }
        else {
            putCompressedUnicode(value, out);
        }
    }
    
    public static void writeUnicodeStringFlagAndData(final LittleEndianOutput out, final String value) {
        final boolean is16Bit = hasMultibyte(value);
        out.writeByte(is16Bit ? 1 : 0);
        if (is16Bit) {
            putUnicodeLE(value, out);
        }
        else {
            putCompressedUnicode(value, out);
        }
    }
    
    public static int getEncodedSize(final String value) {
        int result = 3;
        result += value.length() * (hasMultibyte(value) ? 2 : 1);
        return result;
    }
    
    public static void putCompressedUnicode(final String input, final byte[] output, final int offset) {
        byte[] bytes;
        try {
            bytes = input.getBytes("ISO-8859-1");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        System.arraycopy(bytes, 0, output, offset, bytes.length);
    }
    
    public static void putCompressedUnicode(final String input, final LittleEndianOutput out) {
        byte[] bytes;
        try {
            bytes = input.getBytes("ISO-8859-1");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        out.write(bytes);
    }
    
    public static void putUnicodeLE(final String input, final byte[] output, final int offset) {
        byte[] bytes;
        try {
            bytes = input.getBytes("UTF-16LE");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        System.arraycopy(bytes, 0, output, offset, bytes.length);
    }
    
    public static void putUnicodeLE(final String input, final LittleEndianOutput out) {
        byte[] bytes;
        try {
            bytes = input.getBytes("UTF-16LE");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        out.write(bytes);
    }
    
    public static String readUnicodeLE(final LittleEndianInput in, final int nChars) {
        final char[] buf = new char[nChars];
        for (int i = 0; i < buf.length; ++i) {
            buf[i] = (char)in.readUShort();
        }
        return new String(buf);
    }
    
    public static String format(final String message, final Object[] params) {
        int currentParamNumber = 0;
        final StringBuffer formattedMessage = new StringBuffer();
        for (int i = 0; i < message.length(); ++i) {
            if (message.charAt(i) == '%') {
                if (currentParamNumber >= params.length) {
                    formattedMessage.append("?missing data?");
                }
                else if (params[currentParamNumber] instanceof Number && i + 1 < message.length()) {
                    i += matchOptionalFormatting((Number)params[currentParamNumber++], message.substring(i + 1), formattedMessage);
                }
                else {
                    formattedMessage.append(params[currentParamNumber++].toString());
                }
            }
            else if (message.charAt(i) == '\\' && i + 1 < message.length() && message.charAt(i + 1) == '%') {
                formattedMessage.append('%');
                ++i;
            }
            else {
                formattedMessage.append(message.charAt(i));
            }
        }
        return formattedMessage.toString();
    }
    
    private static int matchOptionalFormatting(final Number number, final String formatting, final StringBuffer outputTo) {
        final NumberFormat numberFormat = NumberFormat.getInstance();
        if (0 < formatting.length() && Character.isDigit(formatting.charAt(0))) {
            numberFormat.setMinimumIntegerDigits(Integer.parseInt(formatting.charAt(0) + ""));
            if (2 < formatting.length() && formatting.charAt(1) == '.' && Character.isDigit(formatting.charAt(2))) {
                numberFormat.setMaximumFractionDigits(Integer.parseInt(formatting.charAt(2) + ""));
                numberFormat.format(number, outputTo, new FieldPosition(0));
                return 3;
            }
            numberFormat.format(number, outputTo, new FieldPosition(0));
            return 1;
        }
        else {
            if (0 < formatting.length() && formatting.charAt(0) == '.' && 1 < formatting.length() && Character.isDigit(formatting.charAt(1))) {
                numberFormat.setMaximumFractionDigits(Integer.parseInt(formatting.charAt(1) + ""));
                numberFormat.format(number, outputTo, new FieldPosition(0));
                return 2;
            }
            numberFormat.format(number, outputTo, new FieldPosition(0));
            return 1;
        }
    }
    
    public static String getPreferredEncoding() {
        return "ISO-8859-1";
    }
    
    public static boolean hasMultibyte(final String value) {
        if (value == null) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            final char c = value.charAt(i);
            if (c > '\u00ff') {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isUnicodeString(final String value) {
        try {
            return !value.equals(new String(value.getBytes("ISO-8859-1"), "ISO-8859-1"));
        }
        catch (UnsupportedEncodingException e) {
            return true;
        }
    }
    
    public static class StringsIterator implements Iterator<String>
    {
        private String[] strings;
        private int position;
        
        public StringsIterator(final String[] strings) {
            this.position = 0;
            if (strings != null) {
                this.strings = strings;
            }
            else {
                this.strings = new String[0];
            }
        }
        
        public boolean hasNext() {
            return this.position < this.strings.length;
        }
        
        public String next() {
            final int ourPos = this.position++;
            if (ourPos >= this.strings.length) {
                throw new ArrayIndexOutOfBoundsException(ourPos);
            }
            return this.strings[ourPos];
        }
        
        public void remove() {
        }
    }
}
