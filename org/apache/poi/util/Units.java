// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.util;

public class Units
{
    public static final int EMU_PER_PIXEL = 9525;
    public static final int EMU_PER_POINT = 12700;
    
    public static int toEMU(final double value) {
        return (int)Math.round(12700.0 * value);
    }
    
    public static double toPoints(final long emu) {
        return emu / 12700.0;
    }
}
