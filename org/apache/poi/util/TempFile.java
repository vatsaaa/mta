// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.util;

import java.util.Random;
import java.io.File;

public final class TempFile
{
    private static File dir;
    private static final Random rnd;
    
    public static File createTempFile(final String prefix, final String suffix) {
        if (TempFile.dir == null) {
            (TempFile.dir = new File(System.getProperty("java.io.tmpdir"), "poifiles")).mkdir();
            if (System.getProperty("poi.keep.tmp.files") == null) {
                TempFile.dir.deleteOnExit();
            }
        }
        final File newFile = new File(TempFile.dir, prefix + TempFile.rnd.nextInt() + suffix);
        if (System.getProperty("poi.keep.tmp.files") == null) {
            newFile.deleteOnExit();
        }
        return newFile;
    }
    
    static {
        rnd = new Random();
    }
}
