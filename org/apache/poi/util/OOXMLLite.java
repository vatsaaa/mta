// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.util;

import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import junit.framework.Test;
import junit.textui.TestRunner;
import junit.framework.TestSuite;
import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.lang.reflect.Field;

public final class OOXMLLite
{
    private static final Field _classes;
    private File _destDest;
    private File _testDir;
    private File _ooxmlJar;
    
    OOXMLLite(final String dest, final String test, final String ooxmlJar) {
        this._destDest = new File(dest);
        this._testDir = new File(test);
        this._ooxmlJar = new File(ooxmlJar);
    }
    
    public static void main(final String[] args) throws IOException {
        String dest = null;
        String test = null;
        String ooxml = null;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-dest")) {
                dest = args[++i];
            }
            else if (args[i].equals("-test")) {
                test = args[++i];
            }
            else if (args[i].equals("-ooxml")) {
                ooxml = args[++i];
            }
        }
        final OOXMLLite builder = new OOXMLLite(dest, test, ooxml);
        builder.build();
    }
    
    void build() throws IOException {
        final List<String> lst = new ArrayList<String>();
        System.out.println("Collecting unit tests from " + this._testDir);
        collectTests(this._testDir, this._testDir, lst, ".+?\\.Test.+?\\.class$");
        final TestSuite suite = new TestSuite();
        for (final String arg : lst) {
            if (arg.indexOf(36) != -1) {
                continue;
            }
            final String cls = arg.replace(".class", "");
            try {
                final Class test = Class.forName(cls);
                suite.addTestSuite(test);
            }
            catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        TestRunner.run((Test)suite);
        System.out.println("Copying classes to " + this._destDest);
        final Map<String, Class<?>> classes = getLoadedClasses(this._ooxmlJar.getName());
        for (final Class<?> cls2 : classes.values()) {
            String className = cls2.getName();
            String classRef = className.replace('.', '/') + ".class";
            File destFile = new File(this._destDest, classRef);
            copyFile(cls2.getResourceAsStream('/' + classRef), destFile);
            if (cls2.isInterface()) {
                for (final Class fc : cls2.getDeclaredClasses()) {
                    className = fc.getName();
                    classRef = className.replace('.', '/') + ".class";
                    destFile = new File(this._destDest, classRef);
                    copyFile(fc.getResourceAsStream('/' + classRef), destFile);
                }
            }
        }
        System.out.println("Copying .xsb resources");
        final JarFile jar = new JarFile(this._ooxmlJar);
        final Enumeration<JarEntry> e2 = jar.entries();
        while (e2.hasMoreElements()) {
            final JarEntry je = e2.nextElement();
            if (je.getName().matches("schemaorg_apache_xmlbeans/system/\\w+/\\w+\\.xsb")) {
                final File destFile2 = new File(this._destDest, je.getName());
                copyFile(jar.getInputStream(je), destFile2);
            }
        }
        jar.close();
    }
    
    private static void collectTests(final File root, final File arg, final List<String> out, final String ptrn) {
        if (arg.isDirectory()) {
            for (final File f : arg.listFiles()) {
                collectTests(root, f, out, ptrn);
            }
        }
        else {
            final String path = arg.getAbsolutePath();
            final String prefix = root.getAbsolutePath();
            final String cls = path.substring(prefix.length() + 1).replace(File.separator, ".");
            if (cls.matches(ptrn)) {
                out.add(cls);
            }
        }
    }
    
    private static Map<String, Class<?>> getLoadedClasses(final String ptrn) {
        final ClassLoader appLoader = ClassLoader.getSystemClassLoader();
        try {
            final Vector<Class<?>> classes = (Vector<Class<?>>)OOXMLLite._classes.get(appLoader);
            final Map<String, Class<?>> map = new HashMap<String, Class<?>>();
            for (final Class<?> cls : classes) {
                final String jar = cls.getProtectionDomain().getCodeSource().getLocation().toString();
                if (jar.indexOf(ptrn) != -1) {
                    map.put(cls.getName(), cls);
                }
            }
            return map;
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
    
    private static void copyFile(final InputStream srcStream, final File destFile) throws IOException {
        final File destDirectory = destFile.getParentFile();
        destDirectory.mkdirs();
        final OutputStream destStream = new FileOutputStream(destFile);
        try {
            IOUtils.copy(srcStream, destStream);
        }
        finally {
            destStream.close();
        }
    }
    
    static {
        try {
            (_classes = ClassLoader.class.getDeclaredField("classes")).setAccessible(true);
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
