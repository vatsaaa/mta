// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import org.apache.poi.openxml4j.opc.OPCPackage;

public abstract class POIXMLTextExtractor extends POITextExtractor
{
    private final POIXMLDocument _document;
    
    public POIXMLTextExtractor(final POIXMLDocument document) {
        super((POIDocument)null);
        this._document = document;
    }
    
    public POIXMLProperties.CoreProperties getCoreProperties() {
        return this._document.getProperties().getCoreProperties();
    }
    
    public POIXMLProperties.ExtendedProperties getExtendedProperties() {
        return this._document.getProperties().getExtendedProperties();
    }
    
    public POIXMLProperties.CustomProperties getCustomProperties() {
        return this._document.getProperties().getCustomProperties();
    }
    
    public final POIXMLDocument getDocument() {
        return this._document;
    }
    
    public OPCPackage getPackage() {
        return this._document.getPackage();
    }
    
    @Override
    public POIXMLPropertiesTextExtractor getMetadataTextExtractor() {
        return new POIXMLPropertiesTextExtractor(this._document);
    }
}
