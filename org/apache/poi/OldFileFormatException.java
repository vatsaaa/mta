// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

public abstract class OldFileFormatException extends IllegalArgumentException
{
    public OldFileFormatException(final String s) {
        super(s);
    }
}
