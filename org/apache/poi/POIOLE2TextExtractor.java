// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.hpsf.extractor.HPSFPropertiesExtractor;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hpsf.DocumentSummaryInformation;

public abstract class POIOLE2TextExtractor extends POITextExtractor
{
    public POIOLE2TextExtractor(final POIDocument document) {
        super(document);
    }
    
    public DocumentSummaryInformation getDocSummaryInformation() {
        return this.document.getDocumentSummaryInformation();
    }
    
    public SummaryInformation getSummaryInformation() {
        return this.document.getSummaryInformation();
    }
    
    @Override
    public POITextExtractor getMetadataTextExtractor() {
        return new HPSFPropertiesExtractor(this);
    }
    
    public DirectoryEntry getRoot() {
        return this.document.directory;
    }
    
    @Deprecated
    public POIFSFileSystem getFileSystem() {
        return this.document.directory.getFileSystem();
    }
}
