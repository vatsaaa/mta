// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.dev;

import java.io.FileNotFoundException;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import java.io.Reader;
import javax.xml.transform.stream.StreamResult;
import java.util.Properties;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.FileReader;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class RecordGenerator
{
    public static void main(final String[] args) throws Exception {
        Class.forName("org.apache.poi.generator.FieldIterator");
        if (args.length != 4) {
            System.out.println("Usage:");
            System.out.println("  java org.apache.poi.hssf.util.RecordGenerator RECORD_DEFINTIONS RECORD_STYLES DEST_SRC_PATH TEST_SRC_PATH");
        }
        else {
            generateRecords(args[0], args[1], args[2], args[3]);
        }
    }
    
    private static void generateRecords(final String defintionsDir, final String recordStyleDir, final String destSrcPathDir, final String testSrcPathDir) throws Exception {
        final File definitionsFile = new File(defintionsDir);
        for (int i = 0; i < definitionsFile.listFiles().length; ++i) {
            final File file = definitionsFile.listFiles()[i];
            if (file.isFile() && (file.getName().endsWith("_record.xml") || file.getName().endsWith("_type.xml"))) {
                final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                final DocumentBuilder builder = factory.newDocumentBuilder();
                final Document document = builder.parse(file);
                final Element record = document.getDocumentElement();
                final String extendstg = record.getElementsByTagName("extends").item(0).getFirstChild().getNodeValue();
                final String suffix = record.getElementsByTagName("suffix").item(0).getFirstChild().getNodeValue();
                final String recordName = record.getAttributes().getNamedItem("name").getNodeValue();
                String packageName = record.getAttributes().getNamedItem("package").getNodeValue();
                packageName = packageName.replace('.', '/');
                String destinationPath = destSrcPathDir + "/" + packageName;
                File destinationPathFile = new File(destinationPath);
                destinationPathFile.mkdirs();
                String destinationFilepath = destinationPath + "/" + recordName + suffix + ".java";
                transform(file, new File(destinationFilepath), new File(recordStyleDir + "/" + extendstg.toLowerCase() + ".xsl"));
                System.out.println("Generated " + suffix + ": " + destinationFilepath);
                destinationPath = testSrcPathDir + "/" + packageName;
                destinationPathFile = new File(destinationPath);
                destinationPathFile.mkdirs();
                destinationFilepath = destinationPath + "/Test" + recordName + suffix + ".java";
                if (!new File(destinationFilepath).exists()) {
                    final String temp = recordStyleDir + "/" + extendstg.toLowerCase() + "_test.xsl";
                    transform(file, new File(destinationFilepath), new File(temp));
                    System.out.println("Generated test: " + destinationFilepath);
                }
                else {
                    System.out.println("Skipped test generation: " + destinationFilepath);
                }
            }
        }
    }
    
    private static void transform(final File in, final File out, final File xslt) throws FileNotFoundException, TransformerException {
        final Reader r = new FileReader(xslt);
        final StreamSource ss = new StreamSource(r);
        final TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t;
        try {
            t = tf.newTransformer(ss);
        }
        catch (TransformerException ex) {
            System.err.println("Error compiling XSL style sheet " + xslt);
            throw ex;
        }
        final Properties p = new Properties();
        p.setProperty("method", "text");
        t.setOutputProperties(p);
        final Result result = new StreamResult(out);
        t.transform(new StreamSource(in), result);
    }
}
