// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.filesystem;

import org.apache.poi.util.StringUtil;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Ole10Native
{
    private final int totalSize;
    private short flags1;
    private final String label;
    private final String fileName;
    private short flags2;
    private byte[] unknown1;
    private byte[] unknown2;
    private final String command;
    private final int dataSize;
    private final byte[] dataBuffer;
    private short flags3;
    public static final String OLE10_NATIVE = "\u0001Ole10Native";
    
    public static Ole10Native createFromEmbeddedOleObject(final POIFSFileSystem poifs) throws IOException, Ole10NativeException {
        return createFromEmbeddedOleObject(poifs.getRoot());
    }
    
    public static Ole10Native createFromEmbeddedOleObject(final DirectoryNode directory) throws IOException, Ole10NativeException {
        boolean plain = false;
        try {
            directory.getEntry("\u0001Ole10ItemName");
            plain = true;
        }
        catch (FileNotFoundException ex) {
            plain = false;
        }
        final DocumentEntry nativeEntry = (DocumentEntry)directory.getEntry("\u0001Ole10Native");
        final byte[] data = new byte[nativeEntry.getSize()];
        directory.createDocumentInputStream(nativeEntry).read(data);
        return new Ole10Native(data, 0, plain);
    }
    
    public Ole10Native(final byte[] data, final int offset) throws Ole10NativeException {
        this(data, offset, false);
    }
    
    public Ole10Native(final byte[] data, final int offset, final boolean plain) throws Ole10NativeException {
        int ofs = offset;
        if (data.length < offset + 2) {
            throw new Ole10NativeException("data is too small");
        }
        this.totalSize = LittleEndian.getInt(data, ofs);
        ofs += 4;
        if (plain) {
            System.arraycopy(data, 4, this.dataBuffer = new byte[this.totalSize - 4], 0, this.dataBuffer.length);
            this.dataSize = this.totalSize - 4;
            final byte[] oleLabel = new byte[8];
            System.arraycopy(this.dataBuffer, 0, oleLabel, 0, Math.min(this.dataBuffer.length, 8));
            this.label = "ole-" + HexDump.toHex(oleLabel);
            this.fileName = this.label;
            this.command = this.label;
        }
        else {
            this.flags1 = LittleEndian.getShort(data, ofs);
            ofs += 2;
            int len = getStringLength(data, ofs);
            this.label = StringUtil.getFromCompressedUnicode(data, ofs, len - 1);
            ofs += len;
            len = getStringLength(data, ofs);
            this.fileName = StringUtil.getFromCompressedUnicode(data, ofs, len - 1);
            ofs += len;
            this.flags2 = LittleEndian.getShort(data, ofs);
            ofs += 2;
            len = LittleEndian.getUByte(data, ofs);
            this.unknown1 = new byte[len];
            ofs += len;
            len = 3;
            this.unknown2 = new byte[len];
            ofs += len;
            len = getStringLength(data, ofs);
            this.command = StringUtil.getFromCompressedUnicode(data, ofs, len - 1);
            ofs += len;
            if (this.totalSize + 4 - ofs <= 4) {
                throw new Ole10NativeException("Invalid Ole10Native");
            }
            this.dataSize = LittleEndian.getInt(data, ofs);
            ofs += 4;
            if (this.dataSize > this.totalSize || this.dataSize < 0) {
                throw new Ole10NativeException("Invalid Ole10Native");
            }
            System.arraycopy(data, ofs, this.dataBuffer = new byte[this.dataSize], 0, this.dataSize);
            ofs += this.dataSize;
            if (this.unknown1.length > 0) {
                this.flags3 = LittleEndian.getShort(data, ofs);
                ofs += 2;
            }
            else {
                this.flags3 = 0;
            }
        }
    }
    
    private static int getStringLength(final byte[] data, final int ofs) {
        int len;
        for (len = 0; len + ofs < data.length && data[ofs + len] != 0; ++len) {}
        return ++len;
    }
    
    public int getTotalSize() {
        return this.totalSize;
    }
    
    public short getFlags1() {
        return this.flags1;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public String getFileName() {
        return this.fileName;
    }
    
    public short getFlags2() {
        return this.flags2;
    }
    
    public byte[] getUnknown1() {
        return this.unknown1;
    }
    
    public byte[] getUnknown2() {
        return this.unknown2;
    }
    
    public String getCommand() {
        return this.command;
    }
    
    public int getDataSize() {
        return this.dataSize;
    }
    
    public byte[] getDataBuffer() {
        return this.dataBuffer;
    }
    
    public short getFlags3() {
        return this.flags3;
    }
}
