// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.filesystem;

import java.io.IOException;
import java.util.Iterator;
import java.nio.ByteBuffer;

public class NPOIFSStream implements Iterable<ByteBuffer>
{
    private BlockStore blockStore;
    private int startBlock;
    
    public NPOIFSStream(final BlockStore blockStore, final int startBlock) {
        this.blockStore = blockStore;
        this.startBlock = startBlock;
    }
    
    public NPOIFSStream(final BlockStore blockStore) {
        this.blockStore = blockStore;
        this.startBlock = -2;
    }
    
    public int getStartBlock() {
        return this.startBlock;
    }
    
    public Iterator<ByteBuffer> iterator() {
        return this.getBlockIterator();
    }
    
    public Iterator<ByteBuffer> getBlockIterator() {
        if (this.startBlock == -2) {
            throw new IllegalStateException("Can't read from a new stream before it has been written to");
        }
        return new StreamBlockByteBufferIterator(this.startBlock);
    }
    
    public void updateContents(final byte[] contents) throws IOException {
        final int blockSize = this.blockStore.getBlockStoreBlockSize();
        final int blocks = (int)Math.ceil(contents.length / (double)blockSize);
        final BlockStore.ChainLoopDetector loopDetector = this.blockStore.getChainLoopDetector();
        int prevBlock = -2;
        int nextBlock = this.startBlock;
        for (int i = 0; i < blocks; ++i) {
            int thisBlock = nextBlock;
            if (thisBlock == -2) {
                thisBlock = this.blockStore.getFreeBlock();
                loopDetector.claim(thisBlock);
                nextBlock = -2;
                if (prevBlock != -2) {
                    this.blockStore.setNextBlock(prevBlock, thisBlock);
                }
                this.blockStore.setNextBlock(thisBlock, -2);
                if (this.startBlock == -2) {
                    this.startBlock = thisBlock;
                }
            }
            else {
                loopDetector.claim(thisBlock);
                nextBlock = this.blockStore.getNextBlock(thisBlock);
            }
            final ByteBuffer buffer = this.blockStore.createBlockIfNeeded(thisBlock);
            final int startAt = i * blockSize;
            final int endAt = Math.min(contents.length - startAt, blockSize);
            buffer.put(contents, startAt, endAt);
            prevBlock = thisBlock;
        }
        final int lastBlock = prevBlock;
        final NPOIFSStream toFree = new NPOIFSStream(this.blockStore, nextBlock);
        toFree.free(loopDetector);
        this.blockStore.setNextBlock(lastBlock, -2);
    }
    
    public void free() throws IOException {
        final BlockStore.ChainLoopDetector loopDetector = this.blockStore.getChainLoopDetector();
        this.free(loopDetector);
    }
    
    private void free(final BlockStore.ChainLoopDetector loopDetector) {
        int nextBlock = this.startBlock;
        while (nextBlock != -2) {
            final int thisBlock = nextBlock;
            loopDetector.claim(thisBlock);
            nextBlock = this.blockStore.getNextBlock(thisBlock);
            this.blockStore.setNextBlock(thisBlock, -1);
        }
        this.startBlock = -2;
    }
    
    protected class StreamBlockByteBufferIterator implements Iterator<ByteBuffer>
    {
        private BlockStore.ChainLoopDetector loopDetector;
        private int nextBlock;
        
        protected StreamBlockByteBufferIterator(final int firstBlock) {
            this.nextBlock = firstBlock;
            try {
                this.loopDetector = NPOIFSStream.this.blockStore.getChainLoopDetector();
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        
        public boolean hasNext() {
            return this.nextBlock != -2;
        }
        
        public ByteBuffer next() {
            if (this.nextBlock == -2) {
                throw new IndexOutOfBoundsException("Can't read past the end of the stream");
            }
            try {
                this.loopDetector.claim(this.nextBlock);
                final ByteBuffer data = NPOIFSStream.this.blockStore.getBlockAt(this.nextBlock);
                this.nextBlock = NPOIFSStream.this.blockStore.getNextBlock(this.nextBlock);
                return data;
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
