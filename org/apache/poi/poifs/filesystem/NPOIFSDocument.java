// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.filesystem;

import org.apache.poi.util.HexDump;
import java.util.List;
import java.util.Collections;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.io.OutputStream;
import org.apache.poi.util.IOUtils;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.poifs.property.DocumentProperty;
import org.apache.poi.poifs.dev.POIFSViewable;

public final class NPOIFSDocument implements POIFSViewable
{
    private DocumentProperty _property;
    private NPOIFSFileSystem _filesystem;
    private NPOIFSStream _stream;
    private int _block_size;
    
    public NPOIFSDocument(final DocumentProperty property, final NPOIFSFileSystem filesystem) throws IOException {
        this._property = property;
        this._filesystem = filesystem;
        if (property.getSize() < 4096) {
            this._stream = new NPOIFSStream(this._filesystem.getMiniStore(), property.getStartBlock());
            this._block_size = this._filesystem.getMiniStore().getBlockStoreBlockSize();
        }
        else {
            this._stream = new NPOIFSStream(this._filesystem, property.getStartBlock());
            this._block_size = this._filesystem.getBlockStoreBlockSize();
        }
    }
    
    public NPOIFSDocument(final String name, final NPOIFSFileSystem filesystem, final InputStream stream) throws IOException {
        this._filesystem = filesystem;
        byte[] contents;
        if (stream instanceof ByteArrayInputStream) {
            final ByteArrayInputStream bais = (ByteArrayInputStream)stream;
            contents = new byte[bais.available()];
            bais.read(contents);
        }
        else {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(stream, baos);
            contents = baos.toByteArray();
        }
        if (contents.length <= 4096) {
            this._stream = new NPOIFSStream(filesystem.getMiniStore());
            this._block_size = this._filesystem.getMiniStore().getBlockStoreBlockSize();
        }
        else {
            this._stream = new NPOIFSStream(filesystem);
            this._block_size = this._filesystem.getBlockStoreBlockSize();
        }
        this._stream.updateContents(contents);
        (this._property = new DocumentProperty(name, contents.length)).setStartBlock(this._stream.getStartBlock());
    }
    
    int getDocumentBlockSize() {
        return this._block_size;
    }
    
    Iterator<ByteBuffer> getBlockIterator() {
        if (this.getSize() > 0) {
            return this._stream.getBlockIterator();
        }
        final List<ByteBuffer> empty = Collections.emptyList();
        return empty.iterator();
    }
    
    public int getSize() {
        return this._property.getSize();
    }
    
    DocumentProperty getDocumentProperty() {
        return this._property;
    }
    
    public Object[] getViewableArray() {
        final Object[] results = { null };
        String result;
        try {
            if (this.getSize() > 0) {
                final byte[] data = new byte[this.getSize()];
                int offset = 0;
                for (final ByteBuffer buffer : this._stream) {
                    final int length = Math.min(this._block_size, data.length - offset);
                    buffer.get(data, offset, length);
                    offset += length;
                }
                final ByteArrayOutputStream output = new ByteArrayOutputStream();
                HexDump.dump(data, 0L, output, 0);
                result = output.toString();
            }
            else {
                result = "<NO DATA>";
            }
        }
        catch (IOException e) {
            result = e.getMessage();
        }
        results[0] = result;
        return results;
    }
    
    public Iterator getViewableIterator() {
        return Collections.EMPTY_LIST.iterator();
    }
    
    public boolean preferArray() {
        return true;
    }
    
    public String getShortDescription() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("Document: \"").append(this._property.getName()).append("\"");
        buffer.append(" size = ").append(this.getSize());
        return buffer.toString();
    }
}
