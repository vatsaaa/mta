// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.nio;

import java.nio.channels.WritableByteChannel;
import java.nio.channels.Channels;
import java.io.OutputStream;
import java.io.IOException;
import java.nio.channels.ReadableByteChannel;
import org.apache.poi.util.IOUtils;
import java.nio.ByteBuffer;
import java.io.RandomAccessFile;
import java.io.FileNotFoundException;
import java.io.File;
import java.nio.channels.FileChannel;

public class FileBackedDataSource extends DataSource
{
    private FileChannel channel;
    
    public FileBackedDataSource(final File file) throws FileNotFoundException {
        if (!file.exists()) {
            throw new FileNotFoundException(file.toString());
        }
        this.channel = new RandomAccessFile(file, "r").getChannel();
    }
    
    public FileBackedDataSource(final FileChannel channel) {
        this.channel = channel;
    }
    
    @Override
    public ByteBuffer read(final int length, final long position) throws IOException {
        if (position >= this.size()) {
            throw new IllegalArgumentException("Position " + position + " past the end of the file");
        }
        this.channel.position(position);
        final ByteBuffer dst = ByteBuffer.allocate(length);
        final int worked = IOUtils.readFully(this.channel, dst);
        if (worked == -1) {
            throw new IllegalArgumentException("Position " + position + " past the end of the file");
        }
        dst.position(0);
        return dst;
    }
    
    @Override
    public void write(final ByteBuffer src, final long position) throws IOException {
        this.channel.write(src, position);
    }
    
    @Override
    public void copyTo(final OutputStream stream) throws IOException {
        final WritableByteChannel out = Channels.newChannel(stream);
        this.channel.transferTo(0L, this.channel.size(), out);
    }
    
    @Override
    public long size() throws IOException {
        return this.channel.size();
    }
    
    @Override
    public void close() throws IOException {
        this.channel.close();
    }
}
