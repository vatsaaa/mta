// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.crypt;

import java.security.NoSuchAlgorithmException;
import org.apache.poi.util.LittleEndian;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.EncryptedDocumentException;
import java.security.GeneralSecurityException;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;

public abstract class Decryptor
{
    public static final String DEFAULT_PASSWORD = "VelvetSweatshop";
    
    public abstract InputStream getDataStream(final DirectoryNode p0) throws IOException, GeneralSecurityException;
    
    public abstract boolean verifyPassword(final String p0) throws GeneralSecurityException;
    
    public abstract long getLength();
    
    public static Decryptor getInstance(final EncryptionInfo info) {
        final int major = info.getVersionMajor();
        final int minor = info.getVersionMinor();
        if (major == 4 && minor == 4) {
            return new AgileDecryptor(info);
        }
        if (minor == 2 && (major == 3 || major == 4)) {
            return new EcmaDecryptor(info);
        }
        throw new EncryptedDocumentException("Unsupported version");
    }
    
    public InputStream getDataStream(final NPOIFSFileSystem fs) throws IOException, GeneralSecurityException {
        return this.getDataStream(fs.getRoot());
    }
    
    public InputStream getDataStream(final POIFSFileSystem fs) throws IOException, GeneralSecurityException {
        return this.getDataStream(fs.getRoot());
    }
    
    protected static int getBlockSize(final int algorithm) {
        switch (algorithm) {
            case 26126: {
                return 16;
            }
            case 26127: {
                return 24;
            }
            case 26128: {
                return 32;
            }
            default: {
                throw new EncryptedDocumentException("Unknown block size");
            }
        }
    }
    
    protected byte[] hashPassword(final EncryptionInfo info, final String password) throws NoSuchAlgorithmException {
        final MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] bytes;
        try {
            bytes = password.getBytes("UTF-16LE");
        }
        catch (UnsupportedEncodingException e) {
            throw new EncryptedDocumentException("UTF16 not supported");
        }
        sha1.update(info.getVerifier().getSalt());
        byte[] hash = sha1.digest(bytes);
        final byte[] iterator = new byte[4];
        for (int i = 0; i < info.getVerifier().getSpinCount(); ++i) {
            sha1.reset();
            LittleEndian.putInt(iterator, 0, i);
            sha1.update(iterator);
            hash = sha1.digest(hash);
        }
        return hash;
    }
}
