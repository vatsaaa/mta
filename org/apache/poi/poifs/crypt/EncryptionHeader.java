// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.crypt;

import org.w3c.dom.NamedNodeMap;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.EncryptedDocumentException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.DocumentInputStream;

public class EncryptionHeader
{
    public static final int ALGORITHM_RC4 = 26625;
    public static final int ALGORITHM_AES_128 = 26126;
    public static final int ALGORITHM_AES_192 = 26127;
    public static final int ALGORITHM_AES_256 = 26128;
    public static final int HASH_SHA1 = 32772;
    public static final int PROVIDER_RC4 = 1;
    public static final int PROVIDER_AES = 24;
    public static final int MODE_ECB = 1;
    public static final int MODE_CBC = 2;
    public static final int MODE_CFB = 3;
    private final int flags;
    private final int sizeExtra;
    private final int algorithm;
    private final int hashAlgorithm;
    private final int keySize;
    private final int providerType;
    private final int cipherMode;
    private final byte[] keySalt;
    private final String cspName;
    
    public EncryptionHeader(final DocumentInputStream is) throws IOException {
        this.flags = is.readInt();
        this.sizeExtra = is.readInt();
        this.algorithm = is.readInt();
        this.hashAlgorithm = is.readInt();
        this.keySize = is.readInt();
        this.providerType = is.readInt();
        is.readLong();
        final StringBuilder builder = new StringBuilder();
        while (true) {
            final char c = (char)is.readShort();
            if (c == '\0') {
                break;
            }
            builder.append(c);
        }
        this.cspName = builder.toString();
        this.cipherMode = 1;
        this.keySalt = null;
    }
    
    public EncryptionHeader(final String descriptor) throws IOException {
        NamedNodeMap keyData;
        try {
            final ByteArrayInputStream is = new ByteArrayInputStream(descriptor.getBytes());
            keyData = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getElementsByTagName("keyData").item(0).getAttributes();
        }
        catch (Exception e) {
            throw new EncryptedDocumentException("Unable to parse keyData");
        }
        this.keySize = Integer.parseInt(keyData.getNamedItem("keyBits").getNodeValue());
        this.flags = 0;
        this.sizeExtra = 0;
        this.cspName = null;
        final int blockSize = Integer.parseInt(keyData.getNamedItem("blockSize").getNodeValue());
        final String cipher = keyData.getNamedItem("cipherAlgorithm").getNodeValue();
        if (!"AES".equals(cipher)) {
            throw new EncryptedDocumentException("Unsupported cipher");
        }
        this.providerType = 24;
        if (blockSize == 16) {
            this.algorithm = 26126;
        }
        else if (blockSize == 24) {
            this.algorithm = 26127;
        }
        else {
            if (blockSize != 32) {
                throw new EncryptedDocumentException("Unsupported key length");
            }
            this.algorithm = 26128;
        }
        final String chaining = keyData.getNamedItem("cipherChaining").getNodeValue();
        if ("ChainingModeCBC".equals(chaining)) {
            this.cipherMode = 2;
        }
        else {
            if (!"ChainingModeCFB".equals(chaining)) {
                throw new EncryptedDocumentException("Unsupported chaining mode");
            }
            this.cipherMode = 3;
        }
        final String hashAlg = keyData.getNamedItem("hashAlgorithm").getNodeValue();
        final int hashSize = Integer.parseInt(keyData.getNamedItem("hashSize").getNodeValue());
        if (!"SHA1".equals(hashAlg) || hashSize != 20) {
            throw new EncryptedDocumentException("Unsupported hash algorithm");
        }
        this.hashAlgorithm = 32772;
        final String salt = keyData.getNamedItem("saltValue").getNodeValue();
        final int saltLength = Integer.parseInt(keyData.getNamedItem("saltSize").getNodeValue());
        this.keySalt = Base64.decodeBase64(salt.getBytes());
        if (this.keySalt.length != saltLength) {
            throw new EncryptedDocumentException("Invalid salt length");
        }
    }
    
    public int getCipherMode() {
        return this.cipherMode;
    }
    
    public int getFlags() {
        return this.flags;
    }
    
    public int getSizeExtra() {
        return this.sizeExtra;
    }
    
    public int getAlgorithm() {
        return this.algorithm;
    }
    
    public int getHashAlgorithm() {
        return this.hashAlgorithm;
    }
    
    public int getKeySize() {
        return this.keySize;
    }
    
    public byte[] getKeySalt() {
        return this.keySalt;
    }
    
    public int getProviderType() {
        return this.providerType;
    }
    
    public String getCspName() {
        return this.cspName;
    }
}
