// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.crypt;

import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.EncryptedDocumentException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class EncryptionVerifier
{
    private final byte[] salt;
    private final byte[] verifier;
    private final byte[] verifierHash;
    private final byte[] encryptedKey;
    private final int verifierHashSize;
    private final int spinCount;
    private final int algorithm;
    private final int cipherMode;
    
    public EncryptionVerifier(final String descriptor) {
        NamedNodeMap keyData = null;
        try {
            final ByteArrayInputStream is = new ByteArrayInputStream(descriptor.getBytes());
            final NodeList keyEncryptor = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getElementsByTagName("keyEncryptor").item(0).getChildNodes();
            for (int i = 0; i < keyEncryptor.getLength(); ++i) {
                final Node node = keyEncryptor.item(i);
                if (node.getNodeName().equals("p:encryptedKey")) {
                    keyData = node.getAttributes();
                    break;
                }
            }
            if (keyData == null) {
                throw new EncryptedDocumentException("");
            }
        }
        catch (Exception e) {
            throw new EncryptedDocumentException("Unable to parse keyEncryptor");
        }
        this.spinCount = Integer.parseInt(keyData.getNamedItem("spinCount").getNodeValue());
        this.verifier = Base64.decodeBase64(keyData.getNamedItem("encryptedVerifierHashInput").getNodeValue().getBytes());
        this.salt = Base64.decodeBase64(keyData.getNamedItem("saltValue").getNodeValue().getBytes());
        this.encryptedKey = Base64.decodeBase64(keyData.getNamedItem("encryptedKeyValue").getNodeValue().getBytes());
        final int saltSize = Integer.parseInt(keyData.getNamedItem("saltSize").getNodeValue());
        if (saltSize != this.salt.length) {
            throw new EncryptedDocumentException("Invalid salt size");
        }
        this.verifierHash = Base64.decodeBase64(keyData.getNamedItem("encryptedVerifierHashValue").getNodeValue().getBytes());
        final int blockSize = Integer.parseInt(keyData.getNamedItem("blockSize").getNodeValue());
        final String alg = keyData.getNamedItem("cipherAlgorithm").getNodeValue();
        if ("AES".equals(alg)) {
            if (blockSize == 16) {
                this.algorithm = 26126;
            }
            else if (blockSize == 24) {
                this.algorithm = 26127;
            }
            else {
                if (blockSize != 32) {
                    throw new EncryptedDocumentException("Unsupported block size");
                }
                this.algorithm = 26128;
            }
            final String chain = keyData.getNamedItem("cipherChaining").getNodeValue();
            if ("ChainingModeCBC".equals(chain)) {
                this.cipherMode = 2;
            }
            else {
                if (!"ChainingModeCFB".equals(chain)) {
                    throw new EncryptedDocumentException("Unsupported chaining mode");
                }
                this.cipherMode = 3;
            }
            this.verifierHashSize = Integer.parseInt(keyData.getNamedItem("hashSize").getNodeValue());
            return;
        }
        throw new EncryptedDocumentException("Unsupported cipher");
    }
    
    public EncryptionVerifier(final DocumentInputStream is, final int encryptedLength) {
        final int saltSize = is.readInt();
        if (saltSize != 16) {
            throw new RuntimeException("Salt size != 16 !?");
        }
        is.readFully(this.salt = new byte[16]);
        is.readFully(this.verifier = new byte[16]);
        this.verifierHashSize = is.readInt();
        is.readFully(this.verifierHash = new byte[encryptedLength]);
        this.spinCount = 50000;
        this.algorithm = 26126;
        this.cipherMode = 1;
        this.encryptedKey = null;
    }
    
    public byte[] getSalt() {
        return this.salt;
    }
    
    public byte[] getVerifier() {
        return this.verifier;
    }
    
    public byte[] getVerifierHash() {
        return this.verifierHash;
    }
    
    public int getSpinCount() {
        return this.spinCount;
    }
    
    public int getCipherMode() {
        return this.cipherMode;
    }
    
    public int getAlgorithm() {
        return this.algorithm;
    }
    
    public byte[] getEncryptedKey() {
        return this.encryptedKey;
    }
}
