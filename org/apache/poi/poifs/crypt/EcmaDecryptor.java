// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.crypt;

import java.io.IOException;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import javax.crypto.CipherInputStream;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import javax.crypto.SecretKey;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import org.apache.poi.util.LittleEndian;
import java.security.MessageDigest;

public class EcmaDecryptor extends Decryptor
{
    private final EncryptionInfo info;
    private byte[] passwordHash;
    private long _length;
    
    public EcmaDecryptor(final EncryptionInfo info) {
        this._length = -1L;
        this.info = info;
    }
    
    private byte[] generateKey(final int block) throws NoSuchAlgorithmException {
        final MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        sha1.update(this.passwordHash);
        final byte[] blockValue = new byte[4];
        LittleEndian.putInt(blockValue, 0, block);
        final byte[] finalHash = sha1.digest(blockValue);
        final int requiredKeyLength = this.info.getHeader().getKeySize() / 8;
        final byte[] buff = new byte[64];
        Arrays.fill(buff, (byte)54);
        for (int i = 0; i < finalHash.length; ++i) {
            buff[i] ^= finalHash[i];
        }
        sha1.reset();
        final byte[] x1 = sha1.digest(buff);
        Arrays.fill(buff, (byte)92);
        for (int j = 0; j < finalHash.length; ++j) {
            buff[j] ^= finalHash[j];
        }
        sha1.reset();
        final byte[] x2 = sha1.digest(buff);
        final byte[] x3 = new byte[x1.length + x2.length];
        System.arraycopy(x1, 0, x3, 0, x1.length);
        System.arraycopy(x2, 0, x3, x1.length, x2.length);
        return this.truncateOrPad(x3, requiredKeyLength);
    }
    
    @Override
    public boolean verifyPassword(final String password) throws GeneralSecurityException {
        this.passwordHash = this.hashPassword(this.info, password);
        final Cipher cipher = this.getCipher();
        final byte[] verifier = cipher.doFinal(this.info.getVerifier().getVerifier());
        final MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        final byte[] calcVerifierHash = sha1.digest(verifier);
        final byte[] verifierHash = this.truncateOrPad(cipher.doFinal(this.info.getVerifier().getVerifierHash()), calcVerifierHash.length);
        return Arrays.equals(calcVerifierHash, verifierHash);
    }
    
    private byte[] truncateOrPad(final byte[] source, final int length) {
        final byte[] result = new byte[length];
        System.arraycopy(source, 0, result, 0, Math.min(length, source.length));
        if (length > source.length) {
            for (int i = source.length; i < length; ++i) {
                result[i] = 0;
            }
        }
        return result;
    }
    
    private Cipher getCipher() throws GeneralSecurityException {
        final byte[] key = this.generateKey(0);
        final Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        final SecretKey skey = new SecretKeySpec(key, "AES");
        cipher.init(2, skey);
        return cipher;
    }
    
    @Override
    public InputStream getDataStream(final DirectoryNode dir) throws IOException, GeneralSecurityException {
        final DocumentInputStream dis = dir.createDocumentInputStream("EncryptedPackage");
        this._length = dis.readLong();
        return new CipherInputStream(dis, this.getCipher());
    }
    
    @Override
    public long getLength() {
        if (this._length == -1L) {
            throw new IllegalStateException("EcmaDecryptor.getDataStream() was not called");
        }
        return this._length;
    }
}
