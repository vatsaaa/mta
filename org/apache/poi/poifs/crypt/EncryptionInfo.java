// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.crypt;

import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class EncryptionInfo
{
    private final int versionMajor;
    private final int versionMinor;
    private final int encryptionFlags;
    private final EncryptionHeader header;
    private final EncryptionVerifier verifier;
    
    public EncryptionInfo(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public EncryptionInfo(final NPOIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public EncryptionInfo(final DirectoryNode dir) throws IOException {
        final DocumentInputStream dis = dir.createDocumentInputStream("EncryptionInfo");
        this.versionMajor = dis.readShort();
        this.versionMinor = dis.readShort();
        this.encryptionFlags = dis.readInt();
        if (this.versionMajor == 4 && this.versionMinor == 4 && this.encryptionFlags == 64) {
            final StringBuilder builder = new StringBuilder();
            final byte[] xmlDescriptor = new byte[dis.available()];
            dis.read(xmlDescriptor);
            for (final byte b : xmlDescriptor) {
                builder.append((char)b);
            }
            final String descriptor = builder.toString();
            this.header = new EncryptionHeader(descriptor);
            this.verifier = new EncryptionVerifier(descriptor);
        }
        else {
            final int hSize = dis.readInt();
            this.header = new EncryptionHeader(dis);
            if (this.header.getAlgorithm() == 26625) {
                this.verifier = new EncryptionVerifier(dis, 20);
            }
            else {
                this.verifier = new EncryptionVerifier(dis, 32);
            }
        }
    }
    
    public int getVersionMajor() {
        return this.versionMajor;
    }
    
    public int getVersionMinor() {
        return this.versionMinor;
    }
    
    public int getEncryptionFlags() {
        return this.encryptionFlags;
    }
    
    public EncryptionHeader getHeader() {
        return this.header;
    }
    
    public EncryptionVerifier getVerifier() {
        return this.verifier;
    }
}
