// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.poifs.crypt;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.EncryptedDocumentException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import java.util.Arrays;
import java.security.MessageDigest;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.SecretKey;

public class AgileDecryptor extends Decryptor
{
    private final EncryptionInfo _info;
    private SecretKey _secretKey;
    private long _length;
    private static final byte[] kVerifierInputBlock;
    private static final byte[] kHashedVerifierBlock;
    private static final byte[] kCryptoKeyBlock;
    
    @Override
    public boolean verifyPassword(final String password) throws GeneralSecurityException {
        final EncryptionVerifier verifier = this._info.getVerifier();
        final int algorithm = verifier.getAlgorithm();
        final int mode = verifier.getCipherMode();
        final byte[] pwHash = this.hashPassword(this._info, password);
        byte[] iv = this.generateIv(algorithm, verifier.getSalt(), null);
        SecretKey skey = new SecretKeySpec(this.generateKey(pwHash, AgileDecryptor.kVerifierInputBlock), "AES");
        Cipher cipher = this.getCipher(algorithm, mode, skey, iv);
        final byte[] verifierHashInput = cipher.doFinal(verifier.getVerifier());
        final MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] trimmed = new byte[verifier.getSalt().length];
        System.arraycopy(verifierHashInput, 0, trimmed, 0, trimmed.length);
        final byte[] hashedVerifier = sha1.digest(trimmed);
        skey = new SecretKeySpec(this.generateKey(pwHash, AgileDecryptor.kHashedVerifierBlock), "AES");
        iv = this.generateIv(algorithm, verifier.getSalt(), null);
        cipher = this.getCipher(algorithm, mode, skey, iv);
        final byte[] verifierHash = cipher.doFinal(verifier.getVerifierHash());
        trimmed = new byte[hashedVerifier.length];
        System.arraycopy(verifierHash, 0, trimmed, 0, trimmed.length);
        if (Arrays.equals(trimmed, hashedVerifier)) {
            skey = new SecretKeySpec(this.generateKey(pwHash, AgileDecryptor.kCryptoKeyBlock), "AES");
            iv = this.generateIv(algorithm, verifier.getSalt(), null);
            cipher = this.getCipher(algorithm, mode, skey, iv);
            final byte[] inter = cipher.doFinal(verifier.getEncryptedKey());
            final byte[] keyspec = new byte[this._info.getHeader().getKeySize() / 8];
            System.arraycopy(inter, 0, keyspec, 0, keyspec.length);
            this._secretKey = new SecretKeySpec(keyspec, "AES");
            return true;
        }
        return false;
    }
    
    @Override
    public InputStream getDataStream(final DirectoryNode dir) throws IOException, GeneralSecurityException {
        final DocumentInputStream dis = dir.createDocumentInputStream("EncryptedPackage");
        this._length = dis.readLong();
        return new ChunkedCipherInputStream(dis, this._length);
    }
    
    @Override
    public long getLength() {
        if (this._length == -1L) {
            throw new IllegalStateException("EcmaDecryptor.getDataStream() was not called");
        }
        return this._length;
    }
    
    protected AgileDecryptor(final EncryptionInfo info) {
        this._length = -1L;
        this._info = info;
    }
    
    private Cipher getCipher(final int algorithm, final int mode, final SecretKey key, final byte[] vec) throws GeneralSecurityException {
        String name = null;
        String chain = null;
        if (algorithm == 26126 || algorithm == 26127 || algorithm == 26128) {
            name = "AES";
        }
        if (mode == 2) {
            chain = "CBC";
        }
        else if (mode == 3) {
            chain = "CFB";
        }
        final Cipher cipher = Cipher.getInstance(name + "/" + chain + "/NoPadding");
        final IvParameterSpec iv = new IvParameterSpec(vec);
        cipher.init(2, key, iv);
        return cipher;
    }
    
    private byte[] getBlock(final int algorithm, final byte[] hash) {
        final byte[] result = new byte[Decryptor.getBlockSize(algorithm)];
        Arrays.fill(result, (byte)54);
        System.arraycopy(hash, 0, result, 0, Math.min(result.length, hash.length));
        return result;
    }
    
    private byte[] generateKey(final byte[] hash, final byte[] blockKey) throws NoSuchAlgorithmException {
        final MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        sha1.update(hash);
        return this.getBlock(this._info.getVerifier().getAlgorithm(), sha1.digest(blockKey));
    }
    
    protected byte[] generateIv(final int algorithm, final byte[] salt, final byte[] blockKey) throws NoSuchAlgorithmException {
        if (blockKey == null) {
            return this.getBlock(algorithm, salt);
        }
        final MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        sha1.update(salt);
        return this.getBlock(algorithm, sha1.digest(blockKey));
    }
    
    static {
        kVerifierInputBlock = new byte[] { -2, -89, -46, 118, 59, 75, -98, 121 };
        kHashedVerifierBlock = new byte[] { -41, -86, 15, 109, 48, 97, 52, 78 };
        kCryptoKeyBlock = new byte[] { 20, 110, 11, -25, -85, -84, -48, -42 };
    }
    
    private class ChunkedCipherInputStream extends InputStream
    {
        private int _lastIndex;
        private long _pos;
        private final long _size;
        private final DocumentInputStream _stream;
        private byte[] _chunk;
        private Cipher _cipher;
        
        public ChunkedCipherInputStream(final DocumentInputStream stream, final long size) throws GeneralSecurityException {
            this._lastIndex = 0;
            this._pos = 0L;
            this._size = size;
            this._stream = stream;
            this._cipher = AgileDecryptor.this.getCipher(AgileDecryptor.this._info.getHeader().getAlgorithm(), AgileDecryptor.this._info.getHeader().getCipherMode(), AgileDecryptor.this._secretKey, AgileDecryptor.this._info.getHeader().getKeySalt());
        }
        
        @Override
        public int read() throws IOException {
            final byte[] b = { 0 };
            if (this.read(b) == 1) {
                return b[0];
            }
            return -1;
        }
        
        @Override
        public int read(final byte[] b) throws IOException {
            return this.read(b, 0, b.length);
        }
        
        @Override
        public int read(final byte[] b, int off, int len) throws IOException {
            int total = 0;
            while (len > 0) {
                if (this._chunk == null) {
                    try {
                        this._chunk = this.nextChunk();
                    }
                    catch (GeneralSecurityException e) {
                        throw new EncryptedDocumentException(e.getMessage());
                    }
                }
                int count = (int)(4096L - (this._pos & 0xFFFL));
                count = Math.min(this.available(), Math.min(count, len));
                System.arraycopy(this._chunk, (int)(this._pos & 0xFFFL), b, off, count);
                off += count;
                len -= count;
                this._pos += count;
                if ((this._pos & 0xFFFL) == 0x0L) {
                    this._chunk = null;
                }
                total += count;
            }
            return total;
        }
        
        @Override
        public long skip(final long n) throws IOException {
            final long start = this._pos;
            final long skip = Math.min(this.available(), n);
            if (((this._pos + skip ^ start) & 0xFFFFFFFFFFFFF000L) != 0x0L) {
                this._chunk = null;
            }
            this._pos += skip;
            return skip;
        }
        
        @Override
        public int available() throws IOException {
            return (int)(this._size - this._pos);
        }
        
        @Override
        public void close() throws IOException {
            this._stream.close();
        }
        
        @Override
        public boolean markSupported() {
            return false;
        }
        
        private byte[] nextChunk() throws GeneralSecurityException, IOException {
            final int index = (int)(this._pos >> 12);
            final byte[] blockKey = new byte[4];
            LittleEndian.putInt(blockKey, 0, index);
            final byte[] iv = AgileDecryptor.this.generateIv(AgileDecryptor.this._info.getHeader().getAlgorithm(), AgileDecryptor.this._info.getHeader().getKeySalt(), blockKey);
            this._cipher.init(2, AgileDecryptor.this._secretKey, new IvParameterSpec(iv));
            if (this._lastIndex != index) {
                this._stream.skip(index - this._lastIndex << 12);
            }
            final byte[] block = new byte[Math.min(this._stream.available(), 4096)];
            this._stream.readFully(block);
            this._lastIndex = index + 1;
            return this._cipher.doFinal(block);
        }
    }
}
