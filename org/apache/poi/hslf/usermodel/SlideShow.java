// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.usermodel;

import org.apache.poi.hslf.record.ExHyperlinkAtom;
import org.apache.poi.hslf.record.ExHyperlink;
import org.apache.poi.hslf.model.Hyperlink;
import org.apache.poi.hslf.record.ExOleObjAtom;
import org.apache.poi.hslf.record.ExControl;
import org.apache.poi.hslf.record.ExVideoContainer;
import org.apache.poi.hslf.record.ExObjListAtom;
import org.apache.poi.hslf.record.ExAviMovie;
import org.apache.poi.hslf.record.ExMCIMovie;
import org.apache.poi.hslf.record.ExObjList;
import org.apache.poi.hslf.model.Sheet;
import org.apache.poi.hslf.record.HeadersFootersContainer;
import org.apache.poi.hslf.model.HeadersFooters;
import org.apache.poi.hslf.record.FontEntityAtom;
import org.apache.poi.hslf.model.PPFont;
import java.io.FileInputStream;
import java.io.File;
import java.util.Iterator;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.hslf.model.Shape;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.hslf.model.MasterSheet;
import org.apache.poi.hslf.record.UserEditAtom;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.ByteArrayOutputStream;
import java.util.Collection;
import org.apache.poi.hslf.record.DocumentAtom;
import java.awt.Dimension;
import java.io.OutputStream;
import org.apache.poi.hslf.record.MainMaster;
import java.util.ArrayList;
import org.apache.poi.hslf.exceptions.CorruptPowerPointFileException;
import org.apache.poi.hslf.record.SlidePersistAtom;
import org.apache.poi.hslf.record.SlideListWithText;
import java.util.Enumeration;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.hslf.record.PositionDependentRecordContainer;
import org.apache.poi.hslf.record.PositionDependentRecord;
import java.util.Arrays;
import org.apache.poi.hslf.record.PersistPtrHolder;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hslf.record.RecordContainer;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;
import org.apache.poi.hslf.record.FontCollection;
import org.apache.poi.hslf.model.Notes;
import org.apache.poi.hslf.model.Slide;
import org.apache.poi.hslf.model.TitleMaster;
import org.apache.poi.hslf.model.SlideMaster;
import org.apache.poi.hslf.record.Document;
import java.util.Hashtable;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.HSLFSlideShow;

public final class SlideShow
{
    private HSLFSlideShow _hslfSlideShow;
    private Record[] _records;
    private Record[] _mostRecentCoreRecords;
    private Hashtable<Integer, Integer> _sheetIdToCoreRecordsLookup;
    private Document _documentRecord;
    private SlideMaster[] _masters;
    private TitleMaster[] _titleMasters;
    private Slide[] _slides;
    private Notes[] _notes;
    private FontCollection _fonts;
    private POILogger logger;
    
    public SlideShow(final HSLFSlideShow hslfSlideShow) {
        this.logger = POILogFactory.getLogger(this.getClass());
        this._hslfSlideShow = hslfSlideShow;
        this._records = this._hslfSlideShow.getRecords();
        for (final Record record : this._records) {
            if (record instanceof RecordContainer) {
                RecordContainer.handleParentAwareRecords((RecordContainer)record);
            }
        }
        this.findMostRecentCoreRecords();
        this.buildSlidesAndNotes();
    }
    
    public SlideShow() {
        this(HSLFSlideShow.create());
    }
    
    public SlideShow(final InputStream inputStream) throws IOException {
        this(new HSLFSlideShow(inputStream));
    }
    
    private void findMostRecentCoreRecords() {
        final Hashtable<Integer, Integer> mostRecentByBytes = new Hashtable<Integer, Integer>();
        for (int i = 0; i < this._records.length; ++i) {
            if (this._records[i] instanceof PersistPtrHolder) {
                final PersistPtrHolder pph = (PersistPtrHolder)this._records[i];
                final int[] ids = pph.getKnownSlideIDs();
                for (int j = 0; j < ids.length; ++j) {
                    final Integer id = ids[j];
                    if (mostRecentByBytes.containsKey(id)) {
                        mostRecentByBytes.remove(id);
                    }
                }
                final Hashtable<Integer, Integer> thisSetOfLocations = pph.getSlideLocationsLookup();
                for (int k = 0; k < ids.length; ++k) {
                    final Integer id2 = ids[k];
                    mostRecentByBytes.put(id2, thisSetOfLocations.get(id2));
                }
            }
        }
        this._mostRecentCoreRecords = new Record[mostRecentByBytes.size()];
        this._sheetIdToCoreRecordsLookup = new Hashtable<Integer, Integer>();
        final int[] allIDs = new int[this._mostRecentCoreRecords.length];
        final Enumeration<Integer> ids2 = mostRecentByBytes.keys();
        for (int l = 0; l < allIDs.length; ++l) {
            final Integer id3 = ids2.nextElement();
            allIDs[l] = id3;
        }
        Arrays.sort(allIDs);
        for (int l = 0; l < allIDs.length; ++l) {
            this._sheetIdToCoreRecordsLookup.put(allIDs[l], l);
        }
        for (int l = 0; l < this._records.length; ++l) {
            if (this._records[l] instanceof PositionDependentRecord) {
                final PositionDependentRecord pdr = (PositionDependentRecord)this._records[l];
                final Integer recordAt = pdr.getLastOnDiskOffset();
                for (int m = 0; m < allIDs.length; ++m) {
                    final Integer thisID = allIDs[m];
                    final Integer thatRecordAt = mostRecentByBytes.get(thisID);
                    if (thatRecordAt.equals(recordAt)) {
                        final Integer storeAtI = this._sheetIdToCoreRecordsLookup.get(thisID);
                        final int storeAt = storeAtI;
                        if (pdr instanceof PositionDependentRecordContainer) {
                            final PositionDependentRecordContainer pdrc = (PositionDependentRecordContainer)this._records[l];
                            pdrc.setSheetId(thisID);
                        }
                        this._mostRecentCoreRecords[storeAt] = this._records[l];
                    }
                }
            }
        }
        for (int l = 0; l < this._mostRecentCoreRecords.length; ++l) {
            if (this._mostRecentCoreRecords[l] != null && this._mostRecentCoreRecords[l].getRecordType() == RecordTypes.Document.typeID) {
                this._documentRecord = (Document)this._mostRecentCoreRecords[l];
                this._fonts = this._documentRecord.getEnvironment().getFontCollection();
            }
        }
    }
    
    private Record getCoreRecordForSAS(final SlideListWithText.SlideAtomsSet sas) {
        final SlidePersistAtom spa = sas.getSlidePersistAtom();
        final int refID = spa.getRefID();
        return this.getCoreRecordForRefID(refID);
    }
    
    private Record getCoreRecordForRefID(final int refID) {
        final Integer coreRecordId = this._sheetIdToCoreRecordsLookup.get(refID);
        if (coreRecordId != null) {
            final Record r = this._mostRecentCoreRecords[coreRecordId];
            return r;
        }
        this.logger.log(POILogger.ERROR, "We tried to look up a reference to a core record, but there was no core ID for reference ID " + refID);
        return null;
    }
    
    private void buildSlidesAndNotes() {
        if (this._documentRecord == null) {
            throw new CorruptPowerPointFileException("The PowerPoint file didn't contain a Document Record in its PersistPtr blocks. It is probably corrupt.");
        }
        final SlideListWithText masterSLWT = this._documentRecord.getMasterSlideListWithText();
        final SlideListWithText slidesSLWT = this._documentRecord.getSlideSlideListWithText();
        final SlideListWithText notesSLWT = this._documentRecord.getNotesSlideListWithText();
        SlideListWithText.SlideAtomsSet[] masterSets = new SlideListWithText.SlideAtomsSet[0];
        if (masterSLWT != null) {
            masterSets = masterSLWT.getSlideAtomsSets();
            final ArrayList<SlideMaster> mmr = new ArrayList<SlideMaster>();
            final ArrayList<TitleMaster> tmr = new ArrayList<TitleMaster>();
            for (int i = 0; i < masterSets.length; ++i) {
                final Record r = this.getCoreRecordForSAS(masterSets[i]);
                final SlideListWithText.SlideAtomsSet sas = masterSets[i];
                final int sheetNo = sas.getSlidePersistAtom().getSlideIdentifier();
                if (r instanceof org.apache.poi.hslf.record.Slide) {
                    final TitleMaster master = new TitleMaster((org.apache.poi.hslf.record.Slide)r, sheetNo);
                    master.setSlideShow(this);
                    tmr.add(master);
                }
                else if (r instanceof MainMaster) {
                    final SlideMaster master2 = new SlideMaster((MainMaster)r, sheetNo);
                    master2.setSlideShow(this);
                    mmr.add(master2);
                }
            }
            mmr.toArray(this._masters = new SlideMaster[mmr.size()]);
            tmr.toArray(this._titleMasters = new TitleMaster[tmr.size()]);
        }
        SlideListWithText.SlideAtomsSet[] notesSets = new SlideListWithText.SlideAtomsSet[0];
        final Hashtable<Integer, Integer> slideIdToNotes = new Hashtable<Integer, Integer>();
        org.apache.poi.hslf.record.Notes[] notesRecords;
        if (notesSLWT == null) {
            notesRecords = new org.apache.poi.hslf.record.Notes[0];
        }
        else {
            notesSets = notesSLWT.getSlideAtomsSets();
            final ArrayList<org.apache.poi.hslf.record.Notes> notesRecordsL = new ArrayList<org.apache.poi.hslf.record.Notes>();
            for (int j = 0; j < notesSets.length; ++j) {
                final Record r2 = this.getCoreRecordForSAS(notesSets[j]);
                if (r2 instanceof org.apache.poi.hslf.record.Notes) {
                    final org.apache.poi.hslf.record.Notes notesRecord = (org.apache.poi.hslf.record.Notes)r2;
                    notesRecordsL.add(notesRecord);
                    final SlidePersistAtom spa = notesSets[j].getSlidePersistAtom();
                    final Integer slideId = spa.getSlideIdentifier();
                    slideIdToNotes.put(slideId, j);
                }
                else {
                    this.logger.log(POILogger.ERROR, "A Notes SlideAtomSet at " + j + " said its record was at refID " + notesSets[j].getSlidePersistAtom().getRefID() + ", but that was actually a " + r2);
                }
            }
            notesRecords = new org.apache.poi.hslf.record.Notes[notesRecordsL.size()];
            notesRecords = notesRecordsL.toArray(notesRecords);
        }
        SlideListWithText.SlideAtomsSet[] slidesSets = new SlideListWithText.SlideAtomsSet[0];
        org.apache.poi.hslf.record.Slide[] slidesRecords;
        if (slidesSLWT == null) {
            slidesRecords = new org.apache.poi.hslf.record.Slide[0];
        }
        else {
            slidesSets = slidesSLWT.getSlideAtomsSets();
            slidesRecords = new org.apache.poi.hslf.record.Slide[slidesSets.length];
            for (int k = 0; k < slidesSets.length; ++k) {
                final Record r3 = this.getCoreRecordForSAS(slidesSets[k]);
                if (r3 instanceof org.apache.poi.hslf.record.Slide) {
                    slidesRecords[k] = (org.apache.poi.hslf.record.Slide)r3;
                }
                else {
                    this.logger.log(POILogger.ERROR, "A Slide SlideAtomSet at " + k + " said its record was at refID " + slidesSets[k].getSlidePersistAtom().getRefID() + ", but that was actually a " + r3);
                }
            }
        }
        this._notes = new Notes[notesRecords.length];
        for (int k = 0; k < this._notes.length; ++k) {
            (this._notes[k] = new Notes(notesRecords[k])).setSlideShow(this);
        }
        this._slides = new Slide[slidesRecords.length];
        for (int k = 0; k < this._slides.length; ++k) {
            final SlideListWithText.SlideAtomsSet sas2 = slidesSets[k];
            final int slideIdentifier = sas2.getSlidePersistAtom().getSlideIdentifier();
            Notes notes = null;
            final int noteId = slidesRecords[k].getSlideAtom().getNotesID();
            if (noteId != 0) {
                final Integer notesPos = slideIdToNotes.get(noteId);
                if (notesPos != null) {
                    notes = this._notes[notesPos];
                }
                else {
                    this.logger.log(POILogger.ERROR, "Notes not found for noteId=" + noteId);
                }
            }
            (this._slides[k] = new Slide(slidesRecords[k], notes, sas2, slideIdentifier, k + 1)).setSlideShow(this);
        }
    }
    
    public void write(final OutputStream out) throws IOException {
        this._hslfSlideShow.write(out);
    }
    
    public Record[] getMostRecentCoreRecords() {
        return this._mostRecentCoreRecords;
    }
    
    public Slide[] getSlides() {
        return this._slides;
    }
    
    public Notes[] getNotes() {
        return this._notes;
    }
    
    public SlideMaster[] getSlidesMasters() {
        return this._masters;
    }
    
    public TitleMaster[] getTitleMasters() {
        return this._titleMasters;
    }
    
    public PictureData[] getPictureData() {
        return this._hslfSlideShow.getPictures();
    }
    
    public ObjectData[] getEmbeddedObjects() {
        return this._hslfSlideShow.getEmbeddedObjects();
    }
    
    public SoundData[] getSoundData() {
        return SoundData.find(this._documentRecord);
    }
    
    public Dimension getPageSize() {
        final DocumentAtom docatom = this._documentRecord.getDocumentAtom();
        final int pgx = (int)docatom.getSlideSizeX() * 72 / 576;
        final int pgy = (int)docatom.getSlideSizeY() * 72 / 576;
        return new Dimension(pgx, pgy);
    }
    
    public void setPageSize(final Dimension pgsize) {
        final DocumentAtom docatom = this._documentRecord.getDocumentAtom();
        docatom.setSlideSizeX(pgsize.width * 576 / 72);
        docatom.setSlideSizeY(pgsize.height * 576 / 72);
    }
    
    protected FontCollection getFontCollection() {
        return this._fonts;
    }
    
    public Document getDocumentRecord() {
        return this._documentRecord;
    }
    
    public void reorderSlide(final int oldSlideNumber, final int newSlideNumber) {
        if (oldSlideNumber < 1 || newSlideNumber < 1) {
            throw new IllegalArgumentException("Old and new slide numbers must be greater than 0");
        }
        if (oldSlideNumber > this._slides.length || newSlideNumber > this._slides.length) {
            throw new IllegalArgumentException("Old and new slide numbers must not exceed the number of slides (" + this._slides.length + ")");
        }
        final SlideListWithText slwt = this._documentRecord.getSlideSlideListWithText();
        final SlideListWithText.SlideAtomsSet[] sas = slwt.getSlideAtomsSets();
        final SlideListWithText.SlideAtomsSet tmp = sas[oldSlideNumber - 1];
        sas[oldSlideNumber - 1] = sas[newSlideNumber - 1];
        sas[newSlideNumber - 1] = tmp;
        final ArrayList<Record> lst = new ArrayList<Record>();
        for (int i = 0; i < sas.length; ++i) {
            lst.add(sas[i].getSlidePersistAtom());
            final Record[] r = sas[i].getSlideRecords();
            for (int j = 0; j < r.length; ++j) {
                lst.add(r[j]);
            }
            this._slides[i].setSlideNumber(i + 1);
        }
        final Record[] r2 = lst.toArray(new Record[lst.size()]);
        slwt.setChildRecord(r2);
    }
    
    public Slide removeSlide(final int index) {
        final int lastSlideIdx = this._slides.length - 1;
        if (index < 0 || index > lastSlideIdx) {
            throw new IllegalArgumentException("Slide index (" + index + ") is out of range (0.." + lastSlideIdx + ")");
        }
        final SlideListWithText slwt = this._documentRecord.getSlideSlideListWithText();
        final SlideListWithText.SlideAtomsSet[] sas = slwt.getSlideAtomsSets();
        Slide removedSlide = null;
        ArrayList<Record> records = new ArrayList<Record>();
        final ArrayList<SlideListWithText.SlideAtomsSet> sa = new ArrayList<SlideListWithText.SlideAtomsSet>();
        final ArrayList<Slide> sl = new ArrayList<Slide>();
        final ArrayList<Notes> nt = new ArrayList<Notes>();
        for (final Notes notes : this.getNotes()) {
            nt.add(notes);
        }
        int i = 0;
        int num = 0;
        while (i < this._slides.length) {
            if (i != index) {
                sl.add(this._slides[i]);
                sa.add(sas[i]);
                this._slides[i].setSlideNumber(num++);
                records.add(sas[i].getSlidePersistAtom());
                records.addAll(Arrays.asList(sas[i].getSlideRecords()));
            }
            else {
                removedSlide = this._slides[i];
                nt.remove(this._slides[i].getNotesSheet());
            }
            ++i;
        }
        if (sa.size() == 0) {
            this._documentRecord.removeSlideListWithText(slwt);
        }
        else {
            slwt.setSlideAtomsSets(sa.toArray(new SlideListWithText.SlideAtomsSet[sa.size()]));
            slwt.setChildRecord(records.toArray(new Record[records.size()]));
        }
        this._slides = sl.toArray(new Slide[sl.size()]);
        if (removedSlide != null) {
            final int notesId = removedSlide.getSlideRecord().getSlideAtom().getNotesID();
            if (notesId != 0) {
                final SlideListWithText nslwt = this._documentRecord.getNotesSlideListWithText();
                records = new ArrayList<Record>();
                final ArrayList<SlideListWithText.SlideAtomsSet> na = new ArrayList<SlideListWithText.SlideAtomsSet>();
                for (final SlideListWithText.SlideAtomsSet ns : nslwt.getSlideAtomsSets()) {
                    if (ns.getSlidePersistAtom().getSlideIdentifier() != notesId) {
                        na.add(ns);
                        records.add(ns.getSlidePersistAtom());
                        if (ns.getSlideRecords() != null) {
                            records.addAll(Arrays.asList(ns.getSlideRecords()));
                        }
                    }
                }
                if (na.size() == 0) {
                    this._documentRecord.removeSlideListWithText(nslwt);
                }
                else {
                    nslwt.setSlideAtomsSets(na.toArray(new SlideListWithText.SlideAtomsSet[na.size()]));
                    nslwt.setChildRecord(records.toArray(new Record[records.size()]));
                }
            }
        }
        this._notes = nt.toArray(new Notes[nt.size()]);
        return removedSlide;
    }
    
    public Slide createSlide() {
        SlideListWithText slist = null;
        slist = this._documentRecord.getSlideSlideListWithText();
        if (slist == null) {
            slist = new SlideListWithText();
            slist.setInstance(0);
            this._documentRecord.addSlideListWithText(slist);
        }
        SlidePersistAtom prev = null;
        final SlideListWithText.SlideAtomsSet[] sas = slist.getSlideAtomsSets();
        for (int j = 0; j < sas.length; ++j) {
            final SlidePersistAtom spa = sas[j].getSlidePersistAtom();
            if (spa.getSlideIdentifier() >= 0) {
                if (prev == null) {
                    prev = spa;
                }
                if (prev.getSlideIdentifier() < spa.getSlideIdentifier()) {
                    prev = spa;
                }
            }
        }
        final SlidePersistAtom sp = new SlidePersistAtom();
        sp.setSlideIdentifier((prev == null) ? 256 : (prev.getSlideIdentifier() + 1));
        slist.addSlidePersistAtom(sp);
        final Slide slide = new Slide(sp.getSlideIdentifier(), sp.getRefID(), this._slides.length + 1);
        slide.setSlideShow(this);
        slide.onCreate();
        final Slide[] s = new Slide[this._slides.length + 1];
        System.arraycopy(this._slides, 0, s, 0, this._slides.length);
        s[this._slides.length] = slide;
        this._slides = s;
        this.logger.log(POILogger.INFO, "Added slide " + this._slides.length + " with ref " + sp.getRefID() + " and identifier " + sp.getSlideIdentifier());
        final org.apache.poi.hslf.record.Slide slideRecord = slide.getSlideRecord();
        final int slideRecordPos = this._hslfSlideShow.appendRootLevelRecord(slideRecord);
        this._records = this._hslfSlideShow.getRecords();
        int offset = 0;
        int slideOffset = 0;
        PersistPtrHolder ptr = null;
        UserEditAtom usr = null;
        for (int i = 0; i < this._records.length; ++i) {
            final Record record = this._records[i];
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                record.writeOut(out);
            }
            catch (IOException e) {
                throw new HSLFException(e);
            }
            if (this._records[i].getRecordType() == RecordTypes.PersistPtrIncrementalBlock.typeID) {
                ptr = (PersistPtrHolder)this._records[i];
            }
            if (this._records[i].getRecordType() == RecordTypes.UserEditAtom.typeID) {
                usr = (UserEditAtom)this._records[i];
            }
            if (i == slideRecordPos) {
                slideOffset = offset;
            }
            offset += out.size();
        }
        final int psrId = usr.getMaxPersistWritten() + 1;
        sp.setRefID(psrId);
        slideRecord.setSheetId(psrId);
        usr.setLastViewType((short)1);
        usr.setMaxPersistWritten(psrId);
        slideRecord.setLastOnDiskOffset(slideOffset);
        ptr.addSlideLookup(sp.getRefID(), slideOffset);
        this.logger.log(POILogger.INFO, "New slide ended up at " + slideOffset);
        slide.setMasterSheet(this._masters[0]);
        return slide;
    }
    
    public int addPicture(final byte[] data, final int format) throws IOException {
        final byte[] uid = PictureData.getChecksum(data);
        final EscherContainerRecord dggContainer = this._documentRecord.getPPDrawingGroup().getDggContainer();
        EscherContainerRecord bstore = (EscherContainerRecord)Shape.getEscherChild(dggContainer, -4095);
        if (bstore == null) {
            bstore = new EscherContainerRecord();
            bstore.setRecordId((short)(-4095));
            dggContainer.addChildBefore(bstore, -4085);
        }
        else {
            final Iterator<EscherRecord> iter = bstore.getChildIterator();
            int i = 0;
            while (iter.hasNext()) {
                final EscherBSERecord bse = iter.next();
                if (Arrays.equals(bse.getUid(), uid)) {
                    return i + 1;
                }
                ++i;
            }
        }
        final PictureData pict = PictureData.create(format);
        pict.setData(data);
        final int offset = this._hslfSlideShow.addPicture(pict);
        final EscherBSERecord bse = new EscherBSERecord();
        bse.setRecordId((short)(-4089));
        bse.setOptions((short)(0x2 | format << 4));
        bse.setSize(pict.getRawData().length + 8);
        bse.setUid(uid);
        bse.setBlipTypeMacOS((byte)format);
        bse.setBlipTypeWin32((byte)format);
        if (format == 2) {
            bse.setBlipTypeMacOS((byte)4);
        }
        else if (format == 3) {
            bse.setBlipTypeMacOS((byte)4);
        }
        else if (format == 4) {
            bse.setBlipTypeWin32((byte)3);
        }
        bse.setRef(0);
        bse.setOffset(offset);
        bse.setRemainingData(new byte[0]);
        bstore.addChildRecord(bse);
        final int count = bstore.getChildRecords().size();
        bstore.setOptions((short)(count << 4 | 0xF));
        return count;
    }
    
    public int addPicture(final File pict, final int format) throws IOException {
        final int length = (int)pict.length();
        final byte[] data = new byte[length];
        try {
            final FileInputStream is = new FileInputStream(pict);
            is.read(data);
            is.close();
        }
        catch (IOException e) {
            throw new HSLFException(e);
        }
        return this.addPicture(data, format);
    }
    
    public int addFont(final PPFont font) {
        final FontCollection fonts = this.getDocumentRecord().getEnvironment().getFontCollection();
        int idx = fonts.getFontIndex(font.getFontName());
        if (idx == -1) {
            idx = fonts.addFont(font.getFontName(), font.getCharSet(), font.getFontFlags(), font.getFontType(), font.getPitchAndFamily());
        }
        return idx;
    }
    
    public PPFont getFont(final int idx) {
        PPFont font = null;
        final FontCollection fonts = this.getDocumentRecord().getEnvironment().getFontCollection();
        final Record[] ch = fonts.getChildRecords();
        for (int i = 0; i < ch.length; ++i) {
            if (ch[i] instanceof FontEntityAtom) {
                final FontEntityAtom atom = (FontEntityAtom)ch[i];
                if (atom.getFontIndex() == idx) {
                    font = new PPFont(atom);
                    break;
                }
            }
        }
        return font;
    }
    
    public int getNumberOfFonts() {
        return this.getDocumentRecord().getEnvironment().getFontCollection().getNumberOfFonts();
    }
    
    public HeadersFooters getSlideHeadersFooters() {
        final String tag = this.getSlidesMasters()[0].getProgrammableTag();
        final boolean ppt2007 = "___PPT12".equals(tag);
        HeadersFootersContainer hdd = null;
        final Record[] ch = this._documentRecord.getChildRecords();
        for (int i = 0; i < ch.length; ++i) {
            if (ch[i] instanceof HeadersFootersContainer && ((HeadersFootersContainer)ch[i]).getOptions() == 63) {
                hdd = (HeadersFootersContainer)ch[i];
                break;
            }
        }
        boolean newRecord = false;
        if (hdd == null) {
            hdd = new HeadersFootersContainer((short)63);
            newRecord = true;
        }
        return new HeadersFooters(hdd, this, newRecord, ppt2007);
    }
    
    public HeadersFooters getNotesHeadersFooters() {
        final String tag = this.getSlidesMasters()[0].getProgrammableTag();
        final boolean ppt2007 = "___PPT12".equals(tag);
        HeadersFootersContainer hdd = null;
        final Record[] ch = this._documentRecord.getChildRecords();
        for (int i = 0; i < ch.length; ++i) {
            if (ch[i] instanceof HeadersFootersContainer && ((HeadersFootersContainer)ch[i]).getOptions() == 79) {
                hdd = (HeadersFootersContainer)ch[i];
                break;
            }
        }
        boolean newRecord = false;
        if (hdd == null) {
            hdd = new HeadersFootersContainer((short)79);
            newRecord = true;
        }
        if (ppt2007 && this._notes.length > 0) {
            return new HeadersFooters(hdd, this._notes[0], newRecord, ppt2007);
        }
        return new HeadersFooters(hdd, this, newRecord, ppt2007);
    }
    
    public int addMovie(final String path, final int type) {
        ExObjList lst = (ExObjList)this._documentRecord.findFirstOfType(RecordTypes.ExObjList.typeID);
        if (lst == null) {
            lst = new ExObjList();
            this._documentRecord.addChildAfter(lst, this._documentRecord.getDocumentAtom());
        }
        final ExObjListAtom objAtom = lst.getExObjListAtom();
        final int objectId = (int)objAtom.getObjectIDSeed() + 1;
        objAtom.setObjectIDSeed(objectId);
        ExMCIMovie mci = null;
        switch (type) {
            case 1: {
                mci = new ExMCIMovie();
                break;
            }
            case 2: {
                mci = new ExAviMovie();
                break;
            }
            default: {
                throw new IllegalArgumentException("Unsupported Movie: " + type);
            }
        }
        lst.appendChildRecord(mci);
        final ExVideoContainer exVideo = mci.getExVideo();
        exVideo.getExMediaAtom().setObjectId(objectId);
        exVideo.getExMediaAtom().setMask(15204352);
        exVideo.getPathAtom().setText(path);
        return objectId;
    }
    
    public int addControl(final String name, final String progId) {
        ExObjList lst = (ExObjList)this._documentRecord.findFirstOfType(RecordTypes.ExObjList.typeID);
        if (lst == null) {
            lst = new ExObjList();
            this._documentRecord.addChildAfter(lst, this._documentRecord.getDocumentAtom());
        }
        final ExObjListAtom objAtom = lst.getExObjListAtom();
        final int objectId = (int)objAtom.getObjectIDSeed() + 1;
        objAtom.setObjectIDSeed(objectId);
        final ExControl ctrl = new ExControl();
        final ExOleObjAtom oleObj = ctrl.getExOleObjAtom();
        oleObj.setObjID(objectId);
        oleObj.setDrawAspect(1);
        oleObj.setType(2);
        oleObj.setSubType(0);
        ctrl.setProgId(progId);
        ctrl.setMenuName(name);
        ctrl.setClipboardName(name);
        lst.addChildAfter(ctrl, objAtom);
        return objectId;
    }
    
    public int addHyperlink(final Hyperlink link) {
        ExObjList lst = (ExObjList)this._documentRecord.findFirstOfType(RecordTypes.ExObjList.typeID);
        if (lst == null) {
            lst = new ExObjList();
            this._documentRecord.addChildAfter(lst, this._documentRecord.getDocumentAtom());
        }
        final ExObjListAtom objAtom = lst.getExObjListAtom();
        final int objectId = (int)objAtom.getObjectIDSeed() + 1;
        objAtom.setObjectIDSeed(objectId);
        final ExHyperlink ctrl = new ExHyperlink();
        final ExHyperlinkAtom obj = ctrl.getExHyperlinkAtom();
        obj.setNumber(objectId);
        ctrl.setLinkURL(link.getAddress());
        ctrl.setLinkTitle(link.getTitle());
        lst.addChildAfter(ctrl, objAtom);
        link.setId(objectId);
        return objectId;
    }
}
