// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.usermodel;

import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hslf.record.ExOleObjStg;

public class ObjectData
{
    private ExOleObjStg storage;
    
    public ObjectData(final ExOleObjStg storage) {
        this.storage = storage;
    }
    
    public InputStream getData() {
        return this.storage.getData();
    }
    
    public void setData(final byte[] data) throws IOException {
        this.storage.setData(data);
    }
    
    public ExOleObjStg getExOleObjStg() {
        return this.storage;
    }
}
