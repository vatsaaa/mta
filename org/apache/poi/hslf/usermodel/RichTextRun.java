// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.usermodel;

import org.apache.poi.hslf.record.ColorSchemeAtom;
import java.awt.Color;
import org.apache.poi.hslf.model.textproperties.TextProp;
import org.apache.poi.hslf.model.MasterSheet;
import org.apache.poi.hslf.model.Sheet;
import org.apache.poi.hslf.model.textproperties.BitMaskTextProp;
import org.apache.poi.hslf.model.textproperties.ParagraphFlagsTextProp;
import org.apache.poi.hslf.model.textproperties.CharFlagsTextProp;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.hslf.model.textproperties.TextPropCollection;
import org.apache.poi.hslf.model.TextRun;
import org.apache.poi.util.POILogger;

public final class RichTextRun
{
    protected POILogger logger;
    private TextRun parentRun;
    private SlideShow slideShow;
    private int startPos;
    private int length;
    private String _fontname;
    private TextPropCollection paragraphStyle;
    private TextPropCollection characterStyle;
    private boolean sharingParagraphStyle;
    private boolean sharingCharacterStyle;
    
    public RichTextRun(final TextRun parent, final int startAt, final int len) {
        this(parent, startAt, len, null, null, false, false);
    }
    
    public RichTextRun(final TextRun parent, final int startAt, final int len, final TextPropCollection pStyle, final TextPropCollection cStyle, final boolean pShared, final boolean cShared) {
        this.logger = POILogFactory.getLogger(this.getClass());
        this.parentRun = parent;
        this.startPos = startAt;
        this.length = len;
        this.paragraphStyle = pStyle;
        this.characterStyle = cStyle;
        this.sharingParagraphStyle = pShared;
        this.sharingCharacterStyle = cShared;
    }
    
    public void supplyTextProps(final TextPropCollection pStyle, final TextPropCollection cStyle, final boolean pShared, final boolean cShared) {
        if (this.paragraphStyle != null || this.characterStyle != null) {
            throw new IllegalStateException("Can't call supplyTextProps if run already has some");
        }
        this.paragraphStyle = pStyle;
        this.characterStyle = cStyle;
        this.sharingParagraphStyle = pShared;
        this.sharingCharacterStyle = cShared;
    }
    
    public void supplySlideShow(final SlideShow ss) {
        this.slideShow = ss;
        if (this._fontname != null) {
            this.setFontName(this._fontname);
            this._fontname = null;
        }
    }
    
    public int getLength() {
        return this.length;
    }
    
    public int getStartIndex() {
        return this.startPos;
    }
    
    public int getEndIndex() {
        return this.startPos + this.length;
    }
    
    public String getText() {
        return this.parentRun.getText().substring(this.startPos, this.startPos + this.length);
    }
    
    public String getRawText() {
        return this.parentRun.getRawText().substring(this.startPos, this.startPos + this.length);
    }
    
    public void setText(final String text) {
        final String s = this.parentRun.normalize(text);
        this.setRawText(s);
    }
    
    public void setRawText(final String text) {
        this.length = text.length();
        this.parentRun.changeTextInRichTextRun(this, text);
    }
    
    public void updateStartPosition(final int startAt) {
        this.startPos = startAt;
    }
    
    private boolean isCharFlagsTextPropVal(final int index) {
        return this.getFlag(true, index);
    }
    
    private boolean getFlag(final boolean isCharacter, final int index) {
        TextPropCollection props;
        String propname;
        if (isCharacter) {
            props = this.characterStyle;
            propname = CharFlagsTextProp.NAME;
        }
        else {
            props = this.paragraphStyle;
            propname = ParagraphFlagsTextProp.NAME;
        }
        BitMaskTextProp prop = null;
        if (props != null) {
            prop = (BitMaskTextProp)props.findByName(propname);
        }
        if (prop == null) {
            final Sheet sheet = this.parentRun.getSheet();
            if (sheet != null) {
                final int txtype = this.parentRun.getRunType();
                final MasterSheet master = sheet.getMasterSheet();
                if (master != null) {
                    prop = (BitMaskTextProp)master.getStyleAttribute(txtype, this.getIndentLevel(), propname, isCharacter);
                }
            }
            else {
                this.logger.log(POILogger.WARN, "MasterSheet is not available");
            }
        }
        return prop != null && prop.getSubValue(index);
    }
    
    private void setCharFlagsTextPropVal(final int index, final boolean value) {
        if (this.getFlag(true, index) != value) {
            this.setFlag(true, index, value);
        }
    }
    
    public void setFlag(final boolean isCharacter, final int index, final boolean value) {
        TextPropCollection props;
        String propname;
        if (isCharacter) {
            props = this.characterStyle;
            propname = CharFlagsTextProp.NAME;
        }
        else {
            props = this.paragraphStyle;
            propname = ParagraphFlagsTextProp.NAME;
        }
        if (props == null) {
            this.parentRun.ensureStyleAtomPresent();
            props = (isCharacter ? this.characterStyle : this.paragraphStyle);
        }
        final BitMaskTextProp prop = (BitMaskTextProp)this.fetchOrAddTextProp(props, propname);
        prop.setSubValue(value, index);
    }
    
    private TextProp fetchOrAddTextProp(final TextPropCollection textPropCol, final String textPropName) {
        TextProp tp = textPropCol.findByName(textPropName);
        if (tp == null) {
            tp = textPropCol.addWithName(textPropName);
        }
        return tp;
    }
    
    private int getCharTextPropVal(final String propName) {
        TextProp prop = null;
        if (this.characterStyle != null) {
            prop = this.characterStyle.findByName(propName);
        }
        if (prop == null) {
            final Sheet sheet = this.parentRun.getSheet();
            final int txtype = this.parentRun.getRunType();
            final MasterSheet master = sheet.getMasterSheet();
            if (master != null) {
                prop = master.getStyleAttribute(txtype, this.getIndentLevel(), propName, true);
            }
        }
        return (prop == null) ? -1 : prop.getValue();
    }
    
    private int getParaTextPropVal(final String propName) {
        TextProp prop = null;
        boolean hardAttribute = false;
        if (this.paragraphStyle != null) {
            prop = this.paragraphStyle.findByName(propName);
            final BitMaskTextProp maskProp = (BitMaskTextProp)this.paragraphStyle.findByName(ParagraphFlagsTextProp.NAME);
            hardAttribute = (maskProp != null && maskProp.getValue() == 0);
        }
        if (prop == null && !hardAttribute) {
            final Sheet sheet = this.parentRun.getSheet();
            final int txtype = this.parentRun.getRunType();
            final MasterSheet master = sheet.getMasterSheet();
            if (master != null) {
                prop = master.getStyleAttribute(txtype, this.getIndentLevel(), propName, false);
            }
        }
        return (prop == null) ? -1 : prop.getValue();
    }
    
    public void setParaTextPropVal(final String propName, final int val) {
        if (this.paragraphStyle == null) {
            this.parentRun.ensureStyleAtomPresent();
        }
        final TextProp tp = this.fetchOrAddTextProp(this.paragraphStyle, propName);
        tp.setValue(val);
    }
    
    public void setCharTextPropVal(final String propName, final int val) {
        if (this.characterStyle == null) {
            this.parentRun.ensureStyleAtomPresent();
        }
        final TextProp tp = this.fetchOrAddTextProp(this.characterStyle, propName);
        tp.setValue(val);
    }
    
    public boolean isBold() {
        return this.isCharFlagsTextPropVal(0);
    }
    
    public void setBold(final boolean bold) {
        this.setCharFlagsTextPropVal(0, bold);
    }
    
    public boolean isItalic() {
        return this.isCharFlagsTextPropVal(1);
    }
    
    public void setItalic(final boolean italic) {
        this.setCharFlagsTextPropVal(1, italic);
    }
    
    public boolean isUnderlined() {
        return this.isCharFlagsTextPropVal(2);
    }
    
    public void setUnderlined(final boolean underlined) {
        this.setCharFlagsTextPropVal(2, underlined);
    }
    
    public boolean isShadowed() {
        return this.isCharFlagsTextPropVal(4);
    }
    
    public void setShadowed(final boolean flag) {
        this.setCharFlagsTextPropVal(4, flag);
    }
    
    public boolean isEmbossed() {
        return this.isCharFlagsTextPropVal(9);
    }
    
    public void setEmbossed(final boolean flag) {
        this.setCharFlagsTextPropVal(9, flag);
    }
    
    public boolean isStrikethrough() {
        return this.isCharFlagsTextPropVal(8);
    }
    
    public void setStrikethrough(final boolean flag) {
        this.setCharFlagsTextPropVal(8, flag);
    }
    
    public int getSuperscript() {
        final int val = this.getCharTextPropVal("superscript");
        return (val == -1) ? 0 : val;
    }
    
    public void setSuperscript(final int val) {
        this.setCharTextPropVal("superscript", val);
    }
    
    public int getFontSize() {
        return this.getCharTextPropVal("font.size");
    }
    
    public void setFontSize(final int fontSize) {
        this.setCharTextPropVal("font.size", fontSize);
    }
    
    public int getFontIndex() {
        return this.getCharTextPropVal("font.index");
    }
    
    public void setFontIndex(final int idx) {
        this.setCharTextPropVal("font.index", idx);
    }
    
    public void setFontName(final String fontName) {
        if (this.slideShow == null) {
            this._fontname = fontName;
        }
        else {
            final int fontIdx = this.slideShow.getFontCollection().addFont(fontName);
            this.setCharTextPropVal("font.index", fontIdx);
        }
    }
    
    public String getFontName() {
        if (this.slideShow == null) {
            return this._fontname;
        }
        final int fontIdx = this.getCharTextPropVal("font.index");
        if (fontIdx == -1) {
            return null;
        }
        return this.slideShow.getFontCollection().getFontWithId(fontIdx);
    }
    
    public Color getFontColor() {
        int rgb = this.getCharTextPropVal("font.color");
        final int cidx = rgb >> 24;
        if (rgb % 16777216 == 0) {
            final ColorSchemeAtom ca = this.parentRun.getSheet().getColorScheme();
            if (cidx >= 0 && cidx <= 7) {
                rgb = ca.getColor(cidx);
            }
        }
        final Color tmp = new Color(rgb, true);
        return new Color(tmp.getBlue(), tmp.getGreen(), tmp.getRed());
    }
    
    public void setFontColor(final int bgr) {
        this.setCharTextPropVal("font.color", bgr);
    }
    
    public void setFontColor(final Color color) {
        final int rgb = new Color(color.getBlue(), color.getGreen(), color.getRed(), 254).getRGB();
        this.setFontColor(rgb);
    }
    
    public void setAlignment(final int align) {
        this.setParaTextPropVal("alignment", align);
    }
    
    public int getAlignment() {
        return this.getParaTextPropVal("alignment");
    }
    
    public int getIndentLevel() {
        return (this.paragraphStyle == null) ? 0 : this.paragraphStyle.getReservedField();
    }
    
    public void setIndentLevel(final int level) {
        if (this.paragraphStyle != null) {
            this.paragraphStyle.setReservedField((short)level);
        }
    }
    
    public void setBullet(final boolean flag) {
        this.setFlag(false, 0, flag);
    }
    
    public boolean isBullet() {
        return this.getFlag(false, 0);
    }
    
    public boolean isBulletHard() {
        return this.getFlag(false, 0);
    }
    
    public void setBulletChar(final char c) {
        this.setParaTextPropVal("bullet.char", c);
    }
    
    public char getBulletChar() {
        return (char)this.getParaTextPropVal("bullet.char");
    }
    
    public void setBulletOffset(final int offset) {
        this.setParaTextPropVal("bullet.offset", offset * 576 / 72);
    }
    
    public int getBulletOffset() {
        return this.getParaTextPropVal("bullet.offset") * 72 / 576;
    }
    
    public void setTextOffset(final int offset) {
        this.setParaTextPropVal("text.offset", offset * 576 / 72);
    }
    
    public int getTextOffset() {
        return this.getParaTextPropVal("text.offset") * 72 / 576;
    }
    
    public void setBulletSize(final int size) {
        this.setParaTextPropVal("bullet.size", size);
    }
    
    public int getBulletSize() {
        return this.getParaTextPropVal("bullet.size");
    }
    
    public void setBulletColor(final Color color) {
        final int rgb = new Color(color.getBlue(), color.getGreen(), color.getRed(), 254).getRGB();
        this.setParaTextPropVal("bullet.color", rgb);
    }
    
    public Color getBulletColor() {
        int rgb = this.getParaTextPropVal("bullet.color");
        if (rgb == -1) {
            return this.getFontColor();
        }
        final int cidx = rgb >> 24;
        if (rgb % 16777216 == 0) {
            final ColorSchemeAtom ca = this.parentRun.getSheet().getColorScheme();
            if (cidx >= 0 && cidx <= 7) {
                rgb = ca.getColor(cidx);
            }
        }
        final Color tmp = new Color(rgb, true);
        return new Color(tmp.getBlue(), tmp.getGreen(), tmp.getRed());
    }
    
    public void setBulletFont(final int idx) {
        this.setParaTextPropVal("bullet.font", idx);
        this.setFlag(false, 1, true);
    }
    
    public int getBulletFont() {
        return this.getParaTextPropVal("bullet.font");
    }
    
    public void setLineSpacing(final int val) {
        this.setParaTextPropVal("linespacing", val);
    }
    
    public int getLineSpacing() {
        final int val = this.getParaTextPropVal("linespacing");
        return (val == -1) ? 0 : val;
    }
    
    public void setSpaceBefore(final int val) {
        this.setParaTextPropVal("spacebefore", val);
    }
    
    public int getSpaceBefore() {
        final int val = this.getParaTextPropVal("spacebefore");
        return (val == -1) ? 0 : val;
    }
    
    public void setSpaceAfter(final int val) {
        this.setParaTextPropVal("spaceafter", val);
    }
    
    public int getSpaceAfter() {
        final int val = this.getParaTextPropVal("spaceafter");
        return (val == -1) ? 0 : val;
    }
    
    public TextPropCollection _getRawParagraphStyle() {
        return this.paragraphStyle;
    }
    
    public TextPropCollection _getRawCharacterStyle() {
        return this.characterStyle;
    }
    
    public boolean _isParagraphStyleShared() {
        return this.sharingParagraphStyle;
    }
    
    public boolean _isCharacterStyleShared() {
        return this.sharingCharacterStyle;
    }
}
