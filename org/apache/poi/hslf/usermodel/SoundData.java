// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.usermodel;

import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.RecordContainer;
import org.apache.poi.hslf.record.RecordTypes;
import java.util.ArrayList;
import org.apache.poi.hslf.record.Document;
import org.apache.poi.hslf.record.Sound;

public final class SoundData
{
    private Sound _container;
    
    public SoundData(final Sound container) {
        this._container = container;
    }
    
    public String getSoundName() {
        return this._container.getSoundName();
    }
    
    public String getSoundType() {
        return this._container.getSoundType();
    }
    
    public byte[] getData() {
        return this._container.getSoundData();
    }
    
    public static SoundData[] find(final Document document) {
        final ArrayList<SoundData> lst = new ArrayList<SoundData>();
        final Record[] ch = document.getChildRecords();
        for (int i = 0; i < ch.length; ++i) {
            if (ch[i].getRecordType() == RecordTypes.SoundCollection.typeID) {
                final RecordContainer col = (RecordContainer)ch[i];
                final Record[] sr = col.getChildRecords();
                for (int j = 0; j < sr.length; ++j) {
                    if (sr[j] instanceof Sound) {
                        lst.add(new SoundData((Sound)sr[j]));
                    }
                }
            }
        }
        return lst.toArray(new SoundData[lst.size()]);
    }
}
