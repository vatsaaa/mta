// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.usermodel;

import org.apache.poi.hslf.blip.BitmapPainter;
import org.apache.poi.hslf.model.Picture;
import java.awt.Graphics2D;
import org.apache.poi.hslf.blip.DIB;
import org.apache.poi.hslf.blip.PNG;
import org.apache.poi.hslf.blip.JPEG;
import org.apache.poi.hslf.blip.PICT;
import org.apache.poi.hslf.blip.WMF;
import org.apache.poi.hslf.blip.EMF;
import org.apache.poi.util.LittleEndian;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.security.MessageDigest;
import java.io.IOException;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.hslf.blip.ImagePainter;
import org.apache.poi.util.POILogger;

public abstract class PictureData
{
    protected POILogger logger;
    protected static final int CHECKSUM_SIZE = 16;
    private byte[] rawdata;
    protected int offset;
    protected static ImagePainter[] painters;
    
    public PictureData() {
        this.logger = POILogFactory.getLogger(this.getClass());
    }
    
    public abstract int getType();
    
    public abstract byte[] getData();
    
    public abstract void setData(final byte[] p0) throws IOException;
    
    protected abstract int getSignature();
    
    public byte[] getRawData() {
        return this.rawdata;
    }
    
    public void setRawData(final byte[] data) {
        this.rawdata = data;
    }
    
    public int getOffset() {
        return this.offset;
    }
    
    public void setOffset(final int offset) {
        this.offset = offset;
    }
    
    public byte[] getUID() {
        final byte[] uid = new byte[16];
        System.arraycopy(this.rawdata, 0, uid, 0, uid.length);
        return uid;
    }
    
    public static byte[] getChecksum(final byte[] data) {
        MessageDigest sha;
        try {
            sha = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new HSLFException(e.getMessage());
        }
        sha.update(data);
        return sha.digest();
    }
    
    public void write(final OutputStream out) throws IOException {
        byte[] data = new byte[2];
        LittleEndian.putUShort(data, 0, this.getSignature());
        out.write(data);
        data = new byte[2];
        LittleEndian.putUShort(data, 0, this.getType() + 61464);
        out.write(data);
        final byte[] rawdata = this.getRawData();
        data = new byte[4];
        LittleEndian.putInt(data, 0, rawdata.length);
        out.write(data);
        out.write(rawdata);
    }
    
    public static PictureData create(final int type) {
        PictureData pict = null;
        switch (type) {
            case 2: {
                pict = new EMF();
                break;
            }
            case 3: {
                pict = new WMF();
                break;
            }
            case 4: {
                pict = new PICT();
                break;
            }
            case 5: {
                pict = new JPEG();
                break;
            }
            case 6: {
                pict = new PNG();
                break;
            }
            case 7: {
                pict = new DIB();
                break;
            }
            default: {
                throw new IllegalArgumentException("Unsupported picture type: " + type);
            }
        }
        return pict;
    }
    
    public byte[] getHeader() {
        final byte[] header = new byte[24];
        LittleEndian.putInt(header, 0, this.getSignature());
        LittleEndian.putInt(header, 4, this.getRawData().length);
        System.arraycopy(this.rawdata, 0, header, 8, 16);
        return header;
    }
    
    @Deprecated
    public int getSize() {
        return this.getData().length;
    }
    
    public void draw(final Graphics2D graphics, final Picture parent) {
        final ImagePainter painter = PictureData.painters[this.getType()];
        if (painter != null) {
            painter.paint(graphics, this, parent);
        }
        else {
            this.logger.log(POILogger.WARN, "Rendering is not supported: " + this.getClass().getName());
        }
    }
    
    public static void setImagePainter(final int type, final ImagePainter painter) {
        PictureData.painters[type] = painter;
    }
    
    public static ImagePainter getImagePainter(final int type) {
        return PictureData.painters[type];
    }
    
    static {
        PictureData.painters = new ImagePainter[8];
        setImagePainter(6, new BitmapPainter());
        setImagePainter(5, new BitmapPainter());
        setImagePainter(7, new BitmapPainter());
    }
}
