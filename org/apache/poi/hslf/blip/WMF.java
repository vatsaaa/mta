// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.util.zip.InflaterInputStream;
import java.io.OutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public final class WMF extends Metafile
{
    @Override
    public byte[] getData() {
        try {
            final byte[] rawdata = this.getRawData();
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final InputStream is = new ByteArrayInputStream(rawdata);
            final Header header = new Header();
            header.read(rawdata, 16);
            is.skip(header.getSize() + 16);
            final AldusHeader aldus = new AldusHeader();
            aldus.left = header.bounds.x;
            aldus.top = header.bounds.y;
            aldus.right = header.bounds.x + header.bounds.width;
            aldus.bottom = header.bounds.y + header.bounds.height;
            aldus.write(out);
            final InflaterInputStream inflater = new InflaterInputStream(is);
            final byte[] chunk = new byte[4096];
            int count;
            while ((count = inflater.read(chunk)) >= 0) {
                out.write(chunk, 0, count);
            }
            inflater.close();
            return out.toByteArray();
        }
        catch (IOException e) {
            throw new HSLFException(e);
        }
    }
    
    @Override
    public void setData(final byte[] data) throws IOException {
        int pos = 0;
        final AldusHeader aldus = new AldusHeader();
        aldus.read(data, pos);
        pos += aldus.getSize();
        final byte[] compressed = this.compress(data, pos, data.length - pos);
        final Header header = new Header();
        header.wmfsize = data.length - aldus.getSize();
        header.bounds = new Rectangle((short)aldus.left, (short)aldus.top, (short)aldus.right - (short)aldus.left, (short)aldus.bottom - (short)aldus.top);
        final int coeff = 1219200 / aldus.inch;
        header.size = new Dimension(header.bounds.width * coeff, header.bounds.height * coeff);
        header.zipsize = compressed.length;
        final byte[] checksum = PictureData.getChecksum(data);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(checksum);
        header.write(out);
        out.write(compressed);
        this.setRawData(out.toByteArray());
    }
    
    @Override
    public int getType() {
        return 3;
    }
    
    public int getSignature() {
        return 8544;
    }
    
    public class AldusHeader
    {
        public static final int APMHEADER_KEY = -1698247209;
        public int handle;
        public int left;
        public int top;
        public int right;
        public int bottom;
        public int inch;
        public int reserved;
        public int checksum;
        
        public AldusHeader() {
            this.inch = 72;
        }
        
        public void read(final byte[] data, final int offset) {
            int pos = offset;
            final int key = LittleEndian.getInt(data, pos);
            pos += 4;
            if (key != -1698247209) {
                throw new HSLFException("Not a valid WMF file");
            }
            this.handle = LittleEndian.getUShort(data, pos);
            pos += 2;
            this.left = LittleEndian.getUShort(data, pos);
            pos += 2;
            this.top = LittleEndian.getUShort(data, pos);
            pos += 2;
            this.right = LittleEndian.getUShort(data, pos);
            pos += 2;
            this.bottom = LittleEndian.getUShort(data, pos);
            pos += 2;
            this.inch = LittleEndian.getUShort(data, pos);
            pos += 2;
            this.reserved = LittleEndian.getInt(data, pos);
            pos += 4;
            this.checksum = LittleEndian.getShort(data, pos);
            pos += 2;
            if (this.checksum != this.getChecksum()) {
                WMF.this.logger.log(POILogger.WARN, "WMF checksum does not match the header data");
            }
        }
        
        public int getChecksum() {
            int checksum = 0;
            checksum ^= 0xCDD7;
            checksum ^= 0xFFFF9AC6;
            checksum ^= this.left;
            checksum ^= this.top;
            checksum ^= this.right;
            checksum ^= this.bottom;
            checksum ^= this.inch;
            return checksum;
        }
        
        public void write(final OutputStream out) throws IOException {
            final byte[] header = new byte[22];
            int pos = 0;
            LittleEndian.putInt(header, pos, -1698247209);
            pos += 4;
            LittleEndian.putUShort(header, pos, 0);
            pos += 2;
            LittleEndian.putUShort(header, pos, this.left);
            pos += 2;
            LittleEndian.putUShort(header, pos, this.top);
            pos += 2;
            LittleEndian.putUShort(header, pos, this.right);
            pos += 2;
            LittleEndian.putUShort(header, pos, this.bottom);
            pos += 2;
            LittleEndian.putUShort(header, pos, this.inch);
            pos += 2;
            LittleEndian.putInt(header, pos, 0);
            pos += 4;
            LittleEndian.putUShort(header, pos, this.checksum = this.getChecksum());
            out.write(header);
        }
        
        public int getSize() {
            return 22;
        }
    }
}
