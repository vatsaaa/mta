// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import java.io.OutputStream;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public final class EMF extends Metafile
{
    @Override
    public byte[] getData() {
        try {
            final byte[] rawdata = this.getRawData();
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final InputStream is = new ByteArrayInputStream(rawdata);
            final Header header = new Header();
            header.read(rawdata, 16);
            is.skip(header.getSize() + 16);
            final InflaterInputStream inflater = new InflaterInputStream(is);
            final byte[] chunk = new byte[4096];
            int count;
            while ((count = inflater.read(chunk)) >= 0) {
                out.write(chunk, 0, count);
            }
            inflater.close();
            return out.toByteArray();
        }
        catch (IOException e) {
            throw new HSLFException(e);
        }
    }
    
    @Override
    public void setData(final byte[] data) throws IOException {
        final byte[] compressed = this.compress(data, 0, data.length);
        final Header header = new Header();
        header.wmfsize = data.length;
        header.bounds = new Rectangle(0, 0, 200, 200);
        header.size = new Dimension(header.bounds.width * 12700, header.bounds.height * 12700);
        header.zipsize = compressed.length;
        final byte[] checksum = PictureData.getChecksum(data);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(checksum);
        header.write(out);
        out.write(compressed);
        this.setRawData(out.toByteArray());
    }
    
    @Override
    public int getType() {
        return 2;
    }
    
    public int getSignature() {
        return 15680;
    }
}
