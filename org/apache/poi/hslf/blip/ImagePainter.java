// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import org.apache.poi.hslf.model.Picture;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.Graphics2D;

public interface ImagePainter
{
    void paint(final Graphics2D p0, final PictureData p1, final Picture p2);
}
