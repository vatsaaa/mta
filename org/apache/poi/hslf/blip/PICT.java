// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import java.io.OutputStream;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

public final class PICT extends Metafile
{
    @Override
    public byte[] getData() {
        final byte[] rawdata = this.getRawData();
        try {
            final byte[] macheader = new byte[512];
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            out.write(macheader);
            final int pos = 16;
            byte[] pict;
            try {
                pict = this.read(rawdata, pos);
            }
            catch (IOException e2) {
                pict = this.read(rawdata, pos + 16);
            }
            out.write(pict);
            return out.toByteArray();
        }
        catch (IOException e) {
            throw new HSLFException(e);
        }
    }
    
    private byte[] read(final byte[] data, final int pos) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ByteArrayInputStream bis = new ByteArrayInputStream(data);
        final Header header = new Header();
        header.read(data, pos);
        bis.skip(pos + header.getSize());
        final InflaterInputStream inflater = new InflaterInputStream(bis);
        final byte[] chunk = new byte[4096];
        int count;
        while ((count = inflater.read(chunk)) >= 0) {
            out.write(chunk, 0, count);
        }
        inflater.close();
        return out.toByteArray();
    }
    
    @Override
    public void setData(final byte[] data) throws IOException {
        final int pos = 512;
        final byte[] compressed = this.compress(data, pos, data.length - pos);
        final Header header = new Header();
        header.wmfsize = data.length - 512;
        header.bounds = new Rectangle(0, 0, 200, 200);
        header.size = new Dimension(header.bounds.width * 12700, header.bounds.height * 12700);
        header.zipsize = compressed.length;
        final byte[] checksum = PictureData.getChecksum(data);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(checksum);
        out.write(new byte[16]);
        header.write(out);
        out.write(compressed);
        this.setRawData(out.toByteArray());
    }
    
    @Override
    public int getType() {
        return 4;
    }
    
    public int getSignature() {
        return 21552;
    }
}
