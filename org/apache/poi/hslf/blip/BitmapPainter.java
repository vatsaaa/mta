// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import org.apache.poi.hslf.model.Picture;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.Graphics2D;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;

public final class BitmapPainter implements ImagePainter
{
    protected POILogger logger;
    
    public BitmapPainter() {
        this.logger = POILogFactory.getLogger(this.getClass());
    }
    
    public void paint(final Graphics2D graphics, final PictureData pict, final Picture parent) {
        BufferedImage img;
        try {
            img = ImageIO.read(new ByteArrayInputStream(pict.getData()));
        }
        catch (Exception e) {
            this.logger.log(POILogger.WARN, "ImageIO failed to create image. image.type: " + pict.getType());
            return;
        }
        final Rectangle anchor = parent.getLogicalAnchor2D().getBounds();
        graphics.drawImage(img, anchor.x, anchor.y, anchor.width, anchor.height, null);
    }
}
