// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import org.apache.poi.util.LittleEndian;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.usermodel.PictureData;

public abstract class Metafile extends PictureData
{
    protected byte[] compress(final byte[] bytes, final int offset, final int length) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final DeflaterOutputStream deflater = new DeflaterOutputStream(out);
        deflater.write(bytes, offset, length);
        deflater.close();
        return out.toByteArray();
    }
    
    public static class Header
    {
        public int wmfsize;
        public Rectangle bounds;
        public Dimension size;
        public int zipsize;
        public int compression;
        public int filter;
        
        public Header() {
            this.filter = 254;
        }
        
        public void read(final byte[] data, final int offset) {
            int pos = offset;
            this.wmfsize = LittleEndian.getInt(data, pos);
            pos += 4;
            final int left = LittleEndian.getInt(data, pos);
            pos += 4;
            final int top = LittleEndian.getInt(data, pos);
            pos += 4;
            final int right = LittleEndian.getInt(data, pos);
            pos += 4;
            final int bottom = LittleEndian.getInt(data, pos);
            pos += 4;
            this.bounds = new Rectangle(left, top, right - left, bottom - top);
            final int width = LittleEndian.getInt(data, pos);
            pos += 4;
            final int height = LittleEndian.getInt(data, pos);
            pos += 4;
            this.size = new Dimension(width, height);
            this.zipsize = LittleEndian.getInt(data, pos);
            pos += 4;
            this.compression = LittleEndian.getUnsignedByte(data, pos);
            ++pos;
            this.filter = LittleEndian.getUnsignedByte(data, pos);
            ++pos;
        }
        
        public void write(final OutputStream out) throws IOException {
            final byte[] header = new byte[34];
            int pos = 0;
            LittleEndian.putInt(header, pos, this.wmfsize);
            pos += 4;
            LittleEndian.putInt(header, pos, this.bounds.x);
            pos += 4;
            LittleEndian.putInt(header, pos, this.bounds.y);
            pos += 4;
            LittleEndian.putInt(header, pos, this.bounds.x + this.bounds.width);
            pos += 4;
            LittleEndian.putInt(header, pos, this.bounds.y + this.bounds.height);
            pos += 4;
            LittleEndian.putInt(header, pos, this.size.width);
            pos += 4;
            LittleEndian.putInt(header, pos, this.size.height);
            pos += 4;
            LittleEndian.putInt(header, pos, this.zipsize);
            pos += 4;
            header[pos] = 0;
            ++pos;
            header[pos] = (byte)this.filter;
            ++pos;
            out.write(header);
        }
        
        public int getSize() {
            return 34;
        }
    }
}
