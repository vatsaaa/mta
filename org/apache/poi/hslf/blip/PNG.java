// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import java.awt.image.BufferedImage;
import java.io.IOException;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;

public final class PNG extends Bitmap
{
    @Override
    public byte[] getData() {
        byte[] data = super.getData();
        try {
            final BufferedImage bi = ImageIO.read(new ByteArrayInputStream(data));
            if (bi == null) {
                final byte[] png = new byte[data.length - 16];
                System.arraycopy(data, 16, png, 0, png.length);
                data = png;
            }
        }
        catch (IOException e) {
            throw new HSLFException(e);
        }
        return data;
    }
    
    @Override
    public int getType() {
        return 6;
    }
    
    public int getSignature() {
        return 28160;
    }
}
