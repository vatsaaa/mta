// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.usermodel.PictureData;

public abstract class Bitmap extends PictureData
{
    @Override
    public byte[] getData() {
        final byte[] rawdata = this.getRawData();
        final byte[] imgdata = new byte[rawdata.length - 17];
        System.arraycopy(rawdata, 17, imgdata, 0, imgdata.length);
        return imgdata;
    }
    
    @Override
    public void setData(final byte[] data) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final byte[] checksum = PictureData.getChecksum(data);
        out.write(checksum);
        out.write(0);
        out.write(data);
        this.setRawData(out.toByteArray());
    }
}
