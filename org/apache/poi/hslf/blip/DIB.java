// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.blip;

import java.io.IOException;
import org.apache.poi.util.LittleEndian;

public final class DIB extends Bitmap
{
    public static final int HEADER_SIZE = 14;
    
    @Override
    public int getType() {
        return 7;
    }
    
    public int getSignature() {
        return 31360;
    }
    
    @Override
    public byte[] getData() {
        return addBMPHeader(super.getData());
    }
    
    public static byte[] addBMPHeader(final byte[] data) {
        final byte[] header = new byte[14];
        LittleEndian.putInt(header, 0, 19778);
        final int imageSize = LittleEndian.getInt(data, 20);
        final int fileSize = data.length + 14;
        final int offset = fileSize - imageSize;
        LittleEndian.putInt(header, 2, fileSize);
        LittleEndian.putInt(header, 6, 0);
        LittleEndian.putInt(header, 10, offset);
        final byte[] dib = new byte[header.length + data.length];
        System.arraycopy(header, 0, dib, 0, header.length);
        System.arraycopy(data, 0, dib, header.length, data.length);
        return dib;
    }
    
    @Override
    public void setData(final byte[] data) throws IOException {
        final byte[] dib = new byte[data.length - 14];
        System.arraycopy(data, 14, dib, 0, dib.length);
        super.setData(dib);
    }
}
