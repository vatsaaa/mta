// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.exceptions;

public final class InvalidRecordFormatException extends Exception
{
    public InvalidRecordFormatException(final String s) {
        super(s);
    }
}
