// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.exceptions;

import org.apache.poi.OldFileFormatException;

public class OldPowerPointFormatException extends OldFileFormatException
{
    public OldPowerPointFormatException(final String s) {
        super(s);
    }
}
