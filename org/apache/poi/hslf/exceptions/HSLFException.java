// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.exceptions;

public final class HSLFException extends RuntimeException
{
    public HSLFException() {
    }
    
    public HSLFException(final String message) {
        super(message);
    }
    
    public HSLFException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public HSLFException(final Throwable cause) {
        super(cause);
    }
}
