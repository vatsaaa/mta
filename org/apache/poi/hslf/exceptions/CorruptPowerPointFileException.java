// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.exceptions;

public final class CorruptPowerPointFileException extends IllegalStateException
{
    public CorruptPowerPointFileException(final String s) {
        super(s);
    }
}
