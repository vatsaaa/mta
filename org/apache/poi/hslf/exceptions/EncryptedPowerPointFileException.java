// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.exceptions;

import org.apache.poi.EncryptedDocumentException;

public final class EncryptedPowerPointFileException extends EncryptedDocumentException
{
    public EncryptedPowerPointFileException(final String s) {
        super(s);
    }
}
