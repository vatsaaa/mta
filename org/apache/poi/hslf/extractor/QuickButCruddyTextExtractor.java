// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.extractor;

import org.apache.poi.hslf.record.CString;
import org.apache.poi.hslf.record.TextCharsAtom;
import org.apache.poi.hslf.model.TextRun;
import org.apache.poi.hslf.record.StyleTextPropAtom;
import org.apache.poi.hslf.record.TextHeaderAtom;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.TextBytesAtom;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.util.LittleEndian;
import java.util.Iterator;
import java.util.Vector;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public final class QuickButCruddyTextExtractor
{
    private POIFSFileSystem fs;
    private InputStream is;
    private byte[] pptContents;
    
    public static void main(final String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("Useage:");
            System.err.println("\tQuickButCruddyTextExtractor <file>");
            System.exit(1);
        }
        final String file = args[0];
        final QuickButCruddyTextExtractor ppe = new QuickButCruddyTextExtractor(file);
        System.out.println(ppe.getTextAsString());
        ppe.close();
    }
    
    public QuickButCruddyTextExtractor(final String fileName) throws IOException {
        this(new FileInputStream(fileName));
    }
    
    public QuickButCruddyTextExtractor(final InputStream iStream) throws IOException {
        this(new POIFSFileSystem(iStream));
        this.is = iStream;
    }
    
    public QuickButCruddyTextExtractor(final POIFSFileSystem poifs) throws IOException {
        this.fs = poifs;
        final DocumentEntry docProps = (DocumentEntry)this.fs.getRoot().getEntry("PowerPoint Document");
        this.pptContents = new byte[docProps.getSize()];
        this.fs.createDocumentInputStream("PowerPoint Document").read(this.pptContents);
    }
    
    public void close() throws IOException {
        if (this.is != null) {
            this.is.close();
        }
        this.fs = null;
    }
    
    public String getTextAsString() {
        final StringBuffer ret = new StringBuffer();
        final Vector<String> textV = this.getTextAsVector();
        for (final String text : textV) {
            ret.append(text);
            if (!text.endsWith("\n")) {
                ret.append('\n');
            }
        }
        return ret.toString();
    }
    
    public Vector<String> getTextAsVector() {
        final Vector<String> textV = new Vector<String>();
        int newPos;
        for (int walkPos = 0; walkPos != -1; newPos = (walkPos = this.findTextRecords(walkPos, textV))) {}
        return textV;
    }
    
    public int findTextRecords(final int startPos, final Vector<String> textV) {
        final int len = (int)LittleEndian.getUInt(this.pptContents, startPos + 4);
        final byte opt = this.pptContents[startPos];
        final int container = opt & 0xF;
        if (container == 15) {
            return startPos + 8;
        }
        final long type = LittleEndian.getUShort(this.pptContents, startPos + 2);
        TextRun trun = null;
        if (type == RecordTypes.TextBytesAtom.typeID) {
            final TextBytesAtom tba = (TextBytesAtom)Record.createRecordForType(type, this.pptContents, startPos, len + 8);
            trun = new TextRun(null, tba, null);
        }
        if (type == RecordTypes.TextCharsAtom.typeID) {
            final TextCharsAtom tca = (TextCharsAtom)Record.createRecordForType(type, this.pptContents, startPos, len + 8);
            trun = new TextRun(null, tca, null);
        }
        if (type == RecordTypes.CString.typeID) {
            final CString cs = (CString)Record.createRecordForType(type, this.pptContents, startPos, len + 8);
            final String text = cs.getText();
            if (!text.equals("___PPT10")) {
                if (!text.equals("Default Design")) {
                    textV.add(text);
                }
            }
        }
        if (trun != null) {
            textV.add(trun.getText());
        }
        int newPos = startPos + 8 + len;
        if (newPos > this.pptContents.length - 8) {
            newPos = -1;
        }
        return newPos;
    }
}
