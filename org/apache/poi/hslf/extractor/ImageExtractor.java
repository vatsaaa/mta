// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.extractor;

import java.io.IOException;
import org.apache.poi.hslf.usermodel.PictureData;
import java.io.FileOutputStream;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.HSLFSlideShow;

public final class ImageExtractor
{
    public static void main(final String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("Usage:");
            System.err.println("\tImageExtractor <file>");
            return;
        }
        final SlideShow ppt = new SlideShow(new HSLFSlideShow(args[0]));
        final PictureData[] pdata = ppt.getPictureData();
        for (int i = 0; i < pdata.length; ++i) {
            final PictureData pict = pdata[i];
            final byte[] data = pict.getData();
            final int type = pict.getType();
            String ext = null;
            switch (type) {
                case 5: {
                    ext = ".jpg";
                    break;
                }
                case 6: {
                    ext = ".png";
                    break;
                }
                case 3: {
                    ext = ".wmf";
                    break;
                }
                case 2: {
                    ext = ".emf";
                    break;
                }
                case 4: {
                    ext = ".pict";
                    break;
                }
                case 7: {
                    ext = ".dib";
                    break;
                }
                default: {
                    continue;
                }
            }
            final FileOutputStream out = new FileOutputStream("pict_" + i + ext);
            out.write(data);
            out.close();
        }
    }
}
