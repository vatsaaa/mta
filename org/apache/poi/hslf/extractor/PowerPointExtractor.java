// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.extractor;

import org.apache.poi.hslf.model.TextRun;
import org.apache.poi.hslf.model.Notes;
import org.apache.poi.hslf.model.Comment;
import org.apache.poi.hslf.model.HeadersFooters;
import org.apache.poi.hslf.model.SlideMaster;
import java.util.HashSet;
import org.apache.poi.hslf.model.MasterSheet;
import org.apache.poi.hslf.model.TextShape;
import org.apache.poi.hslf.model.Shape;
import java.util.ArrayList;
import org.apache.poi.hslf.model.OLEShape;
import java.util.List;
import org.apache.poi.POIDocument;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hslf.model.Slide;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.HSLFSlideShow;
import org.apache.poi.POIOLE2TextExtractor;

public final class PowerPointExtractor extends POIOLE2TextExtractor
{
    private HSLFSlideShow _hslfshow;
    private SlideShow _show;
    private Slide[] _slides;
    private boolean _slidesByDefault;
    private boolean _notesByDefault;
    private boolean _commentsByDefault;
    private boolean _masterByDefault;
    
    public static void main(final String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("Useage:");
            System.err.println("\tPowerPointExtractor [-notes] <file>");
            System.exit(1);
        }
        boolean notes = false;
        boolean comments = false;
        final boolean master = true;
        String file;
        if (args.length > 1) {
            notes = true;
            file = args[1];
            if (args.length > 2) {
                comments = true;
            }
        }
        else {
            file = args[0];
        }
        final PowerPointExtractor ppe = new PowerPointExtractor(file);
        System.out.println(ppe.getText(true, notes, comments, master));
    }
    
    public PowerPointExtractor(final String fileName) throws IOException {
        this(new FileInputStream(fileName));
    }
    
    public PowerPointExtractor(final InputStream iStream) throws IOException {
        this(new POIFSFileSystem(iStream));
    }
    
    public PowerPointExtractor(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public PowerPointExtractor(final NPOIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public PowerPointExtractor(final DirectoryNode dir) throws IOException {
        this(new HSLFSlideShow(dir));
    }
    
    @Deprecated
    public PowerPointExtractor(final DirectoryNode dir, final POIFSFileSystem fs) throws IOException {
        this(new HSLFSlideShow(dir, fs));
    }
    
    public PowerPointExtractor(final HSLFSlideShow ss) {
        super(ss);
        this._slidesByDefault = true;
        this._notesByDefault = false;
        this._commentsByDefault = false;
        this._masterByDefault = false;
        this._hslfshow = ss;
        this._show = new SlideShow(this._hslfshow);
        this._slides = this._show.getSlides();
    }
    
    public void setSlidesByDefault(final boolean slidesByDefault) {
        this._slidesByDefault = slidesByDefault;
    }
    
    public void setNotesByDefault(final boolean notesByDefault) {
        this._notesByDefault = notesByDefault;
    }
    
    public void setCommentsByDefault(final boolean commentsByDefault) {
        this._commentsByDefault = commentsByDefault;
    }
    
    public void setMasterByDefault(final boolean masterByDefault) {
        this._masterByDefault = masterByDefault;
    }
    
    @Override
    public String getText() {
        return this.getText(this._slidesByDefault, this._notesByDefault, this._commentsByDefault, this._masterByDefault);
    }
    
    public String getNotes() {
        return this.getText(false, true);
    }
    
    public List<OLEShape> getOLEShapes() {
        final List<OLEShape> list = new ArrayList<OLEShape>();
        for (int i = 0; i < this._slides.length; ++i) {
            final Slide slide = this._slides[i];
            final Shape[] shapes = slide.getShapes();
            for (int j = 0; j < shapes.length; ++j) {
                if (shapes[j] instanceof OLEShape) {
                    list.add((OLEShape)shapes[j]);
                }
            }
        }
        return list;
    }
    
    public String getText(final boolean getSlideText, final boolean getNoteText) {
        return this.getText(getSlideText, getNoteText, this._commentsByDefault, this._masterByDefault);
    }
    
    public String getText(final boolean getSlideText, final boolean getNoteText, final boolean getCommentText, final boolean getMasterText) {
        final StringBuffer ret = new StringBuffer();
        if (getSlideText) {
            if (getMasterText) {
                for (final SlideMaster master : this._show.getSlidesMasters()) {
                    for (final Shape sh : master.getShapes()) {
                        if (sh instanceof TextShape) {
                            if (!MasterSheet.isPlaceholder(sh)) {
                                final TextShape tsh = (TextShape)sh;
                                final String text = tsh.getText();
                                ret.append(text);
                                if (!text.endsWith("\n")) {
                                    ret.append("\n");
                                }
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < this._slides.length; ++i) {
                final Slide slide = this._slides[i];
                final HeadersFooters hf = slide.getHeadersFooters();
                if (hf != null && hf.isHeaderVisible() && hf.getHeaderText() != null) {
                    ret.append(hf.getHeaderText() + "\n");
                }
                this.textRunsToText(ret, slide.getTextRuns());
                if (hf != null && hf.isFooterVisible() && hf.getFooterText() != null) {
                    ret.append(hf.getFooterText() + "\n");
                }
                if (getCommentText) {
                    final Comment[] comments = slide.getComments();
                    for (int j = 0; j < comments.length; ++j) {
                        ret.append(comments[j].getAuthor() + " - " + comments[j].getText() + "\n");
                    }
                }
            }
            if (getNoteText) {
                ret.append("\n");
            }
        }
        if (getNoteText) {
            final HashSet<Integer> seenNotes = new HashSet<Integer>();
            final HeadersFooters hf2 = this._show.getNotesHeadersFooters();
            for (int k = 0; k < this._slides.length; ++k) {
                final Notes notes = this._slides[k].getNotesSheet();
                if (notes != null) {
                    final Integer id = notes._getSheetNumber();
                    if (!seenNotes.contains(id)) {
                        seenNotes.add(id);
                        if (hf2 != null && hf2.isHeaderVisible() && hf2.getHeaderText() != null) {
                            ret.append(hf2.getHeaderText() + "\n");
                        }
                        this.textRunsToText(ret, notes.getTextRuns());
                        if (hf2 != null && hf2.isFooterVisible() && hf2.getFooterText() != null) {
                            ret.append(hf2.getFooterText() + "\n");
                        }
                    }
                }
            }
        }
        return ret.toString();
    }
    
    private void textRunsToText(final StringBuffer ret, final TextRun[] runs) {
        if (runs == null) {
            return;
        }
        for (int j = 0; j < runs.length; ++j) {
            final TextRun run = runs[j];
            if (run != null) {
                final String text = run.getText();
                ret.append(text);
                if (!text.endsWith("\n")) {
                    ret.append("\n");
                }
            }
        }
    }
}
