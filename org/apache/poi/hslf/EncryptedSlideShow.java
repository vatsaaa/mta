// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf;

import org.apache.poi.hslf.record.CurrentUserAtom;
import org.apache.poi.hslf.record.PersistPtrHolder;
import org.apache.poi.hslf.record.UserEditAtom;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.exceptions.CorruptPowerPointFileException;
import org.apache.poi.hslf.record.DocumentEncryptionAtom;
import java.io.FileNotFoundException;

public final class EncryptedSlideShow
{
    public static boolean checkIfEncrypted(final HSLFSlideShow hss) {
        try {
            hss.getPOIFSDirectory().getEntry("EncryptedSummary");
            return true;
        }
        catch (FileNotFoundException fnfe) {
            final DocumentEncryptionAtom dea = fetchDocumentEncryptionAtom(hss);
            return dea != null;
        }
    }
    
    public static DocumentEncryptionAtom fetchDocumentEncryptionAtom(final HSLFSlideShow hss) {
        final CurrentUserAtom cua = hss.getCurrentUserAtom();
        if (cua.getCurrentEditOffset() != 0L) {
            if (cua.getCurrentEditOffset() > hss.getUnderlyingBytes().length) {
                throw new CorruptPowerPointFileException("The CurrentUserAtom claims that the offset of last edit details are past the end of the file");
            }
            Record r = null;
            try {
                r = Record.buildRecordAtOffset(hss.getUnderlyingBytes(), (int)cua.getCurrentEditOffset());
            }
            catch (ArrayIndexOutOfBoundsException e) {
                return null;
            }
            if (r == null) {
                return null;
            }
            if (!(r instanceof UserEditAtom)) {
                return null;
            }
            final UserEditAtom uea = (UserEditAtom)r;
            final Record r2 = Record.buildRecordAtOffset(hss.getUnderlyingBytes(), uea.getPersistPointersOffset());
            if (!(r2 instanceof PersistPtrHolder)) {
                return null;
            }
            final PersistPtrHolder pph = (PersistPtrHolder)r2;
            final int[] slideIds = pph.getKnownSlideIDs();
            int maxSlideId = -1;
            for (int i = 0; i < slideIds.length; ++i) {
                if (slideIds[i] > maxSlideId) {
                    maxSlideId = slideIds[i];
                }
            }
            if (maxSlideId == -1) {
                return null;
            }
            final int offset = pph.getSlideLocationsLookup().get(maxSlideId);
            final Record r3 = Record.buildRecordAtOffset(hss.getUnderlyingBytes(), offset);
            if (r3 instanceof DocumentEncryptionAtom) {
                return (DocumentEncryptionAtom)r3;
            }
        }
        return null;
    }
}
