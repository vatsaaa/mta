// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.Graphics2D;
import org.apache.poi.hslf.record.ColorSchemeAtom;
import java.awt.Color;
import java.util.List;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherOptRecord;
import java.util.Iterator;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherClientAnchorRecord;
import org.apache.poi.ddf.EscherChildAnchorRecord;
import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.util.POILogger;

public abstract class Shape
{
    protected POILogger logger;
    public static final int EMU_PER_INCH = 914400;
    public static final int EMU_PER_POINT = 12700;
    public static final int EMU_PER_CENTIMETER = 360000;
    public static final int MASTER_DPI = 576;
    public static final int PIXEL_DPI = 96;
    public static final int POINT_DPI = 72;
    protected EscherContainerRecord _escherContainer;
    protected Shape _parent;
    protected Sheet _sheet;
    protected Fill _fill;
    
    protected Shape(final EscherContainerRecord escherRecord, final Shape parent) {
        this.logger = POILogFactory.getLogger(this.getClass());
        this._escherContainer = escherRecord;
        this._parent = parent;
    }
    
    protected abstract EscherContainerRecord createSpContainer(final boolean p0);
    
    public Shape getParent() {
        return this._parent;
    }
    
    public String getShapeName() {
        return ShapeTypes.typeName(this.getShapeType());
    }
    
    public int getShapeType() {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        return spRecord.getShapeType();
    }
    
    public void setShapeType(final int type) {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        spRecord.setShapeType((short)type);
        spRecord.setVersion((short)2);
    }
    
    public Rectangle getAnchor() {
        final Rectangle2D anchor2d = this.getAnchor2D();
        return anchor2d.getBounds();
    }
    
    public Rectangle2D getAnchor2D() {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        final int flags = spRecord.getFlags();
        Rectangle2D anchor = null;
        if ((flags & 0x2) != 0x0) {
            final EscherChildAnchorRecord rec = (EscherChildAnchorRecord)getEscherChild(this._escherContainer, -4081);
            anchor = new Rectangle();
            if (rec == null) {
                this.logger.log(POILogger.WARN, "EscherSpRecord.FLAG_CHILD is set but EscherChildAnchorRecord was not found");
                final EscherClientAnchorRecord clrec = (EscherClientAnchorRecord)getEscherChild(this._escherContainer, -4080);
                anchor = new Rectangle();
                anchor = new Rectangle2D.Float(clrec.getCol1() * 72.0f / 576.0f, clrec.getFlag() * 72.0f / 576.0f, (clrec.getDx1() - clrec.getCol1()) * 72.0f / 576.0f, (clrec.getRow1() - clrec.getFlag()) * 72.0f / 576.0f);
            }
            else {
                anchor = new Rectangle2D.Float(rec.getDx1() * 72.0f / 576.0f, rec.getDy1() * 72.0f / 576.0f, (rec.getDx2() - rec.getDx1()) * 72.0f / 576.0f, (rec.getDy2() - rec.getDy1()) * 72.0f / 576.0f);
            }
        }
        else {
            final EscherClientAnchorRecord rec2 = (EscherClientAnchorRecord)getEscherChild(this._escherContainer, -4080);
            anchor = new Rectangle();
            anchor = new Rectangle2D.Float(rec2.getCol1() * 72.0f / 576.0f, rec2.getFlag() * 72.0f / 576.0f, (rec2.getDx1() - rec2.getCol1()) * 72.0f / 576.0f, (rec2.getRow1() - rec2.getFlag()) * 72.0f / 576.0f);
        }
        return anchor;
    }
    
    public Rectangle2D getLogicalAnchor2D() {
        return this.getAnchor2D();
    }
    
    public void setAnchor(final Rectangle2D anchor) {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        final int flags = spRecord.getFlags();
        if ((flags & 0x2) != 0x0) {
            final EscherChildAnchorRecord rec = (EscherChildAnchorRecord)getEscherChild(this._escherContainer, -4081);
            rec.setDx1((int)(anchor.getX() * 576.0 / 72.0));
            rec.setDy1((int)(anchor.getY() * 576.0 / 72.0));
            rec.setDx2((int)((anchor.getWidth() + anchor.getX()) * 576.0 / 72.0));
            rec.setDy2((int)((anchor.getHeight() + anchor.getY()) * 576.0 / 72.0));
        }
        else {
            final EscherClientAnchorRecord rec2 = (EscherClientAnchorRecord)getEscherChild(this._escherContainer, -4080);
            rec2.setFlag((short)(anchor.getY() * 576.0 / 72.0));
            rec2.setCol1((short)(anchor.getX() * 576.0 / 72.0));
            rec2.setDx1((short)((anchor.getWidth() + anchor.getX()) * 576.0 / 72.0));
            rec2.setRow1((short)((anchor.getHeight() + anchor.getY()) * 576.0 / 72.0));
        }
    }
    
    public void moveTo(final float x, final float y) {
        final Rectangle2D anchor = this.getAnchor2D();
        anchor.setRect(x, y, anchor.getWidth(), anchor.getHeight());
        this.setAnchor(anchor);
    }
    
    public static EscherRecord getEscherChild(final EscherContainerRecord owner, final int recordId) {
        final Iterator<EscherRecord> iterator = owner.getChildIterator();
        while (iterator.hasNext()) {
            final EscherRecord escherRecord = iterator.next();
            if (escherRecord.getRecordId() == recordId) {
                return escherRecord;
            }
        }
        return null;
    }
    
    public static EscherProperty getEscherProperty(final EscherOptRecord opt, final int propId) {
        if (opt != null) {
            for (final EscherProperty prop : opt.getEscherProperties()) {
                if (prop.getPropertyNumber() == propId) {
                    return prop;
                }
            }
        }
        return null;
    }
    
    public static void setEscherProperty(final EscherOptRecord opt, final short propId, final int value) {
        final List props = opt.getEscherProperties();
        final Iterator iterator = props.iterator();
        while (iterator.hasNext()) {
            final EscherProperty prop = iterator.next();
            if (prop.getId() == propId) {
                iterator.remove();
            }
        }
        if (value != -1) {
            opt.addEscherProperty(new EscherSimpleProperty(propId, value));
            opt.sortProperties();
        }
    }
    
    public void setEscherProperty(final short propId, final int value) {
        final EscherOptRecord opt = (EscherOptRecord)getEscherChild(this._escherContainer, -4085);
        setEscherProperty(opt, propId, value);
    }
    
    public int getEscherProperty(final short propId) {
        final EscherOptRecord opt = (EscherOptRecord)getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)getEscherProperty(opt, propId);
        return (prop == null) ? 0 : prop.getPropertyValue();
    }
    
    public int getEscherProperty(final short propId, final int defaultValue) {
        final EscherOptRecord opt = (EscherOptRecord)getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)getEscherProperty(opt, propId);
        return (prop == null) ? defaultValue : prop.getPropertyValue();
    }
    
    public EscherContainerRecord getSpContainer() {
        return this._escherContainer;
    }
    
    protected void afterInsert(final Sheet sh) {
        if (this._fill != null) {
            this._fill.afterInsert(sh);
        }
    }
    
    public Sheet getSheet() {
        return this._sheet;
    }
    
    public void setSheet(final Sheet sheet) {
        this._sheet = sheet;
    }
    
    Color getColor(final short colorProperty, final short opacityProperty, final int defaultColor) {
        final EscherOptRecord opt = (EscherOptRecord)getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty p = (EscherSimpleProperty)getEscherProperty(opt, colorProperty);
        if (p == null && defaultColor == -1) {
            return null;
        }
        final int val = (p == null) ? defaultColor : p.getPropertyValue();
        final int a = val >> 24 & 0xFF;
        int b = val >> 16 & 0xFF;
        int g = val >> 8 & 0xFF;
        int r = val >> 0 & 0xFF;
        final boolean fPaletteIndex = (a & 0x1) != 0x0;
        final boolean fPaletteRGB = (a & 0x2) != 0x0;
        final boolean fSystemRGB = (a & 0x4) != 0x0;
        final boolean fSchemeIndex = (a & 0x8) != 0x0;
        final boolean fSysIndex = (a & 0x10) != 0x0;
        final Sheet sheet = this.getSheet();
        if (fSchemeIndex && sheet != null) {
            final ColorSchemeAtom ca = sheet.getColorScheme();
            final int schemeColor = ca.getColor(r);
            r = (schemeColor >> 0 & 0xFF);
            g = (schemeColor >> 8 & 0xFF);
            b = (schemeColor >> 16 & 0xFF);
        }
        else if (!fPaletteIndex) {
            if (!fPaletteRGB) {
                if (!fSystemRGB) {
                    if (fSysIndex) {}
                }
            }
        }
        final EscherSimpleProperty op = (EscherSimpleProperty)getEscherProperty(opt, opacityProperty);
        final int defaultOpacity = 65536;
        final int opacity = (op == null) ? defaultOpacity : op.getPropertyValue();
        final int i = opacity >> 16;
        final int f = opacity >> 0 & 0xFFFF;
        final double alpha = (i + f / 65536.0) * 255.0;
        return new Color(r, g, b, (int)alpha);
    }
    
    Color toRGB(final int val) {
        final int a = val >> 24 & 0xFF;
        int b = val >> 16 & 0xFF;
        int g = val >> 8 & 0xFF;
        int r = val >> 0 & 0xFF;
        if (a != 254) {
            if (a != 255) {
                final ColorSchemeAtom ca = this.getSheet().getColorScheme();
                final int schemeColor = ca.getColor(a);
                r = (schemeColor >> 0 & 0xFF);
                g = (schemeColor >> 8 & 0xFF);
                b = (schemeColor >> 16 & 0xFF);
            }
        }
        return new Color(r, g, b);
    }
    
    public int getShapeId() {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        return (spRecord == null) ? 0 : spRecord.getShapeId();
    }
    
    public void setShapeId(final int id) {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        if (spRecord != null) {
            spRecord.setShapeId(id);
        }
    }
    
    public Fill getFill() {
        if (this._fill == null) {
            this._fill = new Fill(this);
        }
        return this._fill;
    }
    
    public Hyperlink getHyperlink() {
        return Hyperlink.find(this);
    }
    
    public void draw(final Graphics2D graphics) {
        this.logger.log(POILogger.INFO, "Rendering " + this.getShapeName());
    }
    
    public java.awt.Shape getOutline() {
        return this.getLogicalAnchor2D();
    }
}
