// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.InteractiveInfoAtom;
import org.apache.poi.hslf.record.TxInteractiveInfoAtom;
import org.apache.poi.hslf.record.InteractiveInfo;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.hslf.record.OEPlaceholderAtom;
import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.util.POILogger;
import org.apache.poi.hslf.record.OutlineTextRefAtom;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.hslf.usermodel.RichTextRun;
import java.awt.font.TextLayout;
import java.awt.Font;
import java.awt.geom.Rectangle2D;
import org.apache.poi.ddf.EscherTextboxRecord;
import org.apache.poi.hslf.record.PPDrawing;
import java.awt.Rectangle;
import java.io.IOException;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.OutputStream;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.hslf.record.StyleTextPropAtom;
import org.apache.poi.hslf.record.TextCharsAtom;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.RecordContainer;
import org.apache.poi.hslf.record.TextHeaderAtom;
import org.apache.poi.ddf.EscherContainerRecord;
import java.awt.font.FontRenderContext;
import org.apache.poi.hslf.record.EscherTextboxWrapper;

public abstract class TextShape extends SimpleShape
{
    public static final int AnchorTop = 0;
    public static final int AnchorMiddle = 1;
    public static final int AnchorBottom = 2;
    public static final int AnchorTopCentered = 3;
    public static final int AnchorMiddleCentered = 4;
    public static final int AnchorBottomCentered = 5;
    public static final int AnchorTopBaseline = 6;
    public static final int AnchorBottomBaseline = 7;
    public static final int AnchorTopCenteredBaseline = 8;
    public static final int AnchorBottomCenteredBaseline = 9;
    public static final int WrapSquare = 0;
    public static final int WrapByPoints = 1;
    public static final int WrapNone = 2;
    public static final int WrapTopBottom = 3;
    public static final int WrapThrough = 4;
    public static final int AlignLeft = 0;
    public static final int AlignCenter = 1;
    public static final int AlignRight = 2;
    public static final int AlignJustify = 3;
    protected TextRun _txtrun;
    protected EscherTextboxWrapper _txtbox;
    protected static final FontRenderContext _frc;
    
    protected TextShape(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public TextShape(final Shape parent) {
        super(null, parent);
        this._escherContainer = this.createSpContainer(parent instanceof ShapeGroup);
    }
    
    public TextShape() {
        this(null);
    }
    
    public TextRun createTextRun() {
        this._txtbox = this.getEscherTextboxWrapper();
        if (this._txtbox == null) {
            this._txtbox = new EscherTextboxWrapper();
        }
        this._txtrun = this.getTextRun();
        if (this._txtrun == null) {
            final TextHeaderAtom tha = new TextHeaderAtom();
            tha.setParentRecord(this._txtbox);
            this._txtbox.appendChildRecord(tha);
            final TextCharsAtom tca = new TextCharsAtom();
            this._txtbox.appendChildRecord(tca);
            final StyleTextPropAtom sta = new StyleTextPropAtom(0);
            this._txtbox.appendChildRecord(sta);
            this._txtrun = new TextRun(tha, tca, sta);
            this._txtrun._records = new Record[] { tha, tca, sta };
            this._txtrun.setText("");
            this._escherContainer.addChildRecord(this._txtbox.getEscherRecord());
            this.setDefaultTextProperties(this._txtrun);
        }
        return this._txtrun;
    }
    
    protected void setDefaultTextProperties(final TextRun _txtrun) {
    }
    
    public String getText() {
        final TextRun tx = this.getTextRun();
        return (tx == null) ? null : tx.getText();
    }
    
    public void setText(final String text) {
        TextRun tx = this.getTextRun();
        if (tx == null) {
            tx = this.createTextRun();
        }
        tx.setText(text);
        this.setTextId(text.hashCode());
    }
    
    @Override
    protected void afterInsert(final Sheet sh) {
        super.afterInsert(sh);
        final EscherTextboxWrapper _txtbox = this.getEscherTextboxWrapper();
        if (_txtbox != null) {
            final PPDrawing ppdrawing = sh.getPPDrawing();
            ppdrawing.addTextboxWrapper(_txtbox);
            try {
                _txtbox.writeOut(null);
            }
            catch (IOException e) {
                throw new HSLFException(e);
            }
            if (this.getAnchor().equals(new Rectangle()) && !"".equals(this.getText())) {
                this.resizeToFitText();
            }
        }
        if (this._txtrun != null) {
            this._txtrun.setShapeId(this.getShapeId());
            sh.onAddTextShape(this);
        }
    }
    
    protected EscherTextboxWrapper getEscherTextboxWrapper() {
        if (this._txtbox == null) {
            final EscherTextboxRecord textRecord = (EscherTextboxRecord)Shape.getEscherChild(this._escherContainer, -4083);
            if (textRecord != null) {
                this._txtbox = new EscherTextboxWrapper(textRecord);
            }
        }
        return this._txtbox;
    }
    
    public Rectangle2D resizeToFitText() {
        final String txt = this.getText();
        if (txt == null || txt.length() == 0) {
            return new Rectangle2D.Float();
        }
        final RichTextRun rt = this.getTextRun().getRichTextRuns()[0];
        final int size = rt.getFontSize();
        int style = 0;
        if (rt.isBold()) {
            style |= 0x1;
        }
        if (rt.isItalic()) {
            style |= 0x2;
        }
        final String fntname = rt.getFontName();
        final Font font = new Font(fntname, style, size);
        float width = 0.0f;
        float height = 0.0f;
        float leading = 0.0f;
        final String[] lines = txt.split("\n");
        for (int i = 0; i < lines.length; ++i) {
            if (lines[i].length() != 0) {
                final TextLayout layout = new TextLayout(lines[i], font, TextShape._frc);
                leading = Math.max(leading, layout.getLeading());
                width = Math.max(width, layout.getAdvance());
                height = Math.max(height, height + (layout.getDescent() + layout.getAscent()));
            }
        }
        final Rectangle2D charBounds = font.getMaxCharBounds(TextShape._frc);
        width += (float)(this.getMarginLeft() + this.getMarginRight() + charBounds.getWidth());
        height += this.getMarginTop() + this.getMarginBottom() + leading;
        final Rectangle2D anchor = this.getAnchor2D();
        anchor.setRect(anchor.getX(), anchor.getY(), width, height);
        this.setAnchor(anchor);
        return anchor;
    }
    
    public int getVerticalAlignment() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 135);
        int valign = 0;
        if (prop == null) {
            final int type = this.getTextRun().getRunType();
            final MasterSheet master = this.getSheet().getMasterSheet();
            if (master != null) {
                final TextShape masterShape = master.getPlaceholderByTextType(type);
                if (masterShape != null) {
                    valign = masterShape.getVerticalAlignment();
                }
            }
            else {
                switch (type) {
                    case 0:
                    case 6: {
                        valign = 1;
                        break;
                    }
                    default: {
                        valign = 0;
                        break;
                    }
                }
            }
        }
        else {
            valign = prop.getPropertyValue();
        }
        return valign;
    }
    
    public void setVerticalAlignment(final int align) {
        this.setEscherProperty((short)135, align);
    }
    
    public void setHorizontalAlignment(final int align) {
        final TextRun tx = this.getTextRun();
        if (tx != null) {
            tx.getRichTextRuns()[0].setAlignment(align);
        }
    }
    
    public int getHorizontalAlignment() {
        final TextRun tx = this.getTextRun();
        return (tx == null) ? -1 : tx.getRichTextRuns()[0].getAlignment();
    }
    
    public float getMarginBottom() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 132);
        final int val = (prop == null) ? 45720 : prop.getPropertyValue();
        return val / 12700.0f;
    }
    
    public void setMarginBottom(final float margin) {
        this.setEscherProperty((short)132, (int)(margin * 12700.0f));
    }
    
    public float getMarginLeft() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 129);
        final int val = (prop == null) ? 91440 : prop.getPropertyValue();
        return val / 12700.0f;
    }
    
    public void setMarginLeft(final float margin) {
        this.setEscherProperty((short)129, (int)(margin * 12700.0f));
    }
    
    public float getMarginRight() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 131);
        final int val = (prop == null) ? 91440 : prop.getPropertyValue();
        return val / 12700.0f;
    }
    
    public void setMarginRight(final float margin) {
        this.setEscherProperty((short)131, (int)(margin * 12700.0f));
    }
    
    public float getMarginTop() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 130);
        final int val = (prop == null) ? 45720 : prop.getPropertyValue();
        return val / 12700.0f;
    }
    
    public void setMarginTop(final float margin) {
        this.setEscherProperty((short)130, (int)(margin * 12700.0f));
    }
    
    public int getWordWrap() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 133);
        return (prop == null) ? 0 : prop.getPropertyValue();
    }
    
    public void setWordWrap(final int wrap) {
        this.setEscherProperty((short)133, wrap);
    }
    
    public int getTextId() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 128);
        return (prop == null) ? 0 : prop.getPropertyValue();
    }
    
    public void setTextId(final int id) {
        this.setEscherProperty((short)128, id);
    }
    
    public TextRun getTextRun() {
        if (this._txtrun == null) {
            this.initTextRun();
        }
        return this._txtrun;
    }
    
    @Override
    public void setSheet(final Sheet sheet) {
        this._sheet = sheet;
        final TextRun tx = this.getTextRun();
        if (tx != null) {
            tx.setSheet(this._sheet);
            final RichTextRun[] rt = tx.getRichTextRuns();
            for (int i = 0; i < rt.length; ++i) {
                rt[i].supplySlideShow(this._sheet.getSlideShow());
            }
        }
    }
    
    protected void initTextRun() {
        final EscherTextboxWrapper txtbox = this.getEscherTextboxWrapper();
        final Sheet sheet = this.getSheet();
        if (sheet == null || txtbox == null) {
            return;
        }
        OutlineTextRefAtom ota = null;
        final Record[] child = txtbox.getChildRecords();
        for (int i = 0; i < child.length; ++i) {
            if (child[i] instanceof OutlineTextRefAtom) {
                ota = (OutlineTextRefAtom)child[i];
                break;
            }
        }
        final TextRun[] runs = this._sheet.getTextRuns();
        if (ota != null) {
            final int idx = ota.getTextIndex();
            for (int j = 0; j < runs.length; ++j) {
                if (runs[j].getIndex() == idx) {
                    this._txtrun = runs[j];
                    break;
                }
            }
            if (this._txtrun == null) {
                this.logger.log(POILogger.WARN, "text run not found for OutlineTextRefAtom.TextIndex=" + idx);
            }
        }
        else {
            final EscherSpRecord escherSpRecord = this._escherContainer.getChildById((short)(-4086));
            final int shapeId = escherSpRecord.getShapeId();
            if (runs != null) {
                for (int k = 0; k < runs.length; ++k) {
                    if (runs[k].getShapeId() == shapeId) {
                        this._txtrun = runs[k];
                        break;
                    }
                }
            }
        }
        if (this._txtrun != null) {
            for (int l = 0; l < child.length; ++l) {
                for (final Record r : this._txtrun.getRecords()) {
                    if (child[l].getRecordType() == r.getRecordType()) {
                        child[l] = r;
                    }
                }
            }
        }
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final AffineTransform at = graphics.getTransform();
        ShapePainter.paint(this, graphics);
        new TextPainter(this).paint(graphics);
        graphics.setTransform(at);
    }
    
    public OEPlaceholderAtom getPlaceholderAtom() {
        return (OEPlaceholderAtom)this.getClientDataRecord(RecordTypes.OEPlaceholderAtom.typeID);
    }
    
    public void setHyperlink(final int linkId, final int beginIndex, final int endIndex) {
        final InteractiveInfo info = new InteractiveInfo();
        final InteractiveInfoAtom infoAtom = info.getInteractiveInfoAtom();
        infoAtom.setAction((byte)4);
        infoAtom.setHyperlinkType((byte)8);
        infoAtom.setHyperlinkID(linkId);
        this._txtbox.appendChildRecord(info);
        final TxInteractiveInfoAtom txiatom = new TxInteractiveInfoAtom();
        txiatom.setStartIndex(beginIndex);
        txiatom.setEndIndex(endIndex);
        this._txtbox.appendChildRecord(txiatom);
    }
    
    static {
        _frc = new FontRenderContext(null, true, true);
    }
}
