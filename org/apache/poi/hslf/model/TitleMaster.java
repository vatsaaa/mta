// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.SlideAtom;
import org.apache.poi.hslf.model.textproperties.TextProp;
import org.apache.poi.hslf.record.SheetContainer;
import org.apache.poi.hslf.record.Slide;

public final class TitleMaster extends MasterSheet
{
    private TextRun[] _runs;
    
    public TitleMaster(final Slide record, final int sheetNo) {
        super(record, sheetNo);
        this._runs = Sheet.findTextRuns(this.getPPDrawing());
        for (int i = 0; i < this._runs.length; ++i) {
            this._runs[i].setSheet(this);
        }
    }
    
    @Override
    public TextRun[] getTextRuns() {
        return this._runs;
    }
    
    @Override
    public TextProp getStyleAttribute(final int txtype, final int level, final String name, final boolean isCharacter) {
        final MasterSheet master = this.getMasterSheet();
        return (master == null) ? null : master.getStyleAttribute(txtype, level, name, isCharacter);
    }
    
    @Override
    public MasterSheet getMasterSheet() {
        final SlideMaster[] master = this.getSlideShow().getSlidesMasters();
        final SlideAtom sa = ((Slide)this.getSheetContainer()).getSlideAtom();
        final int masterId = sa.getMasterID();
        for (int i = 0; i < master.length; ++i) {
            if (masterId == master[i]._getSheetNumber()) {
                return master[i];
            }
        }
        return null;
    }
}
