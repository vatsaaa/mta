// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.image.renderable.RenderableImage;
import java.awt.image.RenderedImage;
import java.awt.Toolkit;
import java.awt.FontMetrics;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.GraphicsConfiguration;
import java.awt.font.GlyphVector;
import java.awt.Composite;
import java.awt.image.BufferedImageOp;
import java.awt.image.BufferedImage;
import java.awt.font.FontRenderContext;
import java.text.AttributedCharacterIterator;
import java.awt.Rectangle;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import org.apache.poi.hslf.usermodel.RichTextRun;
import java.awt.geom.Rectangle2D;
import java.awt.font.TextLayout;
import java.awt.geom.GeneralPath;
import java.awt.Shape;
import java.util.Map;
import java.awt.BasicStroke;
import org.apache.poi.util.POILogFactory;
import java.awt.RenderingHints;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import org.apache.poi.util.POILogger;
import java.awt.Graphics2D;

public final class PPGraphics2D extends Graphics2D implements Cloneable
{
    protected POILogger log;
    private ShapeGroup _group;
    private AffineTransform _transform;
    private Stroke _stroke;
    private Paint _paint;
    private Font _font;
    private Color _foreground;
    private Color _background;
    private RenderingHints _hints;
    
    public PPGraphics2D(final ShapeGroup group) {
        this.log = POILogFactory.getLogger(this.getClass());
        this._group = group;
        this._transform = new AffineTransform();
        this._stroke = new BasicStroke();
        this._paint = Color.black;
        this._font = new Font("Arial", 0, 12);
        this._background = Color.black;
        this._foreground = Color.white;
        this._hints = new RenderingHints(null);
    }
    
    public ShapeGroup getShapeGroup() {
        return this._group;
    }
    
    @Override
    public Font getFont() {
        return this._font;
    }
    
    @Override
    public void setFont(final Font font) {
        this._font = font;
    }
    
    @Override
    public Color getColor() {
        return this._foreground;
    }
    
    @Override
    public void setColor(final Color c) {
        this.setPaint(c);
    }
    
    @Override
    public Stroke getStroke() {
        return this._stroke;
    }
    
    @Override
    public void setStroke(final Stroke s) {
        this._stroke = s;
    }
    
    @Override
    public Paint getPaint() {
        return this._paint;
    }
    
    @Override
    public void setPaint(final Paint paint) {
        if (paint == null) {
            return;
        }
        this._paint = paint;
        if (paint instanceof Color) {
            this._foreground = (Color)paint;
        }
    }
    
    @Override
    public AffineTransform getTransform() {
        return new AffineTransform(this._transform);
    }
    
    @Override
    public void setTransform(final AffineTransform Tx) {
        this._transform = new AffineTransform(Tx);
    }
    
    @Override
    public void draw(final Shape shape) {
        final GeneralPath path = new GeneralPath(this._transform.createTransformedShape(shape));
        final Freeform p = new Freeform(this._group);
        p.setPath(path);
        p.getFill().setForegroundColor(null);
        this.applyStroke(p);
        this._group.addShape(p);
    }
    
    @Override
    public void drawString(final String s, final float x, float y) {
        final TextBox txt = new TextBox(this._group);
        txt.getTextRun().supplySlideShow(this._group.getSheet().getSlideShow());
        txt.getTextRun().setSheet(this._group.getSheet());
        txt.setText(s);
        final RichTextRun rt = txt.getTextRun().getRichTextRuns()[0];
        rt.setFontSize(this._font.getSize());
        rt.setFontName(this._font.getFamily());
        if (this.getColor() != null) {
            rt.setFontColor(this.getColor());
        }
        if (this._font.isBold()) {
            rt.setBold(true);
        }
        if (this._font.isItalic()) {
            rt.setItalic(true);
        }
        txt.setMarginBottom(0.0f);
        txt.setMarginTop(0.0f);
        txt.setMarginLeft(0.0f);
        txt.setMarginRight(0.0f);
        txt.setWordWrap(2);
        txt.setHorizontalAlignment(0);
        txt.setVerticalAlignment(1);
        final TextLayout layout = new TextLayout(s, this._font, this.getFontRenderContext());
        final float ascent = layout.getAscent();
        final float width = (float)Math.floor(layout.getAdvance());
        final float height = ascent * 2.0f;
        y -= height / 2.0f + ascent / 2.0f;
        txt.setAnchor(new Rectangle2D.Float(x, y, width, height));
        this._group.addShape(txt);
    }
    
    @Override
    public void fill(final Shape shape) {
        final GeneralPath path = new GeneralPath(this._transform.createTransformedShape(shape));
        final Freeform p = new Freeform(this._group);
        p.setPath(path);
        this.applyPaint(p);
        p.setLineColor(null);
        this._group.addShape(p);
    }
    
    @Override
    public void translate(final int x, final int y) {
        this._transform.translate(x, y);
    }
    
    @Override
    public void clip(final Shape s) {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    @Override
    public Shape getClip() {
        this.log.log(POILogger.WARN, "Not implemented");
        return null;
    }
    
    @Override
    public void scale(final double sx, final double sy) {
        this._transform.scale(sx, sy);
    }
    
    @Override
    public void drawRoundRect(final int x, final int y, final int width, final int height, final int arcWidth, final int arcHeight) {
        final RoundRectangle2D rect = new RoundRectangle2D.Float((float)x, (float)y, (float)width, (float)height, (float)arcWidth, (float)arcHeight);
        this.draw(rect);
    }
    
    @Override
    public void drawString(final String str, final int x, final int y) {
        this.drawString(str, (float)x, (float)y);
    }
    
    @Override
    public void fillOval(final int x, final int y, final int width, final int height) {
        final Ellipse2D oval = new Ellipse2D.Float((float)x, (float)y, (float)width, (float)height);
        this.fill(oval);
    }
    
    @Override
    public void fillRoundRect(final int x, final int y, final int width, final int height, final int arcWidth, final int arcHeight) {
        final RoundRectangle2D rect = new RoundRectangle2D.Float((float)x, (float)y, (float)width, (float)height, (float)arcWidth, (float)arcHeight);
        this.fill(rect);
    }
    
    @Override
    public void fillArc(final int x, final int y, final int width, final int height, final int startAngle, final int arcAngle) {
        final Arc2D arc = new Arc2D.Float((float)x, (float)y, (float)width, (float)height, (float)startAngle, (float)arcAngle, 2);
        this.fill(arc);
    }
    
    @Override
    public void drawArc(final int x, final int y, final int width, final int height, final int startAngle, final int arcAngle) {
        final Arc2D arc = new Arc2D.Float((float)x, (float)y, (float)width, (float)height, (float)startAngle, (float)arcAngle, 0);
        this.draw(arc);
    }
    
    @Override
    public void drawPolyline(final int[] xPoints, final int[] yPoints, final int nPoints) {
        if (nPoints > 0) {
            final GeneralPath path = new GeneralPath();
            path.moveTo((float)xPoints[0], (float)yPoints[0]);
            for (int i = 1; i < nPoints; ++i) {
                path.lineTo((float)xPoints[i], (float)yPoints[i]);
            }
            this.draw(path);
        }
    }
    
    @Override
    public void drawOval(final int x, final int y, final int width, final int height) {
        final Ellipse2D oval = new Ellipse2D.Float((float)x, (float)y, (float)width, (float)height);
        this.draw(oval);
    }
    
    @Override
    public boolean drawImage(final Image img, final int x, final int y, final Color bgcolor, final ImageObserver observer) {
        this.log.log(POILogger.WARN, "Not implemented");
        return false;
    }
    
    @Override
    public boolean drawImage(final Image img, final int x, final int y, final int width, final int height, final Color bgcolor, final ImageObserver observer) {
        this.log.log(POILogger.WARN, "Not implemented");
        return false;
    }
    
    @Override
    public boolean drawImage(final Image img, final int dx1, final int dy1, final int dx2, final int dy2, final int sx1, final int sy1, final int sx2, final int sy2, final ImageObserver observer) {
        this.log.log(POILogger.WARN, "Not implemented");
        return false;
    }
    
    @Override
    public boolean drawImage(final Image img, final int dx1, final int dy1, final int dx2, final int dy2, final int sx1, final int sy1, final int sx2, final int sy2, final Color bgcolor, final ImageObserver observer) {
        this.log.log(POILogger.WARN, "Not implemented");
        return false;
    }
    
    @Override
    public boolean drawImage(final Image img, final int x, final int y, final ImageObserver observer) {
        this.log.log(POILogger.WARN, "Not implemented");
        return false;
    }
    
    @Override
    public void dispose() {
    }
    
    @Override
    public void drawLine(final int x1, final int y1, final int x2, final int y2) {
        final Line2D line = new Line2D.Float((float)x1, (float)y1, (float)x2, (float)y2);
        this.draw(line);
    }
    
    @Override
    public void fillPolygon(final int[] xPoints, final int[] yPoints, final int nPoints) {
        final Polygon polygon = new Polygon(xPoints, yPoints, nPoints);
        this.fill(polygon);
    }
    
    @Override
    public void fillRect(final int x, final int y, final int width, final int height) {
        final Rectangle rect = new Rectangle(x, y, width, height);
        this.fill(rect);
    }
    
    @Override
    public void drawRect(final int x, final int y, final int width, final int height) {
        final Rectangle rect = new Rectangle(x, y, width, height);
        this.draw(rect);
    }
    
    @Override
    public void drawPolygon(final int[] xPoints, final int[] yPoints, final int nPoints) {
        final Polygon polygon = new Polygon(xPoints, yPoints, nPoints);
        this.draw(polygon);
    }
    
    @Override
    public void clipRect(final int x, final int y, final int width, final int height) {
        this.clip(new Rectangle(x, y, width, height));
    }
    
    @Override
    public void setClip(final Shape clip) {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    @Override
    public Rectangle getClipBounds() {
        final Shape c = this.getClip();
        if (c == null) {
            return null;
        }
        return c.getBounds();
    }
    
    @Override
    public void drawString(final AttributedCharacterIterator iterator, final int x, final int y) {
        this.drawString(iterator, (float)x, (float)y);
    }
    
    @Override
    public void clearRect(final int x, final int y, final int width, final int height) {
        final Paint paint = this.getPaint();
        this.setColor(this.getBackground());
        this.fillRect(x, y, width, height);
        this.setPaint(paint);
    }
    
    @Override
    public void copyArea(final int x, final int y, final int width, final int height, final int dx, final int dy) {
    }
    
    @Override
    public void setClip(final int x, final int y, final int width, final int height) {
        this.setClip(new Rectangle(x, y, width, height));
    }
    
    @Override
    public void rotate(final double theta) {
        this._transform.rotate(theta);
    }
    
    @Override
    public void rotate(final double theta, final double x, final double y) {
        this._transform.rotate(theta, x, y);
    }
    
    @Override
    public void shear(final double shx, final double shy) {
        this._transform.shear(shx, shy);
    }
    
    @Override
    public FontRenderContext getFontRenderContext() {
        final boolean isAntiAliased = RenderingHints.VALUE_TEXT_ANTIALIAS_ON.equals(this.getRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING));
        final boolean usesFractionalMetrics = RenderingHints.VALUE_FRACTIONALMETRICS_ON.equals(this.getRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS));
        return new FontRenderContext(new AffineTransform(), isAntiAliased, usesFractionalMetrics);
    }
    
    @Override
    public void transform(final AffineTransform Tx) {
        this._transform.concatenate(Tx);
    }
    
    @Override
    public void drawImage(BufferedImage img, final BufferedImageOp op, final int x, final int y) {
        img = op.filter(img, null);
        this.drawImage(img, x, y, null);
    }
    
    @Override
    public void setBackground(final Color color) {
        if (color == null) {
            return;
        }
        this._background = color;
    }
    
    @Override
    public Color getBackground() {
        return this._background;
    }
    
    @Override
    public void setComposite(final Composite comp) {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    @Override
    public Composite getComposite() {
        this.log.log(POILogger.WARN, "Not implemented");
        return null;
    }
    
    @Override
    public Object getRenderingHint(final RenderingHints.Key hintKey) {
        return this._hints.get(hintKey);
    }
    
    @Override
    public void setRenderingHint(final RenderingHints.Key hintKey, final Object hintValue) {
        this._hints.put(hintKey, hintValue);
    }
    
    @Override
    public void drawGlyphVector(final GlyphVector g, final float x, final float y) {
        final Shape glyphOutline = g.getOutline(x, y);
        this.fill(glyphOutline);
    }
    
    @Override
    public GraphicsConfiguration getDeviceConfiguration() {
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
    }
    
    @Override
    public void addRenderingHints(final Map hints) {
        this._hints.putAll(hints);
    }
    
    @Override
    public void translate(final double tx, final double ty) {
        this._transform.translate(tx, ty);
    }
    
    @Override
    public void drawString(final AttributedCharacterIterator iterator, final float x, final float y) {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    @Override
    public boolean hit(final Rectangle rect, Shape s, final boolean onStroke) {
        if (onStroke) {
            s = this.getStroke().createStrokedShape(s);
        }
        s = this.getTransform().createTransformedShape(s);
        return s.intersects(rect);
    }
    
    @Override
    public RenderingHints getRenderingHints() {
        return this._hints;
    }
    
    @Override
    public void setRenderingHints(final Map hints) {
        this._hints = new RenderingHints(hints);
    }
    
    @Override
    public boolean drawImage(final Image img, final AffineTransform xform, final ImageObserver obs) {
        this.log.log(POILogger.WARN, "Not implemented");
        return false;
    }
    
    @Override
    public boolean drawImage(final Image img, final int x, final int y, final int width, final int height, final ImageObserver observer) {
        this.log.log(POILogger.WARN, "Not implemented");
        return false;
    }
    
    @Override
    public Graphics create() {
        try {
            return (Graphics)this.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new HSLFException(e);
        }
    }
    
    @Override
    public FontMetrics getFontMetrics(final Font f) {
        return Toolkit.getDefaultToolkit().getFontMetrics(f);
    }
    
    @Override
    public void setXORMode(final Color c1) {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    @Override
    public void setPaintMode() {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    @Override
    public void drawRenderedImage(final RenderedImage img, final AffineTransform xform) {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    @Override
    public void drawRenderableImage(final RenderableImage img, final AffineTransform xform) {
        this.log.log(POILogger.WARN, "Not implemented");
    }
    
    protected void applyStroke(final SimpleShape shape) {
        if (this._stroke instanceof BasicStroke) {
            final BasicStroke bs = (BasicStroke)this._stroke;
            shape.setLineWidth(bs.getLineWidth());
            final float[] dash = bs.getDashArray();
            if (dash != null) {
                shape.setLineDashing(7);
            }
        }
    }
    
    protected void applyPaint(final SimpleShape shape) {
        if (this._paint instanceof Color) {
            shape.getFill().setForegroundColor((Color)this._paint);
        }
    }
}
