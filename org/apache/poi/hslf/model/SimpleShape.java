// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.InteractiveInfoAtom;
import org.apache.poi.hslf.record.InteractiveInfo;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import java.awt.Graphics2D;
import java.util.List;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.awt.geom.Rectangle2D;
import java.awt.Color;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherClientAnchorRecord;
import org.apache.poi.ddf.EscherChildAnchorRecord;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.hslf.record.Record;

public abstract class SimpleShape extends Shape
{
    public static final double DEFAULT_LINE_WIDTH = 0.75;
    protected Record[] _clientRecords;
    protected EscherClientDataRecord _clientData;
    
    protected SimpleShape(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final boolean isChild) {
        (this._escherContainer = new EscherContainerRecord()).setRecordId((short)(-4092));
        this._escherContainer.setOptions((short)15);
        final EscherSpRecord sp = new EscherSpRecord();
        int flags = 2560;
        if (isChild) {
            flags |= 0x2;
        }
        sp.setFlags(flags);
        this._escherContainer.addChildRecord(sp);
        final EscherOptRecord opt = new EscherOptRecord();
        opt.setRecordId((short)(-4085));
        this._escherContainer.addChildRecord(opt);
        EscherRecord anchor;
        if (isChild) {
            anchor = new EscherChildAnchorRecord();
        }
        else {
            anchor = new EscherClientAnchorRecord();
            final byte[] header = new byte[16];
            LittleEndian.putUShort(header, 0, 0);
            LittleEndian.putUShort(header, 2, 0);
            LittleEndian.putInt(header, 4, 8);
            anchor.fillFields(header, 0, null);
        }
        this._escherContainer.addChildRecord(anchor);
        return this._escherContainer;
    }
    
    public double getLineWidth() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 459);
        final double width = (prop == null) ? 0.75 : (prop.getPropertyValue() / 12700.0);
        return width;
    }
    
    public void setLineWidth(final double width) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        Shape.setEscherProperty(opt, (short)459, (int)(width * 12700.0));
    }
    
    public void setLineColor(final Color color) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        if (color == null) {
            Shape.setEscherProperty(opt, (short)511, 524288);
        }
        else {
            final int rgb = new Color(color.getBlue(), color.getGreen(), color.getRed(), 0).getRGB();
            Shape.setEscherProperty(opt, (short)448, rgb);
            Shape.setEscherProperty(opt, (short)511, (color == null) ? 1572880 : 1572888);
        }
    }
    
    public Color getLineColor() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty p = (EscherSimpleProperty)Shape.getEscherProperty(opt, 511);
        if (p != null && (p.getPropertyValue() & 0x8) == 0x0) {
            return null;
        }
        final Color clr = this.getColor((short)448, (short)449, -1);
        return (clr == null) ? Color.black : clr;
    }
    
    public int getLineDashing() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 462);
        return (prop == null) ? 1 : prop.getPropertyValue();
    }
    
    public void setLineDashing(final int pen) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        Shape.setEscherProperty(opt, (short)462, (pen == 1) ? -1 : pen);
    }
    
    public void setLineStyle(final int style) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        Shape.setEscherProperty(opt, (short)461, (style == 0) ? -1 : style);
    }
    
    public int getLineStyle() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 461);
        return (prop == null) ? 0 : prop.getPropertyValue();
    }
    
    public Color getFillColor() {
        return this.getFill().getForegroundColor();
    }
    
    public void setFillColor(final Color color) {
        this.getFill().setForegroundColor(color);
    }
    
    public boolean getFlipHorizontal() {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        return (spRecord.getFlags() & 0x40) != 0x0;
    }
    
    public boolean getFlipVertical() {
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        return (spRecord.getFlags() & 0x80) != 0x0;
    }
    
    public int getRotation() {
        final int rot = this.getEscherProperty((short)4);
        final int angle = (rot >> 16) % 360;
        return angle;
    }
    
    public void setRotation(final int theta) {
        this.setEscherProperty((short)4, theta << 16);
    }
    
    @Override
    public Rectangle2D getLogicalAnchor2D() {
        Rectangle2D anchor = this.getAnchor2D();
        if (this._parent != null) {
            final List<Shape> lst = new ArrayList<Shape>();
            lst.add(this._parent);
            Shape top = this._parent;
            while (top.getParent() != null) {
                top = top.getParent();
                lst.add(top);
            }
            final AffineTransform tx = new AffineTransform();
            for (int i = lst.size() - 1; i >= 0; --i) {
                final ShapeGroup prnt = lst.get(i);
                final Rectangle2D exterior = prnt.getAnchor2D();
                final Rectangle2D interior = prnt.getCoordinates();
                final double scaleX = exterior.getWidth() / interior.getWidth();
                final double scaleY = exterior.getHeight() / interior.getHeight();
                tx.translate(exterior.getX(), exterior.getY());
                tx.scale(scaleX, scaleY);
                tx.translate(-interior.getX(), -interior.getY());
            }
            anchor = tx.createTransformedShape(anchor).getBounds2D();
        }
        final int angle = this.getRotation();
        if (angle != 0) {
            final double centerX = anchor.getX() + anchor.getWidth() / 2.0;
            final double centerY = anchor.getY() + anchor.getHeight() / 2.0;
            AffineTransform trans = new AffineTransform();
            trans.translate(centerX, centerY);
            trans.rotate(Math.toRadians(angle));
            trans.translate(-centerX, -centerY);
            final Rectangle2D rect = trans.createTransformedShape(anchor).getBounds2D();
            if ((anchor.getWidth() < anchor.getHeight() && rect.getWidth() > rect.getHeight()) || (anchor.getWidth() > anchor.getHeight() && rect.getWidth() < rect.getHeight())) {
                trans = new AffineTransform();
                trans.translate(centerX, centerY);
                trans.rotate(1.5707963267948966);
                trans.translate(-centerX, -centerY);
                anchor = trans.createTransformedShape(anchor).getBounds2D();
            }
        }
        return anchor;
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final AffineTransform at = graphics.getTransform();
        ShapePainter.paint(this, graphics);
        graphics.setTransform(at);
    }
    
    protected Record getClientDataRecord(final int recordType) {
        final Record[] records = this.getClientRecords();
        if (records != null) {
            for (int i = 0; i < records.length; ++i) {
                if (records[i].getRecordType() == recordType) {
                    return records[i];
                }
            }
        }
        return null;
    }
    
    protected Record[] getClientRecords() {
        if (this._clientData == null) {
            EscherRecord r = Shape.getEscherChild(this.getSpContainer(), -4079);
            if (r != null && !(r instanceof EscherClientDataRecord)) {
                final byte[] data = r.serialize();
                r = new EscherClientDataRecord();
                r.fillFields(data, 0, new DefaultEscherRecordFactory());
            }
            this._clientData = (EscherClientDataRecord)r;
        }
        if (this._clientData != null && this._clientRecords == null) {
            final byte[] data2 = this._clientData.getRemainingData();
            this._clientRecords = Record.findChildRecords(data2, 0, data2.length);
        }
        return this._clientRecords;
    }
    
    protected void updateClientData() {
        if (this._clientData != null && this._clientRecords != null) {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                for (int i = 0; i < this._clientRecords.length; ++i) {
                    this._clientRecords[i].writeOut(out);
                }
            }
            catch (Exception e) {
                throw new HSLFException(e);
            }
            this._clientData.setRemainingData(out.toByteArray());
        }
    }
    
    public void setHyperlink(final Hyperlink link) {
        if (link.getId() == -1) {
            throw new HSLFException("You must call SlideShow.addHyperlink(Hyperlink link) first");
        }
        final EscherClientDataRecord cldata = new EscherClientDataRecord();
        cldata.setOptions((short)15);
        this.getSpContainer().addChildRecord(cldata);
        final InteractiveInfo info = new InteractiveInfo();
        final InteractiveInfoAtom infoAtom = info.getInteractiveInfoAtom();
        switch (link.getType()) {
            case 2: {
                infoAtom.setAction((byte)3);
                infoAtom.setJump((byte)3);
                infoAtom.setHyperlinkType((byte)2);
                break;
            }
            case 3: {
                infoAtom.setAction((byte)3);
                infoAtom.setJump((byte)4);
                infoAtom.setHyperlinkType((byte)3);
                break;
            }
            case 0: {
                infoAtom.setAction((byte)3);
                infoAtom.setJump((byte)1);
                infoAtom.setHyperlinkType((byte)0);
                break;
            }
            case 1: {
                infoAtom.setAction((byte)3);
                infoAtom.setJump((byte)2);
                infoAtom.setHyperlinkType((byte)1);
                break;
            }
            case 8: {
                infoAtom.setAction((byte)4);
                infoAtom.setJump((byte)0);
                infoAtom.setHyperlinkType((byte)8);
                break;
            }
        }
        infoAtom.setHyperlinkID(link.getId());
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            info.writeOut(out);
        }
        catch (Exception e) {
            throw new HSLFException(e);
        }
        cldata.setRemainingData(out.toByteArray());
    }
}
