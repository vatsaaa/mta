// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.geom.Line2D;
import java.awt.geom.Arc2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.Shape;

public final class AutoShapes
{
    protected static ShapeOutline[] shapes;
    
    public static ShapeOutline getShapeOutline(final int type) {
        final ShapeOutline outline = AutoShapes.shapes[type];
        return outline;
    }
    
    public static Shape transform(final Shape outline, final Rectangle2D anchor) {
        final AffineTransform at = new AffineTransform();
        at.translate(anchor.getX(), anchor.getY());
        at.scale(4.6296296204673126E-5 * anchor.getWidth(), 4.6296296204673126E-5 * anchor.getHeight());
        return at.createTransformedShape(outline);
    }
    
    static {
        (AutoShapes.shapes = new ShapeOutline[255])[1] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final Rectangle2D path = new Rectangle2D.Float(0.0f, 0.0f, 21600.0f, 21600.0f);
                return path;
            }
        };
        AutoShapes.shapes[2] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final RoundRectangle2D path = new RoundRectangle2D.Float(0.0f, 0.0f, 21600.0f, 21600.0f, (float)adjval, (float)adjval);
                return path;
            }
        };
        AutoShapes.shapes[3] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final Ellipse2D path = new Ellipse2D.Float(0.0f, 0.0f, 21600.0f, 21600.0f);
                return path;
            }
        };
        AutoShapes.shapes[4] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final GeneralPath path = new GeneralPath();
                path.moveTo(10800.0f, 0.0f);
                path.lineTo(21600.0f, 10800.0f);
                path.lineTo(10800.0f, 21600.0f);
                path.lineTo(0.0f, 10800.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[5] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 10800);
                final GeneralPath path = new GeneralPath();
                path.moveTo((float)adjval, 0.0f);
                path.lineTo(0.0f, 21600.0f);
                path.lineTo(21600.0f, 21600.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[6] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final GeneralPath path = new GeneralPath();
                path.moveTo(0.0f, 0.0f);
                path.lineTo(21600.0f, 21600.0f);
                path.lineTo(0.0f, 21600.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[7] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo((float)adjval, 0.0f);
                path.lineTo(21600.0f, 0.0f);
                path.lineTo((float)(21600 - adjval), 21600.0f);
                path.lineTo(0.0f, 21600.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[8] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo(0.0f, 0.0f);
                path.lineTo((float)adjval, 21600.0f);
                path.lineTo((float)(21600 - adjval), 21600.0f);
                path.lineTo(21600.0f, 0.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[9] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo((float)adjval, 0.0f);
                path.lineTo((float)(21600 - adjval), 0.0f);
                path.lineTo(21600.0f, 10800.0f);
                path.lineTo((float)(21600 - adjval), 21600.0f);
                path.lineTo((float)adjval, 21600.0f);
                path.lineTo(0.0f, 10800.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[10] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 6326);
                final GeneralPath path = new GeneralPath();
                path.moveTo((float)adjval, 0.0f);
                path.lineTo((float)(21600 - adjval), 0.0f);
                path.lineTo(21600.0f, (float)adjval);
                path.lineTo(21600.0f, (float)(21600 - adjval));
                path.lineTo((float)(21600 - adjval), 21600.0f);
                path.lineTo((float)adjval, 21600.0f);
                path.lineTo(0.0f, (float)(21600 - adjval));
                path.lineTo(0.0f, (float)adjval);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[11] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo((float)adjval, 0.0f);
                path.lineTo((float)(21600 - adjval), 0.0f);
                path.lineTo((float)(21600 - adjval), (float)adjval);
                path.lineTo(21600.0f, (float)adjval);
                path.lineTo(21600.0f, (float)(21600 - adjval));
                path.lineTo((float)(21600 - adjval), (float)(21600 - adjval));
                path.lineTo((float)(21600 - adjval), 21600.0f);
                path.lineTo((float)adjval, 21600.0f);
                path.lineTo((float)adjval, (float)(21600 - adjval));
                path.lineTo(0.0f, (float)(21600 - adjval));
                path.lineTo(0.0f, (float)adjval);
                path.lineTo((float)adjval, (float)adjval);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[56] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final GeneralPath path = new GeneralPath();
                path.moveTo(10800.0f, 0.0f);
                path.lineTo(21600.0f, 8259.0f);
                path.lineTo(17400.0f, 21600.0f);
                path.lineTo(4200.0f, 21600.0f);
                path.lineTo(0.0f, 8259.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[67] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 16200);
                final int adjval2 = shape.getEscherProperty((short)328, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo(0.0f, (float)adjval);
                path.lineTo((float)adjval2, (float)adjval);
                path.lineTo((float)adjval2, 0.0f);
                path.lineTo((float)(21600 - adjval2), 0.0f);
                path.lineTo((float)(21600 - adjval2), (float)adjval);
                path.lineTo(21600.0f, (float)adjval);
                path.lineTo(10800.0f, 21600.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[68] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final int adjval2 = shape.getEscherProperty((short)328, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo(0.0f, (float)adjval);
                path.lineTo((float)adjval2, (float)adjval);
                path.lineTo((float)adjval2, 21600.0f);
                path.lineTo((float)(21600 - adjval2), 21600.0f);
                path.lineTo((float)(21600 - adjval2), (float)adjval);
                path.lineTo(21600.0f, (float)adjval);
                path.lineTo(10800.0f, 0.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[13] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 16200);
                final int adjval2 = shape.getEscherProperty((short)328, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo((float)adjval, 0.0f);
                path.lineTo((float)adjval, (float)adjval2);
                path.lineTo(0.0f, (float)adjval2);
                path.lineTo(0.0f, (float)(21600 - adjval2));
                path.lineTo((float)adjval, (float)(21600 - adjval2));
                path.lineTo((float)adjval, 21600.0f);
                path.lineTo(21600.0f, 10800.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[66] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final int adjval2 = shape.getEscherProperty((short)328, 5400);
                final GeneralPath path = new GeneralPath();
                path.moveTo((float)adjval, 0.0f);
                path.lineTo((float)adjval, (float)adjval2);
                path.lineTo(21600.0f, (float)adjval2);
                path.lineTo(21600.0f, (float)(21600 - adjval2));
                path.lineTo((float)adjval, (float)(21600 - adjval2));
                path.lineTo((float)adjval, 21600.0f);
                path.lineTo(0.0f, 10800.0f);
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[22] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 5400);
                final GeneralPath path = new GeneralPath();
                path.append(new Arc2D.Float(0.0f, 0.0f, 21600.0f, (float)adjval, 0.0f, 180.0f, 0), false);
                path.moveTo(0.0f, (float)(adjval / 2));
                path.lineTo(0.0f, (float)(21600 - adjval / 2));
                path.closePath();
                path.append(new Arc2D.Float(0.0f, (float)(21600 - adjval), 21600.0f, (float)adjval, 180.0f, 180.0f, 0), false);
                path.moveTo(21600.0f, (float)(21600 - adjval / 2));
                path.lineTo(21600.0f, (float)(adjval / 2));
                path.append(new Arc2D.Float(0.0f, 0.0f, 21600.0f, (float)adjval, 180.0f, 180.0f, 0), false);
                path.moveTo(0.0f, (float)(adjval / 2));
                path.closePath();
                return path;
            }
        };
        AutoShapes.shapes[87] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 1800);
                final int adjval2 = shape.getEscherProperty((short)328, 10800);
                final GeneralPath path = new GeneralPath();
                path.moveTo(21600.0f, 0.0f);
                path.append(new Arc2D.Float(10800.0f, 0.0f, 21600.0f, (float)(adjval * 2), 90.0f, 90.0f, 0), false);
                path.moveTo(10800.0f, (float)adjval);
                path.lineTo(10800.0f, (float)(adjval2 - adjval));
                path.append(new Arc2D.Float(-10800.0f, (float)(adjval2 - 2 * adjval), 21600.0f, (float)(adjval * 2), 270.0f, 90.0f, 0), false);
                path.moveTo(0.0f, (float)adjval2);
                path.append(new Arc2D.Float(-10800.0f, (float)adjval2, 21600.0f, (float)(adjval * 2), 0.0f, 90.0f, 0), false);
                path.moveTo(10800.0f, (float)(adjval2 + adjval));
                path.lineTo(10800.0f, (float)(21600 - adjval));
                path.append(new Arc2D.Float(10800.0f, (float)(21600 - 2 * adjval), 21600.0f, (float)(adjval * 2), 180.0f, 90.0f, 0), false);
                return path;
            }
        };
        AutoShapes.shapes[88] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                final int adjval = shape.getEscherProperty((short)327, 1800);
                final int adjval2 = shape.getEscherProperty((short)328, 10800);
                final GeneralPath path = new GeneralPath();
                path.moveTo(0.0f, 0.0f);
                path.append(new Arc2D.Float(-10800.0f, 0.0f, 21600.0f, (float)(adjval * 2), 0.0f, 90.0f, 0), false);
                path.moveTo(10800.0f, (float)adjval);
                path.lineTo(10800.0f, (float)(adjval2 - adjval));
                path.append(new Arc2D.Float(10800.0f, (float)(adjval2 - 2 * adjval), 21600.0f, (float)(adjval * 2), 180.0f, 90.0f, 0), false);
                path.moveTo(21600.0f, (float)adjval2);
                path.append(new Arc2D.Float(10800.0f, (float)adjval2, 21600.0f, (float)(adjval * 2), 90.0f, 90.0f, 0), false);
                path.moveTo(10800.0f, (float)(adjval2 + adjval));
                path.lineTo(10800.0f, (float)(21600 - adjval));
                path.append(new Arc2D.Float(-10800.0f, (float)(21600 - 2 * adjval), 21600.0f, (float)(adjval * 2), 270.0f, 90.0f, 0), false);
                return path;
            }
        };
        AutoShapes.shapes[32] = new ShapeOutline() {
            public Shape getOutline(final org.apache.poi.hslf.model.Shape shape) {
                return new Line2D.Float(0.0f, 0.0f, 21600.0f, 21600.0f);
            }
        };
    }
}
