// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.ExHyperlink;
import org.apache.poi.hslf.record.InteractiveInfoAtom;
import org.apache.poi.hslf.record.TxInteractiveInfoAtom;
import org.apache.poi.hslf.record.InteractiveInfo;
import java.util.Iterator;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.ExObjList;
import org.apache.poi.hslf.usermodel.SlideShow;
import java.util.List;
import java.util.ArrayList;

public final class Hyperlink
{
    public static final byte LINK_NEXTSLIDE = 0;
    public static final byte LINK_PREVIOUSSLIDE = 1;
    public static final byte LINK_FIRSTSLIDE = 2;
    public static final byte LINK_LASTSLIDE = 3;
    public static final byte LINK_URL = 8;
    public static final byte LINK_NULL = -1;
    private int id;
    private int type;
    private String address;
    private String title;
    private int startIndex;
    private int endIndex;
    
    public Hyperlink() {
        this.id = -1;
    }
    
    public int getType() {
        return this.type;
    }
    
    public void setType(final int val) {
        switch (this.type = val) {
            case 0: {
                this.title = "NEXT";
                this.address = "1,-1,NEXT";
                break;
            }
            case 1: {
                this.title = "PREV";
                this.address = "1,-1,PREV";
                break;
            }
            case 2: {
                this.title = "FIRST";
                this.address = "1,-1,FIRST";
                break;
            }
            case 3: {
                this.title = "LAST";
                this.address = "1,-1,LAST";
                break;
            }
            default: {
                this.title = "";
                this.address = "";
                break;
            }
        }
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(final String str) {
        this.address = str;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String str) {
        this.title = str;
    }
    
    public int getStartIndex() {
        return this.startIndex;
    }
    
    public int getEndIndex() {
        return this.endIndex;
    }
    
    protected static Hyperlink[] find(final TextRun run) {
        final ArrayList lst = new ArrayList();
        final SlideShow ppt = run.getSheet().getSlideShow();
        final ExObjList exobj = ppt.getDocumentRecord().getExObjList();
        if (exobj == null) {
            return null;
        }
        final Record[] records = run._records;
        if (records != null) {
            find(records, exobj, lst);
        }
        Hyperlink[] links = null;
        if (lst.size() > 0) {
            links = new Hyperlink[lst.size()];
            lst.toArray(links);
        }
        return links;
    }
    
    protected static Hyperlink find(final Shape shape) {
        final ArrayList lst = new ArrayList();
        final SlideShow ppt = shape.getSheet().getSlideShow();
        final ExObjList exobj = ppt.getDocumentRecord().getExObjList();
        if (exobj == null) {
            return null;
        }
        final EscherContainerRecord spContainer = shape.getSpContainer();
        final Iterator<EscherRecord> it = spContainer.getChildIterator();
        while (it.hasNext()) {
            final EscherRecord obj = it.next();
            if (obj.getRecordId() == -4079) {
                final byte[] data = obj.serialize();
                final Record[] records = Record.findChildRecords(data, 8, data.length - 8);
                if (records == null) {
                    continue;
                }
                find(records, exobj, lst);
            }
        }
        return (lst.size() == 1) ? lst.get(0) : null;
    }
    
    private static void find(final Record[] records, final ExObjList exobj, final List out) {
        for (int i = 0; i < records.length; ++i) {
            if (records[i] instanceof InteractiveInfo) {
                final InteractiveInfo hldr = (InteractiveInfo)records[i];
                final InteractiveInfoAtom info = hldr.getInteractiveInfoAtom();
                final int id = info.getHyperlinkID();
                final ExHyperlink linkRecord = exobj.get(id);
                if (linkRecord != null) {
                    final Hyperlink link = new Hyperlink();
                    link.title = linkRecord.getLinkTitle();
                    link.address = linkRecord.getLinkURL();
                    link.type = info.getAction();
                    if (++i < records.length && records[i] instanceof TxInteractiveInfoAtom) {
                        final TxInteractiveInfoAtom txinfo = (TxInteractiveInfoAtom)records[i];
                        link.startIndex = txinfo.getStartIndex();
                        link.endIndex = txinfo.getEndIndex();
                    }
                    out.add(link);
                }
            }
        }
    }
}
