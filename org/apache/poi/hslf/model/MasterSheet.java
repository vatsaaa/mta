// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.model.textproperties.TextProp;
import org.apache.poi.hslf.record.SheetContainer;

public abstract class MasterSheet extends Sheet
{
    public MasterSheet(final SheetContainer container, final int sheetNo) {
        super(container, sheetNo);
    }
    
    public abstract TextProp getStyleAttribute(final int p0, final int p1, final String p2, final boolean p3);
    
    public static boolean isPlaceholder(final Shape shape) {
        if (!(shape instanceof TextShape)) {
            return false;
        }
        final TextShape tx = (TextShape)shape;
        return tx.getPlaceholderAtom() != null;
    }
}
