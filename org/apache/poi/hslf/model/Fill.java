// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.Color;
import java.util.List;
import org.apache.poi.hslf.record.Document;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;

public final class Fill
{
    protected POILogger logger;
    public static final int FILL_SOLID = 0;
    public static final int FILL_PATTERN = 1;
    public static final int FILL_TEXTURE = 2;
    public static final int FILL_PICTURE = 3;
    public static final int FILL_SHADE = 4;
    public static final int FILL_SHADE_CENTER = 5;
    public static final int FILL_SHADE_SHAPE = 6;
    public static final int FILL_SHADE_SCALE = 7;
    public static final int FILL_SHADE_TITLE = 8;
    public static final int FILL_BACKGROUND = 9;
    protected Shape shape;
    
    public Fill(final Shape shape) {
        this.logger = POILogFactory.getLogger(this.getClass());
        this.shape = shape;
    }
    
    public int getFillType() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 384);
        return (prop == null) ? 0 : prop.getPropertyValue();
    }
    
    protected void afterInsert(final Sheet sh) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        final EscherSimpleProperty p = (EscherSimpleProperty)Shape.getEscherProperty(opt, 390);
        if (p != null) {
            final int idx = p.getPropertyValue();
            final EscherBSERecord bse = this.getEscherBSERecord(idx);
            bse.setRef(bse.getRef() + 1);
        }
    }
    
    protected EscherBSERecord getEscherBSERecord(final int idx) {
        final Sheet sheet = this.shape.getSheet();
        if (sheet == null) {
            this.logger.log(POILogger.DEBUG, "Fill has not yet been assigned to a sheet");
            return null;
        }
        final SlideShow ppt = sheet.getSlideShow();
        final Document doc = ppt.getDocumentRecord();
        final EscherContainerRecord dggContainer = doc.getPPDrawingGroup().getDggContainer();
        final EscherContainerRecord bstore = (EscherContainerRecord)Shape.getEscherChild(dggContainer, -4095);
        if (bstore == null) {
            this.logger.log(POILogger.DEBUG, "EscherContainerRecord.BSTORE_CONTAINER was not found ");
            return null;
        }
        final List lst = bstore.getChildRecords();
        return lst.get(idx - 1);
    }
    
    public void setFillType(final int type) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        Shape.setEscherProperty(opt, (short)384, type);
    }
    
    public Color getForegroundColor() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        final EscherSimpleProperty p = (EscherSimpleProperty)Shape.getEscherProperty(opt, 447);
        if (p != null && (p.getPropertyValue() & 0x10) == 0x0) {
            return null;
        }
        return this.shape.getColor((short)385, (short)386, -1);
    }
    
    public void setForegroundColor(final Color color) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        if (color == null) {
            Shape.setEscherProperty(opt, (short)447, 1376256);
        }
        else {
            final int rgb = new Color(color.getBlue(), color.getGreen(), color.getRed(), 0).getRGB();
            Shape.setEscherProperty(opt, (short)385, rgb);
            Shape.setEscherProperty(opt, (short)447, 1376273);
        }
    }
    
    public Color getBackgroundColor() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        final EscherSimpleProperty p = (EscherSimpleProperty)Shape.getEscherProperty(opt, 447);
        if (p != null && (p.getPropertyValue() & 0x10) == 0x0) {
            return null;
        }
        return this.shape.getColor((short)387, (short)386, -1);
    }
    
    public void setBackgroundColor(final Color color) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        if (color == null) {
            Shape.setEscherProperty(opt, (short)387, -1);
        }
        else {
            final int rgb = new Color(color.getBlue(), color.getGreen(), color.getRed(), 0).getRGB();
            Shape.setEscherProperty(opt, (short)387, rgb);
        }
    }
    
    public PictureData getPictureData() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        final EscherSimpleProperty p = (EscherSimpleProperty)Shape.getEscherProperty(opt, 390);
        if (p == null) {
            return null;
        }
        final SlideShow ppt = this.shape.getSheet().getSlideShow();
        final PictureData[] pict = ppt.getPictureData();
        final Document doc = ppt.getDocumentRecord();
        final EscherContainerRecord dggContainer = doc.getPPDrawingGroup().getDggContainer();
        final EscherContainerRecord bstore = (EscherContainerRecord)Shape.getEscherChild(dggContainer, -4095);
        final List<EscherRecord> lst = bstore.getChildRecords();
        final int idx = p.getPropertyValue();
        if (idx == 0) {
            this.logger.log(POILogger.WARN, "no reference to picture data found ");
        }
        else {
            final EscherBSERecord bse = lst.get(idx - 1);
            for (int i = 0; i < pict.length; ++i) {
                if (pict[i].getOffset() == bse.getOffset()) {
                    return pict[i];
                }
            }
        }
        return null;
    }
    
    public void setPictureData(final int idx) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this.shape.getSpContainer(), -4085);
        Shape.setEscherProperty(opt, (short)16774, idx);
        if (idx != 0 && this.shape.getSheet() != null) {
            final EscherBSERecord bse = this.getEscherBSERecord(idx);
            bse.setRef(bse.getRef() + 1);
        }
    }
}
