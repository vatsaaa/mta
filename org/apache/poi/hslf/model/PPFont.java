// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.FontEntityAtom;

public final class PPFont
{
    public static final byte ANSI_CHARSET = 0;
    public static final byte DEFAULT_CHARSET = 1;
    public static final byte SYMBOL_CHARSET = 2;
    public static final byte DEFAULT_PITCH = 0;
    public static final byte FIXED_PITCH = 1;
    public static final byte VARIABLE_PITCH = 2;
    public static final byte FF_DONTCARE = 0;
    public static final byte FF_ROMAN = 16;
    public static final byte FF_SWISS = 32;
    public static final byte FF_SCRIPT = 64;
    public static final byte FF_MODERN = 48;
    public static final byte FF_DECORATIVE = 80;
    protected int charset;
    protected int type;
    protected int flags;
    protected int pitch;
    protected String name;
    public static final PPFont ARIAL;
    public static final PPFont TIMES_NEW_ROMAN;
    public static final PPFont COURIER_NEW;
    public static final PPFont WINGDINGS;
    
    public PPFont() {
    }
    
    public PPFont(final FontEntityAtom fontAtom) {
        this.name = fontAtom.getFontName();
        this.charset = fontAtom.getCharSet();
        this.type = fontAtom.getFontType();
        this.flags = fontAtom.getFontFlags();
        this.pitch = fontAtom.getPitchAndFamily();
    }
    
    public void setFontName(final String val) {
        this.name = val;
    }
    
    public String getFontName() {
        return this.name;
    }
    
    public void setCharSet(final int val) {
        this.charset = val;
    }
    
    public int getCharSet() {
        return this.charset;
    }
    
    public void setFontFlags(final int val) {
        this.flags = val;
    }
    
    public int getFontFlags() {
        return this.flags;
    }
    
    public void setFontType(final int val) {
        this.type = val;
    }
    
    public int getFontType() {
        return this.type;
    }
    
    public void setPitchAndFamily(final int val) {
        this.pitch = val;
    }
    
    public int getPitchAndFamily() {
        return this.pitch;
    }
    
    static {
        (ARIAL = new PPFont()).setFontName("Arial");
        PPFont.ARIAL.setCharSet(0);
        PPFont.ARIAL.setFontType(4);
        PPFont.ARIAL.setFontFlags(0);
        PPFont.ARIAL.setPitchAndFamily(34);
        (TIMES_NEW_ROMAN = new PPFont()).setFontName("Times New Roman");
        PPFont.TIMES_NEW_ROMAN.setCharSet(0);
        PPFont.TIMES_NEW_ROMAN.setFontType(4);
        PPFont.TIMES_NEW_ROMAN.setFontFlags(0);
        PPFont.TIMES_NEW_ROMAN.setPitchAndFamily(18);
        (COURIER_NEW = new PPFont()).setFontName("Courier New");
        PPFont.COURIER_NEW.setCharSet(0);
        PPFont.COURIER_NEW.setFontType(4);
        PPFont.COURIER_NEW.setFontFlags(0);
        PPFont.COURIER_NEW.setPitchAndFamily(49);
        (WINGDINGS = new PPFont()).setFontName("Wingdings");
        PPFont.WINGDINGS.setCharSet(2);
        PPFont.WINGDINGS.setFontType(4);
        PPFont.WINGDINGS.setFontFlags(0);
        PPFont.WINGDINGS.setPitchAndFamily(2);
    }
}
