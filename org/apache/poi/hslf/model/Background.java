// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.ImageObserver;
import org.apache.poi.util.POILogger;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import org.apache.poi.hslf.blip.Bitmap;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import org.apache.poi.ddf.EscherContainerRecord;

public final class Background extends Shape
{
    protected Background(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final boolean isChild) {
        return null;
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final Fill f = this.getFill();
        final Dimension pg = this.getSheet().getSlideShow().getPageSize();
        final Rectangle anchor = new Rectangle(0, 0, pg.width, pg.height);
        switch (f.getFillType()) {
            case 0: {
                final Color color = f.getForegroundColor();
                graphics.setPaint(color);
                graphics.fill(anchor);
                break;
            }
            case 3: {
                final PictureData data = f.getPictureData();
                if (data instanceof Bitmap) {
                    BufferedImage img = null;
                    try {
                        img = ImageIO.read(new ByteArrayInputStream(data.getData()));
                    }
                    catch (Exception e) {
                        this.logger.log(POILogger.WARN, "ImageIO failed to create image. image.type: " + data.getType());
                        return;
                    }
                    final Image scaledImg = img.getScaledInstance(anchor.width, anchor.height, 4);
                    graphics.drawImage(scaledImg, anchor.x, anchor.y, null);
                    break;
                }
                break;
            }
            default: {
                this.logger.log(POILogger.WARN, "unsuported fill type: " + f.getFillType());
                break;
            }
        }
    }
}
