// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.util.POILogFactory;
import java.awt.Stroke;
import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.awt.BasicStroke;
import java.awt.Paint;
import java.awt.Graphics2D;
import org.apache.poi.util.POILogger;

public final class ShapePainter
{
    protected static POILogger logger;
    
    public static void paint(final SimpleShape shape, final Graphics2D graphics) {
        final Rectangle2D anchor = shape.getLogicalAnchor2D();
        final Shape outline = shape.getOutline();
        if (shape.getFlipVertical()) {
            graphics.translate(anchor.getX(), anchor.getY() + anchor.getHeight());
            graphics.scale(1.0, -1.0);
            graphics.translate(-anchor.getX(), -anchor.getY());
        }
        if (shape.getFlipHorizontal()) {
            graphics.translate(anchor.getX() + anchor.getWidth(), anchor.getY());
            graphics.scale(-1.0, 1.0);
            graphics.translate(-anchor.getX(), -anchor.getY());
        }
        final double angle = shape.getRotation();
        if (angle != 0.0) {
            final double centerX = anchor.getX() + anchor.getWidth() / 2.0;
            final double centerY = anchor.getY() + anchor.getHeight() / 2.0;
            graphics.translate(centerX, centerY);
            graphics.rotate(Math.toRadians(angle));
            graphics.translate(-centerX, -centerY);
        }
        final Color fillColor = shape.getFill().getForegroundColor();
        if (fillColor != null) {
            graphics.setPaint(fillColor);
            graphics.fill(outline);
        }
        final Color lineColor = shape.getLineColor();
        if (lineColor != null) {
            graphics.setPaint(lineColor);
            final float width = (float)shape.getLineWidth();
            final int dashing = shape.getLineDashing();
            float[] dashptrn = null;
            switch (dashing) {
                case 1: {
                    dashptrn = null;
                    break;
                }
                case 2: {
                    dashptrn = new float[] { width, width };
                    break;
                }
                case 6: {
                    dashptrn = new float[] { width * 4.0f, width * 3.0f };
                    break;
                }
                default: {
                    ShapePainter.logger.log(POILogger.WARN, "unsupported dashing: " + dashing);
                    dashptrn = new float[] { width, width };
                    break;
                }
            }
            final Stroke stroke = new BasicStroke(width, 0, 0, 10.0f, dashptrn, 0.0f);
            graphics.setStroke(stroke);
            graphics.draw(outline);
        }
    }
    
    static {
        ShapePainter.logger = POILogFactory.getLogger(ShapePainter.class);
    }
}
