// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.util.POILogFactory;
import java.util.Iterator;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.hslf.record.OEShapeAtom;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.hslf.record.InteractiveInfo;
import org.apache.poi.ddf.EscherSpRecord;
import java.util.List;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherPropertyFactory;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.util.POILogger;

public final class ShapeFactory
{
    protected static POILogger logger;
    
    public static Shape createShape(final EscherContainerRecord spContainer, final Shape parent) {
        if (spContainer.getRecordId() == -4093) {
            return createShapeGroup(spContainer, parent);
        }
        return createSimpeShape(spContainer, parent);
    }
    
    public static ShapeGroup createShapeGroup(final EscherContainerRecord spContainer, final Shape parent) {
        ShapeGroup group = null;
        final EscherRecord opt = Shape.getEscherChild((EscherContainerRecord)spContainer.getChild(0), -3806);
        if (opt != null) {
            try {
                final EscherPropertyFactory f = new EscherPropertyFactory();
                final List props = f.createProperties(opt.serialize(), 8, opt.getInstance());
                final EscherSimpleProperty p = props.get(0);
                if (p.getPropertyNumber() == 927 && p.getPropertyValue() == 1) {
                    group = new Table(spContainer, parent);
                }
                else {
                    group = new ShapeGroup(spContainer, parent);
                }
            }
            catch (Exception e) {
                ShapeFactory.logger.log(POILogger.WARN, e.getMessage());
                group = new ShapeGroup(spContainer, parent);
            }
        }
        else {
            group = new ShapeGroup(spContainer, parent);
        }
        return group;
    }
    
    public static Shape createSimpeShape(final EscherContainerRecord spContainer, final Shape parent) {
        Shape shape = null;
        final EscherSpRecord spRecord = spContainer.getChildById((short)(-4086));
        final int type = spRecord.getShapeType();
        switch (type) {
            case 202: {
                shape = new TextBox(spContainer, parent);
                break;
            }
            case 75:
            case 201: {
                final InteractiveInfo info = (InteractiveInfo)getClientDataRecord(spContainer, RecordTypes.InteractiveInfo.typeID);
                final OEShapeAtom oes = (OEShapeAtom)getClientDataRecord(spContainer, RecordTypes.OEShapeAtom.typeID);
                if (info != null && info.getInteractiveInfoAtom() != null) {
                    switch (info.getInteractiveInfoAtom().getAction()) {
                        case 5: {
                            shape = new OLEShape(spContainer, parent);
                            break;
                        }
                        case 6: {
                            shape = new MovieShape(spContainer, parent);
                            break;
                        }
                    }
                }
                else if (oes != null) {
                    shape = new OLEShape(spContainer, parent);
                }
                if (shape == null) {
                    shape = new Picture(spContainer, parent);
                    break;
                }
                break;
            }
            case 20: {
                shape = new Line(spContainer, parent);
                break;
            }
            case 0: {
                final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(spContainer, -4085);
                final EscherProperty prop = Shape.getEscherProperty(opt, 325);
                if (prop != null) {
                    shape = new Freeform(spContainer, parent);
                    break;
                }
                ShapeFactory.logger.log(POILogger.WARN, "Creating AutoShape for a NotPrimitive shape");
                shape = new AutoShape(spContainer, parent);
                break;
            }
            default: {
                shape = new AutoShape(spContainer, parent);
                break;
            }
        }
        return shape;
    }
    
    protected static Record getClientDataRecord(final EscherContainerRecord spContainer, final int recordType) {
        final Record oep = null;
        final Iterator<EscherRecord> it = spContainer.getChildIterator();
        while (it.hasNext()) {
            final EscherRecord obj = it.next();
            if (obj.getRecordId() == -4079) {
                final byte[] data = obj.serialize();
                final Record[] records = Record.findChildRecords(data, 8, data.length - 8);
                for (int j = 0; j < records.length; ++j) {
                    if (records[j].getRecordType() == recordType) {
                        return records[j];
                    }
                }
            }
        }
        return oep;
    }
    
    static {
        ShapeFactory.logger = POILogFactory.getLogger(ShapeFactory.class);
    }
}
