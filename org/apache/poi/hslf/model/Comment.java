// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.Comment2000;

public final class Comment
{
    private Comment2000 _comment2000;
    
    public Comment(final Comment2000 comment2000) {
        this._comment2000 = comment2000;
    }
    
    protected Comment2000 getComment2000() {
        return this._comment2000;
    }
    
    public String getAuthor() {
        return this._comment2000.getAuthor();
    }
    
    public void setAuthor(final String author) {
        this._comment2000.setAuthor(author);
    }
    
    public String getAuthorInitials() {
        return this._comment2000.getAuthorInitials();
    }
    
    public void setAuthorInitials(final String initials) {
        this._comment2000.setAuthorInitials(initials);
    }
    
    public String getText() {
        return this._comment2000.getText();
    }
    
    public void setText(final String text) {
        this._comment2000.setText(text);
    }
}
