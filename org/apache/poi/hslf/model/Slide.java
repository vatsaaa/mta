// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.HeadersFootersContainer;
import java.awt.Graphics2D;
import org.apache.poi.hslf.record.Comment2000;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.hslf.record.RecordContainer;
import org.apache.poi.hslf.record.ColorSchemeAtom;
import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;
import java.util.Iterator;
import org.apache.poi.ddf.EscherDggRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherDgRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.hslf.record.SlideAtom;
import java.util.List;
import java.util.Vector;
import org.apache.poi.hslf.record.SheetContainer;
import org.apache.poi.hslf.record.SlideListWithText;

public final class Slide extends Sheet
{
    private int _slideNo;
    private SlideListWithText.SlideAtomsSet _atomSet;
    private TextRun[] _runs;
    private Notes _notes;
    
    public Slide(final org.apache.poi.hslf.record.Slide slide, final Notes notes, final SlideListWithText.SlideAtomsSet atomSet, final int slideIdentifier, final int slideNumber) {
        super(slide, slideIdentifier);
        this._notes = notes;
        this._atomSet = atomSet;
        this._slideNo = slideNumber;
        final TextRun[] _otherRuns = Sheet.findTextRuns(this.getPPDrawing());
        final Vector textRuns = new Vector();
        if (this._atomSet != null) {
            Sheet.findTextRuns(this._atomSet.getSlideRecords(), textRuns);
        }
        this._runs = new TextRun[textRuns.size() + _otherRuns.length];
        int i;
        for (i = 0, i = 0; i < textRuns.size(); ++i) {
            (this._runs[i] = textRuns.get(i)).setSheet(this);
        }
        for (int k = 0; k < _otherRuns.length; ++k) {
            (this._runs[i] = _otherRuns[k]).setSheet(this);
            this._runs[i].setIndex(-1);
            ++i;
        }
    }
    
    public Slide(final int sheetNumber, final int sheetRefId, final int slideNumber) {
        super(new org.apache.poi.hslf.record.Slide(), sheetNumber);
        this._slideNo = slideNumber;
        this.getSheetContainer().setSheetId(sheetRefId);
    }
    
    public void setNotes(final Notes notes) {
        this._notes = notes;
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        if (notes == null) {
            sa.setNotesID(0);
        }
        else {
            sa.setNotesID(notes._getSheetNumber());
        }
    }
    
    public void setSlideNumber(final int newSlideNumber) {
        this._slideNo = newSlideNumber;
    }
    
    @Override
    public void onCreate() {
        final EscherDggRecord dgg = this.getSlideShow().getDocumentRecord().getPPDrawingGroup().getEscherDggRecord();
        final EscherContainerRecord dgContainer = (EscherContainerRecord)this.getSheetContainer().getPPDrawing().getEscherRecords()[0];
        final EscherDgRecord dg = (EscherDgRecord)Shape.getEscherChild(dgContainer, -4088);
        final int dgId = dgg.getMaxDrawingGroupId() + 1;
        dg.setOptions((short)(dgId << 4));
        dgg.setDrawingsSaved(dgg.getDrawingsSaved() + 1);
        dgg.setMaxDrawingGroupId(dgId);
        for (final EscherContainerRecord c : dgContainer.getChildContainers()) {
            EscherSpRecord spr = null;
            switch (c.getRecordId()) {
                case -4093: {
                    final EscherContainerRecord dc = (EscherContainerRecord)c.getChild(0);
                    spr = dc.getChildById((short)(-4086));
                    break;
                }
                case -4092: {
                    spr = c.getChildById((short)(-4086));
                    break;
                }
            }
            if (spr != null) {
                spr.setShapeId(this.allocateShapeId());
            }
        }
        dg.setNumShapes(1);
    }
    
    public TextBox addTitle() {
        final Placeholder pl = new Placeholder();
        pl.setShapeType(1);
        pl.getTextRun().setRunType(0);
        pl.setText("Click to edit title");
        pl.setAnchor(new Rectangle(54, 48, 612, 90));
        this.addShape(pl);
        return pl;
    }
    
    public String getTitle() {
        final TextRun[] txt = this.getTextRuns();
        for (int i = 0; i < txt.length; ++i) {
            final int type = txt[i].getRunType();
            if (type == 6 || type == 0) {
                final String title = txt[i].getText();
                return title;
            }
        }
        return null;
    }
    
    @Override
    public TextRun[] getTextRuns() {
        return this._runs;
    }
    
    public int getSlideNumber() {
        return this._slideNo;
    }
    
    public org.apache.poi.hslf.record.Slide getSlideRecord() {
        return (org.apache.poi.hslf.record.Slide)this.getSheetContainer();
    }
    
    public Notes getNotesSheet() {
        return this._notes;
    }
    
    protected SlideListWithText.SlideAtomsSet getSlideAtomsSet() {
        return this._atomSet;
    }
    
    @Override
    public MasterSheet getMasterSheet() {
        final SlideMaster[] master = this.getSlideShow().getSlidesMasters();
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        final int masterId = sa.getMasterID();
        MasterSheet sheet = null;
        for (int i = 0; i < master.length; ++i) {
            if (masterId == master[i]._getSheetNumber()) {
                sheet = master[i];
                break;
            }
        }
        if (sheet == null) {
            final TitleMaster[] titleMaster = this.getSlideShow().getTitleMasters();
            if (titleMaster != null) {
                for (int j = 0; j < titleMaster.length; ++j) {
                    if (masterId == titleMaster[j]._getSheetNumber()) {
                        sheet = titleMaster[j];
                        break;
                    }
                }
            }
        }
        return sheet;
    }
    
    public void setMasterSheet(final MasterSheet master) {
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        final int sheetNo = master._getSheetNumber();
        sa.setMasterID(sheetNo);
    }
    
    public void setFollowMasterBackground(final boolean flag) {
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        sa.setFollowMasterBackground(flag);
    }
    
    public boolean getFollowMasterBackground() {
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        return sa.getFollowMasterBackground();
    }
    
    public void setFollowMasterObjects(final boolean flag) {
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        sa.setFollowMasterObjects(flag);
    }
    
    public boolean getFollowMasterScheme() {
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        return sa.getFollowMasterScheme();
    }
    
    public void setFollowMasterScheme(final boolean flag) {
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        sa.setFollowMasterScheme(flag);
    }
    
    public boolean getFollowMasterObjects() {
        final SlideAtom sa = this.getSlideRecord().getSlideAtom();
        return sa.getFollowMasterObjects();
    }
    
    @Override
    public Background getBackground() {
        if (this.getFollowMasterBackground()) {
            return this.getMasterSheet().getBackground();
        }
        return super.getBackground();
    }
    
    @Override
    public ColorSchemeAtom getColorScheme() {
        if (this.getFollowMasterScheme()) {
            return this.getMasterSheet().getColorScheme();
        }
        return super.getColorScheme();
    }
    
    public Comment[] getComments() {
        final RecordContainer progTags = (RecordContainer)this.getSheetContainer().findFirstOfType(RecordTypes.ProgTags.typeID);
        if (progTags != null) {
            final RecordContainer progBinaryTag = (RecordContainer)progTags.findFirstOfType(RecordTypes.ProgBinaryTag.typeID);
            if (progBinaryTag != null) {
                final RecordContainer binaryTags = (RecordContainer)progBinaryTag.findFirstOfType(RecordTypes.BinaryTagData.typeID);
                if (binaryTags != null) {
                    int count = 0;
                    for (int i = 0; i < binaryTags.getChildRecords().length; ++i) {
                        if (binaryTags.getChildRecords()[i] instanceof Comment2000) {
                            ++count;
                        }
                    }
                    final Comment[] comments = new Comment[count];
                    count = 0;
                    for (int j = 0; j < binaryTags.getChildRecords().length; ++j) {
                        if (binaryTags.getChildRecords()[j] instanceof Comment2000) {
                            comments[j] = new Comment((Comment2000)binaryTags.getChildRecords()[j]);
                            ++count;
                        }
                    }
                    return comments;
                }
            }
        }
        return new Comment[0];
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final MasterSheet master = this.getMasterSheet();
        final Background bg = this.getBackground();
        if (bg != null) {
            bg.draw(graphics);
        }
        if (this.getFollowMasterObjects()) {
            final Shape[] sh = master.getShapes();
            for (int i = 0; i < sh.length; ++i) {
                if (!MasterSheet.isPlaceholder(sh[i])) {
                    sh[i].draw(graphics);
                }
            }
        }
        final Shape[] sh = this.getShapes();
        for (int i = 0; i < sh.length; ++i) {
            sh[i].draw(graphics);
        }
    }
    
    public HeadersFooters getHeadersFooters() {
        HeadersFootersContainer hdd = null;
        final Record[] ch = this.getSheetContainer().getChildRecords();
        boolean ppt2007 = false;
        for (int i = 0; i < ch.length; ++i) {
            if (ch[i] instanceof HeadersFootersContainer) {
                hdd = (HeadersFootersContainer)ch[i];
            }
            else if (ch[i].getRecordType() == RecordTypes.RoundTripContentMasterId.typeID) {
                ppt2007 = true;
            }
        }
        boolean newRecord = false;
        if (hdd == null && !ppt2007) {
            return this.getSlideShow().getSlideHeadersFooters();
        }
        if (hdd == null) {
            hdd = new HeadersFootersContainer((short)63);
            newRecord = true;
        }
        return new HeadersFooters(hdd, this, newRecord, ppt2007);
    }
    
    @Override
    protected void onAddTextShape(final TextShape shape) {
        final TextRun run = shape.getTextRun();
        if (this._runs == null) {
            this._runs = new TextRun[] { run };
        }
        else {
            final TextRun[] tmp = new TextRun[this._runs.length + 1];
            System.arraycopy(this._runs, 0, tmp, 0, this._runs.length);
            tmp[tmp.length - 1] = run;
            this._runs = tmp;
        }
    }
}
