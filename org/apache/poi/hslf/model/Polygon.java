// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.geom.Point2D;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherArrayProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public final class Polygon extends AutoShape
{
    protected Polygon(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public Polygon(final Shape parent) {
        super(null, parent);
        this._escherContainer = this.createSpContainer(0, parent instanceof ShapeGroup);
    }
    
    public Polygon() {
        this(null);
    }
    
    public void setPoints(final float[] xPoints, final float[] yPoints) {
        final float right = this.findBiggest(xPoints);
        final float bottom = this.findBiggest(yPoints);
        final float left = this.findSmallest(xPoints);
        final float top = this.findSmallest(yPoints);
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        opt.addEscherProperty(new EscherSimpleProperty((short)322, (int)((right - left) * 72.0f / 576.0f)));
        opt.addEscherProperty(new EscherSimpleProperty((short)323, (int)((bottom - top) * 72.0f / 576.0f)));
        for (int i = 0; i < xPoints.length; ++i) {
            final int n = i;
            xPoints[n] += -left;
            final int n2 = i;
            yPoints[n2] += -top;
        }
        final int numpoints = xPoints.length;
        final EscherArrayProperty verticesProp = new EscherArrayProperty((short)325, false, new byte[0]);
        verticesProp.setNumberOfElementsInArray(numpoints + 1);
        verticesProp.setNumberOfElementsInMemory(numpoints + 1);
        verticesProp.setSizeOfElements(65520);
        for (int j = 0; j < numpoints; ++j) {
            final byte[] data = new byte[4];
            LittleEndian.putShort(data, 0, (short)(xPoints[j] * 72.0f / 576.0f));
            LittleEndian.putShort(data, 2, (short)(yPoints[j] * 72.0f / 576.0f));
            verticesProp.setElement(j, data);
        }
        final byte[] data2 = new byte[4];
        LittleEndian.putShort(data2, 0, (short)(xPoints[0] * 72.0f / 576.0f));
        LittleEndian.putShort(data2, 2, (short)(yPoints[0] * 72.0f / 576.0f));
        verticesProp.setElement(numpoints, data2);
        opt.addEscherProperty(verticesProp);
        final EscherArrayProperty segmentsProp = new EscherArrayProperty((short)326, false, null);
        segmentsProp.setSizeOfElements(2);
        segmentsProp.setNumberOfElementsInArray(numpoints * 2 + 4);
        segmentsProp.setNumberOfElementsInMemory(numpoints * 2 + 4);
        segmentsProp.setElement(0, new byte[] { 0, 64 });
        segmentsProp.setElement(1, new byte[] { 0, -84 });
        for (int k = 0; k < numpoints; ++k) {
            segmentsProp.setElement(2 + k * 2, new byte[] { 1, 0 });
            segmentsProp.setElement(3 + k * 2, new byte[] { 0, -84 });
        }
        segmentsProp.setElement(segmentsProp.getNumberOfElementsInArray() - 2, new byte[] { 1, 96 });
        segmentsProp.setElement(segmentsProp.getNumberOfElementsInArray() - 1, new byte[] { 0, -128 });
        opt.addEscherProperty(segmentsProp);
        opt.sortProperties();
    }
    
    public void setPoints(final Point2D[] points) {
        final float[] xpoints = new float[points.length];
        final float[] ypoints = new float[points.length];
        for (int i = 0; i < points.length; ++i) {
            xpoints[i] = (float)points[i].getX();
            ypoints[i] = (float)points[i].getY();
        }
        this.setPoints(xpoints, ypoints);
    }
    
    private float findBiggest(final float[] values) {
        float result = Float.MIN_VALUE;
        for (int i = 0; i < values.length; ++i) {
            if (values[i] > result) {
                result = values[i];
            }
        }
        return result;
    }
    
    private float findSmallest(final float[] values) {
        float result = Float.MAX_VALUE;
        for (int i = 0; i < values.length; ++i) {
            if (values[i] < result) {
                result = values[i];
            }
        }
        return result;
    }
}
