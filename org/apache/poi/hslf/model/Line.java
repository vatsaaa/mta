// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.geom.Rectangle2D;
import java.awt.geom.Line2D;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public final class Line extends SimpleShape
{
    public static final int PEN_SOLID = 1;
    public static final int PEN_PS_DASH = 2;
    public static final int PEN_DOT = 3;
    public static final int PEN_DASHDOT = 4;
    public static final int PEN_DASHDOTDOT = 5;
    public static final int PEN_DOTGEL = 6;
    public static final int PEN_DASH = 7;
    public static final int PEN_LONGDASHGEL = 8;
    public static final int PEN_DASHDOTGEL = 9;
    public static final int PEN_LONGDASHDOTGEL = 10;
    public static final int PEN_LONGDASHDOTDOTGEL = 11;
    public static final int LINE_SIMPLE = 0;
    public static final int LINE_DOUBLE = 1;
    public static final int LINE_THICKTHIN = 2;
    public static final int LINE_THINTHICK = 3;
    public static final int LINE_TRIPLE = 4;
    
    protected Line(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public Line(final Shape parent) {
        super(null, parent);
        this._escherContainer = this.createSpContainer(parent instanceof ShapeGroup);
    }
    
    public Line() {
        this(null);
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final boolean isChild) {
        this._escherContainer = super.createSpContainer(isChild);
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        final short type = 322;
        spRecord.setOptions(type);
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        Shape.setEscherProperty(opt, (short)324, 4);
        Shape.setEscherProperty(opt, (short)383, 65536);
        Shape.setEscherProperty(opt, (short)447, 1048576);
        Shape.setEscherProperty(opt, (short)448, 134217729);
        Shape.setEscherProperty(opt, (short)511, 655368);
        Shape.setEscherProperty(opt, (short)513, 134217730);
        return this._escherContainer;
    }
    
    @Override
    public java.awt.Shape getOutline() {
        final Rectangle2D anchor = this.getLogicalAnchor2D();
        return new Line2D.Double(anchor.getX(), anchor.getY(), anchor.getX() + anchor.getWidth(), anchor.getY() + anchor.getHeight());
    }
}
