// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public final class TableCell extends TextBox
{
    protected static final int DEFAULT_WIDTH = 100;
    protected static final int DEFAULT_HEIGHT = 40;
    private Line borderLeft;
    private Line borderRight;
    private Line borderTop;
    private Line borderBottom;
    
    protected TableCell(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public TableCell(final Shape parent) {
        super(parent);
        this.setShapeType(1);
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final boolean isChild) {
        this._escherContainer = super.createSpContainer(isChild);
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        Shape.setEscherProperty(opt, (short)128, 0);
        Shape.setEscherProperty(opt, (short)191, 131072);
        Shape.setEscherProperty(opt, (short)447, 1376257);
        Shape.setEscherProperty(opt, (short)575, 131072);
        Shape.setEscherProperty(opt, (short)127, 262144);
        return this._escherContainer;
    }
    
    protected void anchorBorder(final int type, final Line line) {
        final Rectangle cellAnchor = this.getAnchor();
        final Rectangle lineAnchor = new Rectangle();
        switch (type) {
            case 1: {
                lineAnchor.x = cellAnchor.x;
                lineAnchor.y = cellAnchor.y;
                lineAnchor.width = cellAnchor.width;
                lineAnchor.height = 0;
                break;
            }
            case 2: {
                lineAnchor.x = cellAnchor.x + cellAnchor.width;
                lineAnchor.y = cellAnchor.y;
                lineAnchor.width = 0;
                lineAnchor.height = cellAnchor.height;
                break;
            }
            case 3: {
                lineAnchor.x = cellAnchor.x;
                lineAnchor.y = cellAnchor.y + cellAnchor.height;
                lineAnchor.width = cellAnchor.width;
                lineAnchor.height = 0;
                break;
            }
            case 4: {
                lineAnchor.x = cellAnchor.x;
                lineAnchor.y = cellAnchor.y;
                lineAnchor.width = 0;
                lineAnchor.height = cellAnchor.height;
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown border type: " + type);
            }
        }
        line.setAnchor(lineAnchor);
    }
    
    public Line getBorderLeft() {
        return this.borderLeft;
    }
    
    public void setBorderLeft(final Line line) {
        if (line != null) {
            this.anchorBorder(4, line);
        }
        this.borderLeft = line;
    }
    
    public Line getBorderRight() {
        return this.borderRight;
    }
    
    public void setBorderRight(final Line line) {
        if (line != null) {
            this.anchorBorder(2, line);
        }
        this.borderRight = line;
    }
    
    public Line getBorderTop() {
        return this.borderTop;
    }
    
    public void setBorderTop(final Line line) {
        if (line != null) {
            this.anchorBorder(1, line);
        }
        this.borderTop = line;
    }
    
    public Line getBorderBottom() {
        return this.borderBottom;
    }
    
    public void setBorderBottom(final Line line) {
        if (line != null) {
            this.anchorBorder(3, line);
        }
        this.borderBottom = line;
    }
    
    public void setAnchor(final Rectangle anchor) {
        super.setAnchor(anchor);
        if (this.borderTop != null) {
            this.anchorBorder(1, this.borderTop);
        }
        if (this.borderRight != null) {
            this.anchorBorder(2, this.borderRight);
        }
        if (this.borderBottom != null) {
            this.anchorBorder(3, this.borderBottom);
        }
        if (this.borderLeft != null) {
            this.anchorBorder(4, this.borderLeft);
        }
    }
}
