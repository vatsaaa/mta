// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.ExVideoContainer;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.record.ExMCIMovie;
import org.apache.poi.hslf.record.ExObjList;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.hslf.record.AnimationInfoAtom;
import org.apache.poi.hslf.record.InteractiveInfoAtom;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.record.AnimationInfo;
import org.apache.poi.hslf.record.InteractiveInfo;
import org.apache.poi.hslf.record.OEShapeAtom;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public final class MovieShape extends Picture
{
    public static final int DEFAULT_MOVIE_THUMBNAIL = -1;
    public static final int MOVIE_MPEG = 1;
    public static final int MOVIE_AVI = 2;
    
    public MovieShape(final int movieIdx, final int pictureIdx) {
        super(pictureIdx, null);
        this.setMovieIndex(movieIdx);
        this.setAutoPlay(true);
    }
    
    public MovieShape(final int movieIdx, final int idx, final Shape parent) {
        super(idx, parent);
        this.setMovieIndex(movieIdx);
    }
    
    protected MovieShape(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final int idx, final boolean isChild) {
        this._escherContainer = super.createSpContainer(idx, isChild);
        this.setEscherProperty((short)127, 16777472);
        this.setEscherProperty((short)447, 65537);
        final EscherClientDataRecord cldata = new EscherClientDataRecord();
        cldata.setOptions((short)15);
        this._escherContainer.addChildRecord(cldata);
        final OEShapeAtom oe = new OEShapeAtom();
        final InteractiveInfo info = new InteractiveInfo();
        final InteractiveInfoAtom infoAtom = info.getInteractiveInfoAtom();
        infoAtom.setAction((byte)6);
        infoAtom.setHyperlinkType((byte)(-1));
        final AnimationInfo an = new AnimationInfo();
        final AnimationInfoAtom anAtom = an.getAnimationInfoAtom();
        anAtom.setFlag(4, true);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            oe.writeOut(out);
            an.writeOut(out);
            info.writeOut(out);
        }
        catch (Exception e) {
            throw new HSLFException(e);
        }
        cldata.setRemainingData(out.toByteArray());
        return this._escherContainer;
    }
    
    public void setMovieIndex(final int idx) {
        final OEShapeAtom oe = (OEShapeAtom)this.getClientDataRecord(RecordTypes.OEShapeAtom.typeID);
        oe.setOptions(idx);
        final AnimationInfo an = (AnimationInfo)this.getClientDataRecord(RecordTypes.AnimationInfo.typeID);
        if (an != null) {
            final AnimationInfoAtom ai = an.getAnimationInfoAtom();
            ai.setDimColor(117440512);
            ai.setFlag(4, true);
            ai.setFlag(256, true);
            ai.setFlag(1024, true);
            ai.setOrderID(idx + 1);
        }
    }
    
    public void setAutoPlay(final boolean flag) {
        final AnimationInfo an = (AnimationInfo)this.getClientDataRecord(RecordTypes.AnimationInfo.typeID);
        if (an != null) {
            an.getAnimationInfoAtom().setFlag(4, flag);
            this.updateClientData();
        }
    }
    
    public boolean isAutoPlay() {
        final AnimationInfo an = (AnimationInfo)this.getClientDataRecord(RecordTypes.AnimationInfo.typeID);
        return an != null && an.getAnimationInfoAtom().getFlag(4);
    }
    
    public String getPath() {
        final OEShapeAtom oe = (OEShapeAtom)this.getClientDataRecord(RecordTypes.OEShapeAtom.typeID);
        final int idx = oe.getOptions();
        final SlideShow ppt = this.getSheet().getSlideShow();
        final ExObjList lst = (ExObjList)ppt.getDocumentRecord().findFirstOfType(RecordTypes.ExObjList.typeID);
        if (lst == null) {
            return null;
        }
        final Record[] r = lst.getChildRecords();
        for (int i = 0; i < r.length; ++i) {
            if (r[i] instanceof ExMCIMovie) {
                final ExMCIMovie mci = (ExMCIMovie)r[i];
                final ExVideoContainer exVideo = mci.getExVideo();
                final int objectId = exVideo.getExMediaAtom().getObjectId();
                if (objectId == idx) {
                    return exVideo.getPathAtom().getText();
                }
            }
        }
        return null;
    }
}
