// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.Color;
import java.awt.font.TextLayout;
import org.apache.poi.hslf.record.TextRulerAtom;
import java.util.List;
import java.util.Map;
import java.awt.Font;
import java.awt.font.LineBreakMeasurer;
import java.util.ArrayList;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;
import java.awt.Graphics2D;
import org.apache.poi.hslf.usermodel.RichTextRun;
import java.text.AttributedCharacterIterator;
import java.awt.font.TextAttribute;
import java.text.AttributedString;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;

public final class TextPainter
{
    protected POILogger logger;
    protected static final char DEFAULT_BULLET_CHAR = '\u25a0';
    protected TextShape _shape;
    
    public TextPainter(final TextShape shape) {
        this.logger = POILogFactory.getLogger(this.getClass());
        this._shape = shape;
    }
    
    public AttributedString getAttributedString(final TextRun txrun) {
        String text = txrun.getText();
        text = text.replace('\t', ' ');
        text = text.replace(' ', ' ');
        final AttributedString at = new AttributedString(text);
        final RichTextRun[] rt = txrun.getRichTextRuns();
        for (int i = 0; i < rt.length; ++i) {
            final int start = rt[i].getStartIndex();
            final int end = rt[i].getEndIndex();
            if (start == end) {
                this.logger.log(POILogger.INFO, "Skipping RichTextRun with zero length");
            }
            else {
                at.addAttribute(TextAttribute.FAMILY, rt[i].getFontName(), start, end);
                at.addAttribute(TextAttribute.SIZE, new Float((float)rt[i].getFontSize()), start, end);
                at.addAttribute(TextAttribute.FOREGROUND, rt[i].getFontColor(), start, end);
                if (rt[i].isBold()) {
                    at.addAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD, start, end);
                }
                if (rt[i].isItalic()) {
                    at.addAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE, start, end);
                }
                if (rt[i].isUnderlined()) {
                    at.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, start, end);
                    at.addAttribute(TextAttribute.INPUT_METHOD_UNDERLINE, TextAttribute.UNDERLINE_LOW_TWO_PIXEL, start, end);
                }
                if (rt[i].isStrikethrough()) {
                    at.addAttribute(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON, start, end);
                }
                final int superScript = rt[i].getSuperscript();
                if (superScript != 0) {
                    at.addAttribute(TextAttribute.SUPERSCRIPT, (superScript > 0) ? TextAttribute.SUPERSCRIPT_SUPER : TextAttribute.SUPERSCRIPT_SUB, start, end);
                }
            }
        }
        return at;
    }
    
    public void paint(final Graphics2D graphics) {
        final Rectangle2D anchor = this._shape.getLogicalAnchor2D();
        final TextElement[] elem = this.getTextElements((float)anchor.getWidth(), graphics.getFontRenderContext());
        if (elem == null) {
            return;
        }
        float textHeight = 0.0f;
        for (int i = 0; i < elem.length; ++i) {
            textHeight += elem[i].ascent + elem[i].descent;
        }
        final int valign = this._shape.getVerticalAlignment();
        double y0 = anchor.getY();
        switch (valign) {
            case 0:
            case 6: {
                y0 += this._shape.getMarginTop();
                break;
            }
            case 2: {
                y0 += anchor.getHeight() - textHeight - this._shape.getMarginBottom();
                break;
            }
            default: {
                final float delta = (float)anchor.getHeight() - textHeight - this._shape.getMarginTop() - this._shape.getMarginBottom();
                y0 += this._shape.getMarginTop() + delta / 2.0f;
                break;
            }
        }
        for (int j = 0; j < elem.length; ++j) {
            y0 += elem[j].ascent;
            final Point2D.Double pen = new Point2D.Double();
            pen.y = y0;
            switch (elem[j]._align) {
                default: {
                    pen.x = anchor.getX() + this._shape.getMarginLeft();
                    break;
                }
                case 1: {
                    pen.x = anchor.getX() + this._shape.getMarginLeft() + (anchor.getWidth() - elem[j].advance - this._shape.getMarginLeft() - this._shape.getMarginRight()) / 2.0;
                    break;
                }
                case 2: {
                    pen.x = anchor.getX() + this._shape.getMarginLeft() + (anchor.getWidth() - elem[j].advance - this._shape.getMarginLeft() - this._shape.getMarginRight());
                    break;
                }
            }
            if (elem[j]._bullet != null) {
                graphics.drawString(elem[j]._bullet.getIterator(), (float)(pen.x + elem[j]._bulletOffset), (float)pen.y);
            }
            final AttributedCharacterIterator chIt = elem[j]._text.getIterator();
            if (chIt.getEndIndex() > chIt.getBeginIndex()) {
                graphics.drawString(chIt, (float)(pen.x + elem[j]._textOffset), (float)pen.y);
            }
            y0 += elem[j].descent;
        }
    }
    
    public TextElement[] getTextElements(final float textWidth, final FontRenderContext frc) {
        final TextRun run = this._shape.getTextRun();
        if (run == null) {
            return null;
        }
        final String text = run.getText();
        if (text == null || text.equals("")) {
            return null;
        }
        final AttributedString at = this.getAttributedString(run);
        final AttributedCharacterIterator it = at.getIterator();
        final int paragraphStart = it.getBeginIndex();
        final int paragraphEnd = it.getEndIndex();
        final List<TextElement> lines = new ArrayList<TextElement>();
        final LineBreakMeasurer measurer = new LineBreakMeasurer(it, frc);
        measurer.setPosition(paragraphStart);
        while (measurer.getPosition() < paragraphEnd) {
            int startIndex = measurer.getPosition();
            final int nextBreak = text.indexOf(10, measurer.getPosition() + 1);
            final boolean prStart = text.charAt(startIndex) == '\n';
            if (prStart) {
                measurer.setPosition(startIndex++);
            }
            final RichTextRun rt = run.getRichTextRunAt((startIndex == text.length()) ? (startIndex - 1) : startIndex);
            if (rt == null) {
                this.logger.log(POILogger.WARN, "RichTextRun not found at pos" + startIndex + "; text.length: " + text.length());
                break;
            }
            float wrappingWidth = textWidth - this._shape.getMarginLeft() - this._shape.getMarginRight();
            int bulletOffset = rt.getBulletOffset();
            int textOffset = rt.getTextOffset();
            final int indent = rt.getIndentLevel();
            final TextRulerAtom ruler = run.getTextRuler();
            if (ruler != null) {
                int bullet_val = ruler.getBulletOffsets()[indent] * 72 / 576;
                int text_val = ruler.getTextOffsets()[indent] * 72 / 576;
                if (bullet_val > text_val) {
                    final int a = bullet_val;
                    bullet_val = text_val;
                    text_val = a;
                }
                if (bullet_val != 0) {
                    bulletOffset = bullet_val;
                }
                if (text_val != 0) {
                    textOffset = text_val;
                }
            }
            if (bulletOffset > 0 || prStart || startIndex == 0) {
                wrappingWidth -= textOffset;
            }
            if (this._shape.getWordWrap() == 2) {
                wrappingWidth = (float)this._shape.getSheet().getSlideShow().getPageSize().width;
            }
            TextLayout textLayout = measurer.nextLayout(wrappingWidth + 1.0f, (nextBreak == -1) ? paragraphEnd : nextBreak, true);
            if (textLayout == null) {
                textLayout = measurer.nextLayout(textWidth, (nextBreak == -1) ? paragraphEnd : nextBreak, false);
            }
            if (textLayout == null) {
                this.logger.log(POILogger.WARN, "Failed to break text into lines: wrappingWidth: " + wrappingWidth + "; text: " + rt.getText());
                measurer.setPosition(rt.getEndIndex());
            }
            else {
                final int endIndex = measurer.getPosition();
                final float lineHeight = (float)textLayout.getBounds().getHeight();
                int linespacing = rt.getLineSpacing();
                if (linespacing == 0) {
                    linespacing = 100;
                }
                final TextElement el = new TextElement();
                if (linespacing >= 0) {
                    el.ascent = textLayout.getAscent() * linespacing / 100.0f;
                }
                else {
                    el.ascent = (float)(-linespacing * 72 / 576);
                }
                el._align = rt.getAlignment();
                el.advance = textLayout.getAdvance();
                el._textOffset = textOffset;
                el._text = new AttributedString(it, startIndex, endIndex);
                el.textStartIndex = startIndex;
                el.textEndIndex = endIndex;
                if (prStart) {
                    final int sp = rt.getSpaceBefore();
                    float spaceBefore;
                    if (sp >= 0) {
                        spaceBefore = lineHeight * sp / 100.0f;
                    }
                    else {
                        spaceBefore = (float)(-sp * 72 / 576);
                    }
                    final TextElement textElement = el;
                    textElement.ascent += spaceBefore;
                }
                float descent;
                if (linespacing >= 0) {
                    descent = (textLayout.getDescent() + textLayout.getLeading()) * linespacing / 100.0f;
                }
                else {
                    descent = (float)(-linespacing * 72 / 576);
                }
                if (prStart) {
                    final int sp2 = rt.getSpaceAfter();
                    float spaceAfter;
                    if (sp2 >= 0) {
                        spaceAfter = lineHeight * sp2 / 100.0f;
                    }
                    else {
                        spaceAfter = (float)(-sp2 * 72 / 576);
                    }
                    final TextElement textElement2 = el;
                    textElement2.ascent += spaceAfter;
                }
                el.descent = descent;
                if (rt.isBullet() && (prStart || startIndex == 0)) {
                    it.setIndex(startIndex);
                    AttributedString bat = new AttributedString(Character.toString(rt.getBulletChar()));
                    final Color clr = rt.getBulletColor();
                    if (clr != null) {
                        bat.addAttribute(TextAttribute.FOREGROUND, clr);
                    }
                    else {
                        bat.addAttribute(TextAttribute.FOREGROUND, it.getAttribute(TextAttribute.FOREGROUND));
                    }
                    int fontIdx = rt.getBulletFont();
                    if (fontIdx == -1) {
                        fontIdx = rt.getFontIndex();
                    }
                    final PPFont bulletFont = this._shape.getSheet().getSlideShow().getFont(fontIdx);
                    bat.addAttribute(TextAttribute.FAMILY, bulletFont.getFontName());
                    final int bulletSize = rt.getBulletSize();
                    int fontSize = rt.getFontSize();
                    if (bulletSize != -1) {
                        fontSize = Math.round(fontSize * bulletSize * 0.01f);
                    }
                    bat.addAttribute(TextAttribute.SIZE, new Float((float)fontSize));
                    if (!new Font(bulletFont.getFontName(), 0, 1).canDisplay(rt.getBulletChar())) {
                        bat.addAttribute(TextAttribute.FAMILY, "Arial");
                        bat = new AttributedString("\u25a0", bat.getIterator().getAttributes());
                    }
                    if (text.substring(startIndex, endIndex).length() > 1) {
                        el._bullet = bat;
                        el._bulletOffset = bulletOffset;
                    }
                }
                lines.add(el);
            }
        }
        final TextElement[] elems = new TextElement[lines.size()];
        return lines.toArray(elems);
    }
    
    public static class TextElement
    {
        public AttributedString _text;
        public int _textOffset;
        public AttributedString _bullet;
        public int _bulletOffset;
        public int _align;
        public float ascent;
        public float descent;
        public float advance;
        public int textStartIndex;
        public int textEndIndex;
    }
}
