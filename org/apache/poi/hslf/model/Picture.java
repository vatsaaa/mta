// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import org.apache.poi.ddf.EscherProperty;
import java.io.UnsupportedEncodingException;
import org.apache.poi.hslf.exceptions.HSLFException;
import org.apache.poi.ddf.EscherComplexProperty;
import java.util.List;
import org.apache.poi.hslf.record.Document;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.util.POILogger;
import java.awt.image.BufferedImage;
import org.apache.poi.hslf.usermodel.PictureData;
import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import org.apache.poi.hslf.blip.Bitmap;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public class Picture extends SimpleShape
{
    public static final int EMF = 2;
    public static final int WMF = 3;
    public static final int PICT = 4;
    public static final int JPEG = 5;
    public static final int PNG = 6;
    public static final byte DIB = 7;
    
    public Picture(final int idx) {
        this(idx, null);
    }
    
    public Picture(final int idx, final Shape parent) {
        super(null, parent);
        this._escherContainer = this.createSpContainer(idx, parent instanceof ShapeGroup);
    }
    
    protected Picture(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public int getPictureIndex() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherSimpleProperty prop = (EscherSimpleProperty)Shape.getEscherProperty(opt, 260);
        return (prop == null) ? 0 : prop.getPropertyValue();
    }
    
    protected EscherContainerRecord createSpContainer(final int idx, final boolean isChild) {
        (this._escherContainer = super.createSpContainer(isChild)).setOptions((short)15);
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        spRecord.setOptions((short)1202);
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        Shape.setEscherProperty(opt, (short)127, 8388736);
        Shape.setEscherProperty(opt, (short)16644, idx);
        return this._escherContainer;
    }
    
    public void setDefaultSize() {
        final PictureData pict = this.getPictureData();
        if (pict instanceof Bitmap) {
            BufferedImage img = null;
            try {
                img = ImageIO.read(new ByteArrayInputStream(pict.getData()));
            }
            catch (IOException e) {}
            catch (NegativeArraySizeException ex) {}
            if (img != null) {
                this.setAnchor(new Rectangle(0, 0, img.getWidth() * 72 / 96, img.getHeight() * 72 / 96));
            }
            else {
                this.setAnchor(new Rectangle(0, 0, 200, 200));
            }
        }
        else {
            this.setAnchor(new Rectangle(50, 50, 200, 200));
        }
    }
    
    public PictureData getPictureData() {
        final SlideShow ppt = this.getSheet().getSlideShow();
        final PictureData[] pict = ppt.getPictureData();
        final EscherBSERecord bse = this.getEscherBSERecord();
        if (bse == null) {
            this.logger.log(POILogger.ERROR, "no reference to picture data found ");
        }
        else {
            for (int i = 0; i < pict.length; ++i) {
                if (pict[i].getOffset() == bse.getOffset()) {
                    return pict[i];
                }
            }
            this.logger.log(POILogger.ERROR, "no picture found for our BSE offset " + bse.getOffset());
        }
        return null;
    }
    
    protected EscherBSERecord getEscherBSERecord() {
        final SlideShow ppt = this.getSheet().getSlideShow();
        final Document doc = ppt.getDocumentRecord();
        final EscherContainerRecord dggContainer = doc.getPPDrawingGroup().getDggContainer();
        final EscherContainerRecord bstore = (EscherContainerRecord)Shape.getEscherChild(dggContainer, -4095);
        if (bstore == null) {
            this.logger.log(POILogger.DEBUG, "EscherContainerRecord.BSTORE_CONTAINER was not found ");
            return null;
        }
        final List lst = bstore.getChildRecords();
        final int idx = this.getPictureIndex();
        if (idx == 0) {
            this.logger.log(POILogger.DEBUG, "picture index was not found, returning ");
            return null;
        }
        return lst.get(idx - 1);
    }
    
    public String getPictureName() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        final EscherComplexProperty prop = (EscherComplexProperty)Shape.getEscherProperty(opt, 261);
        String name = null;
        if (prop != null) {
            try {
                name = new String(prop.getComplexData(), "UTF-16LE");
                final int idx = name.indexOf(0);
                return (idx == -1) ? name : name.substring(0, idx);
            }
            catch (UnsupportedEncodingException e) {
                throw new HSLFException(e);
            }
        }
        return name;
    }
    
    public void setPictureName(final String name) {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        try {
            final byte[] data = (name + '\0').getBytes("UTF-16LE");
            final EscherComplexProperty prop = new EscherComplexProperty((short)261, false, data);
            opt.addEscherProperty(prop);
        }
        catch (UnsupportedEncodingException e) {
            throw new HSLFException(e);
        }
    }
    
    @Override
    protected void afterInsert(final Sheet sh) {
        super.afterInsert(sh);
        final EscherBSERecord bse = this.getEscherBSERecord();
        bse.setRef(bse.getRef() + 1);
        final Rectangle anchor = this.getAnchor();
        if (anchor.equals(new Rectangle())) {
            this.setDefaultSize();
        }
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final AffineTransform at = graphics.getTransform();
        ShapePainter.paint(this, graphics);
        final PictureData data = this.getPictureData();
        if (data != null) {
            data.draw(graphics, this);
        }
        graphics.setTransform(at);
    }
}
