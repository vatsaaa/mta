// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.record.OEPlaceholderAtom;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public final class Placeholder extends TextBox
{
    protected Placeholder(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public Placeholder(final Shape parent) {
        super(parent);
    }
    
    public Placeholder() {
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final boolean isChild) {
        this._escherContainer = super.createSpContainer(isChild);
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        spRecord.setFlags(544);
        final EscherClientDataRecord cldata = new EscherClientDataRecord();
        cldata.setOptions((short)15);
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        Shape.setEscherProperty(opt, (short)127, 262144);
        final OEPlaceholderAtom oep = new OEPlaceholderAtom();
        oep.setPlacementId(-1);
        oep.setPlaceholderId((byte)14);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            oep.writeOut(out);
        }
        catch (Exception e) {
            throw new HSLFException(e);
        }
        cldata.setRemainingData(out.toByteArray());
        this._escherContainer.addChildBefore(cldata, -4083);
        return this._escherContainer;
    }
}
