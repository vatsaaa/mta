// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.Document;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.hslf.record.CString;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.record.HeadersFootersContainer;

public final class HeadersFooters
{
    private HeadersFootersContainer _container;
    private boolean _newRecord;
    private SlideShow _ppt;
    private Sheet _sheet;
    private boolean _ppt2007;
    
    public HeadersFooters(final HeadersFootersContainer rec, final SlideShow ppt, final boolean newRecord, final boolean isPpt2007) {
        this._container = rec;
        this._newRecord = newRecord;
        this._ppt = ppt;
        this._ppt2007 = isPpt2007;
    }
    
    public HeadersFooters(final HeadersFootersContainer rec, final Sheet sheet, final boolean newRecord, final boolean isPpt2007) {
        this._container = rec;
        this._newRecord = newRecord;
        this._sheet = sheet;
        this._ppt2007 = isPpt2007;
    }
    
    public String getHeaderText() {
        final CString cs = (this._container == null) ? null : this._container.getHeaderAtom();
        return this.getPlaceholderText(10, cs);
    }
    
    public void setHeaderText(final String text) {
        if (this._newRecord) {
            this.attach();
        }
        this.setHeaderVisible(true);
        CString cs = this._container.getHeaderAtom();
        if (cs == null) {
            cs = this._container.addHeaderAtom();
        }
        cs.setText(text);
    }
    
    public String getFooterText() {
        final CString cs = (this._container == null) ? null : this._container.getFooterAtom();
        return this.getPlaceholderText(9, cs);
    }
    
    public void setFootersText(final String text) {
        if (this._newRecord) {
            this.attach();
        }
        this.setFooterVisible(true);
        CString cs = this._container.getFooterAtom();
        if (cs == null) {
            cs = this._container.addFooterAtom();
        }
        cs.setText(text);
    }
    
    public String getDateTimeText() {
        final CString cs = (this._container == null) ? null : this._container.getUserDateAtom();
        return this.getPlaceholderText(7, cs);
    }
    
    public void setDateTimeText(final String text) {
        if (this._newRecord) {
            this.attach();
        }
        this.setUserDateVisible(true);
        this.setDateTimeVisible(true);
        CString cs = this._container.getUserDateAtom();
        if (cs == null) {
            cs = this._container.addUserDateAtom();
        }
        cs.setText(text);
    }
    
    public boolean isFooterVisible() {
        return this.isVisible(32, 9);
    }
    
    public void setFooterVisible(final boolean flag) {
        if (this._newRecord) {
            this.attach();
        }
        this._container.getHeadersFootersAtom().setFlag(32, flag);
    }
    
    public boolean isHeaderVisible() {
        return this.isVisible(16, 10);
    }
    
    public void setHeaderVisible(final boolean flag) {
        if (this._newRecord) {
            this.attach();
        }
        this._container.getHeadersFootersAtom().setFlag(16, flag);
    }
    
    public boolean isDateTimeVisible() {
        return this.isVisible(1, 7);
    }
    
    public void setDateTimeVisible(final boolean flag) {
        if (this._newRecord) {
            this.attach();
        }
        this._container.getHeadersFootersAtom().setFlag(1, flag);
    }
    
    public boolean isUserDateVisible() {
        return this.isVisible(4, 7);
    }
    
    public void setUserDateVisible(final boolean flag) {
        if (this._newRecord) {
            this.attach();
        }
        this._container.getHeadersFootersAtom().setFlag(4, flag);
    }
    
    public boolean isSlideNumberVisible() {
        return this.isVisible(8, 8);
    }
    
    public void setSlideNumberVisible(final boolean flag) {
        if (this._newRecord) {
            this.attach();
        }
        this._container.getHeadersFootersAtom().setFlag(8, flag);
    }
    
    public int getDateTimeFormat() {
        return this._container.getHeadersFootersAtom().getFormatId();
    }
    
    public void setDateTimeFormat(final int formatId) {
        if (this._newRecord) {
            this.attach();
        }
        this._container.getHeadersFootersAtom().setFormatId(formatId);
    }
    
    private void attach() {
        final Document doc = this._ppt.getDocumentRecord();
        final Record[] ch = doc.getChildRecords();
        Record lst = null;
        for (int i = 0; i < ch.length; ++i) {
            if (ch[i].getRecordType() == RecordTypes.List.typeID) {
                lst = ch[i];
                break;
            }
        }
        doc.addChildAfter(this._container, lst);
        this._newRecord = false;
    }
    
    private boolean isVisible(final int flag, final int placeholderId) {
        boolean visible;
        if (this._ppt2007) {
            final Sheet master = (this._sheet != null) ? this._sheet : this._ppt.getSlidesMasters()[0];
            final TextShape placeholder = master.getPlaceholder(placeholderId);
            visible = (placeholder != null && placeholder.getText() != null);
        }
        else {
            visible = this._container.getHeadersFootersAtom().getFlag(flag);
        }
        return visible;
    }
    
    private String getPlaceholderText(final int placeholderId, final CString cs) {
        String text = null;
        if (this._ppt2007) {
            final Sheet master = (this._sheet != null) ? this._sheet : this._ppt.getSlidesMasters()[0];
            final TextShape placeholder = master.getPlaceholder(placeholderId);
            if (placeholder != null) {
                text = placeholder.getText();
            }
            if ("*".equals(text)) {
                text = null;
            }
        }
        else {
            text = ((cs == null) ? null : cs.getText());
        }
        return text;
    }
}
