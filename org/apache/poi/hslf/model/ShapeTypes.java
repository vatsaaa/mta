// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.lang.reflect.Field;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.util.HashMap;

public final class ShapeTypes implements org.apache.poi.sl.usermodel.ShapeTypes
{
    public static HashMap types;
    
    public static String typeName(final int type) {
        final String name = ShapeTypes.types.get(type);
        return name;
    }
    
    static {
        ShapeTypes.types = new HashMap();
        try {
            final Field[] f = org.apache.poi.sl.usermodel.ShapeTypes.class.getFields();
            for (int i = 0; i < f.length; ++i) {
                final Object val = f[i].get(null);
                if (val instanceof Integer) {
                    ShapeTypes.types.put(val, f[i].getName());
                }
            }
        }
        catch (IllegalAccessException e) {
            throw new HSLFException("Failed to initialize shape types");
        }
    }
}
