// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.model.textproperties.TextPropCollection;
import org.apache.poi.hslf.model.textproperties.TextProp;
import org.apache.poi.hslf.record.SheetContainer;
import org.apache.poi.hslf.record.MainMaster;
import org.apache.poi.hslf.record.TxMasterStyleAtom;

public final class SlideMaster extends MasterSheet
{
    private TextRun[] _runs;
    private TxMasterStyleAtom[] _txmaster;
    
    public SlideMaster(final MainMaster record, final int sheetNo) {
        super(record, sheetNo);
        this._runs = Sheet.findTextRuns(this.getPPDrawing());
        for (int i = 0; i < this._runs.length; ++i) {
            this._runs[i].setSheet(this);
        }
    }
    
    @Override
    public TextRun[] getTextRuns() {
        return this._runs;
    }
    
    @Override
    public MasterSheet getMasterSheet() {
        return null;
    }
    
    @Override
    public TextProp getStyleAttribute(int txtype, final int level, final String name, final boolean isCharacter) {
        TextProp prop = null;
        for (int i = level; i >= 0; --i) {
            final TextPropCollection[] styles = isCharacter ? this._txmaster[txtype].getCharacterStyles() : this._txmaster[txtype].getParagraphStyles();
            if (i < styles.length) {
                prop = styles[i].findByName(name);
            }
            if (prop != null) {
                break;
            }
        }
        if (prop == null) {
            if (isCharacter) {
                switch (txtype) {
                    case 5:
                    case 7:
                    case 8: {
                        txtype = 1;
                        break;
                    }
                    case 6: {
                        txtype = 0;
                        break;
                    }
                    default: {
                        return null;
                    }
                }
            }
            else {
                switch (txtype) {
                    case 5:
                    case 7:
                    case 8: {
                        txtype = 1;
                        break;
                    }
                    case 6: {
                        txtype = 0;
                        break;
                    }
                    default: {
                        return null;
                    }
                }
            }
            prop = this.getStyleAttribute(txtype, level, name, isCharacter);
        }
        return prop;
    }
    
    @Override
    public void setSlideShow(final SlideShow ss) {
        super.setSlideShow(ss);
        if (this._txmaster == null) {
            this._txmaster = new TxMasterStyleAtom[9];
            final TxMasterStyleAtom txdoc = this.getSlideShow().getDocumentRecord().getEnvironment().getTxMasterStyleAtom();
            this._txmaster[txdoc.getTextType()] = txdoc;
            final TxMasterStyleAtom[] txrec = ((MainMaster)this.getSheetContainer()).getTxMasterStyleAtoms();
            for (int i = 0; i < txrec.length; ++i) {
                final int txType = txrec[i].getTextType();
                if (this._txmaster[txType] == null) {
                    this._txmaster[txType] = txrec[i];
                }
            }
        }
    }
    
    @Override
    protected void onAddTextShape(final TextShape shape) {
        final TextRun run = shape.getTextRun();
        if (this._runs == null) {
            this._runs = new TextRun[] { run };
        }
        else {
            final TextRun[] tmp = new TextRun[this._runs.length + 1];
            System.arraycopy(this._runs, 0, tmp, 0, this._runs.length);
            tmp[tmp.length - 1] = run;
            this._runs = tmp;
        }
    }
    
    public TxMasterStyleAtom[] getTxMasterStyleAtoms() {
        return this._txmaster;
    }
}
