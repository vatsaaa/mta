// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.geom.Rectangle2D;
import org.apache.poi.util.POILogger;
import org.apache.poi.ddf.EscherContainerRecord;

public class AutoShape extends TextShape
{
    protected AutoShape(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public AutoShape(final int type, final Shape parent) {
        super(null, parent);
        this._escherContainer = this.createSpContainer(type, parent instanceof ShapeGroup);
    }
    
    public AutoShape(final int type) {
        this(type, null);
    }
    
    protected EscherContainerRecord createSpContainer(final int shapeType, final boolean isChild) {
        this._escherContainer = super.createSpContainer(isChild);
        this.setShapeType(shapeType);
        this.setEscherProperty((short)127, 262144);
        this.setEscherProperty((short)385, 134217732);
        this.setEscherProperty((short)385, 134217732);
        this.setEscherProperty((short)387, 134217728);
        this.setEscherProperty((short)447, 1048592);
        this.setEscherProperty((short)448, 134217729);
        this.setEscherProperty((short)511, 524296);
        this.setEscherProperty((short)513, 134217730);
        return this._escherContainer;
    }
    
    @Override
    protected void setDefaultTextProperties(final TextRun _txtrun) {
        this.setVerticalAlignment(1);
        this.setHorizontalAlignment(1);
        this.setWordWrap(2);
    }
    
    public int getAdjustmentValue(final int idx) {
        if (idx < 0 || idx > 9) {
            throw new IllegalArgumentException("The index of an adjustment value must be in the [0, 9] range");
        }
        return this.getEscherProperty((short)(327 + idx));
    }
    
    public void setAdjustmentValue(final int idx, final int val) {
        if (idx < 0 || idx > 9) {
            throw new IllegalArgumentException("The index of an adjustment value must be in the [0, 9] range");
        }
        this.setEscherProperty((short)(327 + idx), val);
    }
    
    @Override
    public java.awt.Shape getOutline() {
        final ShapeOutline outline = AutoShapes.getShapeOutline(this.getShapeType());
        final Rectangle2D anchor = this.getLogicalAnchor2D();
        if (outline == null) {
            this.logger.log(POILogger.WARN, "Outline not found for " + ShapeTypes.typeName(this.getShapeType()));
            return anchor;
        }
        final java.awt.Shape shape = outline.getOutline(this);
        return AutoShapes.transform(shape, anchor);
    }
}
