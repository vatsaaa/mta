// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.RecordContainer;
import org.apache.poi.hslf.record.TextSpecInfoAtom;
import org.apache.poi.util.StringUtil;
import org.apache.poi.hslf.model.textproperties.TextPropCollection;
import java.util.Vector;
import java.util.LinkedList;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.usermodel.RichTextRun;
import org.apache.poi.hslf.record.TextRulerAtom;
import org.apache.poi.hslf.record.StyleTextPropAtom;
import org.apache.poi.hslf.record.TextCharsAtom;
import org.apache.poi.hslf.record.TextBytesAtom;
import org.apache.poi.hslf.record.TextHeaderAtom;

public final class TextRun
{
    protected TextHeaderAtom _headerAtom;
    protected TextBytesAtom _byteAtom;
    protected TextCharsAtom _charAtom;
    protected StyleTextPropAtom _styleAtom;
    protected TextRulerAtom _ruler;
    protected boolean _isUnicode;
    protected RichTextRun[] _rtRuns;
    private SlideShow slideShow;
    private Sheet _sheet;
    private int shapeId;
    private int slwtIndex;
    protected Record[] _records;
    
    public TextRun(final TextHeaderAtom tha, final TextCharsAtom tca, final StyleTextPropAtom sta) {
        this(tha, null, tca, sta);
    }
    
    public TextRun(final TextHeaderAtom tha, final TextBytesAtom tba, final StyleTextPropAtom sta) {
        this(tha, tba, null, sta);
    }
    
    private TextRun(final TextHeaderAtom tha, final TextBytesAtom tba, final TextCharsAtom tca, final StyleTextPropAtom sta) {
        this.slwtIndex = -1;
        this._headerAtom = tha;
        this._styleAtom = sta;
        if (tba != null) {
            this._byteAtom = tba;
            this._isUnicode = false;
        }
        else {
            this._charAtom = tca;
            this._isUnicode = true;
        }
        final String runRawText = this.getText();
        LinkedList pStyles = new LinkedList();
        LinkedList cStyles = new LinkedList();
        if (this._styleAtom != null) {
            this._styleAtom.setParentTextSize(runRawText.length());
            pStyles = this._styleAtom.getParagraphStyles();
            cStyles = this._styleAtom.getCharacterStyles();
        }
        this.buildRichTextRuns(pStyles, cStyles, runRawText);
    }
    
    public void buildRichTextRuns(final LinkedList pStyles, final LinkedList cStyles, final String runRawText) {
        if (pStyles.size() == 0 || cStyles.size() == 0) {
            (this._rtRuns = new RichTextRun[1])[0] = new RichTextRun(this, 0, runRawText.length());
        }
        else {
            final Vector rtrs = new Vector();
            int pos = 0;
            int curP = 0;
            int curC = 0;
            int pLenRemain = -1;
            int cLenRemain = -1;
            while (pos <= runRawText.length() && curP < pStyles.size() && curC < cStyles.size()) {
                final TextPropCollection pProps = pStyles.get(curP);
                final TextPropCollection cProps = cStyles.get(curC);
                final int pLen = pProps.getCharactersCovered();
                final int cLen = cProps.getCharactersCovered();
                boolean freshSet = false;
                if (pLenRemain == -1 && cLenRemain == -1) {
                    freshSet = true;
                }
                if (pLenRemain == -1) {
                    pLenRemain = pLen;
                }
                if (cLenRemain == -1) {
                    cLenRemain = cLen;
                }
                int runLen = -1;
                boolean pShared = false;
                boolean cShared = false;
                if (pLen == cLen && freshSet) {
                    runLen = cLen;
                    pShared = false;
                    cShared = false;
                    ++curP;
                    ++curC;
                    pLenRemain = -1;
                    cLenRemain = -1;
                }
                else if (pLenRemain < pLen) {
                    pShared = true;
                    if (pLenRemain == cLenRemain) {
                        cShared = false;
                        runLen = pLenRemain;
                        ++curP;
                        ++curC;
                        pLenRemain = -1;
                        cLenRemain = -1;
                    }
                    else if (pLenRemain < cLenRemain) {
                        cShared = true;
                        runLen = pLenRemain;
                        ++curP;
                        cLenRemain -= pLenRemain;
                        pLenRemain = -1;
                    }
                    else {
                        cShared = false;
                        runLen = cLenRemain;
                        ++curC;
                        pLenRemain -= cLenRemain;
                        cLenRemain = -1;
                    }
                }
                else if (cLenRemain < cLen) {
                    cShared = true;
                    if (pLenRemain == cLenRemain) {
                        pShared = false;
                        runLen = cLenRemain;
                        ++curP;
                        ++curC;
                        pLenRemain = -1;
                        cLenRemain = -1;
                    }
                    else if (cLenRemain < pLenRemain) {
                        pShared = true;
                        runLen = cLenRemain;
                        ++curC;
                        pLenRemain -= cLenRemain;
                        cLenRemain = -1;
                    }
                    else {
                        pShared = false;
                        runLen = pLenRemain;
                        ++curP;
                        cLenRemain -= pLenRemain;
                        pLenRemain = -1;
                    }
                }
                else if (pLenRemain < cLenRemain) {
                    pShared = false;
                    cShared = true;
                    runLen = pLenRemain;
                    ++curP;
                    cLenRemain -= pLenRemain;
                    pLenRemain = -1;
                }
                else {
                    pShared = true;
                    cShared = false;
                    runLen = cLenRemain;
                    ++curC;
                    pLenRemain -= cLenRemain;
                    cLenRemain = -1;
                }
                final int prevPos = pos;
                pos += runLen;
                if (pos > runRawText.length()) {
                    --runLen;
                }
                final RichTextRun rtr = new RichTextRun(this, prevPos, runLen, pProps, cProps, pShared, cShared);
                rtrs.add(rtr);
            }
            rtrs.copyInto(this._rtRuns = new RichTextRun[rtrs.size()]);
        }
    }
    
    public RichTextRun appendText(final String s) {
        this.ensureStyleAtomPresent();
        final int oldSize = this.getRawText().length();
        this.storeText(this.getRawText() + s);
        final int pOverRun = this._styleAtom.getParagraphTextLengthCovered() - oldSize;
        final int cOverRun = this._styleAtom.getCharacterTextLengthCovered() - oldSize;
        if (pOverRun > 0) {
            final TextPropCollection tpc = this._styleAtom.getParagraphStyles().getLast();
            tpc.updateTextSize(tpc.getCharactersCovered() - pOverRun);
        }
        if (cOverRun > 0) {
            final TextPropCollection tpc = this._styleAtom.getCharacterStyles().getLast();
            tpc.updateTextSize(tpc.getCharactersCovered() - cOverRun);
        }
        final TextPropCollection newPTP = this._styleAtom.addParagraphTextPropCollection(s.length() + pOverRun);
        final TextPropCollection newCTP = this._styleAtom.addCharacterTextPropCollection(s.length() + cOverRun);
        final RichTextRun nr = new RichTextRun(this, oldSize, s.length(), newPTP, newCTP, false, false);
        final RichTextRun[] newRuns = new RichTextRun[this._rtRuns.length + 1];
        System.arraycopy(this._rtRuns, 0, newRuns, 0, this._rtRuns.length);
        newRuns[newRuns.length - 1] = nr;
        this._rtRuns = newRuns;
        return nr;
    }
    
    private void storeText(final String s) {
        if (this._isUnicode) {
            this._charAtom.setText(s);
        }
        else {
            final boolean hasMultibyte = StringUtil.hasMultibyte(s);
            if (!hasMultibyte) {
                final byte[] text = new byte[s.length()];
                StringUtil.putCompressedUnicode(s, text, 0);
                this._byteAtom.setText(text);
            }
            else {
                (this._charAtom = new TextCharsAtom()).setText(s);
                final RecordContainer parent = this._headerAtom.getParentRecord();
                final Record[] cr = parent.getChildRecords();
                for (int i = 0; i < cr.length; ++i) {
                    if (cr[i].equals(this._byteAtom)) {
                        cr[i] = this._charAtom;
                        break;
                    }
                }
                this._byteAtom = null;
                this._isUnicode = true;
            }
        }
        if (this._records != null) {
            for (int j = 0; j < this._records.length; ++j) {
                if (this._records[j] instanceof TextSpecInfoAtom) {
                    final TextSpecInfoAtom specAtom = (TextSpecInfoAtom)this._records[j];
                    if (s.length() + 1 != specAtom.getCharactersCovered()) {
                        specAtom.reset(s.length() + 1);
                    }
                }
            }
        }
    }
    
    public void changeTextInRichTextRun(final RichTextRun run, final String s) {
        int runID = -1;
        for (int i = 0; i < this._rtRuns.length; ++i) {
            if (run.equals(this._rtRuns[i])) {
                runID = i;
            }
        }
        if (runID == -1) {
            throw new IllegalArgumentException("Supplied RichTextRun wasn't from this TextRun");
        }
        this.ensureStyleAtomPresent();
        final TextPropCollection pCol = run._getRawParagraphStyle();
        final TextPropCollection cCol = run._getRawCharacterStyle();
        int newSize = s.length();
        if (runID == this._rtRuns.length - 1) {
            ++newSize;
        }
        if (run._isParagraphStyleShared()) {
            pCol.updateTextSize(pCol.getCharactersCovered() - run.getLength() + s.length());
        }
        else {
            pCol.updateTextSize(newSize);
        }
        if (run._isCharacterStyleShared()) {
            cCol.updateTextSize(cCol.getCharactersCovered() - run.getLength() + s.length());
        }
        else {
            cCol.updateTextSize(newSize);
        }
        final StringBuffer newText = new StringBuffer();
        for (int j = 0; j < this._rtRuns.length; ++j) {
            final int newStartPos = newText.length();
            if (j != runID) {
                newText.append(this._rtRuns[j].getRawText());
            }
            else {
                newText.append(s);
            }
            if (j > runID) {
                this._rtRuns[j].updateStartPosition(newStartPos);
            }
        }
        this.storeText(newText.toString());
    }
    
    public void setRawText(final String s) {
        this.storeText(s);
        final RichTextRun fst = this._rtRuns[0];
        for (int i = 0; i < this._rtRuns.length; ++i) {
            this._rtRuns[i] = null;
        }
        (this._rtRuns = new RichTextRun[1])[0] = fst;
        if (this._styleAtom != null) {
            final LinkedList pStyles = this._styleAtom.getParagraphStyles();
            while (pStyles.size() > 1) {
                pStyles.removeLast();
            }
            final LinkedList cStyles = this._styleAtom.getCharacterStyles();
            while (cStyles.size() > 1) {
                cStyles.removeLast();
            }
            this._rtRuns[0].setText(s);
        }
        else {
            this._rtRuns[0] = new RichTextRun(this, 0, s.length());
        }
    }
    
    public void setText(final String s) {
        final String text = this.normalize(s);
        this.setRawText(text);
    }
    
    public void ensureStyleAtomPresent() {
        if (this._styleAtom != null) {
            return;
        }
        this._styleAtom = new StyleTextPropAtom(this.getRawText().length() + 1);
        final RecordContainer runAtomsParent = this._headerAtom.getParentRecord();
        Record addAfter = this._byteAtom;
        if (this._byteAtom == null) {
            addAfter = this._charAtom;
        }
        runAtomsParent.addChildAfter(this._styleAtom, addAfter);
        if (this._rtRuns.length != 1) {
            throw new IllegalStateException("Needed to add StyleTextPropAtom when had many rich text runs");
        }
        this._rtRuns[0].supplyTextProps(this._styleAtom.getParagraphStyles().get(0), this._styleAtom.getCharacterStyles().get(0), false, false);
    }
    
    public String getText() {
        final String rawText = this.getRawText();
        String text = rawText.replace('\r', '\n');
        final int type = (this._headerAtom == null) ? 0 : this._headerAtom.getTextType();
        if (type == 0 || type == 6) {
            text = text.replace('\u000b', '\n');
        }
        else {
            text = text.replace('\u000b', ' ');
        }
        return text;
    }
    
    public String getRawText() {
        if (this._isUnicode) {
            return this._charAtom.getText();
        }
        return this._byteAtom.getText();
    }
    
    public RichTextRun[] getRichTextRuns() {
        return this._rtRuns;
    }
    
    public int getRunType() {
        return this._headerAtom.getTextType();
    }
    
    public void setRunType(final int type) {
        this._headerAtom.setTextType(type);
    }
    
    public void supplySlideShow(final SlideShow ss) {
        this.slideShow = ss;
        if (this._rtRuns != null) {
            for (int i = 0; i < this._rtRuns.length; ++i) {
                this._rtRuns[i].supplySlideShow(this.slideShow);
            }
        }
    }
    
    public void setSheet(final Sheet sheet) {
        this._sheet = sheet;
    }
    
    public Sheet getSheet() {
        return this._sheet;
    }
    
    protected int getShapeId() {
        return this.shapeId;
    }
    
    protected void setShapeId(final int id) {
        this.shapeId = id;
    }
    
    protected int getIndex() {
        return this.slwtIndex;
    }
    
    protected void setIndex(final int id) {
        this.slwtIndex = id;
    }
    
    public Hyperlink[] getHyperlinks() {
        return Hyperlink.find(this);
    }
    
    public RichTextRun getRichTextRunAt(final int pos) {
        for (int i = 0; i < this._rtRuns.length; ++i) {
            final int start = this._rtRuns[i].getStartIndex();
            final int end = this._rtRuns[i].getEndIndex();
            if (pos >= start && pos < end) {
                return this._rtRuns[i];
            }
        }
        return null;
    }
    
    public TextRulerAtom getTextRuler() {
        if (this._ruler == null && this._records != null) {
            for (int i = 0; i < this._records.length; ++i) {
                if (this._records[i] instanceof TextRulerAtom) {
                    this._ruler = (TextRulerAtom)this._records[i];
                    break;
                }
            }
        }
        return this._ruler;
    }
    
    public TextRulerAtom createTextRuler() {
        this._ruler = this.getTextRuler();
        if (this._ruler == null) {
            this._ruler = TextRulerAtom.getParagraphInstance();
            this._headerAtom.getParentRecord().appendChildRecord(this._ruler);
        }
        return this._ruler;
    }
    
    public String normalize(final String s) {
        final String ns = s.replaceAll("\\r?\\n", "\r");
        return ns;
    }
    
    public Record[] getRecords() {
        return this._records;
    }
}
