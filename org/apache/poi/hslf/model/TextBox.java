// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.ddf.EscherContainerRecord;

public class TextBox extends TextShape
{
    protected TextBox(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public TextBox(final Shape parent) {
        super(parent);
    }
    
    public TextBox() {
        this(null);
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final boolean isChild) {
        this._escherContainer = super.createSpContainer(isChild);
        this.setShapeType(202);
        this.setEscherProperty((short)385, 134217732);
        this.setEscherProperty((short)387, 134217728);
        this.setEscherProperty((short)447, 1048576);
        this.setEscherProperty((short)448, 134217729);
        this.setEscherProperty((short)511, 524288);
        this.setEscherProperty((short)513, 134217730);
        this._txtrun = this.createTextRun();
        return this._escherContainer;
    }
    
    @Override
    protected void setDefaultTextProperties(final TextRun _txtrun) {
        this.setVerticalAlignment(0);
        this.setEscherProperty((short)191, 131074);
    }
}
