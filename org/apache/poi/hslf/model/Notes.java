// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.SheetContainer;

public final class Notes extends Sheet
{
    private TextRun[] _runs;
    
    public Notes(final org.apache.poi.hslf.record.Notes notes) {
        super(notes, notes.getNotesAtom().getSlideID());
        this._runs = Sheet.findTextRuns(this.getPPDrawing());
        for (int i = 0; i < this._runs.length; ++i) {
            this._runs[i].setSheet(this);
        }
    }
    
    @Override
    public TextRun[] getTextRuns() {
        return this._runs;
    }
    
    @Override
    public MasterSheet getMasterSheet() {
        return null;
    }
}
