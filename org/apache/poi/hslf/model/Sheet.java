// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.CString;
import org.apache.poi.hslf.record.OEPlaceholderAtom;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.hslf.record.RoundTripHFPlaceholder12;
import java.awt.Graphics2D;
import org.apache.poi.hslf.record.ColorSchemeAtom;
import org.apache.poi.ddf.EscherDgRecord;
import org.apache.poi.ddf.EscherDggRecord;
import java.util.Iterator;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.hslf.record.TextBytesAtom;
import org.apache.poi.hslf.record.TextCharsAtom;
import org.apache.poi.hslf.record.StyleTextPropAtom;
import org.apache.poi.hslf.record.TextHeaderAtom;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.EscherTextboxWrapper;
import java.util.List;
import org.apache.poi.hslf.record.RecordContainer;
import java.util.ArrayList;
import org.apache.poi.hslf.record.PPDrawing;
import org.apache.poi.hslf.record.SheetContainer;
import org.apache.poi.hslf.usermodel.SlideShow;

public abstract class Sheet
{
    private SlideShow _slideShow;
    private Background _background;
    private SheetContainer _container;
    private int _sheetNo;
    
    public Sheet(final SheetContainer container, final int sheetNo) {
        this._container = container;
        this._sheetNo = sheetNo;
    }
    
    public abstract TextRun[] getTextRuns();
    
    public int _getSheetRefId() {
        return this._container.getSheetId();
    }
    
    public int _getSheetNumber() {
        return this._sheetNo;
    }
    
    protected PPDrawing getPPDrawing() {
        return this._container.getPPDrawing();
    }
    
    public SlideShow getSlideShow() {
        return this._slideShow;
    }
    
    public SheetContainer getSheetContainer() {
        return this._container;
    }
    
    public void setSlideShow(final SlideShow ss) {
        this._slideShow = ss;
        final TextRun[] trs = this.getTextRuns();
        if (trs != null) {
            for (int i = 0; i < trs.length; ++i) {
                trs[i].supplySlideShow(this._slideShow);
            }
        }
    }
    
    public static TextRun[] findTextRuns(final PPDrawing ppdrawing) {
        final List<TextRun> runsV = new ArrayList<TextRun>();
        final EscherTextboxWrapper[] wrappers = ppdrawing.getTextboxWrappers();
        for (int i = 0; i < wrappers.length; ++i) {
            final int s1 = runsV.size();
            RecordContainer.handleParentAwareRecords(wrappers[i]);
            findTextRuns(wrappers[i].getChildRecords(), runsV);
            final int s2 = runsV.size();
            if (s2 != s1) {
                final TextRun t = runsV.get(runsV.size() - 1);
                t.setShapeId(wrappers[i].getShapeId());
            }
        }
        return runsV.toArray(new TextRun[runsV.size()]);
    }
    
    protected static void findTextRuns(final Record[] records, final List<TextRun> found) {
        int i = 0;
        int slwtIndex = 0;
        while (i < records.length - 1) {
            if (records[i] instanceof TextHeaderAtom) {
                TextRun trun = null;
                final TextHeaderAtom tha = (TextHeaderAtom)records[i];
                StyleTextPropAtom stpa = null;
                if (i < records.length - 2 && records[i + 2] instanceof StyleTextPropAtom) {
                    stpa = (StyleTextPropAtom)records[i + 2];
                }
                if (records[i + 1] instanceof TextCharsAtom) {
                    final TextCharsAtom tca = (TextCharsAtom)records[i + 1];
                    trun = new TextRun(tha, tca, stpa);
                }
                else if (records[i + 1] instanceof TextBytesAtom) {
                    final TextBytesAtom tba = (TextBytesAtom)records[i + 1];
                    trun = new TextRun(tha, tba, stpa);
                }
                else if (records[i + 1].getRecordType() != 4001L) {
                    if (records[i + 1].getRecordType() != 4010L) {
                        System.err.println("Found a TextHeaderAtom not followed by a TextBytesAtom or TextCharsAtom: Followed by " + records[i + 1].getRecordType());
                    }
                }
                if (trun != null) {
                    final ArrayList lst = new ArrayList();
                    for (int j = i; j < records.length && (j <= i || !(records[j] instanceof TextHeaderAtom)); ++j) {
                        lst.add(records[j]);
                    }
                    final Record[] recs = new Record[lst.size()];
                    lst.toArray(recs);
                    trun._records = recs;
                    trun.setIndex(slwtIndex);
                    found.add(trun);
                    ++i;
                }
                ++slwtIndex;
            }
            ++i;
        }
    }
    
    public Shape[] getShapes() {
        final PPDrawing ppdrawing = this.getPPDrawing();
        final EscherContainerRecord dg = (EscherContainerRecord)ppdrawing.getEscherRecords()[0];
        EscherContainerRecord spgr = null;
        final Iterator<EscherRecord> it = dg.getChildIterator();
        while (it.hasNext()) {
            final EscherRecord rec = it.next();
            if (rec.getRecordId() == -4093) {
                spgr = (EscherContainerRecord)rec;
                break;
            }
        }
        if (spgr == null) {
            throw new IllegalStateException("spgr not found");
        }
        final List<Shape> shapes = new ArrayList<Shape>();
        final Iterator<EscherRecord> it2 = spgr.getChildIterator();
        if (it2.hasNext()) {
            it2.next();
        }
        while (it2.hasNext()) {
            final EscherContainerRecord sp = it2.next();
            final Shape sh = ShapeFactory.createShape(sp, null);
            sh.setSheet(this);
            shapes.add(sh);
        }
        return shapes.toArray(new Shape[shapes.size()]);
    }
    
    public void addShape(final Shape shape) {
        final PPDrawing ppdrawing = this.getPPDrawing();
        final EscherContainerRecord dgContainer = (EscherContainerRecord)ppdrawing.getEscherRecords()[0];
        final EscherContainerRecord spgr = (EscherContainerRecord)Shape.getEscherChild(dgContainer, -4093);
        spgr.addChildRecord(shape.getSpContainer());
        shape.setSheet(this);
        shape.setShapeId(this.allocateShapeId());
        shape.afterInsert(this);
    }
    
    public int allocateShapeId() {
        final EscherDggRecord dgg = this._slideShow.getDocumentRecord().getPPDrawingGroup().getEscherDggRecord();
        final EscherDgRecord dg = this._container.getPPDrawing().getEscherDgRecord();
        dgg.setNumShapesSaved(dgg.getNumShapesSaved() + 1);
        for (int i = 0; i < dgg.getFileIdClusters().length; ++i) {
            final EscherDggRecord.FileIdCluster c = dgg.getFileIdClusters()[i];
            if (c.getDrawingGroupId() == dg.getDrawingGroupId() && c.getNumShapeIdsUsed() != 1024) {
                final int result = c.getNumShapeIdsUsed() + 1024 * (i + 1);
                c.incrementShapeId();
                dg.setNumShapes(dg.getNumShapes() + 1);
                dg.setLastMSOSPID(result);
                if (result >= dgg.getShapeIdMax()) {
                    dgg.setShapeIdMax(result + 1);
                }
                return result;
            }
        }
        dgg.addCluster(dg.getDrawingGroupId(), 0, false);
        dgg.getFileIdClusters()[dgg.getFileIdClusters().length - 1].incrementShapeId();
        dg.setNumShapes(dg.getNumShapes() + 1);
        final int result2 = 1024 * dgg.getFileIdClusters().length;
        dg.setLastMSOSPID(result2);
        if (result2 >= dgg.getShapeIdMax()) {
            dgg.setShapeIdMax(result2 + 1);
        }
        return result2;
    }
    
    public boolean removeShape(final Shape shape) {
        final PPDrawing ppdrawing = this.getPPDrawing();
        final EscherContainerRecord dg = (EscherContainerRecord)ppdrawing.getEscherRecords()[0];
        EscherContainerRecord spgr = null;
        final Iterator<EscherRecord> it = dg.getChildIterator();
        while (it.hasNext()) {
            final EscherRecord rec = it.next();
            if (rec.getRecordId() == -4093) {
                spgr = (EscherContainerRecord)rec;
                break;
            }
        }
        if (spgr == null) {
            return false;
        }
        final List<EscherRecord> lst = spgr.getChildRecords();
        final boolean result = lst.remove(shape.getSpContainer());
        spgr.setChildRecords(lst);
        return result;
    }
    
    public void onCreate() {
    }
    
    public abstract MasterSheet getMasterSheet();
    
    public ColorSchemeAtom getColorScheme() {
        return this._container.getColorScheme();
    }
    
    public Background getBackground() {
        if (this._background == null) {
            final PPDrawing ppdrawing = this.getPPDrawing();
            final EscherContainerRecord dg = (EscherContainerRecord)ppdrawing.getEscherRecords()[0];
            EscherContainerRecord spContainer = null;
            final Iterator<EscherRecord> it = dg.getChildIterator();
            while (it.hasNext()) {
                final EscherRecord rec = it.next();
                if (rec.getRecordId() == -4092) {
                    spContainer = (EscherContainerRecord)rec;
                    break;
                }
            }
            (this._background = new Background(spContainer, null)).setSheet(this);
        }
        return this._background;
    }
    
    public void draw(final Graphics2D graphics) {
    }
    
    protected void onAddTextShape(final TextShape shape) {
    }
    
    public TextShape getPlaceholderByTextType(final int type) {
        final Shape[] shape = this.getShapes();
        for (int i = 0; i < shape.length; ++i) {
            if (shape[i] instanceof TextShape) {
                final TextShape tx = (TextShape)shape[i];
                final TextRun run = tx.getTextRun();
                if (run != null && run.getRunType() == type) {
                    return tx;
                }
            }
        }
        return null;
    }
    
    public TextShape getPlaceholder(final int type) {
        final Shape[] shape = this.getShapes();
        for (int i = 0; i < shape.length; ++i) {
            if (shape[i] instanceof TextShape) {
                final TextShape tx = (TextShape)shape[i];
                int placeholderId = 0;
                final OEPlaceholderAtom oep = tx.getPlaceholderAtom();
                if (oep != null) {
                    placeholderId = oep.getPlaceholderId();
                }
                else {
                    final RoundTripHFPlaceholder12 hldr = (RoundTripHFPlaceholder12)tx.getClientDataRecord(RecordTypes.RoundTripHFPlaceholder12.typeID);
                    if (hldr != null) {
                        placeholderId = hldr.getPlaceholderId();
                    }
                }
                if (placeholderId == type) {
                    return tx;
                }
            }
        }
        return null;
    }
    
    public String getProgrammableTag() {
        String tag = null;
        final RecordContainer progTags = (RecordContainer)this.getSheetContainer().findFirstOfType(RecordTypes.ProgTags.typeID);
        if (progTags != null) {
            final RecordContainer progBinaryTag = (RecordContainer)progTags.findFirstOfType(RecordTypes.ProgBinaryTag.typeID);
            if (progBinaryTag != null) {
                final CString binaryTag = (CString)progBinaryTag.findFirstOfType(RecordTypes.CString.typeID);
                if (binaryTag != null) {
                    tag = binaryTag.getText();
                }
            }
        }
        return tag;
    }
}
