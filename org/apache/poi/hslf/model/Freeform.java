// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.util.Arrays;
import java.util.List;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherArrayProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.util.POILogger;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import org.apache.poi.ddf.EscherContainerRecord;

public final class Freeform extends AutoShape
{
    public static final byte[] SEGMENTINFO_MOVETO;
    public static final byte[] SEGMENTINFO_LINETO;
    public static final byte[] SEGMENTINFO_ESCAPE;
    public static final byte[] SEGMENTINFO_ESCAPE2;
    public static final byte[] SEGMENTINFO_CUBICTO;
    public static final byte[] SEGMENTINFO_CUBICTO2;
    public static final byte[] SEGMENTINFO_CLOSE;
    public static final byte[] SEGMENTINFO_END;
    
    protected Freeform(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public Freeform(final Shape parent) {
        super(null, parent);
        this._escherContainer = this.createSpContainer(0, parent instanceof ShapeGroup);
    }
    
    public Freeform() {
        this(null);
    }
    
    public void setPath(final GeneralPath path) {
        final Rectangle2D bounds = path.getBounds2D();
        final PathIterator it = path.getPathIterator(new AffineTransform());
        final List<byte[]> segInfo = new ArrayList<byte[]>();
        final List<Point2D.Double> pntInfo = new ArrayList<Point2D.Double>();
        boolean isClosed = false;
        while (!it.isDone()) {
            final double[] vals = new double[6];
            final int type = it.currentSegment(vals);
            switch (type) {
                case 0: {
                    pntInfo.add(new Point2D.Double(vals[0], vals[1]));
                    segInfo.add(Freeform.SEGMENTINFO_MOVETO);
                    break;
                }
                case 1: {
                    pntInfo.add(new Point2D.Double(vals[0], vals[1]));
                    segInfo.add(Freeform.SEGMENTINFO_LINETO);
                    segInfo.add(Freeform.SEGMENTINFO_ESCAPE);
                    break;
                }
                case 3: {
                    pntInfo.add(new Point2D.Double(vals[0], vals[1]));
                    pntInfo.add(new Point2D.Double(vals[2], vals[3]));
                    pntInfo.add(new Point2D.Double(vals[4], vals[5]));
                    segInfo.add(Freeform.SEGMENTINFO_CUBICTO);
                    segInfo.add(Freeform.SEGMENTINFO_ESCAPE2);
                    break;
                }
                case 2: {
                    this.logger.log(POILogger.WARN, "SEG_QUADTO is not supported");
                    break;
                }
                case 4: {
                    pntInfo.add(pntInfo.get(0));
                    segInfo.add(Freeform.SEGMENTINFO_LINETO);
                    segInfo.add(Freeform.SEGMENTINFO_ESCAPE);
                    segInfo.add(Freeform.SEGMENTINFO_LINETO);
                    segInfo.add(Freeform.SEGMENTINFO_CLOSE);
                    isClosed = true;
                    break;
                }
            }
            it.next();
        }
        if (!isClosed) {
            segInfo.add(Freeform.SEGMENTINFO_LINETO);
        }
        segInfo.add(new byte[] { 0, -128 });
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        opt.addEscherProperty(new EscherSimpleProperty((short)324, 4));
        final EscherArrayProperty verticesProp = new EscherArrayProperty((short)16709, false, null);
        verticesProp.setNumberOfElementsInArray(pntInfo.size());
        verticesProp.setNumberOfElementsInMemory(pntInfo.size());
        verticesProp.setSizeOfElements(65520);
        for (int i = 0; i < pntInfo.size(); ++i) {
            final Point2D.Double pnt = pntInfo.get(i);
            final byte[] data = new byte[4];
            LittleEndian.putShort(data, 0, (short)((pnt.getX() - bounds.getX()) * 576.0 / 72.0));
            LittleEndian.putShort(data, 2, (short)((pnt.getY() - bounds.getY()) * 576.0 / 72.0));
            verticesProp.setElement(i, data);
        }
        opt.addEscherProperty(verticesProp);
        final EscherArrayProperty segmentsProp = new EscherArrayProperty((short)16710, false, null);
        segmentsProp.setNumberOfElementsInArray(segInfo.size());
        segmentsProp.setNumberOfElementsInMemory(segInfo.size());
        segmentsProp.setSizeOfElements(2);
        for (int j = 0; j < segInfo.size(); ++j) {
            final byte[] seg = segInfo.get(j);
            segmentsProp.setElement(j, seg);
        }
        opt.addEscherProperty(segmentsProp);
        opt.addEscherProperty(new EscherSimpleProperty((short)322, (int)(bounds.getWidth() * 576.0 / 72.0)));
        opt.addEscherProperty(new EscherSimpleProperty((short)323, (int)(bounds.getHeight() * 576.0 / 72.0)));
        opt.sortProperties();
        this.setAnchor(bounds);
    }
    
    public GeneralPath getPath() {
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
        opt.addEscherProperty(new EscherSimpleProperty((short)324, 4));
        EscherArrayProperty verticesProp = (EscherArrayProperty)Shape.getEscherProperty(opt, 16709);
        if (verticesProp == null) {
            verticesProp = (EscherArrayProperty)Shape.getEscherProperty(opt, 325);
        }
        EscherArrayProperty segmentsProp = (EscherArrayProperty)Shape.getEscherProperty(opt, 16710);
        if (segmentsProp == null) {
            segmentsProp = (EscherArrayProperty)Shape.getEscherProperty(opt, 326);
        }
        if (verticesProp == null) {
            this.logger.log(POILogger.WARN, "Freeform is missing GEOMETRY__VERTICES ");
            return null;
        }
        if (segmentsProp == null) {
            this.logger.log(POILogger.WARN, "Freeform is missing GEOMETRY__SEGMENTINFO ");
            return null;
        }
        final GeneralPath path = new GeneralPath();
        for (int numPoints = verticesProp.getNumberOfElementsInArray(), numSegments = segmentsProp.getNumberOfElementsInArray(), i = 0, j = 0; i < numSegments && j < numPoints; ++i) {
            final byte[] elem = segmentsProp.getElement(i);
            if (Arrays.equals(elem, Freeform.SEGMENTINFO_MOVETO)) {
                final byte[] p = verticesProp.getElement(j++);
                final short x = LittleEndian.getShort(p, 0);
                final short y = LittleEndian.getShort(p, 2);
                path.moveTo(x * 72.0f / 576.0f, y * 72.0f / 576.0f);
            }
            else if (Arrays.equals(elem, Freeform.SEGMENTINFO_CUBICTO) || Arrays.equals(elem, Freeform.SEGMENTINFO_CUBICTO2)) {
                ++i;
                final byte[] p2 = verticesProp.getElement(j++);
                final short x2 = LittleEndian.getShort(p2, 0);
                final short y2 = LittleEndian.getShort(p2, 2);
                final byte[] p3 = verticesProp.getElement(j++);
                final short x3 = LittleEndian.getShort(p3, 0);
                final short y3 = LittleEndian.getShort(p3, 2);
                final byte[] p4 = verticesProp.getElement(j++);
                final short x4 = LittleEndian.getShort(p4, 0);
                final short y4 = LittleEndian.getShort(p4, 2);
                path.curveTo(x2 * 72.0f / 576.0f, y2 * 72.0f / 576.0f, x3 * 72.0f / 576.0f, y3 * 72.0f / 576.0f, x4 * 72.0f / 576.0f, y4 * 72.0f / 576.0f);
            }
            else if (Arrays.equals(elem, Freeform.SEGMENTINFO_LINETO)) {
                ++i;
                final byte[] pnext = segmentsProp.getElement(i);
                if (Arrays.equals(pnext, Freeform.SEGMENTINFO_ESCAPE)) {
                    if (j + 1 < numPoints) {
                        final byte[] p5 = verticesProp.getElement(j++);
                        final short x5 = LittleEndian.getShort(p5, 0);
                        final short y5 = LittleEndian.getShort(p5, 2);
                        path.lineTo(x5 * 72.0f / 576.0f, y5 * 72.0f / 576.0f);
                    }
                }
                else if (Arrays.equals(pnext, Freeform.SEGMENTINFO_CLOSE)) {
                    path.closePath();
                }
            }
        }
        return path;
    }
    
    @Override
    public java.awt.Shape getOutline() {
        final GeneralPath path = this.getPath();
        final Rectangle2D anchor = this.getAnchor2D();
        final Rectangle2D bounds = path.getBounds2D();
        final AffineTransform at = new AffineTransform();
        at.translate(anchor.getX(), anchor.getY());
        at.scale(anchor.getWidth() / bounds.getWidth(), anchor.getHeight() / bounds.getHeight());
        return at.createTransformedShape(path);
    }
    
    static {
        SEGMENTINFO_MOVETO = new byte[] { 0, 64 };
        SEGMENTINFO_LINETO = new byte[] { 0, -84 };
        SEGMENTINFO_ESCAPE = new byte[] { 1, 0 };
        SEGMENTINFO_ESCAPE2 = new byte[] { 1, 32 };
        SEGMENTINFO_CUBICTO = new byte[] { 0, -83 };
        SEGMENTINFO_CUBICTO2 = new byte[] { 0, -77 };
        SEGMENTINFO_CLOSE = new byte[] { 1, 96 };
        SEGMENTINFO_END = new byte[] { 0, -128 };
    }
}
