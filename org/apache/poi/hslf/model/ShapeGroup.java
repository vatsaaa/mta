// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import org.apache.poi.ddf.EscherChildAnchorRecord;
import org.apache.poi.ddf.EscherSpRecord;
import java.awt.geom.Rectangle2D;
import org.apache.poi.ddf.EscherSpgrRecord;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherClientAnchorRecord;
import java.awt.Rectangle;
import java.util.List;
import java.util.Iterator;
import org.apache.poi.util.POILogger;
import org.apache.poi.ddf.EscherRecord;
import java.util.ArrayList;
import org.apache.poi.ddf.EscherContainerRecord;

public class ShapeGroup extends Shape
{
    public ShapeGroup() {
        this(null, null);
        this._escherContainer = this.createSpContainer(false);
    }
    
    protected ShapeGroup(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public Shape[] getShapes() {
        final Iterator<EscherRecord> iter = this._escherContainer.getChildIterator();
        if (iter.hasNext()) {
            iter.next();
        }
        final List<Shape> shapeList = new ArrayList<Shape>();
        while (iter.hasNext()) {
            final EscherRecord r = iter.next();
            if (r instanceof EscherContainerRecord) {
                final EscherContainerRecord container = (EscherContainerRecord)r;
                final Shape shape = ShapeFactory.createShape(container, this);
                shape.setSheet(this.getSheet());
                shapeList.add(shape);
            }
            else {
                this.logger.log(POILogger.ERROR, "Shape contained non container escher record, was " + r.getClass().getName());
            }
        }
        final Shape[] shapes = shapeList.toArray(new Shape[shapeList.size()]);
        return shapes;
    }
    
    public void setAnchor(final Rectangle anchor) {
        final EscherContainerRecord spContainer = (EscherContainerRecord)this._escherContainer.getChild(0);
        final EscherClientAnchorRecord clientAnchor = (EscherClientAnchorRecord)Shape.getEscherChild(spContainer, -4080);
        final byte[] header = new byte[16];
        LittleEndian.putUShort(header, 0, 0);
        LittleEndian.putUShort(header, 2, 0);
        LittleEndian.putInt(header, 4, 8);
        clientAnchor.fillFields(header, 0, null);
        clientAnchor.setFlag((short)(anchor.y * 576 / 72));
        clientAnchor.setCol1((short)(anchor.x * 576 / 72));
        clientAnchor.setDx1((short)((anchor.width + anchor.x) * 576 / 72));
        clientAnchor.setRow1((short)((anchor.height + anchor.y) * 576 / 72));
        final EscherSpgrRecord spgr = (EscherSpgrRecord)Shape.getEscherChild(spContainer, -4087);
        spgr.setRectX1(anchor.x * 576 / 72);
        spgr.setRectY1(anchor.y * 576 / 72);
        spgr.setRectX2((anchor.x + anchor.width) * 576 / 72);
        spgr.setRectY2((anchor.y + anchor.height) * 576 / 72);
    }
    
    public void setCoordinates(final Rectangle2D anchor) {
        final EscherContainerRecord spContainer = (EscherContainerRecord)this._escherContainer.getChild(0);
        final EscherSpgrRecord spgr = (EscherSpgrRecord)Shape.getEscherChild(spContainer, -4087);
        final int x1 = (int)Math.round(anchor.getX() * 576.0 / 72.0);
        final int y1 = (int)Math.round(anchor.getY() * 576.0 / 72.0);
        final int x2 = (int)Math.round((anchor.getX() + anchor.getWidth()) * 576.0 / 72.0);
        final int y2 = (int)Math.round((anchor.getY() + anchor.getHeight()) * 576.0 / 72.0);
        spgr.setRectX1(x1);
        spgr.setRectY1(y1);
        spgr.setRectX2(x2);
        spgr.setRectY2(y2);
    }
    
    public Rectangle2D getCoordinates() {
        final EscherContainerRecord spContainer = (EscherContainerRecord)this._escherContainer.getChild(0);
        final EscherSpgrRecord spgr = (EscherSpgrRecord)Shape.getEscherChild(spContainer, -4087);
        final Rectangle2D.Float anchor = new Rectangle2D.Float();
        anchor.x = spgr.getRectX1() * 72.0f / 576.0f;
        anchor.y = spgr.getRectY1() * 72.0f / 576.0f;
        anchor.width = (spgr.getRectX2() - spgr.getRectX1()) * 72.0f / 576.0f;
        anchor.height = (spgr.getRectY2() - spgr.getRectY1()) * 72.0f / 576.0f;
        return anchor;
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final boolean isChild) {
        final EscherContainerRecord spgr = new EscherContainerRecord();
        spgr.setRecordId((short)(-4093));
        spgr.setOptions((short)15);
        final EscherContainerRecord spcont = new EscherContainerRecord();
        spcont.setRecordId((short)(-4092));
        spcont.setOptions((short)15);
        final EscherSpgrRecord spg = new EscherSpgrRecord();
        spg.setOptions((short)1);
        spcont.addChildRecord(spg);
        final EscherSpRecord sp = new EscherSpRecord();
        final short type = 2;
        sp.setOptions(type);
        sp.setFlags(513);
        spcont.addChildRecord(sp);
        final EscherClientAnchorRecord anchor = new EscherClientAnchorRecord();
        spcont.addChildRecord(anchor);
        spgr.addChildRecord(spcont);
        return spgr;
    }
    
    public void addShape(final Shape shape) {
        this._escherContainer.addChildRecord(shape.getSpContainer());
        final Sheet sheet = this.getSheet();
        shape.setSheet(sheet);
        shape.setShapeId(sheet.allocateShapeId());
        shape.afterInsert(sheet);
    }
    
    public void moveTo(final int x, final int y) {
        final Rectangle anchor = this.getAnchor();
        final int dx = x - anchor.x;
        final int dy = y - anchor.y;
        anchor.translate(dx, dy);
        this.setAnchor(anchor);
        final Shape[] shape = this.getShapes();
        for (int i = 0; i < shape.length; ++i) {
            final Rectangle chanchor = shape[i].getAnchor();
            chanchor.translate(dx, dy);
            shape[i].setAnchor(chanchor);
        }
    }
    
    @Override
    public Rectangle2D getAnchor2D() {
        final EscherContainerRecord spContainer = (EscherContainerRecord)this._escherContainer.getChild(0);
        final EscherClientAnchorRecord clientAnchor = (EscherClientAnchorRecord)Shape.getEscherChild(spContainer, -4080);
        Rectangle2D.Float anchor = new Rectangle2D.Float();
        if (clientAnchor == null) {
            this.logger.log(POILogger.INFO, "EscherClientAnchorRecord was not found for shape group. Searching for EscherChildAnchorRecord.");
            final EscherChildAnchorRecord rec = (EscherChildAnchorRecord)Shape.getEscherChild(spContainer, -4081);
            anchor = new Rectangle2D.Float(rec.getDx1() * 72.0f / 576.0f, rec.getDy1() * 72.0f / 576.0f, (rec.getDx2() - rec.getDx1()) * 72.0f / 576.0f, (rec.getDy2() - rec.getDy1()) * 72.0f / 576.0f);
        }
        else {
            anchor.x = clientAnchor.getCol1() * 72.0f / 576.0f;
            anchor.y = clientAnchor.getFlag() * 72.0f / 576.0f;
            anchor.width = (clientAnchor.getDx1() - clientAnchor.getCol1()) * 72.0f / 576.0f;
            anchor.height = (clientAnchor.getRow1() - clientAnchor.getFlag()) * 72.0f / 576.0f;
        }
        return anchor;
    }
    
    @Override
    public int getShapeType() {
        final EscherContainerRecord groupInfoContainer = (EscherContainerRecord)this._escherContainer.getChild(0);
        final EscherSpRecord spRecord = groupInfoContainer.getChildById((short)(-4086));
        return spRecord.getOptions() >> 4;
    }
    
    @Override
    public Hyperlink getHyperlink() {
        return null;
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final AffineTransform at = graphics.getTransform();
        final Shape[] sh = this.getShapes();
        for (int i = 0; i < sh.length; ++i) {
            sh[i].draw(graphics);
        }
        graphics.setTransform(at);
    }
}
