// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherRecord;
import java.util.List;
import org.apache.poi.ddf.EscherArrayProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import java.awt.Rectangle;

public final class Table extends ShapeGroup
{
    protected static final int BORDER_TOP = 1;
    protected static final int BORDER_RIGHT = 2;
    protected static final int BORDER_BOTTOM = 3;
    protected static final int BORDER_LEFT = 4;
    protected static final int BORDERS_ALL = 5;
    protected static final int BORDERS_OUTSIDE = 6;
    protected static final int BORDERS_INSIDE = 7;
    protected static final int BORDERS_NONE = 8;
    protected TableCell[][] cells;
    
    public Table(final int numrows, final int numcols) {
        if (numrows < 1) {
            throw new IllegalArgumentException("The number of rows must be greater than 1");
        }
        if (numcols < 1) {
            throw new IllegalArgumentException("The number of columns must be greater than 1");
        }
        int x = 0;
        int y = 0;
        int tblWidth = 0;
        int tblHeight = 0;
        this.cells = new TableCell[numrows][numcols];
        for (int i = 0; i < this.cells.length; ++i) {
            x = 0;
            for (int j = 0; j < this.cells[i].length; ++j) {
                this.cells[i][j] = new TableCell(this);
                final Rectangle anchor = new Rectangle(x, y, 100, 40);
                this.cells[i][j].setAnchor(anchor);
                x += 100;
            }
            y += 40;
        }
        tblWidth = x;
        tblHeight = y;
        this.setAnchor(new Rectangle(0, 0, tblWidth, tblHeight));
        final EscherContainerRecord spCont = (EscherContainerRecord)this.getSpContainer().getChild(0);
        final EscherOptRecord opt = new EscherOptRecord();
        opt.setRecordId((short)(-3806));
        opt.addEscherProperty(new EscherSimpleProperty((short)927, 1));
        final EscherArrayProperty p = new EscherArrayProperty((short)17312, false, null);
        p.setSizeOfElements(4);
        p.setNumberOfElementsInArray(numrows);
        p.setNumberOfElementsInMemory(numrows);
        opt.addEscherProperty(p);
        final List<EscherRecord> lst = spCont.getChildRecords();
        lst.add(lst.size() - 1, opt);
        spCont.setChildRecords(lst);
    }
    
    public Table(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public TableCell getCell(final int row, final int col) {
        return this.cells[row][col];
    }
    
    public int getNumberOfColumns() {
        return this.cells[0].length;
    }
    
    public int getNumberOfRows() {
        return this.cells.length;
    }
    
    @Override
    protected void afterInsert(final Sheet sh) {
        super.afterInsert(sh);
        final EscherContainerRecord spCont = (EscherContainerRecord)this.getSpContainer().getChild(0);
        final List<EscherRecord> lst = spCont.getChildRecords();
        final EscherOptRecord opt = lst.get(lst.size() - 2);
        final EscherArrayProperty p = (EscherArrayProperty)opt.getEscherProperty(1);
        for (int i = 0; i < this.cells.length; ++i) {
            final TableCell cell = this.cells[i][0];
            final int rowHeight = cell.getAnchor().height * 576 / 72;
            final byte[] val = new byte[4];
            LittleEndian.putInt(val, rowHeight);
            p.setElement(i, val);
            for (int j = 0; j < this.cells[i].length; ++j) {
                final TableCell c = this.cells[i][j];
                this.addShape(c);
                final Line bt = c.getBorderTop();
                if (bt != null) {
                    this.addShape(bt);
                }
                final Line br = c.getBorderRight();
                if (br != null) {
                    this.addShape(br);
                }
                final Line bb = c.getBorderBottom();
                if (bb != null) {
                    this.addShape(bb);
                }
                final Line bl = c.getBorderLeft();
                if (bl != null) {
                    this.addShape(bl);
                }
            }
        }
    }
    
    protected void initTable() {
        final Shape[] sh = this.getShapes();
        Arrays.sort(sh, new Comparator() {
            public int compare(final Object o1, final Object o2) {
                final Rectangle anchor1 = ((Shape)o1).getAnchor();
                final Rectangle anchor2 = ((Shape)o2).getAnchor();
                int delta = anchor1.y - anchor2.y;
                if (delta == 0) {
                    delta = anchor1.x - anchor2.x;
                }
                return delta;
            }
        });
        int y0 = -1;
        int maxrowlen = 0;
        final ArrayList lst = new ArrayList();
        ArrayList row = null;
        for (int i = 0; i < sh.length; ++i) {
            if (sh[i] instanceof TextShape) {
                final Rectangle anchor = sh[i].getAnchor();
                if (anchor.y != y0) {
                    y0 = anchor.y;
                    row = new ArrayList();
                    lst.add(row);
                }
                row.add(sh[i]);
                maxrowlen = Math.max(maxrowlen, row.size());
            }
        }
        this.cells = new TableCell[lst.size()][maxrowlen];
        for (int i = 0; i < lst.size(); ++i) {
            row = lst.get(i);
            for (int j = 0; j < row.size(); ++j) {
                final TextShape tx = row.get(j);
                (this.cells[i][j] = new TableCell(tx.getSpContainer(), this.getParent())).setSheet(tx.getSheet());
            }
        }
    }
    
    @Override
    public void setSheet(final Sheet sheet) {
        super.setSheet(sheet);
        if (this.cells == null) {
            this.initTable();
        }
    }
    
    public void setRowHeight(final int row, final int height) {
        final int currentHeight = this.cells[row][0].getAnchor().height;
        final int dy = height - currentHeight;
        for (int i = row; i < this.cells.length; ++i) {
            for (int j = 0; j < this.cells[i].length; ++j) {
                final Rectangle anchor = this.cells[i][j].getAnchor();
                if (i == row) {
                    anchor.height = height;
                }
                else {
                    final Rectangle rectangle = anchor;
                    rectangle.y += dy;
                }
                this.cells[i][j].setAnchor(anchor);
            }
        }
        final Rectangle anchor2;
        final Rectangle tblanchor = anchor2 = this.getAnchor();
        anchor2.height += dy;
        this.setAnchor(tblanchor);
    }
    
    public void setColumnWidth(final int col, final int width) {
        final int currentWidth = this.cells[0][col].getAnchor().width;
        final int dx = width - currentWidth;
        for (int i = 0; i < this.cells.length; ++i) {
            Rectangle anchor = this.cells[i][col].getAnchor();
            anchor.width = width;
            this.cells[i][col].setAnchor(anchor);
            if (col < this.cells[i].length - 1) {
                for (int j = col + 1; j < this.cells[i].length; ++j) {
                    final Rectangle anchor2;
                    anchor = (anchor2 = this.cells[i][j].getAnchor());
                    anchor2.x += dx;
                    this.cells[i][j].setAnchor(anchor);
                }
            }
        }
        final Rectangle anchor3;
        final Rectangle tblanchor = anchor3 = this.getAnchor();
        anchor3.width += dx;
        this.setAnchor(tblanchor);
    }
    
    public void setAllBorders(final Line line) {
        for (int i = 0; i < this.cells.length; ++i) {
            for (int j = 0; j < this.cells[i].length; ++j) {
                final TableCell cell = this.cells[i][j];
                cell.setBorderTop(this.cloneBorder(line));
                cell.setBorderLeft(this.cloneBorder(line));
                if (j == this.cells[i].length - 1) {
                    cell.setBorderRight(this.cloneBorder(line));
                }
                if (i == this.cells.length - 1) {
                    cell.setBorderBottom(this.cloneBorder(line));
                }
            }
        }
    }
    
    public void setOutsideBorders(final Line line) {
        for (int i = 0; i < this.cells.length; ++i) {
            for (int j = 0; j < this.cells[i].length; ++j) {
                final TableCell cell = this.cells[i][j];
                if (j == 0) {
                    cell.setBorderLeft(this.cloneBorder(line));
                }
                if (j == this.cells[i].length - 1) {
                    cell.setBorderRight(this.cloneBorder(line));
                }
                else {
                    cell.setBorderLeft(null);
                    cell.setBorderLeft(null);
                }
                if (i == 0) {
                    cell.setBorderTop(this.cloneBorder(line));
                }
                else if (i == this.cells.length - 1) {
                    cell.setBorderBottom(this.cloneBorder(line));
                }
                else {
                    cell.setBorderTop(null);
                    cell.setBorderBottom(null);
                }
            }
        }
    }
    
    public void setInsideBorders(final Line line) {
        for (int i = 0; i < this.cells.length; ++i) {
            for (int j = 0; j < this.cells[i].length; ++j) {
                final TableCell cell = this.cells[i][j];
                if (j != this.cells[i].length - 1) {
                    cell.setBorderRight(this.cloneBorder(line));
                }
                else {
                    cell.setBorderLeft(null);
                    cell.setBorderLeft(null);
                }
                if (i != this.cells.length - 1) {
                    cell.setBorderBottom(this.cloneBorder(line));
                }
                else {
                    cell.setBorderTop(null);
                    cell.setBorderBottom(null);
                }
            }
        }
    }
    
    private Line cloneBorder(final Line line) {
        final Line border = this.createBorder();
        border.setLineWidth(line.getLineWidth());
        border.setLineStyle(line.getLineStyle());
        border.setLineDashing(line.getLineDashing());
        border.setLineColor(line.getLineColor());
        return border;
    }
    
    public Line createBorder() {
        final Line line = new Line(this);
        final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(line.getSpContainer(), -4085);
        Shape.setEscherProperty(opt, (short)324, -1);
        Shape.setEscherProperty(opt, (short)383, -1);
        Shape.setEscherProperty(opt, (short)575, 131072);
        Shape.setEscherProperty(opt, (short)703, 524288);
        return line;
    }
}
