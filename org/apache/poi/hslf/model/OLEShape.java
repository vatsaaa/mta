// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.ExObjList;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.util.POILogger;
import org.apache.poi.hslf.usermodel.ObjectData;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.hslf.record.ExEmbed;

public final class OLEShape extends Picture
{
    protected ExEmbed _exEmbed;
    
    public OLEShape(final int idx) {
        super(idx);
    }
    
    public OLEShape(final int idx, final Shape parent) {
        super(idx, parent);
    }
    
    protected OLEShape(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    public int getObjectID() {
        return this.getEscherProperty((short)267);
    }
    
    public ObjectData getObjectData() {
        final SlideShow ppt = this.getSheet().getSlideShow();
        final ObjectData[] ole = ppt.getEmbeddedObjects();
        final ExEmbed exEmbed = this.getExEmbed();
        ObjectData data = null;
        if (exEmbed != null) {
            final int ref = exEmbed.getExOleObjAtom().getObjStgDataRef();
            for (int i = 0; i < ole.length; ++i) {
                if (ole[i].getExOleObjStg().getPersistId() == ref) {
                    data = ole[i];
                }
            }
        }
        if (data == null) {
            this.logger.log(POILogger.WARN, "OLE data not found");
        }
        return data;
    }
    
    public ExEmbed getExEmbed() {
        if (this._exEmbed == null) {
            final SlideShow ppt = this.getSheet().getSlideShow();
            final ExObjList lst = ppt.getDocumentRecord().getExObjList();
            if (lst == null) {
                this.logger.log(POILogger.WARN, "ExObjList not found");
                return null;
            }
            final int id = this.getObjectID();
            final Record[] ch = lst.getChildRecords();
            for (int i = 0; i < ch.length; ++i) {
                if (ch[i] instanceof ExEmbed) {
                    final ExEmbed embd = (ExEmbed)ch[i];
                    if (embd.getExOleObjAtom().getObjID() == id) {
                        this._exEmbed = embd;
                    }
                }
            }
        }
        return this._exEmbed;
    }
    
    public String getInstanceName() {
        return this.getExEmbed().getMenuName();
    }
    
    public String getFullName() {
        return this.getExEmbed().getClipboardName();
    }
    
    public String getProgID() {
        return this.getExEmbed().getProgId();
    }
}
