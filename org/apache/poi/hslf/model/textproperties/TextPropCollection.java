// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model.textproperties;

import java.io.IOException;
import org.apache.poi.hslf.record.Record;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hslf.record.StyleTextPropAtom;
import java.util.LinkedList;

public class TextPropCollection
{
    private int charactersCovered;
    private short reservedField;
    private LinkedList<TextProp> textPropList;
    private int maskSpecial;
    
    public int getSpecialMask() {
        return this.maskSpecial;
    }
    
    public int getCharactersCovered() {
        return this.charactersCovered;
    }
    
    public LinkedList<TextProp> getTextPropList() {
        return this.textPropList;
    }
    
    public TextProp findByName(final String textPropName) {
        for (int i = 0; i < this.textPropList.size(); ++i) {
            final TextProp prop = this.textPropList.get(i);
            if (prop.getName().equals(textPropName)) {
                return prop;
            }
        }
        return null;
    }
    
    public TextProp addWithName(final String name) {
        TextProp base = null;
        for (int i = 0; i < StyleTextPropAtom.characterTextPropTypes.length; ++i) {
            if (StyleTextPropAtom.characterTextPropTypes[i].getName().equals(name)) {
                base = StyleTextPropAtom.characterTextPropTypes[i];
            }
        }
        for (int i = 0; i < StyleTextPropAtom.paragraphTextPropTypes.length; ++i) {
            if (StyleTextPropAtom.paragraphTextPropTypes[i].getName().equals(name)) {
                base = StyleTextPropAtom.paragraphTextPropTypes[i];
            }
        }
        if (base == null) {
            throw new IllegalArgumentException("No TextProp with name " + name + " is defined to add from");
        }
        final TextProp textProp = (TextProp)base.clone();
        int pos = 0;
        for (int j = 0; j < this.textPropList.size(); ++j) {
            final TextProp curProp = this.textPropList.get(j);
            if (textProp.getMask() > curProp.getMask()) {
                ++pos;
            }
        }
        this.textPropList.add(pos, textProp);
        return textProp;
    }
    
    public int buildTextPropList(final int containsField, final TextProp[] potentialProperties, final byte[] data, final int dataOffset) {
        int bytesPassed = 0;
        for (int i = 0; i < potentialProperties.length; ++i) {
            if ((containsField & potentialProperties[i].getMask()) != 0x0) {
                if (dataOffset + bytesPassed >= data.length) {
                    this.maskSpecial |= potentialProperties[i].getMask();
                    return bytesPassed;
                }
                final TextProp prop = (TextProp)potentialProperties[i].clone();
                int val = 0;
                if (prop.getSize() == 2) {
                    val = LittleEndian.getShort(data, dataOffset + bytesPassed);
                }
                else if (prop.getSize() == 4) {
                    val = LittleEndian.getInt(data, dataOffset + bytesPassed);
                }
                else if (prop.getSize() == 0) {
                    this.maskSpecial |= potentialProperties[i].getMask();
                    continue;
                }
                prop.setValue(val);
                bytesPassed += prop.getSize();
                this.textPropList.add(prop);
            }
        }
        return bytesPassed;
    }
    
    public TextPropCollection(final int charactersCovered, final short reservedField) {
        this.maskSpecial = 0;
        this.charactersCovered = charactersCovered;
        this.reservedField = reservedField;
        this.textPropList = new LinkedList<TextProp>();
    }
    
    public TextPropCollection(final int textSize) {
        this.maskSpecial = 0;
        this.charactersCovered = textSize;
        this.reservedField = -1;
        this.textPropList = new LinkedList<TextProp>();
    }
    
    public void updateTextSize(final int textSize) {
        this.charactersCovered = textSize;
    }
    
    public void writeOut(final OutputStream o) throws IOException {
        Record.writeLittleEndian(this.charactersCovered, o);
        if (this.reservedField > -1) {
            Record.writeLittleEndian(this.reservedField, o);
        }
        int mask = this.maskSpecial;
        for (int i = 0; i < this.textPropList.size(); ++i) {
            final TextProp textProp = this.textPropList.get(i);
            if (textProp instanceof BitMaskTextProp) {
                if (mask == 0) {
                    mask |= textProp.getWriteMask();
                }
            }
            else {
                mask |= textProp.getWriteMask();
            }
        }
        Record.writeLittleEndian(mask, o);
        for (int i = 0; i < this.textPropList.size(); ++i) {
            final TextProp textProp = this.textPropList.get(i);
            final int val = textProp.getValue();
            if (textProp.getSize() == 2) {
                Record.writeLittleEndian((short)val, o);
            }
            else if (textProp.getSize() == 4) {
                Record.writeLittleEndian(val, o);
            }
        }
    }
    
    public short getReservedField() {
        return this.reservedField;
    }
    
    public void setReservedField(final short val) {
        this.reservedField = val;
    }
}
