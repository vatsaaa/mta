// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model.textproperties;

public final class ParagraphFlagsTextProp extends BitMaskTextProp
{
    public static final int BULLET_IDX = 0;
    public static final int BULLET_HARDFONT_IDX = 1;
    public static final int BULLET_HARDCOLOR_IDX = 2;
    public static final int BULLET_HARDSIZE_IDX = 4;
    public static String NAME;
    
    public ParagraphFlagsTextProp() {
        super(2, 15, ParagraphFlagsTextProp.NAME, new String[] { "bullet", "bullet.hardfont", "bullet.hardcolor", "bullet.hardsize" });
    }
    
    static {
        ParagraphFlagsTextProp.NAME = "paragraph_flags";
    }
}
