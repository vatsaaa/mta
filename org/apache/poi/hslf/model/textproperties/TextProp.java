// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model.textproperties;

public class TextProp implements Cloneable
{
    protected int sizeOfDataBlock;
    protected String propName;
    protected int dataValue;
    protected int maskInHeader;
    
    public TextProp(final int sizeOfDataBlock, final int maskInHeader, final String propName) {
        this.sizeOfDataBlock = sizeOfDataBlock;
        this.maskInHeader = maskInHeader;
        this.propName = propName;
        this.dataValue = 0;
    }
    
    public String getName() {
        return this.propName;
    }
    
    public int getSize() {
        return this.sizeOfDataBlock;
    }
    
    public int getMask() {
        return this.maskInHeader;
    }
    
    public int getWriteMask() {
        return this.getMask();
    }
    
    public int getValue() {
        return this.dataValue;
    }
    
    public void setValue(final int val) {
        this.dataValue = val;
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError(e.getMessage());
        }
    }
}
