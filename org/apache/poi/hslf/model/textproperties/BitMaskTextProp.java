// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model.textproperties;

public class BitMaskTextProp extends TextProp implements Cloneable
{
    private String[] subPropNames;
    private int[] subPropMasks;
    private boolean[] subPropMatches;
    
    public String[] getSubPropNames() {
        return this.subPropNames;
    }
    
    public boolean[] getSubPropMatches() {
        return this.subPropMatches;
    }
    
    public BitMaskTextProp(final int sizeOfDataBlock, final int maskInHeader, final String overallName, final String[] subPropNames) {
        super(sizeOfDataBlock, maskInHeader, "bitmask");
        this.subPropNames = subPropNames;
        this.propName = overallName;
        this.subPropMasks = new int[subPropNames.length];
        this.subPropMatches = new boolean[subPropNames.length];
        for (int i = 0; i < this.subPropMasks.length; ++i) {
            this.subPropMasks[i] = 1 << i;
        }
    }
    
    @Override
    public int getWriteMask() {
        return this.dataValue;
    }
    
    @Override
    public void setValue(final int val) {
        this.dataValue = val;
        for (int i = 0; i < this.subPropMatches.length; ++i) {
            this.subPropMatches[i] = false;
            if ((this.dataValue & this.subPropMasks[i]) != 0x0) {
                this.subPropMatches[i] = true;
            }
        }
    }
    
    public boolean getSubValue(final int idx) {
        return this.subPropMatches[idx];
    }
    
    public void setSubValue(final boolean value, final int idx) {
        if (this.subPropMatches[idx] == value) {
            return;
        }
        if (value) {
            this.dataValue += this.subPropMasks[idx];
        }
        else {
            this.dataValue -= this.subPropMasks[idx];
        }
        this.subPropMatches[idx] = value;
    }
    
    @Override
    public Object clone() {
        final BitMaskTextProp newObj = (BitMaskTextProp)super.clone();
        newObj.subPropMatches = new boolean[this.subPropMatches.length];
        return newObj;
    }
}
