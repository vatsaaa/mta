// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.model;

import java.io.UnsupportedEncodingException;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherComplexProperty;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.Document;
import org.apache.poi.hslf.record.ExObjList;
import org.apache.poi.hslf.record.ExControl;
import org.apache.poi.hslf.record.RecordTypes;
import java.util.Iterator;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.record.OEShapeAtom;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public final class ActiveXShape extends Picture
{
    public static final int DEFAULT_ACTIVEX_THUMBNAIL = -1;
    
    public ActiveXShape(final int movieIdx, final int pictureIdx) {
        super(pictureIdx, null);
        this.setActiveXIndex(movieIdx);
    }
    
    protected ActiveXShape(final EscherContainerRecord escherRecord, final Shape parent) {
        super(escherRecord, parent);
    }
    
    @Override
    protected EscherContainerRecord createSpContainer(final int idx, final boolean isChild) {
        this._escherContainer = super.createSpContainer(idx, isChild);
        final EscherSpRecord spRecord = this._escherContainer.getChildById((short)(-4086));
        spRecord.setFlags(2576);
        this.setShapeType(201);
        this.setEscherProperty((short)267, idx);
        this.setEscherProperty((short)448, 134217729);
        this.setEscherProperty((short)511, 524296);
        this.setEscherProperty((short)513, 134217730);
        this.setEscherProperty((short)127, -1);
        final EscherClientDataRecord cldata = new EscherClientDataRecord();
        cldata.setOptions((short)15);
        this._escherContainer.addChildRecord(cldata);
        final OEShapeAtom oe = new OEShapeAtom();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            oe.writeOut(out);
        }
        catch (Exception e) {
            throw new HSLFException(e);
        }
        cldata.setRemainingData(out.toByteArray());
        return this._escherContainer;
    }
    
    public void setActiveXIndex(final int idx) {
        final EscherContainerRecord spContainer = this.getSpContainer();
        final Iterator<EscherRecord> it = spContainer.getChildIterator();
        while (it.hasNext()) {
            final EscherRecord obj = it.next();
            if (obj.getRecordId() == -4079) {
                final EscherClientDataRecord clientRecord = (EscherClientDataRecord)obj;
                final byte[] recdata = clientRecord.getRemainingData();
                LittleEndian.putInt(recdata, 8, idx);
            }
        }
    }
    
    public int getControlIndex() {
        int idx = -1;
        final OEShapeAtom oe = (OEShapeAtom)this.getClientDataRecord(RecordTypes.OEShapeAtom.typeID);
        if (oe != null) {
            idx = oe.getOptions();
        }
        return idx;
    }
    
    public void setProperty(final String key, final String value) {
    }
    
    public ExControl getExControl() {
        final int idx = this.getControlIndex();
        ExControl ctrl = null;
        final Document doc = this.getSheet().getSlideShow().getDocumentRecord();
        final ExObjList lst = (ExObjList)doc.findFirstOfType(RecordTypes.ExObjList.typeID);
        if (lst != null) {
            final Record[] ch = lst.getChildRecords();
            for (int i = 0; i < ch.length; ++i) {
                if (ch[i] instanceof ExControl) {
                    final ExControl c = (ExControl)ch[i];
                    if (c.getExOleObjAtom().getObjID() == idx) {
                        ctrl = c;
                        break;
                    }
                }
            }
        }
        return ctrl;
    }
    
    @Override
    protected void afterInsert(final Sheet sheet) {
        final ExControl ctrl = this.getExControl();
        ctrl.getExControlAtom().setSlideId(sheet._getSheetNumber());
        try {
            final String name = ctrl.getProgId() + "-" + this.getControlIndex();
            final byte[] data = (name + '\0').getBytes("UTF-16LE");
            final EscherComplexProperty prop = new EscherComplexProperty((short)896, false, data);
            final EscherOptRecord opt = (EscherOptRecord)Shape.getEscherChild(this._escherContainer, -4085);
            opt.addEscherProperty(prop);
        }
        catch (UnsupportedEncodingException e) {
            throw new HSLFException(e);
        }
    }
}
