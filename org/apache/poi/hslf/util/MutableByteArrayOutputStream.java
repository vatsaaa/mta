// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.util;

import java.io.ByteArrayOutputStream;

public final class MutableByteArrayOutputStream extends ByteArrayOutputStream
{
    public int getBytesWritten() {
        return -1;
    }
    
    @Override
    public void write(final byte[] b) {
    }
    
    @Override
    public void write(final int b) {
    }
    
    public void overwrite(final byte[] b, final int startPos) {
    }
}
