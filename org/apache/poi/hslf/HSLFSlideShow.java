// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf;

import org.apache.poi.hslf.record.ExOleObjStg;
import org.apache.poi.hslf.exceptions.HSLFException;
import java.io.ByteArrayInputStream;
import org.apache.poi.hslf.record.PositionDependentRecord;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.hslf.exceptions.CorruptPowerPointFileException;
import org.apache.poi.util.LittleEndian;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Hashtable;
import org.apache.poi.hslf.record.PersistRecord;
import java.util.Arrays;
import org.apache.poi.hslf.record.PersistPtrHolder;
import org.apache.poi.hslf.record.UserEditAtom;
import java.util.HashMap;
import java.util.ArrayList;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.hslf.exceptions.EncryptedPowerPointFileException;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hslf.usermodel.ObjectData;
import org.apache.poi.hslf.usermodel.PictureData;
import java.util.List;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.CurrentUserAtom;
import org.apache.poi.util.POILogger;
import org.apache.poi.POIDocument;

public final class HSLFSlideShow extends POIDocument
{
    private POILogger logger;
    private CurrentUserAtom currentUser;
    private byte[] _docstream;
    private Record[] _records;
    private List<PictureData> _pictures;
    private ObjectData[] _objects;
    
    protected POIFSFileSystem getPOIFSFileSystem() {
        return this.directory.getFileSystem();
    }
    
    protected DirectoryNode getPOIFSDirectory() {
        return this.directory;
    }
    
    public HSLFSlideShow(final String fileName) throws IOException {
        this(new FileInputStream(fileName));
    }
    
    public HSLFSlideShow(final InputStream inputStream) throws IOException {
        this(new POIFSFileSystem(inputStream));
    }
    
    public HSLFSlideShow(final POIFSFileSystem filesystem) throws IOException {
        this(filesystem.getRoot());
    }
    
    public HSLFSlideShow(final NPOIFSFileSystem filesystem) throws IOException {
        this(filesystem.getRoot());
    }
    
    @Deprecated
    public HSLFSlideShow(final DirectoryNode dir, final POIFSFileSystem filesystem) throws IOException {
        this(dir);
    }
    
    public HSLFSlideShow(final DirectoryNode dir) throws IOException {
        super(dir);
        this.logger = POILogFactory.getLogger(this.getClass());
        this.readCurrentUserStream();
        this.readPowerPointStream();
        final boolean encrypted = EncryptedSlideShow.checkIfEncrypted(this);
        if (encrypted) {
            throw new EncryptedPowerPointFileException("Encrypted PowerPoint files are not supported");
        }
        this.buildRecords();
        this.readOtherStreams();
    }
    
    public static final HSLFSlideShow create() {
        final InputStream is = HSLFSlideShow.class.getResourceAsStream("data/empty.ppt");
        if (is == null) {
            throw new RuntimeException("Missing resource 'empty.ppt'");
        }
        try {
            return new HSLFSlideShow(is);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private void readPowerPointStream() throws IOException {
        final DocumentEntry docProps = (DocumentEntry)this.directory.getEntry("PowerPoint Document");
        this._docstream = new byte[docProps.getSize()];
        this.directory.createDocumentInputStream("PowerPoint Document").read(this._docstream);
    }
    
    private void buildRecords() {
        this._records = this.read(this._docstream, (int)this.currentUser.getCurrentEditOffset());
    }
    
    private Record[] read(final byte[] docstream, int usrOffset) {
        final ArrayList<Integer> lst = new ArrayList<Integer>();
        final HashMap<Integer, Integer> offset2id = new HashMap<Integer, Integer>();
        while (usrOffset != 0) {
            final UserEditAtom usr = (UserEditAtom)Record.buildRecordAtOffset(docstream, usrOffset);
            lst.add(usrOffset);
            final int psrOffset = usr.getPersistPointersOffset();
            final PersistPtrHolder ptr = (PersistPtrHolder)Record.buildRecordAtOffset(docstream, psrOffset);
            lst.add(psrOffset);
            final Hashtable<Integer, Integer> entries = ptr.getSlideLocationsLookup();
            for (final Integer id : entries.keySet()) {
                final Integer offset = entries.get(id);
                lst.add(offset);
                offset2id.put(offset, id);
            }
            usrOffset = usr.getLastUserEditAtomOffset();
        }
        final Integer[] a = lst.toArray(new Integer[lst.size()]);
        Arrays.sort(a);
        final Record[] rec = new Record[lst.size()];
        for (int i = 0; i < a.length; ++i) {
            final Integer offset2 = a[i];
            rec[i] = Record.buildRecordAtOffset(docstream, offset2);
            if (rec[i] instanceof PersistRecord) {
                final PersistRecord psr = (PersistRecord)rec[i];
                final Integer id = offset2id.get(offset2);
                psr.setPersistId(id);
            }
        }
        return rec;
    }
    
    private void readCurrentUserStream() {
        try {
            this.currentUser = new CurrentUserAtom(this.directory);
        }
        catch (IOException ie) {
            this.logger.log(POILogger.ERROR, "Error finding Current User Atom:\n" + ie);
            this.currentUser = new CurrentUserAtom();
        }
    }
    
    private void readOtherStreams() {
    }
    
    private void readPictures() throws IOException {
        this._pictures = new ArrayList<PictureData>();
        byte[] pictstream;
        try {
            final DocumentEntry entry = (DocumentEntry)this.directory.getEntry("Pictures");
            pictstream = new byte[entry.getSize()];
            final DocumentInputStream is = this.directory.createDocumentInputStream("Pictures");
            is.read(pictstream);
        }
        catch (FileNotFoundException e2) {
            return;
        }
        int imgsize;
        for (int pos = 0; pos <= pictstream.length - 8; pos += imgsize) {
            final int offset = pos;
            final int signature = LittleEndian.getUShort(pictstream, pos);
            pos += 2;
            final int type = LittleEndian.getUShort(pictstream, pos);
            pos += 2;
            imgsize = LittleEndian.getInt(pictstream, pos);
            pos += 4;
            if (type != 61447) {
                if (type < 61464) {
                    break;
                }
                if (type > 61719) {
                    break;
                }
            }
            if (imgsize < 0) {
                throw new CorruptPowerPointFileException("The file contains a picture, at position " + this._pictures.size() + ", which has a negatively sized data length, so we can't trust any of the picture data");
            }
            if (type == 0) {
                this.logger.log(POILogger.ERROR, "Problem reading picture: Invalid image type 0, on picture with length " + imgsize + ".\nYou document will probably become corrupted if you save it!");
                this.logger.log(POILogger.ERROR, "" + pos);
            }
            else {
                try {
                    final PictureData pict = PictureData.create(type - 61464);
                    final byte[] imgdata = new byte[imgsize];
                    System.arraycopy(pictstream, pos, imgdata, 0, imgdata.length);
                    pict.setRawData(imgdata);
                    pict.setOffset(offset);
                    this._pictures.add(pict);
                }
                catch (IllegalArgumentException e) {
                    this.logger.log(POILogger.ERROR, "Problem reading picture: " + e + "\nYou document will probably become corrupted if you save it!");
                }
            }
        }
    }
    
    @Override
    public void write(final OutputStream out) throws IOException {
        this.write(out, false);
    }
    
    public void write(final OutputStream out, final boolean preserveNodes) throws IOException {
        final POIFSFileSystem outFS = new POIFSFileSystem();
        final List<String> writtenEntries = new ArrayList<String>(1);
        this.writeProperties(outFS, writtenEntries);
        final Hashtable<Integer, Integer> oldToNewPositions = new Hashtable<Integer, Integer>();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < this._records.length; ++i) {
            if (this._records[i] instanceof PositionDependentRecord) {
                final PositionDependentRecord pdr = (PositionDependentRecord)this._records[i];
                final int oldPos = pdr.getLastOnDiskOffset();
                final int newPos = baos.size();
                pdr.setLastOnDiskOffset(newPos);
                oldToNewPositions.put(oldPos, newPos);
            }
            this._records[i].writeOut(baos);
        }
        baos.reset();
        for (int i = 0; i < this._records.length; ++i) {
            if (this._records[i] instanceof PositionDependentRecord) {
                final PositionDependentRecord pdr = (PositionDependentRecord)this._records[i];
                pdr.updateOtherRecordReferences(oldToNewPositions);
            }
            this._records[i].writeOut(baos);
        }
        this._docstream = baos.toByteArray();
        final ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        outFS.createDocument(bais, "PowerPoint Document");
        writtenEntries.add("PowerPoint Document");
        final int oldLastUserEditAtomPos = (int)this.currentUser.getCurrentEditOffset();
        final Integer newLastUserEditAtomPos = oldToNewPositions.get(oldLastUserEditAtomPos);
        if (newLastUserEditAtomPos == null) {
            throw new HSLFException("Couldn't find the new location of the UserEditAtom that used to be at " + oldLastUserEditAtomPos);
        }
        this.currentUser.setCurrentEditOffset(newLastUserEditAtomPos);
        this.currentUser.writeToFS(outFS);
        writtenEntries.add("Current User");
        if (this._pictures == null) {
            this.readPictures();
        }
        if (this._pictures.size() > 0) {
            final ByteArrayOutputStream pict = new ByteArrayOutputStream();
            for (final PictureData p : this._pictures) {
                p.write(pict);
            }
            outFS.createDocument(new ByteArrayInputStream(pict.toByteArray()), "Pictures");
            writtenEntries.add("Pictures");
        }
        if (preserveNodes) {
            this.copyNodes(this.directory.getFileSystem(), outFS, writtenEntries);
        }
        outFS.writeFilesystem(out);
    }
    
    public synchronized int appendRootLevelRecord(final Record newRecord) {
        int addedAt = -1;
        final Record[] r = new Record[this._records.length + 1];
        boolean added = false;
        for (int i = this._records.length - 1; i >= 0; --i) {
            if (added) {
                r[i] = this._records[i];
            }
            else {
                r[i + 1] = this._records[i];
                if (this._records[i] instanceof PersistPtrHolder) {
                    r[i] = newRecord;
                    added = true;
                    addedAt = i;
                }
            }
        }
        this._records = r;
        return addedAt;
    }
    
    public int addPicture(final PictureData img) {
        if (this._pictures == null) {
            try {
                this.readPictures();
            }
            catch (IOException e) {
                throw new CorruptPowerPointFileException(e.getMessage());
            }
        }
        int offset = 0;
        if (this._pictures.size() > 0) {
            final PictureData prev = this._pictures.get(this._pictures.size() - 1);
            offset = prev.getOffset() + prev.getRawData().length + 8;
        }
        img.setOffset(offset);
        this._pictures.add(img);
        return offset;
    }
    
    public Record[] getRecords() {
        return this._records;
    }
    
    public byte[] getUnderlyingBytes() {
        return this._docstream;
    }
    
    public CurrentUserAtom getCurrentUserAtom() {
        return this.currentUser;
    }
    
    public PictureData[] getPictures() {
        if (this._pictures == null) {
            try {
                this.readPictures();
            }
            catch (IOException e) {
                throw new CorruptPowerPointFileException(e.getMessage());
            }
        }
        return this._pictures.toArray(new PictureData[this._pictures.size()]);
    }
    
    public ObjectData[] getEmbeddedObjects() {
        if (this._objects == null) {
            final List<ObjectData> objects = new ArrayList<ObjectData>();
            for (int i = 0; i < this._records.length; ++i) {
                if (this._records[i] instanceof ExOleObjStg) {
                    objects.add(new ObjectData((ExOleObjStg)this._records[i]));
                }
            }
            this._objects = objects.toArray(new ObjectData[objects.size()]);
        }
        return this._objects;
    }
}
