// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import org.apache.poi.hslf.record.SlidePersistAtom;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.TextCharsAtom;
import org.apache.poi.hslf.record.TextBytesAtom;
import org.apache.poi.hslf.record.SlideListWithText;
import org.apache.poi.hslf.record.Document;
import org.apache.poi.hslf.HSLFSlideShow;

public final class SLWTTextListing
{
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Need to give a filename");
            System.exit(1);
        }
        final HSLFSlideShow ss = new HSLFSlideShow(args[0]);
        final Record[] records = ss.getRecords();
        for (int i = 0; i < records.length; ++i) {
            if (records[i] instanceof Document) {
                final Record docRecord = records[i];
                final Record[] docChildren = docRecord.getChildRecords();
                for (int j = 0; j < docChildren.length; ++j) {
                    if (docChildren[j] instanceof SlideListWithText) {
                        System.out.println("Found SLWT at pos " + j + " in the Document at " + i);
                        System.out.println("  Has " + docChildren[j].getChildRecords().length + " children");
                        final SlideListWithText slwt = (SlideListWithText)docChildren[j];
                        final SlideListWithText.SlideAtomsSet[] thisSets = slwt.getSlideAtomsSets();
                        System.out.println("  Has " + thisSets.length + " AtomSets in it");
                        for (int k = 0; k < thisSets.length; ++k) {
                            final SlidePersistAtom spa = thisSets[k].getSlidePersistAtom();
                            System.out.println("    " + k + " has slide id " + spa.getSlideIdentifier());
                            System.out.println("    " + k + " has ref id " + spa.getRefID());
                            final Record[] slwtc = thisSets[k].getSlideRecords();
                            for (int l = 0; l < slwtc.length; ++l) {
                                String text = null;
                                if (slwtc[l] instanceof TextBytesAtom) {
                                    final TextBytesAtom tba = (TextBytesAtom)slwtc[l];
                                    text = tba.getText();
                                }
                                if (slwtc[l] instanceof TextCharsAtom) {
                                    final TextCharsAtom tca = (TextCharsAtom)slwtc[l];
                                    text = tca.getText();
                                }
                                if (text != null) {
                                    text = text.replace('\r', '\n');
                                    System.out.println("        ''" + text + "''");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
