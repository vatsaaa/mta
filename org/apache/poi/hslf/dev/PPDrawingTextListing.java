// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import org.apache.poi.hslf.record.EscherTextboxWrapper;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.TextCharsAtom;
import org.apache.poi.hslf.record.TextBytesAtom;
import org.apache.poi.hslf.record.PPDrawing;
import org.apache.poi.hslf.HSLFSlideShow;

public final class PPDrawingTextListing
{
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Need to give a filename");
            System.exit(1);
        }
        final HSLFSlideShow ss = new HSLFSlideShow(args[0]);
        final Record[] records = ss.getRecords();
        for (int i = 0; i < records.length; ++i) {
            final Record[] children = records[i].getChildRecords();
            if (children != null && children.length != 0) {
                for (int j = 0; j < children.length; ++j) {
                    if (children[j] instanceof PPDrawing) {
                        System.out.println("Found PPDrawing at " + j + " in top level record " + i + " (" + records[i].getRecordType() + ")");
                        final PPDrawing ppd = (PPDrawing)children[j];
                        final EscherTextboxWrapper[] wrappers = ppd.getTextboxWrappers();
                        System.out.println("  Has " + wrappers.length + " textbox wrappers within");
                        for (int k = 0; k < wrappers.length; ++k) {
                            final EscherTextboxWrapper tbw = wrappers[k];
                            System.out.println("    " + k + " has " + tbw.getChildRecords().length + " PPT atoms within");
                            final Record[] pptatoms = tbw.getChildRecords();
                            for (int l = 0; l < pptatoms.length; ++l) {
                                String text = null;
                                if (pptatoms[l] instanceof TextBytesAtom) {
                                    final TextBytesAtom tba = (TextBytesAtom)pptatoms[l];
                                    text = tba.getText();
                                }
                                if (pptatoms[l] instanceof TextCharsAtom) {
                                    final TextCharsAtom tca = (TextCharsAtom)pptatoms[l];
                                    text = tca.getText();
                                }
                                if (text != null) {
                                    text = text.replace('\r', '\n');
                                    System.out.println("        ''" + text + "''");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
