// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import java.io.StringWriter;
import java.io.FileWriter;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.util.LittleEndian;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.FileInputStream;
import java.io.File;
import java.io.Writer;

public final class PPTXMLDump
{
    public static final int HEADER_SIZE = 8;
    public static final int PICT_HEADER_SIZE = 25;
    public static final String PPDOC_ENTRY = "PowerPoint Document";
    public static final String PICTURES_ENTRY = "Pictures";
    public static String CR;
    protected Writer out;
    protected byte[] docstream;
    protected byte[] pictstream;
    protected boolean hexHeader;
    private static final byte[] hexval;
    
    public PPTXMLDump(final File ppt) throws IOException {
        this.hexHeader = true;
        final FileInputStream fis = new FileInputStream(ppt);
        final POIFSFileSystem fs = new POIFSFileSystem(fis);
        fis.close();
        DocumentEntry entry = (DocumentEntry)fs.getRoot().getEntry("PowerPoint Document");
        this.docstream = new byte[entry.getSize()];
        DocumentInputStream is = fs.createDocumentInputStream("PowerPoint Document");
        is.read(this.docstream);
        try {
            entry = (DocumentEntry)fs.getRoot().getEntry("Pictures");
            this.pictstream = new byte[entry.getSize()];
            is = fs.createDocumentInputStream("Pictures");
            is.read(this.pictstream);
        }
        catch (FileNotFoundException ex) {}
    }
    
    public void dump(final Writer out) throws IOException {
        this.out = out;
        int padding = 0;
        write(out, "<Presentation>" + PPTXMLDump.CR, padding);
        ++padding;
        if (this.pictstream != null) {
            write(out, "<Pictures>" + PPTXMLDump.CR, padding);
            this.dumpPictures(this.pictstream, padding);
            write(out, "</Pictures>" + PPTXMLDump.CR, padding);
        }
        write(out, "<PowerPointDocument>" + PPTXMLDump.CR, padding);
        ++padding;
        this.dump(this.docstream, 0, this.docstream.length, padding);
        --padding;
        write(out, "</PowerPointDocument>" + PPTXMLDump.CR, padding);
        --padding;
        write(out, "</Presentation>", padding);
    }
    
    public void dump(final byte[] data, final int offset, final int length, int padding) throws IOException {
        int size;
        for (int pos = offset; pos <= offset + length - 8 && pos >= 0; pos += size) {
            final int info = LittleEndian.getUShort(data, pos);
            pos += 2;
            final int type = LittleEndian.getUShort(data, pos);
            pos += 2;
            size = (int)LittleEndian.getUInt(data, pos);
            pos += 4;
            final String recname = RecordTypes.recordName(type);
            write(this.out, "<" + recname + " info=\"" + info + "\" type=\"" + type + "\" size=\"" + size + "\" offset=\"" + (pos - 8) + "\"", padding);
            if (this.hexHeader) {
                this.out.write(" header=\"");
                dump(this.out, data, pos - 8, 8, 0, false);
                this.out.write("\"");
            }
            this.out.write(">" + PPTXMLDump.CR);
            ++padding;
            final boolean isContainer = (info & 0xF) == 0xF;
            if (isContainer) {
                this.dump(data, pos, size, padding);
            }
            else {
                dump(this.out, data, pos, Math.min(size, data.length - pos), padding, true);
            }
            --padding;
            write(this.out, "</" + recname + ">" + PPTXMLDump.CR, padding);
        }
    }
    
    public void dumpPictures(final byte[] data, int padding) throws IOException {
        int pos = 0;
        while (pos < data.length) {
            final byte[] header = new byte[25];
            System.arraycopy(data, pos, header, 0, header.length);
            final int size = LittleEndian.getInt(header, 4) - 17;
            final byte[] pictdata = new byte[size];
            System.arraycopy(data, pos + 25, pictdata, 0, pictdata.length);
            pos += 25 + size;
            ++padding;
            write(this.out, "<picture size=\"" + size + "\" type=\"" + this.getPictureType(header) + "\">" + PPTXMLDump.CR, padding);
            ++padding;
            write(this.out, "<header>" + PPTXMLDump.CR, padding);
            dump(this.out, header, 0, header.length, padding, true);
            write(this.out, "</header>" + PPTXMLDump.CR, padding);
            write(this.out, "<imgdata>" + PPTXMLDump.CR, padding);
            dump(this.out, pictdata, 0, Math.min(pictdata.length, 100), padding, true);
            write(this.out, "</imgdata>" + PPTXMLDump.CR, padding);
            --padding;
            write(this.out, "</picture>" + PPTXMLDump.CR, padding);
            --padding;
        }
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length == 0) {
            System.out.println("Usage: PPTXMLDump (options) pptfile\nWhere options include:\n    -f     write output to <pptfile>.xml file in the current directory");
            return;
        }
        boolean outFile = false;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].startsWith("-")) {
                if ("-f".equals(args[i])) {
                    outFile = true;
                }
            }
            else {
                final File ppt = new File(args[i]);
                final PPTXMLDump dump = new PPTXMLDump(ppt);
                System.out.println("Dumping " + args[i]);
                if (outFile) {
                    final FileWriter out = new FileWriter(ppt.getName() + ".xml");
                    dump.dump(out);
                    out.close();
                }
                else {
                    final StringWriter out2 = new StringWriter();
                    dump.dump(out2);
                    System.out.println(out2.toString());
                }
            }
        }
    }
    
    private static void write(final Writer out, final String str, final int padding) throws IOException {
        for (int i = 0; i < padding; ++i) {
            out.write("  ");
        }
        out.write(str);
    }
    
    private String getPictureType(final byte[] header) {
        final int meta = LittleEndian.getUShort(header, 0);
        String type = null;
        switch (meta) {
            case 18080: {
                type = "jpeg";
                break;
            }
            case 8544: {
                type = "wmf";
                break;
            }
            case 28160: {
                type = "png";
                break;
            }
            default: {
                type = "unknown";
                break;
            }
        }
        return type;
    }
    
    private static void dump(final Writer out, final byte[] data, final int offset, final int length, final int padding, final boolean nl) throws IOException {
        final int linesize = 25;
        for (int i = 0; i < padding; ++i) {
            out.write("  ");
        }
        for (int i = offset; i < offset + length; ++i) {
            final int c = data[i];
            out.write((char)PPTXMLDump.hexval[(c & 0xF0) >> 4]);
            out.write((char)PPTXMLDump.hexval[(c & 0xF) >> 0]);
            out.write(32);
            if ((i + 1 - offset) % linesize == 0 && i != offset + length - 1) {
                out.write(PPTXMLDump.CR);
                for (int j = 0; j < padding; ++j) {
                    out.write("  ");
                }
            }
        }
        if (nl && length > 0) {
            out.write(PPTXMLDump.CR);
        }
    }
    
    static {
        PPTXMLDump.CR = System.getProperty("line.separator");
        hexval = new byte[] { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70 };
    }
}
