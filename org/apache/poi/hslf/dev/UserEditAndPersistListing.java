// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.hslf.record.CurrentUserAtom;
import java.util.Hashtable;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.UserEditAtom;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.record.PositionDependentRecord;
import org.apache.poi.hslf.record.PersistPtrHolder;
import org.apache.poi.hslf.HSLFSlideShow;

public final class UserEditAndPersistListing
{
    private static byte[] fileContents;
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Need to give a filename");
            System.exit(1);
        }
        final HSLFSlideShow ss = new HSLFSlideShow(args[0]);
        UserEditAndPersistListing.fileContents = ss.getUnderlyingBytes();
        System.out.println("");
        final Record[] records = ss.getRecords();
        int pos = 0;
        for (int i = 0; i < records.length; ++i) {
            final Record r = records[i];
            if (r.getRecordType() == 6001L) {
                System.out.println("Found PersistPtrFullBlock at " + pos + " (" + Integer.toHexString(pos) + ")");
            }
            if (r.getRecordType() == 6002L) {
                System.out.println("Found PersistPtrIncrementalBlock at " + pos + " (" + Integer.toHexString(pos) + ")");
                final PersistPtrHolder pph = (PersistPtrHolder)r;
                final int[] sheetIDs = pph.getKnownSlideIDs();
                final Hashtable sheetOffsets = pph.getSlideLocationsLookup();
                for (int j = 0; j < sheetIDs.length; ++j) {
                    final Integer id = sheetIDs[j];
                    final Integer offset = sheetOffsets.get(id);
                    System.out.println("  Knows about sheet " + id);
                    System.out.println("    That sheet lives at " + offset);
                    final Record atPos = findRecordAtPos(offset);
                    System.out.println("    The record at that pos is of type " + atPos.getRecordType());
                    System.out.println("    The record at that pos has class " + atPos.getClass().getName());
                    if (!(atPos instanceof PositionDependentRecord)) {
                        System.out.println("    ** The record class isn't position aware! **");
                    }
                }
            }
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            r.writeOut(baos);
            pos += baos.size();
        }
        System.out.println("");
        pos = 0;
        for (int i = 0; i < records.length; ++i) {
            final Record r = records[i];
            if (r instanceof UserEditAtom) {
                final UserEditAtom uea = (UserEditAtom)r;
                System.out.println("Found UserEditAtom at " + pos + " (" + Integer.toHexString(pos) + ")");
                System.out.println("  lastUserEditAtomOffset = " + uea.getLastUserEditAtomOffset());
                System.out.println("  persistPointersOffset  = " + uea.getPersistPointersOffset());
                System.out.println("  docPersistRef          = " + uea.getDocPersistRef());
                System.out.println("  maxPersistWritten      = " + uea.getMaxPersistWritten());
            }
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            r.writeOut(baos);
            pos += baos.size();
        }
        System.out.println("");
        final CurrentUserAtom cua = ss.getCurrentUserAtom();
        System.out.println("Checking Current User Atom");
        System.out.println("  Thinks the CurrentEditOffset is " + cua.getCurrentEditOffset());
        System.out.println("");
    }
    
    public static Record findRecordAtPos(final int pos) {
        final long type = LittleEndian.getUShort(UserEditAndPersistListing.fileContents, pos + 2);
        final long rlen = LittleEndian.getUInt(UserEditAndPersistListing.fileContents, pos + 4);
        final Record r = Record.createRecordForType(type, UserEditAndPersistListing.fileContents, pos, (int)rlen + 8);
        return r;
    }
}
