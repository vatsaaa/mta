// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import org.apache.poi.util.LittleEndian;
import java.util.Hashtable;
import org.apache.poi.hslf.record.NotesAtom;
import org.apache.poi.hslf.record.SlideAtom;
import org.apache.poi.hslf.record.SlideListWithText;
import org.apache.poi.hslf.record.Record;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.record.PositionDependentRecord;
import org.apache.poi.hslf.record.PersistPtrHolder;
import org.apache.poi.hslf.record.Notes;
import org.apache.poi.hslf.record.Slide;
import org.apache.poi.hslf.record.SlidePersistAtom;
import org.apache.poi.hslf.record.Document;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.HSLFSlideShow;

public final class SlideIdListing
{
    private static byte[] fileContents;
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Need to give a filename");
            System.exit(1);
        }
        final HSLFSlideShow hss = new HSLFSlideShow(args[0]);
        final SlideShow ss = new SlideShow(hss);
        SlideIdListing.fileContents = hss.getUnderlyingBytes();
        final Record[] records = hss.getRecords();
        final Record[] latestRecords = ss.getMostRecentCoreRecords();
        Document document = null;
        for (int i = 0; i < latestRecords.length; ++i) {
            if (latestRecords[i] instanceof Document) {
                document = (Document)latestRecords[i];
            }
        }
        System.out.println("");
        final SlideListWithText[] slwts = document.getSlideListWithTexts();
        for (int j = 0; j < slwts.length; ++j) {
            final Record[] cr = slwts[j].getChildRecords();
            for (int k = 0; k < cr.length; ++k) {
                if (cr[k] instanceof SlidePersistAtom) {
                    final SlidePersistAtom spa = (SlidePersistAtom)cr[k];
                    System.out.println("SlidePersistAtom knows about slide:");
                    System.out.println("\t" + spa.getRefID());
                    System.out.println("\t" + spa.getSlideIdentifier());
                }
            }
        }
        System.out.println("");
        for (int j = 0; j < latestRecords.length; ++j) {
            if (latestRecords[j] instanceof Slide) {
                final Slide s = (Slide)latestRecords[j];
                final SlideAtom sa = s.getSlideAtom();
                System.out.println("Found the latest version of a slide record:");
                System.out.println("\tCore ID is " + s.getSheetId());
                System.out.println("\t(Core Records count is " + j + ")");
                System.out.println("\tDisk Position is " + s.getLastOnDiskOffset());
                System.out.println("\tMaster ID is " + sa.getMasterID());
                System.out.println("\tNotes ID is " + sa.getNotesID());
            }
        }
        System.out.println("");
        for (int j = 0; j < latestRecords.length; ++j) {
            if (latestRecords[j] instanceof Notes) {
                final Notes n = (Notes)latestRecords[j];
                final NotesAtom na = n.getNotesAtom();
                System.out.println("Found the latest version of a notes record:");
                System.out.println("\tCore ID is " + n.getSheetId());
                System.out.println("\t(Core Records count is " + j + ")");
                System.out.println("\tDisk Position is " + n.getLastOnDiskOffset());
                System.out.println("\tMatching slide is " + na.getSlideID());
            }
        }
        System.out.println("");
        int pos = 0;
        for (int l = 0; l < records.length; ++l) {
            final Record r = records[l];
            if (r.getRecordType() == 6001L) {
                System.out.println("Found PersistPtrFullBlock at " + pos + " (" + Integer.toHexString(pos) + ")");
            }
            if (r.getRecordType() == 6002L) {
                System.out.println("Found PersistPtrIncrementalBlock at " + pos + " (" + Integer.toHexString(pos) + ")");
                final PersistPtrHolder pph = (PersistPtrHolder)r;
                final int[] sheetIDs = pph.getKnownSlideIDs();
                final Hashtable sheetOffsets = pph.getSlideLocationsLookup();
                for (int m = 0; m < sheetIDs.length; ++m) {
                    final Integer id = sheetIDs[m];
                    final Integer offset = sheetOffsets.get(id);
                    System.out.println("  Knows about sheet " + id);
                    System.out.println("    That sheet lives at " + offset);
                    final Record atPos = findRecordAtPos(offset);
                    System.out.println("    The record at that pos is of type " + atPos.getRecordType());
                    System.out.println("    The record at that pos has class " + atPos.getClass().getName());
                    if (!(atPos instanceof PositionDependentRecord)) {
                        System.out.println("    ** The record class isn't position aware! **");
                    }
                }
            }
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            r.writeOut(baos);
            pos += baos.size();
        }
        System.out.println("");
    }
    
    public static Record findRecordAtPos(final int pos) {
        final long type = LittleEndian.getUShort(SlideIdListing.fileContents, pos + 2);
        final long rlen = LittleEndian.getUInt(SlideIdListing.fileContents, pos + 4);
        final Record r = Record.createRecordForType(type, SlideIdListing.fileContents, pos, (int)rlen + 8);
        return r;
    }
}
