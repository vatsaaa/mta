// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import org.apache.poi.hslf.record.NotesAtom;
import org.apache.poi.hslf.record.SlideAtom;
import org.apache.poi.hslf.record.Record;
import org.apache.poi.hslf.record.Notes;
import org.apache.poi.hslf.record.Slide;
import org.apache.poi.hslf.HSLFSlideShow;

public final class SlideAndNotesAtomListing
{
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Need to give a filename");
            System.exit(1);
        }
        final HSLFSlideShow ss = new HSLFSlideShow(args[0]);
        System.out.println("");
        final Record[] records = ss.getRecords();
        for (int i = 0; i < records.length; ++i) {
            final Record r = records[i];
            if (r instanceof Slide) {
                final Slide s = (Slide)r;
                final SlideAtom sa = s.getSlideAtom();
                System.out.println("Found Slide at " + i);
                System.out.println("  Slide's master ID is " + sa.getMasterID());
                System.out.println("  Slide's notes ID is  " + sa.getNotesID());
                System.out.println("");
            }
            if (r instanceof Notes) {
                final Notes n = (Notes)r;
                final NotesAtom na = n.getNotesAtom();
                System.out.println("Found Notes at " + i);
                System.out.println("  Notes ID is " + na.getSlideID());
                System.out.println("");
            }
        }
    }
}
