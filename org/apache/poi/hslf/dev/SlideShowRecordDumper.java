// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import java.util.Iterator;
import org.apache.poi.hslf.record.TextBytesAtom;
import org.apache.poi.hslf.record.TextCharsAtom;
import org.apache.poi.hslf.record.StyleTextPropAtom;
import org.apache.poi.hslf.record.EscherTextboxWrapper;
import org.apache.poi.ddf.EscherTextboxRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.util.HexDump;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hslf.record.Record;
import java.io.IOException;
import org.apache.poi.hslf.HSLFSlideShow;

public final class SlideShowRecordDumper
{
    private boolean optVerbose;
    private boolean optEscher;
    private HSLFSlideShow doc;
    
    public static void main(final String[] args) throws IOException {
        String filename = "";
        boolean verbose = false;
        boolean escher = false;
        int ndx;
        for (ndx = 0; ndx < args.length && args[ndx].substring(0, 1).equals("-"); ++ndx) {
            if (args[ndx].equals("-escher")) {
                escher = true;
            }
            else {
                if (!args[ndx].equals("-verbose")) {
                    printUsage();
                    return;
                }
                verbose = true;
            }
        }
        if (ndx != args.length - 1) {
            printUsage();
            return;
        }
        filename = args[ndx];
        final SlideShowRecordDumper foo = new SlideShowRecordDumper(filename, verbose, escher);
        foo.printDump();
    }
    
    public static void printUsage() {
        System.err.println("Usage: SlideShowRecordDumper [-escher] [-verbose] <filename>");
        System.err.println("Valid Options:");
        System.err.println("-escher\t\t: dump contents of escher records");
        System.err.println("-verbose\t: dump binary contents of each record");
    }
    
    public SlideShowRecordDumper(final String fileName, final boolean verbose, final boolean escher) throws IOException {
        this.optVerbose = verbose;
        this.optEscher = escher;
        this.doc = new HSLFSlideShow(fileName);
    }
    
    public void printDump() throws IOException {
        this.walkTree(0, 0, this.doc.getRecords());
    }
    
    public String makeHex(final int number, final int padding) {
        String hex;
        for (hex = Integer.toHexString(number).toUpperCase(); hex.length() < padding; hex = "0" + hex) {}
        return hex;
    }
    
    public String reverseHex(String s) {
        final StringBuffer ret = new StringBuffer();
        if (s.length() / 2 * 2 != s.length()) {
            s = "0" + s;
        }
        final char[] c = s.toCharArray();
        for (int i = c.length; i > 0; i -= 2) {
            ret.append(c[i - 2]);
            ret.append(c[i - 1]);
            if (i != 2) {
                ret.append(' ');
            }
        }
        return ret.toString();
    }
    
    public int getDiskLen(final Record r) throws IOException {
        if (r == null) {
            return 0;
        }
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        r.writeOut(baos);
        final byte[] b = baos.toByteArray();
        return b.length;
    }
    
    public String getPrintableRecordContents(final Record r) throws IOException {
        if (r == null) {
            return "<<null>>";
        }
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        r.writeOut(baos);
        final byte[] b = baos.toByteArray();
        return HexDump.dump(b, 0L, 0);
    }
    
    public String printEscherRecord(final EscherRecord er) {
        final String nl = System.getProperty("line.separator");
        final StringBuffer buf = new StringBuffer();
        if (er instanceof EscherContainerRecord) {
            buf.append(this.printEscherContainerRecord((EscherContainerRecord)er));
        }
        else if (er instanceof EscherTextboxRecord) {
            buf.append("EscherTextboxRecord:" + nl);
            final EscherTextboxWrapper etw = new EscherTextboxWrapper((EscherTextboxRecord)er);
            final Record[] children = etw.getChildRecords();
            for (int j = 0; j < children.length; ++j) {
                if (children[j] instanceof StyleTextPropAtom) {
                    if (j > 0 && (children[j - 1] instanceof TextCharsAtom || children[j - 1] instanceof TextBytesAtom)) {
                        final int size = (children[j - 1] instanceof TextCharsAtom) ? ((TextCharsAtom)children[j - 1]).getText().length() : ((TextBytesAtom)children[j - 1]).getText().length();
                        final StyleTextPropAtom tsp = (StyleTextPropAtom)children[j];
                        tsp.setParentTextSize(size);
                    }
                    else {
                        buf.append("Error! Couldn't find preceding TextAtom for style\n");
                    }
                    buf.append(children[j].toString() + nl);
                }
                else {
                    buf.append(children[j].toString() + nl);
                }
            }
        }
        else {
            buf.append(er.toString());
        }
        return buf.toString();
    }
    
    public String printEscherContainerRecord(final EscherContainerRecord ecr) {
        final String indent = "";
        final String nl = System.getProperty("line.separator");
        final StringBuffer children = new StringBuffer();
        int count = 0;
        final Iterator<EscherRecord> iterator = ecr.getChildIterator();
        while (iterator.hasNext()) {
            if (count < 1) {
                children.append("  children: " + nl);
            }
            final String newIndent = "   ";
            final EscherRecord record = iterator.next();
            children.append(newIndent + "Child " + count + ":" + nl);
            children.append(this.printEscherRecord(record));
            ++count;
        }
        return indent + ecr.getClass().getName() + " (" + ecr.getRecordName() + "):" + nl + indent + "  isContainer: " + ecr.isContainerRecord() + nl + indent + "  options: 0x" + HexDump.toHex(ecr.getOptions()) + nl + indent + "  recordId: 0x" + HexDump.toHex(ecr.getRecordId()) + nl + indent + "  numchildren: " + ecr.getChildRecords().size() + nl + indent + children.toString();
    }
    
    public void walkTree(final int depth, int pos, final Record[] records) throws IOException {
        final int indent = depth;
        String ind = "";
        for (int i = 0; i < indent; ++i) {
            ind += " ";
        }
        for (int i = 0; i < records.length; ++i) {
            final Record r = records[i];
            if (r == null) {
                System.out.println(ind + "At position " + pos + " (" + this.makeHex(pos, 6) + "):");
                System.out.println(ind + "Warning! Null record found.");
            }
            else {
                final int len = this.getDiskLen(r);
                final String hexType = this.makeHex((int)r.getRecordType(), 4);
                final String rHexType = this.reverseHex(hexType);
                final Class c = r.getClass();
                String cname = c.toString();
                if (cname.startsWith("class ")) {
                    cname = cname.substring(6);
                }
                if (cname.startsWith("org.apache.poi.hslf.record.")) {
                    cname = cname.substring(27);
                }
                System.out.println(ind + "At position " + pos + " (" + this.makeHex(pos, 6) + "):");
                System.out.println(ind + " Record is of type " + cname);
                System.out.println(ind + " Type is " + r.getRecordType() + " (" + hexType + " -> " + rHexType + " )");
                System.out.println(ind + " Len is " + (len - 8) + " (" + this.makeHex(len - 8, 8) + "), on disk len is " + len);
                if (this.optEscher && cname.equals("PPDrawing")) {
                    final DefaultEscherRecordFactory factory = new DefaultEscherRecordFactory();
                    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    r.writeOut(baos);
                    final byte[] b = baos.toByteArray();
                    final EscherRecord er = factory.createRecord(b, 0);
                    er.fillFields(b, 0, factory);
                    System.out.println(this.printEscherRecord(er));
                }
                else if (this.optVerbose && r.getChildRecords() == null) {
                    final String recData = this.getPrintableRecordContents(r);
                    System.out.println(ind + recData);
                }
                System.out.println();
                if (r.getChildRecords() != null) {
                    this.walkTree(depth + 3, pos + 8, r.getChildRecords());
                }
                pos += len;
            }
        }
    }
}
