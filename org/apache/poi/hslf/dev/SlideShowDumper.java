// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.dev;

import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherTextboxRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.hslf.record.RecordTypes;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.InputStream;

public final class SlideShowDumper
{
    private InputStream istream;
    private POIFSFileSystem filesystem;
    private byte[] _docstream;
    private boolean ddfEscher;
    private boolean basicEscher;
    
    public static void main(final String[] args) throws IOException {
        if (args.length == 0) {
            System.err.println("Useage: SlideShowDumper [-escher|-basicescher] <filename>");
            return;
        }
        String filename = args[0];
        if (args.length > 1) {
            filename = args[1];
        }
        final SlideShowDumper foo = new SlideShowDumper(filename);
        if (args.length > 1) {
            if (args[0].equalsIgnoreCase("-escher")) {
                foo.setDDFEscher(true);
            }
            else {
                foo.setBasicEscher(true);
            }
        }
        foo.printDump();
        foo.close();
    }
    
    public SlideShowDumper(final String fileName) throws IOException {
        this(new FileInputStream(fileName));
    }
    
    public SlideShowDumper(final InputStream inputStream) throws IOException {
        this(new POIFSFileSystem(inputStream));
        this.istream = inputStream;
    }
    
    public SlideShowDumper(final POIFSFileSystem filesystem) throws IOException {
        this.ddfEscher = false;
        this.basicEscher = false;
        this.filesystem = filesystem;
        final DocumentEntry docProps = (DocumentEntry)filesystem.getRoot().getEntry("PowerPoint Document");
        this._docstream = new byte[docProps.getSize()];
        filesystem.createDocumentInputStream("PowerPoint Document").read(this._docstream);
    }
    
    public void setDDFEscher(final boolean grok) {
        this.ddfEscher = grok;
        this.basicEscher = !grok;
    }
    
    public void setBasicEscher(final boolean grok) {
        this.basicEscher = grok;
        this.ddfEscher = !grok;
    }
    
    public void close() throws IOException {
        if (this.istream != null) {
            this.istream.close();
        }
        this.filesystem = null;
    }
    
    public void printDump() {
        this.walkTree(0, 0, this._docstream.length);
    }
    
    public String makeHex(final short s) {
        final String hex = Integer.toHexString(s).toUpperCase();
        if (hex.length() == 1) {
            return "0" + hex;
        }
        return hex;
    }
    
    public String makeHex(final int i) {
        final String hex = Integer.toHexString(i).toUpperCase();
        if (hex.length() == 1) {
            return "000" + hex;
        }
        if (hex.length() == 2) {
            return "00" + hex;
        }
        if (hex.length() == 3) {
            return "0" + hex;
        }
        return hex;
    }
    
    public void walkTree(final int depth, final int startPos, final int maxLen) {
        int pos = startPos;
        final int endPos = startPos + maxLen;
        final int indent = depth;
        while (pos <= endPos - 8) {
            final long type = LittleEndian.getUShort(this._docstream, pos + 2);
            final long len = LittleEndian.getUInt(this._docstream, pos + 4);
            final byte opt = this._docstream[pos];
            String ind = "";
            for (int i = 0; i < indent; ++i) {
                ind += " ";
            }
            System.out.println(ind + "At position " + pos + " (" + this.makeHex(pos) + "):");
            System.out.println(ind + "Type is " + type + " (" + this.makeHex((int)type) + "), len is " + len + " (" + this.makeHex((int)len) + ")");
            final String recordName = RecordTypes.recordName((int)type);
            pos += 8;
            if (recordName != null) {
                System.out.println(ind + "That's a " + recordName);
                int container = opt & 0xF;
                if (type == 5003L && opt == 0L) {
                    container = 15;
                }
                if (type == 0L || container != 15) {
                    System.out.println();
                }
                else if (type == 1035L || type == 1036L) {
                    System.out.println();
                    if (this.ddfEscher) {
                        this.walkEscherDDF(indent + 3, pos + 8, (int)len - 8);
                    }
                    else if (this.basicEscher) {
                        this.walkEscherBasic(indent + 3, pos + 8, (int)len - 8);
                    }
                }
                else {
                    System.out.println();
                    this.walkTree(indent + 2, pos, (int)len);
                }
            }
            else {
                System.out.println(ind + "** unknown record **");
                System.out.println();
            }
            pos += (int)len;
        }
    }
    
    public void walkEscherDDF(final int indent, int pos, int len) {
        if (len < 8) {
            return;
        }
        String ind = "";
        for (int i = 0; i < indent; ++i) {
            ind += " ";
        }
        final byte[] contents = new byte[len];
        System.arraycopy(this._docstream, pos, contents, 0, len);
        final DefaultEscherRecordFactory erf = new DefaultEscherRecordFactory();
        final EscherRecord record = erf.createRecord(contents, 0);
        record.fillFields(contents, 0, erf);
        final long atomType = LittleEndian.getUShort(contents, 2);
        final long atomLen = LittleEndian.getUShort(contents, 4);
        int recordLen = record.getRecordSize();
        System.out.println(ind + "At position " + pos + " (" + this.makeHex(pos) + "):");
        System.out.println(ind + "Type is " + atomType + " (" + this.makeHex((int)atomType) + "), len is " + atomLen + " (" + this.makeHex((int)atomLen) + ") (" + (atomLen + 8L) + ") - record claims " + recordLen);
        if (recordLen != 8 && recordLen != atomLen + 8L) {
            System.out.println(ind + "** Atom length of " + atomLen + " (" + (atomLen + 8L) + ") doesn't match record length of " + recordLen);
        }
        if (record instanceof EscherContainerRecord) {
            final EscherContainerRecord ecr = (EscherContainerRecord)record;
            System.out.println(ind + ecr.toString());
            this.walkEscherDDF(indent + 3, pos + 8, (int)atomLen);
        }
        else {
            System.out.println(ind + record.toString());
        }
        if (atomType == 61451L) {
            recordLen = (int)atomLen + 8;
        }
        if (atomType == 61453L) {
            recordLen = (int)atomLen + 8;
            record.fillFields(contents, 0, erf);
            if (!(record instanceof EscherTextboxRecord)) {
                System.out.println(ind + "** Really a msofbtClientTextbox !");
            }
        }
        if (recordLen == 8 && atomLen > 8L) {
            this.walkEscherDDF(indent + 3, pos + 8, (int)atomLen);
            pos += (int)atomLen;
            pos += 8;
            len -= (int)atomLen;
            len -= 8;
        }
        else {
            pos += (int)atomLen;
            pos += 8;
            len -= (int)atomLen;
            len -= 8;
        }
        if (len >= 8) {
            this.walkEscherDDF(indent, pos, len);
        }
    }
    
    public void walkEscherBasic(final int indent, final int pos, final int len) {
        if (len < 8) {
            return;
        }
        String ind = "";
        for (int i = 0; i < indent; ++i) {
            ind += " ";
        }
        final long type = LittleEndian.getUShort(this._docstream, pos + 2);
        final long atomlen = LittleEndian.getUInt(this._docstream, pos + 4);
        final String typeS = this.makeHex((int)type);
        System.out.println(ind + "At position " + pos + " (" + this.makeHex(pos) + "):");
        System.out.println(ind + "Type is " + type + " (" + typeS + "), len is " + atomlen + " (" + this.makeHex((int)atomlen) + ")");
        final String typeName = RecordTypes.recordName((int)type);
        if (typeName != null) {
            System.out.println(ind + "That's an Escher Record: " + typeName);
        }
        else {
            System.out.println(ind + "(Unknown Escher Record)");
        }
        if (type == 61453L) {
            System.out.print(ind);
            for (int j = 8; j < 16; ++j) {
                short bv = this._docstream[j + pos];
                if (bv < 0) {
                    bv += 256;
                }
                System.out.print(j + "=" + bv + " (" + this.makeHex(bv) + ")  ");
            }
            System.out.println("");
            System.out.print(ind);
            for (int j = 20; j < 28; ++j) {
                short bv = this._docstream[j + pos];
                if (bv < 0) {
                    bv += 256;
                }
                System.out.print(j + "=" + bv + " (" + this.makeHex(bv) + ")  ");
            }
            System.out.println("");
        }
        System.out.println("");
        if (type == 61443L || type == 61444L) {
            this.walkEscherBasic(indent + 3, pos + 8, (int)atomlen);
        }
        if (atomlen < len) {
            final int atomleni = (int)atomlen;
            this.walkEscherBasic(indent, pos + atomleni + 8, len - atomleni - 8);
        }
    }
}
