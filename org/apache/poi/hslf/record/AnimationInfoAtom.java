// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class AnimationInfoAtom extends RecordAtom
{
    public static final int Reverse = 1;
    public static final int Automatic = 4;
    public static final int Sound = 16;
    public static final int StopSound = 64;
    public static final int Play = 256;
    public static final int Synchronous = 1024;
    public static final int Hide = 4096;
    public static final int AnimateBg = 16384;
    private byte[] _header;
    private byte[] _recdata;
    
    protected AnimationInfoAtom() {
        this._recdata = new byte[28];
        LittleEndian.putShort(this._header = new byte[8], 0, (short)1);
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._recdata.length);
    }
    
    protected AnimationInfoAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._recdata = new byte[len - 8], 0, len - 8);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.AnimationInfoAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._recdata);
    }
    
    public int getDimColor() {
        return LittleEndian.getInt(this._recdata, 0);
    }
    
    public void setDimColor(final int rgb) {
        LittleEndian.putInt(this._recdata, 0, rgb);
    }
    
    public int getMask() {
        return LittleEndian.getInt(this._recdata, 4);
    }
    
    public void setMask(final int mask) {
        LittleEndian.putInt(this._recdata, 4, mask);
    }
    
    public boolean getFlag(final int bit) {
        return (this.getMask() & bit) != 0x0;
    }
    
    public void setFlag(final int bit, final boolean value) {
        int mask = this.getMask();
        if (value) {
            mask |= bit;
        }
        else {
            mask &= ~bit;
        }
        this.setMask(mask);
    }
    
    public int getSoundIdRef() {
        return LittleEndian.getInt(this._recdata, 8);
    }
    
    public void setSoundIdRef(final int id) {
        LittleEndian.putInt(this._recdata, 8, id);
    }
    
    public int getDelayTime() {
        return LittleEndian.getInt(this._recdata, 12);
    }
    
    public void setDelayTime(final int id) {
        LittleEndian.putInt(this._recdata, 12, id);
    }
    
    public int getOrderID() {
        return LittleEndian.getInt(this._recdata, 16);
    }
    
    public void setOrderID(final int id) {
        LittleEndian.putInt(this._recdata, 16, id);
    }
    
    public int getSlideCount() {
        return LittleEndian.getInt(this._recdata, 18);
    }
    
    public void setSlideCount(final int id) {
        LittleEndian.putInt(this._recdata, 18, id);
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        buf.append("AnimationInfoAtom\n");
        buf.append("\tDimColor: " + this.getDimColor() + "\n");
        final int mask = this.getMask();
        buf.append("\tMask: " + mask + ", 0x" + Integer.toHexString(mask) + "\n");
        buf.append("\t  Reverse: " + this.getFlag(1) + "\n");
        buf.append("\t  Automatic: " + this.getFlag(4) + "\n");
        buf.append("\t  Sound: " + this.getFlag(16) + "\n");
        buf.append("\t  StopSound: " + this.getFlag(64) + "\n");
        buf.append("\t  Play: " + this.getFlag(256) + "\n");
        buf.append("\t  Synchronous: " + this.getFlag(1024) + "\n");
        buf.append("\t  Hide: " + this.getFlag(4096) + "\n");
        buf.append("\t  AnimateBg: " + this.getFlag(16384) + "\n");
        buf.append("\tSoundIdRef: " + this.getSoundIdRef() + "\n");
        buf.append("\tDelayTime: " + this.getDelayTime() + "\n");
        buf.append("\tOrderID: " + this.getOrderID() + "\n");
        buf.append("\tSlideCount: " + this.getSlideCount() + "\n");
        return buf.toString();
    }
}
