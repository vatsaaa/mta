// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

public final class ExAviMovie extends ExMCIMovie
{
    protected ExAviMovie(final byte[] source, final int start, final int len) {
        super(source, start, len);
    }
    
    public ExAviMovie() {
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExAviMovie.typeID;
    }
}
