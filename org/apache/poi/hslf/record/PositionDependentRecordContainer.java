// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.util.Hashtable;

public abstract class PositionDependentRecordContainer extends RecordContainer implements PositionDependentRecord
{
    private int sheetId;
    protected int myLastOnDiskOffset;
    
    public int getSheetId() {
        return this.sheetId;
    }
    
    public void setSheetId(final int id) {
        this.sheetId = id;
    }
    
    public int getLastOnDiskOffset() {
        return this.myLastOnDiskOffset;
    }
    
    public void setLastOnDiskOffset(final int offset) {
        this.myLastOnDiskOffset = offset;
    }
    
    public void updateOtherRecordReferences(final Hashtable<Integer, Integer> oldToNewReferencesLookup) {
    }
}
