// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class UnknownRecordPlaceholder extends RecordAtom
{
    private byte[] _contents;
    private long _type;
    
    protected UnknownRecordPlaceholder(final byte[] source, final int start, int len) {
        if (len < 0) {
            len = 0;
        }
        System.arraycopy(source, start, this._contents = new byte[len], 0, len);
        this._type = LittleEndian.getUShort(this._contents, 2);
    }
    
    @Override
    public long getRecordType() {
        return this._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._contents);
    }
}
