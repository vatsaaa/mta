// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public class ExObjListAtom extends RecordAtom
{
    private byte[] _header;
    private byte[] _data;
    
    protected ExObjListAtom() {
        this._header = new byte[8];
        this._data = new byte[4];
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    protected ExObjListAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
        if (this._data.length < 4) {
            throw new IllegalArgumentException("The length of the data for a ExObjListAtom must be at least 4 bytes, but was only " + this._data.length);
        }
    }
    
    public long getObjectIDSeed() {
        return LittleEndian.getUInt(this._data, 0);
    }
    
    public void setObjectIDSeed(final int seed) {
        LittleEndian.putInt(this._data, 0, seed);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExObjListAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
}
