// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import org.apache.poi.util.HexDump;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.StringUtil;

public final class TextBytesAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private byte[] _text;
    
    public String getText() {
        return StringUtil.getFromCompressedUnicode(this._text, 0, this._text.length);
    }
    
    public void setText(final byte[] b) {
        this._text = b;
        LittleEndian.putInt(this._header, 4, this._text.length);
    }
    
    protected TextBytesAtom(final byte[] source, final int start, int len) {
        if (len < 8) {
            len = 8;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._text = new byte[len - 8], 0, len - 8);
    }
    
    public TextBytesAtom() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 0);
        LittleEndian.putUShort(this._header, 2, (int)TextBytesAtom._type);
        LittleEndian.putInt(this._header, 4, 0);
        this._text = new byte[0];
    }
    
    @Override
    public long getRecordType() {
        return TextBytesAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._text);
    }
    
    @Override
    public String toString() {
        final StringBuffer out = new StringBuffer();
        out.append("TextBytesAtom:\n");
        out.append(HexDump.dump(this._text, 0L, 0));
        return out.toString();
    }
    
    static {
        TextBytesAtom._type = 4008L;
    }
}
