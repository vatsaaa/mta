// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

public interface ParentAwareRecord
{
    RecordContainer getParentRecord();
    
    void setParentRecord(final RecordContainer p0);
}
