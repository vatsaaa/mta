// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import org.apache.poi.hslf.model.textproperties.CharFlagsTextProp;
import org.apache.poi.hslf.model.textproperties.AlignmentTextProp;
import org.apache.poi.hslf.model.textproperties.ParagraphFlagsTextProp;
import org.apache.poi.util.HexDump;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.POILogger;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import java.util.Iterator;
import org.apache.poi.hslf.model.textproperties.TextProp;
import org.apache.poi.hslf.model.textproperties.TextPropCollection;
import java.util.LinkedList;

public final class StyleTextPropAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private byte[] reserved;
    private byte[] rawContents;
    private boolean initialised;
    private LinkedList<TextPropCollection> paragraphStyles;
    private LinkedList<TextPropCollection> charStyles;
    public static TextProp[] paragraphTextPropTypes;
    public static TextProp[] characterTextPropTypes;
    
    public LinkedList<TextPropCollection> getParagraphStyles() {
        return this.paragraphStyles;
    }
    
    public void setParagraphStyles(final LinkedList<TextPropCollection> ps) {
        this.paragraphStyles = ps;
    }
    
    public LinkedList<TextPropCollection> getCharacterStyles() {
        return this.charStyles;
    }
    
    public void setCharacterStyles(final LinkedList<TextPropCollection> cs) {
        this.charStyles = cs;
    }
    
    public int getParagraphTextLengthCovered() {
        return this.getCharactersCovered(this.paragraphStyles);
    }
    
    public int getCharacterTextLengthCovered() {
        return this.getCharactersCovered(this.charStyles);
    }
    
    private int getCharactersCovered(final LinkedList<TextPropCollection> styles) {
        int length = 0;
        for (final TextPropCollection tpc : styles) {
            length += tpc.getCharactersCovered();
        }
        return length;
    }
    
    public StyleTextPropAtom(final byte[] source, final int start, int len) {
        this.initialised = false;
        if (len < 18) {
            len = 18;
            if (source.length - start < 18) {
                throw new RuntimeException("Not enough data to form a StyleTextPropAtom (min size 18 bytes long) - found " + (source.length - start));
            }
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this.rawContents = new byte[len - 8], 0, this.rawContents.length);
        this.reserved = new byte[0];
        this.paragraphStyles = new LinkedList<TextPropCollection>();
        this.charStyles = new LinkedList<TextPropCollection>();
    }
    
    public StyleTextPropAtom(final int parentTextSize) {
        this.initialised = false;
        this._header = new byte[8];
        this.rawContents = new byte[0];
        this.reserved = new byte[0];
        LittleEndian.putInt(this._header, 2, (short)StyleTextPropAtom._type);
        LittleEndian.putInt(this._header, 4, 10);
        this.paragraphStyles = new LinkedList<TextPropCollection>();
        this.charStyles = new LinkedList<TextPropCollection>();
        final TextPropCollection defaultParagraphTextProps = new TextPropCollection(parentTextSize, (short)0);
        this.paragraphStyles.add(defaultParagraphTextProps);
        final TextPropCollection defaultCharacterTextProps = new TextPropCollection(parentTextSize);
        this.charStyles.add(defaultCharacterTextProps);
        this.initialised = true;
    }
    
    @Override
    public long getRecordType() {
        return StyleTextPropAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.updateRawContents();
        final int newSize = this.rawContents.length + this.reserved.length;
        LittleEndian.putInt(this._header, 4, newSize);
        out.write(this._header);
        out.write(this.rawContents);
        out.write(this.reserved);
    }
    
    public void setParentTextSize(final int size) {
        int pos = 0;
        int textHandled = 0;
        for (int prsize = size; pos < this.rawContents.length && textHandled < prsize; ++prsize) {
            final int textLen = LittleEndian.getInt(this.rawContents, pos);
            textHandled += textLen;
            pos += 4;
            final short indent = LittleEndian.getShort(this.rawContents, pos);
            pos += 2;
            final int paraFlags = LittleEndian.getInt(this.rawContents, pos);
            pos += 4;
            final TextPropCollection thisCollection = new TextPropCollection(textLen, indent);
            final int plSize = thisCollection.buildTextPropList(paraFlags, StyleTextPropAtom.paragraphTextPropTypes, this.rawContents, pos);
            pos += plSize;
            this.paragraphStyles.add(thisCollection);
            if (pos < this.rawContents.length && textHandled == size) {}
        }
        if (this.rawContents.length > 0 && textHandled != size + 1) {
            this.logger.log(POILogger.WARN, "Problem reading paragraph style runs: textHandled = " + textHandled + ", text.size+1 = " + (size + 1));
        }
        textHandled = 0;
        for (int chsize = size; pos < this.rawContents.length && textHandled < chsize; ++chsize) {
            final int textLen2 = LittleEndian.getInt(this.rawContents, pos);
            textHandled += textLen2;
            pos += 4;
            final short no_val = -1;
            final int charFlags = LittleEndian.getInt(this.rawContents, pos);
            pos += 4;
            final TextPropCollection thisCollection2 = new TextPropCollection(textLen2, no_val);
            final int chSize = thisCollection2.buildTextPropList(charFlags, StyleTextPropAtom.characterTextPropTypes, this.rawContents, pos);
            pos += chSize;
            this.charStyles.add(thisCollection2);
            if (pos < this.rawContents.length && textHandled == size) {}
        }
        if (this.rawContents.length > 0 && textHandled != size + 1) {
            this.logger.log(POILogger.WARN, "Problem reading character style runs: textHandled = " + textHandled + ", text.size+1 = " + (size + 1));
        }
        if (pos < this.rawContents.length) {
            this.reserved = new byte[this.rawContents.length - pos];
            System.arraycopy(this.rawContents, pos, this.reserved, 0, this.reserved.length);
        }
        this.initialised = true;
    }
    
    private void updateRawContents() throws IOException {
        if (!this.initialised) {
            return;
        }
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < this.paragraphStyles.size(); ++i) {
            final TextPropCollection tpc = this.paragraphStyles.get(i);
            tpc.writeOut(baos);
        }
        for (int i = 0; i < this.charStyles.size(); ++i) {
            final TextPropCollection tpc = this.charStyles.get(i);
            tpc.writeOut(baos);
        }
        this.rawContents = baos.toByteArray();
    }
    
    public void setRawContents(final byte[] bytes) {
        this.rawContents = bytes;
        this.reserved = new byte[0];
        this.initialised = false;
    }
    
    public TextPropCollection addParagraphTextPropCollection(final int charactersCovered) {
        final TextPropCollection tpc = new TextPropCollection(charactersCovered, (short)0);
        this.paragraphStyles.add(tpc);
        return tpc;
    }
    
    public TextPropCollection addCharacterTextPropCollection(final int charactersCovered) {
        final TextPropCollection tpc = new TextPropCollection(charactersCovered);
        this.charStyles.add(tpc);
        return tpc;
    }
    
    @Override
    public String toString() {
        final StringBuffer out = new StringBuffer();
        out.append("StyleTextPropAtom:\n");
        if (!this.initialised) {
            out.append("Uninitialised, dumping Raw Style Data\n");
        }
        else {
            out.append("Paragraph properties\n");
            for (final TextPropCollection pr : this.getParagraphStyles()) {
                out.append("  chars covered: " + pr.getCharactersCovered());
                out.append("  special mask flags: 0x" + HexDump.toHex(pr.getSpecialMask()) + "\n");
                for (final TextProp p : pr.getTextPropList()) {
                    out.append("    " + p.getName() + " = " + p.getValue());
                    out.append(" (0x" + HexDump.toHex(p.getValue()) + ")\n");
                }
                out.append("  para bytes that would be written: \n");
                try {
                    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    pr.writeOut(baos);
                    final byte[] b = baos.toByteArray();
                    out.append(HexDump.dump(b, 0L, 0));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            out.append("Character properties\n");
            for (final TextPropCollection pr : this.getCharacterStyles()) {
                out.append("  chars covered: " + pr.getCharactersCovered());
                out.append("  special mask flags: 0x" + HexDump.toHex(pr.getSpecialMask()) + "\n");
                for (final TextProp p : pr.getTextPropList()) {
                    out.append("    " + p.getName() + " = " + p.getValue());
                    out.append(" (0x" + HexDump.toHex(p.getValue()) + ")\n");
                }
                out.append("  char bytes that would be written: \n");
                try {
                    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    pr.writeOut(baos);
                    final byte[] b = baos.toByteArray();
                    out.append(HexDump.dump(b, 0L, 0));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        out.append("  original byte stream \n");
        out.append(HexDump.dump(this.rawContents, 0L, 0));
        return out.toString();
    }
    
    static {
        StyleTextPropAtom._type = 4001L;
        StyleTextPropAtom.paragraphTextPropTypes = new TextProp[] { new TextProp(0, 1, "hasBullet"), new TextProp(0, 2, "hasBulletFont"), new TextProp(0, 4, "hasBulletColor"), new TextProp(0, 8, "hasBulletSize"), new ParagraphFlagsTextProp(), new TextProp(2, 128, "bullet.char"), new TextProp(2, 16, "bullet.font"), new TextProp(2, 64, "bullet.size"), new TextProp(4, 32, "bullet.color"), new AlignmentTextProp(), new TextProp(2, 256, "text.offset"), new TextProp(2, 1024, "bullet.offset"), new TextProp(2, 4096, "linespacing"), new TextProp(2, 8192, "spacebefore"), new TextProp(2, 16384, "spaceafter"), new TextProp(2, 32768, "defaultTabSize"), new TextProp(2, 1048576, "tabStops"), new TextProp(2, 65536, "fontAlign"), new TextProp(2, 655360, "wrapFlags"), new TextProp(2, 2097152, "textDirection") };
        StyleTextPropAtom.characterTextPropTypes = new TextProp[] { new TextProp(0, 1, "bold"), new TextProp(0, 2, "italic"), new TextProp(0, 4, "underline"), new TextProp(0, 8, "unused1"), new TextProp(0, 16, "shadow"), new TextProp(0, 32, "fehint"), new TextProp(0, 64, "unused2"), new TextProp(0, 128, "kumi"), new TextProp(0, 256, "unused3"), new TextProp(0, 512, "emboss"), new TextProp(0, 1024, "nibble1"), new TextProp(0, 2048, "nibble2"), new TextProp(0, 4096, "nibble3"), new TextProp(0, 8192, "nibble4"), new TextProp(0, 16384, "unused4"), new TextProp(0, 32768, "unused5"), new CharFlagsTextProp(), new TextProp(2, 65536, "font.index"), new TextProp(0, 1048576, "pp10ext"), new TextProp(2, 2097152, "asian.font.index"), new TextProp(2, 4194304, "ansi.font.index"), new TextProp(2, 8388608, "symbol.font.index"), new TextProp(2, 131072, "font.size"), new TextProp(4, 262144, "font.color"), new TextProp(2, 524288, "superscript") };
    }
}
