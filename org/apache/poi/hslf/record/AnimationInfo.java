// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;

public final class AnimationInfo extends RecordContainer
{
    private byte[] _header;
    private AnimationInfoAtom animationAtom;
    
    protected AnimationInfo(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof AnimationInfoAtom) {
            this.animationAtom = (AnimationInfoAtom)this._children[0];
        }
        else {
            this.logger.log(POILogger.ERROR, "First child record wasn't a AnimationInfoAtom, was of type " + this._children[0].getRecordType());
        }
    }
    
    public AnimationInfo() {
        (this._header = new byte[8])[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        (this._children = new Record[1])[0] = (this.animationAtom = new AnimationInfoAtom());
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.AnimationInfo.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this.getRecordType(), this._children, out);
    }
    
    public AnimationInfoAtom getAnimationInfoAtom() {
        return this.animationAtom;
    }
}
