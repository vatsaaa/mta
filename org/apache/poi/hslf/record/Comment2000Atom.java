// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.hslf.util.SystemTimeUtils;
import java.util.Date;
import org.apache.poi.util.LittleEndian;

public final class Comment2000Atom extends RecordAtom
{
    private byte[] _header;
    private byte[] _data;
    
    protected Comment2000Atom() {
        this._header = new byte[8];
        this._data = new byte[28];
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    protected Comment2000Atom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
    }
    
    public int getNumber() {
        return LittleEndian.getInt(this._data, 0);
    }
    
    public void setNumber(final int number) {
        LittleEndian.putInt(this._data, 0, number);
    }
    
    public Date getDate() {
        return SystemTimeUtils.getDate(this._data, 4);
    }
    
    public void setDate(final Date date) {
        SystemTimeUtils.storeDate(date, this._data, 4);
    }
    
    public int getXOffset() {
        return LittleEndian.getInt(this._data, 20);
    }
    
    public void setXOffset(final int xOffset) {
        LittleEndian.putInt(this._data, 20, xOffset);
    }
    
    public int getYOffset() {
        return LittleEndian.getInt(this._data, 24);
    }
    
    public void setYOffset(final int yOffset) {
        LittleEndian.putInt(this._data, 24, yOffset);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.Comment2000Atom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
}
