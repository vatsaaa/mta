// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import java.util.Vector;

public final class SlideListWithText extends RecordContainer
{
    public static final int SLIDES = 0;
    public static final int MASTER = 1;
    public static final int NOTES = 2;
    private byte[] _header;
    private static long _type;
    private SlideAtomsSet[] slideAtomsSets;
    
    protected SlideListWithText(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        final Vector<SlideAtomsSet> sets = new Vector<SlideAtomsSet>();
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof SlidePersistAtom) {
                int endPos;
                for (endPos = i + 1; endPos < this._children.length && !(this._children[endPos] instanceof SlidePersistAtom); ++endPos) {}
                final int clen = endPos - i - 1;
                boolean emptySet = false;
                if (clen == 0) {
                    emptySet = true;
                }
                final Record[] spaChildren = new Record[clen];
                System.arraycopy(this._children, i + 1, spaChildren, 0, clen);
                final SlideAtomsSet set = new SlideAtomsSet((SlidePersistAtom)this._children[i], spaChildren);
                sets.add(set);
                i += clen;
            }
        }
        this.slideAtomsSets = sets.toArray(new SlideAtomsSet[sets.size()]);
    }
    
    public SlideListWithText() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 15);
        LittleEndian.putUShort(this._header, 2, (int)SlideListWithText._type);
        LittleEndian.putInt(this._header, 4, 0);
        this._children = new Record[0];
        this.slideAtomsSets = new SlideAtomsSet[0];
    }
    
    public void addSlidePersistAtom(final SlidePersistAtom spa) {
        this.appendChildRecord(spa);
        final SlideAtomsSet newSAS = new SlideAtomsSet(spa, new Record[0]);
        final SlideAtomsSet[] sas = new SlideAtomsSet[this.slideAtomsSets.length + 1];
        System.arraycopy(this.slideAtomsSets, 0, sas, 0, this.slideAtomsSets.length);
        sas[sas.length - 1] = newSAS;
        this.slideAtomsSets = sas;
    }
    
    public int getInstance() {
        return LittleEndian.getShort(this._header, 0) >> 4;
    }
    
    public void setInstance(final int inst) {
        LittleEndian.putShort(this._header, (short)(inst << 4 | 0xF));
    }
    
    public SlideAtomsSet[] getSlideAtomsSets() {
        return this.slideAtomsSets;
    }
    
    public void setSlideAtomsSets(final SlideAtomsSet[] sas) {
        this.slideAtomsSets = sas;
    }
    
    @Override
    public long getRecordType() {
        return SlideListWithText._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], SlideListWithText._type, this._children, out);
    }
    
    static {
        SlideListWithText._type = 4080L;
    }
    
    public class SlideAtomsSet
    {
        private SlidePersistAtom slidePersistAtom;
        private Record[] slideRecords;
        
        public SlidePersistAtom getSlidePersistAtom() {
            return this.slidePersistAtom;
        }
        
        public Record[] getSlideRecords() {
            return this.slideRecords;
        }
        
        public SlideAtomsSet(final SlidePersistAtom s, final Record[] r) {
            this.slidePersistAtom = s;
            this.slideRecords = r;
        }
    }
}
