// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.util.Iterator;
import org.apache.poi.ddf.EscherBoolProperty;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherRGBProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherSpgrRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherTextboxRecord;
import org.apache.poi.util.POILogger;
import org.apache.poi.ddf.EscherRecordFactory;
import java.util.Vector;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherDgRecord;
import org.apache.poi.ddf.EscherRecord;

public final class PPDrawing extends RecordAtom
{
    private byte[] _header;
    private long _type;
    private EscherRecord[] childRecords;
    private EscherTextboxWrapper[] textboxWrappers;
    private EscherDgRecord dg;
    
    public EscherRecord[] getEscherRecords() {
        return this.childRecords;
    }
    
    public EscherTextboxWrapper[] getTextboxWrappers() {
        return this.textboxWrappers;
    }
    
    protected PPDrawing(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._type = LittleEndian.getUShort(this._header, 2);
        final byte[] contents = new byte[len];
        System.arraycopy(source, start, contents, 0, len);
        final DefaultEscherRecordFactory erf = new DefaultEscherRecordFactory();
        final Vector escherChildren = new Vector();
        this.findEscherChildren(erf, contents, 8, len - 8, escherChildren);
        this.childRecords = new EscherRecord[escherChildren.size()];
        for (int i = 0; i < this.childRecords.length; ++i) {
            this.childRecords[i] = escherChildren.get(i);
        }
        final Vector textboxes = new Vector();
        this.findEscherTextboxRecord(this.childRecords, textboxes);
        this.textboxWrappers = new EscherTextboxWrapper[textboxes.size()];
        for (int j = 0; j < this.textboxWrappers.length; ++j) {
            this.textboxWrappers[j] = textboxes.get(j);
        }
    }
    
    public PPDrawing() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 15);
        LittleEndian.putUShort(this._header, 2, RecordTypes.PPDrawing.typeID);
        LittleEndian.putInt(this._header, 4, 0);
        this.textboxWrappers = new EscherTextboxWrapper[0];
        this.create();
    }
    
    private void findEscherChildren(final DefaultEscherRecordFactory erf, final byte[] source, int startPos, int lenToGo, final Vector found) {
        final int escherBytes = LittleEndian.getInt(source, startPos + 4) + 8;
        final EscherRecord r = erf.createRecord(source, startPos);
        r.fillFields(source, startPos, erf);
        found.add(r);
        int size = r.getRecordSize();
        if (size < 8) {
            this.logger.log(POILogger.WARN, "Hit short DDF record at " + startPos + " - " + size);
        }
        if (size != escherBytes) {
            this.logger.log(POILogger.WARN, "Record length=" + escherBytes + " but getRecordSize() returned " + r.getRecordSize() + "; record: " + r.getClass());
            size = escherBytes;
        }
        startPos += size;
        lenToGo -= size;
        if (lenToGo >= 8) {
            this.findEscherChildren(erf, source, startPos, lenToGo, found);
        }
    }
    
    private void findEscherTextboxRecord(final EscherRecord[] toSearch, final Vector found) {
        for (int i = 0; i < toSearch.length; ++i) {
            if (toSearch[i] instanceof EscherTextboxRecord) {
                final EscherTextboxRecord tbr = (EscherTextboxRecord)toSearch[i];
                final EscherTextboxWrapper w = new EscherTextboxWrapper(tbr);
                found.add(w);
                for (int j = i; j >= 0; --j) {
                    if (toSearch[j] instanceof EscherSpRecord) {
                        final EscherSpRecord sp = (EscherSpRecord)toSearch[j];
                        w.setShapeId(sp.getShapeId());
                        break;
                    }
                }
            }
            else if (toSearch[i].isContainerRecord()) {
                final List<EscherRecord> childrenL = toSearch[i].getChildRecords();
                final EscherRecord[] children = new EscherRecord[childrenL.size()];
                childrenL.toArray(children);
                this.findEscherTextboxRecord(children, found);
            }
        }
    }
    
    @Override
    public long getRecordType() {
        return this._type;
    }
    
    @Override
    public Record[] getChildRecords() {
        return null;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        for (int i = 0; i < this.textboxWrappers.length; ++i) {
            this.textboxWrappers[i].writeOut(null);
        }
        int newSize = 0;
        for (int j = 0; j < this.childRecords.length; ++j) {
            newSize += this.childRecords[j].getRecordSize();
        }
        LittleEndian.putInt(this._header, 4, newSize);
        out.write(this._header);
        final byte[] b = new byte[newSize];
        int done = 0;
        for (int k = 0; k < this.childRecords.length; ++k) {
            final int written = this.childRecords[k].serialize(done, b);
            done += written;
        }
        out.write(b);
    }
    
    private void create() {
        final EscherContainerRecord dgContainer = new EscherContainerRecord();
        dgContainer.setRecordId((short)(-4094));
        dgContainer.setOptions((short)15);
        final EscherDgRecord dg = new EscherDgRecord();
        dg.setOptions((short)16);
        dg.setNumShapes(1);
        dgContainer.addChildRecord(dg);
        final EscherContainerRecord spgrContainer = new EscherContainerRecord();
        spgrContainer.setOptions((short)15);
        spgrContainer.setRecordId((short)(-4093));
        EscherContainerRecord spContainer = new EscherContainerRecord();
        spContainer.setOptions((short)15);
        spContainer.setRecordId((short)(-4092));
        final EscherSpgrRecord spgr = new EscherSpgrRecord();
        spgr.setOptions((short)1);
        spContainer.addChildRecord(spgr);
        EscherSpRecord sp = new EscherSpRecord();
        sp.setOptions((short)2);
        sp.setFlags(5);
        spContainer.addChildRecord(sp);
        spgrContainer.addChildRecord(spContainer);
        dgContainer.addChildRecord(spgrContainer);
        spContainer = new EscherContainerRecord();
        spContainer.setOptions((short)15);
        spContainer.setRecordId((short)(-4092));
        sp = new EscherSpRecord();
        sp.setOptions((short)18);
        sp.setFlags(3072);
        spContainer.addChildRecord(sp);
        final EscherOptRecord opt = new EscherOptRecord();
        opt.setRecordId((short)(-4085));
        opt.addEscherProperty(new EscherRGBProperty((short)385, 134217728));
        opt.addEscherProperty(new EscherRGBProperty((short)387, 134217733));
        opt.addEscherProperty(new EscherSimpleProperty((short)403, 10064750));
        opt.addEscherProperty(new EscherSimpleProperty((short)404, 7778750));
        opt.addEscherProperty(new EscherBoolProperty((short)447, 1179666));
        opt.addEscherProperty(new EscherBoolProperty((short)511, 524288));
        opt.addEscherProperty(new EscherSimpleProperty((short)772, 9));
        opt.addEscherProperty(new EscherSimpleProperty((short)831, 65537));
        spContainer.addChildRecord(opt);
        dgContainer.addChildRecord(spContainer);
        this.childRecords = new EscherRecord[] { dgContainer };
    }
    
    public void addTextboxWrapper(final EscherTextboxWrapper txtbox) {
        final EscherTextboxWrapper[] tw = new EscherTextboxWrapper[this.textboxWrappers.length + 1];
        System.arraycopy(this.textboxWrappers, 0, tw, 0, this.textboxWrappers.length);
        tw[this.textboxWrappers.length] = txtbox;
        this.textboxWrappers = tw;
    }
    
    public EscherDgRecord getEscherDgRecord() {
        if (this.dg == null) {
            final EscherContainerRecord dgContainer = (EscherContainerRecord)this.childRecords[0];
            final Iterator<EscherRecord> it = dgContainer.getChildIterator();
            while (it.hasNext()) {
                final EscherRecord r = it.next();
                if (r instanceof EscherDgRecord) {
                    this.dg = (EscherDgRecord)r;
                    break;
                }
            }
        }
        return this.dg;
    }
}
