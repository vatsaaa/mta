// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;

public final class HeadersFootersContainer extends RecordContainer
{
    public static final short SlideHeadersFootersContainer = 63;
    public static final short NotesHeadersFootersContainer = 79;
    public static final int USERDATEATOM = 0;
    public static final int HEADERATOM = 1;
    public static final int FOOTERATOM = 2;
    private byte[] _header;
    private HeadersFootersAtom hdAtom;
    private CString csDate;
    private CString csHeader;
    private CString csFooter;
    
    protected HeadersFootersContainer(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof HeadersFootersAtom) {
                this.hdAtom = (HeadersFootersAtom)this._children[i];
            }
            else if (this._children[i] instanceof CString) {
                final CString cs = (CString)this._children[i];
                final int opts = cs.getOptions() >> 4;
                switch (opts) {
                    case 0: {
                        this.csDate = cs;
                        break;
                    }
                    case 1: {
                        this.csHeader = cs;
                        break;
                    }
                    case 2: {
                        this.csFooter = cs;
                        break;
                    }
                    default: {
                        this.logger.log(POILogger.WARN, "Unexpected CString.Options in HeadersFootersContainer: " + opts);
                        break;
                    }
                }
            }
            else {
                this.logger.log(POILogger.WARN, "Unexpected record in HeadersFootersContainer: " + this._children[i]);
            }
        }
    }
    
    public HeadersFootersContainer(final short options) {
        LittleEndian.putShort(this._header = new byte[8], 0, options);
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        this.hdAtom = new HeadersFootersAtom();
        this._children = new Record[] { this.hdAtom };
        final CString csDate = null;
        this.csFooter = csDate;
        this.csHeader = csDate;
        this.csDate = csDate;
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.HeadersFooters.typeID;
    }
    
    public int getOptions() {
        return LittleEndian.getShort(this._header, 0);
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this.getRecordType(), this._children, out);
    }
    
    public HeadersFootersAtom getHeadersFootersAtom() {
        return this.hdAtom;
    }
    
    public CString getUserDateAtom() {
        return this.csDate;
    }
    
    public CString getHeaderAtom() {
        return this.csHeader;
    }
    
    public CString getFooterAtom() {
        return this.csFooter;
    }
    
    public CString addUserDateAtom() {
        if (this.csDate != null) {
            return this.csDate;
        }
        (this.csDate = new CString()).setOptions(0);
        this.addChildAfter(this.csDate, this.hdAtom);
        return this.csDate;
    }
    
    public CString addHeaderAtom() {
        if (this.csHeader != null) {
            return this.csHeader;
        }
        (this.csHeader = new CString()).setOptions(16);
        Record r = this.hdAtom;
        if (this.csDate != null) {
            r = this.hdAtom;
        }
        this.addChildAfter(this.csHeader, r);
        return this.csHeader;
    }
    
    public CString addFooterAtom() {
        if (this.csFooter != null) {
            return this.csFooter;
        }
        (this.csFooter = new CString()).setOptions(32);
        Record r = this.hdAtom;
        if (this.csHeader != null) {
            r = this.csHeader;
        }
        else if (this.csDate != null) {
            r = this.csDate;
        }
        this.addChildAfter(this.csFooter, r);
        return this.csFooter;
    }
}
