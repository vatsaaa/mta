// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.util.ArrayList;
import org.apache.poi.util.LittleEndian;
import java.io.IOException;
import java.io.OutputStream;

public final class TextSpecInfoAtom extends RecordAtom
{
    private byte[] _header;
    private byte[] _data;
    
    protected TextSpecInfoAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.TextSpecInfoAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
    
    public void setTextSize(final int size) {
        LittleEndian.putInt(this._data, 0, size);
    }
    
    public void reset(final int size) {
        LittleEndian.putInt(this._data = new byte[10], 0, size);
        LittleEndian.putInt(this._data, 4, 1);
        LittleEndian.putShort(this._data, 8, (short)0);
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    public int getCharactersCovered() {
        int covered = 0;
        final TextSpecInfoRun[] runs = this.getTextSpecInfoRuns();
        for (int i = 0; i < runs.length; ++i) {
            covered += runs[i].len;
        }
        return covered;
    }
    
    public TextSpecInfoRun[] getTextSpecInfoRuns() {
        final ArrayList<TextSpecInfoRun> lst = new ArrayList<TextSpecInfoRun>();
        int pos = 0;
        final int[] bits = { 1, 0, 2 };
        while (pos < this._data.length) {
            final TextSpecInfoRun run = new TextSpecInfoRun();
            run.len = LittleEndian.getInt(this._data, pos);
            pos += 4;
            run.mask = LittleEndian.getInt(this._data, pos);
            pos += 4;
            for (int i = 0; i < bits.length; ++i) {
                if ((run.mask & 1 << bits[i]) != 0x0) {
                    switch (bits[i]) {
                        case 0: {
                            run.spellInfo = LittleEndian.getShort(this._data, pos);
                            pos += 2;
                            break;
                        }
                        case 1: {
                            run.langId = LittleEndian.getShort(this._data, pos);
                            pos += 2;
                            break;
                        }
                        case 2: {
                            run.altLangId = LittleEndian.getShort(this._data, pos);
                            pos += 2;
                            break;
                        }
                    }
                }
            }
            lst.add(run);
        }
        return lst.toArray(new TextSpecInfoRun[lst.size()]);
    }
    
    public static class TextSpecInfoRun
    {
        protected int len;
        protected int mask;
        protected short spellInfo;
        protected short langId;
        protected short altLangId;
        
        public TextSpecInfoRun() {
            this.spellInfo = -1;
            this.langId = -1;
            this.altLangId = -1;
        }
        
        public short getSpellInfo() {
            return this.spellInfo;
        }
        
        public short getLangId() {
            return this.spellInfo;
        }
        
        public short getAltLangId() {
            return this.altLangId;
        }
        
        public int length() {
            return this.len;
        }
    }
}
