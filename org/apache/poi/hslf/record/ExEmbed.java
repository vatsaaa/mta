// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.LittleEndian;

public class ExEmbed extends RecordContainer
{
    private byte[] _header;
    protected RecordAtom embedAtom;
    private ExOleObjAtom oleObjAtom;
    private CString menuName;
    private CString progId;
    private CString clipboardName;
    
    protected ExEmbed(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    public ExEmbed() {
        this._header = new byte[8];
        this._children = new Record[5];
        this._header[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        final CString cs1 = new CString();
        cs1.setOptions(16);
        final CString cs2 = new CString();
        cs2.setOptions(32);
        final CString cs3 = new CString();
        cs3.setOptions(48);
        this._children[0] = new ExEmbedAtom();
        this._children[1] = new ExOleObjAtom();
        this._children[2] = cs1;
        this._children[3] = cs2;
        this._children[4] = cs3;
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof ExEmbedAtom) {
            this.embedAtom = (ExEmbedAtom)this._children[0];
        }
        else {
            this.logger.log(POILogger.ERROR, "First child record wasn't a ExEmbedAtom, was of type " + this._children[0].getRecordType());
        }
        if (this._children[1] instanceof ExOleObjAtom) {
            this.oleObjAtom = (ExOleObjAtom)this._children[1];
        }
        else {
            this.logger.log(POILogger.ERROR, "Second child record wasn't a ExOleObjAtom, was of type " + this._children[1].getRecordType());
        }
        for (int i = 2; i < this._children.length; ++i) {
            if (this._children[i] instanceof CString) {
                final CString cs = (CString)this._children[i];
                final int opts = cs.getOptions() >> 4;
                switch (opts) {
                    case 1: {
                        this.menuName = cs;
                        break;
                    }
                    case 2: {
                        this.progId = cs;
                        break;
                    }
                    case 3: {
                        this.clipboardName = cs;
                        break;
                    }
                }
            }
        }
    }
    
    public ExEmbedAtom getExEmbedAtom() {
        return (ExEmbedAtom)this.embedAtom;
    }
    
    public ExOleObjAtom getExOleObjAtom() {
        return this.oleObjAtom;
    }
    
    public String getMenuName() {
        return (this.menuName == null) ? null : this.menuName.getText();
    }
    
    public void setMenuName(final String s) {
        if (this.menuName != null) {
            this.menuName.setText(s);
        }
    }
    
    public String getProgId() {
        return (this.progId == null) ? null : this.progId.getText();
    }
    
    public void setProgId(final String s) {
        if (this.progId != null) {
            this.progId.setText(s);
        }
    }
    
    public String getClipboardName() {
        return (this.clipboardName == null) ? null : this.clipboardName.getText();
    }
    
    public void setClipboardName(final String s) {
        if (this.clipboardName != null) {
            this.clipboardName.setText(s);
        }
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExEmbed.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this.getRecordType(), this._children, out);
    }
}
