// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import org.apache.poi.hslf.model.textproperties.CharFlagsTextProp;
import org.apache.poi.hslf.model.textproperties.ParagraphFlagsTextProp;
import org.apache.poi.hslf.model.textproperties.TextProp;
import org.apache.poi.util.LittleEndian;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.hslf.model.textproperties.TextPropCollection;

public final class TxMasterStyleAtom extends RecordAtom
{
    private static final int MAX_INDENT = 5;
    private byte[] _header;
    private static long _type;
    private byte[] _data;
    private TextPropCollection[] prstyles;
    private TextPropCollection[] chstyles;
    
    protected TxMasterStyleAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, this._data.length);
        try {
            this.init();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public long getRecordType() {
        return TxMasterStyleAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
    
    public TextPropCollection[] getCharacterStyles() {
        return this.chstyles;
    }
    
    public TextPropCollection[] getParagraphStyles() {
        return this.prstyles;
    }
    
    public int getTextType() {
        return LittleEndian.getShort(this._header, 0) >> 4;
    }
    
    protected void init() {
        final int type = this.getTextType();
        int pos = 0;
        final short levels = LittleEndian.getShort(this._data, 0);
        pos += 2;
        this.prstyles = new TextPropCollection[levels];
        this.chstyles = new TextPropCollection[levels];
        for (short j = 0; j < levels; ++j) {
            if (type >= 5) {
                final short val = LittleEndian.getShort(this._data, pos);
                pos += 2;
            }
            int head = LittleEndian.getInt(this._data, pos);
            pos += 4;
            final TextPropCollection prprops = new TextPropCollection(0);
            pos += prprops.buildTextPropList(head, this.getParagraphProps(type, j), this._data, pos);
            this.prstyles[j] = prprops;
            head = LittleEndian.getInt(this._data, pos);
            pos += 4;
            final TextPropCollection chprops = new TextPropCollection(0);
            pos += chprops.buildTextPropList(head, this.getCharacterProps(type, j), this._data, pos);
            this.chstyles[j] = chprops;
        }
    }
    
    protected TextProp[] getParagraphProps(final int type, final int level) {
        if (level != 0 || type >= 5) {
            return StyleTextPropAtom.paragraphTextPropTypes;
        }
        return new TextProp[] { new ParagraphFlagsTextProp(), new TextProp(2, 128, "bullet.char"), new TextProp(2, 16, "bullet.font"), new TextProp(2, 64, "bullet.size"), new TextProp(4, 32, "bullet.color"), new TextProp(2, 3328, "alignment"), new TextProp(2, 4096, "linespacing"), new TextProp(2, 8192, "spacebefore"), new TextProp(2, 16384, "spaceafter"), new TextProp(2, 32768, "text.offset"), new TextProp(2, 65536, "bullet.offset"), new TextProp(2, 131072, "defaulttab"), new TextProp(2, 262144, "para_unknown_2"), new TextProp(2, 524288, "para_unknown_3"), new TextProp(2, 1048576, "para_unknown_4"), new TextProp(2, 2097152, "para_unknown_5") };
    }
    
    protected TextProp[] getCharacterProps(final int type, final int level) {
        if (level != 0 || type >= 5) {
            return StyleTextPropAtom.characterTextPropTypes;
        }
        return new TextProp[] { new CharFlagsTextProp(), new TextProp(2, 65536, "font.index"), new TextProp(2, 131072, "char_unknown_1"), new TextProp(4, 262144, "char_unknown_2"), new TextProp(2, 524288, "font.size"), new TextProp(2, 1048576, "char_unknown_3"), new TextProp(4, 2097152, "font.color"), new TextProp(2, 8388608, "char_unknown_4") };
    }
    
    static {
        TxMasterStyleAtom._type = 4003L;
    }
}
