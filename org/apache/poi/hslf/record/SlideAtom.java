// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class SlideAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    public static final int MASTER_SLIDE_ID = 0;
    public static final int USES_MASTER_SLIDE_ID = Integer.MIN_VALUE;
    private int masterID;
    private int notesID;
    private boolean followMasterObjects;
    private boolean followMasterScheme;
    private boolean followMasterBackground;
    private SSlideLayoutAtom layoutAtom;
    private byte[] reserved;
    
    public int getMasterID() {
        return this.masterID;
    }
    
    public void setMasterID(final int id) {
        this.masterID = id;
    }
    
    public int getNotesID() {
        return this.notesID;
    }
    
    public SSlideLayoutAtom getSSlideLayoutAtom() {
        return this.layoutAtom;
    }
    
    public void setNotesID(final int id) {
        this.notesID = id;
    }
    
    public boolean getFollowMasterObjects() {
        return this.followMasterObjects;
    }
    
    public boolean getFollowMasterScheme() {
        return this.followMasterScheme;
    }
    
    public boolean getFollowMasterBackground() {
        return this.followMasterBackground;
    }
    
    public void setFollowMasterObjects(final boolean flag) {
        this.followMasterObjects = flag;
    }
    
    public void setFollowMasterScheme(final boolean flag) {
        this.followMasterScheme = flag;
    }
    
    public void setFollowMasterBackground(final boolean flag) {
        this.followMasterBackground = flag;
    }
    
    protected SlideAtom(final byte[] source, final int start, int len) {
        if (len < 30) {
            len = 30;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        final byte[] SSlideLayoutAtomData = new byte[12];
        System.arraycopy(source, start + 8, SSlideLayoutAtomData, 0, 12);
        this.layoutAtom = new SSlideLayoutAtom(SSlideLayoutAtomData);
        this.masterID = LittleEndian.getInt(source, start + 12 + 8);
        this.notesID = LittleEndian.getInt(source, start + 16 + 8);
        final int flags = LittleEndian.getUShort(source, start + 20 + 8);
        if ((flags & 0x4) == 0x4) {
            this.followMasterBackground = true;
        }
        else {
            this.followMasterBackground = false;
        }
        if ((flags & 0x2) == 0x2) {
            this.followMasterScheme = true;
        }
        else {
            this.followMasterScheme = false;
        }
        if ((flags & 0x1) == 0x1) {
            this.followMasterObjects = true;
        }
        else {
            this.followMasterObjects = false;
        }
        System.arraycopy(source, start + 30, this.reserved = new byte[len - 30], 0, this.reserved.length);
    }
    
    public SlideAtom() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 2);
        LittleEndian.putUShort(this._header, 2, (int)SlideAtom._type);
        LittleEndian.putInt(this._header, 4, 24);
        final byte[] ssdate = new byte[12];
        (this.layoutAtom = new SSlideLayoutAtom(ssdate)).setGeometryType(16);
        this.followMasterObjects = true;
        this.followMasterScheme = true;
        this.followMasterBackground = true;
        this.masterID = Integer.MIN_VALUE;
        this.notesID = 0;
        this.reserved = new byte[2];
    }
    
    @Override
    public long getRecordType() {
        return SlideAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        this.layoutAtom.writeOut(out);
        Record.writeLittleEndian(this.masterID, out);
        Record.writeLittleEndian(this.notesID, out);
        short flags = 0;
        if (this.followMasterObjects) {
            ++flags;
        }
        if (this.followMasterScheme) {
            flags += 2;
        }
        if (this.followMasterBackground) {
            flags += 4;
        }
        Record.writeLittleEndian(flags, out);
        out.write(this.reserved);
    }
    
    static {
        SlideAtom._type = 1007L;
    }
    
    public class SSlideLayoutAtom
    {
        public static final int TITLE_SLIDE = 0;
        public static final int TITLE_BODY_SLIDE = 1;
        public static final int TITLE_MASTER_SLIDE = 2;
        public static final int MASTER_SLIDE = 3;
        public static final int MASTER_NOTES = 4;
        public static final int NOTES_TITLE_BODY = 5;
        public static final int HANDOUT = 6;
        public static final int TITLE_ONLY = 7;
        public static final int TITLE_2_COLUMN_BODY = 8;
        public static final int TITLE_2_ROW_BODY = 9;
        public static final int TITLE_2_COLUNM_RIGHT_2_ROW_BODY = 10;
        public static final int TITLE_2_COLUNM_LEFT_2_ROW_BODY = 11;
        public static final int TITLE_2_ROW_BOTTOM_2_COLUMN_BODY = 12;
        public static final int TITLE_2_ROW_TOP_2_COLUMN_BODY = 13;
        public static final int FOUR_OBJECTS = 14;
        public static final int BIG_OBJECT = 15;
        public static final int BLANK_SLIDE = 16;
        public static final int VERTICAL_TITLE_BODY_LEFT = 17;
        public static final int VERTICAL_TITLE_2_ROW_BODY_LEFT = 17;
        private int geometry;
        private byte[] placeholderIDs;
        
        public int getGeometryType() {
            return this.geometry;
        }
        
        public void setGeometryType(final int geom) {
            this.geometry = geom;
        }
        
        public SSlideLayoutAtom(final byte[] data) {
            if (data.length != 12) {
                throw new RuntimeException("SSlideLayoutAtom created with byte array not 12 bytes long - was " + data.length + " bytes in size");
            }
            this.geometry = LittleEndian.getInt(data, 0);
            System.arraycopy(data, 4, this.placeholderIDs = new byte[8], 0, 8);
        }
        
        public void writeOut(final OutputStream out) throws IOException {
            Record.writeLittleEndian(this.geometry, out);
            out.write(this.placeholderIDs);
        }
    }
}
