// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;

public final class Environment extends PositionDependentRecordContainer
{
    private byte[] _header;
    private static long _type;
    private FontCollection fontCollection;
    private TxMasterStyleAtom txmaster;
    
    public FontCollection getFontCollection() {
        return this.fontCollection;
    }
    
    protected Environment(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof FontCollection) {
                this.fontCollection = (FontCollection)this._children[i];
            }
            else if (this._children[i] instanceof TxMasterStyleAtom) {
                this.txmaster = (TxMasterStyleAtom)this._children[i];
            }
        }
        if (this.fontCollection == null) {
            throw new IllegalStateException("Environment didn't contain a FontCollection record!");
        }
    }
    
    public TxMasterStyleAtom getTxMasterStyleAtom() {
        return this.txmaster;
    }
    
    @Override
    public long getRecordType() {
        return Environment._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], Environment._type, this._children, out);
    }
    
    static {
        Environment._type = 1010L;
    }
}
