// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;

public class ExHyperlink extends RecordContainer
{
    private byte[] _header;
    private static long _type;
    private ExHyperlinkAtom linkAtom;
    private CString linkDetailsA;
    private CString linkDetailsB;
    
    public ExHyperlinkAtom getExHyperlinkAtom() {
        return this.linkAtom;
    }
    
    public String getLinkURL() {
        return (this.linkDetailsB == null) ? null : this.linkDetailsB.getText();
    }
    
    public String getLinkTitle() {
        return (this.linkDetailsA == null) ? null : this.linkDetailsA.getText();
    }
    
    public void setLinkURL(final String url) {
        if (this.linkDetailsB != null) {
            this.linkDetailsB.setText(url);
        }
    }
    
    public void setLinkTitle(final String title) {
        if (this.linkDetailsA != null) {
            this.linkDetailsA.setText(title);
        }
    }
    
    public String _getDetailsA() {
        return (this.linkDetailsA == null) ? null : this.linkDetailsA.getText();
    }
    
    public String _getDetailsB() {
        return (this.linkDetailsB == null) ? null : this.linkDetailsB.getText();
    }
    
    protected ExHyperlink(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof ExHyperlinkAtom) {
            this.linkAtom = (ExHyperlinkAtom)this._children[0];
        }
        else {
            this.logger.log(POILogger.ERROR, "First child record wasn't a ExHyperlinkAtom, was of type " + this._children[0].getRecordType());
        }
        for (int i = 1; i < this._children.length; ++i) {
            if (this._children[i] instanceof CString) {
                if (this.linkDetailsA == null) {
                    this.linkDetailsA = (CString)this._children[i];
                }
                else {
                    this.linkDetailsB = (CString)this._children[i];
                }
            }
            else {
                this.logger.log(POILogger.ERROR, "Record after ExHyperlinkAtom wasn't a CString, was of type " + this._children[1].getRecordType());
            }
        }
    }
    
    public ExHyperlink() {
        this._header = new byte[8];
        this._children = new Record[3];
        this._header[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)ExHyperlink._type);
        final CString csa = new CString();
        final CString csb = new CString();
        csa.setOptions(0);
        csb.setOptions(16);
        this._children[0] = new ExHyperlinkAtom();
        this._children[1] = csa;
        this._children[2] = csb;
        this.findInterestingChildren();
    }
    
    @Override
    public long getRecordType() {
        return ExHyperlink._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], ExHyperlink._type, this._children, out);
    }
    
    static {
        ExHyperlink._type = 4055L;
    }
}
