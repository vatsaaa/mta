// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class OEPlaceholderAtom extends RecordAtom
{
    public static final int PLACEHOLDER_FULLSIZE = 0;
    public static final int PLACEHOLDER_HALFSIZE = 1;
    public static final int PLACEHOLDER_QUARTSIZE = 2;
    public static final byte None = 0;
    public static final byte MasterTitle = 1;
    public static final byte MasterBody = 2;
    public static final byte MasterCenteredTitle = 3;
    public static final byte MasterSubTitle = 4;
    public static final byte MasterNotesSlideImage = 5;
    public static final byte MasterNotesBody = 6;
    public static final byte MasterDate = 7;
    public static final byte MasterSlideNumber = 8;
    public static final byte MasterFooter = 9;
    public static final byte MasterHeader = 10;
    public static final byte NotesSlideImage = 11;
    public static final byte NotesBody = 12;
    public static final byte Title = 13;
    public static final byte Body = 14;
    public static final byte CenteredTitle = 15;
    public static final byte Subtitle = 16;
    public static final byte VerticalTextTitle = 17;
    public static final byte VerticalTextBody = 18;
    public static final byte Object = 19;
    public static final byte Graph = 20;
    public static final byte Table = 21;
    public static final byte ClipArt = 22;
    public static final byte OrganizationChart = 23;
    public static final byte MediaClip = 24;
    private byte[] _header;
    private int placementId;
    private int placeholderId;
    private int placeholderSize;
    
    public OEPlaceholderAtom() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 0);
        LittleEndian.putUShort(this._header, 2, (int)this.getRecordType());
        LittleEndian.putInt(this._header, 4, 8);
        this.placementId = 0;
        this.placeholderId = 0;
        this.placeholderSize = 0;
    }
    
    protected OEPlaceholderAtom(final byte[] source, final int start, final int len) {
        this._header = new byte[8];
        int offset = start;
        System.arraycopy(source, start, this._header, 0, 8);
        offset += this._header.length;
        this.placementId = LittleEndian.getInt(source, offset);
        offset += 4;
        this.placeholderId = LittleEndian.getUnsignedByte(source, offset);
        ++offset;
        this.placeholderSize = LittleEndian.getUnsignedByte(source, offset);
        ++offset;
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.OEPlaceholderAtom.typeID;
    }
    
    public int getPlacementId() {
        return this.placementId;
    }
    
    public void setPlacementId(final int id) {
        this.placementId = id;
    }
    
    public int getPlaceholderId() {
        return this.placeholderId;
    }
    
    public void setPlaceholderId(final byte id) {
        this.placeholderId = id;
    }
    
    public int getPlaceholderSize() {
        return this.placeholderSize;
    }
    
    public void setPlaceholderSize(final byte size) {
        this.placeholderSize = size;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        final byte[] recdata = new byte[8];
        LittleEndian.putInt(recdata, 0, this.placementId);
        recdata[4] = (byte)this.placeholderId;
        recdata[5] = (byte)this.placeholderSize;
        out.write(recdata);
    }
}
