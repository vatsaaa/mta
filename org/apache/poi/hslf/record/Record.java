// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.apache.poi.hslf.exceptions.CorruptPowerPointFileException;
import java.util.ArrayList;
import org.apache.poi.util.LittleEndian;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;

public abstract class Record
{
    protected POILogger logger;
    
    public Record() {
        this.logger = POILogFactory.getLogger(this.getClass());
    }
    
    public abstract boolean isAnAtom();
    
    public abstract long getRecordType();
    
    public abstract Record[] getChildRecords();
    
    public abstract void writeOut(final OutputStream p0) throws IOException;
    
    public static void writeLittleEndian(final int i, final OutputStream o) throws IOException {
        final byte[] bi = new byte[4];
        LittleEndian.putInt(bi, i);
        o.write(bi);
    }
    
    public static void writeLittleEndian(final short s, final OutputStream o) throws IOException {
        final byte[] bs = new byte[2];
        LittleEndian.putShort(bs, s);
        o.write(bs);
    }
    
    public static Record buildRecordAtOffset(final byte[] b, final int offset) {
        final long type = LittleEndian.getUShort(b, offset + 2);
        final long rlen = LittleEndian.getUInt(b, offset + 4);
        int rleni = (int)rlen;
        if (rleni < 0) {
            rleni = 0;
        }
        return createRecordForType(type, b, offset, 8 + rleni);
    }
    
    public static Record[] findChildRecords(final byte[] b, final int start, final int len) {
        final List<Record> children = new ArrayList<Record>(5);
        int rleni;
        for (int pos = start; pos <= start + len - 8; pos += 8, pos += rleni) {
            final long type = LittleEndian.getUShort(b, pos + 2);
            final long rlen = LittleEndian.getUInt(b, pos + 4);
            rleni = (int)rlen;
            if (rleni < 0) {
                rleni = 0;
            }
            if (pos == 0 && type == 0L && rleni == 65535) {
                throw new CorruptPowerPointFileException("Corrupt document - starts with record of type 0000 and length 0xFFFF");
            }
            final Record r = createRecordForType(type, b, pos, 8 + rleni);
            if (r != null) {
                children.add(r);
            }
        }
        final Record[] cRecords = children.toArray(new Record[children.size()]);
        return cRecords;
    }
    
    public static Record createRecordForType(final long type, final byte[] b, final int start, final int len) {
        Record toReturn = null;
        if (start + len > b.length) {
            System.err.println("Warning: Skipping record of type " + type + " at position " + start + " which claims to be longer than the file! (" + len + " vs " + (b.length - start) + ")");
            return null;
        }
        Class<? extends Record> c = null;
        try {
            c = RecordTypes.recordHandlingClass((int)type);
            if (c == null) {
                c = RecordTypes.recordHandlingClass(RecordTypes.Unknown.typeID);
            }
            final Constructor<? extends Record> con = c.getDeclaredConstructor(byte[].class, Integer.TYPE, Integer.TYPE);
            toReturn = (Record)con.newInstance(b, start, len);
        }
        catch (InstantiationException ie) {
            throw new RuntimeException("Couldn't instantiate the class for type with id " + type + " on class " + c + " : " + ie, ie);
        }
        catch (InvocationTargetException ite) {
            throw new RuntimeException("Couldn't instantiate the class for type with id " + type + " on class " + c + " : " + ite + "\nCause was : " + ite.getCause(), ite);
        }
        catch (IllegalAccessException iae) {
            throw new RuntimeException("Couldn't access the constructor for type with id " + type + " on class " + c + " : " + iae, iae);
        }
        catch (NoSuchMethodException nsme) {
            throw new RuntimeException("Couldn't access the constructor for type with id " + type + " on class " + c + " : " + nsme, nsme);
        }
        if (toReturn instanceof PositionDependentRecord) {
            final PositionDependentRecord pdr = (PositionDependentRecord)toReturn;
            pdr.setLastOnDiskOffset(start);
        }
        return toReturn;
    }
}
