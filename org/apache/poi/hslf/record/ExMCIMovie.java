// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.LittleEndian;

public class ExMCIMovie extends RecordContainer
{
    private byte[] _header;
    private ExVideoContainer exVideo;
    
    protected ExMCIMovie(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    public ExMCIMovie() {
        (this._header = new byte[8])[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        this.exVideo = new ExVideoContainer();
        this._children = new Record[] { this.exVideo };
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof ExVideoContainer) {
            this.exVideo = (ExVideoContainer)this._children[0];
        }
        else {
            this.logger.log(POILogger.ERROR, "First child record wasn't a ExVideoContainer, was of type " + this._children[0].getRecordType());
        }
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExMCIMovie.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this.getRecordType(), this._children, out);
    }
    
    public ExVideoContainer getExVideo() {
        return this.exVideo;
    }
}
