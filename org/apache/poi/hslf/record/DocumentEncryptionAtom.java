// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.StringUtil;

public final class DocumentEncryptionAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private byte[] data;
    private String encryptionProviderName;
    
    protected DocumentEncryptionAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this.data = new byte[len - 8], 0, len - 8);
        int endPos = -1;
        for (int pos = start + 8 + 44; pos < start + len && endPos < 0; pos += 2) {
            if (source[pos] == 0 && source[pos + 1] == 0) {
                endPos = pos;
            }
        }
        int pos = start + 8 + 44;
        final int stringLen = (endPos - pos) / 2;
        this.encryptionProviderName = StringUtil.getFromUnicodeLE(source, pos, stringLen);
    }
    
    public int getKeyLength() {
        return this.data[28];
    }
    
    public String getEncryptionProviderName() {
        return this.encryptionProviderName;
    }
    
    @Override
    public long getRecordType() {
        return DocumentEncryptionAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this.data);
    }
    
    static {
        DocumentEncryptionAtom._type = 12052L;
    }
}
