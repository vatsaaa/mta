// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class ExControlAtom extends RecordAtom
{
    private byte[] _header;
    private int _id;
    
    protected ExControlAtom() {
        LittleEndian.putShort(this._header = new byte[8], 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, 4);
    }
    
    protected ExControlAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._id = LittleEndian.getInt(source, start + 8);
    }
    
    public int getSlideId() {
        return this._id;
    }
    
    public void setSlideId(final int id) {
        this._id = id;
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExControlAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        final byte[] data = new byte[4];
        LittleEndian.putInt(data, this._id);
        out.write(data);
    }
}
