// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;

public final class RoundTripHFPlaceholder12 extends RecordAtom
{
    private byte[] _header;
    private byte _placeholderId;
    
    protected RoundTripHFPlaceholder12(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._placeholderId = source[start + 8];
    }
    
    public int getPlaceholderId() {
        return this._placeholderId;
    }
    
    public void setPlaceholderId(final int number) {
        this._placeholderId = (byte)number;
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.RoundTripHFPlaceholder12.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._placeholderId);
    }
}
