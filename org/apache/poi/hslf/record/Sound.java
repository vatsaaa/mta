// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.POILogger;

public final class Sound extends RecordContainer
{
    private byte[] _header;
    private CString _name;
    private CString _type;
    private SoundData _data;
    
    protected Sound(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof CString) {
            this._name = (CString)this._children[0];
        }
        else {
            this.logger.log(POILogger.ERROR, "First child record wasn't a CString, was of type " + this._children[0].getRecordType());
        }
        if (this._children[1] instanceof CString) {
            this._type = (CString)this._children[1];
        }
        else {
            this.logger.log(POILogger.ERROR, "Second child record wasn't a CString, was of type " + this._children[1].getRecordType());
        }
        for (int i = 2; i < this._children.length; ++i) {
            if (this._children[i] instanceof SoundData) {
                this._data = (SoundData)this._children[i];
                break;
            }
        }
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.Sound.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this.getRecordType(), this._children, out);
    }
    
    public String getSoundName() {
        return this._name.getText();
    }
    
    public String getSoundType() {
        return this._type.getText();
    }
    
    public byte[] getSoundData() {
        return (byte[])((this._data == null) ? null : this._data.getData());
    }
}
