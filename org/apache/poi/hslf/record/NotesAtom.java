// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class NotesAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private int slideID;
    private boolean followMasterObjects;
    private boolean followMasterScheme;
    private boolean followMasterBackground;
    private byte[] reserved;
    
    public int getSlideID() {
        return this.slideID;
    }
    
    public void setSlideID(final int id) {
        this.slideID = id;
    }
    
    public boolean getFollowMasterObjects() {
        return this.followMasterObjects;
    }
    
    public boolean getFollowMasterScheme() {
        return this.followMasterScheme;
    }
    
    public boolean getFollowMasterBackground() {
        return this.followMasterBackground;
    }
    
    public void setFollowMasterObjects(final boolean flag) {
        this.followMasterObjects = flag;
    }
    
    public void setFollowMasterScheme(final boolean flag) {
        this.followMasterScheme = flag;
    }
    
    public void setFollowMasterBackground(final boolean flag) {
        this.followMasterBackground = flag;
    }
    
    protected NotesAtom(final byte[] source, final int start, int len) {
        if (len < 8) {
            len = 8;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this.slideID = LittleEndian.getInt(source, start + 8);
        final int flags = LittleEndian.getUShort(source, start + 12);
        if ((flags & 0x4) == 0x4) {
            this.followMasterBackground = true;
        }
        else {
            this.followMasterBackground = false;
        }
        if ((flags & 0x2) == 0x2) {
            this.followMasterScheme = true;
        }
        else {
            this.followMasterScheme = false;
        }
        if ((flags & 0x1) == 0x1) {
            this.followMasterObjects = true;
        }
        else {
            this.followMasterObjects = false;
        }
        System.arraycopy(source, start + 14, this.reserved = new byte[len - 14], 0, this.reserved.length);
    }
    
    @Override
    public long getRecordType() {
        return NotesAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        Record.writeLittleEndian(this.slideID, out);
        short flags = 0;
        if (this.followMasterObjects) {
            ++flags;
        }
        if (this.followMasterScheme) {
            flags += 2;
        }
        if (this.followMasterBackground) {
            flags += 4;
        }
        Record.writeLittleEndian(flags, out);
        out.write(this.reserved);
    }
    
    static {
        NotesAtom._type = 1009L;
    }
}
