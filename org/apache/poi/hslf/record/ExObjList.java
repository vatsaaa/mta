// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;

public class ExObjList extends RecordContainer
{
    private byte[] _header;
    private static long _type;
    private ExObjListAtom exObjListAtom;
    
    public ExObjListAtom getExObjListAtom() {
        return this.exObjListAtom;
    }
    
    public ExHyperlink[] getExHyperlinks() {
        final ArrayList<ExHyperlink> links = new ArrayList<ExHyperlink>();
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof ExHyperlink) {
                links.add((ExHyperlink)this._children[i]);
            }
        }
        return links.toArray(new ExHyperlink[links.size()]);
    }
    
    protected ExObjList(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof ExObjListAtom) {
            this.exObjListAtom = (ExObjListAtom)this._children[0];
            return;
        }
        throw new IllegalStateException("First child record wasn't a ExObjListAtom, was of type " + this._children[0].getRecordType());
    }
    
    public ExObjList() {
        this._header = new byte[8];
        this._children = new Record[1];
        this._header[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)ExObjList._type);
        this._children[0] = new ExObjListAtom();
        this.findInterestingChildren();
    }
    
    @Override
    public long getRecordType() {
        return ExObjList._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], ExObjList._type, this._children, out);
    }
    
    public ExHyperlink get(final int id) {
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof ExHyperlink) {
                final ExHyperlink rec = (ExHyperlink)this._children[i];
                if (rec.getExHyperlinkAtom().getNumber() == id) {
                    return rec;
                }
            }
        }
        return null;
    }
    
    static {
        ExObjList._type = 1033L;
    }
}
