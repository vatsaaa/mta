// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public class InteractiveInfo extends RecordContainer
{
    private byte[] _header;
    private static long _type;
    private InteractiveInfoAtom infoAtom;
    
    public InteractiveInfoAtom getInteractiveInfoAtom() {
        return this.infoAtom;
    }
    
    protected InteractiveInfo(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof InteractiveInfoAtom) {
            this.infoAtom = (InteractiveInfoAtom)this._children[0];
            return;
        }
        throw new IllegalStateException("First child record wasn't a InteractiveInfoAtom, was of type " + this._children[0].getRecordType());
    }
    
    public InteractiveInfo() {
        this._header = new byte[8];
        this._children = new Record[1];
        this._header[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)InteractiveInfo._type);
        this.infoAtom = new InteractiveInfoAtom();
        this._children[0] = this.infoAtom;
    }
    
    @Override
    public long getRecordType() {
        return InteractiveInfo._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], InteractiveInfo._type, this._children, out);
    }
    
    static {
        InteractiveInfo._type = 4082L;
    }
}
