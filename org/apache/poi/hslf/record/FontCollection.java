// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.POILogger;
import java.util.ArrayList;
import java.util.List;

public final class FontCollection extends RecordContainer
{
    private List<String> fonts;
    private byte[] _header;
    
    protected FontCollection(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.fonts = new ArrayList<String>();
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof FontEntityAtom) {
                final FontEntityAtom atom = (FontEntityAtom)this._children[i];
                this.fonts.add(atom.getFontName());
            }
            else {
                this.logger.log(POILogger.WARN, "Warning: FontCollection child wasn't a FontEntityAtom, was " + this._children[i]);
            }
        }
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.FontCollection.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this.getRecordType(), this._children, out);
    }
    
    public int addFont(final String name) {
        final int idx = this.getFontIndex(name);
        if (idx != -1) {
            return idx;
        }
        return this.addFont(name, 0, 0, 4, 34);
    }
    
    public int addFont(final String name, final int charset, final int flags, final int type, final int pitch) {
        final FontEntityAtom fnt = new FontEntityAtom();
        fnt.setFontIndex(this.fonts.size() << 4);
        fnt.setFontName(name);
        fnt.setCharSet(charset);
        fnt.setFontFlags(flags);
        fnt.setFontType(type);
        fnt.setPitchAndFamily(pitch);
        this.fonts.add(name);
        this.appendChildRecord(fnt);
        return this.fonts.size() - 1;
    }
    
    public int getFontIndex(final String name) {
        for (int i = 0; i < this.fonts.size(); ++i) {
            if (this.fonts.get(i).equals(name)) {
                return i;
            }
        }
        return -1;
    }
    
    public int getNumberOfFonts() {
        return this.fonts.size();
    }
    
    public String getFontWithId(final int id) {
        if (id >= this.fonts.size()) {
            return null;
        }
        return this.fonts.get(id);
    }
}
