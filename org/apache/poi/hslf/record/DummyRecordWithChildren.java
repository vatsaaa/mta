// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class DummyRecordWithChildren extends RecordContainer
{
    private byte[] _header;
    private long _type;
    
    protected DummyRecordWithChildren(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._type = LittleEndian.getUShort(this._header, 2);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
    }
    
    @Override
    public long getRecordType() {
        return this._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this._type, this._children, out);
    }
}
