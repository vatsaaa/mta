// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;

public final class Comment2000 extends RecordContainer
{
    private byte[] _header;
    private static long _type;
    private CString authorRecord;
    private CString authorInitialsRecord;
    private CString commentRecord;
    private Comment2000Atom commentAtom;
    
    public Comment2000Atom getComment2000Atom() {
        return this.commentAtom;
    }
    
    public String getAuthor() {
        return (this.authorRecord == null) ? null : this.authorRecord.getText();
    }
    
    public void setAuthor(final String author) {
        this.authorRecord.setText(author);
    }
    
    public String getAuthorInitials() {
        return (this.authorInitialsRecord == null) ? null : this.authorInitialsRecord.getText();
    }
    
    public void setAuthorInitials(final String initials) {
        this.authorInitialsRecord.setText(initials);
    }
    
    public String getText() {
        return (this.commentRecord == null) ? null : this.commentRecord.getText();
    }
    
    public void setText(final String text) {
        this.commentRecord.setText(text);
    }
    
    protected Comment2000(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        for (final Record r : this._children) {
            if (r instanceof CString) {
                final CString cs = (CString)r;
                final int recInstance = cs.getOptions() >> 4;
                switch (recInstance) {
                    case 0: {
                        this.authorRecord = cs;
                        break;
                    }
                    case 1: {
                        this.commentRecord = cs;
                        break;
                    }
                    case 2: {
                        this.authorInitialsRecord = cs;
                        break;
                    }
                }
            }
            else if (r instanceof Comment2000Atom) {
                this.commentAtom = (Comment2000Atom)r;
            }
            else {
                this.logger.log(POILogger.WARN, "Unexpected record with type=" + r.getRecordType() + " in Comment2000: " + r.getClass().getName());
            }
        }
    }
    
    public Comment2000() {
        this._header = new byte[8];
        this._children = new Record[4];
        this._header[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)Comment2000._type);
        final CString csa = new CString();
        final CString csb = new CString();
        final CString csc = new CString();
        csa.setOptions(0);
        csb.setOptions(16);
        csc.setOptions(32);
        this._children[0] = csa;
        this._children[1] = csb;
        this._children[2] = csc;
        this._children[3] = new Comment2000Atom();
        this.findInterestingChildren();
    }
    
    @Override
    public long getRecordType() {
        return Comment2000._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], Comment2000._type, this._children, out);
    }
    
    static {
        Comment2000._type = 12000L;
    }
}
