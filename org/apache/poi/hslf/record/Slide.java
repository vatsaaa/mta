// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class Slide extends SheetContainer
{
    private byte[] _header;
    private static long _type;
    private SlideAtom slideAtom;
    private PPDrawing ppDrawing;
    private ColorSchemeAtom _colorScheme;
    
    public SlideAtom getSlideAtom() {
        return this.slideAtom;
    }
    
    @Override
    public PPDrawing getPPDrawing() {
        return this.ppDrawing;
    }
    
    protected Slide(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof SlideAtom) {
                this.slideAtom = (SlideAtom)this._children[i];
            }
            else if (this._children[i] instanceof PPDrawing) {
                this.ppDrawing = (PPDrawing)this._children[i];
            }
            if (this.ppDrawing != null && this._children[i] instanceof ColorSchemeAtom) {
                this._colorScheme = (ColorSchemeAtom)this._children[i];
            }
        }
    }
    
    public Slide() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 15);
        LittleEndian.putUShort(this._header, 2, (int)Slide._type);
        LittleEndian.putInt(this._header, 4, 0);
        this.slideAtom = new SlideAtom();
        this.ppDrawing = new PPDrawing();
        final ColorSchemeAtom colorAtom = new ColorSchemeAtom();
        this._children = new Record[] { this.slideAtom, this.ppDrawing, colorAtom };
    }
    
    @Override
    public long getRecordType() {
        return Slide._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], Slide._type, this._children, out);
    }
    
    @Override
    public ColorSchemeAtom getColorScheme() {
        return this._colorScheme;
    }
    
    static {
        Slide._type = 1006L;
    }
}
