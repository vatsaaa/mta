// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import org.apache.poi.util.POILogger;

public final class Document extends PositionDependentRecordContainer
{
    private byte[] _header;
    private static long _type;
    private DocumentAtom documentAtom;
    private Environment environment;
    private PPDrawingGroup ppDrawing;
    private SlideListWithText[] slwts;
    private ExObjList exObjList;
    
    public DocumentAtom getDocumentAtom() {
        return this.documentAtom;
    }
    
    public Environment getEnvironment() {
        return this.environment;
    }
    
    public PPDrawingGroup getPPDrawingGroup() {
        return this.ppDrawing;
    }
    
    public ExObjList getExObjList() {
        return this.exObjList;
    }
    
    public SlideListWithText[] getSlideListWithTexts() {
        return this.slwts;
    }
    
    public SlideListWithText getMasterSlideListWithText() {
        for (int i = 0; i < this.slwts.length; ++i) {
            if (this.slwts[i].getInstance() == 1) {
                return this.slwts[i];
            }
        }
        return null;
    }
    
    public SlideListWithText getSlideSlideListWithText() {
        for (int i = 0; i < this.slwts.length; ++i) {
            if (this.slwts[i].getInstance() == 0) {
                return this.slwts[i];
            }
        }
        return null;
    }
    
    public SlideListWithText getNotesSlideListWithText() {
        for (int i = 0; i < this.slwts.length; ++i) {
            if (this.slwts[i].getInstance() == 2) {
                return this.slwts[i];
            }
        }
        return null;
    }
    
    protected Document(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        if (!(this._children[0] instanceof DocumentAtom)) {
            throw new IllegalStateException("The first child of a Document must be a DocumentAtom");
        }
        this.documentAtom = (DocumentAtom)this._children[0];
        int slwtcount = 0;
        for (int i = 1; i < this._children.length; ++i) {
            if (this._children[i] instanceof SlideListWithText) {
                ++slwtcount;
            }
            if (this._children[i] instanceof Environment) {
                this.environment = (Environment)this._children[i];
            }
            if (this._children[i] instanceof PPDrawingGroup) {
                this.ppDrawing = (PPDrawingGroup)this._children[i];
            }
            if (this._children[i] instanceof ExObjList) {
                this.exObjList = (ExObjList)this._children[i];
            }
        }
        if (slwtcount == 0) {
            this.logger.log(POILogger.WARN, "No SlideListWithText's found - there should normally be at least one!");
        }
        if (slwtcount > 3) {
            this.logger.log(POILogger.WARN, "Found " + slwtcount + " SlideListWithTexts - normally there should only be three!");
        }
        this.slwts = new SlideListWithText[slwtcount];
        slwtcount = 0;
        for (int i = 1; i < this._children.length; ++i) {
            if (this._children[i] instanceof SlideListWithText) {
                this.slwts[slwtcount] = (SlideListWithText)this._children[i];
                ++slwtcount;
            }
        }
    }
    
    public void addSlideListWithText(final SlideListWithText slwt) {
        final Record endDoc = this._children[this._children.length - 1];
        if (endDoc.getRecordType() != RecordTypes.EndDocument.typeID) {
            throw new IllegalStateException("The last child record of a Document should be EndDocument, but it was " + endDoc);
        }
        this.addChildBefore(slwt, endDoc);
        final int newSize = this.slwts.length + 1;
        final SlideListWithText[] nl = new SlideListWithText[newSize];
        System.arraycopy(this.slwts, 0, nl, 0, this.slwts.length);
        nl[nl.length - 1] = slwt;
        this.slwts = nl;
    }
    
    public void removeSlideListWithText(final SlideListWithText slwt) {
        final ArrayList<SlideListWithText> lst = new ArrayList<SlideListWithText>();
        for (final SlideListWithText s : this.slwts) {
            if (s != slwt) {
                lst.add(s);
            }
            else {
                this.removeChild(slwt);
            }
        }
        this.slwts = lst.toArray(new SlideListWithText[lst.size()]);
    }
    
    @Override
    public long getRecordType() {
        return Document._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], Document._type, this._children, out);
    }
    
    static {
        Document._type = 1000L;
    }
}
