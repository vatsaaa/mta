// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public final class MainMaster extends SheetContainer
{
    private byte[] _header;
    private static long _type;
    private SlideAtom slideAtom;
    private PPDrawing ppDrawing;
    private TxMasterStyleAtom[] txmasters;
    private ColorSchemeAtom[] clrscheme;
    private ColorSchemeAtom _colorScheme;
    
    public SlideAtom getSlideAtom() {
        return this.slideAtom;
    }
    
    @Override
    public PPDrawing getPPDrawing() {
        return this.ppDrawing;
    }
    
    public TxMasterStyleAtom[] getTxMasterStyleAtoms() {
        return this.txmasters;
    }
    
    public ColorSchemeAtom[] getColorSchemeAtoms() {
        return this.clrscheme;
    }
    
    protected MainMaster(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        final ArrayList<TxMasterStyleAtom> tx = new ArrayList<TxMasterStyleAtom>();
        final ArrayList<ColorSchemeAtom> clr = new ArrayList<ColorSchemeAtom>();
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof SlideAtom) {
                this.slideAtom = (SlideAtom)this._children[i];
            }
            else if (this._children[i] instanceof PPDrawing) {
                this.ppDrawing = (PPDrawing)this._children[i];
            }
            else if (this._children[i] instanceof TxMasterStyleAtom) {
                tx.add((TxMasterStyleAtom)this._children[i]);
            }
            else if (this._children[i] instanceof ColorSchemeAtom) {
                clr.add((ColorSchemeAtom)this._children[i]);
            }
            if (this.ppDrawing != null && this._children[i] instanceof ColorSchemeAtom) {
                this._colorScheme = (ColorSchemeAtom)this._children[i];
            }
        }
        this.txmasters = tx.toArray(new TxMasterStyleAtom[tx.size()]);
        this.clrscheme = clr.toArray(new ColorSchemeAtom[clr.size()]);
    }
    
    @Override
    public long getRecordType() {
        return MainMaster._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], MainMaster._type, this._children, out);
    }
    
    @Override
    public ColorSchemeAtom getColorScheme() {
        return this._colorScheme;
    }
    
    static {
        MainMaster._type = 1016L;
    }
}
