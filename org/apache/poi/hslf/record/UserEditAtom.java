// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;
import org.apache.poi.util.LittleEndian;

public final class UserEditAtom extends PositionDependentRecordAtom
{
    public static final int LAST_VIEW_NONE = 0;
    public static final int LAST_VIEW_SLIDE_VIEW = 1;
    public static final int LAST_VIEW_OUTLINE_VIEW = 2;
    public static final int LAST_VIEW_NOTES = 3;
    private byte[] _header;
    private static long _type;
    private byte[] reserved;
    private int lastViewedSlideID;
    private int pptVersion;
    private int lastUserEditAtomOffset;
    private int persistPointersOffset;
    private int docPersistRef;
    private int maxPersistWritten;
    private short lastViewType;
    
    public int getLastViewedSlideID() {
        return this.lastViewedSlideID;
    }
    
    public short getLastViewType() {
        return this.lastViewType;
    }
    
    public int getLastUserEditAtomOffset() {
        return this.lastUserEditAtomOffset;
    }
    
    public int getPersistPointersOffset() {
        return this.persistPointersOffset;
    }
    
    public int getDocPersistRef() {
        return this.docPersistRef;
    }
    
    public int getMaxPersistWritten() {
        return this.maxPersistWritten;
    }
    
    public void setLastUserEditAtomOffset(final int offset) {
        this.lastUserEditAtomOffset = offset;
    }
    
    public void setPersistPointersOffset(final int offset) {
        this.persistPointersOffset = offset;
    }
    
    public void setLastViewType(final short type) {
        this.lastViewType = type;
    }
    
    public void setMaxPersistWritten(final int max) {
        this.maxPersistWritten = max;
    }
    
    protected UserEditAtom(final byte[] source, final int start, int len) {
        if (len < 34) {
            len = 34;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this.lastViewedSlideID = LittleEndian.getInt(source, start + 0 + 8);
        this.pptVersion = LittleEndian.getInt(source, start + 4 + 8);
        this.lastUserEditAtomOffset = LittleEndian.getInt(source, start + 8 + 8);
        this.persistPointersOffset = LittleEndian.getInt(source, start + 12 + 8);
        this.docPersistRef = LittleEndian.getInt(source, start + 16 + 8);
        this.maxPersistWritten = LittleEndian.getInt(source, start + 20 + 8);
        this.lastViewType = LittleEndian.getShort(source, start + 24 + 8);
        System.arraycopy(source, start + 26 + 8, this.reserved = new byte[len - 26 - 8], 0, this.reserved.length);
    }
    
    @Override
    public long getRecordType() {
        return UserEditAtom._type;
    }
    
    @Override
    public void updateOtherRecordReferences(final Hashtable<Integer, Integer> oldToNewReferencesLookup) {
        if (this.lastUserEditAtomOffset != 0) {
            final Integer newLocation = oldToNewReferencesLookup.get(this.lastUserEditAtomOffset);
            if (newLocation == null) {
                throw new RuntimeException("Couldn't find the new location of the UserEditAtom that used to be at " + this.lastUserEditAtomOffset);
            }
            this.lastUserEditAtomOffset = newLocation;
        }
        final Integer newLocation = oldToNewReferencesLookup.get(this.persistPointersOffset);
        if (newLocation == null) {
            throw new RuntimeException("Couldn't find the new location of the PersistPtr that used to be at " + this.persistPointersOffset);
        }
        this.persistPointersOffset = newLocation;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        Record.writeLittleEndian(this.lastViewedSlideID, out);
        Record.writeLittleEndian(this.pptVersion, out);
        Record.writeLittleEndian(this.lastUserEditAtomOffset, out);
        Record.writeLittleEndian(this.persistPointersOffset, out);
        Record.writeLittleEndian(this.docPersistRef, out);
        Record.writeLittleEndian(this.maxPersistWritten, out);
        Record.writeLittleEndian(this.lastViewType, out);
        out.write(this.reserved);
    }
    
    static {
        UserEditAtom._type = 4085L;
    }
}
