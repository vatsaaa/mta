// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherBSERecord;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.ddf.EscherDggRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public final class PPDrawingGroup extends RecordAtom
{
    private byte[] _header;
    private EscherContainerRecord dggContainer;
    private EscherDggRecord dgg;
    
    protected PPDrawingGroup(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        final byte[] contents = new byte[len];
        System.arraycopy(source, start, contents, 0, len);
        final DefaultEscherRecordFactory erf = new DefaultEscherRecordFactory();
        final EscherRecord child = erf.createRecord(contents, 0);
        child.fillFields(contents, 0, erf);
        this.dggContainer = (EscherContainerRecord)child.getChild(0);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.PPDrawingGroup.typeID;
    }
    
    @Override
    public Record[] getChildRecords() {
        return null;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        final ByteArrayOutputStream bout = new ByteArrayOutputStream();
        final Iterator<EscherRecord> iter = this.dggContainer.getChildIterator();
        while (iter.hasNext()) {
            final EscherRecord r = iter.next();
            if (r.getRecordId() == -4095) {
                final EscherContainerRecord bstore = (EscherContainerRecord)r;
                final ByteArrayOutputStream b2 = new ByteArrayOutputStream();
                final Iterator<EscherRecord> it = bstore.getChildIterator();
                while (it.hasNext()) {
                    final EscherBSERecord bse = it.next();
                    final byte[] b3 = new byte[44];
                    bse.serialize(0, b3);
                    b2.write(b3);
                }
                final byte[] bstorehead = new byte[8];
                LittleEndian.putShort(bstorehead, 0, bstore.getOptions());
                LittleEndian.putShort(bstorehead, 2, bstore.getRecordId());
                LittleEndian.putInt(bstorehead, 4, b2.size());
                bout.write(bstorehead);
                bout.write(b2.toByteArray());
            }
            else {
                bout.write(r.serialize());
            }
        }
        final int size = bout.size();
        LittleEndian.putInt(this._header, 4, size + 8);
        out.write(this._header);
        final byte[] dgghead = new byte[8];
        LittleEndian.putShort(dgghead, 0, this.dggContainer.getOptions());
        LittleEndian.putShort(dgghead, 2, this.dggContainer.getRecordId());
        LittleEndian.putInt(dgghead, 4, size);
        out.write(dgghead);
        out.write(bout.toByteArray());
    }
    
    public EscherContainerRecord getDggContainer() {
        return this.dggContainer;
    }
    
    public EscherDggRecord getEscherDggRecord() {
        if (this.dgg == null) {
            final Iterator<EscherRecord> it = this.dggContainer.getChildIterator();
            while (it.hasNext()) {
                final EscherRecord r = it.next();
                if (r instanceof EscherDggRecord) {
                    this.dgg = (EscherDggRecord)r;
                    break;
                }
            }
        }
        return this.dgg;
    }
}
