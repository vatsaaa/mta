// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.lang.reflect.Field;
import java.util.HashMap;

public final class RecordTypes
{
    public static HashMap<Integer, String> typeToName;
    public static HashMap<Integer, Class<? extends Record>> typeToClass;
    public static final Type Unknown;
    public static final Type Document;
    public static final Type DocumentAtom;
    public static final Type EndDocument;
    public static final Type Slide;
    public static final Type SlideAtom;
    public static final Type Notes;
    public static final Type NotesAtom;
    public static final Type Environment;
    public static final Type SlidePersistAtom;
    public static final Type SSlideLayoutAtom;
    public static final Type MainMaster;
    public static final Type SSSlideInfoAtom;
    public static final Type SlideViewInfo;
    public static final Type GuideAtom;
    public static final Type ViewInfo;
    public static final Type ViewInfoAtom;
    public static final Type SlideViewInfoAtom;
    public static final Type VBAInfo;
    public static final Type VBAInfoAtom;
    public static final Type SSDocInfoAtom;
    public static final Type Summary;
    public static final Type DocRoutingSlip;
    public static final Type OutlineViewInfo;
    public static final Type SorterViewInfo;
    public static final Type ExObjList;
    public static final Type ExObjListAtom;
    public static final Type PPDrawingGroup;
    public static final Type PPDrawing;
    public static final Type NamedShows;
    public static final Type NamedShow;
    public static final Type NamedShowSlides;
    public static final Type SheetProperties;
    public static final Type List;
    public static final Type FontCollection;
    public static final Type BookmarkCollection;
    public static final Type SoundCollection;
    public static final Type SoundCollAtom;
    public static final Type Sound;
    public static final Type SoundData;
    public static final Type BookmarkSeedAtom;
    public static final Type ColorSchemeAtom;
    public static final Type ExObjRefAtom;
    public static final Type OEShapeAtom;
    public static final Type OEPlaceholderAtom;
    public static final Type GPopublicintAtom;
    public static final Type GRatioAtom;
    public static final Type OutlineTextRefAtom;
    public static final Type TextHeaderAtom;
    public static final Type TextCharsAtom;
    public static final Type StyleTextPropAtom;
    public static final Type BaseTextPropAtom;
    public static final Type TxMasterStyleAtom;
    public static final Type TxCFStyleAtom;
    public static final Type TxPFStyleAtom;
    public static final Type TextRulerAtom;
    public static final Type TextBookmarkAtom;
    public static final Type TextBytesAtom;
    public static final Type TxSIStyleAtom;
    public static final Type TextSpecInfoAtom;
    public static final Type DefaultRulerAtom;
    public static final Type FontEntityAtom;
    public static final Type FontEmbeddedData;
    public static final Type CString;
    public static final Type MetaFile;
    public static final Type ExOleObjAtom;
    public static final Type SrKinsoku;
    public static final Type HandOut;
    public static final Type ExEmbed;
    public static final Type ExEmbedAtom;
    public static final Type ExLink;
    public static final Type BookmarkEntityAtom;
    public static final Type ExLinkAtom;
    public static final Type SrKinsokuAtom;
    public static final Type ExHyperlinkAtom;
    public static final Type ExHyperlink;
    public static final Type SlideNumberMCAtom;
    public static final Type HeadersFooters;
    public static final Type HeadersFootersAtom;
    public static final Type TxInteractiveInfoAtom;
    public static final Type CharFormatAtom;
    public static final Type ParaFormatAtom;
    public static final Type RecolorInfoAtom;
    public static final Type ExQuickTimeMovie;
    public static final Type ExQuickTimeMovieData;
    public static final Type ExControl;
    public static final Type SlideListWithText;
    public static final Type InteractiveInfo;
    public static final Type InteractiveInfoAtom;
    public static final Type UserEditAtom;
    public static final Type CurrentUserAtom;
    public static final Type DateTimeMCAtom;
    public static final Type GenericDateMCAtom;
    public static final Type FooterMCAtom;
    public static final Type ExControlAtom;
    public static final Type ExMediaAtom;
    public static final Type ExVideoContainer;
    public static final Type ExAviMovie;
    public static final Type ExMCIMovie;
    public static final Type ExMIDIAudio;
    public static final Type ExCDAudio;
    public static final Type ExWAVAudioEmbedded;
    public static final Type ExWAVAudioLink;
    public static final Type ExOleObjStg;
    public static final Type ExCDAudioAtom;
    public static final Type ExWAVAudioEmbeddedAtom;
    public static final Type AnimationInfo;
    public static final Type AnimationInfoAtom;
    public static final Type RTFDateTimeMCAtom;
    public static final Type ProgTags;
    public static final Type ProgStringTag;
    public static final Type ProgBinaryTag;
    public static final Type BinaryTagData;
    public static final Type PrpublicintOptions;
    public static final Type PersistPtrFullBlock;
    public static final Type PersistPtrIncrementalBlock;
    public static final Type GScalingAtom;
    public static final Type GRColorAtom;
    public static final Type Comment2000;
    public static final Type Comment2000Atom;
    public static final Type Comment2000Summary;
    public static final Type Comment2000SummaryAtom;
    public static final Type DocumentEncryptionAtom;
    public static final Type OriginalMainMasterId;
    public static final Type CompositeMasterId;
    public static final Type RoundTripContentMasterInfo12;
    public static final Type RoundTripShapeId12;
    public static final Type RoundTripHFPlaceholder12;
    public static final Type RoundTripContentMasterId;
    public static final Type RoundTripOArtTextStyles12;
    public static final Type RoundTripShapeCheckSumForCustomLayouts12;
    public static final Type RoundTripNotesMasterTextStyles12;
    public static final Type RoundTripCustomTableStyles12;
    public static final int EscherDggContainer = 61440;
    public static final int EscherDgg = 61446;
    public static final int EscherCLSID = 61462;
    public static final int EscherOPT = 61451;
    public static final int EscherBStoreContainer = 61441;
    public static final int EscherBSE = 61447;
    public static final int EscherBlip_START = 61464;
    public static final int EscherBlip_END = 61719;
    public static final int EscherDgContainer = 61442;
    public static final int EscherDg = 61448;
    public static final int EscherRegroupItems = 61720;
    public static final int EscherColorScheme = 61728;
    public static final int EscherSpgrContainer = 61443;
    public static final int EscherSpContainer = 61444;
    public static final int EscherSpgr = 61449;
    public static final int EscherSp = 61450;
    public static final int EscherTextbox = 61452;
    public static final int EscherClientTextbox = 61453;
    public static final int EscherAnchor = 61454;
    public static final int EscherChildAnchor = 61455;
    public static final int EscherClientAnchor = 61456;
    public static final int EscherClientData = 61457;
    public static final int EscherSolverContainer = 61445;
    public static final int EscherConnectorRule = 61458;
    public static final int EscherAlignRule = 61459;
    public static final int EscherArcRule = 61460;
    public static final int EscherClientRule = 61461;
    public static final int EscherCalloutRule = 61463;
    public static final int EscherSelection = 61721;
    public static final int EscherColorMRU = 61722;
    public static final int EscherDeletedPspl = 61725;
    public static final int EscherSplitMenuColors = 61726;
    public static final int EscherOleObject = 61727;
    public static final int EscherUserDefined = 61730;
    
    public static String recordName(final int type) {
        String name = RecordTypes.typeToName.get(type);
        if (name == null) {
            name = "Unknown" + type;
        }
        return name;
    }
    
    public static Class<? extends Record> recordHandlingClass(final int type) {
        final Class<? extends Record> c = RecordTypes.typeToClass.get(type);
        return c;
    }
    
    static {
        Unknown = new Type(0, null);
        Document = new Type(1000, Document.class);
        DocumentAtom = new Type(1001, DocumentAtom.class);
        EndDocument = new Type(1002, null);
        Slide = new Type(1006, Slide.class);
        SlideAtom = new Type(1007, SlideAtom.class);
        Notes = new Type(1008, Notes.class);
        NotesAtom = new Type(1009, NotesAtom.class);
        Environment = new Type(1010, Environment.class);
        SlidePersistAtom = new Type(1011, SlidePersistAtom.class);
        SSlideLayoutAtom = new Type(1015, null);
        MainMaster = new Type(1016, MainMaster.class);
        SSSlideInfoAtom = new Type(1017, null);
        SlideViewInfo = new Type(1018, null);
        GuideAtom = new Type(1019, null);
        ViewInfo = new Type(1020, null);
        ViewInfoAtom = new Type(1021, null);
        SlideViewInfoAtom = new Type(1022, null);
        VBAInfo = new Type(1023, null);
        VBAInfoAtom = new Type(1024, null);
        SSDocInfoAtom = new Type(1025, null);
        Summary = new Type(1026, null);
        DocRoutingSlip = new Type(1030, null);
        OutlineViewInfo = new Type(1031, null);
        SorterViewInfo = new Type(1032, null);
        ExObjList = new Type(1033, ExObjList.class);
        ExObjListAtom = new Type(1034, ExObjListAtom.class);
        PPDrawingGroup = new Type(1035, PPDrawingGroup.class);
        PPDrawing = new Type(1036, PPDrawing.class);
        NamedShows = new Type(1040, null);
        NamedShow = new Type(1041, null);
        NamedShowSlides = new Type(1042, null);
        SheetProperties = new Type(1044, null);
        List = new Type(2000, null);
        FontCollection = new Type(2005, FontCollection.class);
        BookmarkCollection = new Type(2019, null);
        SoundCollection = new Type(2020, SoundCollection.class);
        SoundCollAtom = new Type(2021, null);
        Sound = new Type(2022, Sound.class);
        SoundData = new Type(2023, SoundData.class);
        BookmarkSeedAtom = new Type(2025, null);
        ColorSchemeAtom = new Type(2032, ColorSchemeAtom.class);
        ExObjRefAtom = new Type(3009, null);
        OEShapeAtom = new Type(3009, OEShapeAtom.class);
        OEPlaceholderAtom = new Type(3011, OEPlaceholderAtom.class);
        GPopublicintAtom = new Type(3024, null);
        GRatioAtom = new Type(3031, null);
        OutlineTextRefAtom = new Type(3998, OutlineTextRefAtom.class);
        TextHeaderAtom = new Type(3999, TextHeaderAtom.class);
        TextCharsAtom = new Type(4000, TextCharsAtom.class);
        StyleTextPropAtom = new Type(4001, StyleTextPropAtom.class);
        BaseTextPropAtom = new Type(4002, null);
        TxMasterStyleAtom = new Type(4003, TxMasterStyleAtom.class);
        TxCFStyleAtom = new Type(4004, null);
        TxPFStyleAtom = new Type(4005, null);
        TextRulerAtom = new Type(4006, TextRulerAtom.class);
        TextBookmarkAtom = new Type(4007, null);
        TextBytesAtom = new Type(4008, TextBytesAtom.class);
        TxSIStyleAtom = new Type(4009, null);
        TextSpecInfoAtom = new Type(4010, TextSpecInfoAtom.class);
        DefaultRulerAtom = new Type(4011, null);
        FontEntityAtom = new Type(4023, FontEntityAtom.class);
        FontEmbeddedData = new Type(4024, null);
        CString = new Type(4026, CString.class);
        MetaFile = new Type(4033, null);
        ExOleObjAtom = new Type(4035, ExOleObjAtom.class);
        SrKinsoku = new Type(4040, null);
        HandOut = new Type(4041, DummyPositionSensitiveRecordWithChildren.class);
        ExEmbed = new Type(4044, ExEmbed.class);
        ExEmbedAtom = new Type(4045, ExEmbedAtom.class);
        ExLink = new Type(4046, null);
        BookmarkEntityAtom = new Type(4048, null);
        ExLinkAtom = new Type(4049, null);
        SrKinsokuAtom = new Type(4050, null);
        ExHyperlinkAtom = new Type(4051, ExHyperlinkAtom.class);
        ExHyperlink = new Type(4055, ExHyperlink.class);
        SlideNumberMCAtom = new Type(4056, null);
        HeadersFooters = new Type(4057, HeadersFootersContainer.class);
        HeadersFootersAtom = new Type(4058, HeadersFootersAtom.class);
        TxInteractiveInfoAtom = new Type(4063, TxInteractiveInfoAtom.class);
        CharFormatAtom = new Type(4066, null);
        ParaFormatAtom = new Type(4067, null);
        RecolorInfoAtom = new Type(4071, null);
        ExQuickTimeMovie = new Type(4074, null);
        ExQuickTimeMovieData = new Type(4075, null);
        ExControl = new Type(4078, ExControl.class);
        SlideListWithText = new Type(4080, SlideListWithText.class);
        InteractiveInfo = new Type(4082, InteractiveInfo.class);
        InteractiveInfoAtom = new Type(4083, InteractiveInfoAtom.class);
        UserEditAtom = new Type(4085, UserEditAtom.class);
        CurrentUserAtom = new Type(4086, null);
        DateTimeMCAtom = new Type(4087, null);
        GenericDateMCAtom = new Type(4088, null);
        FooterMCAtom = new Type(4090, null);
        ExControlAtom = new Type(4091, ExControlAtom.class);
        ExMediaAtom = new Type(4100, ExMediaAtom.class);
        ExVideoContainer = new Type(4101, ExVideoContainer.class);
        ExAviMovie = new Type(4102, ExAviMovie.class);
        ExMCIMovie = new Type(4103, ExMCIMovie.class);
        ExMIDIAudio = new Type(4109, null);
        ExCDAudio = new Type(4110, null);
        ExWAVAudioEmbedded = new Type(4111, null);
        ExWAVAudioLink = new Type(4112, null);
        ExOleObjStg = new Type(4113, ExOleObjStg.class);
        ExCDAudioAtom = new Type(4114, null);
        ExWAVAudioEmbeddedAtom = new Type(4115, null);
        AnimationInfo = new Type(4116, AnimationInfo.class);
        AnimationInfoAtom = new Type(4081, AnimationInfoAtom.class);
        RTFDateTimeMCAtom = new Type(4117, null);
        ProgTags = new Type(5000, DummyPositionSensitiveRecordWithChildren.class);
        ProgStringTag = new Type(5001, null);
        ProgBinaryTag = new Type(5002, DummyPositionSensitiveRecordWithChildren.class);
        BinaryTagData = new Type(5003, DummyPositionSensitiveRecordWithChildren.class);
        PrpublicintOptions = new Type(6000, null);
        PersistPtrFullBlock = new Type(6001, PersistPtrHolder.class);
        PersistPtrIncrementalBlock = new Type(6002, PersistPtrHolder.class);
        GScalingAtom = new Type(10001, null);
        GRColorAtom = new Type(10002, null);
        Comment2000 = new Type(12000, Comment2000.class);
        Comment2000Atom = new Type(12001, Comment2000Atom.class);
        Comment2000Summary = new Type(12004, null);
        Comment2000SummaryAtom = new Type(12005, null);
        DocumentEncryptionAtom = new Type(12052, DocumentEncryptionAtom.class);
        OriginalMainMasterId = new Type(1052, null);
        CompositeMasterId = new Type(1052, null);
        RoundTripContentMasterInfo12 = new Type(1054, null);
        RoundTripShapeId12 = new Type(1055, null);
        RoundTripHFPlaceholder12 = new Type(1056, RoundTripHFPlaceholder12.class);
        RoundTripContentMasterId = new Type(1058, null);
        RoundTripOArtTextStyles12 = new Type(1059, null);
        RoundTripShapeCheckSumForCustomLayouts12 = new Type(1062, null);
        RoundTripNotesMasterTextStyles12 = new Type(1063, null);
        RoundTripCustomTableStyles12 = new Type(1064, null);
        RecordTypes.typeToName = new HashMap<Integer, String>();
        RecordTypes.typeToClass = new HashMap<Integer, Class<? extends Record>>();
        try {
            final Field[] f = RecordTypes.class.getFields();
            for (int i = 0; i < f.length; ++i) {
                final Object val = f[i].get(null);
                if (val instanceof Integer) {
                    RecordTypes.typeToName.put((Integer)val, f[i].getName());
                }
                if (val instanceof Type) {
                    final Type t = (Type)val;
                    Class<? extends Record> c = t.handlingClass;
                    final Integer id = t.typeID;
                    if (c == null) {
                        c = UnknownRecordPlaceholder.class;
                    }
                    RecordTypes.typeToName.put(id, f[i].getName());
                    RecordTypes.typeToClass.put(id, c);
                }
            }
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException("Failed to initialize records types");
        }
    }
    
    public static class Type
    {
        public int typeID;
        public Class<? extends Record> handlingClass;
        
        public Type(final int typeID, final Class<? extends Record> handlingClass) {
            this.typeID = typeID;
            this.handlingClass = handlingClass;
        }
    }
}
