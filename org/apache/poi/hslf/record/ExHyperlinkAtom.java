// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class ExHyperlinkAtom extends RecordAtom
{
    private byte[] _header;
    private byte[] _data;
    
    protected ExHyperlinkAtom() {
        this._header = new byte[8];
        this._data = new byte[4];
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    protected ExHyperlinkAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
        if (this._data.length < 4) {
            throw new IllegalArgumentException("The length of the data for a ExHyperlinkAtom must be at least 4 bytes, but was only " + this._data.length);
        }
    }
    
    public int getNumber() {
        return LittleEndian.getInt(this._data, 0);
    }
    
    public void setNumber(final int number) {
        LittleEndian.putInt(this._data, 0, number);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExHyperlinkAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
}
