// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

public abstract class SheetContainer extends PositionDependentRecordContainer
{
    public abstract PPDrawing getPPDrawing();
    
    public abstract ColorSchemeAtom getColorScheme();
}
