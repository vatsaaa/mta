// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public class ExEmbedAtom extends RecordAtom
{
    public static final int DOES_NOT_FOLLOW_COLOR_SCHEME = 0;
    public static final int FOLLOWS_ENTIRE_COLOR_SCHEME = 1;
    public static final int FOLLOWS_TEXT_AND_BACKGROUND_SCHEME = 2;
    private byte[] _header;
    private byte[] _data;
    
    protected ExEmbedAtom() {
        this._header = new byte[8];
        this._data = new byte[7];
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    protected ExEmbedAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
        if (this._data.length < 7) {
            throw new IllegalArgumentException("The length of the data for a ExEmbedAtom must be at least 4 bytes, but was only " + this._data.length);
        }
    }
    
    public int getFollowColorScheme() {
        return LittleEndian.getInt(this._data, 0);
    }
    
    public boolean getCantLockServerB() {
        return this._data[4] != 0;
    }
    
    public boolean getNoSizeToServerB() {
        return this._data[5] != 0;
    }
    
    public boolean getIsTable() {
        return this._data[6] != 0;
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExEmbedAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
}
