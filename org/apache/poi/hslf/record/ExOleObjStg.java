// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.util.Hashtable;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.poi.util.LittleEndian;

public class ExOleObjStg extends RecordAtom implements PositionDependentRecord, PersistRecord
{
    private int _persistId;
    private byte[] _header;
    private byte[] _data;
    protected int myLastOnDiskOffset;
    
    public ExOleObjStg() {
        this._header = new byte[8];
        this._data = new byte[0];
        LittleEndian.putShort(this._header, 0, (short)16);
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    protected ExOleObjStg(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
    }
    
    public boolean isCompressed() {
        return LittleEndian.getShort(this._header, 0) != 0;
    }
    
    public int getDataLength() {
        if (this.isCompressed()) {
            return LittleEndian.getInt(this._data, 0);
        }
        return this._data.length;
    }
    
    public InputStream getData() {
        if (this.isCompressed()) {
            final InputStream compressedStream = new ByteArrayInputStream(this._data, 4, this._data.length);
            return new InflaterInputStream(compressedStream);
        }
        return new ByteArrayInputStream(this._data, 0, this._data.length);
    }
    
    public byte[] getRawData() {
        return this._data;
    }
    
    public void setData(final byte[] data) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final byte[] b = new byte[4];
        LittleEndian.putInt(b, data.length);
        out.write(b);
        final DeflaterOutputStream def = new DeflaterOutputStream(out);
        def.write(data, 0, data.length);
        def.finish();
        this._data = out.toByteArray();
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExOleObjStg.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
    
    public int getPersistId() {
        return this._persistId;
    }
    
    public void setPersistId(final int id) {
        this._persistId = id;
    }
    
    public int getLastOnDiskOffset() {
        return this.myLastOnDiskOffset;
    }
    
    public void setLastOnDiskOffset(final int offset) {
        this.myLastOnDiskOffset = offset;
    }
    
    public void updateOtherRecordReferences(final Hashtable<Integer, Integer> oldToNewReferencesLookup) {
    }
}
