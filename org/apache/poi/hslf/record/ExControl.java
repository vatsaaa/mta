// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

public final class ExControl extends ExEmbed
{
    protected ExControl(final byte[] source, final int start, final int len) {
        super(source, start, len);
    }
    
    public ExControl() {
        this._children[0] = (this.embedAtom = new ExControlAtom());
    }
    
    public ExControlAtom getExControlAtom() {
        return (ExControlAtom)this._children[0];
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExControl.typeID;
    }
}
