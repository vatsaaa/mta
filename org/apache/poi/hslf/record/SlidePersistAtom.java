// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class SlidePersistAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private int refID;
    private boolean hasShapesOtherThanPlaceholders;
    private int numPlaceholderTexts;
    private int slideIdentifier;
    private byte[] reservedFields;
    
    public int getRefID() {
        return this.refID;
    }
    
    public int getSlideIdentifier() {
        return this.slideIdentifier;
    }
    
    public int getNumPlaceholderTexts() {
        return this.numPlaceholderTexts;
    }
    
    public boolean getHasShapesOtherThanPlaceholders() {
        return this.hasShapesOtherThanPlaceholders;
    }
    
    public void setRefID(final int id) {
        this.refID = id;
    }
    
    public void setSlideIdentifier(final int id) {
        this.slideIdentifier = id;
    }
    
    protected SlidePersistAtom(final byte[] source, final int start, int len) {
        if (len < 8) {
            len = 8;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this.refID = LittleEndian.getInt(source, start + 8);
        final int flags = LittleEndian.getInt(source, start + 12);
        if (flags == 4) {
            this.hasShapesOtherThanPlaceholders = true;
        }
        else {
            this.hasShapesOtherThanPlaceholders = false;
        }
        this.numPlaceholderTexts = LittleEndian.getInt(source, start + 16);
        this.slideIdentifier = LittleEndian.getInt(source, start + 20);
        System.arraycopy(source, start + 24, this.reservedFields = new byte[len - 24], 0, this.reservedFields.length);
    }
    
    public SlidePersistAtom() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 0);
        LittleEndian.putUShort(this._header, 2, (int)SlidePersistAtom._type);
        LittleEndian.putInt(this._header, 4, 20);
        this.hasShapesOtherThanPlaceholders = true;
        this.reservedFields = new byte[4];
    }
    
    @Override
    public long getRecordType() {
        return SlidePersistAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        int flags = 0;
        if (this.hasShapesOtherThanPlaceholders) {
            flags = 4;
        }
        Record.writeLittleEndian(this.refID, out);
        Record.writeLittleEndian(flags, out);
        Record.writeLittleEndian(this.numPlaceholderTexts, out);
        Record.writeLittleEndian(this.slideIdentifier, out);
        out.write(this.reservedFields);
    }
    
    static {
        SlidePersistAtom._type = 1011L;
    }
}
