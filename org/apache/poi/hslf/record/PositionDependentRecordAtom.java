// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.util.Hashtable;

public abstract class PositionDependentRecordAtom extends RecordAtom implements PositionDependentRecord
{
    protected int myLastOnDiskOffset;
    
    public int getLastOnDiskOffset() {
        return this.myLastOnDiskOffset;
    }
    
    public void setLastOnDiskOffset(final int offset) {
        this.myLastOnDiskOffset = offset;
    }
    
    public abstract void updateOtherRecordReferences(final Hashtable<Integer, Integer> p0);
}
