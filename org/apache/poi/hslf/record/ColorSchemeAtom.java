// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.LittleEndian;

public final class ColorSchemeAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private int backgroundColourRGB;
    private int textAndLinesColourRGB;
    private int shadowsColourRGB;
    private int titleTextColourRGB;
    private int fillsColourRGB;
    private int accentColourRGB;
    private int accentAndHyperlinkColourRGB;
    private int accentAndFollowingHyperlinkColourRGB;
    
    public int getBackgroundColourRGB() {
        return this.backgroundColourRGB;
    }
    
    public void setBackgroundColourRGB(final int rgb) {
        this.backgroundColourRGB = rgb;
    }
    
    public int getTextAndLinesColourRGB() {
        return this.textAndLinesColourRGB;
    }
    
    public void setTextAndLinesColourRGB(final int rgb) {
        this.textAndLinesColourRGB = rgb;
    }
    
    public int getShadowsColourRGB() {
        return this.shadowsColourRGB;
    }
    
    public void setShadowsColourRGB(final int rgb) {
        this.shadowsColourRGB = rgb;
    }
    
    public int getTitleTextColourRGB() {
        return this.titleTextColourRGB;
    }
    
    public void setTitleTextColourRGB(final int rgb) {
        this.titleTextColourRGB = rgb;
    }
    
    public int getFillsColourRGB() {
        return this.fillsColourRGB;
    }
    
    public void setFillsColourRGB(final int rgb) {
        this.fillsColourRGB = rgb;
    }
    
    public int getAccentColourRGB() {
        return this.accentColourRGB;
    }
    
    public void setAccentColourRGB(final int rgb) {
        this.accentColourRGB = rgb;
    }
    
    public int getAccentAndHyperlinkColourRGB() {
        return this.accentAndHyperlinkColourRGB;
    }
    
    public void setAccentAndHyperlinkColourRGB(final int rgb) {
        this.accentAndHyperlinkColourRGB = rgb;
    }
    
    public int getAccentAndFollowingHyperlinkColourRGB() {
        return this.accentAndFollowingHyperlinkColourRGB;
    }
    
    public void setAccentAndFollowingHyperlinkColourRGB(final int rgb) {
        this.accentAndFollowingHyperlinkColourRGB = rgb;
    }
    
    protected ColorSchemeAtom(final byte[] source, final int start, int len) {
        if (len < 40) {
            len = 40;
            if (source.length - start < 40) {
                throw new RuntimeException("Not enough data to form a ColorSchemeAtom (always 40 bytes long) - found " + (source.length - start));
            }
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this.backgroundColourRGB = LittleEndian.getInt(source, start + 8 + 0);
        this.textAndLinesColourRGB = LittleEndian.getInt(source, start + 8 + 4);
        this.shadowsColourRGB = LittleEndian.getInt(source, start + 8 + 8);
        this.titleTextColourRGB = LittleEndian.getInt(source, start + 8 + 12);
        this.fillsColourRGB = LittleEndian.getInt(source, start + 8 + 16);
        this.accentColourRGB = LittleEndian.getInt(source, start + 8 + 20);
        this.accentAndHyperlinkColourRGB = LittleEndian.getInt(source, start + 8 + 24);
        this.accentAndFollowingHyperlinkColourRGB = LittleEndian.getInt(source, start + 8 + 28);
    }
    
    public ColorSchemeAtom() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 16);
        LittleEndian.putUShort(this._header, 2, (int)ColorSchemeAtom._type);
        LittleEndian.putInt(this._header, 4, 32);
        this.backgroundColourRGB = 16777215;
        this.textAndLinesColourRGB = 0;
        this.shadowsColourRGB = 8421504;
        this.titleTextColourRGB = 0;
        this.fillsColourRGB = 10079232;
        this.accentColourRGB = 13382451;
        this.accentAndHyperlinkColourRGB = 16764108;
        this.accentAndFollowingHyperlinkColourRGB = 11711154;
    }
    
    @Override
    public long getRecordType() {
        return ColorSchemeAtom._type;
    }
    
    public static byte[] splitRGB(final int rgb) {
        final byte[] ret = new byte[3];
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            Record.writeLittleEndian(rgb, baos);
        }
        catch (IOException ie) {
            throw new RuntimeException(ie);
        }
        final byte[] b = baos.toByteArray();
        System.arraycopy(b, 0, ret, 0, 3);
        return ret;
    }
    
    public static int joinRGB(final byte r, final byte g, final byte b) {
        return joinRGB(new byte[] { r, g, b });
    }
    
    public static int joinRGB(final byte[] rgb) {
        if (rgb.length != 3) {
            throw new RuntimeException("joinRGB accepts a byte array of 3 values, but got one of " + rgb.length + " values!");
        }
        final byte[] with_zero = new byte[4];
        System.arraycopy(rgb, 0, with_zero, 0, 3);
        with_zero[3] = 0;
        final int ret = LittleEndian.getInt(with_zero, 0);
        return ret;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        Record.writeLittleEndian(this.backgroundColourRGB, out);
        Record.writeLittleEndian(this.textAndLinesColourRGB, out);
        Record.writeLittleEndian(this.shadowsColourRGB, out);
        Record.writeLittleEndian(this.titleTextColourRGB, out);
        Record.writeLittleEndian(this.fillsColourRGB, out);
        Record.writeLittleEndian(this.accentColourRGB, out);
        Record.writeLittleEndian(this.accentAndHyperlinkColourRGB, out);
        Record.writeLittleEndian(this.accentAndFollowingHyperlinkColourRGB, out);
    }
    
    public int getColor(final int idx) {
        final int[] clr = { this.backgroundColourRGB, this.textAndLinesColourRGB, this.shadowsColourRGB, this.titleTextColourRGB, this.fillsColourRGB, this.accentColourRGB, this.accentAndHyperlinkColourRGB, this.accentAndFollowingHyperlinkColourRGB };
        return clr[idx];
    }
    
    static {
        ColorSchemeAtom._type = 2032L;
    }
}
