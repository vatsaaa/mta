// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class DocumentAtom extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private long slideSizeX;
    private long slideSizeY;
    private long notesSizeX;
    private long notesSizeY;
    private long serverZoomFrom;
    private long serverZoomTo;
    private long notesMasterPersist;
    private long handoutMasterPersist;
    private int firstSlideNum;
    private int slideSizeType;
    private byte saveWithFonts;
    private byte omitTitlePlace;
    private byte rightToLeft;
    private byte showComments;
    private byte[] reserved;
    
    public long getSlideSizeX() {
        return this.slideSizeX;
    }
    
    public long getSlideSizeY() {
        return this.slideSizeY;
    }
    
    public long getNotesSizeX() {
        return this.notesSizeX;
    }
    
    public long getNotesSizeY() {
        return this.notesSizeY;
    }
    
    public void setSlideSizeX(final long x) {
        this.slideSizeX = x;
    }
    
    public void setSlideSizeY(final long y) {
        this.slideSizeY = y;
    }
    
    public void setNotesSizeX(final long x) {
        this.notesSizeX = x;
    }
    
    public void setNotesSizeY(final long y) {
        this.notesSizeY = y;
    }
    
    public long getServerZoomFrom() {
        return this.serverZoomFrom;
    }
    
    public long getServerZoomTo() {
        return this.serverZoomTo;
    }
    
    public void setServerZoomFrom(final long zoom) {
        this.serverZoomFrom = zoom;
    }
    
    public void setServerZoomTo(final long zoom) {
        this.serverZoomTo = zoom;
    }
    
    public long getNotesMasterPersist() {
        return this.notesMasterPersist;
    }
    
    public long getHandoutMasterPersist() {
        return this.handoutMasterPersist;
    }
    
    public int getFirstSlideNum() {
        return this.firstSlideNum;
    }
    
    public int getSlideSizeType() {
        return this.slideSizeType;
    }
    
    public boolean getSaveWithFonts() {
        return this.saveWithFonts != 0;
    }
    
    public boolean getOmitTitlePlace() {
        return this.omitTitlePlace != 0;
    }
    
    public boolean getRightToLeft() {
        return this.rightToLeft != 0;
    }
    
    public boolean getShowComments() {
        return this.showComments != 0;
    }
    
    protected DocumentAtom(final byte[] source, final int start, int len) {
        if (len < 48) {
            len = 48;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this.slideSizeX = LittleEndian.getInt(source, start + 0 + 8);
        this.slideSizeY = LittleEndian.getInt(source, start + 4 + 8);
        this.notesSizeX = LittleEndian.getInt(source, start + 8 + 8);
        this.notesSizeY = LittleEndian.getInt(source, start + 12 + 8);
        this.serverZoomFrom = LittleEndian.getInt(source, start + 16 + 8);
        this.serverZoomTo = LittleEndian.getInt(source, start + 20 + 8);
        this.notesMasterPersist = LittleEndian.getInt(source, start + 24 + 8);
        this.handoutMasterPersist = LittleEndian.getInt(source, start + 28 + 8);
        this.firstSlideNum = LittleEndian.getShort(source, start + 32 + 8);
        this.slideSizeType = LittleEndian.getShort(source, start + 34 + 8);
        this.saveWithFonts = source[start + 36 + 8];
        this.omitTitlePlace = source[start + 37 + 8];
        this.rightToLeft = source[start + 38 + 8];
        this.showComments = source[start + 39 + 8];
        System.arraycopy(source, start + 48, this.reserved = new byte[len - 40 - 8], 0, this.reserved.length);
    }
    
    @Override
    public long getRecordType() {
        return DocumentAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        Record.writeLittleEndian((int)this.slideSizeX, out);
        Record.writeLittleEndian((int)this.slideSizeY, out);
        Record.writeLittleEndian((int)this.notesSizeX, out);
        Record.writeLittleEndian((int)this.notesSizeY, out);
        Record.writeLittleEndian((int)this.serverZoomFrom, out);
        Record.writeLittleEndian((int)this.serverZoomTo, out);
        Record.writeLittleEndian((int)this.notesMasterPersist, out);
        Record.writeLittleEndian((int)this.handoutMasterPersist, out);
        Record.writeLittleEndian((short)this.firstSlideNum, out);
        Record.writeLittleEndian((short)this.slideSizeType, out);
        out.write(this.saveWithFonts);
        out.write(this.omitTitlePlace);
        out.write(this.rightToLeft);
        out.write(this.showComments);
        out.write(this.reserved);
    }
    
    static {
        DocumentAtom._type = 1001L;
    }
    
    public static final class SlideSize
    {
        public static final int ON_SCREEN = 0;
        public static final int LETTER_SIZED_PAPER = 1;
        public static final int A4_SIZED_PAPER = 2;
        public static final int ON_35MM = 3;
        public static final int OVERHEAD = 4;
        public static final int BANNER = 5;
        public static final int CUSTOM = 6;
    }
}
