// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.util.Hashtable;

public interface PositionDependentRecord
{
    int getLastOnDiskOffset();
    
    void setLastOnDiskOffset(final int p0);
    
    void updateOtherRecordReferences(final Hashtable<Integer, Integer> p0);
}
