// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class TxInteractiveInfoAtom extends RecordAtom
{
    private byte[] _header;
    private byte[] _data;
    
    public TxInteractiveInfoAtom() {
        this._header = new byte[8];
        this._data = new byte[8];
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    protected TxInteractiveInfoAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
    }
    
    public int getStartIndex() {
        return LittleEndian.getInt(this._data, 0);
    }
    
    public void setStartIndex(final int idx) {
        LittleEndian.putInt(this._data, 0, idx);
    }
    
    public int getEndIndex() {
        return LittleEndian.getInt(this._data, 4);
    }
    
    public void setEndIndex(final int idx) {
        LittleEndian.putInt(this._data, 4, idx);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.TxInteractiveInfoAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
}
