// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.StringUtil;

public final class CString extends RecordAtom
{
    private byte[] _header;
    private static long _type;
    private byte[] _text;
    
    public String getText() {
        return StringUtil.getFromUnicodeLE(this._text);
    }
    
    public void setText(final String text) {
        StringUtil.putUnicodeLE(text, this._text = new byte[text.length() * 2], 0);
        LittleEndian.putInt(this._header, 4, this._text.length);
    }
    
    public int getOptions() {
        return LittleEndian.getShort(this._header);
    }
    
    public void setOptions(final int count) {
        LittleEndian.putShort(this._header, (short)count);
    }
    
    protected CString(final byte[] source, final int start, int len) {
        if (len < 8) {
            len = 8;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._text = new byte[len - 8], 0, len - 8);
    }
    
    public CString() {
        this._header = new byte[] { 0, 0, -70, 15, 0, 0, 0, 0 };
        this._text = new byte[0];
    }
    
    @Override
    public long getRecordType() {
        return CString._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._text);
    }
    
    @Override
    public String toString() {
        return this.getText();
    }
    
    static {
        CString._type = 4026L;
    }
}
