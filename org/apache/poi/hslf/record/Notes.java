// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;

public final class Notes extends SheetContainer
{
    private byte[] _header;
    private static long _type;
    private NotesAtom notesAtom;
    private PPDrawing ppDrawing;
    private ColorSchemeAtom _colorScheme;
    
    public NotesAtom getNotesAtom() {
        return this.notesAtom;
    }
    
    @Override
    public PPDrawing getPPDrawing() {
        return this.ppDrawing;
    }
    
    protected Notes(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i] instanceof NotesAtom) {
                this.notesAtom = (NotesAtom)this._children[i];
            }
            if (this._children[i] instanceof PPDrawing) {
                this.ppDrawing = (PPDrawing)this._children[i];
            }
            if (this.ppDrawing != null && this._children[i] instanceof ColorSchemeAtom) {
                this._colorScheme = (ColorSchemeAtom)this._children[i];
            }
        }
    }
    
    @Override
    public long getRecordType() {
        return Notes._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], Notes._type, this._children, out);
    }
    
    @Override
    public ColorSchemeAtom getColorScheme() {
        return this._colorScheme;
    }
    
    static {
        Notes._type = 1008L;
    }
}
