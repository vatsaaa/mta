// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import org.apache.poi.util.LittleEndian;

public final class FontEntityAtom extends RecordAtom
{
    private byte[] _header;
    private byte[] _recdata;
    
    protected FontEntityAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._recdata = new byte[len - 8], 0, len - 8);
    }
    
    public FontEntityAtom() {
        this._recdata = new byte[68];
        LittleEndian.putShort(this._header = new byte[8], 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._recdata.length);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.FontEntityAtom.typeID;
    }
    
    public String getFontName() {
        String name = null;
        try {
            for (int i = 0; i < 64; i += 2) {
                if (this._recdata[i] == 0 && this._recdata[i + 1] == 0) {
                    name = new String(this._recdata, 0, i, "UTF-16LE");
                    break;
                }
            }
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return name;
    }
    
    public void setFontName(String name) {
        if (!name.endsWith("\u0000")) {
            name += "\u0000";
        }
        if (name.length() > 32) {
            throw new RuntimeException("The length of the font name, including null termination, must not exceed 32 characters");
        }
        try {
            final byte[] bytes = name.getBytes("UTF-16LE");
            System.arraycopy(bytes, 0, this._recdata, 0, bytes.length);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    
    public void setFontIndex(final int idx) {
        LittleEndian.putShort(this._header, 0, (short)idx);
    }
    
    public int getFontIndex() {
        return LittleEndian.getShort(this._header, 0) >> 4;
    }
    
    public void setCharSet(final int charset) {
        this._recdata[64] = (byte)charset;
    }
    
    public int getCharSet() {
        return this._recdata[64];
    }
    
    public void setFontFlags(final int flags) {
        this._recdata[65] = (byte)flags;
    }
    
    public int getFontFlags() {
        return this._recdata[65];
    }
    
    public void setFontType(final int type) {
        this._recdata[66] = (byte)type;
    }
    
    public int getFontType() {
        return this._recdata[66];
    }
    
    public void setPitchAndFamily(final int val) {
        this._recdata[67] = (byte)val;
    }
    
    public int getPitchAndFamily() {
        return this._recdata[67];
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._recdata);
    }
}
