// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class OEShapeAtom extends RecordAtom
{
    private byte[] _header;
    private byte[] _recdata;
    
    public OEShapeAtom() {
        this._recdata = new byte[4];
        LittleEndian.putShort(this._header = new byte[8], 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._recdata.length);
    }
    
    protected OEShapeAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._recdata = new byte[len - 8], 0, len - 8);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.OEShapeAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._recdata);
    }
    
    public int getOptions() {
        return LittleEndian.getInt(this._recdata, 0);
    }
    
    public void setOptions(final int id) {
        LittleEndian.putInt(this._recdata, 0, id);
    }
}
