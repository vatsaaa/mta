// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.LittleEndian;
import java.util.Enumeration;
import java.util.Hashtable;

public final class PersistPtrHolder extends PositionDependentRecordAtom
{
    private byte[] _header;
    private byte[] _ptrData;
    private long _type;
    private Hashtable<Integer, Integer> _slideLocations;
    private Hashtable<Integer, Integer> _slideOffsetDataLocation;
    
    public int[] getKnownSlideIDs() {
        final int[] ids = new int[this._slideLocations.size()];
        final Enumeration<Integer> e = this._slideLocations.keys();
        for (int i = 0; i < ids.length; ++i) {
            final Integer id = e.nextElement();
            ids[i] = id;
        }
        return ids;
    }
    
    public Hashtable<Integer, Integer> getSlideLocationsLookup() {
        return this._slideLocations;
    }
    
    public Hashtable<Integer, Integer> getSlideOffsetDataLocationsLookup() {
        return this._slideOffsetDataLocation;
    }
    
    public void addSlideLookup(final int slideID, final int posOnDisk) {
        final byte[] newPtrData = new byte[this._ptrData.length + 8];
        System.arraycopy(this._ptrData, 0, newPtrData, 0, this._ptrData.length);
        this._slideLocations.put(slideID, posOnDisk);
        this._slideOffsetDataLocation.put(slideID, this._ptrData.length + 4);
        int infoBlock = slideID;
        infoBlock += 1048576;
        LittleEndian.putInt(newPtrData, newPtrData.length - 8, infoBlock);
        LittleEndian.putInt(newPtrData, newPtrData.length - 4, posOnDisk);
        this._ptrData = newPtrData;
        LittleEndian.putInt(this._header, 4, newPtrData.length);
    }
    
    protected PersistPtrHolder(final byte[] source, final int start, int len) {
        if (len < 8) {
            len = 8;
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._type = LittleEndian.getUShort(this._header, 2);
        this._slideLocations = new Hashtable<Integer, Integer>();
        this._slideOffsetDataLocation = new Hashtable<Integer, Integer>();
        System.arraycopy(source, start + 8, this._ptrData = new byte[len - 8], 0, this._ptrData.length);
        int pos = 0;
        while (pos < this._ptrData.length) {
            final long info = LittleEndian.getUInt(this._ptrData, pos);
            final int offset_count = (int)(info >> 20);
            final int offset_no = (int)(info - (offset_count << 20));
            pos += 4;
            for (int i = 0; i < offset_count; ++i) {
                final int sheet_no = offset_no + i;
                final long sheet_offset = LittleEndian.getUInt(this._ptrData, pos);
                this._slideLocations.put(sheet_no, (int)sheet_offset);
                this._slideOffsetDataLocation.put(sheet_no, pos);
                pos += 4;
            }
        }
    }
    
    @Override
    public long getRecordType() {
        return this._type;
    }
    
    @Override
    public void updateOtherRecordReferences(final Hashtable<Integer, Integer> oldToNewReferencesLookup) {
        final int[] slideIDs = this.getKnownSlideIDs();
        for (int i = 0; i < slideIDs.length; ++i) {
            final Integer id = slideIDs[i];
            final Integer oldPos = this._slideLocations.get(id);
            Integer newPos = oldToNewReferencesLookup.get(oldPos);
            if (newPos == null) {
                this.logger.log(POILogger.WARN, "Couldn't find the new location of the \"slide\" with id " + id + " that used to be at " + oldPos);
                this.logger.log(POILogger.WARN, "Not updating the position of it, you probably won't be able to find it any more (if you ever could!)");
                newPos = oldPos;
            }
            final Integer dataOffset = this._slideOffsetDataLocation.get(id);
            LittleEndian.putInt(this._ptrData, dataOffset, newPos);
            this._slideLocations.remove(id);
            this._slideLocations.put(id, newPos);
        }
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._ptrData);
    }
}
