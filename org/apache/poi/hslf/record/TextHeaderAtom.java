// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class TextHeaderAtom extends RecordAtom implements ParentAwareRecord
{
    private byte[] _header;
    private static long _type;
    private RecordContainer parentRecord;
    public static final int TITLE_TYPE = 0;
    public static final int BODY_TYPE = 1;
    public static final int NOTES_TYPE = 2;
    public static final int OTHER_TYPE = 4;
    public static final int CENTRE_BODY_TYPE = 5;
    public static final int CENTER_TITLE_TYPE = 6;
    public static final int HALF_BODY_TYPE = 7;
    public static final int QUARTER_BODY_TYPE = 8;
    private int textType;
    
    public int getTextType() {
        return this.textType;
    }
    
    public void setTextType(final int type) {
        this.textType = type;
    }
    
    public RecordContainer getParentRecord() {
        return this.parentRecord;
    }
    
    public void setParentRecord(final RecordContainer record) {
        this.parentRecord = record;
    }
    
    protected TextHeaderAtom(final byte[] source, final int start, int len) {
        if (len < 12) {
            len = 12;
            if (source.length - start < 12) {
                throw new RuntimeException("Not enough data to form a TextHeaderAtom (always 12 bytes long) - found " + (source.length - start));
            }
        }
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this.textType = LittleEndian.getInt(source, start + 8);
    }
    
    public TextHeaderAtom() {
        LittleEndian.putUShort(this._header = new byte[8], 0, 0);
        LittleEndian.putUShort(this._header, 2, (int)TextHeaderAtom._type);
        LittleEndian.putInt(this._header, 4, 4);
        this.textType = 4;
    }
    
    @Override
    public long getRecordType() {
        return TextHeaderAtom._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        Record.writeLittleEndian(this.textType, out);
    }
    
    static {
        TextHeaderAtom._type = 3999L;
    }
}
