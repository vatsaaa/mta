// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class ExMediaAtom extends RecordAtom
{
    public static final int fLoop = 1;
    public static final int fRewind = 2;
    public static final int fNarration = 4;
    private byte[] _header;
    private byte[] _recdata;
    
    protected ExMediaAtom() {
        this._recdata = new byte[8];
        LittleEndian.putShort(this._header = new byte[8], 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._recdata.length);
    }
    
    protected ExMediaAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._recdata = new byte[len - 8], 0, len - 8);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExMediaAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._recdata);
    }
    
    public int getObjectId() {
        return LittleEndian.getInt(this._recdata, 0);
    }
    
    public void setObjectId(final int id) {
        LittleEndian.putInt(this._recdata, 0, id);
    }
    
    public int getMask() {
        return LittleEndian.getInt(this._recdata, 4);
    }
    
    public void setMask(final int mask) {
        LittleEndian.putInt(this._recdata, 4, mask);
    }
    
    public boolean getFlag(final int bit) {
        return (this.getMask() & bit) != 0x0;
    }
    
    public void setFlag(final int bit, final boolean value) {
        int mask = this.getMask();
        if (value) {
            mask |= bit;
        }
        else {
            mask &= ~bit;
        }
        this.setMask(mask);
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        buf.append("ExMediaAtom\n");
        buf.append("\tObjectId: " + this.getObjectId() + "\n");
        buf.append("\tMask    : " + this.getMask() + "\n");
        buf.append("\t  fLoop        : " + this.getFlag(1) + "\n");
        buf.append("\t  fRewind   : " + this.getFlag(2) + "\n");
        buf.append("\t  fNarration    : " + this.getFlag(4) + "\n");
        return buf.toString();
    }
}
