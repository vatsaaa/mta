// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;

public final class ExVideoContainer extends RecordContainer
{
    private byte[] _header;
    private ExMediaAtom mediaAtom;
    private CString pathAtom;
    
    protected ExVideoContainer(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._children = Record.findChildRecords(source, start + 8, len - 8);
        this.findInterestingChildren();
    }
    
    private void findInterestingChildren() {
        if (this._children[0] instanceof ExMediaAtom) {
            this.mediaAtom = (ExMediaAtom)this._children[0];
        }
        else {
            this.logger.log(POILogger.ERROR, "First child record wasn't a ExMediaAtom, was of type " + this._children[0].getRecordType());
        }
        if (this._children[1] instanceof CString) {
            this.pathAtom = (CString)this._children[1];
        }
        else {
            this.logger.log(POILogger.ERROR, "Second child record wasn't a CString, was of type " + this._children[1].getRecordType());
        }
    }
    
    public ExVideoContainer() {
        (this._header = new byte[8])[0] = 15;
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        (this._children = new Record[2])[0] = (this.mediaAtom = new ExMediaAtom());
        this._children[1] = (this.pathAtom = new CString());
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.ExVideoContainer.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        this.writeOut(this._header[0], this._header[1], this.getRecordType(), this._children, out);
    }
    
    public ExMediaAtom getExMediaAtom() {
        return this.mediaAtom;
    }
    
    public CString getPathAtom() {
        return this.pathAtom;
    }
}
