// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class OutlineTextRefAtom extends RecordAtom
{
    private byte[] _header;
    private int _index;
    
    protected OutlineTextRefAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        this._index = LittleEndian.getInt(source, start + 8);
    }
    
    protected OutlineTextRefAtom() {
        this._index = 0;
        LittleEndian.putUShort(this._header = new byte[8], 0, 0);
        LittleEndian.putUShort(this._header, 2, (int)this.getRecordType());
        LittleEndian.putInt(this._header, 4, 4);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.OutlineTextRefAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        final byte[] recdata = new byte[4];
        LittleEndian.putInt(recdata, 0, this._index);
        out.write(recdata);
    }
    
    public void setTextIndex(final int idx) {
        this._index = idx;
    }
    
    public int getTextIndex() {
        return this._index;
    }
}
