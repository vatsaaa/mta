// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.apache.poi.util.StringUtil;
import org.apache.poi.hslf.exceptions.EncryptedPowerPointFileException;
import java.io.InputStream;
import org.apache.poi.hslf.exceptions.OldPowerPointFormatException;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hslf.exceptions.CorruptPowerPointFileException;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class CurrentUserAtom
{
    public static final byte[] atomHeader;
    public static final byte[] headerToken;
    public static final byte[] encHeaderToken;
    public static final byte[] ppt97FileVer;
    private int docFinalVersion;
    private byte docMajorNo;
    private byte docMinorNo;
    private long currentEditOffset;
    private String lastEditUser;
    private long releaseVersion;
    private byte[] _contents;
    
    public int getDocFinalVersion() {
        return this.docFinalVersion;
    }
    
    public byte getDocMajorNo() {
        return this.docMajorNo;
    }
    
    public byte getDocMinorNo() {
        return this.docMinorNo;
    }
    
    public long getReleaseVersion() {
        return this.releaseVersion;
    }
    
    public void setReleaseVersion(final long rv) {
        this.releaseVersion = rv;
    }
    
    public long getCurrentEditOffset() {
        return this.currentEditOffset;
    }
    
    public void setCurrentEditOffset(final long id) {
        this.currentEditOffset = id;
    }
    
    public String getLastEditUsername() {
        return this.lastEditUser;
    }
    
    public void setLastEditUsername(final String u) {
        this.lastEditUser = u;
    }
    
    public CurrentUserAtom() {
        this._contents = new byte[0];
        this.docFinalVersion = 1012;
        this.docMajorNo = 3;
        this.docMinorNo = 0;
        this.releaseVersion = 8L;
        this.currentEditOffset = 0L;
        this.lastEditUser = "Apache POI";
    }
    
    public CurrentUserAtom(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public CurrentUserAtom(final DirectoryNode dir) throws IOException {
        final DocumentEntry docProps = (DocumentEntry)dir.getEntry("Current User");
        if (docProps.getSize() > 131072) {
            throw new CorruptPowerPointFileException("The Current User stream is implausably long. It's normally 28-200 bytes long, but was " + docProps.getSize() + " bytes");
        }
        this._contents = new byte[docProps.getSize()];
        final InputStream in = dir.createDocumentInputStream("Current User");
        in.read(this._contents);
        if (this._contents.length < 28) {
            if (this._contents.length >= 4) {
                final int size = LittleEndian.getInt(this._contents);
                System.err.println(size);
                if (size + 4 == this._contents.length) {
                    throw new OldPowerPointFormatException("Based on the Current User stream, you seem to have supplied a PowerPoint95 file, which isn't supported");
                }
            }
            throw new CorruptPowerPointFileException("The Current User stream must be at least 28 bytes long, but was only " + this._contents.length);
        }
        this.init();
    }
    
    public CurrentUserAtom(final byte[] b) {
        this._contents = b;
        this.init();
    }
    
    private void init() {
        if (this._contents[12] == CurrentUserAtom.encHeaderToken[0] && this._contents[13] == CurrentUserAtom.encHeaderToken[1] && this._contents[14] == CurrentUserAtom.encHeaderToken[2] && this._contents[15] == CurrentUserAtom.encHeaderToken[3]) {
            throw new EncryptedPowerPointFileException("The CurrentUserAtom specifies that the document is encrypted");
        }
        this.currentEditOffset = LittleEndian.getUInt(this._contents, 16);
        this.docFinalVersion = LittleEndian.getUShort(this._contents, 22);
        this.docMajorNo = this._contents[24];
        this.docMinorNo = this._contents[25];
        long usernameLen = LittleEndian.getUShort(this._contents, 20);
        if (usernameLen > 512L) {
            System.err.println("Warning - invalid username length " + usernameLen + " found, treating as if there was no username set");
            usernameLen = 0L;
        }
        if (this._contents.length >= 28 + (int)usernameLen + 4) {
            this.releaseVersion = LittleEndian.getUInt(this._contents, 28 + (int)usernameLen);
        }
        else {
            this.releaseVersion = 0L;
        }
        final int start = 28 + (int)usernameLen + 4;
        final int len = 2 * (int)usernameLen;
        if (this._contents.length >= start + len) {
            final byte[] textBytes = new byte[len];
            System.arraycopy(this._contents, start, textBytes, 0, len);
            this.lastEditUser = StringUtil.getFromUnicodeLE(textBytes);
        }
        else {
            final byte[] textBytes = new byte[(int)usernameLen];
            System.arraycopy(this._contents, 28, textBytes, 0, (int)usernameLen);
            this.lastEditUser = StringUtil.getFromCompressedUnicode(textBytes, 0, (int)usernameLen);
        }
    }
    
    public void writeOut(final OutputStream out) throws IOException {
        final int size = 32 + 3 * this.lastEditUser.length();
        this._contents = new byte[size];
        System.arraycopy(CurrentUserAtom.atomHeader, 0, this._contents, 0, 4);
        final int atomSize = 24 + this.lastEditUser.length();
        LittleEndian.putInt(this._contents, 4, atomSize);
        LittleEndian.putInt(this._contents, 8, 20);
        System.arraycopy(CurrentUserAtom.headerToken, 0, this._contents, 12, 4);
        LittleEndian.putInt(this._contents, 16, (int)this.currentEditOffset);
        final byte[] asciiUN = new byte[this.lastEditUser.length()];
        StringUtil.putCompressedUnicode(this.lastEditUser, asciiUN, 0);
        LittleEndian.putShort(this._contents, 20, (short)asciiUN.length);
        LittleEndian.putShort(this._contents, 22, (short)this.docFinalVersion);
        this._contents[24] = this.docMajorNo;
        this._contents[25] = this.docMinorNo;
        this._contents[26] = 0;
        this._contents[27] = 0;
        System.arraycopy(asciiUN, 0, this._contents, 28, asciiUN.length);
        LittleEndian.putInt(this._contents, 28 + asciiUN.length, (int)this.releaseVersion);
        final byte[] ucUN = new byte[this.lastEditUser.length() * 2];
        StringUtil.putUnicodeLE(this.lastEditUser, ucUN, 0);
        System.arraycopy(ucUN, 0, this._contents, 28 + asciiUN.length + 4, ucUN.length);
        out.write(this._contents);
    }
    
    public void writeToFS(final POIFSFileSystem fs) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        this.writeOut(baos);
        final ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        fs.createDocument(bais, "Current User");
    }
    
    static {
        atomHeader = new byte[] { 0, 0, -10, 15 };
        headerToken = new byte[] { 95, -64, -111, -29 };
        encHeaderToken = new byte[] { -33, -60, -47, -13 };
        ppt97FileVer = new byte[] { 8, 0, -13, 3, 3, 0 };
    }
}
