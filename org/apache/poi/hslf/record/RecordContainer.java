// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hslf.util.MutableByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import org.apache.poi.util.ArrayUtil;

public abstract class RecordContainer extends Record
{
    protected Record[] _children;
    private Boolean changingChildRecordsLock;
    
    public RecordContainer() {
        this.changingChildRecordsLock = Boolean.TRUE;
    }
    
    @Override
    public Record[] getChildRecords() {
        return this._children;
    }
    
    @Override
    public boolean isAnAtom() {
        return false;
    }
    
    private int findChildLocation(final Record child) {
        synchronized (this.changingChildRecordsLock) {
            for (int i = 0; i < this._children.length; ++i) {
                if (this._children[i].equals(child)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    private void appendChild(final Record newChild) {
        synchronized (this.changingChildRecordsLock) {
            final Record[] nc = new Record[this._children.length + 1];
            System.arraycopy(this._children, 0, nc, 0, this._children.length);
            nc[this._children.length] = newChild;
            this._children = nc;
        }
    }
    
    private void addChildAt(final Record newChild, final int position) {
        synchronized (this.changingChildRecordsLock) {
            this.appendChild(newChild);
            this.moveChildRecords(this._children.length - 1, position, 1);
        }
    }
    
    private void moveChildRecords(final int oldLoc, final int newLoc, final int number) {
        if (oldLoc == newLoc) {
            return;
        }
        if (number == 0) {
            return;
        }
        if (oldLoc + number > this._children.length) {
            throw new IllegalArgumentException("Asked to move more records than there are!");
        }
        ArrayUtil.arrayMoveWithin(this._children, oldLoc, newLoc, number);
    }
    
    public Record findFirstOfType(final long type) {
        for (int i = 0; i < this._children.length; ++i) {
            if (this._children[i].getRecordType() == type) {
                return this._children[i];
            }
        }
        return null;
    }
    
    public Record removeChild(final Record ch) {
        Record rm = null;
        final ArrayList<Record> lst = new ArrayList<Record>();
        for (final Record r : this._children) {
            if (r != ch) {
                lst.add(r);
            }
            else {
                rm = r;
            }
        }
        this._children = lst.toArray(new Record[lst.size()]);
        return rm;
    }
    
    public void appendChildRecord(final Record newChild) {
        synchronized (this.changingChildRecordsLock) {
            this.appendChild(newChild);
        }
    }
    
    public void addChildAfter(final Record newChild, final Record after) {
        synchronized (this.changingChildRecordsLock) {
            final int loc = this.findChildLocation(after);
            if (loc == -1) {
                throw new IllegalArgumentException("Asked to add a new child after another record, but that record wasn't one of our children!");
            }
            this.addChildAt(newChild, loc + 1);
        }
    }
    
    public void addChildBefore(final Record newChild, final Record before) {
        synchronized (this.changingChildRecordsLock) {
            final int loc = this.findChildLocation(before);
            if (loc == -1) {
                throw new IllegalArgumentException("Asked to add a new child before another record, but that record wasn't one of our children!");
            }
            this.addChildAt(newChild, loc);
        }
    }
    
    public void moveChildBefore(final Record child, final Record before) {
        this.moveChildrenBefore(child, 1, before);
    }
    
    public void moveChildrenBefore(final Record firstChild, final int number, final Record before) {
        if (number < 1) {
            return;
        }
        synchronized (this.changingChildRecordsLock) {
            final int newLoc = this.findChildLocation(before);
            if (newLoc == -1) {
                throw new IllegalArgumentException("Asked to move children before another record, but that record wasn't one of our children!");
            }
            final int oldLoc = this.findChildLocation(firstChild);
            if (oldLoc == -1) {
                throw new IllegalArgumentException("Asked to move a record that wasn't a child!");
            }
            this.moveChildRecords(oldLoc, newLoc, number);
        }
    }
    
    public void moveChildrenAfter(final Record firstChild, final int number, final Record after) {
        if (number < 1) {
            return;
        }
        synchronized (this.changingChildRecordsLock) {
            int newLoc = this.findChildLocation(after);
            if (newLoc == -1) {
                throw new IllegalArgumentException("Asked to move children before another record, but that record wasn't one of our children!");
            }
            ++newLoc;
            final int oldLoc = this.findChildLocation(firstChild);
            if (oldLoc == -1) {
                throw new IllegalArgumentException("Asked to move a record that wasn't a child!");
            }
            this.moveChildRecords(oldLoc, newLoc, number);
        }
    }
    
    public void setChildRecord(final Record[] records) {
        this._children = records;
    }
    
    public void writeOut(final byte headerA, final byte headerB, final long type, final Record[] children, final OutputStream out) throws IOException {
        if (out instanceof MutableByteArrayOutputStream) {
            final MutableByteArrayOutputStream mout = (MutableByteArrayOutputStream)out;
            final int oldSize = mout.getBytesWritten();
            mout.write(new byte[] { headerA, headerB });
            final byte[] typeB = new byte[2];
            LittleEndian.putShort(typeB, (short)type);
            mout.write(typeB);
            mout.write(new byte[4]);
            for (int i = 0; i < children.length; ++i) {
                children[i].writeOut(mout);
            }
            final int length = mout.getBytesWritten() - oldSize - 8;
            final byte[] size = new byte[4];
            LittleEndian.putInt(size, 0, length);
            mout.overwrite(size, oldSize + 4);
        }
        else {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(new byte[] { headerA, headerB });
            final byte[] typeB2 = new byte[2];
            LittleEndian.putShort(typeB2, (short)type);
            baos.write(typeB2);
            baos.write(new byte[] { 0, 0, 0, 0 });
            for (int j = 0; j < children.length; ++j) {
                children[j].writeOut(baos);
            }
            final byte[] toWrite = baos.toByteArray();
            LittleEndian.putInt(toWrite, 4, toWrite.length - 8);
            out.write(toWrite);
        }
    }
    
    public static void handleParentAwareRecords(final RecordContainer br) {
        for (final Record record : br.getChildRecords()) {
            if (record instanceof ParentAwareRecord) {
                ((ParentAwareRecord)record).setParentRecord(br);
            }
            if (record instanceof RecordContainer) {
                handleParentAwareRecords((RecordContainer)record);
            }
        }
    }
}
