// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.apache.poi.ddf.EscherTextboxRecord;

public final class EscherTextboxWrapper extends RecordContainer
{
    private EscherTextboxRecord _escherRecord;
    private long _type;
    private int shapeId;
    
    public EscherTextboxRecord getEscherRecord() {
        return this._escherRecord;
    }
    
    public EscherTextboxWrapper(final EscherTextboxRecord textbox) {
        this._escherRecord = textbox;
        this._type = this._escherRecord.getRecordId();
        final byte[] data = this._escherRecord.getData();
        this._children = Record.findChildRecords(data, 0, data.length);
    }
    
    public EscherTextboxWrapper() {
        (this._escherRecord = new EscherTextboxRecord()).setRecordId((short)(-4083));
        this._escherRecord.setOptions((short)15);
        this._children = new Record[0];
    }
    
    @Override
    public long getRecordType() {
        return this._type;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < this._children.length; ++i) {
            this._children[i].writeOut(baos);
        }
        final byte[] data = baos.toByteArray();
        this._escherRecord.setData(data);
    }
    
    public int getShapeId() {
        return this.shapeId;
    }
    
    public void setShapeId(final int id) {
        this.shapeId = id;
    }
}
