// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;

public final class HeadersFootersAtom extends RecordAtom
{
    public static final int fHasDate = 1;
    public static final int fHasTodayDate = 2;
    public static final int fHasUserDate = 4;
    public static final int fHasSlideNumber = 8;
    public static final int fHasHeader = 16;
    public static final int fHasFooter = 32;
    private byte[] _header;
    private byte[] _recdata;
    
    protected HeadersFootersAtom(final byte[] source, final int start, final int len) {
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._recdata = new byte[len - 8], 0, len - 8);
    }
    
    public HeadersFootersAtom() {
        this._recdata = new byte[4];
        LittleEndian.putShort(this._header = new byte[8], 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._recdata.length);
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.HeadersFootersAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._recdata);
    }
    
    public int getFormatId() {
        return LittleEndian.getShort(this._recdata, 0);
    }
    
    public void setFormatId(final int formatId) {
        LittleEndian.putUShort(this._recdata, 0, formatId);
    }
    
    public int getMask() {
        return LittleEndian.getShort(this._recdata, 2);
    }
    
    public void setMask(final int mask) {
        LittleEndian.putUShort(this._recdata, 2, mask);
    }
    
    public boolean getFlag(final int bit) {
        return (this.getMask() & bit) != 0x0;
    }
    
    public void setFlag(final int bit, final boolean value) {
        int mask = this.getMask();
        if (value) {
            mask |= bit;
        }
        else {
            mask &= ~bit;
        }
        this.setMask(mask);
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        buf.append("HeadersFootersAtom\n");
        buf.append("\tFormatId: " + this.getFormatId() + "\n");
        buf.append("\tMask    : " + this.getMask() + "\n");
        buf.append("\t  fHasDate        : " + this.getFlag(1) + "\n");
        buf.append("\t  fHasTodayDate   : " + this.getFlag(2) + "\n");
        buf.append("\t  fHasUserDate    : " + this.getFlag(4) + "\n");
        buf.append("\t  fHasSlideNumber : " + this.getFlag(8) + "\n");
        buf.append("\t  fHasHeader      : " + this.getFlag(16) + "\n");
        buf.append("\t  fHasFooter      : " + this.getFlag(32) + "\n");
        return buf.toString();
    }
}
