// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hslf.record;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.LittleEndian;

public final class TextRulerAtom extends RecordAtom
{
    private byte[] _header;
    private byte[] _data;
    private int defaultTabSize;
    private int numLevels;
    private int[] tabStops;
    private int[] bulletOffsets;
    private int[] textOffsets;
    
    public TextRulerAtom() {
        this.bulletOffsets = new int[5];
        this.textOffsets = new int[5];
        this._header = new byte[8];
        this._data = new byte[0];
        LittleEndian.putShort(this._header, 2, (short)this.getRecordType());
        LittleEndian.putInt(this._header, 4, this._data.length);
    }
    
    protected TextRulerAtom(final byte[] source, final int start, final int len) {
        this.bulletOffsets = new int[5];
        this.textOffsets = new int[5];
        System.arraycopy(source, start, this._header = new byte[8], 0, 8);
        System.arraycopy(source, start + 8, this._data = new byte[len - 8], 0, len - 8);
        try {
            this.read();
        }
        catch (Exception e) {
            this.logger.log(POILogger.ERROR, "Failed to parse TextRulerAtom: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    @Override
    public long getRecordType() {
        return RecordTypes.TextRulerAtom.typeID;
    }
    
    @Override
    public void writeOut(final OutputStream out) throws IOException {
        out.write(this._header);
        out.write(this._data);
    }
    
    private void read() {
        int pos = 0;
        final short mask = LittleEndian.getShort(this._data);
        pos += 4;
        final int[] bits = { 1, 0, 2, 3, 8, 4, 9, 5, 10, 6, 11, 7, 12 };
        for (int i = 0; i < bits.length; ++i) {
            if ((mask & 1 << bits[i]) != 0x0) {
                switch (bits[i]) {
                    case 0: {
                        this.defaultTabSize = LittleEndian.getShort(this._data, pos);
                        pos += 2;
                        break;
                    }
                    case 1: {
                        this.numLevels = LittleEndian.getShort(this._data, pos);
                        pos += 2;
                        break;
                    }
                    case 2: {
                        final short val = LittleEndian.getShort(this._data, pos);
                        pos += 2;
                        this.tabStops = new int[val * 2];
                        for (int j = 0; j < this.tabStops.length; ++j) {
                            this.tabStops[j] = LittleEndian.getUShort(this._data, pos);
                            pos += 2;
                        }
                        break;
                    }
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7: {
                        final short val = LittleEndian.getShort(this._data, pos);
                        pos += 2;
                        this.bulletOffsets[bits[i] - 3] = val;
                        break;
                    }
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12: {
                        final short val = LittleEndian.getShort(this._data, pos);
                        pos += 2;
                        this.textOffsets[bits[i] - 8] = val;
                        break;
                    }
                }
            }
        }
    }
    
    public int getDefaultTabSize() {
        return this.defaultTabSize;
    }
    
    public int getNumberOfLevels() {
        return this.numLevels;
    }
    
    public int[] getTabStops() {
        return this.tabStops;
    }
    
    public int[] getTextOffsets() {
        return this.textOffsets;
    }
    
    public int[] getBulletOffsets() {
        return this.bulletOffsets;
    }
    
    public static TextRulerAtom getParagraphInstance() {
        final byte[] data = { 0, 0, -90, 15, 10, 0, 0, 0, 16, 3, 0, 0, -7, 0, 65, 1, 65, 1 };
        final TextRulerAtom ruler = new TextRulerAtom(data, 0, data.length);
        return ruler;
    }
    
    public void setParagraphIndent(final short tetxOffset, final short bulletOffset) {
        LittleEndian.putShort(this._data, 4, tetxOffset);
        LittleEndian.putShort(this._data, 6, bulletOffset);
        LittleEndian.putShort(this._data, 8, bulletOffset);
    }
}
