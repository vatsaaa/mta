// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.util;

import org.apache.poi.util.POILogFactory;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import javax.imageio.stream.ImageInputStream;
import java.io.IOException;
import javax.imageio.ImageReader;
import javax.imageio.ImageIO;
import java.awt.Dimension;
import java.io.InputStream;
import org.apache.poi.util.POILogger;

public class ImageUtils
{
    private static final POILogger logger;
    public static final int PIXEL_DPI = 96;
    
    public static Dimension getImageDimension(final InputStream is, final int type) {
        final Dimension size = new Dimension();
        switch (type) {
            case 5:
            case 6:
            case 7: {
                try {
                    final ImageInputStream iis = ImageIO.createImageInputStream(is);
                    final Iterator i = ImageIO.getImageReaders(iis);
                    final ImageReader r = i.next();
                    r.setInput(iis);
                    final BufferedImage img = r.read(0);
                    final int[] dpi = getResolution(r);
                    if (dpi[0] == 0) {
                        dpi[0] = 96;
                    }
                    if (dpi[1] == 0) {
                        dpi[1] = 96;
                    }
                    size.width = img.getWidth() * 96 / dpi[0];
                    size.height = img.getHeight() * 96 / dpi[1];
                    r.dispose();
                    iis.close();
                }
                catch (IOException e) {
                    ImageUtils.logger.log(POILogger.WARN, e);
                }
                break;
            }
            default: {
                ImageUtils.logger.log(POILogger.WARN, "Only JPEG, PNG and DIB pictures can be automatically sized");
                break;
            }
        }
        return size;
    }
    
    public static int[] getResolution(final ImageReader r) throws IOException {
        int hdpi = 96;
        int vdpi = 96;
        final double mm2inch = 25.4;
        final Element node = (Element)r.getImageMetadata(0).getAsTree("javax_imageio_1.0");
        NodeList lst = node.getElementsByTagName("HorizontalPixelSize");
        if (lst != null && lst.getLength() == 1) {
            hdpi = (int)(mm2inch / Float.parseFloat(((Element)lst.item(0)).getAttribute("value")));
        }
        lst = node.getElementsByTagName("VerticalPixelSize");
        if (lst != null && lst.getLength() == 1) {
            vdpi = (int)(mm2inch / Float.parseFloat(((Element)lst.item(0)).getAttribute("value")));
        }
        return new int[] { hdpi, vdpi };
    }
    
    static {
        logger = POILogFactory.getLogger(ImageUtils.class);
    }
}
