// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.usermodel;

public interface Picture
{
    void resize();
    
    void resize(final double p0);
    
    ClientAnchor getPreferredSize();
    
    PictureData getPictureData();
}
