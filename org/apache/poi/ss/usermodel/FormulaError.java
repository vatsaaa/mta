// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.usermodel;

import java.util.HashMap;
import java.util.Map;

public enum FormulaError
{
    NULL(0, "#NULL!"), 
    DIV0(7, "#DIV/0!"), 
    VALUE(15, "#VALUE!"), 
    REF(23, "#REF!"), 
    NAME(29, "#NAME?"), 
    NUM(36, "#NUM!"), 
    NA(42, "#N/A");
    
    private byte type;
    private String repr;
    private static Map<String, FormulaError> smap;
    private static Map<Byte, FormulaError> imap;
    
    private FormulaError(final int type, final String repr) {
        this.type = (byte)type;
        this.repr = repr;
    }
    
    public byte getCode() {
        return this.type;
    }
    
    public String getString() {
        return this.repr;
    }
    
    public static FormulaError forInt(final byte type) {
        final FormulaError err = FormulaError.imap.get(type);
        if (err == null) {
            throw new IllegalArgumentException("Unknown error type: " + type);
        }
        return err;
    }
    
    public static FormulaError forString(final String code) {
        final FormulaError err = FormulaError.smap.get(code);
        if (err == null) {
            throw new IllegalArgumentException("Unknown error code: " + code);
        }
        return err;
    }
    
    static {
        FormulaError.smap = new HashMap<String, FormulaError>();
        FormulaError.imap = new HashMap<Byte, FormulaError>();
        for (final FormulaError error : values()) {
            FormulaError.imap.put(error.getCode(), error);
            FormulaError.smap.put(error.getString(), error);
        }
    }
}
