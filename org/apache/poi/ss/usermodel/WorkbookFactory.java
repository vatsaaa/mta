// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.usermodel;

import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import java.io.FileNotFoundException;
import java.io.File;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.POIXMLDocument;
import java.io.PushbackInputStream;
import java.io.InputStream;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class WorkbookFactory
{
    public static Workbook create(final POIFSFileSystem fs) throws IOException {
        return new HSSFWorkbook(fs);
    }
    
    public static Workbook create(final NPOIFSFileSystem fs) throws IOException {
        return new HSSFWorkbook(fs.getRoot(), true);
    }
    
    public static Workbook create(final OPCPackage pkg) throws IOException {
        return new XSSFWorkbook(pkg);
    }
    
    public static Workbook create(InputStream inp) throws IOException, InvalidFormatException {
        if (!inp.markSupported()) {
            inp = new PushbackInputStream(inp, 8);
        }
        if (POIFSFileSystem.hasPOIFSHeader(inp)) {
            return new HSSFWorkbook(inp);
        }
        if (POIXMLDocument.hasOOXMLHeader(inp)) {
            return new XSSFWorkbook(OPCPackage.open(inp));
        }
        throw new IllegalArgumentException("Your InputStream was neither an OLE2 stream, nor an OOXML stream");
    }
    
    public static Workbook create(final File file) throws IOException, InvalidFormatException {
        if (!file.exists()) {
            throw new FileNotFoundException(file.toString());
        }
        try {
            final NPOIFSFileSystem fs = new NPOIFSFileSystem(file);
            return new HSSFWorkbook(fs.getRoot(), true);
        }
        catch (OfficeXmlFileException e) {
            final OPCPackage pkg = OPCPackage.openOrCreate(file);
            return new XSSFWorkbook(pkg);
        }
    }
}
