// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.eval;

public abstract class RefEvalBase implements RefEval
{
    private final int _rowIndex;
    private final int _columnIndex;
    
    protected RefEvalBase(final int rowIndex, final int columnIndex) {
        this._rowIndex = rowIndex;
        this._columnIndex = columnIndex;
    }
    
    public final int getRow() {
        return this._rowIndex;
    }
    
    public final int getColumn() {
        return this._columnIndex;
    }
}
