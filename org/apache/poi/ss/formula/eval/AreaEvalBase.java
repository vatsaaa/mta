// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.eval;

import org.apache.poi.ss.formula.ptg.AreaI;

public abstract class AreaEvalBase implements AreaEval
{
    private final int _firstColumn;
    private final int _firstRow;
    private final int _lastColumn;
    private final int _lastRow;
    private final int _nColumns;
    private final int _nRows;
    
    protected AreaEvalBase(final int firstRow, final int firstColumn, final int lastRow, final int lastColumn) {
        this._firstColumn = firstColumn;
        this._firstRow = firstRow;
        this._lastColumn = lastColumn;
        this._lastRow = lastRow;
        this._nColumns = this._lastColumn - this._firstColumn + 1;
        this._nRows = this._lastRow - this._firstRow + 1;
    }
    
    protected AreaEvalBase(final AreaI ptg) {
        this._firstRow = ptg.getFirstRow();
        this._firstColumn = ptg.getFirstColumn();
        this._lastRow = ptg.getLastRow();
        this._lastColumn = ptg.getLastColumn();
        this._nColumns = this._lastColumn - this._firstColumn + 1;
        this._nRows = this._lastRow - this._firstRow + 1;
    }
    
    public final int getFirstColumn() {
        return this._firstColumn;
    }
    
    public final int getFirstRow() {
        return this._firstRow;
    }
    
    public final int getLastColumn() {
        return this._lastColumn;
    }
    
    public final int getLastRow() {
        return this._lastRow;
    }
    
    public final ValueEval getAbsoluteValue(final int row, final int col) {
        final int rowOffsetIx = row - this._firstRow;
        final int colOffsetIx = col - this._firstColumn;
        if (rowOffsetIx < 0 || rowOffsetIx >= this._nRows) {
            throw new IllegalArgumentException("Specified row index (" + row + ") is outside the allowed range (" + this._firstRow + ".." + this._lastRow + ")");
        }
        if (colOffsetIx < 0 || colOffsetIx >= this._nColumns) {
            throw new IllegalArgumentException("Specified column index (" + col + ") is outside the allowed range (" + this._firstColumn + ".." + col + ")");
        }
        return this.getRelativeValue(rowOffsetIx, colOffsetIx);
    }
    
    public final boolean contains(final int row, final int col) {
        return this._firstRow <= row && this._lastRow >= row && this._firstColumn <= col && this._lastColumn >= col;
    }
    
    public final boolean containsRow(final int row) {
        return this._firstRow <= row && this._lastRow >= row;
    }
    
    public final boolean containsColumn(final int col) {
        return this._firstColumn <= col && this._lastColumn >= col;
    }
    
    public final boolean isColumn() {
        return this._firstColumn == this._lastColumn;
    }
    
    public final boolean isRow() {
        return this._firstRow == this._lastRow;
    }
    
    public int getHeight() {
        return this._lastRow - this._firstRow + 1;
    }
    
    public final ValueEval getValue(final int row, final int col) {
        return this.getRelativeValue(row, col);
    }
    
    public abstract ValueEval getRelativeValue(final int p0, final int p1);
    
    public int getWidth() {
        return this._lastColumn - this._firstColumn + 1;
    }
    
    public boolean isSubTotal(final int rowIndex, final int columnIndex) {
        return false;
    }
}
