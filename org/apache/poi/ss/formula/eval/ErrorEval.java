// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.eval;

import org.apache.poi.ss.usermodel.ErrorConstants;

public final class ErrorEval implements ValueEval
{
    private static final ErrorConstants EC;
    public static final ErrorEval NULL_INTERSECTION;
    public static final ErrorEval DIV_ZERO;
    public static final ErrorEval VALUE_INVALID;
    public static final ErrorEval REF_INVALID;
    public static final ErrorEval NAME_INVALID;
    public static final ErrorEval NUM_ERROR;
    public static final ErrorEval NA;
    private static final int CIRCULAR_REF_ERROR_CODE = -60;
    private static final int FUNCTION_NOT_IMPLEMENTED_CODE = -30;
    public static final ErrorEval CIRCULAR_REF_ERROR;
    private int _errorCode;
    
    public static ErrorEval valueOf(final int errorCode) {
        switch (errorCode) {
            case 0: {
                return ErrorEval.NULL_INTERSECTION;
            }
            case 7: {
                return ErrorEval.DIV_ZERO;
            }
            case 15: {
                return ErrorEval.VALUE_INVALID;
            }
            case 23: {
                return ErrorEval.REF_INVALID;
            }
            case 29: {
                return ErrorEval.NAME_INVALID;
            }
            case 36: {
                return ErrorEval.NUM_ERROR;
            }
            case 42: {
                return ErrorEval.NA;
            }
            case -60: {
                return ErrorEval.CIRCULAR_REF_ERROR;
            }
            default: {
                throw new RuntimeException("Unexpected error code (" + errorCode + ")");
            }
        }
    }
    
    public static String getText(final int errorCode) {
        if (ErrorConstants.isValidCode(errorCode)) {
            return ErrorConstants.getText(errorCode);
        }
        switch (errorCode) {
            case -60: {
                return "~CIRCULAR~REF~";
            }
            case -30: {
                return "~FUNCTION~NOT~IMPLEMENTED~";
            }
            default: {
                return "~non~std~err(" + errorCode + ")~";
            }
        }
    }
    
    private ErrorEval(final int errorCode) {
        this._errorCode = errorCode;
    }
    
    public int getErrorCode() {
        return this._errorCode;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer(64);
        sb.append(this.getClass().getName()).append(" [");
        sb.append(getText(this._errorCode));
        sb.append("]");
        return sb.toString();
    }
    
    static {
        EC = null;
        final ErrorConstants ec = ErrorEval.EC;
        NULL_INTERSECTION = new ErrorEval(0);
        final ErrorConstants ec2 = ErrorEval.EC;
        DIV_ZERO = new ErrorEval(7);
        final ErrorConstants ec3 = ErrorEval.EC;
        VALUE_INVALID = new ErrorEval(15);
        final ErrorConstants ec4 = ErrorEval.EC;
        REF_INVALID = new ErrorEval(23);
        final ErrorConstants ec5 = ErrorEval.EC;
        NAME_INVALID = new ErrorEval(29);
        final ErrorConstants ec6 = ErrorEval.EC;
        NUM_ERROR = new ErrorEval(36);
        final ErrorConstants ec7 = ErrorEval.EC;
        NA = new ErrorEval(42);
        CIRCULAR_REF_ERROR = new ErrorEval(-60);
    }
}
