// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.eval;

public final class NotImplementedException extends RuntimeException
{
    public NotImplementedException(final String message) {
        super(message);
    }
    
    public NotImplementedException(final String message, final NotImplementedException cause) {
        super(message, cause);
    }
}
