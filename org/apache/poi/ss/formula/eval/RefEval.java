// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.eval;

public interface RefEval extends ValueEval
{
    ValueEval getInnerValueEval();
    
    int getColumn();
    
    int getRow();
    
    AreaEval offset(final int p0, final int p1, final int p2, final int p3);
}
