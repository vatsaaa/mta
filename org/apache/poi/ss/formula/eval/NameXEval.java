// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.eval;

import org.apache.poi.ss.formula.ptg.NameXPtg;

public final class NameXEval implements ValueEval
{
    private final NameXPtg _ptg;
    
    public NameXEval(final NameXPtg ptg) {
        this._ptg = ptg;
    }
    
    public NameXPtg getPtg() {
        return this._ptg;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer(64);
        sb.append(this.getClass().getName()).append(" [");
        sb.append(this._ptg.getSheetRefIndex()).append(", ").append(this._ptg.getNameIndex());
        sb.append("]");
        return sb.toString();
    }
}
