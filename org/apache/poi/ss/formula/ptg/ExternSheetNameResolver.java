// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.ptg;

import org.apache.poi.ss.formula.EvaluationWorkbook;
import org.apache.poi.ss.formula.SheetNameFormatter;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;

final class ExternSheetNameResolver
{
    private ExternSheetNameResolver() {
    }
    
    public static String prependSheetName(final FormulaRenderingWorkbook book, final int field_1_index_extern_sheet, final String cellRefText) {
        final EvaluationWorkbook.ExternalSheet externalSheet = book.getExternalSheet(field_1_index_extern_sheet);
        StringBuffer sb;
        if (externalSheet != null) {
            final String wbName = externalSheet.getWorkbookName();
            final String sheetName = externalSheet.getSheetName();
            sb = new StringBuffer(wbName.length() + sheetName.length() + cellRefText.length() + 4);
            SheetNameFormatter.appendFormat(sb, wbName, sheetName);
        }
        else {
            final String sheetName2 = book.getSheetNameByExternSheet(field_1_index_extern_sheet);
            sb = new StringBuffer(sheetName2.length() + cellRefText.length() + 4);
            if (sheetName2.length() < 1) {
                sb.append("#REF");
            }
            else {
                SheetNameFormatter.appendFormat(sb, sheetName2);
            }
        }
        sb.append('!');
        sb.append(cellRefText);
        return sb.toString();
    }
}
