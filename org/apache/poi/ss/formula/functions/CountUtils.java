// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula.functions;

import org.apache.poi.ss.formula.eval.RefEval;
import org.apache.poi.ss.formula.eval.ValueEval;
import org.apache.poi.ss.formula.TwoDEval;

final class CountUtils
{
    private CountUtils() {
    }
    
    public static int countMatchingCellsInArea(final TwoDEval areaEval, final I_MatchPredicate criteriaPredicate) {
        int result = 0;
        final int height = areaEval.getHeight();
        final int width = areaEval.getWidth();
        for (int rrIx = 0; rrIx < height; ++rrIx) {
            for (int rcIx = 0; rcIx < width; ++rcIx) {
                final ValueEval ve = areaEval.getValue(rrIx, rcIx);
                if (criteriaPredicate instanceof I_MatchAreaPredicate) {
                    final I_MatchAreaPredicate areaPredicate = (I_MatchAreaPredicate)criteriaPredicate;
                    if (!areaPredicate.matches(areaEval, rrIx, rcIx)) {
                        continue;
                    }
                }
                if (criteriaPredicate.matches(ve)) {
                    ++result;
                }
            }
        }
        return result;
    }
    
    public static int countMatchingCell(final RefEval refEval, final I_MatchPredicate criteriaPredicate) {
        if (criteriaPredicate.matches(refEval.getInnerValueEval())) {
            return 1;
        }
        return 0;
    }
    
    public static int countArg(final ValueEval eval, final I_MatchPredicate criteriaPredicate) {
        if (eval == null) {
            throw new IllegalArgumentException("eval must not be null");
        }
        if (eval instanceof TwoDEval) {
            return countMatchingCellsInArea((TwoDEval)eval, criteriaPredicate);
        }
        if (eval instanceof RefEval) {
            return countMatchingCell((RefEval)eval, criteriaPredicate);
        }
        return criteriaPredicate.matches(eval) ? 1 : 0;
    }
    
    public interface I_MatchAreaPredicate extends I_MatchPredicate
    {
        boolean matches(final TwoDEval p0, final int p1, final int p2);
    }
    
    public interface I_MatchPredicate
    {
        boolean matches(final ValueEval p0);
    }
}
