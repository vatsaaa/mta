// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula;

import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.Area3DPtg;
import org.apache.poi.ss.formula.ptg.Ref3DPtg;
import org.apache.poi.ss.formula.eval.NameXEval;
import org.apache.poi.ss.formula.ptg.NameXPtg;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.formula.eval.ValueEval;
import org.apache.poi.ss.formula.functions.FreeRefFunction;

public final class OperationEvaluationContext
{
    public static final FreeRefFunction UDF;
    private final EvaluationWorkbook _workbook;
    private final int _sheetIndex;
    private final int _rowIndex;
    private final int _columnIndex;
    private final EvaluationTracker _tracker;
    private final WorkbookEvaluator _bookEvaluator;
    
    public OperationEvaluationContext(final WorkbookEvaluator bookEvaluator, final EvaluationWorkbook workbook, final int sheetIndex, final int srcRowNum, final int srcColNum, final EvaluationTracker tracker) {
        this._bookEvaluator = bookEvaluator;
        this._workbook = workbook;
        this._sheetIndex = sheetIndex;
        this._rowIndex = srcRowNum;
        this._columnIndex = srcColNum;
        this._tracker = tracker;
    }
    
    public EvaluationWorkbook getWorkbook() {
        return this._workbook;
    }
    
    public int getRowIndex() {
        return this._rowIndex;
    }
    
    public int getColumnIndex() {
        return this._columnIndex;
    }
    
    SheetRefEvaluator createExternSheetRefEvaluator(final ExternSheetReferenceToken ptg) {
        return this.createExternSheetRefEvaluator(ptg.getExternSheetIndex());
    }
    
    SheetRefEvaluator createExternSheetRefEvaluator(final int externSheetIndex) {
        final EvaluationWorkbook.ExternalSheet externalSheet = this._workbook.getExternalSheet(externSheetIndex);
        int otherSheetIndex;
        WorkbookEvaluator targetEvaluator;
        if (externalSheet == null) {
            otherSheetIndex = this._workbook.convertFromExternSheetIndex(externSheetIndex);
            targetEvaluator = this._bookEvaluator;
        }
        else {
            final String workbookName = externalSheet.getWorkbookName();
            try {
                targetEvaluator = this._bookEvaluator.getOtherWorkbookEvaluator(workbookName);
            }
            catch (CollaboratingWorkbooksEnvironment.WorkbookNotFoundException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
            otherSheetIndex = targetEvaluator.getSheetIndex(externalSheet.getSheetName());
            if (otherSheetIndex < 0) {
                throw new RuntimeException("Invalid sheet name '" + externalSheet.getSheetName() + "' in bool '" + workbookName + "'.");
            }
        }
        return new SheetRefEvaluator(targetEvaluator, this._tracker, otherSheetIndex);
    }
    
    private SheetRefEvaluator createExternSheetRefEvaluator(final String workbookName, final String sheetName) {
        WorkbookEvaluator targetEvaluator;
        if (workbookName == null) {
            targetEvaluator = this._bookEvaluator;
        }
        else {
            if (sheetName == null) {
                throw new IllegalArgumentException("sheetName must not be null if workbookName is provided");
            }
            try {
                targetEvaluator = this._bookEvaluator.getOtherWorkbookEvaluator(workbookName);
            }
            catch (CollaboratingWorkbooksEnvironment.WorkbookNotFoundException e) {
                return null;
            }
        }
        final int otherSheetIndex = (sheetName == null) ? this._sheetIndex : targetEvaluator.getSheetIndex(sheetName);
        if (otherSheetIndex < 0) {
            return null;
        }
        return new SheetRefEvaluator(targetEvaluator, this._tracker, otherSheetIndex);
    }
    
    public SheetRefEvaluator getRefEvaluatorForCurrentSheet() {
        return new SheetRefEvaluator(this._bookEvaluator, this._tracker, this._sheetIndex);
    }
    
    public ValueEval getDynamicReference(final String workbookName, final String sheetName, final String refStrPart1, final String refStrPart2, final boolean isA1Style) {
        if (!isA1Style) {
            throw new RuntimeException("R1C1 style not supported yet");
        }
        final SheetRefEvaluator sre = this.createExternSheetRefEvaluator(workbookName, sheetName);
        if (sre == null) {
            return ErrorEval.REF_INVALID;
        }
        final SpreadsheetVersion ssVersion = ((FormulaParsingWorkbook)this._workbook).getSpreadsheetVersion();
        final CellReference.NameType part1refType = classifyCellReference(refStrPart1, ssVersion);
        switch (part1refType) {
            case BAD_CELL_OR_NAMED_RANGE: {
                return ErrorEval.REF_INVALID;
            }
            case NAMED_RANGE: {
                final EvaluationName nm = ((FormulaParsingWorkbook)this._workbook).getName(refStrPart1, this._sheetIndex);
                if (!nm.isRange()) {
                    throw new RuntimeException("Specified name '" + refStrPart1 + "' is not a range as expected.");
                }
                return this._bookEvaluator.evaluateNameFormula(nm.getNameDefinition(), this);
            }
            default: {
                if (refStrPart2 == null) {
                    switch (part1refType) {
                        case COLUMN:
                        case ROW: {
                            return ErrorEval.REF_INVALID;
                        }
                        case CELL: {
                            final CellReference cr = new CellReference(refStrPart1);
                            return new LazyRefEval(cr.getRow(), cr.getCol(), sre);
                        }
                        default: {
                            throw new IllegalStateException("Unexpected reference classification of '" + refStrPart1 + "'.");
                        }
                    }
                }
                else {
                    final CellReference.NameType part2refType = classifyCellReference(refStrPart1, ssVersion);
                    switch (part2refType) {
                        case BAD_CELL_OR_NAMED_RANGE: {
                            return ErrorEval.REF_INVALID;
                        }
                        case NAMED_RANGE: {
                            throw new RuntimeException("Cannot evaluate '" + refStrPart1 + "'. Indirect evaluation of defined names not supported yet");
                        }
                        default: {
                            if (part2refType != part1refType) {
                                return ErrorEval.REF_INVALID;
                            }
                            int firstRow = 0;
                            int lastRow = 0;
                            int firstCol = 0;
                            int lastCol = 0;
                            switch (part1refType) {
                                case COLUMN: {
                                    firstRow = 0;
                                    lastRow = ssVersion.getLastRowIndex();
                                    firstCol = parseColRef(refStrPart1);
                                    lastCol = parseColRef(refStrPart2);
                                    break;
                                }
                                case ROW: {
                                    firstCol = 0;
                                    lastCol = ssVersion.getLastColumnIndex();
                                    firstRow = parseRowRef(refStrPart1);
                                    lastRow = parseRowRef(refStrPart2);
                                    break;
                                }
                                case CELL: {
                                    CellReference cr2 = new CellReference(refStrPart1);
                                    firstRow = cr2.getRow();
                                    firstCol = cr2.getCol();
                                    cr2 = new CellReference(refStrPart2);
                                    lastRow = cr2.getRow();
                                    lastCol = cr2.getCol();
                                    break;
                                }
                                default: {
                                    throw new IllegalStateException("Unexpected reference classification of '" + refStrPart1 + "'.");
                                }
                            }
                            return new LazyAreaEval(firstRow, firstCol, lastRow, lastCol, sre);
                        }
                    }
                }
                break;
            }
        }
    }
    
    private static int parseRowRef(final String refStrPart) {
        return CellReference.convertColStringToIndex(refStrPart);
    }
    
    private static int parseColRef(final String refStrPart) {
        return Integer.parseInt(refStrPart) - 1;
    }
    
    private static CellReference.NameType classifyCellReference(final String str, final SpreadsheetVersion ssVersion) {
        final int len = str.length();
        if (len < 1) {
            return CellReference.NameType.BAD_CELL_OR_NAMED_RANGE;
        }
        return CellReference.classifyCellReference(str, ssVersion);
    }
    
    public FreeRefFunction findUserDefinedFunction(final String functionName) {
        return this._bookEvaluator.findUserDefinedFunction(functionName);
    }
    
    public ValueEval getRefEval(final int rowIndex, final int columnIndex) {
        final SheetRefEvaluator sre = this.getRefEvaluatorForCurrentSheet();
        return new LazyRefEval(rowIndex, columnIndex, sre);
    }
    
    public ValueEval getRef3DEval(final int rowIndex, final int columnIndex, final int extSheetIndex) {
        final SheetRefEvaluator sre = this.createExternSheetRefEvaluator(extSheetIndex);
        return new LazyRefEval(rowIndex, columnIndex, sre);
    }
    
    public ValueEval getAreaEval(final int firstRowIndex, final int firstColumnIndex, final int lastRowIndex, final int lastColumnIndex) {
        final SheetRefEvaluator sre = this.getRefEvaluatorForCurrentSheet();
        return new LazyAreaEval(firstRowIndex, firstColumnIndex, lastRowIndex, lastColumnIndex, sre);
    }
    
    public ValueEval getArea3DEval(final int firstRowIndex, final int firstColumnIndex, final int lastRowIndex, final int lastColumnIndex, final int extSheetIndex) {
        final SheetRefEvaluator sre = this.createExternSheetRefEvaluator(extSheetIndex);
        return new LazyAreaEval(firstRowIndex, firstColumnIndex, lastRowIndex, lastColumnIndex, sre);
    }
    
    public ValueEval getNameXEval(final NameXPtg nameXPtg) {
        final EvaluationWorkbook.ExternalSheet externSheet = this._workbook.getExternalSheet(nameXPtg.getSheetRefIndex());
        if (externSheet == null) {
            return new NameXEval(nameXPtg);
        }
        final String workbookName = externSheet.getWorkbookName();
        final EvaluationWorkbook.ExternalName externName = this._workbook.getExternalName(nameXPtg.getSheetRefIndex(), nameXPtg.getNameIndex());
        try {
            final WorkbookEvaluator refWorkbookEvaluator = this._bookEvaluator.getOtherWorkbookEvaluator(workbookName);
            final EvaluationName evaluationName = refWorkbookEvaluator.getName(externName.getName(), externName.getIx() - 1);
            if (evaluationName != null && evaluationName.hasFormula()) {
                if (evaluationName.getNameDefinition().length > 1) {
                    throw new RuntimeException("Complex name formulas not supported yet");
                }
                final Ptg ptg = evaluationName.getNameDefinition()[0];
                if (ptg instanceof Ref3DPtg) {
                    final Ref3DPtg ref3D = (Ref3DPtg)ptg;
                    final int sheetIndex = refWorkbookEvaluator.getSheetIndexByExternIndex(ref3D.getExternSheetIndex());
                    final String sheetName = refWorkbookEvaluator.getSheetName(sheetIndex);
                    final SheetRefEvaluator sre = this.createExternSheetRefEvaluator(workbookName, sheetName);
                    return new LazyRefEval(ref3D.getRow(), ref3D.getColumn(), sre);
                }
                if (ptg instanceof Area3DPtg) {
                    final Area3DPtg area3D = (Area3DPtg)ptg;
                    final int sheetIndex = refWorkbookEvaluator.getSheetIndexByExternIndex(area3D.getExternSheetIndex());
                    final String sheetName = refWorkbookEvaluator.getSheetName(sheetIndex);
                    final SheetRefEvaluator sre = this.createExternSheetRefEvaluator(workbookName, sheetName);
                    return new LazyAreaEval(area3D.getFirstRow(), area3D.getFirstColumn(), area3D.getLastRow(), area3D.getLastColumn(), sre);
                }
            }
            return ErrorEval.REF_INVALID;
        }
        catch (CollaboratingWorkbooksEnvironment.WorkbookNotFoundException wnfe) {
            return ErrorEval.REF_INVALID;
        }
    }
    
    static {
        UDF = UserDefinedFunction.instance;
    }
}
