// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ss.formula;

import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.formula.ptg.NameXPtg;

public interface FormulaParsingWorkbook
{
    EvaluationName getName(final String p0, final int p1);
    
    NameXPtg getNameXPtg(final String p0);
    
    int getExternalSheetIndex(final String p0);
    
    int getExternalSheetIndex(final String p0, final String p1);
    
    SpreadsheetVersion getSpreadsheetVersion();
}
