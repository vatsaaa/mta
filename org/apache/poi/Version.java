// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

public class Version
{
    private static final String VERSION_STRING = "3.8";
    private static final String RELEASE_DATE = "20120326";
    
    public static String getVersion() {
        return "3.8";
    }
    
    public static String getReleaseDate() {
        return "20120326";
    }
    
    public static String getProduct() {
        return "POI";
    }
    
    public static String getImplementationLanguage() {
        return "Java";
    }
    
    public static void main(final String[] args) {
        System.out.println("Apache " + getProduct() + " " + getVersion() + " (" + getReleaseDate() + ")");
    }
}
