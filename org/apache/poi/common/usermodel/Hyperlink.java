// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.common.usermodel;

public interface Hyperlink
{
    public static final int LINK_URL = 1;
    public static final int LINK_DOCUMENT = 2;
    public static final int LINK_EMAIL = 3;
    public static final int LINK_FILE = 4;
    
    String getAddress();
    
    void setAddress(final String p0);
    
    String getLabel();
    
    void setLabel(final String p0);
    
    int getType();
}
