// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.common.usermodel;

import java.awt.Color;

public interface Fill
{
    Color getColor();
    
    void setColor(final Color p0);
}
