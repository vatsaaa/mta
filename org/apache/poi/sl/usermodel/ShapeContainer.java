// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

public interface ShapeContainer
{
    Shape[] getShapes();
    
    void addShape(final Shape p0);
    
    boolean removeShape(final Shape p0);
}
