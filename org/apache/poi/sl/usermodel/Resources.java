// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

public interface Resources
{
    FontCollection getFontCollection();
    
    PictureData[] getPictureData();
    
    int addPictureData(final PictureData p0);
}
