// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

import java.awt.geom.Rectangle2D;

public interface Shape
{
    int getShapeType();
    
    Rectangle2D getAnchor();
    
    void setAnchor(final Rectangle2D p0);
    
    void moveTo(final float p0, final float p1);
    
    Shape getParent();
}
