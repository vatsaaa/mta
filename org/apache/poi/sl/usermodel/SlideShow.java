// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

import java.io.IOException;

public interface SlideShow
{
    Slide createSlide() throws IOException;
    
    MasterSheet createMasterSheet() throws IOException;
    
    Slide[] getSlides();
    
    MasterSheet[] getMasterSheet();
    
    Resources getResources();
}
