// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

public interface Slide extends Sheet
{
    Notes getNotes();
    
    void setNotes(final Notes p0);
    
    boolean getFollowMasterBackground();
    
    void setFollowMasterBackground(final boolean p0);
    
    boolean getFollowMasterColourScheme();
    
    void setFollowMasterColourScheme(final boolean p0);
    
    boolean getFollowMasterObjects();
    
    void setFollowMasterObjects(final boolean p0);
}
