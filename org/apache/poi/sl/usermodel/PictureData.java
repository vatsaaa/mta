// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

public interface PictureData
{
    int getType();
    
    byte[] getUID();
    
    byte[] getData();
    
    void setData(final byte[] p0);
}
