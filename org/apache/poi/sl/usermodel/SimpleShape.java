// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

public interface SimpleShape extends Shape
{
    Fill getFill();
    
    LineStyle getLineStyle();
    
    Hyperlink getHyperlink();
    
    void setHyperlink(final Hyperlink p0);
}
