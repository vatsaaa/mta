// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.sl.usermodel;

public interface Picture extends SimpleShape
{
    PictureData getPictureData();
}
