// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.model;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCommentList;
import java.util.HashMap;
import schemasMicrosoftComVml.CTShape;
import org.apache.poi.xssf.usermodel.XSSFComment;
import java.io.OutputStream;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CommentsDocument;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTComment;
import java.util.Map;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTComments;
import org.apache.poi.POIXMLDocumentPart;

public class CommentsTable extends POIXMLDocumentPart
{
    private CTComments comments;
    private Map<String, CTComment> commentRefs;
    
    public CommentsTable() {
        (this.comments = CTComments.Factory.newInstance()).addNewCommentList();
        this.comments.addNewAuthors().addAuthor("");
    }
    
    public CommentsTable(final PackagePart part, final PackageRelationship rel) throws IOException {
        super(part, rel);
        this.readFrom(part.getInputStream());
    }
    
    public void readFrom(final InputStream is) throws IOException {
        try {
            final CommentsDocument doc = CommentsDocument.Factory.parse(is);
            this.comments = doc.getComments();
        }
        catch (XmlException e) {
            throw new IOException(e.getLocalizedMessage());
        }
    }
    
    public void writeTo(final OutputStream out) throws IOException {
        final CommentsDocument doc = CommentsDocument.Factory.newInstance();
        doc.setComments(this.comments);
        doc.save(out, CommentsTable.DEFAULT_XML_OPTIONS);
    }
    
    @Override
    protected void commit() throws IOException {
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.writeTo(out);
        out.close();
    }
    
    public void referenceUpdated(final String oldReference, final CTComment comment) {
        if (this.commentRefs != null) {
            this.commentRefs.remove(oldReference);
            this.commentRefs.put(comment.getRef(), comment);
        }
    }
    
    public int getNumberOfComments() {
        return this.comments.getCommentList().sizeOfCommentArray();
    }
    
    public int getNumberOfAuthors() {
        return this.comments.getAuthors().sizeOfAuthorArray();
    }
    
    public String getAuthor(final long authorId) {
        return this.comments.getAuthors().getAuthorArray((int)authorId);
    }
    
    public int findAuthor(final String author) {
        for (int i = 0; i < this.comments.getAuthors().sizeOfAuthorArray(); ++i) {
            if (this.comments.getAuthors().getAuthorArray(i).equals(author)) {
                return i;
            }
        }
        return this.addNewAuthor(author);
    }
    
    public XSSFComment findCellComment(final String cellRef) {
        final CTComment ct = this.getCTComment(cellRef);
        return (ct == null) ? null : new XSSFComment(this, ct, null);
    }
    
    public CTComment getCTComment(final String cellRef) {
        if (this.commentRefs == null) {
            this.commentRefs = new HashMap<String, CTComment>();
            for (final CTComment comment : this.comments.getCommentList().getCommentArray()) {
                this.commentRefs.put(comment.getRef(), comment);
            }
        }
        return this.commentRefs.get(cellRef);
    }
    
    public CTComment newComment() {
        final CTComment ct = this.comments.getCommentList().addNewComment();
        ct.setRef("A1");
        ct.setAuthorId(0L);
        if (this.commentRefs != null) {
            this.commentRefs.put(ct.getRef(), ct);
        }
        return ct;
    }
    
    public boolean removeComment(final String cellRef) {
        final CTCommentList lst = this.comments.getCommentList();
        if (lst != null) {
            for (int i = 0; i < lst.sizeOfCommentArray(); ++i) {
                final CTComment comment = lst.getCommentArray(i);
                if (cellRef.equals(comment.getRef())) {
                    lst.removeComment(i);
                    if (this.commentRefs != null) {
                        this.commentRefs.remove(cellRef);
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    private int addNewAuthor(final String author) {
        final int index = this.comments.getAuthors().sizeOfAuthorArray();
        this.comments.getAuthors().insertAuthor(index, author);
        return index;
    }
    
    public CTComments getCTComments() {
        return this.comments;
    }
}
