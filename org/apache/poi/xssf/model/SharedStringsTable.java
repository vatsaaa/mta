// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.model;

import java.util.Collections;
import java.io.OutputStream;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSst;
import org.apache.xmlbeans.XmlException;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.HashMap;
import java.util.ArrayList;
import org.apache.xmlbeans.XmlOptions;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.SstDocument;
import java.util.Map;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRst;
import java.util.List;
import org.apache.poi.POIXMLDocumentPart;

public class SharedStringsTable extends POIXMLDocumentPart
{
    private final List<CTRst> strings;
    private final Map<String, Integer> stmap;
    private int count;
    private int uniqueCount;
    private SstDocument _sstDoc;
    private static final XmlOptions options;
    
    public SharedStringsTable() {
        this.strings = new ArrayList<CTRst>();
        this.stmap = new HashMap<String, Integer>();
        (this._sstDoc = SstDocument.Factory.newInstance()).addNewSst();
    }
    
    public SharedStringsTable(final PackagePart part, final PackageRelationship rel) throws IOException {
        super(part, rel);
        this.strings = new ArrayList<CTRst>();
        this.stmap = new HashMap<String, Integer>();
        this.readFrom(part.getInputStream());
    }
    
    public void readFrom(final InputStream is) throws IOException {
        try {
            int cnt = 0;
            this._sstDoc = SstDocument.Factory.parse(is);
            final CTSst sst = this._sstDoc.getSst();
            this.count = (int)sst.getCount();
            this.uniqueCount = (int)sst.getUniqueCount();
            for (final CTRst st : sst.getSiArray()) {
                this.stmap.put(this.getKey(st), cnt);
                this.strings.add(st);
                ++cnt;
            }
        }
        catch (XmlException e) {
            throw new IOException(e.getLocalizedMessage());
        }
    }
    
    private String getKey(final CTRst st) {
        return st.xmlText(SharedStringsTable.options);
    }
    
    public CTRst getEntryAt(final int idx) {
        return this.strings.get(idx);
    }
    
    public int getCount() {
        return this.count;
    }
    
    public int getUniqueCount() {
        return this.uniqueCount;
    }
    
    public int addEntry(final CTRst st) {
        final String s = this.getKey(st);
        ++this.count;
        if (this.stmap.containsKey(s)) {
            return this.stmap.get(s);
        }
        ++this.uniqueCount;
        final CTRst newSt = this._sstDoc.getSst().addNewSi();
        newSt.set(st);
        final int idx = this.strings.size();
        this.stmap.put(s, idx);
        this.strings.add(newSt);
        return idx;
    }
    
    public List<CTRst> getItems() {
        return this.strings;
    }
    
    public void writeTo(final OutputStream out) throws IOException {
        final XmlOptions options = new XmlOptions(SharedStringsTable.DEFAULT_XML_OPTIONS);
        options.setSaveCDataLengthThreshold(1000000);
        options.setSaveCDataEntityCountThreshold(-1);
        final CTSst sst = this._sstDoc.getSst();
        sst.setCount(this.count);
        sst.setUniqueCount(this.uniqueCount);
        this._sstDoc.save(out, options);
    }
    
    @Override
    protected void commit() throws IOException {
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.writeTo(out);
        out.close();
    }
    
    static {
        (options = new XmlOptions()).put("SAVE_INNER");
        SharedStringsTable.options.put("SAVE_AGGRESSIVE_NAMESPACES");
        SharedStringsTable.options.put("SAVE_USE_DEFAULT_NAMESPACE");
        SharedStringsTable.options.setSaveImplicitNamespaces(Collections.singletonMap("", "http://schemas.openxmlformats.org/spreadsheetml/2006/main"));
    }
}
