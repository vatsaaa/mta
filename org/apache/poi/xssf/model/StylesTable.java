// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.model;

import org.apache.poi.ss.usermodel.FontScheme;
import org.apache.poi.ss.usermodel.FontFamily;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPatternType;
import org.apache.xmlbeans.XmlOptions;
import java.io.OutputStream;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDxfs;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellStyleXfs;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellXfs;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorder;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorders;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFill;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFills;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFont;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFonts;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTNumFmt;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTNumFmts;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTStylesheet;
import org.apache.xmlbeans.XmlException;
import java.util.Collection;
import java.util.Arrays;
import java.io.InputStream;
import java.util.Iterator;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.StyleSheetDocument;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDxf;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTXf;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellFill;
import org.apache.poi.xssf.usermodel.XSSFFont;
import java.util.List;
import java.util.Map;
import org.apache.poi.POIXMLDocumentPart;

public class StylesTable extends POIXMLDocumentPart
{
    private final Map<Integer, String> numberFormats;
    private final List<XSSFFont> fonts;
    private final List<XSSFCellFill> fills;
    private final List<XSSFCellBorder> borders;
    private final List<CTXf> styleXfs;
    private final List<CTXf> xfs;
    private final List<CTDxf> dxfs;
    public static final int FIRST_CUSTOM_STYLE_ID = 165;
    private StyleSheetDocument doc;
    private ThemesTable theme;
    
    public StylesTable() {
        this.numberFormats = new LinkedHashMap<Integer, String>();
        this.fonts = new ArrayList<XSSFFont>();
        this.fills = new ArrayList<XSSFCellFill>();
        this.borders = new ArrayList<XSSFCellBorder>();
        this.styleXfs = new ArrayList<CTXf>();
        this.xfs = new ArrayList<CTXf>();
        this.dxfs = new ArrayList<CTDxf>();
        (this.doc = StyleSheetDocument.Factory.newInstance()).addNewStyleSheet();
        this.initialize();
    }
    
    public StylesTable(final PackagePart part, final PackageRelationship rel) throws IOException {
        super(part, rel);
        this.numberFormats = new LinkedHashMap<Integer, String>();
        this.fonts = new ArrayList<XSSFFont>();
        this.fills = new ArrayList<XSSFCellFill>();
        this.borders = new ArrayList<XSSFCellBorder>();
        this.styleXfs = new ArrayList<CTXf>();
        this.xfs = new ArrayList<CTXf>();
        this.dxfs = new ArrayList<CTDxf>();
        this.readFrom(part.getInputStream());
    }
    
    public ThemesTable getTheme() {
        return this.theme;
    }
    
    public void setTheme(final ThemesTable theme) {
        this.theme = theme;
        for (final XSSFFont font : this.fonts) {
            font.setThemesTable(theme);
        }
        for (final XSSFCellBorder border : this.borders) {
            border.setThemesTable(theme);
        }
    }
    
    protected void readFrom(final InputStream is) throws IOException {
        try {
            this.doc = StyleSheetDocument.Factory.parse(is);
            final CTStylesheet styleSheet = this.doc.getStyleSheet();
            final CTNumFmts ctfmts = styleSheet.getNumFmts();
            if (ctfmts != null) {
                for (final CTNumFmt nfmt : ctfmts.getNumFmtArray()) {
                    this.numberFormats.put((int)nfmt.getNumFmtId(), nfmt.getFormatCode());
                }
            }
            final CTFonts ctfonts = styleSheet.getFonts();
            if (ctfonts != null) {
                int idx = 0;
                for (final CTFont font : ctfonts.getFontArray()) {
                    final XSSFFont f = new XSSFFont(font, idx);
                    this.fonts.add(f);
                    ++idx;
                }
            }
            final CTFills ctfills = styleSheet.getFills();
            if (ctfills != null) {
                for (final CTFill fill : ctfills.getFillArray()) {
                    this.fills.add(new XSSFCellFill(fill));
                }
            }
            final CTBorders ctborders = styleSheet.getBorders();
            if (ctborders != null) {
                for (final CTBorder border : ctborders.getBorderArray()) {
                    this.borders.add(new XSSFCellBorder(border));
                }
            }
            final CTCellXfs cellXfs = styleSheet.getCellXfs();
            if (cellXfs != null) {
                this.xfs.addAll(Arrays.asList(cellXfs.getXfArray()));
            }
            final CTCellStyleXfs cellStyleXfs = styleSheet.getCellStyleXfs();
            if (cellStyleXfs != null) {
                this.styleXfs.addAll(Arrays.asList(cellStyleXfs.getXfArray()));
            }
            final CTDxfs styleDxfs = styleSheet.getDxfs();
            if (styleDxfs != null) {
                this.dxfs.addAll(Arrays.asList(styleDxfs.getDxfArray()));
            }
        }
        catch (XmlException e) {
            throw new IOException(e.getLocalizedMessage());
        }
    }
    
    public String getNumberFormatAt(final int idx) {
        return this.numberFormats.get(idx);
    }
    
    public int putNumberFormat(final String fmt) {
        if (this.numberFormats.containsValue(fmt)) {
            for (final Integer key : this.numberFormats.keySet()) {
                if (this.numberFormats.get(key).equals(fmt)) {
                    return key;
                }
            }
            throw new IllegalStateException("Found the format, but couldn't figure out where - should never happen!");
        }
        int newKey;
        for (newKey = 165; this.numberFormats.containsKey(newKey); ++newKey) {}
        this.numberFormats.put(newKey, fmt);
        return newKey;
    }
    
    public XSSFFont getFontAt(final int idx) {
        return this.fonts.get(idx);
    }
    
    public int putFont(final XSSFFont font, final boolean forceRegistration) {
        int idx = -1;
        if (!forceRegistration) {
            idx = this.fonts.indexOf(font);
        }
        if (idx != -1) {
            return idx;
        }
        idx = this.fonts.size();
        this.fonts.add(font);
        return idx;
    }
    
    public int putFont(final XSSFFont font) {
        return this.putFont(font, false);
    }
    
    public XSSFCellStyle getStyleAt(final int idx) {
        int styleXfId = 0;
        if (this.xfs.get(idx).getXfId() > 0L) {
            styleXfId = (int)this.xfs.get(idx).getXfId();
        }
        return new XSSFCellStyle(idx, styleXfId, this, this.theme);
    }
    
    public int putStyle(final XSSFCellStyle style) {
        final CTXf mainXF = style.getCoreXf();
        if (!this.xfs.contains(mainXF)) {
            this.xfs.add(mainXF);
        }
        return this.xfs.indexOf(mainXF);
    }
    
    public XSSFCellBorder getBorderAt(final int idx) {
        return this.borders.get(idx);
    }
    
    public int putBorder(final XSSFCellBorder border) {
        final int idx = this.borders.indexOf(border);
        if (idx != -1) {
            return idx;
        }
        this.borders.add(border);
        border.setThemesTable(this.theme);
        return this.borders.size() - 1;
    }
    
    public XSSFCellFill getFillAt(final int idx) {
        return this.fills.get(idx);
    }
    
    public List<XSSFCellBorder> getBorders() {
        return this.borders;
    }
    
    public List<XSSFCellFill> getFills() {
        return this.fills;
    }
    
    public List<XSSFFont> getFonts() {
        return this.fonts;
    }
    
    public Map<Integer, String> getNumberFormats() {
        return this.numberFormats;
    }
    
    public int putFill(final XSSFCellFill fill) {
        final int idx = this.fills.indexOf(fill);
        if (idx != -1) {
            return idx;
        }
        this.fills.add(fill);
        return this.fills.size() - 1;
    }
    
    public CTXf getCellXfAt(final int idx) {
        return this.xfs.get(idx);
    }
    
    public int putCellXf(final CTXf cellXf) {
        this.xfs.add(cellXf);
        return this.xfs.size();
    }
    
    public void replaceCellXfAt(final int idx, final CTXf cellXf) {
        this.xfs.set(idx, cellXf);
    }
    
    public CTXf getCellStyleXfAt(final int idx) {
        return this.styleXfs.get(idx);
    }
    
    public int putCellStyleXf(final CTXf cellStyleXf) {
        this.styleXfs.add(cellStyleXf);
        return this.styleXfs.size();
    }
    
    public void replaceCellStyleXfAt(final int idx, final CTXf cellStyleXf) {
        this.styleXfs.set(idx, cellStyleXf);
    }
    
    public int getNumCellStyles() {
        return this.xfs.size();
    }
    
    public int _getNumberFormatSize() {
        return this.numberFormats.size();
    }
    
    public int _getXfsSize() {
        return this.xfs.size();
    }
    
    public int _getStyleXfsSize() {
        return this.styleXfs.size();
    }
    
    public CTStylesheet getCTStylesheet() {
        return this.doc.getStyleSheet();
    }
    
    public int _getDXfsSize() {
        return this.dxfs.size();
    }
    
    public void writeTo(final OutputStream out) throws IOException {
        final XmlOptions options = new XmlOptions(StylesTable.DEFAULT_XML_OPTIONS);
        final CTStylesheet styleSheet = this.doc.getStyleSheet();
        final CTNumFmts formats = CTNumFmts.Factory.newInstance();
        formats.setCount(this.numberFormats.size());
        for (final Map.Entry<Integer, String> fmt : this.numberFormats.entrySet()) {
            final CTNumFmt ctFmt = formats.addNewNumFmt();
            ctFmt.setNumFmtId(fmt.getKey());
            ctFmt.setFormatCode(fmt.getValue());
        }
        styleSheet.setNumFmts(formats);
        final CTFonts ctFonts = CTFonts.Factory.newInstance();
        ctFonts.setCount(this.fonts.size());
        final CTFont[] ctfnt = new CTFont[this.fonts.size()];
        int idx = 0;
        for (final XSSFFont f : this.fonts) {
            ctfnt[idx++] = f.getCTFont();
        }
        ctFonts.setFontArray(ctfnt);
        styleSheet.setFonts(ctFonts);
        final CTFills ctFills = CTFills.Factory.newInstance();
        ctFills.setCount(this.fills.size());
        final CTFill[] ctf = new CTFill[this.fills.size()];
        idx = 0;
        for (final XSSFCellFill f2 : this.fills) {
            ctf[idx++] = f2.getCTFill();
        }
        ctFills.setFillArray(ctf);
        styleSheet.setFills(ctFills);
        final CTBorders ctBorders = CTBorders.Factory.newInstance();
        ctBorders.setCount(this.borders.size());
        final CTBorder[] ctb = new CTBorder[this.borders.size()];
        idx = 0;
        for (final XSSFCellBorder b : this.borders) {
            ctb[idx++] = b.getCTBorder();
        }
        ctBorders.setBorderArray(ctb);
        styleSheet.setBorders(ctBorders);
        if (this.xfs.size() > 0) {
            final CTCellXfs ctXfs = CTCellXfs.Factory.newInstance();
            ctXfs.setCount(this.xfs.size());
            ctXfs.setXfArray(this.xfs.toArray(new CTXf[this.xfs.size()]));
            styleSheet.setCellXfs(ctXfs);
        }
        if (this.styleXfs.size() > 0) {
            final CTCellStyleXfs ctSXfs = CTCellStyleXfs.Factory.newInstance();
            ctSXfs.setCount(this.styleXfs.size());
            ctSXfs.setXfArray(this.styleXfs.toArray(new CTXf[this.styleXfs.size()]));
            styleSheet.setCellStyleXfs(ctSXfs);
        }
        if (this.dxfs.size() > 0) {
            final CTDxfs ctDxfs = CTDxfs.Factory.newInstance();
            ctDxfs.setCount(this.dxfs.size());
            ctDxfs.setDxfArray(this.dxfs.toArray(new CTDxf[this.dxfs.size()]));
            styleSheet.setDxfs(ctDxfs);
        }
        this.doc.save(out, options);
    }
    
    @Override
    protected void commit() throws IOException {
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.writeTo(out);
        out.close();
    }
    
    private void initialize() {
        final XSSFFont xssfFont = createDefaultFont();
        this.fonts.add(xssfFont);
        final CTFill[] ctFill = createDefaultFills();
        this.fills.add(new XSSFCellFill(ctFill[0]));
        this.fills.add(new XSSFCellFill(ctFill[1]));
        final CTBorder ctBorder = createDefaultBorder();
        this.borders.add(new XSSFCellBorder(ctBorder));
        final CTXf styleXf = createDefaultXf();
        this.styleXfs.add(styleXf);
        final CTXf xf = createDefaultXf();
        xf.setXfId(0L);
        this.xfs.add(xf);
    }
    
    private static CTXf createDefaultXf() {
        final CTXf ctXf = CTXf.Factory.newInstance();
        ctXf.setNumFmtId(0L);
        ctXf.setFontId(0L);
        ctXf.setFillId(0L);
        ctXf.setBorderId(0L);
        return ctXf;
    }
    
    private static CTBorder createDefaultBorder() {
        final CTBorder ctBorder = CTBorder.Factory.newInstance();
        ctBorder.addNewBottom();
        ctBorder.addNewTop();
        ctBorder.addNewLeft();
        ctBorder.addNewRight();
        ctBorder.addNewDiagonal();
        return ctBorder;
    }
    
    private static CTFill[] createDefaultFills() {
        final CTFill[] ctFill = { CTFill.Factory.newInstance(), CTFill.Factory.newInstance() };
        ctFill[0].addNewPatternFill().setPatternType(STPatternType.NONE);
        ctFill[1].addNewPatternFill().setPatternType(STPatternType.DARK_GRAY);
        return ctFill;
    }
    
    private static XSSFFont createDefaultFont() {
        final CTFont ctFont = CTFont.Factory.newInstance();
        final XSSFFont xssfFont = new XSSFFont(ctFont, 0);
        xssfFont.setFontHeightInPoints((short)11);
        xssfFont.setColor(XSSFFont.DEFAULT_FONT_COLOR);
        xssfFont.setFontName("Calibri");
        xssfFont.setFamily(FontFamily.SWISS);
        xssfFont.setScheme(FontScheme.MINOR);
        return xssfFont;
    }
    
    public CTDxf getDxfAt(final int idx) {
        return this.dxfs.get(idx);
    }
    
    public int putDxf(final CTDxf dxf) {
        this.dxfs.add(dxf);
        return this.dxfs.size();
    }
    
    public XSSFCellStyle createCellStyle() {
        final CTXf xf = CTXf.Factory.newInstance();
        xf.setNumFmtId(0L);
        xf.setFontId(0L);
        xf.setFillId(0L);
        xf.setBorderId(0L);
        xf.setXfId(0L);
        final int xfSize = this.styleXfs.size();
        final int indexXf = this.putCellXf(xf);
        return new XSSFCellStyle(indexXf - 1, xfSize - 1, this, this.theme);
    }
    
    public XSSFFont findFont(final short boldWeight, final short color, final short fontHeight, final String name, final boolean italic, final boolean strikeout, final short typeOffset, final byte underline) {
        for (final XSSFFont font : this.fonts) {
            if (font.getBoldweight() == boldWeight && font.getColor() == color && font.getFontHeight() == fontHeight && font.getFontName().equals(name) && font.getItalic() == italic && font.getStrikeout() == strikeout && font.getTypeOffset() == typeOffset && font.getUnderline() == underline) {
                return font;
            }
        }
        return null;
    }
}
