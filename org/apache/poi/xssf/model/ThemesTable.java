// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.model;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorScheme;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColor;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.drawingml.x2006.main.ThemeDocument;
import org.apache.poi.POIXMLDocumentPart;

public class ThemesTable extends POIXMLDocumentPart
{
    private ThemeDocument theme;
    
    public ThemesTable(final PackagePart part, final PackageRelationship rel) throws IOException {
        super(part, rel);
        try {
            this.theme = ThemeDocument.Factory.parse(part.getInputStream());
        }
        catch (XmlException e) {
            throw new IOException(e.getLocalizedMessage());
        }
    }
    
    public ThemesTable(final ThemeDocument theme) {
        this.theme = theme;
    }
    
    public XSSFColor getThemeColor(final int idx) {
        final CTColorScheme colorScheme = this.theme.getTheme().getThemeElements().getClrScheme();
        CTColor ctColor = null;
        int cnt = 0;
        for (final XmlObject obj : colorScheme.selectPath("./*")) {
            if (obj instanceof CTColor) {
                if (cnt == idx) {
                    ctColor = (CTColor)obj;
                    byte[] rgb = null;
                    if (ctColor.getSrgbClr() != null) {
                        rgb = ctColor.getSrgbClr().getVal();
                    }
                    else if (ctColor.getSysClr() != null) {
                        rgb = ctColor.getSysClr().getLastClr();
                    }
                    return new XSSFColor(rgb);
                }
                ++cnt;
            }
        }
        return null;
    }
    
    public void inheritFromThemeAsRequired(final XSSFColor color) {
        if (color == null) {
            return;
        }
        if (!color.getCTColor().isSetTheme()) {
            return;
        }
        final XSSFColor themeColor = this.getThemeColor(color.getTheme());
        color.getCTColor().setRgb(themeColor.getCTColor().getRgb());
    }
}
