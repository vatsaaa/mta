// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.util;

public class NumericRanges
{
    public static int NO_OVERLAPS;
    public static int OVERLAPS_1_MINOR;
    public static int OVERLAPS_2_MINOR;
    public static int OVERLAPS_1_WRAPS;
    public static int OVERLAPS_2_WRAPS;
    
    public static long[] getOverlappingRange(final long[] range1, final long[] range2) {
        final int overlappingType = getOverlappingType(range1, range2);
        if (overlappingType == NumericRanges.OVERLAPS_1_MINOR) {
            return new long[] { range2[0], range1[1] };
        }
        if (overlappingType == NumericRanges.OVERLAPS_2_MINOR) {
            return new long[] { range1[0], range2[1] };
        }
        if (overlappingType == NumericRanges.OVERLAPS_2_WRAPS) {
            return range1;
        }
        if (overlappingType == NumericRanges.OVERLAPS_1_WRAPS) {
            return range2;
        }
        return new long[] { -1L, -1L };
    }
    
    public static int getOverlappingType(final long[] range1, final long[] range2) {
        final long min1 = range1[0];
        final long max1 = range1[1];
        final long min2 = range2[0];
        final long max2 = range2[1];
        if (min1 >= min2 && max1 <= max2) {
            return NumericRanges.OVERLAPS_2_WRAPS;
        }
        if (min2 >= min1 && max2 <= max1) {
            return NumericRanges.OVERLAPS_1_WRAPS;
        }
        if (min2 >= min1 && min2 <= max1 && max2 >= max1) {
            return NumericRanges.OVERLAPS_1_MINOR;
        }
        if (min1 >= min2 && min1 <= max2 && max1 >= max2) {
            return NumericRanges.OVERLAPS_2_MINOR;
        }
        return NumericRanges.NO_OVERLAPS;
    }
    
    static {
        NumericRanges.NO_OVERLAPS = -1;
        NumericRanges.OVERLAPS_1_MINOR = 0;
        NumericRanges.OVERLAPS_2_MINOR = 1;
        NumericRanges.OVERLAPS_1_WRAPS = 2;
        NumericRanges.OVERLAPS_2_WRAPS = 3;
    }
}
