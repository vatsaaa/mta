// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.util;

import java.util.Iterator;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStream;

public class EvilUnclosedBRFixingInputStream extends InputStream
{
    private InputStream source;
    private byte[] spare;
    private static byte[] detect;
    
    public EvilUnclosedBRFixingInputStream(final InputStream source) {
        this.source = source;
    }
    
    @Override
    public int read() throws IOException {
        return this.source.read();
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final int readA = this.readFromSpare(b, off, len);
        final int readB = this.source.read(b, off + readA, len - readA);
        int read;
        if (readB == -1 || readB == 0) {
            read = readA;
        }
        else {
            read = readA + readB;
        }
        if (read > 0) {
            read = this.fixUp(b, off, read);
        }
        return read;
    }
    
    @Override
    public int read(final byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }
    
    private int readFromSpare(final byte[] b, final int offset, final int len) {
        if (this.spare == null) {
            return 0;
        }
        if (len == 0) {
            throw new IllegalArgumentException("Asked to read 0 bytes");
        }
        if (this.spare.length <= len) {
            System.arraycopy(this.spare, 0, b, offset, this.spare.length);
            final int read = this.spare.length;
            this.spare = null;
            return read;
        }
        final byte[] newspare = new byte[this.spare.length - len];
        System.arraycopy(this.spare, 0, b, offset, len);
        System.arraycopy(this.spare, len, newspare, 0, newspare.length);
        this.spare = newspare;
        return len;
    }
    
    private void addToSpare(final byte[] b, final int offset, final int len, final boolean atTheEnd) {
        if (this.spare == null) {
            System.arraycopy(b, offset, this.spare = new byte[len], 0, len);
        }
        else {
            final byte[] newspare = new byte[this.spare.length + len];
            if (atTheEnd) {
                System.arraycopy(this.spare, 0, newspare, 0, this.spare.length);
                System.arraycopy(b, offset, newspare, this.spare.length, len);
            }
            else {
                System.arraycopy(b, offset, newspare, 0, len);
                System.arraycopy(this.spare, 0, newspare, len, this.spare.length);
            }
            this.spare = newspare;
        }
    }
    
    private int fixUp(final byte[] b, final int offset, int read) {
        for (int i = 0; i < EvilUnclosedBRFixingInputStream.detect.length - 1; ++i) {
            final int base = offset + read - 1 - i;
            if (base >= 0) {
                boolean going = true;
                for (int j = 0; j <= i && going; ++j) {
                    if (b[base + j] != EvilUnclosedBRFixingInputStream.detect[j]) {
                        going = false;
                    }
                }
                if (going) {
                    this.addToSpare(b, base, i + 1, true);
                    read = --read - i;
                    break;
                }
            }
        }
        final ArrayList<Integer> fixAt = new ArrayList<Integer>();
        for (int k = offset; k <= offset + read - EvilUnclosedBRFixingInputStream.detect.length; ++k) {
            boolean going = true;
            for (int j = 0; j < EvilUnclosedBRFixingInputStream.detect.length && going; ++j) {
                if (b[k + j] != EvilUnclosedBRFixingInputStream.detect[j]) {
                    going = false;
                }
            }
            if (going) {
                fixAt.add(k);
            }
        }
        if (fixAt.size() == 0) {
            return read;
        }
        final int needed = offset + read + fixAt.size();
        int overshoot = needed - b.length;
        if (overshoot > 0) {
            int fixes = 0;
            for (final int at : fixAt) {
                if (at > offset + read - EvilUnclosedBRFixingInputStream.detect.length - overshoot - fixes) {
                    overshoot = needed - at - 1 - fixes;
                    break;
                }
                ++fixes;
            }
            this.addToSpare(b, offset + read - overshoot, overshoot, false);
            read -= overshoot;
        }
        for (int j = fixAt.size() - 1; j >= 0; --j) {
            final int l = fixAt.get(j);
            if (l < read + offset) {
                if (l <= read - 3) {
                    final byte[] tmp = new byte[read - l - 3];
                    System.arraycopy(b, l + 3, tmp, 0, tmp.length);
                    b[l + 3] = 47;
                    System.arraycopy(tmp, 0, b, l + 4, tmp.length);
                    ++read;
                }
            }
        }
        return read;
    }
    
    static {
        EvilUnclosedBRFixingInputStream.detect = new byte[] { 60, 98, 114, 62 };
    }
}
