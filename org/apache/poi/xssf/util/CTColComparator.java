// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.util;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCol;
import java.util.Comparator;

public class CTColComparator implements Comparator<CTCol>
{
    public int compare(final CTCol o1, final CTCol o2) {
        if (o1.getMin() < o2.getMin()) {
            return -1;
        }
        if (o1.getMin() > o2.getMin()) {
            return 1;
        }
        if (o1.getMax() < o2.getMax()) {
            return -1;
        }
        if (o1.getMax() > o2.getMax()) {
            return 1;
        }
        return 0;
    }
}
