// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.eventusermodel;

import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.ContentHandler;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import java.io.InputStream;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.util.List;
import org.xml.sax.helpers.DefaultHandler;

public class ReadOnlySharedStringsTable extends DefaultHandler
{
    private int count;
    private int uniqueCount;
    private List<String> strings;
    private StringBuffer characters;
    private boolean tIsOpen;
    
    public ReadOnlySharedStringsTable(final OPCPackage pkg) throws IOException, SAXException {
        final ArrayList<PackagePart> parts = pkg.getPartsByContentType(XSSFRelation.SHARED_STRINGS.getContentType());
        if (parts.size() > 0) {
            final PackagePart sstPart = parts.get(0);
            this.readFrom(sstPart.getInputStream());
        }
    }
    
    public ReadOnlySharedStringsTable(final PackagePart part, final PackageRelationship rel_ignored) throws IOException, SAXException {
        this.readFrom(part.getInputStream());
    }
    
    public void readFrom(final InputStream is) throws IOException, SAXException {
        final InputSource sheetSource = new InputSource(is);
        final SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        try {
            final SAXParser saxParser = saxFactory.newSAXParser();
            final XMLReader sheetParser = saxParser.getXMLReader();
            sheetParser.setContentHandler(this);
            sheetParser.parse(sheetSource);
        }
        catch (ParserConfigurationException e) {
            throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
        }
    }
    
    public int getCount() {
        return this.count;
    }
    
    public int getUniqueCount() {
        return this.uniqueCount;
    }
    
    public String getEntryAt(final int idx) {
        return this.strings.get(idx);
    }
    
    public List<String> getItems() {
        return this.strings;
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String name, final Attributes attributes) throws SAXException {
        if ("sst".equals(name)) {
            final String count = attributes.getValue("count");
            if (count != null) {
                this.count = Integer.parseInt(count);
            }
            final String uniqueCount = attributes.getValue("uniqueCount");
            if (uniqueCount != null) {
                this.uniqueCount = Integer.parseInt(uniqueCount);
            }
            this.strings = new ArrayList<String>(this.uniqueCount);
            this.characters = new StringBuffer();
        }
        else if ("si".equals(name)) {
            this.characters.setLength(0);
        }
        else if ("t".equals(name)) {
            this.tIsOpen = true;
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String name) throws SAXException {
        if ("si".equals(name)) {
            this.strings.add(this.characters.toString());
        }
        else if ("t".equals(name)) {
            this.tIsOpen = false;
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (this.tIsOpen) {
            this.characters.append(ch, start, length);
        }
    }
}
