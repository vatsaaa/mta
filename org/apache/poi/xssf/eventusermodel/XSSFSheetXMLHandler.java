// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.eventusermodel;

import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.SAXException;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.xml.sax.Attributes;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.model.StylesTable;
import org.xml.sax.helpers.DefaultHandler;

public class XSSFSheetXMLHandler extends DefaultHandler
{
    private StylesTable stylesTable;
    private ReadOnlySharedStringsTable sharedStringsTable;
    private final SheetContentsHandler output;
    private boolean vIsOpen;
    private boolean fIsOpen;
    private boolean isIsOpen;
    private boolean hfIsOpen;
    private xssfDataType nextDataType;
    private short formatIndex;
    private String formatString;
    private final DataFormatter formatter;
    private String cellRef;
    private boolean formulasNotResults;
    private StringBuffer value;
    private StringBuffer formula;
    private StringBuffer headerFooter;
    
    public XSSFSheetXMLHandler(final StylesTable styles, final ReadOnlySharedStringsTable strings, final SheetContentsHandler sheetContentsHandler, final DataFormatter dataFormatter, final boolean formulasNotResults) {
        this.value = new StringBuffer();
        this.formula = new StringBuffer();
        this.headerFooter = new StringBuffer();
        this.stylesTable = styles;
        this.sharedStringsTable = strings;
        this.output = sheetContentsHandler;
        this.formulasNotResults = formulasNotResults;
        this.nextDataType = xssfDataType.NUMBER;
        this.formatter = dataFormatter;
    }
    
    public XSSFSheetXMLHandler(final StylesTable styles, final ReadOnlySharedStringsTable strings, final SheetContentsHandler sheetContentsHandler, final boolean formulasNotResults) {
        this(styles, strings, sheetContentsHandler, new DataFormatter(), formulasNotResults);
    }
    
    private boolean isTextTag(final String name) {
        return "v".equals(name) || "inlineStr".equals(name) || ("t".equals(name) && this.isIsOpen);
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String name, final Attributes attributes) throws SAXException {
        if (this.isTextTag(name)) {
            this.vIsOpen = true;
            this.value.setLength(0);
        }
        else if ("is".equals(name)) {
            this.isIsOpen = true;
        }
        else if ("f".equals(name)) {
            this.formula.setLength(0);
            if (this.nextDataType == xssfDataType.NUMBER) {
                this.nextDataType = xssfDataType.FORMULA;
            }
            final String type = attributes.getValue("t");
            if (type != null && type.equals("shared")) {
                final String ref = attributes.getValue("ref");
                final String si = attributes.getValue("si");
                if (ref != null) {
                    this.fIsOpen = true;
                }
                else if (this.formulasNotResults) {
                    System.err.println("Warning - shared formulas not yet supported!");
                }
            }
            else {
                this.fIsOpen = true;
            }
        }
        else if ("oddHeader".equals(name) || "evenHeader".equals(name) || "firstHeader".equals(name) || "firstFooter".equals(name) || "oddFooter".equals(name) || "evenFooter".equals(name)) {
            this.hfIsOpen = true;
            this.headerFooter.setLength(0);
        }
        else if ("row".equals(name)) {
            final int rowNum = Integer.parseInt(attributes.getValue("r")) - 1;
            this.output.startRow(rowNum);
        }
        else if ("c".equals(name)) {
            this.nextDataType = xssfDataType.NUMBER;
            this.formatIndex = -1;
            this.formatString = null;
            this.cellRef = attributes.getValue("r");
            final String cellType = attributes.getValue("t");
            final String cellStyleStr = attributes.getValue("s");
            if ("b".equals(cellType)) {
                this.nextDataType = xssfDataType.BOOLEAN;
            }
            else if ("e".equals(cellType)) {
                this.nextDataType = xssfDataType.ERROR;
            }
            else if ("inlineStr".equals(cellType)) {
                this.nextDataType = xssfDataType.INLINE_STRING;
            }
            else if ("s".equals(cellType)) {
                this.nextDataType = xssfDataType.SST_STRING;
            }
            else if ("str".equals(cellType)) {
                this.nextDataType = xssfDataType.FORMULA;
            }
            else if (cellStyleStr != null) {
                final int styleIndex = Integer.parseInt(cellStyleStr);
                final XSSFCellStyle style = this.stylesTable.getStyleAt(styleIndex);
                this.formatIndex = style.getDataFormat();
                this.formatString = style.getDataFormatString();
                if (this.formatString == null) {
                    this.formatString = BuiltinFormats.getBuiltinFormat(this.formatIndex);
                }
            }
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String name) throws SAXException {
        String thisStr = null;
        if (this.isTextTag(name)) {
            this.vIsOpen = false;
            switch (this.nextDataType) {
                case BOOLEAN: {
                    final char first = this.value.charAt(0);
                    thisStr = ((first == '0') ? "FALSE" : "TRUE");
                    break;
                }
                case ERROR: {
                    thisStr = "ERROR:" + this.value.toString();
                    break;
                }
                case FORMULA: {
                    if (this.formulasNotResults) {
                        thisStr = this.formula.toString();
                        break;
                    }
                    final String fv = this.value.toString();
                    if (this.formatString != null) {
                        try {
                            final double d = Double.parseDouble(fv);
                            thisStr = this.formatter.formatRawCellContents(d, this.formatIndex, this.formatString);
                        }
                        catch (NumberFormatException e) {
                            thisStr = fv;
                        }
                    }
                    else {
                        thisStr = fv;
                    }
                    break;
                }
                case INLINE_STRING: {
                    final XSSFRichTextString rtsi = new XSSFRichTextString(this.value.toString());
                    thisStr = rtsi.toString();
                    break;
                }
                case SST_STRING: {
                    final String sstIndex = this.value.toString();
                    try {
                        final int idx = Integer.parseInt(sstIndex);
                        final XSSFRichTextString rtss = new XSSFRichTextString(this.sharedStringsTable.getEntryAt(idx));
                        thisStr = rtss.toString();
                    }
                    catch (NumberFormatException ex) {
                        System.err.println("Failed to parse SST index '" + sstIndex + "': " + ex.toString());
                    }
                    break;
                }
                case NUMBER: {
                    final String n = this.value.toString();
                    if (this.formatString != null) {
                        thisStr = this.formatter.formatRawCellContents(Double.parseDouble(n), this.formatIndex, this.formatString);
                        break;
                    }
                    thisStr = n;
                    break;
                }
                default: {
                    thisStr = "(TODO: Unexpected type: " + this.nextDataType + ")";
                    break;
                }
            }
            this.output.cell(this.cellRef, thisStr);
        }
        else if ("f".equals(name)) {
            this.fIsOpen = false;
        }
        else if ("is".equals(name)) {
            this.isIsOpen = false;
        }
        else if ("row".equals(name)) {
            this.output.endRow();
        }
        else if ("oddHeader".equals(name) || "evenHeader".equals(name) || "firstHeader".equals(name)) {
            this.hfIsOpen = false;
            this.output.headerFooter(this.headerFooter.toString(), true, name);
        }
        else if ("oddFooter".equals(name) || "evenFooter".equals(name) || "firstFooter".equals(name)) {
            this.hfIsOpen = false;
            this.output.headerFooter(this.headerFooter.toString(), false, name);
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (this.vIsOpen) {
            this.value.append(ch, start, length);
        }
        if (this.fIsOpen) {
            this.formula.append(ch, start, length);
        }
        if (this.hfIsOpen) {
            this.headerFooter.append(ch, start, length);
        }
    }
    
    enum xssfDataType
    {
        BOOLEAN, 
        ERROR, 
        FORMULA, 
        INLINE_STRING, 
        SST_STRING, 
        NUMBER;
    }
    
    public interface SheetContentsHandler
    {
        void startRow(final int p0);
        
        void endRow();
        
        void cell(final String p0, final String p1);
        
        void headerFooter(final String p0, final boolean p1, final String p2);
    }
}
