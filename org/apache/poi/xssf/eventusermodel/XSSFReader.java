// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.eventusermodel;

import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.xssf.model.CommentsTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorkbook;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.WorkbookDocument;
import java.util.HashMap;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheet;
import java.util.Map;
import java.util.Iterator;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import java.io.InputStream;
import org.apache.poi.xssf.model.ThemesTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.util.ArrayList;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.OPCPackage;

public class XSSFReader
{
    private OPCPackage pkg;
    private PackagePart workbookPart;
    
    public XSSFReader(final OPCPackage pkg) throws IOException, OpenXML4JException {
        this.pkg = pkg;
        final PackageRelationship coreDocRelationship = this.pkg.getRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument").getRelationship(0);
        this.workbookPart = this.pkg.getPart(coreDocRelationship);
    }
    
    public SharedStringsTable getSharedStringsTable() throws IOException, InvalidFormatException {
        final ArrayList<PackagePart> parts = this.pkg.getPartsByContentType(XSSFRelation.SHARED_STRINGS.getContentType());
        return (parts.size() == 0) ? null : new SharedStringsTable(parts.get(0), null);
    }
    
    public StylesTable getStylesTable() throws IOException, InvalidFormatException {
        ArrayList<PackagePart> parts = this.pkg.getPartsByContentType(XSSFRelation.STYLES.getContentType());
        if (parts.size() == 0) {
            return null;
        }
        final StylesTable styles = new StylesTable(parts.get(0), null);
        parts = this.pkg.getPartsByContentType(XSSFRelation.THEME.getContentType());
        if (parts.size() != 0) {
            styles.setTheme(new ThemesTable(parts.get(0), null));
        }
        return styles;
    }
    
    public InputStream getSharedStringsData() throws IOException, InvalidFormatException {
        return XSSFRelation.SHARED_STRINGS.getContents(this.workbookPart);
    }
    
    public InputStream getStylesData() throws IOException, InvalidFormatException {
        return XSSFRelation.STYLES.getContents(this.workbookPart);
    }
    
    public InputStream getThemesData() throws IOException, InvalidFormatException {
        return XSSFRelation.THEME.getContents(this.workbookPart);
    }
    
    public InputStream getWorkbookData() throws IOException, InvalidFormatException {
        return this.workbookPart.getInputStream();
    }
    
    public InputStream getSheet(final String relId) throws IOException, InvalidFormatException {
        final PackageRelationship rel = this.workbookPart.getRelationship(relId);
        if (rel == null) {
            throw new IllegalArgumentException("No Sheet found with r:id " + relId);
        }
        final PackagePartName relName = PackagingURIHelper.createPartName(rel.getTargetURI());
        final PackagePart sheet = this.pkg.getPart(relName);
        if (sheet == null) {
            throw new IllegalArgumentException("No data found for Sheet with r:id " + relId);
        }
        return sheet.getInputStream();
    }
    
    public Iterator<InputStream> getSheetsData() throws IOException, InvalidFormatException {
        return new SheetIterator(this.workbookPart);
    }
    
    public static class SheetIterator implements Iterator<InputStream>
    {
        private Map<String, PackagePart> sheetMap;
        private CTSheet ctSheet;
        private Iterator<CTSheet> sheetIterator;
        
        private SheetIterator(final PackagePart wb) throws IOException {
            try {
                this.sheetMap = new HashMap<String, PackagePart>();
                for (final PackageRelationship rel : wb.getRelationships()) {
                    if (rel.getRelationshipType().equals(XSSFRelation.WORKSHEET.getRelation()) || rel.getRelationshipType().equals(XSSFRelation.CHARTSHEET.getRelation())) {
                        final PackagePartName relName = PackagingURIHelper.createPartName(rel.getTargetURI());
                        this.sheetMap.put(rel.getId(), wb.getPackage().getPart(relName));
                    }
                }
                final CTWorkbook wbBean = WorkbookDocument.Factory.parse(wb.getInputStream()).getWorkbook();
                this.sheetIterator = wbBean.getSheets().getSheetList().iterator();
            }
            catch (InvalidFormatException e) {
                throw new POIXMLException(e);
            }
            catch (XmlException e2) {
                throw new POIXMLException(e2);
            }
        }
        
        public boolean hasNext() {
            return this.sheetIterator.hasNext();
        }
        
        public InputStream next() {
            this.ctSheet = this.sheetIterator.next();
            final String sheetId = this.ctSheet.getId();
            try {
                final PackagePart sheetPkg = this.sheetMap.get(sheetId);
                return sheetPkg.getInputStream();
            }
            catch (IOException e) {
                throw new POIXMLException(e);
            }
        }
        
        public String getSheetName() {
            return this.ctSheet.getName();
        }
        
        public CommentsTable getSheetComments() {
            final PackagePart sheetPkg = this.getSheetPart();
            try {
                final PackageRelationshipCollection commentsList = sheetPkg.getRelationshipsByType(XSSFRelation.SHEET_COMMENTS.getRelation());
                if (commentsList.size() > 0) {
                    final PackageRelationship comments = commentsList.getRelationship(0);
                    final PackagePartName commentsName = PackagingURIHelper.createPartName(comments.getTargetURI());
                    final PackagePart commentsPart = sheetPkg.getPackage().getPart(commentsName);
                    return new CommentsTable(commentsPart, comments);
                }
            }
            catch (InvalidFormatException e) {
                return null;
            }
            catch (IOException e2) {
                return null;
            }
            return null;
        }
        
        public PackagePart getSheetPart() {
            final String sheetId = this.ctSheet.getId();
            return this.sheetMap.get(sheetId);
        }
        
        public void remove() {
            throw new IllegalStateException("Not supported");
        }
    }
}
