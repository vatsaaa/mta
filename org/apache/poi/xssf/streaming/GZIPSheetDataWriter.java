// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.streaming;

import java.util.zip.GZIPInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.FileOutputStream;
import java.io.Writer;
import java.io.File;
import java.io.IOException;

public class GZIPSheetDataWriter extends SheetDataWriter
{
    public GZIPSheetDataWriter() throws IOException {
    }
    
    @Override
    public File createTempFile() throws IOException {
        final File fd = File.createTempFile("poi-sxssf-sheet-xml", ".gz");
        fd.deleteOnExit();
        return fd;
    }
    
    @Override
    public Writer createWriter(final File fd) throws IOException {
        return new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(fd)));
    }
    
    @Override
    public InputStream getWorksheetXMLInputStream() throws IOException {
        final File fd = this.getTempFile();
        return new GZIPInputStream(new FileInputStream(fd));
    }
}
