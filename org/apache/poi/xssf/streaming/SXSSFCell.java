// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.streaming;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.formula.FormulaParseException;
import org.apache.poi.ss.usermodel.RichTextString;
import java.util.Calendar;
import org.apache.poi.ss.usermodel.DateUtil;
import java.util.Date;
import org.apache.poi.ss.usermodel.FormulaError;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Cell;

public class SXSSFCell implements Cell
{
    SXSSFRow _row;
    Value _value;
    CellStyle _style;
    Property _firstProperty;
    
    public SXSSFCell(final SXSSFRow row, final int cellType) {
        this._row = row;
        this.setType(cellType);
    }
    
    public int getColumnIndex() {
        return this._row.getCellIndex(this);
    }
    
    public int getRowIndex() {
        return this._row.getRowNum();
    }
    
    public Sheet getSheet() {
        return this._row.getSheet();
    }
    
    public Row getRow() {
        return this._row;
    }
    
    public void setCellType(final int cellType) {
        this.ensureType(cellType);
    }
    
    public int getCellType() {
        return this._value.getType();
    }
    
    public int getCachedFormulaResultType() {
        if (this._value.getType() != 2) {
            throw new IllegalStateException("Only formula cells have cached results");
        }
        return ((FormulaValue)this._value).getFormulaType();
    }
    
    public void setCellValue(final double value) {
        if (Double.isInfinite(value)) {
            this.setCellErrorValue(FormulaError.DIV0.getCode());
        }
        else if (Double.isNaN(value)) {
            this.setCellErrorValue(FormulaError.NUM.getCode());
        }
        else {
            this.ensureTypeOrFormulaType(0);
            if (this._value.getType() == 2) {
                ((NumericFormulaValue)this._value).setPreEvaluatedValue(value);
            }
            else {
                ((NumericValue)this._value).setValue(value);
            }
        }
    }
    
    public void setCellValue(final Date value) {
        final boolean date1904 = false;
        this.setCellValue(DateUtil.getExcelDate(value, date1904));
    }
    
    public void setCellValue(final Calendar value) {
        final boolean date1904 = false;
        this.setCellValue(DateUtil.getExcelDate(value, date1904));
    }
    
    public void setCellValue(final RichTextString value) {
        this.ensureRichTextStringType();
        ((RichTextValue)this._value).setValue(value);
    }
    
    public void setCellValue(final String value) {
        this.ensureTypeOrFormulaType(1);
        if (this._value.getType() == 2) {
            ((StringFormulaValue)this._value).setPreEvaluatedValue(value);
        }
        else {
            ((PlainStringValue)this._value).setValue(value);
        }
    }
    
    public void setCellFormula(final String formula) throws FormulaParseException {
        if (formula == null) {
            this.setType(3);
            return;
        }
        this.ensureFormulaType(this.computeTypeFromFormula(formula));
        ((FormulaValue)this._value).setValue(formula);
    }
    
    public String getCellFormula() {
        if (this._value.getType() != 2) {
            throw typeMismatch(2, this._value.getType(), false);
        }
        return ((FormulaValue)this._value).getValue();
    }
    
    public double getNumericCellValue() {
        final int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return 0.0;
            }
            case 2: {
                final FormulaValue fv = (FormulaValue)this._value;
                if (fv.getFormulaType() != 0) {
                    throw typeMismatch(0, 2, false);
                }
                return ((NumericFormulaValue)this._value).getPreEvaluatedValue();
            }
            case 0: {
                return ((NumericValue)this._value).getValue();
            }
            default: {
                throw typeMismatch(0, cellType, false);
            }
        }
    }
    
    public Date getDateCellValue() {
        final int cellType = this.getCellType();
        if (cellType == 3) {
            return null;
        }
        final double value = this.getNumericCellValue();
        final boolean date1904 = false;
        return DateUtil.getJavaDate(value, date1904);
    }
    
    public RichTextString getRichStringCellValue() {
        final int cellType = this.getCellType();
        if (this.getCellType() != 1) {
            throw typeMismatch(1, cellType, false);
        }
        final StringValue sval = (StringValue)this._value;
        if (sval.isRichText()) {
            return ((RichTextValue)this._value).getValue();
        }
        final String plainText = this.getStringCellValue();
        return this.getSheet().getWorkbook().getCreationHelper().createRichTextString(plainText);
    }
    
    public String getStringCellValue() {
        final int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return "";
            }
            case 2: {
                final FormulaValue fv = (FormulaValue)this._value;
                if (fv.getFormulaType() != 1) {
                    throw typeMismatch(1, 2, false);
                }
                return ((StringFormulaValue)this._value).getPreEvaluatedValue();
            }
            case 1: {
                if (((StringValue)this._value).isRichText()) {
                    return ((RichTextValue)this._value).getValue().getString();
                }
                return ((PlainStringValue)this._value).getValue();
            }
            default: {
                throw typeMismatch(1, cellType, false);
            }
        }
    }
    
    public void setCellValue(final boolean value) {
        this.ensureTypeOrFormulaType(4);
        if (this._value.getType() == 2) {
            ((BooleanFormulaValue)this._value).setPreEvaluatedValue(value);
        }
        else {
            ((BooleanValue)this._value).setValue(value);
        }
    }
    
    public void setCellErrorValue(final byte value) {
        this.ensureType(5);
        if (this._value.getType() == 2) {
            ((ErrorFormulaValue)this._value).setPreEvaluatedValue(value);
        }
        else {
            ((ErrorValue)this._value).setValue(value);
        }
    }
    
    public boolean getBooleanCellValue() {
        final int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return false;
            }
            case 2: {
                final FormulaValue fv = (FormulaValue)this._value;
                if (fv.getFormulaType() != 4) {
                    throw typeMismatch(4, 2, false);
                }
                return ((BooleanFormulaValue)this._value).getPreEvaluatedValue();
            }
            case 4: {
                return ((BooleanValue)this._value).getValue();
            }
            default: {
                throw typeMismatch(4, cellType, false);
            }
        }
    }
    
    public byte getErrorCellValue() {
        final int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return 0;
            }
            case 2: {
                final FormulaValue fv = (FormulaValue)this._value;
                if (fv.getFormulaType() != 5) {
                    throw typeMismatch(5, 2, false);
                }
                return ((ErrorFormulaValue)this._value).getPreEvaluatedValue();
            }
            case 5: {
                return ((ErrorValue)this._value).getValue();
            }
            default: {
                throw typeMismatch(5, cellType, false);
            }
        }
    }
    
    public void setCellStyle(final CellStyle style) {
        this._style = style;
    }
    
    public CellStyle getCellStyle() {
        if (this._style == null) {
            final SXSSFWorkbook wb = (SXSSFWorkbook)this.getRow().getSheet().getWorkbook();
            return wb.getCellStyleAt((short)0);
        }
        return this._style;
    }
    
    public void setAsActiveCell() {
    }
    
    public void setCellComment(final Comment comment) {
        this.setProperty(1, comment);
    }
    
    public Comment getCellComment() {
        return (Comment)this.getPropertyValue(1);
    }
    
    public void removeCellComment() {
        this.removeProperty(1);
    }
    
    public Hyperlink getHyperlink() {
        return (Hyperlink)this.getPropertyValue(2);
    }
    
    public void setHyperlink(final Hyperlink link) {
        this.setProperty(2, link);
        final XSSFHyperlink xssfobj = (XSSFHyperlink)link;
        final CellReference ref = new CellReference(this.getRowIndex(), this.getColumnIndex());
        xssfobj.getCTHyperlink().setRef(ref.formatAsString());
        ((SXSSFSheet)this.getSheet())._sh.addHyperlink(xssfobj);
    }
    
    public CellRangeAddress getArrayFormulaRange() {
        return null;
    }
    
    public boolean isPartOfArrayFormulaGroup() {
        return false;
    }
    
    @Override
    public String toString() {
        switch (this.getCellType()) {
            case 3: {
                return "";
            }
            case 4: {
                return this.getBooleanCellValue() ? "TRUE" : "FALSE";
            }
            case 5: {
                return ErrorEval.getText(this.getErrorCellValue());
            }
            case 2: {
                return this.getCellFormula();
            }
            case 0: {
                if (DateUtil.isCellDateFormatted(this)) {
                    final DateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                    return sdf.format(this.getDateCellValue());
                }
                return this.getNumericCellValue() + "";
            }
            case 1: {
                return this.getRichStringCellValue().toString();
            }
            default: {
                return "Unknown Cell Type: " + this.getCellType();
            }
        }
    }
    
    void removeProperty(final int type) {
        Property current = this._firstProperty;
        Property previous = null;
        while (current != null && current.getType() != type) {
            previous = current;
            current = current._next;
        }
        if (current != null) {
            if (previous != null) {
                previous._next = current._next;
            }
            else {
                this._firstProperty = current._next;
            }
        }
    }
    
    void setProperty(final int type, final Object value) {
        Property current = this._firstProperty;
        Property previous = null;
        while (current != null && current.getType() != type) {
            previous = current;
            current = current._next;
        }
        if (current != null) {
            current.setValue(value);
        }
        else {
            switch (type) {
                case 1: {
                    current = new CommentProperty(value);
                    break;
                }
                case 2: {
                    current = new HyperlinkProperty(value);
                    break;
                }
            }
            if (previous != null) {
                previous._next = current;
            }
            else {
                this._firstProperty = current;
            }
        }
    }
    
    Object getPropertyValue(final int type) {
        return this.getPropertyValue(type, null);
    }
    
    Object getPropertyValue(final int type, final String defaultValue) {
        Property current;
        for (current = this._firstProperty; current != null && current.getType() != type; current = current._next) {}
        return (current == null) ? defaultValue : current.getValue();
    }
    
    void ensurePlainStringType() {
        if (this._value.getType() != 1 || ((StringValue)this._value).isRichText()) {
            this._value = new PlainStringValue();
        }
    }
    
    void ensureRichTextStringType() {
        if (this._value.getType() != 1 || !((StringValue)this._value).isRichText()) {
            this._value = new RichTextValue();
        }
    }
    
    void ensureType(final int type) {
        if (this._value.getType() != type) {
            this.setType(type);
        }
    }
    
    void ensureFormulaType(final int type) {
        if (this._value.getType() != 2 || ((FormulaValue)this._value).getFormulaType() != type) {
            this.setFormulaType(type);
        }
    }
    
    void ensureTypeOrFormulaType(final int type) {
        assert type == 5;
        if (this._value.getType() == type) {
            if (type == 1 && ((StringValue)this._value).isRichText()) {
                this.setType(1);
            }
            return;
        }
        if (this._value.getType() != 2) {
            this.setType(type);
            return;
        }
        if (((FormulaValue)this._value).getFormulaType() == type) {
            return;
        }
        this.setFormulaType(type);
    }
    
    void setType(final int type) {
        switch (type) {
            case 0: {
                this._value = new NumericValue();
                break;
            }
            case 1: {
                final PlainStringValue sval = new PlainStringValue();
                if (this._value != null) {
                    final String str = this.convertCellValueToString();
                    sval.setValue(str);
                }
                this._value = sval;
                break;
            }
            case 2: {
                this._value = new NumericFormulaValue();
                break;
            }
            case 3: {
                this._value = new BlankValue();
                break;
            }
            case 4: {
                final BooleanValue bval = new BooleanValue();
                if (this._value != null) {
                    final boolean val = this.convertCellValueToBoolean();
                    bval.setValue(val);
                }
                this._value = bval;
                break;
            }
            case 5: {
                this._value = new ErrorValue();
                break;
            }
            default: {
                throw new IllegalArgumentException("Illegal type " + type);
            }
        }
    }
    
    void setFormulaType(final int type) {
        switch (type) {
            case 0: {
                this._value = new NumericFormulaValue();
                break;
            }
            case 1: {
                this._value = new StringFormulaValue();
                break;
            }
            case 4: {
                this._value = new BooleanFormulaValue();
                break;
            }
            case 5: {
                this._value = new ErrorFormulaValue();
                break;
            }
            default: {
                throw new IllegalArgumentException("Illegal type " + type);
            }
        }
    }
    
    int computeTypeFromFormula(final String formula) {
        return 0;
    }
    
    private static RuntimeException typeMismatch(final int expectedTypeCode, final int actualTypeCode, final boolean isFormulaCell) {
        final String msg = "Cannot get a " + getCellTypeName(expectedTypeCode) + " value from a " + getCellTypeName(actualTypeCode) + " " + (isFormulaCell ? "formula " : "") + "cell";
        return new IllegalStateException(msg);
    }
    
    private static String getCellTypeName(final int cellTypeCode) {
        switch (cellTypeCode) {
            case 3: {
                return "blank";
            }
            case 1: {
                return "text";
            }
            case 4: {
                return "boolean";
            }
            case 5: {
                return "error";
            }
            case 0: {
                return "numeric";
            }
            case 2: {
                return "formula";
            }
            default: {
                return "#unknown cell type (" + cellTypeCode + ")#";
            }
        }
    }
    
    private boolean convertCellValueToBoolean() {
        int cellType = this.getCellType();
        if (cellType == 2) {
            cellType = this.getCachedFormulaResultType();
        }
        switch (cellType) {
            case 4: {
                return this.getBooleanCellValue();
            }
            case 1: {
                final String text = this.getStringCellValue();
                return Boolean.parseBoolean(text);
            }
            case 0: {
                return this.getNumericCellValue() != 0.0;
            }
            case 3:
            case 5: {
                return false;
            }
            default: {
                throw new RuntimeException("Unexpected cell type (" + cellType + ")");
            }
        }
    }
    
    private String convertCellValueToString() {
        final int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return "";
            }
            case 4: {
                return this.getBooleanCellValue() ? "TRUE" : "FALSE";
            }
            case 1: {
                return this.getStringCellValue();
            }
            case 0:
            case 5: {
                final byte errVal = this.getErrorCellValue();
                return FormulaError.forInt(errVal).getString();
            }
            case 2: {
                return "";
            }
            default: {
                throw new IllegalStateException("Unexpected cell type (" + cellType + ")");
            }
        }
    }
    
    abstract static class Property
    {
        static final int COMMENT = 1;
        static final int HYPERLINK = 2;
        Object _value;
        Property _next;
        
        public Property(final Object value) {
            this._value = value;
        }
        
        abstract int getType();
        
        void setValue(final Object value) {
            this._value = value;
        }
        
        Object getValue() {
            return this._value;
        }
    }
    
    static class CommentProperty extends Property
    {
        public CommentProperty(final Object value) {
            super(value);
        }
        
        public int getType() {
            return 1;
        }
    }
    
    static class HyperlinkProperty extends Property
    {
        public HyperlinkProperty(final Object value) {
            super(value);
        }
        
        public int getType() {
            return 2;
        }
    }
    
    static class NumericValue implements Value
    {
        double _value;
        
        public int getType() {
            return 0;
        }
        
        void setValue(final double value) {
            this._value = value;
        }
        
        double getValue() {
            return this._value;
        }
    }
    
    abstract static class StringValue implements Value
    {
        public int getType() {
            return 1;
        }
        
        abstract boolean isRichText();
    }
    
    static class PlainStringValue extends StringValue
    {
        String _value;
        
        void setValue(final String value) {
            this._value = value;
        }
        
        String getValue() {
            return this._value;
        }
        
        @Override
        boolean isRichText() {
            return false;
        }
    }
    
    static class RichTextValue extends StringValue
    {
        RichTextString _value;
        
        @Override
        public int getType() {
            return 1;
        }
        
        void setValue(final RichTextString value) {
            this._value = value;
        }
        
        RichTextString getValue() {
            return this._value;
        }
        
        @Override
        boolean isRichText() {
            return true;
        }
    }
    
    abstract static class FormulaValue implements Value
    {
        String _value;
        
        public int getType() {
            return 2;
        }
        
        void setValue(final String value) {
            this._value = value;
        }
        
        String getValue() {
            return this._value;
        }
        
        abstract int getFormulaType();
    }
    
    static class NumericFormulaValue extends FormulaValue
    {
        double _preEvaluatedValue;
        
        @Override
        int getFormulaType() {
            return 0;
        }
        
        void setPreEvaluatedValue(final double value) {
            this._preEvaluatedValue = value;
        }
        
        double getPreEvaluatedValue() {
            return this._preEvaluatedValue;
        }
    }
    
    static class StringFormulaValue extends FormulaValue
    {
        String _preEvaluatedValue;
        
        @Override
        int getFormulaType() {
            return 1;
        }
        
        void setPreEvaluatedValue(final String value) {
            this._preEvaluatedValue = value;
        }
        
        String getPreEvaluatedValue() {
            return this._preEvaluatedValue;
        }
    }
    
    static class BooleanFormulaValue extends FormulaValue
    {
        boolean _preEvaluatedValue;
        
        @Override
        int getFormulaType() {
            return 4;
        }
        
        void setPreEvaluatedValue(final boolean value) {
            this._preEvaluatedValue = value;
        }
        
        boolean getPreEvaluatedValue() {
            return this._preEvaluatedValue;
        }
    }
    
    static class ErrorFormulaValue extends FormulaValue
    {
        byte _preEvaluatedValue;
        
        @Override
        int getFormulaType() {
            return 5;
        }
        
        void setPreEvaluatedValue(final byte value) {
            this._preEvaluatedValue = value;
        }
        
        byte getPreEvaluatedValue() {
            return this._preEvaluatedValue;
        }
    }
    
    static class BlankValue implements Value
    {
        public int getType() {
            return 3;
        }
    }
    
    static class BooleanValue implements Value
    {
        boolean _value;
        
        public int getType() {
            return 4;
        }
        
        void setValue(final boolean value) {
            this._value = value;
        }
        
        boolean getValue() {
            return this._value;
        }
    }
    
    static class ErrorValue implements Value
    {
        byte _value;
        
        public int getType() {
            return 5;
        }
        
        void setValue(final byte value) {
            this._value = value;
        }
        
        byte getValue() {
            return this._value;
        }
    }
    
    interface Value
    {
        int getType();
    }
}
