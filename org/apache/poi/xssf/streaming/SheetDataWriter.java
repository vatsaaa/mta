// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.streaming;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FormulaError;
import org.apache.poi.ss.util.CellReference;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.io.File;

public class SheetDataWriter
{
    private final File _fd;
    private final Writer _out;
    private int _rownum;
    private boolean _rowContainedNullCells;
    int _numberOfFlushedRows;
    int _lowestIndexOfFlushedRows;
    int _numberOfCellsOfLastFlushedRow;
    
    public SheetDataWriter() throws IOException {
        this._rowContainedNullCells = false;
        this._fd = this.createTempFile();
        this._out = this.createWriter(this._fd);
    }
    
    public File createTempFile() throws IOException {
        final File fd = File.createTempFile("poi-sxssf-sheet", ".xml");
        fd.deleteOnExit();
        return fd;
    }
    
    public Writer createWriter(final File fd) throws IOException {
        return new BufferedWriter(new FileWriter(fd));
    }
    
    public void close() throws IOException {
        this._out.flush();
        this._out.close();
    }
    
    File getTempFile() {
        return this._fd;
    }
    
    public InputStream getWorksheetXMLInputStream() throws IOException {
        final File fd = this.getTempFile();
        return new FileInputStream(fd);
    }
    
    public int getNumberOfFlushedRows() {
        return this._numberOfFlushedRows;
    }
    
    public int getNumberOfCellsOfLastFlushedRow() {
        return this._numberOfCellsOfLastFlushedRow;
    }
    
    public int getLowestIndexOfFlushedRows() {
        return this._lowestIndexOfFlushedRows;
    }
    
    @Override
    protected void finalize() throws Throwable {
        this._fd.delete();
    }
    
    public void writeRow(final int rownum, final SXSSFRow row) throws IOException {
        if (this._numberOfFlushedRows == 0) {
            this._lowestIndexOfFlushedRows = rownum;
        }
        this._numberOfCellsOfLastFlushedRow = row.getLastCellNum();
        ++this._numberOfFlushedRows;
        this.beginRow(rownum, row);
        final Iterator<Cell> cells = row.allCellsIterator();
        int columnIndex = 0;
        while (cells.hasNext()) {
            this.writeCell(columnIndex++, cells.next());
        }
        this.endRow();
    }
    
    void beginRow(final int rownum, final SXSSFRow row) throws IOException {
        this._out.write("<row r=\"" + (rownum + 1) + "\"");
        if (row.hasCustomHeight()) {
            this._out.write(" customHeight=\"true\"  ht=\"" + row.getHeightInPoints() + "\"");
        }
        if (row.getZeroHeight()) {
            this._out.write(" hidden=\"true\"");
        }
        if (row.isFormatted()) {
            this._out.write(" s=\"" + row._style + "\"");
            this._out.write(" customFormat=\"1\"");
        }
        if (row.getOutlineLevel() != 0) {
            this._out.write(" outlineLevel=\"" + row.getOutlineLevel() + "\"");
        }
        this._out.write(">\n");
        this._rownum = rownum;
        this._rowContainedNullCells = false;
    }
    
    void endRow() throws IOException {
        this._out.write("</row>\n");
    }
    
    public void writeCell(final int columnIndex, final Cell cell) throws IOException {
        if (cell == null) {
            this._rowContainedNullCells = true;
            return;
        }
        final String ref = new CellReference(this._rownum, columnIndex).formatAsString();
        this._out.write("<c r=\"" + ref + "\"");
        final CellStyle cellStyle = cell.getCellStyle();
        if (cellStyle.getIndex() != 0) {
            this._out.write(" s=\"" + cellStyle.getIndex() + "\"");
        }
        final int cellType = cell.getCellType();
        switch (cellType) {
            case 3: {
                this._out.write(">");
                break;
            }
            case 2: {
                this._out.write(">");
                this._out.write("<f>");
                this.outputQuotedString(cell.getCellFormula());
                this._out.write("</f>");
                switch (cell.getCachedFormulaResultType()) {
                    case 0: {
                        final double nval = cell.getNumericCellValue();
                        if (!Double.isNaN(nval)) {
                            this._out.write("<v>" + nval + "</v>");
                            break;
                        }
                        break;
                    }
                }
                break;
            }
            case 1: {
                this._out.write(" t=\"inlineStr\">");
                this._out.write("<is><t>");
                this.outputQuotedString(cell.getStringCellValue());
                this._out.write("</t></is>");
                break;
            }
            case 0: {
                this._out.write(" t=\"n\">");
                this._out.write("<v>" + cell.getNumericCellValue() + "</v>");
                break;
            }
            case 4: {
                this._out.write(" t=\"b\">");
                this._out.write("<v>" + (cell.getBooleanCellValue() ? "1" : "0") + "</v>");
                break;
            }
            case 5: {
                final FormulaError error = FormulaError.forInt(cell.getErrorCellValue());
                this._out.write(" t=\"e\">");
                this._out.write("<v>" + error.getString() + "</v>");
                break;
            }
            default: {
                assert false;
                throw new RuntimeException("Huh?");
            }
        }
        this._out.write("</c>");
    }
    
    protected void outputQuotedString(final String s) throws IOException {
        if (s == null || s.length() == 0) {
            return;
        }
        final char[] chars = s.toCharArray();
        int last = 0;
        final int length = s.length();
        for (int counter = 0; counter < length; ++counter) {
            final char c = chars[counter];
            switch (c) {
                case '<': {
                    if (counter > last) {
                        this._out.write(chars, last, counter - last);
                    }
                    last = counter + 1;
                    this._out.write("&lt;");
                    break;
                }
                case '>': {
                    if (counter > last) {
                        this._out.write(chars, last, counter - last);
                    }
                    last = counter + 1;
                    this._out.write("&gt;");
                    break;
                }
                case '&': {
                    if (counter > last) {
                        this._out.write(chars, last, counter - last);
                    }
                    last = counter + 1;
                    this._out.write("&amp;");
                    break;
                }
                case '\"': {
                    if (counter > last) {
                        this._out.write(chars, last, counter - last);
                    }
                    last = counter + 1;
                    this._out.write("&quot;");
                    break;
                }
                case '\n':
                case '\r': {
                    if (counter > last) {
                        this._out.write(chars, last, counter - last);
                    }
                    this._out.write("&#xa;");
                    last = counter + 1;
                    break;
                }
                case '\t': {
                    if (counter > last) {
                        this._out.write(chars, last, counter - last);
                    }
                    this._out.write("&#x9;");
                    last = counter + 1;
                    break;
                }
                case ' ': {
                    if (counter > last) {
                        this._out.write(chars, last, counter - last);
                    }
                    this._out.write("&#xa0;");
                    last = counter + 1;
                    break;
                }
                default: {
                    if (c < ' ' || Character.isLowSurrogate(c) || Character.isHighSurrogate(c) || ('\ufffe' <= c && c <= '\uffff')) {
                        this._out.write(63);
                        last = counter + 1;
                        break;
                    }
                    if (c > '\u007f') {
                        if (counter > last) {
                            this._out.write(chars, last, counter - last);
                        }
                        last = counter + 1;
                        this._out.write("&#");
                        this._out.write(String.valueOf((int)c));
                        this._out.write(";");
                        break;
                    }
                    break;
                }
            }
        }
        if (last < length) {
            this._out.write(chars, last, length - last);
        }
    }
}
