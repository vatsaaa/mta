// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.streaming;

import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.usermodel.AutoFilter;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellRange;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.util.SheetUtil;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetFormatPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet;
import org.apache.poi.hssf.util.PaneInformation;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.CellStyle;
import java.util.Map;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Row;
import java.util.Iterator;
import java.io.InputStream;
import java.io.IOException;
import java.util.TreeMap;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.ss.usermodel.Sheet;

public class SXSSFSheet implements Sheet, Cloneable
{
    SXSSFWorkbook _workbook;
    XSSFSheet _sh;
    TreeMap<Integer, SXSSFRow> _rows;
    SheetDataWriter _writer;
    int _randomAccessWindowSize;
    int outlineLevelRow;
    
    public SXSSFSheet(final SXSSFWorkbook workbook, final XSSFSheet xSheet) throws IOException {
        this._rows = new TreeMap<Integer, SXSSFRow>();
        this._randomAccessWindowSize = 100;
        this.outlineLevelRow = 0;
        this._workbook = workbook;
        this._sh = xSheet;
        this._writer = workbook.createSheetDataWriter();
        this.setRandomAccessWindowSize(this._workbook.getRandomAccessWindowSize());
    }
    
    SheetDataWriter getSheetDataWriter() {
        return this._writer;
    }
    
    public InputStream getWorksheetXMLInputStream() throws IOException {
        this.flushRows(0);
        this._writer.close();
        return this._writer.getWorksheetXMLInputStream();
    }
    
    public Iterator<Row> iterator() {
        return this.rowIterator();
    }
    
    public Row createRow(final int rownum) {
        final int maxrow = SpreadsheetVersion.EXCEL2007.getLastRowIndex();
        if (rownum < 0 || rownum > maxrow) {
            throw new IllegalArgumentException("Invalid row number (" + rownum + ") outside allowable range (0.." + maxrow + ")");
        }
        final Row previousRow = (rownum > 0) ? this.getRow(rownum - 1) : null;
        int initialAllocationSize = 0;
        if (previousRow != null) {
            initialAllocationSize = previousRow.getLastCellNum();
        }
        if (initialAllocationSize <= 0 && this._writer.getNumberOfFlushedRows() > 0) {
            initialAllocationSize = this._writer.getNumberOfCellsOfLastFlushedRow();
        }
        if (initialAllocationSize <= 0) {
            initialAllocationSize = 10;
        }
        final SXSSFRow newRow = new SXSSFRow(this, initialAllocationSize);
        this._rows.put(new Integer(rownum), newRow);
        if (this._randomAccessWindowSize >= 0 && this._rows.size() > this._randomAccessWindowSize) {
            try {
                this.flushRows(this._randomAccessWindowSize);
            }
            catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
        return newRow;
    }
    
    public void removeRow(final Row row) {
        if (row.getSheet() != this) {
            throw new IllegalArgumentException("Specified row does not belong to this sheet");
        }
        final Iterator<Map.Entry<Integer, SXSSFRow>> iter = this._rows.entrySet().iterator();
        while (iter.hasNext()) {
            final Map.Entry<Integer, SXSSFRow> entry = iter.next();
            if (entry.getValue() == row) {
                iter.remove();
            }
        }
    }
    
    public Row getRow(final int rownum) {
        return this._rows.get(new Integer(rownum));
    }
    
    public int getPhysicalNumberOfRows() {
        return this._rows.size() + this._writer.getNumberOfFlushedRows();
    }
    
    public int getFirstRowNum() {
        if (this._writer.getNumberOfFlushedRows() > 0) {
            return this._writer.getLowestIndexOfFlushedRows();
        }
        return (this._rows.size() == 0) ? 0 : this._rows.firstKey();
    }
    
    public int getLastRowNum() {
        return (this._rows.size() == 0) ? 0 : this._rows.lastKey();
    }
    
    public void setColumnHidden(final int columnIndex, final boolean hidden) {
        this._sh.setColumnHidden(columnIndex, hidden);
    }
    
    public boolean isColumnHidden(final int columnIndex) {
        return this._sh.isColumnHidden(columnIndex);
    }
    
    public void setColumnWidth(final int columnIndex, final int width) {
        this._sh.setColumnWidth(columnIndex, width);
    }
    
    public int getColumnWidth(final int columnIndex) {
        return this._sh.getColumnWidth(columnIndex);
    }
    
    public void setDefaultColumnWidth(final int width) {
        this._sh.setDefaultColumnWidth(width);
    }
    
    public int getDefaultColumnWidth() {
        return this._sh.getDefaultColumnWidth();
    }
    
    public short getDefaultRowHeight() {
        return this._sh.getDefaultRowHeight();
    }
    
    public float getDefaultRowHeightInPoints() {
        return this._sh.getDefaultRowHeightInPoints();
    }
    
    public void setDefaultRowHeight(final short height) {
        this._sh.setDefaultRowHeight(height);
    }
    
    public void setDefaultRowHeightInPoints(final float height) {
        this._sh.setDefaultRowHeightInPoints(height);
    }
    
    public CellStyle getColumnStyle(final int column) {
        return this._sh.getColumnStyle(column);
    }
    
    public int addMergedRegion(final CellRangeAddress region) {
        return this._sh.addMergedRegion(region);
    }
    
    public void setVerticallyCenter(final boolean value) {
        this._sh.setVerticallyCenter(value);
    }
    
    public void setHorizontallyCenter(final boolean value) {
        this._sh.setHorizontallyCenter(value);
    }
    
    public boolean getHorizontallyCenter() {
        return this._sh.getHorizontallyCenter();
    }
    
    public boolean getVerticallyCenter() {
        return this._sh.getVerticallyCenter();
    }
    
    public void removeMergedRegion(final int index) {
        this._sh.removeMergedRegion(index);
    }
    
    public int getNumMergedRegions() {
        return this._sh.getNumMergedRegions();
    }
    
    public CellRangeAddress getMergedRegion(final int index) {
        return this._sh.getMergedRegion(index);
    }
    
    public Iterator<Row> rowIterator() {
        final Iterator<Row> result = (Iterator<Row>)this._rows.values().iterator();
        return result;
    }
    
    public void setAutobreaks(final boolean value) {
        this._sh.setAutobreaks(value);
    }
    
    public void setDisplayGuts(final boolean value) {
        this._sh.setDisplayGuts(value);
    }
    
    public void setDisplayZeros(final boolean value) {
        this._sh.setDisplayZeros(value);
    }
    
    public boolean isDisplayZeros() {
        return this._sh.isDisplayZeros();
    }
    
    public void setRightToLeft(final boolean value) {
        this._sh.setRightToLeft(value);
    }
    
    public boolean isRightToLeft() {
        return this._sh.isRightToLeft();
    }
    
    public void setFitToPage(final boolean value) {
        this._sh.setFitToPage(value);
    }
    
    public void setRowSumsBelow(final boolean value) {
        this._sh.setRowSumsBelow(value);
    }
    
    public void setRowSumsRight(final boolean value) {
        this._sh.setRowSumsRight(value);
    }
    
    public boolean getAutobreaks() {
        return this._sh.getAutobreaks();
    }
    
    public boolean getDisplayGuts() {
        return this._sh.getDisplayGuts();
    }
    
    public boolean getFitToPage() {
        return this._sh.getFitToPage();
    }
    
    public boolean getRowSumsBelow() {
        return this._sh.getRowSumsBelow();
    }
    
    public boolean getRowSumsRight() {
        return this._sh.getRowSumsRight();
    }
    
    public boolean isPrintGridlines() {
        return this._sh.isPrintGridlines();
    }
    
    public void setPrintGridlines(final boolean show) {
        this._sh.setPrintGridlines(show);
    }
    
    public PrintSetup getPrintSetup() {
        return this._sh.getPrintSetup();
    }
    
    public Header getHeader() {
        return this._sh.getHeader();
    }
    
    public Footer getFooter() {
        return this._sh.getFooter();
    }
    
    public void setSelected(final boolean value) {
        this._sh.setSelected(value);
    }
    
    public double getMargin(final short margin) {
        return this._sh.getMargin(margin);
    }
    
    public void setMargin(final short margin, final double size) {
        this._sh.setMargin(margin, size);
    }
    
    public boolean getProtect() {
        return this._sh.getProtect();
    }
    
    public void protectSheet(final String password) {
        this._sh.protectSheet(password);
    }
    
    public boolean getScenarioProtect() {
        return this._sh.getScenarioProtect();
    }
    
    public void setZoom(final int numerator, final int denominator) {
        this._sh.setZoom(numerator, denominator);
    }
    
    public short getTopRow() {
        return this._sh.getTopRow();
    }
    
    public short getLeftCol() {
        return this._sh.getLeftCol();
    }
    
    public void showInPane(final short toprow, final short leftcol) {
        this._sh.showInPane(toprow, leftcol);
    }
    
    public void setForceFormulaRecalculation(final boolean value) {
        this._sh.setForceFormulaRecalculation(value);
    }
    
    public boolean getForceFormulaRecalculation() {
        return this._sh.getForceFormulaRecalculation();
    }
    
    public void shiftRows(final int startRow, final int endRow, final int n) {
        throw new RuntimeException("NotImplemented");
    }
    
    public void shiftRows(final int startRow, final int endRow, final int n, final boolean copyRowHeight, final boolean resetOriginalRowHeight) {
        throw new RuntimeException("NotImplemented");
    }
    
    public void createFreezePane(final int colSplit, final int rowSplit, final int leftmostColumn, final int topRow) {
        this._sh.createFreezePane(colSplit, rowSplit, leftmostColumn, topRow);
    }
    
    public void createFreezePane(final int colSplit, final int rowSplit) {
        this._sh.createFreezePane(colSplit, rowSplit);
    }
    
    public void createSplitPane(final int xSplitPos, final int ySplitPos, final int leftmostColumn, final int topRow, final int activePane) {
        this._sh.createSplitPane(xSplitPos, ySplitPos, leftmostColumn, topRow, activePane);
    }
    
    public PaneInformation getPaneInformation() {
        return this._sh.getPaneInformation();
    }
    
    public void setDisplayGridlines(final boolean show) {
        this._sh.setDisplayGridlines(show);
    }
    
    public boolean isDisplayGridlines() {
        return this._sh.isDisplayGridlines();
    }
    
    public void setDisplayFormulas(final boolean show) {
        this._sh.setDisplayFormulas(show);
    }
    
    public boolean isDisplayFormulas() {
        return this._sh.isDisplayFormulas();
    }
    
    public void setDisplayRowColHeadings(final boolean show) {
        this._sh.setDisplayRowColHeadings(show);
    }
    
    public boolean isDisplayRowColHeadings() {
        return this._sh.isDisplayRowColHeadings();
    }
    
    public void setRowBreak(final int row) {
        this._sh.setRowBreak(row);
    }
    
    public boolean isRowBroken(final int row) {
        return this._sh.isRowBroken(row);
    }
    
    public void removeRowBreak(final int row) {
        this._sh.removeRowBreak(row);
    }
    
    public int[] getRowBreaks() {
        return this._sh.getRowBreaks();
    }
    
    public int[] getColumnBreaks() {
        return this._sh.getColumnBreaks();
    }
    
    public void setColumnBreak(final int column) {
        this._sh.setColumnBreak(column);
    }
    
    public boolean isColumnBroken(final int column) {
        return this._sh.isColumnBroken(column);
    }
    
    public void removeColumnBreak(final int column) {
        this._sh.removeColumnBreak(column);
    }
    
    public void setColumnGroupCollapsed(final int columnNumber, final boolean collapsed) {
        this._sh.setColumnGroupCollapsed(columnNumber, collapsed);
    }
    
    public void groupColumn(final int fromColumn, final int toColumn) {
        this._sh.groupColumn(fromColumn, toColumn);
    }
    
    public void ungroupColumn(final int fromColumn, final int toColumn) {
        this._sh.ungroupColumn(fromColumn, toColumn);
    }
    
    public void groupRow(final int fromRow, final int toRow) {
        for (final SXSSFRow row : this._rows.subMap(fromRow, toRow + 1).values()) {
            final int level = row.getOutlineLevel() + 1;
            row.setOutlineLevel(level);
            if (level > this.outlineLevelRow) {
                this.outlineLevelRow = level;
            }
        }
        final CTWorksheet ct = this._sh.getCTWorksheet();
        final CTSheetFormatPr pr = ct.isSetSheetFormatPr() ? ct.getSheetFormatPr() : ct.addNewSheetFormatPr();
        if (this.outlineLevelRow > 0) {
            pr.setOutlineLevelRow((short)this.outlineLevelRow);
        }
    }
    
    public void ungroupRow(final int fromRow, final int toRow) {
        this._sh.ungroupRow(fromRow, toRow);
    }
    
    public void setRowGroupCollapsed(final int row, final boolean collapse) {
        throw new RuntimeException("Not Implemented");
    }
    
    public void setDefaultColumnStyle(final int column, final CellStyle style) {
        this._sh.setDefaultColumnStyle(column, style);
    }
    
    public void autoSizeColumn(final int column) {
        this.autoSizeColumn(column, false);
    }
    
    public void autoSizeColumn(final int column, final boolean useMergedCells) {
        double width = SheetUtil.getColumnWidth(this, column, useMergedCells);
        if (width != -1.0) {
            width *= 256.0;
            final int maxColumnWidth = 65280;
            if (width > maxColumnWidth) {
                width = maxColumnWidth;
            }
            this.setColumnWidth(column, (int)width);
        }
    }
    
    public Comment getCellComment(final int row, final int column) {
        return this._sh.getCellComment(row, column);
    }
    
    public Drawing createDrawingPatriarch() {
        return this._sh.createDrawingPatriarch();
    }
    
    public Workbook getWorkbook() {
        return this._workbook;
    }
    
    public String getSheetName() {
        return this._sh.getSheetName();
    }
    
    public boolean isSelected() {
        return this._sh.isSelected();
    }
    
    public CellRange<? extends Cell> setArrayFormula(final String formula, final CellRangeAddress range) {
        return this._sh.setArrayFormula(formula, range);
    }
    
    public CellRange<? extends Cell> removeArrayFormula(final Cell cell) {
        return this._sh.removeArrayFormula(cell);
    }
    
    public DataValidationHelper getDataValidationHelper() {
        return this._sh.getDataValidationHelper();
    }
    
    public void addValidationData(final DataValidation dataValidation) {
        this._sh.addValidationData(dataValidation);
    }
    
    public AutoFilter setAutoFilter(final CellRangeAddress range) {
        return this._sh.setAutoFilter(range);
    }
    
    public SheetConditionalFormatting getSheetConditionalFormatting() {
        return this._sh.getSheetConditionalFormatting();
    }
    
    public void setRandomAccessWindowSize(final int value) {
        if (value == 0 || value < -1) {
            throw new IllegalArgumentException("RandomAccessWindowSize must be either -1 or a positive integer");
        }
        this._randomAccessWindowSize = value;
    }
    
    public void flushRows(final int remaining) throws IOException {
        while (this._rows.size() > remaining) {
            this.flushOneRow();
        }
    }
    
    public void flushRows() throws IOException {
        this.flushRows(0);
    }
    
    private void flushOneRow() throws IOException {
        final Integer firstRowNum = this._rows.firstKey();
        if (firstRowNum != null) {
            final int rowIndex = firstRowNum;
            final SXSSFRow row = this._rows.get(firstRowNum);
            this._writer.writeRow(rowIndex, row);
            this._rows.remove(firstRowNum);
        }
    }
    
    public void changeRowNum(final SXSSFRow row, final int newRowNum) {
        this.removeRow(row);
        this._rows.put(new Integer(newRowNum), row);
    }
    
    public int getRowNum(final SXSSFRow row) {
        for (final Map.Entry<Integer, SXSSFRow> entry : this._rows.entrySet()) {
            if (entry.getValue() == row) {
                return entry.getKey();
            }
        }
        assert false;
        return -1;
    }
}
