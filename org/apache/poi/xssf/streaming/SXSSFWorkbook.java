// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.streaming;

import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.PictureData;
import java.util.List;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Name;
import java.io.FileOutputStream;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Sheet;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipFile;
import java.io.OutputStream;
import java.io.File;
import java.util.Iterator;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import java.util.HashMap;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class SXSSFWorkbook implements Workbook
{
    public static final int DEFAULT_WINDOW_SIZE = 100;
    XSSFWorkbook _wb;
    HashMap<SXSSFSheet, XSSFSheet> _sxFromXHash;
    HashMap<XSSFSheet, SXSSFSheet> _xFromSxHash;
    private int _randomAccessWindowSize;
    private boolean _compressTmpFiles;
    
    public SXSSFWorkbook() {
        this(null);
    }
    
    public SXSSFWorkbook(final XSSFWorkbook workbook) {
        this(workbook, 100);
    }
    
    public SXSSFWorkbook(final XSSFWorkbook workbook, final int rowAccessWindowSize) {
        this(workbook, rowAccessWindowSize, false);
    }
    
    public SXSSFWorkbook(final XSSFWorkbook workbook, final int rowAccessWindowSize, final boolean compressTmpFiles) {
        this._sxFromXHash = new HashMap<SXSSFSheet, XSSFSheet>();
        this._xFromSxHash = new HashMap<XSSFSheet, SXSSFSheet>();
        this._randomAccessWindowSize = 100;
        this._compressTmpFiles = false;
        this.setRandomAccessWindowSize(rowAccessWindowSize);
        this.setCompressTempFiles(compressTmpFiles);
        if (workbook == null) {
            this._wb = new XSSFWorkbook();
        }
        else {
            this._wb = workbook;
            for (int i = 0; i < this._wb.getNumberOfSheets(); ++i) {
                final XSSFSheet sheet = this._wb.getSheetAt(i);
                this.createAndRegisterSXSSFSheet(sheet);
            }
        }
    }
    
    public SXSSFWorkbook(final int rowAccessWindowSize) {
        this(null, rowAccessWindowSize);
    }
    
    public int getRandomAccessWindowSize() {
        return this._randomAccessWindowSize;
    }
    
    private void setRandomAccessWindowSize(final int rowAccessWindowSize) {
        if (rowAccessWindowSize == 0 || rowAccessWindowSize < -1) {
            throw new IllegalArgumentException("rowAccessWindowSize must be greater than 0 or -1");
        }
        this._randomAccessWindowSize = rowAccessWindowSize;
    }
    
    public void setCompressTempFiles(final boolean compress) {
        this._compressTmpFiles = compress;
    }
    
    SheetDataWriter createSheetDataWriter() throws IOException {
        if (this._compressTmpFiles) {
            return new GZIPSheetDataWriter();
        }
        return new SheetDataWriter();
    }
    
    XSSFSheet getXSSFSheet(final SXSSFSheet sheet) {
        final XSSFSheet result = this._sxFromXHash.get(sheet);
        assert result != null;
        return result;
    }
    
    SXSSFSheet getSXSSFSheet(final XSSFSheet sheet) {
        final SXSSFSheet result = this._xFromSxHash.get(sheet);
        return result;
    }
    
    void registerSheetMapping(final SXSSFSheet sxSheet, final XSSFSheet xSheet) {
        this._sxFromXHash.put(sxSheet, xSheet);
        this._xFromSxHash.put(xSheet, sxSheet);
    }
    
    void deregisterSheetMapping(final XSSFSheet xSheet) {
        final SXSSFSheet sxSheet = this.getSXSSFSheet(xSheet);
        this._sxFromXHash.remove(sxSheet);
        this._xFromSxHash.remove(xSheet);
    }
    
    private XSSFSheet getSheetFromZipEntryName(final String sheetRef) {
        for (final XSSFSheet sheet : this._sxFromXHash.values()) {
            if (sheetRef.equals(sheet.getPackagePart().getPartName().getName().substring(1))) {
                return sheet;
            }
        }
        return null;
    }
    
    private void injectData(final File zipfile, final OutputStream out) throws IOException {
        final ZipFile zip = new ZipFile(zipfile);
        try {
            final ZipOutputStream zos = new ZipOutputStream(out);
            try {
                final Enumeration<ZipEntry> en = (Enumeration<ZipEntry>)zip.entries();
                while (en.hasMoreElements()) {
                    final ZipEntry ze = en.nextElement();
                    zos.putNextEntry(new ZipEntry(ze.getName()));
                    final InputStream is = zip.getInputStream(ze);
                    final XSSFSheet xSheet = this.getSheetFromZipEntryName(ze.getName());
                    if (xSheet != null) {
                        final SXSSFSheet sxSheet = this.getSXSSFSheet(xSheet);
                        final InputStream xis = sxSheet.getWorksheetXMLInputStream();
                        try {
                            copyStreamAndInjectWorksheet(is, zos, xis);
                        }
                        finally {
                            xis.close();
                        }
                    }
                    else {
                        copyStream(is, zos);
                    }
                    is.close();
                }
            }
            finally {
                zos.close();
            }
        }
        finally {
            zip.close();
        }
    }
    
    private static void copyStream(final InputStream in, final OutputStream out) throws IOException {
        final byte[] chunk = new byte[1024];
        int count;
        while ((count = in.read(chunk)) >= 0) {
            out.write(chunk, 0, count);
        }
    }
    
    private static void copyStreamAndInjectWorksheet(final InputStream in, final OutputStream out, final InputStream worksheetData) throws IOException {
        final InputStreamReader inReader = new InputStreamReader(in, "UTF-8");
        final OutputStreamWriter outWriter = new OutputStreamWriter(out, "UTF-8");
        boolean needsStartTag = true;
        int pos = 0;
        String s = "<sheetData";
        int n = s.length();
        int c;
        while ((c = inReader.read()) != -1) {
            if (c == s.charAt(pos)) {
                if (++pos != n) {
                    continue;
                }
                if (!"<sheetData".equals(s)) {
                    break;
                }
                c = inReader.read();
                if (c == -1) {
                    outWriter.write(s);
                    break;
                }
                if (c == 62) {
                    outWriter.write(s);
                    outWriter.write(c);
                    s = "</sheetData>";
                    n = s.length();
                    pos = 0;
                    needsStartTag = false;
                }
                else if (c == 47) {
                    c = inReader.read();
                    if (c == -1) {
                        outWriter.write(s);
                        break;
                    }
                    if (c == 62) {
                        break;
                    }
                    outWriter.write(s);
                    outWriter.write(47);
                    outWriter.write(c);
                    pos = 0;
                }
                else {
                    outWriter.write(s);
                    outWriter.write(47);
                    outWriter.write(c);
                    pos = 0;
                }
            }
            else {
                if (pos > 0) {
                    outWriter.write(s, 0, pos);
                }
                if (c == s.charAt(0)) {
                    pos = 1;
                }
                else {
                    outWriter.write(c);
                    pos = 0;
                }
            }
        }
        outWriter.flush();
        if (needsStartTag) {
            outWriter.write("<sheetData>\n");
            outWriter.flush();
        }
        copyStream(worksheetData, out);
        outWriter.write("</sheetData>");
        outWriter.flush();
        while ((c = inReader.read()) != -1) {
            outWriter.write(c);
        }
        outWriter.flush();
    }
    
    public XSSFWorkbook getXSSFWorkbook() {
        return this._wb;
    }
    
    public int getActiveSheetIndex() {
        return this._wb.getActiveSheetIndex();
    }
    
    public void setActiveSheet(final int sheetIndex) {
        this._wb.setActiveSheet(sheetIndex);
    }
    
    public int getFirstVisibleTab() {
        return this._wb.getFirstVisibleTab();
    }
    
    public void setFirstVisibleTab(final int sheetIndex) {
        this._wb.setFirstVisibleTab(sheetIndex);
    }
    
    public void setSheetOrder(final String sheetname, final int pos) {
        this._wb.setSheetOrder(sheetname, pos);
    }
    
    public void setSelectedTab(final int index) {
        this._wb.setSelectedTab(index);
    }
    
    public void setSheetName(final int sheet, final String name) {
        this._wb.setSheetName(sheet, name);
    }
    
    public String getSheetName(final int sheet) {
        return this._wb.getSheetName(sheet);
    }
    
    public int getSheetIndex(final String name) {
        return this._wb.getSheetIndex(name);
    }
    
    public int getSheetIndex(final Sheet sheet) {
        assert sheet instanceof SXSSFSheet;
        return this._wb.getSheetIndex(this.getXSSFSheet((SXSSFSheet)sheet));
    }
    
    public Sheet createSheet() {
        return this.createAndRegisterSXSSFSheet(this._wb.createSheet());
    }
    
    SXSSFSheet createAndRegisterSXSSFSheet(final XSSFSheet xSheet) {
        SXSSFSheet sxSheet = null;
        try {
            sxSheet = new SXSSFSheet(this, xSheet);
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        this.registerSheetMapping(sxSheet, xSheet);
        return sxSheet;
    }
    
    public Sheet createSheet(final String sheetname) {
        return this.createAndRegisterSXSSFSheet(this._wb.createSheet(sheetname));
    }
    
    public Sheet cloneSheet(final int sheetNum) {
        throw new RuntimeException("NotImplemented");
    }
    
    public int getNumberOfSheets() {
        return this._wb.getNumberOfSheets();
    }
    
    public Sheet getSheetAt(final int index) {
        return this.getSXSSFSheet(this._wb.getSheetAt(index));
    }
    
    public Sheet getSheet(final String name) {
        return this.getSXSSFSheet(this._wb.getSheet(name));
    }
    
    public void removeSheetAt(final int index) {
        final XSSFSheet xSheet = this._wb.getSheetAt(index);
        this._wb.removeSheetAt(index);
        this.deregisterSheetMapping(xSheet);
    }
    
    public void setRepeatingRowsAndColumns(final int sheetIndex, final int startColumn, final int endColumn, final int startRow, final int endRow) {
        this._wb.setRepeatingRowsAndColumns(sheetIndex, startColumn, endColumn, startRow, endRow);
    }
    
    public Font createFont() {
        return this._wb.createFont();
    }
    
    public Font findFont(final short boldWeight, final short color, final short fontHeight, final String name, final boolean italic, final boolean strikeout, final short typeOffset, final byte underline) {
        return this._wb.findFont(boldWeight, color, fontHeight, name, italic, strikeout, typeOffset, underline);
    }
    
    public short getNumberOfFonts() {
        return this._wb.getNumberOfFonts();
    }
    
    public Font getFontAt(final short idx) {
        return this._wb.getFontAt(idx);
    }
    
    public CellStyle createCellStyle() {
        return this._wb.createCellStyle();
    }
    
    public short getNumCellStyles() {
        return this._wb.getNumCellStyles();
    }
    
    public CellStyle getCellStyleAt(final short idx) {
        return this._wb.getCellStyleAt(idx);
    }
    
    public void write(final OutputStream stream) throws IOException {
        for (final SXSSFSheet sheet : this._xFromSxHash.values()) {
            sheet.flushRows();
        }
        final File tmplFile = File.createTempFile("poi-sxssf-template", ".xlsx");
        tmplFile.deleteOnExit();
        final FileOutputStream os = new FileOutputStream(tmplFile);
        this._wb.write(os);
        os.close();
        this.injectData(tmplFile, stream);
        tmplFile.delete();
    }
    
    public int getNumberOfNames() {
        return this._wb.getNumberOfNames();
    }
    
    public Name getName(final String name) {
        return this._wb.getName(name);
    }
    
    public Name getNameAt(final int nameIndex) {
        return this._wb.getNameAt(nameIndex);
    }
    
    public Name createName() {
        return this._wb.createName();
    }
    
    public int getNameIndex(final String name) {
        return this._wb.getNameIndex(name);
    }
    
    public void removeName(final int index) {
        this._wb.removeName(index);
    }
    
    public void removeName(final String name) {
        this._wb.removeName(name);
    }
    
    public void setPrintArea(final int sheetIndex, final String reference) {
        this._wb.setPrintArea(sheetIndex, reference);
    }
    
    public void setPrintArea(final int sheetIndex, final int startColumn, final int endColumn, final int startRow, final int endRow) {
        this._wb.setPrintArea(sheetIndex, startColumn, endColumn, startRow, endRow);
    }
    
    public String getPrintArea(final int sheetIndex) {
        return this._wb.getPrintArea(sheetIndex);
    }
    
    public void removePrintArea(final int sheetIndex) {
        this._wb.removePrintArea(sheetIndex);
    }
    
    public Row.MissingCellPolicy getMissingCellPolicy() {
        return this._wb.getMissingCellPolicy();
    }
    
    public void setMissingCellPolicy(final Row.MissingCellPolicy missingCellPolicy) {
        this._wb.setMissingCellPolicy(missingCellPolicy);
    }
    
    public DataFormat createDataFormat() {
        return this._wb.createDataFormat();
    }
    
    public int addPicture(final byte[] pictureData, final int format) {
        return this._wb.addPicture(pictureData, format);
    }
    
    public List<? extends PictureData> getAllPictures() {
        return this._wb.getAllPictures();
    }
    
    public CreationHelper getCreationHelper() {
        return this._wb.getCreationHelper();
    }
    
    public boolean isHidden() {
        return this._wb.isHidden();
    }
    
    public void setHidden(final boolean hiddenFlag) {
        this._wb.setHidden(hiddenFlag);
    }
    
    public boolean isSheetHidden(final int sheetIx) {
        return this._wb.isSheetHidden(sheetIx);
    }
    
    public boolean isSheetVeryHidden(final int sheetIx) {
        return this._wb.isSheetVeryHidden(sheetIx);
    }
    
    public void setSheetHidden(final int sheetIx, final boolean hidden) {
        this._wb.setSheetHidden(sheetIx, hidden);
    }
    
    public void setSheetHidden(final int sheetIx, final int hidden) {
        this._wb.setSheetHidden(sheetIx, hidden);
    }
    
    public void addToolPack(final UDFFinder toopack) {
        this._wb.addToolPack(toopack);
    }
    
    public void setForceFormulaRecalculation(final boolean value) {
        this._wb.setForceFormulaRecalculation(value);
    }
    
    public boolean getForceFormulaRecalculation() {
        return this._wb.getForceFormulaRecalculation();
    }
}
