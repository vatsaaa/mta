// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.streaming;

import java.util.NoSuchElementException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Row;

public class SXSSFRow implements Row
{
    SXSSFSheet _sheet;
    SXSSFCell[] _cells;
    int _maxColumn;
    short _style;
    short _height;
    boolean _zHeight;
    int _outlineLevel;
    
    public SXSSFRow(final SXSSFSheet sheet, final int initialSize) {
        this._maxColumn = -1;
        this._style = -1;
        this._height = -1;
        this._zHeight = false;
        this._outlineLevel = 0;
        this._sheet = sheet;
        this._cells = new SXSSFCell[initialSize];
    }
    
    public Iterator<Cell> allCellsIterator() {
        return new CellIterator();
    }
    
    public boolean hasCustomHeight() {
        return this._height != -1;
    }
    
    int getOutlineLevel() {
        return this._outlineLevel;
    }
    
    void setOutlineLevel(final int level) {
        this._outlineLevel = level;
    }
    
    public Iterator<Cell> iterator() {
        return new FilledCellIterator();
    }
    
    public Cell createCell(final int column) {
        return this.createCell(column, 3);
    }
    
    public Cell createCell(final int column, final int type) {
        checkBounds(column);
        if (column >= this._cells.length) {
            final SXSSFCell[] newCells = new SXSSFCell[Math.max(column + 1, this._cells.length * 2)];
            System.arraycopy(this._cells, 0, newCells, 0, this._cells.length);
            this._cells = newCells;
        }
        this._cells[column] = new SXSSFCell(this, type);
        if (column > this._maxColumn) {
            this._maxColumn = column;
        }
        return this._cells[column];
    }
    
    private static void checkBounds(final int cellIndex) {
        final SpreadsheetVersion v = SpreadsheetVersion.EXCEL2007;
        final int maxcol = SpreadsheetVersion.EXCEL2007.getLastColumnIndex();
        if (cellIndex < 0 || cellIndex > maxcol) {
            throw new IllegalArgumentException("Invalid column index (" + cellIndex + ").  Allowable column range for " + v.name() + " is (0.." + maxcol + ") or ('A'..'" + v.getLastColumnName() + "')");
        }
    }
    
    public void removeCell(final Cell cell) {
        final int index = this.getCellIndex(cell);
        if (index >= 0) {
            this._cells[index] = null;
            while (this._maxColumn >= 0 && this._cells[this._maxColumn] == null) {
                --this._maxColumn;
            }
        }
    }
    
    int getCellIndex(final Cell cell) {
        for (int i = 0; i <= this._maxColumn; ++i) {
            if (this._cells[i] == cell) {
                return i;
            }
        }
        return -1;
    }
    
    public void setRowNum(final int rowNum) {
        this._sheet.changeRowNum(this, rowNum);
    }
    
    public int getRowNum() {
        return this._sheet.getRowNum(this);
    }
    
    public Cell getCell(final int cellnum) {
        if (cellnum < 0) {
            throw new IllegalArgumentException("Cell index must be >= 0");
        }
        final Cell cell = (cellnum > this._maxColumn) ? null : this._cells[cellnum];
        final MissingCellPolicy policy = this._sheet.getWorkbook().getMissingCellPolicy();
        if (policy == SXSSFRow.RETURN_NULL_AND_BLANK) {
            return cell;
        }
        if (policy == SXSSFRow.RETURN_BLANK_AS_NULL) {
            if (cell == null) {
                return cell;
            }
            if (cell.getCellType() == 3) {
                return null;
            }
            return cell;
        }
        else {
            if (policy != SXSSFRow.CREATE_NULL_AS_BLANK) {
                throw new IllegalArgumentException("Illegal policy " + policy + " (" + policy.id + ")");
            }
            if (cell == null) {
                return this.createCell((short)cellnum, 3);
            }
            return cell;
        }
    }
    
    public Cell getCell(final int cellnum, final MissingCellPolicy policy) {
        assert false;
        final Cell cell = this.getCell(cellnum);
        if (policy == SXSSFRow.RETURN_NULL_AND_BLANK) {
            return cell;
        }
        if (policy == SXSSFRow.RETURN_BLANK_AS_NULL) {
            if (cell == null) {
                return cell;
            }
            if (cell.getCellType() == 3) {
                return null;
            }
            return cell;
        }
        else {
            if (policy != SXSSFRow.CREATE_NULL_AS_BLANK) {
                throw new IllegalArgumentException("Illegal policy " + policy + " (" + policy.id + ")");
            }
            if (cell == null) {
                return this.createCell(cellnum, 3);
            }
            return cell;
        }
    }
    
    public short getFirstCellNum() {
        for (int i = 0; i <= this._maxColumn; ++i) {
            if (this._cells[i] != null) {
                return (short)i;
            }
        }
        return -1;
    }
    
    public short getLastCellNum() {
        return (short)((this._maxColumn == -1) ? -1 : ((short)(this._maxColumn + 1)));
    }
    
    public int getPhysicalNumberOfCells() {
        int count = 0;
        for (int i = 0; i <= this._maxColumn; ++i) {
            if (this._cells[i] != null) {
                ++count;
            }
        }
        return count;
    }
    
    public void setHeight(final short height) {
        this._height = height;
    }
    
    public void setZeroHeight(final boolean zHeight) {
        this._zHeight = zHeight;
    }
    
    public boolean getZeroHeight() {
        return this._zHeight;
    }
    
    public void setHeightInPoints(final float height) {
        if (height == -1.0f) {
            this._height = -1;
        }
        else {
            this._height = (short)(height * 20.0f);
        }
    }
    
    public short getHeight() {
        return (short)((this._height == -1) ? (this.getSheet().getDefaultRowHeightInPoints() * 20.0f) : this._height);
    }
    
    public float getHeightInPoints() {
        return (float)((this._height == -1) ? this.getSheet().getDefaultRowHeightInPoints() : (this._height / 20.0));
    }
    
    public boolean isFormatted() {
        return this._style > -1;
    }
    
    public CellStyle getRowStyle() {
        if (!this.isFormatted()) {
            return null;
        }
        return this.getSheet().getWorkbook().getCellStyleAt(this._style);
    }
    
    public void setRowStyle(final CellStyle style) {
        if (style == null) {
            this._style = -1;
            return;
        }
        this._style = style.getIndex();
    }
    
    public Iterator<Cell> cellIterator() {
        return this.iterator();
    }
    
    public Sheet getSheet() {
        return this._sheet;
    }
    
    public class FilledCellIterator implements Iterator<Cell>
    {
        int pos;
        
        FilledCellIterator() {
            this.pos = 0;
            for (int i = 0; i <= SXSSFRow.this._maxColumn; ++i) {
                if (SXSSFRow.this._cells[i] != null) {
                    this.pos = i;
                    break;
                }
            }
        }
        
        public boolean hasNext() {
            return this.pos <= SXSSFRow.this._maxColumn;
        }
        
        void advanceToNext() {
            ++this.pos;
            while (this.pos <= SXSSFRow.this._maxColumn && SXSSFRow.this._cells[this.pos] == null) {
                ++this.pos;
            }
        }
        
        public Cell next() throws NoSuchElementException {
            if (this.hasNext()) {
                final Cell retval = SXSSFRow.this._cells[this.pos];
                this.advanceToNext();
                return retval;
            }
            throw new NoSuchElementException();
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    public class CellIterator implements Iterator<Cell>
    {
        int pos;
        
        public CellIterator() {
            this.pos = 0;
        }
        
        public boolean hasNext() {
            return this.pos <= SXSSFRow.this._maxColumn;
        }
        
        public Cell next() throws NoSuchElementException {
            if (this.hasNext()) {
                return SXSSFRow.this._cells[this.pos++];
            }
            throw new NoSuchElementException();
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
