// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.charts.ValueAxis;
import org.apache.poi.ss.usermodel.charts.ManualLayout;
import org.apache.poi.ss.usermodel.charts.ChartDataFactory;
import org.apache.poi.ss.usermodel.charts.ChartLegend;
import java.util.Iterator;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTValAx;
import org.apache.poi.xssf.usermodel.charts.XSSFChartLegend;
import org.w3c.dom.NodeList;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTTitle;
import org.w3c.dom.Text;
import org.apache.poi.xssf.usermodel.charts.XSSFManualLayout;
import org.apache.poi.xssf.usermodel.charts.XSSFValueAxis;
import org.apache.poi.ss.usermodel.charts.AxisPosition;
import org.apache.poi.ss.usermodel.charts.ChartAxis;
import org.apache.poi.ss.usermodel.charts.ChartData;
import org.apache.poi.xssf.usermodel.charts.XSSFChartDataFactory;
import java.io.OutputStream;
import java.util.Map;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import java.util.HashMap;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPageMargins;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPrintSettings;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPlotArea;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.openxmlformats.schemas.drawingml.x2006.chart.ChartSpaceDocument;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.ArrayList;
import org.apache.poi.xssf.usermodel.charts.XSSFChartAxis;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTChartSpace;
import org.apache.poi.ss.usermodel.charts.ChartAxisFactory;
import org.apache.poi.ss.usermodel.Chart;
import org.apache.poi.POIXMLDocumentPart;

public final class XSSFChart extends POIXMLDocumentPart implements Chart, ChartAxisFactory
{
    private XSSFGraphicFrame frame;
    private CTChartSpace chartSpace;
    private CTChart chart;
    List<XSSFChartAxis> axis;
    
    protected XSSFChart() {
        this.axis = new ArrayList<XSSFChartAxis>();
        this.createChart();
    }
    
    protected XSSFChart(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        this.chartSpace = ChartSpaceDocument.Factory.parse(part.getInputStream()).getChartSpace();
        this.chart = this.chartSpace.getChart();
    }
    
    private void createChart() {
        this.chartSpace = CTChartSpace.Factory.newInstance();
        this.chart = this.chartSpace.addNewChart();
        final CTPlotArea plotArea = this.chart.addNewPlotArea();
        plotArea.addNewLayout();
        this.chart.addNewPlotVisOnly().setVal(true);
        final CTPrintSettings printSettings = this.chartSpace.addNewPrintSettings();
        printSettings.addNewHeaderFooter();
        final CTPageMargins pageMargins = printSettings.addNewPageMargins();
        pageMargins.setB(0.75);
        pageMargins.setL(0.7);
        pageMargins.setR(0.7);
        pageMargins.setT(0.75);
        pageMargins.setHeader(0.3);
        pageMargins.setFooter(0.3);
        printSettings.addNewPageSetup();
    }
    
    @Internal
    public CTChartSpace getCTChartSpace() {
        return this.chartSpace;
    }
    
    @Internal
    public CTChart getCTChart() {
        return this.chart;
    }
    
    @Override
    protected void commit() throws IOException {
        final XmlOptions xmlOptions = new XmlOptions(XSSFChart.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTChartSpace.type.getName().getNamespaceURI(), "chartSpace", "c"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put("http://schemas.openxmlformats.org/drawingml/2006/main", "a");
        map.put("http://schemas.openxmlformats.org/drawingml/2006/chart", "c");
        map.put(STRelationshipId.type.getName().getNamespaceURI(), "r");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.chartSpace.save(out, xmlOptions);
        out.close();
    }
    
    public XSSFGraphicFrame getGraphicFrame() {
        return this.frame;
    }
    
    protected void setGraphicFrame(final XSSFGraphicFrame frame) {
        this.frame = frame;
    }
    
    public XSSFChartDataFactory getChartDataFactory() {
        return XSSFChartDataFactory.getInstance();
    }
    
    public XSSFChart getChartAxisFactory() {
        return this;
    }
    
    public void plot(final ChartData data, final ChartAxis... axis) {
        data.fillChart(this, axis);
    }
    
    public XSSFValueAxis createValueAxis(final AxisPosition pos) {
        final long id = this.axis.size() + 1;
        final XSSFValueAxis valueAxis = new XSSFValueAxis(this, id, pos);
        if (this.axis.size() == 1) {
            final ChartAxis ax = this.axis.get(0);
            ax.crossAxis(valueAxis);
            valueAxis.crossAxis(ax);
        }
        this.axis.add(valueAxis);
        return valueAxis;
    }
    
    public List<? extends XSSFChartAxis> getAxis() {
        if (this.axis.isEmpty() && this.hasAxis()) {
            this.parseAxis();
        }
        return this.axis;
    }
    
    public XSSFManualLayout getManualLayout() {
        return new XSSFManualLayout(this);
    }
    
    public boolean isPlotOnlyVisibleCells() {
        return this.chart.getPlotVisOnly().getVal();
    }
    
    public void setPlotOnlyVisibleCells(final boolean plotVisOnly) {
        this.chart.getPlotVisOnly().setVal(plotVisOnly);
    }
    
    public XSSFRichTextString getTitle() {
        if (!this.chart.isSetTitle()) {
            return null;
        }
        final CTTitle title = this.chart.getTitle();
        final StringBuffer text = new StringBuffer();
        final XmlObject[] t = title.selectPath("declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' .//a:t");
        for (int m = 0; m < t.length; ++m) {
            final NodeList kids = t[m].getDomNode().getChildNodes();
            for (int n = 0; n < kids.getLength(); ++n) {
                if (kids.item(n) instanceof Text) {
                    text.append(kids.item(n).getNodeValue());
                }
            }
        }
        return new XSSFRichTextString(text.toString());
    }
    
    public XSSFChartLegend getOrCreateLegend() {
        return new XSSFChartLegend(this);
    }
    
    public void deleteLegend() {
        if (this.chart.isSetLegend()) {
            this.chart.unsetLegend();
        }
    }
    
    private boolean hasAxis() {
        final CTPlotArea ctPlotArea = this.chart.getPlotArea();
        final int totalAxisCount = ctPlotArea.sizeOfValAxArray() + ctPlotArea.sizeOfCatAxArray() + ctPlotArea.sizeOfDateAxArray() + ctPlotArea.sizeOfSerAxArray();
        return totalAxisCount > 0;
    }
    
    private void parseAxis() {
        this.parseValueAxis();
    }
    
    private void parseValueAxis() {
        for (final CTValAx valAx : this.chart.getPlotArea().getValAxList()) {
            this.axis.add(new XSSFValueAxis(this, valAx));
        }
    }
}
