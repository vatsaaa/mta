// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

public abstract class XSSFAnchor
{
    public abstract int getDx1();
    
    public abstract void setDx1(final int p0);
    
    public abstract int getDy1();
    
    public abstract void setDy1(final int p0);
    
    public abstract int getDy2();
    
    public abstract void setDy2(final int p0);
    
    public abstract int getDx2();
    
    public abstract void setDx2(final int p0);
}
