// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.BorderFormatting;
import org.apache.poi.ss.usermodel.FontFormatting;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STConditionalFormattingOperator;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFill;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFont;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorder;
import org.apache.poi.xssf.model.StylesTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDxf;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCfRule;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;

public class XSSFConditionalFormattingRule implements ConditionalFormattingRule
{
    private final CTCfRule _cfRule;
    private XSSFSheet _sh;
    
    XSSFConditionalFormattingRule(final XSSFSheet sh) {
        this._cfRule = CTCfRule.Factory.newInstance();
        this._sh = sh;
    }
    
    XSSFConditionalFormattingRule(final XSSFSheet sh, final CTCfRule cfRule) {
        this._cfRule = cfRule;
        this._sh = sh;
    }
    
    CTCfRule getCTCfRule() {
        return this._cfRule;
    }
    
    CTDxf getDxf(final boolean create) {
        final StylesTable styles = this._sh.getWorkbook().getStylesSource();
        CTDxf dxf = null;
        if (styles._getDXfsSize() > 0 && this._cfRule.isSetDxfId()) {
            final int dxfId = (int)this._cfRule.getDxfId();
            dxf = styles.getDxfAt(dxfId);
        }
        if (create && dxf == null) {
            dxf = CTDxf.Factory.newInstance();
            final int dxfId = styles.putDxf(dxf);
            this._cfRule.setDxfId(dxfId - 1);
        }
        return dxf;
    }
    
    public XSSFBorderFormatting createBorderFormatting() {
        final CTDxf dxf = this.getDxf(true);
        CTBorder border;
        if (!dxf.isSetBorder()) {
            border = dxf.addNewBorder();
        }
        else {
            border = dxf.getBorder();
        }
        return new XSSFBorderFormatting(border);
    }
    
    public XSSFBorderFormatting getBorderFormatting() {
        final CTDxf dxf = this.getDxf(false);
        if (dxf == null || !dxf.isSetBorder()) {
            return null;
        }
        return new XSSFBorderFormatting(dxf.getBorder());
    }
    
    public XSSFFontFormatting createFontFormatting() {
        final CTDxf dxf = this.getDxf(true);
        CTFont font;
        if (!dxf.isSetFont()) {
            font = dxf.addNewFont();
        }
        else {
            font = dxf.getFont();
        }
        return new XSSFFontFormatting(font);
    }
    
    public XSSFFontFormatting getFontFormatting() {
        final CTDxf dxf = this.getDxf(false);
        if (dxf == null || !dxf.isSetFont()) {
            return null;
        }
        return new XSSFFontFormatting(dxf.getFont());
    }
    
    public XSSFPatternFormatting createPatternFormatting() {
        final CTDxf dxf = this.getDxf(true);
        CTFill fill;
        if (!dxf.isSetFill()) {
            fill = dxf.addNewFill();
        }
        else {
            fill = dxf.getFill();
        }
        return new XSSFPatternFormatting(fill);
    }
    
    public XSSFPatternFormatting getPatternFormatting() {
        final CTDxf dxf = this.getDxf(false);
        if (dxf == null || !dxf.isSetFill()) {
            return null;
        }
        return new XSSFPatternFormatting(dxf.getFill());
    }
    
    public byte getConditionType() {
        switch (this._cfRule.getType().intValue()) {
            case 1: {
                return 2;
            }
            case 2: {
                return 1;
            }
            default: {
                return 0;
            }
        }
    }
    
    public byte getComparisonOperation() {
        final STConditionalFormattingOperator.Enum op = this._cfRule.getOperator();
        if (op == null) {
            return 0;
        }
        switch (op.intValue()) {
            case 1: {
                return 6;
            }
            case 2: {
                return 8;
            }
            case 6: {
                return 5;
            }
            case 5: {
                return 7;
            }
            case 3: {
                return 3;
            }
            case 4: {
                return 4;
            }
            case 7: {
                return 1;
            }
            case 8: {
                return 2;
            }
            default: {
                return 0;
            }
        }
    }
    
    public String getFormula1() {
        return (this._cfRule.sizeOfFormulaArray() > 0) ? this._cfRule.getFormulaArray(0) : null;
    }
    
    public String getFormula2() {
        return (this._cfRule.sizeOfFormulaArray() == 2) ? this._cfRule.getFormulaArray(1) : null;
    }
}
