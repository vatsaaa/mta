// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.xssf.model.CalculationChain;
import org.apache.poi.xssf.model.ThemesTable;
import org.apache.poi.xssf.model.CommentsTable;
import org.apache.poi.xssf.model.SingleXmlCells;
import org.apache.poi.xssf.model.MapInfo;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.model.SharedStringsTable;
import java.util.HashMap;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackagePartName;
import java.util.Iterator;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import java.io.InputStream;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.POIXMLDocumentPart;
import java.util.Map;
import org.apache.poi.util.POILogger;
import org.apache.poi.POIXMLRelation;

public final class XSSFRelation extends POIXMLRelation
{
    private static POILogger log;
    protected static Map<String, XSSFRelation> _table;
    public static final XSSFRelation WORKBOOK;
    public static final XSSFRelation MACROS_WORKBOOK;
    public static final XSSFRelation TEMPLATE_WORKBOOK;
    public static final XSSFRelation MACRO_TEMPLATE_WORKBOOK;
    public static final XSSFRelation MACRO_ADDIN_WORKBOOK;
    public static final XSSFRelation WORKSHEET;
    public static final XSSFRelation CHARTSHEET;
    public static final XSSFRelation SHARED_STRINGS;
    public static final XSSFRelation STYLES;
    public static final XSSFRelation DRAWINGS;
    public static final XSSFRelation VML_DRAWINGS;
    public static final XSSFRelation CHART;
    public static final XSSFRelation CUSTOM_XML_MAPPINGS;
    public static final XSSFRelation SINGLE_XML_CELLS;
    public static final XSSFRelation TABLE;
    public static final XSSFRelation IMAGES;
    public static final XSSFRelation IMAGE_EMF;
    public static final XSSFRelation IMAGE_WMF;
    public static final XSSFRelation IMAGE_PICT;
    public static final XSSFRelation IMAGE_JPEG;
    public static final XSSFRelation IMAGE_PNG;
    public static final XSSFRelation IMAGE_DIB;
    public static final XSSFRelation IMAGE_GIF;
    public static final XSSFRelation IMAGE_TIFF;
    public static final XSSFRelation IMAGE_EPS;
    public static final XSSFRelation IMAGE_BMP;
    public static final XSSFRelation IMAGE_WPG;
    public static final XSSFRelation SHEET_COMMENTS;
    public static final XSSFRelation SHEET_HYPERLINKS;
    public static final XSSFRelation OLEEMBEDDINGS;
    public static final XSSFRelation PACKEMBEDDINGS;
    public static final XSSFRelation VBA_MACROS;
    public static final XSSFRelation ACTIVEX_CONTROLS;
    public static final XSSFRelation ACTIVEX_BINS;
    public static final XSSFRelation THEME;
    public static final XSSFRelation CALC_CHAIN;
    public static final XSSFRelation PRINTER_SETTINGS;
    
    private XSSFRelation(final String type, final String rel, final String defaultName, final Class<? extends POIXMLDocumentPart> cls) {
        super(type, rel, defaultName, cls);
        if (cls != null && !XSSFRelation._table.containsKey(rel)) {
            XSSFRelation._table.put(rel, this);
        }
    }
    
    public InputStream getContents(final PackagePart corePart) throws IOException, InvalidFormatException {
        final PackageRelationshipCollection prc = corePart.getRelationshipsByType(this._relation);
        final Iterator<PackageRelationship> it = prc.iterator();
        if (it.hasNext()) {
            final PackageRelationship rel = it.next();
            final PackagePartName relName = PackagingURIHelper.createPartName(rel.getTargetURI());
            final PackagePart part = corePart.getPackage().getPart(relName);
            return part.getInputStream();
        }
        XSSFRelation.log.log(POILogger.WARN, "No part " + this._defaultName + " found");
        return null;
    }
    
    public static XSSFRelation getInstance(final String rel) {
        return XSSFRelation._table.get(rel);
    }
    
    static {
        XSSFRelation.log = POILogFactory.getLogger(XSSFRelation.class);
        XSSFRelation._table = new HashMap<String, XSSFRelation>();
        WORKBOOK = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/workbook", "/xl/workbook.xml", null);
        MACROS_WORKBOOK = new XSSFRelation("application/vnd.ms-excel.sheet.macroEnabled.main+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument", "/xl/workbook.xml", null);
        TEMPLATE_WORKBOOK = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.template.main+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument", "/xl/workbook.xml", null);
        MACRO_TEMPLATE_WORKBOOK = new XSSFRelation("application/vnd.ms-excel.template.macroEnabled.main+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument", "/xl/workbook.xml", null);
        MACRO_ADDIN_WORKBOOK = new XSSFRelation("application/vnd.ms-excel.addin.macroEnabled.main+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument", "/xl/workbook.xml", null);
        WORKSHEET = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet", "/xl/worksheets/sheet#.xml", XSSFSheet.class);
        CHARTSHEET = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.chartsheet+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chartsheet", "/xl/chartsheets/sheet#.xml", XSSFChartSheet.class);
        SHARED_STRINGS = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings", "/xl/sharedStrings.xml", SharedStringsTable.class);
        STYLES = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles", "/xl/styles.xml", StylesTable.class);
        DRAWINGS = new XSSFRelation("application/vnd.openxmlformats-officedocument.drawing+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing", "/xl/drawings/drawing#.xml", XSSFDrawing.class);
        VML_DRAWINGS = new XSSFRelation("application/vnd.openxmlformats-officedocument.vmlDrawing", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/vmlDrawing", "/xl/drawings/vmlDrawing#.vml", XSSFVMLDrawing.class);
        CHART = new XSSFRelation("application/vnd.openxmlformats-officedocument.drawingml.chart+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart", "/xl/charts/chart#.xml", XSSFChart.class);
        CUSTOM_XML_MAPPINGS = new XSSFRelation("application/xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/xmlMaps", "/xl/xmlMaps.xml", MapInfo.class);
        SINGLE_XML_CELLS = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.tableSingleCells+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/tableSingleCells", "/xl/tables/tableSingleCells#.xml", SingleXmlCells.class);
        TABLE = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.table+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/table", "/xl/tables/table#.xml", XSSFTable.class);
        IMAGES = new XSSFRelation(null, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", null, XSSFPictureData.class);
        IMAGE_EMF = new XSSFRelation("image/x-emf", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.emf", XSSFPictureData.class);
        IMAGE_WMF = new XSSFRelation("image/x-wmf", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.wmf", XSSFPictureData.class);
        IMAGE_PICT = new XSSFRelation("image/pict", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.pict", XSSFPictureData.class);
        IMAGE_JPEG = new XSSFRelation("image/jpeg", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.jpeg", XSSFPictureData.class);
        IMAGE_PNG = new XSSFRelation("image/png", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.png", XSSFPictureData.class);
        IMAGE_DIB = new XSSFRelation("image/dib", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.dib", XSSFPictureData.class);
        IMAGE_GIF = new XSSFRelation("image/gif", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.gif", XSSFPictureData.class);
        IMAGE_TIFF = new XSSFRelation("image/tiff", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.tiff", XSSFPictureData.class);
        IMAGE_EPS = new XSSFRelation("image/x-eps", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.eps", XSSFPictureData.class);
        IMAGE_BMP = new XSSFRelation("image/x-ms-bmp", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.bmp", XSSFPictureData.class);
        IMAGE_WPG = new XSSFRelation("image/x-wpg", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/xl/media/image#.wpg", XSSFPictureData.class);
        SHEET_COMMENTS = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.comments+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/comments", "/xl/comments#.xml", CommentsTable.class);
        SHEET_HYPERLINKS = new XSSFRelation(null, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink", null, null);
        OLEEMBEDDINGS = new XSSFRelation(null, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject", null, null);
        PACKEMBEDDINGS = new XSSFRelation(null, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package", null, null);
        VBA_MACROS = new XSSFRelation("application/vnd.ms-office.vbaProject", "http://schemas.microsoft.com/office/2006/relationships/vbaProject", "/xl/vbaProject.bin", null);
        ACTIVEX_CONTROLS = new XSSFRelation("application/vnd.ms-office.activeX+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/control", "/xl/activeX/activeX#.xml", null);
        ACTIVEX_BINS = new XSSFRelation("application/vnd.ms-office.activeX", "http://schemas.microsoft.com/office/2006/relationships/activeXControlBinary", "/xl/activeX/activeX#.bin", null);
        THEME = new XSSFRelation("application/vnd.openxmlformats-officedocument.theme+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme", "/xl/theme/theme#.xml", ThemesTable.class);
        CALC_CHAIN = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.calcChain+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/calcChain", "/xl/calcChain.xml", CalculationChain.class);
        PRINTER_SETTINGS = new XSSFRelation("application/vnd.openxmlformats-officedocument.spreadsheetml.printerSettings", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/printerSettings", "/xl/printerSettings/printerSettings#.bin", null);
    }
}
