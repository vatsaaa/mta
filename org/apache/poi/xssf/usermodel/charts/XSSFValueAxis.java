// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel.charts;

import org.openxmlformats.schemas.drawingml.x2006.chart.STCrossBetween;
import org.apache.poi.ss.usermodel.charts.AxisCrosses;
import org.apache.poi.ss.usermodel.charts.AxisOrientation;
import org.openxmlformats.schemas.drawingml.x2006.chart.STTickLblPos;
import org.apache.poi.ss.usermodel.charts.ChartAxis;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTCrosses;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTScaling;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumFmt;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTAxPos;
import org.apache.poi.ss.usermodel.charts.AxisCrossBetween;
import org.apache.poi.ss.usermodel.charts.AxisPosition;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTValAx;
import org.apache.poi.ss.usermodel.charts.ValueAxis;

public class XSSFValueAxis extends XSSFChartAxis implements ValueAxis
{
    private CTValAx ctValAx;
    
    public XSSFValueAxis(final XSSFChart chart, final long id, final AxisPosition pos) {
        super(chart);
        this.createAxis(id, pos);
    }
    
    public XSSFValueAxis(final XSSFChart chart, final CTValAx ctValAx) {
        super(chart);
        this.ctValAx = ctValAx;
    }
    
    public long getId() {
        return this.ctValAx.getAxId().getVal();
    }
    
    public void setCrossBetween(final AxisCrossBetween crossBetween) {
        this.ctValAx.getCrossBetween().setVal(fromCrossBetween(crossBetween));
    }
    
    public AxisCrossBetween getCrossBetween() {
        return toCrossBetween(this.ctValAx.getCrossBetween().getVal());
    }
    
    @Override
    protected CTAxPos getCTAxPos() {
        return this.ctValAx.getAxPos();
    }
    
    @Override
    protected CTNumFmt getCTNumFmt() {
        if (this.ctValAx.isSetNumFmt()) {
            return this.ctValAx.getNumFmt();
        }
        return this.ctValAx.addNewNumFmt();
    }
    
    @Override
    protected CTScaling getCTScaling() {
        return this.ctValAx.getScaling();
    }
    
    @Override
    protected CTCrosses getCTCrosses() {
        return this.ctValAx.getCrosses();
    }
    
    public void crossAxis(final ChartAxis axis) {
        this.ctValAx.getCrossAx().setVal(axis.getId());
    }
    
    private void createAxis(final long id, final AxisPosition pos) {
        this.ctValAx = this.chart.getCTChart().getPlotArea().addNewValAx();
        this.ctValAx.addNewAxId().setVal(id);
        this.ctValAx.addNewAxPos();
        this.ctValAx.addNewScaling();
        this.ctValAx.addNewCrossBetween();
        this.ctValAx.addNewCrosses();
        this.ctValAx.addNewCrossAx();
        this.ctValAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);
        this.setPosition(pos);
        this.setOrientation(AxisOrientation.MIN_MAX);
        this.setCrossBetween(AxisCrossBetween.MIDPOINT_CATEGORY);
        this.setCrosses(AxisCrosses.AUTO_ZERO);
    }
    
    private static STCrossBetween.Enum fromCrossBetween(final AxisCrossBetween crossBetween) {
        switch (crossBetween) {
            case BETWEEN: {
                return STCrossBetween.BETWEEN;
            }
            case MIDPOINT_CATEGORY: {
                return STCrossBetween.MID_CAT;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    private static AxisCrossBetween toCrossBetween(final STCrossBetween.Enum ctCrossBetween) {
        switch (ctCrossBetween.intValue()) {
            case 1: {
                return AxisCrossBetween.BETWEEN;
            }
            case 2: {
                return AxisCrossBetween.MIDPOINT_CATEGORY;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
}
