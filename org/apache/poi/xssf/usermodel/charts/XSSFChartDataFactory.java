// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel.charts;

import org.apache.poi.ss.usermodel.charts.ScatterChartData;
import org.apache.poi.ss.usermodel.charts.ChartDataFactory;

public class XSSFChartDataFactory implements ChartDataFactory
{
    private static XSSFChartDataFactory instance;
    
    private XSSFChartDataFactory() {
    }
    
    public XSSFScatterChartData createScatterChartData() {
        return new XSSFScatterChartData();
    }
    
    public static XSSFChartDataFactory getInstance() {
        if (XSSFChartDataFactory.instance == null) {
            XSSFChartDataFactory.instance = new XSSFChartDataFactory();
        }
        return XSSFChartDataFactory.instance;
    }
}
