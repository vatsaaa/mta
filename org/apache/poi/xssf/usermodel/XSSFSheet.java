// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.AutoFilter;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import java.util.Collection;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTablePart;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableParts;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTAutoFilter;
import org.apache.poi.ss.usermodel.DataValidation;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDataValidation;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDataValidations;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.util.SSCellRange;
import org.apache.poi.ss.usermodel.CellRange;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import java.io.OutputStream;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCell;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCellFormulaType;
import org.apache.poi.openxml4j.exceptions.PartAlreadyExistsException;
import java.util.Arrays;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetViews;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCommentList;
import org.apache.poi.ss.formula.FormulaShifter;
import org.apache.poi.xssf.usermodel.helpers.XSSFRowShifter;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetCalcPr;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPageBreak;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCols;
import org.apache.poi.util.HexDump;
import org.apache.poi.hssf.record.PasswordRecord;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STUnsignedShortHex;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetProtection;
import org.apache.poi.hssf.util.PaneInformation;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPrintOptions;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.Footer;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTHeaderFooter;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPageSetUpPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTOutlinePr;
import org.apache.poi.ss.usermodel.CellStyle;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCol;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBreak;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTComment;
import org.apache.xmlbeans.XmlObject;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPane;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPane;
import org.apache.poi.ss.util.CellReference;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPaneState;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSelection;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTLegacyDrawing;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDrawing;
import org.apache.poi.POIXMLFactory;
import org.apache.poi.POIXMLRelation;
import org.apache.poi.ss.util.SheetUtil;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTMergeCell;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTMergeCells;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPageMargins;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetView;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetFormatPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTHyperlink;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRow;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.WorksheetDocument;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.POIXMLException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.ss.util.CellRangeAddress;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellFormula;
import java.util.Map;
import org.apache.poi.xssf.model.CommentsTable;
import org.apache.poi.xssf.usermodel.helpers.ColumnHelper;
import java.util.List;
import java.util.TreeMap;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheet;
import org.apache.poi.util.POILogger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.POIXMLDocumentPart;

public class XSSFSheet extends POIXMLDocumentPart implements Sheet
{
    private static final POILogger logger;
    protected CTSheet sheet;
    protected CTWorksheet worksheet;
    private TreeMap<Integer, XSSFRow> _rows;
    private List<XSSFHyperlink> hyperlinks;
    private ColumnHelper columnHelper;
    private CommentsTable sheetComments;
    private Map<Integer, CTCellFormula> sharedFormulas;
    private TreeMap<String, XSSFTable> tables;
    private List<CellRangeAddress> arrayFormulas;
    private XSSFDataValidationHelper dataValidationHelper;
    
    protected XSSFSheet() {
        this.dataValidationHelper = new XSSFDataValidationHelper(this);
        this.onDocumentCreate();
    }
    
    protected XSSFSheet(final PackagePart part, final PackageRelationship rel) {
        super(part, rel);
        this.dataValidationHelper = new XSSFDataValidationHelper(this);
    }
    
    public XSSFWorkbook getWorkbook() {
        return (XSSFWorkbook)this.getParent();
    }
    
    @Override
    protected void onDocumentRead() {
        try {
            this.read(this.getPackagePart().getInputStream());
        }
        catch (IOException e) {
            throw new POIXMLException(e);
        }
    }
    
    protected void read(final InputStream is) throws IOException {
        try {
            this.worksheet = WorksheetDocument.Factory.parse(is).getWorksheet();
        }
        catch (XmlException e) {
            throw new POIXMLException(e);
        }
        this.initRows(this.worksheet);
        this.columnHelper = new ColumnHelper(this.worksheet);
        for (final POIXMLDocumentPart p : this.getRelations()) {
            if (p instanceof CommentsTable) {
                this.sheetComments = (CommentsTable)p;
                break;
            }
            if (!(p instanceof XSSFTable)) {
                continue;
            }
            this.tables.put(p.getPackageRelationship().getId(), (XSSFTable)p);
        }
        this.initHyperlinks();
    }
    
    @Override
    protected void onDocumentCreate() {
        this.initRows(this.worksheet = newSheet());
        this.columnHelper = new ColumnHelper(this.worksheet);
        this.hyperlinks = new ArrayList<XSSFHyperlink>();
    }
    
    private void initRows(final CTWorksheet worksheet) {
        this._rows = new TreeMap<Integer, XSSFRow>();
        this.tables = new TreeMap<String, XSSFTable>();
        this.sharedFormulas = new HashMap<Integer, CTCellFormula>();
        this.arrayFormulas = new ArrayList<CellRangeAddress>();
        for (final CTRow row : worksheet.getSheetData().getRowArray()) {
            final XSSFRow r = new XSSFRow(row, this);
            this._rows.put(r.getRowNum(), r);
        }
    }
    
    private void initHyperlinks() {
        this.hyperlinks = new ArrayList<XSSFHyperlink>();
        if (!this.worksheet.isSetHyperlinks()) {
            return;
        }
        try {
            final PackageRelationshipCollection hyperRels = this.getPackagePart().getRelationshipsByType(XSSFRelation.SHEET_HYPERLINKS.getRelation());
            for (final CTHyperlink hyperlink : this.worksheet.getHyperlinks().getHyperlinkArray()) {
                PackageRelationship hyperRel = null;
                if (hyperlink.getId() != null) {
                    hyperRel = hyperRels.getRelationshipByID(hyperlink.getId());
                }
                this.hyperlinks.add(new XSSFHyperlink(hyperlink, hyperRel));
            }
        }
        catch (InvalidFormatException e) {
            throw new POIXMLException(e);
        }
    }
    
    private static CTWorksheet newSheet() {
        final CTWorksheet worksheet = CTWorksheet.Factory.newInstance();
        final CTSheetFormatPr ctFormat = worksheet.addNewSheetFormatPr();
        ctFormat.setDefaultRowHeight(15.0);
        final CTSheetView ctView = worksheet.addNewSheetViews().addNewSheetView();
        ctView.setWorkbookViewId(0L);
        worksheet.addNewDimension().setRef("A1");
        worksheet.addNewSheetData();
        final CTPageMargins ctMargins = worksheet.addNewPageMargins();
        ctMargins.setBottom(0.75);
        ctMargins.setFooter(0.3);
        ctMargins.setHeader(0.3);
        ctMargins.setLeft(0.7);
        ctMargins.setRight(0.7);
        ctMargins.setTop(0.75);
        return worksheet;
    }
    
    @Internal
    public CTWorksheet getCTWorksheet() {
        return this.worksheet;
    }
    
    public ColumnHelper getColumnHelper() {
        return this.columnHelper;
    }
    
    public String getSheetName() {
        return this.sheet.getName();
    }
    
    public int addMergedRegion(final CellRangeAddress region) {
        region.validate(SpreadsheetVersion.EXCEL2007);
        this.validateArrayFormulas(region);
        final CTMergeCells ctMergeCells = this.worksheet.isSetMergeCells() ? this.worksheet.getMergeCells() : this.worksheet.addNewMergeCells();
        final CTMergeCell ctMergeCell = ctMergeCells.addNewMergeCell();
        ctMergeCell.setRef(region.formatAsString());
        return ctMergeCells.sizeOfMergeCellArray();
    }
    
    private void validateArrayFormulas(final CellRangeAddress region) {
        final int firstRow = region.getFirstRow();
        final int firstColumn = region.getFirstColumn();
        final int lastRow = region.getLastRow();
        final int lastColumn = region.getLastColumn();
        for (int rowIn = firstRow; rowIn <= lastRow; ++rowIn) {
            for (int colIn = firstColumn; colIn <= lastColumn; ++colIn) {
                final XSSFRow row = this.getRow(rowIn);
                if (row != null) {
                    final XSSFCell cell = row.getCell(colIn);
                    if (cell != null) {
                        if (cell.isPartOfArrayFormulaGroup()) {
                            final CellRangeAddress arrayRange = cell.getArrayFormulaRange();
                            if (arrayRange.getNumberOfCells() > 1 && (arrayRange.isInRange(region.getFirstRow(), region.getFirstColumn()) || arrayRange.isInRange(region.getFirstRow(), region.getFirstColumn()))) {
                                final String msg = "The range " + region.formatAsString() + " intersects with a multi-cell array formula. " + "You cannot merge cells of an array.";
                                throw new IllegalStateException(msg);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void autoSizeColumn(final int column) {
        this.autoSizeColumn(column, false);
    }
    
    public void autoSizeColumn(final int column, final boolean useMergedCells) {
        double width = SheetUtil.getColumnWidth(this, column, useMergedCells);
        if (width != -1.0) {
            width *= 256.0;
            final int maxColumnWidth = 65280;
            if (width > maxColumnWidth) {
                width = maxColumnWidth;
            }
            this.setColumnWidth(column, (int)width);
            this.columnHelper.setColBestFit(column, true);
        }
    }
    
    public XSSFDrawing createDrawingPatriarch() {
        XSSFDrawing drawing = null;
        CTDrawing ctDrawing = this.getCTDrawing();
        if (ctDrawing == null) {
            final int drawingNumber = this.getPackagePart().getPackage().getPartsByContentType(XSSFRelation.DRAWINGS.getContentType()).size() + 1;
            drawing = (XSSFDrawing)this.createRelationship(XSSFRelation.DRAWINGS, XSSFFactory.getInstance(), drawingNumber);
            final String relId = drawing.getPackageRelationship().getId();
            ctDrawing = this.worksheet.addNewDrawing();
            ctDrawing.setId(relId);
        }
        else {
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSSFDrawing) {
                    final XSSFDrawing dr = (XSSFDrawing)p;
                    final String drId = dr.getPackageRelationship().getId();
                    if (drId.equals(ctDrawing.getId())) {
                        drawing = dr;
                        break;
                    }
                    break;
                }
            }
            if (drawing == null) {
                XSSFSheet.logger.log(POILogger.ERROR, "Can't find drawing with id=" + ctDrawing.getId() + " in the list of the sheet's relationships");
            }
        }
        return drawing;
    }
    
    protected XSSFVMLDrawing getVMLDrawing(final boolean autoCreate) {
        XSSFVMLDrawing drawing = null;
        CTLegacyDrawing ctDrawing = this.getCTLegacyDrawing();
        if (ctDrawing == null) {
            if (autoCreate) {
                final int drawingNumber = this.getPackagePart().getPackage().getPartsByContentType(XSSFRelation.VML_DRAWINGS.getContentType()).size() + 1;
                drawing = (XSSFVMLDrawing)this.createRelationship(XSSFRelation.VML_DRAWINGS, XSSFFactory.getInstance(), drawingNumber);
                final String relId = drawing.getPackageRelationship().getId();
                ctDrawing = this.worksheet.addNewLegacyDrawing();
                ctDrawing.setId(relId);
            }
        }
        else {
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSSFVMLDrawing) {
                    final XSSFVMLDrawing dr = (XSSFVMLDrawing)p;
                    final String drId = dr.getPackageRelationship().getId();
                    if (drId.equals(ctDrawing.getId())) {
                        drawing = dr;
                        break;
                    }
                    break;
                }
            }
            if (drawing == null) {
                XSSFSheet.logger.log(POILogger.ERROR, "Can't find VML drawing with id=" + ctDrawing.getId() + " in the list of the sheet's relationships");
            }
        }
        return drawing;
    }
    
    protected CTDrawing getCTDrawing() {
        return this.worksheet.getDrawing();
    }
    
    protected CTLegacyDrawing getCTLegacyDrawing() {
        return this.worksheet.getLegacyDrawing();
    }
    
    public void createFreezePane(final int colSplit, final int rowSplit) {
        this.createFreezePane(colSplit, rowSplit, colSplit, rowSplit);
    }
    
    public void createFreezePane(final int colSplit, final int rowSplit, final int leftmostColumn, final int topRow) {
        final CTSheetView ctView = this.getDefaultSheetView();
        if (colSplit == 0 && rowSplit == 0) {
            if (ctView.isSetPane()) {
                ctView.unsetPane();
            }
            ctView.setSelectionArray(null);
            return;
        }
        if (!ctView.isSetPane()) {
            ctView.addNewPane();
        }
        final CTPane pane = ctView.getPane();
        if (colSplit > 0) {
            pane.setXSplit(colSplit);
        }
        else if (pane.isSetXSplit()) {
            pane.unsetXSplit();
        }
        if (rowSplit > 0) {
            pane.setYSplit(rowSplit);
        }
        else if (pane.isSetYSplit()) {
            pane.unsetYSplit();
        }
        pane.setState(STPaneState.FROZEN);
        if (rowSplit == 0) {
            pane.setTopLeftCell(new CellReference(0, leftmostColumn).formatAsString());
            pane.setActivePane(STPane.TOP_RIGHT);
        }
        else if (colSplit == 0) {
            pane.setTopLeftCell(new CellReference(topRow, 0).formatAsString());
            pane.setActivePane(STPane.BOTTOM_LEFT);
        }
        else {
            pane.setTopLeftCell(new CellReference(topRow, leftmostColumn).formatAsString());
            pane.setActivePane(STPane.BOTTOM_RIGHT);
        }
        ctView.setSelectionArray(null);
        final CTSelection sel = ctView.addNewSelection();
        sel.setPane(pane.getActivePane());
    }
    
    @Deprecated
    public XSSFComment createComment() {
        return this.createDrawingPatriarch().createCellComment(new XSSFClientAnchor());
    }
    
    public XSSFRow createRow(final int rownum) {
        final XSSFRow prev = this._rows.get(rownum);
        CTRow ctRow;
        if (prev != null) {
            ctRow = prev.getCTRow();
            ctRow.set(CTRow.Factory.newInstance());
        }
        else if (this._rows.isEmpty() || rownum > this._rows.lastKey()) {
            ctRow = this.worksheet.getSheetData().addNewRow();
        }
        else {
            final int idx = this._rows.headMap(rownum).size();
            ctRow = this.worksheet.getSheetData().insertNewRow(idx);
        }
        final XSSFRow r = new XSSFRow(ctRow, this);
        r.setRowNum(rownum);
        this._rows.put(rownum, r);
        return r;
    }
    
    public void createSplitPane(final int xSplitPos, final int ySplitPos, final int leftmostColumn, final int topRow, final int activePane) {
        this.createFreezePane(xSplitPos, ySplitPos, leftmostColumn, topRow);
        this.getPane().setState(STPaneState.SPLIT);
        this.getPane().setActivePane(STPane.Enum.forInt(activePane));
    }
    
    public XSSFComment getCellComment(final int row, final int column) {
        if (this.sheetComments == null) {
            return null;
        }
        final String ref = new CellReference(row, column).formatAsString();
        final CTComment ctComment = this.sheetComments.getCTComment(ref);
        if (ctComment == null) {
            return null;
        }
        final XSSFVMLDrawing vml = this.getVMLDrawing(false);
        return new XSSFComment(this.sheetComments, ctComment, (vml == null) ? null : vml.findCommentShape(row, column));
    }
    
    public XSSFHyperlink getHyperlink(final int row, final int column) {
        final String ref = new CellReference(row, column).formatAsString();
        for (final XSSFHyperlink hyperlink : this.hyperlinks) {
            if (hyperlink.getCellRef().equals(ref)) {
                return hyperlink;
            }
        }
        return null;
    }
    
    public int[] getColumnBreaks() {
        if (!this.worksheet.isSetColBreaks() || this.worksheet.getColBreaks().sizeOfBrkArray() == 0) {
            return new int[0];
        }
        final CTBreak[] brkArray = this.worksheet.getColBreaks().getBrkArray();
        final int[] breaks = new int[brkArray.length];
        for (int i = 0; i < brkArray.length; ++i) {
            final CTBreak brk = brkArray[i];
            breaks[i] = (int)brk.getId() - 1;
        }
        return breaks;
    }
    
    public int getColumnWidth(final int columnIndex) {
        final CTCol col = this.columnHelper.getColumn(columnIndex, false);
        final double width = (col == null || !col.isSetWidth()) ? this.getDefaultColumnWidth() : col.getWidth();
        return (int)(width * 256.0);
    }
    
    public int getDefaultColumnWidth() {
        final CTSheetFormatPr pr = this.worksheet.getSheetFormatPr();
        return (pr == null) ? 8 : ((int)pr.getBaseColWidth());
    }
    
    public short getDefaultRowHeight() {
        return (short)(this.getDefaultRowHeightInPoints() * 20.0f);
    }
    
    public float getDefaultRowHeightInPoints() {
        final CTSheetFormatPr pr = this.worksheet.getSheetFormatPr();
        return (float)((pr == null) ? 0.0 : pr.getDefaultRowHeight());
    }
    
    private CTSheetFormatPr getSheetTypeSheetFormatPr() {
        return this.worksheet.isSetSheetFormatPr() ? this.worksheet.getSheetFormatPr() : this.worksheet.addNewSheetFormatPr();
    }
    
    public CellStyle getColumnStyle(final int column) {
        final int idx = this.columnHelper.getColDefaultStyle(column);
        return this.getWorkbook().getCellStyleAt((short)((idx == -1) ? 0 : idx));
    }
    
    public void setRightToLeft(final boolean value) {
        final CTSheetView view = this.getDefaultSheetView();
        view.setRightToLeft(value);
    }
    
    public boolean isRightToLeft() {
        final CTSheetView view = this.getDefaultSheetView();
        return view != null && view.getRightToLeft();
    }
    
    public boolean getDisplayGuts() {
        final CTSheetPr sheetPr = this.getSheetTypeSheetPr();
        final CTOutlinePr outlinePr = (sheetPr.getOutlinePr() == null) ? CTOutlinePr.Factory.newInstance() : sheetPr.getOutlinePr();
        return outlinePr.getShowOutlineSymbols();
    }
    
    public void setDisplayGuts(final boolean value) {
        final CTSheetPr sheetPr = this.getSheetTypeSheetPr();
        final CTOutlinePr outlinePr = (sheetPr.getOutlinePr() == null) ? sheetPr.addNewOutlinePr() : sheetPr.getOutlinePr();
        outlinePr.setShowOutlineSymbols(value);
    }
    
    public boolean isDisplayZeros() {
        final CTSheetView view = this.getDefaultSheetView();
        return view == null || view.getShowZeros();
    }
    
    public void setDisplayZeros(final boolean value) {
        final CTSheetView view = this.getSheetTypeSheetView();
        view.setShowZeros(value);
    }
    
    public int getFirstRowNum() {
        return (this._rows.size() == 0) ? 0 : this._rows.firstKey();
    }
    
    public boolean getFitToPage() {
        final CTSheetPr sheetPr = this.getSheetTypeSheetPr();
        final CTPageSetUpPr psSetup = (sheetPr == null || !sheetPr.isSetPageSetUpPr()) ? CTPageSetUpPr.Factory.newInstance() : sheetPr.getPageSetUpPr();
        return psSetup.getFitToPage();
    }
    
    private CTSheetPr getSheetTypeSheetPr() {
        if (this.worksheet.getSheetPr() == null) {
            this.worksheet.setSheetPr(CTSheetPr.Factory.newInstance());
        }
        return this.worksheet.getSheetPr();
    }
    
    private CTHeaderFooter getSheetTypeHeaderFooter() {
        if (this.worksheet.getHeaderFooter() == null) {
            this.worksheet.setHeaderFooter(CTHeaderFooter.Factory.newInstance());
        }
        return this.worksheet.getHeaderFooter();
    }
    
    public Footer getFooter() {
        return this.getOddFooter();
    }
    
    public Header getHeader() {
        return this.getOddHeader();
    }
    
    public Footer getOddFooter() {
        return new XSSFOddFooter(this.getSheetTypeHeaderFooter());
    }
    
    public Footer getEvenFooter() {
        return new XSSFEvenFooter(this.getSheetTypeHeaderFooter());
    }
    
    public Footer getFirstFooter() {
        return new XSSFFirstFooter(this.getSheetTypeHeaderFooter());
    }
    
    public Header getOddHeader() {
        return new XSSFOddHeader(this.getSheetTypeHeaderFooter());
    }
    
    public Header getEvenHeader() {
        return new XSSFEvenHeader(this.getSheetTypeHeaderFooter());
    }
    
    public Header getFirstHeader() {
        return new XSSFFirstHeader(this.getSheetTypeHeaderFooter());
    }
    
    public boolean getHorizontallyCenter() {
        final CTPrintOptions opts = this.worksheet.getPrintOptions();
        return opts != null && opts.getHorizontalCentered();
    }
    
    public int getLastRowNum() {
        return (this._rows.size() == 0) ? 0 : this._rows.lastKey();
    }
    
    public short getLeftCol() {
        final String cellRef = this.worksheet.getSheetViews().getSheetViewArray(0).getTopLeftCell();
        final CellReference cellReference = new CellReference(cellRef);
        return cellReference.getCol();
    }
    
    public double getMargin(final short margin) {
        if (!this.worksheet.isSetPageMargins()) {
            return 0.0;
        }
        final CTPageMargins pageMargins = this.worksheet.getPageMargins();
        switch (margin) {
            case 0: {
                return pageMargins.getLeft();
            }
            case 1: {
                return pageMargins.getRight();
            }
            case 2: {
                return pageMargins.getTop();
            }
            case 3: {
                return pageMargins.getBottom();
            }
            case 4: {
                return pageMargins.getHeader();
            }
            case 5: {
                return pageMargins.getFooter();
            }
            default: {
                throw new IllegalArgumentException("Unknown margin constant:  " + margin);
            }
        }
    }
    
    public void setMargin(final short margin, final double size) {
        final CTPageMargins pageMargins = this.worksheet.isSetPageMargins() ? this.worksheet.getPageMargins() : this.worksheet.addNewPageMargins();
        switch (margin) {
            case 0: {
                pageMargins.setLeft(size);
                break;
            }
            case 1: {
                pageMargins.setRight(size);
                break;
            }
            case 2: {
                pageMargins.setTop(size);
                break;
            }
            case 3: {
                pageMargins.setBottom(size);
                break;
            }
            case 4: {
                pageMargins.setHeader(size);
                break;
            }
            case 5: {
                pageMargins.setFooter(size);
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown margin constant:  " + margin);
            }
        }
    }
    
    public CellRangeAddress getMergedRegion(final int index) {
        final CTMergeCells ctMergeCells = this.worksheet.getMergeCells();
        if (ctMergeCells == null) {
            throw new IllegalStateException("This worksheet does not contain merged regions");
        }
        final CTMergeCell ctMergeCell = ctMergeCells.getMergeCellArray(index);
        final String ref = ctMergeCell.getRef();
        return CellRangeAddress.valueOf(ref);
    }
    
    public int getNumMergedRegions() {
        final CTMergeCells ctMergeCells = this.worksheet.getMergeCells();
        return (ctMergeCells == null) ? 0 : ctMergeCells.sizeOfMergeCellArray();
    }
    
    public int getNumHyperlinks() {
        return this.hyperlinks.size();
    }
    
    public PaneInformation getPaneInformation() {
        final CTPane pane = this.getDefaultSheetView().getPane();
        if (pane == null) {
            return null;
        }
        final CellReference cellRef = pane.isSetTopLeftCell() ? new CellReference(pane.getTopLeftCell()) : null;
        return new PaneInformation((short)pane.getXSplit(), (short)pane.getYSplit(), (short)((cellRef == null) ? 0 : cellRef.getRow()), (short)((cellRef == null) ? 0 : cellRef.getCol()), (byte)(pane.getActivePane().intValue() - 1), pane.getState() == STPaneState.FROZEN);
    }
    
    public int getPhysicalNumberOfRows() {
        return this._rows.size();
    }
    
    public XSSFPrintSetup getPrintSetup() {
        return new XSSFPrintSetup(this.worksheet);
    }
    
    public boolean getProtect() {
        return this.worksheet.isSetSheetProtection() && this.sheetProtectionEnabled();
    }
    
    public void protectSheet(final String password) {
        if (password != null) {
            final CTSheetProtection sheetProtection = this.worksheet.addNewSheetProtection();
            sheetProtection.xsetPassword(this.stringToExcelPassword(password));
            sheetProtection.setSheet(true);
            sheetProtection.setScenarios(true);
            sheetProtection.setObjects(true);
        }
        else {
            this.worksheet.unsetSheetProtection();
        }
    }
    
    private STUnsignedShortHex stringToExcelPassword(final String password) {
        final STUnsignedShortHex hexPassword = STUnsignedShortHex.Factory.newInstance();
        hexPassword.setStringValue(String.valueOf(HexDump.shortToHex(PasswordRecord.hashPassword(password))).substring(2));
        return hexPassword;
    }
    
    public XSSFRow getRow(final int rownum) {
        return this._rows.get(rownum);
    }
    
    public int[] getRowBreaks() {
        if (!this.worksheet.isSetRowBreaks() || this.worksheet.getRowBreaks().sizeOfBrkArray() == 0) {
            return new int[0];
        }
        final CTBreak[] brkArray = this.worksheet.getRowBreaks().getBrkArray();
        final int[] breaks = new int[brkArray.length];
        for (int i = 0; i < brkArray.length; ++i) {
            final CTBreak brk = brkArray[i];
            breaks[i] = (int)brk.getId() - 1;
        }
        return breaks;
    }
    
    public boolean getRowSumsBelow() {
        final CTSheetPr sheetPr = this.worksheet.getSheetPr();
        final CTOutlinePr outlinePr = (sheetPr != null && sheetPr.isSetOutlinePr()) ? sheetPr.getOutlinePr() : null;
        return outlinePr == null || outlinePr.getSummaryBelow();
    }
    
    public void setRowSumsBelow(final boolean value) {
        this.ensureOutlinePr().setSummaryBelow(value);
    }
    
    public boolean getRowSumsRight() {
        final CTSheetPr sheetPr = this.worksheet.getSheetPr();
        final CTOutlinePr outlinePr = (sheetPr != null && sheetPr.isSetOutlinePr()) ? sheetPr.getOutlinePr() : CTOutlinePr.Factory.newInstance();
        return outlinePr.getSummaryRight();
    }
    
    public void setRowSumsRight(final boolean value) {
        this.ensureOutlinePr().setSummaryRight(value);
    }
    
    private CTOutlinePr ensureOutlinePr() {
        final CTSheetPr sheetPr = this.worksheet.isSetSheetPr() ? this.worksheet.getSheetPr() : this.worksheet.addNewSheetPr();
        return sheetPr.isSetOutlinePr() ? sheetPr.getOutlinePr() : sheetPr.addNewOutlinePr();
    }
    
    public boolean getScenarioProtect() {
        return this.worksheet.isSetSheetProtection() && this.worksheet.getSheetProtection().getScenarios();
    }
    
    public short getTopRow() {
        final String cellRef = this.getSheetTypeSheetView().getTopLeftCell();
        final CellReference cellReference = new CellReference(cellRef);
        return (short)cellReference.getRow();
    }
    
    public boolean getVerticallyCenter() {
        final CTPrintOptions opts = this.worksheet.getPrintOptions();
        return opts != null && opts.getVerticalCentered();
    }
    
    public void groupColumn(final int fromColumn, final int toColumn) {
        this.groupColumn1Based(fromColumn + 1, toColumn + 1);
    }
    
    private void groupColumn1Based(final int fromColumn, final int toColumn) {
        final CTCols ctCols = this.worksheet.getColsArray(0);
        final CTCol ctCol = CTCol.Factory.newInstance();
        ctCol.setMin(fromColumn);
        ctCol.setMax(toColumn);
        this.columnHelper.addCleanColIntoCols(ctCols, ctCol);
        CTCol col;
        for (int index = fromColumn; index <= toColumn; index = (int)col.getMax(), ++index) {
            col = this.columnHelper.getColumn1Based(index, false);
            final short outlineLevel = col.getOutlineLevel();
            col.setOutlineLevel((short)(outlineLevel + 1));
        }
        this.worksheet.setColsArray(0, ctCols);
        this.setSheetFormatPrOutlineLevelCol();
    }
    
    public void groupRow(final int fromRow, final int toRow) {
        for (int i = fromRow; i <= toRow; ++i) {
            XSSFRow xrow = this.getRow(i);
            if (xrow == null) {
                xrow = this.createRow(i);
            }
            final CTRow ctrow = xrow.getCTRow();
            final short outlineLevel = ctrow.getOutlineLevel();
            ctrow.setOutlineLevel((short)(outlineLevel + 1));
        }
        this.setSheetFormatPrOutlineLevelRow();
    }
    
    private short getMaxOutlineLevelRows() {
        short outlineLevel = 0;
        for (final XSSFRow xrow : this._rows.values()) {
            outlineLevel = ((xrow.getCTRow().getOutlineLevel() > outlineLevel) ? xrow.getCTRow().getOutlineLevel() : outlineLevel);
        }
        return outlineLevel;
    }
    
    private short getMaxOutlineLevelCols() {
        final CTCols ctCols = this.worksheet.getColsArray(0);
        short outlineLevel = 0;
        for (final CTCol col : ctCols.getColArray()) {
            outlineLevel = ((col.getOutlineLevel() > outlineLevel) ? col.getOutlineLevel() : outlineLevel);
        }
        return outlineLevel;
    }
    
    public boolean isColumnBroken(final int column) {
        final int[] colBreaks = this.getColumnBreaks();
        for (int i = 0; i < colBreaks.length; ++i) {
            if (colBreaks[i] == column) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isColumnHidden(final int columnIndex) {
        final CTCol col = this.columnHelper.getColumn(columnIndex, false);
        return col != null && col.getHidden();
    }
    
    public boolean isDisplayFormulas() {
        return this.getSheetTypeSheetView().getShowFormulas();
    }
    
    public boolean isDisplayGridlines() {
        return this.getSheetTypeSheetView().getShowGridLines();
    }
    
    public void setDisplayGridlines(final boolean show) {
        this.getSheetTypeSheetView().setShowGridLines(show);
    }
    
    public boolean isDisplayRowColHeadings() {
        return this.getSheetTypeSheetView().getShowRowColHeaders();
    }
    
    public void setDisplayRowColHeadings(final boolean show) {
        this.getSheetTypeSheetView().setShowRowColHeaders(show);
    }
    
    public boolean isPrintGridlines() {
        final CTPrintOptions opts = this.worksheet.getPrintOptions();
        return opts != null && opts.getGridLines();
    }
    
    public void setPrintGridlines(final boolean value) {
        final CTPrintOptions opts = this.worksheet.isSetPrintOptions() ? this.worksheet.getPrintOptions() : this.worksheet.addNewPrintOptions();
        opts.setGridLines(value);
    }
    
    public boolean isRowBroken(final int row) {
        final int[] rowBreaks = this.getRowBreaks();
        for (int i = 0; i < rowBreaks.length; ++i) {
            if (rowBreaks[i] == row) {
                return true;
            }
        }
        return false;
    }
    
    public void setRowBreak(final int row) {
        final CTPageBreak pgBreak = this.worksheet.isSetRowBreaks() ? this.worksheet.getRowBreaks() : this.worksheet.addNewRowBreaks();
        if (!this.isRowBroken(row)) {
            final CTBreak brk = pgBreak.addNewBrk();
            brk.setId(row + 1);
            brk.setMan(true);
            brk.setMax(SpreadsheetVersion.EXCEL2007.getLastColumnIndex());
            pgBreak.setCount(pgBreak.sizeOfBrkArray());
            pgBreak.setManualBreakCount(pgBreak.sizeOfBrkArray());
        }
    }
    
    public void removeColumnBreak(final int column) {
        if (!this.worksheet.isSetColBreaks()) {
            return;
        }
        final CTPageBreak pgBreak = this.worksheet.getColBreaks();
        final CTBreak[] brkArray = pgBreak.getBrkArray();
        for (int i = 0; i < brkArray.length; ++i) {
            if (brkArray[i].getId() == column + 1) {
                pgBreak.removeBrk(i);
            }
        }
    }
    
    public void removeMergedRegion(final int index) {
        final CTMergeCells ctMergeCells = this.worksheet.getMergeCells();
        final CTMergeCell[] mergeCellsArray = new CTMergeCell[ctMergeCells.sizeOfMergeCellArray() - 1];
        for (int i = 0; i < ctMergeCells.sizeOfMergeCellArray(); ++i) {
            if (i < index) {
                mergeCellsArray[i] = ctMergeCells.getMergeCellArray(i);
            }
            else if (i > index) {
                mergeCellsArray[i - 1] = ctMergeCells.getMergeCellArray(i);
            }
        }
        if (mergeCellsArray.length > 0) {
            ctMergeCells.setMergeCellArray(mergeCellsArray);
        }
        else {
            this.worksheet.unsetMergeCells();
        }
    }
    
    public void removeRow(final Row row) {
        if (row.getSheet() != this) {
            throw new IllegalArgumentException("Specified row does not belong to this sheet");
        }
        final ArrayList<XSSFCell> cellsToDelete = new ArrayList<XSSFCell>();
        for (final Cell cell : row) {
            cellsToDelete.add((XSSFCell)cell);
        }
        for (final XSSFCell cell2 : cellsToDelete) {
            row.removeCell(cell2);
        }
        final int idx = this._rows.headMap(row.getRowNum()).size();
        this._rows.remove(row.getRowNum());
        this.worksheet.getSheetData().removeRow(idx);
    }
    
    public void removeRowBreak(final int row) {
        if (!this.worksheet.isSetRowBreaks()) {
            return;
        }
        final CTPageBreak pgBreak = this.worksheet.getRowBreaks();
        final CTBreak[] brkArray = pgBreak.getBrkArray();
        for (int i = 0; i < brkArray.length; ++i) {
            if (brkArray[i].getId() == row + 1) {
                pgBreak.removeBrk(i);
            }
        }
    }
    
    public void setForceFormulaRecalculation(final boolean value) {
        if (this.worksheet.isSetSheetCalcPr()) {
            final CTSheetCalcPr calc = this.worksheet.getSheetCalcPr();
            calc.setFullCalcOnLoad(value);
        }
        else if (value) {
            final CTSheetCalcPr calc = this.worksheet.addNewSheetCalcPr();
            calc.setFullCalcOnLoad(value);
        }
    }
    
    public boolean getForceFormulaRecalculation() {
        if (this.worksheet.isSetSheetCalcPr()) {
            final CTSheetCalcPr calc = this.worksheet.getSheetCalcPr();
            return calc.getFullCalcOnLoad();
        }
        return false;
    }
    
    public Iterator<Row> rowIterator() {
        return (Iterator<Row>)this._rows.values().iterator();
    }
    
    public Iterator<Row> iterator() {
        return this.rowIterator();
    }
    
    public boolean getAutobreaks() {
        final CTSheetPr sheetPr = this.getSheetTypeSheetPr();
        final CTPageSetUpPr psSetup = (sheetPr == null || !sheetPr.isSetPageSetUpPr()) ? CTPageSetUpPr.Factory.newInstance() : sheetPr.getPageSetUpPr();
        return psSetup.getAutoPageBreaks();
    }
    
    public void setAutobreaks(final boolean value) {
        final CTSheetPr sheetPr = this.getSheetTypeSheetPr();
        final CTPageSetUpPr psSetup = sheetPr.isSetPageSetUpPr() ? sheetPr.getPageSetUpPr() : sheetPr.addNewPageSetUpPr();
        psSetup.setAutoPageBreaks(value);
    }
    
    public void setColumnBreak(final int column) {
        if (!this.isColumnBroken(column)) {
            final CTPageBreak pgBreak = this.worksheet.isSetColBreaks() ? this.worksheet.getColBreaks() : this.worksheet.addNewColBreaks();
            final CTBreak brk = pgBreak.addNewBrk();
            brk.setId(column + 1);
            brk.setMan(true);
            brk.setMax(SpreadsheetVersion.EXCEL2007.getLastRowIndex());
            pgBreak.setCount(pgBreak.sizeOfBrkArray());
            pgBreak.setManualBreakCount(pgBreak.sizeOfBrkArray());
        }
    }
    
    public void setColumnGroupCollapsed(final int columnNumber, final boolean collapsed) {
        if (collapsed) {
            this.collapseColumn(columnNumber);
        }
        else {
            this.expandColumn(columnNumber);
        }
    }
    
    private void collapseColumn(final int columnNumber) {
        final CTCols cols = this.worksheet.getColsArray(0);
        final CTCol col = this.columnHelper.getColumn(columnNumber, false);
        final int colInfoIx = this.columnHelper.getIndexOfColumn(cols, col);
        if (colInfoIx == -1) {
            return;
        }
        final int groupStartColInfoIx = this.findStartOfColumnOutlineGroup(colInfoIx);
        final CTCol columnInfo = cols.getColArray(groupStartColInfoIx);
        final int lastColMax = this.setGroupHidden(groupStartColInfoIx, columnInfo.getOutlineLevel(), true);
        this.setColumn(lastColMax + 1, null, 0, null, null, Boolean.TRUE);
    }
    
    private void setColumn(final int targetColumnIx, final Short xfIndex, final Integer style, final Integer level, final Boolean hidden, final Boolean collapsed) {
        final CTCols cols = this.worksheet.getColsArray(0);
        CTCol ci = null;
        int k;
        CTCol tci;
        for (k = 0, k = 0; k < cols.sizeOfColArray(); ++k) {
            tci = cols.getColArray(k);
            if (tci.getMin() >= targetColumnIx && tci.getMax() <= targetColumnIx) {
                ci = tci;
                break;
            }
            if (tci.getMin() > targetColumnIx) {
                break;
            }
        }
        if (ci == null) {
            final CTCol nci = CTCol.Factory.newInstance();
            nci.setMin(targetColumnIx);
            nci.setMax(targetColumnIx);
            this.unsetCollapsed(collapsed, nci);
            this.columnHelper.addCleanColIntoCols(cols, nci);
            return;
        }
        final boolean styleChanged = style != null && ci.getStyle() != style;
        final boolean levelChanged = level != null && ci.getOutlineLevel() != level;
        final boolean hiddenChanged = hidden != null && ci.getHidden() != hidden;
        final boolean collapsedChanged = collapsed != null && ci.getCollapsed() != collapsed;
        final boolean columnChanged = levelChanged || hiddenChanged || collapsedChanged || styleChanged;
        if (!columnChanged) {
            return;
        }
        if (ci.getMin() == targetColumnIx && ci.getMax() == targetColumnIx) {
            this.unsetCollapsed(collapsed, ci);
            return;
        }
        if (ci.getMin() == targetColumnIx || ci.getMax() == targetColumnIx) {
            if (ci.getMin() == targetColumnIx) {
                ci.setMin(targetColumnIx + 1);
            }
            else {
                ci.setMax(targetColumnIx - 1);
                ++k;
            }
            final CTCol nci2 = this.columnHelper.cloneCol(cols, ci);
            nci2.setMin(targetColumnIx);
            this.unsetCollapsed(collapsed, nci2);
            this.columnHelper.addCleanColIntoCols(cols, nci2);
        }
        else {
            final CTCol ciStart = ci;
            final CTCol ciMid = this.columnHelper.cloneCol(cols, ci);
            final CTCol ciEnd = this.columnHelper.cloneCol(cols, ci);
            final int lastcolumn = (int)ci.getMax();
            ciStart.setMax(targetColumnIx - 1);
            ciMid.setMin(targetColumnIx);
            ciMid.setMax(targetColumnIx);
            this.unsetCollapsed(collapsed, ciMid);
            this.columnHelper.addCleanColIntoCols(cols, ciMid);
            ciEnd.setMin(targetColumnIx + 1);
            ciEnd.setMax(lastcolumn);
            this.columnHelper.addCleanColIntoCols(cols, ciEnd);
        }
    }
    
    private void unsetCollapsed(final boolean collapsed, final CTCol ci) {
        if (collapsed) {
            ci.setCollapsed(collapsed);
        }
        else {
            ci.unsetCollapsed();
        }
    }
    
    private int setGroupHidden(final int pIdx, final int level, final boolean hidden) {
        final CTCols cols = this.worksheet.getColsArray(0);
        int idx = pIdx;
        CTCol columnInfo = cols.getColArray(idx);
        while (idx < cols.sizeOfColArray()) {
            columnInfo.setHidden(hidden);
            if (idx + 1 < cols.sizeOfColArray()) {
                final CTCol nextColumnInfo = cols.getColArray(idx + 1);
                if (!this.isAdjacentBefore(columnInfo, nextColumnInfo)) {
                    break;
                }
                if (nextColumnInfo.getOutlineLevel() < level) {
                    break;
                }
                columnInfo = nextColumnInfo;
            }
            ++idx;
        }
        return (int)columnInfo.getMax();
    }
    
    private boolean isAdjacentBefore(final CTCol col, final CTCol other_col) {
        return col.getMax() == other_col.getMin() - 1L;
    }
    
    private int findStartOfColumnOutlineGroup(final int pIdx) {
        final CTCols cols = this.worksheet.getColsArray(0);
        CTCol columnInfo = cols.getColArray(pIdx);
        final int level = columnInfo.getOutlineLevel();
        int idx;
        CTCol prevColumnInfo;
        for (idx = pIdx; idx != 0; --idx, columnInfo = prevColumnInfo) {
            prevColumnInfo = cols.getColArray(idx - 1);
            if (!this.isAdjacentBefore(prevColumnInfo, columnInfo)) {
                break;
            }
            if (prevColumnInfo.getOutlineLevel() < level) {
                break;
            }
        }
        return idx;
    }
    
    private int findEndOfColumnOutlineGroup(final int colInfoIndex) {
        final CTCols cols = this.worksheet.getColsArray(0);
        CTCol columnInfo = cols.getColArray(colInfoIndex);
        final int level = columnInfo.getOutlineLevel();
        int idx;
        CTCol nextColumnInfo;
        for (idx = colInfoIndex; idx < cols.sizeOfColArray() - 1; ++idx, columnInfo = nextColumnInfo) {
            nextColumnInfo = cols.getColArray(idx + 1);
            if (!this.isAdjacentBefore(columnInfo, nextColumnInfo)) {
                break;
            }
            if (nextColumnInfo.getOutlineLevel() < level) {
                break;
            }
        }
        return idx;
    }
    
    private void expandColumn(final int columnIndex) {
        final CTCols cols = this.worksheet.getColsArray(0);
        final CTCol col = this.columnHelper.getColumn(columnIndex, false);
        final int colInfoIx = this.columnHelper.getIndexOfColumn(cols, col);
        final int idx = this.findColInfoIdx((int)col.getMax(), colInfoIx);
        if (idx == -1) {
            return;
        }
        if (!this.isColumnGroupCollapsed(idx)) {
            return;
        }
        final int startIdx = this.findStartOfColumnOutlineGroup(idx);
        final int endIdx = this.findEndOfColumnOutlineGroup(idx);
        final CTCol columnInfo = cols.getColArray(endIdx);
        if (!this.isColumnGroupHiddenByParent(idx)) {
            final int outlineLevel = columnInfo.getOutlineLevel();
            boolean nestedGroup = false;
            for (int i = startIdx; i <= endIdx; ++i) {
                final CTCol ci = cols.getColArray(i);
                if (outlineLevel == ci.getOutlineLevel()) {
                    ci.unsetHidden();
                    if (nestedGroup) {
                        nestedGroup = false;
                        ci.setCollapsed(true);
                    }
                }
                else {
                    nestedGroup = true;
                }
            }
        }
        this.setColumn((int)columnInfo.getMax() + 1, null, null, null, Boolean.FALSE, Boolean.FALSE);
    }
    
    private boolean isColumnGroupHiddenByParent(final int idx) {
        final CTCols cols = this.worksheet.getColsArray(0);
        int endLevel = 0;
        boolean endHidden = false;
        final int endOfOutlineGroupIdx = this.findEndOfColumnOutlineGroup(idx);
        if (endOfOutlineGroupIdx < cols.sizeOfColArray()) {
            final CTCol nextInfo = cols.getColArray(endOfOutlineGroupIdx + 1);
            if (this.isAdjacentBefore(cols.getColArray(endOfOutlineGroupIdx), nextInfo)) {
                endLevel = nextInfo.getOutlineLevel();
                endHidden = nextInfo.getHidden();
            }
        }
        int startLevel = 0;
        boolean startHidden = false;
        final int startOfOutlineGroupIdx = this.findStartOfColumnOutlineGroup(idx);
        if (startOfOutlineGroupIdx > 0) {
            final CTCol prevInfo = cols.getColArray(startOfOutlineGroupIdx - 1);
            if (this.isAdjacentBefore(prevInfo, cols.getColArray(startOfOutlineGroupIdx))) {
                startLevel = prevInfo.getOutlineLevel();
                startHidden = prevInfo.getHidden();
            }
        }
        if (endLevel > startLevel) {
            return endHidden;
        }
        return startHidden;
    }
    
    private int findColInfoIdx(final int columnValue, final int fromColInfoIdx) {
        final CTCols cols = this.worksheet.getColsArray(0);
        if (columnValue < 0) {
            throw new IllegalArgumentException("column parameter out of range: " + columnValue);
        }
        if (fromColInfoIdx < 0) {
            throw new IllegalArgumentException("fromIdx parameter out of range: " + fromColInfoIdx);
        }
        for (int k = fromColInfoIdx; k < cols.sizeOfColArray(); ++k) {
            final CTCol ci = cols.getColArray(k);
            if (this.containsColumn(ci, columnValue)) {
                return k;
            }
            if (ci.getMin() > fromColInfoIdx) {
                break;
            }
        }
        return -1;
    }
    
    private boolean containsColumn(final CTCol col, final int columnIndex) {
        return col.getMin() <= columnIndex && columnIndex <= col.getMax();
    }
    
    private boolean isColumnGroupCollapsed(final int idx) {
        final CTCols cols = this.worksheet.getColsArray(0);
        final int endOfOutlineGroupIdx = this.findEndOfColumnOutlineGroup(idx);
        final int nextColInfoIx = endOfOutlineGroupIdx + 1;
        if (nextColInfoIx >= cols.sizeOfColArray()) {
            return false;
        }
        final CTCol nextColInfo = cols.getColArray(nextColInfoIx);
        final CTCol col = cols.getColArray(endOfOutlineGroupIdx);
        return this.isAdjacentBefore(col, nextColInfo) && nextColInfo.getCollapsed();
    }
    
    public void setColumnHidden(final int columnIndex, final boolean hidden) {
        this.columnHelper.setColHidden(columnIndex, hidden);
    }
    
    public void setColumnWidth(final int columnIndex, final int width) {
        if (width > 65280) {
            throw new IllegalArgumentException("The maximum column width for an individual cell is 255 characters.");
        }
        this.columnHelper.setColWidth(columnIndex, width / 256.0);
        this.columnHelper.setCustomWidth(columnIndex, true);
    }
    
    public void setDefaultColumnStyle(final int column, final CellStyle style) {
        this.columnHelper.setColDefaultStyle(column, style);
    }
    
    public void setDefaultColumnWidth(final int width) {
        this.getSheetTypeSheetFormatPr().setBaseColWidth(width);
    }
    
    public void setDefaultRowHeight(final short height) {
        this.setDefaultRowHeightInPoints(height / 20.0f);
    }
    
    public void setDefaultRowHeightInPoints(final float height) {
        final CTSheetFormatPr pr = this.getSheetTypeSheetFormatPr();
        pr.setDefaultRowHeight(height);
        pr.setCustomHeight(true);
    }
    
    public void setDisplayFormulas(final boolean show) {
        this.getSheetTypeSheetView().setShowFormulas(show);
    }
    
    private CTSheetView getSheetTypeSheetView() {
        if (this.getDefaultSheetView() == null) {
            this.getSheetTypeSheetViews().setSheetViewArray(0, CTSheetView.Factory.newInstance());
        }
        return this.getDefaultSheetView();
    }
    
    public void setFitToPage(final boolean b) {
        this.getSheetTypePageSetUpPr().setFitToPage(b);
    }
    
    public void setHorizontallyCenter(final boolean value) {
        final CTPrintOptions opts = this.worksheet.isSetPrintOptions() ? this.worksheet.getPrintOptions() : this.worksheet.addNewPrintOptions();
        opts.setHorizontalCentered(value);
    }
    
    public void setVerticallyCenter(final boolean value) {
        final CTPrintOptions opts = this.worksheet.isSetPrintOptions() ? this.worksheet.getPrintOptions() : this.worksheet.addNewPrintOptions();
        opts.setVerticalCentered(value);
    }
    
    public void setRowGroupCollapsed(final int rowIndex, final boolean collapse) {
        if (collapse) {
            this.collapseRow(rowIndex);
        }
        else {
            this.expandRow(rowIndex);
        }
    }
    
    private void collapseRow(final int rowIndex) {
        final XSSFRow row = this.getRow(rowIndex);
        if (row != null) {
            final int startRow = this.findStartOfRowOutlineGroup(rowIndex);
            final int lastRow = this.writeHidden(row, startRow, true);
            if (this.getRow(lastRow) != null) {
                this.getRow(lastRow).getCTRow().setCollapsed(true);
            }
            else {
                final XSSFRow newRow = this.createRow(lastRow);
                newRow.getCTRow().setCollapsed(true);
            }
        }
    }
    
    private int findStartOfRowOutlineGroup(final int rowIndex) {
        final int level = this.getRow(rowIndex).getCTRow().getOutlineLevel();
        int currentRow;
        for (currentRow = rowIndex; this.getRow(currentRow) != null; --currentRow) {
            if (this.getRow(currentRow).getCTRow().getOutlineLevel() < level) {
                return currentRow + 1;
            }
        }
        return currentRow;
    }
    
    private int writeHidden(XSSFRow xRow, int rowIndex, final boolean hidden) {
        final int level = xRow.getCTRow().getOutlineLevel();
        final Iterator<Row> it = this.rowIterator();
        while (it.hasNext()) {
            xRow = it.next();
            if (xRow.getCTRow().getOutlineLevel() >= level) {
                xRow.getCTRow().setHidden(hidden);
                ++rowIndex;
            }
        }
        return rowIndex;
    }
    
    private void expandRow(final int rowNumber) {
        if (rowNumber == -1) {
            return;
        }
        final XSSFRow row = this.getRow(rowNumber);
        if (!row.getCTRow().isSetHidden()) {
            return;
        }
        final int startIdx = this.findStartOfRowOutlineGroup(rowNumber);
        final int endIdx = this.findEndOfRowOutlineGroup(rowNumber);
        if (!this.isRowGroupHiddenByParent(rowNumber)) {
            for (int i = startIdx; i < endIdx; ++i) {
                if (row.getCTRow().getOutlineLevel() == this.getRow(i).getCTRow().getOutlineLevel()) {
                    this.getRow(i).getCTRow().unsetHidden();
                }
                else if (!this.isRowGroupCollapsed(i)) {
                    this.getRow(i).getCTRow().unsetHidden();
                }
            }
        }
        this.getRow(endIdx).getCTRow().unsetCollapsed();
    }
    
    public int findEndOfRowOutlineGroup(final int row) {
        int level;
        int currentRow;
        for (level = this.getRow(row).getCTRow().getOutlineLevel(), currentRow = row; currentRow < this.getLastRowNum() && this.getRow(currentRow) != null && this.getRow(currentRow).getCTRow().getOutlineLevel() >= level; ++currentRow) {}
        return currentRow;
    }
    
    private boolean isRowGroupHiddenByParent(final int row) {
        final int endOfOutlineGroupIdx = this.findEndOfRowOutlineGroup(row);
        int endLevel;
        boolean endHidden;
        if (this.getRow(endOfOutlineGroupIdx) == null) {
            endLevel = 0;
            endHidden = false;
        }
        else {
            endLevel = this.getRow(endOfOutlineGroupIdx).getCTRow().getOutlineLevel();
            endHidden = this.getRow(endOfOutlineGroupIdx).getCTRow().getHidden();
        }
        final int startOfOutlineGroupIdx = this.findStartOfRowOutlineGroup(row);
        int startLevel;
        boolean startHidden;
        if (startOfOutlineGroupIdx < 0 || this.getRow(startOfOutlineGroupIdx) == null) {
            startLevel = 0;
            startHidden = false;
        }
        else {
            startLevel = this.getRow(startOfOutlineGroupIdx).getCTRow().getOutlineLevel();
            startHidden = this.getRow(startOfOutlineGroupIdx).getCTRow().getHidden();
        }
        if (endLevel > startLevel) {
            return endHidden;
        }
        return startHidden;
    }
    
    private boolean isRowGroupCollapsed(final int row) {
        final int collapseRow = this.findEndOfRowOutlineGroup(row) + 1;
        return this.getRow(collapseRow) != null && this.getRow(collapseRow).getCTRow().getCollapsed();
    }
    
    public void setZoom(final int numerator, final int denominator) {
        final int zoom = 100 * numerator / denominator;
        this.setZoom(zoom);
    }
    
    public void setZoom(final int scale) {
        if (scale < 10 || scale > 400) {
            throw new IllegalArgumentException("Valid scale values range from 10 to 400");
        }
        this.getSheetTypeSheetView().setZoomScale(scale);
    }
    
    public void shiftRows(final int startRow, final int endRow, final int n) {
        this.shiftRows(startRow, endRow, n, false, false);
    }
    
    public void shiftRows(final int startRow, final int endRow, final int n, final boolean copyRowHeight, final boolean resetOriginalRowHeight) {
        final Iterator<Row> it = this.rowIterator();
        while (it.hasNext()) {
            final XSSFRow row = it.next();
            final int rownum = row.getRowNum();
            if (rownum < startRow) {
                continue;
            }
            if (!copyRowHeight) {
                row.setHeight((short)(-1));
            }
            if (this.removeRow(startRow, endRow, n, rownum)) {
                final int idx = this._rows.headMap(row.getRowNum()).size();
                this.worksheet.getSheetData().removeRow(idx);
                it.remove();
            }
            else if (rownum >= startRow && rownum <= endRow) {
                row.shift(n);
            }
            if (this.sheetComments == null) {
                continue;
            }
            final CTCommentList lst = this.sheetComments.getCTComments().getCommentList();
            for (final CTComment comment : lst.getCommentArray()) {
                CellReference ref = new CellReference(comment.getRef());
                if (ref.getRow() == rownum) {
                    ref = new CellReference(rownum + n, ref.getCol());
                    comment.setRef(ref.formatAsString());
                }
            }
        }
        final XSSFRowShifter rowShifter = new XSSFRowShifter(this);
        final int sheetIndex = this.getWorkbook().getSheetIndex(this);
        final FormulaShifter shifter = FormulaShifter.createForRowShift(sheetIndex, startRow, endRow, n);
        rowShifter.updateNamedRanges(shifter);
        rowShifter.updateFormulas(shifter);
        rowShifter.shiftMerged(startRow, endRow, n);
        rowShifter.updateConditionalFormatting(shifter);
        final TreeMap<Integer, XSSFRow> map = new TreeMap<Integer, XSSFRow>();
        for (final XSSFRow r : this._rows.values()) {
            map.put(r.getRowNum(), r);
        }
        this._rows = map;
    }
    
    public void showInPane(final short toprow, final short leftcol) {
        final CellReference cellReference = new CellReference(toprow, leftcol);
        final String cellRef = cellReference.formatAsString();
        this.getPane().setTopLeftCell(cellRef);
    }
    
    public void ungroupColumn(final int fromColumn, final int toColumn) {
        final CTCols cols = this.worksheet.getColsArray(0);
        for (int index = fromColumn; index <= toColumn; ++index) {
            final CTCol col = this.columnHelper.getColumn(index, false);
            if (col != null) {
                final short outlineLevel = col.getOutlineLevel();
                col.setOutlineLevel((short)(outlineLevel - 1));
                index = (int)col.getMax();
                if (col.getOutlineLevel() <= 0) {
                    final int colIndex = this.columnHelper.getIndexOfColumn(cols, col);
                    this.worksheet.getColsArray(0).removeCol(colIndex);
                }
            }
        }
        this.worksheet.setColsArray(0, cols);
        this.setSheetFormatPrOutlineLevelCol();
    }
    
    public void ungroupRow(final int fromRow, final int toRow) {
        for (int i = fromRow; i <= toRow; ++i) {
            final XSSFRow xrow = this.getRow(i);
            if (xrow != null) {
                final CTRow ctrow = xrow.getCTRow();
                final short outlinelevel = ctrow.getOutlineLevel();
                ctrow.setOutlineLevel((short)(outlinelevel - 1));
                if (ctrow.getOutlineLevel() == 0 && xrow.getFirstCellNum() == -1) {
                    this.removeRow(xrow);
                }
            }
        }
        this.setSheetFormatPrOutlineLevelRow();
    }
    
    private void setSheetFormatPrOutlineLevelRow() {
        final short maxLevelRow = this.getMaxOutlineLevelRows();
        this.getSheetTypeSheetFormatPr().setOutlineLevelRow(maxLevelRow);
    }
    
    private void setSheetFormatPrOutlineLevelCol() {
        final short maxLevelCol = this.getMaxOutlineLevelCols();
        this.getSheetTypeSheetFormatPr().setOutlineLevelCol(maxLevelCol);
    }
    
    private CTSheetViews getSheetTypeSheetViews() {
        if (this.worksheet.getSheetViews() == null) {
            this.worksheet.setSheetViews(CTSheetViews.Factory.newInstance());
            this.worksheet.getSheetViews().addNewSheetView();
        }
        return this.worksheet.getSheetViews();
    }
    
    public boolean isSelected() {
        final CTSheetView view = this.getDefaultSheetView();
        return view != null && view.getTabSelected();
    }
    
    public void setSelected(final boolean value) {
        final CTSheetViews views = this.getSheetTypeSheetViews();
        for (final CTSheetView view : views.getSheetViewArray()) {
            view.setTabSelected(value);
        }
    }
    
    @Deprecated
    public static void setCellComment(final String cellRef, final XSSFComment comment) {
        final CellReference cellReference = new CellReference(cellRef);
        comment.setRow(cellReference.getRow());
        comment.setColumn(cellReference.getCol());
    }
    
    @Internal
    public void addHyperlink(final XSSFHyperlink hyperlink) {
        this.hyperlinks.add(hyperlink);
    }
    
    public String getActiveCell() {
        return this.getSheetTypeSelection().getActiveCell();
    }
    
    public void setActiveCell(final String cellRef) {
        final CTSelection ctsel = this.getSheetTypeSelection();
        ctsel.setActiveCell(cellRef);
        ctsel.setSqref(Arrays.asList(cellRef));
    }
    
    public boolean hasComments() {
        return this.sheetComments != null && this.sheetComments.getNumberOfComments() > 0;
    }
    
    protected int getNumberOfComments() {
        if (this.sheetComments == null) {
            return 0;
        }
        return this.sheetComments.getNumberOfComments();
    }
    
    private CTSelection getSheetTypeSelection() {
        if (this.getSheetTypeSheetView().sizeOfSelectionArray() == 0) {
            this.getSheetTypeSheetView().insertNewSelection(0);
        }
        return this.getSheetTypeSheetView().getSelectionArray(0);
    }
    
    private CTSheetView getDefaultSheetView() {
        final CTSheetViews views = this.getSheetTypeSheetViews();
        final int sz = (views == null) ? 0 : views.sizeOfSheetViewArray();
        if (sz == 0) {
            return null;
        }
        return views.getSheetViewArray(sz - 1);
    }
    
    protected CommentsTable getCommentsTable(final boolean create) {
        if (this.sheetComments == null && create) {
            try {
                this.sheetComments = (CommentsTable)this.createRelationship(XSSFRelation.SHEET_COMMENTS, XSSFFactory.getInstance(), (int)this.sheet.getSheetId());
            }
            catch (PartAlreadyExistsException e) {
                this.sheetComments = (CommentsTable)this.createRelationship(XSSFRelation.SHEET_COMMENTS, XSSFFactory.getInstance(), -1);
            }
        }
        return this.sheetComments;
    }
    
    private CTPageSetUpPr getSheetTypePageSetUpPr() {
        final CTSheetPr sheetPr = this.getSheetTypeSheetPr();
        return sheetPr.isSetPageSetUpPr() ? sheetPr.getPageSetUpPr() : sheetPr.addNewPageSetUpPr();
    }
    
    private boolean removeRow(final int startRow, final int endRow, final int n, final int rownum) {
        if (rownum >= startRow + n && rownum <= endRow + n) {
            if (n > 0 && rownum > endRow) {
                return true;
            }
            if (n < 0 && rownum < startRow) {
                return true;
            }
        }
        return false;
    }
    
    private CTPane getPane() {
        if (this.getDefaultSheetView().getPane() == null) {
            this.getDefaultSheetView().addNewPane();
        }
        return this.getDefaultSheetView().getPane();
    }
    
    CTCellFormula getSharedFormula(final int sid) {
        return this.sharedFormulas.get(sid);
    }
    
    void onReadCell(final XSSFCell cell) {
        final CTCell ct = cell.getCTCell();
        final CTCellFormula f = ct.getF();
        if (f != null && f.getT() == STCellFormulaType.SHARED && f.isSetRef() && f.getStringValue() != null) {
            final CTCellFormula sf = (CTCellFormula)f.copy();
            final CellRangeAddress sfRef = CellRangeAddress.valueOf(sf.getRef());
            final CellReference cellRef = new CellReference(cell);
            if (cellRef.getCol() > sfRef.getFirstColumn() || cellRef.getRow() > sfRef.getFirstRow()) {
                final String effectiveRef = new CellRangeAddress(Math.max(cellRef.getRow(), sfRef.getFirstRow()), sfRef.getLastRow(), Math.max(cellRef.getCol(), sfRef.getFirstColumn()), sfRef.getLastColumn()).formatAsString();
                sf.setRef(effectiveRef);
            }
            this.sharedFormulas.put((int)f.getSi(), sf);
        }
        if (f != null && f.getT() == STCellFormulaType.ARRAY && f.getRef() != null) {
            this.arrayFormulas.add(CellRangeAddress.valueOf(f.getRef()));
        }
    }
    
    @Override
    protected void commit() throws IOException {
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.write(out);
        out.close();
    }
    
    protected void write(final OutputStream out) throws IOException {
        if (this.worksheet.sizeOfColsArray() == 1) {
            final CTCols col = this.worksheet.getColsArray(0);
            if (col.sizeOfColArray() == 0) {
                this.worksheet.setColsArray(null);
            }
        }
        if (this.hyperlinks.size() > 0) {
            if (this.worksheet.getHyperlinks() == null) {
                this.worksheet.addNewHyperlinks();
            }
            final CTHyperlink[] ctHls = new CTHyperlink[this.hyperlinks.size()];
            for (int i = 0; i < ctHls.length; ++i) {
                final XSSFHyperlink hyperlink = this.hyperlinks.get(i);
                hyperlink.generateRelationIfNeeded(this.getPackagePart());
                ctHls[i] = hyperlink.getCTHyperlink();
            }
            this.worksheet.getHyperlinks().setHyperlinkArray(ctHls);
        }
        for (final XSSFRow row : this._rows.values()) {
            row.onDocumentWrite();
        }
        final XmlOptions xmlOptions = new XmlOptions(XSSFSheet.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTWorksheet.type.getName().getNamespaceURI(), "worksheet"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put(STRelationshipId.type.getName().getNamespaceURI(), "r");
        xmlOptions.setSaveSuggestedPrefixes(map);
        this.worksheet.save(out, xmlOptions);
    }
    
    public boolean isAutoFilterLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getAutoFilter();
    }
    
    public boolean isDeleteColumnsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getDeleteColumns();
    }
    
    public boolean isDeleteRowsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getDeleteRows();
    }
    
    public boolean isFormatCellsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getFormatCells();
    }
    
    public boolean isFormatColumnsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getFormatColumns();
    }
    
    public boolean isFormatRowsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getFormatRows();
    }
    
    public boolean isInsertColumnsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getInsertColumns();
    }
    
    public boolean isInsertHyperlinksLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getInsertHyperlinks();
    }
    
    public boolean isInsertRowsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getInsertRows();
    }
    
    public boolean isPivotTablesLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getPivotTables();
    }
    
    public boolean isSortLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getSort();
    }
    
    public boolean isObjectsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getObjects();
    }
    
    public boolean isScenariosLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getScenarios();
    }
    
    public boolean isSelectLockedCellsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getSelectLockedCells();
    }
    
    public boolean isSelectUnlockedCellsLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getSelectUnlockedCells();
    }
    
    public boolean isSheetLocked() {
        this.createProtectionFieldIfNotPresent();
        return this.sheetProtectionEnabled() && this.worksheet.getSheetProtection().getSheet();
    }
    
    public void enableLocking() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setSheet(true);
    }
    
    public void disableLocking() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setSheet(false);
    }
    
    public void lockAutoFilter() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setAutoFilter(true);
    }
    
    public void lockDeleteColumns() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setDeleteColumns(true);
    }
    
    public void lockDeleteRows() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setDeleteRows(true);
    }
    
    public void lockFormatCells() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setDeleteColumns(true);
    }
    
    public void lockFormatColumns() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setFormatColumns(true);
    }
    
    public void lockFormatRows() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setFormatRows(true);
    }
    
    public void lockInsertColumns() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setInsertColumns(true);
    }
    
    public void lockInsertHyperlinks() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setInsertHyperlinks(true);
    }
    
    public void lockInsertRows() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setInsertRows(true);
    }
    
    public void lockPivotTables() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setPivotTables(true);
    }
    
    public void lockSort() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setSort(true);
    }
    
    public void lockObjects() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setObjects(true);
    }
    
    public void lockScenarios() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setScenarios(true);
    }
    
    public void lockSelectLockedCells() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setSelectLockedCells(true);
    }
    
    public void lockSelectUnlockedCells() {
        this.createProtectionFieldIfNotPresent();
        this.worksheet.getSheetProtection().setSelectUnlockedCells(true);
    }
    
    private void createProtectionFieldIfNotPresent() {
        if (this.worksheet.getSheetProtection() == null) {
            this.worksheet.setSheetProtection(CTSheetProtection.Factory.newInstance());
        }
    }
    
    private boolean sheetProtectionEnabled() {
        return this.worksheet.getSheetProtection().getSheet();
    }
    
    boolean isCellInArrayFormulaContext(final XSSFCell cell) {
        for (final CellRangeAddress range : this.arrayFormulas) {
            if (range.isInRange(cell.getRowIndex(), cell.getColumnIndex())) {
                return true;
            }
        }
        return false;
    }
    
    XSSFCell getFirstCellInArrayFormula(final XSSFCell cell) {
        for (final CellRangeAddress range : this.arrayFormulas) {
            if (range.isInRange(cell.getRowIndex(), cell.getColumnIndex())) {
                return this.getRow(range.getFirstRow()).getCell(range.getFirstColumn());
            }
        }
        return null;
    }
    
    private CellRange<XSSFCell> getCellRange(final CellRangeAddress range) {
        final int firstRow = range.getFirstRow();
        final int firstColumn = range.getFirstColumn();
        final int lastRow = range.getLastRow();
        final int lastColumn = range.getLastColumn();
        final int height = lastRow - firstRow + 1;
        final int width = lastColumn - firstColumn + 1;
        final List<XSSFCell> temp = new ArrayList<XSSFCell>(height * width);
        for (int rowIn = firstRow; rowIn <= lastRow; ++rowIn) {
            for (int colIn = firstColumn; colIn <= lastColumn; ++colIn) {
                XSSFRow row = this.getRow(rowIn);
                if (row == null) {
                    row = this.createRow(rowIn);
                }
                XSSFCell cell = row.getCell(colIn);
                if (cell == null) {
                    cell = row.createCell(colIn);
                }
                temp.add(cell);
            }
        }
        return SSCellRange.create(firstRow, firstColumn, height, width, temp, XSSFCell.class);
    }
    
    public CellRange<XSSFCell> setArrayFormula(final String formula, final CellRangeAddress range) {
        final CellRange<XSSFCell> cr = this.getCellRange(range);
        final XSSFCell mainArrayFormulaCell = cr.getTopLeftCell();
        mainArrayFormulaCell.setCellArrayFormula(formula, range);
        this.arrayFormulas.add(range);
        return cr;
    }
    
    public CellRange<XSSFCell> removeArrayFormula(final Cell cell) {
        if (cell.getSheet() != this) {
            throw new IllegalArgumentException("Specified cell does not belong to this sheet.");
        }
        for (final CellRangeAddress range : this.arrayFormulas) {
            if (range.isInRange(cell.getRowIndex(), cell.getColumnIndex())) {
                this.arrayFormulas.remove(range);
                final CellRange<XSSFCell> cr = this.getCellRange(range);
                for (final XSSFCell c : cr) {
                    c.setCellType(3);
                }
                return cr;
            }
        }
        final String ref = ((XSSFCell)cell).getCTCell().getR();
        throw new IllegalArgumentException("Cell " + ref + " is not part of an array formula.");
    }
    
    public DataValidationHelper getDataValidationHelper() {
        return this.dataValidationHelper;
    }
    
    public List<XSSFDataValidation> getDataValidations() {
        final List<XSSFDataValidation> xssfValidations = new ArrayList<XSSFDataValidation>();
        final CTDataValidations dataValidations = this.worksheet.getDataValidations();
        if (dataValidations != null && dataValidations.getCount() > 0L) {
            for (final CTDataValidation ctDataValidation : dataValidations.getDataValidationArray()) {
                final CellRangeAddressList addressList = new CellRangeAddressList();
                final List<String> sqref = (List<String>)ctDataValidation.getSqref();
                for (final String stRef : sqref) {
                    final String[] regions = stRef.split(" ");
                    for (int i = 0; i < regions.length; ++i) {
                        final String[] parts = regions[i].split(":");
                        final CellReference begin = new CellReference(parts[0]);
                        final CellReference end = (parts.length > 1) ? new CellReference(parts[1]) : begin;
                        final CellRangeAddress cellRangeAddress = new CellRangeAddress(begin.getRow(), end.getRow(), begin.getCol(), end.getCol());
                        addressList.addCellRangeAddress(cellRangeAddress);
                    }
                }
                final XSSFDataValidation xssfDataValidation = new XSSFDataValidation(addressList, ctDataValidation);
                xssfValidations.add(xssfDataValidation);
            }
        }
        return xssfValidations;
    }
    
    public void addValidationData(final DataValidation dataValidation) {
        final XSSFDataValidation xssfDataValidation = (XSSFDataValidation)dataValidation;
        CTDataValidations dataValidations = this.worksheet.getDataValidations();
        if (dataValidations == null) {
            dataValidations = this.worksheet.addNewDataValidations();
        }
        final int currentCount = dataValidations.sizeOfDataValidationArray();
        final CTDataValidation newval = dataValidations.addNewDataValidation();
        newval.set(xssfDataValidation.getCtDdataValidation());
        dataValidations.setCount(currentCount + 1);
    }
    
    public XSSFAutoFilter setAutoFilter(final CellRangeAddress range) {
        CTAutoFilter af = this.worksheet.getAutoFilter();
        if (af == null) {
            af = this.worksheet.addNewAutoFilter();
        }
        final CellRangeAddress norm = new CellRangeAddress(range.getFirstRow(), range.getLastRow(), range.getFirstColumn(), range.getLastColumn());
        final String ref = norm.formatAsString();
        af.setRef(ref);
        final XSSFWorkbook wb = this.getWorkbook();
        final int sheetIndex = this.getWorkbook().getSheetIndex(this);
        XSSFName name = wb.getBuiltInName("_xlnm._FilterDatabase", sheetIndex);
        if (name == null) {
            name = wb.createBuiltInName("_xlnm._FilterDatabase", sheetIndex);
            name.getCTName().setHidden(true);
            final CellReference r1 = new CellReference(this.getSheetName(), range.getFirstRow(), range.getFirstColumn(), true, true);
            final CellReference r2 = new CellReference(null, range.getLastRow(), range.getLastColumn(), true, true);
            final String fmla = r1.formatAsString() + ":" + r2.formatAsString();
            name.setRefersToFormula(fmla);
        }
        return new XSSFAutoFilter(this);
    }
    
    public XSSFTable createTable() {
        if (!this.worksheet.isSetTableParts()) {
            this.worksheet.addNewTableParts();
        }
        final CTTableParts tblParts = this.worksheet.getTableParts();
        final CTTablePart tbl = tblParts.addNewTablePart();
        final int tableNumber = this.getPackagePart().getPackage().getPartsByContentType(XSSFRelation.TABLE.getContentType()).size() + 1;
        final XSSFTable table = (XSSFTable)this.createRelationship(XSSFRelation.TABLE, XSSFFactory.getInstance(), tableNumber);
        tbl.setId(table.getPackageRelationship().getId());
        this.tables.put(tbl.getId(), table);
        return table;
    }
    
    public List<XSSFTable> getTables() {
        final List<XSSFTable> tableList = new ArrayList<XSSFTable>(this.tables.values());
        return tableList;
    }
    
    public XSSFSheetConditionalFormatting getSheetConditionalFormatting() {
        return new XSSFSheetConditionalFormatting(this);
    }
    
    public void setTabColor(final int colorIndex) {
        CTSheetPr pr = this.worksheet.getSheetPr();
        if (pr == null) {
            pr = this.worksheet.addNewSheetPr();
        }
        final CTColor color = CTColor.Factory.newInstance();
        color.setIndexed(colorIndex);
        pr.setTabColor(color);
    }
    
    static {
        logger = POILogFactory.getLogger(XSSFSheet.class);
    }
}
