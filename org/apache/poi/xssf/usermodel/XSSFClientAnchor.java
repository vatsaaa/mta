// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTMarker;
import org.apache.poi.ss.usermodel.ClientAnchor;

public final class XSSFClientAnchor extends XSSFAnchor implements ClientAnchor
{
    private int anchorType;
    private CTMarker cell1;
    private CTMarker cell2;
    
    public XSSFClientAnchor() {
        (this.cell1 = CTMarker.Factory.newInstance()).setCol(0);
        this.cell1.setColOff(0L);
        this.cell1.setRow(0);
        this.cell1.setRowOff(0L);
        (this.cell2 = CTMarker.Factory.newInstance()).setCol(0);
        this.cell2.setColOff(0L);
        this.cell2.setRow(0);
        this.cell2.setRowOff(0L);
    }
    
    public XSSFClientAnchor(final int dx1, final int dy1, final int dx2, final int dy2, final int col1, final int row1, final int col2, final int row2) {
        this();
        this.cell1.setCol(col1);
        this.cell1.setColOff(dx1);
        this.cell1.setRow(row1);
        this.cell1.setRowOff(dy1);
        this.cell2.setCol(col2);
        this.cell2.setColOff(dx2);
        this.cell2.setRow(row2);
        this.cell2.setRowOff(dy2);
    }
    
    protected XSSFClientAnchor(final CTMarker cell1, final CTMarker cell2) {
        this.cell1 = cell1;
        this.cell2 = cell2;
    }
    
    public short getCol1() {
        return (short)this.cell1.getCol();
    }
    
    public void setCol1(final int col1) {
        this.cell1.setCol(col1);
    }
    
    public short getCol2() {
        return (short)this.cell2.getCol();
    }
    
    public void setCol2(final int col2) {
        this.cell2.setCol(col2);
    }
    
    public int getRow1() {
        return this.cell1.getRow();
    }
    
    public void setRow1(final int row1) {
        this.cell1.setRow(row1);
    }
    
    public int getRow2() {
        return this.cell2.getRow();
    }
    
    public void setRow2(final int row2) {
        this.cell2.setRow(row2);
    }
    
    @Override
    public int getDx1() {
        return (int)this.cell1.getColOff();
    }
    
    @Override
    public void setDx1(final int dx1) {
        this.cell1.setColOff(dx1);
    }
    
    @Override
    public int getDy1() {
        return (int)this.cell1.getRowOff();
    }
    
    @Override
    public void setDy1(final int dy1) {
        this.cell1.setRowOff(dy1);
    }
    
    @Override
    public int getDy2() {
        return (int)this.cell2.getRowOff();
    }
    
    @Override
    public void setDy2(final int dy2) {
        this.cell2.setRowOff(dy2);
    }
    
    @Override
    public int getDx2() {
        return (int)this.cell2.getColOff();
    }
    
    @Override
    public void setDx2(final int dx2) {
        this.cell2.setColOff(dx2);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof XSSFClientAnchor)) {
            return false;
        }
        final XSSFClientAnchor anchor = (XSSFClientAnchor)o;
        return this.cell1.toString().equals(anchor.getFrom().toString()) && this.cell2.toString().equals(anchor.getTo().toString());
    }
    
    @Override
    public String toString() {
        return "from : " + this.cell1.toString() + "; to: " + this.cell2.toString();
    }
    
    @Internal
    public CTMarker getFrom() {
        return this.cell1;
    }
    
    protected void setFrom(final CTMarker from) {
        this.cell1 = from;
    }
    
    @Internal
    public CTMarker getTo() {
        return this.cell2;
    }
    
    protected void setTo(final CTMarker to) {
        this.cell2 = to;
    }
    
    public void setAnchorType(final int anchorType) {
        this.anchorType = anchorType;
    }
    
    public int getAnchorType() {
        return this.anchorType;
    }
    
    public boolean isSet() {
        return this.cell1.getCol() != 0 || this.cell2.getCol() != 0 || this.cell1.getRow() != 0 || this.cell2.getRow() != 0;
    }
}
