// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCalcPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorkbookProtection;
import java.util.Collection;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STSheetState;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.util.LinkedList;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheets;
import org.apache.poi.xssf.usermodel.helpers.XSSFFormulaUtils;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDefinedNames;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.formula.SheetNameFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDialogsheet;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet;
import org.apache.xmlbeans.XmlObject;
import java.io.ByteArrayInputStream;
import org.apache.poi.util.IOUtils;
import org.apache.poi.util.Internal;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.POIXMLProperties;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBookView;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBookViews;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorkbookPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDefinedName;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheet;
import java.util.Iterator;
import java.util.Map;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import java.util.ArrayList;
import org.apache.poi.POIXMLRelation;
import org.apache.poi.POIXMLDocumentPart;
import java.util.HashMap;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.WorkbookDocument;
import org.apache.poi.util.PackageHelper;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.POIXMLFactory;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.util.POILogger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.model.MapInfo;
import org.apache.poi.xssf.model.CalculationChain;
import org.apache.poi.xssf.model.IndexedUDFFinder;
import org.apache.poi.xssf.model.ThemesTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.model.SharedStringsTable;
import java.util.List;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorkbook;
import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.POIXMLDocument;

public class XSSFWorkbook extends POIXMLDocument implements Workbook, Iterable<XSSFSheet>
{
    private static final Pattern COMMA_PATTERN;
    public static final float DEFAULT_CHARACTER_WIDTH = 7.0017f;
    private static final int MAX_SENSITIVE_SHEET_NAME_LEN = 31;
    public static final int PICTURE_TYPE_GIF = 8;
    public static final int PICTURE_TYPE_TIFF = 9;
    public static final int PICTURE_TYPE_EPS = 10;
    public static final int PICTURE_TYPE_BMP = 11;
    public static final int PICTURE_TYPE_WPG = 12;
    private CTWorkbook workbook;
    private List<XSSFSheet> sheets;
    private List<XSSFName> namedRanges;
    private SharedStringsTable sharedStringSource;
    private StylesTable stylesSource;
    private ThemesTable theme;
    private IndexedUDFFinder _udfFinder;
    private CalculationChain calcChain;
    private MapInfo mapInfo;
    private XSSFDataFormat formatter;
    private Row.MissingCellPolicy _missingCellPolicy;
    private List<XSSFPictureData> pictures;
    private static POILogger logger;
    private XSSFCreationHelper _creationHelper;
    
    public XSSFWorkbook() {
        super(newPackage());
        this._udfFinder = new IndexedUDFFinder(new UDFFinder[] { UDFFinder.DEFAULT });
        this._missingCellPolicy = Row.RETURN_NULL_AND_BLANK;
        this.onWorkbookCreate();
    }
    
    public XSSFWorkbook(final OPCPackage pkg) throws IOException {
        super(pkg);
        this._udfFinder = new IndexedUDFFinder(new UDFFinder[] { UDFFinder.DEFAULT });
        this._missingCellPolicy = Row.RETURN_NULL_AND_BLANK;
        this.load(XSSFFactory.getInstance());
    }
    
    public XSSFWorkbook(final InputStream is) throws IOException {
        super(PackageHelper.open(is));
        this._udfFinder = new IndexedUDFFinder(new UDFFinder[] { UDFFinder.DEFAULT });
        this._missingCellPolicy = Row.RETURN_NULL_AND_BLANK;
        this.load(XSSFFactory.getInstance());
    }
    
    @Deprecated
    public XSSFWorkbook(final String path) throws IOException {
        this(POIXMLDocument.openPackage(path));
    }
    
    @Override
    protected void onDocumentRead() throws IOException {
        try {
            final WorkbookDocument doc = WorkbookDocument.Factory.parse(this.getPackagePart().getInputStream());
            this.workbook = doc.getWorkbook();
            final Map<String, XSSFSheet> shIdMap = new HashMap<String, XSSFSheet>();
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof SharedStringsTable) {
                    this.sharedStringSource = (SharedStringsTable)p;
                }
                else if (p instanceof StylesTable) {
                    this.stylesSource = (StylesTable)p;
                }
                else if (p instanceof ThemesTable) {
                    this.theme = (ThemesTable)p;
                }
                else if (p instanceof CalculationChain) {
                    this.calcChain = (CalculationChain)p;
                }
                else if (p instanceof MapInfo) {
                    this.mapInfo = (MapInfo)p;
                }
                else {
                    if (!(p instanceof XSSFSheet)) {
                        continue;
                    }
                    shIdMap.put(p.getPackageRelationship().getId(), (XSSFSheet)p);
                }
            }
            this.stylesSource.setTheme(this.theme);
            if (this.sharedStringSource == null) {
                this.sharedStringSource = (SharedStringsTable)this.createRelationship(XSSFRelation.SHARED_STRINGS, XSSFFactory.getInstance());
            }
            this.sheets = new ArrayList<XSSFSheet>(shIdMap.size());
            for (final CTSheet ctSheet : this.workbook.getSheets().getSheetArray()) {
                final XSSFSheet sh = shIdMap.get(ctSheet.getId());
                if (sh == null) {
                    XSSFWorkbook.logger.log(POILogger.WARN, "Sheet with name " + ctSheet.getName() + " and r:id " + ctSheet.getId() + " was defined, but didn't exist in package, skipping");
                }
                else {
                    sh.sheet = ctSheet;
                    sh.onDocumentRead();
                    this.sheets.add(sh);
                }
            }
            this.namedRanges = new ArrayList<XSSFName>();
            if (this.workbook.isSetDefinedNames()) {
                for (final CTDefinedName ctName : this.workbook.getDefinedNames().getDefinedNameArray()) {
                    this.namedRanges.add(new XSSFName(ctName, this));
                }
            }
        }
        catch (XmlException e) {
            throw new POIXMLException(e);
        }
    }
    
    private void onWorkbookCreate() {
        this.workbook = CTWorkbook.Factory.newInstance();
        final CTWorkbookPr workbookPr = this.workbook.addNewWorkbookPr();
        workbookPr.setDate1904(false);
        final CTBookViews bvs = this.workbook.addNewBookViews();
        final CTBookView bv = bvs.addNewWorkbookView();
        bv.setActiveTab(0L);
        this.workbook.addNewSheets();
        final POIXMLProperties.ExtendedProperties expProps = this.getProperties().getExtendedProperties();
        expProps.getUnderlyingProperties().setApplication("Apache POI");
        this.sharedStringSource = (SharedStringsTable)this.createRelationship(XSSFRelation.SHARED_STRINGS, XSSFFactory.getInstance());
        this.stylesSource = (StylesTable)this.createRelationship(XSSFRelation.STYLES, XSSFFactory.getInstance());
        this.namedRanges = new ArrayList<XSSFName>();
        this.sheets = new ArrayList<XSSFSheet>();
    }
    
    protected static OPCPackage newPackage() {
        try {
            final OPCPackage pkg = OPCPackage.create(new ByteArrayOutputStream());
            final PackagePartName corePartName = PackagingURIHelper.createPartName(XSSFRelation.WORKBOOK.getDefaultFileName());
            pkg.addRelationship(corePartName, TargetMode.INTERNAL, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument");
            pkg.createPart(corePartName, XSSFRelation.WORKBOOK.getContentType());
            pkg.getPackageProperties().setCreatorProperty("Apache POI");
            return pkg;
        }
        catch (Exception e) {
            throw new POIXMLException(e);
        }
    }
    
    @Internal
    public CTWorkbook getCTWorkbook() {
        return this.workbook;
    }
    
    public int addPicture(final byte[] pictureData, final int format) {
        final int imageNumber = this.getAllPictures().size() + 1;
        final XSSFPictureData img = (XSSFPictureData)this.createRelationship(XSSFPictureData.RELATIONS[format], XSSFFactory.getInstance(), imageNumber, true);
        try {
            final OutputStream out = img.getPackagePart().getOutputStream();
            out.write(pictureData);
            out.close();
        }
        catch (IOException e) {
            throw new POIXMLException(e);
        }
        this.pictures.add(img);
        return imageNumber - 1;
    }
    
    public int addPicture(final InputStream is, final int format) throws IOException {
        final int imageNumber = this.getAllPictures().size() + 1;
        final XSSFPictureData img = (XSSFPictureData)this.createRelationship(XSSFPictureData.RELATIONS[format], XSSFFactory.getInstance(), imageNumber, true);
        final OutputStream out = img.getPackagePart().getOutputStream();
        IOUtils.copy(is, out);
        out.close();
        this.pictures.add(img);
        return imageNumber - 1;
    }
    
    public XSSFSheet cloneSheet(final int sheetNum) {
        this.validateSheetIndex(sheetNum);
        final XSSFSheet srcSheet = this.sheets.get(sheetNum);
        final String srcName = srcSheet.getSheetName();
        final String clonedName = this.getUniqueSheetName(srcName);
        final XSSFSheet clonedSheet = this.createSheet(clonedName);
        try {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            srcSheet.write(out);
            clonedSheet.read(new ByteArrayInputStream(out.toByteArray()));
        }
        catch (IOException e) {
            throw new POIXMLException("Failed to clone sheet", e);
        }
        final CTWorksheet ct = clonedSheet.getCTWorksheet();
        if (ct.isSetLegacyDrawing()) {
            XSSFWorkbook.logger.log(POILogger.WARN, "Cloning sheets with comments is not yet supported.");
            ct.unsetLegacyDrawing();
        }
        if (ct.isSetPageSetup()) {
            XSSFWorkbook.logger.log(POILogger.WARN, "Cloning sheets with page setup is not yet supported.");
            ct.unsetPageSetup();
        }
        clonedSheet.setSelected(false);
        final List<POIXMLDocumentPart> rels = srcSheet.getRelations();
        XSSFDrawing dg = null;
        for (final POIXMLDocumentPart r : rels) {
            if (r instanceof XSSFDrawing) {
                dg = (XSSFDrawing)r;
            }
            else {
                final PackageRelationship rel = r.getPackageRelationship();
                clonedSheet.getPackagePart().addRelationship(rel.getTargetURI(), rel.getTargetMode(), rel.getRelationshipType());
                clonedSheet.addRelation(rel.getId(), r);
            }
        }
        if (dg != null) {
            if (ct.isSetDrawing()) {
                ct.unsetDrawing();
            }
            final XSSFDrawing clonedDg = clonedSheet.createDrawingPatriarch();
            clonedDg.getCTDrawing().set(dg.getCTDrawing());
            final List<POIXMLDocumentPart> srcRels = srcSheet.createDrawingPatriarch().getRelations();
            for (final POIXMLDocumentPart rel2 : srcRels) {
                final PackageRelationship relation = rel2.getPackageRelationship();
                clonedSheet.createDrawingPatriarch().getPackagePart().addRelationship(relation.getTargetURI(), relation.getTargetMode(), relation.getRelationshipType(), relation.getId());
            }
        }
        return clonedSheet;
    }
    
    private String getUniqueSheetName(final String srcName) {
        int uniqueIndex = 2;
        String baseName = srcName;
        final int bracketPos = srcName.lastIndexOf(40);
        if (bracketPos > 0 && srcName.endsWith(")")) {
            final String suffix = srcName.substring(bracketPos + 1, srcName.length() - ")".length());
            try {
                uniqueIndex = Integer.parseInt(suffix.trim());
                ++uniqueIndex;
                baseName = srcName.substring(0, bracketPos).trim();
            }
            catch (NumberFormatException ex) {}
        }
        String name;
        do {
            final String index = Integer.toString(uniqueIndex++);
            if (baseName.length() + index.length() + 2 < 31) {
                name = baseName + " (" + index + ")";
            }
            else {
                name = baseName.substring(0, 31 - index.length() - 2) + "(" + index + ")";
            }
        } while (this.getSheetIndex(name) != -1);
        return name;
    }
    
    public XSSFCellStyle createCellStyle() {
        return this.stylesSource.createCellStyle();
    }
    
    public XSSFDataFormat createDataFormat() {
        if (this.formatter == null) {
            this.formatter = new XSSFDataFormat(this.stylesSource);
        }
        return this.formatter;
    }
    
    public XSSFFont createFont() {
        final XSSFFont font = new XSSFFont();
        font.registerTo(this.stylesSource);
        return font;
    }
    
    public XSSFName createName() {
        final CTDefinedName ctName = CTDefinedName.Factory.newInstance();
        ctName.setName("");
        final XSSFName name = new XSSFName(ctName, this);
        this.namedRanges.add(name);
        return name;
    }
    
    public XSSFSheet createSheet() {
        String sheetname = "Sheet" + this.sheets.size();
        for (int idx = 0; this.getSheet(sheetname) != null; sheetname = "Sheet" + idx, ++idx) {}
        return this.createSheet(sheetname);
    }
    
    public XSSFSheet createSheet(String sheetname) {
        if (sheetname == null) {
            throw new IllegalArgumentException("sheetName must not be null");
        }
        if (this.containsSheet(sheetname, this.sheets.size())) {
            throw new IllegalArgumentException("The workbook already contains a sheet of this name");
        }
        if (sheetname.length() > 31) {
            sheetname = sheetname.substring(0, 31);
        }
        WorkbookUtil.validateSheetName(sheetname);
        final CTSheet sheet = this.addSheet(sheetname);
        int sheetNumber = 1;
        for (final XSSFSheet sh : this.sheets) {
            sheetNumber = (int)Math.max(sh.sheet.getSheetId() + 1L, sheetNumber);
        }
        final XSSFSheet wrapper = (XSSFSheet)this.createRelationship(XSSFRelation.WORKSHEET, XSSFFactory.getInstance(), sheetNumber);
        (wrapper.sheet = sheet).setId(wrapper.getPackageRelationship().getId());
        sheet.setSheetId(sheetNumber);
        if (this.sheets.size() == 0) {
            wrapper.setSelected(true);
        }
        this.sheets.add(wrapper);
        return wrapper;
    }
    
    protected XSSFDialogsheet createDialogsheet(final String sheetname, final CTDialogsheet dialogsheet) {
        final XSSFSheet sheet = this.createSheet(sheetname);
        return new XSSFDialogsheet(sheet);
    }
    
    private CTSheet addSheet(final String sheetname) {
        final CTSheet sheet = this.workbook.getSheets().addNewSheet();
        sheet.setName(sheetname);
        return sheet;
    }
    
    public XSSFFont findFont(final short boldWeight, final short color, final short fontHeight, final String name, final boolean italic, final boolean strikeout, final short typeOffset, final byte underline) {
        return this.stylesSource.findFont(boldWeight, color, fontHeight, name, italic, strikeout, typeOffset, underline);
    }
    
    public int getActiveSheetIndex() {
        return (int)this.workbook.getBookViews().getWorkbookViewArray(0).getActiveTab();
    }
    
    public List<XSSFPictureData> getAllPictures() {
        if (this.pictures == null) {
            this.pictures = new ArrayList<XSSFPictureData>();
            for (final XSSFSheet sh : this.sheets) {
                for (final POIXMLDocumentPart dr : sh.getRelations()) {
                    if (dr instanceof XSSFDrawing) {
                        for (final POIXMLDocumentPart img : dr.getRelations()) {
                            if (img instanceof XSSFPictureData) {
                                this.pictures.add((XSSFPictureData)img);
                            }
                        }
                    }
                }
            }
        }
        return this.pictures;
    }
    
    public XSSFCellStyle getCellStyleAt(final short idx) {
        return this.stylesSource.getStyleAt(idx);
    }
    
    public XSSFFont getFontAt(final short idx) {
        return this.stylesSource.getFontAt(idx);
    }
    
    public XSSFName getName(final String name) {
        final int nameIndex = this.getNameIndex(name);
        if (nameIndex < 0) {
            return null;
        }
        return this.namedRanges.get(nameIndex);
    }
    
    public XSSFName getNameAt(final int nameIndex) {
        final int nNames = this.namedRanges.size();
        if (nNames < 1) {
            throw new IllegalStateException("There are no defined names in this workbook");
        }
        if (nameIndex < 0 || nameIndex > nNames) {
            throw new IllegalArgumentException("Specified name index " + nameIndex + " is outside the allowable range (0.." + (nNames - 1) + ").");
        }
        return this.namedRanges.get(nameIndex);
    }
    
    public int getNameIndex(final String name) {
        int i = 0;
        for (final XSSFName nr : this.namedRanges) {
            if (nr.getNameName().equals(name)) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public short getNumCellStyles() {
        return (short)this.stylesSource.getNumCellStyles();
    }
    
    public short getNumberOfFonts() {
        return (short)this.stylesSource.getFonts().size();
    }
    
    public int getNumberOfNames() {
        return this.namedRanges.size();
    }
    
    public int getNumberOfSheets() {
        return this.sheets.size();
    }
    
    public String getPrintArea(final int sheetIndex) {
        final XSSFName name = this.getBuiltInName("_xlnm.Print_Area", sheetIndex);
        if (name == null) {
            return null;
        }
        return name.getRefersToFormula();
    }
    
    public XSSFSheet getSheet(final String name) {
        for (final XSSFSheet sheet : this.sheets) {
            if (name.equalsIgnoreCase(sheet.getSheetName())) {
                return sheet;
            }
        }
        return null;
    }
    
    public XSSFSheet getSheetAt(final int index) {
        this.validateSheetIndex(index);
        return this.sheets.get(index);
    }
    
    public int getSheetIndex(final String name) {
        for (int i = 0; i < this.sheets.size(); ++i) {
            final XSSFSheet sheet = this.sheets.get(i);
            if (name.equalsIgnoreCase(sheet.getSheetName())) {
                return i;
            }
        }
        return -1;
    }
    
    public int getSheetIndex(final Sheet sheet) {
        int idx = 0;
        for (final XSSFSheet sh : this.sheets) {
            if (sh == sheet) {
                return idx;
            }
            ++idx;
        }
        return -1;
    }
    
    public String getSheetName(final int sheetIx) {
        this.validateSheetIndex(sheetIx);
        return this.sheets.get(sheetIx).getSheetName();
    }
    
    public Iterator<XSSFSheet> iterator() {
        return this.sheets.iterator();
    }
    
    public boolean isMacroEnabled() {
        return this.getPackagePart().getContentType().equals(XSSFRelation.MACROS_WORKBOOK.getContentType());
    }
    
    public void removeName(final int nameIndex) {
        this.namedRanges.remove(nameIndex);
    }
    
    public void removeName(final String name) {
        for (int i = 0; i < this.namedRanges.size(); ++i) {
            final XSSFName nm = this.namedRanges.get(i);
            if (nm.getNameName().equalsIgnoreCase(name)) {
                this.removeName(i);
                return;
            }
        }
        throw new IllegalArgumentException("Named range was not found: " + name);
    }
    
    public void removePrintArea(final int sheetIndex) {
        int cont = 0;
        for (final XSSFName name : this.namedRanges) {
            if (name.getNameName().equals("_xlnm.Print_Area") && name.getSheetIndex() == sheetIndex) {
                this.namedRanges.remove(cont);
                break;
            }
            ++cont;
        }
    }
    
    public void removeSheetAt(final int index) {
        this.validateSheetIndex(index);
        this.onSheetDelete(index);
        final XSSFSheet sheet = this.getSheetAt(index);
        this.removeRelation(sheet);
        this.sheets.remove(index);
    }
    
    private void onSheetDelete(final int index) {
        this.workbook.getSheets().removeSheet(index);
        if (this.calcChain != null) {
            this.removeRelation(this.calcChain);
            this.calcChain = null;
        }
        final Iterator<XSSFName> it = this.namedRanges.iterator();
        while (it.hasNext()) {
            final XSSFName nm = it.next();
            final CTDefinedName ct = nm.getCTName();
            if (!ct.isSetLocalSheetId()) {
                continue;
            }
            if (ct.getLocalSheetId() == index) {
                it.remove();
            }
            else {
                if (ct.getLocalSheetId() <= index) {
                    continue;
                }
                ct.setLocalSheetId(ct.getLocalSheetId() - 1L);
            }
        }
    }
    
    public Row.MissingCellPolicy getMissingCellPolicy() {
        return this._missingCellPolicy;
    }
    
    public void setMissingCellPolicy(final Row.MissingCellPolicy missingCellPolicy) {
        this._missingCellPolicy = missingCellPolicy;
    }
    
    public void setActiveSheet(final int index) {
        this.validateSheetIndex(index);
        for (final CTBookView arrayBook : this.workbook.getBookViews().getWorkbookViewArray()) {
            arrayBook.setActiveTab(index);
        }
    }
    
    private void validateSheetIndex(final int index) {
        final int lastSheetIx = this.sheets.size() - 1;
        if (index < 0 || index > lastSheetIx) {
            throw new IllegalArgumentException("Sheet index (" + index + ") is out of range (0.." + lastSheetIx + ")");
        }
    }
    
    public int getFirstVisibleTab() {
        final CTBookViews bookViews = this.workbook.getBookViews();
        final CTBookView bookView = bookViews.getWorkbookViewArray(0);
        return (short)bookView.getActiveTab();
    }
    
    public void setFirstVisibleTab(final int index) {
        final CTBookViews bookViews = this.workbook.getBookViews();
        final CTBookView bookView = bookViews.getWorkbookViewArray(0);
        bookView.setActiveTab(index);
    }
    
    public void setPrintArea(final int sheetIndex, final String reference) {
        XSSFName name = this.getBuiltInName("_xlnm.Print_Area", sheetIndex);
        if (name == null) {
            name = this.createBuiltInName("_xlnm.Print_Area", sheetIndex);
        }
        final String[] parts = XSSFWorkbook.COMMA_PATTERN.split(reference);
        final StringBuffer sb = new StringBuffer(32);
        for (int i = 0; i < parts.length; ++i) {
            if (i > 0) {
                sb.append(",");
            }
            SheetNameFormatter.appendFormat(sb, this.getSheetName(sheetIndex));
            sb.append("!");
            sb.append(parts[i]);
        }
        name.setRefersToFormula(sb.toString());
    }
    
    public void setPrintArea(final int sheetIndex, final int startColumn, final int endColumn, final int startRow, final int endRow) {
        final String reference = getReferencePrintArea(this.getSheetName(sheetIndex), startColumn, endColumn, startRow, endRow);
        this.setPrintArea(sheetIndex, reference);
    }
    
    public void setRepeatingRowsAndColumns(final int sheetIndex, final int startColumn, final int endColumn, final int startRow, final int endRow) {
        if ((startColumn == -1 && endColumn != -1) || startColumn < -1 || endColumn < -1 || startColumn > endColumn) {
            throw new IllegalArgumentException("Invalid column range specification");
        }
        if ((startRow == -1 && endRow != -1) || startRow < -1 || endRow < -1 || startRow > endRow) {
            throw new IllegalArgumentException("Invalid row range specification");
        }
        final XSSFSheet sheet = this.getSheetAt(sheetIndex);
        final boolean removingRange = startColumn == -1 && endColumn == -1 && startRow == -1 && endRow == -1;
        XSSFName name = this.getBuiltInName("_xlnm.Print_Titles", sheetIndex);
        if (removingRange) {
            if (name != null) {
                this.namedRanges.remove(name);
            }
            return;
        }
        if (name == null) {
            name = this.createBuiltInName("_xlnm.Print_Titles", sheetIndex);
        }
        final String reference = getReferenceBuiltInRecord(name.getSheetName(), startColumn, endColumn, startRow, endRow);
        name.setRefersToFormula(reference);
        final CTWorksheet ctSheet = sheet.getCTWorksheet();
        if (!ctSheet.isSetPageSetup() || !ctSheet.isSetPageMargins()) {
            final XSSFPrintSetup printSetup = sheet.getPrintSetup();
            printSetup.setValidSettings(false);
        }
    }
    
    private static String getReferenceBuiltInRecord(final String sheetName, final int startC, final int endC, final int startR, final int endR) {
        final CellReference colRef = new CellReference(sheetName, 0, startC, true, true);
        final CellReference colRef2 = new CellReference(sheetName, 0, endC, true, true);
        final String escapedName = SheetNameFormatter.format(sheetName);
        String c;
        if (startC == -1 && endC == -1) {
            c = "";
        }
        else {
            c = escapedName + "!$" + colRef.getCellRefParts()[2] + ":$" + colRef2.getCellRefParts()[2];
        }
        final CellReference rowRef = new CellReference(sheetName, startR, 0, true, true);
        final CellReference rowRef2 = new CellReference(sheetName, endR, 0, true, true);
        String r = "";
        if (startR == -1 && endR == -1) {
            r = "";
        }
        else if (!rowRef.getCellRefParts()[1].equals("0") && !rowRef2.getCellRefParts()[1].equals("0")) {
            r = escapedName + "!$" + rowRef.getCellRefParts()[1] + ":$" + rowRef2.getCellRefParts()[1];
        }
        final StringBuffer rng = new StringBuffer();
        rng.append(c);
        if (rng.length() > 0 && r.length() > 0) {
            rng.append(',');
        }
        rng.append(r);
        return rng.toString();
    }
    
    private static String getReferencePrintArea(final String sheetName, final int startC, final int endC, final int startR, final int endR) {
        final CellReference colRef = new CellReference(sheetName, startR, startC, true, true);
        final CellReference colRef2 = new CellReference(sheetName, endR, endC, true, true);
        return "$" + colRef.getCellRefParts()[2] + "$" + colRef.getCellRefParts()[1] + ":$" + colRef2.getCellRefParts()[2] + "$" + colRef2.getCellRefParts()[1];
    }
    
    XSSFName getBuiltInName(final String builtInCode, final int sheetNumber) {
        for (final XSSFName name : this.namedRanges) {
            if (name.getNameName().equalsIgnoreCase(builtInCode) && name.getSheetIndex() == sheetNumber) {
                return name;
            }
        }
        return null;
    }
    
    XSSFName createBuiltInName(final String builtInName, final int sheetNumber) {
        this.validateSheetIndex(sheetNumber);
        final CTDefinedNames names = (this.workbook.getDefinedNames() == null) ? this.workbook.addNewDefinedNames() : this.workbook.getDefinedNames();
        final CTDefinedName nameRecord = names.addNewDefinedName();
        nameRecord.setName(builtInName);
        nameRecord.setLocalSheetId(sheetNumber);
        final XSSFName name = new XSSFName(nameRecord, this);
        for (final XSSFName nr : this.namedRanges) {
            if (nr.equals(name)) {
                throw new POIXMLException("Builtin (" + builtInName + ") already exists for sheet (" + sheetNumber + ")");
            }
        }
        this.namedRanges.add(name);
        return name;
    }
    
    public void setSelectedTab(final int index) {
        for (int i = 0; i < this.sheets.size(); ++i) {
            final XSSFSheet sheet = this.sheets.get(i);
            sheet.setSelected(i == index);
        }
    }
    
    public void setSheetName(final int sheetIndex, String sheetname) {
        this.validateSheetIndex(sheetIndex);
        if (sheetname != null && sheetname.length() > 31) {
            sheetname = sheetname.substring(0, 31);
        }
        WorkbookUtil.validateSheetName(sheetname);
        if (this.containsSheet(sheetname, sheetIndex)) {
            throw new IllegalArgumentException("The workbook already contains a sheet of this name");
        }
        final XSSFFormulaUtils utils = new XSSFFormulaUtils(this);
        utils.updateSheetName(sheetIndex, sheetname);
        this.workbook.getSheets().getSheetArray(sheetIndex).setName(sheetname);
    }
    
    public void setSheetOrder(final String sheetname, final int pos) {
        final int idx = this.getSheetIndex(sheetname);
        this.sheets.add(pos, this.sheets.remove(idx));
        final CTSheets ct = this.workbook.getSheets();
        final XmlObject cts = ct.getSheetArray(idx).copy();
        this.workbook.getSheets().removeSheet(idx);
        final CTSheet newcts = ct.insertNewSheet(pos);
        newcts.set(cts);
        for (int i = 0; i < this.sheets.size(); ++i) {
            this.sheets.get(i).sheet = ct.getSheetArray(i);
        }
    }
    
    private void saveNamedRanges() {
        if (this.namedRanges.size() > 0) {
            final CTDefinedNames names = CTDefinedNames.Factory.newInstance();
            final CTDefinedName[] nr = new CTDefinedName[this.namedRanges.size()];
            int i = 0;
            for (final XSSFName name : this.namedRanges) {
                nr[i] = name.getCTName();
                ++i;
            }
            names.setDefinedNameArray(nr);
            this.workbook.setDefinedNames(names);
        }
        else if (this.workbook.isSetDefinedNames()) {
            this.workbook.unsetDefinedNames();
        }
    }
    
    private void saveCalculationChain() {
        if (this.calcChain != null) {
            final int count = this.calcChain.getCTCalcChain().sizeOfCArray();
            if (count == 0) {
                this.removeRelation(this.calcChain);
                this.calcChain = null;
            }
        }
    }
    
    @Override
    protected void commit() throws IOException {
        this.saveNamedRanges();
        this.saveCalculationChain();
        final XmlOptions xmlOptions = new XmlOptions(XSSFWorkbook.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTWorkbook.type.getName().getNamespaceURI(), "workbook"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put(STRelationshipId.type.getName().getNamespaceURI(), "r");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.workbook.save(out, xmlOptions);
        out.close();
    }
    
    @Internal
    public SharedStringsTable getSharedStringSource() {
        return this.sharedStringSource;
    }
    
    public StylesTable getStylesSource() {
        return this.stylesSource;
    }
    
    public ThemesTable getTheme() {
        return this.theme;
    }
    
    public XSSFCreationHelper getCreationHelper() {
        if (this._creationHelper == null) {
            this._creationHelper = new XSSFCreationHelper(this);
        }
        return this._creationHelper;
    }
    
    private boolean containsSheet(String name, final int excludeSheetIdx) {
        final CTSheet[] ctSheetArray = this.workbook.getSheets().getSheetArray();
        if (name.length() > 31) {
            name = name.substring(0, 31);
        }
        for (int i = 0; i < ctSheetArray.length; ++i) {
            String ctName = ctSheetArray[i].getName();
            if (ctName.length() > 31) {
                ctName = ctName.substring(0, 31);
            }
            if (excludeSheetIdx != i && name.equalsIgnoreCase(ctName)) {
                return true;
            }
        }
        return false;
    }
    
    protected boolean isDate1904() {
        final CTWorkbookPr workbookPr = this.workbook.getWorkbookPr();
        return workbookPr != null && workbookPr.getDate1904();
    }
    
    @Override
    public List<PackagePart> getAllEmbedds() throws OpenXML4JException {
        final List<PackagePart> embedds = new LinkedList<PackagePart>();
        for (final XSSFSheet sheet : this.sheets) {
            for (final PackageRelationship rel : sheet.getPackagePart().getRelationshipsByType(XSSFRelation.OLEEMBEDDINGS.getRelation())) {
                embedds.add(sheet.getPackagePart().getRelatedPart(rel));
            }
            for (final PackageRelationship rel : sheet.getPackagePart().getRelationshipsByType(XSSFRelation.PACKEMBEDDINGS.getRelation())) {
                embedds.add(sheet.getPackagePart().getRelatedPart(rel));
            }
        }
        return embedds;
    }
    
    public boolean isHidden() {
        throw new RuntimeException("Not implemented yet");
    }
    
    public void setHidden(final boolean hiddenFlag) {
        throw new RuntimeException("Not implemented yet");
    }
    
    public boolean isSheetHidden(final int sheetIx) {
        this.validateSheetIndex(sheetIx);
        final CTSheet ctSheet = this.sheets.get(sheetIx).sheet;
        return ctSheet.getState() == STSheetState.HIDDEN;
    }
    
    public boolean isSheetVeryHidden(final int sheetIx) {
        this.validateSheetIndex(sheetIx);
        final CTSheet ctSheet = this.sheets.get(sheetIx).sheet;
        return ctSheet.getState() == STSheetState.VERY_HIDDEN;
    }
    
    public void setSheetHidden(final int sheetIx, final boolean hidden) {
        this.setSheetHidden(sheetIx, hidden ? 1 : 0);
    }
    
    public void setSheetHidden(final int sheetIx, final int state) {
        this.validateSheetIndex(sheetIx);
        WorkbookUtil.validateSheetState(state);
        final CTSheet ctSheet = this.sheets.get(sheetIx).sheet;
        ctSheet.setState(STSheetState.Enum.forInt(state + 1));
    }
    
    protected void onDeleteFormula(final XSSFCell cell) {
        if (this.calcChain != null) {
            final int sheetId = (int)cell.getSheet().sheet.getSheetId();
            this.calcChain.removeItem(sheetId, cell.getReference());
        }
    }
    
    @Internal
    public CalculationChain getCalculationChain() {
        return this.calcChain;
    }
    
    public Collection<XSSFMap> getCustomXMLMappings() {
        return (this.mapInfo == null) ? new ArrayList<XSSFMap>() : this.mapInfo.getAllXSSFMaps();
    }
    
    @Internal
    public MapInfo getMapInfo() {
        return this.mapInfo;
    }
    
    public boolean isStructureLocked() {
        return this.workbookProtectionPresent() && this.workbook.getWorkbookProtection().getLockStructure();
    }
    
    public boolean isWindowsLocked() {
        return this.workbookProtectionPresent() && this.workbook.getWorkbookProtection().getLockWindows();
    }
    
    public boolean isRevisionLocked() {
        return this.workbookProtectionPresent() && this.workbook.getWorkbookProtection().getLockRevision();
    }
    
    public void lockStructure() {
        this.createProtectionFieldIfNotPresent();
        this.workbook.getWorkbookProtection().setLockStructure(true);
    }
    
    public void unLockStructure() {
        this.createProtectionFieldIfNotPresent();
        this.workbook.getWorkbookProtection().setLockStructure(false);
    }
    
    public void lockWindows() {
        this.createProtectionFieldIfNotPresent();
        this.workbook.getWorkbookProtection().setLockWindows(true);
    }
    
    public void unLockWindows() {
        this.createProtectionFieldIfNotPresent();
        this.workbook.getWorkbookProtection().setLockWindows(false);
    }
    
    public void lockRevision() {
        this.createProtectionFieldIfNotPresent();
        this.workbook.getWorkbookProtection().setLockRevision(true);
    }
    
    public void unLockRevision() {
        this.createProtectionFieldIfNotPresent();
        this.workbook.getWorkbookProtection().setLockRevision(false);
    }
    
    private boolean workbookProtectionPresent() {
        return this.workbook.getWorkbookProtection() != null;
    }
    
    private void createProtectionFieldIfNotPresent() {
        if (this.workbook.getWorkbookProtection() == null) {
            this.workbook.setWorkbookProtection(CTWorkbookProtection.Factory.newInstance());
        }
    }
    
    UDFFinder getUDFFinder() {
        return this._udfFinder;
    }
    
    public void addToolPack(final UDFFinder toopack) {
        this._udfFinder.add(toopack);
    }
    
    public void setForceFormulaRecalculation(final boolean value) {
        final CTWorkbook ctWorkbook = this.getCTWorkbook();
        final CTCalcPr calcPr = ctWorkbook.isSetCalcPr() ? ctWorkbook.getCalcPr() : ctWorkbook.addNewCalcPr();
        calcPr.setCalcId(0L);
    }
    
    public boolean getForceFormulaRecalculation() {
        final CTWorkbook ctWorkbook = this.getCTWorkbook();
        final CTCalcPr calcPr = ctWorkbook.getCalcPr();
        return calcPr != null && calcPr.getCalcId() != 0L;
    }
    
    static {
        COMMA_PATTERN = Pattern.compile(",");
        XSSFWorkbook.logger = POILogFactory.getLogger(XSSFWorkbook.class);
    }
}
