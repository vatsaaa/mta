// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.FormulaParser;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDefinedName;
import org.apache.poi.ss.usermodel.Name;

public final class XSSFName implements Name
{
    public static final String BUILTIN_PRINT_AREA = "_xlnm.Print_Area";
    public static final String BUILTIN_PRINT_TITLE = "_xlnm.Print_Titles";
    public static final String BUILTIN_CRITERIA = "_xlnm.Criteria:";
    public static final String BUILTIN_EXTRACT = "_xlnm.Extract:";
    public static final String BUILTIN_FILTER_DB = "_xlnm._FilterDatabase";
    public static final String BUILTIN_CONSOLIDATE_AREA = "_xlnm.Consolidate_Area";
    public static final String BUILTIN_DATABASE = "_xlnm.Database";
    public static final String BUILTIN_SHEET_TITLE = "_xlnm.Sheet_Title";
    private XSSFWorkbook _workbook;
    private CTDefinedName _ctName;
    
    protected XSSFName(final CTDefinedName name, final XSSFWorkbook workbook) {
        this._workbook = workbook;
        this._ctName = name;
    }
    
    protected CTDefinedName getCTName() {
        return this._ctName;
    }
    
    public String getNameName() {
        return this._ctName.getName();
    }
    
    public void setNameName(final String name) {
        validateName(name);
        final int sheetIndex = this.getSheetIndex();
        for (int i = 0; i < this._workbook.getNumberOfNames(); ++i) {
            final XSSFName nm = this._workbook.getNameAt(i);
            if (nm != this && name.equalsIgnoreCase(nm.getNameName()) && sheetIndex == nm.getSheetIndex()) {
                final String msg = "The " + ((sheetIndex == -1) ? "workbook" : "sheet") + " already contains this name: " + name;
                throw new IllegalArgumentException(msg);
            }
        }
        this._ctName.setName(name);
    }
    
    public String getRefersToFormula() {
        final String result = this._ctName.getStringValue();
        if (result == null || result.length() < 1) {
            return null;
        }
        return result;
    }
    
    public void setRefersToFormula(final String formulaText) {
        final XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create(this._workbook);
        FormulaParser.parse(formulaText, fpb, 4, this.getSheetIndex());
        this._ctName.setStringValue(formulaText);
    }
    
    public boolean isDeleted() {
        final String formulaText = this.getRefersToFormula();
        if (formulaText == null) {
            return false;
        }
        final XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create(this._workbook);
        final Ptg[] ptgs = FormulaParser.parse(formulaText, fpb, 4, this.getSheetIndex());
        return Ptg.doesFormulaReferToDeletedCell(ptgs);
    }
    
    public void setSheetIndex(final int index) {
        final int lastSheetIx = this._workbook.getNumberOfSheets() - 1;
        if (index < -1 || index > lastSheetIx) {
            throw new IllegalArgumentException("Sheet index (" + index + ") is out of range" + ((lastSheetIx == -1) ? "" : (" (0.." + lastSheetIx + ")")));
        }
        if (index == -1) {
            if (this._ctName.isSetLocalSheetId()) {
                this._ctName.unsetLocalSheetId();
            }
        }
        else {
            this._ctName.setLocalSheetId(index);
        }
    }
    
    public int getSheetIndex() {
        return this._ctName.isSetLocalSheetId() ? ((int)this._ctName.getLocalSheetId()) : -1;
    }
    
    public void setFunction(final boolean value) {
        this._ctName.setFunction(value);
    }
    
    public boolean getFunction() {
        return this._ctName.getFunction();
    }
    
    public void setFunctionGroupId(final int functionGroupId) {
        this._ctName.setFunctionGroupId(functionGroupId);
    }
    
    public int getFunctionGroupId() {
        return (int)this._ctName.getFunctionGroupId();
    }
    
    public String getSheetName() {
        if (this._ctName.isSetLocalSheetId()) {
            final int sheetId = (int)this._ctName.getLocalSheetId();
            return this._workbook.getSheetName(sheetId);
        }
        final String ref = this.getRefersToFormula();
        final AreaReference areaRef = new AreaReference(ref);
        return areaRef.getFirstCell().getSheetName();
    }
    
    public boolean isFunctionName() {
        return this.getFunction();
    }
    
    public String getComment() {
        return this._ctName.getComment();
    }
    
    public void setComment(final String comment) {
        this._ctName.setComment(comment);
    }
    
    @Override
    public int hashCode() {
        return this._ctName.toString().hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof XSSFName)) {
            return false;
        }
        final XSSFName cf = (XSSFName)o;
        return this._ctName.toString().equals(cf.getCTName().toString());
    }
    
    private static void validateName(final String name) {
        if (name.length() == 0) {
            throw new IllegalArgumentException("Name cannot be blank");
        }
        final char c = name.charAt(0);
        if ((c != '_' && !Character.isLetter(c)) || name.indexOf(32) != -1) {
            throw new IllegalArgumentException("Invalid name: '" + name + "'; Names must begin with a letter or underscore and not contain spaces");
        }
    }
}
