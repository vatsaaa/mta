// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.RichTextString;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRst;
import java.math.BigInteger;
import org.apache.poi.ss.util.CellReference;
import schemasMicrosoftComVml.CTShape;
import org.apache.poi.xssf.model.CommentsTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTComment;
import org.apache.poi.ss.usermodel.Comment;

public class XSSFComment implements Comment
{
    private final CTComment _comment;
    private final CommentsTable _comments;
    private final CTShape _vmlShape;
    private XSSFRichTextString _str;
    
    public XSSFComment(final CommentsTable comments, final CTComment comment, final CTShape vmlShape) {
        this._comment = comment;
        this._comments = comments;
        this._vmlShape = vmlShape;
    }
    
    public String getAuthor() {
        return this._comments.getAuthor((int)this._comment.getAuthorId());
    }
    
    public void setAuthor(final String author) {
        this._comment.setAuthorId(this._comments.findAuthor(author));
    }
    
    public int getColumn() {
        return new CellReference(this._comment.getRef()).getCol();
    }
    
    public int getRow() {
        return new CellReference(this._comment.getRef()).getRow();
    }
    
    public boolean isVisible() {
        boolean visible = false;
        if (this._vmlShape != null) {
            final String style = this._vmlShape.getStyle();
            visible = (style != null && style.indexOf("visibility:visible") != -1);
        }
        return visible;
    }
    
    public void setVisible(final boolean visible) {
        if (this._vmlShape != null) {
            String style;
            if (visible) {
                style = "position:absolute;visibility:visible";
            }
            else {
                style = "position:absolute;visibility:hidden";
            }
            this._vmlShape.setStyle(style);
        }
    }
    
    public void setColumn(final int col) {
        final String oldRef = this._comment.getRef();
        final CellReference ref = new CellReference(this.getRow(), col);
        this._comment.setRef(ref.formatAsString());
        this._comments.referenceUpdated(oldRef, this._comment);
        if (this._vmlShape != null) {
            this._vmlShape.getClientDataArray(0).setColumnArray(new BigInteger[] { new BigInteger(String.valueOf(col)) });
            this._vmlShape.getClientDataList().toString();
        }
    }
    
    public void setRow(final int row) {
        final String oldRef = this._comment.getRef();
        final String newRef = new CellReference(row, this.getColumn()).formatAsString();
        this._comment.setRef(newRef);
        this._comments.referenceUpdated(oldRef, this._comment);
        if (this._vmlShape != null) {
            this._vmlShape.getClientDataArray(0).setRowArray(0, new BigInteger(String.valueOf(row)));
        }
    }
    
    public XSSFRichTextString getString() {
        if (this._str == null) {
            final CTRst rst = this._comment.getText();
            if (rst != null) {
                this._str = new XSSFRichTextString(this._comment.getText());
            }
        }
        return this._str;
    }
    
    public void setString(final RichTextString string) {
        if (!(string instanceof XSSFRichTextString)) {
            throw new IllegalArgumentException("Only XSSFRichTextString argument is supported");
        }
        this._str = (XSSFRichTextString)string;
        this._comment.setText(this._str.getCTRst());
    }
    
    public void setString(final String string) {
        this.setString(new XSSFRichTextString(string));
    }
    
    protected CTComment getCTComment() {
        return this._comment;
    }
    
    protected CTShape getCTShape() {
        return this._vmlShape;
    }
}
