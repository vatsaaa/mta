// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorderPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STBorderStyle;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorder;
import org.apache.poi.ss.usermodel.BorderFormatting;

public class XSSFBorderFormatting implements BorderFormatting
{
    CTBorder _border;
    
    XSSFBorderFormatting(final CTBorder border) {
        this._border = border;
    }
    
    public short getBorderBottom() {
        final STBorderStyle.Enum ptrn = this._border.isSetBottom() ? this._border.getBottom().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public short getBorderDiagonal() {
        final STBorderStyle.Enum ptrn = this._border.isSetDiagonal() ? this._border.getDiagonal().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public short getBorderLeft() {
        final STBorderStyle.Enum ptrn = this._border.isSetLeft() ? this._border.getLeft().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public short getBorderRight() {
        final STBorderStyle.Enum ptrn = this._border.isSetRight() ? this._border.getRight().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public short getBorderTop() {
        final STBorderStyle.Enum ptrn = this._border.isSetTop() ? this._border.getTop().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public short getBottomBorderColor() {
        if (!this._border.isSetBottom()) {
            return 0;
        }
        final CTBorderPr pr = this._border.getBottom();
        return (short)pr.getColor().getIndexed();
    }
    
    public short getDiagonalBorderColor() {
        if (!this._border.isSetDiagonal()) {
            return 0;
        }
        final CTBorderPr pr = this._border.getDiagonal();
        return (short)pr.getColor().getIndexed();
    }
    
    public short getLeftBorderColor() {
        if (!this._border.isSetLeft()) {
            return 0;
        }
        final CTBorderPr pr = this._border.getLeft();
        return (short)pr.getColor().getIndexed();
    }
    
    public short getRightBorderColor() {
        if (!this._border.isSetRight()) {
            return 0;
        }
        final CTBorderPr pr = this._border.getRight();
        return (short)pr.getColor().getIndexed();
    }
    
    public short getTopBorderColor() {
        if (!this._border.isSetTop()) {
            return 0;
        }
        final CTBorderPr pr = this._border.getTop();
        return (short)pr.getColor().getIndexed();
    }
    
    public void setBorderBottom(final short border) {
        final CTBorderPr pr = this._border.isSetBottom() ? this._border.getBottom() : this._border.addNewBottom();
        if (border == 0) {
            this._border.unsetBottom();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
    }
    
    public void setBorderDiagonal(final short border) {
        final CTBorderPr pr = this._border.isSetDiagonal() ? this._border.getDiagonal() : this._border.addNewDiagonal();
        if (border == 0) {
            this._border.unsetDiagonal();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
    }
    
    public void setBorderLeft(final short border) {
        final CTBorderPr pr = this._border.isSetLeft() ? this._border.getLeft() : this._border.addNewLeft();
        if (border == 0) {
            this._border.unsetLeft();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
    }
    
    public void setBorderRight(final short border) {
        final CTBorderPr pr = this._border.isSetRight() ? this._border.getRight() : this._border.addNewRight();
        if (border == 0) {
            this._border.unsetRight();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
    }
    
    public void setBorderTop(final short border) {
        final CTBorderPr pr = this._border.isSetTop() ? this._border.getTop() : this._border.addNewTop();
        if (border == 0) {
            this._border.unsetTop();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
    }
    
    public void setBottomBorderColor(final short color) {
        final CTBorderPr pr = this._border.isSetBottom() ? this._border.getBottom() : this._border.addNewBottom();
        final CTColor ctColor = CTColor.Factory.newInstance();
        ctColor.setIndexed(color);
        pr.setColor(ctColor);
    }
    
    public void setDiagonalBorderColor(final short color) {
        final CTBorderPr pr = this._border.isSetDiagonal() ? this._border.getDiagonal() : this._border.addNewDiagonal();
        final CTColor ctColor = CTColor.Factory.newInstance();
        ctColor.setIndexed(color);
        pr.setColor(ctColor);
    }
    
    public void setLeftBorderColor(final short color) {
        final CTBorderPr pr = this._border.isSetLeft() ? this._border.getLeft() : this._border.addNewLeft();
        final CTColor ctColor = CTColor.Factory.newInstance();
        ctColor.setIndexed(color);
        pr.setColor(ctColor);
    }
    
    public void setRightBorderColor(final short color) {
        final CTBorderPr pr = this._border.isSetRight() ? this._border.getRight() : this._border.addNewRight();
        final CTColor ctColor = CTColor.Factory.newInstance();
        ctColor.setIndexed(color);
        pr.setColor(ctColor);
    }
    
    public void setTopBorderColor(final short color) {
        final CTBorderPr pr = this._border.isSetTop() ? this._border.getTop() : this._border.addNewTop();
        final CTColor ctColor = CTColor.Factory.newInstance();
        ctColor.setIndexed(color);
        pr.setColor(ctColor);
    }
}
