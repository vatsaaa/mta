// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import java.util.Iterator;
import java.util.HashMap;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STDataValidationErrorStyle;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STDataValidationType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STDataValidationOperator;
import java.util.Map;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDataValidation;
import org.apache.poi.ss.usermodel.DataValidation;

public class XSSFDataValidation implements DataValidation
{
    private CTDataValidation ctDdataValidation;
    private XSSFDataValidationConstraint validationConstraint;
    private CellRangeAddressList regions;
    static Map<Integer, STDataValidationOperator.Enum> operatorTypeMappings;
    static Map<STDataValidationOperator.Enum, Integer> operatorTypeReverseMappings;
    static Map<Integer, STDataValidationType.Enum> validationTypeMappings;
    static Map<STDataValidationType.Enum, Integer> validationTypeReverseMappings;
    static Map<Integer, STDataValidationErrorStyle.Enum> errorStyleMappings;
    
    XSSFDataValidation(final CellRangeAddressList regions, final CTDataValidation ctDataValidation) {
        this.validationConstraint = this.getConstraint(ctDataValidation);
        this.ctDdataValidation = ctDataValidation;
        this.regions = regions;
        this.ctDdataValidation.setErrorStyle(STDataValidationErrorStyle.STOP);
        this.ctDdataValidation.setAllowBlank(true);
    }
    
    public XSSFDataValidation(final XSSFDataValidationConstraint constraint, final CellRangeAddressList regions, final CTDataValidation ctDataValidation) {
        this.validationConstraint = constraint;
        this.ctDdataValidation = ctDataValidation;
        this.regions = regions;
        this.ctDdataValidation.setErrorStyle(STDataValidationErrorStyle.STOP);
        this.ctDdataValidation.setAllowBlank(true);
    }
    
    CTDataValidation getCtDdataValidation() {
        return this.ctDdataValidation;
    }
    
    public void createErrorBox(final String title, final String text) {
        this.ctDdataValidation.setErrorTitle(title);
        this.ctDdataValidation.setError(text);
    }
    
    public void createPromptBox(final String title, final String text) {
        this.ctDdataValidation.setPromptTitle(title);
        this.ctDdataValidation.setPrompt(text);
    }
    
    public boolean getEmptyCellAllowed() {
        return this.ctDdataValidation.getAllowBlank();
    }
    
    public String getErrorBoxText() {
        return this.ctDdataValidation.getError();
    }
    
    public String getErrorBoxTitle() {
        return this.ctDdataValidation.getErrorTitle();
    }
    
    public int getErrorStyle() {
        return this.ctDdataValidation.getErrorStyle().intValue();
    }
    
    public String getPromptBoxText() {
        return this.ctDdataValidation.getPrompt();
    }
    
    public String getPromptBoxTitle() {
        return this.ctDdataValidation.getPromptTitle();
    }
    
    public boolean getShowErrorBox() {
        return this.ctDdataValidation.getShowErrorMessage();
    }
    
    public boolean getShowPromptBox() {
        return this.ctDdataValidation.getShowInputMessage();
    }
    
    public boolean getSuppressDropDownArrow() {
        return !this.ctDdataValidation.getShowDropDown();
    }
    
    public DataValidationConstraint getValidationConstraint() {
        return this.validationConstraint;
    }
    
    public void setEmptyCellAllowed(final boolean allowed) {
        this.ctDdataValidation.setAllowBlank(allowed);
    }
    
    public void setErrorStyle(final int errorStyle) {
        this.ctDdataValidation.setErrorStyle(XSSFDataValidation.errorStyleMappings.get(errorStyle));
    }
    
    public void setShowErrorBox(final boolean show) {
        this.ctDdataValidation.setShowErrorMessage(show);
    }
    
    public void setShowPromptBox(final boolean show) {
        this.ctDdataValidation.setShowInputMessage(show);
    }
    
    public void setSuppressDropDownArrow(final boolean suppress) {
        if (this.validationConstraint.getValidationType() == 3) {
            this.ctDdataValidation.setShowDropDown(!suppress);
        }
    }
    
    public CellRangeAddressList getRegions() {
        return this.regions;
    }
    
    public String prettyPrint() {
        final StringBuilder builder = new StringBuilder();
        for (final CellRangeAddress address : this.regions.getCellRangeAddresses()) {
            builder.append(address.formatAsString());
        }
        builder.append(" => ");
        builder.append(this.validationConstraint.prettyPrint());
        return builder.toString();
    }
    
    private XSSFDataValidationConstraint getConstraint(final CTDataValidation ctDataValidation) {
        XSSFDataValidationConstraint constraint = null;
        final String formula1 = ctDataValidation.getFormula1();
        final String formula2 = ctDataValidation.getFormula2();
        final STDataValidationOperator.Enum operator = ctDataValidation.getOperator();
        final STDataValidationType.Enum type = ctDataValidation.getType();
        final Integer validationType = XSSFDataValidation.validationTypeReverseMappings.get(type);
        final Integer operatorType = XSSFDataValidation.operatorTypeReverseMappings.get(operator);
        constraint = new XSSFDataValidationConstraint(validationType, operatorType, formula1, formula2);
        return constraint;
    }
    
    static {
        XSSFDataValidation.operatorTypeMappings = new HashMap<Integer, STDataValidationOperator.Enum>();
        XSSFDataValidation.operatorTypeReverseMappings = new HashMap<STDataValidationOperator.Enum, Integer>();
        XSSFDataValidation.validationTypeMappings = new HashMap<Integer, STDataValidationType.Enum>();
        XSSFDataValidation.validationTypeReverseMappings = new HashMap<STDataValidationType.Enum, Integer>();
        (XSSFDataValidation.errorStyleMappings = new HashMap<Integer, STDataValidationErrorStyle.Enum>()).put(2, STDataValidationErrorStyle.INFORMATION);
        XSSFDataValidation.errorStyleMappings.put(0, STDataValidationErrorStyle.STOP);
        XSSFDataValidation.errorStyleMappings.put(1, STDataValidationErrorStyle.WARNING);
        XSSFDataValidation.operatorTypeMappings.put(0, STDataValidationOperator.BETWEEN);
        XSSFDataValidation.operatorTypeMappings.put(1, STDataValidationOperator.NOT_BETWEEN);
        XSSFDataValidation.operatorTypeMappings.put(2, STDataValidationOperator.EQUAL);
        XSSFDataValidation.operatorTypeMappings.put(3, STDataValidationOperator.NOT_EQUAL);
        XSSFDataValidation.operatorTypeMappings.put(4, STDataValidationOperator.GREATER_THAN);
        XSSFDataValidation.operatorTypeMappings.put(6, STDataValidationOperator.GREATER_THAN_OR_EQUAL);
        XSSFDataValidation.operatorTypeMappings.put(5, STDataValidationOperator.LESS_THAN);
        XSSFDataValidation.operatorTypeMappings.put(7, STDataValidationOperator.LESS_THAN_OR_EQUAL);
        for (final Map.Entry<Integer, STDataValidationOperator.Enum> entry : XSSFDataValidation.operatorTypeMappings.entrySet()) {
            XSSFDataValidation.operatorTypeReverseMappings.put(entry.getValue(), entry.getKey());
        }
        XSSFDataValidation.validationTypeMappings.put(7, STDataValidationType.CUSTOM);
        XSSFDataValidation.validationTypeMappings.put(4, STDataValidationType.DATE);
        XSSFDataValidation.validationTypeMappings.put(2, STDataValidationType.DECIMAL);
        XSSFDataValidation.validationTypeMappings.put(3, STDataValidationType.LIST);
        XSSFDataValidation.validationTypeMappings.put(0, STDataValidationType.NONE);
        XSSFDataValidation.validationTypeMappings.put(6, STDataValidationType.TEXT_LENGTH);
        XSSFDataValidation.validationTypeMappings.put(5, STDataValidationType.TIME);
        XSSFDataValidation.validationTypeMappings.put(1, STDataValidationType.WHOLE);
        for (final Map.Entry<Integer, STDataValidationType.Enum> entry2 : XSSFDataValidation.validationTypeMappings.entrySet()) {
            XSSFDataValidation.validationTypeReverseMappings.put(entry2.getValue(), entry2.getKey());
        }
    }
}
