// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel.helpers;

import org.apache.poi.ss.formula.ptg.AreaErrPtg;
import org.apache.poi.ss.formula.ptg.AreaPtg;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCfRule;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTConditionalFormatting;
import org.apache.poi.ss.usermodel.Sheet;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellFormula;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;
import org.apache.poi.ss.formula.FormulaRenderer;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.ss.formula.FormulaShifter;
import java.util.Iterator;
import java.util.ArrayList;
import org.apache.poi.ss.util.CellRangeAddress;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public final class XSSFRowShifter
{
    private final XSSFSheet sheet;
    
    public XSSFRowShifter(final XSSFSheet sh) {
        this.sheet = sh;
    }
    
    public List<CellRangeAddress> shiftMerged(final int startRow, final int endRow, final int n) {
        final List<CellRangeAddress> shiftedRegions = new ArrayList<CellRangeAddress>();
        for (int i = 0; i < this.sheet.getNumMergedRegions(); ++i) {
            final CellRangeAddress merged = this.sheet.getMergedRegion(i);
            final boolean inStart = merged.getFirstRow() >= startRow || merged.getLastRow() >= startRow;
            final boolean inEnd = merged.getFirstRow() <= endRow || merged.getLastRow() <= endRow;
            if (inStart) {
                if (inEnd) {
                    if (!containsCell(merged, startRow - 1, 0) && !containsCell(merged, endRow + 1, 0)) {
                        merged.setFirstRow(merged.getFirstRow() + n);
                        merged.setLastRow(merged.getLastRow() + n);
                        shiftedRegions.add(merged);
                        this.sheet.removeMergedRegion(i);
                        --i;
                    }
                }
            }
        }
        for (final CellRangeAddress region : shiftedRegions) {
            this.sheet.addMergedRegion(region);
        }
        return shiftedRegions;
    }
    
    private static boolean containsCell(final CellRangeAddress cr, final int rowIx, final int colIx) {
        return cr.getFirstRow() <= rowIx && cr.getLastRow() >= rowIx && cr.getFirstColumn() <= colIx && cr.getLastColumn() >= colIx;
    }
    
    public void updateNamedRanges(final FormulaShifter shifter) {
        final XSSFWorkbook wb = this.sheet.getWorkbook();
        final XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create(wb);
        for (int i = 0; i < wb.getNumberOfNames(); ++i) {
            final XSSFName name = wb.getNameAt(i);
            final String formula = name.getRefersToFormula();
            final int sheetIndex = name.getSheetIndex();
            final Ptg[] ptgs = FormulaParser.parse(formula, fpb, 4, sheetIndex);
            if (shifter.adjustFormula(ptgs, sheetIndex)) {
                final String shiftedFmla = FormulaRenderer.toFormulaString(fpb, ptgs);
                name.setRefersToFormula(shiftedFmla);
            }
        }
    }
    
    public void updateFormulas(final FormulaShifter shifter) {
        this.updateSheetFormulas(this.sheet, shifter);
        final XSSFWorkbook wb = this.sheet.getWorkbook();
        for (final XSSFSheet sh : wb) {
            if (this.sheet == sh) {
                continue;
            }
            this.updateSheetFormulas(sh, shifter);
        }
    }
    
    private void updateSheetFormulas(final XSSFSheet sh, final FormulaShifter shifter) {
        for (final Row r : sh) {
            final XSSFRow row = (XSSFRow)r;
            this.updateRowFormulas(row, shifter);
        }
    }
    
    private void updateRowFormulas(final XSSFRow row, final FormulaShifter shifter) {
        for (final Cell c : row) {
            final XSSFCell cell = (XSSFCell)c;
            final CTCell ctCell = cell.getCTCell();
            if (ctCell.isSetF()) {
                final CTCellFormula f = ctCell.getF();
                final String formula = f.getStringValue();
                if (formula.length() > 0) {
                    final String shiftedFormula = shiftFormula(row, formula, shifter);
                    if (shiftedFormula != null) {
                        f.setStringValue(shiftedFormula);
                    }
                }
                if (!f.isSetRef()) {
                    continue;
                }
                final String ref = f.getRef();
                final String shiftedRef = shiftFormula(row, ref, shifter);
                if (shiftedRef == null) {
                    continue;
                }
                f.setRef(shiftedRef);
            }
        }
    }
    
    private static String shiftFormula(final XSSFRow row, final String formula, final FormulaShifter shifter) {
        final XSSFSheet sheet = row.getSheet();
        final XSSFWorkbook wb = sheet.getWorkbook();
        final int sheetIndex = wb.getSheetIndex(sheet);
        final XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create(wb);
        final Ptg[] ptgs = FormulaParser.parse(formula, fpb, 0, sheetIndex);
        String shiftedFmla = null;
        if (shifter.adjustFormula(ptgs, sheetIndex)) {
            shiftedFmla = FormulaRenderer.toFormulaString(fpb, ptgs);
        }
        return shiftedFmla;
    }
    
    public void updateConditionalFormatting(final FormulaShifter shifter) {
        final XSSFWorkbook wb = this.sheet.getWorkbook();
        final int sheetIndex = wb.getSheetIndex(this.sheet);
        final XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create(wb);
        final List<CTConditionalFormatting> cfList = this.sheet.getCTWorksheet().getConditionalFormattingList();
        for (int j = 0; j < cfList.size(); ++j) {
            final CTConditionalFormatting cf = cfList.get(j);
            final ArrayList<CellRangeAddress> cellRanges = new ArrayList<CellRangeAddress>();
            for (final Object stRef : cf.getSqref()) {
                final String[] regions = stRef.toString().split(" ");
                for (int i = 0; i < regions.length; ++i) {
                    cellRanges.add(CellRangeAddress.valueOf(regions[i]));
                }
            }
            boolean changed = false;
            final List<CellRangeAddress> temp = new ArrayList<CellRangeAddress>();
            for (int k = 0; k < cellRanges.size(); ++k) {
                final CellRangeAddress craOld = cellRanges.get(k);
                final CellRangeAddress craNew = shiftRange(shifter, craOld, sheetIndex);
                if (craNew == null) {
                    changed = true;
                }
                else {
                    temp.add(craNew);
                    if (craNew != craOld) {
                        changed = true;
                    }
                }
            }
            if (changed) {
                final int nRanges = temp.size();
                if (nRanges == 0) {
                    cfList.remove(j);
                    continue;
                }
                final List<String> refs = new ArrayList<String>();
                for (final CellRangeAddress a : temp) {
                    refs.add(a.formatAsString());
                }
                cf.setSqref(refs);
            }
            for (final CTCfRule cfRule : cf.getCfRuleList()) {
                final List<String> formulas = cfRule.getFormulaList();
                for (int l = 0; l < formulas.size(); ++l) {
                    final String formula = formulas.get(l);
                    final Ptg[] ptgs = FormulaParser.parse(formula, fpb, 0, sheetIndex);
                    if (shifter.adjustFormula(ptgs, sheetIndex)) {
                        final String shiftedFmla = FormulaRenderer.toFormulaString(fpb, ptgs);
                        formulas.set(l, shiftedFmla);
                    }
                }
            }
        }
    }
    
    private static CellRangeAddress shiftRange(final FormulaShifter shifter, final CellRangeAddress cra, final int currentExternSheetIx) {
        final AreaPtg aptg = new AreaPtg(cra.getFirstRow(), cra.getLastRow(), cra.getFirstColumn(), cra.getLastColumn(), false, false, false, false);
        final Ptg[] ptgs = { aptg };
        if (!shifter.adjustFormula(ptgs, currentExternSheetIx)) {
            return cra;
        }
        final Ptg ptg0 = ptgs[0];
        if (ptg0 instanceof AreaPtg) {
            final AreaPtg bptg = (AreaPtg)ptg0;
            return new CellRangeAddress(bptg.getFirstRow(), bptg.getLastRow(), bptg.getFirstColumn(), bptg.getLastColumn());
        }
        if (ptg0 instanceof AreaErrPtg) {
            return null;
        }
        throw new IllegalStateException("Unexpected shifted ptg class (" + ptg0.getClass().getName() + ")");
    }
}
