// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel.helpers;

import org.apache.poi.ss.formula.ptg.Ptg;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellFormula;
import org.apache.poi.ss.formula.FormulaRenderer;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.usermodel.Sheet;
import java.util.Iterator;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.ss.formula.ptg.NamePtg;
import org.apache.poi.ss.formula.ptg.NameXPtg;
import org.apache.poi.ss.formula.EvaluationWorkbook;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public final class XSSFFormulaUtils
{
    private final XSSFWorkbook _wb;
    private final XSSFEvaluationWorkbook _fpwb;
    
    public XSSFFormulaUtils(final XSSFWorkbook wb) {
        this._wb = wb;
        this._fpwb = XSSFEvaluationWorkbook.create(this._wb);
    }
    
    public void updateSheetName(final int sheetIndex, final String name) {
        final FormulaRenderingWorkbook frwb = new FormulaRenderingWorkbook() {
            public EvaluationWorkbook.ExternalSheet getExternalSheet(final int externSheetIndex) {
                return XSSFFormulaUtils.this._fpwb.getExternalSheet(externSheetIndex);
            }
            
            public String getSheetNameByExternSheet(final int externSheetIndex) {
                if (externSheetIndex == sheetIndex) {
                    return name;
                }
                return XSSFFormulaUtils.this._fpwb.getSheetNameByExternSheet(externSheetIndex);
            }
            
            public String resolveNameXText(final NameXPtg nameXPtg) {
                return XSSFFormulaUtils.this._fpwb.resolveNameXText(nameXPtg);
            }
            
            public String getNameText(final NamePtg namePtg) {
                return XSSFFormulaUtils.this._fpwb.getNameText(namePtg);
            }
        };
        for (int i = 0; i < this._wb.getNumberOfNames(); ++i) {
            final XSSFName nm = this._wb.getNameAt(i);
            if (nm.getSheetIndex() == -1 || nm.getSheetIndex() == sheetIndex) {
                this.updateName(nm, frwb);
            }
        }
        for (final Sheet sh : this._wb) {
            for (final Row row : sh) {
                for (final Cell cell : row) {
                    if (cell.getCellType() == 2) {
                        this.updateFormula((XSSFCell)cell, frwb);
                    }
                }
            }
        }
    }
    
    private void updateFormula(final XSSFCell cell, final FormulaRenderingWorkbook frwb) {
        final CTCellFormula f = cell.getCTCell().getF();
        if (f != null) {
            final String formula = f.getStringValue();
            if (formula != null && formula.length() > 0) {
                final int sheetIndex = this._wb.getSheetIndex(cell.getSheet());
                final Ptg[] ptgs = FormulaParser.parse(formula, this._fpwb, 0, sheetIndex);
                final String updatedFormula = FormulaRenderer.toFormulaString(frwb, ptgs);
                if (!formula.equals(updatedFormula)) {
                    f.setStringValue(updatedFormula);
                }
            }
        }
    }
    
    private void updateName(final XSSFName name, final FormulaRenderingWorkbook frwb) {
        final String formula = name.getRefersToFormula();
        if (formula != null) {
            final int sheetIndex = name.getSheetIndex();
            final Ptg[] ptgs = FormulaParser.parse(formula, this._fpwb, 4, sheetIndex);
            final String updatedFormula = FormulaRenderer.toFormulaString(frwb, ptgs);
            if (!formula.equals(updatedFormula)) {
                name.setRefersToFormula(updatedFormula);
            }
        }
    }
}
