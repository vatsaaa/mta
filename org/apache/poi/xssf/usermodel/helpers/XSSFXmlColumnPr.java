// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel.helpers;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.STXmlDataType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTXmlColumnPr;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumn;
import org.apache.poi.xssf.usermodel.XSSFTable;

public class XSSFXmlColumnPr
{
    private XSSFTable table;
    private CTTableColumn ctTableColumn;
    private CTXmlColumnPr ctXmlColumnPr;
    
    public XSSFXmlColumnPr(final XSSFTable table, final CTTableColumn ctTableColum, final CTXmlColumnPr ctXmlColumnPr) {
        this.table = table;
        this.ctTableColumn = ctTableColum;
        this.ctXmlColumnPr = ctXmlColumnPr;
    }
    
    public long getMapId() {
        return this.ctXmlColumnPr.getMapId();
    }
    
    public String getXPath() {
        return this.ctXmlColumnPr.getXpath();
    }
    
    public long getId() {
        return this.ctTableColumn.getId();
    }
    
    public String getLocalXPath() {
        String localXPath = "";
        final int numberOfCommonXPathAxis = this.table.getCommonXpath().split("/").length - 1;
        final String[] xPathTokens = this.ctXmlColumnPr.getXpath().split("/");
        for (int i = numberOfCommonXPathAxis; i < xPathTokens.length; ++i) {
            localXPath = localXPath + "/" + xPathTokens[i];
        }
        return localXPath;
    }
    
    public STXmlDataType.Enum getXmlDataType() {
        return this.ctXmlColumnPr.getXmlDataType();
    }
}
