// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel.helpers;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.util.NumericRanges;
import java.util.Comparator;
import java.util.Arrays;
import org.apache.poi.xssf.util.CTColComparator;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCol;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCols;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet;

public class ColumnHelper
{
    private CTWorksheet worksheet;
    private CTCols newCols;
    
    public ColumnHelper(final CTWorksheet worksheet) {
        this.worksheet = worksheet;
        this.cleanColumns();
    }
    
    public void cleanColumns() {
        this.newCols = CTCols.Factory.newInstance();
        CTCols[] colsArray;
        int i;
        CTCols cols;
        CTCol[] colArray;
        int y;
        CTCol col;
        for (colsArray = this.worksheet.getColsArray(), i = 0, i = 0; i < colsArray.length; ++i) {
            cols = colsArray[i];
            for (colArray = cols.getColArray(), y = 0; y < colArray.length; ++y) {
                col = colArray[y];
                this.newCols = this.addCleanColIntoCols(this.newCols, col);
            }
        }
        for (int y2 = i - 1; y2 >= 0; --y2) {
            this.worksheet.removeCols(y2);
        }
        this.worksheet.addNewCols();
        this.worksheet.setColsArray(0, this.newCols);
    }
    
    public static void sortColumns(final CTCols newCols) {
        final CTCol[] colArray = newCols.getColArray();
        Arrays.sort(colArray, new CTColComparator());
        newCols.setColArray(colArray);
    }
    
    public CTCol cloneCol(final CTCols cols, final CTCol col) {
        final CTCol newCol = cols.addNewCol();
        newCol.setMin(col.getMin());
        newCol.setMax(col.getMax());
        this.setColumnAttributes(col, newCol);
        return newCol;
    }
    
    public CTCol getColumn(final long index, final boolean splitColumns) {
        return this.getColumn1Based(index + 1L, splitColumns);
    }
    
    public CTCol getColumn1Based(final long index1, final boolean splitColumns) {
        final CTCols colsArray = this.worksheet.getColsArray(0);
        for (int i = 0; i < colsArray.sizeOfColArray(); ++i) {
            final CTCol colArray = colsArray.getColArray(i);
            if (colArray.getMin() <= index1 && colArray.getMax() >= index1) {
                if (splitColumns) {
                    if (colArray.getMin() < index1) {
                        this.insertCol(colsArray, colArray.getMin(), index1 - 1L, new CTCol[] { colArray });
                    }
                    if (colArray.getMax() > index1) {
                        this.insertCol(colsArray, index1 + 1L, colArray.getMax(), new CTCol[] { colArray });
                    }
                    colArray.setMin(index1);
                    colArray.setMax(index1);
                }
                return colArray;
            }
        }
        return null;
    }
    
    public CTCols addCleanColIntoCols(final CTCols cols, final CTCol col) {
        boolean colOverlaps = false;
        for (int i = 0; i < cols.sizeOfColArray(); ++i) {
            final CTCol ithCol = cols.getColArray(i);
            final long[] range1 = { ithCol.getMin(), ithCol.getMax() };
            final long[] range2 = { col.getMin(), col.getMax() };
            final long[] overlappingRange = NumericRanges.getOverlappingRange(range1, range2);
            final int overlappingType = NumericRanges.getOverlappingType(range1, range2);
            if (overlappingType == NumericRanges.OVERLAPS_1_MINOR) {
                ithCol.setMax(overlappingRange[0] - 1L);
                final CTCol rangeCol = this.insertCol(cols, overlappingRange[0], overlappingRange[1], new CTCol[] { ithCol, col });
                ++i;
                final CTCol newCol = this.insertCol(cols, overlappingRange[1] + 1L, col.getMax(), new CTCol[] { col });
                ++i;
            }
            else if (overlappingType == NumericRanges.OVERLAPS_2_MINOR) {
                ithCol.setMin(overlappingRange[1] + 1L);
                final CTCol rangeCol = this.insertCol(cols, overlappingRange[0], overlappingRange[1], new CTCol[] { ithCol, col });
                ++i;
                final CTCol newCol = this.insertCol(cols, col.getMin(), overlappingRange[0] - 1L, new CTCol[] { col });
                ++i;
            }
            else if (overlappingType == NumericRanges.OVERLAPS_2_WRAPS) {
                this.setColumnAttributes(col, ithCol);
                if (col.getMin() != ithCol.getMin()) {
                    final CTCol newColBefore = this.insertCol(cols, col.getMin(), ithCol.getMin() - 1L, new CTCol[] { col });
                    ++i;
                }
                if (col.getMax() != ithCol.getMax()) {
                    final CTCol newColAfter = this.insertCol(cols, ithCol.getMax() + 1L, col.getMax(), new CTCol[] { col });
                    ++i;
                }
            }
            else if (overlappingType == NumericRanges.OVERLAPS_1_WRAPS) {
                if (col.getMin() != ithCol.getMin()) {
                    final CTCol newColBefore = this.insertCol(cols, ithCol.getMin(), col.getMin() - 1L, new CTCol[] { ithCol });
                    ++i;
                }
                if (col.getMax() != ithCol.getMax()) {
                    final CTCol newColAfter = this.insertCol(cols, col.getMax() + 1L, ithCol.getMax(), new CTCol[] { ithCol });
                    ++i;
                }
                ithCol.setMin(overlappingRange[0]);
                ithCol.setMax(overlappingRange[1]);
                this.setColumnAttributes(col, ithCol);
            }
            if (overlappingType != NumericRanges.NO_OVERLAPS) {
                colOverlaps = true;
            }
        }
        if (!colOverlaps) {
            final CTCol newCol2 = this.cloneCol(cols, col);
        }
        sortColumns(cols);
        return cols;
    }
    
    private CTCol insertCol(final CTCols cols, final long min, final long max, final CTCol[] colsWithAttributes) {
        if (!this.columnExists(cols, min, max)) {
            final CTCol newCol = cols.insertNewCol(0);
            newCol.setMin(min);
            newCol.setMax(max);
            for (final CTCol col : colsWithAttributes) {
                this.setColumnAttributes(col, newCol);
            }
            return newCol;
        }
        return null;
    }
    
    public boolean columnExists(final CTCols cols, final long index) {
        return this.columnExists1Based(cols, index + 1L);
    }
    
    private boolean columnExists1Based(final CTCols cols, final long index1) {
        for (int i = 0; i < cols.sizeOfColArray(); ++i) {
            if (cols.getColArray(i).getMin() == index1) {
                return true;
            }
        }
        return false;
    }
    
    public void setColumnAttributes(final CTCol fromCol, final CTCol toCol) {
        if (fromCol.isSetBestFit()) {
            toCol.setBestFit(fromCol.getBestFit());
        }
        if (fromCol.isSetCustomWidth()) {
            toCol.setCustomWidth(fromCol.getCustomWidth());
        }
        if (fromCol.isSetHidden()) {
            toCol.setHidden(fromCol.getHidden());
        }
        if (fromCol.isSetStyle()) {
            toCol.setStyle(fromCol.getStyle());
        }
        if (fromCol.isSetWidth()) {
            toCol.setWidth(fromCol.getWidth());
        }
        if (fromCol.isSetCollapsed()) {
            toCol.setCollapsed(fromCol.getCollapsed());
        }
        if (fromCol.isSetPhonetic()) {
            toCol.setPhonetic(fromCol.getPhonetic());
        }
        if (fromCol.isSetOutlineLevel()) {
            toCol.setOutlineLevel(fromCol.getOutlineLevel());
        }
        toCol.setCollapsed(fromCol.isSetCollapsed());
    }
    
    public void setColBestFit(final long index, final boolean bestFit) {
        final CTCol col = this.getOrCreateColumn1Based(index + 1L, false);
        col.setBestFit(bestFit);
    }
    
    public void setCustomWidth(final long index, final boolean bestFit) {
        final CTCol col = this.getOrCreateColumn1Based(index + 1L, true);
        col.setCustomWidth(bestFit);
    }
    
    public void setColWidth(final long index, final double width) {
        final CTCol col = this.getOrCreateColumn1Based(index + 1L, true);
        col.setWidth(width);
    }
    
    public void setColHidden(final long index, final boolean hidden) {
        final CTCol col = this.getOrCreateColumn1Based(index + 1L, true);
        col.setHidden(hidden);
    }
    
    protected CTCol getOrCreateColumn1Based(final long index1, final boolean splitColumns) {
        CTCol col = this.getColumn1Based(index1, splitColumns);
        if (col == null) {
            col = this.worksheet.getColsArray(0).addNewCol();
            col.setMin(index1);
            col.setMax(index1);
        }
        return col;
    }
    
    public void setColDefaultStyle(final long index, final CellStyle style) {
        this.setColDefaultStyle(index, style.getIndex());
    }
    
    public void setColDefaultStyle(final long index, final int styleId) {
        final CTCol col = this.getOrCreateColumn1Based(index + 1L, true);
        col.setStyle(styleId);
    }
    
    public int getColDefaultStyle(final long index) {
        if (this.getColumn(index, false) != null) {
            return (int)this.getColumn(index, false).getStyle();
        }
        return -1;
    }
    
    private boolean columnExists(final CTCols cols, final long min, final long max) {
        for (int i = 0; i < cols.sizeOfColArray(); ++i) {
            if (cols.getColArray(i).getMin() == min && cols.getColArray(i).getMax() == max) {
                return true;
            }
        }
        return false;
    }
    
    public int getIndexOfColumn(final CTCols cols, final CTCol col) {
        for (int i = 0; i < cols.sizeOfColArray(); ++i) {
            if (cols.getColArray(i).getMin() == col.getMin() && cols.getColArray(i).getMax() == col.getMax()) {
                return i;
            }
        }
        return -1;
    }
}
