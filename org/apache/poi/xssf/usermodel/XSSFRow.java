// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.model.CalculationChain;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.Internal;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.xmlbeans.XmlObject;
import org.apache.poi.ss.usermodel.Cell;
import java.util.Iterator;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCell;
import java.util.TreeMap;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRow;
import org.apache.poi.util.POILogger;
import org.apache.poi.ss.usermodel.Row;

public class XSSFRow implements Row, Comparable<XSSFRow>
{
    private static final POILogger _logger;
    private final CTRow _row;
    private final TreeMap<Integer, XSSFCell> _cells;
    private final XSSFSheet _sheet;
    
    protected XSSFRow(final CTRow row, final XSSFSheet sheet) {
        this._row = row;
        this._sheet = sheet;
        this._cells = new TreeMap<Integer, XSSFCell>();
        for (final CTCell c : row.getCArray()) {
            final XSSFCell cell = new XSSFCell(this, c);
            this._cells.put(cell.getColumnIndex(), cell);
            sheet.onReadCell(cell);
        }
    }
    
    public XSSFSheet getSheet() {
        return this._sheet;
    }
    
    public Iterator<Cell> cellIterator() {
        return (Iterator<Cell>)this._cells.values().iterator();
    }
    
    public Iterator<Cell> iterator() {
        return this.cellIterator();
    }
    
    public int compareTo(final XSSFRow row) {
        final int thisVal = this.getRowNum();
        if (row.getSheet() != this.getSheet()) {
            throw new IllegalArgumentException("The compared rows must belong to the same XSSFSheet");
        }
        final int anotherVal = row.getRowNum();
        return (thisVal < anotherVal) ? -1 : ((thisVal == anotherVal) ? 0 : 1);
    }
    
    public XSSFCell createCell(final int columnIndex) {
        return this.createCell(columnIndex, 3);
    }
    
    public XSSFCell createCell(final int columnIndex, final int type) {
        final XSSFCell prev = this._cells.get(columnIndex);
        CTCell ctCell;
        if (prev != null) {
            ctCell = prev.getCTCell();
            ctCell.set(CTCell.Factory.newInstance());
        }
        else {
            ctCell = this._row.addNewC();
        }
        final XSSFCell xcell = new XSSFCell(this, ctCell);
        xcell.setCellNum(columnIndex);
        if (type != 3) {
            xcell.setCellType(type);
        }
        this._cells.put(columnIndex, xcell);
        return xcell;
    }
    
    public XSSFCell getCell(final int cellnum) {
        return this.getCell(cellnum, this._sheet.getWorkbook().getMissingCellPolicy());
    }
    
    public XSSFCell getCell(final int cellnum, final MissingCellPolicy policy) {
        if (cellnum < 0) {
            throw new IllegalArgumentException("Cell index must be >= 0");
        }
        final XSSFCell cell = this._cells.get(cellnum);
        if (policy == XSSFRow.RETURN_NULL_AND_BLANK) {
            return cell;
        }
        if (policy == XSSFRow.RETURN_BLANK_AS_NULL) {
            if (cell == null) {
                return cell;
            }
            if (cell.getCellType() == 3) {
                return null;
            }
            return cell;
        }
        else {
            if (policy != XSSFRow.CREATE_NULL_AS_BLANK) {
                throw new IllegalArgumentException("Illegal policy " + policy + " (" + policy.id + ")");
            }
            if (cell == null) {
                return this.createCell((int)(short)cellnum, 3);
            }
            return cell;
        }
    }
    
    public short getFirstCellNum() {
        return (short)((this._cells.size() == 0) ? -1 : this._cells.firstKey());
    }
    
    public short getLastCellNum() {
        return (short)((this._cells.size() == 0) ? -1 : (this._cells.lastKey() + 1));
    }
    
    public short getHeight() {
        return (short)(this.getHeightInPoints() * 20.0f);
    }
    
    public float getHeightInPoints() {
        if (this._row.isSetHt()) {
            return (float)this._row.getHt();
        }
        return this._sheet.getDefaultRowHeightInPoints();
    }
    
    public void setHeight(final short height) {
        if (height == -1) {
            if (this._row.isSetHt()) {
                this._row.unsetHt();
            }
            if (this._row.isSetCustomHeight()) {
                this._row.unsetCustomHeight();
            }
        }
        else {
            this._row.setHt(height / 20.0);
            this._row.setCustomHeight(true);
        }
    }
    
    public void setHeightInPoints(final float height) {
        this.setHeight((short)((height == -1.0f) ? -1.0f : (height * 20.0f)));
    }
    
    public int getPhysicalNumberOfCells() {
        return this._cells.size();
    }
    
    public int getRowNum() {
        return (int)(this._row.getR() - 1L);
    }
    
    public void setRowNum(final int rowIndex) {
        final int maxrow = SpreadsheetVersion.EXCEL2007.getLastRowIndex();
        if (rowIndex < 0 || rowIndex > maxrow) {
            throw new IllegalArgumentException("Invalid row number (" + rowIndex + ") outside allowable range (0.." + maxrow + ")");
        }
        this._row.setR(rowIndex + 1);
    }
    
    public boolean getZeroHeight() {
        return this._row.getHidden();
    }
    
    public void setZeroHeight(final boolean height) {
        this._row.setHidden(height);
    }
    
    public boolean isFormatted() {
        return this._row.isSetS();
    }
    
    public XSSFCellStyle getRowStyle() {
        if (!this.isFormatted()) {
            return null;
        }
        final StylesTable stylesSource = this.getSheet().getWorkbook().getStylesSource();
        if (stylesSource.getNumCellStyles() > 0) {
            return stylesSource.getStyleAt((int)this._row.getS());
        }
        return null;
    }
    
    public void setRowStyle(final CellStyle style) {
        if (style == null) {
            if (this._row.isSetS()) {
                this._row.unsetS();
                this._row.unsetCustomFormat();
            }
        }
        else {
            final StylesTable styleSource = this.getSheet().getWorkbook().getStylesSource();
            final XSSFCellStyle xStyle = (XSSFCellStyle)style;
            xStyle.verifyBelongsToStylesSource(styleSource);
            final long idx = styleSource.putStyle(xStyle);
            this._row.setS(idx);
            this._row.setCustomFormat(true);
        }
    }
    
    public void removeCell(final Cell cell) {
        if (cell.getRow() != this) {
            throw new IllegalArgumentException("Specified cell does not belong to this row");
        }
        final XSSFCell xcell = (XSSFCell)cell;
        if (xcell.isPartOfArrayFormulaGroup()) {
            xcell.notifyArrayFormulaChanging();
        }
        if (cell.getCellType() == 2) {
            this._sheet.getWorkbook().onDeleteFormula(xcell);
        }
        this._cells.remove(cell.getColumnIndex());
    }
    
    @Internal
    public CTRow getCTRow() {
        return this._row;
    }
    
    protected void onDocumentWrite() {
        boolean isOrdered = true;
        if (this._row.sizeOfCArray() != this._cells.size()) {
            isOrdered = false;
        }
        else {
            int i = 0;
            for (final XSSFCell cell : this._cells.values()) {
                final CTCell c1 = cell.getCTCell();
                final CTCell c2 = this._row.getCArray(i++);
                final String r1 = c1.getR();
                final String r2 = c2.getR();
                Label_0124: {
                    if (r1 == null) {
                        if (r2 == null) {
                            continue;
                        }
                        break Label_0124;
                    }
                    else {
                        if (!r1.equals(r2)) {
                            break Label_0124;
                        }
                        continue;
                    }
                    continue;
                }
                isOrdered = false;
                break;
            }
        }
        if (!isOrdered) {
            final CTCell[] cArray = new CTCell[this._cells.size()];
            int j = 0;
            for (final XSSFCell c3 : this._cells.values()) {
                cArray[j++] = c3.getCTCell();
            }
            this._row.setCArray(cArray);
        }
    }
    
    @Override
    public String toString() {
        return this._row.toString();
    }
    
    protected void shift(final int n) {
        final int rownum = this.getRowNum() + n;
        final CalculationChain calcChain = this._sheet.getWorkbook().getCalculationChain();
        final int sheetId = (int)this._sheet.sheet.getSheetId();
        final String msg = "Row[rownum=" + this.getRowNum() + "] contains cell(s) included in a multi-cell array formula. " + "You cannot change part of an array.";
        for (final Cell c : this) {
            final XSSFCell cell = (XSSFCell)c;
            if (cell.isPartOfArrayFormulaGroup()) {
                cell.notifyArrayFormulaChanging(msg);
            }
            if (calcChain != null) {
                calcChain.removeItem(sheetId, cell.getReference());
            }
            final CTCell ctCell = cell.getCTCell();
            final String r = new CellReference(rownum, cell.getColumnIndex()).formatAsString();
            ctCell.setR(r);
        }
        this.setRowNum(rownum);
    }
    
    static {
        _logger = POILogFactory.getLogger(XSSFRow.class);
    }
}
