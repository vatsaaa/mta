// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.Internal;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Comment;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.xmlbeans.XmlObject;
import java.util.Calendar;
import org.apache.poi.ss.usermodel.DateUtil;
import java.util.Date;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;
import org.apache.poi.ss.formula.FormulaRenderer;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.formula.SharedFormula;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellFormula;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCellFormulaType;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.FormulaError;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCellType;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCell;
import org.apache.poi.ss.usermodel.Cell;

public final class XSSFCell implements Cell
{
    private static final String FALSE_AS_STRING = "0";
    private static final String TRUE_AS_STRING = "1";
    private final CTCell _cell;
    private final XSSFRow _row;
    private int _cellNum;
    private SharedStringsTable _sharedStringSource;
    private StylesTable _stylesSource;
    
    protected XSSFCell(final XSSFRow row, final CTCell cell) {
        this._cell = cell;
        this._row = row;
        if (cell.getR() != null) {
            this._cellNum = new CellReference(cell.getR()).getCol();
        }
        this._sharedStringSource = row.getSheet().getWorkbook().getSharedStringSource();
        this._stylesSource = row.getSheet().getWorkbook().getStylesSource();
    }
    
    protected SharedStringsTable getSharedStringSource() {
        return this._sharedStringSource;
    }
    
    protected StylesTable getStylesSource() {
        return this._stylesSource;
    }
    
    public XSSFSheet getSheet() {
        return this.getRow().getSheet();
    }
    
    public XSSFRow getRow() {
        return this._row;
    }
    
    public boolean getBooleanCellValue() {
        final int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return false;
            }
            case 4: {
                return this._cell.isSetV() && "1".equals(this._cell.getV());
            }
            case 2: {
                return this._cell.isSetV() && "1".equals(this._cell.getV());
            }
            default: {
                throw typeMismatch(4, cellType, false);
            }
        }
    }
    
    public void setCellValue(final boolean value) {
        this._cell.setT(STCellType.B);
        this._cell.setV(value ? "1" : "0");
    }
    
    public double getNumericCellValue() {
        final int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return 0.0;
            }
            case 0:
            case 2: {
                if (this._cell.isSetV()) {
                    try {
                        return Double.parseDouble(this._cell.getV());
                    }
                    catch (NumberFormatException e) {
                        throw typeMismatch(0, 1, false);
                    }
                }
                return 0.0;
            }
            default: {
                throw typeMismatch(0, cellType, false);
            }
        }
    }
    
    public void setCellValue(final double value) {
        if (Double.isInfinite(value)) {
            this._cell.setT(STCellType.E);
            this._cell.setV(FormulaError.DIV0.getString());
        }
        else if (Double.isNaN(value)) {
            this._cell.setT(STCellType.E);
            this._cell.setV(FormulaError.NUM.getString());
        }
        else {
            this._cell.setT(STCellType.N);
            this._cell.setV(String.valueOf(value));
        }
    }
    
    public String getStringCellValue() {
        final XSSFRichTextString str = this.getRichStringCellValue();
        return (str == null) ? null : str.getString();
    }
    
    public XSSFRichTextString getRichStringCellValue() {
        final int cellType = this.getCellType();
        XSSFRichTextString rt = null;
        switch (cellType) {
            case 3: {
                rt = new XSSFRichTextString("");
                break;
            }
            case 1: {
                if (this._cell.getT() == STCellType.INLINE_STR) {
                    if (this._cell.isSetIs()) {
                        rt = new XSSFRichTextString(this._cell.getIs());
                        break;
                    }
                    if (this._cell.isSetV()) {
                        rt = new XSSFRichTextString(this._cell.getV());
                        break;
                    }
                    rt = new XSSFRichTextString("");
                    break;
                }
                else {
                    if (this._cell.getT() == STCellType.STR) {
                        rt = new XSSFRichTextString(this._cell.isSetV() ? this._cell.getV() : "");
                        break;
                    }
                    if (this._cell.isSetV()) {
                        final int idx = Integer.parseInt(this._cell.getV());
                        rt = new XSSFRichTextString(this._sharedStringSource.getEntryAt(idx));
                        break;
                    }
                    rt = new XSSFRichTextString("");
                    break;
                }
                break;
            }
            case 2: {
                checkFormulaCachedValueType(1, this.getBaseCellType(false));
                rt = new XSSFRichTextString(this._cell.isSetV() ? this._cell.getV() : "");
                break;
            }
            default: {
                throw typeMismatch(1, cellType, false);
            }
        }
        rt.setStylesTableReference(this._stylesSource);
        return rt;
    }
    
    private static void checkFormulaCachedValueType(final int expectedTypeCode, final int cachedValueType) {
        if (cachedValueType != expectedTypeCode) {
            throw typeMismatch(expectedTypeCode, cachedValueType, true);
        }
    }
    
    public void setCellValue(final String str) {
        this.setCellValue((str == null) ? null : new XSSFRichTextString(str));
    }
    
    public void setCellValue(final RichTextString str) {
        if (str == null || str.getString() == null) {
            this.setCellType(3);
            return;
        }
        final int cellType = this.getCellType();
        switch (cellType) {
            case 2: {
                this._cell.setV(str.getString());
                this._cell.setT(STCellType.STR);
                break;
            }
            default: {
                if (this._cell.getT() == STCellType.INLINE_STR) {
                    this._cell.setV(str.getString());
                    break;
                }
                this._cell.setT(STCellType.S);
                final XSSFRichTextString rt = (XSSFRichTextString)str;
                rt.setStylesTableReference(this._stylesSource);
                final int sRef = this._sharedStringSource.addEntry(rt.getCTRst());
                this._cell.setV(Integer.toString(sRef));
                break;
            }
        }
    }
    
    public String getCellFormula() {
        final int cellType = this.getCellType();
        if (cellType != 2) {
            throw typeMismatch(2, cellType, false);
        }
        final CTCellFormula f = this._cell.getF();
        if (this.isPartOfArrayFormulaGroup() && f == null) {
            final XSSFCell cell = this.getSheet().getFirstCellInArrayFormula(this);
            return cell.getCellFormula();
        }
        if (f.getT() == STCellFormulaType.SHARED) {
            return this.convertSharedFormula((int)f.getSi());
        }
        return f.getStringValue();
    }
    
    private String convertSharedFormula(final int si) {
        final XSSFSheet sheet = this.getSheet();
        final CTCellFormula f = sheet.getSharedFormula(si);
        if (f == null) {
            throw new IllegalStateException("Master cell of a shared formula with sid=" + si + " was not found");
        }
        final String sharedFormula = f.getStringValue();
        final String sharedFormulaRange = f.getRef();
        final CellRangeAddress ref = CellRangeAddress.valueOf(sharedFormulaRange);
        final int sheetIndex = sheet.getWorkbook().getSheetIndex(sheet);
        final XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create(sheet.getWorkbook());
        final SharedFormula sf = new SharedFormula(SpreadsheetVersion.EXCEL2007);
        final Ptg[] ptgs = FormulaParser.parse(sharedFormula, fpb, 0, sheetIndex);
        final Ptg[] fmla = sf.convertSharedFormulas(ptgs, this.getRowIndex() - ref.getFirstRow(), this.getColumnIndex() - ref.getFirstColumn());
        return FormulaRenderer.toFormulaString(fpb, fmla);
    }
    
    public void setCellFormula(final String formula) {
        if (this.isPartOfArrayFormulaGroup()) {
            this.notifyArrayFormulaChanging();
        }
        this.setFormula(formula, 0);
    }
    
    void setCellArrayFormula(final String formula, final CellRangeAddress range) {
        this.setFormula(formula, 2);
        final CTCellFormula cellFormula = this._cell.getF();
        cellFormula.setT(STCellFormulaType.ARRAY);
        cellFormula.setRef(range.formatAsString());
    }
    
    private void setFormula(final String formula, final int formulaType) {
        final XSSFWorkbook wb = this._row.getSheet().getWorkbook();
        if (formula == null) {
            wb.onDeleteFormula(this);
            if (this._cell.isSetF()) {
                this._cell.unsetF();
            }
            return;
        }
        final XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create(wb);
        FormulaParser.parse(formula, fpb, formulaType, wb.getSheetIndex(this.getSheet()));
        final CTCellFormula f = CTCellFormula.Factory.newInstance();
        f.setStringValue(formula);
        this._cell.setF(f);
        if (this._cell.isSetV()) {
            this._cell.unsetV();
        }
    }
    
    public int getColumnIndex() {
        return this._cellNum;
    }
    
    public int getRowIndex() {
        return this._row.getRowNum();
    }
    
    public String getReference() {
        return this._cell.getR();
    }
    
    public XSSFCellStyle getCellStyle() {
        XSSFCellStyle style = null;
        if (this._stylesSource.getNumCellStyles() > 0) {
            final long idx = this._cell.isSetS() ? this._cell.getS() : 0L;
            style = this._stylesSource.getStyleAt((int)idx);
        }
        return style;
    }
    
    public void setCellStyle(final CellStyle style) {
        if (style == null) {
            if (this._cell.isSetS()) {
                this._cell.unsetS();
            }
        }
        else {
            final XSSFCellStyle xStyle = (XSSFCellStyle)style;
            xStyle.verifyBelongsToStylesSource(this._stylesSource);
            final long idx = this._stylesSource.putStyle(xStyle);
            this._cell.setS(idx);
        }
    }
    
    public int getCellType() {
        if (this._cell.getF() != null || this.getSheet().isCellInArrayFormulaContext(this)) {
            return 2;
        }
        return this.getBaseCellType(true);
    }
    
    public int getCachedFormulaResultType() {
        if (this._cell.getF() == null) {
            throw new IllegalStateException("Only formula cells have cached results");
        }
        return this.getBaseCellType(false);
    }
    
    private int getBaseCellType(final boolean blankCells) {
        switch (this._cell.getT().intValue()) {
            case 1: {
                return 4;
            }
            case 2: {
                if (!this._cell.isSetV() && blankCells) {
                    return 3;
                }
                return 0;
            }
            case 3: {
                return 5;
            }
            case 4:
            case 5:
            case 6: {
                return 1;
            }
            default: {
                throw new IllegalStateException("Illegal cell type: " + this._cell.getT());
            }
        }
    }
    
    public Date getDateCellValue() {
        final int cellType = this.getCellType();
        if (cellType == 3) {
            return null;
        }
        final double value = this.getNumericCellValue();
        final boolean date1904 = this.getSheet().getWorkbook().isDate1904();
        return DateUtil.getJavaDate(value, date1904);
    }
    
    public void setCellValue(final Date value) {
        final boolean date1904 = this.getSheet().getWorkbook().isDate1904();
        this.setCellValue(DateUtil.getExcelDate(value, date1904));
    }
    
    public void setCellValue(final Calendar value) {
        final boolean date1904 = this.getSheet().getWorkbook().isDate1904();
        this.setCellValue(DateUtil.getExcelDate(value, date1904));
    }
    
    public String getErrorCellString() {
        final int cellType = this.getBaseCellType(true);
        if (cellType != 5) {
            throw typeMismatch(5, cellType, false);
        }
        return this._cell.getV();
    }
    
    public byte getErrorCellValue() {
        final String code = this.getErrorCellString();
        if (code == null) {
            return 0;
        }
        return FormulaError.forString(code).getCode();
    }
    
    public void setCellErrorValue(final byte errorCode) {
        final FormulaError error = FormulaError.forInt(errorCode);
        this.setCellErrorValue(error);
    }
    
    public void setCellErrorValue(final FormulaError error) {
        this._cell.setT(STCellType.E);
        this._cell.setV(error.getString());
    }
    
    public void setAsActiveCell() {
        this.getSheet().setActiveCell(this._cell.getR());
    }
    
    private void setBlank() {
        final CTCell blank = CTCell.Factory.newInstance();
        blank.setR(this._cell.getR());
        if (this._cell.isSetS()) {
            blank.setS(this._cell.getS());
        }
        this._cell.set(blank);
    }
    
    protected void setCellNum(final int num) {
        checkBounds(num);
        this._cellNum = num;
        final String ref = new CellReference(this.getRowIndex(), this.getColumnIndex()).formatAsString();
        this._cell.setR(ref);
    }
    
    public void setCellType(final int cellType) {
        final int prevType = this.getCellType();
        if (this.isPartOfArrayFormulaGroup()) {
            this.notifyArrayFormulaChanging();
        }
        if (prevType == 2 && cellType != 2) {
            this.getSheet().getWorkbook().onDeleteFormula(this);
        }
        switch (cellType) {
            case 3: {
                this.setBlank();
                break;
            }
            case 4: {
                final String newVal = this.convertCellValueToBoolean() ? "1" : "0";
                this._cell.setT(STCellType.B);
                this._cell.setV(newVal);
                break;
            }
            case 0: {
                this._cell.setT(STCellType.N);
                break;
            }
            case 5: {
                this._cell.setT(STCellType.E);
                break;
            }
            case 1: {
                if (prevType != 1) {
                    final String str = this.convertCellValueToString();
                    final XSSFRichTextString rt = new XSSFRichTextString(str);
                    rt.setStylesTableReference(this._stylesSource);
                    final int sRef = this._sharedStringSource.addEntry(rt.getCTRst());
                    this._cell.setV(Integer.toString(sRef));
                }
                this._cell.setT(STCellType.S);
                break;
            }
            case 2: {
                if (!this._cell.isSetF()) {
                    final CTCellFormula f = CTCellFormula.Factory.newInstance();
                    f.setStringValue("0");
                    this._cell.setF(f);
                    if (this._cell.isSetT()) {
                        this._cell.unsetT();
                    }
                    break;
                }
                break;
            }
            default: {
                throw new IllegalArgumentException("Illegal cell type: " + cellType);
            }
        }
        if (cellType != 2 && this._cell.isSetF()) {
            this._cell.unsetF();
        }
    }
    
    @Override
    public String toString() {
        switch (this.getCellType()) {
            case 3: {
                return "";
            }
            case 4: {
                return this.getBooleanCellValue() ? "TRUE" : "FALSE";
            }
            case 5: {
                return ErrorEval.getText(this.getErrorCellValue());
            }
            case 2: {
                return this.getCellFormula();
            }
            case 0: {
                if (DateUtil.isCellDateFormatted(this)) {
                    final DateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                    return sdf.format(this.getDateCellValue());
                }
                return this.getNumericCellValue() + "";
            }
            case 1: {
                return this.getRichStringCellValue().toString();
            }
            default: {
                return "Unknown Cell Type: " + this.getCellType();
            }
        }
    }
    
    public String getRawValue() {
        return this._cell.getV();
    }
    
    private static String getCellTypeName(final int cellTypeCode) {
        switch (cellTypeCode) {
            case 3: {
                return "blank";
            }
            case 1: {
                return "text";
            }
            case 4: {
                return "boolean";
            }
            case 5: {
                return "error";
            }
            case 0: {
                return "numeric";
            }
            case 2: {
                return "formula";
            }
            default: {
                return "#unknown cell type (" + cellTypeCode + ")#";
            }
        }
    }
    
    private static RuntimeException typeMismatch(final int expectedTypeCode, final int actualTypeCode, final boolean isFormulaCell) {
        final String msg = "Cannot get a " + getCellTypeName(expectedTypeCode) + " value from a " + getCellTypeName(actualTypeCode) + " " + (isFormulaCell ? "formula " : "") + "cell";
        return new IllegalStateException(msg);
    }
    
    private static void checkBounds(final int cellIndex) {
        final SpreadsheetVersion v = SpreadsheetVersion.EXCEL2007;
        final int maxcol = SpreadsheetVersion.EXCEL2007.getLastColumnIndex();
        if (cellIndex < 0 || cellIndex > maxcol) {
            throw new IllegalArgumentException("Invalid column index (" + cellIndex + ").  Allowable column range for " + v.name() + " is (0.." + maxcol + ") or ('A'..'" + v.getLastColumnName() + "')");
        }
    }
    
    public XSSFComment getCellComment() {
        return this.getSheet().getCellComment(this._row.getRowNum(), this.getColumnIndex());
    }
    
    public void setCellComment(final Comment comment) {
        if (comment == null) {
            this.removeCellComment();
            return;
        }
        comment.setRow(this.getRowIndex());
        comment.setColumn(this.getColumnIndex());
    }
    
    public void removeCellComment() {
        final XSSFComment comment = this.getCellComment();
        if (comment != null) {
            final String ref = this._cell.getR();
            final XSSFSheet sh = this.getSheet();
            sh.getCommentsTable(false).removeComment(ref);
            sh.getVMLDrawing(false).removeCommentShape(this.getRowIndex(), this.getColumnIndex());
        }
    }
    
    public XSSFHyperlink getHyperlink() {
        return this.getSheet().getHyperlink(this._row.getRowNum(), this._cellNum);
    }
    
    public void setHyperlink(final Hyperlink hyperlink) {
        final XSSFHyperlink link = (XSSFHyperlink)hyperlink;
        link.setCellReference(new CellReference(this._row.getRowNum(), this._cellNum).formatAsString());
        this.getSheet().addHyperlink(link);
    }
    
    @Internal
    public CTCell getCTCell() {
        return this._cell;
    }
    
    private boolean convertCellValueToBoolean() {
        int cellType = this.getCellType();
        if (cellType == 2) {
            cellType = this.getBaseCellType(false);
        }
        switch (cellType) {
            case 4: {
                return "1".equals(this._cell.getV());
            }
            case 1: {
                final int sstIndex = Integer.parseInt(this._cell.getV());
                final XSSFRichTextString rt = new XSSFRichTextString(this._sharedStringSource.getEntryAt(sstIndex));
                final String text = rt.getString();
                return Boolean.parseBoolean(text);
            }
            case 0: {
                return Double.parseDouble(this._cell.getV()) != 0.0;
            }
            case 3:
            case 5: {
                return false;
            }
            default: {
                throw new RuntimeException("Unexpected cell type (" + cellType + ")");
            }
        }
    }
    
    private String convertCellValueToString() {
        int cellType = this.getCellType();
        switch (cellType) {
            case 3: {
                return "";
            }
            case 4: {
                return "1".equals(this._cell.getV()) ? "TRUE" : "FALSE";
            }
            case 1: {
                final int sstIndex = Integer.parseInt(this._cell.getV());
                final XSSFRichTextString rt = new XSSFRichTextString(this._sharedStringSource.getEntryAt(sstIndex));
                return rt.getString();
            }
            case 0:
            case 5: {
                return this._cell.getV();
            }
            case 2: {
                cellType = this.getBaseCellType(false);
                final String textValue = this._cell.getV();
                switch (cellType) {
                    case 4: {
                        if ("1".equals(textValue)) {
                            return "TRUE";
                        }
                        if ("0".equals(textValue)) {
                            return "FALSE";
                        }
                        throw new IllegalStateException("Unexpected boolean cached formula value '" + textValue + "'.");
                    }
                    case 0:
                    case 1:
                    case 5: {
                        return textValue;
                    }
                    default: {
                        throw new IllegalStateException("Unexpected formula result type (" + cellType + ")");
                    }
                }
                break;
            }
            default: {
                throw new IllegalStateException("Unexpected cell type (" + cellType + ")");
            }
        }
    }
    
    public CellRangeAddress getArrayFormulaRange() {
        final XSSFCell cell = this.getSheet().getFirstCellInArrayFormula(this);
        if (cell == null) {
            throw new IllegalStateException("Cell " + this._cell.getR() + " is not part of an array formula.");
        }
        final String formulaRef = cell._cell.getF().getRef();
        return CellRangeAddress.valueOf(formulaRef);
    }
    
    public boolean isPartOfArrayFormulaGroup() {
        return this.getSheet().isCellInArrayFormulaContext(this);
    }
    
    void notifyArrayFormulaChanging(final String msg) {
        if (this.isPartOfArrayFormulaGroup()) {
            final CellRangeAddress cra = this.getArrayFormulaRange();
            if (cra.getNumberOfCells() > 1) {
                throw new IllegalStateException(msg);
            }
            this.getRow().getSheet().removeArrayFormula(this);
        }
    }
    
    void notifyArrayFormulaChanging() {
        final CellReference ref = new CellReference(this);
        final String msg = "Cell " + ref.formatAsString() + " is part of a multi-cell array formula. " + "You cannot change part of an array.";
        this.notifyArrayFormulaChanging(msg);
    }
}
