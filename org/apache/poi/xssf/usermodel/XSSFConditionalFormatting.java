// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.xmlbeans.XmlObject;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;
import java.util.Iterator;
import java.util.ArrayList;
import org.apache.poi.ss.util.CellRangeAddress;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTConditionalFormatting;
import org.apache.poi.ss.usermodel.ConditionalFormatting;

public class XSSFConditionalFormatting implements ConditionalFormatting
{
    private final CTConditionalFormatting _cf;
    private final XSSFSheet _sh;
    
    XSSFConditionalFormatting(final XSSFSheet sh) {
        this._cf = CTConditionalFormatting.Factory.newInstance();
        this._sh = sh;
    }
    
    XSSFConditionalFormatting(final XSSFSheet sh, final CTConditionalFormatting cf) {
        this._cf = cf;
        this._sh = sh;
    }
    
    CTConditionalFormatting getCTConditionalFormatting() {
        return this._cf;
    }
    
    public CellRangeAddress[] getFormattingRanges() {
        final ArrayList<CellRangeAddress> lst = new ArrayList<CellRangeAddress>();
        for (final Object stRef : this._cf.getSqref()) {
            final String[] regions = stRef.toString().split(" ");
            for (int i = 0; i < regions.length; ++i) {
                lst.add(CellRangeAddress.valueOf(regions[i]));
            }
        }
        return lst.toArray(new CellRangeAddress[lst.size()]);
    }
    
    public void setRule(final int idx, final ConditionalFormattingRule cfRule) {
        final XSSFConditionalFormattingRule xRule = (XSSFConditionalFormattingRule)cfRule;
        this._cf.getCfRuleArray(idx).set(xRule.getCTCfRule());
    }
    
    public void addRule(final ConditionalFormattingRule cfRule) {
        final XSSFConditionalFormattingRule xRule = (XSSFConditionalFormattingRule)cfRule;
        this._cf.addNewCfRule().set(xRule.getCTCfRule());
    }
    
    public XSSFConditionalFormattingRule getRule(final int idx) {
        return new XSSFConditionalFormattingRule(this._sh, this._cf.getCfRuleArray(idx));
    }
    
    public int getNumberOfRules() {
        return this._cf.sizeOfCfRuleArray();
    }
}
