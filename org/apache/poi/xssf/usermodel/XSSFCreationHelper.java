// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;

public class XSSFCreationHelper implements CreationHelper
{
    private XSSFWorkbook workbook;
    
    XSSFCreationHelper(final XSSFWorkbook wb) {
        this.workbook = wb;
    }
    
    public XSSFRichTextString createRichTextString(final String text) {
        final XSSFRichTextString rt = new XSSFRichTextString(text);
        rt.setStylesTableReference(this.workbook.getStylesSource());
        return rt;
    }
    
    public XSSFDataFormat createDataFormat() {
        return this.workbook.createDataFormat();
    }
    
    public XSSFHyperlink createHyperlink(final int type) {
        return new XSSFHyperlink(type);
    }
    
    public XSSFFormulaEvaluator createFormulaEvaluator() {
        return new XSSFFormulaEvaluator(this.workbook);
    }
    
    public XSSFClientAnchor createClientAnchor() {
        return new XSSFClientAnchor();
    }
}
