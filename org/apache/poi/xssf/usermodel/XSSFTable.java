// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import java.util.Vector;
import java.util.Arrays;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumn;
import java.util.Iterator;
import java.io.OutputStream;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.TableDocument;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.helpers.XSSFXmlColumnPr;
import java.util.List;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;
import org.apache.poi.POIXMLDocumentPart;

public class XSSFTable extends POIXMLDocumentPart
{
    private CTTable ctTable;
    private List<XSSFXmlColumnPr> xmlColumnPr;
    private CellReference startCellReference;
    private CellReference endCellReference;
    private String commonXPath;
    
    public XSSFTable() {
        this.ctTable = CTTable.Factory.newInstance();
    }
    
    public XSSFTable(final PackagePart part, final PackageRelationship rel) throws IOException {
        super(part, rel);
        this.readFrom(part.getInputStream());
    }
    
    public void readFrom(final InputStream is) throws IOException {
        try {
            final TableDocument doc = TableDocument.Factory.parse(is);
            this.ctTable = doc.getTable();
        }
        catch (XmlException e) {
            throw new IOException(e.getLocalizedMessage());
        }
    }
    
    public XSSFSheet getXSSFSheet() {
        return (XSSFSheet)this.getParent();
    }
    
    public void writeTo(final OutputStream out) throws IOException {
        final TableDocument doc = TableDocument.Factory.newInstance();
        doc.setTable(this.ctTable);
        doc.save(out, XSSFTable.DEFAULT_XML_OPTIONS);
    }
    
    @Override
    protected void commit() throws IOException {
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.writeTo(out);
        out.close();
    }
    
    public CTTable getCTTable() {
        return this.ctTable;
    }
    
    public boolean mapsTo(final long id) {
        boolean maps = false;
        final List<XSSFXmlColumnPr> pointers = this.getXmlColumnPrs();
        for (final XSSFXmlColumnPr pointer : pointers) {
            if (pointer.getMapId() == id) {
                maps = true;
                break;
            }
        }
        return maps;
    }
    
    public String getCommonXpath() {
        if (this.commonXPath == null) {
            String[] commonTokens = new String[0];
            for (final CTTableColumn column : this.ctTable.getTableColumns().getTableColumnList()) {
                if (column.getXmlColumnPr() != null) {
                    final String xpath = column.getXmlColumnPr().getXpath();
                    final String[] tokens = xpath.split("/");
                    if (commonTokens.length == 0) {
                        commonTokens = tokens;
                    }
                    else {
                        for (int maxLenght = (commonTokens.length > tokens.length) ? tokens.length : commonTokens.length, i = 0; i < maxLenght; ++i) {
                            if (!commonTokens[i].equals(tokens[i])) {
                                final List<String> subCommonTokens = Arrays.asList(commonTokens).subList(0, i);
                                final String[] container = new String[0];
                                commonTokens = subCommonTokens.toArray(container);
                                break;
                            }
                        }
                    }
                }
            }
            this.commonXPath = "";
            for (int j = 1; j < commonTokens.length; ++j) {
                this.commonXPath = this.commonXPath + "/" + commonTokens[j];
            }
        }
        return this.commonXPath;
    }
    
    public List<XSSFXmlColumnPr> getXmlColumnPrs() {
        if (this.xmlColumnPr == null) {
            this.xmlColumnPr = new Vector<XSSFXmlColumnPr>();
            for (final CTTableColumn column : this.ctTable.getTableColumns().getTableColumnList()) {
                if (column.getXmlColumnPr() != null) {
                    final XSSFXmlColumnPr columnPr = new XSSFXmlColumnPr(this, column, column.getXmlColumnPr());
                    this.xmlColumnPr.add(columnPr);
                }
            }
        }
        return this.xmlColumnPr;
    }
    
    public String getName() {
        return this.ctTable.getName();
    }
    
    public void setName(final String name) {
        if (name == null) {
            this.ctTable.unsetName();
            return;
        }
        this.ctTable.setName(name);
    }
    
    public String getDisplayName() {
        return this.ctTable.getDisplayName();
    }
    
    public void setDisplayName(final String name) {
        this.ctTable.setDisplayName(name);
    }
    
    public long getNumerOfMappedColumns() {
        return this.ctTable.getTableColumns().getCount();
    }
    
    public CellReference getStartCellReference() {
        if (this.startCellReference == null) {
            final String ref = this.ctTable.getRef();
            final String[] boundaries = ref.split(":");
            final String from = boundaries[0];
            this.startCellReference = new CellReference(from);
        }
        return this.startCellReference;
    }
    
    public CellReference getEndCellReference() {
        if (this.endCellReference == null) {
            final String ref = this.ctTable.getRef();
            final String[] boundaries = ref.split(":");
            final String from = boundaries[1];
            this.endCellReference = new CellReference(from);
        }
        return this.endCellReference;
    }
    
    public int getRowCount() {
        final CellReference from = this.getStartCellReference();
        final CellReference to = this.getEndCellReference();
        int rowCount = -1;
        if (from != null && to != null) {
            rowCount = to.getRow() - from.getRow();
        }
        return rowCount;
    }
}
