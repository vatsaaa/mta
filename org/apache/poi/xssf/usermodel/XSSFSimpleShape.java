// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSolidColorFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextUnderlineType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STUnderlineValues;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRPrElt;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRElt;
import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextCharacterProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBodyProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.drawingml.x2006.main.CTFontReference;
import org.openxmlformats.schemas.drawingml.x2006.main.CTStyleMatrixReference;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetGeometry2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTShapeNonVisual;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextAlignType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextAnchoringType;
import org.openxmlformats.schemas.drawingml.x2006.main.STFontCollectionIndex;
import org.openxmlformats.schemas.drawingml.x2006.main.STSchemeColorVal;
import org.openxmlformats.schemas.drawingml.x2006.main.STShapeType;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTShape;

public class XSSFSimpleShape extends XSSFShape
{
    private static CTShape prototype;
    private CTShape ctShape;
    
    protected XSSFSimpleShape(final XSSFDrawing drawing, final CTShape ctShape) {
        this.drawing = drawing;
        this.ctShape = ctShape;
    }
    
    protected static CTShape prototype() {
        if (XSSFSimpleShape.prototype == null) {
            final CTShape shape = CTShape.Factory.newInstance();
            final CTShapeNonVisual nv = shape.addNewNvSpPr();
            final CTNonVisualDrawingProps nvp = nv.addNewCNvPr();
            nvp.setId(1L);
            nvp.setName("Shape 1");
            nv.addNewCNvSpPr();
            final CTShapeProperties sp = shape.addNewSpPr();
            final CTTransform2D t2d = sp.addNewXfrm();
            final CTPositiveSize2D p1 = t2d.addNewExt();
            p1.setCx(0L);
            p1.setCy(0L);
            final CTPoint2D p2 = t2d.addNewOff();
            p2.setX(0L);
            p2.setY(0L);
            final CTPresetGeometry2D geom = sp.addNewPrstGeom();
            geom.setPrst(STShapeType.RECT);
            geom.addNewAvLst();
            final CTShapeStyle style = shape.addNewStyle();
            final CTSchemeColor scheme = style.addNewLnRef().addNewSchemeClr();
            scheme.setVal(STSchemeColorVal.ACCENT_1);
            scheme.addNewShade().setVal(50000);
            style.getLnRef().setIdx(2L);
            final CTStyleMatrixReference fillref = style.addNewFillRef();
            fillref.setIdx(1L);
            fillref.addNewSchemeClr().setVal(STSchemeColorVal.ACCENT_1);
            final CTStyleMatrixReference effectRef = style.addNewEffectRef();
            effectRef.setIdx(0L);
            effectRef.addNewSchemeClr().setVal(STSchemeColorVal.ACCENT_1);
            final CTFontReference fontRef = style.addNewFontRef();
            fontRef.setIdx(STFontCollectionIndex.MINOR);
            fontRef.addNewSchemeClr().setVal(STSchemeColorVal.LT_1);
            final CTTextBody body = shape.addNewTxBody();
            final CTTextBodyProperties bodypr = body.addNewBodyPr();
            bodypr.setAnchor(STTextAnchoringType.CTR);
            bodypr.setRtlCol(false);
            final CTTextParagraph p3 = body.addNewP();
            p3.addNewPPr().setAlgn(STTextAlignType.CTR);
            final CTTextCharacterProperties endPr = p3.addNewEndParaRPr();
            endPr.setLang("en-US");
            endPr.setSz(1100);
            body.addNewLstStyle();
            XSSFSimpleShape.prototype = shape;
        }
        return XSSFSimpleShape.prototype;
    }
    
    @Internal
    public CTShape getCTShape() {
        return this.ctShape;
    }
    
    public int getShapeType() {
        return this.ctShape.getSpPr().getPrstGeom().getPrst().intValue();
    }
    
    public void setShapeType(final int type) {
        this.ctShape.getSpPr().getPrstGeom().setPrst(STShapeType.Enum.forInt(type));
    }
    
    @Override
    protected CTShapeProperties getShapeProperties() {
        return this.ctShape.getSpPr();
    }
    
    public void setText(final XSSFRichTextString str) {
        final XSSFWorkbook wb = (XSSFWorkbook)this.getDrawing().getParent().getParent();
        str.setStylesTableReference(wb.getStylesSource());
        final CTTextParagraph p = CTTextParagraph.Factory.newInstance();
        if (str.numFormattingRuns() == 0) {
            final CTRegularTextRun r = p.addNewR();
            final CTTextCharacterProperties rPr = r.addNewRPr();
            rPr.setLang("en-US");
            rPr.setSz(1100);
            r.setT(str.getString());
        }
        else {
            for (int i = 0; i < str.getCTRst().sizeOfRArray(); ++i) {
                final CTRElt lt = str.getCTRst().getRArray(i);
                CTRPrElt ltPr = lt.getRPr();
                if (ltPr == null) {
                    ltPr = lt.addNewRPr();
                }
                final CTRegularTextRun r2 = p.addNewR();
                final CTTextCharacterProperties rPr2 = r2.addNewRPr();
                rPr2.setLang("en-US");
                applyAttributes(ltPr, rPr2);
                r2.setT(lt.getT());
            }
        }
        this.ctShape.getTxBody().setPArray(new CTTextParagraph[] { p });
    }
    
    private static void applyAttributes(final CTRPrElt pr, final CTTextCharacterProperties rPr) {
        if (pr.sizeOfBArray() > 0) {
            rPr.setB(pr.getBArray(0).getVal());
        }
        if (pr.sizeOfUArray() > 0) {
            final STUnderlineValues.Enum u1 = pr.getUArray(0).getVal();
            if (u1 == STUnderlineValues.SINGLE) {
                rPr.setU(STTextUnderlineType.SNG);
            }
            else if (u1 == STUnderlineValues.DOUBLE) {
                rPr.setU(STTextUnderlineType.DBL);
            }
            else if (u1 == STUnderlineValues.NONE) {
                rPr.setU(STTextUnderlineType.NONE);
            }
        }
        if (pr.sizeOfIArray() > 0) {
            rPr.setI(pr.getIArray(0).getVal());
        }
        if (pr.sizeOfFamilyArray() > 0) {
            final CTTextFont rFont = rPr.addNewLatin();
            rFont.setTypeface(pr.getRFontArray(0).getVal());
        }
        if (pr.sizeOfSzArray() > 0) {
            final int sz = (int)(pr.getSzArray(0).getVal() * 100.0);
            rPr.setSz(sz);
        }
        if (pr.sizeOfColorArray() > 0) {
            final CTSolidColorFillProperties fill = rPr.isSetSolidFill() ? rPr.getSolidFill() : rPr.addNewSolidFill();
            final CTColor xlsColor = pr.getColorArray(0);
            if (xlsColor.isSetRgb()) {
                final CTSRgbColor clr = fill.isSetSrgbClr() ? fill.getSrgbClr() : fill.addNewSrgbClr();
                clr.setVal(xlsColor.getRgb());
            }
            else if (xlsColor.isSetIndexed()) {
                final HSSFColor indexed = HSSFColor.getIndexHash().get((int)xlsColor.getIndexed());
                if (indexed != null) {
                    final byte[] rgb = { (byte)indexed.getTriplet()[0], (byte)indexed.getTriplet()[1], (byte)indexed.getTriplet()[2] };
                    final CTSRgbColor clr2 = fill.isSetSrgbClr() ? fill.getSrgbClr() : fill.addNewSrgbClr();
                    clr2.setVal(rgb);
                }
            }
        }
    }
    
    static {
        XSSFSimpleShape.prototype = null;
    }
}
