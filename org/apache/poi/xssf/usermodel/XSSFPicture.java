// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.PictureData;
import java.util.Iterator;
import org.apache.poi.POIXMLDocumentPart;
import java.io.IOException;
import org.apache.poi.ss.util.ImageUtils;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCol;
import java.awt.Dimension;
import org.apache.poi.util.Internal;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetGeometry2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlipFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualPictureProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTPictureNonVisual;
import org.openxmlformats.schemas.drawingml.x2006.main.STShapeType;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTPicture;
import org.apache.poi.util.POILogger;
import org.apache.poi.ss.usermodel.Picture;

public final class XSSFPicture extends XSSFShape implements Picture
{
    private static final POILogger logger;
    private static float DEFAULT_COLUMN_WIDTH;
    private static CTPicture prototype;
    private CTPicture ctPicture;
    
    protected XSSFPicture(final XSSFDrawing drawing, final CTPicture ctPicture) {
        this.drawing = drawing;
        this.ctPicture = ctPicture;
    }
    
    protected static CTPicture prototype() {
        if (XSSFPicture.prototype == null) {
            final CTPicture pic = CTPicture.Factory.newInstance();
            final CTPictureNonVisual nvpr = pic.addNewNvPicPr();
            final CTNonVisualDrawingProps nvProps = nvpr.addNewCNvPr();
            nvProps.setId(1L);
            nvProps.setName("Picture 1");
            nvProps.setDescr("Picture");
            final CTNonVisualPictureProperties nvPicProps = nvpr.addNewCNvPicPr();
            nvPicProps.addNewPicLocks().setNoChangeAspect(true);
            final CTBlipFillProperties blip = pic.addNewBlipFill();
            blip.addNewBlip().setEmbed("");
            blip.addNewStretch().addNewFillRect();
            final CTShapeProperties sppr = pic.addNewSpPr();
            final CTTransform2D t2d = sppr.addNewXfrm();
            final CTPositiveSize2D ext = t2d.addNewExt();
            ext.setCx(0L);
            ext.setCy(0L);
            final CTPoint2D off = t2d.addNewOff();
            off.setX(0L);
            off.setY(0L);
            final CTPresetGeometry2D prstGeom = sppr.addNewPrstGeom();
            prstGeom.setPrst(STShapeType.RECT);
            prstGeom.addNewAvLst();
            XSSFPicture.prototype = pic;
        }
        return XSSFPicture.prototype;
    }
    
    protected void setPictureReference(final PackageRelationship rel) {
        this.ctPicture.getBlipFill().getBlip().setEmbed(rel.getId());
    }
    
    @Internal
    public CTPicture getCTPicture() {
        return this.ctPicture;
    }
    
    public void resize() {
        this.resize(1.0);
    }
    
    public void resize(final double scale) {
        final XSSFClientAnchor anchor = (XSSFClientAnchor)this.getAnchor();
        final XSSFClientAnchor pref = this.getPreferredSize(scale);
        final int row2 = anchor.getRow1() + (pref.getRow2() - pref.getRow1());
        final int col2 = anchor.getCol1() + (pref.getCol2() - pref.getCol1());
        anchor.setCol2(col2);
        anchor.setDx1(0);
        anchor.setDx2(pref.getDx2());
        anchor.setRow2(row2);
        anchor.setDy1(0);
        anchor.setDy2(pref.getDy2());
    }
    
    public XSSFClientAnchor getPreferredSize() {
        return this.getPreferredSize(1.0);
    }
    
    public XSSFClientAnchor getPreferredSize(final double scale) {
        final XSSFClientAnchor anchor = (XSSFClientAnchor)this.getAnchor();
        final XSSFPictureData data = this.getPictureData();
        final Dimension size = getImageDimension(data.getPackagePart(), data.getPictureType());
        final double scaledWidth = size.getWidth() * scale;
        final double scaledHeight = size.getHeight() * scale;
        float w = 0.0f;
        int col2 = anchor.getCol1();
        int dx2 = 0;
        while (true) {
            w += this.getColumnWidthInPixels(col2);
            if (w > scaledWidth) {
                break;
            }
            ++col2;
        }
        if (w > scaledWidth) {
            final double cw = this.getColumnWidthInPixels(col2);
            final double delta = w - scaledWidth;
            dx2 = (int)(9525.0 * (cw - delta));
        }
        anchor.setCol2(col2);
        anchor.setDx2(dx2);
        double h = 0.0;
        int row2 = anchor.getRow1();
        int dy2 = 0;
        while (true) {
            h += this.getRowHeightInPixels(row2);
            if (h > scaledHeight) {
                break;
            }
            ++row2;
        }
        if (h > scaledHeight) {
            final double ch = this.getRowHeightInPixels(row2);
            final double delta2 = h - scaledHeight;
            dy2 = (int)(9525.0 * (ch - delta2));
        }
        anchor.setRow2(row2);
        anchor.setDy2(dy2);
        final CTPositiveSize2D size2d = this.ctPicture.getSpPr().getXfrm().getExt();
        size2d.setCx((long)(scaledWidth * 9525.0));
        size2d.setCy((long)(scaledHeight * 9525.0));
        return anchor;
    }
    
    private float getColumnWidthInPixels(final int columnIndex) {
        final XSSFSheet sheet = (XSSFSheet)this.getDrawing().getParent();
        final CTCol col = sheet.getColumnHelper().getColumn(columnIndex, false);
        final double numChars = (col == null || !col.isSetWidth()) ? XSSFPicture.DEFAULT_COLUMN_WIDTH : col.getWidth();
        return (float)numChars * 7.0017f;
    }
    
    private float getRowHeightInPixels(final int rowIndex) {
        final XSSFSheet sheet = (XSSFSheet)this.getDrawing().getParent();
        final XSSFRow row = sheet.getRow(rowIndex);
        final float height = (row != null) ? row.getHeightInPoints() : sheet.getDefaultRowHeightInPoints();
        return height * 96.0f / 72.0f;
    }
    
    protected static Dimension getImageDimension(final PackagePart part, final int type) {
        try {
            return ImageUtils.getImageDimension(part.getInputStream(), type);
        }
        catch (IOException e) {
            XSSFPicture.logger.log(POILogger.WARN, e);
            return new Dimension();
        }
    }
    
    public XSSFPictureData getPictureData() {
        final String blipId = this.ctPicture.getBlipFill().getBlip().getEmbed();
        for (final POIXMLDocumentPart part : this.getDrawing().getRelations()) {
            if (part.getPackageRelationship().getId().equals(blipId)) {
                return (XSSFPictureData)part;
            }
        }
        XSSFPicture.logger.log(POILogger.WARN, "Picture data was not found for blipId=" + blipId);
        return null;
    }
    
    @Override
    protected CTShapeProperties getShapeProperties() {
        return this.ctPicture.getSpPr();
    }
    
    static {
        logger = POILogFactory.getLogger(XSSFPicture.class);
        XSSFPicture.DEFAULT_COLUMN_WIDTH = 9.140625f;
        XSSFPicture.prototype = null;
    }
}
