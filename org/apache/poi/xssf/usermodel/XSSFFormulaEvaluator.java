// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.formula.eval.ValueEval;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.formula.eval.StringEval;
import org.apache.poi.ss.formula.eval.BoolEval;
import org.apache.poi.ss.formula.eval.NumberEval;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.formula.EvaluationCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.formula.EvaluationWorkbook;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.ss.formula.IStabilityClassifier;
import org.apache.poi.ss.formula.WorkbookEvaluator;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

public class XSSFFormulaEvaluator implements FormulaEvaluator
{
    private WorkbookEvaluator _bookEvaluator;
    private XSSFWorkbook _book;
    
    public XSSFFormulaEvaluator(final XSSFWorkbook workbook) {
        this(workbook, null, null);
    }
    
    @Deprecated
    public XSSFFormulaEvaluator(final XSSFWorkbook workbook, final IStabilityClassifier stabilityClassifier) {
        this._bookEvaluator = new WorkbookEvaluator(XSSFEvaluationWorkbook.create(workbook), stabilityClassifier, null);
        this._book = workbook;
    }
    
    private XSSFFormulaEvaluator(final XSSFWorkbook workbook, final IStabilityClassifier stabilityClassifier, final UDFFinder udfFinder) {
        this._bookEvaluator = new WorkbookEvaluator(XSSFEvaluationWorkbook.create(workbook), stabilityClassifier, udfFinder);
        this._book = workbook;
    }
    
    public static XSSFFormulaEvaluator create(final XSSFWorkbook workbook, final IStabilityClassifier stabilityClassifier, final UDFFinder udfFinder) {
        return new XSSFFormulaEvaluator(workbook, stabilityClassifier, udfFinder);
    }
    
    public void clearAllCachedResultValues() {
        this._bookEvaluator.clearAllCachedResultValues();
    }
    
    public void notifySetFormula(final Cell cell) {
        this._bookEvaluator.notifyUpdateCell(new XSSFEvaluationCell((XSSFCell)cell));
    }
    
    public void notifyDeleteCell(final Cell cell) {
        this._bookEvaluator.notifyDeleteCell(new XSSFEvaluationCell((XSSFCell)cell));
    }
    
    public void notifyUpdateCell(final Cell cell) {
        this._bookEvaluator.notifyUpdateCell(new XSSFEvaluationCell((XSSFCell)cell));
    }
    
    public CellValue evaluate(final Cell cell) {
        if (cell == null) {
            return null;
        }
        switch (cell.getCellType()) {
            case 4: {
                return CellValue.valueOf(cell.getBooleanCellValue());
            }
            case 5: {
                return CellValue.getError(cell.getErrorCellValue());
            }
            case 2: {
                return this.evaluateFormulaCellValue(cell);
            }
            case 0: {
                return new CellValue(cell.getNumericCellValue());
            }
            case 1: {
                return new CellValue(cell.getRichStringCellValue().getString());
            }
            case 3: {
                return null;
            }
            default: {
                throw new IllegalStateException("Bad cell type (" + cell.getCellType() + ")");
            }
        }
    }
    
    public int evaluateFormulaCell(final Cell cell) {
        if (cell == null || cell.getCellType() != 2) {
            return -1;
        }
        final CellValue cv = this.evaluateFormulaCellValue(cell);
        setCellValue(cell, cv);
        return cv.getCellType();
    }
    
    public XSSFCell evaluateInCell(final Cell cell) {
        if (cell == null) {
            return null;
        }
        final XSSFCell result = (XSSFCell)cell;
        if (cell.getCellType() == 2) {
            final CellValue cv = this.evaluateFormulaCellValue(cell);
            setCellType(cell, cv);
            setCellValue(cell, cv);
        }
        return result;
    }
    
    private static void setCellType(final Cell cell, final CellValue cv) {
        final int cellType = cv.getCellType();
        switch (cellType) {
            case 0:
            case 1:
            case 4:
            case 5: {
                cell.setCellType(cellType);
            }
            default: {
                throw new IllegalStateException("Unexpected cell value type (" + cellType + ")");
            }
        }
    }
    
    private static void setCellValue(final Cell cell, final CellValue cv) {
        final int cellType = cv.getCellType();
        switch (cellType) {
            case 4: {
                cell.setCellValue(cv.getBooleanValue());
                break;
            }
            case 5: {
                cell.setCellErrorValue(cv.getErrorValue());
                break;
            }
            case 0: {
                cell.setCellValue(cv.getNumberValue());
                break;
            }
            case 1: {
                cell.setCellValue(new XSSFRichTextString(cv.getStringValue()));
                break;
            }
            default: {
                throw new IllegalStateException("Unexpected cell value type (" + cellType + ")");
            }
        }
    }
    
    public static void evaluateAllFormulaCells(final XSSFWorkbook wb) {
        HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
    }
    
    public void evaluateAll() {
        HSSFFormulaEvaluator.evaluateAllFormulaCells(this._book);
    }
    
    private CellValue evaluateFormulaCellValue(final Cell cell) {
        if (!(cell instanceof XSSFCell)) {
            throw new IllegalArgumentException("Unexpected type of cell: " + cell.getClass() + "." + " Only XSSFCells can be evaluated.");
        }
        final ValueEval eval = this._bookEvaluator.evaluate(new XSSFEvaluationCell((XSSFCell)cell));
        if (eval instanceof NumberEval) {
            final NumberEval ne = (NumberEval)eval;
            return new CellValue(ne.getNumberValue());
        }
        if (eval instanceof BoolEval) {
            final BoolEval be = (BoolEval)eval;
            return CellValue.valueOf(be.getBooleanValue());
        }
        if (eval instanceof StringEval) {
            final StringEval ne2 = (StringEval)eval;
            return new CellValue(ne2.getStringValue());
        }
        if (eval instanceof ErrorEval) {
            return CellValue.getError(((ErrorEval)eval).getErrorCode());
        }
        throw new RuntimeException("Unexpected eval class (" + eval.getClass().getName() + ")");
    }
}
