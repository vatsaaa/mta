// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel.extensions;

import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPatternType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPatternFill;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFill;

public final class XSSFCellFill
{
    private CTFill _fill;
    
    public XSSFCellFill(final CTFill fill) {
        this._fill = fill;
    }
    
    public XSSFCellFill() {
        this._fill = CTFill.Factory.newInstance();
    }
    
    public XSSFColor getFillBackgroundColor() {
        final CTPatternFill ptrn = this._fill.getPatternFill();
        if (ptrn == null) {
            return null;
        }
        final CTColor ctColor = ptrn.getBgColor();
        return (ctColor == null) ? null : new XSSFColor(ctColor);
    }
    
    public void setFillBackgroundColor(final int index) {
        final CTPatternFill ptrn = this.ensureCTPatternFill();
        final CTColor ctColor = ptrn.isSetBgColor() ? ptrn.getBgColor() : ptrn.addNewBgColor();
        ctColor.setIndexed(index);
    }
    
    public void setFillBackgroundColor(final XSSFColor color) {
        final CTPatternFill ptrn = this.ensureCTPatternFill();
        ptrn.setBgColor(color.getCTColor());
    }
    
    public XSSFColor getFillForegroundColor() {
        final CTPatternFill ptrn = this._fill.getPatternFill();
        if (ptrn == null) {
            return null;
        }
        final CTColor ctColor = ptrn.getFgColor();
        return (ctColor == null) ? null : new XSSFColor(ctColor);
    }
    
    public void setFillForegroundColor(final int index) {
        final CTPatternFill ptrn = this.ensureCTPatternFill();
        final CTColor ctColor = ptrn.isSetFgColor() ? ptrn.getFgColor() : ptrn.addNewFgColor();
        ctColor.setIndexed(index);
    }
    
    public void setFillForegroundColor(final XSSFColor color) {
        final CTPatternFill ptrn = this.ensureCTPatternFill();
        ptrn.setFgColor(color.getCTColor());
    }
    
    public STPatternType.Enum getPatternType() {
        final CTPatternFill ptrn = this._fill.getPatternFill();
        return (ptrn == null) ? null : ptrn.getPatternType();
    }
    
    public void setPatternType(final STPatternType.Enum patternType) {
        final CTPatternFill ptrn = this.ensureCTPatternFill();
        ptrn.setPatternType(patternType);
    }
    
    private CTPatternFill ensureCTPatternFill() {
        CTPatternFill patternFill = this._fill.getPatternFill();
        if (patternFill == null) {
            patternFill = this._fill.addNewPatternFill();
        }
        return patternFill;
    }
    
    @Internal
    public CTFill getCTFill() {
        return this._fill;
    }
    
    @Override
    public int hashCode() {
        return this._fill.toString().hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof XSSFCellFill)) {
            return false;
        }
        final XSSFCellFill cf = (XSSFCellFill)o;
        return this._fill.toString().equals(cf.getCTFill().toString());
    }
}
