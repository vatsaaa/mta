// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.util.Internal;
import org.apache.poi.hssf.util.HSSFColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import org.apache.poi.ss.usermodel.Color;

public class XSSFColor implements Color
{
    private CTColor ctColor;
    
    public XSSFColor(final CTColor color) {
        this.ctColor = color;
    }
    
    public XSSFColor() {
        this.ctColor = CTColor.Factory.newInstance();
    }
    
    public XSSFColor(final java.awt.Color clr) {
        this();
        this.ctColor.setRgb(new byte[] { (byte)clr.getRed(), (byte)clr.getGreen(), (byte)clr.getBlue() });
    }
    
    public XSSFColor(final byte[] rgb) {
        this();
        this.ctColor.setRgb(rgb);
    }
    
    public boolean isAuto() {
        return this.ctColor.getAuto();
    }
    
    public void setAuto(final boolean auto) {
        this.ctColor.setAuto(auto);
    }
    
    public short getIndexed() {
        return (short)this.ctColor.getIndexed();
    }
    
    public void setIndexed(final int indexed) {
        this.ctColor.setIndexed(indexed);
    }
    
    private byte[] correctRGB(byte[] rgb) {
        if (rgb.length == 4) {
            return rgb;
        }
        if (rgb[0] == 0 && rgb[1] == 0 && rgb[2] == 0) {
            rgb = new byte[] { -1, -1, -1 };
        }
        else if (rgb[0] == -1 && rgb[1] == -1 && rgb[2] == -1) {
            rgb = new byte[] { 0, 0, 0 };
        }
        return rgb;
    }
    
    public byte[] getRgb() {
        final byte[] rgb = this.getRGBOrARGB();
        if (rgb == null) {
            return null;
        }
        if (rgb.length == 4) {
            final byte[] tmp = new byte[3];
            System.arraycopy(rgb, 1, tmp, 0, 3);
            return tmp;
        }
        return rgb;
    }
    
    public byte[] getARgb() {
        final byte[] rgb = this.getRGBOrARGB();
        if (rgb == null) {
            return null;
        }
        if (rgb.length == 3) {
            final byte[] tmp = new byte[4];
            tmp[0] = -1;
            System.arraycopy(rgb, 0, tmp, 1, 3);
            return tmp;
        }
        return rgb;
    }
    
    private byte[] getRGBOrARGB() {
        byte[] rgb = null;
        if (this.ctColor.isSetIndexed() && this.ctColor.getIndexed() > 0L) {
            final HSSFColor indexed = HSSFColor.getIndexHash().get((int)this.ctColor.getIndexed());
            if (indexed != null) {
                rgb = new byte[] { (byte)indexed.getTriplet()[0], (byte)indexed.getTriplet()[1], (byte)indexed.getTriplet()[2] };
                return rgb;
            }
        }
        if (!this.ctColor.isSetRgb()) {
            return null;
        }
        rgb = this.ctColor.getRgb();
        return this.correctRGB(rgb);
    }
    
    public byte[] getRgbWithTint() {
        byte[] rgb = this.ctColor.getRgb();
        if (rgb != null) {
            if (rgb.length == 4) {
                final byte[] tmp = new byte[3];
                System.arraycopy(rgb, 1, tmp, 0, 3);
                rgb = tmp;
            }
            for (int i = 0; i < rgb.length; ++i) {
                rgb[i] = applyTint(rgb[i] & 0xFF, this.ctColor.getTint());
            }
        }
        return rgb;
    }
    
    public String getARGBHex() {
        final StringBuffer sb = new StringBuffer();
        final byte[] rgb = this.getARgb();
        if (rgb == null) {
            return null;
        }
        for (final int i : rgb) {
            final byte c = (byte)i;
            if (i < 0) {
                i += 256;
            }
            final String cs = Integer.toHexString(i);
            if (cs.length() == 1) {
                sb.append('0');
            }
            sb.append(cs);
        }
        return sb.toString().toUpperCase();
    }
    
    private static byte applyTint(final int lum, final double tint) {
        if (tint > 0.0) {
            return (byte)(lum * (1.0 - tint) + (255.0 - 255.0 * (1.0 - tint)));
        }
        if (tint < 0.0) {
            return (byte)(lum * (1.0 + tint));
        }
        return (byte)lum;
    }
    
    public void setRgb(final byte[] rgb) {
        this.ctColor.setRgb(this.correctRGB(rgb));
    }
    
    public int getTheme() {
        return (int)this.ctColor.getTheme();
    }
    
    public void setTheme(final int theme) {
        this.ctColor.setTheme(theme);
    }
    
    public double getTint() {
        return this.ctColor.getTint();
    }
    
    public void setTint(final double tint) {
        this.ctColor.setTint(tint);
    }
    
    @Internal
    public CTColor getCTColor() {
        return this.ctColor;
    }
    
    @Override
    public int hashCode() {
        return this.ctColor.toString().hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof XSSFColor)) {
            return false;
        }
        final XSSFColor cf = (XSSFColor)o;
        return this.ctColor.toString().equals(cf.getCTColor().toString());
    }
}
