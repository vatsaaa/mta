// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.formula.EvaluationCell;
import org.apache.poi.ss.formula.EvaluationSheet;

final class XSSFEvaluationSheet implements EvaluationSheet
{
    private final XSSFSheet _xs;
    
    public XSSFEvaluationSheet(final XSSFSheet sheet) {
        this._xs = sheet;
    }
    
    public XSSFSheet getXSSFSheet() {
        return this._xs;
    }
    
    public EvaluationCell getCell(final int rowIndex, final int columnIndex) {
        final XSSFRow row = this._xs.getRow(rowIndex);
        if (row == null) {
            return null;
        }
        final XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            return null;
        }
        return new XSSFEvaluationCell(cell, this);
    }
}
