// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.Chart;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.STEditAs;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTGraphicalObjectFrame;
import org.apache.poi.xssf.model.CommentsTable;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTGroupShape;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTConnector;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.POIXMLFactory;
import org.apache.poi.POIXMLRelation;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTPicture;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTShape;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTTwoCellAnchor;
import org.apache.xmlbeans.XmlObject;
import java.io.OutputStream;
import java.util.Map;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import java.util.HashMap;
import org.apache.poi.util.Internal;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTDrawing;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.POIXMLDocumentPart;

public final class XSSFDrawing extends POIXMLDocumentPart implements Drawing
{
    private CTDrawing drawing;
    private long numOfGraphicFrames;
    protected static final String NAMESPACE_A = "http://schemas.openxmlformats.org/drawingml/2006/main";
    protected static final String NAMESPACE_C = "http://schemas.openxmlformats.org/drawingml/2006/chart";
    
    protected XSSFDrawing() {
        this.numOfGraphicFrames = 0L;
        this.drawing = newDrawing();
    }
    
    protected XSSFDrawing(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        this.numOfGraphicFrames = 0L;
        final XmlOptions options = new XmlOptions(XSSFDrawing.DEFAULT_XML_OPTIONS);
        options.setLoadReplaceDocumentElement(null);
        this.drawing = CTDrawing.Factory.parse(part.getInputStream(), options);
    }
    
    private static CTDrawing newDrawing() {
        return CTDrawing.Factory.newInstance();
    }
    
    @Internal
    public CTDrawing getCTDrawing() {
        return this.drawing;
    }
    
    @Override
    protected void commit() throws IOException {
        final XmlOptions xmlOptions = new XmlOptions(XSSFDrawing.DEFAULT_XML_OPTIONS);
        xmlOptions.setSaveSyntheticDocumentElement(new QName(CTDrawing.type.getName().getNamespaceURI(), "wsDr", "xdr"));
        final Map<String, String> map = new HashMap<String, String>();
        map.put("http://schemas.openxmlformats.org/drawingml/2006/main", "a");
        map.put(STRelationshipId.type.getName().getNamespaceURI(), "r");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.drawing.save(out, xmlOptions);
        out.close();
    }
    
    public XSSFClientAnchor createAnchor(final int dx1, final int dy1, final int dx2, final int dy2, final int col1, final int row1, final int col2, final int row2) {
        return new XSSFClientAnchor(dx1, dy1, dx2, dy2, col1, row1, col2, row2);
    }
    
    public XSSFTextBox createTextbox(final XSSFClientAnchor anchor) {
        final long shapeId = this.newShapeId();
        final CTTwoCellAnchor ctAnchor = this.createTwoCellAnchor(anchor);
        final CTShape ctShape = ctAnchor.addNewSp();
        ctShape.set(XSSFSimpleShape.prototype());
        ctShape.getNvSpPr().getCNvPr().setId(shapeId);
        final XSSFTextBox shape = new XSSFTextBox(this, ctShape);
        shape.anchor = anchor;
        return shape;
    }
    
    public XSSFPicture createPicture(final XSSFClientAnchor anchor, final int pictureIndex) {
        final PackageRelationship rel = this.addPictureReference(pictureIndex);
        final long shapeId = this.newShapeId();
        final CTTwoCellAnchor ctAnchor = this.createTwoCellAnchor(anchor);
        final CTPicture ctShape = ctAnchor.addNewPic();
        ctShape.set(XSSFPicture.prototype());
        ctShape.getNvPicPr().getCNvPr().setId(shapeId);
        final XSSFPicture shape = new XSSFPicture(this, ctShape);
        shape.anchor = anchor;
        shape.setPictureReference(rel);
        return shape;
    }
    
    public XSSFPicture createPicture(final ClientAnchor anchor, final int pictureIndex) {
        return this.createPicture((XSSFClientAnchor)anchor, pictureIndex);
    }
    
    public XSSFChart createChart(final XSSFClientAnchor anchor) {
        final int chartNumber = this.getPackagePart().getPackage().getPartsByContentType(XSSFRelation.CHART.getContentType()).size() + 1;
        final XSSFChart chart = (XSSFChart)this.createRelationship(XSSFRelation.CHART, XSSFFactory.getInstance(), chartNumber);
        final String chartRelId = chart.getPackageRelationship().getId();
        final XSSFGraphicFrame frame = this.createGraphicFrame(anchor);
        frame.setChart(chart, chartRelId);
        return chart;
    }
    
    public XSSFChart createChart(final ClientAnchor anchor) {
        return this.createChart((XSSFClientAnchor)anchor);
    }
    
    protected PackageRelationship addPictureReference(final int pictureIndex) {
        final XSSFWorkbook wb = (XSSFWorkbook)this.getParent().getParent();
        final XSSFPictureData data = wb.getAllPictures().get(pictureIndex);
        final PackagePartName ppName = data.getPackagePart().getPartName();
        final PackageRelationship rel = this.getPackagePart().addRelationship(ppName, TargetMode.INTERNAL, XSSFRelation.IMAGES.getRelation());
        this.addRelation(rel.getId(), new XSSFPictureData(data.getPackagePart(), rel));
        return rel;
    }
    
    public XSSFSimpleShape createSimpleShape(final XSSFClientAnchor anchor) {
        final long shapeId = this.newShapeId();
        final CTTwoCellAnchor ctAnchor = this.createTwoCellAnchor(anchor);
        final CTShape ctShape = ctAnchor.addNewSp();
        ctShape.set(XSSFSimpleShape.prototype());
        ctShape.getNvSpPr().getCNvPr().setId(shapeId);
        final XSSFSimpleShape shape = new XSSFSimpleShape(this, ctShape);
        shape.anchor = anchor;
        return shape;
    }
    
    public XSSFConnector createConnector(final XSSFClientAnchor anchor) {
        final CTTwoCellAnchor ctAnchor = this.createTwoCellAnchor(anchor);
        final CTConnector ctShape = ctAnchor.addNewCxnSp();
        ctShape.set(XSSFConnector.prototype());
        final XSSFConnector shape = new XSSFConnector(this, ctShape);
        shape.anchor = anchor;
        return shape;
    }
    
    public XSSFShapeGroup createGroup(final XSSFClientAnchor anchor) {
        final CTTwoCellAnchor ctAnchor = this.createTwoCellAnchor(anchor);
        final CTGroupShape ctGroup = ctAnchor.addNewGrpSp();
        ctGroup.set(XSSFShapeGroup.prototype());
        final XSSFShapeGroup shape = new XSSFShapeGroup(this, ctGroup);
        shape.anchor = anchor;
        return shape;
    }
    
    public XSSFComment createCellComment(final ClientAnchor anchor) {
        final XSSFClientAnchor ca = (XSSFClientAnchor)anchor;
        final XSSFSheet sheet = (XSSFSheet)this.getParent();
        final CommentsTable comments = sheet.getCommentsTable(true);
        final XSSFVMLDrawing vml = sheet.getVMLDrawing(true);
        final schemasMicrosoftComVml.CTShape vmlShape = vml.newCommentShape();
        if (ca.isSet()) {
            final String position = ca.getCol1() + ", 0, " + ca.getRow1() + ", 0, " + ca.getCol2() + ", 0, " + ca.getRow2() + ", 0";
            vmlShape.getClientDataArray(0).setAnchorArray(0, position);
        }
        final XSSFComment shape = new XSSFComment(comments, comments.newComment(), vmlShape);
        shape.setColumn(ca.getCol1());
        shape.setRow(ca.getRow1());
        return shape;
    }
    
    private XSSFGraphicFrame createGraphicFrame(final XSSFClientAnchor anchor) {
        final CTTwoCellAnchor ctAnchor = this.createTwoCellAnchor(anchor);
        final CTGraphicalObjectFrame ctGraphicFrame = ctAnchor.addNewGraphicFrame();
        ctGraphicFrame.set(XSSFGraphicFrame.prototype());
        final long frameId = this.numOfGraphicFrames++;
        final XSSFGraphicFrame graphicFrame = new XSSFGraphicFrame(this, ctGraphicFrame);
        graphicFrame.setAnchor(anchor);
        graphicFrame.setId(frameId);
        graphicFrame.setName("Diagramm" + frameId);
        return graphicFrame;
    }
    
    public List<XSSFChart> getCharts() {
        final List<XSSFChart> charts = new ArrayList<XSSFChart>();
        for (final POIXMLDocumentPart part : this.getRelations()) {
            if (part instanceof XSSFChart) {
                charts.add((XSSFChart)part);
            }
        }
        return charts;
    }
    
    private CTTwoCellAnchor createTwoCellAnchor(final XSSFClientAnchor anchor) {
        final CTTwoCellAnchor ctAnchor = this.drawing.addNewTwoCellAnchor();
        ctAnchor.setFrom(anchor.getFrom());
        ctAnchor.setTo(anchor.getTo());
        ctAnchor.addNewClientData();
        anchor.setTo(ctAnchor.getTo());
        anchor.setFrom(ctAnchor.getFrom());
        STEditAs.Enum aditAs = null;
        switch (anchor.getAnchorType()) {
            case 3: {
                aditAs = STEditAs.ABSOLUTE;
                break;
            }
            case 0: {
                aditAs = STEditAs.TWO_CELL;
                break;
            }
            case 2: {
                aditAs = STEditAs.ONE_CELL;
                break;
            }
            default: {
                aditAs = STEditAs.ONE_CELL;
                break;
            }
        }
        ctAnchor.setEditAs(aditAs);
        return ctAnchor;
    }
    
    private long newShapeId() {
        return this.drawing.sizeOfTwoCellAnchorArray() + 1;
    }
    
    public List<XSSFShape> getShapes() {
        final List<XSSFShape> lst = new ArrayList<XSSFShape>();
        for (final XmlObject obj : this.drawing.selectPath("./*/*")) {
            if (obj instanceof CTPicture) {
                lst.add(new XSSFPicture(this, (CTPicture)obj));
            }
            else if (obj instanceof CTConnector) {
                lst.add(new XSSFConnector(this, (CTConnector)obj));
            }
            else if (obj instanceof CTShape) {
                lst.add(new XSSFSimpleShape(this, (CTShape)obj));
            }
            else if (obj instanceof CTGraphicalObjectFrame) {
                lst.add(new XSSFGraphicFrame(this, (CTGraphicalObjectFrame)obj));
            }
            else if (obj instanceof CTGroupShape) {
                lst.add(new XSSFShapeGroup(this, (CTGroupShape)obj));
            }
        }
        return lst;
    }
}
