// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPatternType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPatternFill;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFill;
import org.apache.poi.ss.usermodel.PatternFormatting;

public class XSSFPatternFormatting implements PatternFormatting
{
    CTFill _fill;
    
    XSSFPatternFormatting(final CTFill fill) {
        this._fill = fill;
    }
    
    public short getFillBackgroundColor() {
        if (!this._fill.isSetPatternFill()) {
            return 0;
        }
        return (short)this._fill.getPatternFill().getBgColor().getIndexed();
    }
    
    public short getFillForegroundColor() {
        if (!this._fill.isSetPatternFill() || !this._fill.getPatternFill().isSetFgColor()) {
            return 0;
        }
        return (short)this._fill.getPatternFill().getFgColor().getIndexed();
    }
    
    public short getFillPattern() {
        if (!this._fill.isSetPatternFill() || !this._fill.getPatternFill().isSetPatternType()) {
            return 0;
        }
        return (short)(this._fill.getPatternFill().getPatternType().intValue() - 1);
    }
    
    public void setFillBackgroundColor(final short bg) {
        final CTPatternFill ptrn = this._fill.isSetPatternFill() ? this._fill.getPatternFill() : this._fill.addNewPatternFill();
        final CTColor bgColor = CTColor.Factory.newInstance();
        bgColor.setIndexed(bg);
        ptrn.setBgColor(bgColor);
    }
    
    public void setFillForegroundColor(final short fg) {
        final CTPatternFill ptrn = this._fill.isSetPatternFill() ? this._fill.getPatternFill() : this._fill.addNewPatternFill();
        final CTColor fgColor = CTColor.Factory.newInstance();
        fgColor.setIndexed(fg);
        ptrn.setFgColor(fgColor);
    }
    
    public void setFillPattern(final short fp) {
        final CTPatternFill ptrn = this._fill.isSetPatternFill() ? this._fill.getPatternFill() : this._fill.addNewPatternFill();
        if (fp == 0) {
            ptrn.unsetPatternType();
        }
        else {
            ptrn.setPatternType(STPatternType.Enum.forInt(fp + 1));
        }
    }
}
