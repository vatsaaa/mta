// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTDefinedName;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.EvaluationCell;
import org.apache.poi.ss.formula.ptg.NamePtg;
import org.apache.poi.ss.formula.functions.FreeRefFunction;
import org.apache.poi.xssf.model.IndexedUDFFinder;
import org.apache.poi.ss.formula.ptg.NameXPtg;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.formula.EvaluationSheet;
import org.apache.poi.ss.formula.EvaluationName;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.EvaluationWorkbook;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;

public final class XSSFEvaluationWorkbook implements FormulaRenderingWorkbook, EvaluationWorkbook, FormulaParsingWorkbook
{
    private final XSSFWorkbook _uBook;
    
    public static XSSFEvaluationWorkbook create(final XSSFWorkbook book) {
        if (book == null) {
            return null;
        }
        return new XSSFEvaluationWorkbook(book);
    }
    
    private XSSFEvaluationWorkbook(final XSSFWorkbook book) {
        this._uBook = book;
    }
    
    private int convertFromExternalSheetIndex(final int externSheetIndex) {
        return externSheetIndex;
    }
    
    public int convertFromExternSheetIndex(final int externSheetIndex) {
        return externSheetIndex;
    }
    
    private int convertToExternalSheetIndex(final int sheetIndex) {
        return sheetIndex;
    }
    
    public int getExternalSheetIndex(final String sheetName) {
        final int sheetIndex = this._uBook.getSheetIndex(sheetName);
        return this.convertToExternalSheetIndex(sheetIndex);
    }
    
    public EvaluationName getName(final String name, final int sheetIndex) {
        for (int i = 0; i < this._uBook.getNumberOfNames(); ++i) {
            final XSSFName nm = this._uBook.getNameAt(i);
            final String nameText = nm.getNameName();
            if (name.equalsIgnoreCase(nameText) && nm.getSheetIndex() == sheetIndex) {
                return new Name(this._uBook.getNameAt(i), i, this);
            }
        }
        return (sheetIndex == -1) ? null : this.getName(name, -1);
    }
    
    public int getSheetIndex(final EvaluationSheet evalSheet) {
        final XSSFSheet sheet = ((XSSFEvaluationSheet)evalSheet).getXSSFSheet();
        return this._uBook.getSheetIndex(sheet);
    }
    
    public String getSheetName(final int sheetIndex) {
        return this._uBook.getSheetName(sheetIndex);
    }
    
    public ExternalName getExternalName(final int externSheetIndex, final int externNameIndex) {
        throw new RuntimeException("Not implemented yet");
    }
    
    public NameXPtg getNameXPtg(final String name) {
        final IndexedUDFFinder udfFinder = (IndexedUDFFinder)this.getUDFFinder();
        final FreeRefFunction func = udfFinder.findFunction(name);
        if (func == null) {
            return null;
        }
        return new NameXPtg(0, udfFinder.getFunctionIndex(name));
    }
    
    public String resolveNameXText(final NameXPtg n) {
        final int idx = n.getNameIndex();
        final IndexedUDFFinder udfFinder = (IndexedUDFFinder)this.getUDFFinder();
        return udfFinder.getFunctionName(idx);
    }
    
    public EvaluationSheet getSheet(final int sheetIndex) {
        return new XSSFEvaluationSheet(this._uBook.getSheetAt(sheetIndex));
    }
    
    public ExternalSheet getExternalSheet(final int externSheetIndex) {
        return null;
    }
    
    public int getExternalSheetIndex(final String workbookName, final String sheetName) {
        throw new RuntimeException("not implemented yet");
    }
    
    public int getSheetIndex(final String sheetName) {
        return this._uBook.getSheetIndex(sheetName);
    }
    
    public String getSheetNameByExternSheet(final int externSheetIndex) {
        final int sheetIndex = this.convertFromExternalSheetIndex(externSheetIndex);
        return this._uBook.getSheetName(sheetIndex);
    }
    
    public String getNameText(final NamePtg namePtg) {
        return this._uBook.getNameAt(namePtg.getIndex()).getNameName();
    }
    
    public EvaluationName getName(final NamePtg namePtg) {
        final int ix = namePtg.getIndex();
        return new Name(this._uBook.getNameAt(ix), ix, this);
    }
    
    public Ptg[] getFormulaTokens(final EvaluationCell evalCell) {
        final XSSFCell cell = ((XSSFEvaluationCell)evalCell).getXSSFCell();
        final XSSFEvaluationWorkbook frBook = create(this._uBook);
        return FormulaParser.parse(cell.getCellFormula(), frBook, 0, this._uBook.getSheetIndex(cell.getSheet()));
    }
    
    public UDFFinder getUDFFinder() {
        return this._uBook.getUDFFinder();
    }
    
    public SpreadsheetVersion getSpreadsheetVersion() {
        return SpreadsheetVersion.EXCEL2007;
    }
    
    private static final class Name implements EvaluationName
    {
        private final XSSFName _nameRecord;
        private final int _index;
        private final FormulaParsingWorkbook _fpBook;
        
        public Name(final XSSFName name, final int index, final FormulaParsingWorkbook fpBook) {
            this._nameRecord = name;
            this._index = index;
            this._fpBook = fpBook;
        }
        
        public Ptg[] getNameDefinition() {
            return FormulaParser.parse(this._nameRecord.getRefersToFormula(), this._fpBook, 4, this._nameRecord.getSheetIndex());
        }
        
        public String getNameText() {
            return this._nameRecord.getNameName();
        }
        
        public boolean hasFormula() {
            final CTDefinedName ctn = this._nameRecord.getCTName();
            final String strVal = ctn.getStringValue();
            return !ctn.getFunction() && strVal != null && strVal.length() > 0;
        }
        
        public boolean isFunctionName() {
            return this._nameRecord.isFunctionName();
        }
        
        public boolean isRange() {
            return this.hasFormula();
        }
        
        public NamePtg createPtg() {
            return new NamePtg(this._index);
        }
    }
}
