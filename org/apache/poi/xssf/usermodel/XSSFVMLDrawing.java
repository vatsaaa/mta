// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import java.util.Iterator;
import schemasMicrosoftComOfficeExcel.CTClientData;
import schemasMicrosoftComVml.CTShadow;
import java.math.BigInteger;
import schemasMicrosoftComOfficeExcel.STObjectType;
import schemasMicrosoftComOfficeOffice.STInsetMode;
import schemasMicrosoftComVml.CTPath;
import schemasMicrosoftComOfficeOffice.CTIdMap;
import schemasMicrosoftComOfficeOffice.STConnectType;
import schemasMicrosoftComVml.STTrueFalse;
import schemasMicrosoftComVml.STStrokeJoinStyle;
import schemasMicrosoftComVml.STExt;
import java.util.Map;
import java.util.HashMap;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlCursor;
import java.io.OutputStream;
import java.util.regex.Matcher;
import org.w3c.dom.Node;
import schemasMicrosoftComVml.CTShape;
import schemasMicrosoftComVml.CTShapetype;
import schemasMicrosoftComOfficeOffice.CTShapeLayout;
import org.apache.poi.xssf.util.EvilUnclosedBRFixingInputStream;
import java.io.InputStream;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.ArrayList;
import org.apache.xmlbeans.XmlObject;
import java.util.List;
import java.util.regex.Pattern;
import javax.xml.namespace.QName;
import org.apache.poi.POIXMLDocumentPart;

public final class XSSFVMLDrawing extends POIXMLDocumentPart
{
    private static final QName QNAME_SHAPE_LAYOUT;
    private static final QName QNAME_SHAPE_TYPE;
    private static final QName QNAME_SHAPE;
    private static final Pattern ptrn_shapeId;
    private List<QName> _qnames;
    private List<XmlObject> _items;
    private String _shapeTypeId;
    private int _shapeId;
    
    protected XSSFVMLDrawing() {
        this._qnames = new ArrayList<QName>();
        this._items = new ArrayList<XmlObject>();
        this._shapeId = 1024;
        this.newDrawing();
    }
    
    protected XSSFVMLDrawing(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        this._qnames = new ArrayList<QName>();
        this._items = new ArrayList<XmlObject>();
        this._shapeId = 1024;
        this.read(this.getPackagePart().getInputStream());
    }
    
    protected void read(final InputStream is) throws IOException, XmlException {
        final XmlObject root = XmlObject.Factory.parse(new EvilUnclosedBRFixingInputStream(is));
        this._qnames = new ArrayList<QName>();
        this._items = new ArrayList<XmlObject>();
        for (final XmlObject obj : root.selectPath("$this/xml/*")) {
            final Node nd = obj.getDomNode();
            final QName qname = new QName(nd.getNamespaceURI(), nd.getLocalName());
            if (qname.equals(XSSFVMLDrawing.QNAME_SHAPE_LAYOUT)) {
                this._items.add(CTShapeLayout.Factory.parse(obj.xmlText()));
            }
            else if (qname.equals(XSSFVMLDrawing.QNAME_SHAPE_TYPE)) {
                final CTShapetype st = CTShapetype.Factory.parse(obj.xmlText());
                this._items.add(st);
                this._shapeTypeId = st.getId();
            }
            else if (qname.equals(XSSFVMLDrawing.QNAME_SHAPE)) {
                final CTShape shape = CTShape.Factory.parse(obj.xmlText());
                final String id = shape.getId();
                if (id != null) {
                    final Matcher m = XSSFVMLDrawing.ptrn_shapeId.matcher(id);
                    if (m.find()) {
                        this._shapeId = Math.max(this._shapeId, Integer.parseInt(m.group(1)));
                    }
                }
                this._items.add(shape);
            }
            else {
                this._items.add(XmlObject.Factory.parse(obj.xmlText()));
            }
            this._qnames.add(qname);
        }
    }
    
    protected List<XmlObject> getItems() {
        return this._items;
    }
    
    protected void write(final OutputStream out) throws IOException {
        final XmlObject rootObject = XmlObject.Factory.newInstance();
        final XmlCursor rootCursor = rootObject.newCursor();
        rootCursor.toNextToken();
        rootCursor.beginElement("xml");
        for (int i = 0; i < this._items.size(); ++i) {
            final XmlCursor xc = this._items.get(i).newCursor();
            rootCursor.beginElement(this._qnames.get(i));
            while (xc.toNextToken() == XmlCursor.TokenType.ATTR) {
                final Node anode = xc.getDomNode();
                rootCursor.insertAttributeWithValue(anode.getLocalName(), anode.getNamespaceURI(), anode.getNodeValue());
            }
            xc.toStartDoc();
            xc.copyXmlContents(rootCursor);
            rootCursor.toNextToken();
            xc.dispose();
        }
        rootCursor.dispose();
        final XmlOptions xmlOptions = new XmlOptions(XSSFVMLDrawing.DEFAULT_XML_OPTIONS);
        xmlOptions.setSavePrettyPrint();
        final HashMap<String, String> map = new HashMap<String, String>();
        map.put("urn:schemas-microsoft-com:vml", "v");
        map.put("urn:schemas-microsoft-com:office:office", "o");
        map.put("urn:schemas-microsoft-com:office:excel", "x");
        xmlOptions.setSaveSuggestedPrefixes(map);
        rootObject.save(out, xmlOptions);
    }
    
    @Override
    protected void commit() throws IOException {
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.write(out);
        out.close();
    }
    
    private void newDrawing() {
        final CTShapeLayout layout = CTShapeLayout.Factory.newInstance();
        layout.setExt(STExt.EDIT);
        final CTIdMap idmap = layout.addNewIdmap();
        idmap.setExt(STExt.EDIT);
        idmap.setData("1");
        this._items.add(layout);
        this._qnames.add(XSSFVMLDrawing.QNAME_SHAPE_LAYOUT);
        final CTShapetype shapetype = CTShapetype.Factory.newInstance();
        shapetype.setId(this._shapeTypeId = "_xssf_cell_comment");
        shapetype.setCoordsize("21600,21600");
        shapetype.setSpt(202.0f);
        shapetype.setPath2("m,l,21600r21600,l21600,xe");
        shapetype.addNewStroke().setJoinstyle(STStrokeJoinStyle.MITER);
        final CTPath path = shapetype.addNewPath();
        path.setGradientshapeok(STTrueFalse.T);
        path.setConnecttype(STConnectType.RECT);
        this._items.add(shapetype);
        this._qnames.add(XSSFVMLDrawing.QNAME_SHAPE_TYPE);
    }
    
    protected CTShape newCommentShape() {
        final CTShape shape = CTShape.Factory.newInstance();
        shape.setId("_x0000_s" + ++this._shapeId);
        shape.setType("#" + this._shapeTypeId);
        shape.setStyle("position:absolute; visibility:hidden");
        shape.setFillcolor("#ffffe1");
        shape.setInsetmode(STInsetMode.AUTO);
        shape.addNewFill().setColor("#ffffe1");
        final CTShadow shadow = shape.addNewShadow();
        shadow.setOn(STTrueFalse.T);
        shadow.setColor("black");
        shadow.setObscured(STTrueFalse.T);
        shape.addNewPath().setConnecttype(STConnectType.NONE);
        shape.addNewTextbox().setStyle("mso-direction-alt:auto");
        final CTClientData cldata = shape.addNewClientData();
        cldata.setObjectType(STObjectType.NOTE);
        cldata.addNewMoveWithCells();
        cldata.addNewSizeWithCells();
        cldata.addNewAnchor().setStringValue("1, 15, 0, 2, 3, 15, 3, 16");
        cldata.addNewAutoFill().setStringValue("False");
        cldata.addNewRow().setBigIntegerValue(new BigInteger("0"));
        cldata.addNewColumn().setBigIntegerValue(new BigInteger("0"));
        this._items.add(shape);
        this._qnames.add(XSSFVMLDrawing.QNAME_SHAPE);
        return shape;
    }
    
    protected CTShape findCommentShape(final int row, final int col) {
        for (final XmlObject itm : this._items) {
            if (itm instanceof CTShape) {
                final CTShape sh = (CTShape)itm;
                if (sh.sizeOfClientDataArray() <= 0) {
                    continue;
                }
                final CTClientData cldata = sh.getClientDataArray(0);
                if (cldata.getObjectType() != STObjectType.NOTE) {
                    continue;
                }
                final int crow = cldata.getRowArray(0).intValue();
                final int ccol = cldata.getColumnArray(0).intValue();
                if (crow == row && ccol == col) {
                    return sh;
                }
                continue;
            }
        }
        return null;
    }
    
    protected boolean removeCommentShape(final int row, final int col) {
        final CTShape shape = this.findCommentShape(row, col);
        return shape != null && this._items.remove(shape);
    }
    
    static {
        QNAME_SHAPE_LAYOUT = new QName("urn:schemas-microsoft-com:office:office", "shapelayout");
        QNAME_SHAPE_TYPE = new QName("urn:schemas-microsoft-com:vml", "shapetype");
        QNAME_SHAPE = new QName("urn:schemas-microsoft-com:vml", "shape");
        ptrn_shapeId = Pattern.compile("_x0000_s(\\d+)");
    }
}
