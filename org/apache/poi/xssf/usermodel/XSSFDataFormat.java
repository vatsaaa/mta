// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.ss.usermodel.DataFormat;

public class XSSFDataFormat implements DataFormat
{
    private StylesTable stylesSource;
    
    protected XSSFDataFormat(final StylesTable stylesSource) {
        this.stylesSource = stylesSource;
    }
    
    public short getFormat(final String format) {
        int idx = BuiltinFormats.getBuiltinFormat(format);
        if (idx == -1) {
            idx = this.stylesSource.putNumberFormat(format);
        }
        return (short)idx;
    }
    
    public String getFormat(final short index) {
        String fmt = this.stylesSource.getNumberFormatAt(index);
        if (fmt == null) {
            fmt = BuiltinFormats.getBuiltinFormat(index);
        }
        return fmt;
    }
}
