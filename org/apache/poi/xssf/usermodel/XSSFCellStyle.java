// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.usermodel;

import org.apache.poi.ss.usermodel.Color;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTPatternFill;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFill;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorderPr;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STPatternType;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellFill;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STBorderStyle;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTBorder;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCellAlignment;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Font;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTFont;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import org.apache.xmlbeans.XmlObject;
import org.apache.poi.util.Internal;
import org.apache.poi.xssf.model.ThemesTable;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellAlignment;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTXf;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.ss.usermodel.CellStyle;

public class XSSFCellStyle implements CellStyle
{
    private int _cellXfId;
    private StylesTable _stylesSource;
    private CTXf _cellXf;
    private CTXf _cellStyleXf;
    private XSSFFont _font;
    private XSSFCellAlignment _cellAlignment;
    private ThemesTable _theme;
    
    public XSSFCellStyle(final int cellXfId, final int cellStyleXfId, final StylesTable stylesSource, final ThemesTable theme) {
        this._cellXfId = cellXfId;
        this._stylesSource = stylesSource;
        this._cellXf = stylesSource.getCellXfAt(this._cellXfId);
        this._cellStyleXf = ((cellStyleXfId == -1) ? null : stylesSource.getCellStyleXfAt(cellStyleXfId));
        this._theme = theme;
    }
    
    @Internal
    public CTXf getCoreXf() {
        return this._cellXf;
    }
    
    @Internal
    public CTXf getStyleXf() {
        return this._cellStyleXf;
    }
    
    public XSSFCellStyle(final StylesTable stylesSource) {
        this._stylesSource = stylesSource;
        this._cellXf = CTXf.Factory.newInstance();
        this._cellStyleXf = null;
    }
    
    public void verifyBelongsToStylesSource(final StylesTable src) {
        if (this._stylesSource != src) {
            throw new IllegalArgumentException("This Style does not belong to the supplied Workbook Stlyes Source. Are you trying to assign a style from one workbook to the cell of a differnt workbook?");
        }
    }
    
    public void cloneStyleFrom(final CellStyle source) {
        if (source instanceof XSSFCellStyle) {
            final XSSFCellStyle src = (XSSFCellStyle)source;
            if (src._stylesSource == this._stylesSource) {
                this._cellXf.set(src.getCoreXf());
                this._cellStyleXf.set(src.getStyleXf());
            }
            else {
                try {
                    if (this._cellXf.isSetAlignment()) {
                        this._cellXf.unsetAlignment();
                    }
                    if (this._cellXf.isSetExtLst()) {
                        this._cellXf.unsetExtLst();
                    }
                    this._cellXf = CTXf.Factory.parse(src.getCoreXf().toString());
                    this._stylesSource.replaceCellXfAt(this._cellXfId, this._cellXf);
                }
                catch (XmlException e) {
                    throw new POIXMLException(e);
                }
                final String fmt = src.getDataFormatString();
                this.setDataFormat(new XSSFDataFormat(this._stylesSource).getFormat(fmt));
                try {
                    final CTFont ctFont = CTFont.Factory.parse(src.getFont().getCTFont().toString());
                    final XSSFFont font = new XSSFFont(ctFont);
                    font.registerTo(this._stylesSource);
                    this.setFont(font);
                }
                catch (XmlException e2) {
                    throw new POIXMLException(e2);
                }
            }
            this._font = null;
            this._cellAlignment = null;
            return;
        }
        throw new IllegalArgumentException("Can only clone from one XSSFCellStyle to another, not between HSSFCellStyle and XSSFCellStyle");
    }
    
    public short getAlignment() {
        return (short)this.getAlignmentEnum().ordinal();
    }
    
    public HorizontalAlignment getAlignmentEnum() {
        final CTCellAlignment align = this._cellXf.getAlignment();
        if (align != null && align.isSetHorizontal()) {
            return HorizontalAlignment.values()[align.getHorizontal().intValue() - 1];
        }
        return HorizontalAlignment.GENERAL;
    }
    
    public short getBorderBottom() {
        if (!this._cellXf.getApplyBorder()) {
            return 0;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final CTBorder ct = this._stylesSource.getBorderAt(idx).getCTBorder();
        final STBorderStyle.Enum ptrn = ct.isSetBottom() ? ct.getBottom().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public BorderStyle getBorderBottomEnum() {
        final int style = this.getBorderBottom();
        return BorderStyle.values()[style];
    }
    
    public short getBorderLeft() {
        if (!this._cellXf.getApplyBorder()) {
            return 0;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final CTBorder ct = this._stylesSource.getBorderAt(idx).getCTBorder();
        final STBorderStyle.Enum ptrn = ct.isSetLeft() ? ct.getLeft().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public BorderStyle getBorderLeftEnum() {
        final int style = this.getBorderLeft();
        return BorderStyle.values()[style];
    }
    
    public short getBorderRight() {
        if (!this._cellXf.getApplyBorder()) {
            return 0;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final CTBorder ct = this._stylesSource.getBorderAt(idx).getCTBorder();
        final STBorderStyle.Enum ptrn = ct.isSetRight() ? ct.getRight().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public BorderStyle getBorderRightEnum() {
        final int style = this.getBorderRight();
        return BorderStyle.values()[style];
    }
    
    public short getBorderTop() {
        if (!this._cellXf.getApplyBorder()) {
            return 0;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final CTBorder ct = this._stylesSource.getBorderAt(idx).getCTBorder();
        final STBorderStyle.Enum ptrn = ct.isSetTop() ? ct.getTop().getStyle() : null;
        return (short)((ptrn == null) ? 0 : ((short)(ptrn.intValue() - 1)));
    }
    
    public BorderStyle getBorderTopEnum() {
        final int style = this.getBorderTop();
        return BorderStyle.values()[style];
    }
    
    public short getBottomBorderColor() {
        final XSSFColor clr = this.getBottomBorderXSSFColor();
        return (clr == null) ? IndexedColors.BLACK.getIndex() : clr.getIndexed();
    }
    
    public XSSFColor getBottomBorderXSSFColor() {
        if (!this._cellXf.getApplyBorder()) {
            return null;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final XSSFCellBorder border = this._stylesSource.getBorderAt(idx);
        return border.getBorderColor(XSSFCellBorder.BorderSide.BOTTOM);
    }
    
    public short getDataFormat() {
        return (short)this._cellXf.getNumFmtId();
    }
    
    public String getDataFormatString() {
        final int idx = this.getDataFormat();
        return new XSSFDataFormat(this._stylesSource).getFormat((short)idx);
    }
    
    public short getFillBackgroundColor() {
        final XSSFColor clr = this.getFillBackgroundXSSFColor();
        return (clr == null) ? IndexedColors.AUTOMATIC.getIndex() : clr.getIndexed();
    }
    
    public XSSFColor getFillBackgroundColorColor() {
        return this.getFillBackgroundXSSFColor();
    }
    
    public XSSFColor getFillBackgroundXSSFColor() {
        if (!this._cellXf.getApplyFill()) {
            return null;
        }
        final int fillIndex = (int)this._cellXf.getFillId();
        final XSSFCellFill fg = this._stylesSource.getFillAt(fillIndex);
        final XSSFColor fillBackgroundColor = fg.getFillBackgroundColor();
        if (fillBackgroundColor != null && this._theme != null) {
            this._theme.inheritFromThemeAsRequired(fillBackgroundColor);
        }
        return fillBackgroundColor;
    }
    
    public short getFillForegroundColor() {
        final XSSFColor clr = this.getFillForegroundXSSFColor();
        return (clr == null) ? IndexedColors.AUTOMATIC.getIndex() : clr.getIndexed();
    }
    
    public XSSFColor getFillForegroundColorColor() {
        return this.getFillForegroundXSSFColor();
    }
    
    public XSSFColor getFillForegroundXSSFColor() {
        if (!this._cellXf.getApplyFill()) {
            return null;
        }
        final int fillIndex = (int)this._cellXf.getFillId();
        final XSSFCellFill fg = this._stylesSource.getFillAt(fillIndex);
        final XSSFColor fillForegroundColor = fg.getFillForegroundColor();
        if (fillForegroundColor != null && this._theme != null) {
            this._theme.inheritFromThemeAsRequired(fillForegroundColor);
        }
        return fillForegroundColor;
    }
    
    public short getFillPattern() {
        if (!this._cellXf.getApplyFill()) {
            return 0;
        }
        final int fillIndex = (int)this._cellXf.getFillId();
        final XSSFCellFill fill = this._stylesSource.getFillAt(fillIndex);
        final STPatternType.Enum ptrn = fill.getPatternType();
        if (ptrn == null) {
            return 0;
        }
        return (short)(ptrn.intValue() - 1);
    }
    
    public FillPatternType getFillPatternEnum() {
        final int style = this.getFillPattern();
        return FillPatternType.values()[style];
    }
    
    public XSSFFont getFont() {
        if (this._font == null) {
            this._font = this._stylesSource.getFontAt(this.getFontId());
        }
        return this._font;
    }
    
    public short getFontIndex() {
        return (short)this.getFontId();
    }
    
    public boolean getHidden() {
        return this._cellXf.isSetProtection() && this._cellXf.getProtection().isSetHidden() && this._cellXf.getProtection().getHidden();
    }
    
    public short getIndention() {
        final CTCellAlignment align = this._cellXf.getAlignment();
        return (short)((align == null) ? 0L : align.getIndent());
    }
    
    public short getIndex() {
        return (short)this._cellXfId;
    }
    
    public short getLeftBorderColor() {
        final XSSFColor clr = this.getLeftBorderXSSFColor();
        return (clr == null) ? IndexedColors.BLACK.getIndex() : clr.getIndexed();
    }
    
    public XSSFColor getLeftBorderXSSFColor() {
        if (!this._cellXf.getApplyBorder()) {
            return null;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final XSSFCellBorder border = this._stylesSource.getBorderAt(idx);
        return border.getBorderColor(XSSFCellBorder.BorderSide.LEFT);
    }
    
    public boolean getLocked() {
        return !this._cellXf.isSetProtection() || !this._cellXf.getProtection().isSetLocked() || this._cellXf.getProtection().getLocked();
    }
    
    public short getRightBorderColor() {
        final XSSFColor clr = this.getRightBorderXSSFColor();
        return (clr == null) ? IndexedColors.BLACK.getIndex() : clr.getIndexed();
    }
    
    public XSSFColor getRightBorderXSSFColor() {
        if (!this._cellXf.getApplyBorder()) {
            return null;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final XSSFCellBorder border = this._stylesSource.getBorderAt(idx);
        return border.getBorderColor(XSSFCellBorder.BorderSide.RIGHT);
    }
    
    public short getRotation() {
        final CTCellAlignment align = this._cellXf.getAlignment();
        return (short)((align == null) ? 0L : align.getTextRotation());
    }
    
    public short getTopBorderColor() {
        final XSSFColor clr = this.getTopBorderXSSFColor();
        return (clr == null) ? IndexedColors.BLACK.getIndex() : clr.getIndexed();
    }
    
    public XSSFColor getTopBorderXSSFColor() {
        if (!this._cellXf.getApplyBorder()) {
            return null;
        }
        final int idx = (int)this._cellXf.getBorderId();
        final XSSFCellBorder border = this._stylesSource.getBorderAt(idx);
        return border.getBorderColor(XSSFCellBorder.BorderSide.TOP);
    }
    
    public short getVerticalAlignment() {
        return (short)this.getVerticalAlignmentEnum().ordinal();
    }
    
    public VerticalAlignment getVerticalAlignmentEnum() {
        final CTCellAlignment align = this._cellXf.getAlignment();
        if (align != null && align.isSetVertical()) {
            return VerticalAlignment.values()[align.getVertical().intValue() - 1];
        }
        return VerticalAlignment.BOTTOM;
    }
    
    public boolean getWrapText() {
        final CTCellAlignment align = this._cellXf.getAlignment();
        return align != null && align.getWrapText();
    }
    
    public void setAlignment(final short align) {
        this.getCellAlignment().setHorizontal(HorizontalAlignment.values()[align]);
    }
    
    public void setAlignment(final HorizontalAlignment align) {
        this.setAlignment((short)align.ordinal());
    }
    
    public void setBorderBottom(final short border) {
        final CTBorder ct = this.getCTBorder();
        final CTBorderPr pr = ct.isSetBottom() ? ct.getBottom() : ct.addNewBottom();
        if (border == 0) {
            ct.unsetBottom();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setBorderBottom(final BorderStyle border) {
        this.setBorderBottom((short)border.ordinal());
    }
    
    public void setBorderLeft(final short border) {
        final CTBorder ct = this.getCTBorder();
        final CTBorderPr pr = ct.isSetLeft() ? ct.getLeft() : ct.addNewLeft();
        if (border == 0) {
            ct.unsetLeft();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setBorderLeft(final BorderStyle border) {
        this.setBorderLeft((short)border.ordinal());
    }
    
    public void setBorderRight(final short border) {
        final CTBorder ct = this.getCTBorder();
        final CTBorderPr pr = ct.isSetRight() ? ct.getRight() : ct.addNewRight();
        if (border == 0) {
            ct.unsetRight();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setBorderRight(final BorderStyle border) {
        this.setBorderRight((short)border.ordinal());
    }
    
    public void setBorderTop(final short border) {
        final CTBorder ct = this.getCTBorder();
        final CTBorderPr pr = ct.isSetTop() ? ct.getTop() : ct.addNewTop();
        if (border == 0) {
            ct.unsetTop();
        }
        else {
            pr.setStyle(STBorderStyle.Enum.forInt(border + 1));
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setBorderTop(final BorderStyle border) {
        this.setBorderTop((short)border.ordinal());
    }
    
    public void setBottomBorderColor(final short color) {
        final XSSFColor clr = new XSSFColor();
        clr.setIndexed(color);
        this.setBottomBorderColor(clr);
    }
    
    public void setBottomBorderColor(final XSSFColor color) {
        final CTBorder ct = this.getCTBorder();
        if (color == null && !ct.isSetBottom()) {
            return;
        }
        final CTBorderPr pr = ct.isSetBottom() ? ct.getBottom() : ct.addNewBottom();
        if (color != null) {
            pr.setColor(color.getCTColor());
        }
        else {
            pr.unsetColor();
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setDataFormat(final short fmt) {
        this._cellXf.setApplyNumberFormat(true);
        this._cellXf.setNumFmtId(fmt);
    }
    
    public void setFillBackgroundColor(final XSSFColor color) {
        final CTFill ct = this.getCTFill();
        CTPatternFill ptrn = ct.getPatternFill();
        if (color == null) {
            if (ptrn != null) {
                ptrn.unsetBgColor();
            }
        }
        else {
            if (ptrn == null) {
                ptrn = ct.addNewPatternFill();
            }
            ptrn.setBgColor(color.getCTColor());
        }
        final int idx = this._stylesSource.putFill(new XSSFCellFill(ct));
        this._cellXf.setFillId(idx);
        this._cellXf.setApplyFill(true);
    }
    
    public void setFillBackgroundColor(final short bg) {
        final XSSFColor clr = new XSSFColor();
        clr.setIndexed(bg);
        this.setFillBackgroundColor(clr);
    }
    
    public void setFillForegroundColor(final XSSFColor color) {
        final CTFill ct = this.getCTFill();
        CTPatternFill ptrn = ct.getPatternFill();
        if (color == null) {
            if (ptrn != null) {
                ptrn.unsetFgColor();
            }
        }
        else {
            if (ptrn == null) {
                ptrn = ct.addNewPatternFill();
            }
            ptrn.setFgColor(color.getCTColor());
        }
        final int idx = this._stylesSource.putFill(new XSSFCellFill(ct));
        this._cellXf.setFillId(idx);
        this._cellXf.setApplyFill(true);
    }
    
    public void setFillForegroundColor(final short fg) {
        final XSSFColor clr = new XSSFColor();
        clr.setIndexed(fg);
        this.setFillForegroundColor(clr);
    }
    
    private CTFill getCTFill() {
        CTFill ct;
        if (this._cellXf.getApplyFill()) {
            final int fillIndex = (int)this._cellXf.getFillId();
            final XSSFCellFill cf = this._stylesSource.getFillAt(fillIndex);
            ct = (CTFill)cf.getCTFill().copy();
        }
        else {
            ct = CTFill.Factory.newInstance();
        }
        return ct;
    }
    
    private CTBorder getCTBorder() {
        CTBorder ct;
        if (this._cellXf.getApplyBorder()) {
            final int idx = (int)this._cellXf.getBorderId();
            final XSSFCellBorder cf = this._stylesSource.getBorderAt(idx);
            ct = (CTBorder)cf.getCTBorder().copy();
        }
        else {
            ct = CTBorder.Factory.newInstance();
        }
        return ct;
    }
    
    public void setFillPattern(final short fp) {
        final CTFill ct = this.getCTFill();
        final CTPatternFill ptrn = ct.isSetPatternFill() ? ct.getPatternFill() : ct.addNewPatternFill();
        if (fp == 0 && ptrn.isSetPatternType()) {
            ptrn.unsetPatternType();
        }
        else {
            ptrn.setPatternType(STPatternType.Enum.forInt(fp + 1));
        }
        final int idx = this._stylesSource.putFill(new XSSFCellFill(ct));
        this._cellXf.setFillId(idx);
        this._cellXf.setApplyFill(true);
    }
    
    public void setFillPattern(final FillPatternType ptrn) {
        this.setFillPattern((short)ptrn.ordinal());
    }
    
    public void setFont(final Font font) {
        if (font != null) {
            final long index = font.getIndex();
            this._cellXf.setFontId(index);
            this._cellXf.setApplyFont(true);
        }
        else {
            this._cellXf.setApplyFont(false);
        }
    }
    
    public void setHidden(final boolean hidden) {
        if (!this._cellXf.isSetProtection()) {
            this._cellXf.addNewProtection();
        }
        this._cellXf.getProtection().setHidden(hidden);
    }
    
    public void setIndention(final short indent) {
        this.getCellAlignment().setIndent(indent);
    }
    
    public void setLeftBorderColor(final short color) {
        final XSSFColor clr = new XSSFColor();
        clr.setIndexed(color);
        this.setLeftBorderColor(clr);
    }
    
    public void setLeftBorderColor(final XSSFColor color) {
        final CTBorder ct = this.getCTBorder();
        if (color == null && !ct.isSetLeft()) {
            return;
        }
        final CTBorderPr pr = ct.isSetLeft() ? ct.getLeft() : ct.addNewLeft();
        if (color != null) {
            pr.setColor(color.getCTColor());
        }
        else {
            pr.unsetColor();
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setLocked(final boolean locked) {
        if (!this._cellXf.isSetProtection()) {
            this._cellXf.addNewProtection();
        }
        this._cellXf.getProtection().setLocked(locked);
    }
    
    public void setRightBorderColor(final short color) {
        final XSSFColor clr = new XSSFColor();
        clr.setIndexed(color);
        this.setRightBorderColor(clr);
    }
    
    public void setRightBorderColor(final XSSFColor color) {
        final CTBorder ct = this.getCTBorder();
        if (color == null && !ct.isSetRight()) {
            return;
        }
        final CTBorderPr pr = ct.isSetRight() ? ct.getRight() : ct.addNewRight();
        if (color != null) {
            pr.setColor(color.getCTColor());
        }
        else {
            pr.unsetColor();
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setRotation(final short rotation) {
        this.getCellAlignment().setTextRotation(rotation);
    }
    
    public void setTopBorderColor(final short color) {
        final XSSFColor clr = new XSSFColor();
        clr.setIndexed(color);
        this.setTopBorderColor(clr);
    }
    
    public void setTopBorderColor(final XSSFColor color) {
        final CTBorder ct = this.getCTBorder();
        if (color == null && !ct.isSetTop()) {
            return;
        }
        final CTBorderPr pr = ct.isSetTop() ? ct.getTop() : ct.addNewTop();
        if (color != null) {
            pr.setColor(color.getCTColor());
        }
        else {
            pr.unsetColor();
        }
        final int idx = this._stylesSource.putBorder(new XSSFCellBorder(ct, this._theme));
        this._cellXf.setBorderId(idx);
        this._cellXf.setApplyBorder(true);
    }
    
    public void setVerticalAlignment(final short align) {
        this.getCellAlignment().setVertical(VerticalAlignment.values()[align]);
    }
    
    public void setVerticalAlignment(final VerticalAlignment align) {
        this.getCellAlignment().setVertical(align);
    }
    
    public void setWrapText(final boolean wrapped) {
        this.getCellAlignment().setWrapText(wrapped);
    }
    
    public XSSFColor getBorderColor(final XSSFCellBorder.BorderSide side) {
        switch (side) {
            case BOTTOM: {
                return this.getBottomBorderXSSFColor();
            }
            case RIGHT: {
                return this.getRightBorderXSSFColor();
            }
            case TOP: {
                return this.getTopBorderXSSFColor();
            }
            case LEFT: {
                return this.getLeftBorderXSSFColor();
            }
            default: {
                throw new IllegalArgumentException("Unknown border: " + side);
            }
        }
    }
    
    public void setBorderColor(final XSSFCellBorder.BorderSide side, final XSSFColor color) {
        switch (side) {
            case BOTTOM: {
                this.setBottomBorderColor(color);
                break;
            }
            case RIGHT: {
                this.setRightBorderColor(color);
                break;
            }
            case TOP: {
                this.setTopBorderColor(color);
                break;
            }
            case LEFT: {
                this.setLeftBorderColor(color);
                break;
            }
        }
    }
    
    private int getFontId() {
        if (this._cellXf.isSetFontId()) {
            return (int)this._cellXf.getFontId();
        }
        return (int)this._cellStyleXf.getFontId();
    }
    
    protected XSSFCellAlignment getCellAlignment() {
        if (this._cellAlignment == null) {
            this._cellAlignment = new XSSFCellAlignment(this.getCTCellAlignment());
        }
        return this._cellAlignment;
    }
    
    private CTCellAlignment getCTCellAlignment() {
        if (this._cellXf.getAlignment() == null) {
            this._cellXf.setAlignment(CTCellAlignment.Factory.newInstance());
        }
        return this._cellXf.getAlignment();
    }
    
    @Override
    public int hashCode() {
        return this._cellXf.toString().hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof XSSFCellStyle)) {
            return false;
        }
        final XSSFCellStyle cf = (XSSFCellStyle)o;
        return this._cellXf.toString().equals(cf.getCoreXf().toString());
    }
    
    public Object clone() {
        final CTXf xf = (CTXf)this._cellXf.copy();
        final int xfSize = this._stylesSource._getStyleXfsSize();
        final int indexXf = this._stylesSource.putCellXf(xf);
        return new XSSFCellStyle(indexXf - 1, xfSize - 1, this._stylesSource, this._theme);
    }
}
