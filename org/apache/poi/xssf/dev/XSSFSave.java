// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.dev;

import java.io.OutputStream;
import java.io.FileOutputStream;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;

public final class XSSFSave
{
    public static void main(final String[] args) throws Exception {
        for (int i = 0; i < args.length; ++i) {
            final OPCPackage pkg = OPCPackage.open(args[i]);
            final XSSFWorkbook wb = new XSSFWorkbook(pkg);
            final int sep = args[i].lastIndexOf(46);
            final String outfile = args[i].substring(0, sep) + "-save.xls" + (wb.isMacroEnabled() ? "m" : "x");
            final FileOutputStream out = new FileOutputStream(outfile);
            wb.write(out);
            out.close();
            pkg.close();
        }
    }
}
