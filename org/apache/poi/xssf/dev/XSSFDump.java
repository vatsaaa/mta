// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.dev;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlObject;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.io.File;
import java.util.zip.ZipFile;

public final class XSSFDump
{
    public static void main(final String[] args) throws Exception {
        for (int i = 0; i < args.length; ++i) {
            System.out.println("Dumping " + args[i]);
            final ZipFile zip = new ZipFile(args[i]);
            dump(zip);
        }
    }
    
    public static void dump(final ZipFile zip) throws Exception {
        final String zipname = zip.getName();
        final int sep = zipname.lastIndexOf(46);
        final File root = new File(zipname.substring(0, sep));
        root.mkdir();
        final Enumeration en = zip.entries();
        while (en.hasMoreElements()) {
            final ZipEntry entry = en.nextElement();
            final String name = entry.getName();
            final int idx = name.lastIndexOf(47);
            if (idx != -1) {
                final File bs = new File(root, name.substring(0, idx));
                bs.mkdirs();
            }
            final File f = new File(root, entry.getName());
            final FileOutputStream out = new FileOutputStream(f);
            Label_0271: {
                if (!entry.getName().endsWith(".xml") && !entry.getName().endsWith(".vml")) {
                    if (!entry.getName().endsWith(".rels")) {
                        dump(zip.getInputStream(entry), out);
                        break Label_0271;
                    }
                }
                try {
                    final XmlObject xml = XmlObject.Factory.parse(zip.getInputStream(entry));
                    final XmlOptions options = new XmlOptions();
                    options.setSavePrettyPrint();
                    xml.save(out, options);
                }
                catch (Exception e) {
                    System.err.println("Failed to parse " + entry.getName() + ", dumping raw content");
                    dump(zip.getInputStream(entry), out);
                }
            }
            out.close();
        }
    }
    
    protected static void dump(final InputStream is, final OutputStream out) throws IOException {
        final byte[] chunk = new byte[2048];
        int pos;
        while ((pos = is.read(chunk)) > 0) {
            out.write(chunk, 0, pos);
        }
    }
}
