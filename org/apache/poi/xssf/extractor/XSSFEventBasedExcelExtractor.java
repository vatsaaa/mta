// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.extractor;

import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.xml.sax.SAXException;
import org.xml.sax.ContentHandler;
import org.xml.sax.XMLReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.apache.poi.ss.usermodel.DataFormatter;
import java.io.InputStream;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.POIXMLDocument;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import java.util.Locale;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.POIXMLTextExtractor;

public class XSSFEventBasedExcelExtractor extends POIXMLTextExtractor
{
    private OPCPackage container;
    private POIXMLProperties properties;
    private Locale locale;
    private boolean includeSheetNames;
    private boolean formulasNotResults;
    
    public XSSFEventBasedExcelExtractor(final String path) throws XmlException, OpenXML4JException, IOException {
        this(OPCPackage.open(path));
    }
    
    public XSSFEventBasedExcelExtractor(final OPCPackage container) throws XmlException, OpenXML4JException, IOException {
        super((POIXMLDocument)null);
        this.includeSheetNames = true;
        this.formulasNotResults = false;
        this.container = container;
        this.properties = new POIXMLProperties(container);
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Use:");
            System.err.println("  XSSFEventBasedExcelExtractor <filename.xlsx>");
            System.exit(1);
        }
        final POIXMLTextExtractor extractor = new XSSFEventBasedExcelExtractor(args[0]);
        System.out.println(extractor.getText());
    }
    
    public void setIncludeSheetNames(final boolean includeSheetNames) {
        this.includeSheetNames = includeSheetNames;
    }
    
    public void setFormulasNotResults(final boolean formulasNotResults) {
        this.formulasNotResults = formulasNotResults;
    }
    
    public void setLocale(final Locale locale) {
        this.locale = locale;
    }
    
    @Override
    public OPCPackage getPackage() {
        return this.container;
    }
    
    @Override
    public POIXMLProperties.CoreProperties getCoreProperties() {
        return this.properties.getCoreProperties();
    }
    
    @Override
    public POIXMLProperties.ExtendedProperties getExtendedProperties() {
        return this.properties.getExtendedProperties();
    }
    
    @Override
    public POIXMLProperties.CustomProperties getCustomProperties() {
        return this.properties.getCustomProperties();
    }
    
    public void processSheet(final XSSFSheetXMLHandler.SheetContentsHandler sheetContentsExtractor, final StylesTable styles, final ReadOnlySharedStringsTable strings, final InputStream sheetInputStream) throws IOException, SAXException {
        DataFormatter formatter;
        if (this.locale == null) {
            formatter = new DataFormatter();
        }
        else {
            formatter = new DataFormatter(this.locale);
        }
        final InputSource sheetSource = new InputSource(sheetInputStream);
        final SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        try {
            final SAXParser saxParser = saxFactory.newSAXParser();
            final XMLReader sheetParser = saxParser.getXMLReader();
            final ContentHandler handler = new XSSFSheetXMLHandler(styles, strings, sheetContentsExtractor, formatter, this.formulasNotResults);
            sheetParser.setContentHandler(handler);
            sheetParser.parse(sheetSource);
        }
        catch (ParserConfigurationException e) {
            throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
        }
    }
    
    @Override
    public String getText() {
        try {
            final ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(this.container);
            final XSSFReader xssfReader = new XSSFReader(this.container);
            final StylesTable styles = xssfReader.getStylesTable();
            final XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator)xssfReader.getSheetsData();
            final StringBuffer text = new StringBuffer();
            final SheetTextExtractor sheetExtractor = new SheetTextExtractor(text);
            while (iter.hasNext()) {
                final InputStream stream = iter.next();
                if (this.includeSheetNames) {
                    text.append(iter.getSheetName());
                    text.append('\n');
                }
                this.processSheet(sheetExtractor, styles, strings, stream);
                stream.close();
            }
            return text.toString();
        }
        catch (IOException e) {
            System.err.println(e);
            return null;
        }
        catch (SAXException se) {
            System.err.println(se);
            return null;
        }
        catch (OpenXML4JException o4je) {
            System.err.println(o4je);
            return null;
        }
    }
    
    protected class SheetTextExtractor implements XSSFSheetXMLHandler.SheetContentsHandler
    {
        private final StringBuffer output;
        private boolean firstCellOfRow;
        
        protected SheetTextExtractor(final StringBuffer output) {
            this.firstCellOfRow = true;
            this.output = output;
        }
        
        public void startRow(final int rowNum) {
            this.firstCellOfRow = true;
        }
        
        public void endRow() {
            this.output.append('\n');
        }
        
        public void cell(final String cellRef, final String formattedValue) {
            if (this.firstCellOfRow) {
                this.firstCellOfRow = false;
            }
            else {
                this.output.append('\t');
            }
            this.output.append(formattedValue);
        }
        
        public void headerFooter(final String text, final boolean isHeader, final String tagName) {
        }
    }
}
