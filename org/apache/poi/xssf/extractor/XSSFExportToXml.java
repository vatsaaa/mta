// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.extractor;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import javax.xml.validation.Validator;
import javax.xml.validation.Schema;
import java.io.IOException;
import javax.xml.validation.SchemaFactory;
import javax.xml.transform.Transformer;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STXmlDataType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import java.util.Iterator;
import java.util.Map;
import org.w3c.dom.Element;
import java.util.List;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerFactory;
import org.apache.poi.xssf.usermodel.helpers.XSSFXmlColumnPr;
import java.util.Collections;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.helpers.XSSFSingleXmlCell;
import java.util.HashMap;
import java.util.Vector;
import org.w3c.dom.Node;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import javax.xml.transform.TransformerException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.io.OutputStream;
import org.apache.poi.xssf.usermodel.XSSFMap;
import java.util.Comparator;

public class XSSFExportToXml implements Comparator<String>
{
    private XSSFMap map;
    
    public XSSFExportToXml(final XSSFMap map) {
        this.map = map;
    }
    
    public void exportToXML(final OutputStream os, final boolean validate) throws SAXException, ParserConfigurationException, TransformerException {
        this.exportToXML(os, "UTF-8", validate);
    }
    
    private Document getEmptyDocument() throws ParserConfigurationException {
        final DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        final DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        final Document doc = docBuilder.newDocument();
        return doc;
    }
    
    public void exportToXML(final OutputStream os, final String encoding, final boolean validate) throws SAXException, ParserConfigurationException, TransformerException {
        final List<XSSFSingleXmlCell> singleXMLCells = this.map.getRelatedSingleXMLCell();
        final List<XSSFTable> tables = this.map.getRelatedTables();
        final String rootElement = this.map.getCtMap().getRootElement();
        final Document doc = this.getEmptyDocument();
        Element root = null;
        if (this.isNamespaceDeclared()) {
            root = doc.createElementNS(this.getNamespace(), rootElement);
        }
        else {
            root = doc.createElementNS("", rootElement);
        }
        doc.appendChild(root);
        final List<String> xpaths = new Vector<String>();
        final Map<String, XSSFSingleXmlCell> singleXmlCellsMappings = new HashMap<String, XSSFSingleXmlCell>();
        final Map<String, XSSFTable> tableMappings = new HashMap<String, XSSFTable>();
        for (final XSSFSingleXmlCell simpleXmlCell : singleXMLCells) {
            xpaths.add(simpleXmlCell.getXpath());
            singleXmlCellsMappings.put(simpleXmlCell.getXpath(), simpleXmlCell);
        }
        for (final XSSFTable table : tables) {
            final String commonXPath = table.getCommonXpath();
            xpaths.add(commonXPath);
            tableMappings.put(commonXPath, table);
        }
        Collections.sort(xpaths, this);
        for (final String xpath : xpaths) {
            final XSSFSingleXmlCell simpleXmlCell2 = singleXmlCellsMappings.get(xpath);
            final XSSFTable table2 = tableMappings.get(xpath);
            if (!xpath.matches(".*\\[.*")) {
                if (simpleXmlCell2 != null) {
                    final XSSFCell cell = simpleXmlCell2.getReferencedCell();
                    if (cell != null) {
                        final Node currentNode = this.getNodeByXPath(xpath, doc.getFirstChild(), doc, false);
                        final STXmlDataType.Enum dataType = simpleXmlCell2.getXmlDataType();
                        this.mapCellOnNode(cell, currentNode, dataType);
                    }
                }
                if (table2 == null) {
                    continue;
                }
                final List<XSSFXmlColumnPr> tableColumns = table2.getXmlColumnPrs();
                final XSSFSheet sheet = table2.getXSSFSheet();
                int startRow = table2.getStartCellReference().getRow();
                ++startRow;
                for (int endRow = table2.getEndCellReference().getRow(), i = startRow; i <= endRow; ++i) {
                    final XSSFRow row = sheet.getRow(i);
                    final Node tableRootNode = this.getNodeByXPath(table2.getCommonXpath(), doc.getFirstChild(), doc, true);
                    int j;
                    for (short startColumnIndex = (short)(j = table2.getStartCellReference().getCol()); j <= table2.getEndCellReference().getCol(); ++j) {
                        final XSSFCell cell2 = row.getCell(j);
                        if (cell2 != null) {
                            final XSSFXmlColumnPr pointer = tableColumns.get(j - startColumnIndex);
                            final String localXPath = pointer.getLocalXPath();
                            final Node currentNode2 = this.getNodeByXPath(localXPath, tableRootNode, doc, false);
                            final STXmlDataType.Enum dataType2 = pointer.getXmlDataType();
                            this.mapCellOnNode(cell2, currentNode2, dataType2);
                        }
                    }
                }
            }
        }
        boolean isValid = true;
        if (validate) {
            isValid = this.isValid(doc);
        }
        if (isValid) {
            final TransformerFactory transfac = TransformerFactory.newInstance();
            final Transformer trans = transfac.newTransformer();
            trans.setOutputProperty("omit-xml-declaration", "yes");
            trans.setOutputProperty("indent", "yes");
            trans.setOutputProperty("encoding", encoding);
            final StreamResult result = new StreamResult(os);
            final DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
        }
    }
    
    private boolean isValid(final Document xml) throws SAXException {
        boolean isValid = false;
        try {
            final String language = "http://www.w3.org/2001/XMLSchema";
            final SchemaFactory factory = SchemaFactory.newInstance(language);
            final Source source = new DOMSource(this.map.getSchema());
            final Schema schema = factory.newSchema(source);
            final Validator validator = schema.newValidator();
            validator.validate(new DOMSource(xml));
            isValid = true;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return isValid;
    }
    
    private void mapCellOnNode(final XSSFCell cell, final Node node, final STXmlDataType.Enum outputDataType) {
        String value = "";
        switch (cell.getCellType()) {
            case 1: {
                value = cell.getStringCellValue();
                break;
            }
            case 4: {
                value += cell.getBooleanCellValue();
                break;
            }
            case 5: {
                value = cell.getErrorCellString();
                break;
            }
            case 2: {
                value = cell.getStringCellValue();
                break;
            }
            case 0: {
                value += cell.getRawValue();
                break;
            }
        }
        if (node instanceof Element) {
            final Element currentElement = (Element)node;
            currentElement.setTextContent(value);
        }
        else {
            node.setNodeValue(value);
        }
    }
    
    private String removeNamespace(final String elementName) {
        return elementName.matches(".*:.*") ? elementName.split(":")[1] : elementName;
    }
    
    private Node getNodeByXPath(final String xpath, final Node rootNode, final Document doc, final boolean createMultipleInstances) {
        final String[] xpathTokens = xpath.split("/");
        Node currentNode = rootNode;
        for (int i = 2; i < xpathTokens.length; ++i) {
            final String axisName = this.removeNamespace(xpathTokens[i]);
            if (!axisName.startsWith("@")) {
                final NodeList list = currentNode.getChildNodes();
                Node selectedNode = null;
                if (!createMultipleInstances || i != xpathTokens.length - 1) {
                    selectedNode = this.selectNode(axisName, list);
                }
                if (selectedNode == null) {
                    selectedNode = this.createElement(doc, currentNode, axisName);
                }
                currentNode = selectedNode;
            }
            else {
                final Node attribute = currentNode = this.createAttribute(doc, currentNode, axisName);
            }
        }
        return currentNode;
    }
    
    private Node createAttribute(final Document doc, final Node currentNode, final String axisName) {
        final String attributeName = axisName.substring(1);
        final NamedNodeMap attributesMap = currentNode.getAttributes();
        Node attribute = attributesMap.getNamedItem(attributeName);
        if (attribute == null) {
            attribute = doc.createAttributeNS("", attributeName);
            attributesMap.setNamedItem(attribute);
        }
        return attribute;
    }
    
    private Node createElement(final Document doc, final Node currentNode, final String axisName) {
        Node selectedNode;
        if (this.isNamespaceDeclared()) {
            selectedNode = doc.createElementNS(this.getNamespace(), axisName);
        }
        else {
            selectedNode = doc.createElementNS("", axisName);
        }
        currentNode.appendChild(selectedNode);
        return selectedNode;
    }
    
    private Node selectNode(final String axisName, final NodeList list) {
        Node selectedNode = null;
        for (int j = 0; j < list.getLength(); ++j) {
            final Node node = list.item(j);
            if (node.getNodeName().equals(axisName)) {
                selectedNode = node;
                break;
            }
        }
        return selectedNode;
    }
    
    private boolean isNamespaceDeclared() {
        final String schemaNamespace = this.getNamespace();
        return schemaNamespace != null && !schemaNamespace.equals("");
    }
    
    private String getNamespace() {
        return this.map.getCTSchema().getNamespace();
    }
    
    public int compare(final String leftXpath, final String rightXpath) {
        int result = 0;
        final Node xmlSchema = this.map.getSchema();
        final String[] leftTokens = leftXpath.split("/");
        final String[] rightTokens = rightXpath.split("/");
        final int minLenght = (leftTokens.length < rightTokens.length) ? leftTokens.length : rightTokens.length;
        Node localComplexTypeRootNode = xmlSchema;
        for (int i = 1; i < minLenght; ++i) {
            final String leftElementName = leftTokens[i];
            final String rightElementName = rightTokens[i];
            if (leftElementName.equals(rightElementName)) {
                final Node complexType = localComplexTypeRootNode = this.getComplexTypeForElement(leftElementName, xmlSchema, localComplexTypeRootNode);
            }
            else {
                final int leftIndex = this.indexOfElementInComplexType(leftElementName, localComplexTypeRootNode);
                final int rightIndex = this.indexOfElementInComplexType(rightElementName, localComplexTypeRootNode);
                if (leftIndex != -1 && rightIndex != -1) {
                    if (leftIndex < rightIndex) {
                        result = -1;
                    }
                    if (leftIndex > rightIndex) {
                        result = 1;
                    }
                }
            }
        }
        return result;
    }
    
    private int indexOfElementInComplexType(final String elementName, final Node complexType) {
        final NodeList list = complexType.getChildNodes();
        int indexOf = -1;
        for (int i = 0; i < list.getLength(); ++i) {
            final Node node = list.item(i);
            if (node instanceof Element && node.getLocalName().equals("element")) {
                final Node nameAttribute = node.getAttributes().getNamedItem("name");
                if (nameAttribute.getNodeValue().equals(this.removeNamespace(elementName))) {
                    indexOf = i;
                    break;
                }
            }
        }
        return indexOf;
    }
    
    private Node getComplexTypeForElement(final String elementName, final Node xmlSchema, final Node localComplexTypeRootNode) {
        Node complexTypeNode = null;
        final String elementNameWithoutNamespace = this.removeNamespace(elementName);
        final NodeList list = localComplexTypeRootNode.getChildNodes();
        String complexTypeName = "";
        for (int i = 0; i < list.getLength(); ++i) {
            final Node node = list.item(i);
            if (node instanceof Element && node.getLocalName().equals("element")) {
                final Node nameAttribute = node.getAttributes().getNamedItem("name");
                if (nameAttribute.getNodeValue().equals(elementNameWithoutNamespace)) {
                    final Node complexTypeAttribute = node.getAttributes().getNamedItem("type");
                    if (complexTypeAttribute != null) {
                        complexTypeName = complexTypeAttribute.getNodeValue();
                        break;
                    }
                }
            }
        }
        if (!"".equals(complexTypeName)) {
            final NodeList complexTypeList = xmlSchema.getChildNodes();
            for (int j = 0; j < complexTypeList.getLength(); ++j) {
                final Node node2 = list.item(j);
                if (node2 instanceof Element && node2.getLocalName().equals("complexType")) {
                    final Node nameAttribute2 = node2.getAttributes().getNamedItem("name");
                    if (nameAttribute2.getNodeValue().equals(complexTypeName)) {
                        final NodeList complexTypeChildList = node2.getChildNodes();
                        for (int k = 0; k < complexTypeChildList.getLength(); ++k) {
                            final Node sequence = complexTypeChildList.item(k);
                            if (sequence instanceof Element && sequence.getLocalName().equals("sequence")) {
                                complexTypeNode = sequence;
                                break;
                            }
                        }
                        if (complexTypeNode != null) {
                            break;
                        }
                    }
                }
            }
        }
        return complexTypeNode;
    }
}
