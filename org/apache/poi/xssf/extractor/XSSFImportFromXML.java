// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.extractor;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Element;
import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFCell;
import java.util.Iterator;
import javax.xml.xpath.XPath;
import java.util.List;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import org.apache.poi.xssf.usermodel.helpers.XSSFXmlColumnPr;
import org.w3c.dom.NodeList;
import org.apache.poi.xssf.usermodel.XSSFTable;
import javax.xml.xpath.XPathConstants;
import org.w3c.dom.Node;
import org.apache.poi.xssf.usermodel.helpers.XSSFSingleXmlCell;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPathFactory;
import java.io.Reader;
import org.xml.sax.InputSource;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.poi.util.POILogger;
import org.apache.poi.xssf.usermodel.XSSFMap;

public class XSSFImportFromXML
{
    private final XSSFMap _map;
    private static POILogger logger;
    
    public XSSFImportFromXML(final XSSFMap map) {
        this._map = map;
    }
    
    public void importFromXML(final String xmlInputString) throws SAXException, XPathExpressionException, ParserConfigurationException, IOException {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final Document doc = builder.parse(new InputSource(new StringReader(xmlInputString.trim())));
        final List<XSSFSingleXmlCell> singleXmlCells = this._map.getRelatedSingleXMLCell();
        final List<XSSFTable> tables = this._map.getRelatedTables();
        final XPathFactory xpathFactory = XPathFactory.newInstance();
        final XPath xpath = xpathFactory.newXPath();
        xpath.setNamespaceContext(new DefaultNamespaceContext(doc));
        for (final XSSFSingleXmlCell singleXmlCell : singleXmlCells) {
            final String xpathString = singleXmlCell.getXpath();
            final Node result = (Node)xpath.evaluate(xpathString, doc, XPathConstants.NODE);
            final String textContent = result.getTextContent();
            XSSFImportFromXML.logger.log(POILogger.DEBUG, "Extracting with xpath " + xpathString + " : value is '" + textContent + "'");
            final XSSFCell cell = singleXmlCell.getReferencedCell();
            XSSFImportFromXML.logger.log(POILogger.DEBUG, "Setting '" + textContent + "' to cell " + cell.getColumnIndex() + "-" + cell.getRowIndex() + " in sheet " + cell.getSheet().getSheetName());
            cell.setCellValue(textContent);
        }
        for (final XSSFTable table : tables) {
            final String commonXPath = table.getCommonXpath();
            final NodeList result2 = (NodeList)xpath.evaluate(commonXPath, doc, XPathConstants.NODESET);
            final int rowOffset = table.getStartCellReference().getRow() + 1;
            final int columnOffset = table.getStartCellReference().getCol() - 1;
            for (int i = 0; i < result2.getLength(); ++i) {
                for (final XSSFXmlColumnPr xmlColumnPr : table.getXmlColumnPrs()) {
                    final int localColumnId = (int)xmlColumnPr.getId();
                    final int rowId = rowOffset + i;
                    final int columnId = columnOffset + localColumnId;
                    String localXPath = xmlColumnPr.getLocalXPath();
                    localXPath = localXPath.substring(localXPath.substring(1).indexOf(47) + 1);
                    final String nodeXPath = commonXPath + "[" + (i + 1) + "]" + localXPath;
                    final String value = (String)xpath.evaluate(nodeXPath, result2.item(i), XPathConstants.STRING);
                    XSSFImportFromXML.logger.log(POILogger.DEBUG, "Extracting with xpath " + nodeXPath + " : value is '" + value + "'");
                    XSSFRow row = table.getXSSFSheet().getRow(rowId);
                    if (row == null) {
                        row = table.getXSSFSheet().createRow(rowId);
                    }
                    XSSFCell cell2 = row.getCell(columnId);
                    if (cell2 == null) {
                        cell2 = row.createCell(columnId);
                    }
                    XSSFImportFromXML.logger.log(POILogger.DEBUG, "Setting '" + value + "' to cell " + cell2.getColumnIndex() + "-" + cell2.getRowIndex() + " in sheet " + table.getXSSFSheet().getSheetName());
                    cell2.setCellValue(value.trim());
                }
            }
        }
    }
    
    static {
        XSSFImportFromXML.logger = POILogFactory.getLogger(XSSFImportFromXML.class);
    }
    
    private static final class DefaultNamespaceContext implements NamespaceContext
    {
        private final Element _docElem;
        
        public DefaultNamespaceContext(final Document doc) {
            this._docElem = doc.getDocumentElement();
        }
        
        public String getNamespaceURI(final String prefix) {
            return this.getNamespaceForPrefix(prefix);
        }
        
        private String getNamespaceForPrefix(final String prefix) {
            if (prefix.equals("xml")) {
                return "http://www.w3.org/XML/1998/namespace";
            }
            Node parent = this._docElem;
            while (parent != null) {
                final int type = parent.getNodeType();
                if (type == 1) {
                    if (parent.getNodeName().startsWith(prefix + ":")) {
                        return parent.getNamespaceURI();
                    }
                    final NamedNodeMap nnm = parent.getAttributes();
                    for (int i = 0; i < nnm.getLength(); ++i) {
                        final Node attr = nnm.item(i);
                        final String aname = attr.getNodeName();
                        final boolean isPrefix = aname.startsWith("xmlns:");
                        if (isPrefix || aname.equals("xmlns")) {
                            final int index = aname.indexOf(58);
                            final String p = isPrefix ? aname.substring(index + 1) : "";
                            if (p.equals(prefix)) {
                                return attr.getNodeValue();
                            }
                        }
                    }
                    parent = parent.getParentNode();
                }
                else {
                    if (type == 5) {
                        continue;
                    }
                    break;
                }
            }
            return null;
        }
        
        public Iterator getPrefixes(final String val) {
            return null;
        }
        
        public String getPrefix(final String uri) {
            return null;
        }
    }
}
